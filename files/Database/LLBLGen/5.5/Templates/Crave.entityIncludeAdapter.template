<[If Not IsInHierarchyType None]>
		/// <summary>Gets a predicateexpression which filters on this entity. Only useful in entity fetches</summary>
		/// <param name="negate">Optional flag to produce a NOT filter, (true), or a normal filter (false, default). </param>
		/// <returns>ready to use predicateexpression</returns>
		public <[If IsSubType]>new<[EndIf]> static IPredicateExpression GetEntityTypeFilter(bool negate=false) { return ModelInfoProviderSingleton.GetInstance().GetEntityTypeFilter("<[CurrentEntityName]>Entity", negate); }
<[EndIf]><[Foreach UniqueConstraint]>
		/// <summary>Method which will construct a filter (predicate expression) for the unique constraint defined on the fields: <[Foreach UniqueConstraintEntityField Comma]><[EntityFieldName]> <[NextForeach]>.</summary>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public IPredicateExpression ConstructFilterForUC<[Foreach UniqueConstraintEntityField]><[EntityFieldName]><[NextForeach]>()
		{
			var filter = new PredicateExpression();
<[Foreach UniqueConstraintEntityField]>			filter.Add(<[RootNamespace]>.HelperClasses.<[CurrentEntityName]>Fields.<[EntityFieldName]> == this.Fields.GetCurrentValue((int)<[CurrentEntityName]>FieldIndex.<[EntityFieldName]>));
<[NextForeach]> 			return filter;
		}
<[NextForeach]><[Foreach RelatedEntity OneToMany]><[If Not MappedFieldRelationIsHidden]>
		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type '<[RelatedEntityName]>' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfo<[MappedFieldNameRelation]>() { return CreateRelationInfoForNavigator("<[MappedFieldNameRelation]>"); }
<[EndIf]><[NextForeach]><[Foreach RelatedEntity ManyToMany]><[If Not MappedFieldRelationIsHidden]>
		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type '<[RelatedEntityName]>' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfo<[MappedFieldNameRelation]>() { return CreateRelationInfoForNavigator("<[MappedFieldNameRelation]>"); }
<[EndIf]><[NextForeach]><[Foreach RelatedEntity ManyToOne]><[If Not MappedFieldRelationIsHidden]>
		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type '<[RelatedEntityName]>' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfo<[MappedFieldNameRelation]>() { return CreateRelationInfoForNavigator("<[MappedFieldNameRelation]>"); }
<[EndIf]><[NextForeach]><[Foreach RelatedEntity OneToOne]><[If Not MappedFieldRelationIsHidden]>
		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type '<[RelatedEntityName]>' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfo<[MappedFieldNameRelation]>() { return CreateRelationInfoForNavigator("<[MappedFieldNameRelation]>"); }
<[EndIf]><[NextForeach]>		
		/// <inheritdoc/>
		protected override EntityStaticMetaDataBase GetEntityStaticMetaData() {	return _staticMetaData; }

		/// <summary>Initializes the class members</summary>
		private void InitClassMembers()
		{
<[If Not IsSubType]>			PerformDependencyInjection();<[EndIf]><[ UserCodeRegion "InitClassMembers" ]>
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
<[ EndUserCodeRegion ]>
<[If Not IsSubType]>			OnInitClassMembersComplete();
<[EndIf]>		}
<[If HasCustomProperty]>
		/// <summary>Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
<[If HasCustomProperty Entity]>			_customProperties = new Dictionary<string, string>();
<[Foreach CustomProperty Entity]>			_customProperties.Add("<[CustomPropertyName]>", @"<[CustomPropertyValue]>");
<[NextForeach]><[EndIf]><[If HasCustomProperty EntityFields]>			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
<[Foreach EntityField]><[If HasCustomProperty EntityField]>			fieldHashtable = new Dictionary<string, string>();
<[Foreach CustomProperty EntityField]>			fieldHashtable.Add("<[CustomPropertyName]>", @"<[CustomPropertyValue]>");
<[NextForeach]>			_fieldsCustomProperties.Add("<[EntityFieldName]>", fieldHashtable);
<[EndIf]><[NextForeach]><[EndIf]>		}
<[EndIf]><[Foreach RelatedEntity ManyToOne]><[If Not MappedFieldRelationIsHidden]><[If HasForf]>
		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void On<[MappedFieldNameRelation]>PropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{<[ Foreach RelatedEntityField CurrentRelatedEntity]>
				case "<[RelatedEntityFieldName]>":
					this.OnPropertyChanged("<[MappedFieldNameRelatedField]>");
					break;<[NextForeach]>
			}
		}
<[EndIf]><[EndIf]><[NextForeach]><[Foreach RelatedEntity OneToOne]><[If Not MappedFieldRelationIsHidden]><[If HasForf]>
		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void On<[MappedFieldNameRelation]>PropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{<[ Foreach RelatedEntityField CurrentRelatedEntity]>
				case "<[RelatedEntityFieldName]>":
					this.OnPropertyChanged("<[MappedFieldNameRelatedField]>");
					break;<[NextForeach]>
			}
		}
<[EndIf]><[EndIf]><[NextForeach]><[If IsSubType]>
		/// <summary>Initializes the class with empty data, as if it is a new Entity.</summary>
		private void InitClassEmpty()
		{
<[Else]>
		/// <summary>Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this <[CurrentEntityName]>Entity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
<[EndIf]>			InitClassMembers();<[If IsInHierarchyType TargetPerEntityHierarchy]>
			if(this.Fields.State==EntityState.New)
			{
				this.Fields.ForcedValueWrite((int)<[CurrentEntityName]>FieldIndex.<[DiscriminatorColumnName]>, <[DiscriminatorValue]>);
			}<[EndIf]><[ UserCodeRegion "InitClassEmpty" ]>
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END
<[ EndUserCodeRegion ]>
<# Custom_EntityInitializationTemplate #>
<[If Not IsSubType]>			OnInitialized();
<[EndIf]>		}

		/// <summary>The relations object holding all relations of this entity with other entity classes.</summary>
		public <[If IsSubType]>new <[EndIf]>static <[CurrentEntityName]>Relations Relations { get { return _relationsFactory; } }
<[If HasCustomProperty Entity]>
		/// <summary>The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public <[If IsSubType]>new<[EndIf]> static Dictionary<string, string> CustomProperties { get { return _customProperties;} }
<[EndIf]><[Foreach RelatedEntity OneToMany]><[If Not MappedFieldRelationIsHidden]>
		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type '<[RelatedEntityName]>' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPath<[MappedFieldNameRelation]> { get { return _staticMetaData.GetPrefetchPathElement("<[MappedFieldNameRelation]>", CommonEntityBase.CreateEntityCollection<<[RelatedEntityName]>Entity>()); } }
<[EndIf]><[NextForeach]><[Foreach RelatedEntity ManyToMany]><[If Not MappedFieldRelationIsHidden]>
		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type '<[RelatedEntityName]>' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPath<[MappedFieldNameRelation]> { get { return _staticMetaData.GetPrefetchPathElement("<[MappedFieldNameRelation]>", CommonEntityBase.CreateEntityCollection<<[RelatedEntityName]>Entity>()); } }
<[EndIf]><[NextForeach]><[Foreach RelatedEntity ManyToOne]><[If Not MappedFieldRelationIsHidden]>
		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type '<[RelatedEntityName]>' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPath<[MappedFieldNameRelation]>Entity { get { return _staticMetaData.GetPrefetchPathElement("<[MappedFieldNameRelation]>", CommonEntityBase.CreateEntityCollection<<[RelatedEntityName]>Entity>()); } }
<[EndIf]><[NextForeach]><[Foreach RelatedEntity OneToOne]><[If Not MappedFieldRelationIsHidden]>
		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type '<[RelatedEntityName]>' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPath<[MappedFieldNameRelation]>Entity { get { return _staticMetaData.GetPrefetchPathElement("<[MappedFieldNameRelation]>", CommonEntityBase.CreateEntityCollection<<[RelatedEntityName]>Entity>()); } }
<[EndIf]><[NextForeach]><[If HasCustomProperty Entity]>
		/// <inheritdoc/>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType { get { return CustomProperties;} }
<[EndIf]><[If HasCustomProperty EntityFields]>
		/// <summary>The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public <[If IsSubType]>new<[EndIf]> static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties { get { return _fieldsCustomProperties;} }

		/// <inheritdoc/>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType { get { return FieldsCustomProperties;} }
<[EndIf]><[Foreach EntityField]>
		/// <summary>The <[EntityFieldName]> property of the Entity <[CurrentEntityName]><br/><br/><[Foreach CustomProperty EntityField]><[CustomPropertyName]>: <[CustomPropertyValue]><br/><[NextForeach]><[Description]></summary>
		/// <remarks>Mapped on  <[ CaseCamel TargetType ]> field: "<[SourceObjectName]>"."<[SourceColumnName]>".<br/><[ TargetType ]> field type characteristics (type, precision, scale, length): <[SourceColumnDbType]>, <[SourceColumnPrecision]>, <[SourceColumnScale]>, <[SourceColumnMaxLength]>.<br/><[ TargetType ]> field behavior characteristics (is nullable, is PK, is identity): <[SourceColumnIsNullable]>, <[IsPrimaryKey]>, <[IsIdentity]></remarks><[Foreach Attribute Field]>
		<[Attribute]><[NextForeach]>
		<[ If SettingValueEquals Field "FieldPropertyIsPublic" "true"]>public<[Else]>internal<[EndIf]> virtual <[If GenerateAsNullableType]>Nullable<<[TypeOfField]>><[Else]><[TypeOfField]><[EndIf]> <[EntityFieldName]>
		{
			get { return (<[If GenerateAsNullableType]>Nullable<<[TypeOfField]>><[Else]><[TypeOfField]><[EndIf]>)GetValue((int)<[CurrentEntityName]>FieldIndex.<[EntityFieldName]>, <[If GenerateAsNullableType]>false<[Else]>true<[EndIf]>); }
<[If IsReadOnly ]><[If IsPrimaryKey ]>			set { SetValue((int)<[CurrentEntityName]>FieldIndex.<[EntityFieldName]>, value); }<[EndIf]><[Else]>			set	{ SetValue((int)<[CurrentEntityName]>FieldIndex.<[EntityFieldName]>, value); }
<[EndIf]>		}
<[NextForeach]><[Foreach RelatedEntity OneToMany]><[If Not MappedFieldRelationIsHidden]>
		/// <summary>Gets the EntityCollection with the related entities of type '<[RelatedEntityName]>Entity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/><[Foreach CustomProperty MappedFieldNameRelation]><[CustomPropertyName]>: <[CustomPropertyValue]><br/><[NextForeach]></summary>
		[TypeContainedAttribute(typeof(<[RelatedEntityName]>Entity))]<[Foreach Attribute MappedFieldNameRelation]>
		<[Attribute]><[NextForeach]>
		<[ If SettingValueEquals MappedFieldNameRelation "NavigatorPropertyIsPublic" "true"]>public<[Else]>internal<[EndIf]> virtual EntityCollection<<[RelatedEntityName]>Entity> <[MappedFieldNameRelation]> { get { return GetOrCreateEntityCollection<<[RelatedEntityName]>Entity, <[RelatedEntityName]>EntityFactory>("<[RelatedMappedFieldNameRelation]>", <[ If OppositeRelationPresent ]>true<[Else]>false<[EndIf]>, false, ref _<[CaseCamel MappedFieldNameRelation]>); } }
<[EndIf]><[NextForeach]><[Foreach RelatedEntity ManyToMany]><[If Not MappedFieldRelationIsHidden]>
		/// <summary>Gets the EntityCollection with the related entities of type '<[RelatedEntityName]>Entity' which are related to this entity via a relation of type 'm:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/><[Foreach CustomProperty MappedFieldNameRelation]><[CustomPropertyName]>: <[CustomPropertyValue]><br/><[NextForeach]></summary>
		[TypeContainedAttribute(typeof(<[RelatedEntityName]>Entity))]<[Foreach Attribute MappedFieldNameRelation]>
		<[Attribute]><[NextForeach]>
		<[ If SettingValueEquals MappedFieldNameRelation "NavigatorPropertyIsPublic" "true"]>public<[Else]>internal<[EndIf]> virtual EntityCollection<<[RelatedEntityName]>Entity> <[MappedFieldNameRelation]> { get { return GetOrCreateEntityCollection<<[RelatedEntityName]>Entity, <[RelatedEntityName]>EntityFactory>("<[RelatedMappedFieldNameRelation]>", false, true, ref _<[CaseCamel MappedFieldNameRelation]>); } }
<[EndIf]><[NextForeach]><[Foreach RelatedEntity ManyToOne]><[If Not MappedFieldRelationIsHidden]>
		/// <summary>Gets / sets related entity of type '<[RelatedEntityName]>Entity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/><[Foreach CustomProperty MappedFieldNameRelation]><[CustomPropertyName]>: <[CustomPropertyValue]><br/><[NextForeach]></summary><[Foreach Attribute MappedFieldNameRelation]>
		<[Attribute]><[NextForeach]>
		<[ If SettingValueEquals MappedFieldNameRelation "NavigatorPropertyIsPublic" "true"]>public<[Else]>internal<[EndIf]> virtual <[RelatedEntityName]>Entity <[MappedFieldNameRelation]>
		{
			get { return _<[CaseCamel MappedFieldNameRelation]>; }
			set { SetSingleRelatedEntityNavigator(value, "<[MappedFieldNameRelation]>"); }
		}
<[EndIf]><[NextForeach]><[Foreach RelatedEntity OneToOne]><[If Not MappedFieldRelationIsHidden]>
		/// <summary>Gets / sets related entity of type '<[RelatedEntityName]>Entity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned.<br/><br/><[Foreach CustomProperty MappedFieldNameRelation]><[CustomPropertyName]>: <[CustomPropertyValue]><br/><[NextForeach]></summary><[Foreach Attribute MappedFieldNameRelation]>
		<[Attribute]><[NextForeach]>
		<[ If SettingValueEquals MappedFieldNameRelation "NavigatorPropertyIsPublic" "true"]>public<[Else]>internal<[EndIf]> virtual <[RelatedEntityName]>Entity <[MappedFieldNameRelation]>
		{
			get { return _<[CaseCamel MappedFieldNameRelation]>; }
			set { SetSingleRelatedEntityNavigator(value, "<[MappedFieldNameRelation]>"); }
		}
<[EndIf]><[NextForeach]><[ Foreach RelatedEntityField]> 
		/// <summary>Gets <[ If Not IsReadOnly ]>/ Sets <[ EndIf ]>the value of the related field this.<[ MappedFieldNameRelation ]>.<[ RelatedEntityFieldName ]>.<br/><br/><[Foreach CustomProperty RelatedEntityField]><[CustomPropertyName]>: <[CustomPropertyValue]><br/><[NextForeach]><[Description]></summary><[Foreach Attribute RelatedEntityField]>
		<[Attribute]><[NextForeach]>
		public virtual <[If GenerateAsNullableType]>Nullable<<[TypeOfField]>><[Else]><[TypeOfField]><[EndIf]> <[ MappedFieldNameRelatedField ]>
		{
			get { return this.<[ MappedFieldNameRelation ]>==null ? (<[If GenerateAsNullableType]>Nullable<<[TypeOfField]>><[Else]><[TypeOfField]><[EndIf]>)TypeDefaultValue.GetDefaultValue(typeof(<[If GenerateAsNullableType]>Nullable<<[TypeOfField]>><[Else]><[TypeOfField]><[EndIf]>)) : this.<[ MappedFieldNameRelation ]>.<[ RelatedEntityFieldName ]>; }
<[ If Not IsReadOnly ]>			set
			{
				<[RelatedEntityName]>Entity relatedEntity = this.<[ MappedFieldNameRelation ]>;
				if(relatedEntity!=null)
				{
					relatedEntity.<[ RelatedEntityFieldName ]> = value;
				}				
			}
<[ EndIf ]>		}<[ NextForeach ]>