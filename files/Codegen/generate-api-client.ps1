Write-Output "Working dir: $PSScriptRoot"

$MainDir = $PSScriptRoot
$ApiUrl = "https://localhost:44355"
$ApiSwaggerUrl = "$ApiUrl/swagger/v1/swagger.json"

Write-Output "Installing NSwag as global NPM package"
npm install -g nswag@latest

Write-Output "`nBuilding & Starting Ordering API (port 54321)"
$webServerStartArgs = "run --project $MainDir\..\..\src\Apps\Api\Crave.Ordering.Api.WebApplication\Crave.Ordering.Api.WebApplication.csproj --urls=$ApiUrl"
$process = Start-Process dotnet -ArgumentList $webServerStartArgs -PassThru

Write-Output "`nWaiting for API to be available"
$counter = 0
$isApiOnline = $false
while($counter -le 10) {     
    try {
        $response = Invoke-WebRequest -TimeoutSec 5 -Uri $ApiSwaggerUrl -Method Head
        if ($response.StatusCode -eq 200) {
            Write-Output "API is online!"
            $isApiOnline = $true
            break
        } else {
            Write-Output "Online but wrong status code: $response.StatusCode"
            break
        }
    } catch {
        Write-Output "Not yet..."
        Start-Sleep 2
    } 
    $counter++
}

if ($isApiOnline) {
    Write-Output "`nGenerating code using NSwag..."
    nswag run "$PSScriptRoot\ordering-api.nswag"
} else {
    # Print job output
    Receive-Job -Name ApiApp
}

Write-Output "`nStopping Api Process"
Stop-Process $process

Write-Output "`nFinished"