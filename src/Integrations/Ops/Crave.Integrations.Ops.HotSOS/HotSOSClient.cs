﻿using System;
using System.ServiceModel;
using System.Threading.Tasks;
using Crave.Integrations.Ops.Interfaces;
using Crave.Integrations.Ops.Models;
using Crave.Integrations.Ops.Results;
using TechApi;

namespace Crave.Integrations.Ops.HotSOS
{
    public class HotSOSClient : IOpsClient
    {
        private readonly HotSOSApi api;

        public HotSOSClient(HotSOSConfiguration configuration)
        {
            api = new HotSOSApi(configuration.ApiUrl, configuration.Username, configuration.Password);
        }

        public async Task<OpsResult<ExternalOrder>> SubmitServiceOrder(ExternalOrder externalOrder)
        {
            try
            {
                ServiceOrder serviceOrder = await api.CreateServiceOrder(new ServiceOrder 
                {
                    Location = new Room { ID = externalOrder.ExternalDeliverypoint?.Id },
                    Issue = new Issue { ID = externalOrder.ExternalProduct?.Id }
                });

                externalOrder = new ExternalOrder {ExternalId = serviceOrder.ID};

                return OpsResult<ExternalOrder>.SuccessResult(externalOrder);
            }
            catch (FaultException<apiFault> faultException)
            {
                return OpsResult<ExternalOrder>.ExceptionResult(faultException);
            }
            catch (Exception exception)
            {
                return OpsResult<ExternalOrder>.ExceptionResult(exception);
            }
        }
    }
}
