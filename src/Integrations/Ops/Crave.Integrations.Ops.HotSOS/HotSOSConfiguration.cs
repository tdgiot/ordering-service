﻿using System;

namespace Crave.Integrations.Ops.HotSOS
{
    public class HotSOSConfiguration
    {
        public HotSOSConfiguration(string username,
                                   string password,
                                   Uri apiUrl)
        {
            this.Username = username;
            this.Password = password;
            this.ApiUrl = apiUrl;
        }

        public string Username { get; }
        public string Password { get; }
        public Uri ApiUrl { get; }
    }
}
