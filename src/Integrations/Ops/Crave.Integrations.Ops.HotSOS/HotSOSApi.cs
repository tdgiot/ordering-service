﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Threading.Tasks;
using TechApi;

namespace Crave.Integrations.Ops.HotSOS
{
    public class HotSOSApi
    {
        private readonly Uri apiUrl;
        private readonly string username;
        private readonly string password;

        public HotSOSApi(Uri apiUrl,
                         string username,
                         string password)
        {
            this.apiUrl = apiUrl;
            this.username = username;
            this.password = password;
        }

        public async Task<ServiceOrder> CreateServiceOrder(ServiceOrder serviceOrder)
        {
            MTechAPIClient client = CreateClient();
            sendResponse response = await client.sendAsync(serviceOrder);

            if (!(response.sendResult is ServiceOrder serviceOrderResult))
            {
                throw new InvalidOperationException($"Unexpected result. (Result={response.sendResult})");
            }

            return serviceOrderResult;
            
        }

        public async Task<IEnumerable<Room>> GetRooms()
        {
            GetRoomCollection request = new GetRoomCollection();
            request.nameLike = "%";

            MTechAPIClient client = CreateClient();
            getResponse getResponse = await client.getAsync(request);

            ApiObjectCollection? objectResponse = getResponse.getResult as ApiObjectCollection;

            Dictionary<string, Room> rooms = new Dictionary<string, Room>();

            if (objectResponse?.Items != null && objectResponse.Items.Length > 0)
            {
                foreach (apiObject o in objectResponse.Items)
                {
                    Room room = (Room)o;

                    if (!rooms.ContainsKey(room.ID))
                    {
                        rooms.Add(room.ID, room);
                    }
                }
            }

            return rooms.Values;
        }

        private MTechAPIClient CreateClient()
        {
            TimeSpan timeout = TimeSpan.FromSeconds(60);

            BasicHttpBinding basicHttpBinding = new BasicHttpBinding(BasicHttpSecurityMode.TransportWithMessageCredential);
            basicHttpBinding.Security.Message.ClientCredentialType = BasicHttpMessageCredentialType.UserName;
            basicHttpBinding.OpenTimeout = timeout;
            basicHttpBinding.CloseTimeout = timeout;
            basicHttpBinding.ReceiveTimeout = timeout;
            basicHttpBinding.SendTimeout = timeout;

            MTechAPIClient client = new MTechAPIClient(basicHttpBinding, new EndpointAddress(apiUrl));
            client.ClientCredentials.UserName.UserName = username;
            client.ClientCredentials.UserName.Password = password;

            return client;
        }
    }
}
