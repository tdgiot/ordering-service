﻿namespace Crave.Integrations.Ops.Quore.Models
{
    /// <summary>
    /// Class which represents a house keeping item from the Quore integration.
    /// </summary>
    public class HKItem
    {
        public string id { get; set; }

        public string name { get; set; }
    }
}
