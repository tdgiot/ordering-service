﻿namespace Crave.Integrations.Ops.Quore.Models
{
    /// <summary>
    /// Class which represents an area (room) from the Quore integration.
    /// </summary>
    public class Area
    {
        public string id { get; set; }

        public string name { get; set; }

        public string alt_name { get; set; }

        public string qac_id { get; set; }

        public string qac_name { get; set; }
    }
}
