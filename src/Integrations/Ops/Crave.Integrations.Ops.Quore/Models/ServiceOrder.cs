﻿namespace Crave.Integrations.Ops.Quore.Models
{
    /// <summary>
    /// Class which represents a service order from the Quore integration.
    /// </summary>
    public class ServiceOrder
    {
        public string request_id { get; set; }

        public string request_type { get; set; }

        public string ok { get; set; }

        public string push_result { get; set; }
    }
}
