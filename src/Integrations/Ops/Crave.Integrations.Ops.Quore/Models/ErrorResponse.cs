﻿namespace Crave.Integrations.Ops.Quore.Models
{
    public class ErrorResponse
    {
        [Newtonsoft.Json.JsonProperty("error")]
        public string Error { get; set; }
    }
}
