﻿using System;

namespace Crave.Integrations.Ops.Quore
{
    public class QuoreConfiguration
    {
        public QuoreConfiguration(Uri apiUrl,
                                  string apiToken)
        {
            this.ApiUrl = apiUrl;
            this.ApiToken = apiToken;
        }

        public Uri ApiUrl { get; }
        public string ApiToken { get; }
    }
}
