﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Crave.Integrations.Ops.Interfaces;
using Crave.Integrations.Ops.Models;
using Crave.Integrations.Ops.Quore.Models;
using Crave.Integrations.Ops.Results;
using RestSharp;

namespace Crave.Integrations.Ops.Quore
{
    public class QuoreClient : IOpsClient
    {
        private readonly QuoreApi api;

        public QuoreClient(QuoreConfiguration configuration)
        {
            api = new QuoreApi(configuration.ApiUrl, configuration.ApiToken);
        }

        public async Task<OpsResult<ExternalOrder>> SubmitServiceOrder(ExternalOrder externalOrder)
        {
            IRestResponse<IEnumerable<ServiceOrder>> response = api.CreateServiceOrder(externalOrder);

            if (!IsResponseSuccessful(response))
            {
                return OpsResult<ExternalOrder>.FailureResult($"Failed to submit order to Quore. ExternalDeliverypoint={externalOrder.ExternalDeliverypoint.Id}, ExternalProduct={externalOrder.ExternalProduct.Id}, StatusCode={response.StatusCode}, Content={response.Content}");
            }

            externalOrder = new ExternalOrder { ExternalId = response.Data.FirstOrDefault().request_id };

            return await Task.FromResult(OpsResult<ExternalOrder>.SuccessResult(externalOrder));
        }

        private static bool IsResponseSuccessful(IRestResponse<IEnumerable<ServiceOrder>> response)
        {
            return response.IsSuccessful && 
                   response.Data.Any() && 
                   !response.Data.FirstOrDefault().request_id.IsNullOrWhiteSpace();
        }
    }
}
