﻿using System;
using System.Collections.Generic;
using Crave.Integrations.Ops.Models;
using Crave.Integrations.Ops.Quore.Enums;
using Crave.Integrations.Ops.Quore.Models;
using RestSharp;

namespace Crave.Integrations.Ops.Quore
{
    public class QuoreApi
    {
        private readonly Uri apiUrl;
        private readonly string apiToken;

        public QuoreApi(Uri apiUrl,
                        string apiToken)
        {
            this.apiUrl = apiUrl;
            this.apiToken = apiToken;
        }

        public IRestResponse<IEnumerable<ServiceOrder>> CreateServiceOrder(ExternalOrder externalOrder)
        {
            IRestClient client = SecuredRestClient();
            IRestRequest request = new RestRequest()
                                   .AddQueryParameter("action", "addServiceRequestByAreaName")
                                   .AddQueryParameter("area_name", externalOrder.ExternalDeliverypoint.Name)
                                   .AddQueryParameter("item_id", externalOrder.ExternalProduct.Id)
                                   .AddQueryParameter("where", HKWhere.BringIntoTheRoom.ToIntString())
                                   .AddQueryParameter("when", HKWhen.AsSoonAsPossible.ToIntString());

            return client.Post<IEnumerable<ServiceOrder>>(request);
        }

        public IRestResponse<IEnumerable<HKItem>> GetItems()
        {
            IRestClient client = SecuredRestClient();
            IRestRequest request = new RestRequest()
                .AddQueryParameter("action", "getHKItems");

            return client.Post<IEnumerable<HKItem>>(request);
        }

        public IRestResponse<IEnumerable<Area>> GetAreas()
        {
            IRestClient client = SecuredRestClient();
            IRestRequest request = new RestRequest()
                .AddQueryParameter("action", "getAreas");

            return client.Post<IEnumerable<Area>>(request);
        }

        private IRestClient SecuredRestClient()
        {
            IRestClient client = new RestClient(apiUrl);
            client.AddDefaultHeader("Authorization", $"OAuth oauth_token={apiToken}");
            client.AddDefaultHeader("Version", "2");
            return client;
        }
    }
}
