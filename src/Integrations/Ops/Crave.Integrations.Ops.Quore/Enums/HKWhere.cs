﻿namespace Crave.Integrations.Ops.Quore.Enums
{
    /// <summary>
    /// Enumerator which represents the location indication for a housekeeping request.
    /// </summary>
    public enum HKWhere
    {
        BringIntoTheRoom = 1,
        LeaveOutsideRoom = 2,
        PickupAtFrontDesk = 3
    }
}
