﻿using System;

namespace Crave.Integrations.Ops.Hyatt
{
    public class HyattConfiguration
    {
        public HyattConfiguration(string applicationId,
                                  Uri apiUrl,
                                  string publicKey,
                                  string privateKey,
                                  string hotelId)
        {
            this.ApplicationId = applicationId;
            this.ApiUrl = apiUrl;
            this.PublicKey = publicKey;
            this.PrivateKey = privateKey;
            this.HotelId = hotelId;
        }

        public string ApplicationId { get; }
        public Uri ApiUrl { get; }
        public string PublicKey { get; }
        public string PrivateKey { get; }
        public string HotelId { get; }
    }
}
