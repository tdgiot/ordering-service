﻿using System;
using System.Security.Authentication;
using Crave.Integrations.Ops.Hyatt.Models;
using RestSharp;
using RestSharp.Authenticators;
using Authorization = Crave.Integrations.Ops.Hyatt.Models.Authorization;

namespace Crave.Integrations.Ops.Hyatt
{
    public class HyattApi
    {
        private readonly Uri apiUrl;
        private readonly string hotelId;
        private readonly string publicKey;
        private readonly string privateKey;

        public HyattApi(Uri apiUrl,
                        string hotelId,
                        string publicKey,
                        string privateKey)
        {
            this.apiUrl = apiUrl;
            this.publicKey = publicKey;
            this.privateKey = privateKey;
            this.hotelId = hotelId;
        }

        private Authorization Authenticate()
        {
            IRestClient client = AuthenticateRestClient();
            IRestRequest request = new RestRequest("/oauth/token?grant_type=client_credentials");
            IRestResponse<Authorization> response = client.Post<Authorization>(request);

            if (!response.IsSuccessful)
            {
                throw new AuthenticationException($"Failed to authenticate to Hyatt on url {apiUrl}. StatusCode={response.StatusCode}, Message={response.ErrorMessage}");
            }

            return response.Data;
        }

        public IRestResponse<ServiceOrderResponse> CreateServiceRequest(CreateServiceOrder requestBody)
        {
            Authorization authorization = Authenticate();
            
            IRestClient client = SecuredRestClient(authorization.access_token);
            IRestRequest request = new RestRequest("/v1/hotsos/property/{hotelId}/serviceOrders")
                                   .AddUrlSegment("hotelId", hotelId)
                                   .AddJsonBody(requestBody);
            
            return client.Post<ServiceOrderResponse>(request);
        }

        private IRestClient AuthenticateRestClient()
        {
            IRestClient client = new RestClient(apiUrl);
            client.Authenticator = new HttpBasicAuthenticator(publicKey, privateKey);
            return client;
        }

        private IRestClient SecuredRestClient(string accessToken)
        {
            IRestClient client = new RestClient(apiUrl);
            client.Authenticator = new OAuth2AuthorizationRequestHeaderAuthenticator(accessToken, "Bearer");
            return client;
        }
    }
}
