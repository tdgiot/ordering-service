﻿using System;
using System.Threading.Tasks;
using Crave.Integrations.Ops.Hyatt.Models;
using Crave.Integrations.Ops.Interfaces;
using Crave.Integrations.Ops.Models;
using Crave.Integrations.Ops.Results;
using Newtonsoft.Json;
using RestSharp;

namespace Crave.Integrations.Ops.Hyatt
{
    public class HyattClient : IOpsClient
    {
        private readonly HyattConfiguration configuration;
        private readonly HyattApi api;

        public HyattClient(HyattConfiguration configuration)
        {
            this.configuration = configuration;
            this.api = new HyattApi(configuration.ApiUrl, configuration.HotelId, configuration.PublicKey, configuration.PrivateKey);
        }
        
        public async Task<OpsResult<ExternalOrder>> SubmitServiceOrder(ExternalOrder externalOrder)
        {
            IRestResponse<ServiceOrderResponse> response = api.CreateServiceRequest(new CreateServiceOrder
            {
                applicationId = configuration.ApplicationId,
                issueId = externalOrder.ExternalProduct.Id,
                roomId = externalOrder.ExternalDeliverypoint.Id,
                newRemark = externalOrder.Notes
            });

            if (!response.IsSuccessful)
            {
                FailureReason? reason = HyattClient.ConvertContentToFailureReason(response.Content);

                return OpsResult<ExternalOrder>.FailureResult($"Failed to submit order to Hyatt. ExternalDeliverypoint={externalOrder.ExternalDeliverypoint.Id}, ExternalProduct={externalOrder.ExternalProduct.Id}, StatusCode={response.StatusCode}, Message={reason?.Message} ({reason?.Code})");
            }

            externalOrder = new ExternalOrder { ExternalId = response.Data.service_order.soId };

            return await Task.FromResult(OpsResult<ExternalOrder>.SuccessResult(externalOrder));
        }

        private static FailureReason? ConvertContentToFailureReason(string content)
        {
            return content.IsNullOrWhiteSpace() ? null : JsonConvert.DeserializeObject<FailureReason>(content);
        }
    }
}
