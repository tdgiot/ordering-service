﻿using System;

namespace Crave.Integrations.Ops.Hyatt.Models
{
    public class CreateServiceOrder
    {
        public string? applicationId { get; set; }
        public string? issueId { get; set; }
        public string? roomId { get; set; }

        /// <remarks>Optional</remarks>
        public string? roomNumber { get; set; }

        /// <remarks>Optional</remarks>
        public string? requestedById { get; set; }

        /// <remarks>Optional</remarks>
        public DateTime? actionTime { get; set; }

        /// <remarks>Optional</remarks>
        public string? newRemark { get; set; }

        /// <remarks>Optional</remarks>
        public string? reservationId { get; set; }

        /// <remarks>Optional</remarks>
        public string? assignedId { get; set; }

        /// <remarks>Optional</remarks>
        public int? priority { get; set; }

        /// <remarks>Optional</remarks>
        public string? trade { get; set; }
    }
}
