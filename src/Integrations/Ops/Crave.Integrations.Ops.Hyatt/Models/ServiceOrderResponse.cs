﻿namespace Crave.Integrations.Ops.Hyatt.Models
{
    public class ServiceOrderResponse
    {
        public ServiceOrder service_order { get; set; }
    }
}
