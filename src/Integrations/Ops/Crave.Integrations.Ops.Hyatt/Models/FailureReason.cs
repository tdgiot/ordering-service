﻿namespace Crave.Integrations.Ops.Hyatt.Models
{
    public class FailureReason
    {
        public int Code { get; set; }
        public string? Message { get; set; }
    }
}
