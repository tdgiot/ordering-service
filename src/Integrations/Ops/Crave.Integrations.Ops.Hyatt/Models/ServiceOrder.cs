﻿namespace Crave.Integrations.Ops.Hyatt.Models
{
    public class ServiceOrder
    {
        public string? spiritCode { get; set; }
        public string? soId { get; set; }
        public string? actionTime { get; set; }
        public string? assignedId { get; set; }
        public string? assignedDevice { get; set; }
        public string? assignedName { get; set; }
        public string? createdById { get; set; }
        public string? createdByName { get; set; }
        public string? issueId { get; set; }
        public string? issueName { get; set; }
        public string? lastEvent { get; set; }
        public string? lastModifiedTime { get; set; }
        public string? roomId { get; set; }
        public string? roomNumber { get; set; }
        public bool donotDisturbFlag { get; set; }
        public bool guestInRoom { get; set; }
        public bool makeUp { get; set; }
        public string? roomName { get; set; }
        public bool occupied { get; set; }
        public string? roomRecordId { get; set; }
        public string? roomServiceStatus { get; set; }
        public string? roomStatus { get; set; }
        public string? url { get; set; }
        public string? remark { get; set; }
        public int priority { get; set; }
        public string? requestedById { get; set; }
        public string? requestedByName { get; set; }
        public string? requestedByUserStatus { get; set; }
        public string? requestTime { get; set; }
        public string? reservationId { get; set; }
        public string? profileId { get; set; }
        public string? soStatus { get; set; }
        public string? trade { get; set; }
        public string? serviceOrderType { get; set; }
    }
}