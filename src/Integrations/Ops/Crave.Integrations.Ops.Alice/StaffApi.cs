﻿using System;
using Crave.Integrations.Ops.Alice.Requests;
using Crave.Integrations.Ops.Alice.Responses;
using RestSharp;
using RestSharp.Authenticators;
using RestSharp.Serializers.NewtonsoftJson;

namespace Crave.Integrations.Ops.Alice
{
    public class StaffApi
    {
        private readonly Uri apiUrl;
        private readonly string apiKey;
        private readonly string hotelId;
        private readonly string username;
        private readonly string password;

        public StaffApi(Uri apiUrl, string apiKey, string username, string password, string hotelId)
        {
            this.apiUrl = apiUrl;
            this.apiKey = apiKey;
            this.hotelId = hotelId;
            this.username = username;
            this.password = password;
        }

        public IRestResponse<ServiceRequestResponse> CreateServiceRequest(ServiceRequestRequest requestBody)
        {
            IRestClient client = CreateClient();

            IRestRequest request = new RestRequest("/staff/v1/hotels/{hotelId}/tickets/serviceRequest", DataFormat.Json)
                                   .AddUrlSegment("hotelId", hotelId)
                                   .AddJsonBody(requestBody);

            return client.Post<ServiceRequestResponse>(request);
        }

        private IRestClient CreateClient()
        {
            IRestClient client = new RestClient(apiUrl)
                .UseSerializer<JsonNetSerializer>();

            client.Authenticator = new HttpBasicAuthenticator(username, password);
            client.AddDefaultQueryParameter("apikey", apiKey);

            return client;
        }
    }
}
