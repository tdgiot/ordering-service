﻿namespace Crave.Integrations.Ops.Alice.Responses
{
    public class FailureReasonResponse
    {
        [Newtonsoft.Json.JsonProperty("reason")]
        public string? Reason { get; set; }
    }
}
