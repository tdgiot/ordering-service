﻿namespace Crave.Integrations.Ops.Alice.Responses
{
    public class ServiceRequestResponse
    {
        [Newtonsoft.Json.JsonProperty("id", Required = Newtonsoft.Json.Required.Default, NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore)]
        public int? Id { get; set; }

        public bool Success { get; set; }
    }
}
