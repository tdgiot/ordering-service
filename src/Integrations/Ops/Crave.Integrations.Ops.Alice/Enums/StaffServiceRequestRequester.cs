﻿namespace Crave.Integrations.Ops.Alice.Enums
{
    public enum StaffServiceRequestRequester
    {
        Guest = 0,
        Internal = 1
    }
}
