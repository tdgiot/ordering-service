﻿using Crave.Integrations.Ops.Alice.Enums;

namespace Crave.Integrations.Ops.Alice.Requests
{
    public class ServiceRequestRequest
    {
        /// <summary>id of the service from hotel info</summary>
        [Newtonsoft.Json.JsonProperty("serviceId", Required = Newtonsoft.Json.Required.Default, NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore)]
        public string? ServiceId { get; set; }

        [Newtonsoft.Json.JsonProperty("roomNumber", Required = Newtonsoft.Json.Required.Default, NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore)]
        public string? RoomNumber { get; set; }

        [Newtonsoft.Json.JsonProperty("requester", Required = Newtonsoft.Json.Required.Default, NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore)]
        [Newtonsoft.Json.JsonConverter(typeof(Newtonsoft.Json.Converters.StringEnumConverter))]
        public StaffServiceRequestRequester Requester { get; } = StaffServiceRequestRequester.Internal;
    }
}
