﻿using System;
using System.Threading.Tasks;
using Crave.Integrations.Ops.Alice.Requests;
using Crave.Integrations.Ops.Alice.Responses;
using Crave.Integrations.Ops.Interfaces;
using Crave.Integrations.Ops.Models;
using Crave.Integrations.Ops.Results;
using Newtonsoft.Json;
using RestSharp;

namespace Crave.Integrations.Ops.Alice
{
    public class AliceClient : IOpsClient
    {
        private readonly StaffApi staffApi;
        
        public AliceClient(AliceConfiguration configuration)
        {
            staffApi = new StaffApi(configuration.ApiUrl, configuration.ApiKey, configuration.Username, configuration.Password, configuration.HotelId);
        }

        public async Task<OpsResult<ExternalOrder>> SubmitServiceOrder(ExternalOrder externalOrder)
        {
            IRestResponse<ServiceRequestResponse> response = staffApi.CreateServiceRequest(new ServiceRequestRequest
            {
                ServiceId = externalOrder.ExternalProduct.Id,
                RoomNumber = externalOrder.ExternalDeliverypoint.Id
            });

            if (!response.IsSuccessful)
            {
                FailureReasonResponse? reason = ConvertContentToFailureReason(response.Content);

                return OpsResult<ExternalOrder>.FailureResult($"Failed to submit order to Alice. ExternalDeliverypoint={externalOrder.ExternalDeliverypoint.Id}, ExternalProduct={externalOrder.ExternalProduct.Id}, StatusCode={response.StatusCode}, Reason={reason?.Reason}");
            }

            externalOrder = new ExternalOrder { ExternalId = response.Data.Id.ToString() };

            return await Task.FromResult(OpsResult<ExternalOrder>.SuccessResult(externalOrder));
        }

        private static FailureReasonResponse? ConvertContentToFailureReason(string content)
        {
            return content.IsNullOrWhiteSpace() ? null : JsonConvert.DeserializeObject<FailureReasonResponse>(content);
        }
    }
}
