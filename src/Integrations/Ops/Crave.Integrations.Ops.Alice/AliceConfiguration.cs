﻿using System;

namespace Crave.Integrations.Ops.Alice
{
    public class AliceConfiguration
    {
        public AliceConfiguration(Uri apiUrl, string apiKey, string username, string password, string hotelId)
        {
            this.ApiUrl = apiUrl;
            this.ApiKey = apiKey;
            this.Username = username;
            this.Password = password;
            this.HotelId = hotelId;
        }

        public Uri ApiUrl { get; }

        public string ApiKey { get; }

        public string Username { get; }

        public string Password { get; }

        public string HotelId { get; }
    }
}
