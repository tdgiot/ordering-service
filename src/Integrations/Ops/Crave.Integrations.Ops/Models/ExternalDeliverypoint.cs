﻿namespace Crave.Integrations.Ops.Models
{
    public class ExternalDeliverypoint
    {
        public ExternalDeliverypoint(string id, string name)
        {
            this.Id = id;
            this.Name = name;
        }

        public string Id { get; }
        public string Name { get; }
    }
}
