﻿namespace Crave.Integrations.Ops.Models
{
    public class ExternalOrder
    {
        public string ExternalId { get; set; }
        public ExternalDeliverypoint ExternalDeliverypoint { get; set; }
        public ExternalProduct ExternalProduct { get; set; }
        public string Notes { get; set; }
    }
}
