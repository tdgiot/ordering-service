﻿namespace Crave.Integrations.Ops.Models
{
    public class ExternalProduct
    {
        public ExternalProduct(string id)
        {
            this.Id = id;
        }

        public string Id { get; }
    }
}
