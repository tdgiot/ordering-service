﻿using System;

namespace Crave.Integrations.Ops.Results
{
    public class OpsResult<T>
    {
        public static OpsResult<T> SuccessResult(T model) => new OpsResult<T>(true, model);
        public static OpsResult<T> FailureResult(string errorMessage) => new OpsResult<T>(false, errorMessage);
        public static OpsResult<T> ExceptionResult(Exception exception) => new OpsResult<T>(false, exception);

        private OpsResult(bool success, T model)
        {
            Model = model;
            Success = success;
        }

        private OpsResult(bool success, string errorMessage)
        {
            Success = success;
            ErrorMessage = errorMessage;
        }

        private OpsResult(bool success, Exception exception)
        {
            Success = success;
            ErrorMessage = OpsResult<T>.FormatExceptionMessage(exception);
        }

        private static string FormatExceptionMessage(Exception exception)
        {
            if (exception == null)
            {
                return string.Empty;
            }
            
            string message = $"Message={exception.Message}";
                
            if (exception.InnerException != null)
            {
                message = string.Join(", ", message, OpsResult<T>.FormatExceptionMessage(exception.InnerException));
            }

            return message;
        }

        public T Model { get; }
        public bool Success { get; }
        public string ErrorMessage { get; } = string.Empty;
    }
}

