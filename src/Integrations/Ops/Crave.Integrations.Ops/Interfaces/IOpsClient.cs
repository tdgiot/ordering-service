﻿using Crave.Integrations.Ops.Models;
using Crave.Integrations.Ops.Results;
using System.Threading.Tasks;

namespace Crave.Integrations.Ops.Interfaces
{
    public interface IOpsClient
    {
        Task<OpsResult<ExternalOrder>> SubmitServiceOrder(ExternalOrder externalOrder);
    }
}
