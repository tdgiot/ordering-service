﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Crave.Integrations.POS.Interfaces.Repositories;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.SQLServer;
using SD.LLBLGen.Pro.ORMSupportClasses;
using SD.LLBLGen.Pro.QuerySpec;

namespace Crave.Integrations.POS.Data.Repositories
{
    /// <summary>
    /// Repository class for external products.
    /// </summary>
    public class ExternalProductRepository : IExternalProductRepository
    {
        public async Task<EntityCollection<ExternalProductEntity>> GetExternalProductsAsync(int externalSystemId)
        {
            PrefetchPath2 prefetch = new PrefetchPath2(EntityType.ExternalProductEntity);
            
            RelationPredicateBucket filterBucket = new RelationPredicateBucket();
            filterBucket.PredicateExpression.Add(ExternalProductFields.ExternalSystemId == externalSystemId);

            return await DataAccessAdapter.FetchEntityCollectionAsync<ExternalProductEntity>(filterBucket, 0, null, prefetch, null);
        }

        public async Task<EntityCollection<ExternalProductEntity>> GetExternalProductsPrefetchedAsync(int externalSystemId)
        {
            PrefetchPath2 prefetch = new PrefetchPath2(EntityType.ExternalProductEntity);
            prefetch.Add(ExternalProductEntity.PrefetchPathProductCollection);
            prefetch.Add(ExternalProductEntity.PrefetchPathAlterationoptionCollection);
            prefetch.Add(ExternalProductEntity.PrefetchPathAlterationCollection);
            prefetch.Add(ExternalProductEntity.PrefetchPathExternalSystemEntity);

            RelationPredicateBucket filterBucket = new RelationPredicateBucket();
            filterBucket.PredicateExpression.Add(ExternalProductFields.ExternalSystemId == externalSystemId);

            SortExpression sort = new SortExpression(ExternalProductFields.Name | SortOperator.Ascending);

            return await DataAccessAdapter.FetchEntityCollectionAsync<ExternalProductEntity>(filterBucket, 0, sort, prefetch, null);
        }

        public async Task<bool> SaveAsync(ExternalProductEntity entity)
        {
            return await DataAccessAdapter.SaveAsync(entity, false);
        }

        public async Task SaveAsync(IEntityCollection2 entities)
        {
            await DataAccessAdapter.SaveAsync(entities, true, true);
        }

        public async Task DeleteAsync(IEntityCollection2 entities)
        {
            await DataAccessAdapter.DeleteAsync(entities);
        }
    }
}
