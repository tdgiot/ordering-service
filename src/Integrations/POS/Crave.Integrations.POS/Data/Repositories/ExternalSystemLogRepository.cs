﻿using System.Threading.Tasks;
using Crave.Integrations.POS.Interfaces;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.SQLServer;

namespace Crave.Integrations.POS.Data.Repositories
{
    /// <summary>
    /// Repository class for external system logs.
    /// </summary>
    public class ExternalSystemLogRepository : IExternalSystemLogRepository
    {
        /// <inheritdoc/>
        public async Task<bool> SaveAsync(ExternalSystemLogEntity entity)
        {
            return await DataAccessAdapter.SaveAsync(entity, false);
        }
    }
}
