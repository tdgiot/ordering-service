﻿using System;
using Crave.Integrations.POS.Interfaces.Repositories;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.SQLServer;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System.Threading.Tasks;
using SD.LLBLGen.Pro.QuerySpec;

namespace Crave.Integrations.POS.Data.Repositories
{
    /// <summary>
    /// Repository class for external systems.
    /// </summary>
    public class ExternalSystemRepository : IExternalSystemRepository
    {
        /// <inheritdoc/>
        public async Task<ExternalSystemEntity> GetByIdAsync(int externalSystemId)
        {
            RelationPredicateBucket filterBucket = new RelationPredicateBucket(ExternalSystemFields.ExternalSystemId == externalSystemId);

            return await DataAccessAdapter.FetchEntityAsync<ExternalSystemEntity>(filterBucket, null, null);
        }

        /// <inheritdoc/>
        public async Task<ExternalSystemEntity> GetByIdWithExternalProductsPrefetchedAsync(int externalSystemId, string menuId)
        {
            RelationPredicateBucket filterBucket = new RelationPredicateBucket(ExternalSystemFields.ExternalSystemId == externalSystemId);
            
            filterBucket.Relations.Add(ExternalSystemEntity.Relations.ExternalMenuEntityUsingExternalSystemId, JoinHint.Left);
            filterBucket.Relations.Add(ExternalMenuEntity.Relations.ExternalProductEntityUsingExternalMenuId, JoinHint.Left);

            PredicateExpression menuFilter = new PredicateExpression(ExternalMenuFields.Id == menuId);

            PrefetchPath2 prefetch = new PrefetchPath2(EntityType.ExternalSystemEntity);
            prefetch.Add(ExternalSystemEntity.PrefetchPathExternalMenuCollection, 0, menuFilter).SubPath
                    .Add(ExternalMenuEntity.PrefetchPathExternalProductCollection);

            return await DataAccessAdapter.FetchEntityAsync<ExternalSystemEntity>(filterBucket, prefetch, null);
        }

        /// <inheritdoc/>
        public async Task<ExternalSystemEntity> GetByChannelLinkIdAsync(string channelLinkId)
        {
            RelationPredicateBucket filterBucket = new RelationPredicateBucket(ExternalSystemFields.StringValue3 == channelLinkId);

            return await DataAccessAdapter.FetchEntityAsync<ExternalSystemEntity>(filterBucket, null, null);
        }

        /// <inheritdoc/>
        public async Task<ExternalSystemEntity> GetByOrderIdAsync(int orderId)
        {
            RelationPredicateBucket filterBucket = new RelationPredicateBucket();
            filterBucket.PredicateExpression.Add(OrderRoutestephandlerFields.OrderId == orderId);

            filterBucket.Relations.Add(ExternalSystemEntity.Relations.OrderRoutestephandlerEntityUsingExternalSystemId);

            return await DataAccessAdapter.FetchEntityAsync<ExternalSystemEntity>(filterBucket, null, null);
        }

        /// <inheritdoc/>
        public async Task<ExternalSystemEntity> GetByLocationIdAsync(string locationId)
        {
            RelationPredicateBucket filterBucket = new RelationPredicateBucket(ExternalSystemFields.StringValue4 == locationId);

            return await DataAccessAdapter.FetchEntityAsync<ExternalSystemEntity>(filterBucket, null, null);
        }

        /// <inheritdoc/>
        public async Task<bool> SaveAsync(ExternalSystemEntity entity) => await DataAccessAdapter.SaveAsync(entity, false);
    }
}
