﻿using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using System.Threading.Tasks;

namespace Crave.Integrations.POS.Interfaces.Repositories
{
    /// <summary>
    /// Interface which is used to implement external system repository classes.
    /// </summary>
    public interface IExternalSystemRepository
	{
		/// <summary>
		/// Gets an external system by it's id.
		/// </summary>
		/// <param name="externalSystemId">The external system id.</param>
		/// <returns>A <see cref="ExternalSystemEntity"/> instance.</returns>
		Task<ExternalSystemEntity> GetByIdAsync(int externalSystemId);

		/// <summary>
		/// Gets an external system by it's id with the external products prefetched.
		/// </summary>
		/// <param name="externalSystemId">The external system id.</param>
		/// <param name="menuId">The external menu id.</param>
		/// <returns>A <see cref="ExternalSystemEntity"/> instance.</returns>
		Task<ExternalSystemEntity> GetByIdWithExternalProductsPrefetchedAsync(int externalSystemId, string menuId);

		/// <summary>
		/// Gets an external system by the order id.
		/// </summary>
		/// <param name="orderId">The order id.</param>
		/// <returns>A <see cref="ExternalSystemEntity"/> instance.</returns>
		Task<ExternalSystemEntity> GetByOrderIdAsync(int orderId);

		/// <summary>
		/// Gets an external system by the specified channel link id.
		/// </summary>
		/// <param name="channelLinkId">The channel link id.</param>
		/// <returns>A <see cref="ExternalSystemEntity"/> instance.</returns>
		Task<ExternalSystemEntity> GetByChannelLinkIdAsync(string channelLinkId);

		/// <summary>
		/// Gets an external system by the specified channel link id.
		/// </summary>
		/// <param name="locationId">The location id.</param>
		/// <returns>A <see cref="ExternalSystemEntity"/> instance.</returns>        
		Task<ExternalSystemEntity> GetByLocationIdAsync(string locationId);

		/// <summary>
		/// SaveAsync external system
		/// </summary>
		/// <param name="entity">External system entity</param>
		/// <returns>True, when successfully saved.</returns>
		Task<bool> SaveAsync(ExternalSystemEntity entity);
	}
}
