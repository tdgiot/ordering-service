﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Crave.Integrations.POS.Interfaces.Repositories
{
    /// <summary>
    /// Interface which is used to implement external product repository classes.
    /// </summary>
    public interface IExternalProductRepository
    {
        Task<EntityCollection<ExternalProductEntity>> GetExternalProductsAsync(int externalSystemId);
        
        Task<EntityCollection<ExternalProductEntity>> GetExternalProductsPrefetchedAsync(int externalSystemId);

        Task<bool> SaveAsync(ExternalProductEntity entity);

        Task SaveAsync(IEntityCollection2 entities);

        Task DeleteAsync(IEntityCollection2 entities);
    }
}