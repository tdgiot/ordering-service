﻿using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using System.Threading.Tasks;

namespace Crave.Integrations.POS.Interfaces.Repositories
{
	/// <summary>
	/// Interface which is used to implement external system repository classes.
	/// </summary>
	public interface IExternalMenuRepository
	{
		/// <summary>
		/// Gets an external system by it's id.
		/// </summary>
		/// <param name="externalSystemId">The external system id.</param>
		/// <returns>A <see cref="ExternalSystemEntity"/> instance.</returns>
		Task<ExternalMenuEntity> GetByExternalIdAsync(string externalMenuExternalId);


	}
}
