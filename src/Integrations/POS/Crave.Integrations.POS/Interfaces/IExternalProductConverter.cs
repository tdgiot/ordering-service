﻿using System.Collections.Generic;
using Crave.Integrations.POS.Models;

namespace Crave.Integrations.POS.Interfaces
{
    public interface IExternalProductConverter<in T>
    {
        ExternalProduct[] Convert(T models);
    }
}
