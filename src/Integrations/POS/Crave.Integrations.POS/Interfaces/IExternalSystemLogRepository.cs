﻿using System.Threading.Tasks;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Integrations.POS.Interfaces
{
    /// <summary>
    /// Interface which is used to implement external system log repository classes.
    /// </summary>
    public interface IExternalSystemLogRepository
    {
        /// <summary>
        /// SaveAsync external system log
        /// </summary>
        /// <param name="entity">External system log entity</param>
        /// <returns>True, when successfully saved.</returns>
        Task<bool> SaveAsync(ExternalSystemLogEntity entity);
    }
}