﻿using System;
using System.Threading.Tasks;
using Crave.Integrations.POS.Domain;

namespace Crave.Integrations.POS.Interfaces
{
    public interface IExternalSystemLogger
    {
        Task LogAsync(Action<ExternalSystemLog> log);
    }
}