﻿using Crave.Integrations.POS.Domain.UseCases.Requests;
using Crave.Ordering.Shared.Domain;
using Crave.Ordering.Shared.Interfaces;

namespace Crave.Integrations.POS.Interfaces.UseCases
{
    public interface IUpdateExternalProductsUseCase : IUseCaseAsync<UpdateExternalProductsRequest, Response>
    {
    }
}
