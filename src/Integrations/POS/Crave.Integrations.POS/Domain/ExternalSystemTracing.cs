﻿using System.Text;

namespace Crave.Integrations.POS.Domain
{
    public class ExternalSystemTracing
    {
        private readonly StringBuilder builder = new StringBuilder();

        public void Append(string format, params object[] args) => builder.AppendFormatLine(format, args);

        public override string ToString() => builder.ToString();
    }
}
