﻿using System.Threading.Tasks;
using Crave.Enums;
using Crave.Integrations.POS.Domain.UseCases.Requests;
using Crave.Integrations.POS.Interfaces.Repositories;
using Crave.Integrations.POS.Interfaces.UseCases;
using Crave.Integrations.POS.Synchronizers;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Domain;

namespace Crave.Integrations.POS.Domain.UseCases
{
    public class UpdateItemsLinkedToExternalProductsUseCase : IUpdateItemsLinkedToExternalProductsUseCase
    {
        private readonly IExternalProductRepository externalProductRepository;

        public UpdateItemsLinkedToExternalProductsUseCase(IExternalProductRepository externalProductRepository)
        {
            this.externalProductRepository = externalProductRepository;
        }

        public async Task<Response> ExecuteAsync(UpdateItemsLinkedToExternalProductsRequest request)
        {
            EntityCollection<ExternalProductEntity> externalProducts = await externalProductRepository.GetExternalProductsPrefetchedAsync(request.ExternalSystemId);

            foreach (ExternalProductEntity externalProduct in externalProducts)
            {
                IExternalProductSynchronizer? synchronizer = GetExternalProductSynchronizer(externalProduct.Type);
                synchronizer?.Synchronize(externalProduct);
            }

            await externalProductRepository.SaveAsync(externalProducts);

            return Response.AsSuccess();
        }

        private static IExternalProductSynchronizer? GetExternalProductSynchronizer(ExternalProductType type) => type switch
        {
            ExternalProductType.Product => new ProductSynchronizer(),
            ExternalProductType.Modifier => new AlterationoptionSynchronizer(),
            ExternalProductType.ModifierGroup => new AlterationSynchronizer(),
            _ => null
        };
    }
}
