﻿using Crave.Integrations.POS.Domain.UseCases.Requests;
using Crave.Integrations.POS.Interfaces.Repositories;
using Crave.Integrations.POS.Interfaces.UseCases;
using Crave.Integrations.POS.Models;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.Extensions;
using Crave.Ordering.Shared.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Crave.Integrations.POS.Domain.UseCases
{
    public class UpdateExternalProductsUseCase : IUpdateExternalProductsUseCase
    {
        private readonly IExternalSystemRepository externalSystemRepository;

        public UpdateExternalProductsUseCase(IExternalSystemRepository externalSystemRepository)
        {
            this.externalSystemRepository = externalSystemRepository;
        }

        public async Task<Response> ExecuteAsync(UpdateExternalProductsRequest request)
        {
            ExternalSystemEntity externalSystem = await externalSystemRepository.GetByIdWithExternalProductsPrefetchedAsync(request.ExternalSystemId, request.MenuId);
            if (externalSystem.IsNullOrNew())
            {
                throw new InvalidOperationException($"No external system found with id {request.ExternalSystemId}");
            }

            ExternalProduct[] updatedProducts = request.ExternalProducts;
            ExternalSystemTracing externalSystemTracing = request.ExternalSystemTracing;

            ExternalMenuEntity? externalMenuEntity = externalSystem.ExternalMenuCollection.FirstOrDefault(menu => menu.Id == request.MenuId);

            if (externalMenuEntity == null || externalMenuEntity.IsNullOrNew())
            {
                externalMenuEntity = externalSystem.ExternalMenuCollection.AddNew();
                externalMenuEntity.Id = request.MenuId;
                externalMenuEntity.CreatedUTC = DateTime.UtcNow;
                externalMenuEntity.ParentCompanyId = externalSystem.CompanyId;
            }

            externalMenuEntity.Name = request.MenuName;

            AddNewExternalProducts(externalMenuEntity, updatedProducts, externalSystem);
            UpdateExternalProducts(externalMenuEntity, updatedProducts, externalSystemTracing);

            bool saved = await externalSystemRepository.SaveAsync(externalSystem);

            return saved ? Response.AsSuccess() : Response.AsError($"Failed to save updated products of external system {request.ExternalSystemId}");
        }

        private static void AddNewExternalProducts(ExternalMenuEntity menuEntity, IEnumerable<ExternalProduct> updatedProducts, ExternalSystemEntity externalSystem)
        {
            foreach (ExternalProduct updatedProduct in updatedProducts)
            {
                if (updatedProduct.Id.IsNullOrWhiteSpace() || menuEntity.ExternalProductCollection.Any(x => x.Id == updatedProduct.Id))
                {
                    continue;
                }

                ExternalProductEntity externalProduct = menuEntity.ExternalProductCollection.AddNew();
                externalProduct.ParentCompanyId = externalSystem.CompanyId;
                externalProduct.Id = updatedProduct.Id;
                externalProduct.ExternalSystem = externalSystem;
                externalProduct.ExternalMenu = menuEntity;
            }
        }

        private static void UpdateExternalProducts(ExternalMenuEntity menuEntity, IEnumerable<ExternalProduct> updatedProducts, ExternalSystemTracing externalSystemTracing)
        {
            IDictionary<string, ExternalProduct> updatedProductMap = updatedProducts.Where(product => product.MenuId == menuEntity.Id)
                                                                                    .GroupBy(product => product.Id, StringComparer.InvariantCultureIgnoreCase)
                                                                                    .ToDictionary(grouped => grouped.Key, grouped => grouped.First());

            foreach (ExternalProductEntity existingProduct in menuEntity.ExternalProductCollection.OrderBy(externalProduct => externalProduct.Name))
            {
                if (existingProduct.Id.IsNullOrWhiteSpace())
                {
                    continue;
                }

                if (updatedProductMap.TryGetValue(existingProduct.Id, out ExternalProduct updatedProduct))
                {
                    existingProduct.IsEnabled = true;
                    existingProduct.Name = updatedProduct.Name;
                    existingProduct.Price = updatedProduct.Price;
                    existingProduct.Visible = updatedProduct.Visible;
                    existingProduct.MinOptions = updatedProduct.MinOptions;
                    existingProduct.MaxOptions = updatedProduct.MaxOptions;
                    existingProduct.Type = updatedProduct.ProductType;
                    existingProduct.AllowDuplicates = updatedProduct.AllowDuplicates;

                    existingProduct.StringValue1 = updatedProduct.StringValue1;
                    existingProduct.StringValue2 = updatedProduct.StringValue2;
                    existingProduct.StringValue3 = updatedProduct.StringValue3;
                    existingProduct.StringValue4 = updatedProduct.StringValue4;
                    existingProduct.StringValue5 = updatedProduct.StringValue5;
                    existingProduct.IntValue1 = updatedProduct.IntValue1;
                    existingProduct.IntValue2 = updatedProduct.IntValue2;
                    existingProduct.IntValue3 = updatedProduct.IntValue3;
                    existingProduct.IntValue4 = updatedProduct.IntValue4;
                    existingProduct.IntValue5 = updatedProduct.IntValue5;
                    existingProduct.BoolValue1 = updatedProduct.BoolValue1;
                    existingProduct.BoolValue2 = updatedProduct.BoolValue2;
                    existingProduct.BoolValue3 = updatedProduct.BoolValue3;
                    existingProduct.BoolValue4 = updatedProduct.BoolValue4;
                    existingProduct.BoolValue5 = updatedProduct.BoolValue5;
                }
                else
                {
                    existingProduct.IsEnabled = false;
                }

                if (existingProduct.IsNew)
                {
                    externalSystemTracing.Append("{0}({1}) -> added", existingProduct.Name, existingProduct.Id);
                }
                else if (existingProduct.IsDirty)
                {
                    externalSystemTracing.Append("{0}({1}) -> changed", existingProduct.Name, existingProduct.Id);
                }
                else
                {
                    externalSystemTracing.Append("{0}({1}) -> unchanged", existingProduct.Name, existingProduct.Id);
                }
            }
        }
    }
}
