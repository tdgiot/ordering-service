﻿using Crave.Integrations.POS.Models;

namespace Crave.Integrations.POS.Domain.UseCases.Requests
{
	public class UpdateExternalProductsRequest
	{
		public UpdateExternalProductsRequest(int externalSystemId, ExternalProduct[] externalProducts, ExternalSystemTracing externalSystemTracing, string menuId, string menuName)
		{
			ExternalSystemId = externalSystemId;
			ExternalProducts = externalProducts;
			ExternalSystemTracing = externalSystemTracing;
			MenuId = menuId;
			MenuName = menuName;
		}

		public int ExternalSystemId { get; }
		public ExternalProduct[] ExternalProducts { get; }
		public ExternalSystemTracing ExternalSystemTracing { get; }
		public string MenuId { get; }
		public string MenuName { get; }
	}
}
