﻿namespace Crave.Integrations.POS.Domain.UseCases.Requests
{
    public class UpdateItemsLinkedToExternalProductsRequest
    {
        public UpdateItemsLinkedToExternalProductsRequest(int externalSystemId)
        {
            ExternalSystemId = externalSystemId;
        }

        public int ExternalSystemId { get; }
    }
}
