﻿using Crave.Enums;
using Crave.Integrations.POS.Interfaces;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using System;
using System.Threading.Tasks;

namespace Crave.Integrations.POS.Domain
{
	public class ExternalSystemLogger : IExternalSystemLogger
	{
		private readonly IExternalSystemLogRepository externalSystemLogRepository;

		public ExternalSystemLogger(IExternalSystemLogRepository externalSystemLogRepository)
		{
			this.externalSystemLogRepository = externalSystemLogRepository;
		}

		public async Task LogAsync(Action<ExternalSystemLog> log)
		{
			ExternalSystemLog externalSystemLog = new ExternalSystemLog(log);

			ExternalSystemLogEntity entity = new ExternalSystemLogEntity
			{
				ExternalSystemId = externalSystemLog.ExternalSystemId,
				Message = externalSystemLog.Message,
				Raw = externalSystemLog.Raw,
				LogType = externalSystemLog.LogType,
				OrderId = externalSystemLog.OrderId,
				Success = externalSystemLog.Success,
				Log = externalSystemLog.Log,
				CreatedUTC = DateTime.UtcNow
			};

			await externalSystemLogRepository.SaveAsync(entity);
		}
	}

	public class ExternalSystemLog
	{
		public ExternalSystemLog(Action<ExternalSystemLog> action)
		{
			action.Invoke(this);
		}

		public ExternalSystemLogType LogType { get; set; }
		public int ExternalSystemId { get; set; }
		public string? Message { get; set; }
		public bool Success { get; set; }
		public string? Raw { get; set; }
		public string? Log { get; set; }
		public int? OrderId { get; set; }
	}
}
