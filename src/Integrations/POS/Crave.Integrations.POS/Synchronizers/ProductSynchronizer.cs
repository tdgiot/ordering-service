﻿using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Integrations.POS.Synchronizers
{
    public class ProductSynchronizer : IExternalProductSynchronizer
    {
        public void Synchronize(ExternalProductEntity externalProduct)
        {
            foreach (ProductEntity product in externalProduct.ProductCollection)
            {
                product.IsAvailable = externalProduct.IsEnabled && !externalProduct.IsSnoozed && !externalProduct.ExternalSystem.IsBusy;
                product.PriceIn = externalProduct.Price;
            }
        }
    }
}
