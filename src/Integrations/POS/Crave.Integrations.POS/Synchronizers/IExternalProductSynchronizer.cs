﻿using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Integrations.POS.Synchronizers
{
    public interface IExternalProductSynchronizer
    {
        public void Synchronize(ExternalProductEntity externalProduct);
    }
}
