﻿using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Integrations.POS.Synchronizers
{
    public class AlterationoptionSynchronizer : IExternalProductSynchronizer
    {
        public void Synchronize(ExternalProductEntity externalProduct)
        {
            foreach (AlterationoptionEntity alterationoption in externalProduct.AlterationoptionCollection)
            {
                alterationoption.IsAvailable = externalProduct.IsEnabled && !externalProduct.IsSnoozed && !externalProduct.ExternalSystem.IsBusy;
                alterationoption.PriceIn = externalProduct.Price;
            }
        }
    }
}
