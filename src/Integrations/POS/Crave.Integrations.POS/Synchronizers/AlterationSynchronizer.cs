﻿using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Integrations.POS.Synchronizers
{
    public class AlterationSynchronizer : IExternalProductSynchronizer
    {
        public void Synchronize(ExternalProductEntity externalProduct)
        {
            foreach (AlterationEntity alteration in externalProduct.AlterationCollection)
            {
                alteration.IsAvailable = externalProduct.IsEnabled && !externalProduct.IsSnoozed && !externalProduct.ExternalSystem.IsBusy;
                alteration.MinOptions = externalProduct.MinOptions;
                alteration.MaxOptions = externalProduct.MaxOptions;
                alteration.AllowDuplicates = externalProduct.AllowDuplicates;
            }
        }
    }
}
