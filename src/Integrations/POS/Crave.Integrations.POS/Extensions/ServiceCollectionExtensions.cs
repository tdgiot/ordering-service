﻿using Crave.Integrations.POS.Data.Repositories;
using Crave.Integrations.POS.Domain;
using Crave.Integrations.POS.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;

namespace Crave.Integrations.POS.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static void AddExternalSystemLogger(this IServiceCollection services)
        {
            services.TryAddSingleton<IExternalSystemLogRepository, ExternalSystemLogRepository>();
            services.TryAddSingleton<IExternalSystemLogger, ExternalSystemLogger>();
        }
    }
}
