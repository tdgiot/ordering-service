﻿using Crave.Enums;

namespace Crave.Integrations.POS.Models
{
    public class ExternalProduct
    {
        public string? Id { get; set; }
        public string? Name { get; set; }
        public string? MenuId { get; set; }
        public decimal Price { get; set; }
        public bool Visible { get; set; }
        public int MinOptions { get; set; }
        public int MaxOptions { get; set; }
        public bool AllowDuplicates { get; set; }
        public ExternalProductType ProductType { get; set; }
        public string? StringValue1 { get; set; }
        public string? StringValue2 { get; set; }
        public string? StringValue3 { get; set; }
        public string? StringValue4 { get; set; }
        public string? StringValue5 { get; set; }
        public int IntValue1 { get; set; }
        public int IntValue2 { get; set; }
        public int IntValue3 { get; set; }
        public int IntValue4 { get; set; }
        public int IntValue5 { get; set; }
        public bool BoolValue1 { get; set; }
        public bool BoolValue2 { get; set; }
        public bool BoolValue3 { get; set; }
        public bool BoolValue4 { get; set; }
        public bool BoolValue5 { get; set; }
        public string[]? SubItemIds { get; set; }
    }
}
