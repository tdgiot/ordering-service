﻿using Newtonsoft.Json;
using System;

namespace Crave.Integrations.POS.Deliverect.Domain.Responses
{
    public class ChannelStatusResponse
    {
        /// <summary>
        /// Receive status updates
        /// </summary>
        [JsonProperty("statusUpdateURL")]
        public string StatusUpdateURL { get; set; } = string.Empty;

        /// <summary>
        /// Receive menu changes
        /// </summary>
        [JsonProperty("menuUpdateURL")]
        public string MenuUpdateURL { get; set; } = string.Empty;

        /// <summary>
        /// Receive disabled products
        /// </summary>
        [JsonProperty("disabledProductsURL")]
        public string DisabledProductsURL { get; set; } = string.Empty;

        /// <summary>
        /// Snooze or unsnooze
        /// </summary>
        [JsonProperty("snoozeUnsnoozeURL")]
        public string SnoozeUnsnoozeURL { get; set; } = string.Empty;

        /// <summary>
        /// Enable or disable busy mode
        /// </summary>
        [JsonProperty("busyModeURL")]
        public string BusyModeURL { get; set; } = string.Empty;
    }
}