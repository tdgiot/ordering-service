﻿using System.Collections.Generic;
using Crave.Integrations.POS.Deliverect.Domain.Models;
using Newtonsoft.Json;

namespace Crave.Integrations.POS.Deliverect.Domain.Responses
{
    public class GetAccountsResponse
    {
        [JsonProperty("_items")]
        public List<Account>? Items { get; set; }

        [JsonProperty("_links")]
        public Links? Links { get; set; }

        [JsonProperty("_meta")]
        public Meta? Meta { get; set; }
    }
}
