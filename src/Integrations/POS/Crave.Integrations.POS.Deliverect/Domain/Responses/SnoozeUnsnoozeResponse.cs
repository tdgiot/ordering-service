﻿using Crave.Integrations.POS.Deliverect.Domain.Models;
using Newtonsoft.Json;

namespace Crave.Integrations.POS.Deliverect.Domain.Responses
{
    public class SnoozeUnsnoozeResponse
    {
        [JsonProperty("results")]
        public SnoozeProductsResult[]? Results { get; set; }
    }
}
