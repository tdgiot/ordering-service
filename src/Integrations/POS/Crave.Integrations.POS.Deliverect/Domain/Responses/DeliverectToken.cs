﻿using System;
using Newtonsoft.Json;

namespace Crave.Integrations.POS.Deliverect.Domain.Responses
{
    public class DeliverectToken
    {
        [JsonProperty("access_token")]
        public string? AccessToken { get; set; }

        [JsonProperty("expires_at")]
        public int ExpiresAt { get; set; }
        
        [JsonProperty("expires_in")]
        public int ExpiresIn { get; set; }

        [JsonProperty("token_type")]
        public string? TokenType { get; set; }

        [JsonProperty("scope")]
        public string? Scope { get; set; }

        public string ChannelName =>
            !Scope.IsNullOrWhiteSpace() && (Scope?.Contains(":") ?? false)
                ? Scope.Split(':')[1]
                : string.Empty;
    }
}
