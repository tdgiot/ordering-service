﻿using System.Text.Json.Serialization;

namespace Crave.Integrations.POS.Deliverect.Domain.Responses
{
    public class BusyModeResponse
    {
        [JsonPropertyName("status")]
        public string? Status { get; set; }
    }
}
