﻿namespace Crave.Integrations.POS.Deliverect.Domain.Enums
{
    public enum OrderType
    {
        Pickup = 1,

        Delivery = 2,

        EatIn = 3,

        Curbside = 4
    }
}
