﻿namespace Crave.Integrations.POS.Deliverect.Domain.Enums
{
    public enum ProductTag
    {
        // Consumable Types
        Alcohol = 1,
        Halal = 2,
        Kosher = 3,
        Vegan = 4,
        Vegetarian = 5,

        // Allergens
        Celery = 100,
        Gluten = 101,
        Crustaceans = 102,
        Fish = 103,
        Eggs = 104,
        Lupin = 105,
        Milk = 106,
        Molluscs = 107,
        Mustard = 108,
        Nuts = 109,
        Peanuts = 110,
        Sesame = 111,
        Soya = 112,
        Sulphites = 113,
        Almonds = 114,
        Barley = 115,
        BrazilNuts = 116,
        Cashew = 117,
        Hazelnuts = 118,
        Kamut = 119,
        Macadamia = 120,
        Oats = 121,
        Pecan = 122,
        Pistachios = 123,
        Rye = 124,
        Spelt = 125,
        Walnuts = 126,
        Wheat = 127
    }
}
