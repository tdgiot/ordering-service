﻿namespace Crave.Integrations.POS.Deliverect.Domain.Enums
{
    public enum ProductType
    {
        Product	= 1,

        Modifier = 2,

        ModifierGroup = 3,

        Bundle = 4
    }
}
