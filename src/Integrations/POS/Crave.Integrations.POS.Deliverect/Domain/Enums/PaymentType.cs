﻿namespace Crave.Integrations.POS.Deliverect.Domain.Enums
{
    public enum PaymentType
    {
        CreditCardOnline = 0,

        Cash = 1,

        OnDelivery = 2,

        PaidOnline = 3,

        CreditCardAtTheDoor = 4,

        PinAtTheDoor = 5,

        VoucherAtTheDoor = 6,

        Check = 7,

        BankContact = 8,

        Other = 9
    }
}
