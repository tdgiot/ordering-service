﻿namespace Crave.Integrations.POS.Deliverect.Domain.Enums
{
    public enum OrderStatus
    {
        /// <summary>
        /// Received and parsed by Deliverect
        /// </summary>
        Parsed = 1,

        /// <summary>
        /// Received by POS
        /// </summary>
        Received = 2,

        /// <summary>
        /// Received by POS and receipt is created
        /// </summary>
        New = 10,

        /// <summary>
        /// Accepted/Confirmed
        /// </summary>
        Accepted = 20,

        /// <summary>
        /// Order has been received multiple times on the POS due to a technical error
        /// </summary>
        Duplicate = 30,

        /// <summary>
        /// Printed
        /// </summary>
        Printed = 40,

        /// <summary>
        /// In preparation
        /// </summary>
        Preparing = 50,

        /// <summary>
        /// Is prepared
        /// </summary>
        Prepared = 60,

        /// <summary>
        /// Ready for pickup
        /// </summary>
        ReadyForPickup = 70,

        /// <summary>
        /// En route to customer
        /// </summary>
        InDelivery = 80,

        /// <summary>
        /// Finalized, order is fully handled and no further status updates are coming
        /// </summary>
        Finalized = 90,

        /// <summary>
        /// Accepted by POS, but any other status updates unsupported by POS (to be avoided at all costs!)	
        /// </summary>
        AutoFinalized = 95,

        /// <summary>
        /// Should be voided
        /// </summary>
        Cancel = 100,

        /// <summary>
        /// Has been voided on POS
        /// </summary>
        Canceled = 110,

        /// <summary>
        /// Failed
        /// </summary>
        Failed = 120,

        /// <summary>
        /// Not received by POS
        /// </summary>
        PosReceivedFailed = 121,

        /// <summary>
        /// Parsing failed
        /// </summary>
        ParseFailed = 124
    }
}
