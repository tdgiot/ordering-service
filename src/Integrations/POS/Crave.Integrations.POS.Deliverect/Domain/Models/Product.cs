﻿using System;
using Crave.Integrations.POS.Deliverect.Domain.Enums;
using Newtonsoft.Json;

namespace Crave.Integrations.POS.Deliverect.Domain.Models
{
    public class Product
    {
        [JsonProperty("_id")]
        public string Id { get; set; }

        [JsonProperty("account")]
        public string Account { get; set; }

        [JsonProperty("location")]
        public string Location { get; set; }

        [JsonProperty("productType")]
        public ProductType ProductType { get; set; }

        [JsonProperty("plu")]
        public string Plu { get; set; }

        [JsonProperty("price")]
        public int Price { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("deliveryTax")]
        public int DeliveryTax { get; set; }

        [JsonProperty("subProducts")]
        public string[] SubProducts { get; set; }

        [JsonProperty("productTags")]
        public ProductTag[] ProductTags { get; set; }

        [JsonProperty("imageUrl")]
        public Uri ImageUrl { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("max")]
        public int Max { get; set; }

        [JsonProperty("min")]
        public int Min { get; set; }

        [JsonProperty("channel")]
        public int Channel { get; set; }
    }
}
