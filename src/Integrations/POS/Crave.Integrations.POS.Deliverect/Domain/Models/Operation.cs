﻿using Newtonsoft.Json;

namespace Crave.Integrations.POS.Deliverect.Domain.Models
{
    public class Operation
    {
        [JsonProperty("action")]
        public string Action { get; set; }

        [JsonProperty("data")]
        public OperationData Data { get; set; }
    }
}
