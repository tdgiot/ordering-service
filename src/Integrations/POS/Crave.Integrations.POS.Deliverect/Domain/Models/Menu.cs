﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Crave.Integrations.POS.Deliverect.Domain.Models
{
    public class Menu
    {
        [JsonProperty("menu")]
        public string Name { get; set; }
        
        [JsonProperty("availabilities")]
        public Availability[] Availabilities { get; set; }

        [JsonProperty("categories")]
        public Category[] Categories { get; set; }

        [JsonProperty("products")]
        public IDictionary<string, Product> Products { get; set; }

        [JsonProperty("modifierGroups")]
        public IDictionary<string, ModifierGroup> ModifierGroups { get; set; }
        
        [JsonProperty("modifiers")]
        public IDictionary<string, Modifier> Modifiers { get; set; }

        [JsonProperty("snoozedProducts")]
        public IDictionary<string, Product> SnoozedProducts { get; set; }

        [JsonProperty("channelLinkId")]
        public string ChannelLinkId { get; set; }
    }
}
