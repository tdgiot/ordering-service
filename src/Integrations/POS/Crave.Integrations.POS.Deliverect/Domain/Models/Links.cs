﻿using Newtonsoft.Json;

namespace Crave.Integrations.POS.Deliverect.Domain.Models
{
    public class Links
    {
        [JsonProperty("self")]
        public Self Self { get; set; }

        [JsonProperty("parent")]
        public Parent Parent { get; set; }
    }
}
