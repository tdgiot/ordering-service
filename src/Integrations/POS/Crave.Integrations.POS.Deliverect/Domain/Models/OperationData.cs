﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Crave.Integrations.POS.Deliverect.Domain.Models
{
    public class OperationData
    {
        [JsonProperty("items")]
        public List<OperationItem> Items { get; set; }
    }
}
