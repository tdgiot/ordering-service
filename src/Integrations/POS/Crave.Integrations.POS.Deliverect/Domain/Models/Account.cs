﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Crave.Integrations.POS.Deliverect.Domain.Models
{
    public class Account
    {
        [JsonProperty("_id")]
        public string Id { get; set; }

        [JsonProperty("_created")]
        public DateTime Created { get; set; }

        [JsonProperty("_updated")]
        public DateTime Updated { get; set; }

        [JsonProperty("_etag")]
        public string Etag { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("accountType")]
        public int AccountType { get; set; }

        [JsonProperty("users")]
        public List<string> Users { get; set; }

        [JsonProperty("locations")]
        public List<string> Locations { get; set; }

        [JsonProperty("_deleted")]
        public bool Deleted { get; set; }

        [JsonProperty("_links")]
        public Links Links { get; set; }
    }
}
