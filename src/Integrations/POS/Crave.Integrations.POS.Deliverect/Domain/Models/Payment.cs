﻿using Newtonsoft.Json;

namespace Crave.Integrations.POS.Deliverect.Domain.Models
{
    public class Payment
    {
        [JsonProperty("amount")]
        public int Amount { get; set; }

        [JsonProperty("type")]
        public Enums.PaymentType Type { get; set; }
    }
}