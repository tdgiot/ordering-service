﻿using Newtonsoft.Json;

namespace Crave.Integrations.POS.Deliverect.Domain.Models
{
    public class SnoozeProductsResult
    {
        [JsonProperty("action")]
        public string Action { get; set; } = "snooze";

        [JsonProperty("data")]
        public SnoozeProductsData Data { get; set; } = new SnoozeProductsData();
    }
}
