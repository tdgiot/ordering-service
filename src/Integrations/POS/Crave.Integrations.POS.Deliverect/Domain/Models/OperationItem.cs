﻿using System;
using Newtonsoft.Json;

namespace Crave.Integrations.POS.Deliverect.Domain.Models
{
    public class OperationItem
    {
        [JsonProperty("_id")]
        public string Id { get; set; }

        [JsonProperty("plu")]
        public string Plu { get; set; }

        [JsonProperty("snoozeStart")]
        public DateTime SnoozeStart { get; set; }

        [JsonProperty("snoozeEnd")]
        public DateTime SnoozeEnd { get; set; }
    }
}
