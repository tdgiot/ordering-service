﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Crave.Integrations.POS.Deliverect.Domain.Models
{
    public class Item
    {
        [JsonProperty("plu")]
        public string Plu { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("price")]
        public int Price { get; set; }

        [JsonProperty("quantity")]
        public int Quantity { get; set; }

        [JsonProperty("productType")]
        public Enums.ProductType ProductType { get; set; }

        [JsonProperty("remark")]
        public string Remark { get; set; }

        [JsonProperty("subItems")]
        public List<Item> SubItems { get; set; }
    }
}