﻿using Newtonsoft.Json;

namespace Crave.Integrations.POS.Deliverect.Domain.Models
{
    public class Tax
    {
        [JsonProperty("taxes")]
        public int Taxes { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
            
        [JsonProperty("total")]
        public int Total { get; set; }
    }
}