﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Crave.Integrations.POS.Deliverect.Domain.Models
{
    public class SnoozeProductsData
    {
        [JsonProperty("locationId")]
        public string LocationId { get; set; }

        [JsonProperty("allSnoozedItems")]
        public List<string> AllSnoozedItems { get; set; } = new List<string>();
    }
}
