﻿using Newtonsoft.Json;

namespace Crave.Integrations.POS.Deliverect.Domain.Models
{
    public class Meta
    {
        [JsonProperty("page")]
        public int Page { get; set; }

        [JsonProperty("max_results")]
        public int MaxResults { get; set; }

        [JsonProperty("total")]
        public int Total { get; set; }
    }
}
