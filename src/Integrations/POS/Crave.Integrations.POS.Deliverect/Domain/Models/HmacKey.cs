using System;
using System.Linq;
using System.Text;

namespace Crave.Integrations.POS.Deliverect.Domain.Models
{
	public class HmacKey
	{
		private readonly byte[] value;

		private HmacKey(byte[] key)
		{
			if (key == null)
			{
				throw new ArgumentNullException(nameof(key));
			}

			if (!key.Any())
			{
				throw new ArgumentException("Invalid HMAC key.");
			}

			value = key;
		}

		public static implicit operator HmacKey(string key) => Encoding.UTF8.GetBytes(key);

		public static implicit operator HmacKey(byte[] key) => new HmacKey(key);

		public static implicit operator byte[](HmacKey payload) => payload.value;
	}
}
