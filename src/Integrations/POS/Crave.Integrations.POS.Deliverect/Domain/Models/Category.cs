﻿using System;
using Newtonsoft.Json;

namespace Crave.Integrations.POS.Deliverect.Domain.Models
{
    public class Category
    {
        [JsonProperty("_id")]
        public string Id { get; set; }

        [JsonProperty("account")]
        public string Account { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("nameTranslations")]
        public object NameTranslations { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("descriptionTranslations")]
        public object DescriptionTranslations { get; set; }

        [JsonProperty("availabilities")]
        public Availability[] Availabilities { get; set; }

        [JsonProperty("imageUrl")]
        public Uri ImageUrl { get; set; }

        [JsonProperty("products")]
        public string[] Products { get; set; }

        [JsonProperty("menu")]
        public string Menu { get; set; }
    }
}
