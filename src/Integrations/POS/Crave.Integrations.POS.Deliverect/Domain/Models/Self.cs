﻿using Newtonsoft.Json;

namespace Crave.Integrations.POS.Deliverect.Domain.Models
{
    public class Self
    {
        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("href")]
        public string Href { get; set; }
    }
}
