using System;
using System.Text;

namespace Crave.Integrations.POS.Deliverect.Domain.Models
{
	public class Payload
	{
		private readonly string value;

		private Payload(string payload) => value = payload;

		public static implicit operator Payload(string value) => new Payload(value);

		public static implicit operator string(Payload payload) => payload.value;

		public static implicit operator byte[](Payload payload) => payload == null ? Array.Empty<byte>() : Encoding.UTF8.GetBytes(payload.value);
	}
}
