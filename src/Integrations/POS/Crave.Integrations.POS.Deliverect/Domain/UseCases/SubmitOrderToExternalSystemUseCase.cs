using Crave.Enums;
using Crave.Integrations.POS.Deliverect.Domain.Requests;
using Crave.Integrations.POS.Deliverect.Interfaces.Factories;
using Crave.Integrations.POS.Deliverect.Interfaces.Repositories;
using Crave.Integrations.POS.Deliverect.Interfaces.UseCases;
using Crave.Integrations.POS.Interfaces;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.Extensions;
using Crave.Ordering.Shared.Domain;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;

namespace Crave.Integrations.POS.Deliverect.Domain.UseCases
{
    public class SubmitOrderToExternalSystemUseCase : ISubmitOrderToExternalSystemUseCase
    {
        private readonly IExternalSystemLogger externalSystemLogger;
        private readonly IOrderRepository orderRepository;
        private readonly ConvertOrderUseCase convertOrderUseCase;
        private readonly IDeliverectClientFactory deliverectClientFactory;

        public SubmitOrderToExternalSystemUseCase(IExternalSystemLogger externalSystemLogger,
                                                  IOrderRepository orderRepository,
                                                  ConvertOrderUseCase convertOrderUseCase,
                                                  IDeliverectClientFactory deliverectClientFactory)
        {
            this.externalSystemLogger = externalSystemLogger;
            this.orderRepository = orderRepository;
            this.convertOrderUseCase = convertOrderUseCase;
            this.deliverectClientFactory = deliverectClientFactory;
        }

        public async Task<Response> ExecuteAsync(SubmitOrderToExternalSystemRequest request)
        {
            ExternalSystemEntity externalSystem = request.ExternalSystem;

            OrderEntity order = await orderRepository.GetOrderAsync(request.OrderId);
            if (order.IsNullOrNew())
            {
                return Response.AsError($"Order with id {request.OrderId} not found");
            }

            Response<OrderRequest> convertOrderResponse = convertOrderUseCase.Execute(new ConvertOrderRequest(externalSystem, order));
            if (!convertOrderResponse.Success)
            {
                await LogToExternalSystemLogAsync(externalSystem, order, convertOrderResponse);
                return Response.AsError(convertOrderResponse.Message);
            }

            OrderRequest orderRequest = convertOrderResponse.Model!;
            orderRequest.ChannelLinkId = externalSystem.StringValue3;

            Response response = await SubmitOrderAsync(externalSystem, orderRequest);

            await LogToExternalSystemLogAsync(externalSystem, order, response, orderRequest);

            return response;
        }

        private async Task<Response> SubmitOrderAsync(ExternalSystemEntity externalSystem, OrderRequest orderRequest)
        {
            try
            {
                DeliverectClient deliverectClient = deliverectClientFactory.Create(externalSystem);
                Response response = await deliverectClient.DispatchOrderAsync(orderRequest);
                return response.Success ?
                    Response.AsSuccess("Successfully dispatched order") :
                    Response.AsError(response.Message);
            }
            catch (Exception ex)
            {
                return Response.AsError($"Exception while trying to dispatch order. (Exception={ex.Message})");
            }
        }

        private async Task LogToExternalSystemLogAsync(ExternalSystemEntity externalSystem, OrderEntity order, Response response, OrderRequest? request = null) => await externalSystemLogger.LogAsync(log =>
        {
            log.LogType = ExternalSystemLogType.DispatchOrder;
            log.ExternalSystemId = externalSystem.ExternalSystemId;
            log.OrderId = order.OrderId;
            log.Message = response.Message;
            log.Success = response.Success;
            log.Raw = request == null ? string.Empty : JsonConvert.SerializeObject(request);
        });
    }
}
