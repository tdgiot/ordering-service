using Crave.Enums;
using Crave.Integrations.POS.Deliverect.Domain.Requests;
using Crave.Integrations.POS.Deliverect.Interfaces.Repositories;
using Crave.Integrations.POS.Deliverect.Interfaces.UseCases;
using Crave.Integrations.POS.Interfaces.Repositories;
using Crave.Ordering.ProcessRoute.Models;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.Extensions;
using Crave.Ordering.Shared.Domain;
using Crave.Ordering.Shared.Gateway.Ordering;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace Crave.Integrations.POS.Deliverect.Domain.UseCases
{
    public class DispatchOrderUseCase
    {
        private readonly ILogger<DispatchOrderUseCase> logger;
        private readonly IExternalSystemRepository externalSystemRepository;
        private readonly IOrderRoutestephandlerRepository orderRoutestephandlerRepository;
        private readonly ISubmitOrderToExternalSystemUseCase submitOrderToExternalSystemUseCase;
        private readonly IOrderingGateway orderingGateway;

        public DispatchOrderUseCase(ILogger<DispatchOrderUseCase> logger,
                                    IExternalSystemRepository externalSystemRepository,
                                    IOrderRoutestephandlerRepository orderRoutestephandlerRepository,
                                    ISubmitOrderToExternalSystemUseCase submitOrderToExternalSystemUseCase,
                                    IOrderingGateway orderingGateway)
        {
            this.logger = logger;
            this.externalSystemRepository = externalSystemRepository;
            this.orderRoutestephandlerRepository = orderRoutestephandlerRepository;
            this.submitOrderToExternalSystemUseCase = submitOrderToExternalSystemUseCase;
            this.orderingGateway = orderingGateway;
        }

        public async Task<Response> ExecuteAsync(DispatchOrderRequest request)
        {
            logger.LogInformation($"Received dispatch order request for order {request.OrderId} and external system {request.ExternalSystemId}");

            Response response = await ExecuteInternalAsync(request);

            if (!response.Success)
            {
                logger.LogWarning(response.Message);
            }
            else
            {
                logger.LogInformation($"Successfully dispatched order {request.OrderId} for external system {request.ExternalSystemId}");
            }

            return response;
        }

        private async Task<Response> ExecuteInternalAsync(DispatchOrderRequest request)
        {
            ExternalSystemEntity externalSystem = await externalSystemRepository.GetByIdAsync(request.ExternalSystemId);
            if (externalSystem.IsNullOrNew())
            {
                // in this case rerun the register process from the Deliverect portal               
                return Response.AsNotFound($"External system not found {request.ExternalSystemId}");
            }

            OrderRoutestephandlerEntity handler = await orderRoutestephandlerRepository.GetByOrderIdAndStatusAsync(request.OrderId, OrderRoutestephandlerStatus.WaitingToBeRetrieved);
            if (handler.IsNullOrNew())
            {
                return Response.AsNotFound($"No handler found with order id {request.OrderId} and status {OrderRoutestephandlerStatus.WaitingToBeRetrieved}");
            }

            bool updatedHandlerToRetrieved = await UpdateHandlerStatus(handler, OrderRoutestephandlerStatus.RetrievedByHandler);
            if (!updatedHandlerToRetrieved)
            {
                return Response.AsError($"Failed to update handler {handler.OrderRoutestephandlerId} to status {OrderRoutestephandlerStatus.RetrievedByHandler}");
            }

            Response response = await submitOrderToExternalSystemUseCase.ExecuteAsync(new SubmitOrderToExternalSystemRequest(externalSystem, handler.OrderId));

            if (!response.Success)
            {
                return await HandleSubmitOrderFailedResponse(handler, response);
            }

            return response;
        }

        private async Task<Response> HandleSubmitOrderFailedResponse(OrderRoutestephandlerEntity handler, Response response)
        {
            bool updatedHandlerFailed = await UpdateHandlerStatus(handler, OrderRoutestephandlerStatus.Failed, response.Message);
            if (!updatedHandlerFailed)
            {
                return Response.AsError($"Failed to update handler {handler.OrderRoutestephandlerId} to status {OrderRoutestephandlerStatus.Failed}. (Message={response.Message})");
            }

            await orderingGateway.FireRoutestepStatusChangedEventAsync(new RoutestepStatusChangedEvent { OrderRoutestephandlerId = handler.OrderRoutestephandlerId });
            return response;
        }

        private async Task<bool> UpdateHandlerStatus(OrderRoutestephandlerEntity handler, OrderRoutestephandlerStatus status, string message = "")
        {
            handler.Status = status;
            handler.StatusText = status.ToString();
            handler.ErrorText = message;

            return await orderRoutestephandlerRepository.SaveAsync(handler);
        }
    }
}
