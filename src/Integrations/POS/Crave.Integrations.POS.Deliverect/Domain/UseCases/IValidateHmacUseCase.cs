﻿using Crave.Integrations.POS.Deliverect.Domain.Requests;

namespace Crave.Integrations.POS.Deliverect.Domain.UseCases
{
    public interface IValidateHmacUseCase
    {
        bool Execute(ValidateHmacRequest request);
    }
}