using Crave.Enums;
using Crave.Integrations.POS.Deliverect.Domain.Enums;
using Crave.Integrations.POS.Deliverect.Domain.Models;
using Crave.Integrations.POS.Deliverect.Domain.Requests;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.Extensions;
using Crave.Ordering.Shared.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OrderType = Crave.Integrations.POS.Deliverect.Domain.Enums.OrderType;

namespace Crave.Integrations.POS.Deliverect.Domain.UseCases
{
    public class ConvertOrderUseCase
    {
        private const int DECIMAL_MULTIPLIER = 100;
        private const int DECIMAL_PLACES = 2;
        private const int ORDER_REQUEST_CHANNEL = 1000;
        private const string TRAY_CHARGE_ALTERATION_NAME = "Tray Charge PPHE";

        public Response<OrderRequest> Execute(ConvertOrderRequest request)
        {
            try
            {
                OrderRequest orderRequest = ExecuteInternal(request.ExternalSystem, request.Order);
                return Response<OrderRequest>.AsSuccess(orderRequest);
            }
            catch (Exception ex)
            {
                return Response<OrderRequest>.AsError(ex.Message);
            }
        }

        private OrderRequest ExecuteInternal(ExternalSystemEntity externalSystem, OrderEntity order)
        {
            DateTime currentTimeUtc = DateTime.UtcNow;

            OrderRequest request = new OrderRequest();
            request.ChannelOrderId = $"{order.OrderId}";
            request.ChannelOrderDisplayId = $"{order.OrderId}";
            request.By = "";
            request.Channel = ORDER_REQUEST_CHANNEL;
            request.PickupTime = currentTimeUtc;
            request.EstimatedPickupTime = currentTimeUtc;
            request.DeliveryTime = currentTimeUtc;
            request.DeliveryIsAsap = true;
            request.OrderIsAlreadyPaid = order.CheckoutMethodType == CheckoutType.PaymentProvider;
            request.DecimalDigits = DECIMAL_PLACES;
            request.NumberOfCustomers = order.CoversCount ?? 1;
            request.DiscountTotal = 0;

            bool pricesIncludeTaxes = externalSystem.BoolValue2.GetValueOrDefault(true);

            EnrichWithNotes(request, order);
            EnrichWithOrderType(request, order);
            EnrichWithTableDetails(request, order);
            EnrichWithCustomerDetails(request, order);
            EnrichWithDeliveryAddress(request, order);
            EnrichWithPaymentDetails(request, order);
            EnrichWithOrderItems(request, order, pricesIncludeTaxes);
            EnrichWithDeliveryCharge(request, order);
            EnrichWithServiceCharge(request, order);
            EnrichWithTip(request, order);
            EnrichWithTrayCharge(request, order);

            return request;
        }

        private static void EnrichWithNotes(OrderRequest request, OrderEntity order)
        {
            PaymentTransactionEntity? paymentTransaction = order.PaymentTransactionCollection.FirstOrDefault(x => x.Status == PaymentTransactionStatus.Paid);

            string coversCount = order.CoversCount != null ? order.CoversCount.ToString() : string.Empty;

            StringBuilder sb = new StringBuilder();
            sb.AppendFormatLine("Order Id: {0},", order.OrderId);
            sb.AppendFormatLine("Checkout method: {0}({1}),", order.CheckoutMethodName, order.CheckoutMethodType.ToString());
            sb.AppendFormatLine("Payment method: {0}({1}),", paymentTransaction?.PaymentMethod, paymentTransaction?.CardSummary);

            if (!string.IsNullOrWhiteSpace(coversCount))
            {
                sb.AppendFormatLine("Cover Count: {0}", coversCount);
            }

            sb.AppendLine();
            sb.Append(order.Notes);

            request.Note = sb.ToString();
        }

        private static void EnrichWithOrderType(OrderRequest request, OrderEntity order)
        {
            switch (order.ServiceMethodType)
            {
                case ServiceMethodType.Delivery:
                    request.OrderType = OrderType.Delivery;
                    break;
                case ServiceMethodType.PickUp:
                    request.OrderType = OrderType.Pickup;
                    break;
                case ServiceMethodType.TableService:
                case ServiceMethodType.RoomService:
                    request.OrderType = OrderType.EatIn;
                    break;
            }
        }

        private static void EnrichWithTableDetails(OrderRequest request, OrderEntity order)
        {
            request.Table = order.DeliverypointName;
        }

        private void EnrichWithCustomerDetails(OrderRequest request, OrderEntity order)
        {
            request.Customer = new Customer
            {
                Name = order.CustomerLastname,
                Email = order.Email,
                PhoneNumber = order.CustomerPhonenumber
            };
        }

        private void EnrichWithDeliveryAddress(OrderRequest request, OrderEntity order)
        {
            if (!order.DeliveryInformationId.HasValue)
            {
                return;
            }

            request.DeliveryAddress = new DeliveryAddress
            {
                Street = order.DeliveryInformation.Address,
                //deliveryAddress.StreetNumber = order.DeliverypointNumber.Address; // TODO: We combine street and number in address.. what to do here?
                City = order.DeliveryInformation.City,
                PostalCode = order.DeliveryInformation.Zipcode,
                ExtraAddressInfo = order.DeliveryInformation.Instructions
            };
        }

        private static void EnrichWithPaymentDetails(OrderRequest request, OrderEntity order)
        {
            Payment payment = new Payment();
            payment.Amount = ConvertPriceToDeliverectCost(order.Total);

            switch (order.CheckoutMethodType)
            {
                case CheckoutType.PaymentProvider:
                    payment.Type = PaymentType.PaidOnline;
                    break;
                default:
                    payment.Type = PaymentType.Other;
                    break;
            }

            request.Payment = payment;
        }

        private static void EnrichWithDeliveryCharge(OrderRequest request, OrderEntity order)
        {
            OrderitemEntity? deliveryChargeItem = order.OrderitemCollection.FirstOrDefault(orderItem => orderItem.Type == OrderitemType.DeliveryCharge);
            if (deliveryChargeItem == null)
            {
                request.DeliveryCost = 0;
                return;
            }

            request.DeliveryCost = ConvertPriceToDeliverectCost(deliveryChargeItem.Total);
        }

        private static void EnrichWithServiceCharge(OrderRequest request, OrderEntity order)
        {
            OrderitemEntity? serviceChargeItem = order.OrderitemCollection.FirstOrDefault(orderItem => orderItem.Type == OrderitemType.ServiceCharge);
            if (serviceChargeItem == null)
            {
                request.ServiceCharge = 0;
                return;
            }

            request.ServiceCharge = ConvertPriceToDeliverectCost(serviceChargeItem.Total);
        }

        private static void EnrichWithTip(OrderRequest request, OrderEntity order)
        {
            OrderitemEntity? tipItem = order.OrderitemCollection.FirstOrDefault(orderItem => orderItem.Type == OrderitemType.Tip);
            if (tipItem == null)
            {
                request.Tip = 0;
                return;
            }

            request.Tip = ConvertPriceToDeliverectCost(tipItem.Total);
        }

        private void EnrichWithOrderItems(OrderRequest request, OrderEntity order, bool pricesIncludeTaxes)
        {
            request.Items = order.OrderitemCollection
                                 .Where(orderItem => orderItem.Type == OrderitemType.Product)
                                 .Select(orderItem => ConvertOrderItemToDeliverectItem(orderItem, pricesIncludeTaxes))
                                 .ToList();

            if (!pricesIncludeTaxes)
            {
                EnrichWithTaxes(request, order);
            }
        }

        private static Item ConvertOrderItemToDeliverectItem(OrderitemEntity orderItem, bool pricesIncludeTaxes)
        {
            ProductEntity product = orderItem.Product;

            if (!product.ExternalProductId.HasValue)
            {
                throw new InvalidOperationException($"Product with id {orderItem.ProductId} not linked to an external product.");
            }
            else if (!product.IsAvailable)
            {
                throw new InvalidOperationException($"Product with id {orderItem.ProductId} unavailable.");
            }

            IList<OrderitemAlterationitemEntity> optionTypeOptions = orderItem.OrderitemAlterationitemCollection
                                                                              .Where(alterationitem => alterationitem.AlterationType == AlterationType.Options &&
                                                                                                       !alterationitem.AlterationName.Equals(TRAY_CHARGE_ALTERATION_NAME, StringComparison.InvariantCultureIgnoreCase))
                                                                              .ToList();

            Item item = new Item();
            item.Plu = product.ExternalProduct.Id;
            item.Name = orderItem.ProductName;
            item.Quantity = orderItem.Quantity;
            item.Price = ConvertPriceToDeliverectCost(pricesIncludeTaxes ? orderItem.PriceInTax : orderItem.PriceExTax);
            item.ProductType = (Enums.ProductType)product.ExternalProduct.Type;

            if (optionTypeOptions.Any())
            {
                item.SubItems = optionTypeOptions
                    .Select(x => ConvertAlterationItemToDeliverectItem(x, pricesIncludeTaxes))
                    .ToList();
            }

            return item;
        }

        private static Item ConvertAlterationItemToDeliverectItem(OrderitemAlterationitemEntity orderItemAlterationItem, bool pricesIncludeTaxes)
        {
            AlterationitemEntity alterationItem = orderItemAlterationItem.Alterationitem;
            AlterationoptionEntity alterationoption = alterationItem.Alterationoption;

            if (!alterationoption.ExternalProductId.HasValue)
            {
                throw new InvalidOperationException($"Alteration option with id {alterationoption.AlterationoptionId} not linked to an external product.");
            }
            else if (!alterationoption.IsAvailable)
            {
                throw new InvalidOperationException($"Alteration option with id {alterationoption.AlterationoptionId} unavailable.");
            }

            Item item = new Item();
            item.Plu = alterationoption.ExternalProduct.Id;
            item.Name = orderItemAlterationItem.AlterationoptionName;
            item.Price = ConvertPriceToDeliverectCost(pricesIncludeTaxes ? orderItemAlterationItem.PriceInTax : orderItemAlterationItem.PriceExTax);
            item.Quantity = 1;
            item.ProductType = (Enums.ProductType)alterationoption.ExternalProduct.Type;

            return item;
        }

        private static void EnrichWithTrayCharge(OrderRequest request, OrderEntity order)
        {
            OrderitemAlterationitemEntity? trayChargeOrderitemAlterationitem = order.OrderitemCollection
                                                                   .SelectMany(x => x.OrderitemAlterationitemCollection)
                                                                   .FirstOrDefault(x => x.AlterationName.Equals(TRAY_CHARGE_ALTERATION_NAME, StringComparison.InvariantCultureIgnoreCase));

            if (trayChargeOrderitemAlterationitem == null ||
                trayChargeOrderitemAlterationitem.Alterationitem.Alterationoption.IsNullOrNew() ||
                trayChargeOrderitemAlterationitem.Alterationitem.Alterationoption.ExternalProduct.IsNullOrNew())
            {
                return;
            }

            Item item = new Item();
            item.Plu = trayChargeOrderitemAlterationitem.Alterationitem.Alterationoption.ExternalProduct.Id;
            item.Name = trayChargeOrderitemAlterationitem.AlterationoptionName;
            item.Quantity = 1;
            item.Price = ConvertPriceToDeliverectCost(trayChargeOrderitemAlterationitem.PriceInTax);
            item.ProductType = Enums.ProductType.Product;

            request.Items?.Add(item);
        }

        private void EnrichWithTaxes(OrderRequest request, OrderEntity order)
        {
            Dictionary<int, Tax> taxes = new Dictionary<int, Tax>();

            foreach (OrderitemEntity orderitem in order.OrderitemCollection)
            {
                if (orderitem.Type != OrderitemType.Product)
                {
                    continue;
                }

                int taxTariffId = orderitem.TaxTariffId.GetValueOrDefault(0);
                int taxAmount = ConvertPriceToDeliverectCost(orderitem.TaxTotal);

                if (taxes.TryGetValue(taxTariffId, out Tax tax))
                {
                    tax.Total += taxAmount;
                }
                else
                {
                    taxes.Add(taxTariffId, new Tax
                    {
                        Taxes = taxTariffId,
                        Name = orderitem.TaxName,
                        Total = taxAmount
                    });
                }
            }

            request.Taxes = taxes.Values.ToList();
        }

        private static int ConvertPriceToDeliverectCost(decimal price) => Convert.ToInt32(price * DECIMAL_MULTIPLIER);
    }
}
