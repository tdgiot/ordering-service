using Crave.Integrations.POS.Deliverect.Domain.Requests;
using Crave.Integrations.POS.Deliverect.Extensions;

namespace Crave.Integrations.POS.Deliverect.Domain.UseCases
{
    public class ValidateHmacUseCase : IValidateHmacUseCase
    {
        public bool Execute(ValidateHmacRequest request) => request.Payload.IsValidHmac(request.HmacKey, request.HmacSignature);
    }
}
