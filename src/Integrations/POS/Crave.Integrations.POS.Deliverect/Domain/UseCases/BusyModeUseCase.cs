﻿using System;
using System.Threading.Tasks;
using Crave.Enums;
using Crave.Integrations.POS.Deliverect.Domain.Constants;
using Crave.Integrations.POS.Deliverect.Domain.Requests;
using Crave.Integrations.POS.Deliverect.Domain.Responses;
using Crave.Integrations.POS.Domain.UseCases.Requests;
using Crave.Integrations.POS.Interfaces;
using Crave.Integrations.POS.Interfaces.Repositories;
using Crave.Integrations.POS.Interfaces.UseCases;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.Extensions;
using Crave.Ordering.Shared.Domain;
using Newtonsoft.Json;

namespace Crave.Integrations.POS.Deliverect.Domain.UseCases
{
    public class BusyModeUseCase
    {
        private readonly IExternalSystemLogger externalSystemLogger;
        private readonly IExternalSystemRepository externalSystemRepository;
        private readonly IUpdateItemsLinkedToExternalProductsUseCase updateItemsLinkedToExternalProductsUseCase;

        public BusyModeUseCase(IExternalSystemLogger externalSystemLogger,
            IExternalSystemRepository externalSystemRepository,
            IUpdateItemsLinkedToExternalProductsUseCase updateItemsLinkedToExternalProductsUseCase)
        {
            this.externalSystemLogger = externalSystemLogger;
            this.externalSystemRepository = externalSystemRepository;
            this.updateItemsLinkedToExternalProductsUseCase = updateItemsLinkedToExternalProductsUseCase;
        }

        public async Task<Response<BusyModeResponse>> ExecuteAsync(BusyModeRequest request)
        {
            ExternalSystemEntity? externalSystem = await externalSystemRepository.GetByChannelLinkIdAsync(request.ChannelLinkId);
            if (externalSystem.IsNullOrNew())
            {
                return Response<BusyModeResponse>.AsNotFound($"No external system found (ChannelLinkId={request.ChannelLinkId})");
            }

            string logMessage;
            if (request.Status.Equals(BusyMode.Paused, StringComparison.InvariantCultureIgnoreCase))
            {
                externalSystem.IsBusy = true;
                logMessage = "Ordering paused, busy mode activated";
            }
            else if (request.Status.Equals(BusyMode.Online, StringComparison.InvariantCultureIgnoreCase))
            {
                externalSystem.IsBusy = false;
                logMessage = "Ordering active, busy mode disabled";
            }
            else
            {
                await LogToExternalSystemLog(externalSystem.ExternalSystemId, $"Unable to handle busy mode status change (status={request.Status})", false, request);
                return Response<BusyModeResponse>.AsError($"Unable to handle busy mode status change (status={request.Status})");
            }

            if (!await externalSystemRepository.SaveAsync(externalSystem))
            {
                await LogToExternalSystemLog(externalSystem.ExternalSystemId, $"Save failed, could not update external systems busy mode status (externalSystemId={externalSystem.ExternalSystemId}, status={request.Status})", false, request);
                return Response<BusyModeResponse>.AsError($"Save failed, could not update external systems busy mode status (externalSystemId={externalSystem.ExternalSystemId}, status={request.Status})");
            }

            await UpdateProductsLinkedToExternalProducts(externalSystem.ExternalSystemId);

            await LogToExternalSystemLog(externalSystem.ExternalSystemId, logMessage, true, request);
            
            return Response<BusyModeResponse>.AsSuccess(new BusyModeResponse
            {
                Status = request.Status
            });
        }

        private async Task<Response> UpdateProductsLinkedToExternalProducts(int externalSystemId)
        {
            UpdateItemsLinkedToExternalProductsRequest request = new UpdateItemsLinkedToExternalProductsRequest(externalSystemId);
            return await updateItemsLinkedToExternalProductsUseCase.ExecuteAsync(request);
        }

        private async Task LogToExternalSystemLog(int externalSystemId, string message, bool success, BusyModeRequest request) => await externalSystemLogger.LogAsync(log =>
        {
            log.LogType = ExternalSystemLogType.BusyMode;
            log.ExternalSystemId = externalSystemId;
            log.Success = success;
            log.Message = message;
            log.Raw = JsonConvert.SerializeObject(request);
        });
    }
}
