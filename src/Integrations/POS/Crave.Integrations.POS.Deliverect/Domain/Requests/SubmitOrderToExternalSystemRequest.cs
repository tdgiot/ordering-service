﻿using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Integrations.POS.Deliverect.Domain.Requests
{
    public class SubmitOrderToExternalSystemRequest
    {
        public SubmitOrderToExternalSystemRequest(ExternalSystemEntity externalSystem, int orderId)
        {
            ExternalSystem = externalSystem;
            OrderId = orderId;
        }

        public ExternalSystemEntity ExternalSystem { get; }
        public int OrderId { get; }
    }
}
