using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Integrations.POS.Deliverect.Domain.Requests
{
    public class ConvertOrderRequest
    {
        public ConvertOrderRequest(
            ExternalSystemEntity externalSystem,
            OrderEntity order)
        {
            ExternalSystem = externalSystem;
            Order = order;
        }

        public ExternalSystemEntity ExternalSystem { get; }
        public OrderEntity Order { get; }
    }
}
