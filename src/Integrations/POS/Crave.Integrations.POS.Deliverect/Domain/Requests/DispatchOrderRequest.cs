﻿namespace Crave.Integrations.POS.Deliverect.Domain.Requests
{
    public class DispatchOrderRequest
    {
        public DispatchOrderRequest(int externalSystemId,
                                    int orderId)
        {
            ExternalSystemId = externalSystemId;
            OrderId = orderId;
        }

        public int ExternalSystemId { get; }
        public int OrderId { get; }
    }
}
