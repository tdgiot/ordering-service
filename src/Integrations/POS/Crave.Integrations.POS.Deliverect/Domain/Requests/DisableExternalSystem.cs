﻿namespace Crave.Integrations.POS.Deliverect.Domain.Requests
{
    public class DisableExternalSystem
    {
        public string? ChannelLinkId { get; set; }
    }
}