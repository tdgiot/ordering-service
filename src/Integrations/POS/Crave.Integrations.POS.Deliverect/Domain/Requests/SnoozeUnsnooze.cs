﻿namespace Crave.Integrations.POS.Deliverect.Domain.UseCases
{
    public class SnoozeUnsnooze
    {
        public int ExternalSystemId { get; set; }
        
        public string[]? ProductIds { get; set; }
    }
}