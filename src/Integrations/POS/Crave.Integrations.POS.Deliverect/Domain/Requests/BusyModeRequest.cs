﻿using Newtonsoft.Json;

namespace Crave.Integrations.POS.Deliverect.Domain.Requests
{
    public class BusyModeRequest
    {
        [JsonProperty("accountId")]
        public string? AccountId { get; set; }

        [JsonProperty("locationId")]
        public string? LocationId { get; set; }

        [JsonProperty("channelLinkId")]
        public string? ChannelLinkId { get; set; }

        [JsonProperty("status")]
        public string? Status { get; set; }
    }
}
