﻿using Newtonsoft.Json;

namespace Crave.Integrations.POS.Deliverect.Domain.Requests
{
    public class AuthenticateRequest
    {
        [JsonProperty("client_id")]
        public string? ClientId { get; set; }

        [JsonProperty("client_secret")]
        public string? ClientSecret { get; set; }

        [JsonProperty("audience")]
        public string? Audience { get; set; }

        [JsonProperty("grant_type")]
        public string? GrantType { get; set; }
    }
}
