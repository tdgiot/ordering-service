using Crave.Integrations.POS.Deliverect.Domain.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Crave.Integrations.POS.Deliverect.Domain.Requests
{
    public class OrderRequest
    {
        [JsonProperty("channelOrderId")]
        public string? ChannelOrderId { get; set; }

        [JsonProperty("channelOrderDisplayId")]
        public string? ChannelOrderDisplayId { get; set; }

        [JsonProperty("channelLinkId")]
        public string? ChannelLinkId { get; set; }

        [JsonProperty("by")]
        public string? By { get; set; }

        [JsonProperty("orderType")]
        public Enums.OrderType OrderType { get; set; }

        [JsonProperty("table")]
        public string? Table { get; set; }

        [JsonProperty("channel")]
        public int Channel { get; set; }

        [JsonProperty("pickupTime")]
        public DateTime PickupTime { get; set; }

        [JsonProperty("estimatedPickupTime")]
        public DateTime EstimatedPickupTime { get; set; }

        [JsonProperty("deliveryTime")]
        public DateTime DeliveryTime { get; set; }

        [JsonProperty("deliveryIsAsap")]
        public bool DeliveryIsAsap { get; set; }

        [JsonProperty("courier")]
        public string? Courier { get; set; }

        [JsonProperty("customer")]
        public Customer? Customer { get; set; }

        [JsonProperty("deliveryAddress")]
        public DeliveryAddress? DeliveryAddress { get; set; }

        [JsonProperty("orderIsAlreadyPaid")]
        public bool OrderIsAlreadyPaid { get; set; }

        [JsonProperty("payment")]
        public Payment? Payment { get; set; }

        [JsonProperty("note")]
        public string? Note { get; set; }

        [JsonProperty("items")]
        public List<Item>? Items { get; set; }

        [JsonProperty("decimalDigits")]
        public int DecimalDigits { get; set; } = 2;

        [JsonProperty("numberOfCustomers")]
        public int NumberOfCustomers { get; set; }

        [JsonProperty("deliveryCost")]
        public int DeliveryCost { get; set; }

        [JsonProperty("serviceCharge")]
        public int ServiceCharge { get; set; }

        [JsonProperty("tip")]
        public int Tip { get; set; }

        [JsonProperty("discountTotal")]
        public int DiscountTotal { get; set; }

        [JsonProperty("taxes")]
        public List<Tax>? Taxes { get; set; }
    }
}
