using Crave.Integrations.POS.Deliverect.Domain.Models;

namespace Crave.Integrations.POS.Deliverect.Domain.Requests
{
	public class ValidateHmacRequest
	{
		public Payload Payload { get; }
		public string HmacSignature { get; }
		public HmacKey HmacKey { get; }

		public ValidateHmacRequest(Payload payload, string hmacSignature, HmacKey hmacKey)
		{
			Payload = payload;
			HmacSignature = hmacSignature;
			HmacKey = hmacKey;
		}
	}
}
