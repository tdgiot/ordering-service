﻿using System;
using Crave.Integrations.POS.Deliverect.Domain.Enums;
using Newtonsoft.Json;

namespace Crave.Integrations.POS.Deliverect.Domain.Requests
{
    public class OrderStatusRequest
    {
        [JsonProperty("channelOrderId")]
        public string? ChannelOrderId { get; set; }

        [JsonProperty("orderId")]
        public string? OrderId { get; set; }

        [JsonProperty("location")]
        public string? Location { get; set; }

        [JsonProperty("status")]
        public OrderStatus? Status { get; set; }

        [JsonProperty("timeStamp")]
        public DateTime? TimeStamp { get; set; }
    }
}