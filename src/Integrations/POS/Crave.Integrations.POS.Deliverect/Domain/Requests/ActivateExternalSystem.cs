﻿namespace Crave.Integrations.POS.Deliverect.Domain.Requests
{
    public class ActivateExternalSystem
    {
        public string? ChannelLinkId { get; set; }
    }
}