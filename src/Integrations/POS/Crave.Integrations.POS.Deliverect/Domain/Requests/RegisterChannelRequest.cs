﻿using Newtonsoft.Json;

namespace Crave.Integrations.POS.Deliverect.Domain.Requests
{
    public class RegisterChannelRequest
    {
        [JsonProperty("channelLocationId")]
        public string? ChannelLocationId { get; set; }
        
        [JsonProperty("channelLinkId")]
        public string? ChannelLinkId { get; set; }
    }
}