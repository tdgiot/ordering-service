﻿using Crave.Integrations.POS.Deliverect.Interfaces;

namespace Crave.Integrations.POS.Deliverect.Domain.Requests
{
	public class HmacValidatedRequest<TRequest> : IHmacValidationContent
	{
		public HmacValidatedRequest(TRequest originalRequest, IHmacValidationContent validationContent)
		{
			OriginalRequest = originalRequest;
			RequestBody = validationContent.RequestBody;
			HmacSignature = validationContent.HmacSignature;
		}

		public HmacValidatedRequest(TRequest originalRequest, string requestBody, string hmacSignature)
		{
			OriginalRequest = originalRequest;
			RequestBody = requestBody;
			HmacSignature = hmacSignature;
		}

		public TRequest OriginalRequest { get; }

		public string RequestBody { get; }

		public string HmacSignature { get; }
	}
}
