﻿using System.Collections.Generic;
using Crave.Integrations.POS.Deliverect.Domain.Models;
using Newtonsoft.Json;

namespace Crave.Integrations.POS.Deliverect.Domain.Requests
{
    public class SnoozeUnsnoozeRequest
    {
        [JsonProperty("accountId")]
        public string? AccountId { get; set; }

        [JsonProperty("locationId")]
        public string? LocationId { get; set; }

        [JsonProperty("channelLinkId")]
        public string? ChannelLinkId { get; set; }

        [JsonProperty("operations")]
        public List<Operation>? Operations { get; set; }
    }
}
