﻿namespace Crave.Integrations.POS.Deliverect.Domain.Constants
{
    public static class BusyMode
    {
        /// <summary>
        /// This is a request to enable busy mode.
        /// The store should be 'paused', or closed temporarily, indicating this is because the restaurant is busy.
        /// </summary>
        public const string Paused = "PAUSED";

        /// <summary>
        /// This is a request to disable busy mode. The store should be open as normal.
        /// </summary>
        public const string Online = "ONLINE";
    }
}
