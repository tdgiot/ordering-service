﻿namespace Crave.Integrations.POS.Deliverect.Domain.Constants
{
    public static class ActionNames
    {
        public const string Snooze = "snooze";

        public const string Unsnooze = "unsnooze";
    }
}
