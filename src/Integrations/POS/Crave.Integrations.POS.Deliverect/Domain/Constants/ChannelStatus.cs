﻿namespace Crave.Integrations.POS.Deliverect.Domain.Constants
{
    public static class ChannelStatus
    {
        /// <summary>
        /// A customer registers a location for this channel.
        /// </summary>
        public const string Register = "register";
        
        /// <summary>
        /// A customer wants to start receiving orders for this location.
        /// </summary>
        public const string Active = "active";

        /// <summary>
        /// A customer wants to stop receiving orders for this location.
        /// </summary>
        public const string Inactive = "inactive";
    }
}