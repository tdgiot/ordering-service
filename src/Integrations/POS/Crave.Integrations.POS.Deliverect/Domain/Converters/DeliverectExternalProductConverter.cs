﻿using Crave.Enums;
using Crave.Integrations.POS.Deliverect.Domain.Models;
using Crave.Integrations.POS.Interfaces;
using Crave.Integrations.POS.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Crave.Integrations.POS.Deliverect.Domain.Converters
{
    public class DeliverectExternalProductConverter : IExternalProductConverter<Menu>
    {
        private const int DECIMAL_DIVISOR = 100;

        public ExternalProduct[] Convert(Menu menu)
        {
            string menuId = menu.Categories.FirstOrDefault()?.Menu ?? throw new InvalidOperationException("No menu id found");

            List<ExternalProduct> externalProducts = new List<ExternalProduct>();

            if (menu.Products != null)
            {
                externalProducts.AddRange(ConvertProductsToExternalProducts(menu.Products));
            }

            if (menu.ModifierGroups != null)
            {
                externalProducts.AddRange(ConvertModifierGroupsToExternalProducts(menu.ModifierGroups));
            }

            if (menu.Modifiers != null)
            {
                externalProducts.AddRange(ConvertModifiersToExternalProducts(menu.Modifiers));
            }


            return externalProducts.Select(product =>
            {
                product.MenuId = menuId;
                return product;
            }).ToArray();
        }

        private static IEnumerable<ExternalProduct> ConvertProductsToExternalProducts(IDictionary<string, Product> products)
        {
            foreach (KeyValuePair<string, Product> kv in products)
            {
                Product? product = kv.Value;

                yield return new ExternalProduct
                {
                    Id = product.Plu,
                    Name = product.Name,
                    Price = ConvertPriceToDecimalPrice(product.Price),
                    ProductType = ExternalProductType.Product,
                    StringValue1 = kv.Key,
                    SubItemIds = product.SubProducts
                };
            }
        }

        public static IEnumerable<ExternalProduct> ConvertModifierGroupsToExternalProducts(IDictionary<string, ModifierGroup> modifierGroups)
        {
            foreach (KeyValuePair<string, ModifierGroup> kv in modifierGroups)
            {
                ModifierGroup? modifierGroup = kv.Value;

                yield return new ExternalProduct
                {
                    Id = modifierGroup.Plu,
                    Name = modifierGroup.Name,
                    Price = ConvertPriceToDecimalPrice(modifierGroup.Price),
                    MinOptions = modifierGroup.Min,
                    MaxOptions = modifierGroup.Max,
                    AllowDuplicates = modifierGroup.MultiMax > 1,
                    ProductType = ExternalProductType.ModifierGroup,
                    StringValue1 = kv.Key,
                    IntValue1 = modifierGroup.MultiMax,
                    SubItemIds = modifierGroup.SubProducts
                };
            }
        }

        private static IEnumerable<ExternalProduct> ConvertModifiersToExternalProducts(IDictionary<string, Modifier> modifiers)
        {
            foreach (KeyValuePair<string, Modifier> kv in modifiers)
            {
                Modifier? modifier = kv.Value;

                yield return new ExternalProduct
                {
                    Id = modifier.Plu,
                    Name = modifier.Name,
                    Price = ConvertPriceToDecimalPrice(modifier.Price),
                    ProductType = ExternalProductType.Modifier,
                    StringValue1 = kv.Key,
                    SubItemIds = modifier.SubProducts
                };
            }
        }

        private static decimal ConvertPriceToDecimalPrice(int price) => price > 0 ? ((decimal)price / DECIMAL_DIVISOR) : 0;
    }
}
