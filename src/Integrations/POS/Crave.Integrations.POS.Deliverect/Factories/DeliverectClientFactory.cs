﻿using System;
using Crave.Enums;
using Crave.Integrations.POS.Deliverect.Interfaces.Factories;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Security;
using Microsoft.Extensions.Caching.Memory;

namespace Crave.Integrations.POS.Deliverect.Factories
{
    public class DeliverectClientFactory : IDeliverectClientFactory
    {
        private readonly IMemoryCache cache;

        public DeliverectClientFactory(IMemoryCache cache)
        {
            this.cache = cache;
        }

        public DeliverectClient Create(ExternalSystemEntity externalSystem)
        {
            if (externalSystem.StringValue1.IsNullOrWhiteSpace())
            {
                throw new InvalidOperationException($"No client id configured for external system {externalSystem.ExternalSystemId}");
            }

            if (externalSystem.StringValue2.IsNullOrWhiteSpace())
            {
                throw new InvalidOperationException($"No client secret configured for external system {externalSystem.ExternalSystemId}");
            }

            string clientId = externalSystem.StringValue1;
            string clientSecret = Cryptographer.DecryptUsingCBC(externalSystem.StringValue2);

            DeliverectConfig config = new DeliverectConfig(clientId, clientSecret);

            if (externalSystem.Environment == ExternalSystemEnvironment.Live)
            {
                config.BaseUri = new Uri("https://api.deliverect.com");
            }
            else if (externalSystem.Environment == ExternalSystemEnvironment.Test)
            {
                config.BaseUri = new Uri("https://api.staging.deliverect.com");
            }
            else
            {
                throw new InvalidOperationException($"External system environment {externalSystem.Environment} not supported for external system {externalSystem.ExternalSystemId}");
            }

            return new DeliverectClient(cache, config);
        }
    }
}
