﻿using Crave.Enums;
using Crave.Integrations.POS.Deliverect.Domain.Models;
using Crave.Integrations.POS.Deliverect.Domain.Requests;
using Crave.Integrations.POS.Deliverect.Interfaces;
using Crave.Integrations.POS.Deliverect.Interfaces.Factories;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Security;

namespace Crave.Integrations.POS.Deliverect.Factories
{
	public class ValidateHmacRequestFactory : IValidateHmacRequestFactory
	{
		public ValidateHmacRequest Create(IHmacValidationContent hmacValidationContent, ExternalSystemEntity externalSystemEntity)
		{
			HmacKey hmacKey = externalSystemEntity.Environment == ExternalSystemEnvironment.Live 
				? GetHmacKey(externalSystemEntity) 
				: GetChannelLinkId(externalSystemEntity);

			return new ValidateHmacRequest(hmacValidationContent.RequestBody, hmacValidationContent.HmacSignature, hmacKey);
		}

		private static string GetChannelLinkId(ExternalSystemEntity externalSystemEntity) => externalSystemEntity.StringValue3;

		private static string GetHmacKey(ExternalSystemEntity externalSystemEntity) => Cryptographer.DecryptUsingCBC(externalSystemEntity.StringValue5);
	}
}
