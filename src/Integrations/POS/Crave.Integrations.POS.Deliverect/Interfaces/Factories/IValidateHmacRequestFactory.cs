﻿using Crave.Integrations.POS.Deliverect.Domain.Requests;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Integrations.POS.Deliverect.Interfaces.Factories
{
	public interface IValidateHmacRequestFactory
	{
		ValidateHmacRequest Create(IHmacValidationContent hmacValidationContent, ExternalSystemEntity externalSystemEntity);
	}
}
