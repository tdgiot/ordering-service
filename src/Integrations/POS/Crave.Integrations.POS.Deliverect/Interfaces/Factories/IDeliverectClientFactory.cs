﻿using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Integrations.POS.Deliverect.Interfaces.Factories
{
    public interface IDeliverectClientFactory
    {
        DeliverectClient Create(ExternalSystemEntity externalSystem);
    }
}
