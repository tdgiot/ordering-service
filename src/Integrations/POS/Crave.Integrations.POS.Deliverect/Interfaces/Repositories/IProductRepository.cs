﻿using System.Threading.Tasks;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;

namespace Crave.Integrations.POS.Deliverect.Interfaces.Repositories
{
    public interface IProductRepository
    {
        Task<EntityCollection<ProductEntity>> GetProductsLinkedToExternalProductsAsync(int externalSystemId);
    }
}
