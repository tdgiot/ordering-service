﻿using System.Threading.Tasks;
using Crave.Enums;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Integrations.POS.Deliverect.Interfaces.Repositories
{
    /// <summary>
    /// Interface which is used to implement order routestep handler repository classes.
    /// </summary>
    public interface IOrderRoutestephandlerRepository
    {
        Task<OrderRoutestephandlerEntity> GetByOrderIdAndStatusAsync(int orderId, OrderRoutestephandlerStatus orderRoutestephandlerStatus);
        Task<bool> SaveAsync(OrderRoutestephandlerEntity handler);
    }
}
