﻿using System.Threading.Tasks;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Integrations.POS.Deliverect.Interfaces.Repositories
{
    /// <summary>
    /// Interface which is used to implement order repository classes.
    /// </summary>
    public interface IOrderRepository
    {
        Task<OrderEntity> GetOrderAsync(int orderId);
    }
}
