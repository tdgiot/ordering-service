﻿namespace Crave.Integrations.POS.Deliverect.Interfaces
{
	public interface IHmacValidationContent
	{
		string RequestBody { get; }
		string HmacSignature { get; }
	}
}
