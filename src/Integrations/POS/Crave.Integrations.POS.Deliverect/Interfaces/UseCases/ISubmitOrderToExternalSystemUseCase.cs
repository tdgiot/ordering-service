﻿using Crave.Integrations.POS.Deliverect.Domain.Requests;
using Crave.Ordering.Shared.Domain;
using Crave.Ordering.Shared.Interfaces;

namespace Crave.Integrations.POS.Deliverect.Interfaces.UseCases
{
    public interface ISubmitOrderToExternalSystemUseCase : IUseCaseAsync<SubmitOrderToExternalSystemRequest, Response>
    {

    }
}
