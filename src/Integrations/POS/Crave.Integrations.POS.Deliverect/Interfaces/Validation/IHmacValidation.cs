﻿using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Integrations.POS.Deliverect.Interfaces.Validation
{
	public interface IHmacValidation
	{
		bool ValidateRequest(IHmacValidationContent validationContent, ExternalSystemEntity externalSystem);
	}
}
