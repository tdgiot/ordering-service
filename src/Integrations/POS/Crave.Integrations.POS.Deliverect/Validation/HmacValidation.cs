﻿using Crave.Integrations.POS.Deliverect.Domain.Requests;
using Crave.Integrations.POS.Deliverect.Domain.UseCases;
using Crave.Integrations.POS.Deliverect.Interfaces;
using Crave.Integrations.POS.Deliverect.Interfaces.Factories;
using Crave.Integrations.POS.Deliverect.Interfaces.Validation;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Integrations.POS.Deliverect.Validation
{
	public class HmacValidation : IHmacValidation
	{
		private readonly IValidateHmacRequestFactory validateHmacRequestFactory;
		private readonly IValidateHmacUseCase validateHmacUseCase;

		public HmacValidation(IValidateHmacRequestFactory validateHmacRequestFactory, IValidateHmacUseCase validateHmacUseCase)
		{
			this.validateHmacRequestFactory = validateHmacRequestFactory;
			this.validateHmacUseCase = validateHmacUseCase;
		}

		public bool ValidateRequest(IHmacValidationContent validationContent, ExternalSystemEntity externalSystem)
		{
			ValidateHmacRequest validateHmacRequest = validateHmacRequestFactory.Create(validationContent, externalSystem);
			return validateHmacUseCase.Execute(validateHmacRequest);
		}
	}
}
