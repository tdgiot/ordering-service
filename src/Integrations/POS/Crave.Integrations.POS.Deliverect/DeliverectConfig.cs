﻿using System;

namespace Crave.Integrations.POS.Deliverect
{
    public class DeliverectConfig
    {
        public DeliverectConfig(string clientId, string clientSecret)
        {
            ClientId = clientId;
            ClientSecret = clientSecret;
            Audience = "https://api.deliverect.com";
            BaseUri = new Uri("https://api.staging.deliverect.com");
        }

        public string ClientId { get; }
        public string ClientSecret { get; }
        public string Audience { get; set; }
        public Uri BaseUri { get; set; } 
    }
}
