﻿// This file is used by Code Analysis to maintain SuppressMessage
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given
// a specific target and scoped to a namespace, type, member, etc.

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Major Code Smell", "S3996:URI properties should not be strings", Justification = "Cannot trust Uri format on responses from an external source.", 
    Scope = "namespaceanddescendants", Target = "~N:Crave.Integrations.POS.Deliverect.Domain.Responses")]
[assembly: SuppressMessage("Major Code Smell", "S3995:URI return values should not be strings", Justification = "Cannot trust Uri format on responses from an external source.", 
    Scope = "member", Target = "~M:Crave.Integrations.POS.Deliverect.WebHooks.ChannelStatusWebHook.CreateEndpointUrl(System.String)~System.String")]
[assembly: SuppressMessage("Major Code Smell", "S3956:\"Generic.List\" instances should not be part of public APIs", Justification = "Type must be serialized and so concrete type should be used",
    Scope = "namespaceanddescendants", Target = "~N:Crave.Integrations.POS.Deliverect.Domain.Models")]
[assembly: SuppressMessage("Major Code Smell", "S3956:\"Generic.List\" instances should not be part of public APIs", Justification = "Type must be serialized and so concrete type should be used",
    Scope = "namespaceanddescendants", Target = "~N:Crave.Integrations.POS.Deliverect.Domain.Responses")]
