﻿using System.Threading.Tasks;
using Crave.Integrations.POS.Deliverect.Interfaces.Repositories;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.SQLServer;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Crave.Integrations.POS.Deliverect.Data.Repositories
{
    public class ProductRepository : IProductRepository
    {
        public async Task<EntityCollection<ProductEntity>> GetProductsLinkedToExternalProductsAsync(int externalSystemId)
        {
            RelationPredicateBucket filterBucket = new RelationPredicateBucket();
            filterBucket.PredicateExpression.Add(ExternalProductFields.ExternalSystemId == externalSystemId);
            filterBucket.Relations.Add(ProductEntity.Relations.ExternalProductEntityUsingExternalProductId);

            return await DataAccessAdapter.FetchEntityCollectionAsync<ProductEntity>(filterBucket, 0, null, null, null);
        }
    }
}
