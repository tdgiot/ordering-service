﻿using System.Threading.Tasks;
using Crave.Enums;
using Crave.Integrations.POS.Deliverect.Interfaces.Repositories;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.FactoryClasses;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.SQLServer;
using SD.LLBLGen.Pro.QuerySpec;
using SD.LLBLGen.Pro.QuerySpec.Adapter;

namespace Crave.Integrations.POS.Deliverect.Data.Repositories
{
    /// <summary>
    /// Repository class for order routestep handlers.
    /// </summary>
    public class OrderRoutestephandlerRepository : IOrderRoutestephandlerRepository
    {
        /// <inheritdoc />
        public async Task<OrderRoutestephandlerEntity> GetByOrderIdAndStatusAsync(int orderId, OrderRoutestephandlerStatus orderRoutestephandlerStatus)
        {
            EntityQuery<OrderRoutestephandlerEntity> entityQuery = new QueryFactory().OrderRoutestephandler
                .Where(OrderRoutestephandlerFields.OrderId == orderId)
                .AndWhere(OrderRoutestephandlerFields.Status == orderRoutestephandlerStatus);

            using DataAccessAdapter adapter = new DataAccessAdapter();
            return await adapter.FetchFirstAsync(entityQuery);
        }

        public async Task<bool> SaveAsync(OrderRoutestephandlerEntity handler)
        {
            return await DataAccessAdapter.SaveAsync(handler, true);
        }
    }
}
