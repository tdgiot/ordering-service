﻿using Crave.Integrations.POS.Deliverect.Interfaces.Repositories;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.SQLServer;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System.Threading.Tasks;

namespace Crave.Integrations.POS.Deliverect.Data.Repositories
{
	/// <summary>
	/// Repository class for orders.
	/// </summary>
	public class OrderRepository : IOrderRepository
	{
		public async Task<OrderEntity> GetOrderAsync(int orderId)
		{
			RelationPredicateBucket filterBucket = new RelationPredicateBucket();
			filterBucket.PredicateExpression.Add(OrderFields.OrderId == orderId);

			IPrefetchPath2 prefetch = new PrefetchPath2(EntityType.OrderEntity);
			prefetch.Add(OrderEntity.PrefetchPathDeliveryInformationEntity);
			prefetch.Add(OrderEntity.PrefetchPathPaymentTransactionCollection);
			IPrefetchPathElement2 orderItemPrefetch = prefetch.Add(OrderEntity.PrefetchPathOrderitemCollection);

			// Order items
			orderItemPrefetch.SubPath.Add(OrderitemEntity.PrefetchPathProductEntity)
				.SubPath.Add(ProductEntity.PrefetchPathExternalProductEntity);

			// Alteration items
			orderItemPrefetch.SubPath.Add(OrderitemEntity.PrefetchPathOrderitemAlterationitemCollection)
				.SubPath.Add(OrderitemAlterationitemEntity.PrefetchPathAlterationitemEntity)
				.SubPath.Add(AlterationitemEntity.PrefetchPathAlterationoptionEntity)
				.SubPath.Add(AlterationoptionEntity.PrefetchPathExternalProductEntity);

			return await DataAccessAdapter.FetchEntityAsync<OrderEntity>(filterBucket, prefetch, null);
		}
	}
}
