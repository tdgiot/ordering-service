﻿using Crave.Integrations.POS.Interfaces.Repositories;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.SQLServer;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System.Threading.Tasks;

namespace Crave.Integrations.POS.Deliverect.Data.Repositories
{
	public class ExternalMenuRepository : IExternalMenuRepository
	{
		public async Task<ExternalMenuEntity> GetByExternalIdAsync(string externalMenuExternalId)
		{
			RelationPredicateBucket filterBucket = new RelationPredicateBucket();
			filterBucket.PredicateExpression.Add(ExternalMenuFields.Id == externalMenuExternalId);

			return await DataAccessAdapter.FetchEntityAsync<ExternalMenuEntity>(filterBucket, null, null);
		}
	}
}
