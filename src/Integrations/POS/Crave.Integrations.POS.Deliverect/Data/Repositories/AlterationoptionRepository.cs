﻿using System.Threading.Tasks;
using Crave.Integrations.POS.Deliverect.Interfaces.Repositories;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.SQLServer;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Crave.Integrations.POS.Deliverect.Data.Repositories
{
    public class AlterationoptionRepository : IAlterationoptionRepository
    {
        public async Task<EntityCollection<AlterationoptionEntity>> GetAlterationoptionsLinkedToExternalProductsAsync(int externalSystemId)
        {
            RelationPredicateBucket filterBucket = new RelationPredicateBucket();
            filterBucket.PredicateExpression.Add(ExternalProductFields.ExternalSystemId == externalSystemId);
            filterBucket.Relations.Add(AlterationoptionEntity.Relations.ExternalProductEntityUsingExternalProductId);

            return await DataAccessAdapter.FetchEntityCollectionAsync<AlterationoptionEntity>(filterBucket, 0, null, null, null);
        }
    }
}
