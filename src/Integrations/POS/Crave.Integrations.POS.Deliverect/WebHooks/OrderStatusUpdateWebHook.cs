﻿using Crave.Enums;
using Crave.Integrations.POS.Deliverect.Domain.Requests;
using Crave.Integrations.POS.Deliverect.Interfaces.Repositories;
using Crave.Integrations.POS.Deliverect.Interfaces.Validation;
using Crave.Integrations.POS.Interfaces;
using Crave.Integrations.POS.Interfaces.Repositories;
using Crave.Ordering.ProcessRoute.Models;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.Extensions;
using Crave.Ordering.Shared.Domain;
using Crave.Ordering.Shared.Gateway.Ordering;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;
using OrderStatus = Crave.Integrations.POS.Deliverect.Domain.Enums.OrderStatus;

namespace Crave.Integrations.POS.Deliverect.WebHooks
{
    public class OrderStatusUpdateWebHook
    {
        private readonly ILogger<OrderStatusUpdateWebHook> logger;
        private readonly IExternalSystemLogger externalSystemLogger;
        private readonly IExternalSystemRepository externalSystemRepository;
        private readonly IOrderRoutestephandlerRepository orderRoutestephandlerRepository;
        private readonly IOrderingGateway orderingGateway;
        private readonly IHmacValidation hmacValidation;

        public OrderStatusUpdateWebHook(ILogger<OrderStatusUpdateWebHook> logger,
                                        IExternalSystemLogger externalSystemLogger,
                                        IExternalSystemRepository externalSystemRepository,
                                        IOrderRoutestephandlerRepository orderRoutestephandlerRepository,
                                        IOrderingGateway orderingGateway,
                                        IHmacValidation hmacValidation)
        {
            this.logger = logger;
            this.externalSystemLogger = externalSystemLogger;
            this.externalSystemRepository = externalSystemRepository;
            this.orderRoutestephandlerRepository = orderRoutestephandlerRepository;
            this.orderingGateway = orderingGateway;
            this.hmacValidation = hmacValidation;
        }

        public async Task<Response> ExecuteAsync(HmacValidatedRequest<OrderStatusRequest> wrappedRequest)
        {
            Response response = await ExecuteInternalAsync(wrappedRequest);

            if (response.Success)
            {
                logger.LogInformation(response.Message);
            }
            else
            {
                logger.LogError(response.Message);
            }

            return response;
        }

        private async Task<Response> ExecuteInternalAsync(HmacValidatedRequest<OrderStatusRequest> wrappedRequest)
        {
            OrderStatusRequest? request = wrappedRequest.OriginalRequest;
            if (request.ChannelOrderId.IsNullOrWhiteSpace() || !int.TryParse(request.ChannelOrderId, out int orderId))
            {
                return Response.AsError($"Invalid channel order id in hmacValidationContent. (OriginalRequest={JsonConvert.SerializeObject(request)})");
            }

            // FO: Commented as it's currently missing from the hmacValidationContent but this is how should work and is being fixed by Deliverect atm
            //if (hmacValidationContent.Location.IsNullOrWhiteSpace())
            //{
            //    return Response.AsError($"Location missing in hmacValidationContent. (OriginalRequest={JsonConvert.SerializeObject(hmacValidationContent)})");
            //}

            //ExternalSystemEntity externalSystem = await externalSystemRepository.GetByLocationIdAsync(hmacValidationContent.Location);
            ExternalSystemEntity externalSystem = await externalSystemRepository.GetByOrderIdAsync(orderId);
            if (externalSystem.IsNullOrNew())
            {
                //return Response.AsError($"No external system found for location. (Location={request.Location})");
                return Response.AsError($"No external system found for order and status. (OrderId={orderId})");
            }

            if (!hmacValidation.ValidateRequest(wrappedRequest, externalSystem))
            {
                return Response.AsError($"Invalid request. External System: {externalSystem.ExternalSystemId}.");
            }

            Response response = await ProcessOrderStatusUpdate(orderId, request.Status);

            await LogToExternalSystemLog(externalSystem, request, response, orderId);

            return response;
        }

        private async Task<Response> ProcessOrderStatusUpdate(int orderId, OrderStatus? orderStatus)
        {
            OrderRoutestephandlerStatus? handlerStatus = ConvertOrderStatusToHandlerStatus(orderStatus);
            if (handlerStatus == null)
            {
                return Response.AsSuccess($"Received status '{orderStatus}' (but ignored)");
            }

            OrderRoutestephandlerEntity handler = await orderRoutestephandlerRepository.GetByOrderIdAndStatusAsync(orderId, OrderRoutestephandlerStatus.RetrievedByHandler);
            if (handler.IsNullOrNew())
            {
                return Response.AsError($"Received status '{orderStatus}' but no handler is awaiting a status update");
            }

            handler.Status = handlerStatus.Value;
            handler.StatusText = handlerStatus.Value.ToString();

            await orderRoutestephandlerRepository.SaveAsync(handler);
            await orderingGateway.FireRoutestepStatusChangedEventAsync(new RoutestepStatusChangedEvent { OrderRoutestephandlerId = handler.OrderRoutestephandlerId });

            return Response.AsSuccess($"Received status '{orderStatus}'");
        }

        private static OrderRoutestephandlerStatus? ConvertOrderStatusToHandlerStatus(OrderStatus? orderStatus) => orderStatus switch
        {
            OrderStatus.AutoFinalized => OrderRoutestephandlerStatus.Completed,
            OrderStatus.Received => OrderRoutestephandlerStatus.Completed,
            OrderStatus.New => OrderRoutestephandlerStatus.Completed,
            OrderStatus.Accepted => OrderRoutestephandlerStatus.Completed,
            OrderStatus.Failed => OrderRoutestephandlerStatus.Failed,
            _ => null
        };

        private async Task LogToExternalSystemLog(ExternalSystemEntity externalSystem, OrderStatusRequest request, Response response, int orderId) => await externalSystemLogger.LogAsync(log =>
        {
            log.LogType = ExternalSystemLogType.OrderStatusUpdate;
            log.ExternalSystemId = externalSystem.ExternalSystemId;
            log.Message = response.Message;
            log.Success = response.Success;
            log.OrderId = orderId;
            log.Raw = JsonConvert.SerializeObject(request);
        });
    }
}
