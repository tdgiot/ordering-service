﻿using Crave.Enums;
using Crave.Integrations.POS.Deliverect.Domain.Constants;
using Crave.Integrations.POS.Deliverect.Domain.Models;
using Crave.Integrations.POS.Deliverect.Domain.Requests;
using Crave.Integrations.POS.Deliverect.Domain.Responses;
using Crave.Integrations.POS.Deliverect.Interfaces.Validation;
using Crave.Integrations.POS.Domain;
using Crave.Integrations.POS.Domain.UseCases.Requests;
using Crave.Integrations.POS.Interfaces;
using Crave.Integrations.POS.Interfaces.Repositories;
using Crave.Integrations.POS.Interfaces.UseCases;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.Extensions;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Domain;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Crave.Integrations.POS.Deliverect.WebHooks
{
    public class SnoozeUnsnoozeWebHook
    {
        private readonly IExternalSystemLogger externalSystemLogger;
        private readonly IExternalSystemRepository externalSystemRepository;
        private readonly IExternalProductRepository externalProductRepository;
        private readonly IUpdateItemsLinkedToExternalProductsUseCase updateItemsLinkedToExternalProductsUseCase;
        private readonly IHmacValidation hmacValidation;

        public SnoozeUnsnoozeWebHook(IExternalSystemLogger externalSystemLogger,
            IExternalSystemRepository externalSystemRepository,
            IExternalProductRepository externalProductRepository,
            IUpdateItemsLinkedToExternalProductsUseCase updateItemsLinkedToExternalProductsUseCase,
            IHmacValidation hmacValidation)
        {
            this.externalSystemLogger = externalSystemLogger;
            this.externalSystemRepository = externalSystemRepository;
            this.externalProductRepository = externalProductRepository;
            this.updateItemsLinkedToExternalProductsUseCase = updateItemsLinkedToExternalProductsUseCase;
            this.hmacValidation = hmacValidation;
        }

        public async Task<Response<SnoozeUnsnoozeResponse>> ExecuteAsync(HmacValidatedRequest<SnoozeUnsnoozeRequest> wrappedRequest)
        {
            SnoozeUnsnoozeRequest request = wrappedRequest.OriginalRequest;

            ExternalSystemEntity externalSystem = await externalSystemRepository.GetByChannelLinkIdAsync(request.ChannelLinkId);
            if (externalSystem.IsNullOrNew())
            {
                throw new InvalidOperationException($"No external system found for channel link id. (ChannelLinkId={request.ChannelLinkId})");
            }

            if (!hmacValidation.ValidateRequest(wrappedRequest, externalSystem))
            {
                return Response<SnoozeUnsnoozeResponse>.AsError("Invalid request.");
            }

            ExternalSystemTracing tracing = new ExternalSystemTracing();

            EntityCollection<ExternalProductEntity> externalProducts = await externalProductRepository.GetExternalProductsAsync(externalSystem.ExternalSystemId);
            UpdateSnoozeStateOnAffectedExternalProducts(externalProducts, request.Operations, tracing);
            await externalProductRepository.SaveAsync(externalProducts);

            UpdateItemsLinkedToExternalProductsRequest updateItemsLinkedToExternalProductsRequest = new UpdateItemsLinkedToExternalProductsRequest(externalSystem.ExternalSystemId);
            Response response = await updateItemsLinkedToExternalProductsUseCase.ExecuteAsync(updateItemsLinkedToExternalProductsRequest);

            string message = $"Handled {request.Operations.Count} snooze/unsnooze operations";

            await LogToExternalSystemLog(externalSystem.ExternalSystemId, message, tracing.ToString(), response.Success, request);

            List<string> allSnoozedItems = DetermineAllSnoozedItems(externalProducts);

            SnoozeProductsResult[] results = request.Operations
                                                    .Select(operation => new SnoozeProductsResult
                                                    {
                                                        Action = operation.Action,
                                                        Data = new SnoozeProductsData
                                                        {
                                                            LocationId = request.LocationId,
                                                            AllSnoozedItems = allSnoozedItems
                                                        }
                                                    }).ToArray();

            return Response<SnoozeUnsnoozeResponse>.AsSuccess(new SnoozeUnsnoozeResponse { Results = results });
        }

        private static void UpdateSnoozeStateOnAffectedExternalProducts(ICollection<ExternalProductEntity> externalProducts, IEnumerable<Operation> operations, ExternalSystemTracing tracing)
        {
            foreach (Operation operation in operations)
            {
                string[] productIds = operation.Data.Items.Select(x => x.Plu).ToArray();

                IList<ExternalProductEntity> affectedProducts = externalProducts
                                                                .Where(product => productIds.Contains(product.Id))
                                                                .ToList();

                UpdateSnoozeStateOnProducts(affectedProducts, operation.Action, tracing);
            }
        }

        private static void UpdateSnoozeStateOnProducts(ICollection<ExternalProductEntity> externalProducts, string action, ExternalSystemTracing tracing)
        {
            if (action != ActionNames.Snooze && action != ActionNames.Unsnooze)
            {
                tracing.Append($"Unable to handle operation action (OperationAction={action})");
                return;
            }

            tracing.Append($"{action} {externalProducts.Count} product(s)");

            bool isSnoozed = action.Equals(ActionNames.Snooze, StringComparison.InvariantCultureIgnoreCase);

            foreach (ExternalProductEntity externalProduct in externalProducts)
            {
                externalProduct.IsSnoozed = isSnoozed;

                tracing.Append("{0}({1}) -> {2}", externalProduct.Name, externalProduct.Id, action);
            }
        }

        private static List<string> DetermineAllSnoozedItems(IEnumerable<ExternalProductEntity> products) => products
            .Where(product => product.IsSnoozed)
            .Select(x => x.Id)
            .ToList();

        private async Task LogToExternalSystemLog(int externalSystemId, string message, string trace, bool success, SnoozeUnsnoozeRequest request) => await externalSystemLogger.LogAsync(log =>
        {
            log.LogType = ExternalSystemLogType.SnoozeProductsUpdate;
            log.ExternalSystemId = externalSystemId;
            log.Success = success;
            log.Message = message;
            log.Log = trace;
            log.Raw = JsonConvert.SerializeObject(request);
        });
    }
}
