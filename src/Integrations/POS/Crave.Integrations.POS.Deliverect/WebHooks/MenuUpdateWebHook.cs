﻿using Crave.Enums;
using Crave.Integrations.POS.Deliverect.Domain.Converters;
using Crave.Integrations.POS.Deliverect.Domain.Models;
using Crave.Integrations.POS.Deliverect.Domain.Requests;
using Crave.Integrations.POS.Deliverect.Interfaces.Validation;
using Crave.Integrations.POS.Domain;
using Crave.Integrations.POS.Domain.UseCases.Requests;
using Crave.Integrations.POS.Interfaces;
using Crave.Integrations.POS.Interfaces.Repositories;
using Crave.Integrations.POS.Interfaces.UseCases;
using Crave.Integrations.POS.Models;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.Extensions;
using Crave.Ordering.Shared.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace Crave.Integrations.POS.Deliverect.WebHooks
{
    public class MenuUpdateWebHook
    {
        private readonly IExternalSystemLogger externalSystemLogger;
        private readonly IExternalSystemRepository externalSystemRepository;
        private readonly IUpdateExternalProductsUseCase updateExternalProductsUseCase;
        private readonly IUpdateItemsLinkedToExternalProductsUseCase updateItemsLinkedToExternalProductsUseCase;
        private readonly IHmacValidation hmacValidation;

        public MenuUpdateWebHook(IExternalSystemLogger externalSystemLogger,
                                 IExternalSystemRepository externalSystemRepository,
                                 IUpdateExternalProductsUseCase updateExternalProductsUseCase,
                                 IUpdateItemsLinkedToExternalProductsUseCase updateItemsLinkedToExternalProductsUseCase,
                                 IHmacValidation hmacValidation)
        {
            this.externalSystemLogger = externalSystemLogger;
            this.externalSystemRepository = externalSystemRepository;
            this.updateExternalProductsUseCase = updateExternalProductsUseCase;
            this.updateItemsLinkedToExternalProductsUseCase = updateItemsLinkedToExternalProductsUseCase;
            this.hmacValidation = hmacValidation;
        }


        public async Task<Response> ExecuteAsync(HmacValidatedRequest<Menu[]> wrappedRequest)
        {
            Menu[] menus = wrappedRequest.OriginalRequest;
            bool requestValidated = false;
            List<Response> responses = new List<Response>();

            foreach (Menu menu in menus)
            {
                ExternalSystemTracing tracing = new ExternalSystemTracing();

                ExternalSystemEntity externalSystem = await DetermineExternalSystem(menu);

                if (!requestValidated)
                {
                    if (!hmacValidation.ValidateRequest(wrappedRequest, externalSystem))
                    {
                        string message = $"Invalid request. Hmac invalid, External System: {externalSystem.ExternalSystemId}.";
                        await LogToExternalSystemLog(wrappedRequest.RequestBody, externalSystem, Response.AsError(message), tracing);
                        return Response.AsError(message);
                    }

                    requestValidated = true;
                }

                Response response = await SynchronizeMenu(externalSystem, menu, tracing);

                await LogToExternalSystemLog(wrappedRequest.RequestBody, externalSystem, response, tracing);
                responses.Add(response);
            }
            return Response.Combine(responses);
        }

        private async Task<ExternalSystemEntity> DetermineExternalSystem(Menu menu)
        {
            if (menu.ChannelLinkId.IsNullOrWhiteSpace())
            {
                throw new InvalidOperationException("No channel link id found.");
            }

            ExternalSystemEntity externalSystem = await externalSystemRepository.GetByChannelLinkIdAsync(menu.ChannelLinkId);
            if (externalSystem.IsNullOrNew())
            {
                throw new InvalidOperationException($"Could not find external system for channel link id (ChannelLinkId={menu.ChannelLinkId}).");
            }

            return externalSystem;
        }

        private async Task<Response> SynchronizeMenu(ExternalSystemEntity externalSystem, Menu menu, ExternalSystemTracing tracing)
        {
            try
            {
                DeliverectExternalProductConverter converter = new DeliverectExternalProductConverter();
                ExternalProduct[] externalProducts = converter.Convert(menu);
                string menuId = menu.Categories.FirstOrDefault()?.Menu ?? throw new InvalidOperationException("No menu id found");

                UpdateExternalProductsRequest updateExternalProductsRequest = new UpdateExternalProductsRequest(externalSystem.ExternalSystemId, externalProducts, tracing, menuId, menu.Name);
                Response response = await updateExternalProductsUseCase.ExecuteAsync(updateExternalProductsRequest);

                if (!response.Success)
                {
                    return Response.AsError(response.Message);
                }


                UpdateItemsLinkedToExternalProductsRequest updateItemsLinkedToExternalProductsRequest = new UpdateItemsLinkedToExternalProductsRequest(externalSystem.ExternalSystemId);
                response = await updateItemsLinkedToExternalProductsUseCase.ExecuteAsync(updateItemsLinkedToExternalProductsRequest);

                return !response.Success
                    ? Response.AsError(response.Message)
                    : Response.AsSuccess("Successfully synchronized menu");
            }
            catch (Exception ex)
            {
                return Response.AsError($"Exception while synchronizing menu. (Message={ex.Message})");
            }
        }


        private async Task LogToExternalSystemLog(string raw, ExternalSystemEntity externalSystem, Response response, ExternalSystemTracing tracing) => await externalSystemLogger.LogAsync(log =>
        {
            log.LogType = ExternalSystemLogType.MenuUpdate;
            log.ExternalSystemId = externalSystem?.ExternalSystemId ?? 0;
            log.Message = response.Message;
            log.Success = response.Success;
            log.Log = tracing.ToString();
            log.Raw = raw;
        });
    }
}
