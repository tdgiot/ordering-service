﻿using Crave.Enums;
using Crave.Integrations.POS.Deliverect.Domain.Constants;
using Crave.Integrations.POS.Deliverect.Domain.Requests;
using Crave.Integrations.POS.Deliverect.Domain.Responses;
using Crave.Integrations.POS.Deliverect.Interfaces.Validation;
using Crave.Integrations.POS.Interfaces;
using Crave.Integrations.POS.Interfaces.Repositories;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.Extensions;
using Crave.Ordering.Shared.Domain;
using Crave.Ordering.Shared.Gateway.Configuration;
using Crave.Ordering.Shared.Gateway.Constants;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;

namespace Crave.Integrations.POS.Deliverect.WebHooks
{
    public class ChannelStatusWebHook
    {
        private readonly ILogger<ChannelStatusWebHook> logger;
        private readonly IExternalSystemLogger externalSystemLogger;
        private readonly IExternalSystemRepository externalSystemRepository;
        private readonly IHmacValidation hmacValidation;
        private readonly CraveConfiguration craveConfiguration;

        public ChannelStatusWebHook(ILogger<ChannelStatusWebHook> logger,
                                    IExternalSystemLogger externalSystemLogger,
                                    IExternalSystemRepository externalSystemRepository,
                                    IHmacValidation hmacValidation,
                                    CraveConfiguration craveConfiguration)
        {
            this.logger = logger;
            this.externalSystemLogger = externalSystemLogger;
            this.externalSystemRepository = externalSystemRepository;
            this.hmacValidation = hmacValidation;
            this.craveConfiguration = craveConfiguration;
        }

        public async Task<ChannelStatusResponse> ExecuteAsync(HmacValidatedRequest<ChannelStatusRequest> wrappedRequest)
        {
            Response<ChannelStatusResponse> response = await ExecuteInternalAsync(wrappedRequest);

            if (!response.Success)
            {
                logger.LogError(response.Message);
            }

            return response.Model!;
        }

        private async Task<Response<ChannelStatusResponse>> ExecuteInternalAsync(HmacValidatedRequest<ChannelStatusRequest> wrappedRequest)
        {
            ExternalSystemEntity externalSystem = await externalSystemRepository.GetByLocationIdAsync(wrappedRequest.OriginalRequest.ChannelLocationId);
            if (externalSystem.IsNullOrNew())
            {
                return Response<ChannelStatusResponse>.AsError($"No external system found for channel location id. (ChannelLocationId={wrappedRequest.OriginalRequest.ChannelLocationId})");
            }

            if (externalSystem.Environment == ExternalSystemEnvironment.Live && !hmacValidation.ValidateRequest(wrappedRequest, externalSystem))
            {
                return Response<ChannelStatusResponse>.AsError($"Invalid request. External System: {externalSystem.ExternalSystemId}.");
            }

            string logMessage = string.Empty;

            switch (wrappedRequest.OriginalRequest.Status)
            {
                case ChannelStatus.Register:
                    externalSystem.StringValue3 = wrappedRequest.OriginalRequest.ChannelLinkId;
                    logMessage = $"Channel link registered: {wrappedRequest.OriginalRequest.ChannelLinkId}";
                    break;
                case ChannelStatus.Active:
                    externalSystem.BoolValue1 = true;
                    logMessage = "Channel became active";
                    break;
                case ChannelStatus.Inactive:
                    externalSystem.BoolValue1 = false;
                    logMessage = "Channel became inactive";
                    break;
            }

            bool success = await externalSystemRepository.SaveAsync(externalSystem);

            await LogToExternalSystemLog(externalSystem.ExternalSystemId, logMessage, success, wrappedRequest.OriginalRequest);

            return Response<ChannelStatusResponse>.AsSuccess(CreateResponse());
        }

        private ChannelStatusResponse CreateResponse() => new ChannelStatusResponse
        {
            StatusUpdateURL = CreateEndpointUrl(RouteConstants.ORDERSTATUSUPDATE_ENDPOINT),
            MenuUpdateURL = CreateEndpointUrl(RouteConstants.MENUPUSH_ENDPOINT),
            SnoozeUnsnoozeURL = CreateEndpointUrl(RouteConstants.TOGGLESNOOZEPRODUCTS_ENDPOINT),
            BusyModeURL = CreateEndpointUrl(RouteConstants.BUSYMODE_ENDPOINT)
        };

        private string CreateEndpointUrl(string endpoint) => $"{craveConfiguration.Gateway.PublicUrl}{RouteConstants.ORDERING}{endpoint}";

        private async Task LogToExternalSystemLog(int externalSystemId, string message, bool success, ChannelStatusRequest request) => await externalSystemLogger.LogAsync(log =>
        {
            log.LogType = ExternalSystemLogType.ChannelStatusUpdate;
            log.ExternalSystemId = externalSystemId;
            log.Message = message;
            log.Success = success;
            log.Raw = JsonConvert.SerializeObject(request);
        });
    }
}
