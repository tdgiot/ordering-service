﻿using System;
using System.Threading.Tasks;
using Crave.Integrations.POS.Deliverect.Domain.Constants;
using Crave.Integrations.POS.Deliverect.Domain.Requests;
using Crave.Integrations.POS.Deliverect.Domain.Responses;
using Crave.Integrations.POS.Interfaces;
using Crave.Ordering.Shared.Domain;
using RestSharp;
using Microsoft.Extensions.Caching.Memory;
using RestSharp.Serializers.NewtonsoftJson;
using Amazon.XRay.Recorder.Core;

namespace Crave.Integrations.POS.Deliverect
{
    public class DeliverectClient : IPosClient
    {
        private readonly DeliverectConfig deliverectConfig;
        private readonly IMemoryCache memoryCache;

        public DeliverectClient(IMemoryCache memoryCache, DeliverectConfig deliverectConfig)
        {
            this.memoryCache = memoryCache;
            this.deliverectConfig = deliverectConfig;
        }

        public async Task<GetAccountsResponse> GetAccountsAsync()
        {
            DeliverectToken token = await GetTokenAsync();

            IRestClient client = CreateClient()
                .AddDefaultHeader("Authorization", $"Bearer {token.AccessToken}");

            IRestRequest restRequest = new RestRequest("/accounts", DataFormat.Json);

            return await client.GetAsync<GetAccountsResponse>(restRequest);
        }

        public async Task<Response> DispatchOrderAsync(OrderRequest request)
        {
            DeliverectToken token = await GetTokenAsync();

            string channelName = token.ChannelName;
            string channelLinkId = request.ChannelLinkId;
            
            IRestClient client = CreateClient()
                .AddDefaultHeader("Authorization", $"Bearer {token.AccessToken}");

            IRestRequest restRequest = new RestRequest($"/{channelName}/order/{channelLinkId}", Method.POST, DataFormat.Json)
                .AddJsonBody(request);
            
            IRestResponse response = await AWSXRayRecorder.Instance.TraceMethodAsync("Deliverect - DeliverectService",  () =>  client.ExecuteAsync(restRequest)); 
            
            return response.IsSuccessful
                ? Response.AsSuccess() 
                : Response.AsError(response.Content);
        }

        private Task<DeliverectToken> FetchTokenAsync()
        {
            AuthenticateRequest authenticateRequest = new AuthenticateRequest
            {
                ClientId = deliverectConfig.ClientId,
                ClientSecret = deliverectConfig.ClientSecret,
                Audience = deliverectConfig.Audience,
                GrantType = GrandType.ClientCredentials
            };

            IRestRequest restRequest = new RestRequest("/oauth/token", DataFormat.Json)
                .AddJsonBody(authenticateRequest);

            return CreateClient().PostAsync<DeliverectToken>(restRequest);
        }

        private async Task<DeliverectToken> GetTokenAsync()
        {
            if (!memoryCache.TryGetValue(deliverectConfig, out DeliverectToken deliverectToken))
            {
                deliverectToken = await FetchTokenAsync();

                //TODO the configured token might be incorrect, this is is not checked in FetchTokenAsync
                // TimeSpan.FromSeconds(deliverectToken.ExpiresIn) will be translated TimeSpan.FromSeconds(0)
                // instead of an auth result, the next exception will be `AbsoluteExpirationRelativeToNow 
                // the expected behaviour would be an result with httpstatuscode 401 + message  

                MemoryCacheEntryOptions options = new MemoryCacheEntryOptions()
                    .SetAbsoluteExpiration(
                        TimeSpan.FromSeconds(deliverectToken.ExpiresIn));

                memoryCache.Set(deliverectConfig, deliverectToken, options);
            }

            return deliverectToken;
        }

        private IRestClient CreateClient() => new RestClient(deliverectConfig.BaseUri)
            .UseSerializer<JsonNetSerializer>();
    }
}
