﻿using Crave.Integrations.POS.Data.Repositories;
using Crave.Integrations.POS.Deliverect.Data.Repositories;
using Crave.Integrations.POS.Deliverect.Domain.UseCases;
using Crave.Integrations.POS.Deliverect.Factories;
using Crave.Integrations.POS.Deliverect.Interfaces.Factories;
using Crave.Integrations.POS.Deliverect.Interfaces.Repositories;
using Crave.Integrations.POS.Deliverect.Interfaces.UseCases;
using Crave.Integrations.POS.Deliverect.Interfaces.Validation;
using Crave.Integrations.POS.Deliverect.Validation;
using Crave.Integrations.POS.Deliverect.WebHooks;
using Crave.Integrations.POS.Domain.UseCases;
using Crave.Integrations.POS.Extensions;
using Crave.Integrations.POS.Interfaces.Repositories;
using Crave.Integrations.POS.Interfaces.UseCases;
using Microsoft.Extensions.DependencyInjection;

namespace Crave.Integrations.POS.Deliverect.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static void AddDeliverect(this IServiceCollection services)
        {
            services.AddExternalSystemLogger();
            services.AddMemoryCache();
            services.AddRepositories();
            services.AddUseCases();
            services.AddWebHooks();
            services.AddFactories();
            services.AddValidators();
        }

        private static void AddRepositories(this IServiceCollection services)
        {
            services.AddSingleton<IExternalSystemRepository, ExternalSystemRepository>();
            services.AddSingleton<IOrderRoutestephandlerRepository, OrderRoutestephandlerRepository>();
            services.AddSingleton<IExternalProductRepository, ExternalProductRepository>();
            services.AddSingleton<IOrderRepository, OrderRepository>();
            services.AddSingleton<IProductRepository, ProductRepository>();
            services.AddSingleton<IAlterationoptionRepository, AlterationoptionRepository>();
            services.AddSingleton<IExternalMenuRepository, ExternalMenuRepository>();

        }

        private static void AddUseCases(this IServiceCollection services)
        {
            services.AddSingleton<ConvertOrderUseCase>();
            services.AddSingleton<DispatchOrderUseCase>();
            services.AddSingleton<ISubmitOrderToExternalSystemUseCase, SubmitOrderToExternalSystemUseCase>();
            services.AddSingleton<IUpdateExternalProductsUseCase, UpdateExternalProductsUseCase>();
            services.AddSingleton<IUpdateItemsLinkedToExternalProductsUseCase, UpdateItemsLinkedToExternalProductsUseCase>();
            services.AddSingleton<IValidateHmacUseCase,ValidateHmacUseCase>();
            services.AddSingleton<BusyModeUseCase>();
        }

        private static void AddWebHooks(this IServiceCollection services)
        {
            services.AddSingleton<MenuUpdateWebHook>();
            services.AddSingleton<ChannelStatusWebHook>();
            services.AddSingleton<OrderStatusUpdateWebHook>();
            services.AddSingleton<SnoozeUnsnoozeWebHook>();
        }

        private static void AddFactories(this IServiceCollection services)
        {
            services.AddSingleton<IDeliverectClientFactory, DeliverectClientFactory>();
            services.AddSingleton<IValidateHmacRequestFactory, ValidateHmacRequestFactory>();
        }

        private static void AddValidators(this IServiceCollection services)
        {
            services.AddSingleton<IHmacValidation, HmacValidation>();
        }
    }
}
