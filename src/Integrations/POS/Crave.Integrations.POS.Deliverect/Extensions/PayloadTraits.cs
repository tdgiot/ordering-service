using Crave.Integrations.POS.Deliverect.Domain.Models;
using System.Security.Cryptography;
using System.Text;

namespace Crave.Integrations.POS.Deliverect.Extensions
{
	public static class PayloadTraits
	{
		public static bool IsValidHmac(this Payload self, HmacKey key, string hmac)
		{
			string computedHmac = ComputeHmac(self, key);
			return string.Equals(computedHmac, hmac);
		}

		private static string ComputeHmac(Payload content, HmacKey key)
		{
			using HMACSHA256 hmacshA256 = new HMACSHA256(key);
			byte[] data = hmacshA256.ComputeHash(content);

			StringBuilder sBuilder = new StringBuilder();

			foreach (byte currentByte in data)
			{
				sBuilder.Append(currentByte.ToString("x2"));
			}

			return sBuilder.ToString();
		}
	}
}
