﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MessageBird;
using MessageBird.Exceptions;
using MessageBird.Objects;

namespace Crave.Libraries.SMS.Messagebird
{
    /// <summary>
    /// MessageBird SMS provider
    /// </summary>
    public class MessagebirdProvider : ISmsProvider
    {
        private readonly Client client;

        /// <summary>
        /// Initializes a new instance of the <see cref="MessagebirdProvider"/> class.
        /// </summary>
        /// <param name="accessKey">MessageBird access key for communicating with the API</param>
        public MessagebirdProvider(string accessKey)
        {
            this.client = Client.CreateDefault(accessKey);
        }

        /// <inheritdoc />
        public MessageResult SendTextMessage(string originator, string message, IEnumerable<long> recipients)
        {
            MessageResult messageResult;

            try
            {
                Message result = this.client.SendMessage(originator, message, recipients.ToArray());

                messageResult = new MessageResult(
                                                  true,
                                                  result.Id,
                                                  result.Recipients.Items.Select(recipient => new SMS.Recipient
                                                  {
                                                      Number = recipient.Msisdn,
                                                      Sent = recipient.Status.Equals(MessageBird.Objects.Recipient.RecipientStatus.Sent)
                                                  }));
            }
            catch (ErrorException e)
            {
                StringBuilder sb = new StringBuilder();
                if (e.HasErrors)
                {
                    // Either the request fails with error descriptions from the endpoint.
                    foreach (Error error in e.Errors)
                    {
                        sb.AppendLine($"code: {error.Code} description: '{error.Description}' parameter: '{error.Parameter}'");
                    }
                }

                if (e.HasReason)
                {
                    // or fails without error information from the endpoint, in which case the reason contains a 'best effort' description.
                    sb.AppendLine($"Reason: {e.Reason}");
                }

                messageResult = new MessageResult(false, sb.ToString());
            }
            catch (Exception ex)
            {
                messageResult = new MessageResult(false, ex.Message);
            }

            return messageResult;
        }
    }
}
