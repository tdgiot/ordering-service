﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Crave.Libraries.SMS
{
    public class DummySmsProvider : ISmsProvider
    {
        public MessageResult SendTextMessage(string originator, string message, IEnumerable<long> recipients)
        {
            return new MessageResult(
                true, 
                Guid.NewGuid().ToString("N"),
                recipients.Select(phoneNumber => new Recipient
                {
                    Number = phoneNumber,
                    Sent = true
                }));
        }
    }
}