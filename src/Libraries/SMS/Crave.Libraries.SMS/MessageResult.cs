﻿using System.Collections.Generic;

namespace Crave.Libraries.SMS
{
    public class MessageResult
    {
        public MessageResult(bool success,
                             string reference,
                             IEnumerable<Recipient> recipients)
        {
            this.Success = success;
            this.Reference = reference;
            this.Recipients = recipients;
        }

        public MessageResult(bool success, 
                             string errorMessage)
        {
            this.Success = success;
            this.ErrorMessage = errorMessage;
        }

        public bool Success { get; }
        public string ErrorMessage { get; }
        public string Reference { get; }
        public IEnumerable<Recipient> Recipients { get; }
    }

    public class Recipient
    {
        public long Number { get; set; }

        public bool Sent { get; set; }
    }
}