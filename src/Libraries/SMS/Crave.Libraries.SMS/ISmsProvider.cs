﻿using System.Collections.Generic;

namespace Crave.Libraries.SMS
{
    public interface ISmsProvider
    {
        /// <summary>
        /// Send text message using configured sms provider
        /// </summary>
        /// <param name="originator">The sender of the message. This can be a telephone number (including country code) or an alphanumeric string. In case of an alphanumeric string, the maximum length is 11 characters.</param>
        /// <param name="message">The message to be send to each recipient</param>
        /// <param name="recipients">List of recipient phone numbers</param>
        /// <returns>Message result object holding reference id and status for each recipient</returns>
        MessageResult SendTextMessage(string originator, string message, IEnumerable<long> recipients);
    }
}
