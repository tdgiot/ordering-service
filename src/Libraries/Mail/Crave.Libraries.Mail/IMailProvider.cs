﻿using System.Net.Mail;
using System.Threading.Tasks;

namespace Crave.Libraries.Mail
{
    /// <summary>
    /// Generic mail provider interface
    /// </summary>
    public interface IMailProvider
    {
        /// <summary>
        /// Sends the specified message to an SMTP server for delivery.
        /// </summary>
        /// <param name="message">The <see cref="MailMessage"/> that contains the message to send</param>
        /// <returns>True when mail has been send</returns>
        Task<MailResponse> SendAsync(MailMessage message);
    }
}