﻿namespace Crave.Libraries.Mail
{
    public class MailResponse
    {
        public MailResponse(bool success, 
                            string errorMessage)
        {
            Success = success;
            ErrorMessage = errorMessage;
        }

        public bool Success { get; }
        public string ErrorMessage { get; }
    }
}
