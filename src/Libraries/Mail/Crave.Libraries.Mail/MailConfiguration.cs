﻿using System.Diagnostics.CodeAnalysis;

namespace Crave.Libraries.Mail
{
    /// <summary>
    /// Mail configurator
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class MailConfiguration
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MailConfiguration"/> class.
        /// </summary>
        /// <param name="host">The hostname or IP of the SMTP server</param>
        /// <param name="port">The port on which to communicate with the SMTP server.</param>
        public MailConfiguration(string host, int port)
        {
            this.Host = host;
            this.Port = port;
        }

        /// <summary>
        /// Gets the name or IP address of the host used for SMTP transactions.
        /// </summary>
        public string Host { get; private set; }

        /// <summary>
        /// Gets the port used for SMTP transactions.
        /// </summary>
        public int Port { get; private set; }

        /// <summary>
        /// Specify whether the SMTP client uses Secure Sockets Layer (SSL) to encrypt the connection.
        /// </summary>
        public bool EnableSsl { get; set; }

        /// <summary>
        /// Gets or sets a value that specifies the amount of time (in milliseconds) after which a synchronous send call times out.
        /// </summary>
        /// <returns>
        /// An System.Int32 that specifies the time-out value in milliseconds. The default value is 30,000 (30 seconds).
        /// </returns>
        public int Timeout { get; set; } = 30000;

        /// <summary>
        /// Gets or sets the username used to authenticate the sender.
        /// </summary>
        public string Username { get; set; }

        /// <summary>
        /// Gets or sets the password used to authenticate the sender.
        /// </summary>
        public string Password { get; set; }
    }
}