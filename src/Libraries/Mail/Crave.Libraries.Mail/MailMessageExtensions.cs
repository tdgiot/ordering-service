﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Mail;
using System.Text.RegularExpressions;

namespace Crave.Libraries.Mail
{
    /// <summary>
    /// Extension methods for <see cref="MailMessage"/> configuration
    /// </summary>
    public static class MailMessageExtensions
    {
        private const string EMAIL_PATTERN = @"\w+([-+.']\w+)*[.]*@\w+([-.]\w+)*\.\w+([-.]\w+)*";

        private enum AddressType
        {
            Recipient,
            Bcc,
            Cc,
            Sender,
            ReplyTo
        }

        /// <summary>
        /// Adds an recipient to this MailMessage instance
        /// </summary>
        /// <param name="message">The mail message to use</param>
        /// <param name="address">The e-mail address to add</param>
        /// <param name="displayName">Name to display</param>
        /// <returns>MailMessage object allowing method chaining.</returns>
        public static MailMessage AddRecipient(this MailMessage message, string address, string displayName = "")
        {
            return message.AddAddress(address, displayName, AddressType.Recipient);
        }

        /// <summary>
        /// Adds an Bcc recipient to this MailMessage instance
        /// </summary>
        /// <param name="message">The mail message to use</param>
        /// <param name="address">The e-mail address to add</param>
        /// <param name="displayName">Name to display</param>
        /// <returns>MailMessage object allowing method chaining.</returns>
        public static MailMessage AddRecipientBcc(this MailMessage message, string address, string displayName = "")
        {
            return message.AddAddress(address, displayName, AddressType.Bcc);
        }

        /// <summary>
        /// Adds an Cc recipient to this MailMessage instance
        /// </summary>
        /// <param name="message">The mail message to use</param>
        /// <param name="address">The e-mail address to add</param>
        /// <param name="displayName">Name to display</param>
        /// <returns>MailMessage object allowing method chaining.</returns>
        public static MailMessage AddRecipientCc(this MailMessage message, string address, string displayName = "")
        {
            return message.AddAddress(address, displayName, AddressType.Cc);
        }

        /// <summary>
        /// Add multiple recipients to this MailMessage instance
        /// </summary>
        /// <param name="message">The mail message to use</param>
        /// <param name="addresses">The list of e-mail addresses to add</param>
        /// <returns>MailMessage object allowing method chaining.</returns>
        public static MailMessage AddRecipients(this MailMessage message, IEnumerable<string> addresses)
        {
            foreach (string address in addresses)
            {
                message.AddRecipient(address);
            }

            return message;
        }

        /// <summary>
        /// Add multiple Bcc recipients to this MailMessage instance
        /// </summary>
        /// <param name="message">The mail message to use</param>
        /// <param name="addresses">The list of e-mail addresses to add</param>
        /// <returns>MailMessage object allowing method chaining.</returns>
        public static MailMessage AddRecipientsBcc(this MailMessage message, IEnumerable<string> addresses)
        {
            foreach (string address in addresses)
            {
                message.AddRecipientBcc(address);
            }

            return message;
        }

        /// <summary>
        /// Add multiple Cc recipients to this MailMessage instance
        /// </summary>
        /// <param name="message">The mail message to use</param>
        /// <param name="addresses">The list of e-mail addresses to add</param>
        /// <returns>MailMessage object allowing method chaining.</returns>
        public static MailMessage AddRecipientsCc(this MailMessage message, IEnumerable<string> addresses)
        {
            foreach (string address in addresses)
            {
                message.AddRecipientCc(address);
            }

            return message;
        }

        /// <summary>
        /// Add reply to address to this MailMessage instance
        /// </summary>
        /// <param name="message">The mail message to use</param>
        /// <param name="address">The e-mail address to add</param>
        /// <param name="displayName">Name to display</param>
        /// <returns>MailMessage object allow method chaning</returns>
        public static MailMessage AddReplyTo(this MailMessage message, string address, string displayName = "")
        {
            return message.AddAddress(address, displayName, AddressType.ReplyTo);
        }

        /// <summary>
        /// Set message body
        /// </summary>
        /// <param name="message">The mail message to use</param>
        /// <param name="text">The e-mail body</param>
        /// <param name="isHtml">Sets whether the body text contains html tags</param>
        /// <returns>Configuration object allowing method chaining.</returns>
        public static MailMessage SetMessage(this MailMessage message, string text, bool isHtml = false)
        {
            text.ThrowIfArgumentNullOrWhiteSpace();

            message.IsBodyHtml = isHtml;
            message.Body = text;

            return message;
        }

        /// <summary>
        /// Sets the sender to this MailMessage instance
        /// </summary>
        /// <param name="message">The mail message to use</param>
        /// <param name="address">The e-mail address to add</param>
        /// <param name="displayName">Name to display</param>
        /// <returns>MailMessage object allowing method chaining.</returns>
        public static MailMessage SetSender(this MailMessage message, string address, string displayName = "")
        {
            return message.AddAddress(address, displayName, AddressType.Sender);
        }

        /// <summary>
        /// Set the mail subject
        /// </summary>
        /// <param name="message">The mail message to use</param>
        /// <param name="subject">The subject of the mail</param>
        /// <returns>MailMessage object allowing method chaining.</returns>
        public static MailMessage SetSubject(this MailMessage message, string subject)
        {
            subject.ThrowIfArgumentNullOrWhiteSpace();

            message.Subject = subject;
            return message;
        }

        /// <summary>
        /// Add attachment to mail
        /// </summary>
        /// <param name="message">The mail message</param>
        /// <param name="stream">The file stream to attach</param>
        /// <param name="attachmentName">The name of the attachment</param>
        /// <returns>MailMessage object allowing method chaining.</returns>
        public static MailMessage AddAttachment(this MailMessage message, Stream stream, string attachmentName)
        {
            stream.ThrowIfArgumentNull();

            message.Attachments.Add(new Attachment(stream, attachmentName));
            return message;
        }

        private static MailMessage AddAddress(this MailMessage message, string address, string displayName, AddressType addressType)
        {
            address.ThrowIfArgumentNullOrWhiteSpace();

            if (displayName.IsNullOrWhiteSpace())
            {
                displayName = address;
            }

            Regex emailExpression = new Regex(MailMessageExtensions.EMAIL_PATTERN);
            if (emailExpression.IsMatch(address))
            {
                MailAddress mailAddress = new MailAddress(address, displayName);

                switch (addressType)
                {
                    case AddressType.Recipient:
                        message.To.Add(mailAddress);
                        break;
                    case AddressType.Bcc:
                        message.Bcc.Add(mailAddress);
                        break;
                    case AddressType.Cc:
                        message.CC.Add(mailAddress);
                        break;
                    case AddressType.Sender:
                        message.From = mailAddress;
                        message.Sender = mailAddress;
                        break;
                    case AddressType.ReplyTo:
                        message.ReplyToList.Add(mailAddress);
                        break;
                    default:
                        throw new ArgumentOutOfRangeException(nameof(addressType), addressType, null);
                }
            }

            return message;
        }
    }
}