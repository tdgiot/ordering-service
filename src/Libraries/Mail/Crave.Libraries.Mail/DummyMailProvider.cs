﻿using System.Net.Mail;
using System.Threading.Tasks;

namespace Crave.Libraries.Mail
{
    /// <summary>
    /// Dummy mail provider for when no mailer is configured
    /// </summary>
    public class DummyMailProvider : IMailProvider
    {
        /// <summary>
        /// sdfjsdf
        /// </summary>
        /// <param name="message">dsfgdsfg</param>
        /// <returns>dgsdfgdsf</returns>
        public Task<MailResponse> SendAsync(MailMessage message)
        {
            return new Task<MailResponse>(() => new MailResponse(true, string.Empty));
        }
    }
}