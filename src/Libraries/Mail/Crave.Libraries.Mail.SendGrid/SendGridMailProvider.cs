﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using SendGrid;
using SendGrid.Helpers.Mail;

namespace Crave.Libraries.Mail.SendGrid
{
    public class SendGridMailProvider : IMailProvider
    {
        private readonly ISendGridClient client;

        public SendGridMailProvider(ISendGridClient client)
        {
            this.client = client;
        }

        public async Task<MailResponse> SendAsync(MailMessage message)
        {
            SendGridMessage sendGridMessage = await new SendGridMessage()
                                                    .AddSender(message.Sender)
                                                    .AddRecipients(message.To)
                                                    .AddRecipientsBcc(message.Bcc)
                                                    .AddSubject(message.Subject)
                                                    .AddBody(message.IsBodyHtml, message.Body)
                                                    .AddAttachments(message.Attachments);

            Response response = await client.SendEmailAsync(sendGridMessage);

            if (response.StatusCode != HttpStatusCode.Accepted)
            {
                string errorMessage = await SendGridMailProvider.FormatErrorMessageAsync(sendGridMessage, response);

                return new MailResponse(false, errorMessage);
            }

            return new MailResponse(true, string.Empty);
        }

        private static async Task<string> FormatErrorMessageAsync(SendGridMessage sendGridMessage, Response response)
        {
            string recipients = SendGridMailProvider.FormatRecipients(sendGridMessage.Personalizations);
            string responseBody = await response.Body.ReadAsStringAsync();

            return $"Failed to send mail through {nameof(SendGridMailProvider)}. (From={sendGridMessage.From.Email}, To={recipients}, Subject={sendGridMessage.Subject}, ResponseStatusCode={response.StatusCode}, ResponseBody={responseBody})";
        }

        private static string FormatRecipients(IReadOnlyCollection<Personalization> personalizations)
        {
            if (personalizations == null || !personalizations.Any(personalization => personalization.Tos != null &&
                                                                                     personalization.Tos.Any()))
            {
                return "No recipients";
            }

            IEnumerable<string> emailAddresses = personalizations.SelectMany(personalization => personalization.Tos
                                                                                                               .Select(to => to.Email));
            return string.Join(",", emailAddresses);
        }
    }
}
