﻿using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using SendGrid.Helpers.Mail;

namespace Crave.Libraries.Mail.SendGrid
{
    public static class SendGridMessageExtensions
    {
        public static SendGridMessage AddSender(this SendGridMessage sendGridMessage, MailAddress sender)
        {
            if (sender != null)
            {
                sendGridMessage.From = new EmailAddress(sender.Address, sender.DisplayName);
            }
            return sendGridMessage;
        }

        public static SendGridMessage AddSubject(this SendGridMessage sendGridMessage, string subject)
        {
            sendGridMessage.Subject = subject;
            return sendGridMessage;
        }

        public static SendGridMessage AddRecipients(this SendGridMessage sendGridMessage, MailAddressCollection recipients)
        {
            if (recipients.Any())
            {
                sendGridMessage.AddTos(recipients
                                       .Select(mailAddress => new EmailAddress(mailAddress.Address))
                                       .ToList());
            }
            return sendGridMessage;
        }

        public static SendGridMessage AddRecipientsBcc(this SendGridMessage sendGridMessage, MailAddressCollection recipientsBcc)
        {
            if (recipientsBcc.Any())
            {
                sendGridMessage.AddBccs(recipientsBcc
                                        .Select(mailAddress => new EmailAddress(mailAddress.Address))
                                        .ToList());
            }
            return sendGridMessage;
        }

        public static SendGridMessage AddBody(this SendGridMessage sendGridMessage, bool isBodyHtml, string body)
        {
            if (isBodyHtml)
            {
                sendGridMessage.HtmlContent = body;
            }
            else
            {
                sendGridMessage.PlainTextContent = body;
            }
            return sendGridMessage;
        }

        public static async Task<SendGridMessage> AddAttachments(this SendGridMessage sendGridMessage, AttachmentCollection attachments)
        {
            foreach (System.Net.Mail.Attachment attachment in attachments)
            {
                await sendGridMessage.AddAttachmentAsync(attachment.Name, attachment.ContentStream);
            }
            return sendGridMessage;
        }
    }
}
