﻿using System.Collections.Generic;
using System.Linq;
using Crave.Libraries.Distance.Google.Enums;

namespace Crave.Libraries.Distance.Google.Requests
{
    public class DistanceMatrixRequest
    {
        public IEnumerable<string> Origins { get; set; }
        public IEnumerable<string> Destinations { get; set; }
        public UnitSystem? UnitSystem { get; set; }
        public TravelMode? TravelMode { get; set; }

        public override string ToString()
        {
            Dictionary<string, string> parameters = new Dictionary<string, string>();

            if (this.Origins != null)
            {
                parameters.Add("origins", string.Join("|", this.Origins.Select(d => d.ToString())));
            }

            if (this.Destinations != null)
            {
                parameters.Add("destinations", string.Join("|", Destinations.Select(d => d.ToString())));
            }

            if (this.UnitSystem != null)
            {
                parameters.Add("units", this.UnitSystem.ToString());
            }

            if (this.TravelMode != null)
            {
                parameters.Add("mode", this.TravelMode.ToString());
            }

            return string.Join("&", parameters.Select(d => d.Key + "=" + d.Value));
        }
    }
}
