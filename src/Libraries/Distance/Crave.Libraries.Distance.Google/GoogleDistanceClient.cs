﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Crave.Libraries.Distance.Google.Enums;
using Crave.Libraries.Distance.Google.Requests;
using Crave.Libraries.Distance.Google.Responses;
using Crave.Libraries.Distance.Google.Responses.Converters;
using Crave.Libraries.Distance.Interfaces;
using Crave.Libraries.Distance.Responses;
using Newtonsoft.Json;

namespace Crave.Libraries.Distance.Google
{
    public class GoogleDistanceClient : IDistanceClient
    {
        private const string GOOGLE_MATRIX_ENDPOINT = "https://maps.googleapis.com/maps/api/distancematrix/json?{0}&key={1}";

        private readonly string apiKey;
        private readonly HttpClient httpClient;

        public GoogleDistanceClient(GoogleDistanceClientConfiguration googleDistanceClientConfiguration,
                                    IHttpClientFactory httpClientFactory)
        {
            googleDistanceClientConfiguration.ThrowIfArgumentNull();
            httpClientFactory.ThrowIfArgumentNull();

            this.apiKey = googleDistanceClientConfiguration.ApiKey;
            this.httpClient = httpClientFactory.CreateClient();
            this.httpClient.Timeout = TimeSpan.FromSeconds(googleDistanceClientConfiguration.TimeoutInSeconds);
        }

        public async Task<GetDistanceResponse> GetDistanceAsync(ILocation originLocation, ILocation destinationLocation)
        {
            DistanceMatrixRequest request = new DistanceMatrixRequest
            {
                Origins = new[] { originLocation.ToSearchString() },
                Destinations = new[] { destinationLocation.ToSearchString() },
                UnitSystem = UnitSystem.metric,
                TravelMode = TravelMode.driving
            };

            DistanceMatrixResponse response = await this.Execute(request.ToString());

            return DistanceMatrixResponseConverter.Convert(response);
        }

        private async Task<DistanceMatrixResponse> Execute(string queryParameters)
        {
            Uri url = new Uri(GOOGLE_MATRIX_ENDPOINT.FormatSafe(queryParameters, this.apiKey));

            HttpResponseMessage response = await this.httpClient.GetAsync(url);

            return await ProcessHttpResponseMessage(response);
        }

        private static async Task<DistanceMatrixResponse> ProcessHttpResponseMessage(HttpResponseMessage response)
        {
            string resultContent = await response.Content.ReadAsStringAsync();

            if (!response.IsSuccessStatusCode)
            {
                throw new HttpRequestException($"Http exception occured with status code: {(int)response.StatusCode}.");
            }

            return JsonConvert.DeserializeObject<DistanceMatrixResponse>(resultContent);
        }
    }
}
