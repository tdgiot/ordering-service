﻿using Newtonsoft.Json;

namespace Crave.Libraries.Distance.Google.Models
{
    public class TextValuePair
    {
        [JsonProperty("text")]
        public string Text { get; set; }

        [JsonProperty("value")]
        public int Value { get; set; }
    }
}
