﻿using Crave.Libraries.Distance.Google.Enums;
using Newtonsoft.Json;

namespace Crave.Libraries.Distance.Google.Models
{
    public class MatrixElement
    {
        [JsonProperty("status")]
        public ElementLevelStatus Status { get; set; }
        [JsonProperty("duration")]
        public Duration Duration { get; set; }
        [JsonProperty("distance")]
        public Distance Distance { get; set; }
    }
}
