﻿using System;
using System.Linq;
using Crave.Libraries.Distance.Enums;
using Crave.Libraries.Distance.Google.Enums;
using Crave.Libraries.Distance.Google.Models;
using Crave.Libraries.Distance.Responses;

namespace Crave.Libraries.Distance.Google.Responses.Converters
{
    public static class DistanceMatrixResponseConverter
    {
        public static GetDistanceResponse Convert(DistanceMatrixResponse distanceMatrixResponse)
        {
            if (distanceMatrixResponse.Status != MatrixStatus.OK)
            {
                return new GetDistanceResponse(ConvertMatrixStatusToDistanceResult(distanceMatrixResponse.Status), distanceMatrixResponse.ErrorMessage);
            }
            
            MatrixRow row = distanceMatrixResponse.Rows.FirstOrDefault();
            if (row == null)
            {
                return new GetDistanceResponse(DistanceResult.ZERO_RESULTS, "Empty result, no matrix rows");
            }
            
            MatrixElement element = row.Elements.FirstOrDefault();
            if (element == null)
            {
                return new GetDistanceResponse(DistanceResult.ZERO_RESULTS, "Empty result, no matrix element");
            }

            if (element.Status != ElementLevelStatus.OK)
            {
                return new GetDistanceResponse(ConvertElementLevelStatusToDistanceResult(element.Status), distanceMatrixResponse.ErrorMessage);
            }
            
            return new GetDistanceResponse(DistanceResult.OK, new Libraries.Distance.Models.Distance(element.Distance.Value));
        }

        private static DistanceResult ConvertMatrixStatusToDistanceResult(MatrixStatus matrixStatus)
        {
            switch (matrixStatus)
            {
                case MatrixStatus.MAX_ELEMENTS_EXCEEDED:
                    return DistanceResult.MAX_ELEMENTS_EXCEEDED;
                case MatrixStatus.INVALID_REQUEST:
                    return DistanceResult.INVALID_REQUEST;
                case MatrixStatus.OVER_DAILY_LIMIT:
                    return DistanceResult.OVER_DAILY_LIMIT;
                case MatrixStatus.OVER_QUERY_LIMIT:
                    return DistanceResult.OVER_QUERY_LIMIT;
                case MatrixStatus.REQUEST_DENIED:
                    return DistanceResult.REQUEST_DENIED;
                case MatrixStatus.UNKNOWN_ERROR:
                    return DistanceResult.UNKNOWN_ERROR;
                case MatrixStatus.OK:
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(matrixStatus), matrixStatus, null);
            }

            return DistanceResult.OK;
        }

        private static DistanceResult ConvertElementLevelStatusToDistanceResult(ElementLevelStatus elementLevelStatus)
        {
            switch (elementLevelStatus)
            {
                case ElementLevelStatus.NOT_FOUND:
                    return DistanceResult.NOT_FOUND;
                case ElementLevelStatus.ZERO_RESULTS:
                    return DistanceResult.ZERO_RESULTS;
                case ElementLevelStatus.MAX_ROUTE_LENGTH_EXCEEDED:
                    return DistanceResult.MAX_ROUTE_LENGTH_EXCEEDED;
                case ElementLevelStatus.OK:
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(elementLevelStatus), elementLevelStatus, null);
            }

            return DistanceResult.OK;
        }
    }
}
