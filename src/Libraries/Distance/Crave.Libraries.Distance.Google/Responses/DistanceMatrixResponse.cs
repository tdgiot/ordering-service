﻿using System.Collections.Generic;
using Crave.Libraries.Distance.Google.Enums;
using Crave.Libraries.Distance.Google.Models;
using Newtonsoft.Json;

namespace Crave.Libraries.Distance.Google.Responses
{
    public class DistanceMatrixResponse
    {
        [JsonProperty("origin_addresses")]
        public List<string> DestinationAddresses { get; set; }

        [JsonProperty("destination_addresses")]
        public List<string> OriginAddresses { get; set; }

        [JsonProperty("status")]
        public MatrixStatus Status { get; set; }

        [JsonProperty("rows")]
        public List<MatrixRow> Rows { get; set; }

        [JsonProperty("error_message ")]
        public string ErrorMessage { get; set; }
    }
}
