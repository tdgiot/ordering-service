﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Crave.Libraries.Distance.Google.Enums
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum TravelMode
    {
        driving, 
        walking, 
        bicycling, 
        transit
    }
}
