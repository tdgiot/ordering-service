﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Crave.Libraries.Distance.Google.Enums
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum MatrixStatus
    {
        OK,
        MAX_ELEMENTS_EXCEEDED,
        INVALID_REQUEST,
        OVER_DAILY_LIMIT,
        OVER_QUERY_LIMIT,
        REQUEST_DENIED,
        UNKNOWN_ERROR
    }
}
