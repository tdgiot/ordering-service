﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Crave.Libraries.Distance.Google.Enums
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum ElementLevelStatus
    {
        OK,
        NOT_FOUND,
        ZERO_RESULTS,
        MAX_ROUTE_LENGTH_EXCEEDED
    }
}
