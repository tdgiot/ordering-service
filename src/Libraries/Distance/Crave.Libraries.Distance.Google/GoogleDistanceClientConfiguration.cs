﻿using System;

namespace Crave.Libraries.Distance.Google
{
    /// <summary>
    /// Google Distance Client Configuration
    /// </summary>
    public class GoogleDistanceClientConfiguration
    {
        public GoogleDistanceClientConfiguration(string apiKey)
        {
            apiKey.ThrowIfArgumentNullOrWhiteSpace();

            this.ApiKey = apiKey;
        }

        public string ApiKey { get; }
        public int TimeoutInSeconds { get; set; } = 10;
    }
}
