﻿using System.Globalization;
using Crave.Libraries.Distance.Interfaces;

namespace Crave.Libraries.Distance.Models
{
    public class Coordinates : ILocation
    {
        public Coordinates(double longitude, double latitude)
        {
            this.Longitude = longitude;
            this.Latitude = latitude;
        }

        public double Longitude { get; }
        public double Latitude { get; }

        public string ToSearchString()
        {
            return string.Join(",", this.Latitude.ToString(CultureInfo.InvariantCulture), this.Longitude.ToString(CultureInfo.InvariantCulture));
        }
    }
}
