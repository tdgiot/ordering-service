﻿namespace Crave.Libraries.Distance.Models
{
    public class Distance
    {
        public Distance(int metres)
        {
            this.Metres = metres;
        }

        public int Metres { get; }
    }
}
