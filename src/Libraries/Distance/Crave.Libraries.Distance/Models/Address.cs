﻿using Crave.Libraries.Distance.Interfaces;

namespace Crave.Libraries.Distance.Models
{
    public class Address : ILocation
    {
        public Address(string streetAddress, string city, string zipcode, string country)
        {
            this.StreetAddress = streetAddress;
            this.City = city;
            this.Zipcode = zipcode;
            this.Country = country;
        }

        public string StreetAddress { get; }
        public string City { get; }
        public string Zipcode { get; }
        public string Country { get; }

        public string ToSearchString()
        {
            return string.Join(",", this.StreetAddress, this.City, this.Zipcode, this.Country);
        }
    }
}
