﻿using System.Threading.Tasks;
using Crave.Libraries.Distance.Enums;
using Crave.Libraries.Distance.Interfaces;
using Crave.Libraries.Distance.Responses;

namespace Crave.Libraries.Distance
{
    public class EmptyDistanceClient : IDistanceClient
    {
        public async Task<GetDistanceResponse> GetDistanceAsync(ILocation originLocation, ILocation destinationLocation)
        {
            return await Task.FromResult(new GetDistanceResponse(DistanceResult.UNKNOWN_ERROR, "No distance client configured in bootstrapper"));
        }
    }
}
