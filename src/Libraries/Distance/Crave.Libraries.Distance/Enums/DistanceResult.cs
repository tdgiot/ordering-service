﻿namespace Crave.Libraries.Distance.Enums
{
    public enum DistanceResult
    {
        OK,
        MAX_ELEMENTS_EXCEEDED,
        INVALID_REQUEST,
        OVER_DAILY_LIMIT,
        OVER_QUERY_LIMIT,
        REQUEST_DENIED,
        UNKNOWN_ERROR,
        NOT_FOUND,
        ZERO_RESULTS,
        MAX_ROUTE_LENGTH_EXCEEDED
    }
}
