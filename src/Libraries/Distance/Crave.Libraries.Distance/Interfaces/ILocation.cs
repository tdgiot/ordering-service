﻿namespace Crave.Libraries.Distance.Interfaces
{
    public interface ILocation
    {
        string ToSearchString();
    }
}
