﻿using Crave.Libraries.Distance.Enums;

namespace Crave.Libraries.Distance.Responses
{
    public class GetDistanceResponse
    {
        public GetDistanceResponse(DistanceResult result, Models.Distance distance)
        {
            this.Result = result;
            this.Distance = distance;
        }

        public GetDistanceResponse(DistanceResult result, string errorMessage)
        {
            this.Result = result;
            this.ErrorMessage = errorMessage;
        }

        public DistanceResult Result { get; }
        public Models.Distance Distance { get; }
        public string ErrorMessage { get; }
    }
}
