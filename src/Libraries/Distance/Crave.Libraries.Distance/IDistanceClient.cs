﻿using System.Threading.Tasks;
using Crave.Libraries.Distance.Interfaces;
using Crave.Libraries.Distance.Responses;

namespace Crave.Libraries.Distance
{
    public interface IDistanceClient
    {
        Task<GetDistanceResponse> GetDistanceAsync(ILocation originLocation, ILocation destinationLocation);
    }
}
