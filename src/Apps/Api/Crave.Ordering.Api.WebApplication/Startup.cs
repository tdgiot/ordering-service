using System;
using System.Threading.Tasks;
using Amazon.XRay.Recorder.Core;
using AutoMapper;
using Crave.Ordering.Api.Core.Configuration;
using Crave.Ordering.Api.WebApplication.Extensions;
using Crave.Ordering.Shared.Configuration.Database.Configuration;
using Crave.Ordering.Shared.Configuration.Database.Extensions;
using Crave.Ordering.Shared.Configuration.Extensions;
using Crave.Ordering.Shared.Configuration.Sections;
using Crave.Ordering.Shared.Gateway.Configuration;
using Crave.Ordering.Shared.Gateway.Extensions;
using Crave.Ordering.Shared.Messaging.Extensions;
using Crave.Ordering.Shared.OrderNotifications.Extensions;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace Crave.Ordering.Api.WebApplication
{
    public class Startup
    {
        public Startup(IConfiguration configuration, bool useAppInsights = true)
        {
            Configuration = configuration;
            ApplicationInsightsEnabled = useAppInsights;
        }

        public IConfiguration Configuration { get; }

        public bool ApplicationInsightsEnabled { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            AWSXRayRecorder.InitializeInstance(Configuration);

            if (ApplicationInsightsEnabled)
            {
                // Needs to be disabled for tests
                services.AddApplicationInsightsTelemetry(options =>
                {
                    options.EnableAdaptiveSampling = false;
                    options.RequestCollectionOptions.TrackExceptions = true;
                });
            }

            services.AddSwaggerConfig();
            services.AddSingleton(Configuration.ReadAndValidate<AmazonConfiguration>());
            services.AddDatabase(Configuration.ReadAndValidate<DatabaseConfiguration>());
            services.AddDistanceProvider(Configuration.ReadAndValidate<GoogleDistanceConfiguration>());
            services.AddOrderingGateway(Configuration.ReadAndValidate<Shared.Gateway.Configuration.CraveConfiguration>());
            services.AddRepositories();
            services.AddUseCases();
            services.AddValidators();
            services.AddConverters();
            services.AddFactories();
            services.AddClients();
            services.AddControllers(options => options.SuppressAsyncSuffixInActionNames = false);
            services.AddAutoMapper(typeof(Startup));
            services.AddHealthChecks();
            services.AddControllers()
                    .AddJsonOptions(options =>
                    {
                        options.JsonSerializerOptions.IgnoreNullValues = true;
                    });
            services.AddOrderNotifications(Configuration);
            services.AddMessaging(Configuration);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILogger<Startup> logger)
        {
            app.UseXRay("OrderingService", Configuration);

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            app.UseHealthChecks("/health");
            app.UseEndpoints(endpoints => endpoints.Map("/log-test", context =>
            {
                string str = "Hello From Log Endpoint: " + DateTime.Now;
                logger.LogDebug("DEBUG: " + str);
                logger.LogInformation("INFO: " + str);
                return context.Response.WriteAsync(str);
                //return Task.FromResult(new OkResult());
            }));

            if (!env.IsProduction())
            {
                app.UseOpenApi();
                app.UseSwaggerUi3();
            }
        }
    }
}
