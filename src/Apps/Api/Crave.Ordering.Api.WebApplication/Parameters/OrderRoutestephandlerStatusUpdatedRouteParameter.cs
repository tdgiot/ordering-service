﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;

namespace Crave.Ordering.Api.WebApplication.Parameters
{
    public class OrderRoutestephandlerStatusUpdatedRouteParameter
    {
        [Required]
        [BindProperty(Name = "orderroutestephandlerId")]
        [FromRoute]
        public int OrderRoutestephandlerId { get; set; }
    }
}