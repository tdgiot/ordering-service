﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;

namespace Crave.Ordering.Api.WebApplication.Parameters
{
    public class OrderRoutestephandlerRouteParameter
    {
        /// <summary>
        /// The guid of the order to fetch.
        /// </summary>
        [Required]
        [BindProperty(Name = "guid")]
        public string Guid { get; set; }

        /// <summary>
        /// The id of the order routestep handler to update.
        /// </summary>
        [Required]
        [BindProperty(Name = "orderroutestephandlerId")]
        [FromRoute]
        public int OrderRoutestephandlerId { get; set; }
    }
}