﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;

namespace Crave.Ordering.Api.WebApplication.Parameters
{
    public class OrderByGuidRouteParameter
    {
        /// <summary>
        /// The guid of the order to fetch.
        /// </summary>
        [Required]
        [BindProperty(Name = "guid")]
        public string Guid { get; set; }
    }
}
