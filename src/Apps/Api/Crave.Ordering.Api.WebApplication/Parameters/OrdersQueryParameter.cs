﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace Crave.Ordering.Api.WebApplication.Parameters
{
    public class OrdersQueryParameter
    {
        /// <summary>
        /// The terminal id.
        /// </summary>
        [BindRequired]
        [BindProperty(Name = "terminalId")]
        [Range(1, double.PositiveInfinity, ErrorMessage = "The field {0} must be greater than {1}.")]
        public int TerminalId { get; set; }
    }
}
