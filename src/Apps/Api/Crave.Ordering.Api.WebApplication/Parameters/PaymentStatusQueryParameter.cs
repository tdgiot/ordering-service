﻿using System.ComponentModel.DataAnnotations;
using Crave.Enums;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace Crave.Ordering.Api.WebApplication.Parameters
{
    public class PaymentStatusQueryParameter
    {
        [BindRequired]
        [Range(1, double.PositiveInfinity, ErrorMessage = "The field {0} must be greater than {1}.")]
        public int OrderId { get; set; }

        [BindRequired]
        public PaymentTransactionStatus PaymentTransactionStatus { get; set; }
    }
}
