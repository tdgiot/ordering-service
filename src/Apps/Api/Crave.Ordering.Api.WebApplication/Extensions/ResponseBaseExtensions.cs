﻿using System;
using Crave.Ordering.Api.Core.Models.Responses;
using Crave.Ordering.Api.Domain.UseCases.Responses;
using Microsoft.AspNetCore.Mvc;

namespace Crave.Ordering.Api.WebApplication.Extensions
{
    public static class ResponseBaseExtensions
    {
        public static ActionResult ToActionResult(this ResponseBase response)
        {
            if (response.Success)
            {
                return new OkResult();
            }
            
            if (response.NotFound)
            {
                if (response.Message.IsNullOrWhiteSpace())
                {
                    return new NotFoundResult();
                }
                
                return new NotFoundObjectResult(new ProblemDetails { Detail = response.Message });
            }

            return new BadRequestObjectResult(new ErrorResponseDto(response.Message));
        }
    }
}