﻿using System;
using Crave.Libraries.Distance;
using Crave.Libraries.Distance.Google;
using Crave.Ordering.Api.Core.Configuration;
using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Api.Data.Repositories;
using Crave.Ordering.Api.Domain.Converters;
using Crave.Ordering.Api.Domain.Factories;
using Crave.Ordering.Api.Domain.Interfaces;
using Crave.Ordering.Api.Domain.Interfaces.UseCases;
using Crave.Ordering.Api.Domain.UseCases;
using Crave.Ordering.Api.Domain.Validation;
using Microsoft.Extensions.DependencyInjection;

namespace Crave.Ordering.Api.WebApplication.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static void AddSwaggerConfig(this IServiceCollection services)
        {
            services.AddOpenApiDocument(configure =>
            {
                configure.Title = "Ordering Service";
                configure.GenerateEnumMappingDescription = true;
                configure.AllowReferencesWithProperties = true;
                configure.IgnoreObsoleteProperties = false;
            });
        }

        public static void AddDistanceProvider(this IServiceCollection services, GoogleDistanceConfiguration googleDistanceConfiguration)
        {
            services.AddSingleton(new GoogleDistanceClientConfiguration(googleDistanceConfiguration.ApiKey));
            services.AddSingleton<IDistanceClient, GoogleDistanceClient>();
        }

        public static void AddRepositories(this IServiceCollection services)
        {
            services.AddSingleton<IAlterationRepository, AlterationRepository>();
            services.AddSingleton<IAlterationitemRepository, AlterationitemRepository>();
            services.AddSingleton<IAlterationoptionTagRepository, AlterationoptionTagRepository>();
            services.AddSingleton<IAnalyticsProcessingTaskRepository, AnalyticsProcessingTaskRepository>();
            services.AddSingleton<ICategoryRepository, CategoryRepository>();
            services.AddSingleton<ICheckoutMethodRepository, CheckoutMethodRepository>();
            services.AddSingleton<IClientConfigurationRepository, ClientConfigurationRepository>();
            services.AddSingleton<IClientRepository, ClientRepository>();
            services.AddSingleton<ICompanyRepository, CompanyRepository>();
            services.AddSingleton<IDeliverypointRepository, DeliverypointRepository>();
            services.AddSingleton<IDeliverypointgroupRepository, DeliverypointgroupRepository>();
            services.AddSingleton<IDeviceRepository, DeviceRepository>();
            services.AddSingleton<IExternalDeliverypointRepository, ExternalDeliverypointRepository>();
            services.AddSingleton<IExternalProductRepository, ExternalProductRepository>();
            services.AddSingleton<IExternalSystemRepository, ExternalSystemRepository>();
            services.AddSingleton<IMenuRepository, MenuRepository>();
            services.AddSingleton<INetmessageRepository, NetmessageRepository>();
            services.AddSingleton<IOrderRepository, OrderRepository>();
            services.AddSingleton<IOrderitemRepository, OrderitemRepository>();
            services.AddSingleton<IOrderRoutestephandlerHistoryRepository, OrderRoutestephandlerHistoryRepository>();
            services.AddSingleton<IOrderRoutestephandlerRepository, OrderRoutestephandlerRepository>();
            services.AddSingleton<IOutletRepository, OutletRepository>();
            services.AddSingleton<IOutletSellerInformationRepository, OutletSellerInformationRepository>();
            services.AddSingleton<IPaymentIntegrationConfigurationRepository, PaymentIntegrationConfigurationRepository>();
            services.AddSingleton<IPriceScheduleRepository, PriceScheduleRepository>();
            services.AddSingleton<IProductRepository, ProductRepository>();
            services.AddSingleton<IProductTagRepository, ProductTagRepository>();
            services.AddSingleton<IProductCategoryTagRepository, ProductCategoryTagRepository>();
            services.AddSingleton<IReceiptRepository, ReceiptRepository>();
            services.AddSingleton<IRouteRepository, RouteRepository>();
            services.AddSingleton<IRoutestephandlerRepository, RoutestephandlerRepository>();
            services.AddSingleton<IScheduleRepository, ScheduleRepository>();
            services.AddSingleton<IServiceMethodRepository, ServiceMethodRepository>();
            services.AddSingleton<ISupportpoolRepository, SupportpoolRepository>();
            services.AddSingleton<ITaxTariffRepository, TaxTariffRepository>();
            services.AddSingleton<ITerminalRepository, TerminalRepository>();
            services.AddSingleton<IOptInRepository, OptInRepository>();
        }

        public static void AddUseCases(this IServiceCollection services)
        {
            services.AddSingleton<ICancelRoutesUseCase, CancelRoutesUseCase>();
            services.AddSingleton<ConvertOrderToEntityUseCase>();
            services.AddSingleton<ICreateAndSaveOrderUseCase, CreateAndSaveOrderUseCase>();
            services.AddSingleton<ICreateOrderRouteUseCase, CreateOrderRouteUseCase>();
            services.AddSingleton<ICreateReceiptUseCase, CreateReceiptUseCase>();
            services.AddSingleton<EnrichOrderWithAnalyticsUseCase>();
            services.AddSingleton<EnrichOrderitemsWithAlterationitemsUseCase>();
            services.AddSingleton<EnrichOrderitemsWithAlterationsUseCase>();
            services.AddSingleton<EnrichOrderitemsWithCategoriesUseCase>();
            services.AddSingleton<EnrichOrderitemsWithPriceLevelItemsUseCase>();
            services.AddSingleton<EnrichOrderitemsWithProductsUseCase>();
            services.AddSingleton<EnrichOrderitemsWithTagsUseCase>();
            services.AddSingleton<EnrichOrderWithChargeToDeliverypointUseCase>();
            services.AddSingleton<EnrichOrderWithCheckoutMethodDetailsUseCase>();
            services.AddSingleton<EnrichOrderWithClientUseCase>();
            services.AddSingleton<EnrichOrderWithCompanyDetailsUseCase>();
            services.AddSingleton<EnrichOrderWithDeliveryInformationUseCase>();
            services.AddSingleton<EnrichOrderWithDeliverypointDetailsUseCase>();
            services.AddSingleton<EnrichOrderWithOrderitemsUseCase>();
            services.AddSingleton<EnrichOrderWithOrderStatusUseCase>();
            services.AddSingleton<EnrichOrderWithOutletDetailsUseCase>();
            services.AddSingleton<EnrichOrderWithServiceMethodDetailsUseCase>();
            services.AddSingleton<EnrichOrderWithTaxesUseCase>();
            services.AddSingleton<ExecuteRoutestepUseCase>();
            services.AddSingleton<FailExpiredOrderroutestephandlersForTerminalUseCase>();
            services.AddSingleton<IGetClientConfigurationUseCase, GetClientConfigurationUseCase>();
            services.AddSingleton<IGetDeliveryRateUseCase, GetDeliveryRateUseCase>();
            services.AddSingleton<GetDistanceBetweenLocationsUseCase>();
            services.AddSingleton<GetDistanceToOutletUseCase>();
            services.AddSingleton<GetEmailAddressesForSupportpoolUseCase>();
            services.AddSingleton<IGetOrderNotesUseCase, GetOrderNotesUseCase>();
            services.AddSingleton<IGetOrdersForTerminalUseCase, GetOrdersForTerminalUseCase>();
            services.AddSingleton<IGetOrderHistoryForTerminalUseCase, GetOrderHistoryForTerminalUseCase>();
            services.AddSingleton<GetOrderHistoryForTerminalUseCase>();
            services.AddSingleton<IGetOrderUseCase, GetOrderUseCase>();
            services.AddSingleton<IGetOrderForPaymentUseCase, GetOrderForPaymentUseCase>();
            services.AddSingleton<GetOutletLocationUseCase>();
            services.AddSingleton<GetPhonenumbersForSupportpoolUseCase>();
            services.AddSingleton<GetTerminalsForwardingFromThisTerminalUseCase>();
            services.AddSingleton<GetTerminalToForwardToUseCase>();
            services.AddSingleton<HandleEmailDocumentRoutestephandlerUseCase>();
            services.AddSingleton<HandleEmailOrderRoutestephandlerUseCase>();
            services.AddSingleton<HandleNonTerminalRoutingstephandlerUseCase>();
            services.AddSingleton<HandleOpsServiceRoutestephandlerUseCase>();
            services.AddSingleton<HandleSmsOrderRoutestephandlerUseCase>();
            services.AddSingleton<HandleExternalSystemRoutestephandlerUseCase>();
            services.AddSingleton<IProcessClientOrdersUseCase, ProcessClientOrdersUseCase>();
            services.AddSingleton<IProcessTerminalOrderUseCase, ProcessTerminalOrderUseCase>();
            services.AddSingleton<ResendReceiptUseCase>();
            services.AddSingleton<IRouteOrderUseCase, RouteOrderUseCase>();
            services.AddSingleton<SaveOrderRoutestephandlerUseCase>();
            services.AddSingleton<ISaveOrderUseCase, SaveOrderUseCase>();
            services.AddSingleton<SetForwardedTerminalUseCase>();
            services.AddSingleton<IStartRouteUseCase, StartRouteUseCase>();
            services.AddSingleton<SubmitOrderToExternalSystemUseCase>();
            services.AddSingleton<UpdateForwardForTerminalUseCase>();
            services.AddSingleton<UpdateOrderRoutestephandlerStatusesUseCase>();
            services.AddSingleton<IUpdateOrderRoutestephandlerStatusesUseCase, UpdateOrderRoutestephandlerStatusesUseCase>();
            services.AddSingleton<UpdateOrderRoutestephandlerUseCase>();
            services.AddSingleton<IUpdateOrderRoutestephandlerUseCase, UpdateOrderRoutestephandlerUseCase>();
            services.AddSingleton<IUpdateOrderStatusUseCase, UpdateOrderStatusUseCase>();
            services.AddSingleton<IUpdateOrderStatusByGuidUseCase, UpdateOrderStatusByGuidUseCase>();
            services.AddSingleton<IUpdateOrderRoutestephandlerForTerminalUseCase, UpdateOrderRoutestephandlerForTerminalUseCase>();
            services.AddSingleton<IValidateOrderitemsUseCase, ValidateOrderitemsUseCase>();
            services.AddSingleton<IValidateOrderUseCase, ValidateOrderUseCase>();
            services.AddSingleton<ValidateOrderBeforeRoutingUseCase>();
            services.AddSingleton<ValidateOrderBeforeSaveUseCase>();
            services.AddSingleton<ValidateOrderitemBeforeSaveUseCase>();
            services.AddSingleton<ValidateOrderitemAlterationitemBeforeSaveUseCase>();
            services.AddSingleton<IWriteRouteUseCase, WriteRouteUseCase>();
            services.AddSingleton(provider => new Lazy<WriteRouteUseCase>(provider.GetService<WriteRouteUseCase>)); // Used in UpdateOrderRoutestephandlerUseCase to write escalation routing
            services.AddSingleton<ProcessOptInUseCase>();
            services.AddSingleton<IOrderRoutestephandlerStatusUpdatedUseCase, OrderRoutestephandlerStatusUpdatedUseCase>();
            services.AddSingleton<IPaymentStatusChangedUseCase, PaymentStatusChangedUseCase>();
        }

        public static void AddValidators(this IServiceCollection services)
        {
            services.AddSingleton<DeliveryInformationValidator>();
        }

        public static void AddConverters(this IServiceCollection services)
        {
            services.AddSingleton<AlterationitemConverter>();
            services.AddSingleton<DeliveryInformationConverter>();
            services.AddSingleton<OrderConverter>();
            services.AddSingleton<OrderitemAlterationitemConverter>();
            services.AddSingleton<OrderitemConverter>();
            services.AddSingleton<OrderRoutestephandlerConverter>();
            services.AddSingleton<ReceiptConverter>();
        }

        public static void AddFactories(this IServiceCollection services)
        {
            services.AddSingleton<IOpsClientFactory, OpsClientFactory>();
        }

        public static void AddClients(this IServiceCollection services)
        {
            services.AddHttpClient();
        }
    }
}
