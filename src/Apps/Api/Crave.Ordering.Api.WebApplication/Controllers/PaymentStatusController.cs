﻿using Crave.Ordering.Api.WebApplication.Constants;
using Crave.Ordering.Api.WebApplication.Parameters;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NSwag.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Crave.Ordering.Api.Domain.UseCases;
using Crave.Ordering.Api.Domain.UseCases.Requests;

namespace Crave.Ordering.Api.WebApplication.Controllers
{
	
	public class PaymentStatusController : ControllerBase
	{
        /// <summary>
        /// Update payment status.
        /// </summary>
        [HttpGet(RouteConstants.UpdatePaymentStatus)]
        [OpenApiOperation("Update payment status", "Update payment status")]
        public async Task<ActionResult> UpdateStatus([FromServices] IPaymentStatusChangedUseCase paymentStatusChangedUseCase,
                                                                              [FromQuery] PaymentStatusQueryParameter paymentStatusQueryParameters)
        {
            await paymentStatusChangedUseCase.ExecuteAsync(new PaymentStatusChanged(paymentStatusQueryParameters.OrderId, paymentStatusQueryParameters.PaymentTransactionStatus));  
            return Ok();
        }
    }
}
