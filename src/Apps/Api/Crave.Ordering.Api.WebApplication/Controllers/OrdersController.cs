﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Crave.Ordering.Api.Core.Models.Requests;
using Crave.Ordering.Api.Core.Models.Responses;
using Crave.Ordering.Api.Domain.Interfaces.UseCases;
using Crave.Ordering.Api.Domain.UseCases.Requests;
using Crave.Ordering.Api.Domain.UseCases.Responses;
using Crave.Ordering.Api.WebApplication.Constants;
using Crave.Ordering.Api.WebApplication.Extensions;
using Crave.Ordering.Api.WebApplication.Parameters;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NSwag.Annotations;

namespace Crave.Ordering.Api.WebApplication.Controllers
{
    [ApiController]
    public class OrdersController : ControllerBase
    {
        private readonly IMapper mapper;

        public OrdersController(IMapper mapper) => this.mapper = mapper;

        /// <summary>
        /// Gets pending orders for the specified terminal.
        /// </summary>
        /// <param name="getOrdersForTerminalUseCase">The use case to fetch the pending orders for a terminal.</param>
        /// <param name="ordersQueryParameter">The query parameters containing the terminal id to fetch the orders for.</param>
        /// <returns>A <see cref="IEnumerable{OrderDto}"/> instance.</returns>
        [HttpGet(RouteConstants.Orders)]
        [OpenApiOperation("Get active orders for terminal", "Get a collection of active orders for a specific terminal")]
        [ProducesResponseType(typeof(IEnumerable<OrderDto>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<IEnumerable<OrderDto>>> GetOrdersAsync([FromServices] IGetOrdersForTerminalUseCase getOrdersForTerminalUseCase,
                                                                              [FromQuery] OrdersQueryParameter ordersQueryParameter)
        {
            ResponseBase<IEnumerable<Order>> response = await getOrdersForTerminalUseCase.ExecuteAsync(new GetOrdersForTerminal(ordersQueryParameter.TerminalId));

            if (!response.Success)
            {
                return response.ToActionResult();
            }

            return Ok(mapper.Map<IEnumerable<OrderDto>>(response.Model));
        }

        /// <summary>
        /// Gets an order by its guid.
        /// </summary>
        /// <param name="getOrderUseCase">The use case to fetch the order.</param>
        /// <param name="orderByGuidRouteParameter">The route parameter containing the guid.</param>
        /// <returns>A <see cref="OrderDto"/> instance.</returns>
        [HttpGet(RouteConstants.OrderByGuid)]
        [OpenApiOperation("Get order by GUID", "Gets a specific order by its GUID")]
        [ProducesResponseType(typeof(OrderDto), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<OrderDto>> GetOrderByGuidAsync([FromServices] IGetOrderUseCase getOrderUseCase,
                                                                      [FromRoute] OrderByGuidRouteParameter orderByGuidRouteParameter)
        {
            Order? order = await getOrderUseCase.ExecuteAsync(orderByGuidRouteParameter.Guid);
            if (order == null)
            {
                return NotFound();
            }

            return Ok(mapper.Map<OrderDto>(order));
        }

        /// <summary>
        /// Gets an orders payment details by its guid.
        /// </summary>
        /// <param name="getOrderForPaymentUseCase">The use case to fetch the order.</param>
        /// <param name="orderByGuidRouteParameter">The route parameter containing the guid.</param>
        /// <returns>A <see cref="OrderPaymentDetails"/> instance.</returns>
        [HttpGet(RouteConstants.OrderPaymentDetails)]
        [ProducesResponseType(typeof(OrderPaymentDetails), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<OrderPaymentDetails>> GetOrderPaymentDetailsAsync(
            [FromServices] IGetOrderForPaymentUseCase getOrderForPaymentUseCase,
            [FromRoute] OrderByGuidRouteParameter orderByGuidRouteParameter)
        {
            OrderPaymentDetails? orderPaymentDetails = await getOrderForPaymentUseCase.ExecuteAsync(orderByGuidRouteParameter.Guid);
            if (orderPaymentDetails == null)
            {
                return NotFound();
            }

            return Ok(orderPaymentDetails);
        }

        /// <summary>
        /// Update an order by its guid
        /// </summary>
        /// <returns></returns>
        [HttpPut(RouteConstants.OrderStatusByGuid)]
        [OpenApiOperation("Update order status", "Update the status of an order")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(typeof(ErrorResponseDto), StatusCodes.Status400BadRequest)]
        public async Task<ActionResult> UpdateOrderByGuidAsync([FromServices] IUpdateOrderStatusByGuidUseCase updateOrderStatusByGuidUseCase,
                                                               [FromRoute] OrderByGuidRouteParameter orderByGuidRouteParameter,
                                                               [FromBody] UpdateOrderStatusRequestDto updateOrderStatusRequestDto)
        {
            ResponseBase response = await updateOrderStatusByGuidUseCase.ExecuteAsync(new UpdateOrderStatusByGuidRequest(orderByGuidRouteParameter.Guid, updateOrderStatusRequestDto.OrderStatus));

            return response.ToActionResult();
        }

        /// <summary>
        /// Validates and saves a new order.
        /// </summary>
        /// <param name="createAndSaveOrderUseCase">The use case to validate and save the order.</param>
        /// <param name="createOrderRequestDto">The request body containing the order configuration.</param>
        /// <returns>A <see cref="OrderDto"/> instance.</returns>
        [HttpPost(RouteConstants.Orders)]
        [OpenApiOperation("Create a new order", "Create a new order for a specific company")]
        [ProducesResponseType(typeof(string), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ValidationResponseDto), StatusCodes.Status400BadRequest)]
        public async Task<ActionResult> CreateOrderAsync([FromServices] ICreateAndSaveOrderUseCase createAndSaveOrderUseCase,
                                                         [FromBody] CreateOrderRequestDto createOrderRequestDto)
        {
            CreateAndSaveOrderResponse response = await createAndSaveOrderUseCase.ExecuteAsync(mapper.Map<CreateOrderRequest>(createOrderRequestDto));

            if (!response.Success)
            {
                return BadRequest(mapper.Map<ValidationResponseDto>(response.ValidationResponse));
            }

            return Ok(response.OrderGuid);
        }

        /// <summary>
        /// Update certain values of an order's route step handler
        /// </summary>
        /// <returns></returns>
        [HttpPut(RouteConstants.OrderRoutestephandlerStatus)]
        [OpenApiOperation("Update routestephandler status", "Update the status of a routestephandler")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(typeof(ErrorResponseDto), StatusCodes.Status400BadRequest)]
        public async Task<ActionResult> UpdateOrderRoutestephandlerStatus([FromServices] IUpdateOrderRoutestephandlerForTerminalUseCase updateRoutestephandlerForTerminalUseCase,
                                                                          [FromRoute] OrderRoutestephandlerRouteParameter orderRoutestephandlerRouteParameter,
                                                                          [FromBody] UpdateOrderRoutestephandlerStatusRequestDto updateOrderRoutestephandlerStatusRequestDto)
        {
            UpdateOrderRoutestephandlerStatusRequest statusRequest = mapper.Map<UpdateOrderRoutestephandlerStatusRequest>(updateOrderRoutestephandlerStatusRequestDto);

            ResponseBase response = await updateRoutestephandlerForTerminalUseCase.ExecuteAsync(new UpdateOrderRoutestephandlersForTerminal(orderRoutestephandlerRouteParameter.OrderRoutestephandlerId, statusRequest));

            return response.ToActionResult();
        }

        /// <summary>
        /// Validates the order items of an order.
        /// </summary>
        /// <param name="validateOrderitemsUseCase">The use case to validate the order.</param>
        /// <param name="validateOrderRequestDto">The request body containing the order items to validate.</param>
        /// <returns>A collection of orders.</returns>
        [HttpPost(RouteConstants.ValidateOrder)]
        [OpenApiOperation("Validate order items", "Run validation on the order to see if all order items are correctly configured and have the correct prices")]
        [ProducesResponseType(typeof(ValidationResponseDto), StatusCodes.Status200OK)]
        public Task<ActionResult> ValidateOrderAsync([FromServices] IValidateOrderitemsUseCase validateOrderitemsUseCase,
                                                           [FromBody] ValidateOrderitemsRequestDto validateOrderRequestDto)
        {
            ValidationResponse response = validateOrderitemsUseCase.Execute(mapper.Map<ValidateOrderitemsRequest>(validateOrderRequestDto));

            return Task.FromResult(Ok(mapper.Map<ValidationResponseDto>(response)) as ActionResult);
        }

        /// <summary>
        /// Calculate the delivery rate.
        /// </summary>
        /// <param name="getDeliveryRateUseCase">The use case to calculate the delivery rate.</param>
        /// <param name="getDeliveryRateRequestDto">The request body containing the information required for calculating the delivery rate.</param>
        /// <returns></returns>
        [HttpPost(RouteConstants.CalculateDeliveryRate)]
        [OpenApiOperation("Calculate the delivery rate.", "Calculates the delivery rate for a location, based on a service method and total price.")]
        [ProducesResponseType(typeof(DeliveryRateResponseDto), StatusCodes.Status200OK)]
        public async Task<ActionResult> GetDeliveryRateAsync(
            [FromServices] IGetDeliveryRateUseCase getDeliveryRateUseCase,
            [FromBody] GetDeliveryRateRequestDto getDeliveryRateRequestDto)
        {
            GetDeliveryRateResponse getDeliveryRateResponse = await getDeliveryRateUseCase.ExecuteAsync(mapper.Map<GetDeliveryRateRequest>(getDeliveryRateRequestDto));

            return Ok(mapper.Map<DeliveryRateResponseDto>(getDeliveryRateResponse));
        }

        [HttpPost(RouteConstants.OrderRoutestephandlerStatusUpdated)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult> HandlerUpdatedAsync([FromServices] IOrderRoutestephandlerStatusUpdatedUseCase orderRoutestephandlerStatusUpdatedUseCase,
                                                            [FromRoute] OrderRoutestephandlerStatusUpdatedRouteParameter orderRoutestephandlerStatusUpdatedRouteParameter)
        {
            ResponseBase response = await orderRoutestephandlerStatusUpdatedUseCase.ExecuteAsync(orderRoutestephandlerStatusUpdatedRouteParameter.OrderRoutestephandlerId);

            return response.ToActionResult();
        }

        /// <summary>
        /// Gets the order history (the last 25 completed orders) for the specified terminal.
        /// </summary>
        /// <param name="getOrderHistoryForTerminalUseCase">The use case to fetch the completed orders for a terminal.</param>
        /// <param name="queryParameter">The query parameters containing the terminal id to fetch the orders for.</param>
        /// <returns>A <see cref="IEnumerable{OrderDto}"/> instance.</returns>
        [HttpGet(RouteConstants.OrderHistory)]
        [OpenApiOperation("Get order history for terminal", "Get a collection of the last 25 completed orders for a specific terminal")]
        [ProducesResponseType(typeof(IEnumerable<OrderDto>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<IEnumerable<OrderDto>>> GetOrderHistoryAsync([FromServices] IGetOrderHistoryForTerminalUseCase getOrderHistoryForTerminalUseCase,
                                                                                    [FromQuery] OrdersQueryParameter queryParameter)
        {
            ResponseBase<IEnumerable<Order>> response = await getOrderHistoryForTerminalUseCase.ExecuteAsync(new GetOrderHistoryForTerminal(queryParameter.TerminalId));
            return !response.Success ? response.ToActionResult() : Ok(this.mapper.Map<IEnumerable<OrderDto>>(response.Model));
        }
    }
}
