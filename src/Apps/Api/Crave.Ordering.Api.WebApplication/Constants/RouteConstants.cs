﻿namespace Crave.Ordering.Api.WebApplication.Constants
{
    public static class RouteConstants
    {
        public const string Prefix = "api";

        public const string Version = Prefix + "/v1";

        public const string Orders = Version + "/orders";

        public const string OrderHistory = Version + "/orderhistory";

        public const string UpdatePaymentStatus = Version + "/PaymentStatus/Update";

        public const string OrderByGuid = Orders + "/{guid}";

        public const string OrderStatusByGuid = Orders + "/{guid}/status";

        public const string ValidateOrder = Orders + "/validate";

        public const string CalculateDeliveryRate = Orders + "/deliveryrates/calculate";

        public const string RouteOrder = OrderByGuid + "/route";

        public const string OrderRoutestephandlerStatus = OrderByGuid + "/orderroutestephandlers/{orderroutestephandlerId}/status";

        public const string OrderRoutestephandlerStatusUpdated = "/orderroutestephandlers/{orderroutestephandlerId}/statusupdated";

        public const string OrderPaymentDetails = OrderByGuid + "/paymentdetails";
    }
}
