﻿using AutoMapper;
using Crave.Libraries.Distance.Models;
using Crave.Ordering.Api.Core.Models.Requests;
using Crave.Ordering.Api.Core.Models.Responses;
using Crave.Ordering.Api.Domain.Models;
using Crave.Ordering.Api.Domain.UseCases.Requests;
using Crave.Ordering.Api.Domain.UseCases.Responses;
using Crave.Ordering.Api.Domain.Validation.Errors;

namespace Crave.Ordering.Api.WebApplication.Mappers
{
    /// <summary>
    /// Information on how the AutoMapper configuration work can be found on:
    /// https://docs.automapper.org/en/stable/Configuration.html
    /// </summary>
    public class AutoMapping : Profile
    {
        public AutoMapping()
        {
            MapRequestsToDomainModels();
            MapDomainModelsToResponses();
        }

        private void MapRequestsToDomainModels()
        {
            CreateMap<AlterationitemRequestDto, AlterationitemRequest>(MemberList.Source);
            CreateMap<AnalyticsRequestDto, AnalyticsRequest>(MemberList.Source);
            CreateMap<CreateOrderRequestDto, CreateOrderRequest>(MemberList.Source);
            CreateMap<CustomerRequestDto, CustomerRequest>(MemberList.Source);
            CreateMap<DeliveryChargeRequestDto, DeliveryChargeRequest>(MemberList.Source);
            CreateMap<DeliveryInformationRequestDto, DeliveryInformationRequest>(MemberList.Source);
            CreateMap<DeliverypointRequestDto, DeliverypointRequest>(MemberList.Source);
            CreateMap<OrderitemRequestDto, OrderitemRequest>(MemberList.Source);
            CreateMap<ServiceChargeRequestDto, ServiceChargeRequest>(MemberList.Source);
            CreateMap<TippingRequestDto, TippingRequest>(MemberList.Source);
            CreateMap<ValidateOrderitemsRequestDto, ValidateOrderitemsRequest>(MemberList.Source);
            CreateMap<UpdateOrderRoutestephandlerStatusRequestDto, UpdateOrderRoutestephandlerStatusRequest>(MemberList.Source);
			CreateMap<DeliveryLocationDto, DeliveryLocation>(MemberList.Source);
			CreateMap<GetDeliveryRateRequestDto, GetDeliveryRateRequest>(MemberList.Source);
        }

        private void MapDomainModelsToResponses()
        {
            CreateMap<Alterationitem, AlterationitemDto>();
            CreateMap<Customer, CustomerDto>();
            CreateMap<DeliveryInformation, DeliveryInformationDto>();
            CreateMap<Deliverypoint, DeliverypointDto>();
            CreateMap<Order, OrderDto>();
            CreateMap<OrderitemAlterationitem, OrderitemAlterationitemDto>();
            CreateMap<Orderitem, OrderitemDto>();
            CreateMap<OrderRoutestephandler, OrderRoutestephandlerDto>();
            CreateMap<Receipt, ReceiptDto>();
            CreateMap<ValidationError, ValidationErrorDto>();
            CreateMap<ValidationResponse, ValidationResponseDto>();
			CreateMap<Distance, DistanceDto>();
			CreateMap<PaymentTransaction, PaymentTransactionDto>();
			CreateMap<GetDeliveryRateResponse, DeliveryRateResponseDto>()
				.ForMember(dto => dto.PriceInformation, memberOptions
					=> memberOptions.MapFrom(src => new PriceInformationDto
					{
						Price = src.Charge,
						TaxTariffId = src.TaxTariffId
					}))
				.ForMember(dto => dto.Distance, memberOptions
					=> memberOptions.MapFrom(src => src.Distance == null
						? new DistanceDto()
						: new DistanceDto
						{
							Metres = src.Distance.Metres
						}));
        }
    }
}
