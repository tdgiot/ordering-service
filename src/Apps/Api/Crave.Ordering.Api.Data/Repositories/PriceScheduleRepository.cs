﻿using System.Collections.Generic;
using System.Linq;
using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.SQLServer;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Crave.Ordering.Api.Data.Repositories
{
    public class PriceScheduleRepository : RepositoryBase<PriceScheduleEntity>, IPriceScheduleRepository
    {
        /// <inheritdoc/>
        public EntityCollection<PriceLevelItemEntity> GetPriceLevelItems(IReadOnlyList<int> priceLevelItemIds)
        {
            if (!priceLevelItemIds.Any())
            {
                return new EntityCollection<PriceLevelItemEntity>();
            }

            IncludeFieldsList includes = new IncludeFieldsList(PriceLevelItemFields.Price);

            return DataAccessAdapter.FetchEntityCollection<PriceLevelItemEntity>(new RelationPredicateBucket(PriceLevelItemFields.PriceLevelItemId == priceLevelItemIds.ToList()), 0, null, null, includes);
        }

        /// <inheritdoc />
        public PriceLevelItemEntity GetPriceLevelItemEntity(int priceLevelItemId)
        {
            RelationPredicateBucket filterBucket = new RelationPredicateBucket();
            filterBucket.PredicateExpression.Add(PriceLevelItemFields.PriceLevelItemId == priceLevelItemId);
            filterBucket.Relations.Add(PriceLevelItemEntity.Relations.PriceLevelEntityUsingPriceLevelId);

            IncludeFieldsList includes = new IncludeFieldsList(PriceLevelItemFields.PriceLevelItemId, PriceLevelItemFields.ProductId, PriceLevelItemFields.AlterationoptionId, PriceLevelItemFields.Price);

            PrefetchPath2 prefetch = new PrefetchPath2(EntityType.PriceLevelItemEntity);
            prefetch.Add(PriceLevelItemEntity.PrefetchPathPriceLevelEntity, new IncludeFieldsList(PriceLevelFields.PriceLevelId, PriceLevelFields.Name));

            return DataAccessAdapter.FetchEntity<PriceLevelItemEntity>(filterBucket, prefetch, includes);
        }
    }
}