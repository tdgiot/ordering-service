﻿using System.Collections.Generic;
using System.Linq;
using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using SD.LLBLGen.Pro.QuerySpec;

namespace Crave.Ordering.Api.Data.Repositories
{
    /// <summary>
    /// Repository class for product tags.
    /// </summary>
    public class ProductTagRepository : RepositoryBase<ProductTagEntity>, IProductTagRepository
    {
        public EntityCollection<ProductTagEntity> GetProductTags(IReadOnlyList<int> productIds)
        {
            if (!productIds.Any())
            {
                return new EntityCollection<ProductTagEntity>();
            }

            PredicateExpression filter = new PredicateExpression();
            filter.Add(ProductTagFields.ProductId.In(productIds));

            return FetchEntityCollection(filter);
        }
    }
}
