﻿using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Crave.Ordering.Api.Data.Repositories
{
    public class PaymentIntegrationConfigurationRepository : RepositoryBase<PaymentIntegrationConfigurationEntity>, IPaymentIntegrationConfigurationRepository
    {
        public PaymentIntegrationConfigurationEntity FetchById(int paymentIntegrationConfigurationId)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(PaymentIntegrationConfigurationFields.PaymentIntegrationConfigurationId == paymentIntegrationConfigurationId);

            RelationCollection relations = new RelationCollection();
            relations.Add(PaymentIntegrationConfigurationEntity.Relations.PaymentProviderEntityUsingPaymentProviderId);

            PrefetchPath2 prefetch = new PrefetchPath2(EntityType.PaymentIntegrationConfigurationEntity);
            prefetch.Add(PaymentIntegrationConfigurationEntity.PrefetchPathPaymentProviderEntity);

            QueryParameters queryParameters = new QueryParameters
            {
                FilterToUse = filter,
                RelationsToUse = relations,
                PrefetchPathToUse = prefetch
            };

            return FetchEntity(queryParameters);
        }
    }
}