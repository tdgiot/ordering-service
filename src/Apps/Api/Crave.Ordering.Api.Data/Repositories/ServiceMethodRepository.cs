﻿using System.Threading.Tasks;
using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.SQLServer;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Crave.Ordering.Api.Data.Repositories
{
    /// <summary>
    /// Repository class for service methods.
    /// </summary>
    public class ServiceMethodRepository : RepositoryBase<ServiceMethodEntity>, IServiceMethodRepository
    {
        /// <inheritdoc/>
        public ServiceMethodEntity GetServiceMethod(int serviceMethodId)
        {
            return DataAccessAdapter.FetchEntityById<ServiceMethodEntity>(serviceMethodId, null, null);
        }

        /// <inheritdoc/>
        public async Task<ServiceMethodEntity> GetServiceMethodWithDeliveryDistancesAsync(int serviceMethodId)
        {
            RelationPredicateBucket filterBucket = new RelationPredicateBucket(new PredicateExpression(ServiceMethodFields.ServiceMethodId == serviceMethodId));

            PrefetchPath2 prefetch = new PrefetchPath2(EntityType.ServiceMethodEntity);
            prefetch.Add(ServiceMethodEntity.PrefetchPathDeliveryDistanceCollection);

            IPrefetchPathElement2 prefetchOutlet = prefetch.Add(ServiceMethodEntity.PrefetchPathOutletEntity, new IncludeFieldsList());

            prefetchOutlet.SubPath.Add(OutletEntity.PrefetchPathDeliveryChargeProductEntity, new IncludeFieldsList(ProductFields.TaxTariffId))
                          .SubPath.Add(ProductEntity.PrefetchPathTaxTariffEntity, new IncludeFieldsList(TaxTariffFields.Percentage));
            prefetchOutlet.SubPath.Add(OutletEntity.PrefetchPathCompanyEntity, new IncludeFieldsList(CompanyFields.PricesIncludeTaxes));

            return await DataAccessAdapter.FetchEntityAsync<ServiceMethodEntity>(filterBucket, prefetch, null);
        }
    }
}
