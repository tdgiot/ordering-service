﻿using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.SQLServer;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Crave.Ordering.Api.Data.Repositories
{
    /// <summary>
    /// Repository class for delivery points.
    /// </summary>
    public class DeliverypointRepository : RepositoryBase<DeliverypointEntity>, IDeliverypointRepository
    {
        /// <inheritdoc/>
        public DeliverypointEntity GetDeliverypoint(int deliverypointId)
        {
            PrefetchPath2 prefetch = new PrefetchPath2(EntityType.DeliverypointEntity);
            prefetch.Add(DeliverypointEntity.PrefetchPathDeliverypointgroupEntity, new IncludeFieldsList(DeliverypointgroupFields.Name));

            IncludeFieldsList includeFieldsList = new IncludeFieldsList(DeliverypointFields.Name, DeliverypointFields.Number);

            return DataAccessAdapter.FetchEntityById<DeliverypointEntity>(deliverypointId, prefetch, includeFieldsList);
        }

        /// <inheritdoc />
        public static EntityCollection<DeliverypointEntity> FetchDeliverypointsLinkedToClients(int companyId)
        {
            IncludeFieldsList includedFields = new IncludeFieldsList();
            includedFields.Add(DeliverypointFields.DeliverypointId);
            includedFields.Add(DeliverypointFields.Number);

            RelationPredicateBucket bucket = new RelationPredicateBucket();
            bucket.Relations.Add(DeliverypointEntity.Relations.ClientEntityUsingDeliverypointId, JoinHint.Right);
            bucket.PredicateExpression.Add(DeliverypointFields.CompanyId == companyId);

            EntityCollection<DeliverypointEntity> collection = new EntityCollection<DeliverypointEntity>();
            using (DataAccessAdapter adapter = new DataAccessAdapter())
            {
                adapter.FetchEntityCollection(collection, includedFields, bucket);
            }

            return collection;
        }

        /// <inheritdoc/>
        public DeliverypointEntity GetDeliverypointForServiceMethod(string number, int? deliverypointgroupId, int serviceMethodId)
        {
            RelationPredicateBucket filterBucket = new RelationPredicateBucket();
            filterBucket.PredicateExpression.Add(DeliverypointFields.Number == number);
            filterBucket.PredicateExpression.Add(ServiceMethodDeliverypointgroupFields.ServiceMethodId == serviceMethodId);

            if (deliverypointgroupId.HasValue)
            {
                filterBucket.PredicateExpression.Add(ServiceMethodDeliverypointgroupFields.DeliverypointgroupId == deliverypointgroupId);
            }

            filterBucket.Relations.Add(DeliverypointEntity.Relations.DeliverypointgroupEntityUsingDeliverypointgroupId);
            filterBucket.Relations.Add(DeliverypointgroupEntity.Relations.ServiceMethodDeliverypointgroupEntityUsingDeliverypointgroupId);

            PrefetchPath2 prefetch = new PrefetchPath2(EntityType.DeliverypointEntity);
            prefetch.Add(DeliverypointEntity.PrefetchPathDeliverypointgroupEntity, new IncludeFieldsList(DeliverypointgroupFields.Name));

            IncludeFieldsList includeFieldsList = new IncludeFieldsList(DeliverypointFields.Name, DeliverypointFields.Number);

            return DataAccessAdapter.FetchEntity<DeliverypointEntity>(filterBucket, prefetch, includeFieldsList);
        }

        /// <inheritdoc/>
        public DeliverypointEntity GetDeliverypointForCheckoutMethod(string number, int? deliverypointgroupId, int checkoutMethodId)
        {
            RelationPredicateBucket filterBucket = new RelationPredicateBucket();
            filterBucket.PredicateExpression.Add(DeliverypointFields.Number == number);
            filterBucket.PredicateExpression.Add(CheckoutMethodDeliverypointgroupFields.CheckoutMethodId == checkoutMethodId);

            if (deliverypointgroupId.HasValue)
            {
                filterBucket.PredicateExpression.Add(CheckoutMethodDeliverypointgroupFields.DeliverypointgroupId == deliverypointgroupId);
            }

            filterBucket.Relations.Add(DeliverypointEntity.Relations.DeliverypointgroupEntityUsingDeliverypointgroupId);
            filterBucket.Relations.Add(DeliverypointgroupEntity.Relations.CheckoutMethodDeliverypointgroupEntityUsingDeliverypointgroupId);

            PrefetchPath2 prefetch = new PrefetchPath2(EntityType.DeliverypointEntity);
            prefetch.Add(DeliverypointEntity.PrefetchPathDeliverypointgroupEntity, new IncludeFieldsList(DeliverypointgroupFields.Name));

            IncludeFieldsList includeFieldsList = new IncludeFieldsList(DeliverypointFields.Name, DeliverypointFields.Number);

            return DataAccessAdapter.FetchEntity<DeliverypointEntity>(filterBucket, prefetch, includeFieldsList);
        }
    }
}
