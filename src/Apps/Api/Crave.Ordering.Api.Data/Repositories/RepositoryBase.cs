﻿using System.Threading.Tasks;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.SQLServer;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Crave.Ordering.Api.Data.Repositories
{
    /// <summary>
    /// Base repository class
    /// </summary>
    /// <typeparam name="TEntity">The type of the entity to do operations on.</typeparam>
    public class RepositoryBase<TEntity> where TEntity : EntityBase2, new()
    {
        /// <summary>
        /// Fetches an entity collection from the database.
        /// </summary>
        /// <param name="filterBucket">The bucket containing the filter and relations to fetch the collection from the database.</param>
        /// <param name="maxNumberOfItemsToReturn">The maximum number of items to return. (0 is all).</param>
        /// <param name="sortClauses">The sort clauses defining the order in which the entity collection is sorted.</param>
        /// <param name="prefetchPath">The pre fetch path which determines additional entities to fetch from.</param>
        /// <param name="excludedIncludedFields">The included or excluded fields to use when fetching the data.</param>
        /// <param name="adapter">The data access adapter to use when fetching the entity collection from the database. Use null to create a new one.</param>
        /// <returns>An <see cref="EntityCollection"/> instance.</returns>
        protected virtual EntityCollection<TEntity> FetchEntityCollection(RelationPredicateBucket filterBucket, int maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath2 prefetchPath, ExcludeIncludeFieldsList excludedIncludedFields, DataAccessAdapter adapter = null)
        {
            return DataAccessAdapter.FetchEntityCollection<TEntity>(filterBucket, maxNumberOfItemsToReturn, sortClauses, prefetchPath, excludedIncludedFields, adapter);
        }

        /// <summary>
        /// Fetches an entity collection from the database.
        /// </summary>
        /// <param name="queryParameters">The query parameters to use for fetching the entity collection.</param>
        /// <param name="adapter">The data access adapter to use when fetching the entity collection from the database. Use null to create a new one.</param>
        /// <returns>An <see cref="EntityCollection{TEntity}"/> instance.</returns>
        protected virtual EntityCollection<TEntity> FetchEntityCollection(QueryParameters queryParameters, DataAccessAdapter adapter = null)
        {
            return DataAccessAdapter.FetchEntityCollection<TEntity>(queryParameters, adapter);
        }

        /// <summary>
        /// Fetches an entity collection from the database.
        /// </summary>
        /// <param name="filter">The filter to fetch the collection from the database.</param>
        /// <param name="relations">The relations to fetch the collection from the database.</param>
        /// <param name="maxNumberOfItemsToReturn">The maximum number of items to return. (0 is all).</param>
        /// <param name="sortClauses">The sort clauses defining the order in which the entity collection is sorted.</param>
        /// <param name="prefetchPath">The pre fetch path which determines additional entities to fetch from.</param>
        /// <param name="excludedIncludedFields">The included or excluded fields to use when fetching the data.</param>
        /// <param name="adapter">The data access adapter to use when fetching the entity collection from the database. Use null to create a new one.</param>
        /// <returns>An <see cref="EntityCollection"/> instance.</returns>
        protected virtual EntityCollection<TEntity> FetchEntityCollection(PredicateExpression filter, RelationCollection relations = null, int maxNumberOfItemsToReturn = 0, 
            ISortExpression sortClauses = null, IPrefetchPath2 prefetchPath = null, ExcludeIncludeFieldsList excludedIncludedFields = null, DataAccessAdapter adapter = null)
        {
            return DataAccessAdapter.FetchEntityCollection<TEntity>(new RelationPredicateBucket(filter, relations), maxNumberOfItemsToReturn, sortClauses, prefetchPath, excludedIncludedFields, adapter);
        }

        /// <summary>
        /// Gets the amount of records in a database table for the specified field and filter.
        /// </summary>
        /// <param name="field">The field to count.</param>
        /// <param name="filter">The filter to count the records on.</param>
        /// <param name="relations">The relations collection to use.</param>
        /// <returns>An <see cref="int"/> containing the amount of records.</returns>
        protected static int GetCount(IEntityField2 field, IPredicate filter, RelationCollection relations, DataAccessAdapterBase adapter = null)
        {
            return DataAccessAdapter.GetCount(field, filter, relations, adapter);
        }

        /// <summary>
        /// Gets a scalar value (min, max, average, etc) for the specified field and filter.
        /// </summary>
        /// <typeparam name="T">The type of the return value.</typeparam>
        /// <param name="field">The field to get the scalar value for.</param>
        /// <param name="filter">The filter to use when getting the scalar value.</param>
        /// <param name="aggregateFunction">The aggregate function (min, max, average, etc) to apply.</param>
        /// <returns>A scalar value.</returns>
        protected static T GetScalar<T>(IEntityField2 field, IPredicate filter, AggregateFunction aggregateFunction, RelationCollection relations, DataAccessAdapterBase adapter = null)
        {
            return DataAccessAdapter.GetScalar<T>(field, filter, aggregateFunction, relations, adapter);
        }

        /// <summary>
        /// Fetches an entity using the specified unique constraint filter.
        /// </summary>
        /// <param name="adapter">The data access adapter to use when fetching the entity from the database. Use null to create a new one.</param>
        /// <param name="entity">The entity to fill.</param>
        /// <param name="uniqueConstraintFilter">The filter to apply when getting the entity.</param>
        /// <param name="prefetchPath">The pre fetch path which determines additional entities to fetch from.</param>
        /// <param name="excludedIncludedFields">The included or excluded fields to use when fetching the data.</param>
        /// <returns>An entity instance.</returns>
        protected virtual bool FetchEntityUsingUniqueConstraint(TEntity entity, IPredicateExpression uniqueConstraintFilter, IPrefetchPath2 prefetchPath, ExcludeIncludeFieldsList excludedIncludedFields, DataAccessAdapter adapter = null)
        {
            return DataAccessAdapter.FetchEntityUsingUniqueConstraint<TEntity>(entity, uniqueConstraintFilter, prefetchPath, excludedIncludedFields, adapter);
        }

        /// <summary>
        /// Fetches an entity using the specified id.
        /// </summary>
        /// <param name="adapter">The data access adapter to use when fetching the entity from the database. Use null to create a new one.</param>
        /// <param name="id">The id of the entity to fetch.</param>
        /// <param name="prefetchPath">The pre fetch path which determines additional entities to fetch from.</param>
        /// <param name="excludedIncludedFields">The included or excluded fields to use when fetching the data.</param>
        /// <returns>An entity instance.</returns>
        protected virtual TEntity FetchEntityById(int id, IPrefetchPath2 prefetchPath, ExcludeIncludeFieldsList excludedIncludedFields, DataAccessAdapter adapter = null)
        {
            return DataAccessAdapter.FetchEntityById<TEntity>(id, prefetchPath, excludedIncludedFields, adapter);
        }

        /// <summary>
        /// Fetches an entity using the specified filter.
        /// </summary>
        /// <param name="filterBucket">The bucket containing the filter and relations to fetch the collection from the database.</param>
        /// <param name="prefetchPath">The pre fetch path which determines additional entities to fetch from.</param>
        /// <param name="excludedIncludedFields">The included or excluded fields to use when fetching the data.</param>
        /// <returns>An entity instance.</returns>
        protected virtual TEntity FetchEntity(RelationPredicateBucket filterBucket, IPrefetchPath2 prefetchPath, ExcludeIncludeFieldsList excludedIncludedFields, DataAccessAdapter adapter = null)
        {
            return DataAccessAdapter.FetchEntity<TEntity>(filterBucket, prefetchPath, excludedIncludedFields, adapter);
        }

        /// <summary>
        /// Fetches an entity using the specified query parameters.
        /// </summary>
        /// <param name="queryParameters">The query parameters to use for fetching the entity.</param>
        /// <param name="adapter">The data access adapter to use when fetching the entity from the database. Use null to create a new one.</param>
        /// <returns>An entity instance.</returns>
        protected virtual TEntity FetchEntity(QueryParameters queryParameters, DataAccessAdapter adapter = null)
        {
            return DataAccessAdapter.FetchEntity<TEntity>(queryParameters, adapter);
        }

        /// <summary>
        /// Fetches an entity using the specified filter.
        /// </summary>
        /// <param name="filterBucket">The bucket containing the filter and relations to fetch the collection from the database.</param>
        /// <param name="prefetchPath">The pre fetch path which determines additional entities to fetch from.</param>
        /// <param name="excludedIncludedFields">The included or excluded fields to use when fetching the data.</param>
        /// <param name="adapter">The adapter to use when fetching data.</param>
        /// <returns>An entity instance.</returns>
        protected virtual Task<TEntity> FetchEntityAsync(RelationPredicateBucket filterBucket, IPrefetchPath2 prefetchPath, ExcludeIncludeFieldsList excludedIncludedFields, DataAccessAdapter adapter = null)
        {
            return DataAccessAdapter.FetchEntityAsync<TEntity>(filterBucket, prefetchPath, excludedIncludedFields, adapter);
        }

        /// <summary>
        /// Saves the specified entity to the database.
        /// </summary>
        /// <param name="entity">The entity to save.</param>
        /// <param name="refetchAfterSave">Flag that determines whether the entity is refetches after it has been saved.</param>
        /// <returns>True if saving the entity was successful, False if not.</returns>
        protected static bool Save(TEntity entity, bool refetchAfterSave, DataAccessAdapter adapter = null)
        {
            return DataAccessAdapter.Save(entity, refetchAfterSave, adapter);
        }

        protected static bool Save(EntityCollection<TEntity> entityCollection, bool refetchAfterSave, bool recurse)
        {
            return DataAccessAdapter.Save(entityCollection, refetchAfterSave, recurse) > 0;
        }

        /// <summary>
        /// Updates entities directly in the database without fetching them first.
        /// </summary>
        /// <param name="entityWithNewValues">The entity containing the new value(s) to use in the update.</param>
        /// <param name="filterBucket">The bucket containing the filter and relations to update the entities in the database.</param>
        /// <param name="adapter">The data access adapter to use when updating the entities in the database. Use null to create a new one.</param>
        /// <returns>An <see cref="int"/> instance containing the amount of entities that were updated.</returns>
        public int UpdateEntitiesDirectly(TEntity entityWithNewValues, RelationPredicateBucket filterBucket, DataAccessAdapterBase adapter = null)
        {
            return DataAccessAdapter.UpdateEntitiesDirectly(entityWithNewValues, filterBucket, adapter);
        }

        /// <summary>
        /// Deletes an entity from the database.
        /// </summary>
        /// <param name="entity">The entity to delete.</param>
        /// <param name="adapter">The data access adapter to use when deleting the entity in the database. Use null to create a new one.</param>
        /// <returns>True if deleting the entity was successful, False if not.</returns>
        protected virtual bool Delete(TEntity entity, DataAccessAdapter adapter = null)
        {
            return DataAccessAdapter.Delete(entity, adapter);
        }

        /// <summary>
        /// Deletes entities directly in the database without fetching them first.
        /// </summary>
        /// <param name="entity">The entity to delete.</param>
        /// <param name="filterBucket">The bucket containing the filter and relations to delete the entities in the database.</param>
        /// <returns>True if deleting the entity was successful, False if not.</returns>
        protected virtual int DeleteEntitiesDirectly(RelationPredicateBucket filterBucket)
        {
            return DataAccessAdapter.DeleteEntitiesDirectly<TEntity>(filterBucket);
        }
    }
}