﻿using System.Collections.Generic;
using System.Linq;
using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.SQLServer;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Crave.Ordering.Api.Data.Repositories
{
    /// <summary>
    /// Repository class for schedules.
    /// </summary>
    public class ScheduleRepository : RepositoryBase<ScheduleEntity>, IScheduleRepository
	{
        /// <inheritdoc/>
        public EntityCollection<ScheduleEntity> GetSchedules(IReadOnlyList<int> scheduleIds)
        {
			if (!scheduleIds.Any())
            {
                return new EntityCollection<ScheduleEntity>();
            }

            PrefetchPath2 prefetch = new PrefetchPath2(EntityType.ScheduleEntity);
            prefetch.Add(ScheduleEntity.PrefetchPathScheduleitemCollection);

            return DataAccessAdapter.FetchEntityCollection<ScheduleEntity>(new RelationPredicateBucket(ScheduleFields.ScheduleId == scheduleIds.ToList()), 0, null, prefetch, null);
		}

		/// <inheritdoc/>
		public ScheduleEntity GetSchedule(int scheduleId)
		{
            PrefetchPath2 prefetch = new PrefetchPath2(EntityType.ScheduleEntity);
            prefetch.Add(ScheduleEntity.PrefetchPathScheduleitemCollection);

            return DataAccessAdapter.FetchEntityById<ScheduleEntity>(scheduleId, prefetch, null);
		}
	}
}
