﻿using System.Linq;
using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Crave.Ordering.Api.Data.Repositories
{
    /// <summary>
    /// Repository class for external deliverypoints.
    /// </summary>
    public class ExternalDeliverypointRepository : RepositoryBase<ExternalDeliverypointEntity>, IExternalDeliverypointRepository
    {
        /// <inheritdoc/>
        public ExternalDeliverypointEntity GetExternalDeliverypointForDeliverypointAndExternalSystem(int deliverypointId, int externalSystemId)
        {
            RelationPredicateBucket filterBucket = new RelationPredicateBucket();
            filterBucket.PredicateExpression.Add(ExternalDeliverypointFields.ExternalSystemId == externalSystemId);
            filterBucket.PredicateExpression.Add(DeliverypointExternalDeliverypointFields.DeliverypointId == deliverypointId);
            filterBucket.Relations.Add(ExternalDeliverypointEntity.Relations.DeliverypointExternalDeliverypointEntityUsingExternalDeliverypointId);

            return FetchEntityCollection(filterBucket, 1, null, null, null).FirstOrDefault();
        }
    }
}
