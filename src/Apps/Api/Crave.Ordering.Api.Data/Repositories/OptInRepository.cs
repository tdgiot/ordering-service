﻿using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Crave.Ordering.Api.Data.Repositories
{
    public class OptInRepository : RepositoryBase<OptInEntity>, IOptInRepository
    {
        public bool Save(OptInEntity optInEntity)
        {
            return Save(optInEntity, false);
        }

        public OptInEntity GetByEmail(int outletId, string email)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(OptInFields.OutletId == outletId);
            filter.Add(OptInFields.Email == email);

            RelationPredicateBucket bucket = new RelationPredicateBucket(filter);

            return FetchEntity(bucket, null, null);
        }

        public OptInEntity GetByPhoneNumber(int outletId, string phoneNumber)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(OptInFields.OutletId == outletId);
            filter.Add(OptInFields.PhoneNumber == phoneNumber);

            RelationPredicateBucket bucket = new RelationPredicateBucket(filter);

            return FetchEntity(bucket, null, null);
        }
        
        public bool DeleteByEmail(int outletId, string email)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(OptInFields.OutletId == outletId);
            filter.Add(OptInFields.Email == email);

            RelationPredicateBucket bucket = new RelationPredicateBucket(filter);

            int deletedEntitiesCount = DeleteEntitiesDirectly(bucket);

            return deletedEntitiesCount > 0;
        }

        public bool DeleteByPhoneNumber(int outletId, string phoneNumber)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(OptInFields.OutletId == outletId);
            filter.Add(OptInFields.PhoneNumber == phoneNumber);

            RelationPredicateBucket bucket = new RelationPredicateBucket(filter);

            int deletedEntitiesCount = DeleteEntitiesDirectly(bucket);

            return deletedEntitiesCount > 0;
        }
    }
}
