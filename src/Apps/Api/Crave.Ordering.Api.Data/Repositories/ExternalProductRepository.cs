﻿using System.Collections.Generic;
using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.SQLServer;
using SD.LLBLGen.Pro.ORMSupportClasses;
using SD.LLBLGen.Pro.QuerySpec;

namespace Crave.Ordering.Api.Data.Repositories
{
    /// <summary>
    /// Repository class for external products.
    /// </summary>
    public class ExternalProductRepository : RepositoryBase<ExternalProductEntity>, IExternalProductRepository
    {
        /// <inheritdoc/>
        public ExternalProductEntity GetExternalProductForProductAndExternalSystem(int productId, int externalSystemId)
        {
            RelationPredicateBucket filterBucket = new RelationPredicateBucket();
            filterBucket.PredicateExpression.Add(ExternalProductFields.ExternalSystemId == externalSystemId);
            filterBucket.PredicateExpression.Add(ProductFields.ProductId == productId);
            filterBucket.Relations.Add(ExternalProductEntity.Relations.ProductEntityUsingExternalProductId);

            return DataAccessAdapter.FetchEntity<ExternalProductEntity>(filterBucket, null, null);
        }

        public EntityCollection<ExternalProductEntity> GetExternalProducts(int externalSystemId)
        {
            RelationPredicateBucket filterBucket = new RelationPredicateBucket(ExternalProductFields.ExternalSystemId == externalSystemId);

            return DataAccessAdapter.FetchEntityCollection<ExternalProductEntity>(filterBucket, 0, null, null, null);
        }

        public EntityCollection<ExternalProductEntity> GetExternalProducts(int externalSystemId, IEnumerable<string> ids)
        {
            RelationPredicateBucket filterBucket = new RelationPredicateBucket();
            filterBucket.PredicateExpression.Add(ExternalProductFields.ExternalSystemId == externalSystemId);
            filterBucket.PredicateExpression.Add(ExternalProductFields.Id.In(ids));

            return DataAccessAdapter.FetchEntityCollection<ExternalProductEntity>(filterBucket, 0, null, null, null);
        }

        public bool Save(ExternalProductEntity entity)
        {
            return DataAccessAdapter.Save(entity, false);
        }

        public bool Save(EntityCollection<ExternalProductEntity> externalProductEntities)
        {
            return DataAccessAdapter.Save(externalProductEntities, false, false) > 0;
        }

        public bool Delete(ExternalProductEntity entity)
        {
            return DataAccessAdapter.Delete(entity);
        }
    }
}
