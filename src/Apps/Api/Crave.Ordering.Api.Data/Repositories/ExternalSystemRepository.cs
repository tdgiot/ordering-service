﻿using Crave.Enums;
using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.SQLServer;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Crave.Ordering.Api.Data.Repositories
{
    /// <summary>
    /// Repository class for external systems.
    /// </summary>
    public class ExternalSystemRepository : RepositoryBase<ExternalSystemEntity>, IExternalSystemRepository
    {
        /// <inheritdoc/>
        public ExternalSystemEntity GetExternalSystemForCompanyByType(int companyId, ExternalSystemType externalSystemType)
        {
            RelationPredicateBucket filterBucket = new RelationPredicateBucket();
            filterBucket.PredicateExpression.Add(ExternalSystemFields.CompanyId == companyId);
            filterBucket.PredicateExpression.Add(ExternalSystemFields.Type == externalSystemType);

            return DataAccessAdapter.FetchEntity<ExternalSystemEntity>(filterBucket, null, null);
        }

        /// <inheritdoc/>
        public ExternalSystemEntity GetExternalSystemByChannelLinkId(string channelLinkId)
        {
            RelationPredicateBucket filterBucket = new RelationPredicateBucket(ExternalSystemFields.StringValue3 == channelLinkId);

            return DataAccessAdapter.FetchEntity<ExternalSystemEntity>(filterBucket, null, null);
        }
    }
}
