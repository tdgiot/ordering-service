﻿using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.SQLServer;

namespace Crave.Ordering.Api.Data.Repositories
{
    /// <summary>
    /// Repository class for checkout methods.
    /// </summary>
    public class CheckoutMethodRepository : RepositoryBase<CheckoutMethodEntity>, ICheckoutMethodRepository
    {
        /// <inheritdoc/>
        public CheckoutMethodEntity GetCheckoutMethod(int checkoutMethodId)
        {
            return DataAccessAdapter.FetchEntityById<CheckoutMethodEntity>(checkoutMethodId, null, null);
        }
    }
}
