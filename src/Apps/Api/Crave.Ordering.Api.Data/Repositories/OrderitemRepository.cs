﻿using System.Collections.Generic;
using System.Linq;
using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.SQLServer;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Crave.Ordering.Api.Data.Repositories
{
    /// <summary>
    /// Repository class for order items.
    /// </summary>
    public class OrderitemRepository : RepositoryBase<OrderitemEntity>, IOrderitemRepository
    {
        /// <inheritdoc/>
        public EntityCollection<OrderitemEntity> GetOrderitems(IEnumerable<int> orderitemIds)
        {
            return DataAccessAdapter.FetchEntityCollection<OrderitemEntity>(new RelationPredicateBucket(OrderitemFields.OrderitemId == orderitemIds.ToArray()), 0, null, null, null);
        }

        /// <inheritdoc/>
        public OrderitemEntity SaveOrderitem(OrderitemEntity orderitemEntity)
        {
            DataAccessAdapter.Save(orderitemEntity, true);
            return orderitemEntity;
        }
    }
}
