﻿using System;
using System.Threading.Tasks;
using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.SQLServer;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Crave.Ordering.Api.Data.Repositories
{
    /// <summary>
    /// Repository class for clients.
    /// </summary>
    public class ClientRepository : RepositoryBase<ClientEntity>, IClientRepository
    {
        /// <inheritdoc/>
        public ClientEntity GetClientForDevice(int deviceId)
        {
            PrefetchPath2 prefetchPath = new PrefetchPath2(EntityType.ClientEntity);
            prefetchPath.Add(ClientEntity.PrefetchPathCompanyEntity, new IncludeFieldsList(CompanyFields.Salt, CompanyFields.CultureCode, CompanyFields.SaltPms));

            QueryParameters queryParameters = new QueryParameters
            {
                FilterToUse = new PredicateExpression(ClientFields.DeviceId == deviceId),
                PrefetchPathToUse = prefetchPath,
                CacheResultset = true,
                CacheTag = $"{nameof(this.GetClientForDevice)}-{deviceId}",
                CacheDuration = new TimeSpan(0, 5, 0)
            };

            return DataAccessAdapter.FetchEntity<ClientEntity>(queryParameters);
        }

        /// <inheritdoc/>
        public Task<ClientEntity> GetClientAsync(int clientId)
        {
            RelationPredicateBucket filterBucket = new RelationPredicateBucket();
            filterBucket.PredicateExpression.Add(ClientFields.ClientId == clientId);

            return DataAccessAdapter.FetchEntityAsync<ClientEntity>(filterBucket, null, null);
        }

        /// <inheritdoc/>
        public bool SaveClient(ClientEntity clientEntity)
        {
            return DataAccessAdapter.Save(clientEntity, true);
        }

        /// <inheritdoc/>
        public int GetCompanyIdForClientId(int clientId)
        {
            return this.FetchEntityById(clientId, null, new IncludeFieldsList(ClientFields.CompanyId)).CompanyId;
        }

        /// <inheritdoc/>
        public bool UpdateLoadedSuccessfully(int clientId, bool loadedSuccessfully)
        {
            ClientEntity clientEntity = new ClientEntity();
            clientEntity.LoadedSuccessfully = loadedSuccessfully;

            return this.UpdateEntitiesDirectly(clientEntity, new RelationPredicateBucket(ClientFields.ClientId == clientId)) > 0;
        }
    }
}
