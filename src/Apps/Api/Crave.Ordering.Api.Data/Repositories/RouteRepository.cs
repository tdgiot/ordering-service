﻿using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.SQLServer;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Crave.Ordering.Api.Data.Repositories
{
    /// <summary>
    /// Repository class for routes.
    /// </summary>
    public class RouteRepository : RepositoryBase<RouteEntity>, IRouteRepository
    {
        /// <inheritdoc/>
        public RouteEntity GetRoute(int routeId)
        {
            PrefetchPath2 prefetch = new PrefetchPath2(EntityType.RouteEntity);
            IPrefetchPathElement2 routestepPrefetch = prefetch.Add(RouteEntity.PrefetchPathRoutestepCollection);
            routestepPrefetch.SubPath.Add(RoutestepEntity.PrefetchPathRoutestephandlerCollection);

            return DataAccessAdapter.FetchEntityById<RouteEntity>(routeId, prefetch, null);
        }
    }
}
