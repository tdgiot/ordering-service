﻿using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.SQLServer;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Crave.Ordering.Api.Data.Repositories
{
    public class RoutestephandlerRepository : RepositoryBase<RoutestephandlerEntity>, IRoutestephandlerRepository
    {
        /// <inheritdoc />
        public EntityCollection<MediaEntity> GetMedia(int routestephandlerId)
        {
            RelationPredicateBucket bucket = new RelationPredicateBucket();
            bucket.PredicateExpression.Add(MediaFields.RoutestephandlerId == routestephandlerId);

            PrefetchPath2 prefetch = new PrefetchPath2(EntityType.MediaEntity);
            prefetch.Add(MediaEntity.PrefetchPathMediaRatioTypeMediaCollection);

            return DataAccessAdapter.FetchEntityCollection<MediaEntity>(bucket, 0, null, prefetch, null);
        }
    }
}