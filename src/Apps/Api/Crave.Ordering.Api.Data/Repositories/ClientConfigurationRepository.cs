﻿using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.SQLServer;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System.Threading.Tasks;

namespace Crave.Ordering.Api.Data.Repositories
{
    /// <summary>
    /// Repository class for client configurations.
    /// </summary>
    public class ClientConfigurationRepository : RepositoryBase<ClientConfigurationEntity>, IClientConfigurationRepository
    {
        /// <inheritdoc/>
        public Task<ClientConfigurationEntity> GetClientConfigurationAsync(int clientConfigurationId)
        {
            RelationPredicateBucket filterBucket = new RelationPredicateBucket();
            filterBucket.PredicateExpression.Add(ClientConfigurationFields.ClientConfigurationId == clientConfigurationId);

            // ClientConfiguration > ClientConfigurationRoute > Route
            PrefetchPath2 prefetch = new PrefetchPath2(EntityType.ClientConfigurationEntity);
            prefetch.Add(ClientConfigurationEntity.PrefetchPathClientConfigurationRouteCollection).SubPath.Add(ClientConfigurationRouteEntity.PrefetchPathRouteEntity);

            return DataAccessAdapter.FetchEntityAsync<ClientConfigurationEntity>(filterBucket, prefetch, null);
        }
    }
}
