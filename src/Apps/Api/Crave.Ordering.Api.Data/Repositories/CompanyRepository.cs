﻿using System.Threading.Tasks;
using Crave.Enums;
using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.SQLServer;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Crave.Ordering.Api.Data.Repositories
{
    public class CompanyRepository : RepositoryBase<CompanyEntity>, ICompanyRepository
    {
        public void SaveCompany(CompanyEntity entity)
        {
            DataAccessAdapter.Save(entity, true);
        }

        public ClockMode GetClockMode(int companyId)
        {
            return DataAccessAdapter.FetchEntityById<CompanyEntity>(companyId, null, new IncludeFieldsList(CompanyFields.ClockMode)).ClockMode;
        }

        public CompanyEntity GetCompanyEntity(int companyId)
        {
            return DataAccessAdapter.FetchEntityById<CompanyEntity>(companyId, null, new IncludeFieldsList(CompanyFields.Name, CompanyFields.CurrencyCode, CompanyFields.CultureCode, CompanyFields.CountryCode, CompanyFields.TimeZoneOlsonId, CompanyFields.ClockMode, CompanyFields.PricesIncludeTaxes));
        }

        public bool GetIncludeDeliveryTimeInOrderNotes(int companyId)
        {
            return DataAccessAdapter.FetchEntityById<CompanyEntity>(companyId, null, new IncludeFieldsList(CompanyFields.IncludeDeliveryTimeInOrderNotes)).IncludeDeliveryTimeInOrderNotes;
        }

        public async Task<CompanyEntity> GetCompanyLocationForOutletAsync(int outletId)
        {
            RelationPredicateBucket filterBucket = new RelationPredicateBucket();
            filterBucket.PredicateExpression.Add(OutletFields.OutletId == outletId);
            filterBucket.Relations.Add(OutletEntity.Relations.CompanyEntityUsingCompanyId);

            IncludeFieldsList includeFieldsList = new IncludeFieldsList
            {
                CompanyFields.Longitude,
                CompanyFields.Latitude,
                CompanyFields.Addressline1,
                CompanyFields.Zipcode,
                CompanyFields.City,
                CompanyFields.CountryCode
            };

            return await DataAccessAdapter.FetchEntityAsync<CompanyEntity>(filterBucket, null, includeFieldsList);
        }
    }
}