﻿using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Crave.Ordering.Api.Data.Repositories
{
    public class OutletSellerInformationRepository : RepositoryBase<OutletSellerInformationEntity>, IOutletSellerInformationRepository
    {
        public OutletSellerInformationEntity FetchByOutletId(int outletId)
        {
            RelationPredicateBucket relationPredicateBucket = new RelationPredicateBucket();
            relationPredicateBucket.PredicateExpression.Add(OutletFields.OutletId == outletId);
            relationPredicateBucket.Relations.Add(OutletEntity.Relations.OutletSellerInformationEntityUsingOutletSellerInformationId);

            return this.FetchEntity(relationPredicateBucket, null, null);
        }
    }
}