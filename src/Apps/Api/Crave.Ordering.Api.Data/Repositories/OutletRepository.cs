﻿using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Crave.Ordering.Api.Data.Repositories
{
    /// <summary>
    /// Repository class for outlets.
    /// </summary>
    public class OutletRepository : RepositoryBase<OutletEntity>, IOutletRepository
    {
        /// <inheritdoc/>
        public OutletEntity GetOutlet(int outletId)
        {
            PrefetchPath2 prefetch = new PrefetchPath2(EntityType.OutletEntity);
            prefetch.Add(OutletEntity.PrefetchPathCompanyEntity, new IncludeFieldsList(CompanyFields.TimeZoneOlsonId))
                    .SubPath.Add(CompanyEntity.PrefetchPathApplicationConfigurationCollection, new IncludeFieldsList(ApplicationConfigurationFields.ApplicationConfigurationId))
                    .SubPath.Add(ApplicationConfigurationEntity.PrefetchPathFeatureFlagCollection, new IncludeFieldsList(FeatureFlagFields.ApplessFeatureFlags));
            prefetch.Add(OutletEntity.PrefetchPathOutletOperationalStateEntity);
            prefetch.Add(OutletEntity.PrefetchPathBusinesshourCollection);

            return FetchEntityById(outletId, prefetch, null);
        }
    }
}
