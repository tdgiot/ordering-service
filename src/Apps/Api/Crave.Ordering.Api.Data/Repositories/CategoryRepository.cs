﻿using System.Collections.Generic;
using System.Linq;
using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.SQLServer;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Crave.Ordering.Api.Data.Repositories
{
    /// <summary>
    /// Repository class for categories.
    /// </summary>
    public class CategoryRepository : RepositoryBase<CategoryEntity>, ICategoryRepository
    {
        /// <inheritdoc/>
        public EntityCollection<CategoryEntity> GetCategories(IReadOnlyList<int> categoryIds)
        {
            if (!categoryIds.Any())
            {
                return new EntityCollection<CategoryEntity>();
            }

            IncludeFieldsList menuIncludeFieldsList = new IncludeFieldsList(MenuFields.Name);
            IncludeFieldsList parentCategoryIncludeFieldsList = new IncludeFieldsList(CategoryFields.Name);

            IPrefetchPath2 prefetch = new PrefetchPath2(EntityType.CategoryEntity);
            prefetch.Add(CategoryEntity.PrefetchPathMenuEntity, menuIncludeFieldsList);
            prefetch.Add(CategoryEntity.PrefetchPathParentCategoryEntity, parentCategoryIncludeFieldsList)
                    .SubPath.Add(CategoryEntity.PrefetchPathParentCategoryEntity, parentCategoryIncludeFieldsList)
                    .SubPath.Add(CategoryEntity.PrefetchPathParentCategoryEntity, parentCategoryIncludeFieldsList)
                    .SubPath.Add(CategoryEntity.PrefetchPathParentCategoryEntity, parentCategoryIncludeFieldsList);

            return FetchEntityCollection(new RelationPredicateBucket(CategoryFields.CategoryId == categoryIds.ToList()), 0, null, prefetch, null);
        }

        /// <inheritdoc/>
        public CategoryEntity GetCategory(int categoryId)
        {
            return DataAccessAdapter.FetchEntityById<CategoryEntity>(categoryId, null, null);
        }

        /// <inheritdoc />
        public CategoryEntity GetCategoryWithParentRoutes(int categoryId)
        {
            IncludeFieldsList includeFieldsList = new IncludeFieldsList(CategoryFields.RouteId, CategoryFields.ParentCategoryId);

            PrefetchPath2 prefetch = new PrefetchPath2(EntityType.CategoryEntity);
            prefetch.Add(CategoryEntity.PrefetchPathParentCategoryEntity, includeFieldsList)
                    .SubPath.Add(CategoryEntity.PrefetchPathParentCategoryEntity, includeFieldsList)
                    .SubPath.Add(CategoryEntity.PrefetchPathParentCategoryEntity, includeFieldsList)
                    .SubPath.Add(CategoryEntity.PrefetchPathParentCategoryEntity, includeFieldsList)
                    .SubPath.Add(CategoryEntity.PrefetchPathParentCategoryEntity, includeFieldsList);

            return DataAccessAdapter.FetchEntityById<CategoryEntity>(categoryId, prefetch, includeFieldsList);
        }
    }
}
