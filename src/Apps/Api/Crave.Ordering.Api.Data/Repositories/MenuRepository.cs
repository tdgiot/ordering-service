﻿using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.Extensions;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.SQLServer;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Crave.Ordering.Api.Data.Repositories
{
    public class MenuRepository : RepositoryBase<MenuEntity>, IMenuRepository
    {
        /// <inheritdoc />
        public string GetMenuName(int menuId)
        {
            QueryParameters param = new QueryParameters
            {
                FilterToUse = MenuFields.MenuId == menuId,
                ExcludedIncludedFields = new IncludeFieldsList(MenuFields.Name)
            };

            MenuEntity entity = DataAccessAdapter.FetchEntity<MenuEntity>(param);

            return entity.IsNullOrNew() ? string.Empty : entity.Name;
        }
    }
}