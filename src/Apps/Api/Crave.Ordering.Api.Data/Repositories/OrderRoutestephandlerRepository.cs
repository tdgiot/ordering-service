﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Crave.Enums;
using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.FactoryClasses;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.SQLServer;
using SD.LLBLGen.Pro.ORMSupportClasses;
using SD.LLBLGen.Pro.QuerySpec;
using SD.LLBLGen.Pro.QuerySpec.Adapter;

namespace Crave.Ordering.Api.Data.Repositories
{
    /// <summary>
    /// Repository class for order routestep handlers.
    /// </summary>
    public class OrderRoutestephandlerRepository : RepositoryBase<OrderRoutestephandlerEntity>, IOrderRoutestephandlerRepository
    {
        /// <inheritdoc/>
        public void SaveOrderRoutestephandler(OrderRoutestephandlerEntity orderRoutestephandlerEntity)
        {
            DataAccessAdapter.Save(orderRoutestephandlerEntity, true, orderRoutestephandlerEntity.Adapter);
        }

        /// <inheritdoc/>
        public void DeleteOrderRoutestephandlers(EntityCollection<OrderRoutestephandlerEntity> orderRoutestephandlerEntities)
        {
            DataAccessAdapter.Delete(orderRoutestephandlerEntities);
        }

        /// <inheritdoc/>
        public EntityCollection<OrderRoutestephandlerEntity> GetStepsOfRoute(OrderRoutestephandlerEntity orderRoutestephandlerEntity)
        {
            RelationPredicateBucket filterBucket = new RelationPredicateBucket();

            // For this order 
            filterBucket.PredicateExpression.Add(OrderRoutestephandlerFields.OrderId == orderRoutestephandlerEntity.OrderId);

            // Determine filter of relevant steps to update                
            if ((orderRoutestephandlerEntity.Status == OrderRoutestephandlerStatus.Failed && !orderRoutestephandlerEntity.ContinueOnFailure) ||
                (orderRoutestephandlerEntity.Status == OrderRoutestephandlerStatus.Completed && orderRoutestephandlerEntity.ErrorCode == OrderErrorCode.ManuallyProcessed))
            {
                // Failed, so cancel all paralele and future steps
                // Or manually processed because the current step failed, so parallel and future steps needs to be cancelled to
                filterBucket.PredicateExpression.Add(OrderRoutestephandlerFields.Number == orderRoutestephandlerEntity.Number | OrderRoutestephandlerFields.Number > orderRoutestephandlerEntity.Number);
            }
            else if (orderRoutestephandlerEntity.Status == OrderRoutestephandlerStatus.Completed && orderRoutestephandlerEntity.CompleteRouteOnComplete)
            {
                // Completed AND CompleteRouteOnComplete, the this route step being completed means we can regard the whole route as 
                // being completed, so complete all paralele and future steps
                filterBucket.PredicateExpression.Add(OrderRoutestephandlerFields.Number == orderRoutestephandlerEntity.Number | OrderRoutestephandlerFields.Number > orderRoutestephandlerEntity.Number);
            }
            else
            {
                // Completed OR Failed but step is set to 'ContinueOnFailure' (ContinueOnFailure means, ignore failure, just behave like it is completed)
                // Update only at current step Number (parallel) or the next step
                filterBucket.PredicateExpression.Add(OrderRoutestephandlerFields.Number == orderRoutestephandlerEntity.Number | OrderRoutestephandlerFields.Number == (orderRoutestephandlerEntity.Number + 1));
            }

            // Don't load self
            filterBucket.PredicateExpression.Add(OrderRoutestephandlerFields.OrderRoutestephandlerId != orderRoutestephandlerEntity.OrderRoutestephandlerId);

            SortExpression sort = new SortExpression();
            sort.Add(OrderRoutestephandlerFields.Number | SortOperator.Ascending);

            return DataAccessAdapter.FetchEntityCollection<OrderRoutestephandlerEntity>(filterBucket, 0, sort, null, null, orderRoutestephandlerEntity.Adapter);
        }

        /// <inheritdoc/>
        public EntityCollection<OrderRoutestephandlerEntity> GetAllStepsOfRoute(int orderId)
        {
            return DataAccessAdapter.FetchEntityCollection<OrderRoutestephandlerEntity>(new RelationPredicateBucket(OrderRoutestephandlerFields.OrderId == orderId), 0, null, null, null);
        }
        /// <inheritdoc/>
        public EntityCollection<OrderRoutestephandlerEntity> GetSpecificStepsOfRoute(int orderId, int stepNumber)
        {
            RelationPredicateBucket bucket = new RelationPredicateBucket();
            bucket.PredicateExpression.Add(OrderRoutestephandlerFields.OrderId == orderId);
            bucket.PredicateExpression.AddWithAnd(OrderRoutestephandlerFields.Number == stepNumber);

            return DataAccessAdapter.FetchEntityCollection<OrderRoutestephandlerEntity>(bucket, 0, null, null, null);
        }

        /// <inheritdoc />
        public OrderRoutestephandlerEntity GetOrderRoutestephandlerEntity(int orderRoutestephandlerId, IDataAccessAdapter adapter)
        {
            PrefetchPath2 prefetch = new PrefetchPath2(EntityType.OrderRoutestephandlerEntity);
            prefetch.Add(OrderRoutestephandlerEntity.PrefetchPathSupportpoolEntity);
            prefetch.Add(OrderRoutestephandlerEntity.PrefetchPathOrderEntity);

            return DataAccessAdapter.FetchEntityById<OrderRoutestephandlerEntity>(orderRoutestephandlerId, prefetch, null, adapter);
        }

        /// <inheritdoc />
        public async Task<OrderRoutestephandlerEntity> GetByIdAsync(int orderRoutestephandlerId)
        {
            EntityQuery<OrderRoutestephandlerEntity> entityQuery = new QueryFactory().OrderRoutestephandler
                .Where(OrderRoutestephandlerFields.OrderRoutestephandlerId == orderRoutestephandlerId);

            using DataAccessAdapter adapter = new DataAccessAdapter();
            return await adapter.FetchFirstAsync(entityQuery);
        }

        /// <inheritdoc/>
        public ICollection<OrderRoutestephandlerEntity> GetOrderRoutestephandlers(List<OrderRoutestephandlerStatus> orderRoutestephandlerStatuses, int terminalId)
        {
            RelationPredicateBucket filterBucket = new RelationPredicateBucket();
            filterBucket.PredicateExpression.Add(OrderRoutestephandlerFields.Status == orderRoutestephandlerStatuses);
            filterBucket.PredicateExpression.Add(OrderRoutestephandlerFields.TerminalId == terminalId);

            // Get the order route step handlers
            EntityCollection<OrderRoutestephandlerEntity> orderRoutestephandlerCollection = this.FetchEntityCollection(filterBucket, 0, null, null, null);

            return orderRoutestephandlerCollection;
        }

        /// <inheritdoc/>
        public EntityCollection<OrderRoutestephandlerEntity> GetExpiredOrdersForTerminal(int? terminalId, ICollection<int> orderIds)
        {
            // Filter for all expired orders that haven't been completed/failed yet.
            RelationPredicateBucket filterBucket = new RelationPredicateBucket();

            if (terminalId.HasValue)
            {
                filterBucket.PredicateExpression.Add(OrderRoutestephandlerFields.TerminalId == terminalId);
            }

            if (!orderIds.IsNullOrEmpty())
            {
                filterBucket.PredicateExpression.Add(OrderRoutestephandlerFields.OrderId == orderIds);
            }

            filterBucket.PredicateExpression.Add(OrderRoutestephandlerFields.TimeoutExpiresUTC != DBNull.Value);
            filterBucket.PredicateExpression.Add(OrderRoutestephandlerFields.TimeoutExpiresUTC < DateTime.UtcNow);
            filterBucket.PredicateExpression.Add(OrderRoutestephandlerFields.Status != OrderRoutestephandlerStatus.Completed);
            filterBucket.PredicateExpression.Add(OrderRoutestephandlerFields.Status != OrderRoutestephandlerStatus.CompletedByOtherStep);
            filterBucket.PredicateExpression.Add(OrderRoutestephandlerFields.Status != OrderRoutestephandlerStatus.Failed);
            filterBucket.PredicateExpression.Add(OrderRoutestephandlerFields.Status != OrderRoutestephandlerStatus.CancelledDueToOtherStepFailure);

            // Exception (yes, I hate them too...) 
            // On-the-case console's won't time-out
            PredicateExpression filterExcludeOnTheCaseConsoles = new PredicateExpression();
            filterExcludeOnTheCaseConsoles.Add(OrderRoutestephandlerFields.Status == OrderRoutestephandlerStatus.BeingHandled);
            filterExcludeOnTheCaseConsoles.Add(OrderRoutestephandlerFields.HandlerType == RoutestephandlerType.Console);
            filterExcludeOnTheCaseConsoles.Negate = true;
            filterBucket.PredicateExpression.Add(filterExcludeOnTheCaseConsoles);

            return this.FetchEntityCollection(filterBucket, 0, null, null, null);
        }

        /// <inheritdoc/>
        public async Task<OrderRoutestephandlerEntity> GetUpdatableTerminalRoutestephandlerAsync(int orderRoutestephandlerId)
        {
            RelationPredicateBucket bucket = new RelationPredicateBucket(OrderRoutestephandlerFields.OrderRoutestephandlerId == orderRoutestephandlerId);

            // Don't load unnecessary data
            ExcludeFieldsList excludedFields = new ExcludeFieldsList();
            excludedFields.Add(OrderRoutestephandlerFields.FieldValue1);
            excludedFields.Add(OrderRoutestephandlerFields.FieldValue2);
            excludedFields.Add(OrderRoutestephandlerFields.FieldValue3);
            excludedFields.Add(OrderRoutestephandlerFields.FieldValue4);
            excludedFields.Add(OrderRoutestephandlerFields.FieldValue5);
            excludedFields.Add(OrderRoutestephandlerFields.FieldValue6);
            excludedFields.Add(OrderRoutestephandlerFields.FieldValue7);
            excludedFields.Add(OrderRoutestephandlerFields.FieldValue8);
            excludedFields.Add(OrderRoutestephandlerFields.FieldValue9);
            excludedFields.Add(OrderRoutestephandlerFields.FieldValue9);

            return await this.FetchEntityAsync(bucket, null, excludedFields);
        }
    }
}
