﻿using System;
using System.Threading.Tasks;
using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.FactoryClasses;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.SQLServer;
using SD.LLBLGen.Pro.ORMSupportClasses;
using SD.LLBLGen.Pro.QuerySpec;
using SD.LLBLGen.Pro.QuerySpec.Adapter;

namespace Crave.Ordering.Api.Data.Repositories
{
    /// <summary>
    /// Repository class for terminals.
    /// </summary>
    public class TerminalRepository : RepositoryBase<TerminalEntity>, ITerminalRepository
    {
        /// <inheritdoc/>
        public TerminalEntity GetTerminalForDevice(int deviceId)
        {
            PrefetchPath2 prefetchPath = new PrefetchPath2(EntityType.TerminalEntity);
            prefetchPath.Add(TerminalEntity.PrefetchPathCompanyEntity, new IncludeFieldsList(CompanyFields.Salt, CompanyFields.CultureCode, CompanyFields.SaltPms));

            QueryParameters queryParameters = new QueryParameters
            {
                FilterToUse = new PredicateExpression(TerminalFields.DeviceId == deviceId),
                PrefetchPathToUse = prefetchPath,
                CacheResultset = true,
                CacheTag = $"{nameof(this.GetTerminalForDevice)}-{deviceId}",
                CacheDuration = new TimeSpan(0, 5, 0)
            };

            return DataAccessAdapter.FetchEntity<TerminalEntity>(queryParameters);
        }

        /// <inheritdoc/>
        public TerminalEntity GetTerminal(int terminalId)
        {
            return DataAccessAdapter.FetchEntityById<TerminalEntity>(terminalId, null, null);
        }

        /// <inheritdoc />
        public async Task<TerminalEntity> GetByIdAsync(int terminalId)
        {
            EntityQuery<TerminalEntity> entityQuery = new QueryFactory().Terminal
                .Where(TerminalFields.TerminalId == terminalId);

            using DataAccessAdapter adapter = new DataAccessAdapter();
            return await adapter.FetchFirstAsync(entityQuery);
        }

        /// <inheritdoc/>
        public void SaveTerminal(TerminalEntity terminalEntity)
        {
            DataAccessAdapter.Save(terminalEntity, false);
        }

        /// <inheritdoc/>
        public int GetCompanyIdForTerminalId(int terminalId)
        {
            return FetchEntityById(terminalId, null, new IncludeFieldsList(TerminalFields.CompanyId)).CompanyId;
        }

        /// <inheritdoc/>
        public bool UpdateLoadedSuccessfully(int terminalId, bool loadedSuccessfully)
        {
            TerminalEntity terminalEntity = new TerminalEntity();
            terminalEntity.LoadedSuccessfully = loadedSuccessfully;

            return this.UpdateEntitiesDirectly(terminalEntity, new RelationPredicateBucket(TerminalFields.TerminalId == terminalId)) > 0;
        }

        /// <inheritdoc/>
        public bool UpdateTerminalForward(int terminalId, int forwardToTerminalId)
        {
            TerminalEntity terminalEntity = new TerminalEntity();
            terminalEntity.TerminalId = terminalId;
            terminalEntity.IsNew = false;

            if (forwardToTerminalId > 0)
            {
                terminalEntity.ForwardToTerminalId = forwardToTerminalId;
            }
            else
            {
                terminalEntity.ForwardToTerminalId = null;
            }
            terminalEntity.Fields[TerminalFields.ForwardToTerminalId.Name].IsChanged = true;

            return Save(terminalEntity, false);
        }
    }
}
