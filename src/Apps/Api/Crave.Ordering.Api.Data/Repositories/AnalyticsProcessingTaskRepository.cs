﻿using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.SQLServer;

namespace Crave.Ordering.Api.Data.Repositories
{
    /// <summary>
    /// Repository class for analytics processing tasks.
    /// </summary>
    public class AnalyticsProcessingTaskRepository : RepositoryBase<AnalyticsProcessingTaskEntity>, IAnalyticsProcessingTaskRepository
    {
        /// <inheritdoc/>
        public bool SaveAnalyticsProcessingTask(AnalyticsProcessingTaskEntity analyticsProcessingTaskEntity)
        {
            return DataAccessAdapter.Save(analyticsProcessingTaskEntity, false);
        }
    }
}
