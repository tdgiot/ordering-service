﻿using System.Collections.Generic;
using System.Linq;
using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.SQLServer;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Crave.Ordering.Api.Data.Repositories
{
    /// <summary>
    /// Repository class for categories.
    /// </summary>
    public class AlterationRepository : RepositoryBase<AlterationEntity>, IAlterationRepository
    {
        /// <inheritdoc/>
        public EntityCollection<AlterationEntity> GetAlterations(IReadOnlyList<int> alterationIds)
        {
            if (!alterationIds.Any())
            {
                return new EntityCollection<AlterationEntity>();
            }

            RelationPredicateBucket filterBucket = new RelationPredicateBucket(AlterationFields.AlterationId == alterationIds.ToList());
            IncludeFieldsList includeFieldsList = new IncludeFieldsList(AlterationFields.Name, AlterationFields.Type, AlterationFields.OrderLevelEnabled);

            return FetchEntityCollection(filterBucket, 0, null, null, includeFieldsList);
        }

        /// <inheritdoc/>
        public AlterationEntity GetAlterationForAlterationitem(int alterationitemId)
        {
            RelationPredicateBucket filterBucket = new RelationPredicateBucket();
            filterBucket.PredicateExpression.Add(AlterationitemFields.AlterationitemId == alterationitemId);
            filterBucket.Relations.Add(AlterationitemEntity.Relations.AlterationEntityUsingAlterationId);

            IncludeFieldsList includedFields = new IncludeFieldsList
            {
                AlterationFields.AlterationId,
                AlterationFields.Type,
                AlterationFields.Name,
                AlterationFields.OrderLevelEnabled
            };

            return FetchEntity(filterBucket, null, includedFields);
        }

        /// <inheritdoc />
        public AlterationoptionEntity GetAlterationoptionForAlterationitem(int alterationitemId)
        {
            PredicateExpression filter = new PredicateExpression(AlterationitemFields.AlterationitemId == alterationitemId);
            RelationCollection relations = new RelationCollection(AlterationitemEntity.Relations.AlterationoptionEntityUsingAlterationoptionId);

            IncludeFieldsList includeFields = new IncludeFieldsList
            {
                AlterationoptionFields.Name,
                AlterationoptionFields.Type,
                AlterationoptionFields.PriceIn
            };

            QueryParameters queryParameters = new QueryParameters
            {
                FilterToUse = filter,
                RelationsToUse = relations,
                ExcludedIncludedFields = includeFields
            };

            return DataAccessAdapter.FetchEntity<AlterationoptionEntity>(queryParameters);
        }

        /// <inheritdoc/>
        public AlterationEntity GetAlteration(int alterationId)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(AlterationFields.AlterationId == alterationId);

            QueryParameters queryParameters = new QueryParameters
            {
                FilterToUse = filter,
                ExcludedIncludedFields = new IncludeFieldsList(AlterationFields.Name, AlterationFields.Type, AlterationFields.OrderLevelEnabled)
            };

            return this.FetchEntity(queryParameters);
        }
    }
}
