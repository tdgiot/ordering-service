﻿using System.Collections.Generic;
using System.Linq;
using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Crave.Ordering.Api.Data.Repositories
{
    /// <summary>
    /// Repository class for tax tariff.
    /// </summary>
    public class TaxTariffRepository : RepositoryBase<TaxTariffEntity>, ITaxTariffRepository
    {
        /// <inheritdoc/>
        public EntityCollection<TaxTariffEntity> GetTaxTariffs(IReadOnlyList<int> taxTariffIds)
        {
            if (!taxTariffIds.Any())
            {
                return new EntityCollection<TaxTariffEntity>();
            }

            IncludeFieldsList includeFieldsList = new IncludeFieldsList(
                TaxTariffFields.Name,
                TaxTariffFields.Percentage
            );

            return FetchEntityCollection(new RelationPredicateBucket(TaxTariffFields.TaxTariffId == taxTariffIds.ToList()), 0, null, null, includeFieldsList);
        }

        /// <inheritdoc/>
        public TaxTariffEntity GetTaxTariff(int taxTariffId)
        {
            return this.FetchEntityById(taxTariffId, null, new IncludeFieldsList(TaxTariffFields.Percentage));
        }
    }
}
