﻿using System;
using System.Collections.Generic;
using System.Linq;
using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Crave.Ordering.Api.Data.Repositories
{
    /// <summary>
    /// Repository class for alteration items.
    /// </summary>
    public class AlterationitemRepository : RepositoryBase<AlterationitemEntity>, IAlterationitemRepository
    {
        /// <inheritdoc/>
        public EntityCollection<AlterationitemEntity> GetAlterationitems(IReadOnlyList<Tuple<int, int>> alterationitems)
        {
            RelationPredicateBucket filterBucket = new RelationPredicateBucket();

            if (!alterationitems.Any())
            {
                return new EntityCollection<AlterationitemEntity>();
            }

            foreach (Tuple<int, int> alterationitem in alterationitems)
            {
                PredicateExpression alterationitemFilter = new PredicateExpression();
                alterationitemFilter.Add(AlterationitemFields.AlterationId == alterationitem.Item1);
                alterationitemFilter.Add(AlterationitemFields.AlterationoptionId == alterationitem.Item2);

                filterBucket.PredicateExpression.AddWithOr(alterationitemFilter);
            }

            PrefetchPath2 prefetchPath = new PrefetchPath2(EntityType.AlterationitemEntity);
            prefetchPath.Add(AlterationitemEntity.PrefetchPathAlterationoptionEntity, new IncludeFieldsList(AlterationoptionFields.Name, AlterationoptionFields.Type, AlterationoptionFields.PriceIn, AlterationoptionFields.ExternalIdentifier));

            IncludeFieldsList includeFieldsList = new IncludeFieldsList(AlterationitemFields.AlterationitemId);

            return FetchEntityCollection(filterBucket, 0, null, prefetchPath, includeFieldsList);
        }

        public AlterationitemEntity GetAlterationitem(int alterationitemId)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(AlterationitemFields.AlterationitemId == alterationitemId);

            PrefetchPath2 prefetchPath = new PrefetchPath2(EntityType.AlterationitemEntity);
            prefetchPath.Add(AlterationitemEntity.PrefetchPathAlterationEntity, new IncludeFieldsList(AlterationFields.Name, AlterationFields.Type));
            prefetchPath.Add(AlterationitemEntity.PrefetchPathAlterationoptionEntity, new IncludeFieldsList(AlterationoptionFields.Name, AlterationoptionFields.PriceIn));

            QueryParameters queryParameters = new QueryParameters
            {
                FilterToUse = filter,
                PrefetchPathToUse = prefetchPath,
                ExcludedIncludedFields = new IncludeFieldsList(AlterationitemFields.AlterationitemId)
            };

            return this.FetchEntity(queryParameters);
        }

        public AlterationitemEntity GetAlterationitem(int alterationId, int alterationoptionId)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(AlterationitemFields.AlterationId == alterationId);
            filter.Add(AlterationitemFields.AlterationoptionId == alterationoptionId);

            PrefetchPath2 prefetchPath = new PrefetchPath2(EntityType.AlterationitemEntity);
            prefetchPath.Add(AlterationitemEntity.PrefetchPathAlterationEntity, new IncludeFieldsList(AlterationFields.Name, AlterationFields.Type));
            prefetchPath.Add(AlterationitemEntity.PrefetchPathAlterationoptionEntity, new IncludeFieldsList(AlterationoptionFields.Name, AlterationoptionFields.Type, AlterationoptionFields.PriceIn));

            QueryParameters queryParameters = new QueryParameters
            {
                FilterToUse = filter,
                PrefetchPathToUse = prefetchPath,
                ExcludedIncludedFields = new IncludeFieldsList(AlterationitemFields.AlterationitemId)
            };

            return this.FetchEntity(queryParameters);
        }
    }
}
