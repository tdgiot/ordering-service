﻿using System.Collections.Generic;
using System.Linq;
using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using SD.LLBLGen.Pro.QuerySpec;

namespace Crave.Ordering.Api.Data.Repositories
{
    public class AlterationoptionTagRepository : RepositoryBase<AlterationoptionTagEntity>, IAlterationoptionTagRepository
    {
        public EntityCollection<AlterationoptionTagEntity> GetAlterationoptionTags(IReadOnlyList<int> alterationoptionIds)
        {
            if (!alterationoptionIds.Any())
            {
                return new EntityCollection<AlterationoptionTagEntity>();
            }

            PredicateExpression filter = new PredicateExpression();
            filter.Add(AlterationoptionTagFields.AlterationoptionId.In(alterationoptionIds));

            return FetchEntityCollection(filter);
        }
    }
}
