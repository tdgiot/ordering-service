﻿using System.Collections.Generic;
using System.Linq;
using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using SD.LLBLGen.Pro.QuerySpec;

namespace Crave.Ordering.Api.Data.Repositories
{
    /// <summary>
    /// Repository class for product category tags.
    /// </summary>
    public class ProductCategoryTagRepository : RepositoryBase<ProductCategoryTagEntity>, IProductCategoryTagRepository
    {
        public EntityCollection<ProductCategoryTagEntity> GetProductCategoryTags(IReadOnlyList<int> productIds, IReadOnlyList<int> categoryIds)
        {
            if (!productIds.Any())
            {
                return new EntityCollection<ProductCategoryTagEntity>();
            }

            PredicateExpression filter = new PredicateExpression();
            filter.Add(ProductCategoryFields.ProductId.In(productIds));
            filter.Add(ProductCategoryFields.CategoryId.In(categoryIds));

            RelationCollection relations = new RelationCollection();
            relations.Add(ProductCategoryTagEntity.Relations.ProductCategoryEntityUsingProductCategoryId);

            return FetchEntityCollection(filter, relations);
        }
    }
}
