﻿using System;
using System.Collections.Generic;
using System.Linq;
using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.SQLServer;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Crave.Ordering.Api.Data.Repositories
{
    /// <summary>
    /// Repository class for products.
    /// </summary>
    public class ProductRepository : RepositoryBase<ProductEntity>, IProductRepository
    {
        private const int ALTERATION_VERSION = 2;

        /// <inheritdoc/>
        public EntityCollection<ProductEntity> GetProducts(IReadOnlyList<int> productIds)
        {
            if (!productIds.Any())
            {
                return new EntityCollection<ProductEntity>();
            }

            return DataAccessAdapter.FetchEntityCollection<ProductEntity>(new RelationPredicateBucket(ProductFields.ProductId == productIds.ToList()), 0, null, null, null);
        }

        /// <inheritdoc/>
        public EntityCollection<ProductEntity> GetProductsWithAlterationsPrefetch(IReadOnlyList<int> productIds)
        {
            if (!productIds.Any())
            {
                return new EntityCollection<ProductEntity>();
            }

            RelationPredicateBucket bucket = new RelationPredicateBucket(ProductFields.ProductId == productIds.ToList());

            PredicateExpression alterationFilter = new PredicateExpression();
            alterationFilter.Add(AlterationFields.Version == ALTERATION_VERSION);
            alterationFilter.Add(AlterationFields.Visible == true);
            alterationFilter.Add(AlterationFields.IsAvailable == true);

            PrefetchPath2 prefetch = new PrefetchPath2(EntityType.ProductEntity);
            prefetch.Add(ProductEntity.PrefetchPathProductAlterationCollection)
                    .SubPath.Add(ProductAlterationEntity.PrefetchPathAlterationEntity, 0, alterationFilter);

            return DataAccessAdapter.FetchEntityCollection<ProductEntity>(bucket, 0, null, prefetch, null);
        }

        /// <inheritdoc/>
        public ProductEntity GetProduct(int productId)
        {
            return DataAccessAdapter.FetchEntityById<ProductEntity>(productId, null, null);
        }
    }
}
