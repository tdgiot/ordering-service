﻿using System.Collections.Generic;
using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.Extensions;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.SQLServer;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Crave.Ordering.Api.Data.Repositories
{
    /// <summary>
    /// Repository class for delivery point groups.
    /// </summary>
    public class DeliverypointgroupRepository : RepositoryBase<DeliverypointgroupEntity>, IDeliverypointgroupRepository
    {
        /// <inheritdoc/>
        public DeliverypointgroupEntity GetDeliverypointgroup(int deliverypointgroupId)
        {
            return DataAccessAdapter.FetchEntityById<DeliverypointgroupEntity>(deliverypointgroupId, null, null);
        }

        /// <inheritdoc/>
        public int? GetEstimatedDeliveryTimeForDeliverypoint(int deliverypointId)
        {
            int? estimatedDeliveryTime = null;

            RelationPredicateBucket filterBucket = new RelationPredicateBucket();
            filterBucket.PredicateExpression.Add(DeliverypointFields.DeliverypointId == deliverypointId);
            filterBucket.Relations.Add(DeliverypointEntity.Relations.DeliverypointgroupEntityUsingDeliverypointgroupId);

            DeliverypointgroupEntity deliverypointgroupEntity = DataAccessAdapter.FetchEntity<DeliverypointgroupEntity>(filterBucket, null, new IncludeFieldsList(DeliverypointgroupFields.EstimatedDeliveryTime));
            if (!deliverypointgroupEntity.IsNullOrNew())
            {
                estimatedDeliveryTime = deliverypointgroupEntity.EstimatedDeliveryTime;
            }

            return estimatedDeliveryTime;
        }

        /// <inheritdoc />
        public IEnumerable<DeliverypointgroupEntity> GetDeliverypointgroupsForCompany(int companyId)
        {
            RelationPredicateBucket filterBucket = new RelationPredicateBucket();
            filterBucket.PredicateExpression.Add(DeliverypointgroupFields.CompanyId == companyId);

            return DataAccessAdapter.FetchEntityCollection<DeliverypointgroupEntity>(filterBucket, 0, null, null, null);
        }

        /// <inheritdoc />
        public bool Save(DeliverypointgroupEntity entity)
        {
            return Save(entity, false);
        }
    }
}
