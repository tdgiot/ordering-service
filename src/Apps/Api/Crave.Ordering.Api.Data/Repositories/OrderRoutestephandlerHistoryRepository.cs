﻿using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.SQLServer;

namespace Crave.Ordering.Api.Data.Repositories
{
    public class OrderRoutestephandlerHistoryRepository : RepositoryBase<OrderRoutestephandlerHistoryEntity>, IOrderRoutestephandlerHistoryRepository
    {
        public void SaveOrderRoutestephandlerHistory(OrderRoutestephandlerHistoryEntity orderRoutestephandlerHistory)
        {
            DataAccessAdapter.Save(orderRoutestephandlerHistory, true);
        }

        public bool SaveOrderRoutestephandlerHistory(EntityCollection<OrderRoutestephandlerHistoryEntity> orderRoutestephandlerHistoryEntities)
        {
            return DataAccessAdapter.Save(orderRoutestephandlerHistoryEntities, false, false) > 0;
        }
    }
}