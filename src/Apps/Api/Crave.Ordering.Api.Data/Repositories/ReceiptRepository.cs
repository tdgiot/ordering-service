﻿using System;
using System.Data;
using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.Extensions;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.SQLServer;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Crave.Ordering.Api.Data.Repositories
{
    /// <summary>
    /// Repository class for receipts.
    /// </summary>
    public class ReceiptRepository : RepositoryBase<ReceiptEntity>, IReceiptRepository
    {
        /// <inheritdoc/>
        public ReceiptEntity CreateReceipt(ReceiptEntity receiptEntity)
        {
            if (!receiptEntity.IsNullOrNew())
            {
                throw new ArgumentNullException(nameof(receiptEntity));
            }

            using (IDataAccessAdapter adapter = new DataAccessAdapter())
            {
                adapter.StartTransaction(IsolationLevel.ReadCommitted, "CreateReceipt");

                receiptEntity.CreatedUTC = DateTime.UtcNow;
                receiptEntity.Number = GetNewNumber(adapter, receiptEntity);

                adapter.SaveEntity(receiptEntity, true);
                adapter.Commit();
            }

            return receiptEntity;
        }

        /// <inheritdoc/>
        public ReceiptEntity GetReceiptForOrder(int orderId)
        {
            RelationPredicateBucket filterBucket = new RelationPredicateBucket();
            filterBucket.PredicateExpression.Add(ReceiptFields.OrderId == orderId);

            return DataAccessAdapter.FetchEntity<ReceiptEntity>(filterBucket, null, null);
        }

        private static int GetNewNumber(IDataAccessAdapter adapter, ReceiptEntity receiptEntity)
        {
            object number = GetLatestReceiptNumberBasedOnOutletSellerInformationId(adapter, receiptEntity.OutletSellerInformationId.GetValueOrDefault(0));

            if (number == DBNull.Value)
            {
                number = GetLatestReceiptNumberBasedOnCompanyId(adapter, receiptEntity.CompanyId);
            }

            return number == DBNull.Value ? 1 : (int)number + 1;
        }

        private static object GetLatestReceiptNumberBasedOnOutletSellerInformationId(IDataAccessAdapter adapter, int outletSellerInformationId)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(ReceiptFields.OutletSellerInformationId == outletSellerInformationId);

            return adapter.GetScalar(ReceiptFields.Number, null, AggregateFunction.Max, filter, null);
        }

        private static object GetLatestReceiptNumberBasedOnCompanyId(IDataAccessAdapter adapter, int companyId)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(ReceiptFields.CompanyId == companyId);

            return adapter.GetScalar(ReceiptFields.Number, null, AggregateFunction.Max, filter, null);
        }
    }
}
