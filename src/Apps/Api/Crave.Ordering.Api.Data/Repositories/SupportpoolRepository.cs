﻿using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Crave.Ordering.Api.Data.Repositories
{
    /// <summary>
    /// Repository class for supportpools.
    /// </summary>
    public class SupportpoolRepository : RepositoryBase<SupportpoolEntity>, ISupportpoolRepository
    {
        /// <inheritdoc/>
        public SupportpoolEntity GetSupportpoolEmailAddresses(int supportpoolId)
        {
            PrefetchPath2 prefetchPath = new PrefetchPath2(EntityType.SupportpoolEntity);
            prefetchPath.Add(SupportpoolEntity.PrefetchPathSupportpoolSupportagentCollection).SubPath.Add(SupportpoolSupportagentEntity.PrefetchPathSupportagentEntity, new IncludeFieldsList(SupportagentFields.Email));
            prefetchPath.Add(SupportpoolEntity.PrefetchPathSupportpoolNotificationRecipientCollection, new IncludeFieldsList(SupportpoolNotificationRecipientFields.Email));

            return this.FetchEntityById(supportpoolId, prefetchPath, new IncludeFieldsList(SupportpoolFields.Email));
        }

        /// <inheritdoc/>
        public SupportpoolEntity GetSupportpoolPhonenumbers(int supportpoolId)
        {
            PrefetchPath2 prefetchPath = new PrefetchPath2(EntityType.SupportpoolEntity);
            prefetchPath.Add(SupportpoolEntity.PrefetchPathSupportpoolSupportagentCollection).SubPath.Add(SupportpoolSupportagentEntity.PrefetchPathSupportagentEntity, new IncludeFieldsList(SupportagentFields.Phonenumber));
            prefetchPath.Add(SupportpoolEntity.PrefetchPathSupportpoolNotificationRecipientCollection, new IncludeFieldsList(SupportpoolNotificationRecipientFields.Phonenumber));

            return this.FetchEntityById(supportpoolId, prefetchPath, new IncludeFieldsList(SupportpoolFields.Phonenumber));
        }
    }
}
