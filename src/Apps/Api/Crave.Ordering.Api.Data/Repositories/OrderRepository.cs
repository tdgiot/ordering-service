﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Crave.Enums;
using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.FactoryClasses;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.SQLServer;
using SD.LLBLGen.Pro.ORMSupportClasses;
using SD.LLBLGen.Pro.QuerySpec;
using SD.LLBLGen.Pro.QuerySpec.Adapter;

namespace Crave.Ordering.Api.Data.Repositories
{
    /// <summary>
    /// Repository class for orders.
    /// </summary>
    public class OrderRepository : RepositoryBase<OrderEntity>, IOrderRepository
    {
        private const int ORDER_HISTORY_FETCH_LIMIT = 25;

        /// <inheritdoc/>
        public async Task<OrderEntity> GetByIdAsync(int orderId)
        {
            EntityQuery<OrderEntity> query = new QueryFactory().Order
                .Where(OrderFields.OrderId == orderId);

            return await new DataAccessAdapter().FetchFirstAsync(query);
        }

        /// <inheritdoc/>
        public async Task<OrderEntity> GetByIdWithPrefetchAsync(int orderId)
        {
            EntityQuery<OrderEntity> orderQuery = new QueryFactory().Order
                .Where(OrderFields.OrderId == orderId)
                .WithPath(OrderEntity.PrefetchPathCompanyEntity)
                .WithPath(OrderEntity.PrefetchPathClientEntity)
                .WithPath(OrderEntity.PrefetchPathDeliverypointEntity.WithSubPath(DeliverypointEntity.PrefetchPathDeliverypointgroupEntity))
                .WithPath(OrderEntity.PrefetchPathOrderitemCollection);

            using IDataAccessAdapter adapter = new DataAccessAdapter();
            return await adapter.FetchFirstAsync(orderQuery);
        }

        /// <inheritdoc />
        public async Task<OrderEntity> GetByGuidWithPrefetchAsync(string guid)
        {
            EntityQuery<OrderEntity> orderQuery = new QueryFactory().Order
                .Where(OrderFields.Guid == guid)
                .WithPath(OrderEntity.PrefetchPathDeliveryInformationEntity) // Order > DeliveryInformation
                .WithPath(OrderEntity.PrefetchPathReceiptCollection) // Order > Receipt
                .WithPath(OrderEntity.PrefetchPathOrderitemCollection // Order > Orderitem
                    .WithSubPath(OrderitemEntity.PrefetchPathOrderitemAlterationitemCollection // Orderitem > OrderitemAlteration
                        .WithSubPath(OrderitemAlterationitemEntity.PrefetchPathAlterationEntity) // OrderitemAlteration > Alteration
                        .WithSubPath(OrderitemAlterationitemEntity.PrefetchPathAlterationitemEntity // OrderitemAlteration > Alterationitem
                            .WithSubPath(AlterationitemEntity.PrefetchPathAlterationoptionEntity)))); // Alterationitem > Alterationoption

            using DataAccessAdapter adapter = new DataAccessAdapter();
            return await adapter.FetchFirstAsync(orderQuery);
        }

        /// <inheritdoc/>
        public Task<OrderEntity> GetByGuidAsync(string guid)
        {
            RelationPredicateBucket filterBucket = new RelationPredicateBucket();
            filterBucket.PredicateExpression.Add(OrderFields.Guid == guid);

            IncludeFieldsList includeFieldsList = new IncludeFieldsList
            {
                OrderFields.OrderId,
                OrderFields.Guid,
                OrderFields.CompanyId,
                OrderFields.Status,
                OrderFields.Total,
                OrderFields.CurrencyCode
            };

            return DataAccessAdapter.FetchEntityAsync<OrderEntity>(filterBucket, null, includeFieldsList);
        }

        /// <inheritdoc/>
        public Task<OrderEntity> GetOrderForPaymentAsync(string guid)
        {
            PrefetchPath2 prefetch = new PrefetchPath2(EntityType.OrderEntity);
            prefetch.Add(OrderEntity.PrefetchPathCheckoutMethodEntity);

            RelationPredicateBucket filterBucket = new RelationPredicateBucket();
            filterBucket.PredicateExpression.Add(OrderFields.Guid == guid);
            filterBucket.Relations.Add(OrderEntity.Relations.CheckoutMethodEntityUsingCheckoutMethodId);

            IncludeFieldsList includeFieldsList = new IncludeFieldsList(
                OrderFields.OrderId,
                OrderFields.Guid,
                OrderFields.CompanyId,
                OrderFields.Status,
                OrderFields.Total,
                OrderFields.CurrencyCode,
                OrderFields.CountryCode
            );

            return DataAccessAdapter.FetchEntityAsync<OrderEntity>(filterBucket, prefetch, includeFieldsList);
        }

        /// <inheritdoc/>
        public OrderEntity GetOrderForNotification(int orderId)
        {
            PrefetchPath2 prefetch = new PrefetchPath2(EntityType.OrderEntity);
            prefetch.Add(OrderEntity.PrefetchPathCompanyEntity, new IncludeFieldsList(CompanyFields.PricesIncludeTaxes, CompanyFields.CultureCode, CompanyFields.ClockMode));
            prefetch.Add(OrderEntity.PrefetchPathDeliveryInformationEntity);
            prefetch.Add(OrderEntity.PrefetchPathPaymentTransactionCollection);
            prefetch.Add(OrderEntity.PrefetchPathCheckoutMethodEntity);
            prefetch.Add(OrderEntity.PrefetchPathServiceMethodEntity);
            prefetch.Add(OrderEntity.PrefetchPathOutletEntity).SubPath.Add(OutletEntity.PrefetchPathOutletSellerInformationEntity);
            prefetch.Add(OrderEntity.PrefetchPathReceiptCollection);

            IPrefetchPathElement2 prefetchOrderitemCollection = prefetch.Add(OrderEntity.PrefetchPathOrderitemCollection);
            prefetchOrderitemCollection.SubPath.Add(OrderitemEntity.PrefetchPathOrderitemAlterationitemCollection).SubPath.Add(OrderitemAlterationitemEntity.PrefetchPathAlterationEntity);

            prefetchOrderitemCollection.SubPath.Add(OrderitemEntity.PrefetchPathTaxTariffEntity);

            return DataAccessAdapter.FetchEntityById<OrderEntity>(orderId, prefetch, null);
        }

        /// <inheritdoc/>
        public Task<OrderEntity> GetByGuidWithOrderRoutestephandlersAsync(string guid)
        {
            RelationPredicateBucket filterBucket = new RelationPredicateBucket();
            filterBucket.PredicateExpression.Add(OrderFields.Guid == guid);

            PredicateExpression filterOrderTypes = new PredicateExpression();
            filterOrderTypes.Add(OrderFields.Type == (int)OrderType.Standard);
            filterOrderTypes.AddWithOr(OrderFields.Type == (int)OrderType.RequestForService);
            filterOrderTypes.AddWithOr(OrderFields.Type == (int)OrderType.RequestForBatteryCharge);
            filterOrderTypes.AddWithOr(OrderFields.Type == (int)OrderType.RequestForPrint);
            filterOrderTypes.AddWithOr(OrderFields.Type == (int)OrderType.RequestForWakeUp);
            filterBucket.PredicateExpression.Add(filterOrderTypes);

            PrefetchPath2 prefetch = new PrefetchPath2(EntityType.OrderEntity);
            prefetch.Add(OrderEntity.PrefetchPathOrderRoutestephandlerCollection);

            return this.FetchEntityAsync(filterBucket, prefetch, null);
        }

        /// <inheritdoc/>
        public async Task<OrderEntity> SaveOrderAsync(OrderEntity orderEntity)
        {
            await DataAccessAdapter.SaveAsync(orderEntity, true);
            return orderEntity;
        }

        /// <inheritdoc/>
        public EntityCollection<OrderEntity> GetOrders(int clientId, int customerId, string orderGuid)
        {
            // We only allow to cancel orders that are not being handled (route was not started)
            RelationPredicateBucket filterBucket = new RelationPredicateBucket();

            if (customerId > 0)
            {
                filterBucket.PredicateExpression.Add(OrderFields.CustomerId == customerId);
            }

            if (clientId > 0)
            {
                filterBucket.PredicateExpression.Add(OrderFields.ClientId == clientId);
            }

            if (!orderGuid.IsNullOrWhiteSpace())
            {
                filterBucket.PredicateExpression.Add(OrderFields.Guid == orderGuid);
            }

            filterBucket.PredicateExpression.Add(OrderFields.Status != OrderStatus.InRoute & OrderFields.Status != OrderStatus.Processed);

            PrefetchPath2 prefetchPath = new PrefetchPath2(EntityType.OrderEntity);
            prefetchPath.Add(OrderEntity.PrefetchPathOrderRoutestephandlerCollection);

            return this.FetchEntityCollection(filterBucket, 0, null, prefetchPath, null);
        }

        /// <inheritdoc/>
        public EntityCollection<OrderEntity> GetPendingOrders(int terminalId, IEnumerable<int> orderIds, IEnumerable<OrderRoutestephandlerStatus> orderRoutestephandlerStatuses)
        {
            // Create the filter
            RelationPredicateBucket filterBucket = new RelationPredicateBucket();
            filterBucket.Relations.Add(OrderEntity.Relations.OrderRoutestephandlerEntityUsingOrderId);

            if (orderIds.Any())
            {
                filterBucket.PredicateExpression.AddWithOr(OrderFields.OrderId == orderIds.ToArray());
            }

            // Create the prefetch
            PrefetchPath2 prefetchPath = new PrefetchPath2(EntityType.OrderEntity);
            prefetchPath.Add(OrderEntity.PrefetchPathDeliveryInformationEntity);
            prefetchPath.Add(OrderEntity.PrefetchPathReceiptCollection);

            PredicateExpression paymentTransactionFilter = new PredicateExpression { PaymentTransactionFields.Status == PaymentTransactionStatus.Paid };
            prefetchPath.Add(OrderEntity.PrefetchPathPaymentTransactionCollection, 0, paymentTransactionFilter);

            // Order > Orderitem
            IPrefetchPathElement2 prefetchOrder_Orderitem = prefetchPath.Add(OrderEntity.PrefetchPathOrderitemCollection);

            // Order > Orderitem > OrderitemAlterationitem > Alterationitem > Alteration
            // Order > Orderitem > OrderitemAlterationitem > Alterationitem > Alterationoption
            IPrefetchPathElement2 prefetchOrder_Orderitem_OrderitemAlterationitem = prefetchOrder_Orderitem.SubPath.Add(OrderitemEntity.PrefetchPathOrderitemAlterationitemCollection);
            prefetchOrder_Orderitem_OrderitemAlterationitem.SubPath.Add(OrderitemAlterationitemEntity.PrefetchPathAlterationEntity);
            IPrefetchPathElement2 prefetchOrder_Orderitem_OrderitemAlterationitem_Alterationitem = prefetchOrder_Orderitem_OrderitemAlterationitem.SubPath.Add(OrderitemAlterationitemEntity.PrefetchPathAlterationitemEntity);
            prefetchOrder_Orderitem_OrderitemAlterationitem_Alterationitem.SubPath.Add(AlterationitemEntity.PrefetchPathAlterationoptionEntity);

            // Order > OrderRoutestephandler
            PredicateExpression orderRoutestephandlerFilter = new PredicateExpression();
            orderRoutestephandlerFilter.Add(OrderRoutestephandlerFields.TerminalId == terminalId);
            orderRoutestephandlerFilter.Add(OrderRoutestephandlerFields.Status == orderRoutestephandlerStatuses.ToArray());
            prefetchPath.Add(OrderEntity.PrefetchPathOrderRoutestephandlerCollection, 0, orderRoutestephandlerFilter);

            // Fetch the orders from the database
            return this.FetchEntityCollection(filterBucket, 0, null, prefetchPath, null);
        }

        /// <inheritdoc/>
        public EntityCollection<OrderEntity> GetOrdersHistory(int terminalId)
        {
            RelationPredicateBucket tempFilterBucket = new RelationPredicateBucket();
            tempFilterBucket.PredicateExpression.Add(OrderRoutestephandlerHistoryFields.TerminalId == terminalId);
            tempFilterBucket.Relations.Add(OrderRoutestephandlerHistoryEntity.Relations.OrderEntityUsingOrderId);

            PrefetchPath2 tempPrefetchPath = new PrefetchPath2(EntityType.OrderRoutestephandlerHistoryEntity);
            IPrefetchPathElement2 prefetchOrder = tempPrefetchPath.Add(OrderRoutestephandlerHistoryEntity.PrefetchPathOrderEntity);
            prefetchOrder.SubPath.Add(OrderEntity.PrefetchPathDeliveryInformationEntity);
            prefetchOrder.SubPath.Add(OrderEntity.PrefetchPathReceiptCollection);

            PredicateExpression paymentTransactionFilter = new PredicateExpression { PaymentTransactionFields.Status == PaymentTransactionStatus.Paid };
            prefetchOrder.SubPath.Add(OrderEntity.PrefetchPathPaymentTransactionCollection, 0, paymentTransactionFilter);

            IPrefetchPathElement2 prefetchOrder_Orderitem = prefetchOrder.SubPath.Add(OrderEntity.PrefetchPathOrderitemCollection);

            IPrefetchPathElement2 prefetchOrder_Orderitem_OrderitemAlterationitem = prefetchOrder_Orderitem.SubPath.Add(OrderitemEntity.PrefetchPathOrderitemAlterationitemCollection);
            prefetchOrder_Orderitem_OrderitemAlterationitem.SubPath.Add(OrderitemAlterationitemEntity.PrefetchPathAlterationitemEntity);

            SortExpression tempSort = new SortExpression();
            tempSort.Add(OrderRoutestephandlerHistoryFields.OrderRoutestephandlerHistoryId | SortOperator.Descending);

            EntityCollection<OrderRoutestephandlerHistoryEntity> history =
                DataAccessAdapter.FetchEntityCollection<OrderRoutestephandlerHistoryEntity>(tempFilterBucket, 25, tempSort, tempPrefetchPath, null);

            // First we fetch the order routestephandler history after which we retrieve the orders from them
            // Due to time outs on the production environment when doing it the other way around

            return new EntityCollection<OrderEntity>(history.Select(x => x.Order));
        }
    }
}
