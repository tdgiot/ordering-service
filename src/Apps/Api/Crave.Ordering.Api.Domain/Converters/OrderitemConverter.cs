﻿using Crave.Ordering.Api.Domain.UseCases.Responses;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Microsoft.Extensions.Logging;

namespace Crave.Ordering.Api.Domain.Converters
{
    public class OrderitemConverter : EntityConverter<OrderitemEntity, Orderitem>
    {
        private readonly OrderitemAlterationitemConverter orderitemAlterationitemConverter;

        public OrderitemConverter(ILogger<OrderitemConverter> logger,
                                  OrderitemAlterationitemConverter orderitemAlterationitemConverter) : base(logger)
        {
            this.orderitemAlterationitemConverter = orderitemAlterationitemConverter;
        }

        public override Orderitem ConvertEntityToTResponseDto(OrderitemEntity entity)
        {
            return new Orderitem(entity.OrderitemId,
                                 entity.ProductId,
                                 entity.CategoryId,
                                 entity.PriceLevelItemId,
                                 entity.TaxTariffId,
                                 entity.Guid, 
                                 entity.ProductName, 
                                 entity.ProductPriceIn, 
                                 entity.Quantity, 
                                 entity.Notes, 
                                 entity.Color, 
                                 entity.TaxName, 
                                 entity.TaxPercentage, 
                                 entity.PriceTotalExTax,
                                 entity.TaxTotal, 
                                 orderitemAlterationitemConverter.ConvertEntityCollectionToTResponseDtoCollection(entity.OrderitemAlterationitemCollection));
        }
    }
}
