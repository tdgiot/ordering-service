﻿using Crave.Ordering.Api.Domain.UseCases.Responses;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Microsoft.Extensions.Logging;

namespace Crave.Ordering.Api.Domain.Converters
{
    public class ReceiptConverter : EntityConverter<ReceiptEntity, Receipt>
    {
        public ReceiptConverter(ILogger<ReceiptConverter> logger) : base(logger)
        { }

        public override Receipt ConvertEntityToTResponseDto(ReceiptEntity entity)
        {
            return new Receipt(entity.Number,
                               entity.SellerName,
                               entity.SellerAddress,
                               entity.SellerContactInfo,
                               entity.ServiceMethodName,
                               entity.CheckoutMethodName);
        }
    }
}
