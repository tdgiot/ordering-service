﻿using Crave.Ordering.Api.Domain.UseCases.Responses;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Microsoft.Extensions.Logging;

namespace Crave.Ordering.Api.Domain.Converters
{
    public class DeliveryInformationConverter : EntityConverter<DeliveryInformationEntity, DeliveryInformation>
    {
        public DeliveryInformationConverter(ILogger<DeliveryInformationConverter> logger) : base(logger)
        { }

        public override DeliveryInformation ConvertEntityToTResponseDto(DeliveryInformationEntity entity)
        {
            return new DeliveryInformation(entity.BuildingName,
                                           entity.Address,
                                           entity.Zipcode,
                                           entity.City,
                                           entity.Instructions);
        }
    }
}
