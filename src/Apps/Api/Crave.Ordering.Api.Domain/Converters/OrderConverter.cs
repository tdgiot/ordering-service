﻿using System.Linq;
using Crave.Enums;
using Crave.Ordering.Api.Domain.UseCases.Responses;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.Extensions;
using Crave.Ordering.Shared.Extensions;
using Microsoft.Extensions.Logging;

namespace Crave.Ordering.Api.Domain.Converters
{
    public class OrderConverter : EntityConverter<OrderEntity, Order>
    {
        private readonly DeliveryInformationConverter deliveryInformationConverter;
        private readonly ReceiptConverter receiptConverter;
        private readonly OrderitemConverter orderitemConverter;
        private readonly OrderRoutestephandlerConverter orderRoutestephandlerConverter;

        public OrderConverter(ILogger<OrderConverter> logger, 
                              DeliveryInformationConverter deliveryInformationConverter,
                              ReceiptConverter receiptConverter,
                              OrderitemConverter orderitemConverter,
                              OrderRoutestephandlerConverter orderRoutestephandlerConverter) : base(logger)
        {
            this.deliveryInformationConverter = deliveryInformationConverter;
            this.receiptConverter = receiptConverter;
            this.orderitemConverter = orderitemConverter;
            this.orderRoutestephandlerConverter = orderRoutestephandlerConverter;
        }

        public override Order ConvertEntityToTResponseDto(OrderEntity entity)
        {
            return new Order(entity.Type,
                             entity.OrderId,
                             entity.Guid,
                             entity.CompanyId, 
                             entity.OutletId, 
                             entity.ClientId, 
                             entity.Printed, 
                             entity.Notes, 
                             entity.Status, 
                             entity.ProcessedUTC, 
                             entity.CreatedUTC, 
                             entity.ErrorCode, 
                             entity.CheckoutMethodType, 
                             entity.PricesIncludeTaxes,
                             entity.TaxBreakdown,
                             entity.Total,
                             entity.SubTotal,
                             entity.CurrencyCode,
                             entity.CountryCode,
                             CreateDeliverypoint(entity),
                             CreateCustomer(entity),
                             deliveryInformationConverter.ConvertEntityIfNotNullOrNewToTResponseDto(entity.DeliveryInformation), 
                             receiptConverter.ConvertEntityIfNotNullOrNewToTResponseDto(entity.ReceiptCollection.FirstOrDefault()), 
                             orderitemConverter.ConvertEntityCollectionToTResponseDtoCollection(entity.OrderitemCollection), 
                             orderRoutestephandlerConverter.ConvertEntityCollectionToTResponseDtoCollection(entity.OrderRoutestephandlerCollection),
                             ConvertPaymentTransactionIfNotNullOrNew(entity.PaymentTransactionCollection.FirstOrDefault()),
                             entity.CoversCount);
        }

        private static Customer CreateCustomer(OrderEntity orderEntity)
        {
            return new Customer(orderEntity.CustomerId, 
                                orderEntity.CustomerFirstname, 
                                orderEntity.CustomerLastname, 
                                orderEntity.CustomerLastnamePrefix, 
                                orderEntity.Email, 
                                orderEntity.CustomerPhonenumber, 
                                orderEntity.IsCustomerVip);
        }

        private static Deliverypoint CreateDeliverypoint(OrderEntity orderEntity)
        {
            return new Deliverypoint(orderEntity.DeliverypointId, 
                                     orderEntity.DeliverypointNumber.ToIntOrNull(), 
                                     orderEntity.DeliverypointName);
        }

        private static PaymentTransaction? ConvertPaymentTransactionIfNotNullOrNew(PaymentTransactionEntity? paymentTransaction) 
            => paymentTransaction == null || paymentTransaction.IsNew ? null : new PaymentTransaction(paymentTransaction.CardSummary, paymentTransaction.PaymentMethod);
    }
}
