﻿using System;
using System.Collections.Generic;
using System.Linq;
using Crave.Ordering.Shared.Data.LLBLGen.Extensions;
using Microsoft.Extensions.Logging;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Crave.Ordering.Api.Domain.Converters
{
    /// <summary>
    /// Abstract Entity Converter class which is used to inherit from when implementing an entity converter.
    /// </summary>
    /// <typeparam name="TEntity">The type of the entity to convert.</typeparam>
    /// <typeparam name="TResponseDto">The type of the response DTO to convert to.</typeparam>
    public abstract class EntityConverter<TEntity, TResponseDto>
        where TEntity : IEntity2
        where TResponseDto : class
    {
        /// <summary>
        /// Initialize a new instance of <see cref="EntityConverter{TEntity,TResponseDto}"/>.
        /// </summary>
        /// <param name="logger">An instance of <see cref="ILogger"/>.</param>
        protected EntityConverter(ILogger logger)
        {
            this.Logger = logger;
        }

        protected ILogger Logger { get; }

        /// <summary>
        /// Convert a <see cref="TEntity"/> into a <see cref="TResponseDto"/>.
        /// </summary>
        /// <param name="entity">The entity to convert.</param>
        /// <returns>A <see cref="TResponseDto"/> instance containing the converted entity.</returns>
        public abstract TResponseDto ConvertEntityToTResponseDto(TEntity entity);

        /// <summary>
        /// Convert a <see cref="TEntity"/> into a <see cref="TResponseDto"/> if the entity is not null or new.
        /// </summary>
        /// <param name="entity">The entity to convert.</param>
        /// <returns>A <see cref="TResponseDto"/> instance containing the converted entity.</returns>
        public TResponseDto? ConvertEntityIfNotNullOrNewToTResponseDto(TEntity entity)
        {
            return entity.IsNullOrNew() ? null : ConvertEntityToTResponseDto(entity);
        }

        /// <summary>
        /// Convert an <see cref="IEnumerable{TEntity}"/> into a <see cref="IEnumerable{TResponseDto}"/>.
        /// </summary>
        /// <param name="entityCollection">The entity collection to convert.</param>
        /// <param name="skipFailedConverts">Pass 'false' to throw an exception when conversion fails.</param>
        /// <returns>A <see cref="IEnumerable{TResponseDto}"/> instance containing the response DTO instances.</returns>
        public virtual IEnumerable<TResponseDto> ConvertEntityCollectionToTResponseDtoCollection(IEnumerable<TEntity> entityCollection, bool skipFailedConverts = true)
        {
            if (entityCollection == null)
            {
                yield break;
            }

            foreach (TEntity entity in entityCollection)
            {
                TResponseDto? result = null;

                try
                {
                    result = this.ConvertEntityToTResponseDto(entity);
                }
                catch (Exception exception)
                {
                    object? primaryKey = entity.Fields.PrimaryKeyFields.FirstOrDefault();
                    this.Logger.LogError(exception, $"Could not convert {typeof(TEntity)}[{primaryKey}]");

                    if (!skipFailedConverts)
                    {
                        throw;
                    }
                }

                if (result != null)
                {
                    yield return result;
                }
            }
        }
    }
}
