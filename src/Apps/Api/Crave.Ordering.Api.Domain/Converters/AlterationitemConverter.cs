﻿using Crave.Ordering.Api.Domain.UseCases.Responses;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Microsoft.Extensions.Logging;

namespace Crave.Ordering.Api.Domain.Converters
{
    public class AlterationitemConverter : EntityConverter<AlterationitemEntity, Alterationitem>
    {
        public AlterationitemConverter(ILogger<AlterationitemConverter> logger) : base(logger)
        {

        }

        public override Alterationitem ConvertEntityToTResponseDto(AlterationitemEntity entity)
        {
            return new Alterationitem(entity.AlterationitemId, 
                                      entity.AlterationId, 
                                      entity.AlterationoptionId);
        }
    }
}
