﻿using Crave.Ordering.Api.Domain.UseCases.Responses;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Microsoft.Extensions.Logging;

namespace Crave.Ordering.Api.Domain.Converters
{
    public class OrderRoutestephandlerConverter : EntityConverter<OrderRoutestephandlerEntity, OrderRoutestephandler>
    {
        public OrderRoutestephandlerConverter(ILogger<OrderRoutestephandlerConverter> logger) : base(logger)
        {
        }

        public override OrderRoutestephandler ConvertEntityToTResponseDto(OrderRoutestephandlerEntity entity)
        {
            return new OrderRoutestephandler(entity.OrderRoutestephandlerId,
                                             entity.OriginatedFromRoutestepHandlerId,
                                             entity.Guid,
                                             entity.OrderId,
                                             entity.TerminalId,
                                             entity.Number, 
                                             entity.HandlerType, 
                                             entity.PrintReportType, 
                                             entity.Status, 
                                             entity.ErrorCode, 
                                             entity.CompletedUTC, 
                                             entity.Timeout, 
                                             entity.TimeoutExpiresUTC, 
                                             entity.ForwardedFromTerminalId,
                                             entity.FieldValue1,
                                             entity.FieldValue2,
                                             entity.FieldValue3,
                                             entity.FieldValue4,
                                             entity.FieldValue5,
                                             entity.FieldValue6,
                                             entity.FieldValue7,
                                             entity.FieldValue8,
                                             entity.FieldValue9,
                                             entity.FieldValue10);
        }
    }
}
