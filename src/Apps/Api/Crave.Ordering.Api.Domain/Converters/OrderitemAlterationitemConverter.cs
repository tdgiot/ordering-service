﻿using Crave.Ordering.Api.Domain.UseCases.Responses;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Microsoft.Extensions.Logging;

namespace Crave.Ordering.Api.Domain.Converters
{
    public class OrderitemAlterationitemConverter : EntityConverter<OrderitemAlterationitemEntity, OrderitemAlterationitem>
    {
        private readonly AlterationitemConverter alterationitemConverter;

        public OrderitemAlterationitemConverter(ILogger<OrderitemAlterationitemConverter> logger,
                                                AlterationitemConverter alterationitemConverter) : base(logger)
        {
            this.alterationitemConverter = alterationitemConverter;
        }

        public override OrderitemAlterationitem ConvertEntityToTResponseDto(OrderitemAlterationitemEntity entity)
        {
            return new OrderitemAlterationitem(entity.AlterationType,
                                               entity.AlterationitemId,
                                               entity.AlterationId,
                                               entity.PriceLevelItemId,
                                               entity.TaxTariffId,
                                               entity.AlterationName, 
                                               entity.AlterationoptionName, 
                                               entity.Value, 
                                               entity.TimeUTC, 
                                               entity.OrderLevelEnabled, 
                                               entity.TaxName, 
                                               entity.TaxPercentage, 
                                               entity.PriceTotalExTax, 
                                               entity.TaxTotal, 
                                               entity.AlterationoptionPriceIn,
                                               alterationitemConverter.ConvertEntityIfNotNullOrNewToTResponseDto(entity.Alterationitem));
        }
    }
}
