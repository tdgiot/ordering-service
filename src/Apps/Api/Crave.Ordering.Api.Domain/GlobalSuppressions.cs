﻿// This file is used by Code Analysis to maintain SuppressMessage
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given
// a specific target and scoped to a namespace, type, member, etc.

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Minor Code Smell", "S4056:Overloads with a \"CultureInfo\" or an \"IFormatProvider\" parameter should be used", Justification = "Using culture other than default breaks URL.", 
    Scope = "member", Target = "~M:Crave.Ordering.Api.Domain.UseCases.HandleEmailOrderRoutestephandlerUseCase.ConvertMediaToUrl(System.Int32,System.Collections.Generic.IDictionary{System.String,System.String})")]
[assembly: SuppressMessage("Minor Code Smell", "S1449:Culture should be specified for \"string\" operations", Justification = "Using culture other than default breaks URL.",
    Scope = "member", Target = "~M:Crave.Ordering.Api.Domain.UseCases.HandleEmailOrderRoutestephandlerUseCase.ConvertMediaToUrl(System.Int32,System.Collections.Generic.IDictionary{System.String,System.String})")]
