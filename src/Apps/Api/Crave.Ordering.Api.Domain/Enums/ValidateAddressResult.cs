﻿namespace Crave.Ordering.Api.Domain.Enums
{
    /// <summary>
    /// Enumeration which represents the result of address validation.
    /// </summary>
    public enum ValidateAddressResult
    {
        /// <summary>
        /// Address in range.
        /// </summary>
        InRange = 1,

        /// <summary>
        /// Address out of range.
        /// </summary>
        OutOfRange = 2,

        /// <summary>
        /// Invalid service method.
        /// </summary>
        InvalidServiceMethod = 3,

        /// <summary>
        /// No delivery distance specified on service method.
        /// </summary>
        NoDeliveryDistanceSpecifiedForServiceMethod = 4
    }
}
