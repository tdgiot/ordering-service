﻿namespace Crave.Ordering.Api.Domain.Enums
{
    /// <summary>
    /// Enumeration which represents the result of delivery rate check.
    /// </summary>
    public enum DeliveryRateResult
    {
        /// <summary>
        /// In range and charge set.
        /// </summary>
        OK = 1,

        /// <summary>
        /// Invalid service method.
        /// </summary>
        InvalidServiceMethod = 2,

        /// <summary>
        /// No delivery distance specified on service method.
        /// </summary>
        NoDeliveryDistanceSpecifiedForServiceMethod = 3,

        /// <summary>
        /// Unable to retrieve the distance between outlet and delivery address.
        /// </summary>
        UnableToRetrieveDistanceBetweenOutletAndDeliveryLocation = 4,

        /// <summary>
        /// Delivery address out of range.
        /// </summary>
        OutOfRange = 5
    }
}
