﻿using Crave.Ordering.Api.Core.Enums;
using Crave.Ordering.Api.Domain.UseCases.Responses;
using Crave.Ordering.Api.Domain.Validation.Errors;
using System;
using System.Collections.Generic;

namespace Crave.Ordering.Api.Domain.Exceptions
{
    public class CheckoutValidationException : Exception
    {
        public CheckoutValidationException(ValidationResponse checkoutValidationResponse)
        {
            CheckoutValidationResponse = checkoutValidationResponse;
        }

        public CheckoutValidationException(IEnumerable<ValidationError> validationErrors)
        {
            CheckoutValidationResponse = new ValidationResponse(validationErrors);
        }

        public CheckoutValidationException(ValidationErrorSubType validationErrorSubType)
        {
            CheckoutValidationResponse = new ValidationResponse(new[]
            {
                new ValidationError(validationErrorSubType),
            });
        }

        public ValidationResponse CheckoutValidationResponse { get; }
    }
}
