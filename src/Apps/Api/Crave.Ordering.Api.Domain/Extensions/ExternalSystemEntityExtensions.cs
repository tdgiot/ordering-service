﻿using System;
using Crave.Integrations.Ops.Alice;
using Crave.Integrations.Ops.HotSOS;
using Crave.Integrations.Ops.Hyatt;
using Crave.Integrations.Ops.Quore;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Ordering.Api.Domain.Extensions
{
    public static class ExternalSystemEntityExtensions
    {
        public static HotSOSConfiguration ToHotSOSConfiguration(this ExternalSystemEntity externalSystemEntity)
        {
            return new HotSOSConfiguration(externalSystemEntity.StringValue1, 
                                           externalSystemEntity.StringValue2, 
                                           new Uri(externalSystemEntity.StringValue3));
        }

        public static HyattConfiguration ToHyattConfiguration(this ExternalSystemEntity externalSystemEntity)
        {
            return new HyattConfiguration(externalSystemEntity.StringValue1,
                                          new Uri(externalSystemEntity.StringValue2), 
                                          externalSystemEntity.StringValue3, 
                                          externalSystemEntity.StringValue4, 
                                          externalSystemEntity.StringValue5);
        }

        public static AliceConfiguration ToAliceConfiguration(this ExternalSystemEntity externalSystemEntity)
        {
            return new AliceConfiguration(new Uri(externalSystemEntity.StringValue1),
                                          externalSystemEntity.StringValue2, 
                                          externalSystemEntity.StringValue3, 
                                          externalSystemEntity.StringValue4, 
                                          externalSystemEntity.StringValue5);
        }

        public static QuoreConfiguration ToQuoreConfiguration(this ExternalSystemEntity externalSystemEntity)
        {
            return new QuoreConfiguration(new Uri(externalSystemEntity.StringValue1),
                                          externalSystemEntity.StringValue2);
        }
    }
}
