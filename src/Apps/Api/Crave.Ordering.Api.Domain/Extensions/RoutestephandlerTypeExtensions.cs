﻿using System;
using Crave.Enums;

namespace Crave.Ordering.Api.Domain.Extensions
{
    public static class RoutestephandlerTypeExtensions
    {
        public static ExternalSystemType ToExternalSystemType(this RoutestephandlerType routestephandlerType)
        {
            switch (routestephandlerType)
            {
                case RoutestephandlerType.HotSOS:
                    return ExternalSystemType.HotSOS;

                case RoutestephandlerType.Quore:
                    return ExternalSystemType.Quore;

                case RoutestephandlerType.Hyatt:
                    return ExternalSystemType.Hyatt_HotSOS;

                case RoutestephandlerType.Alice:
                    return ExternalSystemType.Alice;

                default:
                    throw new ArgumentOutOfRangeException(nameof(routestephandlerType), routestephandlerType, "Unable to convert routestephandler to external system type");
            }
        }
    }
}
