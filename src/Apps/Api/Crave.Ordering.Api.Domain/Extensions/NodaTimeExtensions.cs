﻿using System;
using System.IO;
using NodaTime;
using NodaTime.Text;
using NodaTime.TimeZones;

namespace Crave.Ordering.Api.Domain.Extensions
{
    public static class NodaTimeExtensions
    {
        private const string dateTimeParsingFormat = "ddMMyyHHmmss";

        private static readonly TzdbDateTimeZoneSource source = TzdbDateTimeZoneSource.Default;

        /// <summary>
        /// Converts the current UTC time to the date time based on the provided time zone identity.
        /// </summary>
        /// <param name="utcDateTime">The current UTC time which needs to be converted.</param>
        /// <param name="timeZoneId">The zone identity which the UTC time should be converted into.</param>
        /// <returns>The zoned date time after conversion from the current UTC date time.</returns>
        public static ZonedDateTime ConvertToZonedDateTime(this DateTime utcDateTime, string? timeZoneId) => ConvertToZonedDateTime(utcDateTime, timeZoneId, false);

        /// <summary>
        /// Converts the current UTC time to the date time based on the provided time zone identity.
        /// </summary>
        /// <param name="utcDateTime">The current UTC time which needs to be converted.</param>
        /// <param name="timeZoneId">The zone identity which the UTC time should be converted into.</param>
        /// <param name="isWindowsTimeZone">Determines if the time zone identity is formatted for Windows Operation Systems.</param>
        /// <returns>The zoned date time after conversion from the current UTC date time.</returns>
        public static ZonedDateTime ConvertToZonedDateTime(this DateTime utcDateTime, string? timeZoneId, bool isWindowsTimeZone)
        {
            InstantPattern pattern = InstantPattern.CreateWithInvariantCulture(dateTimeParsingFormat);
            ParseResult<Instant> parsedResult = pattern.Parse(utcDateTime.ToString(dateTimeParsingFormat));

            if (!parsedResult.Success)
            {
                throw new InvalidDataException(string.Format("Failed to parse the following data: '{0}'", utcDateTime.ToString(dateTimeParsingFormat)));
            }

            Instant instant = parsedResult.Value;

            return isWindowsTimeZone
                ? instant.InZone(ConvertWindowsToIanaTimeZone(timeZoneId))
                : instant.InZone(DateTimeZoneProviders.Tzdb[timeZoneId]);
        }

        /// <summary>
        /// Converts the Windows formatted time zone identity to the IANA standard.
        /// </summary>
        /// <param name="timeZoneId">The time zone identity which needs to be converted.</param>
        /// <returns>Date time zone which has been converted into the IANA standard.</returns>
        private static DateTimeZone ConvertWindowsToIanaTimeZone(string? timeZoneId)
        {
            if (string.IsNullOrWhiteSpace(timeZoneId))
            {
                throw new InvalidTimeZoneException("String cannot be null or empty.");
            }

            source.WindowsMapping.PrimaryMapping.TryGetValue(timeZoneId, out string result);
            if (result == null)
            {
                throw new TimeZoneNotFoundException(string.Format("The time zone {0} could not be found.", timeZoneId));
            }

            return DateTimeZoneProviders.Tzdb[result];
        }
    }
}
