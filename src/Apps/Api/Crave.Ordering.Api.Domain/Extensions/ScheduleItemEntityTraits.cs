﻿using System;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using NodaTime;

namespace Crave.Ordering.Api.Domain.Extensions
{
	public static class ScheduleItemEntityTraits
	{
		private const int Seconds = 0;

		public static TimeSpan GetStartTime(this ScheduleitemEntity entity)
		{
			int hours = Convert.ToInt32(entity.TimeStart.Substring(0, 2));
			int minutes = Convert.ToInt32(entity.TimeStart.Substring(2, 2));

			return new TimeSpan(hours, minutes, ScheduleItemEntityTraits.Seconds);
		}

		public static TimeSpan GetEndTime(this ScheduleitemEntity entity)
		{
			int hours = Convert.ToInt32(entity.TimeEnd.Substring(0, 2));
			int minutes = Convert.ToInt32(entity.TimeEnd.Substring(2, 2));

			return new TimeSpan(hours, minutes, ScheduleItemEntityTraits.Seconds);
		}

		public static bool IsActiveAtTime(this ScheduleitemEntity entity, LocalTime localTime)
		{
			localTime = ScheduleItemEntityTraits.HoursAndMinutesOnly(localTime);
			LocalTime startTime = entity.GetStartTime().ToLocalTime();
			LocalTime endTime = entity.GetEndTime().ToLocalTime();

			return localTime >= startTime && localTime <= endTime;
		}

		private static LocalTime HoursAndMinutesOnly(LocalTime localTime) => new LocalTime(localTime.Hour, localTime.Minute);
	}
}
