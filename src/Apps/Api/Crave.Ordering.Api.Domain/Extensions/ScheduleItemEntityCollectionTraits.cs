﻿using System.Collections.Generic;
using System.Linq;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using NodaTime;
using NodaTime.Utility;

namespace Crave.Ordering.Api.Domain.Extensions
{
	public static class ScheduleItemEntityCollectionTraits
	{
		public static IEnumerable<ScheduleitemEntity> GetItemsForDate(this IEnumerable<ScheduleitemEntity> scheduleItems, ZonedDateTime zonedDateTime) =>
			scheduleItems
				.Where(si => si.DayOfWeek == BclConversions.ToDayOfWeek(zonedDateTime.LocalDateTime.DayOfWeek) || si.DayOfWeek == null);
	}
}
