﻿using System;
using NodaTime;

namespace Crave.Ordering.Api.Domain.Extensions
{
	public static class TimeSpanTraits
	{
		public static LocalTime ToLocalTime(this TimeSpan self) => new LocalTime(self.Hours, self.Minutes);
	}
}
