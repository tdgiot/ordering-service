﻿using Crave.Ordering.Api.Core.Enums;

namespace Crave.Ordering.Api.Domain.Validation.Errors
{
    public class ValidationError
    {
        public ValidationError(ValidationErrorSubType validationSubType)
        {
            ValidationErrorSubType = validationSubType;
        }

        public ValidationErrorSubType ValidationErrorSubType { get; }

        public int? CategoryId { get; set; }

        public int? ProductId { get; set; }

        public int? AlterationId { get; set; }

        public int? AlterationOptionId { get; set; }
    }
}
