﻿using Crave.Ordering.Api.Core.Enums;

namespace Crave.Ordering.Api.Domain.Validation.Errors
{
    public class OrderitemValidationError : ValidationError
    {
        public OrderitemValidationError(ValidationErrorSubType validationErrorResult, int? categoryId, int? productId, int? alterationId, int? alterationOptionId)
            : base(validationErrorResult)
        {
            this.CategoryId = categoryId;
            this.ProductId = productId;
            this.AlterationId = alterationId;
            this.AlterationOptionId = alterationOptionId;
        }
    }
}
