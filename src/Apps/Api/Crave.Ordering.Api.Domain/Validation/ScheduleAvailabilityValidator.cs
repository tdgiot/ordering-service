﻿using System;
using System.Collections.Generic;
using System.Linq;
using Crave.Ordering.Api.Domain.Extensions;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Ordering.Api.Domain.Validation
{
    public static class ScheduleAvailabilityValidator
    {
		public static bool IsAvailable(ScheduleEntity scheduleEntity, string timeZoneId)
		{
			NodaTime.ZonedDateTime zonedDateTime = DateTime.UtcNow.ConvertToZonedDateTime(timeZoneId);

			IEnumerable<ScheduleitemEntity> scheduleItems = scheduleEntity.ScheduleitemCollection;

            return scheduleItems.GetItemsForDate(zonedDateTime).Any(scheduleitemEntity => scheduleitemEntity.IsActiveAtTime(zonedDateTime.LocalDateTime.TimeOfDay));
		}
	}
}
