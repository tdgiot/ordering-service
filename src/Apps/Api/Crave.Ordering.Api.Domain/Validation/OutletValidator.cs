﻿using Crave.Ordering.Api.Core.Enums;
using Crave.Ordering.Api.Domain.Exceptions;
using Crave.Ordering.Api.Domain.Validation.Errors;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.Extensions;
using Crave.Ordering.Shared.Extensions;
using System;
using System.Collections.Generic;

namespace Crave.Ordering.Api.Domain.Validation
{
    public static class OutletValidator
    {
        public static void Validate(OrderEntity orderEntity, ICollection<ValidationError> validationErrors)
        {
            OutletEntity outletEntity = orderEntity.Outlet;
            if (outletEntity.IsNullOrNew())
            {
                return;
            }

            ValidateOperationalState(orderEntity, validationErrors);
            ValidateBusinesshours(orderEntity, validationErrors);
            ValidateCustomerName(orderEntity, validationErrors);
            ValidateCustomerEmail(orderEntity, validationErrors);
            ValidateCustomerPhonenumber(orderEntity, validationErrors);
        }

        private static void ValidateOperationalState(OrderEntity orderEntity, ICollection<ValidationError> validationErrors)
        {
            if (orderEntity.Outlet.OutletOperationalState == null)
            {
                throw new CheckoutValidationException(ValidationErrorSubType.OutletOperationalStateInvalid);
            }

            if (orderEntity.Outlet.OutletOperationalState.OrderIntakeDisabled)
            {
                validationErrors.Add(new ValidationError(ValidationErrorSubType.OrderIntakeDisabled));
            }
        }

        private static void ValidateBusinesshours(OrderEntity orderEntity, ICollection<ValidationError> validationErrors)
        {
            if (!BusinesshoursValidator.IsWithinBusinesshours(orderEntity.Outlet.BusinesshourCollection, orderEntity.TimeZoneOlsonId))
            {
                validationErrors.Add(new ValidationError(ValidationErrorSubType.OutletClosedDueBusinessHours));
            }
        }

        private static void ValidateCustomerName(OrderEntity orderEntity, ICollection<ValidationError> validationErrors)
        {
            if (!orderEntity.Outlet.CustomerDetailsNameRequired)
            {
                return;
            }
            
            if (orderEntity.CustomerLastname.IsNullOrWhiteSpace())
            {
                validationErrors.Add(new ValidationError(ValidationErrorSubType.CustomerLastNameInvalid));
            }
        }

        private static void ValidateCustomerEmail(OrderEntity orderEntity, ICollection<ValidationError> validationErrors)
        {
            if (!orderEntity.Outlet.CustomerDetailsEmailRequired)
            {
                return;
            }

            if (orderEntity.Email.IsNullOrWhiteSpace())
            {
                validationErrors.Add(new ValidationError(ValidationErrorSubType.CustomerEmailRequired));
            }
            else if (!orderEntity.Email.IsValidEmail())
            {
                validationErrors.Add(new ValidationError(ValidationErrorSubType.CustomerEmailInvalid));
            }
        }

        private static void ValidateCustomerPhonenumber(OrderEntity orderEntity, ICollection<ValidationError> validationErrors)
        {
            if (!orderEntity.Outlet.CustomerDetailsPhoneRequired)
            {
                return;
            }

            if (orderEntity.CustomerPhonenumber.IsNullOrWhiteSpace())
            {
                validationErrors.Add(new ValidationError(ValidationErrorSubType.CustomerPhoneRequired));
            }
            else if (!orderEntity.CustomerPhonenumber.IsValidPhonenumber())
            {
                validationErrors.Add(new ValidationError(ValidationErrorSubType.CustomerPhoneInvalid));
            }
        }
    }
}
