﻿using Crave.Enums;
using Crave.Ordering.Api.Core.Enums;
using Crave.Ordering.Api.Domain.Exceptions;
using Crave.Ordering.Api.Domain.Validation.Errors;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Crave.Ordering.Api.Domain.Validation
{
    public static class TippingValidator
    {
        public static void Validate(OrderEntity orderEntity, ICollection<ValidationError> validationErrors)
        {
            if (!orderEntity.OutletId.HasValue)
            {
                return;
            }

            OrderitemEntity tippingOrderitem = orderEntity.OrderitemCollection.FirstOrDefault(x => x.Type == OrderitemType.Tip);

            if (orderEntity.TippingActive())
            {
                if (tippingOrderitem == null)
                {
                    throw new CheckoutValidationException(ValidationErrorSubType.TipRequired);
                }

                ValidateTipping(orderEntity, tippingOrderitem, validationErrors);
            }
            else if (tippingOrderitem != null)
            {
                throw new CheckoutValidationException(ValidationErrorSubType.TipNotRequired);
            }
        }

        private static void ValidateTipping(OrderEntity orderEntity, OrderitemEntity tippingOrderitem, ICollection<ValidationError> validationErrors)
        {
            if (!orderEntity.TippingMinimumRequired())
            {
                return;
            }

            decimal minimumTipPercentage = (decimal)orderEntity.Outlet.TippingMinimumPercentage.GetValueOrDefault(0);
            decimal minimumTipAmount = Math.Round(minimumTipPercentage / 100 * orderEntity.OrderitemTotalDisplayPrice, 2, MidpointRounding.AwayFromZero);

            if (tippingOrderitem!.TotalDisplayPrice < minimumTipAmount)
            {
                validationErrors.Add(new ValidationError(ValidationErrorSubType.TipInvalidMinimum));
            }
        }
    }
}
