﻿using Crave.Ordering.Api.Core.Enums;
using Crave.Ordering.Api.Domain.Exceptions;
using Crave.Ordering.Api.Domain.Validation.Errors;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.Extensions;
using System;
using System.Collections.Generic;

namespace Crave.Ordering.Api.Domain.Validation
{
    public static class CheckoutMethodValidator
    {
        public static void Validate(OrderEntity orderEntity, ICollection<ValidationError> validationErrors)
        {
            CheckoutMethodEntity checkoutMethodEntity = orderEntity.CheckoutMethod;
            if (checkoutMethodEntity.IsNullOrNew())
            {
                return;
            }

            ValidateCheckoutMethodActive(orderEntity, validationErrors);
            ValidateCustomerLastName(orderEntity, validationErrors);
            ValidateDeliverypoint(orderEntity, validationErrors);
            ValidatePrice(orderEntity, validationErrors);
        }

        private static void ValidateCheckoutMethodActive(OrderEntity orderEntity, ICollection<ValidationError> validationErrors)
        {
            if (!orderEntity.CheckoutMethod.Active)
            {
                validationErrors.Add(new ValidationError(ValidationErrorSubType.CheckoutMethodNotActive));
            }
        }

        private static void ValidateCustomerLastName(OrderEntity orderEntity, ICollection<ValidationError> validationErrors)
        {
            if (orderEntity.CheckoutMethod.IsCustomerLastNameRequired() && orderEntity.CustomerLastname.IsNullOrWhiteSpace())
            {
                validationErrors.Add(new ValidationError(ValidationErrorSubType.CustomerLastNameInvalid));
            }
        }

        private static void ValidateDeliverypoint(OrderEntity orderEntity, ICollection<ValidationError> validationErrors)
        {
            if (orderEntity.CheckoutMethod.IsDeliverypointRequired() && orderEntity.ChargeToDeliverypointId.GetValueOrDefault(0) <= 0)
            {
                validationErrors.Add(new ValidationError(ValidationErrorSubType.CheckoutMethodDeliveryPointRequired));
            }

            if (!orderEntity.CheckoutMethod.IsDeliverypointRequired() && orderEntity.ChargeToDeliverypointId.GetValueOrDefault(0) > 0)
            {
                throw new CheckoutValidationException(ValidationErrorSubType.CheckoutMethodDeliveryPointNotRequired);
            }
        }

        private static void ValidatePrice(OrderEntity orderEntity, ICollection<ValidationError> validationErrors)
        {
            if ((orderEntity.CheckoutMethod.IsPriceRequired() && orderEntity.Total <= 0M) ||
                (orderEntity.CheckoutMethod.IsFree() && orderEntity.Total > 0M))
            {
                validationErrors.Add(new ValidationError(ValidationErrorSubType.CheckoutMethodInvalid));
            }
        }
    }
}
