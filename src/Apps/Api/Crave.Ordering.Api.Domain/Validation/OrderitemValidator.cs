﻿using Crave.Enums;
using Crave.Ordering.Api.Core.Enums;
using Crave.Ordering.Api.Domain.Exceptions;
using Crave.Ordering.Api.Domain.UseCases.Requests;
using Crave.Ordering.Api.Domain.Validation.Errors;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.Extensions;
using Crave.Ordering.Shared.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Crave.Ordering.Api.Domain.Validation
{
    public static class OrderitemValidator
    {

        public static void Validate(OrderEntity orderEntity, IEnumerable<OrderitemRequest>? orderitems, ICollection<ValidationError> validationErrors, bool validateOrderLevelAlterations)
        {
            if (orderitems.IsNullOrEmpty() && orderEntity.RequiresOrderitems())
            {
                throw new CheckoutValidationException(ValidationErrorSubType.OrderitemsRequired);
            }
                
            ValidateOrderitems(orderEntity, orderitems, validationErrors, validateOrderLevelAlterations);
        }

        private static void ValidateOrderitems(OrderEntity orderEntity, IEnumerable<OrderitemRequest>? orderitems, ICollection<ValidationError> validationErrors, bool validateOrderLevelAlterations)
        {
            IEnumerable<OrderitemEntity> orderitemsOfTypeProduct = orderEntity.OrderitemCollection
                                                                              .Where(x => x.Type == OrderitemType.Product);

            foreach (OrderitemEntity orderitemEntity in orderitemsOfTypeProduct)
            {
                OrderitemRequest? orderitem = orderitems?.FirstOrDefault(x => x.TempInternalGuid == orderitemEntity.TempInternalGuid);
                if (orderitem == null)
                {
                    throw new CheckoutValidationException(ValidationErrorSubType.ProductInvalid);
                }

                ValidateOrderitem(orderEntity, orderitemEntity, orderitem, validationErrors, validateOrderLevelAlterations);
            }
        }

        private static void ValidateOrderitem(OrderEntity orderEntity, OrderitemEntity orderitemEntity, OrderitemRequest orderitem, ICollection<ValidationError> validationErrors, bool validateOrderLevelAlterations)
        {
            if (orderEntity.IsFromRestApi())
            {
                ValidateOrderItemOrderable(orderitemEntity, validationErrors);
                ValidateOrderItemAvailability(orderEntity, orderitemEntity, validationErrors);
            }
            else
            {
                ValidateOrderItemOrderable(orderitemEntity, validationErrors);
                ValidateOrderItemAvailability(orderEntity, orderitemEntity, validationErrors);
                ValidateOrderitemPrice(orderitemEntity, orderitem, validationErrors);
                ValidateOrderitemProductAlterations(orderitemEntity, orderitem, validationErrors, validateOrderLevelAlterations);
                ValidateAlterationitems(orderitemEntity, orderitem, validationErrors);
            }
        }

        private static void ValidateOrderItemOrderable(OrderitemEntity orderitemEntity, ICollection<ValidationError> validationErrors)
        {
            if (orderitemEntity.Product == null)
            {
                throw new CheckoutValidationException(ValidationErrorSubType.ProductInvalid);
            }

            if (!orderitemEntity.Product.IsOrderable())
            {
                validationErrors.Add(new OrderitemValidationError(ValidationErrorSubType.ProductOrderingDisabled, orderitemEntity.CategoryId, orderitemEntity.ProductId, null, null));
            }
        }

        private static void ValidateOrderItemAvailability(OrderEntity orderEntity, OrderitemEntity orderitemEntity, ICollection<ValidationError> validationErrors)
        {
            if (orderitemEntity.Product.Schedule.IsNullOrNew())
            {
                return;
            }

            if (!ScheduleAvailabilityValidator.IsAvailable(orderitemEntity.Product.Schedule, orderEntity.TimeZoneOlsonId))
            {
                validationErrors.Add(new OrderitemValidationError(ValidationErrorSubType.ProductNotAvailable, orderitemEntity.CategoryId, orderitemEntity.ProductId, null, null));
            }
        }

        private static void ValidateOrderitemPrice(OrderitemEntity orderitemEntity, OrderitemRequest orderitem, ICollection<ValidationError> validationErrors)
        {
            if (!orderitemEntity.ProductPriceIn.EqualsIgnoreNull(orderitem.Price))
            {
                validationErrors.Add(new OrderitemValidationError(ValidationErrorSubType.ProductPriceInvalid, orderitemEntity.CategoryId, orderitemEntity.ProductId, null, null));
            }
        }

        /// <summary>
        /// Validate request alterations based on product alterations min/max configuration
        /// </summary>
        private static void ValidateOrderitemProductAlterations(OrderitemEntity orderitemEntity, OrderitemRequest orderitem, ICollection<ValidationError> validationErrors, bool validateOrderLevelAlterations)
        {
            List<AlterationitemRequest> orderitemAlterationitems = orderitem.Alterationitems == null ? new List<AlterationitemRequest>() : orderitem.Alterationitems.ToList();

            foreach (ProductAlterationEntity productAlterationEntity in orderitemEntity.Product.ProductAlterationCollection)
            {
                AlterationEntity alterationEntity = productAlterationEntity.Alteration;

                if (alterationEntity.IsNullOrNew() || IsInvalidOrderLevel(validateOrderLevelAlterations, alterationEntity.OrderLevelEnabled) || 
                    alterationEntity.Type != AlterationType.Options || HasNoOrderOptions(alterationEntity))
                { continue; }

                int alterationitemRequestsCount = orderitemAlterationitems.Count(x => x.AlterationId == alterationEntity.AlterationId);

                if (alterationitemRequestsCount < alterationEntity.MinOptions ||
                    (alterationEntity.MaxOptions > 0 && alterationitemRequestsCount > alterationEntity.MaxOptions))
                {
                    validationErrors.Add(new OrderitemValidationError(ValidationErrorSubType.AlterationItemOptionsInvalid,
                                                                      orderitem.CategoryId,
                                                                      orderitem.ProductId,
                                                                      alterationEntity.AlterationId,
                                                                      null));
                }
            }
        }

        private static bool HasNoOrderOptions(AlterationEntity alterationEntity) => alterationEntity.MinOptions <= 0 && alterationEntity.MaxOptions <= 0;

        private static bool IsInvalidOrderLevel(bool validateOrderLevelAlterations, bool orderLevelEnabled) => !validateOrderLevelAlterations && orderLevelEnabled;

        private static void ValidateAlterationitems(OrderitemEntity orderitemEntity, OrderitemRequest orderitem, ICollection<ValidationError> validationErrors)
        {
            foreach (OrderitemAlterationitemEntity orderitemAlterationitemEntity in orderitemEntity.OrderitemAlterationitemCollection)
            {
                ValidateAlterationitem(orderitemAlterationitemEntity, orderitem, validationErrors);
            }
        }

        private static void ValidateAlterationitem(OrderitemAlterationitemEntity orderitemAlterationitemEntity, OrderitemRequest orderitem, ICollection<ValidationError> validationErrors)
        {
            switch (orderitemAlterationitemEntity.AlterationType)
            {
                case AlterationType.Options:
                    ValidateAlterationitemPrice(orderitemAlterationitemEntity, orderitem, validationErrors);
                    break;
                case AlterationType.Date:
                case AlterationType.DateTime:
                    ValidateAlterationitemTime(orderitemAlterationitemEntity, validationErrors);
                    break;
            }
        }

        private static void ValidateAlterationitemPrice(OrderitemAlterationitemEntity orderitemAlterationitemEntity, OrderitemRequest orderitem, ICollection<ValidationError> validationErrors)
        {
            AlterationitemRequest? alterationitem = orderitem.Alterationitems.FirstOrDefault(x => x.AlterationId == orderitemAlterationitemEntity.AlterationId && x.AlterationoptionId == orderitemAlterationitemEntity.Alterationitem.AlterationoptionId);
            if (alterationitem == null)
            {
                validationErrors.Add(new OrderitemValidationError(ValidationErrorSubType.AlterationitemInvalid, 
                                                                  orderitemAlterationitemEntity.Orderitem.CategoryId, 
                                                                  orderitemAlterationitemEntity.Orderitem.ProductId, 
                                                                  orderitemAlterationitemEntity.AlterationId, 
                                                                  orderitemAlterationitemEntity.Alterationitem.AlterationoptionId));
            }
            else if (!orderitemAlterationitemEntity.AlterationoptionPriceIn.EqualsIgnoreNull(alterationitem.Price))
            {
                validationErrors.Add(new OrderitemValidationError(ValidationErrorSubType.AlterationitemPriceInvalid, 
                                                                  orderitemAlterationitemEntity.Orderitem.CategoryId, 
                                                                  orderitemAlterationitemEntity.Orderitem.ProductId, 
                                                                  orderitemAlterationitemEntity.AlterationId, 
                                                                  orderitemAlterationitemEntity.Alterationitem.AlterationoptionId));
            }
        }

        private static void ValidateAlterationitemTime(OrderitemAlterationitemEntity orderitemAlterationitemEntity, ICollection<ValidationError> validationErrors)
        {
            if (!orderitemAlterationitemEntity.TimeUTC.HasValue)
            {
                validationErrors.Add(new OrderitemValidationError(ValidationErrorSubType.AlterationitemTimeInvalid, orderitemAlterationitemEntity.Orderitem.CategoryId, orderitemAlterationitemEntity.Orderitem.ProductId, orderitemAlterationitemEntity.AlterationId, null));
            }
        }
    }
}
