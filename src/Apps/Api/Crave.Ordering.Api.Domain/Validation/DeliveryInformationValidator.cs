﻿using Crave.Enums;
using Crave.Ordering.Api.Core.Enums;
using Crave.Ordering.Api.Domain.Enums;
using Crave.Ordering.Api.Domain.Exceptions;
using Crave.Ordering.Api.Domain.Interfaces.UseCases;
using Crave.Ordering.Api.Domain.Models;
using Crave.Ordering.Api.Domain.UseCases.Requests;
using Crave.Ordering.Api.Domain.UseCases.Responses;
using Crave.Ordering.Api.Domain.Validation.Errors;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.Extensions;
using Crave.Ordering.Shared.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DeliveryRateResult = Crave.Ordering.Api.Domain.Enums.DeliveryRateResult;

namespace Crave.Ordering.Api.Domain.Validation
{
	public class DeliveryInformationValidator
	{
		private readonly IGetDeliveryRateUseCase getDeliveryRateUseCase;

		public DeliveryInformationValidator(IGetDeliveryRateUseCase getDeliveryRateUseCase) => this.getDeliveryRateUseCase = getDeliveryRateUseCase;

		public async Task ValidateAsync(OrderEntity orderEntity, ICollection<ValidationError> validationErrors)
		{
			
			if (!orderEntity.ServiceMethodId.HasValue)
			{
				return;
			}

			if (orderEntity.ServiceMethod.IsDeliveryInformationRequired())
			{
				if (orderEntity.DeliveryInformation == null)
				{
					throw new CheckoutValidationException(ValidationErrorSubType.DeliveryInformationRequired);
				}

				ValidateDeliveryAddress(orderEntity, validationErrors);
				await ValidateDeliveryCoordinates(orderEntity, validationErrors);
			}
			else if (orderEntity.DeliveryInformation != null)
			{
				throw new CheckoutValidationException(ValidationErrorSubType.DeliveryInformationNotRequired);
			}
		}

		private static void ValidateDeliveryAddress(OrderEntity orderEntity, ICollection<ValidationError> validationErrors)
		{
			if (orderEntity.DeliveryInformation.Address.IsNullOrWhiteSpace())
			{
				validationErrors.Add(new ValidationError(ValidationErrorSubType.DeliveryInformationAddressMissing));
			}
			else if (orderEntity.DeliveryInformation.Zipcode.IsNullOrWhiteSpace())
			{
				validationErrors.Add(new ValidationError(ValidationErrorSubType.DeliveryInformationZipCodeMissing));
			}
			else if (orderEntity.DeliveryInformation.City.IsNullOrWhiteSpace())
			{
				validationErrors.Add(new ValidationError(ValidationErrorSubType.DeliveryInformationCityMissing));
			}
		}

		private async Task ValidateDeliveryCoordinates(OrderEntity orderEntity, ICollection<ValidationError> validationErrors)
		{
			DeliveryLocation deliveryLocation = new DeliveryLocation(orderEntity.DeliveryInformation.Longitude, orderEntity.DeliveryInformation.Latitude);

			// Awaiting response synchronously atm as all methods are currently synchronous
			GetDeliveryRateResponse response = await getDeliveryRateUseCase.ExecuteAsync(new GetDeliveryRateRequest(orderEntity.ServiceMethod.ServiceMethodId, deliveryLocation, orderEntity.OrderitemTotalDisplayPrice));

			switch (response.Result)
			{
				case DeliveryRateResult.OK:
					ValidateDeliveryCharge(orderEntity, response.Charge, validationErrors);
					break;
				case DeliveryRateResult.NoDeliveryDistanceSpecifiedForServiceMethod:
					validationErrors.Add(new ValidationError(ValidationErrorSubType.NoDeliveryDistanceSpecifiedForServiceMethod));
					break;
				case DeliveryRateResult.OutOfRange:
					validationErrors.Add(new ValidationError(ValidationErrorSubType.DeliveryLocationOutsideDeliveryArea));
					break;
				case DeliveryRateResult.UnableToRetrieveDistanceBetweenOutletAndDeliveryLocation:
					validationErrors.Add(new ValidationError(ValidationErrorSubType.UnableToRetrieveDistanceBetweenOutletAndDeliveryLocation));
					break;
			}
		}

		private static void ValidateDeliveryCharge(OrderEntity orderEntity, decimal expectedDeliveryCharge, ICollection<ValidationError> validationErrors)
		{
			OrderitemEntity deliveryChargeOrderitemEntity = orderEntity.OrderitemCollection.FirstOrDefault(x => x.Type == OrderitemType.DeliveryCharge);
			decimal requestedDeliveryCharge = deliveryChargeOrderitemEntity?.DisplayPrice ?? 0;

			if (!requestedDeliveryCharge.EqualsIgnoreRounding(expectedDeliveryCharge))
			{
				validationErrors.Add(new ValidationError(ValidationErrorSubType.DeliveryChargePriceInvalid));
			}
		}
	}
}
