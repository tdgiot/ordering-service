﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Crave.Ordering.Api.Domain.Extensions;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using NodaTime;

namespace Crave.Ordering.Api.Domain.Validation
{
    public static class BusinesshoursValidator
    {
        private const string BUSINESS_HOURS_FORMAT = "hhmm";
        private const int EVEN_DIVISOR = 2;
        private const int BUSINESS_HOURS_SPAN = 2;

        public static bool IsWithinBusinesshours(EntityCollection<BusinesshourEntity> businesshourCollection, string timezoneOlsonId)
        {
            if (businesshourCollection == null || !businesshourCollection.Any())
            {
                return true;
            }

            if (string.IsNullOrWhiteSpace(timezoneOlsonId))
            {
                throw new ArgumentNullException(nameof(timezoneOlsonId));
            }

            // TODO: Extract DateTime.UtcNow into separate service/component to improve testability.
            ZonedDateTime zonedDateTime = DateTime.UtcNow.ConvertToZonedDateTime(timezoneOlsonId);
            DayOfWeek localDayOfWeek = NodaTime.Utility.BclConversions.ToDayOfWeek(zonedDateTime.DayOfWeek);

            IOrderedEnumerable<BusinesshourEntity> todaysBusinessHours = businesshourCollection.Where(bh => ((DayOfWeek)bh.GetDay()) == localDayOfWeek).OrderBy(x => x.DayOfWeekAndTime);
            if (todaysBusinessHours.Count() % EVEN_DIVISOR != 0)
            {
                throw new InvalidOperationException("The business hours contains an odd amount of time schedules.");
            }

            return IsNowWithinBusinessHours(todaysBusinessHours, zonedDateTime);
        }

        private static bool IsNowWithinBusinessHours(IOrderedEnumerable<BusinesshourEntity> todaysBusinessHours, ZonedDateTime zonedDateTime)
        {
            for (int i = 0; i < todaysBusinessHours.Count(); i += BUSINESS_HOURS_SPAN)
            {
                IEnumerable<BusinesshourEntity> businesshourSet = todaysBusinessHours.Skip(i).Take(2);
                bool parsedOpeningTime = TimeSpan.TryParseExact(businesshourSet.FirstOrDefault(bh => bh.Opening).GetTime().ToString().PadLeft(4, '0'),
                    BUSINESS_HOURS_FORMAT, CultureInfo.InvariantCulture, TimeSpanStyles.None, out TimeSpan openingTime);

                bool parsedClosingTime = TimeSpan.TryParseExact(businesshourSet.FirstOrDefault(bh => !bh.Opening).GetTime().ToString().PadLeft(4, '0'),
                    BUSINESS_HOURS_FORMAT, CultureInfo.InvariantCulture, TimeSpanStyles.None, out TimeSpan closingTime);

                if (!parsedOpeningTime || !parsedClosingTime)
                {
                    throw new InvalidCastException(string.Format("Failed to cast the {0} business hours value: {1}",
                        !parsedOpeningTime ? "opening" : "closing",
                        !parsedOpeningTime ? businesshourSet.FirstOrDefault(bh => bh.Opening).GetTime().ToString() : businesshourSet.FirstOrDefault(bh => !bh.Opening).GetTime().ToString()));
                }

                TimeSpan currentTime = new TimeSpan(zonedDateTime.TimeOfDay.Hour, zonedDateTime.TimeOfDay.Minute, zonedDateTime.TimeOfDay.Second);
                if (currentTime.Ticks >= openingTime.Ticks && currentTime.Ticks <= closingTime.Ticks)
                {
                    return true;
                }
            }

            return false;
        }
    }
}
