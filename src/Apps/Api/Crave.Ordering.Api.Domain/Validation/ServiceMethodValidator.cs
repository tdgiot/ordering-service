﻿using Crave.Ordering.Api.Core.Enums;
using Crave.Ordering.Api.Domain.Exceptions;
using Crave.Ordering.Api.Domain.Validation.Errors;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.Extensions;
using System;
using System.Collections.Generic;

namespace Crave.Ordering.Api.Domain.Validation
{
    public static class ServiceMethodValidator
    {
        public static void Validate(OrderEntity orderEntity, ICollection<ValidationError> validationErrors)
        {
            ServiceMethodEntity serviceMethodEntity = orderEntity.ServiceMethod;
            if (!serviceMethodEntity.IsNullOrNew())
            {
                ValidateServiceMethodActive(orderEntity, validationErrors);
                ValidateCustomerLastName(orderEntity, validationErrors);
                ValidateDeliverypoint(orderEntity, validationErrors);
                ValidateCovers(orderEntity, validationErrors, serviceMethodEntity);

            }
            else if (!orderEntity.DeliverypointId.HasValue && orderEntity.Email.IsNullOrWhiteSpace())
            {
                throw new CheckoutValidationException(ValidationErrorSubType.DeliverypointOrEmailRequired);
            }
        }

        private static void ValidateCovers(OrderEntity orderEntity, ICollection<ValidationError> validationErrors, ServiceMethodEntity serviceMethodEntity)
        {
            if (orderEntity.CoversCount.HasValue && serviceMethodEntity.CoversType == Crave.Enums.CoversType.NeverAsk)
            {
                validationErrors.Add(new ValidationError(ValidationErrorSubType.CoversCountNotAllowed));
            }

            if (!orderEntity.CoversCount.HasValue && serviceMethodEntity.CoversType == Crave.Enums.CoversType.AlwaysAsk)
            {
                validationErrors.Add(new ValidationError(ValidationErrorSubType.CoversCountRequired));
            }

            if (orderEntity.CoversCount.HasValue && orderEntity.CoversCount < 1)
            {
                validationErrors.Add(new ValidationError(ValidationErrorSubType.InvalidCoversCountValue));

            }
        }

        private static void ValidateServiceMethodActive(OrderEntity orderEntity, ICollection<ValidationError> validationErrors)
        {
            if (!orderEntity.ServiceMethod.Active)
            {
                validationErrors.Add(new ValidationError(ValidationErrorSubType.ServiceMethodNotActive));
            }
        }

        private static void ValidateCustomerLastName(OrderEntity orderEntity, ICollection<ValidationError> validationErrors)
        {
            if (orderEntity.ServiceMethod.IsCustomerLastNameRequired() && orderEntity.CustomerLastname.IsNullOrWhiteSpace())
            {
                validationErrors.Add(new ValidationError(ValidationErrorSubType.CustomerLastNameInvalid));
            }
        }

        private static void ValidateDeliverypoint(OrderEntity orderEntity, ICollection<ValidationError> validationErrors)
        {
            if (orderEntity.ServiceMethod.IsDeliverypointRequired() && orderEntity.DeliverypointId.GetValueOrDefault(0) <= 0)
            {
                validationErrors.Add(new ValidationError(ValidationErrorSubType.ServiceMethodDeliveryPointRequired));
            }

            if (!orderEntity.ServiceMethod.IsDeliverypointRequired() && orderEntity.DeliverypointId.GetValueOrDefault(0) > 0)
            {
                throw new CheckoutValidationException(ValidationErrorSubType.ServiceMethodDeliveryPointNotRequired);
            }
        }
    }
}
