﻿using Crave.Enums;
using Crave.Ordering.Api.Core.Enums;
using Crave.Ordering.Api.Domain.Exceptions;
using Crave.Ordering.Api.Domain.Validation.Errors;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.Extensions;
using Crave.Ordering.Shared.Extensions;
using System.Collections.Generic;
using System.Linq;

namespace Crave.Ordering.Api.Domain.Validation
{
    public static class ServiceChargeValidator
    {
        public static void Validate(OrderEntity orderEntity, ICollection<ValidationError> validationErrors)
        {
            if (!orderEntity.OutletId.HasValue)
            {
                return;
            }

            OrderitemEntity serviceChargeOrderitem = orderEntity.OrderitemCollection.FirstOrDefault(x => x.Type == OrderitemType.ServiceCharge);

            if (orderEntity.ServiceChargeRequired())
            {
                if (serviceChargeOrderitem == null)
                {
                    throw new CheckoutValidationException(ValidationErrorSubType.ServiceChargeRequired);
                }

                ValidateServiceCharge(orderEntity, serviceChargeOrderitem, validationErrors);
            }
            else if (serviceChargeOrderitem != null)
            {
                throw new CheckoutValidationException(ValidationErrorSubType.ServiceChargeNotRequired);
            }
        }

        private static void ValidateServiceCharge(OrderEntity orderEntity, OrderitemEntity serviceChargeOrderitem, ICollection<ValidationError> validationErrors)
        {
            decimal requestedServiceCharge = serviceChargeOrderitem.ProductPriceIn;

            if (orderEntity.ServiceMethod.IsFreeServiceChargeActive() && orderEntity.ServiceMethod.IsMinimumPriceReachedForFreeServiceCharge(orderEntity.OrderitemTotalDisplayPrice))
            {
                if (requestedServiceCharge != 0)
                {
                    validationErrors.Add(new ValidationError(ValidationErrorSubType.ServiceChargeInvalid));
                }
            }
            else if (orderEntity.Outlet.ServiceChargeProductId.HasValue)
            {
                decimal? expectedServiceCharge = null;

                if (orderEntity.ServiceMethod.ServiceChargePercentage.GetValueOrDefault(0) > 0)
                {
                    expectedServiceCharge = orderEntity.ServiceMethod.CalculateServiceCharge(orderEntity.OrderitemTotalDisplayPrice);
                }
                else if (orderEntity.ServiceMethod.ServiceChargeAmount.GetValueOrDefault(0) > 0)
                {
                    expectedServiceCharge = orderEntity.ServiceMethod.ServiceChargeAmount.GetValueOrDefault(0);
                }

                if (expectedServiceCharge != null && !requestedServiceCharge.EqualsIgnoreRounding(expectedServiceCharge.Value))
                {
                    validationErrors.Add(new ValidationError(ValidationErrorSubType.ServiceChargeInvalid));
                }
            }
        }
    }
}
