﻿using System;
using System.Text.RegularExpressions;

namespace Crave.Ordering.Api.Domain.Utils
{
    public static class PhoneNumberUtil
    {
        /// <summary>
        /// Formats a phone number into a standardized way.
        /// </summary>
        /// <remarks>
        /// - Replaces plus sign prefix with zeros (turns +31 to 0031).<br/>
        /// - Removes all non digit characters.
        /// </remarks>
        /// <param name="phoneNumber">The phone number</param>
        /// <returns>
        /// A standardized phone number or empty string when input is null or empty.<br/> 
        /// </returns>
        public static string Standardize(string phoneNumber)
        {
            if (phoneNumber.IsNullOrWhiteSpace())
            {
                return string.Empty;
            }

            phoneNumber = phoneNumber.Trim();
            phoneNumber = PhoneNumberUtil.ReplaceCallingCodePlusSignWithZeros(phoneNumber);
            phoneNumber = PhoneNumberUtil.RemoveAllNonDigitCharacters(phoneNumber);

            return phoneNumber;
        }

        private static string RemoveAllNonDigitCharacters(string phoneNumber) => Regex.Replace(phoneNumber, @"\D", string.Empty);

        private static string ReplaceCallingCodePlusSignWithZeros(string phoneNumber) => Regex.Replace(phoneNumber, @"^\+", "00");
    }
}
