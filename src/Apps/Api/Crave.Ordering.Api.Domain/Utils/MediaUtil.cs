﻿using System;
using System.Collections.Generic;
using System.Reflection;
using Crave.Attributes;
using Crave.Enums;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Ordering.Api.Domain.Utils
{
    public static class MediaUtil
    {
        /// <summary>
        /// Gets a list of allowed device types.
        /// </summary>
        private static ICollection<DeviceType> DeviceTypes => new List<DeviceType>
        {
            DeviceType.Archos70,
            DeviceType.Tablet1280x800,
            DeviceType.AndroidTvBox,
            DeviceType.PhoneSmall,
            DeviceType.PhoneNormal,
            DeviceType.PhoneLarge,
            DeviceType.PhoneXLarge,
            DeviceType.TabletSmall,
            DeviceType.TabletNormal,
            DeviceType.TabletLarge,
            DeviceType.TabletXLarge,
            DeviceType.iPhone,
            DeviceType.iPhoneRetina,
            DeviceType.iPhoneRetinaHD,
            DeviceType.iPhoneRetinaHDPlus,
            DeviceType.iPad,
            DeviceType.iPadRetina,
            DeviceType.Unknown // Needed for Attachments
        };

        /// <summary>
        /// Gets the mappings for entity types and their corresponding subdirectories.
        /// </summary>
        private static Dictionary<EntityType, string> DirectoryMappings => new Dictionary<EntityType, string>
        {
            { EntityType.AlterationEntity, MediaConstants.AlterationsSubdirectory },
            { EntityType.CategoryEntity, MediaConstants.CategoriesSubdirectory },
            { EntityType.CompanyEntity, MediaConstants.CompanySubdirectory },
            { EntityType.ClientConfigurationEntity, MediaConstants.DeliverypointgroupSubdirectory },
            { EntityType.DeliverypointgroupEntity, MediaConstants.DeliverypointgroupSubdirectory },
            { EntityType.ProductEntity, MediaConstants.ProductsSubdirectory },
            { EntityType.RoutestephandlerEntity, MediaConstants.RoutestephandlerSubdirectory }
        };


        /// <summary>
        /// Gets the name of the subdirectory for the related entity of the media entity.
        /// </summary>
        /// <param name="mediaRatioTypeMedia">The <see cref="MediaRatioTypeMediaEntity"/> instance to get the subdirectory name for.</param>
        /// <returns>A string instance containing the name of the subdirectory.</returns>
        public static string GetEntitySubDirectoryName(MediaRatioTypeMediaEntity mediaRatioTypeMedia)
        {
            string dir = string.Empty;

            if (DeviceTypes.Contains(mediaRatioTypeMedia.MediaRatioType.DeviceType) && DirectoryMappings.ContainsKey(mediaRatioTypeMedia.Media.RelatedEntityType))
            {
                dir = DirectoryMappings[mediaRatioTypeMedia.Media.RelatedEntityType];
            }

            if (dir.IsNullOrWhiteSpace())
            {
                throw new ApplicationException($"Entity sub directory name couldn't be determined for related entity type {mediaRatioTypeMedia.Media.RelatedEntityType}!");
            }

            return dir;
        }

        /// <summary>
        /// Gets the non generic media type for a generic media type.
        /// </summary>
        /// <param name="genericMediaType">The generic media type to convert to it's non generic variant.</param>
        /// <returns>A <see cref="MediaType"/> instance containing the non generic media type if it was found.</returns>
        public static MediaType GetNonGenericMediaTypeForGenericMediaType(MediaType genericMediaType)
        {
            // Set the default
            MediaType nonGenericMediaType = genericMediaType;

            // Get FieldInfo for the Type
            FieldInfo fieldInfo = genericMediaType.GetType().GetField(genericMediaType.ToString());

            // Get the NonGenericMediaTypeAttribute from the Enum value
            NonGenericMediaTypeAttribute nonGenericMediaTypeAttribute = fieldInfo.GetCustomAttribute<NonGenericMediaTypeAttribute>(false);
            if (nonGenericMediaTypeAttribute != null)
            {
                nonGenericMediaType = nonGenericMediaTypeAttribute.NonGenericMediaType;
            }

            return nonGenericMediaType;
        }

        /// <summary>
        /// Gets the file path of the specified media ratio type media entity.
        /// </summary>
        /// <param name="mediaRatioTypeMedia">The media ratio type media entity to get the file path for.</param>
        /// <param name="fileNameType">The type of the file name.</param>
        /// <returns>A string containing the file path.</returns>
        public static string GetMediaRatioTypeMediaPath(MediaRatioTypeMediaEntity mediaRatioTypeMedia, MediaRatioTypeMediaEntity.FileNameType fileNameType)
        {
            List<string> pathComponents = new List<string>
            {
                MediaConstants.FilesDirectory,
                mediaRatioTypeMedia.Media.RelatedCompanyId.HasValue ? mediaRatioTypeMedia.Media.RelatedCompanyId.ToString() : MediaConstants.GenericFilesSubdirectory
            };

            if (mediaRatioTypeMedia.MediaRatioType != null)
            {
                // Add DeviceType                       
                pathComponents.Add(mediaRatioTypeMedia.MediaRatioType.DeviceType.ToString());
            }

            return $"{string.Join("/", pathComponents)}/";
        }
    }
}
