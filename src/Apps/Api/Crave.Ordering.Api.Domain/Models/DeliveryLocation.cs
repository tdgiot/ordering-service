﻿namespace Crave.Ordering.Api.Domain.Models
{
    public class DeliveryLocation
    {
        public DeliveryLocation(double longitude, 
                                double latitude)
        {
            this.Longitude = longitude;
            this.Latitude = latitude;
        }

        public double Longitude { get; }
        public double Latitude { get; }
    }
}
