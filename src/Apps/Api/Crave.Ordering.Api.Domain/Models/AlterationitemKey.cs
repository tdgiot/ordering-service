﻿namespace Crave.Ordering.Api.Domain.Models
{
    public class AlterationitemKey
    {
        private int AlterationId { get; }
        private int AlterationoptionId { get; }

        public AlterationitemKey(int alterationId, int alterationoptionId)
        {
            AlterationId = alterationId;
            AlterationoptionId = alterationoptionId;
        }

        public static implicit operator string(AlterationitemKey key) => AlterationitemKey.FormatKey(key);

        private static string FormatKey(AlterationitemKey key)
        {
            return $"{key.AlterationId}-{key.AlterationoptionId}";
        }
    }
}
