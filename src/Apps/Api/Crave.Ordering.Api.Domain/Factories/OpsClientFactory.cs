﻿using System;
using Crave.Enums;
using Crave.Integrations.Ops.Alice;
using Crave.Integrations.Ops.HotSOS;
using Crave.Integrations.Ops.Hyatt;
using Crave.Integrations.Ops.Interfaces;
using Crave.Integrations.Ops.Quore;
using Crave.Ordering.Api.Domain.Extensions;
using Crave.Ordering.Api.Domain.Interfaces;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Ordering.Api.Domain.Factories
{
    public class OpsClientFactory : IOpsClientFactory
    {
        public IOpsClient Create(ExternalSystemEntity externalSystemEntity)
        {
            IOpsClient client;
            
            switch (externalSystemEntity.Type)
            {
                case ExternalSystemType.HotSOS:
                    client = new HotSOSClient(externalSystemEntity.ToHotSOSConfiguration());
                    break;
                case ExternalSystemType.Quore:
                    client = new QuoreClient(externalSystemEntity.ToQuoreConfiguration());
                    break;
                case ExternalSystemType.Hyatt_HotSOS:
                    client = new HyattClient(externalSystemEntity.ToHyattConfiguration());
                    break;
                case ExternalSystemType.Alice:
                    client = new AliceClient(externalSystemEntity.ToAliceConfiguration());
                    break;
                default:
                    throw new ArgumentOutOfRangeException($"Unable to instantiate an ops client for type {externalSystemEntity.Type}");
            }

            return client;
        }
    }
}
