﻿using System.Collections.Generic;
using System.Linq;
using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;

namespace Crave.Ordering.Api.Domain.UseCases
{
    public class EnrichOrderitemsWithTagsUseCase
    {
        private readonly IProductTagRepository productTagRepository;
        private readonly IProductCategoryTagRepository productCategoryTagRepository;
        private readonly IAlterationoptionTagRepository alterationoptionTagRepository;

        public EnrichOrderitemsWithTagsUseCase(IProductTagRepository productTagRepository,
            IProductCategoryTagRepository productCategoryTagRepository,
            IAlterationoptionTagRepository alterationoptionTagRepository)
        {
            this.productTagRepository = productTagRepository;
            this.productCategoryTagRepository = productCategoryTagRepository;
            this.alterationoptionTagRepository = alterationoptionTagRepository;
        }

        public void Execute(EntityCollection<OrderitemEntity> orderItems)
        {
            EntityCollection<ProductTagEntity> productTags = FetchProductTags(orderItems);
            EntityCollection<ProductCategoryTagEntity> productCategoryTags = FetchProductCategoryTags(orderItems);
            EntityCollection<AlterationoptionTagEntity> alterationoptionTags = FetchAlterationoptionTags(orderItems);

            EnrichOrderItemsWithTags(orderItems, productTags, productCategoryTags);
            EnrichOrderItemAlterationoptionsWithTags(orderItems, alterationoptionTags);

        }

        private void EnrichOrderItemsWithTags(EntityCollection<OrderitemEntity> orderItems, EntityCollection<ProductTagEntity> productTags, EntityCollection<ProductCategoryTagEntity> productCategoryTags)
        {
            ILookup<int, int> productTagLookup = productTags.ToLookup(k => k.ProductId, v => v.TagId);
            ILookup<int, int> productCategoryTagLookup = productCategoryTags.ToLookup(k => k.CategoryId, v => v.TagId);

            foreach (OrderitemEntity orderItem in orderItems)
            {
                int productId = orderItem.ProductId.GetValueOrDefault(0);
                int categoryId = orderItem.CategoryId.GetValueOrDefault(0);

                List<int> productTagIds = new List<int>();
                if (productTagLookup.Contains(productId))
                {
                    productTagIds.AddRange(productTagLookup[productId]);
                }

                List<int> productCategoryTagIds = new List<int>();
                if (productCategoryTagLookup.Contains(categoryId))
                {
                    productCategoryTagIds.AddRange(productCategoryTagLookup[categoryId]);
                }

                List<int> orderitemTagIds = productTagIds.Union(productCategoryTagIds).ToList();
                foreach (int tagId in orderitemTagIds)
                {
                    orderItem.OrderitemTagCollection.Add(new OrderitemTagEntity
                    {
                        TagId = tagId,
                        ParentCompanyId = orderItem.ParentCompanyId
                    });
                }
            }
        }

        private void EnrichOrderItemAlterationoptionsWithTags(EntityCollection<OrderitemEntity> orderItems, EntityCollection<AlterationoptionTagEntity> alterationoptionTags)
        {
            ILookup<int, int> alterationoptionTagLookup = alterationoptionTags.ToLookup(k => k.AlterationoptionId, v => v.TagId);

            foreach (OrderitemAlterationitemEntity alterationItem in orderItems.SelectMany(orderItem => orderItem.OrderitemAlterationitemCollection))
            {
                int alterationoptionId = alterationItem.AlterationoptionId.GetValueOrDefault(0);

                IEnumerable<int> alterationoptionTagIds = Enumerable.Empty<int>();
                if (alterationoptionTagLookup.Contains(alterationoptionId))
                {
                    alterationoptionTagIds = alterationoptionTagLookup[alterationoptionId];
                }

                foreach (int tagId in alterationoptionTagIds)
                {
                    alterationItem.OrderitemAlterationitemTagCollection.Add(new OrderitemAlterationitemTagEntity
                    {
                        TagId = tagId,
                        ParentCompanyId = alterationItem.ParentCompanyId
                    });
                }
            }
        }

        private EntityCollection<ProductTagEntity> FetchProductTags(EntityCollection<OrderitemEntity> orderitemEntities)
        {
            IReadOnlyList<int> productIds = GetProductIds(orderitemEntities);

            return productTagRepository.GetProductTags(productIds);
        }

        private EntityCollection<ProductCategoryTagEntity> FetchProductCategoryTags(EntityCollection<OrderitemEntity> orderitemEntities)
        {
            IReadOnlyList<int> productIds = GetProductIds(orderitemEntities);
            IReadOnlyList<int> categoryIds = GetCategoryIds(orderitemEntities);

            return productCategoryTagRepository.GetProductCategoryTags(productIds, categoryIds);
        }

        private EntityCollection<AlterationoptionTagEntity> FetchAlterationoptionTags(EntityCollection<OrderitemEntity> orderitemEntities)
        {
            IReadOnlyList<int> alterationoptionIds = GetAlterationoptionIds(orderitemEntities);

            return alterationoptionTagRepository.GetAlterationoptionTags(alterationoptionIds);
        }

        private static IReadOnlyList<int> GetProductIds(EntityCollection<OrderitemEntity> orderitemEntities) => orderitemEntities
            .Where(orderitem => orderitem.ProductId.HasValue)
            .Select(orderitem => orderitem.ProductId.GetValueOrDefault(0))
            .ToList();

        private static IReadOnlyList<int> GetCategoryIds(EntityCollection<OrderitemEntity> orderitemEntities) => orderitemEntities
            .Where(orderitem => orderitem.CategoryId.HasValue)
            .Select(orderitem => orderitem.CategoryId.GetValueOrDefault(0))
            .ToList();

        private static IReadOnlyList<int> GetAlterationoptionIds(EntityCollection<OrderitemEntity> orderitemEntities) => orderitemEntities
            .SelectMany(orderitem => orderitem.OrderitemAlterationitemCollection)
            .Select(alterationitem => alterationitem.AlterationoptionId.GetValueOrDefault(0))
            .ToList();
    }
}
