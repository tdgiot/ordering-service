﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Crave.Ordering.Api.Domain.Interfaces.UseCases;
using Crave.Ordering.Api.Domain.UseCases.Requests;
using Crave.Ordering.Api.Domain.UseCases.Responses;
using Crave.Ordering.Api.Domain.Validation;
using Crave.Ordering.Api.Domain.Validation.Errors;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Ordering.Api.Domain.UseCases
{
    /// <summary>
    /// Validates an order.
    /// </summary>
    public class ValidateOrderUseCase : IValidateOrderUseCase
    {
        private readonly ConvertOrderToEntityUseCase convertOrderToEntityUseCase;
        private readonly DeliveryInformationValidator deliveryInformationValidator;

        public ValidateOrderUseCase(ConvertOrderToEntityUseCase convertOrderToEntityUseCase,
                                    DeliveryInformationValidator deliveryInformationValidator)
        {
            this.convertOrderToEntityUseCase = convertOrderToEntityUseCase;
            this.deliveryInformationValidator = deliveryInformationValidator;
        }

        /// <inheritdoc />
        public async Task<ValidateOrderResponse> ExecuteAsync(CreateOrderRequest request)
        {
            OrderEntity orderEntity = convertOrderToEntityUseCase.Execute(request);

            List<ValidationError> errorList = new List<ValidationError>();

            OutletValidator.Validate(orderEntity, errorList);
            ServiceMethodValidator.Validate(orderEntity, errorList);
            CheckoutMethodValidator.Validate(orderEntity, errorList);
            OrderitemValidator.Validate(orderEntity, request.Orderitems, errorList, true);
            await deliveryInformationValidator.ValidateAsync(orderEntity, errorList);
            TippingValidator.Validate(orderEntity, errorList);
            ServiceChargeValidator.Validate(orderEntity, errorList);

            return new ValidateOrderResponse(orderEntity, new ValidationResponse(errorList));
        }
    }
}