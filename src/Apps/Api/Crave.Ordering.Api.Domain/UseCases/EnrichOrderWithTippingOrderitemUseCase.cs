﻿using System;
using Crave.Enums;
using Crave.Ordering.Api.Domain.UseCases.Requests;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Ordering.Api.Domain.UseCases
{
    public static class EnrichOrderWithTippingOrderitemUseCase
    {
        private const int DECIMAL_PLACES = 2;

        public static void Execute(OrderEntity orderEntity, TippingRequest? tipping)
        {
            if (tipping == null || !orderEntity.Outlet.TippingProductId.HasValue)
            {
                return;
            }

            OrderitemEntity orderitemEntity = new OrderitemEntity();
            orderitemEntity.Type = OrderitemType.Tip;
            orderitemEntity.ProductDescription = "Tipping";
            orderitemEntity.ProductId = orderEntity.Outlet.TippingProductId.Value;
            orderitemEntity.Quantity = 1;
            orderitemEntity.ProductPriceIn = Math.Round(tipping.Price, DECIMAL_PLACES, MidpointRounding.AwayFromZero);

            orderEntity.OrderitemCollection.Add(orderitemEntity);
        }
    }
}
