﻿using System;
using Crave.Ordering.Api.Domain.UseCases.Requests;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Ordering.Api.Domain.UseCases
{
    public class ConvertOrderToEntityUseCase : UseCaseBase<CreateOrderRequest, OrderEntity>
    {
        private readonly EnrichOrderWithOutletDetailsUseCase enrichOrderWithOutletDetailsUseCase;
        private readonly EnrichOrderWithCompanyDetailsUseCase enrichOrderWithCompanyDetailsUseCase;
        private readonly EnrichOrderWithServiceMethodDetailsUseCase enrichOrderWithServiceMethodUseCase;
        private readonly EnrichOrderWithCheckoutMethodDetailsUseCase enrichOrderWithCheckoutMethodUseCase;
        private readonly EnrichOrderWithDeliverypointDetailsUseCase enrichOrderWithDeliverypointUseCase;
        private readonly EnrichOrderWithChargeToDeliverypointUseCase enrichOrderWithChargeToDeliverypointUseCase;
        private readonly EnrichOrderWithDeliveryInformationUseCase enrichOrderWithDeliveryInformationUseCase;
        private readonly EnrichOrderWithOrderitemsUseCase enrichOrderWithOrderitemsUseCase;
        private readonly EnrichOrderitemsWithTagsUseCase enrichOrderitemsWithTagsUseCase;
        private readonly EnrichOrderWithTaxesUseCase enrichOrderWithTaxesUseCase;
        private readonly EnrichOrderWithOrderStatusUseCase enrichOrderWithOrderStatusUseCase;
        private readonly EnrichOrderWithClientUseCase enrichOrderWithClientUseCase;
        private readonly EnrichOrderWithAnalyticsUseCase enrichOrderWithAnalyticsUseCase;

        public ConvertOrderToEntityUseCase(EnrichOrderWithOutletDetailsUseCase enrichOrderWithOutletDetailsUseCase, 
                                           EnrichOrderWithCompanyDetailsUseCase enrichOrderWithCompanyDetailsUseCase,
                                           EnrichOrderWithServiceMethodDetailsUseCase enrichOrderWithServiceMethodUseCase,
                                           EnrichOrderWithCheckoutMethodDetailsUseCase enrichOrderWithCheckoutMethodUseCase,
                                           EnrichOrderWithDeliverypointDetailsUseCase enrichOrderWithDeliverypointUseCase,
                                           EnrichOrderWithChargeToDeliverypointUseCase enrichOrderWithChargeToDeliverypointUseCase,
                                           EnrichOrderWithDeliveryInformationUseCase enrichOrderWithDeliveryInformationUseCase,
                                           EnrichOrderWithOrderitemsUseCase enrichOrderWithOrderitemsUseCase,
                                           EnrichOrderWithTaxesUseCase enrichOrderWithTaxesUseCase,
                                           EnrichOrderWithOrderStatusUseCase enrichOrderWithOrderStatusUseCase,
                                           EnrichOrderWithClientUseCase enrichOrderWithClientUseCase,
                                           EnrichOrderWithAnalyticsUseCase enrichOrderWithAnalyticsUseCase,
                                           EnrichOrderitemsWithTagsUseCase enrichOrderitemsWithTagsUseCase)
        {
            this.enrichOrderWithOutletDetailsUseCase = enrichOrderWithOutletDetailsUseCase;
            this.enrichOrderWithCompanyDetailsUseCase = enrichOrderWithCompanyDetailsUseCase;
            this.enrichOrderWithServiceMethodUseCase = enrichOrderWithServiceMethodUseCase;
            this.enrichOrderWithCheckoutMethodUseCase = enrichOrderWithCheckoutMethodUseCase;
            this.enrichOrderWithDeliverypointUseCase = enrichOrderWithDeliverypointUseCase;
            this.enrichOrderWithChargeToDeliverypointUseCase = enrichOrderWithChargeToDeliverypointUseCase;
            this.enrichOrderWithDeliveryInformationUseCase = enrichOrderWithDeliveryInformationUseCase;
            this.enrichOrderWithOrderitemsUseCase = enrichOrderWithOrderitemsUseCase;
            this.enrichOrderWithTaxesUseCase = enrichOrderWithTaxesUseCase;
            this.enrichOrderWithOrderStatusUseCase = enrichOrderWithOrderStatusUseCase;
            this.enrichOrderWithClientUseCase = enrichOrderWithClientUseCase;
            this.enrichOrderWithAnalyticsUseCase = enrichOrderWithAnalyticsUseCase;
            this.enrichOrderitemsWithTagsUseCase = enrichOrderitemsWithTagsUseCase;
        }

        public override OrderEntity Execute(CreateOrderRequest request)
        {
            OrderEntity orderEntity = new OrderEntity
            {
                Guid = GetOrCreateGuid(request),
                Type = request.OrderType,
                OptInStatus = request.OptInStatus,
                Notes = request.Notes,
                CoversCount = request.CoversCount
            };

            enrichOrderWithCompanyDetailsUseCase.Execute(orderEntity, request.CompanyId);
            enrichOrderWithClientUseCase.Execute(orderEntity, request);
            enrichOrderWithCheckoutMethodUseCase.Execute(orderEntity, request.CheckoutMethodId);
            enrichOrderWithServiceMethodUseCase.Execute(orderEntity, request.ServiceMethodId);
            enrichOrderWithOutletDetailsUseCase.Execute(orderEntity);
            EnrichOrderWithTippingOrderitemUseCase.Execute(orderEntity, request.Tipping);
            EnrichOrderWithServiceChargeOrderitemUseCase.Execute(orderEntity, request.ServiceCharge);
            EnrichOrderWithDeliveryChargeOrderitemUseCase.Execute(orderEntity, request.DeliveryCharge);
            enrichOrderWithDeliverypointUseCase.Execute(orderEntity, request);
            enrichOrderWithChargeToDeliverypointUseCase.Execute(orderEntity, request);
            EnrichOrderWithCustomerDetailsUseCase.Execute(orderEntity, request.Customer);
            enrichOrderWithDeliveryInformationUseCase.Execute(orderEntity, request.DeliveryInformation);
            enrichOrderWithOrderitemsUseCase.Execute(orderEntity, request.Orderitems);
            enrichOrderitemsWithTagsUseCase.Execute(orderEntity.OrderitemCollection);
            enrichOrderWithTaxesUseCase.Execute(orderEntity);
            enrichOrderWithOrderStatusUseCase.Execute(orderEntity);
            enrichOrderWithAnalyticsUseCase.Execute(orderEntity);
            EnrichOrderWithOrderContextUseCase.Execute(orderEntity, request.OrderContext);

            return orderEntity;
        }

        private static string GetOrCreateGuid(CreateOrderRequest request) => request.Guid == null || request.Guid.IsNullOrWhiteSpace() ? Guid.NewGuid().ToString() : request.Guid;
    }
}
