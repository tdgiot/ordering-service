﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Crave.Enums;
using Crave.Ordering.Api.Domain.UseCases.Requests;
using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Api.Domain.Interfaces.UseCases;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.Extensions;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.Utils;

namespace Crave.Ordering.Api.Domain.UseCases
{
    public class RouteOrderUseCase : IRouteOrderUseCase
    {
        private readonly IRouteRepository routeRepository;
        private readonly IOrderRepository orderRepository;
        private readonly IOrderitemRepository orderitemRepository;
        private readonly IProductRepository productRepository;
        private readonly ICategoryRepository categoryRepository;

        private readonly IGetClientConfigurationUseCase getClientConfigurationUseCase;
        private readonly IWriteRouteUseCase writeRouteUseCase;
        private readonly ISaveOrderUseCase saveOrderUseCase;
        private readonly ICreateReceiptUseCase createReceiptUseCase;

        /// <summary>
        /// Initializes a new instance of the <see cref="RouteOrder"/> class.
        /// </summary>
        /// <param name="routeRepository">The route repository.</param>
        /// <param name="orderRepository">The order repository.</param>
        /// <param name="orderitemRepository">The order item repository.</param>
        /// <param name="productRepository">The product repository.</param>
        /// <param name="categoryRepository">The category repository.</param>
        /// <param name="getClientConfigurationUseCase">The use-case to get the client configuration.</param>
        /// <param name="writeRouteUseCase">The use-case for writing routes.</param>
        /// <param name="saveOrderUseCase">Use-case for saving orders.</param>
        /// <param name="createReceiptUseCase">Use-case for creating receipt.</param>
        public RouteOrderUseCase(IRouteRepository routeRepository,
                                 IOrderRepository orderRepository,
                                 IOrderitemRepository orderitemRepository,
                                 IProductRepository productRepository,
                                 ICategoryRepository categoryRepository,
                                 IGetClientConfigurationUseCase getClientConfigurationUseCase,
                                 IWriteRouteUseCase writeRouteUseCase,
                                 ISaveOrderUseCase saveOrderUseCase,
                                 ICreateReceiptUseCase createReceiptUseCase)
        {
            this.routeRepository = routeRepository;
            this.orderRepository = orderRepository;
            this.orderitemRepository = orderitemRepository;
            this.productRepository = productRepository;
            this.categoryRepository = categoryRepository;
            this.getClientConfigurationUseCase = getClientConfigurationUseCase;
            this.writeRouteUseCase = writeRouteUseCase;
            this.saveOrderUseCase = saveOrderUseCase;
            this.createReceiptUseCase = createReceiptUseCase;
        }
        
        public async Task<bool> ExecuteAsync(RouteOrder request)
        {
            OrderEntity masterOrderEntity = request.OrderEntity;

            try
            {
                // Split the order into separate orders if needed
                Dictionary<int, OrderEntity> orderPerRoute = await GetOrdersForRouting(masterOrderEntity);
                foreach (int routeId in orderPerRoute.Keys)
                {
                    // Get the order
                    OrderEntity orderEntity = orderPerRoute[routeId];

                    // Get the route for this order, throws an exception when it can't be determined.
                    RouteEntity routeEntity = this.routeRepository.GetRoute(routeId);

                    // Write the route if one is available.
                    if (routeEntity == null)
                    {
                        // Throw exception, can't proceed.
                        throw new CraveException(RoutingResult.NoRouteForOrder, "OrderId: {0}\r\nRoute Info: {1}", orderEntity.OrderId, orderEntity.RoutingLog);
                    }

                    // Write the route
                    await writeRouteUseCase.ExecuteAsync(new WriteRoute(orderEntity, routeEntity, true, false));

                    // Write some diagnostics info
                    if (!orderEntity.RoutingLog.IsNullOrWhiteSpace())
                    {
                        orderEntity.RoutingLog += "\r\n";
                    }
                    orderEntity.RoutingLog += $"Routed according to Route: '{routeEntity.Name}' (RouteId: {routeEntity.RouteId})";

                    // Update the order with the routing log info
                    await saveOrderUseCase.ExecuteAsync(orderEntity);
                }
            }
            catch (CraveException cex)
            {
                // No route available, order fails.
                if (!masterOrderEntity.RoutingLog.IsNullOrWhiteSpace())
                {
                    masterOrderEntity.RoutingLog += "\r\n";
                }
                masterOrderEntity.RoutingLog += $"RoutingHelper.WriteRouteForOrder Failed: {cex.Message} - {cex.ErrorText}";
                masterOrderEntity.Status = OrderStatus.Unprocessable;
                masterOrderEntity.ErrorCode = OrderErrorCode.Unroutable;

                // Update the order
                await saveOrderUseCase.ExecuteAsync(masterOrderEntity);

                throw;
            }
            catch (Exception ex)
            {
                // Some other exception, order fails.
                if (!masterOrderEntity.RoutingLog.IsNullOrWhiteSpace())
                {
                    masterOrderEntity.RoutingLog += "\r\n";
                }
                masterOrderEntity.RoutingLog += $"RoutingHelper.WriteRouteForOrder Failed: {ex.Message}\r\n{ex.StackTrace}";
                masterOrderEntity.Status = OrderStatus.Unprocessable;
                masterOrderEntity.ErrorCode = OrderErrorCode.Unroutable;

                // Update the order
                await saveOrderUseCase.ExecuteAsync(masterOrderEntity);

                throw;
            }

            return true;
        }

        /// <summary>
        /// Gets the client configuration for the specified deliverypoint id
        /// </summary>
        /// <param name="deliverypointId">The id of the deliverypoint to get the client configuration for.</param>
        /// <param name="clientId">The id of the client to get the deliverypoint id from if deliverypoint id is not set.</param>
        /// <returns>A <see cref="ClientConfigurationEntity"/> instance.</returns>
        private async Task<ClientConfigurationEntity?> GetClientConfiguration(int? deliverypointId, int? clientId)
        {
            if (!deliverypointId.HasValue && !clientId.HasValue)
            {
                return null;
            }

            return await this.getClientConfigurationUseCase.ExecuteAsync(new GetClientConfiguration { DeliverypointId = deliverypointId.GetValueOrDefault(), ClientId = clientId.GetValueOrDefault() });
        }

        /// <summary>
        /// Gets a dictionary containing the orders per route id.
        /// </summary>
        /// <param name="masterOrderEntity">The order Entity to split into orders per route.</param>
        /// <returns>A dictionary instance containing the orders per route id.</returns>
        private async Task<Dictionary<int, OrderEntity>> GetOrdersForRouting(OrderEntity masterOrderEntity)
        {
            Dictionary<int, OrderEntity> orders = new Dictionary<int, OrderEntity>();

            // Create a dictionary containing the orderitem ids per route
            Dictionary<int, List<int>> orderitemsPerRoute = new Dictionary<int, List<int>>();

            ClientConfigurationEntity? clientConfigurationEntity = await GetClientConfiguration(masterOrderEntity.DeliverypointId, masterOrderEntity.ClientId);

            // Check for orderitems
            if (masterOrderEntity.OrderitemCollection.Count == 0)
            {
                // This order is a default service item without any orderitems
                AddRouteIdForOrder(orderitemsPerRoute, masterOrderEntity, clientConfigurationEntity);
            }
            else
            {
                // This loop gets the different routes for the order
                // and fills the dictionary with the route ids and corresponding order item ids
                masterOrderEntity.OrderitemCollection
                    .Where(x => x.Type == OrderitemType.Product)
                    .ToList().ForEach(x => AddRouteIdForOrderitem(orderitemsPerRoute, x, masterOrderEntity.Type, clientConfigurationEntity));
            }

            bool isMasterOrder = true;
            int masterRouteId = -1;
            foreach (int routeId in orderitemsPerRoute.Keys)
            {
                if (isMasterOrder) // Skip the first one, so some orderitems wil stay on the original order
                {
                    isMasterOrder = false;
                    masterRouteId = routeId;
                    continue;
                }
                
                // Get the orderitem ids 
                List<int> orderitemIds = orderitemsPerRoute[routeId];

                // Get the orderitems
                EntityCollection<OrderitemEntity> orderitemCollection = this.orderitemRepository.GetOrderitems(orderitemIds);

                // Create a copy of the master order
                OrderEntity orderEntity = new OrderEntity();

                // Copy the field values of the master order
                LLBLGenEntityUtil.CopyFields(masterOrderEntity, orderEntity);

                // Set the master order id
                orderEntity.MasterOrderId = masterOrderEntity.OrderId;
                orderEntity.Guid = Guid.NewGuid().ToString();

                foreach (int orderitemId in orderitemIds)
                {
                    OrderitemEntity orderitemEntity = orderitemCollection.FirstOrDefault(x => x.OrderitemId == orderitemId);
                    if (orderitemEntity != null)
                    {
                        // Set the override flag otherwise an exception will be thrown because an existing orderitem cannot be changed anymore
                        orderitemEntity.OverrideChangeProtection = true;                             
                        orderEntity.OrderitemCollection.Add(orderitemEntity);                            
                    }
                }

                // Save the order
                OrderEntity savedOrderEntity = await saveOrderUseCase.ExecuteAsync(orderEntity);

                // Store order in dictionary for later usage
                orderEntity = await orderRepository.GetByIdWithPrefetchAsync(savedOrderEntity.OrderId);

                // Create order receipt
                if (orderEntity.CheckoutMethodType == CheckoutType.PaymentProvider && orderEntity.CheckoutMethodId.HasValue)
                {
                    this.createReceiptUseCase.Execute(new CreateReceipt { Order = orderEntity });
                }

                orders.Add(routeId, orderEntity);                    
            }

            orders.Add(masterRouteId, masterOrderEntity);

            return orders;
        }

        /// <summary>
        /// Adds the route id to the dictionary for the supplied order entity and client configuration.
        /// </summary>
        /// <param name="routeIds">The route ids to append to.</param>
        /// <param name="orderEntity">The order to route.</param>
        /// <param name="clientConfigurationEntity">The client configuration with configured routes.</param>
        private static void AddRouteIdForOrder(Dictionary<int, List<int>> routeIds, OrderEntity orderEntity, ClientConfigurationEntity? clientConfigurationEntity)
        {
            // Get the routeId for this order
            int? routeId = GetRouteIdForOrder(orderEntity, clientConfigurationEntity);
            if (routeId.HasValue)
            {
                List<int> orderitemIds = new List<int>();
                orderitemIds.Add(-1);
                routeIds.Add(routeId.Value, orderitemIds);
            }
        }

        /// <summary>
        /// Adds the route ids to the dictionary for the supplied order item entity and client configuration.
        /// </summary>
        /// <param name="routeIds">The route ids to append to.</param>
        /// <param name="orderitem">The order item to route.</param>
        /// <param name="orderType">The type of the order.</param>
        /// <param name="clientConfigurationEntity">The client configuration with configured routes.</param>
        private void AddRouteIdForOrderitem(Dictionary<int, List<int>> routeIds, OrderitemEntity orderitem, OrderType orderType, ClientConfigurationEntity? clientConfigurationEntity)
        {
            // Get the route id for the current order item
            int? routeId = GetRouteIdForOrderitem(orderitem, orderType, clientConfigurationEntity);
            if (routeId.HasValue)
            {
                // Check whether the route was already in the dictionary
                if (routeIds.ContainsKey(routeId.Value))
                {
                    List<int> orderitemIds = routeIds[routeId.Value];
                    orderitemIds.Add(orderitem.OrderitemId);
                    routeIds[routeId.Value] = orderitemIds;
                }
                else
                {
                    List<int> orderitemIds = new List<int>();
                    orderitemIds.Add(orderitem.OrderitemId);
                    routeIds.Add(routeId.Value, orderitemIds);
                }
            }
        }

        /// <summary>
        /// Gets the route id for the specified order model.
        /// </summary>
        /// <param name="orderEntity">The order model to get the route id for.</param>
        /// <param name="clientConfigurationEntity">The client configuration model to use when getting the route.</param>
        /// <returns>The route id if it was found, otherwise null.</returns>
        private static int? GetRouteIdForOrder(OrderEntity orderEntity, ClientConfigurationEntity? clientConfigurationEntity)
        {
            int? routeId = null;
            StringBuilder routeDiagnosis = new StringBuilder();

            // Try to use order notes route if order contains a special message
            if (orderEntity.Notes.Length > 0)
            {
                routeId = GetOrderNotesRouteForClientConfiguration(clientConfigurationEntity, routeDiagnosis);
            }

            routeId ??= GetWakeUpRouteForClientConfiguration(orderEntity, clientConfigurationEntity, routeDiagnosis) ?? 
                        GetNormalRouteForClientConfiguration(clientConfigurationEntity, routeDiagnosis) ?? 
                        GetRouteForCompany(orderEntity.Company, routeDiagnosis);

            // Save a report of how we could not determine the route
            if (!routeId.HasValue)
            {
                orderEntity.RoutingLog += routeDiagnosis.ToString();
            }

            return routeId;
        }

        /// <summary>
        /// Gets the route id for the specified order item Entity.
        /// </summary>
        /// <param name="orderitemEntity">The order item Entity to get the route id for.</param>
        /// <param name="orderType">The type of the order.</param>
        /// <param name="clientConfigurationEntity">The client configuration model used to get the route.</param>
        /// <returns>The route id if it was found, otherwise null.</returns>
        private int? GetRouteIdForOrderitem(OrderitemEntity orderitemEntity, OrderType orderType, ClientConfigurationEntity? clientConfigurationEntity)
        {
            int? routeId = null;
            StringBuilder routeDiagnosis = new StringBuilder();

            if (orderType == OrderType.Document)
            {
                routeId = GetEmailDocumentRouteForClientConfiguration(orderitemEntity, clientConfigurationEntity, routeDiagnosis);
            }
            else
            {
                // Check whether master order or the orderitem has notes attached to it
                if (orderitemEntity.Order.Notes.Length > 0 || orderitemEntity.Notes.Length > 0)
                {
                    routeId = GetOrderNotesRouteForClientConfiguration(clientConfigurationEntity, routeDiagnosis);
                }
                
                routeId ??= GetRouteForProduct(orderitemEntity, routeDiagnosis) ?? 
                            GetRouteForCategory(orderitemEntity, routeDiagnosis) ??
                            GetNormalRouteForClientConfiguration(clientConfigurationEntity, routeDiagnosis) ??
                            GetRouteForCompany(orderitemEntity.Order.Company, routeDiagnosis);
            }

            // Save a report of how we could not determine the route
            orderitemEntity.Order.RoutingLog += routeDiagnosis.ToString();

            return routeId;
        }

        private static int? GetWakeUpRouteForClientConfiguration(OrderEntity orderEntity, ClientConfigurationEntity? clientConfigurationEntity, StringBuilder sb)
        {
            // Try to use the system message route
            if (clientConfigurationEntity != null && orderEntity.Type == OrderType.RequestForWakeUp)
            {
                // Get the system message route
                RouteEntity systemMessageRouteEntity = clientConfigurationEntity.GetRoute(RouteType.SystemMessage);
                if (systemMessageRouteEntity == null)
                {
                    sb.AppendLine("Route using SystemMessageRoute ClientConfiguration: No route found via ClientConfiguration (ClientConfiguration: '{0}', Id: '{1}')", clientConfigurationEntity.Name, clientConfigurationEntity.ClientConfigurationId);
                }
                else
                {
                    sb.AppendLine("Route using SystemMessageRoute on ClientConfiguration: One route found from ClientConfiguration: {0} (ClientConfigurationId: '{1}'), RouteId: '{2}'", clientConfigurationEntity.Name, clientConfigurationEntity.ClientConfigurationId, systemMessageRouteEntity.RouteId);
                    return systemMessageRouteEntity.RouteId;
                }
            }

            return null;
        }

        private static int? GetEmailDocumentRouteForClientConfiguration(OrderitemEntity orderitemEntity, ClientConfigurationEntity? clientConfigurationEntity, StringBuilder sb)
        {
            if (clientConfigurationEntity == null)
            {
                return null;
            }

            // A print request for a document from the IRT, always use the EmailDocumentRoute that's set on the deliverypointgroup
            RouteEntity emailDocumentRouteEntity = clientConfigurationEntity.GetRoute(RouteType.EmailDocument);
            if (emailDocumentRouteEntity == null)
            {
                sb.AppendLine("Route using EmailDocumentRoute Deliverypointgroup: No route found via Deliverypointgroup (Deliverypointgroup: '{0}', Id: '{1}')", orderitemEntity.Order.Deliverypoint.Deliverypointgroup.Name, orderitemEntity.Order.Deliverypoint.DeliverypointgroupId);
            }
            else
            {
                sb.AppendLine($"Route using EmailDocumentRoute on Deliverypointgroup: One route found from Deliverypointgroup: {orderitemEntity.Order.Deliverypoint.Deliverypointgroup.Name} " + 
                              $"(DeliverypointgroupId: '{orderitemEntity.Order.Deliverypoint.DeliverypointgroupId}'), " + 
                              $"RouteId: '{emailDocumentRouteEntity.RouteId}'");
                return emailDocumentRouteEntity.RouteId;
            }

            return null;
        }

        private static int? GetOrderNotesRouteForClientConfiguration(ClientConfigurationEntity? clientConfigurationEntity, StringBuilder sb)
        {
            if (clientConfigurationEntity == null)
            {
                return null;
            }

            RouteEntity orderNotesRoute = clientConfigurationEntity.GetRoute(RouteType.OrderNotes);
            if (orderNotesRoute == null)
            {
                sb.AppendLine("Route using OrderNotesRoute ClientConfiguration: No route found via ClientConfiguration (ClientConfiguration: '{0}', Id: '{1}')", clientConfigurationEntity.Name, clientConfigurationEntity.ClientConfigurationId);
            }
            else
            {
                sb.AppendLine("Route using OrderNotesRoute on ClientConfiguration: Route found from ClientConfiguration: {0} (ClientConfigurationId: '{1}'), RouteId: '{2}'", clientConfigurationEntity.Name, clientConfigurationEntity.ClientConfigurationId, orderNotesRoute.RouteId);
                return orderNotesRoute.RouteId;
            }

            return null;
        }

        private int? GetRouteForProduct(OrderitemEntity orderitemEntity, StringBuilder sb)
        {
            if (!orderitemEntity.ProductId.HasValue)
            {
                sb.AppendLine("Route using Orderitem: No is no product connect to Orderitem (Id: '{0}')", orderitemEntity.OrderitemId);
            }
            else
            {
                ProductEntity productEntity = this.productRepository.GetProduct(orderitemEntity.ProductId.Value);
                if (!productEntity.RouteId.HasValue)
                {
                    sb.AppendLine("Route using Product: No route found via Product (Product: '{0}', Id: '{1}')", productEntity.Name, orderitemEntity.ProductId.Value);
                }
                else
                {
                    sb.AppendLine("Route using Product: One route found from Product (Product: '{0}', Id: '{1}'), RouteId: '{2}'", productEntity.Name, orderitemEntity.ProductId.Value, productEntity.RouteId);
                    return productEntity.RouteId;
                }
            }

            return null;
        }

        private int? GetRouteForCategory(OrderitemEntity orderitemEntity, StringBuilder sb)
        {
            if (!orderitemEntity.CategoryId.HasValue)
            {
                sb.AppendLine("Route using Category: No category connected to order item (Product: '{0}', Id: '{1}')", orderitemEntity.ProductName, orderitemEntity.OrderitemId);
            }
            else
            {
                CategoryEntity categoryEntity = this.categoryRepository.GetCategoryWithParentRoutes(orderitemEntity.CategoryId.Value);
                CategoryEntity categoryEntityWithRouteId = categoryEntity.GetCategoryWithRouteId();
                if (categoryEntityWithRouteId == null)
                {
                    sb.AppendLine("Route using Category: No route found via Category (Category: '{0}', Id: '{1}')", categoryEntity.Name, orderitemEntity.CategoryId.Value);
                }
                else
                {
                    sb.AppendLine("Route using Category: One route found from (parent) Category (Category: '{0}', Id: '{1}'), RouteId: '{2}'", categoryEntityWithRouteId.Name, categoryEntityWithRouteId.CategoryId, categoryEntityWithRouteId.RouteId);
                    return categoryEntityWithRouteId.RouteId;
                    
                }
            }

            return null;
        }

        private static int? GetNormalRouteForClientConfiguration(ClientConfigurationEntity? clientConfigurationEntity, StringBuilder sb)
        {
            if (clientConfigurationEntity == null)
            {
                return null;
            }

            RouteEntity normalRouteEntity = clientConfigurationEntity.GetRoute(RouteType.Normal);
            if (normalRouteEntity == null)
            {
                sb.AppendLine("Route using ClientConfiguration: No route found via ClientConfiguration (ClientConfiguration: '{0}', Id: '{1}')", clientConfigurationEntity.Name, clientConfigurationEntity.ClientConfigurationId);
            }
            else
            {
                sb.AppendLine("Route using ClientConfiguration: One route found from ClientConfiguration: {0} (ClientConfigurationId: '{1}'), RouteId: '{2}'", clientConfigurationEntity.Name, clientConfigurationEntity.ClientConfigurationId, normalRouteEntity.RouteId);
                return normalRouteEntity.RouteId;
            }

            return null;
        }

        private static int? GetRouteForCompany(CompanyEntity companyEntity, StringBuilder sb)
        {
            if (!companyEntity.RouteId.HasValue)
            {
                sb.AppendLine("Route using Company: No route found via Company (Company: '{0}', Id: '{1}')", companyEntity.Name, companyEntity.CompanyId);
            }
            else
            {
                sb.AppendLine("Route using Company: One route found from Company (Company: '{0}', Id: '{1}'), RouteId: '{2}'", companyEntity.Name, companyEntity.CompanyId, companyEntity.RouteId.Value);
                return companyEntity.RouteId.Value;
            }

            return null;
        }
    }
}
