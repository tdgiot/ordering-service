﻿using Crave.Ordering.Api.Core.Enums;
using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Api.Domain.Exceptions;
using Crave.Ordering.Api.Domain.UseCases.Requests;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.Extensions;

namespace Crave.Ordering.Api.Domain.UseCases
{
    public class EnrichOrderWithClientUseCase
    {
        private readonly IDeviceRepository deviceRepository;

        public EnrichOrderWithClientUseCase(IDeviceRepository deviceRepository)
        {
            this.deviceRepository = deviceRepository;
        }

        public void Execute(OrderEntity orderEntity, CreateOrderRequest request)
        {
            if (!request.ClientId.HasValue)
            {
                return;
            }

            DeviceEntity deviceEntity = this.deviceRepository.GetDeviceForClient(request.ClientId.Value);
            if (deviceEntity.IsNullOrNew())
            {
                throw new CheckoutValidationException(ValidationErrorSubType.ClientInvalid);
            }

            orderEntity.ClientId = request.ClientId;
            orderEntity.ClientMacAddress = deviceEntity.Identifier;
        }
    }
}
