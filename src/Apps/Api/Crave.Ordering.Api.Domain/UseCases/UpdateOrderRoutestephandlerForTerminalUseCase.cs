﻿using System.Threading.Tasks;
using Crave.Enums;
using Crave.Ordering.Api.Domain.UseCases.Requests;
using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Api.Domain.Interfaces.UseCases;
using Crave.Ordering.Api.Domain.UseCases.Responses;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.Extensions;
using Amazon.XRay.Recorder.Core;
using Microsoft.Extensions.Logging;

namespace Crave.Ordering.Api.Domain.UseCases
{
    public class UpdateOrderRoutestephandlerForTerminalUseCase : IUpdateOrderRoutestephandlerForTerminalUseCase
    {
        private readonly IOrderRoutestephandlerRepository orderRoutestephandlerRepository;
        private readonly UpdateOrderRoutestephandlerUseCase updateOrderRoutestephandlerUseCase;
        private readonly FailExpiredOrderroutestephandlersForTerminalUseCase failExpiredOrderroutestephandlersForTerminalUseCase;
        private readonly ExecuteRoutestepUseCase executeRoutestepUseCase;
        private readonly ILogger<UpdateOrderRoutestephandlerForTerminalUseCase> logger;

        /// <summary>
        /// Initializes a new instance of the <see cref="UpdateOrderRoutestephandlersForTerminal"/> class.
        /// </summary>
        /// <param name="orderRoutestephandlerRepository">The order routestephandler repository</param>
        /// <param name="updateOrderRoutestephandlerUseCase">Save routestephandler use case</param>
        /// <param name="failExpiredOrderroutestephandlersForTerminalUseCase">Fail terminal expired routestephandlers use case</param>
        public UpdateOrderRoutestephandlerForTerminalUseCase(IOrderRoutestephandlerRepository orderRoutestephandlerRepository, 
                                                             UpdateOrderRoutestephandlerUseCase updateOrderRoutestephandlerUseCase, 
                                                             FailExpiredOrderroutestephandlersForTerminalUseCase failExpiredOrderroutestephandlersForTerminalUseCase,
                                                             ExecuteRoutestepUseCase executeRoutestepUseCase,
                                                             ILogger<UpdateOrderRoutestephandlerForTerminalUseCase> logger)
        {
            this.orderRoutestephandlerRepository = orderRoutestephandlerRepository;
            this.updateOrderRoutestephandlerUseCase = updateOrderRoutestephandlerUseCase;
            this.failExpiredOrderroutestephandlersForTerminalUseCase = failExpiredOrderroutestephandlersForTerminalUseCase;
            this.executeRoutestepUseCase = executeRoutestepUseCase;
            this.logger = logger;
        }

        /// <summary>
        /// Executes the logic of the use case by updating the route step handlers of the terminal for the parameters of the specified request DTO.
        /// </summary>
        /// <param name="request">The <see cref="UpdateOrderRoutestephandlersForTerminal"/> instance containing the parameters to update the route step handlers.</param>
        /// <returns>An <see cref="bool"/> instance which indicates whether the update was successful.</returns>
        public async Task<ResponseBase> ExecuteAsync(UpdateOrderRoutestephandlersForTerminal request)
        {
            OrderRoutestephandlerEntity orderRoutestephandlerEntity = await orderRoutestephandlerRepository.GetByIdAsync(request.OrderRoutestephandlerId);
            if (orderRoutestephandlerEntity.IsNullOrNew())
            {
                //This is caused by the user clicking the complete order button on the console too quickly/with dodgy internet, so log it here as a warning and return success rather than clog SEQ with error statements
                logger.LogWarning($"No order routestephandler found for id {request.OrderRoutestephandlerId}");
                return ResponseBase.AsSuccess();
            }

            UpdateOrderRoutestephandlerStatusRequest statusUpdateRequest = request.UpdateOrderRoutestephandlerStatusRequest;
            
            if (statusUpdateRequest.Status <= orderRoutestephandlerEntity.Status)
            {
                return ResponseBase.AsError($"OrderRoutestephandler status can not be downgraded (OrderRoutestephandlerId: {orderRoutestephandlerEntity.OrderRoutestephandlerId}, Current: {orderRoutestephandlerEntity.Status}, New: {statusUpdateRequest.Status})");
            }

            if (statusUpdateRequest.TerminalId != orderRoutestephandlerEntity.TerminalId.GetValueOrDefault(0))
            {
                return ResponseBase.AsError($"Attempt to change OrderRoutestephandler while not being owner of the step (OrderRoutestephandlerId: {orderRoutestephandlerEntity.OrderRoutestephandlerId}, TerminalId: {statusUpdateRequest.TerminalId})");
            }

            orderRoutestephandlerEntity.Status = statusUpdateRequest.Status;
            orderRoutestephandlerEntity.ErrorCode = statusUpdateRequest.ErrorCode;

            await updateOrderRoutestephandlerUseCase.ExecuteAsync(orderRoutestephandlerEntity);

            await failExpiredOrderroutestephandlersForTerminalUseCase.ExecuteAsync(new FailExpiredRoutestephandlersForTerminal 
            {
                TerminalId = statusUpdateRequest.TerminalId
            });

            if (orderRoutestephandlerEntity.Status == OrderRoutestephandlerStatus.Completed && !orderRoutestephandlerEntity.CompleteRouteOnComplete ||
                orderRoutestephandlerEntity.Status == OrderRoutestephandlerStatus.Failed && orderRoutestephandlerEntity.ContinueOnFailure)
            {
                // Run in new task/thread in case next the route step an external integration and we do not want to block this call
                // TODO: This can be changed once we've refactored the whole routing mechanic into something a bit more sensible
                _ = Task.Factory.StartNew(async () =>
                                          {
                                              AWSXRayRecorder.Instance.BeginSegment("ExecuteRoutestepUseCase");
                                              await executeRoutestepUseCase.ExecuteAsync(new ExecuteRoutestepRequest(orderRoutestephandlerEntity.OrderId, orderRoutestephandlerEntity.Number + 1));
                                              AWSXRayRecorder.Instance.EndSegment();
                                          }, TaskCreationOptions.LongRunning);
            }

            return ResponseBase.AsSuccess();
        }
    }
}
