﻿using System.Threading.Tasks;
using Crave.Ordering.Api.Core.Models.Responses;
using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Api.Domain.Interfaces.UseCases;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Ordering.Api.Domain.UseCases
{
    public class GetOrderForPaymentUseCase : IGetOrderForPaymentUseCase
    {
        private readonly IOrderRepository orderRepository;

        public GetOrderForPaymentUseCase(IOrderRepository orderRepository)
        {
            this.orderRepository = orderRepository;
        }

        public async Task<OrderPaymentDetails?> ExecuteAsync(string externalOrderId)
        {
            OrderEntity order = await orderRepository.GetOrderForPaymentAsync(externalOrderId);

            if (order == null || order.IsNew)
            {
                return null;
            }            

            return new OrderPaymentDetails
            {
                InternalId = order.OrderId,
                ExternalId = order.Guid,
                Status = order.Status,
                Total = order.Total,
                CurrencyCode = order.CurrencyCode,
                CountryCode = order.CountryCode,
                PaymentIntegrationConfigurationId = order.CheckoutMethod.PaymentIntegrationConfigurationId
            };
        }
    }
}