﻿using System;
using System.Threading.Tasks;
using Crave.Ordering.Api.Domain.UseCases.Requests;
using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Api.Domain.Interfaces.UseCases;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;

namespace Crave.Ordering.Api.Domain.UseCases
{
    public class CancelRoutesUseCase : ICancelRoutesUseCase
    {
        public enum CancelRoutesResult
        {
            ParameterMissing = 201
        }

        private readonly IOrderRepository orderRepository;
        private readonly IUpdateOrderRoutestephandlerStatusesUseCase updateOrderRoutestephandlerStatusesUseCase;

        /// <summary>
        /// Initialize a new instance of the <see cref="CancelRoutesUseCase"/> class.
        /// </summary>
        /// <param name="orderRepository">The order repository.</param>
        /// <param name="updateOrderRoutestephandlerStatusesUseCase">The use-case to update the status of the order routestep handlers with.</param>
        public CancelRoutesUseCase(IOrderRepository orderRepository, 
                                   IUpdateOrderRoutestephandlerStatusesUseCase updateOrderRoutestephandlerStatusesUseCase)
        {
            this.orderRepository = orderRepository;
            this.updateOrderRoutestephandlerStatusesUseCase = updateOrderRoutestephandlerStatusesUseCase;
        }

        public async Task<bool> ExecuteAsync(CancelRoutes request)
        {
            if (request.ClientId <= 0 && request.CustomerId <= 0 && request.OrderGuid.IsNullOrWhiteSpace())
            {
                throw new CraveException(CancelRoutesResult.ParameterMissing, $"{nameof(request)} DTO requires at least 1 parameter to be filled!");
            }

            EntityCollection<OrderEntity> orderCollection = this.orderRepository.GetOrders(request.ClientId, request.CustomerId, request.OrderGuid);
            foreach (OrderEntity orderEntity in orderCollection)
            {
                UpdateOrderRoutestephandlerStatuses updateOrderRoutestephandlerStatuses = new UpdateOrderRoutestephandlerStatuses
                {
                    OrderEntity = orderEntity,
                    Status = Crave.Enums.OrderStatus.Processed,
                    OnlyWhenFailed = false
                };
                await updateOrderRoutestephandlerStatusesUseCase.ExecuteAsync(updateOrderRoutestephandlerStatuses);
            }

            return true;
        }
    }
}
