﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Crave.Enums;
using Crave.Ordering.Api.Domain.UseCases.Requests;
using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.OrderNotifications.Builders;
using Crave.Ordering.Shared.OrderNotifications.Domain.UseCases.Requests;
using Crave.Ordering.Shared.OrderNotifications.Domain.UseCases.Responses;
using Crave.Ordering.Shared.OrderNotifications.Interfaces.Builders;
using Crave.Ordering.Shared.OrderNotifications.Interfaces.UseCases;
using Crave.Ordering.Shared.OrderNotifications.Models;

namespace Crave.Ordering.Api.Domain.UseCases
{
    public class HandleSmsOrderRoutestephandlerUseCase : UseCaseBase<HandleNonTerminalRoutingstephandler>
    {
        private readonly GetPhonenumbersForSupportpoolUseCase getPhonenumbersForSupportpoolUseCase;
        private readonly ISendTextMessageUseCase sendTextMessageUseCase;
        private readonly IOrderRepository orderRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="HandleSmsOrderRoutestephandlerUseCase"/> class.
        /// </summary>
        /// <param name="getPhonenumbersForSupportpoolUseCase">The use case for getting the phone numbers of a support pool</param>
        /// <param name="sendTextMessageUseCase">The use case for sending text messages</param>
        /// <param name="orderRepository">The order repository</param>
        public HandleSmsOrderRoutestephandlerUseCase(GetPhonenumbersForSupportpoolUseCase getPhonenumbersForSupportpoolUseCase,
                                                     ISendTextMessageUseCase sendTextMessageUseCase,
                                                     IOrderRepository orderRepository)
        {
            this.getPhonenumbersForSupportpoolUseCase = getPhonenumbersForSupportpoolUseCase;
            this.sendTextMessageUseCase = sendTextMessageUseCase;
            this.orderRepository = orderRepository;
        }

        /// <inheritdoc />
        public override void Execute(HandleNonTerminalRoutingstephandler request)
        {
            OrderRoutestephandlerEntity orderRoutestephandlerEntity = request.OrderRoutestephandlerEntity;
            OrderEntity orderEntity = request.OrderEntity;

            if (orderRoutestephandlerEntity.IsNew)
            {
                return;
            }
            
            SendSMSRequest smsRequest = new SendSMSRequest();
            smsRequest.Sender = orderRoutestephandlerEntity.FieldValue2;

            if (orderRoutestephandlerEntity.SupportpoolId.HasValue && orderRoutestephandlerEntity.SupportpoolId.Value > 0)
            {
                GetPhonenumbersForSupportpool getPhonenumbersForSupportpoolRequest = new GetPhonenumbersForSupportpool { SupportpoolId = orderRoutestephandlerEntity.SupportpoolId.Value };
                smsRequest.Phonenumbers = this.getPhonenumbersForSupportpoolUseCase.Execute(getPhonenumbersForSupportpoolRequest);
            }
            else
            {
                char[] separators = new[] { ' ', ',' };
                List<string> splitNumbers = orderRoutestephandlerEntity.FieldValue1.Split(separators).ToList();
                splitNumbers.RemoveAll(string.IsNullOrWhiteSpace);
                smsRequest.Phonenumbers = splitNumbers;
            }

            smsRequest.Message = this.GenerateSmsBody(orderRoutestephandlerEntity, orderEntity.OrderId);

            // Publish SMS request message
            MessageQueueReponse response = this.sendTextMessageUseCase.Execute(smsRequest);
            if (response.Success)
            {
                // OrderRoutestephandler is complete when the email is sent
                orderRoutestephandlerEntity.Status = OrderRoutestephandlerStatus.Completed;
            }
            else
            {
                orderRoutestephandlerEntity.Status = OrderRoutestephandlerStatus.Failed;
                orderRoutestephandlerEntity.ErrorText = $"Failed to send SMS to {string.Join(",", smsRequest.Phonenumbers)}. Result message: {response.ErrorMessage}";
            }
        }

        private string GenerateSmsBody(OrderRoutestephandlerEntity orderRoutestephandlerEntity, int orderId)
        {
            // Refetch order with extra data related to AppLess
            OrderEntity orderEntity = this.orderRepository.GetOrderForNotification(orderId);
            
            StringBuilder smsBody = new StringBuilder();
            if (orderRoutestephandlerEntity.FieldValue4.IsNullOrWhiteSpace())
            {
                smsBody.AppendLine("New Order - #[[ORDERNUMBER]]");
                
                if (orderEntity.OutletId.HasValue)
                {
                    smsBody.AppendLine("Service: [[SERVICEMETHOD]]");
                    smsBody.AppendLine("Checkout: [[CHECKOUTMETHOD]]");
                }
                
                smsBody.AppendLine("[[DELIVERYPOINTCAPTION]]: [[DELIVERYPOINTNUMBER]]");
            }
            else
            {
                // Custom sms
                smsBody.Append(orderRoutestephandlerEntity.FieldValue4);
            }

            IDictionary<string, string> additionalTemplateVars = GetAdditionalTemplateVariables(orderEntity);

            IOrderNotificationBuilder notificationBuilder = new OrderNotificationBuilder()
                                                            .WithTemplate(new OrderNotification(string.Empty, string.Empty, smsBody.ToString(), false))
                                                            .WithOrder(orderEntity)
                                                            .WithAppendAdditionalTemplateVarsIfUnique(false)
                                                            .WithAdditionalTemplateVars(additionalTemplateVars);

            OrderNotification orderNotification = new OrderNotificationDirector(notificationBuilder)
                .BuildNotification();

            return orderNotification.ShortMessage;
        }

        private static IDictionary<string, string> GetAdditionalTemplateVariables(OrderEntity orderEntity)
        {
            StringBuilder orderlistVar = new StringBuilder();
            foreach (OrderitemEntity orderitem in orderEntity.OrderitemCollection)
            {
                orderlistVar.AppendLine(orderitem.Quantity + "x " + orderitem.ProductName);

                if (orderitem.OrderitemAlterationitemCollection != null)
                {
                    foreach (OrderitemAlterationitemEntity alterationitem in orderitem.OrderitemAlterationitemCollection)
                    {
                        orderlistVar.AppendLine("   " + alterationitem.AlterationName + ": " + alterationitem.AlterationoptionName);
                    }
                }
            }

            IDictionary<string, string> additionalVars = new Dictionary<string, string>();
            additionalVars.Add("ORDER", orderlistVar.ToString());
            additionalVars.Add("TAX", string.Empty);
            additionalVars.Add("DELIVERYINFORMATION", string.Empty);

            return additionalVars;
        }
    }
}
