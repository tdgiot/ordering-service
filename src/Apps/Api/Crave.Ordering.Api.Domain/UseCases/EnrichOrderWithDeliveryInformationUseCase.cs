﻿using Crave.Ordering.Api.Domain.UseCases.Requests;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Ordering.Api.Domain.UseCases
{
    public class EnrichOrderWithDeliveryInformationUseCase
    {
        public void Execute(OrderEntity orderEntity, DeliveryInformationRequest? deliveryInformation)
        {
            if (deliveryInformation == null)
            {
                return;
            }

            orderEntity.DeliveryInformation = new DeliveryInformationEntity
            {
                BuildingName = deliveryInformation.BuildingName,
                Address = deliveryInformation.Address,
                Zipcode = deliveryInformation.Zipcode,
                City = deliveryInformation.City,
                Instructions = deliveryInformation.Instructions,
                Longitude = deliveryInformation.Longitude,
                Latitude = deliveryInformation.Latitude,
                DistanceToOutletInMetres = deliveryInformation.DistanceToOutletInMetres
            };
        }
    }
}
