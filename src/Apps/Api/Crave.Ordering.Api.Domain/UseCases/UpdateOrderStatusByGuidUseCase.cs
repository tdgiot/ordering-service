﻿using System;
using System.Threading.Tasks;
using Amazon.XRay.Recorder.Core;
using Crave.Enums;
using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Api.Domain.Interfaces.UseCases;
using Crave.Ordering.Api.Domain.UseCases.Requests;
using Crave.Ordering.Api.Domain.UseCases.Responses;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.Extensions;
using Crave.Ordering.Shared.Enums;
using Microsoft.Extensions.Logging;

namespace Crave.Ordering.Api.Domain.UseCases
{
    public class UpdateOrderStatusByGuidUseCase : IUpdateOrderStatusByGuidUseCase
    {
        private readonly ILogger<IUpdateOrderStatusByGuidUseCase> logger;
        private readonly IOrderRepository orderRepository;
        private readonly ISaveOrderUseCase saveOrderUseCase;
        private readonly ICreateOrderRouteUseCase createOrderRouteUseCase;

        public UpdateOrderStatusByGuidUseCase(ILogger<IUpdateOrderStatusByGuidUseCase> logger,
                                              IOrderRepository orderRepository,
                                              ISaveOrderUseCase saveOrderUseCase,
                                              ICreateOrderRouteUseCase createOrderRouteUseCase)
        {
            this.logger = logger;
            this.orderRepository = orderRepository;
            this.saveOrderUseCase = saveOrderUseCase;
            this.createOrderRouteUseCase = createOrderRouteUseCase;
        }

        public async Task<ResponseBase> ExecuteAsync(UpdateOrderStatusByGuidRequest request)
        {
            OrderEntity orderEntity = await orderRepository.GetByGuidAsync(request.Guid);
            if (orderEntity.IsNullOrNew())
            {
                return ResponseBase.AsNotFound("Order not found");
            }

            if (request.Status == orderEntity.Status)
            {
                return ResponseBase.AsError($"New order status is the same as current (current: {orderEntity.Status})");
            }

            if (request.Status < orderEntity.Status)
            {
                return ResponseBase.AsError($"Can not downgrade order status (current: {orderEntity.Status})");
            }

            if (request.Status == OrderStatus.Processed)
            {
                orderEntity.ProcessedUTC = DateTime.UtcNow;
            }

            orderEntity.Status = request.Status;

            try
            {
                // Save the order (plus other magic)
                await saveOrderUseCase.ExecuteAsync(orderEntity);
            }
            catch (CraveException cex)
            {
                return ResponseBase.AsError($"Something went wrong saving the order: {cex.Message}\n\r{cex.StackTrace}");
            }

            // When order is marked as save, the order can be routed
            if (orderEntity.Status == OrderStatus.Paid)
            {
                CreateAndRouteOrder(orderEntity);
            }
            
            return ResponseBase.AsSuccess();
        }

        private void CreateAndRouteOrder(OrderEntity orderEntity)
        {
            _ = Task.Factory.StartNew(async () =>
            {
                try
                {
                    AWSXRayRecorder.Instance.BeginSegment("CreateAndRouteOrder");

                    CreateOrderRouteResponse createOrderRouteResponse = await createOrderRouteUseCase.ExecuteAsync(new CreateOrderRoute(orderEntity.OrderId));
                    if (createOrderRouteResponse.Result != CreateOrderRouteResult.Ok)
                    {
                        logger.LogError("Order status updated, but unable to be routed (OrderId={orderId}, Status={status}, CreateOrderRouteResult={result}, Exception={exception})", orderEntity.OrderId, orderEntity.Status, createOrderRouteResponse.Result, createOrderRouteResponse.Exception);
                    }
                }
                catch (Exception ex)
                {
                    logger.LogError(ex, "Exception while routing order (OrderId={orderId}", orderEntity.OrderId);
                    AWSXRayRecorder.Instance.AddException(ex);
                }
                finally
                {
                    AWSXRayRecorder.Instance.EndSegment();
                }
            }, TaskCreationOptions.LongRunning);
        }
    }
}