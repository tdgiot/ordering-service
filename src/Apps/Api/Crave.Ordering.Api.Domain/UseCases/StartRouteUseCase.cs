﻿using System;
using System.Threading.Tasks;
using Crave.Enums;
using Crave.Ordering.Api.Domain.UseCases.Requests;
using Crave.Ordering.Shared.Constants;
using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Api.Domain.Interfaces.UseCases;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Ordering.Api.Domain.UseCases
{
    /// <summary>
    /// 
    /// </summary>
    public class StartRouteUseCase : IStartRouteUseCase
    {
        private readonly IOrderRoutestephandlerRepository orderRoutestephandlerRepository;
        private readonly IUpdateOrderStatusUseCase updateOrderStatusUseCase;
        private readonly ExecuteRoutestepUseCase executeRoutestepUseCase;

        public StartRouteUseCase(IOrderRoutestephandlerRepository orderRoutestephandlerRepository,
            IUpdateOrderStatusUseCase updateOrderStatusUseCase,
            ExecuteRoutestepUseCase executeRoutestepUseCase)
        {
            this.orderRoutestephandlerRepository = orderRoutestephandlerRepository;
            this.updateOrderStatusUseCase = updateOrderStatusUseCase;
            this.executeRoutestepUseCase = executeRoutestepUseCase;
        }

        /// <inheritdoc />
        public async Task<bool> ExecuteAsync(OrderRoutestephandlerEntity request)
        {
            OrderRoutestephandlerEntity orderRoutestephandlerEntity = request;

            if (orderRoutestephandlerEntity.Number != RoutingConstants.FirstStepNumber ||
                orderRoutestephandlerEntity.Status > OrderRoutestephandlerStatus.WaitingForPreviousStep)
            {
                // Not a first step or status is not valid
                return false;
            }

            orderRoutestephandlerEntity.Status = OrderRoutestephandlerStatus.WaitingToBeRetrieved;
            orderRoutestephandlerEntity.StatusText = OrderRoutestephandlerStatus.WaitingToBeRetrieved.ToString();
            orderRoutestephandlerEntity.WaitingToBeRetrievedUTC = DateTime.UtcNow;

            orderRoutestephandlerRepository.SaveOrderRoutestephandler(orderRoutestephandlerEntity);

            UpdateOrderStatus updateOrderStatus = new UpdateOrderStatus(orderRoutestephandlerEntity.OrderId, OrderStatus.InRoute);
            await updateOrderStatusUseCase.ExecuteAsync(updateOrderStatus);

            await executeRoutestepUseCase.ExecuteAsync(new ExecuteRoutestepRequest(orderRoutestephandlerEntity.OrderId, orderRoutestephandlerEntity.Number));

            return true;
        }
    }
}