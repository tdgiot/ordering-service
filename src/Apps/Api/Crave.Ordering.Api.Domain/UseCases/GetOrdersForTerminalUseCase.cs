﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Crave.Enums;
using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Api.Domain.Converters;
using Crave.Ordering.Api.Domain.Interfaces.UseCases;
using Crave.Ordering.Api.Domain.UseCases.Requests;
using Crave.Ordering.Api.Domain.UseCases.Responses;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.Extensions;

namespace Crave.Ordering.Api.Domain.UseCases
{
    public class GetOrdersForTerminalUseCase : IGetOrdersForTerminalUseCase
    {
        private readonly ITerminalRepository terminalRepository;
        private readonly IOrderRoutestephandlerRepository orderRoutestephandlerRepository;
        private readonly IOrderRepository orderRepository;
        private readonly IUpdateOrderRoutestephandlerUseCase updateOrderRoutestephandlerUseCase;
        private readonly OrderConverter orderConverter;

        /// <summary>
        /// Initializes a new instance of the <see cref="GetOrdersForTerminalUseCase"/> class.
        /// </summary>
        /// <param name="terminalRepository">The terminal repository.</param>
        /// <param name="orderRoutestephandlerRepository">The order routestep handler repository.</param>
        /// <param name="orderRepository">The order repository.</param>
        /// <param name="updateOrderRoutestephandlerUseCase">Save order routestephandler use case</param>
        /// <param name="orderConverter">Converts an order entity to an order dto.</param>
        public GetOrdersForTerminalUseCase(ITerminalRepository terminalRepository,
                                           IOrderRoutestephandlerRepository orderRoutestephandlerRepository,
                                           IOrderRepository orderRepository,
                                           IUpdateOrderRoutestephandlerUseCase updateOrderRoutestephandlerUseCase,
                                           OrderConverter orderConverter)
        {
            this.terminalRepository = terminalRepository;
            this.orderRoutestephandlerRepository = orderRoutestephandlerRepository;
            this.orderRepository = orderRepository;
            this.updateOrderRoutestephandlerUseCase = updateOrderRoutestephandlerUseCase;
            this.orderConverter = orderConverter;
        }

        /// <summary>
        /// Executes the logic of the use case by getting the orders for the parameters of the specified request DTO.
        /// </summary>
        /// <param name="request">The <see cref="GetOrdersForTerminal"/> instance containing the parameters to get the orders.</param>
        /// <returns>An <see cref="IEnumerable{Order}"/> instance containing the orders.</returns>
        public async Task<ResponseBase<IEnumerable<Order>>> ExecuteAsync(GetOrdersForTerminal request)
        {
            List<OrderEntity> orderCollection = new List<OrderEntity>();

            // Get the terminal to determine the type
            TerminalEntity terminalEntity = terminalRepository.GetTerminal(request.TerminalId);
            if (terminalEntity.IsNullOrNew())
            {
                return ResponseBase<IEnumerable<Order>>.AsNotFound($"Terminal not found for id '{request.TerminalId}'");
            }

            // Get the order routestep handler statuses that apply to the terminal
            List<OrderRoutestephandlerStatus> orderRoutestephandlerStatuses = GetOrderRoutestephandlerStatuses(terminalEntity.Type.GetValueOrDefault(TerminalType.Demo));

            // Get the order routestep handlers
            IEnumerable<OrderRoutestephandlerEntity> orderRoutestephandlers = this.orderRoutestephandlerRepository.GetOrderRoutestephandlers(orderRoutestephandlerStatuses, terminalEntity.TerminalId);
            if (!orderRoutestephandlers.IsNullOrEmpty())
            {
                // Flag the order routestep handlers as retrieved
                foreach (OrderRoutestephandlerEntity orderRoutestephandler in orderRoutestephandlers)
                {
                    if (orderRoutestephandler.Status == OrderRoutestephandlerStatus.WaitingToBeRetrieved)
                    {
                        orderRoutestephandler.Status = OrderRoutestephandlerStatus.RetrievedByHandler;
                        await updateOrderRoutestephandlerUseCase.ExecuteAsync(orderRoutestephandler);
                    }
                }

                ICollection<int> orderIds = orderRoutestephandlers.Select(x => x.OrderId).ToList();
                if (!orderIds.IsNullOrEmpty())
                {
                    orderCollection.AddRange(this.orderRepository.GetPendingOrders(request.TerminalId, orderIds, orderRoutestephandlerStatuses));
                }
            }

            IEnumerable<Order> collection = orderConverter.ConvertEntityCollectionToTResponseDtoCollection(orderCollection);

            return ResponseBase<IEnumerable<Order>>.AsSuccess(collection);
        }

        /// <summary>
        /// Gets a list of order routestep handler statuses based on the specified terminal type.
        /// </summary>
        /// <param name="terminalType">The type of the terminal to get the statuses for.</param>
        /// <returns>A <see cref="List{OrderRoutestephandlerStatus}"/> instance.</returns>
        private static List<OrderRoutestephandlerStatus> GetOrderRoutestephandlerStatuses(TerminalType terminalType)
        {
            List<OrderRoutestephandlerStatus> orderRoutestephandlerStatuses = new List<OrderRoutestephandlerStatus>();
            orderRoutestephandlerStatuses.Add(OrderRoutestephandlerStatus.WaitingToBeRetrieved);

            if (terminalType == TerminalType.Console)
            {
                orderRoutestephandlerStatuses.Add(OrderRoutestephandlerStatus.RetrievedByHandler);
                orderRoutestephandlerStatuses.Add(OrderRoutestephandlerStatus.BeingHandled);
            }

            return orderRoutestephandlerStatuses;
        }
    }
}
