﻿using System.Threading.Tasks;
using Crave.Ordering.Api.Domain.UseCases.Requests;
using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Api.Domain.Interfaces.UseCases;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Ordering.Api.Domain.UseCases
{
    public class GetClientConfigurationUseCase : IGetClientConfigurationUseCase
    {
        private readonly IClientRepository clientRepository;
        private readonly IDeliverypointRepository deliverypointRepository;
        private readonly IDeliverypointgroupRepository deliverypointgroupRepository;
        private readonly IClientConfigurationRepository clientConfigurationRepository;

        public GetClientConfigurationUseCase(IClientRepository clientRepository, 
                                             IDeliverypointRepository deliverypointRepository, 
                                             IDeliverypointgroupRepository deliverypointgroupRepository, 
                                             IClientConfigurationRepository clientConfigurationRepository)
        {
            this.clientRepository = clientRepository;
            this.deliverypointRepository = deliverypointRepository;
            this.deliverypointgroupRepository = deliverypointgroupRepository;
            this.clientConfigurationRepository = clientConfigurationRepository;
        }
        
        public async Task<ClientConfigurationEntity?> ExecuteAsync(GetClientConfiguration request)
        {
            ClientConfigurationEntity? clientConfigurationEntity = null;

            int deliverypointId = request.DeliverypointId;
            if (deliverypointId <= 0)
            {
                ClientEntity clientEntity = await clientRepository.GetClientAsync(request.ClientId);
                if (clientEntity != null)
                {
                    deliverypointId = clientEntity.DeliverypointId ?? 0;
                }
            }

            if (deliverypointId > 0)
            {
                DeliverypointEntity deliverypointEntity = this.deliverypointRepository.GetDeliverypoint(deliverypointId);
                if (deliverypointEntity.ClientConfigurationId.HasValue)
                {
                    clientConfigurationEntity = await clientConfigurationRepository.GetClientConfigurationAsync(deliverypointEntity.ClientConfigurationId.Value);
                }
                else
                {
                    DeliverypointgroupEntity deliverypointgroupEntity = this.deliverypointgroupRepository.GetDeliverypointgroup(deliverypointEntity.DeliverypointgroupId);
                    if (deliverypointgroupEntity.ClientConfigurationId.HasValue)
                    {
                        clientConfigurationEntity = await clientConfigurationRepository.GetClientConfigurationAsync(deliverypointgroupEntity.ClientConfigurationId.Value);
                    }
                }
            }

            return clientConfigurationEntity;
        }
    }
}
