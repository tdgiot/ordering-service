﻿using System;
using Crave.Enums;
using Crave.Ordering.Api.Domain.UseCases.Requests;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Ordering.Api.Domain.UseCases
{
    public static class EnrichOrderWithServiceChargeOrderitemUseCase
    {
        private const int DECIMAL_PLACES = 2;

        public static void Execute(OrderEntity orderEntity, ServiceChargeRequest? serviceCharge)
        {
            if (serviceCharge == null || !orderEntity.Outlet.ServiceChargeProductId.HasValue)
            {
                return;
            }

            OrderitemEntity orderitemEntity = new OrderitemEntity();
            orderitemEntity.Type = OrderitemType.ServiceCharge;
            orderitemEntity.ProductDescription = "Service Charge";
            orderitemEntity.ProductId = orderEntity.Outlet.ServiceChargeProductId.Value;
            orderitemEntity.Quantity = 1;
            orderitemEntity.ProductPriceIn = Math.Round(serviceCharge.Price, DECIMAL_PLACES, MidpointRounding.AwayFromZero);

            orderEntity.OrderitemCollection.Add(orderitemEntity);
        }
    }
}
