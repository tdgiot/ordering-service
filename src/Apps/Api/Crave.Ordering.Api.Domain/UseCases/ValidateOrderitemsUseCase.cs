﻿using System.Collections.Generic;
using Crave.Ordering.Api.Domain.Interfaces.UseCases;
using Crave.Ordering.Api.Domain.UseCases.Requests;
using Crave.Ordering.Api.Domain.UseCases.Responses;
using Crave.Ordering.Api.Domain.Validation;
using Crave.Ordering.Api.Domain.Validation.Errors;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Ordering.Api.Domain.UseCases
{
    /// <summary>
    /// Validates a set of order items.
    /// </summary>
    public class ValidateOrderitemsUseCase : IValidateOrderitemsUseCase
    {
        private readonly EnrichOrderWithCompanyDetailsUseCase enrichOrderWithCompanyDetailsUseCase;
        private readonly EnrichOrderWithOrderitemsUseCase enrichOrderWithOrderitemsUseCase;

        /// <summary>
        /// Initializes a new instance of the <see cref="ValidateOrderitemsUseCase"/> class.
        /// </summary>
        /// <param name="enrichOrderWithOrderitemsUseCase">The use-case for enriching an order entity with order item entities.</param>
        /// <param name="orderitemValidator">The order item validator.</param>
        public ValidateOrderitemsUseCase(EnrichOrderWithOrderitemsUseCase enrichOrderWithOrderitemsUseCase,
                                                EnrichOrderWithCompanyDetailsUseCase enrichOrderWithCompanyDetailsUseCase)
        {
            this.enrichOrderWithOrderitemsUseCase = enrichOrderWithOrderitemsUseCase;
            this.enrichOrderWithCompanyDetailsUseCase = enrichOrderWithCompanyDetailsUseCase;
        }

        /// <inheritdoc />
        public ValidationResponse Execute(ValidateOrderitemsRequest request)
        {
            List<ValidationError> errorList = new List<ValidationError>();

            OrderEntity orderEntity = new OrderEntity();

            enrichOrderWithCompanyDetailsUseCase.Execute(orderEntity, request.CompanyId);
            enrichOrderWithOrderitemsUseCase.Execute(orderEntity, request.Orderitems);

            OrderitemValidator.Validate(orderEntity, request.Orderitems, errorList, false);
            
            return new ValidationResponse(errorList);
        }
    }
}