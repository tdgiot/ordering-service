﻿using Crave.Ordering.Api.Domain.UseCases.Requests;
using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Ordering.Api.Domain.UseCases
{
    public class SetForwardedTerminalUseCase : UseCaseBase<SetForwardedTerminal, OrderRoutestephandlerEntity>
    {
        #region Fields

        private readonly ITerminalRepository terminalRepository;
        private readonly GetTerminalToForwardToUseCase getTerminalToForwardToUseCase;

        #endregion

        #region Constructors

        public SetForwardedTerminalUseCase(ITerminalRepository terminalRepository, 
                                           GetTerminalToForwardToUseCase getTerminalToForwardToUseCase)
        {
            this.terminalRepository = terminalRepository;
            this.getTerminalToForwardToUseCase = getTerminalToForwardToUseCase;
        }

        #endregion

        public override OrderRoutestephandlerEntity Execute(SetForwardedTerminal request)
        {
            OrderRoutestephandlerEntity orderRoutestephandlerEntity = request.OrderRoutestephandlerEntity;

            // Set the correct terminal, since it might be forwarded
            if (orderRoutestephandlerEntity.TerminalId.HasValue)
            {
                TerminalEntity handlingTerminalEntity = this.terminalRepository.GetTerminal(orderRoutestephandlerEntity.TerminalId.Value);
                TerminalEntity? forwardToTerminalEntity = this.getTerminalToForwardToUseCase.Execute(new GetTerminalToForwardTo { TerminalId = handlingTerminalEntity.TerminalId });
                if (forwardToTerminalEntity != null)
                {
                    orderRoutestephandlerEntity.ForwardedFromTerminalId = orderRoutestephandlerEntity.TerminalId;
                    orderRoutestephandlerEntity.TerminalId = forwardToTerminalEntity.TerminalId;
                }
            }

            return orderRoutestephandlerEntity;
        }
    }
}
