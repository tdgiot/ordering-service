﻿using Crave.Ordering.Api.Domain.UseCases.Requests;
using Crave.Ordering.Api.Domain.Utils;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Ordering.Api.Domain.UseCases
{
    public static class EnrichOrderWithCustomerDetailsUseCase
    {
        public static void Execute(OrderEntity orderEntity, CustomerRequest? customer)
        {
            if (customer == null)
            {
                return;
            }

            orderEntity.Email = customer.Email;
            orderEntity.CustomerFirstname = customer.FirstName;
            orderEntity.CustomerLastname = customer.LastName;
            orderEntity.CustomerLastnamePrefix = customer.LastNamePrefix;
            orderEntity.CustomerPhonenumber = customer.Phonenumber == null ? null : PhoneNumberUtil.Standardize(customer.Phonenumber);
        }
    }
}
