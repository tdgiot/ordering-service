﻿using System;
using System.Threading.Tasks;
using Crave.Globalization;
using Crave.Libraries.Distance.Interfaces;
using Crave.Libraries.Distance.Models;
using Crave.Ordering.Api.Domain.UseCases.Requests;
using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.Extensions;

namespace Crave.Ordering.Api.Domain.UseCases
{
    public class GetOutletLocationUseCase : AsyncUseCaseBase<GetOutletLocationRequest, ILocation>
    {
        #region Fields

        private readonly ICompanyRepository companyRepository;

        #endregion

        #region Constructors

        public GetOutletLocationUseCase(ICompanyRepository companyRepository)
        {
            this.companyRepository = companyRepository;
        }

        #endregion

        #region Methods

        public override async Task<ILocation> ExecuteAsync(GetOutletLocationRequest request)
        {
            CompanyEntity companyEntity = await this.companyRepository.GetCompanyLocationForOutletAsync(request.OutletId);
            if (companyEntity.IsNullOrNew())
            {
                throw new InvalidOperationException($"Unable to find company for outlet with id {request.OutletId}");
            }

            if (companyEntity.Longitude.HasValue && companyEntity.Latitude.HasValue)
            {
                return new Coordinates(companyEntity.Longitude.Value, companyEntity.Latitude.Value);
            }
            else
            {
                Country country = (Country)companyEntity.CountryCode;

                return new Address(companyEntity.Addressline1, companyEntity.City, companyEntity.Zipcode, country.Name);
            }
        }

        #endregion
    }
}
