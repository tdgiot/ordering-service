﻿using System;
using Crave.Ordering.Api.Core.Enums;
using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Api.Domain.Exceptions;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.Extensions;

namespace Crave.Ordering.Api.Domain.UseCases
{
    public class EnrichOrderWithCheckoutMethodDetailsUseCase
    {
        private readonly ICheckoutMethodRepository checkoutMethodRepository;

        public EnrichOrderWithCheckoutMethodDetailsUseCase(ICheckoutMethodRepository checkoutMethodRepository)
        {
            this.checkoutMethodRepository = checkoutMethodRepository;
        }

        public void Execute(OrderEntity orderEntity, int? checkoutMethodId)
        {
            if (!checkoutMethodId.HasValue)
            {
                return;
            }

            CheckoutMethodEntity checkoutMethodEntity = checkoutMethodRepository.GetCheckoutMethod(checkoutMethodId.Value);
            if (checkoutMethodEntity.IsNullOrNew())
            {
                throw new CheckoutValidationException(ValidationErrorSubType.CheckoutMethodInvalid);
            }

            if (!checkoutMethodEntity.CurrencyCode.IsNullOrWhiteSpace())
            {
                orderEntity.CurrencyCode = checkoutMethodEntity.CurrencyCode;
            }

            if (!checkoutMethodEntity.CountryCode.IsNullOrWhiteSpace())
            {
                orderEntity.CountryCode = checkoutMethodEntity.CountryCode;
            }

            orderEntity.CheckoutMethod = checkoutMethodEntity;
            orderEntity.CheckoutMethodId = checkoutMethodEntity.CheckoutMethodId;
            orderEntity.OutletId = checkoutMethodEntity.OutletId;
            orderEntity.CheckoutMethodName = checkoutMethodEntity.Name;
            orderEntity.CheckoutMethodType = checkoutMethodEntity.CheckoutType;
        }
    }
}
