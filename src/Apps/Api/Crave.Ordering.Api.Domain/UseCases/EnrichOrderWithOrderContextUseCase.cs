﻿using System;
using System.Text;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using ServiceStack;

namespace Crave.Ordering.Api.Domain.UseCases
{
    public static class EnrichOrderWithOrderContextUseCase
    {
        public static void Execute(OrderEntity orderEntity, string? orderContext)
        {
            if (orderContext.IsNullOrWhiteSpace())
            {
                return;
            }

            StringBuilder sb = new StringBuilder();
            if (!orderEntity.Notes.IsNullOrWhiteSpace())
            {
                sb.AppendLine(orderEntity.Notes);
                sb.AppendLine();
            }
            sb.Append(orderContext.StripHtml());

            orderEntity.Notes = sb.ToString();
        }
    }
}
