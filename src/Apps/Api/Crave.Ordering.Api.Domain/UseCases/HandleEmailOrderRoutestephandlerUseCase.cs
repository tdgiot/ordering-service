﻿using System;
using System.Collections.Generic;
using System.Linq;
using Crave.Enums;
using Crave.Ordering.Api.Core.Configuration;
using Crave.Ordering.Api.Domain.UseCases.Requests;
using Crave.Ordering.Api.Domain.Utils;
using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.OrderNotifications.Builders;
using Crave.Ordering.Shared.OrderNotifications.Domain.UseCases.Requests;
using Crave.Ordering.Shared.OrderNotifications.Domain.UseCases.Responses;
using Crave.Ordering.Shared.OrderNotifications.Interfaces.Builders;
using Crave.Ordering.Shared.OrderNotifications.Interfaces.UseCases;
using Crave.Ordering.Shared.OrderNotifications.Models;
using System.Threading.Tasks;
using System.IO;

namespace Crave.Ordering.Api.Domain.UseCases
{
    public class HandleEmailOrderRoutestephandlerUseCase : IAsyncUseCase<HandleNonTerminalRoutingstephandler>
    {
        private const string DEFAULT_SENDER_EMAIL = "noreply@crave-emenu.com";

        private readonly AmazonConfiguration amazonConfiguration;
        private readonly IRoutestephandlerRepository routestephandlerRepository;
        private readonly GetEmailAddressesForSupportpoolUseCase getEmailAddressesForSupportpoolUseCase;
        private readonly ISendMailUseCase sendMailUseCase;
        private readonly IOrderRepository orderRepository;

        public HandleEmailOrderRoutestephandlerUseCase(AmazonConfiguration amazonConfiguration,
                                                       IRoutestephandlerRepository routestephandlerRepository,
                                                       GetEmailAddressesForSupportpoolUseCase getEmailAddressesForSupportpoolUseCase,
                                                       ISendMailUseCase sendMailUseCase,
                                                       IOrderRepository orderRepository)
        {
            this.amazonConfiguration = amazonConfiguration;
            this.routestephandlerRepository = routestephandlerRepository;
            this.getEmailAddressesForSupportpoolUseCase = getEmailAddressesForSupportpoolUseCase;
            this.sendMailUseCase = sendMailUseCase;
            this.orderRepository = orderRepository;
        }

        public async Task ExecuteAsync(HandleNonTerminalRoutingstephandler request)
        {
            OrderRoutestephandlerEntity orderRoutestephandlerEntity = request.OrderRoutestephandlerEntity;

            // Refetch order with extra data is related to AppLess
            OrderEntity orderEntity = this.orderRepository.GetOrderForNotification(request.OrderEntity.OrderId);

            List<string> toAddresses = this.GetEmailToAddresses(orderRoutestephandlerEntity, orderEntity);
            string fromAddress = orderRoutestephandlerEntity.FieldValue2.IsNullOrWhiteSpace() ? HandleEmailOrderRoutestephandlerUseCase.DEFAULT_SENDER_EMAIL : orderRoutestephandlerEntity.FieldValue2;
            string emailTitle = orderRoutestephandlerEntity.FieldValue3.IsNullOrWhiteSpace() ? Properties.Resources.EmailOrderRoute_DefaultTitle : orderRoutestephandlerEntity.FieldValue3;
            string emailHtml = GetEmailBody(orderEntity, orderRoutestephandlerEntity, out bool customBody);

            IDictionary<string, string> additionalTemplateVariables = GetAdditionalTemplateVariables(customBody, emailHtml, orderRoutestephandlerEntity);

            IOrderNotificationBuilder notificationBuilder = new OrderNotificationBuilder()
                .WithTemplate(new OrderNotification(emailTitle, emailHtml, string.Empty))
                .WithOrder(orderEntity)
                .WithAdditionalTemplateVars(additionalTemplateVariables);

            OrderNotification orderNotification = new OrderNotificationDirector(notificationBuilder)
                .BuildNotification();

            SendMailRequest mqRequest = new SendMailRequest
                                        {
                                            Title = orderNotification.Title,
                                            Message = orderNotification.Message,
                                            IsMessageHtml = true,
                                            Recipients = toAddresses,
                                            SenderAddress = fromAddress
                                        };

            // Publish send mail request on message bus
            MessageQueueReponse response = await sendMailUseCase.ExecuteAsync(mqRequest);
            if (response.Success)
            {
                // OrderRoutestephandler is complete when the email is sent
                orderRoutestephandlerEntity.Status = OrderRoutestephandlerStatus.Completed;
            }
            else
            {
                orderRoutestephandlerEntity.Status = OrderRoutestephandlerStatus.Failed;
                orderRoutestephandlerEntity.ErrorText = $"Failed to send Email to {string.Join(",", mqRequest.Recipients)}. Result message: {response.ErrorMessage}";
            }
        }

        private static string GetEmailBody(OrderEntity orderEntity, OrderRoutestephandlerEntity orderRoutestephandlerEntity, out bool customBody)
        {
            customBody = false;

            string emailHtml = Properties.Resources.NewOrder;
            string customBodyText = orderRoutestephandlerEntity.FieldValue4;
            bool dontUseDefaultTemplate = !orderRoutestephandlerEntity.FieldValue5.IsNullOrWhiteSpace() && orderRoutestephandlerEntity.FieldValue5.Equals("1");

            if (!customBodyText.IsNullOrWhiteSpace())
            {
                if (dontUseDefaultTemplate)
                {
                    emailHtml = customBodyText.Contains("<html>") ? customBodyText : orderRoutestephandlerEntity.FieldValue4.ReplaceLineBreaks("<br/>");
                    customBody = true;
                }
                else
                {
                    // Body with footer/header template                                            
                    emailHtml = emailHtml.Replace("[[BODY]]", customBodyText.ReplaceLineBreaks("<br/>"));
                }
            }
            else
            {
                // Use the default body.
                string emailHtmlBody = orderEntity.OutletId.HasValue ? Properties.Resources.NewOrderBodyAppLess : Properties.Resources.NewOrderBody;
                emailHtml = emailHtml.Replace("[[BODY]]", emailHtmlBody);
            }

            return emailHtml;
        }

        private IDictionary<string, string> GetAdditionalTemplateVariables(bool customBody, string emailHtml, OrderRoutestephandlerEntity orderRoutestephandlerEntity)
        {
            IDictionary<string, string> additionalVars = new Dictionary<string, string>();

            // Get images from Media if it's a customBody                        
            if (customBody && emailHtml.Contains("src=") && orderRoutestephandlerEntity.OriginatedFromRoutestepHandlerId.HasValue)
            {
                this.ConvertMediaToUrl(orderRoutestephandlerEntity.OriginatedFromRoutestepHandlerId.Value, additionalVars);
            }

            return additionalVars;
        }

        private List<string> GetEmailToAddresses(OrderRoutestephandlerEntity orderRoutestephandlerEntity, OrderEntity orderEntity)
        {
            List<string> toAddresses = new List<string>();

            // FieldValue1 = To E-mail (being a support pool, manually entered email address and/or to the customer)
            string toLine = GetEmailToLine(orderRoutestephandlerEntity, orderEntity);

            string[] addresses = toLine.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            toAddresses.AddRange(addresses);

            if (EnumerableExtensions.IsNullOrEmpty(toAddresses) && orderRoutestephandlerEntity.SupportpoolId.HasValue && orderRoutestephandlerEntity.SupportpoolId.Value > 0)
            {
                GetEmailAddressesForSupportpool getEmailAddressesForSupportpoolRequest = new GetEmailAddressesForSupportpool { SupportpoolId = orderRoutestephandlerEntity.SupportpoolId.Value };
                toAddresses = this.getEmailAddressesForSupportpoolUseCase.Execute(getEmailAddressesForSupportpoolRequest).ToList();
            }

            return toAddresses;
        }

        private static string GetEmailToLine(OrderRoutestephandlerEntity orderRoutestephandlerEntity, OrderEntity orderEntity)
        {
            string toLine = string.Empty;

            if (!orderRoutestephandlerEntity.FieldValue1.IsNullOrWhiteSpace())
            {
                toLine = StringUtil.CombineWithSeperator(";", toLine, orderRoutestephandlerEntity.FieldValue1);
            }

            if (!orderEntity.OutletId.HasValue &&
                !orderRoutestephandlerEntity.FieldValue6.IsNullOrWhiteSpace() &&
                orderRoutestephandlerEntity.FieldValue6.Equals("1"))
            {
                // For non-AppLess orders, FieldValue6 = To Customer (backwards-compatibility)
                if (!orderEntity.Email.IsNullOrWhiteSpace())
                {
                    toLine = StringUtil.CombineWithSeperator(";", toLine, orderEntity.Email);
                }
                else if (orderEntity.CustomerId.HasValue && orderEntity.Customer.Email.IsNullOrWhiteSpace())
                {
                    toLine = StringUtil.CombineWithSeperator(";", toLine, orderEntity.Customer.Email);
                }
            }

            // Make 1 splitter:
            return toLine.Replace(',', ';').Replace(' ', ';');
        }

        private void ConvertMediaToUrl(int routestephandlerId, IDictionary<string, string> templateVariables)
        {
            EntityCollection<MediaEntity> mediaCollection = this.routestephandlerRepository.GetMedia(routestephandlerId);
            foreach (MediaEntity media in mediaCollection)
            {
                foreach (MediaRatioTypeMediaEntity mediaRatioTypeMediaEntity in media.MediaRatioTypeMediaCollection)
                {
                    if (mediaRatioTypeMediaEntity.MediaType != MediaType.RouteStepHandlerEmailResource)
                    {
                        continue;
                    }
                    
                    string fileName = mediaRatioTypeMediaEntity.GetFileName(MediaRatioTypeMediaEntity.FileNameType.Cdn);
                    string subDirectoryName = mediaRatioTypeMediaEntity.MediaRatioType != null ? MediaUtil.GetEntitySubDirectoryName(mediaRatioTypeMediaEntity) : string.Empty;

                    string filePath = string.Join("/", subDirectoryName, fileName).ToLower();

                    string cdnPrefix = string.Empty;
                    if (mediaRatioTypeMediaEntity.MediaRatioType != null)
                    {
                        cdnPrefix = MediaUtil.GetMediaRatioTypeMediaPath(mediaRatioTypeMediaEntity, MediaRatioTypeMediaEntity.FileNameType.Cdn);
                    }

                    templateVariables.Add($"MEDIA:{media.Name}", Path.Combine(amazonConfiguration.S3.BaseUrl, cdnPrefix, filePath));
                    break;
                }
            }
        }
    }
}
