﻿using System.Collections.Generic;
using System.Linq;
using Crave.Ordering.Api.Domain.UseCases.Requests;
using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Ordering.Api.Domain.UseCases
{
    public class GetTerminalsForwardingFromThisTerminalUseCase : UseCaseBase<GetTerminalsForwardingFromThisTerminal, List<TerminalEntity>>
    {
        #region Fields

        private readonly ITerminalRepository terminalRepository;

        #endregion

        #region Constructors

        public GetTerminalsForwardingFromThisTerminalUseCase(ITerminalRepository terminalRepository)
        {
            this.terminalRepository = terminalRepository;
        }

        #endregion

        #region Methods

        public override List<TerminalEntity> Execute(GetTerminalsForwardingFromThisTerminal request)
        {
            List<TerminalEntity> forwardingTerminals = new List<TerminalEntity>();
            this.GetTerminalsForwardingFromThisTerminal(request.TerminalEntity, forwardingTerminals);

            return forwardingTerminals;
        }

        private void GetTerminalsForwardingFromThisTerminal(TerminalEntity terminalEntity, List<TerminalEntity> terminals)
        {
            if (terminalEntity.ForwardToTerminalId.HasValue && terminals.All(t => t.TerminalId != terminalEntity.ForwardToTerminalId.Value))
            {
                TerminalEntity forwardToTerminalEntity = this.terminalRepository.GetTerminal(terminalEntity.ForwardToTerminalId.Value);

                terminals.Add(forwardToTerminalEntity);
                this.GetTerminalsForwardingFromThisTerminal(forwardToTerminalEntity, terminals);
            }
        }

        #endregion
    }
}
