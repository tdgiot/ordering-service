﻿using System;
using System.Collections.Generic;
using Crave.Ordering.Api.Domain.UseCases.Requests;
using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using ServiceStack;
using System.Linq;

namespace Crave.Ordering.Api.Domain.UseCases
{
    public class GetEmailAddressesForSupportpoolUseCase : UseCaseBase<GetEmailAddressesForSupportpool, ICollection<string>>
    {
        private readonly ISupportpoolRepository supportpoolRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="GetEmailAddressesForSupportpoolUseCase"/> class.
        /// </summary>
        /// <param name="supportpoolRepository">Support pool repository</param>
        public GetEmailAddressesForSupportpoolUseCase(ISupportpoolRepository supportpoolRepository)
        {
            this.supportpoolRepository = supportpoolRepository;
        }

        /// <summary>
        /// Executes the use-case by getting the email address for the support pool within the request DTO.
        /// </summary>
        /// <param name="request">The request DTO containing the parameters to get the email addresses.</param>
        /// <returns>A <see cref="IEnumerable{string}"/> instance containing the email addresses.</returns>
        public override ICollection<string> Execute(GetEmailAddressesForSupportpool request)
        {
            List<string> emailAddresses = new List<string>();
            SupportpoolEntity supportpoolEntity = supportpoolRepository.GetSupportpoolEmailAddresses(request.SupportpoolId);

            emailAddresses.AddRange(
                supportpoolEntity.SupportpoolSupportagentCollection
                .Select(x => x.Supportagent.Email)
                .Where(x => !x.IsNullOrWhiteSpace() && !emailAddresses.Contains(x)));

            emailAddresses.AddRange(
                supportpoolEntity.SupportpoolNotificationRecipientCollection
                .Select(x => x.Email)
                .Where(x => !x.IsNullOrWhiteSpace() && !emailAddresses.Contains(x)));

            // Code below is for backwards compatibility
            if (supportpoolEntity.Email.Contains(",", StringComparison.InvariantCultureIgnoreCase))
            {
                emailAddresses.AddRange(
                    supportpoolEntity.Email.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                    .Where(x => !x.IsNullOrWhiteSpace() && !emailAddresses.Contains(x)));
            }
            else if (supportpoolEntity.Email.Contains(";", StringComparison.InvariantCultureIgnoreCase))
            {
                emailAddresses.AddRange(
                    supportpoolEntity.Email.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries)
                    .Where(x => !x.IsNullOrWhiteSpace() && !emailAddresses.Contains(x)));
            }
            else if (!supportpoolEntity.Email.IsNullOrEmpty() && !emailAddresses.Contains(supportpoolEntity.Email))
            {
                emailAddresses.Add(supportpoolEntity.Email);
            }

            return emailAddresses;
        }
    }
}