﻿using System;
using Crave.Ordering.Shared.Enums;

namespace Crave.Ordering.Api.Domain.UseCases.Responses
{
    public class CreateOrderRouteResponse
    {
        public CreateOrderRouteResponse(CreateOrderRouteResult result)
        {
            this.Result = result;
        }

        public CreateOrderRouteResponse(CreateOrderRouteResult result, Exception exception, string errorType)
        {
            this.Result = result;
            this.Exception = exception;
            this.ErrorMessage = exception.Message;
            this.StackTrace = exception.StackTrace;
            this.ErrorType = errorType;
        }

        /// <summary>
        /// The result of the create order route use case.
        /// </summary>
        public CreateOrderRouteResult Result { get; }
        /// <summary>
        /// The error message when creation of route is not successful.
        /// </summary>
        public string ErrorMessage { get; }
        /// <summary>
        /// The error type when creation of route is not successful.
        /// </summary>
        public string ErrorType { get; }
        /// <summary>
        /// The stack trace type when creation of order is not successful.
        /// </summary>
        public string StackTrace { get; }

        public Exception? Exception { get; }
    }
}
