﻿namespace Crave.Ordering.Api.Domain.UseCases.Responses
{
    public class CreateAndSendReceiptResponse
    {
        public bool EmailSent { get; set; }
        public bool SmsSent { get; set; }
        public string ErrorMessage { get; set; } = string.Empty;
    }
}
