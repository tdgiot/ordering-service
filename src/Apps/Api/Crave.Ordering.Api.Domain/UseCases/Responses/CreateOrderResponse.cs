﻿using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Enums;

namespace Crave.Ordering.Api.Domain.UseCases.Responses
{
    public class CreateOrderResponse
    {
        public CreateOrderResponse(CreateOrderResult result)
        {
            this.Result = result;
        }

        public CreateOrderResponse(CreateOrderResult result, OrderEntity orderEntity)
        {
            this.Result = result;
            this.OrderEntity = orderEntity;
        }

        public CreateOrderResponse(CreateOrderResult result, string errorMessage, string errorType, string stackTrace)
        {
            this.Result = result;
            this.ErrorMessage = errorMessage;
            this.ErrorType = errorType;
            this.StackTrace = stackTrace;
        }

        /// <summary>
        /// The result of the create order use case.
        /// </summary>
        public CreateOrderResult Result { get; }
        /// <summary>
        /// The saved order entity.
        /// </summary>
        public OrderEntity OrderEntity { get; }
        /// <summary>
        /// The error message when creation of order is not successful.
        /// </summary>
        public string ErrorMessage { get; }
        /// <summary>
        /// The error type when creation of order is not successful.
        /// </summary>
        public string ErrorType { get; }
        /// <summary>
        /// The stack trace when creation of order is not successful.
        /// </summary>
        public string StackTrace { get; }
    }
}
