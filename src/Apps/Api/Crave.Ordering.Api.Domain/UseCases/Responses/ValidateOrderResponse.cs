﻿using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Ordering.Api.Domain.UseCases.Responses
{
    public class ValidateOrderResponse
    {
        public ValidateOrderResponse(OrderEntity orderEntity, ValidationResponse validationResponse)
        {
            this.ValidationResponse = validationResponse;
            this.OrderEntity = orderEntity;
        }

        public ValidationResponse ValidationResponse { get; }
        public OrderEntity OrderEntity { get; }
    }
}
