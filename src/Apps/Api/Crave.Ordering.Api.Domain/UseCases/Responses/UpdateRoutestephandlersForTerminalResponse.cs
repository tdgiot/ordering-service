﻿namespace Crave.Ordering.Api.Domain.UseCases.Responses
{
    public class UpdateRoutestephandlersForTerminalResponse
    {
        public static UpdateRoutestephandlersForTerminalResponse AsSuccess()
        {
            return new UpdateRoutestephandlersForTerminalResponse
            {
                Success = true
            };
        }

        public static UpdateRoutestephandlersForTerminalResponse AsError(string message)
        {
            return new UpdateRoutestephandlersForTerminalResponse
            {
                Message = message
            };
        }

        public bool Success { get; private set; }
        public string Message { get; private set; } = string.Empty;

    }
}