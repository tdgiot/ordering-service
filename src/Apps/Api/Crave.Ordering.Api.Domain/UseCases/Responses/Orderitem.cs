﻿using System.Collections.Generic;

namespace Crave.Ordering.Api.Domain.UseCases.Responses
{
    public class Orderitem
    {
        public Orderitem(int orderitemId,
                         int? productId,
                         int? categoryId,
                         int? priceLevelItemId,
                         int? taxTariffId,
                         string guid,
                         string productName,
                         decimal productPriceIn,
                         int quantity,
                         string notes,
                         int color,
                         string taxName,
                         double taxPercentage,
                         decimal priceTotalExTax,
                         decimal taxTotal,
                         IEnumerable<OrderitemAlterationitem> orderitemAlterationitems)
        {
            this.OrderitemId = orderitemId;
            this.ProductId = productId;
            this.CategoryId = categoryId;
            this.PriceLevelItemId = priceLevelItemId;
            this.TaxTariffId = taxTariffId;
            this.Guid = guid;
            this.ProductName = productName;
            this.ProductPriceIn = productPriceIn;
            this.Quantity = quantity;
            this.Notes = notes;
            this.Color = color;
            this.TaxName = taxName;
            this.TaxPercentage = taxPercentage;
            this.PriceTotalExTax = priceTotalExTax;
            this.TaxTotal = taxTotal;
            this.OrderitemAlterationitems = orderitemAlterationitems;
        }

        public int OrderitemId { get; }
        public int? ProductId { get; }
        public int? CategoryId { get; }
        public int? PriceLevelItemId { get; }
        public int? TaxTariffId { get; }
        public string Guid { get; }
        public string ProductName { get; }
        public decimal ProductPriceIn { get; }
        public int Quantity { get; }
        public string Notes { get; }
        public int Color { get; }
        public string TaxName { get; }
        public double TaxPercentage { get; }
        public decimal PriceTotalExTax { get; }
        public decimal TaxTotal { get; }
        public IEnumerable<OrderitemAlterationitem> OrderitemAlterationitems { get; }
    }
}
