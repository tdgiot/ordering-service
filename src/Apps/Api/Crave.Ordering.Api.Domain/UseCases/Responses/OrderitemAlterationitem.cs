﻿using System;
using Crave.Enums;

namespace Crave.Ordering.Api.Domain.UseCases.Responses
{
    public class OrderitemAlterationitem
    {
        public OrderitemAlterationitem(AlterationType alterationType,
                                       int? alterationitemId,
                                       int? alterationId,
                                       int? priceLevelItemId,
                                       int? taxTariffId,
                                       string alterationName,
                                       string alterationoptionName,
                                       string value,
                                       DateTime? timeUtc,
                                       bool orderLevelEnabled,
                                       string taxName,
                                       double taxPercentage,
                                       decimal priceTotalExTax,
                                       decimal taxTotal,
                                       decimal alterationoptionPriceIn,
                                       Alterationitem? alterationitem)
        {
            this.AlterationType = alterationType;
            this.AlterationitemId = alterationitemId;
            this.AlterationId = alterationId;
            this.PriceLevelItemId = priceLevelItemId;
            this.TaxTariffId = taxTariffId;
            this.AlterationName = alterationName;
            this.AlterationoptionName = alterationoptionName;
            this.Value = value;
            this.TimeUTC = timeUtc;
            this.OrderLevelEnabled = orderLevelEnabled;
            this.TaxName = taxName;
            this.TaxPercentage = taxPercentage;
            this.PriceTotalExTax = priceTotalExTax;
            this.TaxTotal = taxTotal;
            this.AlterationoptionPriceIn = alterationoptionPriceIn;
            this.Alterationitem = alterationitem;
        }

        public AlterationType AlterationType { get; }
        public int? AlterationitemId { get; }
        public int? AlterationId { get; }
        public int? PriceLevelItemId { get; }
        public int? TaxTariffId { get; }
        public string AlterationName { get; }
        public string AlterationoptionName { get; }
        public string Value { get; }
        public DateTime? TimeUTC { get; }
        public bool OrderLevelEnabled { get; }
        public string TaxName { get; }
        public double TaxPercentage { get; }
        public decimal PriceTotalExTax { get; }
        public decimal TaxTotal { get; }
        public decimal AlterationoptionPriceIn { get; }
        public Alterationitem? Alterationitem { get; }
    }
}
