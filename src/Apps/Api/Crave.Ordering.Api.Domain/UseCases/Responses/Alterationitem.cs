﻿namespace Crave.Ordering.Api.Domain.UseCases.Responses
{
    public class Alterationitem
    {
        public Alterationitem(int alterationitemId, 
                              int alterationId, 
                              int alterationoptionId)
        {
            this.AlterationitemId = alterationitemId;
            this.AlterationId = alterationId;
            this.AlterationoptionId = alterationoptionId;
        }

        public int AlterationitemId { get; }
        public int AlterationId { get; }
        public int AlterationoptionId { get; }
    }
}
