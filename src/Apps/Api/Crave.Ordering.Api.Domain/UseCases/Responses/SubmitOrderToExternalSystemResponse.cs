﻿namespace Crave.Ordering.Api.Domain.UseCases.Responses
{
    public class SubmitOrderToExternalSystemResponse
    {
        public static SubmitOrderToExternalSystemResponse SuccessResponse() => new SubmitOrderToExternalSystemResponse(true, string.Empty);
        public static SubmitOrderToExternalSystemResponse FailResponse(string errorText) => new SubmitOrderToExternalSystemResponse(false, errorText);

        private SubmitOrderToExternalSystemResponse(bool success, string errorText)
        {
            this.Success = success;
            this.ErrorText = errorText;
        }

        public bool Success { get; }
        public string ErrorText { get; }
    }
}
