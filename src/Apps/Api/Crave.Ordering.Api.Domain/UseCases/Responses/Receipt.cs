﻿namespace Crave.Ordering.Api.Domain.UseCases.Responses
{
    public class Receipt
    {
        public Receipt(int number,
                       string sellerName,
                       string sellerAddress,
                       string sellerContactInfo,
                       string serviceMethodName,
                       string checkoutmethodName)
        {
            this.Number = number;
            this.SellerName = sellerName;
            this.SellerAddress = sellerAddress;
            this.SellerContactInfo = sellerContactInfo;
            this.ServiceMethodName = serviceMethodName;
            this.CheckoutMethodName = checkoutmethodName;
        }

        public int Number { get; }
        public string SellerName { get; }
        public string SellerAddress { get; }
        public string SellerContactInfo { get; }
        public string ServiceMethodName { get; }
        public string CheckoutMethodName { get; }
    }
}
