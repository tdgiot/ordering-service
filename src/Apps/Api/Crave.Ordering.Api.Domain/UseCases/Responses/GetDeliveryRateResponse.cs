﻿using Crave.Libraries.Distance.Models;
using Crave.Ordering.Api.Domain.Enums;

namespace Crave.Ordering.Api.Domain.UseCases.Responses
{
    public class GetDeliveryRateResponse
    {
        public GetDeliveryRateResponse(DeliveryRateResult result)
        {
            this.Result = result;
        }

        public GetDeliveryRateResponse(DeliveryRateResult result, string errorMessage)
        {
            this.Result = result;
            this.ErrorMessage = errorMessage;
        }

        public GetDeliveryRateResponse(
            DeliveryRateResult result,
            decimal charge, 
            Distance distance, 
            int? taxTariffId,
            decimal chargeTaxIncluded, 
            decimal chargeTaxExcluded)
        {
            this.Result = result;
            this.Charge = charge;
            this.Distance = distance;
            this.TaxTariffId = taxTariffId;
            this.ChargeTaxIncluded = chargeTaxIncluded;
			this.ChargeTaxExcluded = chargeTaxExcluded;
        }

        /// <summary>
        /// Gets the delivery rate result.
        /// </summary>
        public DeliveryRateResult Result { get; }

        /// <summary>
        /// Gets the delivery charge.
        /// </summary>
        public decimal Charge { get; }

        /// <summary>
        /// Gets the delivery charge including tax.
        /// </summary>
        public decimal ChargeTaxIncluded { get; }

        /// <summary>
        /// Gets the delivery charge excluding tax.
        /// </summary>
        public decimal ChargeTaxExcluded { get; }

        /// <summary>
        /// Gets the delivery distance.
        /// </summary>
        public Distance? Distance { get; }

        /// <summary>
        /// Gets the error message.
        /// </summary>
        public string ErrorMessage { get; }
        
        /// <summary>
        /// Gets the TaxTariff Id
        /// </summary>
        public int? TaxTariffId { get; }
    }
}
