﻿namespace Crave.Ordering.Domain.UseCases.Responses
{
    public class CreateAndSendOrderNotificationResponse
    {
        public CreateAndSendOrderNotificationResponse(bool success)
        {
            this.Success = success;
        }

        public CreateAndSendOrderNotificationResponse(bool success, string message)
        {
            this.Success = success;
            this.Message = message;
        }

        public bool Success { get; }
        public string Message { get; } = string.Empty;
    }
}
