﻿namespace Crave.Ordering.Api.Domain.UseCases.Responses
{
    public class PaymentTransaction
    {
        public PaymentTransaction(string cardSummary, string paymentMethod)
        {
            this.CardSummary = cardSummary;
            this.PaymentMethod = paymentMethod;
        }

        public string CardSummary { get; }
        public string PaymentMethod { get; }
    }
}