﻿namespace Crave.Ordering.Api.Domain.UseCases.Responses
{
    public class ResponseBase
    {
        protected const int StatusOk = 0;
        protected const int StatusError = 1;
        protected const int StatusNotFound = 2;

        private readonly int status;

        public static ResponseBase AsSuccess() => new ResponseBase(StatusOk);

        public static ResponseBase AsError(string message) => new ResponseBase(StatusError)
        {
            Message = message
        };

        public static ResponseBase AsNotFound(string message = "") => new ResponseBase(StatusNotFound)
        {
            Message = message
        };

        protected ResponseBase(int status)
        {
            this.status = status;
        }

        public bool Success => status == StatusOk;
        public bool NotFound => status == StatusNotFound;
        public string Message { get; protected set; } = string.Empty;
    }

    public class ResponseBase<T> : ResponseBase where T : class
    {
        private ResponseBase(int status) : base(status) 
        {
            
        }

        public static ResponseBase<T> AsSuccess(T obj) => new ResponseBase<T>(StatusOk)
        {
            Model = obj
        };

        public new static ResponseBase<T> AsError(string message) => new ResponseBase<T>(StatusError)
        {
            Message = message
        };

        public new static ResponseBase<T> AsNotFound(string message = "") => new ResponseBase<T>(StatusNotFound)
        {
            Message = message
        };

        public T? Model { get; private set; }
    }
}