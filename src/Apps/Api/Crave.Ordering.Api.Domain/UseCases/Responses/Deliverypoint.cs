﻿namespace Crave.Ordering.Api.Domain.UseCases.Responses
{
    public class Deliverypoint
    {
        public Deliverypoint(int? deliverypointId, 
                             int? number, 
                             string name)
        {
            this.DeliverypointId = deliverypointId;
            this.Number = number;
            this.Name = name;
        }
        
        public int? DeliverypointId { get; }
        public int? Number { get; }
        public string Name { get; }
    }
}
