﻿namespace Crave.Ordering.Api.Domain.UseCases.Responses
{
    public class Customer
    {
        public Customer(int? customerId, 
                        string? firstname, 
                        string? lastname, 
                        string? lastnamePrefix, 
                        string? email, 
                        string? phonenumber, 
                        bool isVip)
        {
            this.CustomerId = customerId;
            this.Firstname = firstname;
            this.Lastname = lastname;
            this.LastnamePrefix = lastnamePrefix;
            this.Email = email;
            this.Phonenumber = phonenumber;
            this.IsVip = isVip;
        }
        
        public int? CustomerId { get; }
        public string? Firstname { get; }
        public string? Lastname { get; }
        public string? LastnamePrefix { get; }
        public string? Email { get; }
        public string? Phonenumber { get; }
        public bool IsVip { get; }
    }
}
