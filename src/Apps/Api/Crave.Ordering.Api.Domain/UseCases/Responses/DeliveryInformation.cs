﻿namespace Crave.Ordering.Api.Domain.UseCases.Responses
{
    public class DeliveryInformation
    {
        public DeliveryInformation(string buildingName,
                                   string address,
                                   string zipcode,
                                   string city,
                                   string instructions)
        {
            this.BuildingName = buildingName;
            this.Address = address;
            this.Zipcode = zipcode;
            this.City = city;
            this.Instructions = instructions;
        }

        public string BuildingName { get; }
        public string Address { get; }
        public string Zipcode { get; }
        public string City { get; }
        public string Instructions { get; }
    }
}
