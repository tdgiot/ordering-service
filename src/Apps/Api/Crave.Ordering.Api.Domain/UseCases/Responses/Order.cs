﻿using System;
using System.Collections.Generic;
using Crave.Enums;

namespace Crave.Ordering.Api.Domain.UseCases.Responses
{
    public class Order
    {
        public Order(OrderType type,
                     int orderId,
                     string guid,
                     int companyId,
                     int? outletId,
                     int? clientId,
                     bool printed,
                     string notes,
                     OrderStatus status,
                     DateTime? processedUtc,
                     DateTime? createdUtc,
                     OrderErrorCode? errorCode,
                     CheckoutType? checkoutMethodType,
                     bool pricesIncludeTaxes,
                     TaxBreakdown taxBreakdown,
                     decimal total,
                     decimal subTotal,
                     string currencyCode,
                     string countryCode,
                     Deliverypoint deliverypoint,
                     Customer customer,
                     DeliveryInformation? deliveryInformation,
                     Receipt? receipt,
                     IEnumerable<Orderitem> orderitems,
                     IEnumerable<OrderRoutestephandler> orderRoutestephandlers,
                     PaymentTransaction? paymentTransaction,
                     int? coversCount
                     )
        {
            this.Type = type;
            this.OrderId = orderId;
            this.Guid = guid;
            this.CompanyId = companyId;
            this.OutletId = outletId;
            this.ClientId = clientId;
            this.Printed = printed;
            this.Notes = notes;
            this.Status = status;
            this.ProcessedUtc = processedUtc;
            this.CreatedUtc = createdUtc;
            this.ErrorCode = errorCode;
            this.CheckoutMethodType = checkoutMethodType;
            this.PricesIncludeTaxes = pricesIncludeTaxes;
            this.TaxBreakdown = taxBreakdown;
            this.Total = total;
            this.SubTotal = subTotal;
            this.CurrencyCode = currencyCode;
            this.CountryCode = countryCode;
            this.Deliverypoint = deliverypoint;
            this.Customer = customer;
            this.DeliveryInformation = deliveryInformation;
            this.Receipt = receipt;
            this.Orderitems = orderitems;
            this.OrderRoutestephandlers = orderRoutestephandlers;
            this.PaymentTransaction = paymentTransaction;
            this.CoversCount = coversCount;
        }

        public OrderType Type { get; }
        public int OrderId { get; }
        public string Guid { get; }
        public int CompanyId { get; }
        public int? OutletId { get; }
        public int? ClientId { get; }
        public bool Printed { get; }
        public string Notes { get; }
        public OrderStatus Status { get; }
        public DateTime? ProcessedUtc { get; }
        public DateTime? CreatedUtc { get; }
        public OrderErrorCode? ErrorCode { get; }
        public CheckoutType? CheckoutMethodType { get; }
        public bool PricesIncludeTaxes { get; }
        public TaxBreakdown TaxBreakdown { get; }
        public decimal Total { get; }
        public decimal SubTotal { get; }
        public string CurrencyCode { get; }
        public string CountryCode { get; }
        public Deliverypoint Deliverypoint { get; }
        public Customer Customer { get; }
        public DeliveryInformation? DeliveryInformation { get; }
        public Receipt? Receipt { get; }
        public IEnumerable<Orderitem> Orderitems { get; }
        public IEnumerable<OrderRoutestephandler> OrderRoutestephandlers { get; }
        public PaymentTransaction? PaymentTransaction { get; set; }
        public int? CoversCount { get; set; }
    }
}
