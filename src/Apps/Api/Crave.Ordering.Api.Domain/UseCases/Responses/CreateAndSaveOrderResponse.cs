﻿namespace Crave.Ordering.Api.Domain.UseCases.Responses
{
    public class CreateAndSaveOrderResponse
    {
        private CreateAndSaveOrderResponse(bool success,
                                           string orderGuid,
                                           ValidationResponse? validationResponse)
        {
            this.Success = success;
            this.OrderGuid = orderGuid;
            this.ValidationResponse = validationResponse;
        }

        public static CreateAndSaveOrderResponse AsSuccess(string orderGuid)
        {
            return new CreateAndSaveOrderResponse(true, orderGuid, null);
        }

        public static CreateAndSaveOrderResponse AsFailure(ValidationResponse validationResponse)
        {
            return new CreateAndSaveOrderResponse(false, string.Empty, validationResponse);
        }
        
        public bool Success { get; }
        public string OrderGuid { get; }
        public ValidationResponse? ValidationResponse { get; }
    }
}
