﻿namespace Crave.Ordering.Api.Domain.UseCases.Responses
{
    public class UpdateOrderResponse
    {
        public bool Success { get; set; }
        public string Message { get; set; }
    }
}