﻿using System;
using Crave.Enums;

namespace Crave.Ordering.Api.Domain.UseCases.Responses
{
    public class OrderRoutestephandler
    {
        public OrderRoutestephandler(int orderRoutestephandlerId,
                                     int? originatedFromRoutestepHandlerId,
                                     string guid,
                                     int orderId,
                                     int? terminalId,
                                     int number,
                                     RoutestephandlerType routestephandlerType,
                                     PrintReportType? printReportType,
                                     OrderRoutestephandlerStatus status,
                                     OrderErrorCode errorCode,
                                     DateTime? completedUtc,
                                     int timeout,
                                     DateTime? timeoutExpiresUtc,
                                     int? forwardedFromTerminalId,
                                     string fieldValue1,
                                     string fieldValue2,
                                     string fieldValue3,
                                     string fieldValue4,
                                     string fieldValue5,
                                     string fieldValue6,
                                     string fieldValue7,
                                     string fieldValue8,
                                     string fieldValue9,
                                     string fieldValue10)
        {
            this.OrderRoutestephandlerId = orderRoutestephandlerId;
            this.OriginatedFromRoutestepHandlerId = originatedFromRoutestepHandlerId;
            this.Guid = guid;
            this.OrderId = orderId;
            this.TerminalId = terminalId;
            this.Number = number;
            this.RoutestephandlerType = routestephandlerType;
            this.PrintReportType = printReportType;
            this.Status = status;
            this.ErrorCode = errorCode;
            this.CompletedUtc = completedUtc;
            this.Timeout = timeout;
            this.TimeoutExpiresUtc = timeoutExpiresUtc;
            this.ForwardedFromTerminalId = forwardedFromTerminalId;
            this.FieldValue1 = fieldValue1;
            this.FieldValue2 = fieldValue2;
            this.FieldValue3 = fieldValue3;
            this.FieldValue4 = fieldValue4;
            this.FieldValue5 = fieldValue5;
            this.FieldValue6 = fieldValue6;
            this.FieldValue7 = fieldValue7;
            this.FieldValue8 = fieldValue8;
            this.FieldValue9 = fieldValue9;
            this.FieldValue10 = fieldValue10;
        }

        public int OrderRoutestephandlerId { get; }
        public int? OriginatedFromRoutestepHandlerId { get; }
        public string Guid { get; }
        public int OrderId { get; }
        public int? TerminalId { get; }
        public int Number { get; }
        public RoutestephandlerType RoutestephandlerType { get; }
        public PrintReportType? PrintReportType { get; }
        public OrderRoutestephandlerStatus Status { get; }
        public OrderErrorCode ErrorCode { get; }
        public DateTime? CompletedUtc { get; }
        public int Timeout { get; }
        public DateTime? TimeoutExpiresUtc { get; }
        public int? ForwardedFromTerminalId { get; }
        public string FieldValue1 { get; }
        public string FieldValue2 { get; }
        public string FieldValue3 { get; }
        public string FieldValue4 { get; }
        public string FieldValue5 { get; }
        public string FieldValue6 { get; }
        public string FieldValue7 { get; }
        public string FieldValue8 { get; }
        public string FieldValue9 { get; }
        public string FieldValue10 { get; }
    }
}
