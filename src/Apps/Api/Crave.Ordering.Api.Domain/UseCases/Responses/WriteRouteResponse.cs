﻿namespace Crave.Ordering.Api.Domain.UseCases.Responses
{
    public class WriteRouteResponse
    {

        public WriteRouteResponse(bool success, bool markOrderAsProcessed) => (Success, MarkOrderAsProcessed) = (success, markOrderAsProcessed);

        public bool Success { get; }
        public bool MarkOrderAsProcessed { get; }
    }
}