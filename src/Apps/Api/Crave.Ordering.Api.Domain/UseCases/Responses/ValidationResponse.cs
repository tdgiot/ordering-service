﻿using System.Collections.Generic;
using System.Linq;
using Crave.Ordering.Api.Domain.Validation.Errors;

namespace Crave.Ordering.Api.Domain.UseCases.Responses
{
    public class ValidationResponse
    {
        public ValidationResponse(IEnumerable<ValidationError> validationErrors)
        {
            this.ValidationErrors = validationErrors;
        }

        public IEnumerable<ValidationError> ValidationErrors { get; }

        public bool Valid => !this.ValidationErrors.Any();
    }
}
