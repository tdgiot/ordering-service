﻿using System.Collections.Generic;
using Crave.Enums;
using Crave.Ordering.Api.Domain.UseCases.Requests;
using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Interfaces;
using System.Threading.Tasks;

namespace Crave.Ordering.Api.Domain.UseCases
{
    public class FailExpiredOrderroutestephandlersForTerminalUseCase : IUseCaseAsync<FailExpiredRoutestephandlersForTerminal, bool>
    {
        private readonly IOrderRoutestephandlerRepository orderRoutestephandlerRepository;
        private readonly UpdateOrderRoutestephandlerUseCase updateOrderRoutestephandlerUseCase;
        private const int EXCEUTE_ATTEMPT_LIMIT = 7; // 7 is totally arbitrary, but happens to be a 'Newman–Shanks–Williams prime', so good choice.

        /// <summary>
        /// Initializes a new instance of the <see cref="FailExpiredOrderroutestephandlersForTerminalUseCase"/> class.
        /// </summary>
        /// <param name="orderRoutestephandlerRepository">The order routestephandler repository.</param>
        /// <param name="updateOrderRoutestephandlerUseCase">Use case for update the order routestephandler status.</param>
        public FailExpiredOrderroutestephandlersForTerminalUseCase(IOrderRoutestephandlerRepository orderRoutestephandlerRepository,
                                                                   UpdateOrderRoutestephandlerUseCase updateOrderRoutestephandlerUseCase)
        {
            this.orderRoutestephandlerRepository = orderRoutestephandlerRepository;
            this.updateOrderRoutestephandlerUseCase = updateOrderRoutestephandlerUseCase;
        }

        /// <summary>
        /// Executes the logic of the use case by failing the expires order routestephandlers for the terminal id in the specified request DTO.
        /// </summary>
        /// <param name="request">The <see cref="FailExpiredRoutestephandlersForTerminal"/> instance containing the parameters to expire the order routestephandlers.</param>
        /// <returns>An <see cref="bool"/> instance indicates whether the update was successful.</returns>
        public async Task<bool> ExecuteAsync(FailExpiredRoutestephandlersForTerminal request)
        {
            int? terminalId = request.TerminalId;
            List<int> orderIds = new List<int>();
            int attemptNo = 1;

            while (true)
            {
                ICollection<OrderRoutestephandlerEntity> orderRoutestephandlerCollection = this.orderRoutestephandlerRepository.GetExpiredOrdersForTerminal(terminalId, orderIds);

                List<int> processedOrderIds = new List<int>();
                List<int> orderIdsWithMultipleStepsToExpire = new List<int>();

                foreach (OrderRoutestephandlerEntity orderRoutestephandlerEntity in orderRoutestephandlerCollection)
                {
                    // Keep track of the OrderIds that we have handled. We only handle one step per Order 
                    // because normally the validator will cancel the other steps and we would have 'out-of-date' data that
                    // we would be updating as the steps would be updated in the DB but here in memory still have the old data.
                    if (processedOrderIds.Contains(orderRoutestephandlerEntity.OrderId))
                    {
                        // Keep track of Order of which we have multiple steps, those will be processed in a 'recursive'
                        // call of this method.
                        orderIdsWithMultipleStepsToExpire.Add(orderRoutestephandlerEntity.OrderId);
                    }
                    else
                    {
                        // Fail the step.
                        processedOrderIds.Add(orderRoutestephandlerEntity.OrderId);
                        orderRoutestephandlerEntity.Status = OrderRoutestephandlerStatus.Failed;
                        orderRoutestephandlerEntity.ErrorCode = OrderErrorCode.TimedOut;

                        await updateOrderRoutestephandlerUseCase.ExecuteAsync(orderRoutestephandlerEntity);
                    }
                }

                // If we have Orders for which we have multiple steps we will call this method again
                // for those specific Orders.
                if (orderIdsWithMultipleStepsToExpire.Count > 0)
                {
                    if (attemptNo > EXCEUTE_ATTEMPT_LIMIT) 
                    {
                        // Throw exception to prevent StackOverflow
                        throw new CraveException(RoutingResult.PotentialStackOverflowPrevented, "RoutingHelper.FailExpiredRoutestephandlers");
                    }

                    terminalId = null;
                    orderIds = orderIdsWithMultipleStepsToExpire;

                    ++attemptNo;
                    continue;
                }

                break;
            }

            return true;
        }
    }
}
