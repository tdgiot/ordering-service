﻿using System;
using System.Collections.Generic;
using System.Linq;
using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Extensions;

namespace Crave.Ordering.Api.Domain.UseCases
{
    public class EnrichOrderWithTaxesUseCase : UseCaseBase<OrderEntity>
    {
        private readonly ITaxTariffRepository taxTariffRepository;
        private const int DECIMAL_PLACES = 2;

        public EnrichOrderWithTaxesUseCase(ITaxTariffRepository taxTariffRepository)
        {
            this.taxTariffRepository = taxTariffRepository;
        }

        public override void Execute(OrderEntity request)
        {
            OrderEntity orderEntity = request;

            IEnumerable<OrderitemAlterationitemEntity>? orderItemAlterationItemEntities = orderEntity.OrderitemCollection
                .SelectMany(orderItem => orderItem.OrderitemAlterationitemCollection);

            EntityCollection<TaxTariffEntity> taxTariffs = FetchTaxTariffs(orderEntity.OrderitemCollection, orderItemAlterationItemEntities);
            Dictionary<int, TaxTariffEntity> taxTariffMap = taxTariffs.ToDictionary(taxTariff => taxTariff.TaxTariffId, taxTariff => taxTariff);

            foreach (OrderitemEntity orderitemEntity in orderEntity.OrderitemCollection)
            {
                TaxTariffEntity? taxTariffEntity = null;

                if (orderitemEntity.TaxTariffId.HasValue)
                {
                    taxTariffMap.TryGetValue(orderitemEntity.TaxTariffId.Value, out taxTariffEntity);
                }

                SetTaxTariff(orderitemEntity, taxTariffEntity, orderEntity.PricesIncludeTaxes);

                foreach (OrderitemAlterationitemEntity orderitemAlterationitemEntity in orderitemEntity.OrderitemAlterationitemCollection)
                {
                    TaxTariffEntity? alterationItemTaxTariffEntity = taxTariffEntity;

                    if (orderitemAlterationitemEntity.TaxTariffId.HasValue)
                    {
                        taxTariffMap.TryGetValue(orderitemAlterationitemEntity.TaxTariffId.Value, out alterationItemTaxTariffEntity);
                    }

                    SetTaxTariff(orderitemAlterationitemEntity, alterationItemTaxTariffEntity, orderEntity.PricesIncludeTaxes);
                }
            }

            orderEntity.Total = Math.Round(orderEntity.OrderitemCollection.Sum(o => o.Total), DECIMAL_PLACES, MidpointRounding.AwayFromZero);
            orderEntity.SubTotal = Math.Round(orderEntity.OrderitemCollection.Sum(o => o.SubTotal), DECIMAL_PLACES, MidpointRounding.AwayFromZero);
        }

        private static void SetTaxTariff(OrderitemEntity orderitemEntity, TaxTariffEntity? taxTariffEntity, bool pricesIncludeTaxes)
        {
            orderitemEntity.TaxTariffId = taxTariffEntity?.TaxTariffId;
            orderitemEntity.TaxName = taxTariffEntity?.Name;
            orderitemEntity.TaxPercentage = taxTariffEntity?.Percentage ?? 0;

            decimal tax = orderitemEntity.ProductPriceIn.CalculateTax(orderitemEntity.TaxPercentage, pricesIncludeTaxes);
            decimal taxTotal = orderitemEntity.ProductPriceIn.CalculateTaxTotal(orderitemEntity.TaxPercentage, orderitemEntity.Quantity, pricesIncludeTaxes);

            orderitemEntity.Tax = Math.Round(tax, DECIMAL_PLACES, MidpointRounding.AwayFromZero);
            orderitemEntity.TaxTotal = Math.Round(taxTotal, DECIMAL_PLACES, MidpointRounding.AwayFromZero);

            orderitemEntity.PriceInTax = orderitemEntity.ProductPriceIn.ToPriceInTax(tax, pricesIncludeTaxes);
            orderitemEntity.PriceExTax = orderitemEntity.ProductPriceIn.ToPriceExTax(tax, pricesIncludeTaxes);

            orderitemEntity.PriceTotalInTax = orderitemEntity.ProductPriceIn.ToPriceTotalInTax(tax, orderitemEntity.Quantity, pricesIncludeTaxes);
            orderitemEntity.PriceTotalExTax = orderitemEntity.ProductPriceIn.ToPriceTotalExTax(tax, orderitemEntity.Quantity, pricesIncludeTaxes);
        }        

        private static void SetTaxTariff(OrderitemAlterationitemEntity orderitemAlterationitemEntity, TaxTariffEntity? taxTariffEntity, bool pricesIncludeTaxes)
        {
            orderitemAlterationitemEntity.TaxTariffId = taxTariffEntity?.TaxTariffId;
            orderitemAlterationitemEntity.TaxName = taxTariffEntity?.Name;
            orderitemAlterationitemEntity.TaxPercentage = taxTariffEntity?.Percentage ?? 0;

            decimal tax = orderitemAlterationitemEntity.AlterationoptionPriceIn.CalculateTax(orderitemAlterationitemEntity.TaxPercentage, pricesIncludeTaxes);
            decimal taxTotal = orderitemAlterationitemEntity.AlterationoptionPriceIn.CalculateTaxTotal(orderitemAlterationitemEntity.TaxPercentage, orderitemAlterationitemEntity.Quantity, pricesIncludeTaxes);

            orderitemAlterationitemEntity.Tax = Math.Round(tax, DECIMAL_PLACES, MidpointRounding.AwayFromZero);
            orderitemAlterationitemEntity.TaxTotal = Math.Round(taxTotal, DECIMAL_PLACES, MidpointRounding.AwayFromZero);

            orderitemAlterationitemEntity.PriceInTax = orderitemAlterationitemEntity.AlterationoptionPriceIn.ToPriceInTax(tax, pricesIncludeTaxes);
            orderitemAlterationitemEntity.PriceExTax = orderitemAlterationitemEntity.AlterationoptionPriceIn.ToPriceExTax(tax, pricesIncludeTaxes);

            orderitemAlterationitemEntity.PriceTotalInTax = orderitemAlterationitemEntity.AlterationoptionPriceIn.ToPriceTotalInTax(tax, orderitemAlterationitemEntity.Quantity, pricesIncludeTaxes);
            orderitemAlterationitemEntity.PriceTotalExTax = orderitemAlterationitemEntity.AlterationoptionPriceIn.ToPriceTotalExTax(tax, orderitemAlterationitemEntity.Quantity, pricesIncludeTaxes);
        }

        private EntityCollection<TaxTariffEntity> FetchTaxTariffs(IEnumerable<OrderitemEntity> orderitemEntities,
            IEnumerable<OrderitemAlterationitemEntity>? orderItemAlterationItemEntities = default)
        {
            IEnumerable<int> taxTariffIds = orderitemEntities.Where(orderitem => orderitem.TaxTariffId.HasValue)
                .Select(orderitem => orderitem.TaxTariffId.GetValueOrDefault(0));

            if (orderItemAlterationItemEntities != null)
            {
                IEnumerable<int> alterationtaxTariffIds = orderItemAlterationItemEntities.Where(orderItemAlterationItem => orderItemAlterationItem.TaxTariffId.HasValue)
                    .Select(orderItemAlterationItem => orderItemAlterationItem.TaxTariffId.GetValueOrDefault(0));

                if (alterationtaxTariffIds.Any())
                {
                    taxTariffIds = taxTariffIds.Union(alterationtaxTariffIds).Distinct();
                }
            }

            return taxTariffRepository.GetTaxTariffs(taxTariffIds.ToList().AsReadOnly());
        }
    }
}
