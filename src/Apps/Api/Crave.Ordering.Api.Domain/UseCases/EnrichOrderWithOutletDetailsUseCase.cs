﻿using Crave.Ordering.Api.Core.Enums;
using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Api.Domain.Exceptions;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.Extensions;

namespace Crave.Ordering.Api.Domain.UseCases
{
    public class EnrichOrderWithOutletDetailsUseCase
    {
        private readonly IOutletRepository outletRepository;
        
        public EnrichOrderWithOutletDetailsUseCase(IOutletRepository outletRepository)
        {
            this.outletRepository = outletRepository;
        }

        public void Execute(OrderEntity orderEntity)
        {
            if (!orderEntity.OutletId.HasValue)
            {
                return;
            }

            OutletEntity outletEntity = outletRepository.GetOutlet(orderEntity.OutletId.Value);
            if (outletEntity.IsNullOrNew())
            {
                throw new CheckoutValidationException(ValidationErrorSubType.OutletInvalid);
            }

            orderEntity.Outlet = outletEntity;
            orderEntity.OutletId = outletEntity.OutletId;
            orderEntity.TaxBreakdown = outletEntity.ShowTaxBreakdown;
        }
    }
}
