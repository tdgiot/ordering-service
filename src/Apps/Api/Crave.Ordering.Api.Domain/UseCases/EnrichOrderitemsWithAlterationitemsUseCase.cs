﻿using Crave.Ordering.Api.Core.Enums;
using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Api.Domain.Exceptions;
using Crave.Ordering.Api.Domain.Models;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Crave.Ordering.Api.Domain.UseCases
{
    public class EnrichOrderitemsWithAlterationitemsUseCase
    {
        private readonly IAlterationitemRepository alterationitemRepository;

        public EnrichOrderitemsWithAlterationitemsUseCase(IAlterationitemRepository alterationitemRepository)
        {
            this.alterationitemRepository = alterationitemRepository;
        }

        public void Execute(EntityCollection<OrderitemEntity> orderitems)
        {
            Dictionary<string, AlterationitemEntity> alterationitemMap = FetchAlterationitems(orderitems)
                .ToDictionary(alterationitem => (string)new AlterationitemKey(alterationitem.AlterationId, alterationitem.AlterationoptionId), 
                              alterationitem => alterationitem);

            foreach (OrderitemEntity orderitemEntity in orderitems)
            {
                EnrichOrderitemWithAlterationitemsIfPresent(orderitemEntity, alterationitemMap);
            }
        }

        private EntityCollection<AlterationitemEntity> FetchAlterationitems(IEnumerable<OrderitemEntity> orderitemEntities)
        {
            IReadOnlyList<Tuple<int, int>> alterationitems = orderitemEntities.SelectMany(orderitem => orderitem.OrderitemAlterationitemCollection)
                                                                              .Where(orderitemAlterationitem => orderitemAlterationitem.AlterationId.HasValue && orderitemAlterationitem.AlterationoptionId.HasValue)
                                                                              .Select(orderitemAlterationitem => new Tuple<int, int>(orderitemAlterationitem.AlterationId.GetValueOrDefault(0), orderitemAlterationitem.AlterationoptionId.GetValueOrDefault(0)))
                                                                              .ToList();

            return alterationitemRepository.GetAlterationitems(alterationitems);
        }

        private static void EnrichOrderitemWithAlterationitemsIfPresent(OrderitemEntity orderitemEntity, Dictionary<string, AlterationitemEntity> alterationitemMap)
        {
            foreach (OrderitemAlterationitemEntity orderitemAlterationitemEntity in orderitemEntity.OrderitemAlterationitemCollection)
            {
                if (!orderitemAlterationitemEntity.AlterationId.HasValue || !orderitemAlterationitemEntity.AlterationoptionId.HasValue)
                {
                    continue;
                }

                AlterationitemKey key = new AlterationitemKey(orderitemAlterationitemEntity.AlterationId.Value, orderitemAlterationitemEntity.AlterationoptionId.Value);

                if (!alterationitemMap.TryGetValue(key, out AlterationitemEntity alterationitemEntity))
                {
                    throw new CheckoutValidationException(ValidationErrorSubType.AlterationitemInvalid);
                }

                orderitemAlterationitemEntity.Alterationitem = alterationitemEntity;
                orderitemAlterationitemEntity.AlterationitemId = alterationitemEntity.AlterationitemId;
                orderitemAlterationitemEntity.AlterationoptionName = alterationitemEntity.Alterationoption.Name;
                orderitemAlterationitemEntity.AlterationoptionType = alterationitemEntity.Alterationoption.Type;
                orderitemAlterationitemEntity.AlterationoptionProductId = alterationitemEntity.Alterationoption.ProductId;
                orderitemAlterationitemEntity.AlterationoptionPriceIn = alterationitemEntity.Alterationoption.PriceIn.GetValueOrDefault(0);
                orderitemAlterationitemEntity.TaxTariffId = alterationitemEntity.Alterationoption.TaxTariffId;
                orderitemAlterationitemEntity.ExternalIdentifier = alterationitemEntity.Alterationoption.ExternalIdentifier;
            }
        }
    }
}
