﻿using System.Threading.Tasks;
using Crave.Libraries.Distance.Interfaces;
using Crave.Libraries.Distance.Models;
using Crave.Libraries.Distance.Responses;
using Crave.Ordering.Api.Domain.Models;
using Crave.Ordering.Api.Domain.UseCases.Requests;

namespace Crave.Ordering.Api.Domain.UseCases
{
    public class GetDistanceToOutletUseCase : AsyncUseCaseBase<GetDistanceToOutletRequest, GetDistanceResponse>
    {
        #region Fields

        private readonly GetOutletLocationUseCase getOutletAddressUseCase;
        private readonly GetDistanceBetweenLocationsUseCase getDistanceBetweenAddressesUseCase;

        #endregion

        #region Constructors

        public GetDistanceToOutletUseCase(GetOutletLocationUseCase getOutletAddressUseCase,
                                          GetDistanceBetweenLocationsUseCase getDistanceBetweenAddressesUseCase)
        {
            this.getOutletAddressUseCase = getOutletAddressUseCase;
            this.getDistanceBetweenAddressesUseCase = getDistanceBetweenAddressesUseCase;
        }

        #endregion

        #region Methods

        public override async Task<GetDistanceResponse> ExecuteAsync(GetDistanceToOutletRequest request)
        {
            int outletId = request.OutletId;
            DeliveryLocation deliveryLocation = request.DeliveryLocation;

            ILocation outletLocation = await this.getOutletAddressUseCase.ExecuteAsync(new GetOutletLocationRequest(outletId));
            ILocation customerLocation = new Coordinates(deliveryLocation.Longitude, deliveryLocation.Latitude);

            return await this.getDistanceBetweenAddressesUseCase.ExecuteAsync(new GetDistanceBetweenLocationsRequest(outletLocation, customerLocation));
        }

        #endregion
    }
}
