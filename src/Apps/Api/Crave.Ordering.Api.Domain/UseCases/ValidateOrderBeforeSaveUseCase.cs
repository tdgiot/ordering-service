﻿using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Ordering.Api.Domain.UseCases
{
    /// <summary>
    /// Validates an order before save.
    /// </summary>
    public class ValidateOrderBeforeSaveUseCase : UseCaseBase<OrderEntity>
    {
        private readonly ValidateOrderitemBeforeSaveUseCase validateOrderitemBeforeSaveUseCase;

        public ValidateOrderBeforeSaveUseCase(ValidateOrderitemBeforeSaveUseCase validateOrderitemBeforeSaveUseCase)
        {
            this.validateOrderitemBeforeSaveUseCase = validateOrderitemBeforeSaveUseCase;
        }

        /// <inheritdoc />
        public override void Execute(OrderEntity request)
        {
            this.ValidateIfOrderitemsAreValid(request);
        }

        internal void ValidateIfOrderitemsAreValid(OrderEntity useableEntity)
        {
            foreach (OrderitemEntity orderitemEntity in useableEntity.OrderitemCollection)
            {
                this.validateOrderitemBeforeSaveUseCase.Execute(orderitemEntity);
            }
        }
    }
}