﻿using System.Threading.Tasks;
using Crave.Ordering.Api.Domain.Interfaces.UseCases;
using Crave.Ordering.Api.Domain.UseCases.Requests;

namespace Crave.Ordering.Api.Domain.UseCases
{
    public class ProcessClientOrdersUseCase : IProcessClientOrdersUseCase
    {
        private readonly ICancelRoutesUseCase cancelRoutesUseCase;

        /// <summary>
        /// Initialize a new instance of the <see cref="ProcessClientOrdersUseCase"/> class.
        /// </summary>
        /// <param name="cancelRoutesUseCase">The cancel routes use case.</param>
        public ProcessClientOrdersUseCase(ICancelRoutesUseCase cancelRoutesUseCase)
        {
            this.cancelRoutesUseCase = cancelRoutesUseCase;
        }

        /// <summary>
        /// Executes the logic of the use case by processing orders related to client id of the specified request DTO.
        /// </summary>
        /// <param name="request">The <see cref="ProcessClientOrders"/> instance containing the parameters to process the client orders.</param>
        /// <returns>An <see cref="bool"/> instance.</returns>
        public async Task<bool> ExecuteAsync(ProcessClientOrders request)
        {
            return await this.cancelRoutesUseCase.ExecuteAsync(new CancelRoutes { ClientId = request.ClientId });
        }
    }
}
