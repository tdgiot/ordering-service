﻿using System.Collections.Generic;
using Crave.Ordering.Api.Domain.UseCases.Requests;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Ordering.Api.Domain.UseCases
{
    public class EnrichOrderWithOrderitemsUseCase
    {
        private readonly EnrichOrderitemsWithProductsUseCase enrichOrderitemsWithProductsUseCase;
        private readonly EnrichOrderitemsWithCategoriesUseCase enrichOrderitemsWithCategoriesUseCase;
        private readonly EnrichOrderitemsWithAlterationsUseCase enrichOrderitemsWithAlterationsUseCase;
        private readonly EnrichOrderitemsWithAlterationitemsUseCase enrichOrderitemsWithAlterationitemsUseCase;
        private readonly EnrichOrderitemsWithPriceLevelItemsUseCase enrichOrderitemsWithPriceLevelItemsUseCase;

        public EnrichOrderWithOrderitemsUseCase(EnrichOrderitemsWithProductsUseCase enrichOrderitemsWithProductsUseCase,
                                                EnrichOrderitemsWithCategoriesUseCase enrichOrderitemsWithCategoriesUseCase,
                                                EnrichOrderitemsWithAlterationsUseCase enrichOrderitemsWithAlterationsUseCase,
                                                EnrichOrderitemsWithAlterationitemsUseCase enrichOrderitemsWithAlterationitemsUseCase,
                                                EnrichOrderitemsWithPriceLevelItemsUseCase enrichOrderitemsWithPriceLevelItemsUseCase)
        {
            this.enrichOrderitemsWithProductsUseCase = enrichOrderitemsWithProductsUseCase;
            this.enrichOrderitemsWithCategoriesUseCase = enrichOrderitemsWithCategoriesUseCase;
            this.enrichOrderitemsWithAlterationsUseCase = enrichOrderitemsWithAlterationsUseCase;
            this.enrichOrderitemsWithAlterationitemsUseCase = enrichOrderitemsWithAlterationitemsUseCase;
            this.enrichOrderitemsWithPriceLevelItemsUseCase = enrichOrderitemsWithPriceLevelItemsUseCase;
        }

        public void Execute(OrderEntity orderEntity, IEnumerable<OrderitemRequest>? orderitems)
        {
            CreateAndInsertOrderitemEntities(orderEntity, orderitems);

            enrichOrderitemsWithProductsUseCase.Execute(orderEntity.OrderitemCollection);
            enrichOrderitemsWithCategoriesUseCase.Execute(orderEntity.OrderitemCollection);
            enrichOrderitemsWithAlterationsUseCase.Execute(orderEntity.OrderitemCollection);
            enrichOrderitemsWithAlterationitemsUseCase.Execute(orderEntity.OrderitemCollection);
            enrichOrderitemsWithPriceLevelItemsUseCase.Execute(orderEntity.OrderitemCollection);
        }

        private static void CreateAndInsertOrderitemEntities(OrderEntity orderEntity, IEnumerable<OrderitemRequest>? orderitems)
        {
            if (orderitems == null)
            {
                return;
            }

            foreach (OrderitemRequest orderitem in orderitems)
            {
                OrderitemEntity orderitemEntity = new OrderitemEntity();
                orderitemEntity.TempInternalGuid = orderitem.TempInternalGuid;
                orderitemEntity.ProductId = orderitem.ProductId > 0 ? orderitem.ProductId : (int?)null;
                orderitemEntity.CategoryId = orderitem.CategoryId;
                orderitemEntity.Type = orderitem.Type;
                orderitemEntity.Guid = orderitem.Guid;
                orderitemEntity.Quantity = orderitem.Quantity;
                orderitemEntity.PriceLevelItemId = orderitem.PriceLevelItemId;
                orderitemEntity.Notes = orderitem.Notes;

                CreateAndInsertOrderitemAlterationitemEntities(orderitemEntity, orderitem.Alterationitems);

                orderEntity.OrderitemCollection.Insert(0, orderitemEntity);
            }
        }

        private static void CreateAndInsertOrderitemAlterationitemEntities(OrderitemEntity orderitemEntity, IEnumerable<AlterationitemRequest>? alterationitems)
        {
            if (alterationitems == null)
            {
                return;
            }

            foreach (AlterationitemRequest alterationitem in alterationitems)
            {
                OrderitemAlterationitemEntity orderitemAlterationitemEntity = new OrderitemAlterationitemEntity();
                orderitemAlterationitemEntity.Value = alterationitem.Value;
                orderitemAlterationitemEntity.TimeUTC = alterationitem.TimeUTC;
                orderitemAlterationitemEntity.AlterationId = alterationitem.AlterationId > 0 ? alterationitem.AlterationId : (int?)null;
                orderitemAlterationitemEntity.AlterationoptionId = alterationitem.AlterationoptionId;
                orderitemAlterationitemEntity.PriceLevelItemId = alterationitem.PriceLevelItemId;

                orderitemEntity.OrderitemAlterationitemCollection.Add(orderitemAlterationitemEntity);
            }
        }
    }
}
