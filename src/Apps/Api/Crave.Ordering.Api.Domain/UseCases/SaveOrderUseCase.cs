﻿using System;
using System.Linq;
using Crave.Enums;
using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Api.Domain.Interfaces.UseCases;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.Extensions;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using CreateOrderResult = Crave.Enums.CreateOrderResult;
using System.Threading.Tasks;
using Crave.Ordering.Shared.Messaging.Domain.UseCases.Requests;
using Crave.Ordering.Shared.Messaging.Interfaces.UseCases;
using Crave.Ordering.Shared.Messaging.Netmessages;
using System.Text;

namespace Crave.Ordering.Api.Domain.UseCases
{
    /// <summary>
    /// Use case class which is used to create an order. 
    /// </summary>
    public class SaveOrderUseCase : ISaveOrderUseCase
    {
        public enum OrderSaveResult
        {
            // Weird numbering is for backwards compatibility with old API
            ChangeNotAllowedToExistingOrder = 217,
        }

        private readonly IDeviceRepository deviceRepository;
        private readonly IOrderRepository orderRepository;
        private readonly IAnalyticsProcessingTaskRepository analyticsProcessingTaskRepository;
        private readonly ISendNetmessageUseCase sendNetmessageUseCase;
        private readonly ValidateOrderBeforeSaveUseCase validateOrderBeforeSaveUseCase;
        private readonly ProcessOptInUseCase processOptInUseCase;

        /// <summary>
        /// Initializes a new instance of the <see cref="SaveOrderUseCase"/> class.
        /// </summary>
        /// <param name="orderRepository">The order repository.</param>
        /// <param name="deviceRepository">The device repository.</param>
        /// <param name="analyticsProcessingTaskRepository">The analytics processing task repository.</param>
        /// <param name="sendNetmessageUseCase">Use-case for sending netmessages</param>
        /// <param name="validateOrderBeforeSaveUseCase">Use-case for validating an order before save.</param>
        /// <param name="processOptInUseCase">Use-case for handling marketing opt-in</param>
        public SaveOrderUseCase(IOrderRepository orderRepository,
                                IDeviceRepository deviceRepository,
                                IAnalyticsProcessingTaskRepository analyticsProcessingTaskRepository,
                                ISendNetmessageUseCase sendNetmessageUseCase,
                                ValidateOrderBeforeSaveUseCase validateOrderBeforeSaveUseCase,
                                ProcessOptInUseCase processOptInUseCase)
        {
            this.orderRepository = orderRepository;
            this.deviceRepository = deviceRepository;
            this.analyticsProcessingTaskRepository = analyticsProcessingTaskRepository;
            this.sendNetmessageUseCase = sendNetmessageUseCase;
            this.validateOrderBeforeSaveUseCase = validateOrderBeforeSaveUseCase;
            this.processOptInUseCase = processOptInUseCase;
        }

        /// <summary>
        /// Executes the logic of the use case by creating the order for the parameters of the specified request DTO.
        /// </summary>
        /// <param name="request">The <see cref="OrderEntity"/> to save.</param>
        /// <returns>An <see cref="OrderEntity"/> instance containing the order.</returns>
        public async Task<OrderEntity> ExecuteAsync(OrderEntity request)
        {
            OrderEntity orderEntity = request;

            if (!orderEntity.SkipValidation)
            {
                ValidateOrder(orderEntity);
            }

            orderEntity.OrderitemCollection
                       .SelectMany(orderItem =>
                       {
                           if (orderItem.ParentCompanyId == 0)
                           {
                               orderItem.ParentCompanyId = orderEntity.CompanyId;
                           }

                           return orderItem.OrderitemAlterationitemCollection;
                       })
                       .ToList()
                       .ForEach(orderitemAlterationitem =>
                       {
                           if (orderitemAlterationitem.ParentCompanyId == 0)
                           {
                               orderitemAlterationitem.ParentCompanyId = orderEntity.CompanyId;
                           }
                       });

            SetValuesOfStatusFields(orderEntity);

            // Store before saving, because LLBLGen changes fields
            bool isOrderStatusChanged = (orderEntity.IsNew || orderEntity.IsChanged(OrderFields.Status));
            bool manuallyProcessed = IsManuallyProcessed(isOrderStatusChanged, orderEntity);

            // Validate the order entity
            this.validateOrderBeforeSaveUseCase.Execute(orderEntity);

            if (orderEntity.IsNew && (orderEntity.OptInStatus != OptInStatus.NoOptInAsked || orderEntity.OptInStatus != OptInStatus.HardOptInExists))
            {
                this.processOptInUseCase.Execute(orderEntity);
            }

            // Save the order to the database
            orderEntity = await orderRepository.SaveOrderAsync(orderEntity);

            if (orderEntity == null)
            {
                throw new CraveException(CreateOrderResult.EntitySaveRecursiveFalse);
            }

            if (isOrderStatusChanged)
            {
                this.CreateAnalyticsProcessingTask(orderEntity, manuallyProcessed);
                this.SendNetmessageAfterSave(orderEntity);
            }

            return orderEntity;
        }

        private static bool IsManuallyProcessed(bool isOrderStatusChanged, OrderEntity orderEntity)
        {
            if (!isOrderStatusChanged)
            { return false; }

            OrderStatus? oldOrderStatus = (OrderStatus?)orderEntity.Fields[OrderFields.Status.Name].DbValue;
            return oldOrderStatus == OrderStatus.Unprocessable && orderEntity.Status == OrderStatus.Processed;
        }

        private static void SetValuesOfStatusFields(OrderEntity useableEntity)
        {
            if (useableEntity.IsChanged(OrderFields.Status))
            {
                useableEntity.StatusText = useableEntity.Status.ToString();
            }

            if (useableEntity.IsChanged(OrderFields.ErrorCode))
            {
                useableEntity.ErrorText = useableEntity.ErrorCode.ToString();
            }

            if (useableEntity.IsChanged(OrderFields.Type))
            {
                useableEntity.TypeText = useableEntity.Type.ToString();
            }
        }

        private void SendNetmessageAfterSave(OrderEntity useableEntity)
        {
            // Because an order gets set to processed by the noc if could give a false postive
            // therefore, if there's an error we always update pokein listeners as unprocessable.
            OrderStatus status = useableEntity.ErrorCode != OrderErrorCode.None ? OrderStatus.Unprocessable : useableEntity.Status;

            // FO With routing per product
            // We determine the status of an order by the status of its orderitems on the Emenu
            StringBuilder orderitemIds = new StringBuilder();

            orderitemIds.Append(useableEntity.OrderitemCollection
                .Where(x => x.OrderitemId > 0)
                .Select(x => $"{x.OrderitemId}-").ToString());

            // Send update to Client (if there is one, not for Mobile Orders)
            if (useableEntity.ClientId.GetValueOrDefault(0) > 0)
            {
                DeviceEntity clientDeviceEntity = this.deviceRepository.GetDeviceForClient(useableEntity.ClientId.GetValueOrDefault());

                // Check if this client is still connected to a device
                if (clientDeviceEntity != null)
                {
                    NetmessageOrderStatusUpdated message = new NetmessageOrderStatusUpdated();
                    message.ReceiverIdentifier = clientDeviceEntity.Identifier;
                    message.OrderId = useableEntity.OrderId;
                    message.OrderItemIds = orderitemIds.ToString();
                    message.OrderStatus = status;

                    // Check if we can find a TerminalId, is used as sender only.
                    message.SenderIdentifier = GetTerminalDeviceIdentifier(useableEntity);

                    // Send netmessage
                    SendNetmessage sendNetmessage = new SendNetmessage();
                    sendNetmessage.Netmessage = message;
                    sendNetmessage.Recipients.Identifier = clientDeviceEntity.Identifier;
                    this.sendNetmessageUseCase.Execute(sendNetmessage);
                }
            }
        }

        private string GetTerminalDeviceIdentifier(OrderEntity orderEntity)
        {
            int? terminalID = orderEntity.OrderRoutestephandlerCollection.Where(x => x.TerminalId.HasValue).Select(x => x.TerminalId).FirstOrDefault();
            return terminalID.HasValue ? deviceRepository.GetDeviceForTerminal(terminalID.Value).Identifier : string.Empty;
        }

        private void CreateAnalyticsProcessingTask(OrderEntity orderEntity, bool manuallyProcessed)
        {
            AnalyticsProcessingTaskEntity task = new AnalyticsProcessingTaskEntity();
            task.OrderId = orderEntity.OrderId;
            task.EventAction = "Order Status Updated";
            task.EventLabel = orderEntity.StatusText;

            if (manuallyProcessed)
            {
                task.EventLabel += " (Manually Processed)";
            }

            this.analyticsProcessingTaskRepository.SaveAnalyticsProcessingTask(task);
        }

        private static void ValidateOrder(OrderEntity useableEntity)
        {
            // Check if it's an allowed change. Saved orders are only very limited allowed to be changed
            string changedField;
            if (!useableEntity.IsNew && !useableEntity.OverrideChangeProtection && IsNonAllowedFieldChanged(useableEntity, out changedField))
            {
                throw new CraveException(OrderSaveResult.ChangeNotAllowedToExistingOrder, "OrderId: {0}, Field: {1}", useableEntity.OrderId, changedField);
            }
        }

        private static bool IsNonAllowedFieldChanged(OrderEntity order, out string changedFieldName)
        {
            bool toReturn = false;
            changedFieldName = string.Empty;
            OrderFieldIndex[] allowedFields = FieldsAllowedToChangeWhenNotNew();

            for (int i = 0; i < order.Fields.Count; i++)
            {
                IEntityField2 field = order.Fields[i];
                if (field.IsChanged)
                {
                    OrderFieldIndex changedFieldIndex = field.FieldIndex.ToEnum<OrderFieldIndex>();

                    if (!allowedFields.Any(f => f == changedFieldIndex))
                    {
                        toReturn = true;
                        changedFieldName = field.Name;
                        break;
                    }
                }
            }

            return toReturn;
        }

        private static OrderFieldIndex[] FieldsAllowedToChangeWhenNotNew()
        {
            return new[]
            {
                OrderFieldIndex.OrderId,
                OrderFieldIndex.ErrorSentByEmail,
                OrderFieldIndex.ErrorSentBySMS,
                OrderFieldIndex.Guid,
                OrderFieldIndex.PlaceTimeUTC,
                OrderFieldIndex.RoutingLog,
                OrderFieldIndex.Printed,
                OrderFieldIndex.Status,
                OrderFieldIndex.StatusText,
                OrderFieldIndex.ProcessedUTC,
                OrderFieldIndex.UpdatedUTC,
                OrderFieldIndex.UpdatedBy,
                OrderFieldIndex.ErrorCode,
                OrderFieldIndex.ErrorText,
                OrderFieldIndex.DeliverypointName,
                OrderFieldIndex.ClientId,
                OrderFieldIndex.CompanyName,
                OrderFieldIndex.HotSOSServiceOrderId,
                OrderFieldIndex.DeliverypointId
            };
        }
    }
}
