﻿using System.Linq;
using System.Threading.Tasks;
using Crave.Enums;
using Crave.Ordering.Api.Domain.UseCases.Requests;
using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Api.Domain.Interfaces.UseCases;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Ordering.Api.Domain.UseCases
{
    public class ProcessTerminalOrderUseCase : IProcessTerminalOrderUseCase
    {
        private readonly ITerminalRepository terminalRepository;
        private readonly IOrderRepository orderRepository;
        private readonly IUpdateOrderRoutestephandlerStatusesUseCase updateOrderRoutestephandlerStatusesUseCase;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProcessTerminalOrderUseCase"/> class.
        /// </summary>
        /// <param name="terminalRepository">The terminal repository.</param>
        /// <param name="orderRepository">The order repository.</param>
        /// <param name="updateOrderRoutestephandlerStatusesUseCase">The use-case to cancel the order routestep handlers.</param>
        public ProcessTerminalOrderUseCase(ITerminalRepository terminalRepository,
                                           IOrderRepository orderRepository,
                                           IUpdateOrderRoutestephandlerStatusesUseCase updateOrderRoutestephandlerStatusesUseCase)
        {
            this.terminalRepository = terminalRepository;
            this.orderRepository = orderRepository;
            this.updateOrderRoutestephandlerStatusesUseCase = updateOrderRoutestephandlerStatusesUseCase;
        }

        /// <summary>
        /// Executes the logic of the use case by processing order related to terminal id of the specified request DTO.
        /// </summary>
        /// <param name="request">The <see cref="ProcessTerminalOrder"/> instance containing the parameters to process the terminal order.</param>
        /// <returns>An <see cref="bool"/> instance.</returns>
        public async Task<bool> ExecuteAsync(ProcessTerminalOrder request)
        {
            bool success;

            TerminalEntity terminalEntity = await this.terminalRepository.GetByIdAsync(request.TerminalId);
            OrderEntity orderEntity = await this.orderRepository.GetByGuidWithOrderRoutestephandlersAsync(request.OrderGuid);

            if (terminalEntity.IsNew || orderEntity.IsNew)
            {
                success = false;
            }
            else
            {
                // Allow process when not failed when there's a POS in the order route
                bool onlyProcessWhenFailed = orderEntity.OrderRoutestephandlerCollection.All(x => x.HandlerType != RoutestephandlerType.POS);

                UpdateOrderRoutestephandlerStatuses updateOrderRoutestephandlerStatuses = new UpdateOrderRoutestephandlerStatuses
                {
                    OrderEntity = orderEntity,
                    Status = OrderStatus.Processed,
                    OnlyWhenFailed = onlyProcessWhenFailed,
                    TerminalEntity = terminalEntity
                };
                success = await this.updateOrderRoutestephandlerStatusesUseCase.ExecuteAsync(updateOrderRoutestephandlerStatuses);
            }

            return success;
        }
    }
}
