﻿using System.Threading.Tasks;

namespace Crave.Ordering.Api.Domain.UseCases
{
    public interface IUseCase { }
    
    /// <summary>
    /// Base for use cases which return an object after completing
    /// </summary>
    /// <typeparam name="TRequest">Request type</typeparam>
    /// <typeparam name="TResponse">Response type</typeparam>
    public abstract class UseCaseBase<TRequest, TResponse> : IUseCase
    {
        /// <summary>
        /// Execute use case logic with <see cref="T:TRequest" /> as input parameter.
        /// Returns <see cref="T:TResponse" /> object once the execution completed.
        /// </summary>
        /// <param name="request">The <see cref="T:TRequest" /> used as input parameter</param>
        /// <returns>Result of the use case of type <typeparamref name="TResponse"/></returns>
        public abstract TResponse Execute(TRequest request);
    }

    /// <summary>
    /// Base for use cases which return a task async which can be awaited for completion.
    /// </summary>
    /// <typeparam name="TRequest">Request type</typeparam>
    /// <typeparam name="TResponse">Response type</typeparam>
    public abstract class AsyncUseCaseBase<TRequest, TResponse> : IUseCase
    {
        /// <summary>
        /// Execute use case logic with <see cref="T:TRequest" /> as input parameter.
        /// Returns <see cref="T:TResponse" /> object once the execution completed.
        /// </summary>
        /// <param name="request">The <see cref="T:TRequest" /> used as input parameter</param>
        /// <returns>Result of the use case of type <typeparamref name="TResponse"/></returns>
        public abstract Task<TResponse> ExecuteAsync(TRequest request);
    }

    /// <summary>
    /// Base for use cases which return nothing after completing
    /// </summary>
    /// <typeparam name="TRequest">Request type</typeparam>
    public abstract class UseCaseBase<TRequest> : IUseCase
    {
        /// <summary>
        /// Execute use case logic with <see cref="T:TRequest" /> as input parameter.
        /// </summary>
        /// <param name="request">The <see cref="T:TRequest" /> used as input parameter</param>
        public abstract void Execute(TRequest request);
    }

    /// <summary>
    /// Base for use cases which return nothing after completing
    /// </summary>
    /// <typeparam name="TRequest">Request type</typeparam>
    public interface IUseCase<in TRequest> : IUseCase
    {
        /// <summary>
        /// Execute use case logic with <see cref="T:TRequest" /> as input parameter.
        /// </summary>
        /// <param name="request">The <see cref="T:TRequest" /> used as input parameter</param>
        public void Execute(TRequest request);
    }

    /// <summary>
    /// Base for use cases which return an object after completing
    /// </summary>
    /// <typeparam name="TRequest">Request type</typeparam>
    /// <typeparam name="TResponse">Response type</typeparam>
    public interface IUseCase<in TRequest, out TResponse> : IUseCase
    {
        /// <summary>
        /// Execute use case logic with <see cref="T:TRequest" /> as input parameter.
        /// Returns <see cref="T:TResponse" /> object once the execution completed.
        /// </summary>
        /// <param name="request">The <see cref="T:TRequest" /> used as input parameter</param>
        /// <returns>Result of the use case of type <typeparamref name="TResponse"/></returns>
        public TResponse Execute(TRequest request);
    }

    /// <summary>
    /// Base for use cases which return a task async which can be awaited for completion.
    /// </summary>
    /// <typeparam name="TRequest">Request type</typeparam>
    public interface IAsyncUseCase<in TRequest> : IUseCase
    {
        /// <summary>
        /// Execute use case logic with <see cref="T:TRequest" /> as input parameter.
        /// Returns <see cref="T:TResponse" /> object once the execution completed.
        /// </summary>
        /// <param name="request">The <see cref="T:TRequest" /> used as input parameter</param>
        /// <returns>Result of the use case of type <typeparamref name="TResponse"/></returns>
        public Task ExecuteAsync(TRequest request);
    }

    /// <summary>
    /// Base for use cases which return a task async which can be awaited for completion.
    /// </summary>
    /// <typeparam name="TRequest">Request type</typeparam>
    /// <typeparam name="TResponse">Response type</typeparam>
    public interface IAsyncUseCase<in TRequest, TResponse> : IUseCase
    {
        /// <summary>
        /// Execute use case logic with <see cref="T:TRequest" /> as input parameter.
        /// Returns <see cref="T:TResponse" /> object once the execution completed.
        /// </summary>
        /// <param name="request">The <see cref="T:TRequest" /> used as input parameter</param>
        /// <returns>Result of the use case of type <typeparamref name="TResponse"/></returns>
        public Task<TResponse> ExecuteAsync(TRequest request);
    }
}
