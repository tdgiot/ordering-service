﻿using System;
using System.Threading.Tasks;
using Crave.Enums;
using Crave.Ordering.Api.Domain.UseCases.Requests;
using Crave.Ordering.Api.Domain.UseCases.Responses;
using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Api.Domain.Interfaces.UseCases;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Enums;
using Crave.Ordering.Shared.OrderNotifications.Domain.UseCases.Requests;
using Crave.Ordering.Shared.OrderNotifications.Interfaces.UseCases;

namespace Crave.Ordering.Api.Domain.UseCases
{
    /// <summary>
    /// Create a new order route and start it after creation
    /// </summary>
    public class CreateOrderRouteUseCase : ICreateOrderRouteUseCase
    {
        private readonly IOrderRepository orderRepository;
        private readonly ValidateOrderBeforeRoutingUseCase validateOrderBeforeRoutingUseCase;
        private readonly IRouteOrderUseCase routeOrderUseCase;
        private readonly ICreateReceiptUseCase createReceiptUseCase;
        private readonly ISendOrderNotificationUseCase sendOrderNotificationUseCase;
        /// <summary>
        /// Initializes a new instance of the <see cref="CreateOrderRouteUseCase"/> class.
        /// </summary>
        /// <param name="orderRepository">The order repository.</param>
        /// <param name="validateOrderBeforeRoutingUseCase">Use-case for validation.</param>
        /// <param name="routeOrderUseCase">Use-case for routing an order.</param>
        /// <param name="createReceiptUseCase">The use-case for creating an order receipt.</param>
        /// <param name="sendOrderNotificationUseCase">Use-case for sending an order notification</param>
        public CreateOrderRouteUseCase(IOrderRepository orderRepository,
                                       ValidateOrderBeforeRoutingUseCase validateOrderBeforeRoutingUseCase,
                                       IRouteOrderUseCase routeOrderUseCase,
                                       ICreateReceiptUseCase createReceiptUseCase,
                                       ISendOrderNotificationUseCase sendOrderNotificationUseCase)
        {
            this.orderRepository = orderRepository;
            this.validateOrderBeforeRoutingUseCase = validateOrderBeforeRoutingUseCase;
            this.routeOrderUseCase = routeOrderUseCase;
            this.createReceiptUseCase = createReceiptUseCase;
            this.sendOrderNotificationUseCase = sendOrderNotificationUseCase;
        }

        /// <inheritdoc />
        public async Task<CreateOrderRouteResponse> ExecuteAsync(CreateOrderRoute request)
        {
            CreateOrderRouteResponse response = new CreateOrderRouteResponse(CreateOrderRouteResult.Ok);

            try
            {
                // Fetch the order
                OrderEntity orderEntity = await orderRepository.GetByIdWithPrefetchAsync(request.OrderId);

                // Validate the order
                CreateOrderRouteResult validateResult = validateOrderBeforeRoutingUseCase.Execute(new ValidateOrderBeforeRouting(orderEntity));
                if (validateResult != CreateOrderRouteResult.Ok)
                {
                    // Return the validation result
                    response = new CreateOrderRouteResponse(validateResult);
                }
                else
                {
                    // Send order notification
                    await SendOrderNotificationAsync(orderEntity);

                    // Route the order
                    await routeOrderUseCase.ExecuteAsync(new RouteOrder(orderEntity));
                }
            }
            catch (CraveException cex)
            {
                response = new CreateOrderRouteResponse(CreateOrderRouteResult.Exception, cex, cex.ErrorType);
            }
            catch (Exception ex)
            {
                response = new CreateOrderRouteResponse(CreateOrderRouteResult.Exception, ex.InnerException ?? ex, string.Empty);
            }

            return response;
        }

        private async Task SendOrderNotificationAsync(OrderEntity orderEntity)
        {
            if (orderEntity.CheckoutMethodType == CheckoutType.PaymentProvider)
            {
                createReceiptUseCase.Execute(new CreateReceipt { Order = orderEntity });

                await sendOrderNotificationUseCase.ExecuteAsync(new SendOrderNotificationRequest(orderEntity.OrderId, OrderNotificationType.Receipt));
            }
            else
            {
                await sendOrderNotificationUseCase.ExecuteAsync(new SendOrderNotificationRequest(orderEntity.OrderId, OrderNotificationType.OrderConfirmation));
            }
        }
    }
}