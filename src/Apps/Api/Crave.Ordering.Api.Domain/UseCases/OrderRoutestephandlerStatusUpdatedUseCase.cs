﻿using System.Threading.Tasks;
using Crave.Enums;
using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Api.Domain.Interfaces.UseCases;
using Crave.Ordering.Api.Domain.UseCases.Requests;
using Crave.Ordering.Api.Domain.UseCases.Responses;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.Extensions;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Microsoft.Extensions.Logging;

namespace Crave.Ordering.Api.Domain.UseCases
{
    public class OrderRoutestephandlerStatusUpdatedUseCase : IOrderRoutestephandlerStatusUpdatedUseCase
    {
        private readonly ILogger<OrderRoutestephandlerStatusUpdatedUseCase> logger;
        private readonly IOrderRoutestephandlerRepository orderRoutestephandlerRepository;
        private readonly UpdateOrderRoutestephandlerUseCase updateOrderRoutestephandlerUseCase;
        private readonly ExecuteRoutestepUseCase executeRoutestepUseCase;

        public OrderRoutestephandlerStatusUpdatedUseCase(ILogger<OrderRoutestephandlerStatusUpdatedUseCase> logger,
                                                         IOrderRoutestephandlerRepository orderRoutestephandlerRepository,
                                                         UpdateOrderRoutestephandlerUseCase updateOrderRoutestephandlerUseCase,
                                                         ExecuteRoutestepUseCase executeRoutestepUseCase)
        {
            this.logger = logger;
            this.orderRoutestephandlerRepository = orderRoutestephandlerRepository;
            this.updateOrderRoutestephandlerUseCase = updateOrderRoutestephandlerUseCase;
            this.executeRoutestepUseCase = executeRoutestepUseCase;
        }

        public async Task<ResponseBase> ExecuteAsync(int request)
        {
            ResponseBase response = await ExecuteInternalAsync(request);

            if (!response.Success)
            {
                logger.LogWarning(response.Message);
            }

            return response;
        }

        private async Task<ResponseBase> ExecuteInternalAsync(int orderRoutestephandlerId)
        {
            OrderRoutestephandlerEntity orderRoutestephandlerEntity = await orderRoutestephandlerRepository.GetByIdAsync(orderRoutestephandlerId);
            if (orderRoutestephandlerEntity.IsNullOrNew())
            {
                return ResponseBase.AsNotFound($"No order routestephandler found for id {orderRoutestephandlerId}");
            }

            orderRoutestephandlerEntity.Fields[OrderRoutestephandlerFields.Status.Name].IsChanged = true;

            await updateOrderRoutestephandlerUseCase.ExecuteAsync(orderRoutestephandlerEntity);

            if (orderRoutestephandlerEntity.Status == OrderRoutestephandlerStatus.Completed && !orderRoutestephandlerEntity.CompleteRouteOnComplete ||
                orderRoutestephandlerEntity.Status == OrderRoutestephandlerStatus.Failed && orderRoutestephandlerEntity.ContinueOnFailure)
            {
                await executeRoutestepUseCase.ExecuteAsync(new ExecuteRoutestepRequest(orderRoutestephandlerEntity.OrderId, orderRoutestephandlerEntity.Number + 1));
            }

            return ResponseBase.AsSuccess();
        }
    }
}
