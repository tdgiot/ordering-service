﻿using System;
using System.Text;
using Crave.Enums;
using Crave.Ordering.Api.Core.Enums;
using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Api.Domain.Exceptions;
using Crave.Ordering.Api.Domain.UseCases.Requests;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.Extensions;

namespace Crave.Ordering.Api.Domain.UseCases
{
    public class EnrichOrderWithChargeToDeliverypointUseCase
    {
        private readonly IDeliverypointRepository deliverypointRepository;

        public EnrichOrderWithChargeToDeliverypointUseCase(IDeliverypointRepository deliverypointRepository)
        {
            this.deliverypointRepository = deliverypointRepository;
        }

        public void Execute(OrderEntity orderEntity, CreateOrderRequest request)
        {
            DeliverypointRequest? deliverypoint = request.CheckoutMethodDeliverypoint;
            int? checkoutMethodId = request.CheckoutMethodId;

            if (deliverypoint == null || !checkoutMethodId.HasValue)
            {
                return;
            }

            DeliverypointEntity deliverypointEntity = deliverypointRepository.GetDeliverypointForCheckoutMethod(deliverypoint.Number, deliverypoint.DeliverypointgroupId, checkoutMethodId.Value);
            if (deliverypointEntity.IsNullOrNew())
            {
                throw new CheckoutValidationException(ValidationErrorSubType.CheckoutMethodDeliveryPointInvalid);
            }

            orderEntity.ChargeToDeliverypoint = deliverypointEntity;
            orderEntity.ChargeToDeliverypointId = deliverypointEntity.DeliverypointId;
            orderEntity.ChargeToDeliverypointName = deliverypointEntity.Name;
            orderEntity.ChargeToDeliverypointNumber = deliverypointEntity.Number;
            orderEntity.ChargeToDeliverypointgroupName = deliverypointEntity.Deliverypointgroup.Name;

            if (orderEntity.ServiceMethodType == ServiceMethodType.TableService)
            {
                StringBuilder sb = new StringBuilder();
                if (!orderEntity.Notes.IsNullOrWhiteSpace())
                {
                    sb.AppendLine(orderEntity.Notes);
                    sb.AppendLine();
                }
                sb.AppendLine("Charge To Room:");
                sb.AppendLine($"Deliverypointgroup: {deliverypointEntity.Deliverypointgroup.Name}");
                sb.Append($"Room Number: {deliverypointEntity.Number}");

                orderEntity.Notes = sb.ToString();
            }
        }
    }
}
