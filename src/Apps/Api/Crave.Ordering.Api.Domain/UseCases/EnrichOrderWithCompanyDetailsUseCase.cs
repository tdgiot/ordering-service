﻿using Crave.Ordering.Api.Core.Enums;
using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Api.Domain.Exceptions;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.Extensions;

namespace Crave.Ordering.Api.Domain.UseCases
{
    public class EnrichOrderWithCompanyDetailsUseCase
    {
        private readonly ICompanyRepository companyRepository;

        public EnrichOrderWithCompanyDetailsUseCase(ICompanyRepository companyRepository)
        {
            this.companyRepository = companyRepository;
        }

        public void Execute(OrderEntity orderEntity, int companyId)
        {
            CompanyEntity companyEntity = companyRepository.GetCompanyEntity(companyId);
            if (companyEntity.IsNullOrNew())
            {
                throw new CheckoutValidationException(ValidationErrorSubType.CompanyInvalid);
            }

            orderEntity.Company = companyEntity;
            orderEntity.CompanyId = companyEntity.CompanyId;
            orderEntity.CompanyName = companyEntity.Name;
            orderEntity.CultureCode = companyEntity.CultureCode;
            orderEntity.CurrencyCode = companyEntity.CurrencyCode;
            orderEntity.CountryCode = companyEntity.CountryCode;
            orderEntity.TimeZoneOlsonId = companyEntity.TimeZoneOlsonId;
            orderEntity.PricesIncludeTaxes = companyEntity.PricesIncludeTaxes;
        }
    }
}
