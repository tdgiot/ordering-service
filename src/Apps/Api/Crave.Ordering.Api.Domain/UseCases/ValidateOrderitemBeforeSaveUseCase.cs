﻿using System;
using System.Linq;
using Crave.Enums;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Enums;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Crave.Ordering.Api.Domain.UseCases
{
    /// <summary>
    /// Validates an order item before save.
    /// </summary>
    public class ValidateOrderitemBeforeSaveUseCase : UseCaseBase<OrderitemEntity>
    {
        private readonly ValidateOrderitemAlterationitemBeforeSaveUseCase validateOrderitemAlterationitemBeforeSaveUseCase;

        public ValidateOrderitemBeforeSaveUseCase(ValidateOrderitemAlterationitemBeforeSaveUseCase validateOrderitemAlterationitemBeforeSaveUseCase)
        {
            this.validateOrderitemAlterationitemBeforeSaveUseCase = validateOrderitemAlterationitemBeforeSaveUseCase;
        }

        /// <inheritdoc />
        public override void Execute(OrderitemEntity request)
        {
            OrderitemEntity orderitemEntity = request;

            ValidateIfQuantityIsGreaterThanZero(orderitemEntity);
            ValidateIfProductPriceInIsNotLessThanZero(orderitemEntity);
            ValidateIfVatPercentageIsNotLessThanZero(orderitemEntity);
            ValidateIfProductIsValid(orderitemEntity);
            ValidateIfFieldChangeIsAllowed(orderitemEntity);
            ValidateIfOrderitemAlterationitemsAreValid(orderitemEntity);
        }

        internal static void ValidateIfQuantityIsGreaterThanZero(OrderitemEntity useableEntity)
        {
            // Check whether the quantity is greater than zero
            if (useableEntity.Quantity <= 0)
            {
                throw new CraveException(ValidateOrderitemResult.QuantityLessThanOrZero);
            }
        }

        internal static void ValidateIfProductPriceInIsNotLessThanZero(OrderitemEntity useableEntity)
        {
            // Check whether the product price is not negative
            if (useableEntity.ProductPriceIn < 0)
            {
                throw new CraveException(ValidateOrderitemResult.PriceInLessThanZero);
            }
        }

        internal static void ValidateIfVatPercentageIsNotLessThanZero(OrderitemEntity useableEntity)
        {
            // Check whether the vat percentage is not negative
            if (useableEntity.VatPercentage < 0)
            {
                throw new CraveException(ValidateOrderitemResult.VatPercentageLessThanZero);
            }
        }

        internal static void ValidateIfProductIsValid(OrderitemEntity useableEntity)
        {
            // Check whether the product id was set
            if (!useableEntity.ProductId.HasValue)
            {
                throw new CraveException(ValidateOrderitemResult.ProductIdNotSet);
            }
        }

        internal static void ValidateIfFieldChangeIsAllowed(OrderitemEntity useableEntity)
        {
            // Don't allow OrderItems to be changed once saved
            string changedField;
            if (!useableEntity.IsNew && !useableEntity.OverrideChangeProtection && IsNonAllowedFieldChanged(useableEntity, out changedField))
            {
                throw new CraveException(ValidateOrderitemResult.ChangeNotAllowedToExistingOrderitem, "OrderitemId: {0}", useableEntity.OrderitemId);
            }
        }

        internal void ValidateIfOrderitemAlterationitemsAreValid(OrderitemEntity useableEntity)
        {
            foreach (OrderitemAlterationitemEntity orderitemAlterationitemEntity in useableEntity.OrderitemAlterationitemCollection)
            {
                this.validateOrderitemAlterationitemBeforeSaveUseCase.Execute(orderitemAlterationitemEntity);
            }
        }

        private static bool IsNonAllowedFieldChanged(OrderitemEntity orderitem, out string changedFieldName)
        {
            bool toReturn = false;
            changedFieldName = string.Empty;
            OrderitemFieldIndex[] allowedFields = FieldsAllowedToChangeWhenNotNew();

            for (int i = 0; i < orderitem.Fields.Count; i++)
            {
                IEntityField2 field = orderitem.Fields[i];
                if (field.IsChanged)
                {
                    OrderitemFieldIndex changedFieldIndex = field.FieldIndex.ToEnum<OrderitemFieldIndex>();

                    if (allowedFields.All(f => f != changedFieldIndex))
                    {
                        toReturn = true;
                        break;
                    }

                    changedFieldName = field.Name;
                }
            }

            return toReturn;
        }

        private static OrderitemFieldIndex[] FieldsAllowedToChangeWhenNotNew()
        {
            return new[]
            {
                OrderitemFieldIndex.Guid,
                OrderitemFieldIndex.UpdatedUTC,
                OrderitemFieldIndex.UpdatedBy,
                OrderitemFieldIndex.CategoryId,
                OrderitemFieldIndex.ProductId
            };
        }
    }
}