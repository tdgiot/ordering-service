﻿using System.Threading.Tasks;
using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Api.Domain.Converters;
using Crave.Ordering.Api.Domain.Interfaces.UseCases;
using Crave.Ordering.Api.Domain.UseCases.Responses;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Ordering.Api.Domain.UseCases
{
    /// <summary>
    /// Fetches an order entity from the repository.
    /// </summary>
    public class GetOrderUseCase : IGetOrderUseCase
    {
        private readonly IOrderRepository orderRepository;
        private readonly OrderConverter orderConverter;

        /// <summary>
        /// Initializes a new instance of the <see cref="GetOrderUseCase"/> class.
        /// </summary>
        /// <param name="orderRepository">The order repository.</param>
        /// <param name="orderConverter">Converts an order entity to an order dto.</param>
        public GetOrderUseCase(IOrderRepository orderRepository,
                               OrderConverter orderConverter)
        {
            this.orderRepository = orderRepository;
            this.orderConverter = orderConverter;
        }

        /// <inheritdoc />
        public async Task<Order?> ExecuteAsync(string request)
        {
            string guid = request;

            OrderEntity orderEntity = await orderRepository.GetByGuidWithPrefetchAsync(guid);

            return orderConverter.ConvertEntityIfNotNullOrNewToTResponseDto(orderEntity);
        }
    }
}