﻿using System.Collections.Generic;
using System.Linq;
using Crave.Ordering.Api.Domain.UseCases.Requests;
using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Ordering.Api.Domain.UseCases
{
    public class UpdateForwardForTerminalUseCase : UseCaseBase<UpdateForwardForTerminal, bool>
    {
        public enum TerminalValidatorResult
        {
            TerminalForwardWouldCreateInfiniteLoop = 201,
            ForwardToTerminalDoesNotExists = 202,
            ForwardToTerminalBelongsToOtherCompany = 203,
            ForwardToTerminalHasADifferentHandlingType = 203
        }

        private readonly ITerminalRepository terminalRepository;
        private readonly GetTerminalsForwardingFromThisTerminalUseCase getTerminalsForwardingFromThisTerminalUseCase;

        /// <summary>
        /// Initializes a new instance of the <see cref="UpdateForwardForTerminalUseCase"/> class.
        /// </summary>
        /// <param name="terminalRepository">The terminal repository.</param>
        /// <param name="getTerminalsForwardingFromThisTerminalUseCase">Use case for fetching terminals which are forwarding to current terminal</param>
        public UpdateForwardForTerminalUseCase(ITerminalRepository terminalRepository,
                                               GetTerminalsForwardingFromThisTerminalUseCase getTerminalsForwardingFromThisTerminalUseCase)
        {
            this.terminalRepository = terminalRepository;
            this.getTerminalsForwardingFromThisTerminalUseCase = getTerminalsForwardingFromThisTerminalUseCase;
        }

        /// <summary>
        /// Executes the logic of the use case by updating the terminal forward for the parameters of the specified request DTO.
        /// </summary>
        /// <param name="request">The <see cref="UpdateForwardForTerminal"/> instance containing the parameters to update the terminal forward.</param>
        /// <returns>An <see cref="bool"/> instance indicates whether the update was successful.</returns>
        public override bool Execute(UpdateForwardForTerminal request)
        {
            TerminalEntity terminal = this.terminalRepository.GetTerminal(request.TerminalId);

            if (request.ForwardToTerminalId > 0)
            {
                // Check if Terminal Exists and belongs to this company
                TerminalEntity forwardToTerminal = this.terminalRepository.GetTerminal(request.ForwardToTerminalId);
                if (forwardToTerminal == null)
                {
                    // Forward to terminal does not exist
                    throw new CraveException(TerminalValidatorResult.ForwardToTerminalDoesNotExists, "Forward terminal id: {0}", request.ForwardToTerminalId);
                }
                if (forwardToTerminal.CompanyId != terminal.CompanyId)
                {
                    // Forward to terminal belongs to other company
                    throw new CraveException(TerminalValidatorResult.ForwardToTerminalBelongsToOtherCompany);
                }
                if (forwardToTerminal.HandlingMethod != terminal.HandlingMethod)
                {
                    throw new CraveException(TerminalValidatorResult.ForwardToTerminalHasADifferentHandlingType);
                }
            }

            // Retrieve the Ids of all terminals to which we are forwarding from this terminal on. If it includes itself we can't forward because that would create a loop.
            List<TerminalEntity> terminalIdsForwardingFromThisTerminal = this.getTerminalsForwardingFromThisTerminalUseCase.Execute(new GetTerminalsForwardingFromThisTerminal { TerminalEntity = terminal });
            if (terminalIdsForwardingFromThisTerminal.Any(t => t.TerminalId == terminal.TerminalId))
            {
                throw new CraveException(TerminalValidatorResult.TerminalForwardWouldCreateInfiniteLoop);
            }

            return this.terminalRepository.UpdateTerminalForward(request.TerminalId, request.ForwardToTerminalId);
        }
    }
}
