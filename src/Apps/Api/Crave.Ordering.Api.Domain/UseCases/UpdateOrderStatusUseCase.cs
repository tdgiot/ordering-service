﻿using System;
using System.Threading.Tasks;
using Crave.Enums;
using Crave.Ordering.Api.Domain.UseCases.Requests;
using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Api.Domain.Interfaces.UseCases;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.Extensions;

namespace Crave.Ordering.Api.Domain.UseCases
{
    public class UpdateOrderStatusUseCase : IUpdateOrderStatusUseCase
    {
        public enum UpdateOrderStatusResult
        {
            OrderNotFound = 201
        }

        private readonly IOrderRepository orderRepository;
        private readonly ISaveOrderUseCase saveOrderUseCase;

        public UpdateOrderStatusUseCase(IOrderRepository orderRepository,
                                        ISaveOrderUseCase saveOrderUseCase)
        {
            this.orderRepository = orderRepository;
            this.saveOrderUseCase = saveOrderUseCase;
        }

        public async Task<bool> ExecuteAsync(UpdateOrderStatus request)
        {
            OrderEntity orderEntity = await orderRepository.GetByIdAsync(request.OrderId);
            if (orderEntity.IsNullOrNew())
            {
                throw new CraveException(UpdateOrderStatusResult.OrderNotFound, "OrderId: '{0}'", request.OrderId);
            }

            if (request.Status == OrderStatus.Processed)
            {
                orderEntity.ProcessedUTC = DateTime.UtcNow;
            }

            if (request.ErrorCode.HasValue)
            {
                orderEntity.ErrorCode = request.ErrorCode.Value;
            }

            if (request.RoutingLog != null)
            {
                orderEntity.RoutingLog += request.RoutingLog;
            }

            orderEntity.Status = request.Status;

            await saveOrderUseCase.ExecuteAsync(orderEntity);

            return true;
        }
    }
}
