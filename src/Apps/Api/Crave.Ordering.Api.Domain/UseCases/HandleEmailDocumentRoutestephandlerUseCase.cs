﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Crave.Enums;
using Crave.Ordering.Api.Domain.UseCases.Requests;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.OrderNotifications.Domain.UseCases.Requests;
using Crave.Ordering.Shared.OrderNotifications.Domain.UseCases.Responses;
using Crave.Ordering.Shared.OrderNotifications.Interfaces.UseCases;

namespace Crave.Ordering.Api.Domain.UseCases
{
    public class HandleEmailDocumentRoutestephandlerUseCase : IAsyncUseCase<HandleNonTerminalRoutingstephandler>
    {
        private const string FROM_ADDRESS = "no-reply@trueguest.tech";
        private const string TITLE = "New print request!";

        private readonly ISendMailUseCase sendMailUseCase;

        public HandleEmailDocumentRoutestephandlerUseCase(ISendMailUseCase sendMailUseCase)
        {
            this.sendMailUseCase = sendMailUseCase;
        }

        public async Task ExecuteAsync(HandleNonTerminalRoutingstephandler request)
        {
            OrderRoutestephandlerEntity orderRoutestephandlerEntity = request.OrderRoutestephandlerEntity;
            OrderEntity orderEntity = request.OrderEntity;

            // FieldValue 1 = To Address 
            List<string> toAddresses = GetToAddresses(orderRoutestephandlerEntity);

            // Email Template
            string emailHtml = Properties.Resources.NewPrintRequest;

            // Prepare variables to merge with
            Dictionary<string, string> templateVariables = GenerateEmailBodyTemplate(orderEntity);

            string roomCaption = !orderEntity.Deliverypoint.Deliverypointgroup.DeliverypointCaption.IsNullOrWhiteSpace() ? orderEntity.Deliverypoint.Deliverypointgroup.DeliverypointCaption : "Room";
            string filename = $"document-{roomCaption.ToLowerInvariant()}-{orderEntity.Deliverypoint.Number}.pdf";

            SendMailRequest mailRequest = new SendMailRequest
            {
                SenderAddress = FROM_ADDRESS,
                Title = TITLE,
                Recipients = toAddresses,
                Message = emailHtml,
                IsMessageHtml = true,
                TemplateVariables = templateVariables
            };
            mailRequest.Attachments.Add(filename, orderRoutestephandlerEntity.File);

            // Publish request on MQ
            MessageQueueReponse response = await sendMailUseCase.ExecuteAsync(mailRequest);
            if (!response.Success)
            {
                orderRoutestephandlerEntity.Status = OrderRoutestephandlerStatus.Failed;
                orderRoutestephandlerEntity.ErrorText = $"Failed to send email: {response.ErrorMessage}";
                orderRoutestephandlerEntity.Order.RoutingLog += $"\r\nFailure EmailDocumentRouteStepHandler for step: {orderRoutestephandlerEntity.Number} \r\nMessage: {response.ErrorMessage}";
            }
        }

        private static List<string> GetToAddresses(OrderRoutestephandlerEntity orderRoutestephandlerEntity)
        {
            List<string> toAddresses = new List<string>();

            // FieldValue1 = To E-mail (being a support pool, manually entered email address and/or to the customer
            string toLine = string.Empty;
            if (!orderRoutestephandlerEntity.FieldValue1.IsNullOrWhiteSpace())
            {
                toLine = StringUtil.CombineWithSeperator(";", toLine, orderRoutestephandlerEntity.FieldValue1);
            }

            // Make one splittable string
            toLine = toLine.Replace(',', ';').Replace(' ', ';');

            string[] addresses = toLine.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            toAddresses.AddRange(addresses);

            return toAddresses;
        }

        private static Dictionary<string, string> GenerateEmailBodyTemplate(OrderEntity orderEntity)
        {
            Dictionary<string, string> templateVars = new Dictionary<string, string>();
            templateVars.Add("DELIVERYPOINTCAPTION", orderEntity.Deliverypoint.Deliverypointgroup.DeliverypointCaption.ToLowerInvariant());
            templateVars.Add("DELIVERYPOINT", orderEntity.DeliverypointNumber);
            templateVars.Add("DELIVERYPOINTNUMBER", orderEntity.DeliverypointNumber);
            templateVars.Add("DELIVERYPOINTNAME", orderEntity.DeliverypointName);
            templateVars.Add("CREATED", orderEntity.CreatedUTC.ToString());

            return templateVars;
        }
    }
}