﻿using Crave.Ordering.Api.Core.Enums;
using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Api.Domain.Exceptions;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using System.Collections.Generic;
using System.Linq;

namespace Crave.Ordering.Api.Domain.UseCases
{
    public class EnrichOrderitemsWithCategoriesUseCase
    {
        private readonly ICategoryRepository categoryRepository;

        public EnrichOrderitemsWithCategoriesUseCase(ICategoryRepository categoryRepository)
        {
            this.categoryRepository = categoryRepository;
        }

        public void Execute(EntityCollection<OrderitemEntity> orderitems)
        {
            Dictionary<int, CategoryEntity> categoryMap = FetchCategories(orderitems)
                .ToDictionary(category => category.CategoryId, category => category);

            foreach (OrderitemEntity orderitemEntity in orderitems)
            {
                EnrichOrderitemWithCategoryIfPresent(orderitemEntity, categoryMap);
            }
        }

        private EntityCollection<CategoryEntity> FetchCategories(IEnumerable<OrderitemEntity> orderitemEntities)
        {
            IReadOnlyList<int> categoryIds = orderitemEntities.Where(orderitem => orderitem.CategoryId.HasValue)
                                                              .Select(orderitem => orderitem.CategoryId.GetValueOrDefault(0))
                                                              .ToList();

            return this.categoryRepository.GetCategories(categoryIds);
        }

        private static void EnrichOrderitemWithCategoryIfPresent(OrderitemEntity orderitemEntity, Dictionary<int, CategoryEntity> categoryMap)
        {
            if (!orderitemEntity.CategoryId.HasValue)
            {
                return;
            }

            if (!categoryMap.TryGetValue(orderitemEntity.CategoryId.Value, out CategoryEntity categoryEntity))
            {
                throw new CheckoutValidationException(ValidationErrorSubType.CategoryInvalid);
            }

            orderitemEntity.Category = categoryEntity;
            orderitemEntity.CategoryId = categoryEntity.CategoryId;
            orderitemEntity.CategoryName = categoryEntity.Name;
            orderitemEntity.CategoryPath = GetCategoryPath(categoryEntity);
            orderitemEntity.Color = orderitemEntity.Color <= 0 && categoryEntity.Color != 0 ? categoryEntity.Color : orderitemEntity.Color;
        }

        private static string GetCategoryPath(CategoryEntity categoryEntity)
        {
            string menuName = categoryEntity.Menu != null ? categoryEntity.Menu.Name : string.Empty;
            string parentCategoryNames = GetCategoryPath(categoryEntity, categoryEntity.Name);

            return $@"{menuName}\{parentCategoryNames}";
        }

        private static string GetCategoryPath(CategoryEntity categoryEntity, string path)
        {
            if (categoryEntity.ParentCategory == null)
            {
                return path;
            }

            path = $@"{categoryEntity.ParentCategory.Name}\{path}";

            return GetCategoryPath(categoryEntity.ParentCategory, path);
        }
    }
}
