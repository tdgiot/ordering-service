﻿using System.Linq;
using System.Threading.Tasks;
using Crave.Libraries.Distance.Enums;
using Crave.Libraries.Distance.Responses;
using Crave.Ordering.Api.Domain.Enums;
using Crave.Ordering.Api.Domain.Models;
using Crave.Ordering.Api.Domain.UseCases.Requests;
using Crave.Ordering.Api.Domain.UseCases.Responses;
using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Api.Domain.Interfaces.UseCases;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.Extensions;
using Crave.Ordering.Shared.Extensions;

namespace Crave.Ordering.Api.Domain.UseCases
{
    /// <summary>
    /// Determines the delivery charge based on the distance between the outlet and the delivery address and the price of the order.
    /// </summary>
    public class GetDeliveryRateUseCase : IGetDeliveryRateUseCase
    {
        private readonly GetDistanceToOutletUseCase getDistanceToOutletUseCase;
        private readonly IServiceMethodRepository serviceMethodRepository;

        public GetDeliveryRateUseCase(GetDistanceToOutletUseCase getDistanceToOutletUseCase, 
                                      IServiceMethodRepository serviceMethodRepository)
        {
            this.getDistanceToOutletUseCase = getDistanceToOutletUseCase;
            this.serviceMethodRepository = serviceMethodRepository;
        }

        /// <inheritdoc />
        public async Task<GetDeliveryRateResponse> ExecuteAsync(GetDeliveryRateRequest request)
        {
            int serviceMethodId = request.ServiceMethodId;
            DeliveryLocation deliveryLocation = request.DeliveryLocation;
            
            ServiceMethodEntity serviceMethodEntity = await this.serviceMethodRepository.GetServiceMethodWithDeliveryDistancesAsync(serviceMethodId);
            if (serviceMethodEntity == null)
            {
                return new GetDeliveryRateResponse(DeliveryRateResult.InvalidServiceMethod);
            }
            if (!serviceMethodEntity.DeliveryDistanceCollection.Any())
            {
                return new GetDeliveryRateResponse(DeliveryRateResult.NoDeliveryDistanceSpecifiedForServiceMethod);
            }

            GetDistanceResponse getDistanceResponse = await this.getDistanceToOutletUseCase.ExecuteAsync(new GetDistanceToOutletRequest(serviceMethodEntity.OutletId, deliveryLocation));

            if (getDistanceResponse.Result != DistanceResult.OK)
            {
                return new GetDeliveryRateResponse(DeliveryRateResult.UnableToRetrieveDistanceBetweenOutletAndDeliveryLocation, getDistanceResponse.ErrorMessage);
            }

            DeliveryDistanceEntity deliveryDistanceEntity = serviceMethodEntity.DeliveryDistanceCollection.FindInRange(getDistanceResponse.Distance.Metres);
            
            if (deliveryDistanceEntity == null)
            {
                return new GetDeliveryRateResponse(DeliveryRateResult.OutOfRange);
            }

            bool pricesIncludeTaxes = serviceMethodEntity.Outlet.Company.PricesIncludeTaxes;
            int? taxTariffId = serviceMethodEntity.Outlet.DeliveryChargeProduct.TaxTariffId;
            double? percentage = serviceMethodEntity.Outlet.DeliveryChargeProduct.TaxTariff?.Percentage;

            decimal tax = deliveryDistanceEntity.Price.CalculateTax(percentage.GetValueOrDefault(), pricesIncludeTaxes);
            decimal totalPrice = request.TotalPrice;

            if (serviceMethodEntity.IsFreeDeliveryActive() && 
                serviceMethodEntity.IsMinimumPriceReachedForFreeDelivery(totalPrice))
            {
                return new GetDeliveryRateResponse(DeliveryRateResult.OK, 0, getDistanceResponse.Distance, taxTariffId, 0, 0);
            }

            return new GetDeliveryRateResponse(DeliveryRateResult.OK,
                    deliveryDistanceEntity.Price, 
                    getDistanceResponse.Distance,
                    taxTariffId,
                    deliveryDistanceEntity.Price.ToPriceInTax(tax, pricesIncludeTaxes),
                    deliveryDistanceEntity.Price.ToPriceExTax(tax, pricesIncludeTaxes));
        }
    }
}
