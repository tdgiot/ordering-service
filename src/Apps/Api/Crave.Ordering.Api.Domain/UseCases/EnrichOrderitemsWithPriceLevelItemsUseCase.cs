﻿using Crave.Ordering.Api.Core.Enums;
using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Api.Domain.Exceptions;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using System.Collections.Generic;
using System.Linq;

namespace Crave.Ordering.Api.Domain.UseCases
{
    public class EnrichOrderitemsWithPriceLevelItemsUseCase
    {
        private readonly IPriceScheduleRepository priceScheduleRepository;

        public EnrichOrderitemsWithPriceLevelItemsUseCase(IPriceScheduleRepository priceScheduleRepository)
        {
            this.priceScheduleRepository = priceScheduleRepository;
        }

        public void Execute(EntityCollection<OrderitemEntity> orderitems)
        {
            Dictionary<int, PriceLevelItemEntity> priceLevelItemMap = FetchPriceLevelItems(orderitems)
                    .ToDictionary(item => item.PriceLevelItemId, item => item);
            
            foreach (OrderitemEntity orderitemEntity in orderitems)
            {
                EnrichOrderitemWithPriceLevelItemIfPresent(orderitemEntity, priceLevelItemMap);

                foreach (OrderitemAlterationitemEntity orderitemAlterationitemEntity in orderitemEntity.OrderitemAlterationitemCollection)
                {
                    EnrichOrderitemAlterationitemWithPriceLevelItemIfPresent(orderitemAlterationitemEntity, priceLevelItemMap);
                }
            }
        }

        private EntityCollection<PriceLevelItemEntity> FetchPriceLevelItems(ICollection<OrderitemEntity> orderitemEntities)
        {
            IEnumerable<int> productPriceLevelItemIds = orderitemEntities.Where(orderitem => orderitem.PriceLevelItemId.HasValue)
                                                                         .Select(orderitem => orderitem.PriceLevelItemId.GetValueOrDefault(0));

            IEnumerable<int> alterationoptionPriceLevelItemIds = orderitemEntities.SelectMany(orderitem => orderitem.OrderitemAlterationitemCollection)
                                                                                  .Where(orderitemAlterationitem => orderitemAlterationitem.PriceLevelItemId.HasValue)
                                                                                  .Select(orderitemAlterationitem => orderitemAlterationitem.PriceLevelItemId.GetValueOrDefault(0));

            IReadOnlyList<int> priceLevelItemIds = productPriceLevelItemIds
                                                   .Union(alterationoptionPriceLevelItemIds)
                                                   .ToList();

            return priceScheduleRepository.GetPriceLevelItems(priceLevelItemIds);
        }

        private static void EnrichOrderitemWithPriceLevelItemIfPresent(OrderitemEntity orderitemEntity, IReadOnlyDictionary<int, PriceLevelItemEntity> priceLevelItemMap)
        {
            if (!orderitemEntity.PriceLevelItemId.HasValue)
            {
                return;
            }

            if (!priceLevelItemMap.TryGetValue(orderitemEntity.PriceLevelItemId.Value, out PriceLevelItemEntity priceLevelItemEntity))
            {
                throw new CheckoutValidationException(ValidationErrorSubType.PriceLevelItemInvalid);
            }

            orderitemEntity.ProductPriceIn = priceLevelItemEntity.Price;
            orderitemEntity.PriceLevelLog = $"Active price level item: {priceLevelItemEntity.PriceLevelItemId}";
        }

        private static void EnrichOrderitemAlterationitemWithPriceLevelItemIfPresent(OrderitemAlterationitemEntity orderitemAlterationitemEntity, IReadOnlyDictionary<int, PriceLevelItemEntity> priceLevelItemMap)
        {
            if (!orderitemAlterationitemEntity.PriceLevelItemId.HasValue)
            {
                return;
            }

            if (!priceLevelItemMap.TryGetValue(orderitemAlterationitemEntity.PriceLevelItemId.Value, out PriceLevelItemEntity priceLevelItemEntity))
            {
                throw new CheckoutValidationException(ValidationErrorSubType.PriceLevelItemInvalid);
            }

            orderitemAlterationitemEntity.AlterationoptionPriceIn = priceLevelItemEntity.Price;
            orderitemAlterationitemEntity.PriceLevelLog = $"Active price level item: {priceLevelItemEntity.PriceLevelItemId}";
        }
    }
}
