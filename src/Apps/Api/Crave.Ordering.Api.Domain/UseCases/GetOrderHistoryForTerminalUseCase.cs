﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Api.Domain.Converters;
using Crave.Ordering.Api.Domain.Interfaces.UseCases;
using Crave.Ordering.Api.Domain.UseCases.Requests;
using Crave.Ordering.Api.Domain.UseCases.Responses;

namespace Crave.Ordering.Api.Domain.UseCases
{
    public class GetOrderHistoryForTerminalUseCase : IGetOrderHistoryForTerminalUseCase
    {
        private readonly IOrderRepository orderRepository;
        private readonly OrderConverter orderConverter;

        /// <summary>
        /// Initialises a new instance of the <see cref="GetOrderHistoryForTerminalUseCase"/> class.
        /// </summary>
        /// <param name="orderRepository">The order repository.</param>
        /// <param name="orderConverter">Converter for converting <c>OrderEntity</c> objects into <c>Order</c> objects.</param>
        public GetOrderHistoryForTerminalUseCase(
            IOrderRepository orderRepository,
            OrderConverter orderConverter)
        {
            this.orderRepository = orderRepository;
            this.orderConverter = orderConverter;
        }

        /// <summary>
        /// Executes the logic of the use case by getting the orders for the parameters of the specified request DTO.
        /// </summary>
        /// <param name="request">The <see cref="GetOrdersForTerminal"/> instance containing the parameters to get the orders.</param>
        /// <returns>An <see cref="IEnumerable{OrderEntity}"/> instance containing the orders.</returns>
        public Task<ResponseBase<IEnumerable<Order>>> ExecuteAsync(GetOrderHistoryForTerminal request) =>
            Task.Run(() =>
                ResponseBase<IEnumerable<Order>>.AsSuccess(
                    this.orderConverter.ConvertEntityCollectionToTResponseDtoCollection(
                        this.orderRepository.GetOrdersHistory(request.TerminalId))));
    }
}
