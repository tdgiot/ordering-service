﻿using Crave.Enums;
using Crave.Ordering.Api.Domain.UseCases.Requests;
using Crave.Ordering.Api.Domain.UseCases.Responses;
using Crave.Ordering.Shared.Interfaces;
using System.Threading.Tasks;
using Crave.Ordering.Shared.OrderNotifications.Domain.UseCases.Requests;
using Crave.Ordering.Shared.OrderNotifications.Domain.UseCases.Responses;
using Crave.Ordering.Shared.OrderNotifications.Interfaces.UseCases;

namespace Crave.Ordering.Api.Domain.UseCases
{
    public class ResendReceiptUseCase : IUseCaseAsync<ResendReceipt, CreateAndSendReceiptResponse>
    {
        private readonly ISendOrderNotificationUseCase sendOrderNotificationUseCase;

        public ResendReceiptUseCase(ISendOrderNotificationUseCase sendOrderNotificationUseCase)
        {
            this.sendOrderNotificationUseCase = sendOrderNotificationUseCase;
        }

        public async Task<CreateAndSendReceiptResponse> ExecuteAsync(ResendReceipt request)
        {
            SendOrderNotificationRequest sendOrderNotificationRequest = new SendOrderNotificationRequest(request.OrderId, OrderNotificationType.Receipt)
            {
                SendToOriginalReceiver = request.SendToOriginalReceiver,
                AdditionalEmailReceivers = request.AdditionalReceivers
            };

            SendOrderNotificationResponse sendOrderNotificationResponse = await sendOrderNotificationUseCase.ExecuteAsync(sendOrderNotificationRequest);

            return new CreateAndSendReceiptResponse
            {
                EmailSent = sendOrderNotificationResponse.Success,
                SmsSent = false,
                ErrorMessage = sendOrderNotificationResponse.Message
            };
        }
    }
}