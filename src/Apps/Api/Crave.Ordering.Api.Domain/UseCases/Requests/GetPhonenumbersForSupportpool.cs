﻿namespace Crave.Ordering.Api.Domain.UseCases.Requests
{
    public class GetPhonenumbersForSupportpool
    {
        public int SupportpoolId { get; set; }
    }
}
