﻿using System.Collections.Generic;

namespace Crave.Ordering.Api.Domain.UseCases.Requests
{
    /// <summary>
    /// Request class which is used to validate order items.
    /// </summary>
    public class ValidateOrderitemsRequest
    {
        public ValidateOrderitemsRequest(IEnumerable<OrderitemRequest> orderitems, int companyId)
        {
            this.Orderitems = orderitems;
            this.CompanyId = companyId;
        }

        /// <summary>
        /// Gets the order items to validate.
        /// </summary>
        public IEnumerable<OrderitemRequest> Orderitems { get; }

        /// <summary>
        /// Gets the ID of the company where the order is placed.
        /// </summary>
        public int CompanyId { get; }
    }
}
