﻿using System;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Ordering.Api.Domain.UseCases.Requests
{
    /// <summary>
    /// Request class which is used to route an order.
    /// </summary>
    public class RouteOrder
    {
        public RouteOrder(OrderEntity orderEntity)
        {
            orderEntity.ThrowIfArgumentNull();

            this.OrderEntity = orderEntity;
        }

        /// <summary>
        /// Gets the order to route.
        /// </summary>
        public OrderEntity OrderEntity { get; set; }
    }
}
