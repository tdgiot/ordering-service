﻿using Crave.Enums;

namespace Crave.Ordering.Api.Domain.UseCases.Requests
{
    /// <summary>
    /// Request class which is used to create and send an order notification.
    /// </summary>
    public class CreateAndSendOrderNotificationRequest
    {
        public CreateAndSendOrderNotificationRequest(int orderId, 
                                                     OrderNotificationType orderNotificationType)
        {
            this.OrderId = orderId;
            this.OrderNotificationType = orderNotificationType;
        }

        /// <summary>
        /// Gets the id of the order.
        /// </summary>
        public int OrderId { get; }

        /// <summary>
        /// Gets the order notification type.
        /// </summary>
        public OrderNotificationType OrderNotificationType { get; }

        /// <summary>
        /// Gets if the order notification should be sent to the original receiver.
        /// </summary>
        public bool SendToOriginalReceiver { get; set; } = true;

        /// <summary>
        /// Gets the additional email notification receivers.
        /// </summary>
        public string AdditionalEmailReceivers { get; set; } = string.Empty;
    }
}