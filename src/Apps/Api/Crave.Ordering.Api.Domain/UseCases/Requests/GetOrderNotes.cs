﻿using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Ordering.Api.Domain.UseCases.Requests
{
    /// <summary>
    /// Request class which is used to get the notes of an order.
    /// </summary>
    public class GetOrderNotes
    {
        /// <summary>
        /// Gets or sets the order to get the notes for.
        /// </summary>
        public OrderEntity Order { get; set; }
    }
}
