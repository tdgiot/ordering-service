﻿using Crave.Enums;

namespace Crave.Ordering.Api.Domain.UseCases.Requests
{
    public class UpdateOrderStatus
    {
        public UpdateOrderStatus(int orderId, OrderStatus status)
        {
            this.OrderId = orderId;
            this.Status = status;
        }

        public int OrderId { get; set; }

        public OrderStatus Status { get; set; }

        public OrderErrorCode? ErrorCode { get; set; }

        public string? RoutingLog { get; set; }
    }
}
