﻿using System;
using Crave.Libraries.Distance.Interfaces;

namespace Crave.Ordering.Api.Domain.UseCases.Requests
{
    public class GetDistanceBetweenLocationsRequest
    {
        public GetDistanceBetweenLocationsRequest(ILocation originLocation, 
                                                  ILocation destinationLocation)
        {
            originLocation.ThrowIfArgumentNull();
            destinationLocation.ThrowIfArgumentNull();

            this.OriginLocation = originLocation;
            this.DestinationLocation = destinationLocation;
        }

        public ILocation OriginLocation { get; }
        public ILocation DestinationLocation { get; }
    }
}
