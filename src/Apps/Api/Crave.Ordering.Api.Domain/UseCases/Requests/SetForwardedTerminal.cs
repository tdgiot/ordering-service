﻿using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Ordering.Api.Domain.UseCases.Requests
{
    public class SetForwardedTerminal
    {
        public OrderRoutestephandlerEntity OrderRoutestephandlerEntity { get; set; }
    }
}
