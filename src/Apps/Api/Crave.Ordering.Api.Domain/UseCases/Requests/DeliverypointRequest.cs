﻿namespace Crave.Ordering.Api.Domain.UseCases.Requests
{
    public class DeliverypointRequest
    {
        public DeliverypointRequest(string number, 
                                    int? deliverypointgroupId)
        {
            this.Number = number;
            this.DeliverypointgroupId = deliverypointgroupId;
        }

        public string Number { get; }
        public int? DeliverypointgroupId { get; }
    }
}
