﻿using Crave.Enums;

namespace Crave.Ordering.Api.Domain.UseCases.Requests
{
    public class PaymentStatusChanged
    {
        public PaymentStatusChanged(int orderId, PaymentTransactionStatus paymentTransactionStatus)
        {
            OrderId = orderId;
            PaymentTransactionStatus = paymentTransactionStatus;
        }

        public int OrderId { get; set; }
        public PaymentTransactionStatus PaymentTransactionStatus { get; }
    }
}
