﻿namespace Crave.Ordering.Api.Domain.UseCases.Requests
{
    /// <summary>
    /// Request DTO class which is used to retrieve orders for a terminal.
    /// </summary>
    public class GetOrdersForTerminal
    {
        public GetOrdersForTerminal(int terminalId)
        {
            this.TerminalId = terminalId;
        }

        /// <summary>
        /// Gets sets the id of the terminal to retrieve the orders for.
        /// </summary>
        public int TerminalId { get; }
    }
}
