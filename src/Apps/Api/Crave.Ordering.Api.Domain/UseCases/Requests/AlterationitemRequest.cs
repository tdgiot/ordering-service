﻿using System;

namespace Crave.Ordering.Api.Domain.UseCases.Requests
{
    public class AlterationitemRequest
    {
        public AlterationitemRequest(int alterationId, 
                                     int? alterationoptionId, 
                                     decimal? price, 
                                     string? value, 
                                     DateTime? timeUtc, 
                                     int? priceLevelItemId)
        {
            AlterationId = alterationId;
            AlterationoptionId = alterationoptionId;
            Price = price;
            Value = value;
            TimeUTC = timeUtc;
            PriceLevelItemId = priceLevelItemId;
        }

        public int AlterationId { get; }
        public int? AlterationoptionId { get; }
        public decimal? Price { get; }
        public string? Value { get; }
        public DateTime? TimeUTC { get; }
        public int? PriceLevelItemId { get; }
    }
}
