﻿using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Ordering.Api.Domain.UseCases.Requests
{
    /// <summary>
    /// Request class which is used to validate an order before creation.
    /// </summary>
    public class ValidateOrderBeforeCreation
    {
        public ValidateOrderBeforeCreation(OrderEntity orderEntity)
        {
            this.OrderEntity = orderEntity;
        }

        /// <summary>
        /// Gets or sets the order to validate.
        /// </summary>
        public OrderEntity OrderEntity { get; set; }
    }
}