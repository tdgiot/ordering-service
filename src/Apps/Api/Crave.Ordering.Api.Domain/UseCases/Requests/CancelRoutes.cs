﻿namespace Crave.Ordering.Api.Domain.UseCases.Requests
{
    public class CancelRoutes
    {
        public int ClientId { get; set; }

        public int CustomerId { get; set; }

        public string OrderGuid { get; set; }
    }
}
