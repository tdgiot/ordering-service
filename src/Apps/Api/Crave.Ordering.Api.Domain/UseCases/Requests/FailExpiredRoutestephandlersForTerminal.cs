﻿namespace Crave.Ordering.Api.Domain.UseCases.Requests
{
    /// <summary>
    /// Request DTO class which is used to fail expired routestephandlers for a terminal.
    /// </summary>
    public class FailExpiredRoutestephandlersForTerminal
    {
        /// <summary>
        /// Gets or sets the id of the terminal to retrieve the orders for.
        /// </summary>
        public int TerminalId { get; set; }
    }
}
