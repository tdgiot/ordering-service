﻿namespace Crave.Ordering.Api.Domain.UseCases.Requests
{
    public class AnalyticsRequest
    {
        public AnalyticsRequest(string? payload, 
                                string? trackingIds)
        {
            this.Payload = payload;
            this.TrackingIds = trackingIds;
        }

        public string? Payload { get; }
        public string? TrackingIds { get; }
    }
}
