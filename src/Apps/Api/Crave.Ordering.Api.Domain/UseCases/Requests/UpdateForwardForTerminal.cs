﻿namespace Crave.Ordering.Api.Domain.UseCases.Requests
{
    /// <summary>
    /// Request DTO class which is used to update the forwarding of orders for a terminal.
    /// </summary>
    public class UpdateForwardForTerminal
    {
        /// <summary>
        /// Gets or sets the id of the terminal to update.
        /// </summary>
        public int TerminalId { get; set; }

        /// <summary>
        /// Gets or sets the id of the terminal to forward to.
        /// </summary>
        public int ForwardToTerminalId { get; set; }
    }
}
