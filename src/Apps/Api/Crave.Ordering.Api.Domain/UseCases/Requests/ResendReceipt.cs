﻿namespace Crave.Ordering.Api.Domain.UseCases.Requests
{
    public class ResendReceipt
    {
        public int OrderId { get; set; }
        public bool SendToOriginalReceiver { get; set; }
        public string AdditionalReceivers { get; set; }
    }
}