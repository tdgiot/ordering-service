﻿namespace Crave.Ordering.Api.Domain.UseCases.Requests
{
    public class TippingRequest
    {
        public TippingRequest(decimal price)
        {
            Price = price;
        }

        public decimal Price { get; }
    }
}
