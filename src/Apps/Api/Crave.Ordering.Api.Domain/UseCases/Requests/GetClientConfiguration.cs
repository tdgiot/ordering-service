﻿namespace Crave.Ordering.Api.Domain.UseCases.Requests
{
    public class GetClientConfiguration
    {
        public int ClientId { get; set; }

        public int DeliverypointId { get; set; }
    }
}
