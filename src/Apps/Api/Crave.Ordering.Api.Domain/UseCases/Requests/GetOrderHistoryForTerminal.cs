﻿namespace Crave.Ordering.Api.Domain.UseCases.Requests
{
    /// <summary>
    /// Request DTO class which is used to retrieve orders history for a terminal.
    /// </summary>
    public class GetOrderHistoryForTerminal
    {
        public GetOrderHistoryForTerminal(int terminalId) => TerminalId = terminalId;

        /// <summary>
        /// Gets or sets the id of the terminal to retrieve the orders for.
        /// </summary>
        public int TerminalId { get; }
    }
}
