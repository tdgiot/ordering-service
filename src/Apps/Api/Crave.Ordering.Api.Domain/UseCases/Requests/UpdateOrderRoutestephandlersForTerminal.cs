﻿namespace Crave.Ordering.Api.Domain.UseCases.Requests
{
    /// <summary>
    /// Request DTO class which is used to update the statuses of the route step handlers of a terminal.
    /// </summary>
    public class UpdateOrderRoutestephandlersForTerminal
    {
        public UpdateOrderRoutestephandlersForTerminal(int orderRoutestephandlerId,
                                                       UpdateOrderRoutestephandlerStatusRequest updateOrderRoutestephandlerStatusRequest)
        {
            this.UpdateOrderRoutestephandlerStatusRequest = updateOrderRoutestephandlerStatusRequest;
            this.OrderRoutestephandlerId = orderRoutestephandlerId;
        }

        public int OrderRoutestephandlerId { get; }
        public UpdateOrderRoutestephandlerStatusRequest UpdateOrderRoutestephandlerStatusRequest { get; }
    }
}
