﻿namespace Crave.Ordering.Api.Domain.UseCases.Requests
{
    public class ServiceChargeRequest
    {
        public ServiceChargeRequest(decimal price)
        {
            this.Price = price;
        }

        public decimal Price { get; }
    }
}
