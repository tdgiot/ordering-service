﻿using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Ordering.Api.Domain.UseCases.Requests
{
    public class GetTerminalsForwardingFromThisTerminal
    {
        public TerminalEntity TerminalEntity { get; set; }
    }
}
