﻿using Crave.Enums;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Ordering.Api.Domain.UseCases.Requests
{
    public class UpdateOrderRoutestephandlerStatuses
    {
        public OrderEntity OrderEntity { get; set; }

        public OrderStatus Status { get; set; }

        public bool OnlyWhenFailed { get; set; }

        public TerminalEntity TerminalEntity { get; set; }
    }
}
