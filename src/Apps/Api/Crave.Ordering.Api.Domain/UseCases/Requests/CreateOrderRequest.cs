﻿using Crave.Enums;
using System;
using System.Collections.Generic;

namespace Crave.Ordering.Api.Domain.UseCases.Requests
{
    /// <summary>
    /// Request class which is used to create an new order.
    /// </summary>
    public class CreateOrderRequest
    {
        [Obsolete("Ctor should only be used by AutoMapper or UnitTests")]
        public CreateOrderRequest()
        {
        }

        public CreateOrderRequest(OrderType orderType,
                                  string? guid,
                                  int companyId,
                                  int? serviceMethodId,
                                  int? checkoutMethodId,
                                  int? deliverypointId,
                                  int? clientId,
                                  string? notes,
                                  TippingRequest? tipping,
                                  ServiceChargeRequest? serviceCharge,
                                  DeliveryChargeRequest? deliveryCharge,
                                  DeliverypointRequest? serviceMethodDeliverypoint,
                                  DeliverypointRequest? checkoutMethodDeliverypoint,
                                  CustomerRequest? customer,
                                  DeliveryInformationRequest? deliveryInformation,
                                  AnalyticsRequest? analytics,
                                  IEnumerable<OrderitemRequest>? orderitems,
                                  OptInStatus optInStatus,
                                  string? orderContext,
                                  int? coversCount)
        {
            OrderType = orderType;
            Guid = guid;
            CompanyId = companyId;
            ServiceMethodId = serviceMethodId;
            CheckoutMethodId = checkoutMethodId;
            DeliverypointId = deliverypointId;
            ClientId = clientId;
            Notes = notes;
            Tipping = tipping;
            ServiceCharge = serviceCharge;
            DeliveryCharge = deliveryCharge;
            ServiceMethodDeliverypoint = serviceMethodDeliverypoint;
            CheckoutMethodDeliverypoint = checkoutMethodDeliverypoint;
            Customer = customer;
            DeliveryInformation = deliveryInformation;
            Analytics = analytics;
            Orderitems = orderitems;
            OptInStatus = optInStatus;
            OrderContext = orderContext;
            CoversCount = coversCount;
        }

        public OrderType OrderType { get; }
        public string? Guid { get; }
        public int CompanyId { get; }
        public int? ServiceMethodId { get; }
        public int? CheckoutMethodId { get; }
        public int? DeliverypointId { get; }
        public int? ClientId { get; }
        public string? Notes { get; }
        public TippingRequest? Tipping { get; }
        public ServiceChargeRequest? ServiceCharge { get; }
        public DeliveryChargeRequest? DeliveryCharge { get; }
        public DeliverypointRequest? ServiceMethodDeliverypoint { get; }
        public DeliverypointRequest? CheckoutMethodDeliverypoint { get; }
        public CustomerRequest? Customer { get; }
        public DeliveryInformationRequest? DeliveryInformation { get; }
        public AnalyticsRequest? Analytics { get; }
        public IEnumerable<OrderitemRequest>? Orderitems { get; }
        public OptInStatus OptInStatus { get; }
        public string? OrderContext { get; }
        public int? CoversCount { get; }
    }
}