﻿using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Ordering.Api.Domain.UseCases.Requests
{
    public class HandleNonTerminalRoutingstephandler
    {
        public OrderRoutestephandlerEntity OrderRoutestephandlerEntity { get; set; }
        public OrderEntity OrderEntity { get; set; }
    }
}