﻿namespace Crave.Ordering.Api.Domain.UseCases.Requests
{
    public class ProcessClientOrders
    {
        public int ClientId { get; set; }
    }
}
