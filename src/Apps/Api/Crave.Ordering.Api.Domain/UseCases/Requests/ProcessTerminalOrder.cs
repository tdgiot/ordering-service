﻿namespace Crave.Ordering.Api.Domain.UseCases.Requests
{
    /// <summary>
    /// Request DTO class which is used to process an order for a terminal.
    /// </summary>
    public class ProcessTerminalOrder
    {
        /// <summary>
        /// Gets or sets the id of the terminal to process the order for.
        /// </summary>
        public int TerminalId { get; set; }

        /// <summary>
        /// Gets or sets the GUID of the order to process.
        /// </summary>
        public string OrderGuid { get; set; }
    }
}
