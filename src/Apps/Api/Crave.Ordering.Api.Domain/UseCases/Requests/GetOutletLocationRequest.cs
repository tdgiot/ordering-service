﻿using System;

namespace Crave.Ordering.Api.Domain.UseCases.Requests
{
    public class GetOutletLocationRequest
    {
        public GetOutletLocationRequest(int outletId)
        {
            outletId.ThrowIfArgumentNull();

            this.OutletId = outletId;
        }

        public int OutletId { get; }
    }
}
