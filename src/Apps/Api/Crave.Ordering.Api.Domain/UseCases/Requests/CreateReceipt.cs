﻿using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Ordering.Api.Domain.UseCases.Requests
{
    public class CreateReceipt
    {
        public OrderEntity Order { get; set; }
    }
}