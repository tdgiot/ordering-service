﻿namespace Crave.Ordering.Api.Domain.UseCases.Requests
{
    /// <summary>
    /// Request class which is used to create and start an order route.
    /// </summary>
    public class CreateOrderRoute
    {
        public CreateOrderRoute(int orderId)
        {
            this.OrderId = orderId;
        }

        /// <summary>
        /// Gets or sets the order to route.
        /// </summary>
        public int OrderId { get; set; }
    }
}