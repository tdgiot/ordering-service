﻿namespace Crave.Ordering.Api.Domain.UseCases.Requests
{
    public class DeliveryInformationRequest
    {
        public DeliveryInformationRequest(string buildingName, 
                                          string address, 
                                          string zipcode, 
                                          string city,
                                          string instructions, 
                                          double longitude, 
                                          double latitude, 
                                          int distanceToOutletInMetres)
        {
            this.BuildingName = buildingName;
            this.Address = address;
            this.Zipcode = zipcode;
            this.City = city;
            this.Instructions = instructions;
            this.Longitude = longitude;
            this.Latitude = latitude;
            this.DistanceToOutletInMetres = distanceToOutletInMetres;
        }

        public string BuildingName { get; }
        public string Address { get; }
        public string Zipcode { get; }
        public string City { get; }
        public string Instructions { get; }
        public double Longitude { get; }
        public double Latitude { get; }
        public int DistanceToOutletInMetres { get; }
    }
}
