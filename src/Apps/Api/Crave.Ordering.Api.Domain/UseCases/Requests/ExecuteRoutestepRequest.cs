﻿namespace Crave.Ordering.Api.Domain.UseCases.Requests
{
    public class ExecuteRoutestepRequest
    {
        public ExecuteRoutestepRequest(int orderId, int routeStepNumber)
        {
            this.OrderId = orderId;
            this.RouteStepNumber = routeStepNumber;
        }

        public int OrderId { get; }
        public int RouteStepNumber { get; }
    }
}