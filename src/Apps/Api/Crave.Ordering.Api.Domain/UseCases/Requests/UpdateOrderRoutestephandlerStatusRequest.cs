﻿using Crave.Enums;

namespace Crave.Ordering.Api.Domain.UseCases.Requests
{
    public class UpdateOrderRoutestephandlerStatusRequest
    {
        /// <summary>
        /// The new order route step handler status
        /// </summary>
        public OrderRoutestephandlerStatus Status { get; set; }

        /// <summary>
        /// Optional error code for when the route step handler has failed
        /// </summary>
        public OrderErrorCode ErrorCode { get; set; }

        /// <summary>
        /// Gets or sets the id of the terminal to update the route step handlers for.
        /// </summary>
        public int TerminalId { get; set; }
    }
}