﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Crave.Enums;

namespace Crave.Ordering.Api.Domain.UseCases.Requests
{
    public class OrderitemRequest
    {
        public OrderitemRequest(OrderitemType type, 
                                string? guid,
                                int productId, 
                                int? categoryId, 
                                int quantity,
                                decimal? price, 
                                int? priceLevelItemId, 
                                string? notes,
                                IEnumerable<AlterationitemRequest>? alterationitems)
        {
            TempInternalGuid = System.Guid.NewGuid();
            Guid = guid;
            Type = type;
            ProductId = productId;
            CategoryId = categoryId;
            Quantity = quantity;
            Price = price;
            PriceLevelItemId = priceLevelItemId;
            Notes = notes;
            Alterationitems = alterationitems;
        }

        [IgnoreDataMember]
        public Guid TempInternalGuid { get; }
        public OrderitemType Type { get; }
        public string? Guid { get; }
        public int ProductId { get; }
        public int? CategoryId { get; }
        public int Quantity { get; }
        public decimal? Price { get; }
        public int? PriceLevelItemId { get; }
        public string? Notes { get; }
        public IEnumerable<AlterationitemRequest>? Alterationitems { get; }
    }
}
