﻿namespace Crave.Ordering.Api.Domain.UseCases.Requests
{
    public class GetTerminalToForwardTo
    {
        public int TerminalId { get; set; }
    }
}
