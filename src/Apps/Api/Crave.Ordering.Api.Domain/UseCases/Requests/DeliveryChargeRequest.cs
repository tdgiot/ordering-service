﻿namespace Crave.Ordering.Api.Domain.UseCases.Requests
{
    public class DeliveryChargeRequest
    {
        public DeliveryChargeRequest(decimal price)
        {
            this.Price = price;
        }

        public decimal Price { get; }
    }
}
