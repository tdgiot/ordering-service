﻿using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Ordering.Api.Domain.UseCases.Requests
{
    public class SendReceipt
    {
        public ReceiptEntity ReceiptEntity { get; set; }
        public bool EmailEnabled { get; set; }
        public bool SmsEnabled { get; set; }
        public bool ToSeller { get; set; }
        public string ReceiverEmailOverwrite { get; set; } = string.Empty;
    }
}