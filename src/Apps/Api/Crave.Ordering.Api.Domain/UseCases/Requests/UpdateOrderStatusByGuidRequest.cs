﻿using Crave.Enums;

namespace Crave.Ordering.Api.Domain.UseCases.Requests
{
    public class UpdateOrderStatusByGuidRequest
    {
        public UpdateOrderStatusByGuidRequest(string guid, OrderStatus status)
        {
            this.Guid = guid;
            this.Status = status;
        }

        public string Guid { get; set; }
        public OrderStatus Status { get; set; }
    }
}