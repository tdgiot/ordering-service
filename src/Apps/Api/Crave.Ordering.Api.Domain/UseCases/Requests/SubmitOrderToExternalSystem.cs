﻿using Crave.Enums;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Ordering.Api.Domain.UseCases.Requests
{
    public class SubmitOrderToExternalSystem
    {
        public SubmitOrderToExternalSystem(OrderEntity order,
                                           ExternalSystemType externalSystemType)
        {
            this.Order = order;
            this.ExternalSystemType = externalSystemType;
        }

        public OrderEntity Order { get; }

        public ExternalSystemType ExternalSystemType { get; }
    }
}
