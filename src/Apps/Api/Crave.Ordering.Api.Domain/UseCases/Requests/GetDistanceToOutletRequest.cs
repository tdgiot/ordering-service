﻿using System;
using Crave.Ordering.Api.Domain.Models;

namespace Crave.Ordering.Api.Domain.UseCases.Requests
{
    public class GetDistanceToOutletRequest
    {
        public GetDistanceToOutletRequest(int outletId, 
                                          DeliveryLocation deliveryLocation)
        {
            outletId.ThrowIfArgumentNull();
            deliveryLocation.ThrowIfArgumentNull();

            this.OutletId = outletId;
            this.DeliveryLocation = deliveryLocation;
        }

        public int OutletId { get; }
        public DeliveryLocation DeliveryLocation { get; }
    }
}
