﻿namespace Crave.Ordering.Api.Domain.UseCases.Requests
{
    public class CustomerRequest
    {
        public CustomerRequest(string? email, 
                               string? firstName,
                               string? lastName, 
                               string? lastNamePrefix,
                               string? phonenumber)
        {
            this.Email = email;
            this.FirstName = firstName;
            this.LastName = lastName;
            this.LastNamePrefix = lastNamePrefix;
            this.Phonenumber = phonenumber;
        }

        public string? Email { get; }
        public string? FirstName { get; }
        public string? LastName { get; }
        public string? LastNamePrefix { get; }
        public string? Phonenumber { get; }
    }
}
