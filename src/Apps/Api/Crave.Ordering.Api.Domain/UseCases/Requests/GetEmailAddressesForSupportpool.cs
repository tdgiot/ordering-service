﻿namespace Crave.Ordering.Api.Domain.UseCases.Requests
{
    public class GetEmailAddressesForSupportpool
    {
        public int SupportpoolId { get; set; }
    }
}
