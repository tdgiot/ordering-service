﻿using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Ordering.Api.Domain.UseCases.Requests
{
    public class WriteRoute
    {
        public WriteRoute(OrderEntity orderEntity, RouteEntity routeEntity, bool startRoute, bool escalationRoute)
        {
            this.Order = orderEntity;
            this.Route = routeEntity;
            this.StartRoute = startRoute;
            this.EscalationRoute = escalationRoute;
        }

        public OrderEntity Order { get; set; }

        public RouteEntity Route { get; set; }

        public bool StartRoute { get; set; }

        public bool EscalationRoute { get; set; }
    }
}
