﻿using System;
using Crave.Ordering.Api.Domain.Models;

namespace Crave.Ordering.Api.Domain.UseCases.Requests
{
    public class GetDeliveryRateRequest
    {
        public GetDeliveryRateRequest(int serviceMethodId, 
                                      DeliveryLocation deliveryLocation, 
                                      decimal totalPrice)
        {
            serviceMethodId.ThrowIfArgumentNull();
            deliveryLocation.ThrowIfArgumentNull();
            totalPrice.ThrowIfArgumentNull();

            this.ServiceMethodId = serviceMethodId;
            this.DeliveryLocation = deliveryLocation;
            this.TotalPrice = totalPrice;
        }

        public int ServiceMethodId { get; }
        public DeliveryLocation DeliveryLocation { get; }
        public decimal TotalPrice { get; }
    }
}
