﻿using System.Collections.Generic;
using System.Linq;
using Crave.Ordering.Api.Domain.UseCases.Requests;
using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Ordering.Api.Domain.UseCases
{
    public class GetTerminalToForwardToUseCase : UseCaseBase<GetTerminalToForwardTo, TerminalEntity?>
    {
        #region Fields

        private readonly ITerminalRepository terminalRepository;
        private readonly GetTerminalsForwardingFromThisTerminalUseCase getTerminalsForwardingFromThisTerminalUseCase;

        #endregion

        #region Constructors

        public GetTerminalToForwardToUseCase(ITerminalRepository terminalRepository, 
                                             GetTerminalsForwardingFromThisTerminalUseCase getTerminalsForwardingFromThisTerminalUseCase)
        {
            this.terminalRepository = terminalRepository;
            this.getTerminalsForwardingFromThisTerminalUseCase = getTerminalsForwardingFromThisTerminalUseCase;
        }

        #endregion

        public override TerminalEntity? Execute(GetTerminalToForwardTo request)
        {
            TerminalEntity? forwardToTerminalEntity = null;

            TerminalEntity handlingTerminalEntity = this.terminalRepository.GetTerminal(request.TerminalId);
            List<TerminalEntity> forwardingTerminals = this.getTerminalsForwardingFromThisTerminalUseCase.Execute(new GetTerminalsForwardingFromThisTerminal { TerminalEntity = handlingTerminalEntity });

            if (forwardingTerminals.Count > 0)
            {
                forwardToTerminalEntity = forwardingTerminals.Last();
            }

            return forwardToTerminalEntity;
        }
    }
}
