﻿using System;
using System.Collections.Generic;
using System.Linq;
using Crave.Enums;
using Crave.Ordering.Api.Domain.UseCases.Requests;
using Crave.Ordering.Shared.Constants;
using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Api.Domain.Interfaces.UseCases;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.Utils;
using System.Threading.Tasks;

namespace Crave.Ordering.Api.Domain.UseCases
{
    public class WriteRouteUseCase : IWriteRouteUseCase
    {
        public enum WriteRouteResult
        {
            /// <summary>
            ///     No routesteps for this route
            /// </summary>
            RouteHasZeroSteps = 201,

            /// <summary>
            ///     The route doesn't start with number one
            /// </summary>
            RouteDoesNotStartWithNumberOne = 202
        }

        private readonly IOrderRoutestephandlerRepository orderRoutestephandlerRepository;
        private readonly ISaveOrderUseCase saveOrderUseCase;
        private readonly IStartRouteUseCase startRouteUseCase;
        private const int BEING_HANDLED_TIMEOUT_DEFAULT = 15; // Don't change this without updating the Google Doc 'Intervals & Treshholds'
        private const int RETRIEVAL_TIMEOUT_DEFAULT = 3; // Don't change this without updating the Google Doc 'Intervals & Treshholds'

        public WriteRouteUseCase(IOrderRoutestephandlerRepository orderRoutestephandlerRepository,
                                 ISaveOrderUseCase saveOrderUseCase,
                                 IStartRouteUseCase startRouteUseCase)
        {
            this.orderRoutestephandlerRepository = orderRoutestephandlerRepository;
            this.saveOrderUseCase = saveOrderUseCase;
            this.startRouteUseCase = startRouteUseCase;
        }

        /// <summary>
        /// Execute use-case
        /// </summary>
        /// <param name="request"></param>
        /// <returns>Always returns true or throws exception on error</returns>
        public async Task<bool> ExecuteAsync(WriteRoute request)
        {
            RouteEntity routeEntity = request.Route;
            OrderEntity orderEntity = request.Order;
            bool escalationRoute = request.EscalationRoute;

            // Precheck the route (before flight ;))
            if (routeEntity.RoutestepCollection.Count == 0)
            {
                // No steps in the route, at least one would be required.
                throw new CraveException(WriteRouteResult.RouteHasZeroSteps);
            }
            if (routeEntity.RoutestepCollection.Min(x => x.Number) != RoutingConstants.FirstStepNumber)
            {
                // The first step MUST start with One
                throw new CraveException(WriteRouteResult.RouteDoesNotStartWithNumberOne);
            }

            // Stepnumber increase when glueing a escaltionRoute to an existing route
            int stepNumberIncrease = escalationRoute ? orderEntity.OrderRoutestephandlerCollection.Max(x => x.Number) : 0;

            // Iterate over each step
            List<OrderRoutestephandlerEntity> firstSteps = ProcessSteps(routeEntity, orderEntity, escalationRoute, stepNumberIncrease);

            // Mark order as Routed (not when escaltion route, it was already in-route)
            if (!escalationRoute)
            {
                orderEntity.Status = OrderStatus.Routed;
            }

            // Save the order
            await saveOrderUseCase.ExecuteAsync(orderEntity);

            // TODO-PROCESS-ROUTE-LAMBDA

            // Start route here, because we want the route to be completely written before it starts
            foreach (OrderRoutestephandlerEntity step in firstSteps)
            {
                await startRouteUseCase.ExecuteAsync(step);
            }

            return true;
        }

        private List<OrderRoutestephandlerEntity> ProcessSteps(RouteEntity routeEntity, OrderEntity orderEntity, bool escalationRoute, int stepNumberIncrease)
        {
            List<OrderRoutestephandlerEntity> firstSteps = new List<OrderRoutestephandlerEntity>();
            int lastStepnumber = routeEntity.RoutestepCollection.Max(rs => rs.Number);

            foreach (RoutestepEntity templateStep in routeEntity.RoutestepCollection)
            {
                // Iterate over the handlers of the step
                foreach (RoutestephandlerEntity templateStepHandler in templateStep.RoutestephandlerCollection)
                {
                    // Create the new step that we will save for the order
                    OrderRoutestephandlerEntity actualStep = new OrderRoutestephandlerEntity();

                    // Copy all fields from step and stephandle
                    LLBLGenEntityUtil.CopyFields(templateStep, actualStep);
                    LLBLGenEntityUtil.CopyFields(templateStepHandler, actualStep);

                    // Set non-copyable fields
                    actualStep.Guid = Guid.NewGuid().ToString();
                    actualStep.Number = templateStep.Number + stepNumberIncrease;
                    actualStep.OrderId = orderEntity.OrderId;
                    actualStep.LogAlways = routeEntity.LogAlways;
                    actualStep.Timeout = templateStep.TimeoutMinutes.HasValue && templateStep.TimeoutMinutes.Value > 0 ? templateStep.TimeoutMinutes.Value : routeEntity.StepTimeoutMinutes;
                    actualStep.EscalationStep = escalationRoute;
                    actualStep.EscalationRouteId = templateStep.Route.EscalationRouteId;
                    actualStep.BeingHandledSupportNotificationTimeoutMinutes = routeEntity.BeingHandledSupportNotificationTimeoutMinutes ?? BEING_HANDLED_TIMEOUT_DEFAULT;
                    actualStep.RetrievalSupportNotificationTimeoutMinutes = routeEntity.RetrievalSupportNotificationTimeoutMinutes ?? RETRIEVAL_TIMEOUT_DEFAULT;
                    actualStep.OriginatedFromRoutestepHandlerId = templateStepHandler.RoutestephandlerId;
                    actualStep.File = orderEntity.File;

                    // All steps, including the first will always be set to be waiting untill the
                    // order is triggered to be started by this use case
                    actualStep.Status = OrderRoutestephandlerStatus.WaitingForPreviousStep;
                    actualStep.StatusText = actualStep.Status.ToString();

                    actualStep.HandlerTypeText = actualStep.HandlerType.ToString();

                    // Disable continue on failure for last step
                    if (templateStep.Number == lastStepnumber)
                    {
                        actualStep.ContinueOnFailure = false;
                    }

                    // Save directly
                    orderRoutestephandlerRepository.SaveOrderRoutestephandler(actualStep);

                    // Record the first step number we have created to later activate that as the first step
                    if (templateStep.Number == RoutingConstants.FirstStepNumber)
                    {
                        firstSteps.Add(actualStep);
                    }
                }
            }

            return firstSteps;
        }
    }
}
