﻿using System;
using Crave.Enums;
using Crave.Ordering.Api.Domain.UseCases.Requests;
using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Api.Domain.Interfaces.UseCases;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.Extensions;

namespace Crave.Ordering.Api.Domain.UseCases
{
    public class CreateReceiptUseCase : ICreateReceiptUseCase
    {
        private readonly IReceiptRepository receiptRepository;
        private readonly IOutletSellerInformationRepository outletSellerInformationRepository;
        private readonly ICheckoutMethodRepository checkoutMethodRepository;
        private readonly IPaymentIntegrationConfigurationRepository paymentIntegrationConfigurationRepository;

        public CreateReceiptUseCase(IReceiptRepository receiptRepository, 
                                    IOutletSellerInformationRepository outletSellerInformationRepository,
                                    ICheckoutMethodRepository checkoutMethodRepository,
                                    IPaymentIntegrationConfigurationRepository paymentIntegrationConfigurationRepository)
        {
            this.receiptRepository = receiptRepository;
            this.outletSellerInformationRepository = outletSellerInformationRepository;
            this.checkoutMethodRepository = checkoutMethodRepository;
            this.paymentIntegrationConfigurationRepository = paymentIntegrationConfigurationRepository;
        }

        public void Execute(CreateReceipt request)
        {
            OrderEntity order = request.Order;

            if (!order.CheckoutMethodId.HasValue)
            {
                throw new InvalidOperationException($"No checkout method configured on order (orderId={order.OrderId})");
            }

            if (!order.OutletId.HasValue)
            {
                throw new InvalidOperationException($"No outlet configured on order (orderId={order.OrderId})");
            }

            CheckoutMethodEntity checkoutMethod = checkoutMethodRepository.GetCheckoutMethod(order.CheckoutMethodId.Value);

            if (checkoutMethod.CheckoutType != CheckoutType.PaymentProvider)
            {
                throw new InvalidOperationException($"Invalid checkout type {checkoutMethod.CheckoutType} on checkout method for order {order.OrderId}");
            }

            if (!checkoutMethod.PaymentIntegrationConfigurationId.HasValue)
            {
                throw new InvalidOperationException($"Checkout method does not have a payment integration configuration configured (checkoutMethodId={order.CheckoutMethodId}, orderId={order.OrderId})");
            }

            PaymentIntegrationConfigurationEntity paymentIntegrationConfigurationEntity = paymentIntegrationConfigurationRepository.FetchById(checkoutMethod.PaymentIntegrationConfigurationId.Value);
            if (paymentIntegrationConfigurationEntity.IsNullOrNew())
            {
                throw new InvalidOperationException($"No payment integration configuration found (paymentIntegrationConfigurationId={checkoutMethod.PaymentIntegrationConfigurationId}, checkoutMethodId={order.CheckoutMethodId}, orderId={order.OrderId})");
            }
            
            OutletSellerInformationEntity outletSellerInformation = outletSellerInformationRepository.FetchByOutletId(order.OutletId.Value);

            string paymentProcessorInfo = !paymentIntegrationConfigurationEntity.PaymentProcessorInfo.IsNullOrWhiteSpace()
                ? paymentIntegrationConfigurationEntity.PaymentProcessorInfo
                : paymentIntegrationConfigurationEntity.PaymentProvider.PaymentProcessorInfo;

            ReceiptEntity receiptEntity = new ReceiptEntity
            {
                OutletSellerInformationId = outletSellerInformation.OutletSellerInformationId,
                CompanyId = order.CompanyId,
                OrderId = order.OrderId,
                SellerName = outletSellerInformation.SellerName,
                SellerAddress = outletSellerInformation.Address,
                SellerContactInfo = outletSellerInformation.Phonenumber,
                SellerContactEmail = outletSellerInformation.Email,
                PaymentProcessorInfo = paymentProcessorInfo,
                CheckoutMethodName = order.CheckoutMethodName,
                ServiceMethodName = order.ServiceMethodName,
                Phonenumber = outletSellerInformation.ReceiptReceiversSms,
                VatNumber = outletSellerInformation.VatNumber,
                TaxBreakdown = TaxBreakdown.None,
                Email = outletSellerInformation.ReceiptReceiversEmail
            };

            this.receiptRepository.CreateReceipt(receiptEntity);
        }
    }
}