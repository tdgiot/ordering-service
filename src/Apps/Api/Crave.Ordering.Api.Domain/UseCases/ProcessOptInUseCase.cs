﻿using System;
using Crave.Enums;
using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Ordering.Api.Domain.UseCases
{
    public class ProcessOptInUseCase : UseCaseBase<OrderEntity>
    {
        private readonly IOptInRepository optInRepository;

        public ProcessOptInUseCase(IOptInRepository optInRepository)
        {
            this.optInRepository = optInRepository;
        }

        public override void Execute(OrderEntity request)
        {
            if (!request.OutletId.HasValue)
            {
                return;
            }

            switch (request.OptInStatus)
            {
                case OptInStatus.HardOptIn:
                    this.OptInEmail(request.CompanyId, request.OutletId.Value, request.Email, request.CustomerLastname);
                    this.OptInPhoneNumber(request.CompanyId, request.OutletId.Value, request.CustomerPhonenumber, request.CustomerLastname);
                    break;
                case OptInStatus.HardOptOut:
                    this.OptOutEmail(request.OutletId.Value, request.Email);
                    this.OptOutPhoneNumber(request.OutletId.Value, request.CustomerPhonenumber);
                    break;
            }
        }

        private void OptOutEmail(int outletId, string email)
        {
            if (email.IsNullOrWhiteSpace())
            {
                return;
            }

            this.optInRepository.DeleteByEmail(outletId, email);
        }

        private void OptOutPhoneNumber(int outletId, string phoneNumber)
        {
            if (phoneNumber.IsNullOrWhiteSpace())
            {
                return;
            }

            this.optInRepository.DeleteByPhoneNumber(outletId, phoneNumber);
        }

        private void OptInEmail(int companyId, int outletId, string email, string customerName)
        {
            if (email.IsNullOrWhiteSpace())
            {
                return;
            }

            OptInEntity entity = this.optInRepository.GetByEmail(outletId, email) ?? new OptInEntity
            {
                ParentCompanyId = companyId,
                OutletId = outletId,
                Email = email
            };

            if (entity.IsNew)
            {
                if (!customerName.IsNullOrWhiteSpace())
                {
                    entity.CustomerName = customerName;
                }

                this.optInRepository.Save(entity);
            }
        }

        private void OptInPhoneNumber(int companyId, int outletId, string phoneNumber, string customerName)
        {
            if (phoneNumber.IsNullOrWhiteSpace())
            {
                return;
            }

            OptInEntity entity = this.optInRepository.GetByPhoneNumber(outletId, phoneNumber) ?? new OptInEntity
            {
                ParentCompanyId = companyId,
                OutletId = outletId,
                PhoneNumber = phoneNumber
            };

            if (entity.IsNew)
            {
                if (!customerName.IsNullOrWhiteSpace())
                {
                    entity.CustomerName = customerName;
                }

                this.optInRepository.Save(entity);
            }
        }
    }
}
