﻿using System;
using Crave.Enums;
using Crave.Ordering.Api.Domain.UseCases.Requests;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Ordering.Api.Domain.UseCases
{
    public static class EnrichOrderWithDeliveryChargeOrderitemUseCase
    {
        private const int DECIMAL_PLACES = 2;

        public static void Execute(OrderEntity orderEntity, DeliveryChargeRequest? deliveryCharge)
        { 
            if (deliveryCharge == null || !orderEntity.Outlet.DeliveryChargeProductId.HasValue)
            {
                return;
            }

            OrderitemEntity orderitemEntity = new OrderitemEntity();
            orderitemEntity.Type = OrderitemType.DeliveryCharge;
            orderitemEntity.ProductDescription = "Delivery Charge";
            orderitemEntity.ProductId = orderEntity.Outlet.DeliveryChargeProductId.Value;
            orderitemEntity.Quantity = 1;
            orderitemEntity.ProductPriceIn = Math.Round(deliveryCharge.Price, DECIMAL_PLACES, MidpointRounding.AwayFromZero);

            orderEntity.OrderitemCollection.Add(orderitemEntity);
        }
    }
}
