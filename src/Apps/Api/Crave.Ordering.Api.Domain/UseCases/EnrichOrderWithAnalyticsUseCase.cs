﻿using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Ordering.Api.Domain.UseCases
{
    public class EnrichOrderWithAnalyticsUseCase : UseCaseBase<OrderEntity>
    {
        public override void Execute(OrderEntity request)
        {
            OrderEntity orderEntity = request;

            orderEntity.AnalyticsPayLoad = request.AnalyticsPayLoad;
            orderEntity.AnalyticsTrackingIds = request.AnalyticsTrackingIds;
        }
    }
}
