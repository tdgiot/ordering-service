﻿using System;
using System.Threading.Tasks;
using Crave.Enums;
using Crave.Ordering.Api.Domain.UseCases.Requests;
using Crave.Ordering.Api.Domain.UseCases.Responses;
using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Api.Domain.Exceptions;
using Crave.Ordering.Api.Domain.Interfaces.UseCases;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Enums;
using Microsoft.Extensions.Logging;
using Amazon.XRay.Recorder.Core;

namespace Crave.Ordering.Api.Domain.UseCases
{
    /// <summary>
    /// Create and save a new order.
    /// </summary>
    public class CreateAndSaveOrderUseCase : ICreateAndSaveOrderUseCase
    {
        private readonly ILogger<ICreateAndSaveOrderUseCase> logger;
        private readonly IAnalyticsProcessingTaskRepository analyticsProcessingTaskRepository;
        private readonly IValidateOrderUseCase validateOrderUseCase;
        private readonly IGetOrderNotesUseCase getOrderNotesUseCase;
        private readonly ISaveOrderUseCase saveOrderUseCase;
        private readonly ICreateOrderRouteUseCase createOrderRouteUseCase;

        /// <summary>
        /// Initializes a new instance of the <see cref="CreateAndSaveOrderUseCase"/> class.
        /// </summary>
        /// <param name="logger">Logger</param>
        /// <param name="analyticsProcessingTaskRepository">The analytics processing task repository.</param>
        /// <param name="validateOrderUseCase">The use-case for validating an AppLess order.</param>
        /// <param name="getOrderNotesUseCase">The use-case to get the order notes.</param>
        /// <param name="saveOrderUseCase">The use-case for saving the order to the database.</param>
        /// <param name="createOrderRouteUseCase">The use-case for creating a route for the order</param>
        public CreateAndSaveOrderUseCase(ILogger<ICreateAndSaveOrderUseCase> logger,
                                         IAnalyticsProcessingTaskRepository analyticsProcessingTaskRepository,
                                         IValidateOrderUseCase validateOrderUseCase,
                                         IGetOrderNotesUseCase getOrderNotesUseCase,
                                         ISaveOrderUseCase saveOrderUseCase,
                                         ICreateOrderRouteUseCase createOrderRouteUseCase)
        {
            this.logger = logger;
            this.analyticsProcessingTaskRepository = analyticsProcessingTaskRepository;
            this.validateOrderUseCase = validateOrderUseCase;
            this.getOrderNotesUseCase = getOrderNotesUseCase;
            this.saveOrderUseCase = saveOrderUseCase;
            this.createOrderRouteUseCase = createOrderRouteUseCase;
        }

        /// <inheritdoc />
        public async Task<CreateAndSaveOrderResponse> ExecuteAsync(CreateOrderRequest request)
        {
            try
            {
                ValidateOrderResponse validateOrderResponse = await validateOrderUseCase.ExecuteAsync(request);

                if (!validateOrderResponse.ValidationResponse.Valid)
                {
                    return CreateAndSaveOrderResponse.AsFailure(validateOrderResponse.ValidationResponse);
                }

                OrderEntity savedOrderEntity = await SaveOrder(validateOrderResponse.OrderEntity);

                CreateAnalyticsProcessingTask(savedOrderEntity);

                if (savedOrderEntity.Status == OrderStatus.NotRouted)
                {
                    CreateAndRouteOrder(savedOrderEntity);
                }

                return CreateAndSaveOrderResponse.AsSuccess(savedOrderEntity.Guid);
            }
            catch (CheckoutValidationException ex)
            {
                return CreateAndSaveOrderResponse.AsFailure(ex.CheckoutValidationResponse);
            }
        }

        private Task<OrderEntity> SaveOrder(OrderEntity orderEntity)
        {
            orderEntity.Notes = getOrderNotesUseCase.Execute(new GetOrderNotes { Order = orderEntity }) + orderEntity.Notes;

            return saveOrderUseCase.ExecuteAsync(orderEntity);
        }

        private void CreateAnalyticsProcessingTask(OrderEntity orderEntity)
        {
            AnalyticsProcessingTaskEntity task = new AnalyticsProcessingTaskEntity();
            task.OrderId = orderEntity.OrderId;
            task.EventAction = "Order Saved";

            task.EventLabel = orderEntity.DeliverypointId.HasValue ? $"{orderEntity.CompanyName}/{orderEntity.DeliverypointgroupName}" : "-1";

            analyticsProcessingTaskRepository.SaveAnalyticsProcessingTask(task);
        }

        private void CreateAndRouteOrder(OrderEntity orderEntity)
        {
            _ = Task.Factory.StartNew(async () =>
            {
                try
                {
                    AWSXRayRecorder.Instance.BeginSegment("CreateAndRouteOrder");

                    CreateOrderRouteResponse createOrderRouteResponse = await createOrderRouteUseCase.ExecuteAsync(new CreateOrderRoute(orderEntity.OrderId));
                    if (createOrderRouteResponse.Result != CreateOrderRouteResult.Ok)
                    {
                        logger.LogError(createOrderRouteResponse.Exception, "Order saved, but unable to be routed (OrderId={OrderId}, Status={Status}, CreateOrderRouteResult={Result})", orderEntity.OrderId, orderEntity.Status, createOrderRouteResponse.Result);
                    }
                }
                catch (Exception ex)
                {
                    logger.LogError(ex, "Exception while routing order (OrderId={OrderId}", orderEntity.OrderId);
                    AWSXRayRecorder.Instance.AddException(ex);
                }
                finally
                {
                    AWSXRayRecorder.Instance.EndSegment();
                }
            }, TaskCreationOptions.LongRunning);
        }


    }
}