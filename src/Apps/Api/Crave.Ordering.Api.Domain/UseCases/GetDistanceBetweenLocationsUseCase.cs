﻿using System.Threading.Tasks;
using Crave.Libraries.Distance;
using Crave.Libraries.Distance.Interfaces;
using Crave.Libraries.Distance.Responses;
using Crave.Ordering.Api.Domain.UseCases.Requests;

namespace Crave.Ordering.Api.Domain.UseCases
{
    public class GetDistanceBetweenLocationsUseCase : AsyncUseCaseBase<GetDistanceBetweenLocationsRequest, GetDistanceResponse>
    {
        private readonly IDistanceClient distanceClient;

        public GetDistanceBetweenLocationsUseCase(IDistanceClient distanceClient)
        {
            this.distanceClient = distanceClient;
        }

        public override Task<GetDistanceResponse> ExecuteAsync(GetDistanceBetweenLocationsRequest request)
        {
            ILocation originLocation = request.OriginLocation;
            ILocation destinationLocation = request.DestinationLocation;

            return this.distanceClient.GetDistanceAsync(originLocation, destinationLocation);
        }
    }
}
