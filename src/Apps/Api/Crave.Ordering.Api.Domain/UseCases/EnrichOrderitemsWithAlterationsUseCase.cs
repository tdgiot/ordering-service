﻿using Crave.Ordering.Api.Core.Enums;
using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Api.Domain.Exceptions;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using System.Collections.Generic;
using System.Linq;

namespace Crave.Ordering.Api.Domain.UseCases
{
    public class EnrichOrderitemsWithAlterationsUseCase
    {
        private readonly IAlterationRepository alterationRepository;

        public EnrichOrderitemsWithAlterationsUseCase(IAlterationRepository alterationRepository)
        {
            this.alterationRepository = alterationRepository;
        }

        public void Execute(EntityCollection<OrderitemEntity> orderitems)
        {
            Dictionary<int, AlterationEntity> alterationMap = FetchAlterations(orderitems)
                .ToDictionary(alteration => alteration.AlterationId, alteration => alteration);

            foreach (OrderitemEntity orderitemEntity in orderitems)
            {
                EnrichOrderitemWithAlterationsIfPresent(orderitemEntity, alterationMap);
            }
        }

        private EntityCollection<AlterationEntity> FetchAlterations(IEnumerable<OrderitemEntity> orderitemEntities)
        {
            IReadOnlyList<int> alterationIds = orderitemEntities.SelectMany(orderitem => orderitem.OrderitemAlterationitemCollection)
                                                                .Where(orderitemAlterationitem => orderitemAlterationitem.AlterationId.HasValue)
                                                                .Select(orderitemAlterationitem => orderitemAlterationitem.AlterationId.GetValueOrDefault(0))
                                                                .ToList();

            return this.alterationRepository.GetAlterations(alterationIds);
        }

        private static void EnrichOrderitemWithAlterationsIfPresent(OrderitemEntity orderitemEntity, Dictionary<int, AlterationEntity> alterationMap)
        {
            foreach (OrderitemAlterationitemEntity orderitemAlterationitemEntity in orderitemEntity.OrderitemAlterationitemCollection)
            {
                if (!orderitemAlterationitemEntity.AlterationId.HasValue)
                {
                    continue;
                }

                if (!alterationMap.TryGetValue(orderitemAlterationitemEntity.AlterationId.Value, out AlterationEntity alterationEntity))
                {
                    throw new CheckoutValidationException(ValidationErrorSubType.AlterationInvalid);
                }

                orderitemAlterationitemEntity.Alteration = alterationEntity;
                orderitemAlterationitemEntity.AlterationId = alterationEntity.AlterationId;
                orderitemAlterationitemEntity.AlterationName = alterationEntity.Name;
                orderitemAlterationitemEntity.AlterationType = alterationEntity.Type;
                orderitemAlterationitemEntity.OrderLevelEnabled = alterationEntity.OrderLevelEnabled;
            }
        }
    }
}