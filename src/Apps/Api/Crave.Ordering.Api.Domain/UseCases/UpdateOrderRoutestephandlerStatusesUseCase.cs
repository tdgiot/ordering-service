﻿using System;
using System.Threading.Tasks;
using Crave.Enums;
using Crave.Ordering.Api.Domain.Interfaces.UseCases;
using Crave.Ordering.Api.Domain.UseCases.Requests;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.Extensions;

namespace Crave.Ordering.Api.Domain.UseCases
{
    public class UpdateOrderRoutestephandlerStatusesUseCase : IUpdateOrderRoutestephandlerStatusesUseCase
    {
        public enum UpdateOrderRoutestephandlerStatusesResult
        {
            AttemptToChangeOrderRoutestephandlerWhileNotOwnerOrMaster = 201
        }

        private readonly IUpdateOrderStatusUseCase updateOrderStatusUseCase;
        private readonly IUpdateOrderRoutestephandlerUseCase updateOrderRoutestephandlerUseCase;

        public UpdateOrderRoutestephandlerStatusesUseCase(IUpdateOrderStatusUseCase updateOrderStatusUseCase,
                                                          IUpdateOrderRoutestephandlerUseCase updateOrderRoutestephandlerUseCase)
        {
            this.updateOrderStatusUseCase = updateOrderStatusUseCase;
            this.updateOrderRoutestephandlerUseCase = updateOrderRoutestephandlerUseCase;
        }

        public async Task<bool> ExecuteAsync(UpdateOrderRoutestephandlerStatuses request)
        {
            OrderEntity orderEntity = request.OrderEntity;
            OrderStatus status = request.Status;
            bool onlyWhenFailed = request.OnlyWhenFailed;
            TerminalEntity terminalEntity = request.TerminalEntity;

            if (orderEntity.IsNullOrNew() || status != OrderStatus.Processed)
            {
                return false;
            }

            // Check if the order is processed manually
            if (orderEntity.OrderRoutestephandlerCollection.Count > 0)
            {
                // Check if a orderRoutestephandler failed
                await ProcessFailedOrderRouteStephandlers(orderEntity, terminalEntity, onlyWhenFailed);
            }
            else
            {
                // The order has no route step handlers, so only update the status of the order
                await updateOrderStatusUseCase.ExecuteAsync(new UpdateOrderStatus(orderEntity.OrderId, OrderStatus.Processed));
            }

            return true;
        }

        private async Task ProcessFailedOrderRouteStephandlers(OrderEntity orderEntity, TerminalEntity terminalEntity, bool onlyWhenFailed)
        {
            // Check if a orderRoutestephandler failed
            foreach (OrderRoutestephandlerEntity orderRoutestephandlerEntity in orderEntity.OrderRoutestephandlerCollection)
            {
                if (onlyWhenFailed && orderRoutestephandlerEntity.Status != OrderRoutestephandlerStatus.Failed)
                { continue; }

                // If terminal is not orignal owner of the orderroutestephandler and the terminal is not activated as master, throw exception
                if (terminalEntity != null && orderRoutestephandlerEntity.TerminalId.HasValue && orderRoutestephandlerEntity.TerminalId.Value != terminalEntity.TerminalId && !terminalEntity.MasterTab)
                {
                    throw new CraveException(UpdateOrderRoutestephandlerStatusesResult.AttemptToChangeOrderRoutestephandlerWhileNotOwnerOrMaster, "Attempted to change OrderRoutestephandler while not being owner of the step or master. " +
                                                                                                                                                  $"OrderroutestephandlerId: {orderRoutestephandlerEntity.OrderRoutestephandlerId}");
                }

                // TODO Transaction

                // The current step failed
                orderRoutestephandlerEntity.Status = OrderRoutestephandlerStatus.Completed;
                orderRoutestephandlerEntity.ErrorCode = OrderErrorCode.ManuallyProcessed;
                orderRoutestephandlerEntity.IsDirty = true; // Just to make sure the entity is dirty
                orderRoutestephandlerEntity.Fields[(int)OrderRoutestephandlerFieldIndex.Status].IsChanged = true; // Otherwise it will not do it's logic when the status hasn't changed

                await updateOrderRoutestephandlerUseCase.ExecuteAsync(orderRoutestephandlerEntity);
                return;
            }
        }
    }
}
