﻿using Crave.Enums;
using Crave.Ordering.Api.Core.Enums;
using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Api.Domain.Exceptions;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using System.Collections.Generic;
using System.Linq;

namespace Crave.Ordering.Api.Domain.UseCases
{
    public class EnrichOrderitemsWithProductsUseCase
    {
        private readonly IProductRepository productRepository;
        private readonly IScheduleRepository scheduleRepository;

        public EnrichOrderitemsWithProductsUseCase(IProductRepository productRepository,
                                                   IScheduleRepository scheduleRepository)
        {
            this.productRepository = productRepository;
            this.scheduleRepository = scheduleRepository;
        }

        public void Execute(EntityCollection<OrderitemEntity> orderitems)
        {
            IEnumerable<ProductEntity> productEntities = FetchProducts(orderitems);

            Dictionary<int, ProductEntity> productMap = productEntities
                .ToDictionary(product => product.ProductId, product => product);

            Dictionary<int, ScheduleEntity> scheduleMap = FetchSchedules(productEntities)
                .ToDictionary(schedule => schedule.ScheduleId, schedule => schedule);

            foreach (OrderitemEntity orderitemEntity in orderitems)
            {
                EnrichOrderitemWithProductIfPresent(orderitemEntity, productMap);
                EnrichOrderitemWithScheduleIfPresent(orderitemEntity, scheduleMap);
            }
        }

        private ICollection<ProductEntity> FetchProducts(IEnumerable<OrderitemEntity> orderitemEntities)
        {
            IReadOnlyList<int> productIds = orderitemEntities.Where(orderitem => orderitem.ProductId.HasValue)
                                                             .Select(orderitem => orderitem.ProductId.GetValueOrDefault(0))
                                                             .ToList();

            return productRepository.GetProductsWithAlterationsPrefetch(productIds);
        }

        private ICollection<ScheduleEntity> FetchSchedules(IEnumerable<ProductEntity> productEntities)
        {
            IReadOnlyList<int> scheduleIds = productEntities.Where(product => product.ScheduleId.HasValue)
                                                            .Select(product => product.ScheduleId.GetValueOrDefault(0))
                                                            .ToList();

            return scheduleRepository.GetSchedules(scheduleIds);
        }

        private static void EnrichOrderitemWithProductIfPresent(OrderitemEntity orderitemEntity, Dictionary<int, ProductEntity> productMap)
        {
            if (!orderitemEntity.ProductId.HasValue)
            {
                return;
            }

            if (!productMap.TryGetValue(orderitemEntity.ProductId.Value, out ProductEntity productEntity))
            {
                throw new CheckoutValidationException(ValidationErrorSubType.ProductInvalid);
            }

            orderitemEntity.Product = productEntity;
            orderitemEntity.ProductId = productEntity.ProductId;
            orderitemEntity.Color = productEntity.Color;
            orderitemEntity.ProductName = productEntity.Name;
            orderitemEntity.TaxTariffId = productEntity.TaxTariffId;
            orderitemEntity.ExternalIdentifier = productEntity.ExternalIdentifier;

            if (orderitemEntity.Type == OrderitemType.Product)
            {
                orderitemEntity.ProductPriceIn = productEntity.PriceIn.GetValueOrDefault(0);
            }
        }

        private static void EnrichOrderitemWithScheduleIfPresent(OrderitemEntity orderitemEntity, Dictionary<int, ScheduleEntity> scheduleMap)
        {
            if (orderitemEntity.Product?.ScheduleId == null)
            {
                return;
            }

            if (!scheduleMap.TryGetValue(orderitemEntity.Product.ScheduleId.Value, out ScheduleEntity scheduleEntity))
            {
                throw new CheckoutValidationException(ValidationErrorSubType.ScheduleInvalid);
            }

            orderitemEntity.Product.Schedule = scheduleEntity;
        }
    }
}
