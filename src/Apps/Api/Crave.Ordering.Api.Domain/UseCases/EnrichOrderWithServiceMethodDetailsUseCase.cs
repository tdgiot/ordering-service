﻿using Crave.Ordering.Api.Core.Enums;
using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Api.Domain.Exceptions;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.Extensions;

namespace Crave.Ordering.Api.Domain.UseCases
{
    public class EnrichOrderWithServiceMethodDetailsUseCase
    {
        private readonly IServiceMethodRepository serviceMethodRepository;

        public EnrichOrderWithServiceMethodDetailsUseCase(IServiceMethodRepository serviceMethodRepository)
        {
            this.serviceMethodRepository = serviceMethodRepository;
        }

        public void Execute(OrderEntity orderEntity, int? serviceMethodId)
        {
            if (!serviceMethodId.HasValue)
            {
                return;
            }
        
            ServiceMethodEntity serviceMethodEntity = serviceMethodRepository.GetServiceMethod(serviceMethodId.Value);
            if (serviceMethodEntity.IsNullOrNew())
            {
                throw new CheckoutValidationException(ValidationErrorSubType.ServiceMethodInvalid);
            }

            orderEntity.ServiceMethod = serviceMethodEntity;
            orderEntity.ServiceMethodId = serviceMethodEntity.ServiceMethodId;
            orderEntity.OutletId = serviceMethodEntity.OutletId;
            orderEntity.ServiceMethodName = serviceMethodEntity.Name;
            orderEntity.ServiceMethodType = serviceMethodEntity.Type;
        }
    }
}
