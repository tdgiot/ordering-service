﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Crave.Enums;
using Crave.Ordering.Api.Domain.UseCases.Requests;
using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Api.Domain.Interfaces.UseCases;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.Extensions;
using System.Text;
using Crave.Ordering.Shared.Extensions;

namespace Crave.Ordering.Api.Domain.UseCases
{
    /// <summary>
    /// Use case class which is used to get the notes for an order.
    /// </summary>
    public class GetOrderNotesUseCase : IGetOrderNotesUseCase
    {
        private readonly ICompanyRepository companyRepository;
        private readonly IDeliverypointgroupRepository deliverypointgroupRepository;
        private readonly IAlterationRepository alterationRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="GetOrderNotesUseCase"/> class.
        /// </summary>
        /// <param name="companyRepository">The company repository.</param>
        /// <param name="deliverypointgroupRepository">The delivery point group repository.</param>
        /// <param name="alterationRepository">The alteration repository.</param>
        public GetOrderNotesUseCase(ICompanyRepository companyRepository,
                                    IDeliverypointgroupRepository deliverypointgroupRepository,
                                    IAlterationRepository alterationRepository)
        {
            this.companyRepository = companyRepository;
            this.deliverypointgroupRepository = deliverypointgroupRepository;
            this.alterationRepository = alterationRepository;
        }

        /// <summary>
        /// Executes the logic of the use case by getting the order notes for the parameters of the specified request DTO.
        /// </summary>
        /// <param name="request">The <see cref="GetOrderNotes"/> instance containing the parameters to get the order notes.</param>
        /// <returns>An <see cref="OrderEntity"/> instance containing the order.</returns>
        public string Execute(GetOrderNotes request)
        {
            StringBuilder orderNotes = new StringBuilder();

            OrderEntity orderEntity = request.Order;

            if (companyRepository.GetIncludeDeliveryTimeInOrderNotes(orderEntity.CompanyId))
            {
                orderNotes.Append(GetOrderNotesWithEstimatedDeliveryTime(orderEntity));
            }

            orderNotes.Append(GetOrderNotesForOrderLevelAlterations(orderEntity));

            if (orderNotes.Length > 0)
            {
                orderNotes.Append("\n");
            }

            return orderNotes.ToString();
        }

        /// <remarks>This is a temporary method.</remarks>
        private string GetOrderNotesWithEstimatedDeliveryTime(OrderEntity orderEntity)
        {
            if (orderEntity.DeliverypointId.HasValue)
            {
                int? estimatedDeliveryTime = deliverypointgroupRepository.GetEstimatedDeliveryTimeForDeliverypoint(orderEntity.DeliverypointId.Value);
                if (estimatedDeliveryTime.HasValue)
                {
                    return $"Wait Time: {estimatedDeliveryTime.Value} minutes\n";
                }
            }

            return string.Empty;
        }

        private string GetOrderNotesForOrderLevelAlterations(OrderEntity orderEntity)
        {
            StringBuilder orderNotes = new StringBuilder();

            foreach (OrderitemEntity orderitemEntity in orderEntity.OrderitemCollection)
            {
                if (orderitemEntity.OrderitemAlterationitemCollection.Count <= 0)
                { continue; }

                IEnumerable<int> alterationIds = orderitemEntity.OrderitemAlterationitemCollection.Where(x => x.AlterationId.HasValue).Select(x => x.AlterationId.GetValueOrDefault(0)).Distinct();
                orderNotes.Append(GetOrderLevelAlterationNotes(alterationIds, orderitemEntity, orderEntity));
            }

            return orderNotes.ToString();
        }

        private string GetOrderLevelAlterationNotes(IEnumerable<int> alterationIds, OrderitemEntity orderitemEntity, OrderEntity orderEntity)
        {
            StringBuilder orderLevelAlterationNotes = new StringBuilder();

            foreach (int alterationId in alterationIds)
            {
                AlterationEntity alterationEntity = alterationRepository.GetAlteration(alterationId);

                if (alterationEntity.IsNullOrNew())
                { continue; }

                ICollection<OrderitemAlterationitemEntity> alterationitems = orderitemEntity.OrderitemAlterationitemCollection.Where(x => x.OrderLevelEnabled && x.AlterationId == alterationId).ToList();

                if (!alterationitems.Any())
                { continue; }

                bool DisplayPriceIncludingTaxes = orderEntity.DisplayPriceIncludingTaxes;
                var alterationPrice = alterationitems.Sum(x => DisplayPriceIncludingTaxes ? x.PriceTotalInTax : x.PriceTotalExTax);
                string priceValue = alterationPrice > 0 ? $" ({alterationPrice.Format(orderEntity.CultureCode)})" : string.Empty;
                string valueString = GetAlterationitemsValueString(alterationitems, alterationEntity.Type, orderEntity.CompanyId);

                if (!valueString.IsNullOrWhiteSpace())
                { orderLevelAlterationNotes.Append($"{alterationEntity.Name}: {valueString}{priceValue}\n"); }
            }

            return orderLevelAlterationNotes.ToString();
        }

        private string GetAlterationitemsValueString(IEnumerable<OrderitemAlterationitemEntity> alterationitems, AlterationType alterationType, int companyId)
        {
            StringBuilder valueString = new StringBuilder();

            foreach (OrderitemAlterationitemEntity orderitemAlterationitemEntity in alterationitems)
            {
                if (valueString.Length > 0)
                {
                    valueString.Append(", ");
                }
                valueString.Append(GetAlterationitemValueString(orderitemAlterationitemEntity, alterationType, companyId));
            }

            return valueString.ToString();
        }

        private string GetAlterationitemValueString(OrderitemAlterationitemEntity orderitemAlterationitemEntity, AlterationType alterationType, int companyId)
        {
            return alterationType switch
            {
                AlterationType.Options => orderitemAlterationitemEntity.AlterationoptionName,
                AlterationType.DateTime => GetDateTimeValueString(orderitemAlterationitemEntity, alterationType, companyId),
                AlterationType.Date => GetDateTimeValueString(orderitemAlterationitemEntity, alterationType, companyId),
                AlterationType.Numeric => orderitemAlterationitemEntity.Value,
                AlterationType.Text => orderitemAlterationitemEntity.Value,
                AlterationType.Email => orderitemAlterationitemEntity.Value,
                AlterationType.Form => $"{orderitemAlterationitemEntity.AlterationoptionName}: {orderitemAlterationitemEntity.Value}",
                _ => string.Empty
            };
        }

        private string GetDateTimeValueString(OrderitemAlterationitemEntity orderitemAlterationitemEntity, AlterationType alterationType, int companyId)
        {
            if (!orderitemAlterationitemEntity.TimeUTC.HasValue)
            { return string.Empty; }

            CompanyEntity companyEntity = companyRepository.GetCompanyEntity(companyId);

            DateTime localTime = TimeZoneInfo.ConvertTimeFromUtc(orderitemAlterationitemEntity.TimeUTC.Value, companyEntity.GetTimeZone().TimeZoneInfo);

            return alterationType == AlterationType.DateTime
                ? GetDateTimeStringByClockMode(localTime, companyEntity.ClockMode)
                : $"{localTime:d MMMM}";
        }

        private static string GetDateTimeStringByClockMode(DateTime localTime, ClockMode clockMode) =>
            clockMode switch
            {
                ClockMode.Hour24 => $"{localTime:d MMMM HH:mm}",
                ClockMode.AmPm => localTime.ToString("d MMMM hh:mm tt", CultureInfo.InvariantCulture),
                _ => string.Empty
            };
    }
}
