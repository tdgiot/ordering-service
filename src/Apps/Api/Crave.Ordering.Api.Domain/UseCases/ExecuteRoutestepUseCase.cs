﻿using Crave.Enums;
using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Api.Domain.UseCases.Requests;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Interfaces;
using Crave.Ordering.Shared.Messaging.Domain.UseCases.Requests;
using Crave.Ordering.Shared.Messaging.Interfaces.UseCases;
using Crave.Ordering.Shared.Messaging.Netmessages;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Crave.Ordering.Api.Domain.UseCases
{
    public class ExecuteRoutestepUseCase : IUseCaseAsync<ExecuteRoutestepRequest>
    {
        private readonly IOrderRoutestephandlerRepository orderRoutestephandlerRepository;
        private readonly IDeviceRepository deviceRepository;
        private readonly SetForwardedTerminalUseCase setForwardedTerminalUseCase;
        private readonly HandleNonTerminalRoutingstephandlerUseCase handleNonTerminalRoutingstephandlerUseCase;
        private readonly ISendNetmessageUseCase sendNetmessageUseCase;
        private readonly UpdateOrderRoutestephandlerUseCase updateOrderRoutestephandlerUseCase;

        public ExecuteRoutestepUseCase(IOrderRoutestephandlerRepository orderRoutestephandlerRepository,
            IDeviceRepository deviceRepository,
            SetForwardedTerminalUseCase setForwardedTerminalUseCase,
            HandleNonTerminalRoutingstephandlerUseCase handleNonTerminalRoutingstephandlerUseCase,
            ISendNetmessageUseCase sendNetmessageUseCase,
            UpdateOrderRoutestephandlerUseCase updateOrderRoutestephandlerUseCase)
        {
            this.orderRoutestephandlerRepository = orderRoutestephandlerRepository;
            this.deviceRepository = deviceRepository;
            this.setForwardedTerminalUseCase = setForwardedTerminalUseCase;
            this.handleNonTerminalRoutingstephandlerUseCase = handleNonTerminalRoutingstephandlerUseCase;
            this.sendNetmessageUseCase = sendNetmessageUseCase;
            this.updateOrderRoutestephandlerUseCase = updateOrderRoutestephandlerUseCase;
        }

        public async Task ExecuteAsync(ExecuteRoutestepRequest request)
        {
            await ExecuteStepAsync(request, GetStepsOfRoute(request));
        }

        private async Task ExecuteStepAsync(ExecuteRoutestepRequest request, IList<OrderRoutestephandlerEntity> orderRoutestephandlerCollection)
        {
            if (orderRoutestephandlerCollection.Count == 0)
            {
                // We're done here
                return;
            }

            bool completedAutomaticHandlers = false;

            // Execute order routestephandlers
            foreach (OrderRoutestephandlerEntity orderRoutestephandler in orderRoutestephandlerCollection)
            {
                bool success = await HandleRoutestephandlerAsync(orderRoutestephandler);
                if (success)
                {
                    completedAutomaticHandlers = true;
                }
            }

            if (completedAutomaticHandlers)
            {
                // Just completed an automated step, start the next available step (if there is one)
                int nextStepNumber = request.RouteStepNumber + 1;
                await ExecuteAsync(new ExecuteRoutestepRequest(request.OrderId, nextStepNumber));
            }
        }

        private IList<OrderRoutestephandlerEntity> GetStepsOfRoute(ExecuteRoutestepRequest request) => orderRoutestephandlerRepository.GetSpecificStepsOfRoute(request.OrderId, request.RouteStepNumber);

        private async Task<bool> HandleRoutestephandlerAsync(OrderRoutestephandlerEntity useableEntity)
        {
            if (useableEntity.Status != OrderRoutestephandlerStatus.WaitingToBeRetrieved)
            {
                return false;
            }

            // Set the forwarded terminal
            setForwardedTerminalUseCase.Execute(new SetForwardedTerminal { OrderRoutestephandlerEntity = useableEntity });

            // Set Timeout for Completion
            ExecuteRoutestepUseCase.SetNotificationTimeouts(useableEntity);

            // Save the modified order routestephandler
            orderRoutestephandlerRepository.SaveOrderRoutestephandler(useableEntity);

            if (useableEntity.HandlerType.IsNotHandledByTerminal())
            {
                OrderRoutestephandlerEntity resultEntity = await handleNonTerminalRoutingstephandlerUseCase.ExecuteAsync(useableEntity);

                // Update any status changes made by automatic handlers
                // This use-case also sets the next route step handler to the correct status
                await updateOrderRoutestephandlerUseCase.ExecuteAsync(resultEntity);

                if (resultEntity.Status == OrderRoutestephandlerStatus.Completed && !resultEntity.CompleteRouteOnComplete ||
                    resultEntity.Status == OrderRoutestephandlerStatus.Failed && resultEntity.ContinueOnFailure)
                {
                    return true;
                }
            }
            else
            {
                // Send message to terminal
                SendNetmessageAfterSave(useableEntity);
            }

            return false;
        }

        private static void SetNotificationTimeouts(OrderRoutestephandlerEntity useableEntity)
        {
            // Set Timeout for Completion
            if (!useableEntity.TimeoutExpiresUTC.HasValue)
            {
                useableEntity.TimeoutExpiresUTC = useableEntity.Timeout > 0 ? DateTime.UtcNow.AddMinutes(useableEntity.Timeout) : DateTime.MaxValue;
            }

            // Set Notification Timeout for Retrieval            
            if (useableEntity.RetrievalSupportNotificationTimeoutMinutes > 0 && !useableEntity.RetrievalSupportNotificationTimeoutUTC.HasValue)
            {
                useableEntity.RetrievalSupportNotificationTimeoutUTC = DateTime.UtcNow.AddMinutes(useableEntity.RetrievalSupportNotificationTimeoutMinutes);
            }

            // Set Notification Timeout for Handling
            if (useableEntity.BeingHandledSupportNotificationTimeoutMinutes > 0 && !useableEntity.BeingHandledSupportNotificationTimeoutUTC.HasValue)
            {
                useableEntity.BeingHandledSupportNotificationTimeoutUTC = DateTime.UtcNow.AddMinutes(useableEntity.BeingHandledSupportNotificationTimeoutMinutes);
            }
        }

        private void SendNetmessageAfterSave(OrderRoutestephandlerEntity useableEntity)
        {
            if (!useableEntity.TerminalId.HasValue)
            {
                return;
            }

            DeviceEntity deviceEntity = deviceRepository.GetDeviceForTerminal(useableEntity.TerminalId.Value);
            if (deviceEntity == null)
            {
                return;
            }

            NetmessageRoutestephandlerStatusUpdated message = new NetmessageRoutestephandlerStatusUpdated();
            message.OrderId = useableEntity.OrderId;
            message.OrderRoutestephandlerId = useableEntity.OrderRoutestephandlerId;
            message.OrderRoutestephandlerStatus = useableEntity.Status;

            SendNetmessage sendNetmessage = new SendNetmessage();
            sendNetmessage.Netmessage = message;
            sendNetmessage.Recipients.Identifier = deviceEntity.Identifier;

            sendNetmessageUseCase.Execute(sendNetmessage);
        }
    }
}