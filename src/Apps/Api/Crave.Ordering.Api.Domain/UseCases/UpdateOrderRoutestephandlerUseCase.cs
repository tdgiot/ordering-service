﻿using System;
using System.Collections.Generic;
using System.Linq;
using Crave.Enums;
using Crave.Ordering.Api.Domain.UseCases.Requests;
using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Api.Domain.Interfaces.UseCases;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.Extensions;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using System.Threading.Tasks;
using Crave.Ordering.Shared.Gateway.Ordering;
using Crave.Ordering.CompleteRoute.Models;

namespace Crave.Ordering.Api.Domain.UseCases
{
    /// <summary>
    /// Use case for updating an order routestephandler
    /// </summary>
    public class UpdateOrderRoutestephandlerUseCase : IUpdateOrderRoutestephandlerUseCase
    {
        private readonly IOrderRoutestephandlerRepository orderRoutestephandlerRepository;
        private readonly IRouteRepository routeRepository;
        private readonly SaveOrderRoutestephandlerUseCase saveOrderRoutestephandlerUseCase;
        private readonly SetForwardedTerminalUseCase setForwardedTerminalUseCase;   
        private readonly IUpdateOrderStatusUseCase updateOrderStatusUseCase;
        private readonly Lazy<WriteRouteUseCase> writeRouteUseCase;
        private readonly IOrderingGateway orderingGateway;

        public UpdateOrderRoutestephandlerUseCase(IOrderRoutestephandlerRepository orderRoutestephandlerRepository,
                                                  IRouteRepository routeRepository,
                                                  SaveOrderRoutestephandlerUseCase saveOrderRoutestephandlerUseCase,
                                                  SetForwardedTerminalUseCase setForwardedTerminalUseCase,
                                                  IUpdateOrderStatusUseCase updateOrderStatusUseCase,
                                                  Lazy<WriteRouteUseCase> writeRouteUseCase,
                                                  IOrderingGateway orderingGateway)
        {
            this.orderRoutestephandlerRepository = orderRoutestephandlerRepository;
            this.routeRepository = routeRepository;
            this.saveOrderRoutestephandlerUseCase = saveOrderRoutestephandlerUseCase;
            this.setForwardedTerminalUseCase = setForwardedTerminalUseCase;
            this.updateOrderStatusUseCase = updateOrderStatusUseCase;
            this.writeRouteUseCase = writeRouteUseCase;
            this.orderingGateway = orderingGateway;
        }

        /// <inheritdoc />
        public async Task<OrderRoutestephandlerEntity> ExecuteAsync(OrderRoutestephandlerEntity request)
        {
            OrderRoutestephandlerEntity orderRoutestephandlerEntity = request;

            // Update the parallel steps when a step is 'Being Handled'
            if (orderRoutestephandlerEntity.HandlerType == RoutestephandlerType.Console &&
                orderRoutestephandlerEntity.Status == OrderRoutestephandlerStatus.BeingHandled &&
                orderRoutestephandlerEntity.IsChanged(OrderRoutestephandlerFields.Status))
            {
                // Check for parallel consoles to update
                this.UpdateParallelSteps(orderRoutestephandlerEntity);
            }

            bool routeHasFinished = await HasRouteFinished(orderRoutestephandlerEntity);

            // Save current step
            this.saveOrderRoutestephandlerUseCase.Execute(orderRoutestephandlerEntity);

            if (routeHasFinished)
            {
                await orderingGateway.FireRouteCompletedEventAsync(new RouteCompletedEvent { OrderId = orderRoutestephandlerEntity.OrderId });
            }

            return orderRoutestephandlerEntity;
        }        

        private async Task<bool> HasRouteFinished(OrderRoutestephandlerEntity orderRoutestephandlerEntity)
        {
            // Check if the status changed to Completed or Failed, which means actions have to be taken.
            if (orderRoutestephandlerEntity.IsChanged(OrderRoutestephandlerFields.Status) &&
                (orderRoutestephandlerEntity.Status == OrderRoutestephandlerStatus.Completed || orderRoutestephandlerEntity.Status == OrderRoutestephandlerStatus.Failed))
            {
                EntityCollection<OrderRoutestephandlerEntity> orderroutestephandlers = orderRoutestephandlerRepository.GetStepsOfRoute(orderRoutestephandlerEntity);

                // In case of multiple handlers for one step (parallel) don't act on a failed step until all the steps have failed.
                List<OrderRoutestephandlerEntity> parallelSteps = orderroutestephandlers.Where(sh => sh.Number == orderRoutestephandlerEntity.Number)
                                                                                        .ToList();

                if (orderRoutestephandlerEntity.Status == OrderRoutestephandlerStatus.Completed ||
                    !parallelSteps.Any() ||
                    parallelSteps.All(sh => sh.Status != OrderRoutestephandlerStatus.WaitingToBeRetrieved &&
                                            sh.Status != OrderRoutestephandlerStatus.RetrievedByHandler &&
                                            sh.Status != OrderRoutestephandlerStatus.BeingHandled))
                {
                    return await ProcessStepCompletionOrFailureAsync(orderRoutestephandlerEntity, orderroutestephandlers);
                }
            }

            return false;
        }

        private async Task<bool> ProcessStepCompletionOrFailureAsync(OrderRoutestephandlerEntity useableEntity, ICollection<OrderRoutestephandlerEntity> orderRoutestephandlers)
        {
            bool hasNextStep = orderRoutestephandlers.Any(step => step.Number > useableEntity.Number);
            bool completeRouteOnComplete = useableEntity.CompleteRouteOnComplete;
            bool continueOnFailure = useableEntity.ContinueOnFailure;
            bool isManuallyProcessed = useableEntity.ErrorCode == OrderErrorCode.ManuallyProcessed;

            bool isProcessed = useableEntity.Status == OrderRoutestephandlerStatus.Completed && (!hasNextStep || completeRouteOnComplete || isManuallyProcessed);
            bool isUnprocessable = useableEntity.Status == OrderRoutestephandlerStatus.Failed && !continueOnFailure;

            bool routeHasFinished = isProcessed || isUnprocessable;

            UpdateParallelAndFutureSteps(useableEntity, orderRoutestephandlers);

            if (isProcessed)
            {
                // The order is now completed       
                UpdateOrderStatus request = new UpdateOrderStatus(useableEntity.OrderId, OrderStatus.Processed);
                await updateOrderStatusUseCase.ExecuteAsync(request);
            }

            // Check if we should mark the order as 'Failed' or that we can still escalate
            // If this step failed & it's not set to 'ContinueOnFailure', fail the whole order
            if (isUnprocessable)
            {
                if (!useableEntity.EscalationRouteId.HasValue || useableEntity.EscalationStep)
                {
                    // Mark order as unproccesed when we weren't able to escalate the order
                    UpdateOrderStatus request = new UpdateOrderStatus(useableEntity.OrderId, OrderStatus.Unprocessable);
                    request.ErrorCode = useableEntity.ErrorCode;
                    await updateOrderStatusUseCase.ExecuteAsync(request);
                }
                else
                {
                    // We have a fall-back route!
                    RouteEntity escalationRoute = this.routeRepository.GetRoute(useableEntity.EscalationRouteId.Value);

                    // Write the escalation route
                    await writeRouteUseCase.Value.ExecuteAsync(new WriteRoute(useableEntity.Order, escalationRoute, true, true));
                }
            }

            return routeHasFinished;
        }

        private void UpdateParallelAndFutureSteps(OrderRoutestephandlerEntity useableEntity, ICollection<OrderRoutestephandlerEntity> orderRoutestephandlers)
        {
            bool isStepComplete = useableEntity.Status == OrderRoutestephandlerStatus.Completed;
            bool isStepFailed = useableEntity.Status == OrderRoutestephandlerStatus.Failed;
            bool isOrderComplete = isStepComplete && useableEntity.CompleteRouteOnComplete;
            bool isManuallyProcessed = useableEntity.ErrorCode == OrderErrorCode.ManuallyProcessed;
            bool continueOnFailure = useableEntity.ContinueOnFailure;

            // Update all parallel and future steps
            foreach (OrderRoutestephandlerEntity step in orderRoutestephandlers)
            {
                if (isOrderComplete)
                {
                    // This step will complete the whole order, so all steps will be set to completed
                    CompleteStep(step, useableEntity.CompletedUTC);
                }
                else if (isStepComplete || (isStepFailed && continueOnFailure))
                {
                    if (isManuallyProcessed)
                    {
                        // The current step is being manually processed
                        // So that means the order failed because of this step
                        FailStep(step);
                    }
                    else if (step.Number == useableEntity.Number)
                    {
                        // Same level, complete now
                        CompleteStep(step, useableEntity.CompletedUTC);
                    }
                    else
                    {
                        ActivateStep(step);
                    }
                }
                else
                {
                    FailStep(step);
                }
            }
        }

        private void CompleteStep(OrderRoutestephandlerEntity step, DateTime? completedUtc)
        {
            step.CompletedUTC = completedUtc;

            UpdateStepStatus(step, OrderRoutestephandlerStatus.CompletedByOtherStep);
        }

        private void FailStep(OrderRoutestephandlerEntity step)
        {
            UpdateStepStatus(step, OrderRoutestephandlerStatus.CancelledDueToOtherStepFailure);
        }

        private void ActivateStep(OrderRoutestephandlerEntity step)
        {
            // Set the correct terminal, since it might be forwarded
            setForwardedTerminalUseCase.Execute(new SetForwardedTerminal { OrderRoutestephandlerEntity = step });

            step.TimeoutExpiresUTC = step.Timeout > 0 ? DateTime.UtcNow.AddMinutes(step.Timeout) : DateTime.MaxValue;

            UpdateStepStatus(step, OrderRoutestephandlerStatus.WaitingToBeRetrieved);
        }

        private void UpdateStepStatus(OrderRoutestephandlerEntity step, OrderRoutestephandlerStatus status)
        {
            step.Status = status;

            saveOrderRoutestephandlerUseCase.Execute(step);
        }

        private void UpdateParallelSteps(OrderRoutestephandlerEntity useableEntity)
        {
            EntityCollection<OrderRoutestephandlerEntity> orderRoutestephandlerCollection = this.orderRoutestephandlerRepository.GetAllStepsOfRoute(useableEntity.OrderId);
            foreach (OrderRoutestephandlerEntity or in orderRoutestephandlerCollection)
            {
                if ((or.OrderRoutestephandlerId == useableEntity.OrderRoutestephandlerId) || (or.Number != useableEntity.Number))
                {
                    continue;
                }

                // The order has been picked up by another console. Remove order from other consoles
                or.Status = OrderRoutestephandlerStatus.BeingHandledByOtherStep;

                this.saveOrderRoutestephandlerUseCase.Execute(or);
            }
        }
    }
}