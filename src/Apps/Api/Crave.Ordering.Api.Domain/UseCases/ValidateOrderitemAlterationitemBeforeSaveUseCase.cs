﻿using System;
using System.Linq;
using Crave.Enums;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Enums;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Crave.Ordering.Api.Domain.UseCases
{
    /// <summary>
    /// Validates an order item alteration item before save.
    /// </summary>
    public class ValidateOrderitemAlterationitemBeforeSaveUseCase : UseCaseBase<OrderitemAlterationitemEntity>
    {
        /// <inheritdoc />
        public override void Execute(OrderitemAlterationitemEntity request)
        {
            ValidateIfDenormalizedFieldsAreSet(request);
            ValidateIfFieldChangeIsAllowed(request);
        }

        internal static void ValidateIfDenormalizedFieldsAreSet(OrderitemAlterationitemEntity useableEntity)
        {
            if (useableEntity.AlterationName.IsNullOrWhiteSpace())
            {
                throw new CraveException(ValidateOrderitemAlterationitemResult.AlterationNameNotSet);
            }

            switch (useableEntity.AlterationType)
            {
                case AlterationType.DateTime:
                case AlterationType.Date:
                    if (!useableEntity.TimeUTC.HasValue)
                    {
                        throw new CraveException(ValidateOrderitemAlterationitemResult.TimeNotSet);
                    }
                    break;
                case AlterationType.Form:
                    if (useableEntity.Value.IsNullOrWhiteSpace())
                    {
                        throw new CraveException(ValidateOrderitemAlterationitemResult.ValueNotSet);
                    }
                    break;
                case AlterationType.Options:
                    if (useableEntity.AlterationoptionName.IsNullOrWhiteSpace())
                    {
                        throw new CraveException(ValidateOrderitemAlterationitemResult.AlterationoptionNameNotSet);
                    }
                    break;
            }
        }

        internal static void ValidateIfFieldChangeIsAllowed(OrderitemAlterationitemEntity useableEntity)
        {
            // Don't allow OrderItemAlterationitems to be changed once saved.
            string changedField;
            if (!useableEntity.IsNew && !useableEntity.OverrideChangeProtection && IsNonAllowedFieldChanged(useableEntity, out changedField))
            {
                throw new CraveException(ValidateOrderitemAlterationitemResult.ChangeNotAllowedToExistingItem, "OrderitemAlterationitemId: {0}", useableEntity.OrderitemAlterationitemId);
            }
        }

        private static bool IsNonAllowedFieldChanged(OrderitemAlterationitemEntity orderitemAlterationitem, out string changedFieldName)
        {
            bool toReturn = false;
            changedFieldName = string.Empty;
            OrderitemAlterationitemFieldIndex[] allowedFields = FieldsAllowedToChangeWhenNotNew();

            for (int i = 0; i < orderitemAlterationitem.Fields.Count; i++)
            {
                IEntityField2 field = orderitemAlterationitem.Fields[i];
                if (field.IsChanged)
                {
                    OrderitemAlterationitemFieldIndex changedFieldIndex = field.FieldIndex.ToEnum<OrderitemAlterationitemFieldIndex>();

                    if (!allowedFields.Any(f => f == changedFieldIndex))
                    {
                        toReturn = true;
                        changedFieldName = field.Name;
                        break;
                    }
                }
            }

            return toReturn;
        }

        private static OrderitemAlterationitemFieldIndex[] FieldsAllowedToChangeWhenNotNew()
        {
            return new[]
            {
                OrderitemAlterationitemFieldIndex.AlterationitemId, // AlterationitemId can be set to null when an Alterationitem is deleted
                OrderitemAlterationitemFieldIndex.FormerAlterationitemId,
                OrderitemAlterationitemFieldIndex.AlterationType,
                OrderitemAlterationitemFieldIndex.Guid,
                OrderitemAlterationitemFieldIndex.UpdatedUTC,
                OrderitemAlterationitemFieldIndex.UpdatedBy
            };
        }
    }
}