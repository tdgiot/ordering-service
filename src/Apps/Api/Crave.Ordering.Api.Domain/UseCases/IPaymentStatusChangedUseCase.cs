﻿using Crave.Ordering.Api.Domain.UseCases.Requests;
using System.Threading.Tasks;

namespace Crave.Ordering.Api.Domain.UseCases
{
	public interface IPaymentStatusChangedUseCase
	{
		Task ExecuteAsync(PaymentStatusChanged request);
	}
}