﻿using Crave.Enums;
using Crave.Ordering.Api.Domain.UseCases.Requests;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.Extensions;
using Crave.Ordering.Shared.Enums;

namespace Crave.Ordering.Api.Domain.UseCases
{
    /// <summary>
    /// Validates an order before creating and starting a route.
    /// </summary>
    public class ValidateOrderBeforeRoutingUseCase : UseCaseBase<ValidateOrderBeforeRouting, CreateOrderRouteResult>
    {
        /// <inheritdoc />
        public override CreateOrderRouteResult Execute(ValidateOrderBeforeRouting request)
        {
            OrderEntity orderEntity = request.OrderEntity;
            if (orderEntity.IsNullOrNew())
            {
                return CreateOrderRouteResult.OrderNotFound;
            }

            if (orderEntity.Status == OrderStatus.WaitingForPayment)
            {
                return CreateOrderRouteResult.WaitingForPayment;
            }

            if (orderEntity.Status >= OrderStatus.Routed)
            {
                return CreateOrderRouteResult.OrderAlreadyRouted;
            }

            return CreateOrderRouteResult.Ok;
        }
    }
}