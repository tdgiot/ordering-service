﻿using Crave.Enums;
using Crave.Ordering.Api.Domain.UseCases.Requests;
using Crave.Ordering.Api.Domain.Interfaces.UseCases;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.Extensions;
using Crave.Ordering.Shared.OrderNotifications.Domain.UseCases.Requests;
using Crave.Ordering.Shared.OrderNotifications.Interfaces.UseCases;

namespace Crave.Ordering.Api.Domain.UseCases
{
	public class PaymentStatusChangedUseCase : IPaymentStatusChangedUseCase
	{
		private readonly ILogger<PaymentStatusChangedUseCase> logger;
        private readonly IOrderRepository orderRepository;
		private readonly IUpdateOrderStatusByGuidUseCase updateOrderStatusByGuidUseCase;
		private readonly ISendOrderNotificationUseCase sendOrderNotificationUseCase;

		public PaymentStatusChangedUseCase(ILogger<PaymentStatusChangedUseCase> logger,
										   IOrderRepository orderRepository,
                                           IUpdateOrderStatusByGuidUseCase updateOrderStatusByGuidUseCase,
                                           ISendOrderNotificationUseCase sendOrderNotificationUseCase)
		{
			this.logger = logger;
            this.orderRepository = orderRepository;
			this.updateOrderStatusByGuidUseCase = updateOrderStatusByGuidUseCase;
            this.sendOrderNotificationUseCase = sendOrderNotificationUseCase;
        }

		public async Task ExecuteAsync(PaymentStatusChanged request)
		{
			switch (request.PaymentTransactionStatus)
			{
				case PaymentTransactionStatus.Paid:
					await HandlePaidPaymentStatusChangeAsync(request);
					break;
				case PaymentTransactionStatus.Failed:
                    await HandleFailedPaymentStatusChange(request);
					break;
				case PaymentTransactionStatus.Error:
                    await HandleErrorPaymentStatusChange(request);
					break;
				case PaymentTransactionStatus.Pending:
					await HandlePendingPaymentStatusChange(request);
					break;
			}
		}

		private async Task HandlePaidPaymentStatusChangeAsync(PaymentStatusChanged request)
		{
			await StartOrderRouteAsync(request.OrderId);
		}

		private async Task HandleFailedPaymentStatusChange(PaymentStatusChanged request)
		{
            await sendOrderNotificationUseCase.ExecuteAsync(new SendOrderNotificationRequest(request.OrderId, OrderNotificationType.PaymentFailed));
		}

		private async Task HandleErrorPaymentStatusChange(PaymentStatusChanged request)
		{
            await sendOrderNotificationUseCase.ExecuteAsync(new SendOrderNotificationRequest(request.OrderId, OrderNotificationType.PaymentFailed));
		}

		private async Task HandlePendingPaymentStatusChange(PaymentStatusChanged request)
		{
            await sendOrderNotificationUseCase.ExecuteAsync(new SendOrderNotificationRequest(request.OrderId, OrderNotificationType.PaymentPending));
        }

		private async Task StartOrderRouteAsync(int orderId)
        {
            OrderEntity order = await orderRepository.GetByIdAsync(orderId);
            if (order.IsNullOrNew())
            {
				logger.LogError("No order found for order id {OrderId}", orderId);
				return;
            }

            await updateOrderStatusByGuidUseCase.ExecuteAsync(new UpdateOrderStatusByGuidRequest(order.Guid, OrderStatus.Paid));
        }
	}
}