﻿using Crave.Ordering.Api.Core.Enums;
using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Api.Domain.Exceptions;
using Crave.Ordering.Api.Domain.UseCases.Requests;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.Extensions;
using System.Diagnostics.CodeAnalysis;

namespace Crave.Ordering.Api.Domain.UseCases
{
    public class EnrichOrderWithDeliverypointDetailsUseCase
    {
        private readonly IDeliverypointRepository deliverypointRepository;

        public EnrichOrderWithDeliverypointDetailsUseCase(IDeliverypointRepository deliverypointRepository)
        {
            this.deliverypointRepository = deliverypointRepository;
        }

        public void Execute(OrderEntity orderEntity, CreateOrderRequest request)
        {
            if (!TryFetchDeliverypointEntity(request, out DeliverypointEntity? deliverypointEntity))
            {
                return;
            }
                
            orderEntity.Deliverypoint = deliverypointEntity;
            orderEntity.DeliverypointId = deliverypointEntity.DeliverypointId;
            orderEntity.CompanyId = deliverypointEntity.CompanyId;
            orderEntity.DeliverypointName = deliverypointEntity.Name;
            orderEntity.DeliverypointNumber = deliverypointEntity.Number;
            orderEntity.DeliverypointgroupName = deliverypointEntity.Deliverypointgroup.Name;
        }

        private bool TryFetchDeliverypointEntity(CreateOrderRequest request, [NotNullWhen(returnValue: true)] out DeliverypointEntity? deliverypointEntity)
        {
            if (request.DeliverypointId.HasValue)
            {
                deliverypointEntity = FetchDeliverypoint(request.DeliverypointId.Value);
                return true;
            }

            if (request.ServiceMethodDeliverypoint != null && request.ServiceMethodId.HasValue)
            {
                deliverypointEntity = FetchDeliverypoint(request.ServiceMethodDeliverypoint, request.ServiceMethodId.Value);
                return true;
            }

            deliverypointEntity = null;
            return false;
        }

        private DeliverypointEntity FetchDeliverypoint(int deliverypointId)
        {
            DeliverypointEntity deliverypointEntity = deliverypointRepository.GetDeliverypoint(deliverypointId);
            if (deliverypointEntity.IsNullOrNew())
            {
                throw new CheckoutValidationException(ValidationErrorSubType.ServiceMethodDeliveryPointInvalid);
            }

            return deliverypointEntity;
        }

        private DeliverypointEntity FetchDeliverypoint(DeliverypointRequest deliverypoint, int serviceMethodId)
        {
            DeliverypointEntity deliverypointEntity = deliverypointRepository.GetDeliverypointForServiceMethod(deliverypoint.Number, deliverypoint.DeliverypointgroupId, serviceMethodId);
            if (deliverypointEntity.IsNullOrNew())
            {
                throw new CheckoutValidationException(ValidationErrorSubType.ServiceMethodDeliveryPointInvalid);
            }

            return deliverypointEntity;
        }
    }
}
