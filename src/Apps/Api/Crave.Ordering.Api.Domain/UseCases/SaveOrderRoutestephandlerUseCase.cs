﻿using System;
using Crave.Enums;
using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.Extensions;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Messaging.Domain.UseCases.Requests;
using Crave.Ordering.Shared.Messaging.Interfaces.UseCases;
using Crave.Ordering.Shared.Messaging.Netmessages;

namespace Crave.Ordering.Api.Domain.UseCases
{
    public class SaveOrderRoutestephandlerUseCase : UseCaseBase<string, string>
    {
        private readonly IOrderRoutestephandlerRepository orderRoutestephandlerRepository;
        private readonly IDeviceRepository deviceRepository;

        private readonly ISendNetmessageUseCase sendNetmessageUseCase;

        public SaveOrderRoutestephandlerUseCase(IOrderRoutestephandlerRepository orderRoutestephandlerRepository,
                                                IDeviceRepository deviceRepository,
                                                ISendNetmessageUseCase sendNetmessageUseCase)
        {
            this.orderRoutestephandlerRepository = orderRoutestephandlerRepository;
            this.deviceRepository = deviceRepository;

            this.sendNetmessageUseCase = sendNetmessageUseCase;
        }

#pragma warning disable CS0809 // Obsolete member overrides non-obsolete member
        /// <inheritdoc />
        [Obsolete("Will throw exception, use UpdateOrderRoutestephandlerUseCase instead")]
        public override string Execute(string request)
        {
            throw new Exception("Use case can only be called internally. Use UpdateOrderRoutestephandlerUseCase instead");
        }
#pragma warning restore CS0809

        internal void Execute(OrderRoutestephandlerEntity useableEntity)
        {
            // Fill Guid when Empty
            if (useableEntity.Guid.IsNullOrWhiteSpace())
            {
                useableEntity.Guid = Guid.NewGuid().ToString();
            }

            UpdateTimestamps(useableEntity);
            SetTimeouts(useableEntity);

            // Set Error text
            try
            {
                if (useableEntity.ErrorText.IsNullOrWhiteSpace() && useableEntity.ErrorCode != OrderErrorCode.None)
                {
                    useableEntity.ErrorText = useableEntity.ErrorCode.ToString();
                }
            }
            catch
            {
                // Catch wrong errors that have no enum value.
                useableEntity.ErrorText = "Unknown Error Code";
            }

            useableEntity.StatusText = useableEntity.Status.ToString();
            useableEntity.HandlerTypeText = useableEntity.HandlerType.ToString();

            // Save the entity!
            orderRoutestephandlerRepository.SaveOrderRoutestephandler(useableEntity);

            SendOrderroutestepHandlerStatusNetmessage(useableEntity);
        }

        private static void UpdateTimestamps(OrderRoutestephandlerEntity useableEntity)
        {
            if (!useableEntity.IsChanged(OrderRoutestephandlerFields.Status))
            {
                return;
            }

            switch (useableEntity.Status)
            {
                case OrderRoutestephandlerStatus.WaitingToBeRetrieved:
                    useableEntity.WaitingToBeRetrievedUTC = DateTime.UtcNow;
                    break;
                case OrderRoutestephandlerStatus.RetrievedByHandler:
                    useableEntity.RetrievedByHandlerUTC = DateTime.UtcNow;
                    break;
                case OrderRoutestephandlerStatus.BeingHandled:
                    useableEntity.BeingHandledUTC = DateTime.UtcNow;
                    break;
                case OrderRoutestephandlerStatus.Completed:
                case OrderRoutestephandlerStatus.CompletedByOtherStep:
                case OrderRoutestephandlerStatus.CancelledDueToOtherStepFailure:
                case OrderRoutestephandlerStatus.Failed:
                    useableEntity.CompletedUTC = DateTime.UtcNow;
                    break;
            }
        }

        private void SendOrderroutestepHandlerStatusNetmessage(OrderRoutestephandlerEntity useableEntity)
        {
            // For now only to terminals & don't sent useless updates (which is, waiting for previous step)
            if (useableEntity.TerminalId.HasValue && useableEntity.Status > OrderRoutestephandlerStatus.WaitingForPreviousStep)
            {
                DeviceEntity deviceEntity = deviceRepository.GetDeviceForTerminal(useableEntity.TerminalId.Value);
                if (deviceEntity != null)
                {
                    NetmessageRoutestephandlerStatusUpdated netmessage = new NetmessageRoutestephandlerStatusUpdated();
                    netmessage.OrderId = useableEntity.OrderId;
                    netmessage.OrderRoutestephandlerId = useableEntity.OrderRoutestephandlerId;
                    netmessage.OrderRoutestephandlerStatus = useableEntity.Status;
                    netmessage.ReceiverIdentifier = deviceEntity.Identifier;

                    SendNetmessage sendNetmessage = new SendNetmessage();
                    sendNetmessage.Netmessage = netmessage;
                    sendNetmessage.Recipients.Identifier = deviceEntity.Identifier;

                    sendNetmessageUseCase.Execute(sendNetmessage);
                }
            }
        }

        private static void SetTimeouts(OrderRoutestephandlerEntity useableEntity)
        {
            // Set Timeout Expiration if not set yet (fall back for first steps that don't get activated in this Validator)
            if (useableEntity.Status <= OrderRoutestephandlerStatus.WaitingForPreviousStep)
            { return; }

            // Set Timeout for Completion
            if (!useableEntity.TimeoutExpiresUTC.HasValue)
            {
                useableEntity.TimeoutExpiresUTC = useableEntity.Timeout > 0 ? DateTime.UtcNow.AddMinutes(useableEntity.Timeout) : DateTime.MaxValue;
            }

            // Set Notification Timeout for Retrieval            
            if (useableEntity.RetrievalSupportNotificationTimeoutMinutes > 0 && !useableEntity.RetrievalSupportNotificationTimeoutUTC.HasValue)
            {
                useableEntity.RetrievalSupportNotificationTimeoutUTC = DateTime.UtcNow.AddMinutes(useableEntity.RetrievalSupportNotificationTimeoutMinutes);
            }

            // Set Notification Timeout for Handling
            if (useableEntity.BeingHandledSupportNotificationTimeoutMinutes > 0 && !useableEntity.BeingHandledSupportNotificationTimeoutUTC.HasValue)
            {
                useableEntity.BeingHandledSupportNotificationTimeoutUTC = DateTime.UtcNow.AddMinutes(useableEntity.BeingHandledSupportNotificationTimeoutMinutes);
            }
        }
    }
}