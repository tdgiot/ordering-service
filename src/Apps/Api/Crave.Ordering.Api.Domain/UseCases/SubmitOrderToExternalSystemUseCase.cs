﻿using System;
using System.Linq;
using Crave.Enums;
using Crave.Integrations.Ops.Interfaces;
using Crave.Integrations.Ops.Models;
using Crave.Integrations.Ops.Results;
using Crave.Ordering.Api.Domain.Exceptions;
using Crave.Ordering.Api.Domain.Interfaces;
using Crave.Ordering.Api.Domain.UseCases.Requests;
using Crave.Ordering.Api.Domain.UseCases.Responses;
using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using System.Threading.Tasks;

namespace Crave.Ordering.Api.Domain.UseCases
{
    public class SubmitOrderToExternalSystemUseCase : AsyncUseCaseBase<SubmitOrderToExternalSystem, SubmitOrderToExternalSystemResponse>
    {
        private readonly IExternalSystemRepository externalSystemRepository;
        private readonly IExternalDeliverypointRepository externalDeliverypointRepository;
        private readonly IExternalProductRepository externalProductRepository;
        private readonly IOpsClientFactory opsClientFactory;

        public SubmitOrderToExternalSystemUseCase(IExternalSystemRepository externalSystemRepository,
                                                  IExternalDeliverypointRepository externalDeliverypointRepository,
                                                  IExternalProductRepository externalProductRepository, 
                                                  IOpsClientFactory opsClientFactory)
        {
            this.externalSystemRepository = externalSystemRepository;
            this.externalDeliverypointRepository = externalDeliverypointRepository;
            this.externalProductRepository = externalProductRepository;
            this.opsClientFactory = opsClientFactory;
        }
        
        public override async Task<SubmitOrderToExternalSystemResponse> ExecuteAsync(SubmitOrderToExternalSystem request)
        {
            OrderEntity orderEntity = request.Order;
            ExternalSystemType externalSystemType = request.ExternalSystemType;

            try
            {
                ExternalSystemEntity externalSystemEntity = GetExternalSystemEntity(orderEntity, externalSystemType);
                ExternalDeliverypointEntity externalDeliverypointEntity = GetExternalDeliverypoint(orderEntity, externalSystemEntity);
                ExternalProductEntity externalProductEntity = GetExternalProduct(orderEntity, externalSystemEntity);

                IOpsClient opsClient = opsClientFactory.Create(externalSystemEntity);

                ExternalOrder externalOrder = new ExternalOrder
                {
                    ExternalDeliverypoint = new ExternalDeliverypoint(externalDeliverypointEntity.Id, externalDeliverypointEntity.Name), 
                    ExternalProduct = new ExternalProduct(externalProductEntity.Id), Notes = orderEntity.Notes
                };

                OpsResult<ExternalOrder> result = await opsClient.SubmitServiceOrder(externalOrder);

                return !result.Success ? SubmitOrderToExternalSystemResponse.FailResponse(result.ErrorMessage) : SubmitOrderToExternalSystemResponse.SuccessResponse();
            }
            catch (InvalidOperationException ex)
            {
                return SubmitOrderToExternalSystemResponse.FailResponse(ex.Message);
            }
            catch (EntityNotFoundException ex)
            {
                return SubmitOrderToExternalSystemResponse.FailResponse(ex.Message);
            }
        }

        private ExternalSystemEntity GetExternalSystemEntity(OrderEntity orderEntity, ExternalSystemType externalSystemType)
        {
            ExternalSystemEntity? externalSystemEntity = externalSystemRepository.GetExternalSystemForCompanyByType(orderEntity.CompanyId, externalSystemType);
            if (externalSystemEntity == null)
            {
                throw new EntityNotFoundException($"External system of type {externalSystemType} could not be found for company with id {orderEntity.CompanyId}");
            }

            return externalSystemEntity;
        }

        private ExternalDeliverypointEntity GetExternalDeliverypoint(OrderEntity orderEntity, ExternalSystemEntity externalSystemEntity)
        {
            if (!orderEntity.DeliverypointId.HasValue)
            {
                throw new InvalidOperationException("No deliverypoint id on order");
            }

            ExternalDeliverypointEntity? externalDeliverypointEntity = externalDeliverypointRepository.GetExternalDeliverypointForDeliverypointAndExternalSystem(orderEntity.DeliverypointId.Value, externalSystemEntity.ExternalSystemId);
            if (externalDeliverypointEntity == null)
            {
                throw new EntityNotFoundException($"No external deliverypoint found for deliverypoint {orderEntity.DeliverypointId} and external system {externalSystemEntity.Type}");
            }

            return externalDeliverypointEntity;
        }

        private ExternalProductEntity GetExternalProduct(OrderEntity orderEntity, ExternalSystemEntity externalSystemEntity)
        {
            OrderitemEntity? orderitemEntity = orderEntity.OrderitemCollection.FirstOrDefault(orderitem => orderitem.Type == OrderitemType.Product);
            if (orderitemEntity?.ProductId == null)
            {
                throw new InvalidOperationException("No product on order available to submit");
            }

            ExternalProductEntity? externalProductEntity = externalProductRepository.GetExternalProductForProductAndExternalSystem(orderitemEntity.ProductId.Value, externalSystemEntity.ExternalSystemId);
            if (externalProductEntity == null)
            {
                throw new EntityNotFoundException($"No external product found for product {orderitemEntity.ProductId} and external system {externalSystemEntity.Type}");
            }

            return externalProductEntity;
        }
    }
}