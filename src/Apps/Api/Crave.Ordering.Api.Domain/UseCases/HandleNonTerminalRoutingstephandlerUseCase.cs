﻿using Crave.Enums;
using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Api.Domain.UseCases.Requests;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using System;
using System.Threading.Tasks;

namespace Crave.Ordering.Api.Domain.UseCases
{
    public class HandleNonTerminalRoutingstephandlerUseCase : AsyncUseCaseBase<OrderRoutestephandlerEntity, OrderRoutestephandlerEntity>
    {
        private readonly IOrderRepository orderRepository;
        private readonly IOrderRoutestephandlerRepository orderRoutestephandlerRepository;

        private readonly HandleEmailOrderRoutestephandlerUseCase handleEmailOrderRoutestephandlerUseCase;
        private readonly HandleSmsOrderRoutestephandlerUseCase handleSmsOrderRoutestephandlerUseCase;
        private readonly HandleEmailDocumentRoutestephandlerUseCase handleEmailDocumentRoutestephandlerUseCase;
        private readonly HandleOpsServiceRoutestephandlerUseCase handleOpsServiceRoutestephandlerUseCase;
        private readonly HandleExternalSystemRoutestephandlerUseCase handleExternalSystemRoutestephandlerUseCase;

        public HandleNonTerminalRoutingstephandlerUseCase(IOrderRepository orderRepository,
                                                          IOrderRoutestephandlerRepository orderRoutestephandlerRepository,
                                                          HandleEmailOrderRoutestephandlerUseCase handleEmailOrderRoutestephandlerUseCase,
                                                          HandleSmsOrderRoutestephandlerUseCase handleSmsOrderRoutestephandlerUseCase,
                                                          HandleEmailDocumentRoutestephandlerUseCase handleEmailDocumentRoutestephandlerUseCase,
                                                          HandleOpsServiceRoutestephandlerUseCase handleOpsServiceRoutestephandlerUseCase,
                                                          HandleExternalSystemRoutestephandlerUseCase handleExternalSystemRoutestephandlerUseCase)
        {
            this.orderRepository = orderRepository;
            this.orderRoutestephandlerRepository = orderRoutestephandlerRepository;

            this.handleEmailOrderRoutestephandlerUseCase = handleEmailOrderRoutestephandlerUseCase;
            this.handleSmsOrderRoutestephandlerUseCase = handleSmsOrderRoutestephandlerUseCase;
            this.handleEmailDocumentRoutestephandlerUseCase = handleEmailDocumentRoutestephandlerUseCase;
            this.handleOpsServiceRoutestephandlerUseCase = handleOpsServiceRoutestephandlerUseCase;
            this.handleExternalSystemRoutestephandlerUseCase = handleExternalSystemRoutestephandlerUseCase;
        }

        public async override Task<OrderRoutestephandlerEntity> ExecuteAsync(OrderRoutestephandlerEntity request)
        {
            OrderRoutestephandlerEntity originalEntity = request;

            // Re-fetch entity so we have all related data prefetched
            OrderRoutestephandlerEntity entity = orderRoutestephandlerRepository.GetOrderRoutestephandlerEntity(originalEntity.OrderRoutestephandlerId, originalEntity.Adapter);
            if (entity.IsNew)
            {
                originalEntity.Status = OrderRoutestephandlerStatus.Failed;
                originalEntity.ErrorCode = OrderErrorCode.UnspecifiedError;
                originalEntity.ErrorText = $"Unable to refetch OrderRoutestephandlerEntity (OrderRoutestephandlerId: {originalEntity.OrderRoutestephandlerId})";

                return originalEntity;
            }

            OrderEntity orderEntity = await orderRepository.GetByIdWithPrefetchAsync(entity.OrderId);
            if (orderEntity.IsNew)
            {
                originalEntity.Status = OrderRoutestephandlerStatus.Failed;
                originalEntity.ErrorCode = OrderErrorCode.UnspecifiedError;
                originalEntity.ErrorText = $"Unable to fetch Order for OrderRoutestephandlerEntity (OrderId: {originalEntity.OrderId}, OrderRoutestephandlerId: {originalEntity.OrderRoutestephandlerId})";

                return originalEntity;
            }

            HandleNonTerminalRoutingstephandler handleNonTerminalRoutingstephandler = new HandleNonTerminalRoutingstephandler
            {
                OrderRoutestephandlerEntity = entity,
                OrderEntity = orderEntity
            };

            await HandleNonTerminalRoutingstepHandler(entity.HandlerType, handleNonTerminalRoutingstephandler);

            return entity;
        }

        private async Task HandleNonTerminalRoutingstepHandler(RoutestephandlerType handlerType, HandleNonTerminalRoutingstephandler request)
        {
            switch (handlerType)
            {
                case RoutestephandlerType.SMS:
                    handleSmsOrderRoutestephandlerUseCase.Execute(request);
                    break;
                case RoutestephandlerType.EmailOrder:
                    await handleEmailOrderRoutestephandlerUseCase.ExecuteAsync(request);
                    break;
                case RoutestephandlerType.HotSOS:
                case RoutestephandlerType.Quore:
                case RoutestephandlerType.Hyatt:
                case RoutestephandlerType.Alice:
                    await handleOpsServiceRoutestephandlerUseCase.ExecuteAsync(request);
                    break;
                case RoutestephandlerType.EmailDocument:
                    await handleEmailDocumentRoutestephandlerUseCase.ExecuteAsync(request);
                    break;
                case RoutestephandlerType.ExternalSystem:
                    await handleExternalSystemRoutestephandlerUseCase.Execute(request);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(handlerType), $"Routestephandler type {handlerType} is not supported/implemented");
            }
        }
    }
}