﻿using Crave.Enums;
using Crave.Ordering.Api.Domain.UseCases.Requests;
using Crave.Ordering.Pos.Deliverect.Models;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Gateway.Ordering;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Refit;
using System;
using System.Threading.Tasks;

namespace Crave.Ordering.Api.Domain.UseCases
{
    public class HandleExternalSystemRoutestephandlerUseCase
    {
        private readonly IOrderingGateway orderingGateway;
        private readonly ILogger<HandleExternalSystemRoutestephandlerUseCase> logger;

        public HandleExternalSystemRoutestephandlerUseCase(IOrderingGateway orderingGateway, ILogger<HandleExternalSystemRoutestephandlerUseCase> logger)
        {
            this.orderingGateway = orderingGateway;
            this.logger = logger;
        }

        public async Task Execute(HandleNonTerminalRoutingstephandler request)
        {
            OrderRoutestephandlerEntity orderRoutestephandlerEntity = request.OrderRoutestephandlerEntity;

            if (!orderRoutestephandlerEntity.ExternalSystemId.HasValue)
            {
                orderRoutestephandlerEntity.Status = OrderRoutestephandlerStatus.Failed;
                orderRoutestephandlerEntity.ErrorText = $"No external system configured on order routestephandler with id {orderRoutestephandlerEntity.OrderRoutestephandlerId}";
                return;
            }

            try
            {
                DispatchOrder dispatchOrderRequest = new DispatchOrder
                {
                    ExternalSystemId = orderRoutestephandlerEntity.ExternalSystemId.GetValueOrDefault(0),
                    OrderId = orderRoutestephandlerEntity.OrderId
                };

                logger.LogInformation($"{orderRoutestephandlerEntity.OrderId} dispatching order");
                await orderingGateway.DispatchOrderAsync(dispatchOrderRequest);
                logger.LogInformation($"{orderRoutestephandlerEntity.OrderId} dispatched");
            }
            catch (ValidationApiException ex) when ((int)ex.StatusCode < StatusCodes.Status500InternalServerError)
            {
                logger.LogInformation(ex.Message + ex.Content?.Detail);
            }
            catch (Exception ex)
            {
                orderRoutestephandlerEntity.Status = OrderRoutestephandlerStatus.Failed;
                orderRoutestephandlerEntity.ErrorText += $"Failed to dispatch order with id {orderRoutestephandlerEntity.OrderId} to external system. (Message={ex.Message})";
            }
        }
    }
}