﻿using System;
using System.Collections.Generic;
using Crave.Ordering.Api.Domain.UseCases.Requests;
using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using ServiceStack;
using System.Linq;

namespace Crave.Ordering.Api.Domain.UseCases
{
    public class GetPhonenumbersForSupportpoolUseCase : UseCaseBase<GetPhonenumbersForSupportpool, ICollection<string>>
    {
        private readonly ISupportpoolRepository supportpoolRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="GetPhonenumbersForSupportpoolUseCase"/> class.
        /// </summary>
        /// <param name="supportpoolRepository">Support pool repository</param>
        public GetPhonenumbersForSupportpoolUseCase(ISupportpoolRepository supportpoolRepository)
        {
            this.supportpoolRepository = supportpoolRepository;
        }

        /// <summary>
        /// Executes the use-case by getting the phone numbers for the support pool within the request DTO.
        /// </summary>
        /// <param name="request">The request DTO containing the parameters to get the phone numbers.</param>
        /// <returns>A <see cref="IEnumerable{string}"/> instance containing the phone numbers.</returns>
        public override ICollection<string> Execute(GetPhonenumbersForSupportpool request)
        {
            List<string> phoneNumbers = new List<string>();
            SupportpoolEntity supportpoolEntity = supportpoolRepository.GetSupportpoolPhonenumbers(request.SupportpoolId);

            phoneNumbers.AddRange(
                supportpoolEntity.SupportpoolSupportagentCollection
                .Select(x => x.Supportagent.Phonenumber)
                .Where(x => !x.IsNullOrWhiteSpace() && !phoneNumbers.Contains(x)));

            phoneNumbers.AddRange(
                supportpoolEntity.SupportpoolNotificationRecipientCollection
                .Select(x => x.Phonenumber)
                .Where(x => !x.IsNullOrWhiteSpace() && !phoneNumbers.Contains(x)));

            // Code below is for backwards compatibility
            if (supportpoolEntity.Phonenumber.Contains(",", StringComparison.InvariantCultureIgnoreCase))
            {
                phoneNumbers.AddRange(
                    supportpoolEntity.Phonenumber.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                    .Where(x => !x.IsNullOrWhiteSpace() && !phoneNumbers.Contains(x)));
            }
            else if (supportpoolEntity.Phonenumber.Contains(";", StringComparison.InvariantCultureIgnoreCase))
            {
                phoneNumbers.AddRange(
                    supportpoolEntity.Phonenumber.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries)
                    .Where(x => !x.IsNullOrWhiteSpace() && !phoneNumbers.Contains(x)));
            }
            else if (!supportpoolEntity.Phonenumber.IsNullOrEmpty() && !phoneNumbers.Contains(supportpoolEntity.Phonenumber))
            {
                phoneNumbers.Add(supportpoolEntity.Phonenumber);
            }

            return phoneNumbers;
        }
    }
}