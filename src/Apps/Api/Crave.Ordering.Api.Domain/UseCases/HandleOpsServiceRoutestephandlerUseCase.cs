﻿using Crave.Enums;
using Crave.Ordering.Api.Domain.Extensions;
using Crave.Ordering.Api.Domain.UseCases.Requests;
using Crave.Ordering.Api.Domain.UseCases.Responses;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using System.Threading.Tasks;

namespace Crave.Ordering.Api.Domain.UseCases
{
    public class HandleOpsServiceRoutestephandlerUseCase : IAsyncUseCase<HandleNonTerminalRoutingstephandler>
    {
        private readonly SubmitOrderToExternalSystemUseCase submitOrderToExternalSystemUseCase;

        public HandleOpsServiceRoutestephandlerUseCase(SubmitOrderToExternalSystemUseCase submitOrderToExternalSystemUseCase)
        {
            this.submitOrderToExternalSystemUseCase = submitOrderToExternalSystemUseCase;
        }

        public async Task ExecuteAsync(HandleNonTerminalRoutingstephandler request)
        {
            OrderEntity orderEntity = request.OrderEntity;
            OrderRoutestephandlerEntity orderRoutestephandlerEntity = request.OrderRoutestephandlerEntity;

            SubmitOrderToExternalSystem submitRequest = new SubmitOrderToExternalSystem(orderEntity, orderRoutestephandlerEntity.HandlerType.ToExternalSystemType());
            SubmitOrderToExternalSystemResponse response = await submitOrderToExternalSystemUseCase.ExecuteAsync(submitRequest);

            if (!response.Success)
            {
                orderRoutestephandlerEntity.Status = OrderRoutestephandlerStatus.Failed;
                orderRoutestephandlerEntity.ErrorText = response.ErrorText;
            }
            else
            {
                orderRoutestephandlerEntity.Status = OrderRoutestephandlerStatus.Completed;
            }
        }
    }
}