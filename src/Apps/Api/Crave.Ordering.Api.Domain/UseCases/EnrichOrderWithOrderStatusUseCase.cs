﻿using Crave.Enums;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Ordering.Api.Domain.UseCases
{
    public class EnrichOrderWithOrderStatusUseCase : UseCaseBase<OrderEntity>
    {
        public override void Execute(OrderEntity request)
        {
            OrderEntity orderEntity = request;

            if (orderEntity.CheckoutMethodType == CheckoutType.PaymentProvider && 
                orderEntity.Total > 0M)
            {
                orderEntity.Status = OrderStatus.WaitingForPayment;
                return;
            }
            
            orderEntity.Status = OrderStatus.NotRouted;
        }
    }
}
