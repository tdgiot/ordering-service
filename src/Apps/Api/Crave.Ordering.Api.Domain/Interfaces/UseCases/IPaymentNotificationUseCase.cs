﻿using Crave.Ordering.Api.Domain.UseCases.Requests;
using Crave.Ordering.Api.Domain.UseCases.Responses;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Crave.Ordering.Api.Domain.Interfaces.UseCases
{
	interface IPaymentNotificationUseCase
	{
		Task ExecuteAsync(PaymentStatusChanged request);
	}


}
