﻿using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Interfaces;

namespace Crave.Ordering.Api.Domain.Interfaces.UseCases
{
    public interface ISaveOrderUseCase : IUseCaseAsync<OrderEntity, OrderEntity>
    {
        
    }
}