﻿using Crave.Ordering.Api.Domain.UseCases;
using Crave.Ordering.Api.Domain.UseCases.Requests;
using Crave.Ordering.Api.Domain.UseCases.Responses;

namespace Crave.Ordering.Api.Domain.Interfaces.UseCases
{
    public interface IValidateOrderitemsUseCase : IUseCase<ValidateOrderitemsRequest, ValidationResponse>
    {
        
    }
}