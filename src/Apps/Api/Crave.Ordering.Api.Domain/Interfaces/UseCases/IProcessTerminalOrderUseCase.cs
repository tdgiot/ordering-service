﻿using Crave.Ordering.Api.Domain.UseCases;
using Crave.Ordering.Api.Domain.UseCases.Requests;

namespace Crave.Ordering.Api.Domain.Interfaces.UseCases
{
    public interface IProcessTerminalOrderUseCase : IAsyncUseCase<ProcessTerminalOrder, bool>
    {
        
    }
}