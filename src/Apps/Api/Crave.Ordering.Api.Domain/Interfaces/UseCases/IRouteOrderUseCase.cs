﻿using Crave.Ordering.Api.Domain.UseCases.Requests;
using Crave.Ordering.Shared.Interfaces;

namespace Crave.Ordering.Api.Domain.Interfaces.UseCases
{
    public interface IRouteOrderUseCase : IUseCaseAsync<RouteOrder, bool>
    {
    }
}
