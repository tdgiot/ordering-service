﻿using Crave.Ordering.Api.Domain.UseCases;
using Crave.Ordering.Api.Domain.UseCases.Requests;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Ordering.Api.Domain.Interfaces.UseCases
{
    public interface IGetClientConfigurationUseCase : IAsyncUseCase<GetClientConfiguration, ClientConfigurationEntity?>
    {
        
    }
}