﻿using Crave.Ordering.Api.Core.Models.Responses;
using Crave.Ordering.Api.Domain.UseCases;

namespace Crave.Ordering.Api.Domain.Interfaces.UseCases
{
    public interface IGetOrderForPaymentUseCase : IAsyncUseCase<string, OrderPaymentDetails?>
    {
    }
}