﻿using System.Collections.Generic;
using Crave.Ordering.Api.Domain.UseCases.Requests;
using Crave.Ordering.Api.Domain.UseCases.Responses;
using Crave.Ordering.Shared.Interfaces;

namespace Crave.Ordering.Api.Domain.Interfaces.UseCases
{
    public interface IGetOrderHistoryForTerminalUseCase : IUseCaseAsync<GetOrderHistoryForTerminal, ResponseBase<IEnumerable<Order>>>
    {
    }
}
