﻿using Crave.Integrations.Ops.Interfaces;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Ordering.Api.Domain.Interfaces
{
    public interface IOpsClientFactory
    {
        IOpsClient Create(ExternalSystemEntity externalSystemEntity);
    }
}
