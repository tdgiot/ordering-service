﻿using System.ComponentModel.DataAnnotations;

namespace Crave.Ordering.Api.Core.Models.Responses
{
	public class DistanceDto
	{
		[Required]
		public int Metres { get; set; }
	}
}
