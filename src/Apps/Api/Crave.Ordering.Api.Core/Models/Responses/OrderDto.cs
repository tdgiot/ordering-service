﻿using System;
using System.Collections.Generic;
using Crave.Enums;

namespace Crave.Ordering.Api.Core.Models.Responses
{
    public class OrderDto
    {
        public OrderType Type { get; set; }
        public int OrderId { get; set; }
        public string Guid { get; set; }
        public int CompanyId { get; set; }
        public int? OutletId { get; set; }
        public int? ClientId { get; set; }
        public bool Printed { get; set; }
        public string Notes { get; set; }
        public OrderStatus Status { get; set; }
        public DateTime? ProcessedUtc { get; set; }
        public DateTime? CreatedUtc { get; set; }
        public OrderErrorCode? ErrorCode { get; set; }
        public CheckoutType? CheckoutMethodType { get; set; }
        public bool PricesIncludeTaxes { get; set; }
        public TaxBreakdown TaxBreakdown { get; set; }
        public decimal Total { get; set; }
        public decimal SubTotal { get; set; }
        public string CurrencyCode { get; set; }
        public string CountryCode { get; set; }
        public DeliverypointDto Deliverypoint { get; set; }
        public CustomerDto Customer { get; set; }
        public DeliveryInformationDto? DeliveryInformation { get; set; }
        public ReceiptDto? Receipt { get; set; }
        public IEnumerable<OrderitemDto> Orderitems { get; set; }
        public IEnumerable<OrderRoutestephandlerDto> OrderRoutestephandlers { get; set; }
        public PaymentTransactionDto? PaymentTransaction { get; set; }
        public int? CoversCount { get; set; }
    }
}
