﻿using System;
using Crave.Enums;

namespace Crave.Ordering.Api.Core.Models.Responses
{
    public class OrderRoutestephandlerDto
    {
        public int OrderRoutestephandlerId { get; set; }
        public int? OriginatedFromRoutestepHandlerId { get; set; }
        public string Guid { get; set; }
        public int OrderId { get; set; }
        public int? TerminalId { get; set; }
        public int Number { get; set; }
        public RoutestephandlerType RoutestephandlerType { get; set; }
        public PrintReportType? PrintReportType { get; set; }
        public OrderRoutestephandlerStatus Status { get; set; }
        public OrderErrorCode ErrorCode { get; set; }
        public DateTime? CompletedUtc { get; set; }
        public int Timeout { get; set; }
        public DateTime? TimeoutExpiresUtc { get; set; }
        public int? ForwardedFromTerminalId { get; set; }
        public string FieldValue1 { get; set; }
        public string FieldValue2 { get; set; }
        public string FieldValue3 { get; set; }
        public string FieldValue4 { get; set; }
        public string FieldValue5 { get; set; }
        public string FieldValue6 { get; set; }
        public string FieldValue7 { get; set; }
        public string FieldValue8 { get; set; }
        public string FieldValue9 { get; set; }
        public string FieldValue10 { get; set; }
    }
}
