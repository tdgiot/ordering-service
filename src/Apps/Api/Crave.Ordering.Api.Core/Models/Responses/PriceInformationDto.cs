﻿using System.ComponentModel.DataAnnotations;

namespace Crave.Ordering.Api.Core.Models.Responses
{
	public class PriceInformationDto
	{
		[Required]
		public decimal Price { get; set; }

		public int? TaxTariffId { get; set; }
	}
}
