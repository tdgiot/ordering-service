﻿namespace Crave.Ordering.Api.Core.Models.Responses
{
    public class DeliveryInformationDto
    {
        public string BuildingName { get; set; }
        public string Address { get; set; }
        public string Zipcode { get; set; }
        public string City { get; set; }
        public string Instructions { get; set; }
    }
}
