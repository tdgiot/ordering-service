﻿using System.Collections.Generic;

namespace Crave.Ordering.Api.Core.Models.Responses
{
    public class OrderitemDto
    {
        public int OrderitemId { get; set; }
        public int? ProductId { get; set; }
        public int? CategoryId { get; set; }
        public int? PriceLevelItemId { get; set; }
        public int? TaxTariffId { get; set; }
        public string Guid { get; set; }
        public string ProductName { get; set; }
        public decimal ProductPriceIn { get; set; }
        public int Quantity { get; set; }
        public string Notes { get; set; }
        public int Color { get; set; }
        public string TaxName { get; set; }
        public double TaxPercentage { get; set; }
        public decimal PriceTotalExTax { get; set; }
        public decimal TaxTotal { get; set; }
        public IEnumerable<OrderitemAlterationitemDto> OrderitemAlterationitems { get; set; }
    }
}
