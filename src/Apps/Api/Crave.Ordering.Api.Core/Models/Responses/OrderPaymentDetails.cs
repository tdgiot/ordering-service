﻿using System.ComponentModel.DataAnnotations;
using Crave.Enums;

namespace Crave.Ordering.Api.Core.Models.Responses
{
    public class OrderPaymentDetails
    {
        [Required]
        public int InternalId { get; set; }

        [Required]
        public string ExternalId { get; set; }

        [Required]
        public OrderStatus Status { get; set; }

        [Required]
        public decimal Total { get; set; }

        [Required]
        public string CurrencyCode { get; set; }

        [Required]
        public string CountryCode { get; set; }

        public int? PaymentIntegrationConfigurationId { get; set; }
    }
}
