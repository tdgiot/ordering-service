﻿using System;
using Crave.Enums;

namespace Crave.Ordering.Api.Core.Models.Responses
{
    public class OrderitemAlterationitemDto
    {
        public AlterationType AlterationType { get; set; }
        public int? AlterationitemId { get; set; }
        public int? AlterationId { get; set; }
        public int? PriceLevelItemId { get; set; }
        public int? TaxTariffId { get; set; }
        public string AlterationName { get; set; }
        public string AlterationoptionName { get; set; }
        public string Value { get; set; }
        public DateTime? TimeUTC { get; set; }
        public bool OrderLevelEnabled { get; set; }
        public string TaxName { get; set; }
        public double TaxPercentage { get; set; }
        public decimal PriceTotalExTax { get; set; }
        public decimal TaxTotal { get; set; }
        public decimal AlterationoptionPriceIn { get; set; }
        public AlterationitemDto? Alterationitem { get; set; }
}
}
