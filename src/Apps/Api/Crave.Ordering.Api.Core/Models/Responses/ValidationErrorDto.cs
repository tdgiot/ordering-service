﻿using System.ComponentModel.DataAnnotations;
using Crave.Ordering.Api.Core.Enums;

namespace Crave.Ordering.Api.Core.Models.Responses
{
    public class ValidationErrorDto
    {
        [Required]
        public ValidationErrorSubType ValidationErrorSubType { get; set; }

        public int? CategoryId { get; set; }

        public int? ProductId { get; set; }

        public int? AlterationId { get; set; }

        public int? AlterationOptionId { get; set; }
    }
}
