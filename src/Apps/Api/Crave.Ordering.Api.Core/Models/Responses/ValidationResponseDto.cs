﻿using System.Collections.Generic;

namespace Crave.Ordering.Api.Core.Models.Responses
{
	public class ValidationResponseDto
	{
		public bool Valid { get; set; }

		public IEnumerable<ValidationErrorDto> ValidationErrors { get; set; }
	}
}
