﻿namespace Crave.Ordering.Api.Core.Models.Responses
{
    public class AlterationitemDto
    {
        public int AlterationitemId { get; set; }
        public int AlterationId { get; set; }
        public int AlterationoptionId { get; set; }
    }
}
