﻿namespace Crave.Ordering.Api.Core.Models.Responses
{
    public class DeliverypointDto
    {
        public int? DeliverypointId { get; set; }
        public int? Number { get; set; }
        public string Name { get; set; }
    }
}
