﻿namespace Crave.Ordering.Api.Core.Models.Responses
{
    public class ReceiptDto
    {
        public int Number { get; set; }
        public string SellerName { get; set; }
        public string SellerAddress { get; set; }
        public string SellerContactInfo { get; set; }
        public string ServiceMethodName { get; set; }
        public string CheckoutMethodName { get; set; }
    }
}
