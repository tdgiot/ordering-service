﻿namespace Crave.Ordering.Api.Core.Models.Responses
{
    public class ErrorResponseDto
    {        
        public ErrorResponseDto(string message)
        {
            this.Message = message;
        }

        public string Message { get; set; }
    }
}