﻿using System.ComponentModel.DataAnnotations;
using Crave.Enums;

namespace Crave.Ordering.Api.Core.Models.Responses
{
	public class DeliveryRateResponseDto
	{
		[Required] public DeliveryRateResult Result { get; set; }

		[Required] public PriceInformationDto PriceInformation { get; set; } = new PriceInformationDto();

		[Required] public DistanceDto Distance { get; set; } = new DistanceDto();
	}
}
