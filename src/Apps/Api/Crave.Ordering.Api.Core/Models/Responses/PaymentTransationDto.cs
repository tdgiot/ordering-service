﻿namespace Crave.Ordering.Api.Core.Models.Responses
{
    public class PaymentTransactionDto
    {
        public string? CardSummary { set; get; }
        public string? PaymentMethod { get; set; }
    }
}