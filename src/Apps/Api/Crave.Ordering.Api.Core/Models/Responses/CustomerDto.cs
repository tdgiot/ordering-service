﻿namespace Crave.Ordering.Api.Core.Models.Responses
{
    public class CustomerDto
    {
        public int? CustomerId { get; set; }
        public string? Firstname { get; set; }
        public string? Lastname { get; set; }
        public string? LastnamePrefix { get; set; }
        public string? Email { get; set; }
        public string? Phonenumber { get; set; }
        public bool IsVip { get; set; }
    }
}
