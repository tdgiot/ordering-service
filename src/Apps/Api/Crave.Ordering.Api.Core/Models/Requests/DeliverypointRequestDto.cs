﻿namespace Crave.Ordering.Api.Core.Models.Requests
{
    public class DeliverypointRequestDto
    {
        public string Number { get; set; }

        public int? DeliverypointgroupId { get; set; }
    }
}
