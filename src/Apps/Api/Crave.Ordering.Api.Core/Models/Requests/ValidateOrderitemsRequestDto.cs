﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Crave.Ordering.Api.Core.Models.Requests
{
	public class ValidateOrderitemsRequestDto
	{
		[Required]
		public IEnumerable<OrderitemRequestDto> Orderitems { get; set; }

		public int CompanyId { get; set; }
	}
}
