﻿using System.ComponentModel.DataAnnotations;

namespace Crave.Ordering.Api.Core.Models.Requests
{
    public class TippingRequestDto
    {
        [Range(0, double.PositiveInfinity, ErrorMessage = "The field {0} must be greater than {1}.")]
        public decimal Price { get; set; }
    }
}
