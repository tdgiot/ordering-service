﻿using System.ComponentModel.DataAnnotations;
using Crave.Enums;

namespace Crave.Ordering.Api.Core.Models.Requests
{
    public class UpdateOrderRoutestephandlerStatusRequestDto
    {
        /// <summary>
        /// The new order route step handler status
        /// </summary>
        public OrderRoutestephandlerStatus Status { get; set; }
        
        /// <summary>
        /// Optional error code for when the route step handler has failed
        /// </summary>
        public OrderErrorCode ErrorCode { get; set; }

        /// <summary>
        /// Gets or sets the id of the terminal to update the route step handlers for.
        /// </summary>
        [Required]
        public int TerminalId { get; set; }
    }
}