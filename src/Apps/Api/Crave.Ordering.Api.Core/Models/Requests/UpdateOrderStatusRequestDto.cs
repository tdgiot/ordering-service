﻿using Crave.Enums;

namespace Crave.Ordering.Api.Core.Models.Requests
{
    public class UpdateOrderStatusRequestDto
    {
        /// <summary>
        /// The new status of the order
        /// </summary>
        public OrderStatus OrderStatus { get; set; }
    }
}