﻿using System.ComponentModel.DataAnnotations;

namespace Crave.Ordering.Api.Core.Models.Requests
{
    public class DeliveryInformationRequestDto
    {
        public string BuildingName { get; set; }
        
        [Required]
        public string Address { get; set; }

        [Required]
        public string Zipcode { get; set; }

        [Required]
        public string City { get; set; }

        public string Instructions { get; set; }

        public double Longitude { get; set; }

        public double Latitude { get; set; }

        public int DistanceToOutletInMetres { get; set; }
    }
}
