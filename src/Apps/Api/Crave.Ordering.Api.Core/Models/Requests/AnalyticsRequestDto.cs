﻿namespace Crave.Ordering.Api.Core.Models.Requests
{
    public class AnalyticsRequestDto
    {
        public string Payload { get; set; }
        
        public string TrackingIds { get; set; }
    }
}
