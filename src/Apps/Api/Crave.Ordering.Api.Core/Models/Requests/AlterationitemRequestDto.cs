﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Crave.Ordering.Api.Core.Models.Requests
{
    public class AlterationitemRequestDto
    {
        public int AlterationId { get; set; }

        public int? AlterationoptionId { get; set; }

        [Range(0, double.PositiveInfinity, ErrorMessage = "The field {0} must be greater than {1}.")]
        public decimal? Price { get; set; }

        public string? Value { get; set; }

        public DateTime? TimeUTC { get; set; }

        public int? PriceLevelItemId { get; set; }
    }
}
