﻿namespace Crave.Ordering.Api.Core.Models.Requests
{
    public class CustomerRequestDto
    {
        public string? Email { get; set; }

        public string? FirstName { get; set; }

        public string? LastName { get; set; }

        public string? LastNamePrefix { get; set; }

        public string? Phonenumber { get; set; }
    }
}
