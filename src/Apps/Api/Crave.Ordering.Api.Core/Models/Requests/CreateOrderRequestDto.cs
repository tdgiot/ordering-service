﻿using System.Collections.Generic;
using Crave.Enums;

namespace Crave.Ordering.Api.Core.Models.Requests
{
    public class CreateOrderRequestDto
    {
        public OrderType OrderType { get; set; }

        public string? Guid { get; set; }

        public int CompanyId { get; set; }

        public int? ServiceMethodId { get; set; }

        public int? CheckoutMethodId { get; set; }

        public int? DeliverypointId { get; set; }

        public int? ClientId { get; set; }

        public string? Notes { get; set; }

        public TippingRequestDto? Tipping { get; set; }

        public ServiceChargeRequestDto? ServiceCharge { get; set; }

        public DeliveryChargeRequestDto? DeliveryCharge { get; set; }

        public DeliverypointRequestDto? ServiceMethodDeliverypoint { get; set; }

        public DeliverypointRequestDto? CheckoutMethodDeliverypoint { get; set; }

        public CustomerRequestDto? Customer { get; set; }

        public DeliveryInformationRequestDto? DeliveryInformation { get; set; }

        public AnalyticsRequestDto? Analytics { get; set; }

        public IEnumerable<OrderitemRequestDto>? Orderitems { get; set; }

        public OptInStatus OptInStatus { get; set; }

        public string? OrderContext { get; set; }

        public int? CoversCount { get; set; }
    }
}
