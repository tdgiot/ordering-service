﻿using System.ComponentModel.DataAnnotations;

namespace Crave.Ordering.Api.Core.Models.Requests
{
	public class GetDeliveryRateRequestDto
	{
		[Required]
		public int ServiceMethodId { get; set; }

		[Required]
		public DeliveryLocationDto? DeliveryLocation { get; set; }

		[Required]
		[Range(0, double.PositiveInfinity, ErrorMessage = "The field {0} must be greater than {1}.")]
		public decimal TotalPrice { get; set; }
	}
}
