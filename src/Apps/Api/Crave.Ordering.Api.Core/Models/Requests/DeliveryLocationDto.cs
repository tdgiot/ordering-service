﻿using System.ComponentModel.DataAnnotations;

namespace Crave.Ordering.Api.Core.Models.Requests
{
	public class DeliveryLocationDto
	{
		[Required]
		public double Longitude { get; set; }

		[Required]
		public double Latitude { get; set; }
	}
}
