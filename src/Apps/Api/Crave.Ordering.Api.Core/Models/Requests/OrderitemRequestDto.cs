﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Crave.Enums;

namespace Crave.Ordering.Api.Core.Models.Requests
{
    public class OrderitemRequestDto
    {
        public OrderitemType Type { get; set; }

        public string? Guid { get; set; }

        public int ProductId { get; set; }

        public int? CategoryId { get; set; }

        [Range(1, double.PositiveInfinity, ErrorMessage = "The field {0} must be greater than {1}.")]
        public int Quantity { get; set; }

        [Range(0, double.PositiveInfinity, ErrorMessage = "The field {0} must be greater than {1}.")]
        public decimal? Price { get; set; }

        public int? PriceLevelItemId { get; set; }

        public string? Notes { get; set; }

        public IEnumerable<AlterationitemRequestDto>? Alterationitems { get; set; }
    }
}
