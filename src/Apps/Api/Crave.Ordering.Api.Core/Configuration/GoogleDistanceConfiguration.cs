﻿using Crave.Ordering.Shared.Configuration.Interfaces;
using System.ComponentModel.DataAnnotations;

namespace Crave.Ordering.Api.Core.Configuration
{
    public class GoogleDistanceConfiguration : IConfigurationSection
    {
        public string Name => "GoogleDistance";

        [Required]
        public string ApiKey { get; set; }
    }
}
