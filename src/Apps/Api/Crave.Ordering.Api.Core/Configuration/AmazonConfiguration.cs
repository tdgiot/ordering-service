﻿using Crave.Ordering.Shared.Configuration.Interfaces;
using System.ComponentModel.DataAnnotations;

namespace Crave.Ordering.Api.Core.Configuration
{
    public class AmazonConfiguration : IConfigurationSection
    {
        public string Name => "Amazon";

        [Required]
        public AmazonS3Configuration S3 { get; set; }
    }
}
