﻿using System.ComponentModel.DataAnnotations;

namespace Crave.Ordering.Api.Core.Configuration
{
    public class AmazonS3Configuration
    {
        [Required]
        public string BaseUrl { get; set; }
    }
}
