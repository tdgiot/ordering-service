﻿using Crave.Ordering.Shared.Configuration.Interfaces;

namespace Crave.Ordering.Api.Core.Configuration
{
    public class ApplicationInsightsConfiguration : IConfigurationSection
    {
        public string Name => "ApplicationInsights";

        public string InstrumentationKey { get; set; }
    }
}
