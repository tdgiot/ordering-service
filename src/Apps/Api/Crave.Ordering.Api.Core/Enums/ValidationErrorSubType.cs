﻿namespace Crave.Ordering.Api.Core.Enums
{
    public enum ValidationErrorSubType
    {
        OutletInvalid,
        OutletOperationalStateInvalid,
        OutletClosedDueBusinessHours,

        ServiceMethodInvalid,
        ServiceMethodNotActive,

        ServiceChargeRequired,
        ServiceChargeNotRequired,
        ServiceChargeInvalid,

        CheckoutMethodInvalid,
        CheckoutMethodNotActive,

        CustomerLastNameInvalid,
        CustomerEmailInvalid,
        CustomerEmailRequired,
        CustomerPhoneInvalid,
        CustomerPhoneRequired,

        TipRequired,
        TipNotRequired,
        TipInvalidMinimum,

        ServiceMethodDeliveryPointRequired,
        ServiceMethodDeliveryPointNotRequired,
        ServiceMethodDeliveryPointInvalid,

        CheckoutMethodDeliveryPointRequired,
        CheckoutMethodDeliveryPointNotRequired,
        CheckoutMethodDeliveryPointInvalid,

        ProductInvalid,
        ProductPriceInvalid,
        ProductOrderingDisabled,
        ProductNotAvailable,

        AlterationInvalid,

        OrderIntakeDisabled,

        DeliveryInformationRequired,
        DeliveryInformationNotRequired,
        DeliveryInformationAddressMissing,
        DeliveryInformationZipCodeMissing,
        DeliveryInformationCityMissing,

        CompanyInvalid,

        CategoryInvalid,

        AlterationitemInvalid,
        AlterationitemPriceInvalid,
        AlterationitemTimeInvalid,

        CheckoutMethodReceiptTemplateMissing,

        DeliveryChargePriceInvalid,
        DeliveryLocationOutsideDeliveryArea,
        NoDeliveryDistanceSpecifiedForServiceMethod,
        UnableToRetrieveDistanceBetweenOutletAndDeliveryLocation,

        EatOutToHelpOutDiscountNotExpected,
        EatOutToHelpOutDiscountExpected,
        EatOutToHelpOutDiscountInvalid,

        ScheduleInvalid,
        OrderitemsRequired,
        ClientInvalid,
        PriceLevelItemInvalid,
        DeliverypointOrEmailRequired,

        AlterationItemOptionsInvalid,
        CoversCountNotAllowed,
        CoversCountRequired,
        InvalidCoversCountValue
    }
}
