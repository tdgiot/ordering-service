﻿using System.Threading.Tasks;
using Crave.Enums;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Ordering.Api.Data.Abstractions.Repositories
{
    /// <summary>
    /// Interface which is used to implement company repository classes.
    /// </summary>
    public interface ICompanyRepository
    {
        /// <summary>
        /// Save a company entity and re-fetch its data
        /// </summary>
        /// <param name="entity">The entity to save</param>
        void SaveCompany(CompanyEntity entity);

        /// <summary>
        /// Gets the clock mode for a company.
        /// </summary>
        /// <param name="companyId">The id of the company to get the clock mode for.</param>
        /// <returns>A <see cref="ClockMode"/> instance.</returns>
        ClockMode GetClockMode(int companyId);

        /// <summary>
        /// Gets the company entity for the specified company id.
        /// </summary>
        /// <param name="companyId">The id of the company to get.</param>
        /// <returns>A <see cref="CompanyEntity"/> instance.</returns>
        CompanyEntity GetCompanyEntity(int companyId);

        /// <summary>
        /// Gets a flag indicating whether the delivery time should be added to the order notes.
        /// </summary>
        /// <param name="companyId">The id of the company to check the include delivery time flag for.</param>
        /// <returns>True if the delivery time should be added to the order notes, False if not.</returns>
        bool GetIncludeDeliveryTimeInOrderNotes(int companyId);

        /// <summary>
        /// Gets the company location for the specified outlet id.
        /// </summary>
        /// <param name="outletId">The id of the outlet to get.</param>
        /// <returns>A <see cref="CompanyEntity"/> instance.</returns>
        Task<CompanyEntity> GetCompanyLocationForOutletAsync(int outletId);
    }
}