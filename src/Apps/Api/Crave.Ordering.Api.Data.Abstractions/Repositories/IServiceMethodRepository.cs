﻿using System.Threading.Tasks;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Ordering.Api.Data.Abstractions.Repositories
{
    /// <summary>
    /// Interface which is used to implement service method repository classes.
    /// </summary>
    public interface IServiceMethodRepository
    {
        /// <summary>
        /// Gets the service method for the specified id.
        /// </summary>
        /// <param name="serviceMethodId">The id of the service method to get.</param>
        /// <returns>A <see cref="ServiceMethodEntity"/> instance.</returns>
        ServiceMethodEntity GetServiceMethod(int serviceMethodId);

        /// <summary>
        /// Gets the service method with the delivery distances prefetch for the specified id.
        /// </summary>
        /// <param name="serviceMethodId">The id of the service method to get.</param>
        /// <returns>A <see cref="ServiceMethodEntity"/> instance.</returns>
        Task<ServiceMethodEntity> GetServiceMethodWithDeliveryDistancesAsync(int serviceMethodId);
    }
}
