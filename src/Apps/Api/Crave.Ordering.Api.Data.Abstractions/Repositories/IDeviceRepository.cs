﻿using System.Collections.Generic;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Ordering.Api.Data.Abstractions.Repositories
{
    /// <summary>
    /// Interface which is used to implement device repository classes.
    /// </summary>
    public interface IDeviceRepository
    {
        /// <summary>
        /// Gets the device for the specified identifier.
        /// </summary>
        /// <param name="identifier">The identifier of the device to get.</param>
        /// <returns>A <see cref="DeviceEntity"/> instance.</returns>
        DeviceEntity GetDevice(string identifier);

        /// <summary>
        /// Gets the device for the specified id.
        /// </summary>
        /// <param name="deviceId">The id of the device to get.</param>
        /// <returns>A <see cref="DeviceEntity"/> instance.</returns>
        DeviceEntity GetDevice(int deviceId);

        /// <summary>
        /// Gets the device for a specific client id
        /// </summary>
        /// <param name="clientId">The id of the client for which to get the device</param>
        /// <returns>A <see cref="DeviceEntity"/> model</returns>
        DeviceEntity GetDeviceForClient(int clientId);

        /// <summary>
        /// Gets the device for a specific terminal id
        /// </summary>
        /// <param name="terminalId">The id of the terminal for which to get the device</param>
        /// <returns>A <see cref="DeviceEntity"/> model</returns>
        DeviceEntity GetDeviceForTerminal(int terminalId);

        /// <summary>
        /// Gets a set of devices linked to clients for a company.
        /// </summary>
        /// <param name="companyId">The id of the company to get the devices for.</param>
        /// <returns>A <see cref="IEnumerable{DeviceEntity}"/> instance.</returns>
        IEnumerable<DeviceEntity> GetDevicesForCompanyClients(int companyId);

        /// <summary>
        /// Gets a set of devices linked to terminals for a company.
        /// </summary>
        /// <param name="companyId">The id of the company to get the devices for.</param>
        /// <returns>A <see cref="IEnumerable{DeviceEntity}"/> instance.</returns>
        IEnumerable<DeviceEntity> GetDevicesForCompanyTerminals(int companyId);

        /// <summary>
        /// Gets a set of devices for a deliverypoint.
        /// </summary>
        /// <param name="deliverypointId">The id of the deliverypoint to get the devices for.</param>
        /// <returns>A <see cref="IEnumerable{DeviceEntity}"/> instance.</returns>
        IEnumerable<DeviceEntity> GetDevicesForDeliverypoint(int deliverypointId);

        /// <summary>
        /// Gets a set of devices for a deliverypoint group.
        /// </summary>
        /// <param name="deliverypointgroupId">The id of the deliverypoint group to get the devices for.</param>
        /// <returns>A <see cref="IEnumerable{DeviceEntity}"/> instance.</returns>
        IEnumerable<DeviceEntity> GetDevicesForDeliverypointgroup(int deliverypointgroupId);

        /// <summary>
        /// Gets a set of devices for a specific customer
        /// </summary>
        /// <param name="customerId">The id of the customer</param>
        /// <returns>A <see cref="IEnumerable{DeviceEntity}"/> instance.</returns>
        IEnumerable<DeviceEntity> GetDevicesForCustomer(int customerId);

        /// <summary>
        /// Save a device entity
        /// </summary>
        /// <param name="deviceEntity">The entity to save</param>
        /// <returns>True when the entity hsa been saved successfully</returns>
        bool Save(DeviceEntity deviceEntity);
    }
}
