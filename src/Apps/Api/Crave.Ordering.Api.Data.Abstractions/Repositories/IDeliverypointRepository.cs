﻿using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Ordering.Api.Data.Abstractions.Repositories
{
    /// <summary>
    /// Interface which is used to implement delivery point repository classes.
    /// </summary>
    public interface IDeliverypointRepository
    {
        /// <summary>
        /// Gets the delivery point for the specified id.
        /// </summary>
        /// <param name="deliverypointId">The id of the delivery point to get.</param>
        /// <returns>A <see cref="DeliverypointEntity"/> instance.</returns>
        DeliverypointEntity GetDeliverypoint(int deliverypointId);

        /// <summary>
        /// Gets the delivery point for the number, deliverypoint group and service method id.
        /// </summary>
        /// <param name="number">The number of the delivery point to get.</param>
        /// <param name="deliverypointgroupId">The id of the deliverypoint group.</param>
        /// <param name="serviceMethodId">The id of the service method.</param>
        /// <returns>A <see cref="DeliverypointEntity"/> instance.</returns>
        DeliverypointEntity GetDeliverypointForServiceMethod(string number, int? deliverypointgroupId, int serviceMethodId);

        /// <summary>
        /// Gets the delivery point for the number, deliverypoint group and checkout method id.
        /// </summary>
        /// <param name="number">The number of the delivery point to get.</param>
        /// <param name="deliverypointgroupId">The id of the deliverypoint group.</param>
        /// <param name="checkoutMethodId">The id of the checkout method.</param>
        /// <returns>A <see cref="DeliverypointEntity"/> instance.</returns>
        DeliverypointEntity GetDeliverypointForCheckoutMethod(string number, int? deliverypointgroupId, int checkoutMethodId);
    }
}