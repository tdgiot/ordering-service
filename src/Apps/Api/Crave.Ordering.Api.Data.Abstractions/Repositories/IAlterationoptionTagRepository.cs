﻿using System.Collections.Generic;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;

namespace Crave.Ordering.Api.Data.Abstractions.Repositories
{
    public interface IAlterationoptionTagRepository
    {
        EntityCollection<AlterationoptionTagEntity> GetAlterationoptionTags(IReadOnlyList<int> alterationoptionIds);
    }
}
