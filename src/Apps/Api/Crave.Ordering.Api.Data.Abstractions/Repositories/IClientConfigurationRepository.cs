﻿using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using System.Threading.Tasks;

namespace Crave.Ordering.Api.Data.Abstractions.Repositories
{
    /// <summary>
    /// Interface which is used to implement client configuration repository classes.
    /// </summary>
    public interface IClientConfigurationRepository
    {
        /// <summary>
        /// Gets the client configuration for the specified id.
        /// </summary>
        /// <param name="clientConfigurationId">The id of the client configuration to get.</param>
        /// <returns>A <see cref="ClientConfigurationEntity"/> instance.</returns>
        Task<ClientConfigurationEntity> GetClientConfigurationAsync(int clientConfigurationId);
    }
}
