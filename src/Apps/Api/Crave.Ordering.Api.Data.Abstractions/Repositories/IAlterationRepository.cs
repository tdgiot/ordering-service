﻿using System.Collections.Generic;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;

namespace Crave.Ordering.Api.Data.Abstractions.Repositories
{
    /// <summary>
    /// Interface which is used to implement alteration repository classes.
    /// </summary>
    public interface IAlterationRepository
    {
        /// <summary>
        /// Gets a collection of alterations for the specified ids.
        /// </summary>
        /// <param name="alterationIds">The list of ids of the alterations to get.</param>
        /// <returns>A <see cref="EntityCollection"/> instance.</returns>
        EntityCollection<AlterationEntity> GetAlterations(IReadOnlyList<int> alterationIds);

        /// <summary>
        /// Gets the alteration for the specified alteration id.
        /// </summary>
        /// <param name="alterationId">The id of the alteration.</param>        
        /// <returns>An <see cref="AlterationEntity"/> instance.</returns>
        AlterationEntity GetAlteration(int alterationId);

        /// <summary>
        /// Gets an alteration for the specified alteration item id.
        /// </summary>
        /// <param name="alterationitemId">The id of the alteration item to get the alteration for.</param>
        /// <returns>A <see cref="AlterationEntity"/> instance.</returns>
        AlterationEntity GetAlterationForAlterationitem(int alterationitemId);

        /// <summary>
        /// Get an alteration option for the specific alteration item id
        /// </summary>
        /// <param name="alterationitemId">The id of the alteration item to get the alteration for.</param>
        /// <returns>A <see cref="AlterationoptionEntity"/> instance.</returns>
        AlterationoptionEntity GetAlterationoptionForAlterationitem(int alterationitemId);
    }
}