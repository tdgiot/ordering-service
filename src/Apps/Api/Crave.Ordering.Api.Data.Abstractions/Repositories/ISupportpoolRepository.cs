﻿using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Ordering.Api.Data.Abstractions.Repositories
{
    /// <summary>
    /// Interface which is used to implement support pool repository classes.
    /// </summary>
    public interface ISupportpoolRepository
    {
        /// <summary>
        /// Get the support pool entity for the specified support pool id.
        /// </summary>
        /// <param name="supportpoolId">The id of the support pool to get,</param>
        /// <returns>A <see cref="SupportpoolEntity"/> instance.</returns>
        SupportpoolEntity GetSupportpoolEmailAddresses(int supportpoolId);

        /// <summary>
        /// Get the support pool entity for the specified support pool id.
        /// </summary>
        /// <param name="supportpoolId">The id of the support pool to get,</param>
        /// <returns>A <see cref="SupportpoolEntity"/> instance.</returns>
        SupportpoolEntity GetSupportpoolPhonenumbers(int supportpoolId);
    }
}
