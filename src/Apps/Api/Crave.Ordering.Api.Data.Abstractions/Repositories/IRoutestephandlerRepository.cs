﻿using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;

namespace Crave.Ordering.Api.Data.Abstractions.Repositories
{
    public interface IRoutestephandlerRepository
    {
        EntityCollection<MediaEntity> GetMedia(int routestephandlerId);
    }
}