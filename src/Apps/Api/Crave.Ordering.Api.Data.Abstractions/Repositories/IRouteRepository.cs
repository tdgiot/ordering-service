﻿using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Ordering.Api.Data.Abstractions.Repositories
{
    /// <summary>
    /// Interface which is used to implement route repository classes.
    /// </summary>
    public interface IRouteRepository
    {
        /// <summary>
        /// Gets the route for the specified id.
        /// </summary>
        /// <param name="routeId">The id of the route to get.</param>
        /// <returns>A <see cref="RouteEntity"/> instance.</returns>
        RouteEntity GetRoute(int routeId);
    }
}
