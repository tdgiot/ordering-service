﻿using System.Collections.Generic;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;

namespace Crave.Ordering.Api.Data.Abstractions.Repositories
{
    /// <summary>
    /// Interface which is used to implement tax tariff repository classes.
    /// </summary>
    public interface ITaxTariffRepository
    {
        /// <summary>
        /// Gets a collection of tax tariffs for the specified ids.
        /// </summary>
        /// <param name="taxTariffIds">The list of ids of the tax tariffs to get.</param>
        /// <returns>A <see cref="EntityCollection"/> instance.</returns>
        EntityCollection<TaxTariffEntity> GetTaxTariffs(IReadOnlyList<int> taxTariffIds);

        /// <summary>
        /// Gets the specified tax tariff.
        /// </summary>
        /// <param name="taxTariffId">The id of the tax tariff.</param>
        /// <returns>A <see cref="TaxTariffEntity"/> instance.</returns>
        TaxTariffEntity GetTaxTariff(int taxTariffId);
    }
}
