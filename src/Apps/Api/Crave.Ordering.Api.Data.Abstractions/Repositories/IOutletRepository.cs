﻿using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Ordering.Api.Data.Abstractions.Repositories
{
    /// <summary>
    /// Interface which is used to implement outlet repository classes.
    /// </summary>
    public interface IOutletRepository
    {
        /// <summary>
        /// Gets the outlet for the specified id.
        /// </summary>
        /// <param name="outletId">The id of the outlet to get.</param>
        /// <returns>A <see cref="OutletEntity"/> instance.</returns>
        OutletEntity GetOutlet(int outletId);
    }
}
