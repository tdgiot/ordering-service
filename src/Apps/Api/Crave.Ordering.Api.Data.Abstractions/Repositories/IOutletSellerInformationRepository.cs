﻿using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Ordering.Api.Data.Abstractions.Repositories
{
    public interface IOutletSellerInformationRepository
    {
        OutletSellerInformationEntity FetchByOutletId(int outletId);
    }
}