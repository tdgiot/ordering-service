﻿using System.Collections.Generic;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;

namespace Crave.Ordering.Api.Data.Abstractions.Repositories
{
    /// <summary>
    /// Interface which is used to implement order item repository classes.
    /// </summary>
    public interface IOrderitemRepository
    {
        /// <summary>
        /// Gets a collection of order items using the specified ids.
        /// </summary>
        /// <param name="orderitemIds">The ids of the order items to get.</param>
        /// <returns>A <see cref="ModelCollection{OrderitemModel}"/> instance.</returns>
        EntityCollection<OrderitemEntity> GetOrderitems(IEnumerable<int> orderitemIds);

        /// <summary>
        /// Saves an order item.
        /// </summary>
        /// <param name="orderitemEntity">The order item model to save.</param>
        /// <returns>A <see cref="OrderitemEntity"/> instance.</returns>
        OrderitemEntity SaveOrderitem(OrderitemEntity orderitemEntity);
    }
}