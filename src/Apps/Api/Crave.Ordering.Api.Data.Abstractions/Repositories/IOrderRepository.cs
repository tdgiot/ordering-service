﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Crave.Enums;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;

namespace Crave.Ordering.Api.Data.Abstractions.Repositories
{
    /// <summary>
    /// Interface which is used to implement order repository classes.
    /// </summary>
    public interface IOrderRepository
    {
        /// <summary>
        /// Gets an order for a specific order id
        /// </summary>
        /// <param name="orderId">The id of the order to get.</param>
        /// <returns>A <see cref="OrderEntity"/> instance.</returns>
        Task<OrderEntity> GetByIdAsync(int orderId);

        /// <summary>
        /// Gets an order for a specific order id including prefetched data (order items, company, client, deliverypoint)
        /// </summary>
        /// <param name="orderId">The id of the order to get.</param>
        /// <returns>A <see cref="OrderEntity"/> instance.</returns>
        Task<OrderEntity> GetByIdWithPrefetchAsync(int orderId);

        /// <summary>
        /// Gets the order for the specified guid.
        /// </summary>
        /// <param name="guid">The guid of the order to get.</param>
        /// <returns>A <see cref="OrderEntity"/> instance.</returns>
        Task<OrderEntity> GetByGuidAsync(string guid);

        /// <summary>
        /// Gets an order for a specific guid including prefetched data (order items, alterations, delivery info)
        /// </summary>
        /// <param name="guid">The guid of the order to get.</param>
        /// <returns>A <see cref="OrderEntity"/> instance.</returns>
        Task<OrderEntity> GetByGuidWithPrefetchAsync(string guid);

        /// <summary>
        /// Gets an order for a specific guid including prefetched data (checkout method)
        /// </summary>
        /// <param name="guid">The guid of the order to get.</param>
        /// <returns>A <see cref="OrderEntity"/> instance.</returns>
        Task<OrderEntity> GetOrderForPaymentAsync(string guid);

        /// <summary>
        /// Gets the order for a receipt with specified id.
        /// </summary>
        /// <param name="orderId">The id of the order to get.</param>
        /// <returns>A <see cref="OrderEntity"/> instance.</returns>
        OrderEntity GetOrderForNotification(int orderId);

        /// <summary>
        /// Gets an order with order routestep handler using the specified GUID.
        /// </summary>
        /// <param name="guid">The GUID of the order to get.</param>
        /// <returns>An <see cref="OrderEntity"/> if the order was found, otherwise null.</returns>
        Task<OrderEntity> GetByGuidWithOrderRoutestephandlersAsync(string guid);

        /// <summary>
        /// Saves the specified order.
        /// </summary>
        /// <param name="orderEntity">The order model to save.</param>
        /// <returns>A <see cref="OrderEntity"/> instance which is the refetched order.</returns>
        Task<OrderEntity> SaveOrderAsync(OrderEntity orderEntity);

        /// <summary>
        /// Gets the orders for the specified client or customer.
        /// </summary>
        /// <param name="clientId">The id of the client to get the orders for.</param>
        /// <param name="customerId">The id of the customer to get the orders for.</param>
        /// <param name="orderGuid">The GUID of the order to get.</param>
        /// <returns>An <see cref="EntityCollection"/> instance.</returns>
        EntityCollection<OrderEntity> GetOrders(int clientId, int customerId, string orderGuid);

        /// <summary>
        /// Gets the orders currently pending to be handled by the specified terminal.
        /// </summary>
        /// <param name="terminalId">The id of the terminal to get the orders for.</param>
        /// <param name="orderIds">The ids of the orders to retrieve.</param>
        /// <param name="orderRoutestephandlerStatuses">The status of the order routestep handlers to retrieve.</param>
        /// <returns>An <see cref="EntityCollection{OrderEntity}"/> instance.</returns>
        EntityCollection<OrderEntity> GetPendingOrders(int terminalId, IEnumerable<int> orderIds, IEnumerable<OrderRoutestephandlerStatus> orderRoutestephandlerStatuses);

        /// <summary>
        /// Gets a set of historical orders.
        /// </summary>
        /// <param name="terminalId">The id of the terminal to get the historical orders.</param>
        /// <returns>An <see cref="EntityCollection{OrderEntity}" /> instance.</returns>
        EntityCollection<OrderEntity> GetOrdersHistory(int terminalId);
    }
}
