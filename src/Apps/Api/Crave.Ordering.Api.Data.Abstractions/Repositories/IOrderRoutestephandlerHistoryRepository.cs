﻿using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;

namespace Crave.Ordering.Api.Data.Abstractions.Repositories
{
    public interface IOrderRoutestephandlerHistoryRepository
    {
        void SaveOrderRoutestephandlerHistory(OrderRoutestephandlerHistoryEntity orderRoutestephandlerHistory);
        bool SaveOrderRoutestephandlerHistory(EntityCollection<OrderRoutestephandlerHistoryEntity> orderRoutestephandlerHistoryEntities);
    }
}