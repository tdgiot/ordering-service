﻿using System.Collections.Generic;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Ordering.Api.Data.Abstractions.Repositories
{
    /// <summary>
    /// Interface which is used to implement delivery point group repository classes.
    /// </summary>
    public interface IDeliverypointgroupRepository
    {
        /// <summary>
        /// Gets the delivery point group for the specified id.
        /// </summary>
        /// <param name="deliverypointgroupId">The id of the delivery point group to get.</param>
        /// <returns>A <see cref="DeliverypointgroupEntity"/> instance.</returns>
        DeliverypointgroupEntity GetDeliverypointgroup(int deliverypointgroupId);

        /// <summary>
        /// Gets the estimated delivery time for the specified delivery point id.
        /// </summary>
        /// <param name="deliverypointId">The id of the delivery point to get the estimated delivery time for.</param>
        /// <returns>A <see cref="int"/> instance.</returns>
        int? GetEstimatedDeliveryTimeForDeliverypoint(int deliverypointId);

        /// <summary>
        /// Get deliverypointgroups for company
        /// </summary>
        /// <param name="companyId">The company id for which to fetch deliverypoint groups</param>
        /// <returns>List of <see cref="DeliverypointgroupEntity"/></returns>
        IEnumerable<DeliverypointgroupEntity> GetDeliverypointgroupsForCompany(int companyId);

        /// <summary>
        /// Save a deliverypoint group entity
        /// </summary>
        /// <param name="entity">The deliverypoint group entity to save</param>
        /// <returns>True when saved</returns>
        bool Save(DeliverypointgroupEntity entity);
    }
}