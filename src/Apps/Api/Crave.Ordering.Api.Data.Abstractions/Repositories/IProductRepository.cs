﻿using System.Collections.Generic;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;

namespace Crave.Ordering.Api.Data.Abstractions.Repositories
{
    /// <summary>
    /// Interface which is used to implement product repository classes.
    /// </summary>
    public interface IProductRepository
    {
        /// <summary>
        /// Gets a collection of product models for the specified ids.
        /// </summary>
        /// <param name="productIds">The list of ids of the products to get.</param>
        /// <returns>A <see cref="EntityCollection{ProductEntity}"/> instance.</returns>
        EntityCollection<ProductEntity> GetProducts(IReadOnlyList<int> productIds);

        /// <summary>
        /// Gets a collection of product models for the specified ids, including pre-fetched alterations. Only V2 alterations which are Visible and Available are fetched.
        /// </summary>
        /// <remarks>If you ever need this method for non-V2 alterations, you will also have to rewrite the alteration validation part in OrderitemValidator</remarks>
        /// <param name="productIds">The list of ids of the products to get.</param>
        /// <returns>A <see cref="EntityCollection{ProductEntity}"/> instance.</returns>
        EntityCollection<ProductEntity> GetProductsWithAlterationsPrefetch(IReadOnlyList<int> productIds);

        /// <summary>
        /// Gets the product for the specified id.
        /// </summary>
        /// <param name="productId">The id of the product to get.</param>
        /// <returns>A <see cref="ProductEntity"/> instance.</returns>
        ProductEntity GetProduct(int productId);
    }
}