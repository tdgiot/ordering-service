﻿using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Ordering.Api.Data.Abstractions.Repositories
{
    /// <summary>
    /// Interface which is used to implement analytics processing task repository classes.
    /// </summary>
    public interface IAnalyticsProcessingTaskRepository
    {
        /// <summary>
        /// Saves the specified analytics processing task.
        /// </summary>
        /// <param name="analyticsProcessingTask">The analyticsp rocessing task to save.</param>
        /// <returns>A <see cref="bool"/> instance.</returns>
        bool SaveAnalyticsProcessingTask(AnalyticsProcessingTaskEntity analyticsProcessingTaskEntity);
    }
}