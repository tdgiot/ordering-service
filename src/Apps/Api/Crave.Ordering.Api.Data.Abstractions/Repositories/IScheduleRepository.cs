﻿using System.Collections.Generic;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;

namespace Crave.Ordering.Api.Data.Abstractions.Repositories
{
    /// <summary>
    /// Interface which is used to implement schedule repository classes.
    /// </summary>
    public interface IScheduleRepository
    {
        /// <summary>
        /// Gets a collection of schedules for the specified ids.
        /// </summary>
        /// <param name="scheduleIds">The list of ids of the schedules to get.</param>
        /// <returns>A <see cref="EntityCollection"/> instance.</returns>
        EntityCollection<ScheduleEntity> GetSchedules(IReadOnlyList<int> scheduleIds);

        /// <summary>
        /// Gets the schedule for the specified id.
        /// </summary>
        /// <param name="scheduleId">The id of the schedule to get.</param>
        /// <returns>A <see cref="ScheduleEntity"/> instance.</returns>
        ScheduleEntity GetSchedule(int scheduleId);
    }
}
