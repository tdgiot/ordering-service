﻿using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Ordering.Api.Data.Abstractions.Repositories
{
    public interface IPaymentIntegrationConfigurationRepository
    {
        PaymentIntegrationConfigurationEntity FetchById(int paymentIntegrationConfigurationId);
    }
}