﻿using System;
using System.Collections.Generic;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;

namespace Crave.Ordering.Api.Data.Abstractions.Repositories
{
    /// <summary>
    /// Interface which is used to implement alteration item repository classes.
    /// </summary>
    public interface IAlterationitemRepository
    {
        /// <summary>
        /// Gets a collection of alteration items for the specified alteration id, alteration option id combinations.
        /// </summary>
        /// <param name="alterationitems">The list of alteration items to get.</param>
        /// <returns>A <see cref="EntityCollection"/> instance.</returns>
        EntityCollection<AlterationitemEntity> GetAlterationitems(IReadOnlyList<Tuple<int, int>> alterationitems);

        /// <summary>
        /// Gets the alteration item for the specified alteration item id.
        /// </summary>
        /// <param name="alterationId">The id of the alteration item.</param>
        /// <returns>An <see cref="AlterationitemEntity"/> instance.</returns>
        AlterationitemEntity GetAlterationitem(int alterationitemId);

        /// <summary>
        /// Gets the alteration item for the specified alteration and alteration option id.
        /// </summary>
        /// <param name="alterationId">The id of the alteration.</param>
        /// <param name="alterationoptionId">The id of the alteration.</param>
        /// <returns>An <see cref="AlterationitemEntity"/> instance.</returns>
        AlterationitemEntity GetAlterationitem(int alterationId, int alterationoptionId);
    }
}