﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Crave.Enums;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Crave.Ordering.Api.Data.Abstractions.Repositories
{
    /// <summary>
    /// Interface which is used to implement order routestep handler repository classes.
    /// </summary>
    public interface IOrderRoutestephandlerRepository
    {
        /// <summary>
        /// Saves the specified order routestep handler.
        /// </summary>
        /// <param name="orderRoutestephandlerEntity">The order routestep handler model to save.</param>
        void SaveOrderRoutestephandler(OrderRoutestephandlerEntity orderRoutestephandlerEntity);

        /// <summary>
        /// Deletes the specified order routestep handlers.
        /// </summary>
        /// <param name="orderRoutestephandlerEntities">The order routestep handlers to delete.</param>
        void DeleteOrderRoutestephandlers(EntityCollection<OrderRoutestephandlerEntity> orderRoutestephandlerEntities);

        /// <summary>
        /// Gets the relevent steps of the route for the specified order routestep handler.
        /// </summary>
        /// <param name="orderRoutestephandlerEntity">The order routestep handler model to get the route steps for.</param>
        /// <returns>A <see cref="EntityCollection"/> instance.</returns>
        EntityCollection<OrderRoutestephandlerEntity> GetStepsOfRoute(OrderRoutestephandlerEntity orderRoutestephandlerEntity);

        /// <summary>
        /// Gets all of the steps of the route for the specified order routestep handler.
        /// </summary>
        /// <param name="orderId">The id of the order to get all of the steps for.</param>
        /// <returns>A <see cref="EntityCollection{OrderRoutestephandlerModel}"/> instance.</returns>
        EntityCollection<OrderRoutestephandlerEntity> GetAllStepsOfRoute(int orderId);

        /// <summary>
        /// Gets all of the steps of the route for the specified order routestep handler.
        /// </summary>
        /// <param name="orderId">The id of the order to get all of the steps for.</param>
        /// <param name="stepNumber">The number of the step</param>
        /// <returns>A <see cref="EntityCollection{OrderRoutestephandlerModel}"/> instance.</returns>
        EntityCollection<OrderRoutestephandlerEntity> GetSpecificStepsOfRoute(int orderId, int stepNumber);

        /// <summary>
        /// Get a specific order routestephandler with related data prefetched
        /// </summary>
        /// <param name="orderRoutestephandlerId">The id of the order routestep handler.</param>
        /// <param name="adapter">The adapter to use.</param>
        /// <returns>A <see cref="OrderRoutestephandlerEntity"/></returns>
        OrderRoutestephandlerEntity GetOrderRoutestephandlerEntity(int orderRoutestephandlerId, IDataAccessAdapter adapter);

        /// <summary>
        /// Get a specific order routestephandler by id
        /// </summary>
        /// <param name="orderRoutestephandlerId">The id of the order routestep handler.</param>
        /// <returns>A <see cref="OrderRoutestephandlerEntity"/></returns>
        Task<OrderRoutestephandlerEntity> GetByIdAsync(int orderRoutestephandlerId);

        /// <summary>
        /// Gets a set of order routestep handlers.
        /// </summary>
        /// <param name="orderRoutestephandlerStatuses"></param>
        /// <param name="terminalId"></param>
        /// <returns>An <see cref="ICollection{OrderRoutestephandlerEntity}"/> instance.</returns>
        ICollection<OrderRoutestephandlerEntity> GetOrderRoutestephandlers(List<OrderRoutestephandlerStatus> orderRoutestephandlerStatuses, int terminalId);

        /// <summary>
        /// Get expired orders for a specific terminal
        /// </summary>
        /// <param name="terminalId">Optional terminal id</param>
        /// <param name="orderIds">Optional list of order ids</param>
        /// <returns>A collection of order routestephandlers.</returns>
        EntityCollection<OrderRoutestephandlerEntity> GetExpiredOrdersForTerminal(int? terminalId, ICollection<int> orderIds);

        /// <summary>
        /// Get updatable order routestephandlers.
        /// </summary>
        /// <param name="orderRoutestephandlerId">The ids of the order routestephandler to update.</param>
        /// <returns>An <see cref="ICollection{OrderRoutestephandlerEntity}"/> instance.</returns>
        Task<OrderRoutestephandlerEntity> GetUpdatableTerminalRoutestephandlerAsync(int orderRoutestephandlerId);
    }
}
