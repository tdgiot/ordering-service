﻿using System.Threading.Tasks;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Ordering.Api.Data.Abstractions.Repositories
{
    /// <summary>
    /// Interface which is used to implement terminal repository classes.
    /// </summary>
    public interface ITerminalRepository
    {
        /// <summary>
        /// Gets the terminal for the specified device id.
        /// </summary>
        /// <param name="deviceId">The id of the device to get the terminal for.</param>
        /// <returns>A <see cref="TerminalEntity"/> instance.</returns>
        TerminalEntity GetTerminalForDevice(int deviceId);

        /// <summary>
        /// Gets the terminal for the specified id.
        /// </summary>
        /// <param name="terminalId">The id of the terminal to get.</param>
        /// <returns>A <see cref="TerminalEntity"/> instance.</returns>
        TerminalEntity GetTerminal(int terminalId);

        /// <summary>
        /// Gets the terminal for the specified id.
        /// </summary>
        /// <param name="terminalId">The id of the terminal to get.</param>
        /// <returns>A <see cref="TerminalEntity"/> instance.</returns>
        Task<TerminalEntity> GetByIdAsync(int terminalId);

        /// <summary>
        /// Saves a terminal.
        /// </summary>
        /// <param name="terminalEntity">The terminal entity to save.</param>
        /// <returns>True if the save was successful, False if not.</returns>
        void SaveTerminal(TerminalEntity terminalEntity);

        /// <summary>
        /// Gets the company id for the terminal with the specified terminal id.
        /// </summary>
        /// <param name="terminalId">The id of the terminal to get the company id for.</param>
        /// <returns>An <see cref="int"/> containing the company id.</returns>
        int GetCompanyIdForTerminalId(int terminalId);

        /// <summary>
        /// Updates the terminal loaded successfully boolean.
        /// </summary>
        /// <param name="terminalId">The terminal id to update.</param>
        /// <param name="loadedSuccessfully">True if loaded successfully, false otherwise.</param>
        /// <returns>True if updated successfully, false otherwise.</returns>
        bool UpdateLoadedSuccessfully(int terminalId, bool loadedSuccessfully);

        /// <summary>
        /// Updates the forward to terminal of a terminal.
        /// </summary>
        /// <param name="terminalId">The id of the terminal to update.</param>
        /// <param name="forwardToTerminalId">The id of the terminal to forward to.</param>
        /// <returns>A <see cref="bool"/> instance.</returns>
        bool UpdateTerminalForward(int terminalId, int forwardToTerminalId);
    }
}
