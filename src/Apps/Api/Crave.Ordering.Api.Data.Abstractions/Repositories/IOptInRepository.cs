﻿using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Ordering.Api.Data.Abstractions.Repositories
{
    public interface IOptInRepository
    {
        /// <summary>
        /// Saves the specified opt-in model.
        /// </summary>
        /// <param name="optInEntity">The opt-in model to save.</param>
        /// <returns>True, indicates the opt-in model was successfully saved.</returns>
        bool Save(OptInEntity optInEntity);

        /// <summary>
        /// Get an opt-in record.
        /// </summary>
        /// <param name="outletId">The involved outlet id.</param>
        /// <param name="email">The customers email address.</param>
        /// <returns>A <see cref="OptInEntity"/> if one exists for the specified parameters.</returns>
        OptInEntity GetByEmail(int outletId, string email);

        /// <summary>
        /// Get an opt-in record.
        /// </summary>
        /// <param name="outletId">The involved outlet id.</param>
        /// <param name="phoneNumber">The customers email address.</param>
        /// <returns>A <see cref="OptInEntity"/> if one exists for the specified parameters.</returns>
        OptInEntity GetByPhoneNumber(int outletId, string phoneNumber);

        /// <summary>
        /// Delete the specified opt-in model.
        /// </summary>
        /// <param name="outletId">The involved outlet id.</param>
        /// <param name="email">The customers email address.</param>
        /// <returns>True, indicates the opt-in record was successfully removed.</returns>
        bool DeleteByEmail(int outletId, string email);

        /// <summary>
        /// Delete the specified opt-in model.
        /// </summary>
        /// <param name="outletId">The involved outlet id.</param>
        /// <param name="phoneNumber">The customers phone number.</param>
        /// <returns>True, indicates the opt-in record was successfully removed.</returns>
        bool DeleteByPhoneNumber(int outletId, string phoneNumber);
    }
}
