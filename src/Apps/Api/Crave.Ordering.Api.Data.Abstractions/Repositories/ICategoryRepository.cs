﻿using System.Collections.Generic;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;

namespace Crave.Ordering.Api.Data.Abstractions.Repositories
{
    /// <summary>
    /// Interface which is used to implement category repository classes.
    /// </summary>
    public interface ICategoryRepository
    {
        /// <summary>
        /// Gets a collection of categories for the specified ids.
        /// </summary>
        /// <param name="categoryIds">The list of ids of the categories to get.</param>
        /// <returns>A <see cref="EntityCollection"/> instance.</returns>
        EntityCollection<CategoryEntity> GetCategories(IReadOnlyList<int> categoryIds);

        /// <summary>
        /// Gets a category.
        /// </summary>
        /// <param name="categoryId">The id of the category to get.</param>
        /// <returns>A <see cref="CategoryEntity"/> instance.</returns>
        CategoryEntity GetCategory(int categoryId);

        /// <summary>
        /// Gets a category entity with (up to) 6 parent categories prefetched which have a RouteId
        /// </summary>
        /// <param name="categoryId">The id of the category</param>
        /// <returns>a <see cref="CategoryEntity"/> instance</returns>
        CategoryEntity GetCategoryWithParentRoutes(int categoryId);
    }
}