﻿using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using System.Threading.Tasks;

namespace Crave.Ordering.Api.Data.Abstractions.Repositories
{
    /// <summary>
    /// Interface which is used to implement client repository classes.
    /// </summary>
    public interface IClientRepository
    {
        /// <summary>
        /// Gets the client for the specified device id.
        /// </summary>
        /// <param name="deviceId">The id of the device to get the client for.</param>
        /// <returns>A <see cref="ClientEntity"/> instance.</returns>
        ClientEntity GetClientForDevice(int deviceId);

        /// <summary>
        /// Gets the client for the specified id.
        /// </summary>
        /// <param name="clientId">The id of the client to get.</param>
        /// <returns>A <see cref="ClientEntity"/> instance.</returns>
        Task<ClientEntity> GetClientAsync(int clientId);

        /// <summary>
        /// Saves a client.
        /// </summary>
        /// <param name="clientEntity">The client entity to save.</param>
        /// <returns>True if saving the client was successful, False if not.</returns>
        bool SaveClient(ClientEntity clientEntity);

        /// <summary>
        /// Gets the company id for the client with the specified client id.
        /// </summary>
        /// <param name="clientId">The id of the client to get the company id for.</param>
        /// <returns>An <see cref="int"/> containing the company id.</returns>
        int GetCompanyIdForClientId(int clientId);

        /// <summary>
        /// Updates the client loaded successfully boolean.
        /// </summary>
        /// <param name="clientId">The client id to update.</param>
        /// <param name="loadedSuccessfully">True if loaded successfully, false otherwise.</param>
        /// <returns>True if updated successfully, false otherwise.</returns>
        bool UpdateLoadedSuccessfully(int clientId, bool loadedSuccessfully);
    }
}
