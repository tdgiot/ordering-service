﻿using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Ordering.Api.Data.Abstractions.Repositories
{
    /// <summary>
    /// Interface which is used to implement checkout method repository classes.
    /// </summary>
    public interface ICheckoutMethodRepository
    {
        /// <summary>
        /// Gets the checkout method for the specified id.
        /// </summary>
        /// <param name="checkoutMethodId">The id of the checkout method to get.</param>
        /// <returns>A <see cref="CheckoutMethodEntity"/> instance.</returns>
        CheckoutMethodEntity GetCheckoutMethod(int checkoutMethodId);
    }
}
