﻿namespace Crave.Ordering.Api.Data.Abstractions.Repositories
{
    public interface IMenuRepository
    {
        /// <summary>
        /// Get the name of a menu by id
        /// </summary>
        /// <param name="menuId">The id of the menu</param>
        /// <returns>Menu name as string</returns>
        string GetMenuName(int menuId);
    }
}