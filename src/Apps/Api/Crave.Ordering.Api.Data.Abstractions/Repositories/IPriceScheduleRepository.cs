﻿using System.Collections.Generic;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;

namespace Crave.Ordering.Api.Data.Abstractions.Repositories
{
    public interface IPriceScheduleRepository
    {
        /// <summary>
        /// Gets a collection of price level items for the specified ids.
        /// </summary>
        /// <param name="priceLevelItemIds">The list of ids of the price level items to get.</param>
        /// <returns>A <see cref="EntityCollection"/> instance.</returns>
        EntityCollection<PriceLevelItemEntity> GetPriceLevelItems(IReadOnlyList<int> priceLevelItemIds);

        /// <summary>
        /// Get a price level item by id
        /// </summary>
        /// <param name="priceLevelItemId">The id to fetch</param>
        /// <returns>A <see cref="PriceLevelItemEntity"/> instance.</returns>
        PriceLevelItemEntity GetPriceLevelItemEntity(int priceLevelItemId);
    }
}