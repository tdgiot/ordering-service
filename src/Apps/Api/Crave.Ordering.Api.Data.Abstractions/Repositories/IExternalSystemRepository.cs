﻿using Crave.Enums;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Ordering.Api.Data.Abstractions.Repositories
{
    /// <summary>
    /// Interface which is used to implement external system repository classes.
    /// </summary>
    public interface IExternalSystemRepository
    {
        /// <summary>
        /// Gets an external system for a company based on the type.
        /// </summary>
        /// <param name="companyId">The id of the company.</param>
        /// <param name="externalSystemType">The external system type.</param>
        /// <returns>A <see cref="ExternalSystemEntity"/> instance.</returns>
        ExternalSystemEntity GetExternalSystemForCompanyByType(int companyId, ExternalSystemType externalSystemType);

        /// <summary>
        /// Gets an external system by the specified channel link id.
        /// </summary>
        /// <param name="channelLinkId">The channel link id.</param>
        /// <returns>A <see cref="ExternalSystemEntity"/> instance.</returns>
        ExternalSystemEntity GetExternalSystemByChannelLinkId(string channelLinkId);
    }
}