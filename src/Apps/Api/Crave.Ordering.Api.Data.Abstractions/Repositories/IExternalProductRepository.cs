﻿using System.Collections.Generic;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;

namespace Crave.Ordering.Api.Data.Abstractions.Repositories
{
    /// <summary>
    /// Interface which is used to implement external product repository classes.
    /// </summary>
    public interface IExternalProductRepository
    {
        /// <summary>
        /// Gets an external product for the product id and external system id.
        /// </summary>
        /// <param name="productId">The id of the product.</param>
        /// <param name="externalSystemId">The id of the external system.</param>
        /// <returns>A <see cref="ExternalProductEntity"/> instance.</returns>
        ExternalProductEntity GetExternalProductForProductAndExternalSystem(int productId, int externalSystemId);

        EntityCollection<ExternalProductEntity> GetExternalProducts(int externalSystemId);

        EntityCollection<ExternalProductEntity> GetExternalProducts(int externalSystemId, IEnumerable<string> ids);

        bool Save(ExternalProductEntity entity);

        bool Save(EntityCollection<ExternalProductEntity> externalProductEntities);

        bool Delete(ExternalProductEntity entity);
    }
}