﻿using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Ordering.Api.Data.Abstractions.Repositories
{
    /// <summary>
    /// Interface which is used to implement receipt repository classes.
    /// </summary>
    public interface IReceiptRepository
    {
        /// <summary>
        /// Create a new receipt.
        /// </summary>
        /// <param name="receiptEntity">The receipt entity to create.</param>
        /// <returns>A <see cref="ReceiptEntity"/> instance which is the refetched receipt.</returns>
        ReceiptEntity CreateReceipt(ReceiptEntity receiptEntity);

        /// <summary>
        /// Gets the receipt for the specified order id.
        /// </summary>
        /// <param name="orderId">The order id to get the receipt for.</param>
        /// <returns>A <see cref="ReceiptEntity"/> instance.</returns>
        ReceiptEntity GetReceiptForOrder(int orderId);
    }
}
