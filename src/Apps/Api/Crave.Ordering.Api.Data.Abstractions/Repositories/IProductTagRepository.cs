﻿using System.Collections.Generic;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;

namespace Crave.Ordering.Api.Data.Abstractions.Repositories
{
    public interface IProductTagRepository
    {
        EntityCollection<ProductTagEntity> GetProductTags(IReadOnlyList<int> productIds);
    }
}