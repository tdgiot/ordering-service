﻿using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Ordering.Api.Data.Abstractions.Repositories
{
    /// <summary>
    /// Interface which is used to implement external deliverypoint repository classes.
    /// </summary>
    public interface IExternalDeliverypointRepository
    {
        /// <summary>
        /// Gets an external deliverypoint for the deliverypoint id and external system id.
        /// </summary>
        /// <param name="deliverypointId">The id of the deliverypoint.</param>
        /// <param name="externalSystemId">The id of the external system.</param>
        /// <returns>A <see cref="ExternalDeliverypointEntity"/> instance.</returns>
        ExternalDeliverypointEntity GetExternalDeliverypointForDeliverypointAndExternalSystem(int deliverypointId, int externalSystemId);
    }
}