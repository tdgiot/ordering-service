using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Reflection;
using System.Threading.Tasks;
using Crave.Ordering.Shared.Data.LLBLGen.SQLServer;
using Crave.Ordering.Shared.Data.LLBLGen.Injectables.Authorizers;
using SD.LLBLGen.Pro.DQE.SqlServer;
using SD.LLBLGen.Pro.ORMSupportClasses;
using TraceLevel = System.Diagnostics.TraceLevel;

namespace Crave.Ordering.ConsoleApp
{
    class Program
    {
        private const string CONNECTION_STRING = "data source=localhost;initial catalog=ObymobiDevelopment;integrated security=SSPI;persist security info=False;packet size=4096";

        protected static async Task Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            try
            {
                Program.Setup();

                Stopwatch sw = Stopwatch.StartNew();

                sw.Stop();

                Console.WriteLine($"Elapsed: {sw.ElapsedMilliseconds}");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
             
            Console.WriteLine("All done");
            Console.ReadLine();
        }
        private static void Setup()
        {
            RuntimeConfiguration.AddConnectionString(DataAccessAdapter.ConnectionStringKeyName, Program.CONNECTION_STRING);

            RuntimeConfiguration.ConfigureDQE<SQLServerDQEConfiguration>(c =>
            {
                c.AddDbProviderFactory(typeof(SqlClientFactory));
                c.AddCatalogNameOverwrite("*", new SqlConnectionStringBuilder(Program.CONNECTION_STRING).InitialCatalog);
                c.SetDefaultCompatibilityLevel(SqlServerCompatibilityLevel.SqlServer2012);
                c.SetTraceLevel(TraceLevel.Warning);
            });

            RuntimeConfiguration
                .SetDependencyInjectionInfo(new List<Assembly>() { typeof(GeneralAuthorizer).Assembly },
                    new List<string>() { "Crave.Ordering.Data.LLBLGen.Injectables.Authorizers" });

            //Configuration configuration = new ConfigurationBuilder()
            //                                            .WithLoggingConfiguration(new LoggingConfiguration("Test", "Test API"))
            //                                            .WithSoapApiConfiguration(new SoapApiConfiguration(new Uri("https://test.crave-emenu.com/api/")))
            //                                            .WithAmazonConfiguration(new AmazonConfiguration(new Uri("https://s3-eu-west-1.amazonaws.com/cravecloud-test")))
            //                                            .WithMailConfiguration(new SmtpMailConfiguration("smtp.sendgrid.net", 25, "craveinteractive", "9L7KjzaqfRZHE3RRa6Ja"))
            //                                            .WithGoogleDistanceMatrixConfiguration(new GoogleDistanceMatrixConfiguration("AIzaSyAad2q10eF5nl2CCIBP8kbjevRX9qyvZDs"))
            //                                            .GetNotification();

            //IServiceCollection services = new ServiceCollection()
            //    .AddHttpClient();

            //services.AddOrdering(configuration);
            //services.BuildServiceProvider();
        }
    }
}
