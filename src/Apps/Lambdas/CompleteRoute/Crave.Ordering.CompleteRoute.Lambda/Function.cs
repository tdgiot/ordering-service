using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Amazon.Lambda.Core;
using Amazon.Lambda.SQSEvents;
using Crave.Ordering.CompleteRoute.Domain.UseCases.Requests;
using Crave.Ordering.CompleteRoute.Domain.UseCases.Responses;
using Crave.Ordering.CompleteRoute.Interfaces.UseCases;
using Crave.Ordering.CompleteRoute.Models;
using Crave.Ordering.Lambda.DI;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Crave.Ordering.Lambda.Extensions;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.SystemTextJson.DefaultLambdaJsonSerializer))]

namespace Crave.Ordering.CompleteRoute.Lambda
{
    public class Function
    {
        private readonly ILogger<Function> logger;
        private readonly IServiceProvider services;

        public Function()
        {
            services = CreateHostBuilder().Build().Services;
            logger = services.GetRequiredService<ILogger<Function>>();
        }

        public async Task FunctionHandler(SQSEvent evnt, ILambdaContext context)
        {
            List<Exception> exceptions = new List<Exception>();

            ICompleteRouteUseCase completeRouteUseCase = services.GetRequiredService<ICompleteRouteUseCase>();
            
            foreach (SQSEvent.SQSMessage message in evnt.Records)
            {
                try
                {
                    if (!message.Body.TryDeserialize<RouteCompletedEvent>(out RouteCompletedEvent routeFinishedEvent))
                    {
                        throw new InvalidOperationException($"Failed to deserialize to route finished event: {message.Body}");
                    }

                    await ProcessRouteFinishedEvent(completeRouteUseCase, routeFinishedEvent);
                }
                catch (Exception ex)
                {
                    logger.LogError(ex, "Exception while processing complete route event {SQSMessage}", JsonConvert.SerializeObject(message));
                    exceptions.Add(ex);
                }
            }

            if (exceptions.Any())
            {
                throw new AggregateException(exceptions);
            }
        }

        private async Task ProcessRouteFinishedEvent(ICompleteRouteUseCase completeRouteUseCase, RouteCompletedEvent routeFinishedEvent)
        {
            logger.LogInformation("Processing route finished event for order {OrderId}", routeFinishedEvent.OrderId);

            CompleteRouteResponse completeRouteResponse = await completeRouteUseCase.ExecuteAsync(new CompleteRouteRequest(routeFinishedEvent.OrderId));
            if (!completeRouteResponse.Success)
            {
                logger.LogWarning(completeRouteResponse.ErrorMessage, completeRouteResponse.Props);
            }
            else
            {
                logger.LogInformation("Successfully processed route finished event for order {OrderId}", routeFinishedEvent.OrderId);
            }
        }

        private static IHostBuilder CreateHostBuilder() =>
            new CustomHostBuilder()
                .UseStartup<Startup>()
                .ConfigureHostConfiguration((configApp) =>
                {
                    configApp.SetBasePath(Directory.GetCurrentDirectory())
                             .AddJsonFile("appsettings.json", false, true)
                             .AddEnvironmentVariables();
                })
                .ConfigureLogging((hostingContext, logging) =>
                {
                    logging.AddLambdaLogger(new LambdaLoggerOptions(hostingContext.Configuration));
                });
    }
}
