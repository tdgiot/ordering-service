﻿using Crave.Ordering.CompleteRoute.Data.Repositories;
using Crave.Ordering.CompleteRoute.Domain.UseCases;
using Crave.Ordering.CompleteRoute.Interfaces.Repositories;
using Crave.Ordering.CompleteRoute.Interfaces.UseCases;
using Microsoft.Extensions.DependencyInjection;

namespace Crave.Ordering.CompleteRoute.Lambda.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static void AddRepositories(this IServiceCollection services)
        {
            services.AddSingleton<IOrderRepository, OrderRepository>();
            services.AddSingleton<IOrderRoutestephandlerHistoryRepository, OrderRoutestephandlerHistoryRepository>();
            services.AddSingleton<IOrderRoutestephandlerRepository, OrderRoutestephandlerRepository>();
        }

        public static void AddUseCases(this IServiceCollection services)
        {
            services.AddSingleton<CheckOrderCompletedUseCase>();
            services.AddSingleton<MoveOrderRoutestephandlersToHistoryUseCase>();
            services.AddSingleton<ICompleteRouteUseCase, CompleteRouteUseCase>();
        }
    }
}
