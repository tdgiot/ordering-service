﻿using System.ComponentModel.DataAnnotations;

namespace Crave.Ordering.CompleteRoute.Models
{
    public class RouteCompletedEvent
    {
        [Required]
        public int OrderId { get; set; }
    }
}
