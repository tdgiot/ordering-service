﻿using System.Linq;
using System.Threading.Tasks;
using Crave.Ordering.CompleteRoute.Interfaces.Repositories;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.Utils;
using Crave.Ordering.Shared.Interfaces;
using Microsoft.Extensions.Logging;

namespace Crave.Ordering.CompleteRoute.Domain.UseCases
{
    public class MoveOrderRoutestephandlersToHistoryUseCase : IUseCaseAsync<EntityCollection<OrderRoutestephandlerEntity>, bool>
    {
        private readonly IOrderRoutestephandlerHistoryRepository orderRoutestephandlerHistoryRepository;
        private readonly IOrderRoutestephandlerRepository orderRoutestephandlerRepository;

        /// <summary>
        /// Moves the order routestep handlers to history.
        /// </summary>
        /// <param name="orderRoutestephandlerHistoryRepository">Order routestephandler history repository</param>
        /// <param name="orderRoutestephandlerRepository">Order routestephandler repository</param>
        public MoveOrderRoutestephandlersToHistoryUseCase(IOrderRoutestephandlerHistoryRepository orderRoutestephandlerHistoryRepository,
                                                          IOrderRoutestephandlerRepository orderRoutestephandlerRepository)
        {
            this.orderRoutestephandlerHistoryRepository = orderRoutestephandlerHistoryRepository;
            this.orderRoutestephandlerRepository = orderRoutestephandlerRepository;
        }

        public async Task<bool> ExecuteAsync(EntityCollection<OrderRoutestephandlerEntity> orderRoutestephandlerEntities)
        {
            bool copied = await CopyToOrderRoutestephandlerHistory(orderRoutestephandlerEntities);
            if (!copied)
            {
                return false;
            }

            await orderRoutestephandlerRepository.DeleteOrderRoutestephandlersAsync(orderRoutestephandlerEntities);
            return true;
        }

        private async Task<bool> CopyToOrderRoutestephandlerHistory(EntityCollection<OrderRoutestephandlerEntity> orderRoutestephandlerEntities)
        {
            EntityCollection<OrderRoutestephandlerHistoryEntity> orderRoutestephandlerHistoryEntities = new EntityCollection<OrderRoutestephandlerHistoryEntity>();

            foreach (OrderRoutestephandlerEntity orderRoutestephandlerEntity in orderRoutestephandlerEntities)
            {
                OrderRoutestephandlerHistoryEntity orderRoutestephandlerHistoryEntity = new OrderRoutestephandlerHistoryEntity();
                LLBLGenEntityUtil.CopyFields(orderRoutestephandlerEntity, orderRoutestephandlerHistoryEntity);

                orderRoutestephandlerHistoryEntities.Add(orderRoutestephandlerHistoryEntity);
            }

            return await orderRoutestephandlerHistoryRepository.SaveOrderRoutestephandlerHistoryAsync(orderRoutestephandlerHistoryEntities);
        }
    }
}