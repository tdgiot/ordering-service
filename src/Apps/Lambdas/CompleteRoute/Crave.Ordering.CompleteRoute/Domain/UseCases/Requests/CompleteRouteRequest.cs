﻿using Crave.Enums;

namespace Crave.Ordering.CompleteRoute.Domain.UseCases.Requests
{
    public class CompleteRouteRequest
    {
        public CompleteRouteRequest(int orderId)
        {
            OrderId = orderId;
        }

        public int OrderId { get; }
    }
}
