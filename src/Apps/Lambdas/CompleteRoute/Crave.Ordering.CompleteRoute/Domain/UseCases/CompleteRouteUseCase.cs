﻿
using Crave.Enums;
using Crave.Ordering.CompleteRoute.Domain.UseCases.Requests;
using Crave.Ordering.CompleteRoute.Domain.UseCases.Responses;
using Crave.Ordering.CompleteRoute.Interfaces.Repositories;
using Crave.Ordering.Lambda.Extensions;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.Extensions;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using System.Linq;
using System.Threading.Tasks;
using Crave.Ordering.CompleteRoute.Interfaces.UseCases;
using Crave.Ordering.Shared.OrderNotifications.Domain.UseCases.Requests;
using Crave.Ordering.Shared.OrderNotifications.Domain.UseCases.Responses;
using Crave.Ordering.Shared.OrderNotifications.Interfaces.UseCases;

namespace Crave.Ordering.CompleteRoute.Domain.UseCases
{
    public class CompleteRouteUseCase : ICompleteRouteUseCase
    {
        private readonly IOrderRepository orderRepository;
        private readonly IOrderRoutestephandlerRepository orderRoutestephandlerRepository;
        private readonly MoveOrderRoutestephandlersToHistoryUseCase moveOrderRoutestephandlersToHistoryUseCase;
        private readonly CheckOrderCompletedUseCase checkOrderCompletedUseCase;
        private readonly ISendOrderNotificationUseCase sendOrderNotificationUseCase;

        public CompleteRouteUseCase(IOrderRepository orderRepository,
                                    IOrderRoutestephandlerRepository orderRoutestephandlerRepository,
                                    MoveOrderRoutestephandlersToHistoryUseCase moveOrderRoutestephandlersToHistoryUseCase,
                                    CheckOrderCompletedUseCase checkOrderCompletedUseCase,
                                    ISendOrderNotificationUseCase sendOrderNotificationUseCase)
        {
            this.orderRepository = orderRepository;
            this.orderRoutestephandlerRepository = orderRoutestephandlerRepository;
            this.moveOrderRoutestephandlersToHistoryUseCase = moveOrderRoutestephandlersToHistoryUseCase;
            this.checkOrderCompletedUseCase = checkOrderCompletedUseCase;
            this.sendOrderNotificationUseCase = sendOrderNotificationUseCase;
        }

        public async Task<CompleteRouteResponse> ExecuteAsync(CompleteRouteRequest request)
        {
            OrderEntity order = await orderRepository.GetByIdAsync(request.OrderId);
            if (order.IsNullOrNew())
            {
                return CompleteRouteResponse.AsFailure("No order found with order id {OrderId}", request.OrderId);
            }

            if (!order.Status.In(OrderStatus.Processed, OrderStatus.Unprocessable))
            {
                return CompleteRouteResponse.AsFailure("Status {OrderStatus} of order {OrderId} not final (processed or unprocessable)", order.Status, request.OrderId);
            }

            EntityCollection<OrderRoutestephandlerEntity> steps = await orderRoutestephandlerRepository.GetAllStepsOfRouteAsync(order.OrderId);
            if (!steps.Any())
            {
                return CompleteRouteResponse.AsFailure("No order route steps found for order {OrderId}", request.OrderId);
            }

            bool movedToHistory = await moveOrderRoutestephandlersToHistoryUseCase.ExecuteAsync(steps);
            if (!movedToHistory)
            {
                return CompleteRouteResponse.AsFailure("Failed to move order route step handlers to history for order {OrderId}", request.OrderId);
            }

            bool shouldSendCompleteNotification = await ShouldSendCompleteNotification(order);
            if (!shouldSendCompleteNotification)
            {
                return CompleteRouteResponse.AsSuccess();
            }

            SendOrderNotificationResponse sendOrderNotificationResponse = await SendCompleteNotification(order);
            return !sendOrderNotificationResponse.Success
                ? CompleteRouteResponse.AsFailure("Failed to send complete notification for order {OrderId}", request.OrderId)
                : CompleteRouteResponse.AsSuccess();
        }

        private async Task<bool> ShouldSendCompleteNotification(OrderEntity orderEntity) => 
            orderEntity.OutletId.HasValue && 
            orderEntity.IsProcessedWithoutManualIntervention && 
            await checkOrderCompletedUseCase.ExecuteAsync(orderEntity.OrderId);

        private async Task<SendOrderNotificationResponse> SendCompleteNotification(OrderEntity orderEntity) => 
            await sendOrderNotificationUseCase.ExecuteAsync(new SendOrderNotificationRequest(orderEntity.OrderId, OrderNotificationType.OrderComplete));
    }
}
