﻿namespace Crave.Ordering.CompleteRoute.Domain.UseCases.Responses
{
    public class CompleteRouteResponse
    {
        private CompleteRouteResponse(bool success, string errorMessage, object[] props)
        {
            Success = success;
            ErrorMessage = errorMessage;
            Props = props;
        }

        public static CompleteRouteResponse AsSuccess() => new CompleteRouteResponse(true, string.Empty, new object[0]);
        public static CompleteRouteResponse AsFailure(string errorMessage, params object[] props) => new CompleteRouteResponse(false, errorMessage, props);

        public bool Success { get; }
        public string ErrorMessage { get; }
        public object[] Props { get; }
    }
}
