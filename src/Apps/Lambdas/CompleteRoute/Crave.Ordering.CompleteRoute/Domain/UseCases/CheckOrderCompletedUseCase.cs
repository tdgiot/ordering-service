﻿using System.Linq;
using System.Threading.Tasks;
using Crave.Ordering.CompleteRoute.Interfaces.Repositories;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Interfaces;

namespace Crave.Ordering.CompleteRoute.Domain.UseCases
{
    public class CheckOrderCompletedUseCase : IUseCaseAsync<int, bool>
    {
        private readonly IOrderRepository orderRepository;
        
        public CheckOrderCompletedUseCase(IOrderRepository orderRepository)
        {
            this.orderRepository = orderRepository;
        }
        
        public async Task<bool> ExecuteAsync(int request)
        {
            int orderId = request;

            OrderEntity masterOrderEntity = await orderRepository.GetMasterOrderForOrderId(orderId);

            bool isComplete = false;
            if (masterOrderEntity.IsProcessedWithoutManualIntervention)
            {
                // .All also returns true on empty collection 
                isComplete = masterOrderEntity.OrderCollection.All(x => x.IsProcessedWithoutManualIntervention);   
            }

            return isComplete;
        }
    }
}