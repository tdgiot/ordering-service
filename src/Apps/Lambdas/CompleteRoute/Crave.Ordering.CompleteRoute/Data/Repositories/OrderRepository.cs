﻿using System.Linq;
using System.Threading.Tasks;
using Crave.Ordering.CompleteRoute.Interfaces.Repositories;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.FactoryClasses;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.SQLServer;
using SD.LLBLGen.Pro.ORMSupportClasses;
using SD.LLBLGen.Pro.QuerySpec;
using SD.LLBLGen.Pro.QuerySpec.Adapter;

namespace Crave.Ordering.CompleteRoute.Data.Repositories
{
    /// <summary>
    /// Repository class for orders.
    /// </summary>
    public class OrderRepository : IOrderRepository
    {
        /// <inheritdoc/>
        public async Task<OrderEntity> GetByIdAsync(int orderId)
        {
            EntityQuery<OrderEntity> query = new QueryFactory().Order
                .Where(OrderFields.OrderId == orderId);

            return await new DataAccessAdapter().FetchFirstAsync(query);
        }

        public async Task<OrderEntity> GetMasterOrderForOrderId(int orderId)
        {
            RelationPredicateBucket relationPredicateBucket = new RelationPredicateBucket(OrderFields.OrderId == orderId);

            IncludeFieldsList includeFieldsList = new IncludeFieldsList
                                                  {
                                                      OrderFields.OrderId,
                                                      OrderFields.MasterOrderId,
                                                      OrderFields.Status,
                                                      OrderFields.ErrorCode
                                                  };

            PrefetchPath2 prefetch = new PrefetchPath2(EntityType.OrderEntity);
            prefetch.Add(OrderEntity.PrefetchPathMasterOrderEntity, includeFieldsList);
            prefetch.Add(OrderEntity.PrefetchPathOrderCollection, includeFieldsList);

            OrderEntity orderEntity = await DataAccessAdapter.FetchEntityAsync<OrderEntity>(relationPredicateBucket, prefetch, includeFieldsList);

            // Check who is the captain (https://i.kym-cdn.com/entries/icons/original/000/018/459/53697461.jpg)
            return orderEntity.MasterOrderId.HasValue ? orderEntity.MasterOrder : orderEntity;
        }
    }
}
