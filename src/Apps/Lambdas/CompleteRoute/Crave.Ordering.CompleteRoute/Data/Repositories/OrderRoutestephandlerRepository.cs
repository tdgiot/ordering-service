﻿using Crave.Ordering.CompleteRoute.Interfaces.Repositories;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.SQLServer;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System.Threading.Tasks;

namespace Crave.Ordering.CompleteRoute.Data.Repositories
{
    /// <summary>
    /// Repository class for order routestep handlers.
    /// </summary>
    public class OrderRoutestephandlerRepository : IOrderRoutestephandlerRepository
    {
        /// <inheritdoc/>
        public async Task<int> DeleteOrderRoutestephandlersAsync(EntityCollection<OrderRoutestephandlerEntity> orderRoutestephandlerEntities)
        {
            return await DataAccessAdapter.DeleteAsync(orderRoutestephandlerEntities);
        }

        /// <inheritdoc/>
        public async Task<EntityCollection<OrderRoutestephandlerEntity>> GetAllStepsOfRouteAsync(int orderId)
        {
            return await DataAccessAdapter.FetchEntityCollectionAsync<OrderRoutestephandlerEntity>(new RelationPredicateBucket(OrderRoutestephandlerFields.OrderId == orderId), 0, null, null, null);
        }
    }
}
