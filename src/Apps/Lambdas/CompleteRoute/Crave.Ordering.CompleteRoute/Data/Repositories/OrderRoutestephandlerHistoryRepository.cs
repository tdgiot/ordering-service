﻿using Crave.Ordering.CompleteRoute.Interfaces.Repositories;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.SQLServer;
using System.Threading.Tasks;

namespace Crave.Ordering.CompleteRoute.Data.Repositories
{
    public class OrderRoutestephandlerHistoryRepository : IOrderRoutestephandlerHistoryRepository
    {
        public async Task<bool> SaveOrderRoutestephandlerHistoryAsync(EntityCollection<OrderRoutestephandlerHistoryEntity> orderRoutestephandlerHistoryEntities)
        {
            return await DataAccessAdapter.SaveAsync(orderRoutestephandlerHistoryEntities, false, false) > 0;
        }
    }
}