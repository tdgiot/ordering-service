﻿using Crave.Ordering.CompleteRoute.Domain.UseCases.Requests;
using Crave.Ordering.CompleteRoute.Domain.UseCases.Responses;
using Crave.Ordering.Shared.Interfaces;

namespace Crave.Ordering.CompleteRoute.Interfaces.UseCases
{
    public interface ICompleteRouteUseCase : IUseCaseAsync<CompleteRouteRequest, CompleteRouteResponse>
    {

    }
}
