﻿using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using System.Threading.Tasks;

namespace Crave.Ordering.CompleteRoute.Interfaces.Repositories
{
    public interface IOrderRoutestephandlerHistoryRepository
    {
        Task<bool> SaveOrderRoutestephandlerHistoryAsync(EntityCollection<OrderRoutestephandlerHistoryEntity> orderRoutestephandlerHistoryEntities);
    }
}