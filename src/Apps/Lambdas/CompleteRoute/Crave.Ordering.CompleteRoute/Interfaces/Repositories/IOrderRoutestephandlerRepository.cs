﻿using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using System.Threading.Tasks;

namespace Crave.Ordering.CompleteRoute.Interfaces.Repositories
{
    /// <summary>
    /// Interface which is used to implement order routestep handler repository classes.
    /// </summary>
    public interface IOrderRoutestephandlerRepository
    {
        /// <summary>
        /// Deletes the specified order routestep handlers.
        /// </summary>
        /// <param name="orderRoutestephandlerEntities">The order routestep handlers to delete.</param>
        Task<int> DeleteOrderRoutestephandlersAsync(EntityCollection<OrderRoutestephandlerEntity> orderRoutestephandlerEntities);

        /// <summary>
        /// Gets all of the steps of the route for the specified order routestep handler.
        /// </summary>
        /// <param name="orderId">The id of the order to get all of the steps for.</param>
        /// <returns>A <see cref="EntityCollection{OrderRoutestephandlerModel}"/> instance.</returns>
        Task<EntityCollection<OrderRoutestephandlerEntity>> GetAllStepsOfRouteAsync(int orderId);
    }
}
