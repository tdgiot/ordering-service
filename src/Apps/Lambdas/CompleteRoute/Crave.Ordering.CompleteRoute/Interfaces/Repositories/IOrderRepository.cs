﻿using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using System.Threading.Tasks;

namespace Crave.Ordering.CompleteRoute.Interfaces.Repositories
{
    /// <summary>
    /// Interface which is used to implement order repository classes.
    /// </summary>
    public interface IOrderRepository
    {
        /// <summary>
        /// Gets an order for a specific order id
        /// </summary>
        /// <param name="orderId">The id of the order to get.</param>
        /// <returns>A <see cref="OrderEntity"/> instance.</returns>
        Task<OrderEntity> GetByIdAsync(int orderId);

        /// <summary>
        /// Get an order with all master order information pre-fetched
        /// </summary>
        /// <param name="orderId">The id of the order to fetch</param>
        /// <returns>An <see cref="EntityCollection{OrderEntity}" /> instance.</returns>
        Task<OrderEntity> GetMasterOrderForOrderId(int orderId);
    }
}
