﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Crave.Ordering.Pos.Deliverect.Models
{
    public class DispatchOrder
    {
        [Required]
        public int ExternalSystemId { get; set; }

        [Required]
        public int OrderId { get; set; }
    }
}
