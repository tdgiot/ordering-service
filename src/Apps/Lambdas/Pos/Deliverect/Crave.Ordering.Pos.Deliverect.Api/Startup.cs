using Crave.Integrations.POS.Deliverect.Extensions;
using Crave.Ordering.Pos.Deliverect.Api.Middleware;
using Crave.Ordering.Shared.Configuration.Database.Configuration;
using Crave.Ordering.Shared.Configuration.Database.Extensions;
using Crave.Ordering.Shared.Configuration.Extensions;
using Crave.Ordering.Shared.Gateway.Configuration;
using Crave.Ordering.Shared.Gateway.Extensions;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Crave.Ordering.Pos.Deliverect.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Startup.Configuration = configuration;
        }

        public static IConfiguration Configuration { get; private set; }
        private const int MAX_VALIDATION_DEPTH = 999;

        // This method gets called by the runtime. Use this method to add services to the container
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddOpenApiDocument(configure =>
            {
                configure.Title = "Deliverect Api";
                configure.GenerateEnumMappingDescription = true;
                configure.AllowReferencesWithProperties = true;
                configure.IgnoreObsoleteProperties = false;
            });

            CraveConfiguration craveConfiguration = Configuration.ReadAndValidate<CraveConfiguration>();

            services.AddDatabase(Configuration.ReadAndValidate<DatabaseConfiguration>());
            services.AddSingleton(craveConfiguration);

            services.AddOrderingGateway(craveConfiguration);
            services.AddDeliverect();
            services.AddControllers(options =>
                {
                    options.MaxValidationDepth = MAX_VALIDATION_DEPTH;
                }).AddNewtonsoftJson()
                    .ConfigureApiBehaviorOptions(options =>
                    {
                        options.SuppressModelStateInvalidFilter = true;
                    });
            services.AddTransient<GlobalExceptionMiddleware>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseXRay("DeliverectService");

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseMiddleware<GlobalExceptionMiddleware>();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapGet("/", async context =>
                {
                    await context.Response.WriteAsync("Welcome to running ASP.NET Core on AWS Lambda");
                });
            });

            if (!env.IsProduction())
            {
                app.UseOpenApi();
                app.UseSwaggerUi3();
            }
        }
    }
}
