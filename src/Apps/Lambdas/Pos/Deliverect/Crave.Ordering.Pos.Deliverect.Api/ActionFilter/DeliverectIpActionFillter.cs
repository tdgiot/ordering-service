﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;
using System.Net;

namespace Crave.Ordering.Pos.Deliverect.Api.Filter
{
    [AttributeUsage(AttributeTargets.Method)]
    sealed class IpActionFilterAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            IPAddress remoteIp = GetRemoteIp(context);
            string safeList = context.HttpContext.RequestServices.GetRequiredService<IConfiguration>().GetValue<string>("AllowedIps");

            if (!IsIpValid(safeList, remoteIp))
            {
                context.Result = new StatusCodeResult(StatusCodes.Status403Forbidden);
                return;
            }

            base.OnActionExecuting(context);
        }

        private static IPAddress GetRemoteIp(ActionExecutingContext context)
        {
            IPAddress remoteIpAddress = context.HttpContext.Connection.RemoteIpAddress;
            return remoteIpAddress.IsIPv4MappedToIPv6 ? remoteIpAddress.MapToIPv4() : remoteIpAddress;
        }

        private static bool IsIpValid(string safeList, IPAddress remoteIp)
            => safeList != null ? safeList.Split(';').Select(address => IPAddress.Parse(address)).Any(testIp => testIp.Equals(remoteIp)) 
            : throw new ArgumentNullException(nameof(safeList));
    }
}
