﻿using System;
using System.Threading.Tasks;
using Crave.Ordering.Pos.Deliverect.Api.Extensions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Crave.Ordering.Pos.Deliverect.Api.Middleware
{
    public class GlobalExceptionMiddleware : IMiddleware
    {
        private readonly ILogger<GlobalExceptionMiddleware> logger;

        public GlobalExceptionMiddleware([FromServices] ILogger<GlobalExceptionMiddleware> logger)
        {
            this.logger = logger;
        }

        public async Task InvokeAsync(HttpContext context, RequestDelegate next)
        {
            string requestBody = string.Empty;

            try
            {
                // Needed to be able to read the request body multiple times
                context.Request.EnableBuffering();

                requestBody = await context.Request.Body.ReadAsStringAsync() ?? string.Empty;
                await next(context);
            }
            catch (Exception ex)
            {
                logger.LogError($"{ex.Message}, requestBody={requestBody}");
                throw;
            }
        }
    }
}
