using Crave.Integrations.POS.Deliverect.Domain.Models;
using Crave.Integrations.POS.Deliverect.Domain.Requests;
using Crave.Integrations.POS.Deliverect.Domain.Responses;
using Crave.Integrations.POS.Deliverect.Domain.UseCases;
using Crave.Integrations.POS.Deliverect.WebHooks;
using Crave.Ordering.Pos.Deliverect.Api.Extensions;
using Crave.Ordering.Pos.Deliverect.Api.Filter;
using Crave.Ordering.Pos.Deliverect.Models;
using Crave.Ordering.Shared.Domain;
using Crave.Ordering.Shared.Gateway.Constants;
using Microsoft.AspNetCore.Mvc;
using System.IO;
using System.Threading.Tasks;

namespace Crave.Ordering.Pos.Deliverect.Api.Controllers
{
    [ApiController]
    public class DeliverectController : ControllerBase
    {
        [IpActionFilterAttribute]
        [HttpPost(RouteConstants.CHANNELSTATUS_ENDPOINT)]
        public async Task<ChannelStatusResponse?> UpdateChannelStatus(
            [FromServices] ChannelStatusWebHook channelStatusWebHook,
            [FromBody] ChannelStatusRequest request)
        {
            string? body = await GetRequestBody();
            if (body == null)
            {
                return Response<ChannelStatusResponse>.AsError("No request body").Model;
            }

            return await channelStatusWebHook.ExecuteAsync(WrapRequest(request, body));
        }

        [IpActionFilterAttribute]
        [HttpPost(RouteConstants.MENUPUSH_ENDPOINT)]
        public async Task MenuPush(
            [FromServices] MenuUpdateWebHook menuUpdateWebHook,
            [FromBody] Menu[] request)
        {
            string? body = await GetRequestBody();
            if (body == null)
            {
                return;
            }

            await menuUpdateWebHook.ExecuteAsync(WrapRequest(request, body));
        }

        [IpActionFilterAttribute]
        [HttpPost(RouteConstants.ORDERSTATUSUPDATE_ENDPOINT)]
        public async Task<ActionResult> OrderStatusUpdate(
            [FromServices] OrderStatusUpdateWebHook orderStatusUpdateWebHook,
            [FromBody] OrderStatusRequest request)
        {
            string? body = await GetRequestBody();
            if (body == null)
            {
                return new BadRequestResult();
            }

            Response response = await orderStatusUpdateWebHook.ExecuteAsync(WrapRequest(request, body));

            return response.Success ? Ok() : (ActionResult)BadRequest(response.Message);
        }

        [IpActionFilterAttribute]
        [HttpPost(RouteConstants.TOGGLESNOOZEPRODUCTS_ENDPOINT)]
        public async Task<ActionResult> ToggleSnoozeProducts(
            [FromServices] SnoozeUnsnoozeWebHook snoozeUnsnoozeWebHook,
            [FromBody] SnoozeUnsnoozeRequest request)
        {
            string? body = await GetRequestBody();
            if (body == null)
            {
                return BadRequest("No request body");
            }

            Response<SnoozeUnsnoozeResponse> response = await snoozeUnsnoozeWebHook.ExecuteAsync(WrapRequest(request, body));

            return response.Success ? Ok(response.Model) : (ActionResult)BadRequest(response.Message);
        }

        [HttpPost(RouteConstants.DISPATCHORDER_ENDPOINT)]
        public async Task<ActionResult> DispatchOrder(
            [FromServices] DispatchOrderUseCase dispatchOrderUseCase,
            [FromBody] DispatchOrder request)
        {
            Response response = await dispatchOrderUseCase.ExecuteAsync(new DispatchOrderRequest(request.ExternalSystemId, request.OrderId));

            return response.Success ? Ok() : (ActionResult)BadRequest(response.Message);
        }

        [HttpPost(RouteConstants.BUSYMODE_ENDPOINT)]
        public async Task<ActionResult> BusyMode(
            [FromServices] BusyModeUseCase busyModeUseCase,
            [FromBody] BusyModeRequest request)
        {
            Response<BusyModeResponse> response = await busyModeUseCase.ExecuteAsync(request);

            return response.Success ? Ok(response.Model) : (ActionResult)BadRequest(response.Message);
        }

        private async Task<string?> GetRequestBody()
        {
            Stream requestBody = HttpContext.Request.Body;
            return requestBody == null ? null : await requestBody.ReadAsStringAsync();
        }

        private HmacValidatedRequest<TRequest> WrapRequest<TRequest>(TRequest request, string body) => new HmacValidatedRequest<TRequest>(request, body, HttpContext.Request.Headers["X-Server-Authorization-HMAC-SHA256"]);
    }
}
