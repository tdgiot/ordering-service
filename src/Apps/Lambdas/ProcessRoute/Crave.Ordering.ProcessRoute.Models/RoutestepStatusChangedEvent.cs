﻿using System.ComponentModel.DataAnnotations;

namespace Crave.Ordering.ProcessRoute.Models
{
    public class RoutestepStatusChangedEvent
    {
        [Required]
        public int OrderRoutestephandlerId { get; set; }
    }
}
