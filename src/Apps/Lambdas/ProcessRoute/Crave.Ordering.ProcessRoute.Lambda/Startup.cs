﻿using Crave.Ordering.Lambda.DI.Interfaces;
using Crave.Ordering.ProcessRoute.Lambda.Extensions;
using Crave.Ordering.Shared.Configuration.Database.Configuration;
using Crave.Ordering.Shared.Configuration.Database.Extensions;
using Crave.Ordering.Shared.Configuration.Extensions;
using Crave.Ordering.Shared.Configuration.Sections;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Crave.Ordering.ProcessRoute.Lambda
{
    public class Startup : IStartup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void Configure(IHostBuilder app)
        {
            
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDatabase(Configuration.ReadAndValidate<DatabaseConfiguration>());
            services.AddRepositories();
            services.AddUseCases();
            services.AddClients(Configuration.ReadAndValidate<CraveConfiguration>());
        }
    }
}
