using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Amazon.Lambda.Core;
using Amazon.Lambda.SQSEvents;
using Crave.Ordering.Lambda.DI;
using Crave.Ordering.Lambda.Extensions;
using Crave.Ordering.ProcessRoute.Domain.UseCases.Requests;
using Crave.Ordering.ProcessRoute.Interfaces.UseCases;
using Crave.Ordering.ProcessRoute.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.SystemTextJson.DefaultLambdaJsonSerializer))]

namespace Crave.Ordering.ProcessRoute.Lambda
{
    public class Function
    {
        private readonly IServiceProvider services;

        public Function()
        {
            services = CreateHostBuilder().Build().Services;
        }

        public async Task FunctionHandler(SQSEvent evnt, ILambdaContext context)
        {
            List<Exception> exceptions = new List<Exception>();

            IProcessRouteUseCase processRouteUseCase = services.GetRequiredService<IProcessRouteUseCase>();

            foreach (SQSEvent.SQSMessage message in evnt.Records)
            {
                try
                {
                    if (!message.Body.TryDeserialize<RoutestepStatusChangedEvent>(out RoutestepStatusChangedEvent routestepStatusChangedEvent))
                    {
                        throw new InvalidOperationException($"Failed to deserialize to route finished event: {message.Body}");
                    }

                    await processRouteUseCase.ExecuteAsync(new ProcessRouteRequest(routestepStatusChangedEvent.OrderRoutestephandlerId));
                }
                catch (Exception ex)
                {
                    exceptions.Add(ex);
                }
            }

            if (exceptions.Any())
            {
                throw new AggregateException(exceptions);
            }
        }

        private static IHostBuilder CreateHostBuilder() =>
            new CustomHostBuilder()
                .UseStartup<Startup>()
                .ConfigureHostConfiguration((configApp) =>
                {
                    configApp.SetBasePath(Directory.GetCurrentDirectory())
                             .AddJsonFile("appsettings.json", false, true)
                             .AddEnvironmentVariables();
                })
                .ConfigureLogging((hostingContext, logging) =>
                {
                    logging.AddLambdaLogger(new LambdaLoggerOptions(hostingContext.Configuration));
                });
    }
}
