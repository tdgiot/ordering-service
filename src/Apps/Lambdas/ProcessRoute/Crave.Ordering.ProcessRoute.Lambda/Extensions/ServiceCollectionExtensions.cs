﻿using System;
using Crave.Ordering.Api.Client;
using Crave.Ordering.ProcessRoute.Domain.UseCases;
using Crave.Ordering.ProcessRoute.Interfaces.UseCases;
using Crave.Ordering.Shared.Configuration.Sections;
using Microsoft.Extensions.DependencyInjection;

namespace Crave.Ordering.ProcessRoute.Lambda.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static void AddRepositories(this IServiceCollection services)
        {
            
        }

        public static void AddUseCases(this IServiceCollection services)
        {
            services.AddSingleton<IProcessRouteUseCase, ProcessRouteUseCase>();
        }

        public static void AddClients(this IServiceCollection services, CraveConfiguration craveConfiguration)
        {
            services.AddHttpClient<IOrdersClient, OrdersClient>(a => a.BaseAddress = new Uri(craveConfiguration.Ordering.ApiUrl));
        }
    }
}
