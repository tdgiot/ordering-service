﻿using Crave.Ordering.ProcessRoute.Domain.UseCases.Requests;
using Crave.Ordering.Shared.Interfaces;

namespace Crave.Ordering.ProcessRoute.Interfaces.UseCases
{
    public interface IProcessRouteUseCase : IUseCaseAsync<ProcessRouteRequest>
    {
    }
}
