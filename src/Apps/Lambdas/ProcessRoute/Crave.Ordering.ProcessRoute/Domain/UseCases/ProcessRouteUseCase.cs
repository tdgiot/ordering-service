﻿using System;
using System.Threading.Tasks;
using Crave.Ordering.Api.Client;
using Crave.Ordering.ProcessRoute.Domain.UseCases.Requests;
using Crave.Ordering.ProcessRoute.Interfaces.UseCases;
using Microsoft.Extensions.Logging;

namespace Crave.Ordering.ProcessRoute.Domain.UseCases
{
    public class ProcessRouteUseCase : IProcessRouteUseCase
    {
        private readonly ILogger<ProcessRouteUseCase> logger;
        private readonly IOrdersClient ordersClient;

        public ProcessRouteUseCase(ILogger<ProcessRouteUseCase> logger,
                                   IOrdersClient ordersClient)
        {
            this.logger = logger;
            this.ordersClient = ordersClient;
        }

        public async Task ExecuteAsync(ProcessRouteRequest request)
        {
            logger.LogInformation("Processing route for order routestephandler {OrderRoutestephandlerId}", request.OrderRoutestephandlerId);

            try
            {
                await ordersClient.HandlerUpdatedAsync(request.OrderRoutestephandlerId);

                logger.LogInformation("Successfully processed route for order routestephandler {OrderRoutestephandlerId}", request.OrderRoutestephandlerId);
            }
            catch (Exception ex)
            {
                logger.LogError(ex, "Something went wrong trying to process route for order routestephandler {OrderRoutestephandlerId}", request.OrderRoutestephandlerId);
                throw;
            }
        }
    }
}
