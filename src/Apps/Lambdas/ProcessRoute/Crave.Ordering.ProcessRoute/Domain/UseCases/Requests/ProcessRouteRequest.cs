﻿namespace Crave.Ordering.ProcessRoute.Domain.UseCases.Requests
{
    public class ProcessRouteRequest
    {
        public ProcessRouteRequest(int orderRoutestephandlerId)
        {
            this.OrderRoutestephandlerId = orderRoutestephandlerId;
        }

        public int OrderRoutestephandlerId { get; }
    }
}
