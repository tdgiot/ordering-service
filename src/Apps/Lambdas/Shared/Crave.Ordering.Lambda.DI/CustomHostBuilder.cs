﻿using Crave.Ordering.Lambda.DI.Interfaces;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Crave.Ordering.Lambda.DI
{
    public class CustomHostBuilder : IHostBuilder
    {
        private readonly IHostBuilder configHostBuilder;
        private readonly IHostBuilder appHostBuilder;

        public CustomHostBuilder()
        {
            configHostBuilder = new HostBuilder();
            appHostBuilder = new HostBuilder();
        }

        public IHostBuilder UseStartup<TStartup>() where TStartup : IStartup
        {
            configHostBuilder
                .ConfigureServices
                (
                    (context, services) => services.AddSingleton(typeof(IStartup), typeof(TStartup))
                );

            return this;
        }

        public IHostBuilder ConfigureHostConfiguration(Action<IConfigurationBuilder> configureDelegate)
        {
            configHostBuilder.ConfigureHostConfiguration(configureDelegate);
            appHostBuilder.ConfigureHostConfiguration(configureDelegate);

            return this;
        }

        public IHostBuilder ConfigureAppConfiguration(Action<HostBuilderContext, IConfigurationBuilder> configureDelegate)
        {
            configHostBuilder.ConfigureAppConfiguration(configureDelegate);
            appHostBuilder.ConfigureAppConfiguration(configureDelegate);

            return this;
        }

        public IHostBuilder ConfigureServices(Action<HostBuilderContext, IServiceCollection> configureDelegate)
        {
            configHostBuilder.ConfigureServices(configureDelegate);
            appHostBuilder.ConfigureServices(configureDelegate);

            return this;
        }

        public IHostBuilder UseServiceProviderFactory<TContainerBuilder>(Func<HostBuilderContext, IServiceProviderFactory<TContainerBuilder>> factory)
        {
            configHostBuilder.UseServiceProviderFactory(factory);
            appHostBuilder.UseServiceProviderFactory(factory);

            return this;
        }

        public IHostBuilder UseServiceProviderFactory<TContainerBuilder>(IServiceProviderFactory<TContainerBuilder> factory)
        {
            configHostBuilder.UseServiceProviderFactory(factory);
            appHostBuilder.UseServiceProviderFactory(factory);

            return this;
        }

        public IHostBuilder ConfigureContainer<TContainerBuilder>(Action<HostBuilderContext, TContainerBuilder> configureDelegate)
        {
            configHostBuilder.ConfigureContainer(configureDelegate);
            appHostBuilder.ConfigureContainer(configureDelegate);

            return this;
        }

        public IHost Build()
        {
            foreach (KeyValuePair<object, object> keyValuePair in Properties.Select(x => new KeyValuePair<object, object>(x.Key, x.Value)))
            {
                configHostBuilder.Properties.Add(keyValuePair);
                appHostBuilder.Properties.Add(keyValuePair);
            }

            IStartup startup = configHostBuilder
                .Build()
                .Services
                .GetService<IStartup>();

            if (startup != null)
            {
                appHostBuilder
                    .ConfigureServices(services => startup.ConfigureServices(services));

                appHostBuilder
                    .ConfigureHostConfiguration(builder => startup.Configure(appHostBuilder));
            }

            return appHostBuilder.Build();
        }

        public IDictionary<object, object> Properties { get; } = new Dictionary<object, object>();
    }
}
