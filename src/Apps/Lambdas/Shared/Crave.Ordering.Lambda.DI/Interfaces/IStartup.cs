﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Crave.Ordering.Lambda.DI.Interfaces
{
    public interface IStartup
    {
        void ConfigureServices(IServiceCollection services);
        void Configure(IHostBuilder app);
    }
}
