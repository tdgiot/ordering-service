﻿using Newtonsoft.Json;
using System.Diagnostics.CodeAnalysis;

namespace Crave.Ordering.Lambda.Extensions
{
    public static class StringExtensions
    {
        public static bool TryDeserialize<TModel>(this string data, [NotNullWhen(true)] out TModel model) where TModel : class
        {
            model = null;

            if (string.IsNullOrWhiteSpace(data))
            {
                return false;
            }

            try
            {
                model = JsonConvert.DeserializeObject<TModel>(data);
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
