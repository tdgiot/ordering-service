﻿using System.Linq;

namespace Crave.Ordering.Lambda.Extensions
{
    public static class EnumExtensions
    {
        public static bool In<T>(this T val, params T[] values) where T : struct
        {
            return values.Contains(val);
        }
    }
}
