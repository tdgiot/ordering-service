﻿using System;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Crave.Ordering.Shared.Data.LLBLGen.Injectables.Authorizers
{
    [Serializable]
    [DependencyInjectionInfo(typeof(IEntity2), "AuthorizerToUse", ContextType = DependencyInjectionContextType.Singleton)]
    // ReSharper disable once UnusedMember.Global (class is used using dependency injection)
    public class GeneralAuthorizer : AuthorizerBase
    {
        #region Methods

        /// <summary>
        /// Determines whether the caller is allowed to save the new instance passed in
        /// and sets the value of the "Created" and/or "CreatedUTC" field
        /// </summary>
        /// <param name="entity">The entity to save</param>
        /// <returns>True if the entity can be saved, False if not</returns>
        public override bool CanSaveNewEntity(IEntityCore entity)
        {
            IEntity2 useableEntity = entity as IEntity2;
            if (useableEntity != null)
            {
                if (useableEntity.Fields["Created"] != null)
                {
                    useableEntity.SetNewFieldValue("Created", DateTime.Now);
                }

                if (useableEntity.Fields["CreatedUTC"] != null)
                {
                    useableEntity.SetNewFieldValue("CreatedUTC", DateTime.Now.ToUniversalTime());
                }
            }

            return base.CanSaveNewEntity(entity);
        }

        /// <summary>
        /// Determines whether the caller is allowed to save the modified existing instance passed in
        /// and sets the value of the "Updated" and/or "UpdatedUTC" field
        /// </summary>
        /// <param name="entity">The entity to save</param>
        /// <returns>True if the entity can be saved, False if not</returns>
        public override bool CanSaveExistingEntity(IEntityCore entity)
        {
            IEntity2 useableEntity = entity as IEntity2;
            if (useableEntity != null)
            {
                if (useableEntity.Fields["Updated"] != null)
                {
                    useableEntity.SetNewFieldValue("Updated", DateTime.Now);
                }

                if (useableEntity.Fields["UpdatedUTC"] != null)
                {
                    useableEntity.SetNewFieldValue("UpdatedUTC", DateTime.Now.ToUniversalTime());
                }
            }

            return base.CanSaveExistingEntity(entity);
        }

        #endregion
    }
}
