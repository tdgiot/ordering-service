﻿using System.Threading.Tasks;
using Crave.Ordering.CompleteRoute.Models;
using Crave.Ordering.Pos.Deliverect.Models;
using Crave.Ordering.ProcessRoute.Models;
using Crave.Ordering.Shared.Gateway.Constants;
using Refit;

namespace Crave.Ordering.Shared.Gateway.Ordering
{
    public interface IOrderingGateway
    {
        [Post(RouteConstants.COMPLETEROUTE_ENDPOINT)]
        Task FireRouteCompletedEventAsync([Body] RouteCompletedEvent routeCompletedEvent);

        [Post(RouteConstants.PROCESSROUTE_ENDPOINT)]
        Task FireRoutestepStatusChangedEventAsync([Body] RoutestepStatusChangedEvent routestepStatusChangedEvent);

        [Post(RouteConstants.DISPATCHORDER_ENDPOINT)]
        Task DispatchOrderAsync([Body] DispatchOrder dispatchOrder);
    }
}
