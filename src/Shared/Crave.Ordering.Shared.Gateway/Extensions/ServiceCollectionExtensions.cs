﻿using System;
using Amazon.XRay.Recorder.Handlers.System.Net;
using Crave.Ordering.Shared.Gateway.Configuration;
using Crave.Ordering.Shared.Gateway.Constants;
using Crave.Ordering.Shared.Gateway.Ordering;
using Microsoft.Extensions.DependencyInjection;
using Refit;

namespace Crave.Ordering.Shared.Gateway.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static void AddOrderingGateway(this IServiceCollection services, CraveConfiguration craveConfiguration)
        {
            services
                .AddTransient<HttpClientXRayTracingHandler>();

            services
                .AddRefitClient<IOrderingGateway>()
                .ConfigureHttpClient(client => client.BaseAddress = new Uri(craveConfiguration.Gateway.PublicUrl + RouteConstants.ORDERING))
                .AddHttpMessageHandler<HttpClientXRayTracingHandler>();
        }
    }
}
