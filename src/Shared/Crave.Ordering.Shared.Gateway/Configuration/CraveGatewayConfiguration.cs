﻿using System.ComponentModel.DataAnnotations;

namespace Crave.Ordering.Shared.Gateway.Configuration
{
    public class CraveGatewayConfiguration
    {
        [Required]
        public string PublicUrl { get; set; }
    }
}
