﻿using System.ComponentModel.DataAnnotations;
using Crave.Ordering.Shared.Configuration.Interfaces;

namespace Crave.Ordering.Shared.Gateway.Configuration
{
    public class CraveConfiguration : IConfigurationSection
    {
        public string Name => "Crave";

        [Required]
        public CraveGatewayConfiguration Gateway { get; set; }
    }
}
