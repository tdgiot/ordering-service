﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace Crave.Ordering.Shared.Security
{
    public class Cryptographer
    {
        private const string InitVector = "@1B2c3D4e5F6g7H8";
        private const int KEY_SIZE = 128;
        private const int BLOCK_SIZE = 128;

        private static readonly Cryptographer Instance;

        static Cryptographer()
        {
            if (Instance == null)
            {
                // first create the instance
                Instance = new Cryptographer();
            }
        }

        /// <summary>
        /// Encrypts the specified plain text string using a Rijndael managed CBC cipher mode.
        /// </summary>
        /// <param name="plainText">The text to encrypt.</param>
        /// <returns>An encrypted <see cref="string"/> instance.</returns>
        public static string EncryptUsingCBC(string plainText)
        {
            // Convert the plain text to bytes
            byte[] plainBytes = Encoding.UTF8.GetBytes(plainText);

            // Get the RijndaelManaged instance and encrypt the bytes
            RijndaelManaged rijndaelManaged = Cryptographer.GetRijndaelManaged(Cryptographer.InitVector);
            byte[] encryptedBytes = rijndaelManaged.CreateEncryptor().TransformFinalBlock(plainBytes, 0, plainBytes.Length);

            // Convert to base 64 string and return
            return Convert.ToBase64String(encryptedBytes);
        }

        /// <summary>
        /// Decrypts the specified encrypted text string using a Rijndeal managed CB cipher mode.
        /// </summary>
        /// <param name="encryptedText">The text to decrypt.</param>
        /// <returns>An decrypted <see cref="string"/> instance.</returns>
        public static string DecryptUsingCBC(string encryptedText)
        {
            // Convert the encrypted text to bytes
            byte[] encryptedBytes = Convert.FromBase64String(encryptedText);

            // Get the RijndaelManaged instance and decrypt the bytes
            RijndaelManaged rijndaelManaged = Cryptographer.GetRijndaelManaged(Cryptographer.InitVector);
            byte[] decryptedBytes = rijndaelManaged.CreateDecryptor().TransformFinalBlock(encryptedBytes, 0, encryptedBytes.Length);

            // Convert to UTF-8 string and return
            return Encoding.UTF8.GetString(decryptedBytes);
        }

        /// <summary>
        /// Gets the Rijndeal managed instance to encrypts and decrypts strings with.
        /// </summary>
        /// <param name="secretKey">The secret key to use for encryption and decryption.</param>
        /// <returns>A <see cref="RijndaelManaged"/> instance.</returns>
        private static RijndaelManaged GetRijndaelManaged(string secretKey)
        {
            byte[] keyBytes = new byte[16];
            byte[] secretKeyBytes = Encoding.UTF8.GetBytes(secretKey);
            Array.Copy(secretKeyBytes, keyBytes, Math.Min(keyBytes.Length, secretKeyBytes.Length));
            return new RijndaelManaged
            {
                Mode = CipherMode.CBC,
                Padding = PaddingMode.PKCS7,
                KeySize = KEY_SIZE,
                BlockSize = BLOCK_SIZE,
                Key = keyBytes,
                IV = keyBytes
            };
        }
    }
}
