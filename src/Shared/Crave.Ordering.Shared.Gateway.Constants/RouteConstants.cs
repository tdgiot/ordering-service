﻿namespace Crave.Ordering.Shared.Gateway.Constants
{
    public static class RouteConstants
    {
        public const string ORDERING = "ordering";
        
        public const string EVENTS = "events";
        public const string COMPLETEROUTE = "completeroute";
        public const string COMPLETEROUTE_ENDPOINT = "/events/completeroute";
        public const string PROCESSROUTE = "processroute";
        public const string PROCESSROUTE_ENDPOINT = "/events/processroute";

        public const string POS = "pos";
        public const string DELIVERECT = "deliverect";
        
        public const string CHANNELSTATUS_ENDPOINT = "/pos/deliverect/channelstatus";
        public const string MENUPUSH_ENDPOINT = "/pos/deliverect/menupush";
        public const string ORDERSTATUSUPDATE_ENDPOINT = "/pos/deliverect/orderstatusupdate";
        public const string TOGGLESNOOZEPRODUCTS_ENDPOINT = "/pos/deliverect/togglesnoozeproducts";
        public const string DISPATCHORDER_ENDPOINT = "/pos/deliverect/dispatchorder";
        public const string BUSYMODE_ENDPOINT = "/pos/deliverect/busymode";
    }
}
