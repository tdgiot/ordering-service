﻿using Crave.Ordering.Shared.Configuration.Interfaces;
using System.ComponentModel.DataAnnotations;

namespace Crave.Ordering.Shared.Configuration.Database.Configuration
{
    public class DatabaseConfiguration : IConfigurationSection
    {
        public string Name => "Database";

        [Required]
        public string ConnectionString { get; set; }
    }
}
