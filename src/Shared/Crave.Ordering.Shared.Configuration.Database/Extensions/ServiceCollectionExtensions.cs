﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Reflection;
using Crave.Ordering.Shared.Configuration.Database.Configuration;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.Injectables.Authorizers;
using Crave.Ordering.Shared.Data.LLBLGen.SQLServer;
using Microsoft.Extensions.DependencyInjection;
using SD.LLBLGen.Pro.DQE.SqlServer;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Crave.Ordering.Shared.Configuration.Database.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static void AddDatabase(this IServiceCollection services, DatabaseConfiguration databaseConfiguration)
        {
            RuntimeConfiguration.AddConnectionString(DataAccessAdapter.ConnectionStringKeyName, databaseConfiguration.ConnectionString);

            RuntimeConfiguration.ConfigureDQE<SQLServerDQEConfiguration>(c =>
            {
                c.AddDbProviderFactory(typeof(SqlClientFactory));
                c.AddCatalogNameOverwrite("*", new SqlConnectionStringBuilder(databaseConfiguration.ConnectionString).InitialCatalog);
                c.SetDefaultCompatibilityLevel(SqlServerCompatibilityLevel.SqlServer2012);
                c.SetTraceLevel(TraceLevel.Warning);
            });

            RuntimeConfiguration.SetDependencyInjectionInfo(new List<Assembly> { typeof(GeneralAuthorizer).Assembly },
                                                            new List<string> { typeof(GeneralAuthorizer).Namespace ?? string.Empty });

            CustomDynamicQueryEngine.UseNoLockHintOnSelects = true;

            services.AddSingleton(databaseConfiguration);
        }
    }
}
