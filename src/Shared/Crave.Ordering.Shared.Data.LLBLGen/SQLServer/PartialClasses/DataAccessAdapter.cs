﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.Extensions;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Crave.Ordering.Shared.Data.LLBLGen.SQLServer
{
    public partial class DataAccessAdapter
    {
        public bool IsDisposed { get; private set; } = false;

        #region Static wrapper methods

#pragma warning disable CS3002 // Return type is not CLS-compliant
        /// <summary>
        /// Fetches an entity collection from the database.
        /// </summary>
        /// <param name="filterBucket">The bucket containing the filter and relations to fetch the collection from the database.</param>
        /// <param name="maxNumberOfItemsToReturn">The maximum number of items to return. (0 is all).</param>
        /// <param name="sortClauses">The sort clauses defining the order in which the entity collection is sorted.</param>
        /// <param name="prefetchPath">The pre fetch path which determines additional entities to fetch from.</param>
        /// <param name="excludedIncludedFields">The included or excluded fields to use when fetching the data.</param>
        /// <param name="adapter">The data access adapter to use when fetching the entity collection from the database. Use null to create a new one.</param>
        /// <returns>An <see cref="EntityCollection"/> instance.</returns>
        public static EntityCollection<TEntity> FetchEntityCollection<TEntity>(RelationPredicateBucket filterBucket, int maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath2 prefetchPath, ExcludeIncludeFieldsList excludedIncludedFields, IDataAccessAdapter adapter = null) where TEntity : EntityBase2, new()
#pragma warning restore CS3002 // Return type is not CLS-compliant
        {
            EntityCollection<TEntity> entityCollection = new EntityCollection<TEntity>();

            if (adapter == null)
            {
                // No adapter was specified, create a new one
                using (adapter = new DataAccessAdapter())
                {
                    adapter.FetchEntityCollection(entityCollection, filterBucket, maxNumberOfItemsToReturn, sortClauses, prefetchPath, excludedIncludedFields);
                }
            }
            else
            {
                // Use the specified adapter, disposing is done somewhere else
                adapter.FetchEntityCollection(entityCollection, filterBucket, maxNumberOfItemsToReturn, sortClauses, prefetchPath, excludedIncludedFields);
            }

            return entityCollection;
        }

        /// <summary>
        /// Fetches an entity collection async from the database.
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="filterBucket">The bucket containing the filter and relations to fetch the collection from the database.</param>
        /// <param name="maxNumberOfItemsToReturn">The maximum number of items to return. (0 is all).</param>
        /// <param name="sortClauses">The sort clauses defining the order in which the entity collection is sorted.</param>
        /// <param name="prefetchPath">The pre fetch path which determines additional entities to fetch from.</param>
        /// <param name="excludedIncludedFields">The included or excluded fields to use when fetching the data.</param>
        /// <param name="adapter">The data access adapter to use when fetching the entity collection from the database. Use null to create a new one.</param>
        /// <returns>An <see cref="EntityCollection"/> instance.</returns>
        public static async Task<EntityCollection<TEntity>> FetchEntityCollectionAsync<TEntity>(RelationPredicateBucket filterBucket, int maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath2 prefetchPath, ExcludeIncludeFieldsList excludedIncludedFields, IDataAccessAdapter adapter = null) where TEntity : EntityBase2
        {
            EntityCollection<TEntity> entityCollection = new EntityCollection<TEntity>();

            QueryParameters parameters = new QueryParameters
            {
                CollectionToFetch = entityCollection,
                FilterToUse = filterBucket?.PredicateExpression,
                RelationsToUse = filterBucket?.Relations,
                PrefetchPathToUse = prefetchPath,
                SorterToUse = sortClauses
            };

            if (adapter == null)
            {
                using (adapter = new DataAccessAdapter())
                {
                    await adapter.FetchEntityCollectionAsync(parameters, CancellationToken.None).ConfigureAwait(false);
                }
            }
            else
            {
                await adapter.FetchEntityCollectionAsync(parameters, CancellationToken.None).ConfigureAwait(false);
            }

            return entityCollection;
        }


#pragma warning disable CS3002 // Return type is not CLS-compliant
        /// <summary>
        /// Fetches an entity collection from the database.
        /// </summary>
        /// <param name="queryParameters">The query parameters to use for fetching the entity collection.</param>
        /// <param name="adapter">The data access adapter to use when fetching the entity collection from the database. Use null to create a new one.</param>
        /// <returns>An <see cref="EntityCollection{TEntity}"/> instance.</returns>
        public static EntityCollection<TEntity> FetchEntityCollection<TEntity>(QueryParameters queryParameters, IDataAccessAdapter adapter = null) where TEntity : EntityBase2, new()
#pragma warning restore CS3002 // Return type is not CLS-compliant
        {
            EntityCollection<TEntity> entityCollection = new EntityCollection<TEntity>();
            queryParameters.CollectionToFetch = entityCollection;

            if (adapter == null)
            {
                // No adapter was specified, create a new one
                using (adapter = new DataAccessAdapter())
                {
                    adapter.FetchEntityCollection(queryParameters);
                }
            }
            else
            {
                // Use the specified adapter, disposing is done somewhere else
                adapter.FetchEntityCollection(queryParameters);
            }

            return entityCollection;
        }

        /// <summary>
        /// Gets the amount of records in a database table for the specified field and filter.
        /// </summary>
        /// <param name="field">The field to count.</param>
        /// <param name="filter">The filter to count the records on.</param>
        /// <returns>An <see cref="int"/> containing the amount of records.</returns>
        public static int GetCount(IEntityField2 field, IPredicate filter, RelationCollection relations, IDataAccessAdapter adapter = null)
        {
            return DataAccessAdapter.GetScalar<int>(field, filter, AggregateFunction.Count, relations, adapter);
        }

        /// <summary>
        /// Gets a scalar value (min, max, average, etc) for the specified field and filter.
        /// </summary>
        /// <typeparam name="TEntity">The type of the return value.</typeparam>
        /// <param name="field">The field to get the scalar value for.</param>
        /// <param name="filter">The filter to use when getting the scalar value.</param>
        /// <param name="aggregateFunction">The aggregate function (min, max, average, etc) to apply.</param>
        /// <returns>A scalar value.</returns>
        public static TEntity GetScalar<TEntity>(IEntityField2 field, IPredicate filter, AggregateFunction aggregateFunction, RelationCollection relations, IDataAccessAdapter adapter = null)
        {
            TEntity returnValue = default(TEntity);

            if (adapter == null)
            {
                using (adapter = new DataAccessAdapter())
                {
                    object obj = adapter.GetScalar(field, null, aggregateFunction, filter, null, relations);

                    if (obj != null && !obj.Equals(DBNull.Value))
                    {
                        returnValue = (TEntity)obj;
                    }
                }
            }
            else
            {
                object obj = adapter.GetScalar(field, null, aggregateFunction, filter, null, relations);
                if (obj != null && !obj.Equals(DBNull.Value))
                {
                    returnValue = (TEntity)obj;
                }
            }

            return returnValue;
        }

        /// <summary>
        /// Fetches an entity using the specified unique constraint filter.
        /// </summary>
        /// <param name="entity">The entity to fill.</param>
        /// <param name="uniqueConstraintFilter">The filter to apply when getting the entity.</param>
        /// <param name="prefetchPath">The pre fetch path which determines additional entities to fetch from.</param>
        /// <param name="excludedIncludedFields">The included or excluded fields to use when fetching the data.</param>
        /// <param name="adapter">The data access adapter to use when fetching the entity from the database. Use null to create a new one.</param>
        /// <returns>An entity instance.</returns>
        public static bool FetchEntityUsingUniqueConstraint<TEntity>(TEntity entity, IPredicateExpression uniqueConstraintFilter, IPrefetchPath2 prefetchPath, ExcludeIncludeFieldsList excludedIncludedFields, IDataAccessAdapter adapter = null) where TEntity : EntityBase2, new()
        {
            bool success = false;

            if (adapter == null)
            {
                using (adapter = new DataAccessAdapter())
                {
                    success = adapter.FetchEntityUsingUniqueConstraint(entity, uniqueConstraintFilter, prefetchPath, null, excludedIncludedFields);
                }
            }
            else
            {
                success = adapter.FetchEntityUsingUniqueConstraint(entity, uniqueConstraintFilter, prefetchPath, null, excludedIncludedFields);
            }

            return success;
        }

        /// <summary>
        /// Fetches an entity using the specified id.
        /// </summary>
        /// <param name="id">The id of the entity to fetch.</param>
        /// <param name="prefetchPath">The pre fetch path which determines additional entities to fetch from.</param>
        /// <param name="excludedIncludedFields">The included or excluded fields to use when fetching the data.</param>
        /// <param name="adapter">The data access adapter to use when fetching the entity from the database. Use null to create a new one.</param>
        /// <returns>An entity instance.</returns>
        public static TEntity FetchEntityById<TEntity>(int id, IPrefetchPath2 prefetchPath, ExcludeIncludeFieldsList excludedIncludedFields, IDataAccessAdapter adapter = null) where TEntity : EntityBase2, new()
        {
            TEntity entity = new TEntity();

            if (!entity.Fields[0].IsPrimaryKey)
            {
                throw new InvalidOperationException($"The first field of entity '{typeof(TEntity).Name}' is not a primary key field!");
            }

            if (adapter == null)
            {
                using (adapter = new DataAccessAdapter())
                {
                    entity.SetNewFieldValue(0, id); // Assumes primary key field is first field
                    adapter.FetchEntity(entity, prefetchPath, null, excludedIncludedFields);
                }
            }
            else
            {
                entity.SetNewFieldValue(0, id); // Assumes primary key field is first field
                adapter.FetchEntity(entity, prefetchPath, null, excludedIncludedFields);
            }

            if (entity.IsNullOrNew())
            {
                throw new InvalidOperationException($"Entity of type '{entity.GetType().Name}' with id '{id}' does not exist in the database!");
            }

            return entity;
        }

        /// <summary>
        /// Fetches an entity using the specified id.
        /// </summary>
        /// <param name="filterBucket">The bucket containing the filter and relations to fetch the collection from the database.</param>
        /// <param name="prefetchPath">The pre fetch path which determines additional entities to fetch from.</param>
        /// <param name="excludedIncludedFields">The included or excluded fields to use when fetching the data.</param>
        /// <param name="adapter">The data access adapter to use when fetching the entity from the database. Use null to create a new one.</param>
        /// <returns>An entity instance.</returns>
        public static async Task<TEntity> FetchEntityAsync<TEntity>(RelationPredicateBucket filterBucket, IPrefetchPath2 prefetchPath, ExcludeIncludeFieldsList excludedIncludedFields, IDataAccessAdapter adapter = null) where TEntity : EntityBase2
        {
            EntityCollection<TEntity> entityCollection = await DataAccessAdapter.FetchEntityCollectionAsync<TEntity>(filterBucket, 1, null, prefetchPath, excludedIncludedFields, adapter);

            return entityCollection.FirstOrDefault();
        }

        /// <summary>
        /// Fetches an entity using the specified filter.
        /// </summary>
        /// <param name="filterBucket">The bucket containing the filter and relations to fetch the collection from the database.</param>
        /// <param name="prefetchPath">The pre fetch path which determines additional entities to fetch from.</param>
        /// <param name="excludedIncludedFields">The included or excluded fields to use when fetching the data.</param>
        /// <param name="adapter">The data access adapter to use when fetching the entity collection from the database. Use null to create a new one.</param>
        /// <returns>An entity instance.</returns>
        public static TEntity FetchEntity<TEntity>(RelationPredicateBucket filterBucket, IPrefetchPath2 prefetchPath, ExcludeIncludeFieldsList excludedIncludedFields, IDataAccessAdapter adapter = null) where TEntity : EntityBase2, new()
        {
            TEntity entity = null;

            EntityCollection<TEntity> entityCollection = DataAccessAdapter.FetchEntityCollection<TEntity>(filterBucket, 0, null, prefetchPath, excludedIncludedFields, adapter);

            if (entityCollection.Count == 0)
            {
                // No entity found
            }
            else if (entityCollection.Count > 1)
            {
                // More than one entity found
            }
            else
            {
                entity = entityCollection[0];
            }

            return entity;
        }

        /// <summary>
        /// Fetches an entity using the specified filter.
        /// </summary>
        /// <param name="filterBucket">The bucket containing the filter and relations to fetch the collection from the database.</param>
        /// <param name="prefetchPath">The pre fetch path which determines additional entities to fetch from.</param>
        /// <param name="excludedIncludedFields">The included or excluded fields to use when fetching the data.</param>
        /// <returns>An entity instance.</returns>
        public static TEntity FetchEntity<TEntity>(QueryParameters queryParameters, IDataAccessAdapter adapter = null) where TEntity : EntityBase2, new()
        {
            TEntity entity = null;

            EntityCollection<TEntity> entityCollection = DataAccessAdapter.FetchEntityCollection<TEntity>(queryParameters, adapter);

            if (entityCollection.Count == 0)
            {
                // No entity found
            }
            else if (entityCollection.Count > 1)
            {
                // More than one entity found
            }
            else
            {
                entity = entityCollection[0];
            }

            return entity;
        }

        /// <summary>
        /// Saves the specified entity to the database.
        /// </summary>
        /// <param name="entity">The entity to save.</param>
        /// <param name="refetchAfterSave">Flag that determines whether the entity is refetches after it has been saved.</param>
        /// <returns>True if saving the entity was successful, False if not.</returns>
        public static bool Save<TEntity>(TEntity entity, bool refetchAfterSave, IDataAccessAdapter adapter = null) where TEntity : EntityBase2, new()
        {
            bool success = false;

            if (adapter == null && entity is CommonEntityBase)
            {
                adapter = (entity as CommonEntityBase).Adapter;
            }

            if (!entity.Fields[0].IsPrimaryKey)
            {
                throw new InvalidOperationException($"The first field of entity '{typeof(TEntity).Name}' is not a primary key field!");
            }

            entity.IsNew = entity.Fields[0].CurrentValue == null; // Assumes primary key field is first field

            if (adapter == null || ((DataAccessAdapter)adapter).IsDisposed)
            {
                using (adapter = new DataAccessAdapter())
                {
                    success = adapter.SaveEntity(entity, refetchAfterSave);
                }
            }
            else
            {
                success = adapter.SaveEntity(entity, refetchAfterSave);
            }

            return success;
        }

        /// <summary>
        /// Saves the specified entity to the database.
        /// </summary>
        /// <param name="entity">The entity to save.</param>
        /// <param name="refetchAfterSave">Flag that determines whether the entity is refetches after it has been saved.</param>
        /// <returns>True if saving the entity was successful, False if not.</returns>
        public static async Task<bool> SaveAsync<TEntity>(TEntity entity, bool refetchAfterSave, IDataAccessAdapter adapter = null) where TEntity : EntityBase2, new()
        {
            bool success = false;

            if (adapter == null && entity is CommonEntityBase)
            {
                adapter = (entity as CommonEntityBase).Adapter;
            }

            if (!entity.Fields[0].IsPrimaryKey)
            {
                throw new InvalidOperationException($"The first field of entity '{typeof(TEntity).Name}' is not a primary key field!");
            }

            entity.IsNew = entity.Fields[0].CurrentValue == null; // Assumes primary key field is first field

            if (adapter == null || ((DataAccessAdapter)adapter).IsDisposed)
            {
                using (adapter = new DataAccessAdapter())
                {
                    success = await adapter.SaveEntityAsync(entity, refetchAfterSave);
                }
            }
            else
            {
                success = await adapter.SaveEntityAsync(entity, refetchAfterSave);
            }

            return success;
        }

        /// <summary>
        /// Saves the specified entities to the database.
        /// </summary>
        /// <param name="entities">The entities to save.</param>
        /// <param name="refetchAfterSave">Flag that determines whether the entities are refetched after it has been saved.</param>
        /// <param name="recurse">Flag that determines whether the entities are saved recursively.</param>
        /// <returns>The amount of persisted entities.</returns>
        public static int Save(IEntityCollection2 entities, bool refetchAfterSave, bool recurse)
        {
            int persistedEntities;

            using (DataAccessAdapter adapter = new DataAccessAdapter())
            {
                persistedEntities = adapter.SaveEntityCollection(entities, refetchAfterSave, recurse);
            }

            return persistedEntities;
        }

        /// <summary>
        /// Saves the specified entities to the database.
        /// </summary>
        /// <param name="entities">The entities to save.</param>
        /// <param name="refetchAfterSave">Flag that determines whether the entities are refetched after it has been saved.</param>
        /// <param name="recurse">Flag that determines whether the entities are saved recursively.</param>
        /// <returns>The amount of persisted entities.</returns>
        public static async Task<int> SaveAsync(IEntityCollection2 entities, bool refetchAfterSave, bool recurse)
        {
            int persistedEntities;

            using (DataAccessAdapter adapter = new DataAccessAdapter())
            {
                persistedEntities = await adapter.SaveEntityCollectionAsync(entities, refetchAfterSave, recurse);
            }

            return persistedEntities;
        }

        /// <summary>
        /// Delete the specified entities.
        /// </summary>
        /// <param name="entities">The entities to delete.</param>
        /// <returns>The amount of deleted entities.</returns>
        public static int Delete(IEntityCollection2 entities)
        {
            int deletedEntities;

            using (DataAccessAdapter adapter = new DataAccessAdapter())
            {
                deletedEntities = adapter.DeleteEntityCollection(entities);
            }

            return deletedEntities;
        }

        /// <summary>
        /// Delete the specified entities.
        /// </summary>
        /// <param name="entities">The entities to delete.</param>
        /// <returns>The amount of deleted entities.</returns>
        public static async Task<int> DeleteAsync(IEntityCollection2 entities)
        {
            int deletedEntities;

            using (DataAccessAdapter adapter = new DataAccessAdapter())
            {
                deletedEntities = await adapter.DeleteEntityCollectionAsync(entities);
            }

            return deletedEntities;
        }

        /// <summary>
        /// Updates entities directly in the database without fetching them first.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity to update.</typeparam>
        /// <param name="entityWithNewValues">The entity containing the new value(s) to use in the update.</param>
        /// <param name="filterBucket">The bucket containing the filter and relations to update the entities in the database.</param>
        /// <param name="adapter">The data access adapter to use when updating the entities in the database. Use null to create a new one.</param>
        /// <returns>An <see cref="int"/> instance containing the amount of entities that were updated.</returns>
        /// <remarks>Updating entities directly will NOT fire the validators!</remarks>
        public static int UpdateEntitiesDirectly<TEntity>(TEntity entityWithNewValues, RelationPredicateBucket filterBucket, IDataAccessAdapter adapter = null) where TEntity : EntityBase2, new()
        {
            int toReturn = 0;

            if (adapter == null)
            {
                using (adapter = new DataAccessAdapter())
                {
                    toReturn = adapter.UpdateEntitiesDirectly(entityWithNewValues, filterBucket);
                }
            }
            else
            {
                toReturn = adapter.UpdateEntitiesDirectly(entityWithNewValues, filterBucket);
            }

            return toReturn;
        }

        /// <summary>
        /// Deletes an entity from the database.
        /// </summary>
        /// <param name="entity">The entity to delete.</param>
        /// <param name="adapter">The adapter to use when deleting the entity.</param>
        /// <returns>True if deleting the entity was successful, False if not.</returns>
        public static bool Delete<TEntity>(TEntity entity, IDataAccessAdapter adapter = null) where TEntity : EntityBase2, new()
        {
            bool success = false;

            if (adapter == null)
            {
                using (adapter = new DataAccessAdapter())
                {
                    if (entity is CommonEntityBase)
                    {
                        (entity as CommonEntityBase).Adapter = adapter;
                    }

                    success = adapter.DeleteEntity(entity);
                }
            }
            else
            {
                success = adapter.DeleteEntity(entity);
            }

            return success;
        }

        /// <summary>
        /// Deletes an entity from the database.
        /// </summary>
        /// <param name="entity">The entity to delete.</param>
        /// <param name="adapter">The adapter to use when deleting the entity.</param>
        /// <returns>True if deleting the entity was successful, False if not.</returns>
        public static async Task<bool> DeleteAsync<TEntity>(TEntity entity, IDataAccessAdapter adapter = null) where TEntity : EntityBase2, new()
        {
            bool success = false;

            if (adapter == null)
            {
                using (adapter = new DataAccessAdapter())
                {
                    if (entity is CommonEntityBase)
                    {
                        (entity as CommonEntityBase).Adapter = adapter;
                    }

                    success = await adapter.DeleteEntityAsync(entity);
                }
            }
            else
            {
                success = await adapter.DeleteEntityAsync(entity);
            }

            return success;
        }

        /// <summary>
        /// Deletes entities directly in the database without fetching them first.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity to delete.</typeparam>
        /// <param name="filterBucket">The bucket containing the filter and relations to delete the entities in the database.</param>
        /// <param name="adapter">The data access adapter to use when deleting the entities in the database. Use null to create a new one.</param>
        /// <returns>An <see cref="int"/> instance containing the amount of entities that were deleted.</returns>
        /// <remarks>Deleting entities directly will NOT fire the validators!</remarks>
        public static int DeleteEntitiesDirectly<TEntity>(RelationPredicateBucket filterBucket, IDataAccessAdapter adapter = null)
        {
            int toReturn = 0;

            if (adapter == null)
            {
                using (adapter = new DataAccessAdapter())
                {
                    adapter.DeleteEntitiesDirectly(typeof(TEntity), filterBucket);
                }
            }
            else
            {
                adapter.DeleteEntitiesDirectly(typeof(TEntity), filterBucket);
            }

            return toReturn;
        }

        #endregion

        protected override void Dispose(bool isDisposing)
        {
            this.IsDisposed = true;
            base.Dispose(isDisposing);
        }
    }
}
