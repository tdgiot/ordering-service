﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 5.8
// Code is generated on: 
// Code is generated using templates: Crave.TemplateBindings
// Templates vendor: Crave Interactive
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Crave.Ordering.Shared.Data.LLBLGen.FactoryClasses;
using Crave.Ordering.Shared.Data.LLBLGen;

// ReSharper disable All
namespace Crave.Ordering.Shared.Data.LLBLGen.HelperClasses
{
	/// <summary>Field Creation Class for entity AlterationEntity</summary>
	public partial class AlterationFields
	{
		/// <summary>Creates a new AlterationEntity.AlterationId field instance</summary>
		public static EntityField2 AlterationId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationFieldIndex.AlterationId); }
		}
		/// <summary>Creates a new AlterationEntity.ParentAlterationId field instance</summary>
		public static EntityField2 ParentAlterationId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationFieldIndex.ParentAlterationId); }
		}
		/// <summary>Creates a new AlterationEntity.Version field instance</summary>
		public static EntityField2 Version 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationFieldIndex.Version); }
		}
		/// <summary>Creates a new AlterationEntity.Name field instance</summary>
		public static EntityField2 Name 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationFieldIndex.Name); }
		}
		/// <summary>Creates a new AlterationEntity.FriendlyName field instance</summary>
		public static EntityField2 FriendlyName 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationFieldIndex.FriendlyName); }
		}
		/// <summary>Creates a new AlterationEntity.Description field instance</summary>
		public static EntityField2 Description 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationFieldIndex.Description); }
		}
		/// <summary>Creates a new AlterationEntity.Type field instance</summary>
		public static EntityField2 Type 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationFieldIndex.Type); }
		}
		/// <summary>Creates a new AlterationEntity.LayoutType field instance</summary>
		public static EntityField2 LayoutType 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationFieldIndex.LayoutType); }
		}
		/// <summary>Creates a new AlterationEntity.MinOptions field instance</summary>
		public static EntityField2 MinOptions 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationFieldIndex.MinOptions); }
		}
		/// <summary>Creates a new AlterationEntity.MaxOptions field instance</summary>
		public static EntityField2 MaxOptions 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationFieldIndex.MaxOptions); }
		}
		/// <summary>Creates a new AlterationEntity.GenericalterationId field instance</summary>
		public static EntityField2 GenericalterationId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationFieldIndex.GenericalterationId); }
		}
		/// <summary>Creates a new AlterationEntity.PosalterationId field instance</summary>
		public static EntityField2 PosalterationId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationFieldIndex.PosalterationId); }
		}
		/// <summary>Creates a new AlterationEntity.CompanyId field instance</summary>
		public static EntityField2 CompanyId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationFieldIndex.CompanyId); }
		}
		/// <summary>Creates a new AlterationEntity.AvailableOnOtoucho field instance</summary>
		public static EntityField2 AvailableOnOtoucho 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationFieldIndex.AvailableOnOtoucho); }
		}
		/// <summary>Creates a new AlterationEntity.AvailableOnObymobi field instance</summary>
		public static EntityField2 AvailableOnObymobi 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationFieldIndex.AvailableOnObymobi); }
		}
		/// <summary>Creates a new AlterationEntity.StartTime field instance</summary>
		public static EntityField2 StartTime 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationFieldIndex.StartTime); }
		}
		/// <summary>Creates a new AlterationEntity.EndTime field instance</summary>
		public static EntityField2 EndTime 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationFieldIndex.EndTime); }
		}
		/// <summary>Creates a new AlterationEntity.MinLeadMinutes field instance</summary>
		public static EntityField2 MinLeadMinutes 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationFieldIndex.MinLeadMinutes); }
		}
		/// <summary>Creates a new AlterationEntity.MaxLeadHours field instance</summary>
		public static EntityField2 MaxLeadHours 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationFieldIndex.MaxLeadHours); }
		}
		/// <summary>Creates a new AlterationEntity.IntervalMinutes field instance</summary>
		public static EntityField2 IntervalMinutes 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationFieldIndex.IntervalMinutes); }
		}
		/// <summary>Creates a new AlterationEntity.ShowDatePicker field instance</summary>
		public static EntityField2 ShowDatePicker 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationFieldIndex.ShowDatePicker); }
		}
		/// <summary>Creates a new AlterationEntity.Value field instance</summary>
		public static EntityField2 Value 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationFieldIndex.Value); }
		}
		/// <summary>Creates a new AlterationEntity.OrderLevelEnabled field instance</summary>
		public static EntityField2 OrderLevelEnabled 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationFieldIndex.OrderLevelEnabled); }
		}
		/// <summary>Creates a new AlterationEntity.SortOrder field instance</summary>
		public static EntityField2 SortOrder 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationFieldIndex.SortOrder); }
		}
		/// <summary>Creates a new AlterationEntity.Visible field instance</summary>
		public static EntityField2 Visible 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationFieldIndex.Visible); }
		}
		/// <summary>Creates a new AlterationEntity.PriceAddition field instance</summary>
		public static EntityField2 PriceAddition 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationFieldIndex.PriceAddition); }
		}
		/// <summary>Creates a new AlterationEntity.OriginalAlterationId field instance</summary>
		public static EntityField2 OriginalAlterationId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationFieldIndex.OriginalAlterationId); }
		}
		/// <summary>Creates a new AlterationEntity.AllowDuplicates field instance</summary>
		public static EntityField2 AllowDuplicates 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationFieldIndex.AllowDuplicates); }
		}
		/// <summary>Creates a new AlterationEntity.StartTimeUTC field instance</summary>
		public static EntityField2 StartTimeUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationFieldIndex.StartTimeUTC); }
		}
		/// <summary>Creates a new AlterationEntity.EndTimeUTC field instance</summary>
		public static EntityField2 EndTimeUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationFieldIndex.EndTimeUTC); }
		}
		/// <summary>Creates a new AlterationEntity.CreatedUTC field instance</summary>
		public static EntityField2 CreatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationFieldIndex.CreatedUTC); }
		}
		/// <summary>Creates a new AlterationEntity.CreatedBy field instance</summary>
		public static EntityField2 CreatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationFieldIndex.CreatedBy); }
		}
		/// <summary>Creates a new AlterationEntity.UpdatedUTC field instance</summary>
		public static EntityField2 UpdatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationFieldIndex.UpdatedUTC); }
		}
		/// <summary>Creates a new AlterationEntity.UpdatedBy field instance</summary>
		public static EntityField2 UpdatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationFieldIndex.UpdatedBy); }
		}
		/// <summary>Creates a new AlterationEntity.ExternalProductId field instance</summary>
		public static EntityField2 ExternalProductId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationFieldIndex.ExternalProductId); }
		}
		/// <summary>Creates a new AlterationEntity.IsAvailable field instance</summary>
		public static EntityField2 IsAvailable 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationFieldIndex.IsAvailable); }
		}
	}

	/// <summary>Field Creation Class for entity AlterationitemEntity</summary>
	public partial class AlterationitemFields
	{
		/// <summary>Creates a new AlterationitemEntity.AlterationitemId field instance</summary>
		public static EntityField2 AlterationitemId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationitemFieldIndex.AlterationitemId); }
		}
		/// <summary>Creates a new AlterationitemEntity.ParentCompanyId field instance</summary>
		public static EntityField2 ParentCompanyId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationitemFieldIndex.ParentCompanyId); }
		}
		/// <summary>Creates a new AlterationitemEntity.Version field instance</summary>
		public static EntityField2 Version 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationitemFieldIndex.Version); }
		}
		/// <summary>Creates a new AlterationitemEntity.AlterationId field instance</summary>
		public static EntityField2 AlterationId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationitemFieldIndex.AlterationId); }
		}
		/// <summary>Creates a new AlterationitemEntity.AlterationoptionId field instance</summary>
		public static EntityField2 AlterationoptionId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationitemFieldIndex.AlterationoptionId); }
		}
		/// <summary>Creates a new AlterationitemEntity.SelectedOnDefault field instance</summary>
		public static EntityField2 SelectedOnDefault 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationitemFieldIndex.SelectedOnDefault); }
		}
		/// <summary>Creates a new AlterationitemEntity.SortOrder field instance</summary>
		public static EntityField2 SortOrder 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationitemFieldIndex.SortOrder); }
		}
		/// <summary>Creates a new AlterationitemEntity.PosalterationitemId field instance</summary>
		public static EntityField2 PosalterationitemId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationitemFieldIndex.PosalterationitemId); }
		}
		/// <summary>Creates a new AlterationitemEntity.GenericalterationitemId field instance</summary>
		public static EntityField2 GenericalterationitemId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationitemFieldIndex.GenericalterationitemId); }
		}
		/// <summary>Creates a new AlterationitemEntity.CreatedUTC field instance</summary>
		public static EntityField2 CreatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationitemFieldIndex.CreatedUTC); }
		}
		/// <summary>Creates a new AlterationitemEntity.CreatedBy field instance</summary>
		public static EntityField2 CreatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationitemFieldIndex.CreatedBy); }
		}
		/// <summary>Creates a new AlterationitemEntity.UpdatedUTC field instance</summary>
		public static EntityField2 UpdatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationitemFieldIndex.UpdatedUTC); }
		}
		/// <summary>Creates a new AlterationitemEntity.UpdatedBy field instance</summary>
		public static EntityField2 UpdatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationitemFieldIndex.UpdatedBy); }
		}
	}

	/// <summary>Field Creation Class for entity AlterationitemAlterationEntity</summary>
	public partial class AlterationitemAlterationFields
	{
		/// <summary>Creates a new AlterationitemAlterationEntity.AlterationitemAlterationId field instance</summary>
		public static EntityField2 AlterationitemAlterationId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationitemAlterationFieldIndex.AlterationitemAlterationId); }
		}
		/// <summary>Creates a new AlterationitemAlterationEntity.ParentCompanyId field instance</summary>
		public static EntityField2 ParentCompanyId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationitemAlterationFieldIndex.ParentCompanyId); }
		}
		/// <summary>Creates a new AlterationitemAlterationEntity.AlterationitemId field instance</summary>
		public static EntityField2 AlterationitemId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationitemAlterationFieldIndex.AlterationitemId); }
		}
		/// <summary>Creates a new AlterationitemAlterationEntity.AlterationId field instance</summary>
		public static EntityField2 AlterationId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationitemAlterationFieldIndex.AlterationId); }
		}
		/// <summary>Creates a new AlterationitemAlterationEntity.SortOrder field instance</summary>
		public static EntityField2 SortOrder 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationitemAlterationFieldIndex.SortOrder); }
		}
		/// <summary>Creates a new AlterationitemAlterationEntity.CreatedUTC field instance</summary>
		public static EntityField2 CreatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationitemAlterationFieldIndex.CreatedUTC); }
		}
		/// <summary>Creates a new AlterationitemAlterationEntity.CreatedBy field instance</summary>
		public static EntityField2 CreatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationitemAlterationFieldIndex.CreatedBy); }
		}
		/// <summary>Creates a new AlterationitemAlterationEntity.UpdatedUTC field instance</summary>
		public static EntityField2 UpdatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationitemAlterationFieldIndex.UpdatedUTC); }
		}
		/// <summary>Creates a new AlterationitemAlterationEntity.UpdatedBy field instance</summary>
		public static EntityField2 UpdatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationitemAlterationFieldIndex.UpdatedBy); }
		}
	}

	/// <summary>Field Creation Class for entity AlterationoptionEntity</summary>
	public partial class AlterationoptionFields
	{
		/// <summary>Creates a new AlterationoptionEntity.AlterationoptionId field instance</summary>
		public static EntityField2 AlterationoptionId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationoptionFieldIndex.AlterationoptionId); }
		}
		/// <summary>Creates a new AlterationoptionEntity.PosalterationoptionId field instance</summary>
		public static EntityField2 PosalterationoptionId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationoptionFieldIndex.PosalterationoptionId); }
		}
		/// <summary>Creates a new AlterationoptionEntity.GenericalterationoptionId field instance</summary>
		public static EntityField2 GenericalterationoptionId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationoptionFieldIndex.GenericalterationoptionId); }
		}
		/// <summary>Creates a new AlterationoptionEntity.Version field instance</summary>
		public static EntityField2 Version 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationoptionFieldIndex.Version); }
		}
		/// <summary>Creates a new AlterationoptionEntity.Name field instance</summary>
		public static EntityField2 Name 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationoptionFieldIndex.Name); }
		}
		/// <summary>Creates a new AlterationoptionEntity.FriendlyName field instance</summary>
		public static EntityField2 FriendlyName 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationoptionFieldIndex.FriendlyName); }
		}
		/// <summary>Creates a new AlterationoptionEntity.Type field instance</summary>
		public static EntityField2 Type 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationoptionFieldIndex.Type); }
		}
		/// <summary>Creates a new AlterationoptionEntity.PriceIn field instance</summary>
		public static EntityField2 PriceIn 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationoptionFieldIndex.PriceIn); }
		}
		/// <summary>Creates a new AlterationoptionEntity.PriceAddition field instance</summary>
		public static EntityField2 PriceAddition 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationoptionFieldIndex.PriceAddition); }
		}
		/// <summary>Creates a new AlterationoptionEntity.Description field instance</summary>
		public static EntityField2 Description 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationoptionFieldIndex.Description); }
		}
		/// <summary>Creates a new AlterationoptionEntity.PosproductId field instance</summary>
		public static EntityField2 PosproductId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationoptionFieldIndex.PosproductId); }
		}
		/// <summary>Creates a new AlterationoptionEntity.IsProductRelated field instance</summary>
		public static EntityField2 IsProductRelated 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationoptionFieldIndex.IsProductRelated); }
		}
		/// <summary>Creates a new AlterationoptionEntity.CompanyId field instance</summary>
		public static EntityField2 CompanyId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationoptionFieldIndex.CompanyId); }
		}
		/// <summary>Creates a new AlterationoptionEntity.StartTime field instance</summary>
		public static EntityField2 StartTime 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationoptionFieldIndex.StartTime); }
		}
		/// <summary>Creates a new AlterationoptionEntity.EndTime field instance</summary>
		public static EntityField2 EndTime 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationoptionFieldIndex.EndTime); }
		}
		/// <summary>Creates a new AlterationoptionEntity.MinLeadMinutes field instance</summary>
		public static EntityField2 MinLeadMinutes 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationoptionFieldIndex.MinLeadMinutes); }
		}
		/// <summary>Creates a new AlterationoptionEntity.MaxLeadHours field instance</summary>
		public static EntityField2 MaxLeadHours 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationoptionFieldIndex.MaxLeadHours); }
		}
		/// <summary>Creates a new AlterationoptionEntity.IntervalMinutes field instance</summary>
		public static EntityField2 IntervalMinutes 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationoptionFieldIndex.IntervalMinutes); }
		}
		/// <summary>Creates a new AlterationoptionEntity.ShowDatePicker field instance</summary>
		public static EntityField2 ShowDatePicker 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationoptionFieldIndex.ShowDatePicker); }
		}
		/// <summary>Creates a new AlterationoptionEntity.CreatedBy field instance</summary>
		public static EntityField2 CreatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationoptionFieldIndex.CreatedBy); }
		}
		/// <summary>Creates a new AlterationoptionEntity.UpdatedBy field instance</summary>
		public static EntityField2 UpdatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationoptionFieldIndex.UpdatedBy); }
		}
		/// <summary>Creates a new AlterationoptionEntity.StartTimeUTC field instance</summary>
		public static EntityField2 StartTimeUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationoptionFieldIndex.StartTimeUTC); }
		}
		/// <summary>Creates a new AlterationoptionEntity.EndTimeUTC field instance</summary>
		public static EntityField2 EndTimeUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationoptionFieldIndex.EndTimeUTC); }
		}
		/// <summary>Creates a new AlterationoptionEntity.CreatedUTC field instance</summary>
		public static EntityField2 CreatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationoptionFieldIndex.CreatedUTC); }
		}
		/// <summary>Creates a new AlterationoptionEntity.UpdatedUTC field instance</summary>
		public static EntityField2 UpdatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationoptionFieldIndex.UpdatedUTC); }
		}
		/// <summary>Creates a new AlterationoptionEntity.TaxTariffId field instance</summary>
		public static EntityField2 TaxTariffId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationoptionFieldIndex.TaxTariffId); }
		}
		/// <summary>Creates a new AlterationoptionEntity.IsAlcoholic field instance</summary>
		public static EntityField2 IsAlcoholic 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationoptionFieldIndex.IsAlcoholic); }
		}
		/// <summary>Creates a new AlterationoptionEntity.InheritName field instance</summary>
		public static EntityField2 InheritName 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationoptionFieldIndex.InheritName); }
		}
		/// <summary>Creates a new AlterationoptionEntity.InheritPrice field instance</summary>
		public static EntityField2 InheritPrice 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationoptionFieldIndex.InheritPrice); }
		}
		/// <summary>Creates a new AlterationoptionEntity.InheritTaxTariff field instance</summary>
		public static EntityField2 InheritTaxTariff 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationoptionFieldIndex.InheritTaxTariff); }
		}
		/// <summary>Creates a new AlterationoptionEntity.InheritIsAlcoholic field instance</summary>
		public static EntityField2 InheritIsAlcoholic 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationoptionFieldIndex.InheritIsAlcoholic); }
		}
		/// <summary>Creates a new AlterationoptionEntity.InheritTags field instance</summary>
		public static EntityField2 InheritTags 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationoptionFieldIndex.InheritTags); }
		}
		/// <summary>Creates a new AlterationoptionEntity.InheritCustomText field instance</summary>
		public static EntityField2 InheritCustomText 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationoptionFieldIndex.InheritCustomText); }
		}
		/// <summary>Creates a new AlterationoptionEntity.ProductId field instance</summary>
		public static EntityField2 ProductId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationoptionFieldIndex.ProductId); }
		}
		/// <summary>Creates a new AlterationoptionEntity.ExternalProductId field instance</summary>
		public static EntityField2 ExternalProductId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationoptionFieldIndex.ExternalProductId); }
		}
		/// <summary>Creates a new AlterationoptionEntity.IsAvailable field instance</summary>
		public static EntityField2 IsAvailable 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationoptionFieldIndex.IsAvailable); }
		}
		/// <summary>Creates a new AlterationoptionEntity.ExternalIdentifier field instance</summary>
		public static EntityField2 ExternalIdentifier 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationoptionFieldIndex.ExternalIdentifier); }
		}
	}

	/// <summary>Field Creation Class for entity AlterationoptionTagEntity</summary>
	public partial class AlterationoptionTagFields
	{
		/// <summary>Creates a new AlterationoptionTagEntity.AlterationoptionTagId field instance</summary>
		public static EntityField2 AlterationoptionTagId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationoptionTagFieldIndex.AlterationoptionTagId); }
		}
		/// <summary>Creates a new AlterationoptionTagEntity.AlterationoptionId field instance</summary>
		public static EntityField2 AlterationoptionId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationoptionTagFieldIndex.AlterationoptionId); }
		}
		/// <summary>Creates a new AlterationoptionTagEntity.TagId field instance</summary>
		public static EntityField2 TagId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationoptionTagFieldIndex.TagId); }
		}
		/// <summary>Creates a new AlterationoptionTagEntity.ParentCompanyId field instance</summary>
		public static EntityField2 ParentCompanyId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationoptionTagFieldIndex.ParentCompanyId); }
		}
		/// <summary>Creates a new AlterationoptionTagEntity.CreatedUTC field instance</summary>
		public static EntityField2 CreatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationoptionTagFieldIndex.CreatedUTC); }
		}
		/// <summary>Creates a new AlterationoptionTagEntity.UpdatedUTC field instance</summary>
		public static EntityField2 UpdatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationoptionTagFieldIndex.UpdatedUTC); }
		}
	}

	/// <summary>Field Creation Class for entity AlterationProductEntity</summary>
	public partial class AlterationProductFields
	{
		/// <summary>Creates a new AlterationProductEntity.AlterationProductId field instance</summary>
		public static EntityField2 AlterationProductId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationProductFieldIndex.AlterationProductId); }
		}
		/// <summary>Creates a new AlterationProductEntity.ParentCompanyId field instance</summary>
		public static EntityField2 ParentCompanyId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationProductFieldIndex.ParentCompanyId); }
		}
		/// <summary>Creates a new AlterationProductEntity.AlterationId field instance</summary>
		public static EntityField2 AlterationId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationProductFieldIndex.AlterationId); }
		}
		/// <summary>Creates a new AlterationProductEntity.ProductId field instance</summary>
		public static EntityField2 ProductId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationProductFieldIndex.ProductId); }
		}
		/// <summary>Creates a new AlterationProductEntity.SortOrder field instance</summary>
		public static EntityField2 SortOrder 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationProductFieldIndex.SortOrder); }
		}
		/// <summary>Creates a new AlterationProductEntity.Visible field instance</summary>
		public static EntityField2 Visible 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationProductFieldIndex.Visible); }
		}
		/// <summary>Creates a new AlterationProductEntity.CreatedUTC field instance</summary>
		public static EntityField2 CreatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationProductFieldIndex.CreatedUTC); }
		}
		/// <summary>Creates a new AlterationProductEntity.CreatedBy field instance</summary>
		public static EntityField2 CreatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationProductFieldIndex.CreatedBy); }
		}
		/// <summary>Creates a new AlterationProductEntity.UpdatedUTC field instance</summary>
		public static EntityField2 UpdatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationProductFieldIndex.UpdatedUTC); }
		}
		/// <summary>Creates a new AlterationProductEntity.UpdatedBy field instance</summary>
		public static EntityField2 UpdatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AlterationProductFieldIndex.UpdatedBy); }
		}
	}

	/// <summary>Field Creation Class for entity AnalyticsProcessingTaskEntity</summary>
	public partial class AnalyticsProcessingTaskFields
	{
		/// <summary>Creates a new AnalyticsProcessingTaskEntity.AnalyticsProcessingTaskId field instance</summary>
		public static EntityField2 AnalyticsProcessingTaskId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AnalyticsProcessingTaskFieldIndex.AnalyticsProcessingTaskId); }
		}
		/// <summary>Creates a new AnalyticsProcessingTaskEntity.OrderId field instance</summary>
		public static EntityField2 OrderId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AnalyticsProcessingTaskFieldIndex.OrderId); }
		}
		/// <summary>Creates a new AnalyticsProcessingTaskEntity.EventAction field instance</summary>
		public static EntityField2 EventAction 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AnalyticsProcessingTaskFieldIndex.EventAction); }
		}
		/// <summary>Creates a new AnalyticsProcessingTaskEntity.EventLabel field instance</summary>
		public static EntityField2 EventLabel 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AnalyticsProcessingTaskFieldIndex.EventLabel); }
		}
		/// <summary>Creates a new AnalyticsProcessingTaskEntity.CreatedUTC field instance</summary>
		public static EntityField2 CreatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AnalyticsProcessingTaskFieldIndex.CreatedUTC); }
		}
		/// <summary>Creates a new AnalyticsProcessingTaskEntity.UpdatedUTC field instance</summary>
		public static EntityField2 UpdatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(AnalyticsProcessingTaskFieldIndex.UpdatedUTC); }
		}
	}

	/// <summary>Field Creation Class for entity BusinesshourEntity</summary>
	public partial class BusinesshourFields
	{
		/// <summary>Creates a new BusinesshourEntity.BusinesshoursId field instance</summary>
		public static EntityField2 BusinesshoursId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(BusinesshourFieldIndex.BusinesshoursId); }
		}
		/// <summary>Creates a new BusinesshourEntity.CompanyId field instance</summary>
		public static EntityField2 CompanyId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(BusinesshourFieldIndex.CompanyId); }
		}
		/// <summary>Creates a new BusinesshourEntity.PointOfInterestId field instance</summary>
		public static EntityField2 PointOfInterestId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(BusinesshourFieldIndex.PointOfInterestId); }
		}
		/// <summary>Creates a new BusinesshourEntity.DayOfWeekAndTime field instance</summary>
		public static EntityField2 DayOfWeekAndTime 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(BusinesshourFieldIndex.DayOfWeekAndTime); }
		}
		/// <summary>Creates a new BusinesshourEntity.Opening field instance</summary>
		public static EntityField2 Opening 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(BusinesshourFieldIndex.Opening); }
		}
		/// <summary>Creates a new BusinesshourEntity.CreatedBy field instance</summary>
		public static EntityField2 CreatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(BusinesshourFieldIndex.CreatedBy); }
		}
		/// <summary>Creates a new BusinesshourEntity.UpdatedBy field instance</summary>
		public static EntityField2 UpdatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(BusinesshourFieldIndex.UpdatedBy); }
		}
		/// <summary>Creates a new BusinesshourEntity.CreatedUTC field instance</summary>
		public static EntityField2 CreatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(BusinesshourFieldIndex.CreatedUTC); }
		}
		/// <summary>Creates a new BusinesshourEntity.UpdatedUTC field instance</summary>
		public static EntityField2 UpdatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(BusinesshourFieldIndex.UpdatedUTC); }
		}
		/// <summary>Creates a new BusinesshourEntity.OutletId field instance</summary>
		public static EntityField2 OutletId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(BusinesshourFieldIndex.OutletId); }
		}
	}

	/// <summary>Field Creation Class for entity BusinesshoursexceptionEntity</summary>
	public partial class BusinesshoursexceptionFields
	{
		/// <summary>Creates a new BusinesshoursexceptionEntity.BusinesshoursexceptionId field instance</summary>
		public static EntityField2 BusinesshoursexceptionId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(BusinesshoursexceptionFieldIndex.BusinesshoursexceptionId); }
		}
		/// <summary>Creates a new BusinesshoursexceptionEntity.CompanyId field instance</summary>
		public static EntityField2 CompanyId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(BusinesshoursexceptionFieldIndex.CompanyId); }
		}
		/// <summary>Creates a new BusinesshoursexceptionEntity.Opened field instance</summary>
		public static EntityField2 Opened 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(BusinesshoursexceptionFieldIndex.Opened); }
		}
		/// <summary>Creates a new BusinesshoursexceptionEntity.FromDateTime field instance</summary>
		public static EntityField2 FromDateTime 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(BusinesshoursexceptionFieldIndex.FromDateTime); }
		}
		/// <summary>Creates a new BusinesshoursexceptionEntity.TillDateTime field instance</summary>
		public static EntityField2 TillDateTime 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(BusinesshoursexceptionFieldIndex.TillDateTime); }
		}
		/// <summary>Creates a new BusinesshoursexceptionEntity.CreatedBy field instance</summary>
		public static EntityField2 CreatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(BusinesshoursexceptionFieldIndex.CreatedBy); }
		}
		/// <summary>Creates a new BusinesshoursexceptionEntity.UpdatedBy field instance</summary>
		public static EntityField2 UpdatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(BusinesshoursexceptionFieldIndex.UpdatedBy); }
		}
		/// <summary>Creates a new BusinesshoursexceptionEntity.CreatedUTC field instance</summary>
		public static EntityField2 CreatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(BusinesshoursexceptionFieldIndex.CreatedUTC); }
		}
		/// <summary>Creates a new BusinesshoursexceptionEntity.UpdatedUTC field instance</summary>
		public static EntityField2 UpdatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(BusinesshoursexceptionFieldIndex.UpdatedUTC); }
		}
	}

	/// <summary>Field Creation Class for entity CategoryEntity</summary>
	public partial class CategoryFields
	{
		/// <summary>Creates a new CategoryEntity.CategoryId field instance</summary>
		public static EntityField2 CategoryId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CategoryFieldIndex.CategoryId); }
		}
		/// <summary>Creates a new CategoryEntity.CompanyId field instance</summary>
		public static EntityField2 CompanyId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CategoryFieldIndex.CompanyId); }
		}
		/// <summary>Creates a new CategoryEntity.MenuId field instance</summary>
		public static EntityField2 MenuId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CategoryFieldIndex.MenuId); }
		}
		/// <summary>Creates a new CategoryEntity.ParentCategoryId field instance</summary>
		public static EntityField2 ParentCategoryId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CategoryFieldIndex.ParentCategoryId); }
		}
		/// <summary>Creates a new CategoryEntity.GenericcategoryId field instance</summary>
		public static EntityField2 GenericcategoryId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CategoryFieldIndex.GenericcategoryId); }
		}
		/// <summary>Creates a new CategoryEntity.RouteId field instance</summary>
		public static EntityField2 RouteId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CategoryFieldIndex.RouteId); }
		}
		/// <summary>Creates a new CategoryEntity.ScheduleId field instance</summary>
		public static EntityField2 ScheduleId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CategoryFieldIndex.ScheduleId); }
		}
		/// <summary>Creates a new CategoryEntity.Name field instance</summary>
		public static EntityField2 Name 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CategoryFieldIndex.Name); }
		}
		/// <summary>Creates a new CategoryEntity.SortOrder field instance</summary>
		public static EntityField2 SortOrder 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CategoryFieldIndex.SortOrder); }
		}
		/// <summary>Creates a new CategoryEntity.PoscategoryId field instance</summary>
		public static EntityField2 PoscategoryId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CategoryFieldIndex.PoscategoryId); }
		}
		/// <summary>Creates a new CategoryEntity.ProductId field instance</summary>
		public static EntityField2 ProductId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CategoryFieldIndex.ProductId); }
		}
		/// <summary>Creates a new CategoryEntity.AvailableOnOtoucho field instance</summary>
		public static EntityField2 AvailableOnOtoucho 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CategoryFieldIndex.AvailableOnOtoucho); }
		}
		/// <summary>Creates a new CategoryEntity.AvailableOnObymobi field instance</summary>
		public static EntityField2 AvailableOnObymobi 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CategoryFieldIndex.AvailableOnObymobi); }
		}
		/// <summary>Creates a new CategoryEntity.Rateable field instance</summary>
		public static EntityField2 Rateable 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CategoryFieldIndex.Rateable); }
		}
		/// <summary>Creates a new CategoryEntity.Visible field instance</summary>
		public static EntityField2 Visible 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CategoryFieldIndex.Visible); }
		}
		/// <summary>Creates a new CategoryEntity.Type field instance</summary>
		public static EntityField2 Type 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CategoryFieldIndex.Type); }
		}
		/// <summary>Creates a new CategoryEntity.HidePrices field instance</summary>
		public static EntityField2 HidePrices 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CategoryFieldIndex.HidePrices); }
		}
		/// <summary>Creates a new CategoryEntity.AnnouncementAction field instance</summary>
		public static EntityField2 AnnouncementAction 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CategoryFieldIndex.AnnouncementAction); }
		}
		/// <summary>Creates a new CategoryEntity.Color field instance</summary>
		public static EntityField2 Color 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CategoryFieldIndex.Color); }
		}
		/// <summary>Creates a new CategoryEntity.Description field instance</summary>
		public static EntityField2 Description 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CategoryFieldIndex.Description); }
		}
		/// <summary>Creates a new CategoryEntity.Geofencing field instance</summary>
		public static EntityField2 Geofencing 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CategoryFieldIndex.Geofencing); }
		}
		/// <summary>Creates a new CategoryEntity.AllowFreeText field instance</summary>
		public static EntityField2 AllowFreeText 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CategoryFieldIndex.AllowFreeText); }
		}
		/// <summary>Creates a new CategoryEntity.ViewLayoutType field instance</summary>
		public static EntityField2 ViewLayoutType 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CategoryFieldIndex.ViewLayoutType); }
		}
		/// <summary>Creates a new CategoryEntity.DeliveryLocationType field instance</summary>
		public static EntityField2 DeliveryLocationType 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CategoryFieldIndex.DeliveryLocationType); }
		}
		/// <summary>Creates a new CategoryEntity.SupportNotificationType field instance</summary>
		public static EntityField2 SupportNotificationType 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CategoryFieldIndex.SupportNotificationType); }
		}
		/// <summary>Creates a new CategoryEntity.VisibilityType field instance</summary>
		public static EntityField2 VisibilityType 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CategoryFieldIndex.VisibilityType); }
		}
		/// <summary>Creates a new CategoryEntity.ButtonText field instance</summary>
		public static EntityField2 ButtonText 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CategoryFieldIndex.ButtonText); }
		}
		/// <summary>Creates a new CategoryEntity.CustomizeButtonText field instance</summary>
		public static EntityField2 CustomizeButtonText 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CategoryFieldIndex.CustomizeButtonText); }
		}
		/// <summary>Creates a new CategoryEntity.CreatedBy field instance</summary>
		public static EntityField2 CreatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CategoryFieldIndex.CreatedBy); }
		}
		/// <summary>Creates a new CategoryEntity.UpdatedBy field instance</summary>
		public static EntityField2 UpdatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CategoryFieldIndex.UpdatedBy); }
		}
		/// <summary>Creates a new CategoryEntity.CreatedUTC field instance</summary>
		public static EntityField2 CreatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CategoryFieldIndex.CreatedUTC); }
		}
		/// <summary>Creates a new CategoryEntity.UpdatedUTC field instance</summary>
		public static EntityField2 UpdatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CategoryFieldIndex.UpdatedUTC); }
		}
		/// <summary>Creates a new CategoryEntity.ViewType field instance</summary>
		public static EntityField2 ViewType 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CategoryFieldIndex.ViewType); }
		}
		/// <summary>Creates a new CategoryEntity.MenuItemsMustBeLinkedToExternalProduct field instance</summary>
		public static EntityField2 MenuItemsMustBeLinkedToExternalProduct 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CategoryFieldIndex.MenuItemsMustBeLinkedToExternalProduct); }
		}
		/// <summary>Creates a new CategoryEntity.ShowName field instance</summary>
		public static EntityField2 ShowName 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CategoryFieldIndex.ShowName); }
		}
		/// <summary>Creates a new CategoryEntity.CoversType field instance</summary>
		public static EntityField2 CoversType 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CategoryFieldIndex.CoversType); }
		}
	}

	/// <summary>Field Creation Class for entity CheckoutMethodEntity</summary>
	public partial class CheckoutMethodFields
	{
		/// <summary>Creates a new CheckoutMethodEntity.CheckoutMethodId field instance</summary>
		public static EntityField2 CheckoutMethodId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CheckoutMethodFieldIndex.CheckoutMethodId); }
		}
		/// <summary>Creates a new CheckoutMethodEntity.CompanyId field instance</summary>
		public static EntityField2 CompanyId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CheckoutMethodFieldIndex.CompanyId); }
		}
		/// <summary>Creates a new CheckoutMethodEntity.OutletId field instance</summary>
		public static EntityField2 OutletId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CheckoutMethodFieldIndex.OutletId); }
		}
		/// <summary>Creates a new CheckoutMethodEntity.ReceiptTemplateId field instance</summary>
		public static EntityField2 ReceiptTemplateId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CheckoutMethodFieldIndex.ReceiptTemplateId); }
		}
		/// <summary>Creates a new CheckoutMethodEntity.Active field instance</summary>
		public static EntityField2 Active 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CheckoutMethodFieldIndex.Active); }
		}
		/// <summary>Creates a new CheckoutMethodEntity.Name field instance</summary>
		public static EntityField2 Name 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CheckoutMethodFieldIndex.Name); }
		}
		/// <summary>Creates a new CheckoutMethodEntity.Label field instance</summary>
		public static EntityField2 Label 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CheckoutMethodFieldIndex.Label); }
		}
		/// <summary>Creates a new CheckoutMethodEntity.Description field instance</summary>
		public static EntityField2 Description 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CheckoutMethodFieldIndex.Description); }
		}
		/// <summary>Creates a new CheckoutMethodEntity.CheckoutType field instance</summary>
		public static EntityField2 CheckoutType 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CheckoutMethodFieldIndex.CheckoutType); }
		}
		/// <summary>Creates a new CheckoutMethodEntity.ConfirmationActive field instance</summary>
		public static EntityField2 ConfirmationActive 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CheckoutMethodFieldIndex.ConfirmationActive); }
		}
		/// <summary>Creates a new CheckoutMethodEntity.ConfirmationDescription field instance</summary>
		public static EntityField2 ConfirmationDescription 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CheckoutMethodFieldIndex.ConfirmationDescription); }
		}
		/// <summary>Creates a new CheckoutMethodEntity.CreatedUTC field instance</summary>
		public static EntityField2 CreatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CheckoutMethodFieldIndex.CreatedUTC); }
		}
		/// <summary>Creates a new CheckoutMethodEntity.CreatedBy field instance</summary>
		public static EntityField2 CreatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CheckoutMethodFieldIndex.CreatedBy); }
		}
		/// <summary>Creates a new CheckoutMethodEntity.UpdatedUTC field instance</summary>
		public static EntityField2 UpdatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CheckoutMethodFieldIndex.UpdatedUTC); }
		}
		/// <summary>Creates a new CheckoutMethodEntity.UpdatedBy field instance</summary>
		public static EntityField2 UpdatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CheckoutMethodFieldIndex.UpdatedBy); }
		}
		/// <summary>Creates a new CheckoutMethodEntity.PaymentIntegrationConfigurationId field instance</summary>
		public static EntityField2 PaymentIntegrationConfigurationId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CheckoutMethodFieldIndex.PaymentIntegrationConfigurationId); }
		}
		/// <summary>Creates a new CheckoutMethodEntity.OrderConfirmationNotificationMethod field instance</summary>
		public static EntityField2 OrderConfirmationNotificationMethod 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CheckoutMethodFieldIndex.OrderConfirmationNotificationMethod); }
		}
		/// <summary>Creates a new CheckoutMethodEntity.OrderPaymentPendingNotificationMethod field instance</summary>
		public static EntityField2 OrderPaymentPendingNotificationMethod 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CheckoutMethodFieldIndex.OrderPaymentPendingNotificationMethod); }
		}
		/// <summary>Creates a new CheckoutMethodEntity.OrderPaymentFailedNotificationMethod field instance</summary>
		public static EntityField2 OrderPaymentFailedNotificationMethod 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CheckoutMethodFieldIndex.OrderPaymentFailedNotificationMethod); }
		}
		/// <summary>Creates a new CheckoutMethodEntity.OrderReceiptNotificationMethod field instance</summary>
		public static EntityField2 OrderReceiptNotificationMethod 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CheckoutMethodFieldIndex.OrderReceiptNotificationMethod); }
		}
		/// <summary>Creates a new CheckoutMethodEntity.TermsAndConditionsRequired field instance</summary>
		public static EntityField2 TermsAndConditionsRequired 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CheckoutMethodFieldIndex.TermsAndConditionsRequired); }
		}
		/// <summary>Creates a new CheckoutMethodEntity.TermsAndConditionsLabel field instance</summary>
		public static EntityField2 TermsAndConditionsLabel 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CheckoutMethodFieldIndex.TermsAndConditionsLabel); }
		}
		/// <summary>Creates a new CheckoutMethodEntity.CurrencyCode field instance</summary>
		public static EntityField2 CurrencyCode 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CheckoutMethodFieldIndex.CurrencyCode); }
		}
		/// <summary>Creates a new CheckoutMethodEntity.CountryCode field instance</summary>
		public static EntityField2 CountryCode 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CheckoutMethodFieldIndex.CountryCode); }
		}
	}

	/// <summary>Field Creation Class for entity CheckoutMethodDeliverypointgroupEntity</summary>
	public partial class CheckoutMethodDeliverypointgroupFields
	{
		/// <summary>Creates a new CheckoutMethodDeliverypointgroupEntity.DeliverypointgroupId field instance</summary>
		public static EntityField2 DeliverypointgroupId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CheckoutMethodDeliverypointgroupFieldIndex.DeliverypointgroupId); }
		}
		/// <summary>Creates a new CheckoutMethodDeliverypointgroupEntity.CheckoutMethodId field instance</summary>
		public static EntityField2 CheckoutMethodId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CheckoutMethodDeliverypointgroupFieldIndex.CheckoutMethodId); }
		}
		/// <summary>Creates a new CheckoutMethodDeliverypointgroupEntity.ParentCompanyId field instance</summary>
		public static EntityField2 ParentCompanyId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CheckoutMethodDeliverypointgroupFieldIndex.ParentCompanyId); }
		}
		/// <summary>Creates a new CheckoutMethodDeliverypointgroupEntity.CreatedUTC field instance</summary>
		public static EntityField2 CreatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CheckoutMethodDeliverypointgroupFieldIndex.CreatedUTC); }
		}
		/// <summary>Creates a new CheckoutMethodDeliverypointgroupEntity.CreatedBy field instance</summary>
		public static EntityField2 CreatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CheckoutMethodDeliverypointgroupFieldIndex.CreatedBy); }
		}
		/// <summary>Creates a new CheckoutMethodDeliverypointgroupEntity.UpdatedUTC field instance</summary>
		public static EntityField2 UpdatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CheckoutMethodDeliverypointgroupFieldIndex.UpdatedUTC); }
		}
		/// <summary>Creates a new CheckoutMethodDeliverypointgroupEntity.UpdatedBy field instance</summary>
		public static EntityField2 UpdatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CheckoutMethodDeliverypointgroupFieldIndex.UpdatedBy); }
		}
	}

	/// <summary>Field Creation Class for entity ClientEntity</summary>
	public partial class ClientFields
	{
		/// <summary>Creates a new ClientEntity.ClientId field instance</summary>
		public static EntityField2 ClientId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientFieldIndex.ClientId); }
		}
		/// <summary>Creates a new ClientEntity.CompanyId field instance</summary>
		public static EntityField2 CompanyId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientFieldIndex.CompanyId); }
		}
		/// <summary>Creates a new ClientEntity.Notes field instance</summary>
		public static EntityField2 Notes 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientFieldIndex.Notes); }
		}
		/// <summary>Creates a new ClientEntity.DeliverypointgroupId field instance</summary>
		public static EntityField2 DeliverypointgroupId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientFieldIndex.DeliverypointgroupId); }
		}
		/// <summary>Creates a new ClientEntity.OperationMode field instance</summary>
		public static EntityField2 OperationMode 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientFieldIndex.OperationMode); }
		}
		/// <summary>Creates a new ClientEntity.LastOperationMode field instance</summary>
		public static EntityField2 LastOperationMode 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientFieldIndex.LastOperationMode); }
		}
		/// <summary>Creates a new ClientEntity.LastStatus field instance</summary>
		public static EntityField2 LastStatus 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientFieldIndex.LastStatus); }
		}
		/// <summary>Creates a new ClientEntity.LastStatusText field instance</summary>
		public static EntityField2 LastStatusText 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientFieldIndex.LastStatusText); }
		}
		/// <summary>Creates a new ClientEntity.LastDeliverypointId field instance</summary>
		public static EntityField2 LastDeliverypointId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientFieldIndex.LastDeliverypointId); }
		}
		/// <summary>Creates a new ClientEntity.LastDeliverypointNumber field instance</summary>
		public static EntityField2 LastDeliverypointNumber 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientFieldIndex.LastDeliverypointNumber); }
		}
		/// <summary>Creates a new ClientEntity.LastCommunicationMethod field instance</summary>
		public static EntityField2 LastCommunicationMethod 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientFieldIndex.LastCommunicationMethod); }
		}
		/// <summary>Creates a new ClientEntity.LastCloudEnvironment field instance</summary>
		public static EntityField2 LastCloudEnvironment 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientFieldIndex.LastCloudEnvironment); }
		}
		/// <summary>Creates a new ClientEntity.LastStateOnline field instance</summary>
		public static EntityField2 LastStateOnline 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientFieldIndex.LastStateOnline); }
		}
		/// <summary>Creates a new ClientEntity.LastStateOperationMode field instance</summary>
		public static EntityField2 LastStateOperationMode 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientFieldIndex.LastStateOperationMode); }
		}
		/// <summary>Creates a new ClientEntity.OutOfChargeNotificationsSent field instance</summary>
		public static EntityField2 OutOfChargeNotificationsSent 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientFieldIndex.OutOfChargeNotificationsSent); }
		}
		/// <summary>Creates a new ClientEntity.OutOfChargeNotificationLastSentUTC field instance</summary>
		public static EntityField2 OutOfChargeNotificationLastSentUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientFieldIndex.OutOfChargeNotificationLastSentUTC); }
		}
		/// <summary>Creates a new ClientEntity.HeartbeatPoll field instance</summary>
		public static EntityField2 HeartbeatPoll 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientFieldIndex.HeartbeatPoll); }
		}
		/// <summary>Creates a new ClientEntity.LogToFile field instance</summary>
		public static EntityField2 LogToFile 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientFieldIndex.LogToFile); }
		}
		/// <summary>Creates a new ClientEntity.DeviceId field instance</summary>
		public static EntityField2 DeviceId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientFieldIndex.DeviceId); }
		}
		/// <summary>Creates a new ClientEntity.DeliverypointId field instance</summary>
		public static EntityField2 DeliverypointId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientFieldIndex.DeliverypointId); }
		}
		/// <summary>Creates a new ClientEntity.BluetoothKeyboardConnected field instance</summary>
		public static EntityField2 BluetoothKeyboardConnected 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientFieldIndex.BluetoothKeyboardConnected); }
		}
		/// <summary>Creates a new ClientEntity.RoomControlAreaId field instance</summary>
		public static EntityField2 RoomControlAreaId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientFieldIndex.RoomControlAreaId); }
		}
		/// <summary>Creates a new ClientEntity.WakeUpTimeUtc field instance</summary>
		public static EntityField2 WakeUpTimeUtc 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientFieldIndex.WakeUpTimeUtc); }
		}
		/// <summary>Creates a new ClientEntity.RoomControlConnected field instance</summary>
		public static EntityField2 RoomControlConnected 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientFieldIndex.RoomControlConnected); }
		}
		/// <summary>Creates a new ClientEntity.LoadedSuccessfully field instance</summary>
		public static EntityField2 LoadedSuccessfully 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientFieldIndex.LoadedSuccessfully); }
		}
		/// <summary>Creates a new ClientEntity.DoNotDisturbActive field instance</summary>
		public static EntityField2 DoNotDisturbActive 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientFieldIndex.DoNotDisturbActive); }
		}
		/// <summary>Creates a new ClientEntity.ServiceRoomActive field instance</summary>
		public static EntityField2 ServiceRoomActive 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientFieldIndex.ServiceRoomActive); }
		}
		/// <summary>Creates a new ClientEntity.LastUserInteraction field instance</summary>
		public static EntityField2 LastUserInteraction 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientFieldIndex.LastUserInteraction); }
		}
		/// <summary>Creates a new ClientEntity.IsTestClient field instance</summary>
		public static EntityField2 IsTestClient 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientFieldIndex.IsTestClient); }
		}
		/// <summary>Creates a new ClientEntity.StatusUpdatedUTC field instance</summary>
		public static EntityField2 StatusUpdatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientFieldIndex.StatusUpdatedUTC); }
		}
		/// <summary>Creates a new ClientEntity.CreatedBy field instance</summary>
		public static EntityField2 CreatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientFieldIndex.CreatedBy); }
		}
		/// <summary>Creates a new ClientEntity.UpdatedBy field instance</summary>
		public static EntityField2 UpdatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientFieldIndex.UpdatedBy); }
		}
		/// <summary>Creates a new ClientEntity.CreatedUTC field instance</summary>
		public static EntityField2 CreatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientFieldIndex.CreatedUTC); }
		}
		/// <summary>Creates a new ClientEntity.UpdatedUTC field instance</summary>
		public static EntityField2 UpdatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientFieldIndex.UpdatedUTC); }
		}
		/// <summary>Creates a new ClientEntity.LastWifiSsid field instance</summary>
		public static EntityField2 LastWifiSsid 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientFieldIndex.LastWifiSsid); }
		}
		/// <summary>Creates a new ClientEntity.LastApiVersion field instance</summary>
		public static EntityField2 LastApiVersion 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientFieldIndex.LastApiVersion); }
		}
	}

	/// <summary>Field Creation Class for entity ClientConfigurationEntity</summary>
	public partial class ClientConfigurationFields
	{
		/// <summary>Creates a new ClientConfigurationEntity.ClientConfigurationId field instance</summary>
		public static EntityField2 ClientConfigurationId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientConfigurationFieldIndex.ClientConfigurationId); }
		}
		/// <summary>Creates a new ClientConfigurationEntity.CompanyId field instance</summary>
		public static EntityField2 CompanyId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientConfigurationFieldIndex.CompanyId); }
		}
		/// <summary>Creates a new ClientConfigurationEntity.Name field instance</summary>
		public static EntityField2 Name 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientConfigurationFieldIndex.Name); }
		}
		/// <summary>Creates a new ClientConfigurationEntity.DeliverypointgroupId field instance</summary>
		public static EntityField2 DeliverypointgroupId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientConfigurationFieldIndex.DeliverypointgroupId); }
		}
		/// <summary>Creates a new ClientConfigurationEntity.MenuId field instance</summary>
		public static EntityField2 MenuId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientConfigurationFieldIndex.MenuId); }
		}
		/// <summary>Creates a new ClientConfigurationEntity.UIThemeId field instance</summary>
		public static EntityField2 UIThemeId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientConfigurationFieldIndex.UIThemeId); }
		}
		/// <summary>Creates a new ClientConfigurationEntity.UIScheduleId field instance</summary>
		public static EntityField2 UIScheduleId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientConfigurationFieldIndex.UIScheduleId); }
		}
		/// <summary>Creates a new ClientConfigurationEntity.WifiConfigurationId field instance</summary>
		public static EntityField2 WifiConfigurationId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientConfigurationFieldIndex.WifiConfigurationId); }
		}
		/// <summary>Creates a new ClientConfigurationEntity.Pincode field instance</summary>
		public static EntityField2 Pincode 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientConfigurationFieldIndex.Pincode); }
		}
		/// <summary>Creates a new ClientConfigurationEntity.PincodeSU field instance</summary>
		public static EntityField2 PincodeSU 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientConfigurationFieldIndex.PincodeSU); }
		}
		/// <summary>Creates a new ClientConfigurationEntity.PincodeGM field instance</summary>
		public static EntityField2 PincodeGM 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientConfigurationFieldIndex.PincodeGM); }
		}
		/// <summary>Creates a new ClientConfigurationEntity.PriceScheduleId field instance</summary>
		public static EntityField2 PriceScheduleId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientConfigurationFieldIndex.PriceScheduleId); }
		}
		/// <summary>Creates a new ClientConfigurationEntity.HidePrices field instance</summary>
		public static EntityField2 HidePrices 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientConfigurationFieldIndex.HidePrices); }
		}
		/// <summary>Creates a new ClientConfigurationEntity.ShowCurrencySymbol field instance</summary>
		public static EntityField2 ShowCurrencySymbol 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientConfigurationFieldIndex.ShowCurrencySymbol); }
		}
		/// <summary>Creates a new ClientConfigurationEntity.ClearSessionOnTimeout field instance</summary>
		public static EntityField2 ClearSessionOnTimeout 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientConfigurationFieldIndex.ClearSessionOnTimeout); }
		}
		/// <summary>Creates a new ClientConfigurationEntity.ResetTimeout field instance</summary>
		public static EntityField2 ResetTimeout 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientConfigurationFieldIndex.ResetTimeout); }
		}
		/// <summary>Creates a new ClientConfigurationEntity.ScreenTimeoutInterval field instance</summary>
		public static EntityField2 ScreenTimeoutInterval 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientConfigurationFieldIndex.ScreenTimeoutInterval); }
		}
		/// <summary>Creates a new ClientConfigurationEntity.DimLevelDull field instance</summary>
		public static EntityField2 DimLevelDull 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientConfigurationFieldIndex.DimLevelDull); }
		}
		/// <summary>Creates a new ClientConfigurationEntity.DimLevelMedium field instance</summary>
		public static EntityField2 DimLevelMedium 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientConfigurationFieldIndex.DimLevelMedium); }
		}
		/// <summary>Creates a new ClientConfigurationEntity.DimLevelBright field instance</summary>
		public static EntityField2 DimLevelBright 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientConfigurationFieldIndex.DimLevelBright); }
		}
		/// <summary>Creates a new ClientConfigurationEntity.DockedDimLevelDull field instance</summary>
		public static EntityField2 DockedDimLevelDull 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientConfigurationFieldIndex.DockedDimLevelDull); }
		}
		/// <summary>Creates a new ClientConfigurationEntity.DockedDimLevelMedium field instance</summary>
		public static EntityField2 DockedDimLevelMedium 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientConfigurationFieldIndex.DockedDimLevelMedium); }
		}
		/// <summary>Creates a new ClientConfigurationEntity.DockedDimLevelBright field instance</summary>
		public static EntityField2 DockedDimLevelBright 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientConfigurationFieldIndex.DockedDimLevelBright); }
		}
		/// <summary>Creates a new ClientConfigurationEntity.PowerSaveTimeout field instance</summary>
		public static EntityField2 PowerSaveTimeout 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientConfigurationFieldIndex.PowerSaveTimeout); }
		}
		/// <summary>Creates a new ClientConfigurationEntity.PowerSaveLevel field instance</summary>
		public static EntityField2 PowerSaveLevel 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientConfigurationFieldIndex.PowerSaveLevel); }
		}
		/// <summary>Creates a new ClientConfigurationEntity.OutOfChargeLevel field instance</summary>
		public static EntityField2 OutOfChargeLevel 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientConfigurationFieldIndex.OutOfChargeLevel); }
		}
		/// <summary>Creates a new ClientConfigurationEntity.DailyOrderReset field instance</summary>
		public static EntityField2 DailyOrderReset 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientConfigurationFieldIndex.DailyOrderReset); }
		}
		/// <summary>Creates a new ClientConfigurationEntity.SleepTime field instance</summary>
		public static EntityField2 SleepTime 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientConfigurationFieldIndex.SleepTime); }
		}
		/// <summary>Creates a new ClientConfigurationEntity.WakeUpTime field instance</summary>
		public static EntityField2 WakeUpTime 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientConfigurationFieldIndex.WakeUpTime); }
		}
		/// <summary>Creates a new ClientConfigurationEntity.HomepageSlideshowInterval field instance</summary>
		public static EntityField2 HomepageSlideshowInterval 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientConfigurationFieldIndex.HomepageSlideshowInterval); }
		}
		/// <summary>Creates a new ClientConfigurationEntity.IsChargerRemovedDialogEnabled field instance</summary>
		public static EntityField2 IsChargerRemovedDialogEnabled 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientConfigurationFieldIndex.IsChargerRemovedDialogEnabled); }
		}
		/// <summary>Creates a new ClientConfigurationEntity.IsChargerRemovedReminderDialogEnabled field instance</summary>
		public static EntityField2 IsChargerRemovedReminderDialogEnabled 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientConfigurationFieldIndex.IsChargerRemovedReminderDialogEnabled); }
		}
		/// <summary>Creates a new ClientConfigurationEntity.PowerButtonHardBehaviour field instance</summary>
		public static EntityField2 PowerButtonHardBehaviour 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientConfigurationFieldIndex.PowerButtonHardBehaviour); }
		}
		/// <summary>Creates a new ClientConfigurationEntity.PowerButtonSoftBehaviour field instance</summary>
		public static EntityField2 PowerButtonSoftBehaviour 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientConfigurationFieldIndex.PowerButtonSoftBehaviour); }
		}
		/// <summary>Creates a new ClientConfigurationEntity.ScreenOffMode field instance</summary>
		public static EntityField2 ScreenOffMode 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientConfigurationFieldIndex.ScreenOffMode); }
		}
		/// <summary>Creates a new ClientConfigurationEntity.ScreensaverMode field instance</summary>
		public static EntityField2 ScreensaverMode 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientConfigurationFieldIndex.ScreensaverMode); }
		}
		/// <summary>Creates a new ClientConfigurationEntity.DeviceRebootMethod field instance</summary>
		public static EntityField2 DeviceRebootMethod 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientConfigurationFieldIndex.DeviceRebootMethod); }
		}
		/// <summary>Creates a new ClientConfigurationEntity.RoomserviceCharge field instance</summary>
		public static EntityField2 RoomserviceCharge 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientConfigurationFieldIndex.RoomserviceCharge); }
		}
		/// <summary>Creates a new ClientConfigurationEntity.AllowFreeText field instance</summary>
		public static EntityField2 AllowFreeText 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientConfigurationFieldIndex.AllowFreeText); }
		}
		/// <summary>Creates a new ClientConfigurationEntity.BrowserAgeVerificationEnabled field instance</summary>
		public static EntityField2 BrowserAgeVerificationEnabled 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientConfigurationFieldIndex.BrowserAgeVerificationEnabled); }
		}
		/// <summary>Creates a new ClientConfigurationEntity.BrowserAgeVerificationLayout field instance</summary>
		public static EntityField2 BrowserAgeVerificationLayout 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientConfigurationFieldIndex.BrowserAgeVerificationLayout); }
		}
		/// <summary>Creates a new ClientConfigurationEntity.RestartApplicationDialogEnabled field instance</summary>
		public static EntityField2 RestartApplicationDialogEnabled 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientConfigurationFieldIndex.RestartApplicationDialogEnabled); }
		}
		/// <summary>Creates a new ClientConfigurationEntity.RebootDeviceDialogEnabled field instance</summary>
		public static EntityField2 RebootDeviceDialogEnabled 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientConfigurationFieldIndex.RebootDeviceDialogEnabled); }
		}
		/// <summary>Creates a new ClientConfigurationEntity.RestartTimeoutSeconds field instance</summary>
		public static EntityField2 RestartTimeoutSeconds 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientConfigurationFieldIndex.RestartTimeoutSeconds); }
		}
		/// <summary>Creates a new ClientConfigurationEntity.GooglePrinterId field instance</summary>
		public static EntityField2 GooglePrinterId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientConfigurationFieldIndex.GooglePrinterId); }
		}
		/// <summary>Creates a new ClientConfigurationEntity.PmsIntegration field instance</summary>
		public static EntityField2 PmsIntegration 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientConfigurationFieldIndex.PmsIntegration); }
		}
		/// <summary>Creates a new ClientConfigurationEntity.PmsAllowShowBill field instance</summary>
		public static EntityField2 PmsAllowShowBill 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientConfigurationFieldIndex.PmsAllowShowBill); }
		}
		/// <summary>Creates a new ClientConfigurationEntity.PmsAllowExpressCheckout field instance</summary>
		public static EntityField2 PmsAllowExpressCheckout 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientConfigurationFieldIndex.PmsAllowExpressCheckout); }
		}
		/// <summary>Creates a new ClientConfigurationEntity.PmsAllowShowGuestName field instance</summary>
		public static EntityField2 PmsAllowShowGuestName 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientConfigurationFieldIndex.PmsAllowShowGuestName); }
		}
		/// <summary>Creates a new ClientConfigurationEntity.PmsLockClientWhenNotCheckedIn field instance</summary>
		public static EntityField2 PmsLockClientWhenNotCheckedIn 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientConfigurationFieldIndex.PmsLockClientWhenNotCheckedIn); }
		}
		/// <summary>Creates a new ClientConfigurationEntity.PmsWelcomeTimeoutMinutes field instance</summary>
		public static EntityField2 PmsWelcomeTimeoutMinutes 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientConfigurationFieldIndex.PmsWelcomeTimeoutMinutes); }
		}
		/// <summary>Creates a new ClientConfigurationEntity.PmsCheckoutCompleteTimeoutMinutes field instance</summary>
		public static EntityField2 PmsCheckoutCompleteTimeoutMinutes 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientConfigurationFieldIndex.PmsCheckoutCompleteTimeoutMinutes); }
		}
		/// <summary>Creates a new ClientConfigurationEntity.IsOrderitemAddedDialogEnabled field instance</summary>
		public static EntityField2 IsOrderitemAddedDialogEnabled 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientConfigurationFieldIndex.IsOrderitemAddedDialogEnabled); }
		}
		/// <summary>Creates a new ClientConfigurationEntity.IsClearBasketDialogEnabled field instance</summary>
		public static EntityField2 IsClearBasketDialogEnabled 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientConfigurationFieldIndex.IsClearBasketDialogEnabled); }
		}
		/// <summary>Creates a new ClientConfigurationEntity.OrderCompletedNotificationEnabled field instance</summary>
		public static EntityField2 OrderCompletedNotificationEnabled 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientConfigurationFieldIndex.OrderCompletedNotificationEnabled); }
		}
		/// <summary>Creates a new ClientConfigurationEntity.CreatedUTC field instance</summary>
		public static EntityField2 CreatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientConfigurationFieldIndex.CreatedUTC); }
		}
		/// <summary>Creates a new ClientConfigurationEntity.CreatedBy field instance</summary>
		public static EntityField2 CreatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientConfigurationFieldIndex.CreatedBy); }
		}
		/// <summary>Creates a new ClientConfigurationEntity.UpdatedUTC field instance</summary>
		public static EntityField2 UpdatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientConfigurationFieldIndex.UpdatedUTC); }
		}
		/// <summary>Creates a new ClientConfigurationEntity.UpdatedBy field instance</summary>
		public static EntityField2 UpdatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientConfigurationFieldIndex.UpdatedBy); }
		}
	}

	/// <summary>Field Creation Class for entity ClientConfigurationRouteEntity</summary>
	public partial class ClientConfigurationRouteFields
	{
		/// <summary>Creates a new ClientConfigurationRouteEntity.ClientConfigurationRouteId field instance</summary>
		public static EntityField2 ClientConfigurationRouteId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientConfigurationRouteFieldIndex.ClientConfigurationRouteId); }
		}
		/// <summary>Creates a new ClientConfigurationRouteEntity.ParentCompanyId field instance</summary>
		public static EntityField2 ParentCompanyId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientConfigurationRouteFieldIndex.ParentCompanyId); }
		}
		/// <summary>Creates a new ClientConfigurationRouteEntity.ClientConfigurationId field instance</summary>
		public static EntityField2 ClientConfigurationId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientConfigurationRouteFieldIndex.ClientConfigurationId); }
		}
		/// <summary>Creates a new ClientConfigurationRouteEntity.RouteId field instance</summary>
		public static EntityField2 RouteId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientConfigurationRouteFieldIndex.RouteId); }
		}
		/// <summary>Creates a new ClientConfigurationRouteEntity.Type field instance</summary>
		public static EntityField2 Type 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientConfigurationRouteFieldIndex.Type); }
		}
		/// <summary>Creates a new ClientConfigurationRouteEntity.CreatedUTC field instance</summary>
		public static EntityField2 CreatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientConfigurationRouteFieldIndex.CreatedUTC); }
		}
		/// <summary>Creates a new ClientConfigurationRouteEntity.CreatedBy field instance</summary>
		public static EntityField2 CreatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientConfigurationRouteFieldIndex.CreatedBy); }
		}
		/// <summary>Creates a new ClientConfigurationRouteEntity.UpdatedUTC field instance</summary>
		public static EntityField2 UpdatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientConfigurationRouteFieldIndex.UpdatedUTC); }
		}
		/// <summary>Creates a new ClientConfigurationRouteEntity.UpdatedBy field instance</summary>
		public static EntityField2 UpdatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientConfigurationRouteFieldIndex.UpdatedBy); }
		}
	}

	/// <summary>Field Creation Class for entity ClientLogEntity</summary>
	public partial class ClientLogFields
	{
		/// <summary>Creates a new ClientLogEntity.ClientLogId field instance</summary>
		public static EntityField2 ClientLogId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientLogFieldIndex.ClientLogId); }
		}
		/// <summary>Creates a new ClientLogEntity.ParentCompanyId field instance</summary>
		public static EntityField2 ParentCompanyId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientLogFieldIndex.ParentCompanyId); }
		}
		/// <summary>Creates a new ClientLogEntity.ClientId field instance</summary>
		public static EntityField2 ClientId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientLogFieldIndex.ClientId); }
		}
		/// <summary>Creates a new ClientLogEntity.Type field instance</summary>
		public static EntityField2 Type 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientLogFieldIndex.Type); }
		}
		/// <summary>Creates a new ClientLogEntity.TypeText field instance</summary>
		public static EntityField2 TypeText 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientLogFieldIndex.TypeText); }
		}
		/// <summary>Creates a new ClientLogEntity.Message field instance</summary>
		public static EntityField2 Message 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientLogFieldIndex.Message); }
		}
		/// <summary>Creates a new ClientLogEntity.FromStatus field instance</summary>
		public static EntityField2 FromStatus 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientLogFieldIndex.FromStatus); }
		}
		/// <summary>Creates a new ClientLogEntity.FromStatusText field instance</summary>
		public static EntityField2 FromStatusText 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientLogFieldIndex.FromStatusText); }
		}
		/// <summary>Creates a new ClientLogEntity.ToStatus field instance</summary>
		public static EntityField2 ToStatus 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientLogFieldIndex.ToStatus); }
		}
		/// <summary>Creates a new ClientLogEntity.ToStatusText field instance</summary>
		public static EntityField2 ToStatusText 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientLogFieldIndex.ToStatusText); }
		}
		/// <summary>Creates a new ClientLogEntity.FromOperationMode field instance</summary>
		public static EntityField2 FromOperationMode 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientLogFieldIndex.FromOperationMode); }
		}
		/// <summary>Creates a new ClientLogEntity.FromOperationModeText field instance</summary>
		public static EntityField2 FromOperationModeText 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientLogFieldIndex.FromOperationModeText); }
		}
		/// <summary>Creates a new ClientLogEntity.ToOperationMode field instance</summary>
		public static EntityField2 ToOperationMode 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientLogFieldIndex.ToOperationMode); }
		}
		/// <summary>Creates a new ClientLogEntity.ToOperationModeText field instance</summary>
		public static EntityField2 ToOperationModeText 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientLogFieldIndex.ToOperationModeText); }
		}
		/// <summary>Creates a new ClientLogEntity.ClientLogFileId field instance</summary>
		public static EntityField2 ClientLogFileId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientLogFieldIndex.ClientLogFileId); }
		}
		/// <summary>Creates a new ClientLogEntity.CreatedBy field instance</summary>
		public static EntityField2 CreatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientLogFieldIndex.CreatedBy); }
		}
		/// <summary>Creates a new ClientLogEntity.UpdatedBy field instance</summary>
		public static EntityField2 UpdatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientLogFieldIndex.UpdatedBy); }
		}
		/// <summary>Creates a new ClientLogEntity.CreatedUTC field instance</summary>
		public static EntityField2 CreatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientLogFieldIndex.CreatedUTC); }
		}
		/// <summary>Creates a new ClientLogEntity.UpdatedUTC field instance</summary>
		public static EntityField2 UpdatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ClientLogFieldIndex.UpdatedUTC); }
		}
	}

	/// <summary>Field Creation Class for entity CompanyEntity</summary>
	public partial class CompanyFields
	{
		/// <summary>Creates a new CompanyEntity.CompanyId field instance</summary>
		public static EntityField2 CompanyId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CompanyFieldIndex.CompanyId); }
		}
		/// <summary>Creates a new CompanyEntity.CompanyOwnerId field instance</summary>
		public static EntityField2 CompanyOwnerId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CompanyFieldIndex.CompanyOwnerId); }
		}
		/// <summary>Creates a new CompanyEntity.RouteId field instance</summary>
		public static EntityField2 RouteId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CompanyFieldIndex.RouteId); }
		}
		/// <summary>Creates a new CompanyEntity.SupportpoolId field instance</summary>
		public static EntityField2 SupportpoolId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CompanyFieldIndex.SupportpoolId); }
		}
		/// <summary>Creates a new CompanyEntity.Name field instance</summary>
		public static EntityField2 Name 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CompanyFieldIndex.Name); }
		}
		/// <summary>Creates a new CompanyEntity.NameWithoutDiacritics field instance</summary>
		public static EntityField2 NameWithoutDiacritics 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CompanyFieldIndex.NameWithoutDiacritics); }
		}
		/// <summary>Creates a new CompanyEntity.Code field instance</summary>
		public static EntityField2 Code 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CompanyFieldIndex.Code); }
		}
		/// <summary>Creates a new CompanyEntity.Addressline1 field instance</summary>
		public static EntityField2 Addressline1 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CompanyFieldIndex.Addressline1); }
		}
		/// <summary>Creates a new CompanyEntity.Addressline2 field instance</summary>
		public static EntityField2 Addressline2 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CompanyFieldIndex.Addressline2); }
		}
		/// <summary>Creates a new CompanyEntity.Addressline3 field instance</summary>
		public static EntityField2 Addressline3 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CompanyFieldIndex.Addressline3); }
		}
		/// <summary>Creates a new CompanyEntity.Zipcode field instance</summary>
		public static EntityField2 Zipcode 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CompanyFieldIndex.Zipcode); }
		}
		/// <summary>Creates a new CompanyEntity.City field instance</summary>
		public static EntityField2 City 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CompanyFieldIndex.City); }
		}
		/// <summary>Creates a new CompanyEntity.CountryCode field instance</summary>
		public static EntityField2 CountryCode 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CompanyFieldIndex.CountryCode); }
		}
		/// <summary>Creates a new CompanyEntity.TimeZoneOlsonId field instance</summary>
		public static EntityField2 TimeZoneOlsonId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CompanyFieldIndex.TimeZoneOlsonId); }
		}
		/// <summary>Creates a new CompanyEntity.CultureCode field instance</summary>
		public static EntityField2 CultureCode 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CompanyFieldIndex.CultureCode); }
		}
		/// <summary>Creates a new CompanyEntity.CurrencyCode field instance</summary>
		public static EntityField2 CurrencyCode 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CompanyFieldIndex.CurrencyCode); }
		}
		/// <summary>Creates a new CompanyEntity.Latitude field instance</summary>
		public static EntityField2 Latitude 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CompanyFieldIndex.Latitude); }
		}
		/// <summary>Creates a new CompanyEntity.LatitudeOverridden field instance</summary>
		public static EntityField2 LatitudeOverridden 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CompanyFieldIndex.LatitudeOverridden); }
		}
		/// <summary>Creates a new CompanyEntity.Longitude field instance</summary>
		public static EntityField2 Longitude 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CompanyFieldIndex.Longitude); }
		}
		/// <summary>Creates a new CompanyEntity.LongitudeOverriden field instance</summary>
		public static EntityField2 LongitudeOverriden 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CompanyFieldIndex.LongitudeOverriden); }
		}
		/// <summary>Creates a new CompanyEntity.Telephone field instance</summary>
		public static EntityField2 Telephone 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CompanyFieldIndex.Telephone); }
		}
		/// <summary>Creates a new CompanyEntity.Fax field instance</summary>
		public static EntityField2 Fax 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CompanyFieldIndex.Fax); }
		}
		/// <summary>Creates a new CompanyEntity.Website field instance</summary>
		public static EntityField2 Website 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CompanyFieldIndex.Website); }
		}
		/// <summary>Creates a new CompanyEntity.Email field instance</summary>
		public static EntityField2 Email 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CompanyFieldIndex.Email); }
		}
		/// <summary>Creates a new CompanyEntity.GoogleAnalyticsId field instance</summary>
		public static EntityField2 GoogleAnalyticsId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CompanyFieldIndex.GoogleAnalyticsId); }
		}
		/// <summary>Creates a new CompanyEntity.StandardCheckInterval field instance</summary>
		public static EntityField2 StandardCheckInterval 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CompanyFieldIndex.StandardCheckInterval); }
		}
		/// <summary>Creates a new CompanyEntity.BrowserAgeVerification field instance</summary>
		public static EntityField2 BrowserAgeVerification 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CompanyFieldIndex.BrowserAgeVerification); }
		}
		/// <summary>Creates a new CompanyEntity.Description field instance</summary>
		public static EntityField2 Description 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CompanyFieldIndex.Description); }
		}
		/// <summary>Creates a new CompanyEntity.DescriptionSingleLine field instance</summary>
		public static EntityField2 DescriptionSingleLine 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CompanyFieldIndex.DescriptionSingleLine); }
		}
		/// <summary>Creates a new CompanyEntity.AutomaticClientOperationModes field instance</summary>
		public static EntityField2 AutomaticClientOperationModes 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CompanyFieldIndex.AutomaticClientOperationModes); }
		}
		/// <summary>Creates a new CompanyEntity.AvailableOnOtoucho field instance</summary>
		public static EntityField2 AvailableOnOtoucho 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CompanyFieldIndex.AvailableOnOtoucho); }
		}
		/// <summary>Creates a new CompanyEntity.AvailableOnObymobi field instance</summary>
		public static EntityField2 AvailableOnObymobi 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CompanyFieldIndex.AvailableOnObymobi); }
		}
		/// <summary>Creates a new CompanyEntity.MobileOrderingDisabled field instance</summary>
		public static EntityField2 MobileOrderingDisabled 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CompanyFieldIndex.MobileOrderingDisabled); }
		}
		/// <summary>Creates a new CompanyEntity.UseMonitoring field instance</summary>
		public static EntityField2 UseMonitoring 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CompanyFieldIndex.UseMonitoring); }
		}
		/// <summary>Creates a new CompanyEntity.SystemType field instance</summary>
		public static EntityField2 SystemType 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CompanyFieldIndex.SystemType); }
		}
		/// <summary>Creates a new CompanyEntity.AllowRequestService field instance</summary>
		public static EntityField2 AllowRequestService 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CompanyFieldIndex.AllowRequestService); }
		}
		/// <summary>Creates a new CompanyEntity.AllowRateProduct field instance</summary>
		public static EntityField2 AllowRateProduct 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CompanyFieldIndex.AllowRateProduct); }
		}
		/// <summary>Creates a new CompanyEntity.AllowRateService field instance</summary>
		public static EntityField2 AllowRateService 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CompanyFieldIndex.AllowRateService); }
		}
		/// <summary>Creates a new CompanyEntity.AllowFreeText field instance</summary>
		public static EntityField2 AllowFreeText 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CompanyFieldIndex.AllowFreeText); }
		}
		/// <summary>Creates a new CompanyEntity.UseBillButton field instance</summary>
		public static EntityField2 UseBillButton 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CompanyFieldIndex.UseBillButton); }
		}
		/// <summary>Creates a new CompanyEntity.Salt field instance</summary>
		public static EntityField2 Salt 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CompanyFieldIndex.Salt); }
		}
		/// <summary>Creates a new CompanyEntity.SaltPms field instance</summary>
		public static EntityField2 SaltPms 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CompanyFieldIndex.SaltPms); }
		}
		/// <summary>Creates a new CompanyEntity.MaxDownloadConnections field instance</summary>
		public static EntityField2 MaxDownloadConnections 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CompanyFieldIndex.MaxDownloadConnections); }
		}
		/// <summary>Creates a new CompanyEntity.ClientApplicationVersion field instance</summary>
		public static EntityField2 ClientApplicationVersion 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CompanyFieldIndex.ClientApplicationVersion); }
		}
		/// <summary>Creates a new CompanyEntity.TerminalApplicationVersion field instance</summary>
		public static EntityField2 TerminalApplicationVersion 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CompanyFieldIndex.TerminalApplicationVersion); }
		}
		/// <summary>Creates a new CompanyEntity.OsVersion field instance</summary>
		public static EntityField2 OsVersion 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CompanyFieldIndex.OsVersion); }
		}
		/// <summary>Creates a new CompanyEntity.AgentVersion field instance</summary>
		public static EntityField2 AgentVersion 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CompanyFieldIndex.AgentVersion); }
		}
		/// <summary>Creates a new CompanyEntity.SupportToolsVersion field instance</summary>
		public static EntityField2 SupportToolsVersion 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CompanyFieldIndex.SupportToolsVersion); }
		}
		/// <summary>Creates a new CompanyEntity.DeviceRebootMethod field instance</summary>
		public static EntityField2 DeviceRebootMethod 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CompanyFieldIndex.DeviceRebootMethod); }
		}
		/// <summary>Creates a new CompanyEntity.CorrespondenceEmail field instance</summary>
		public static EntityField2 CorrespondenceEmail 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CompanyFieldIndex.CorrespondenceEmail); }
		}
		/// <summary>Creates a new CompanyEntity.ShowCurrencySymbol field instance</summary>
		public static EntityField2 ShowCurrencySymbol 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CompanyFieldIndex.ShowCurrencySymbol); }
		}
		/// <summary>Creates a new CompanyEntity.Pincode field instance</summary>
		public static EntityField2 Pincode 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CompanyFieldIndex.Pincode); }
		}
		/// <summary>Creates a new CompanyEntity.PincodeSU field instance</summary>
		public static EntityField2 PincodeSU 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CompanyFieldIndex.PincodeSU); }
		}
		/// <summary>Creates a new CompanyEntity.PincodeGM field instance</summary>
		public static EntityField2 PincodeGM 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CompanyFieldIndex.PincodeGM); }
		}
		/// <summary>Creates a new CompanyEntity.ActionButtonId field instance</summary>
		public static EntityField2 ActionButtonId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CompanyFieldIndex.ActionButtonId); }
		}
		/// <summary>Creates a new CompanyEntity.ActionButtonUrlTablet field instance</summary>
		public static EntityField2 ActionButtonUrlTablet 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CompanyFieldIndex.ActionButtonUrlTablet); }
		}
		/// <summary>Creates a new CompanyEntity.ActionButtonUrlMobile field instance</summary>
		public static EntityField2 ActionButtonUrlMobile 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CompanyFieldIndex.ActionButtonUrlMobile); }
		}
		/// <summary>Creates a new CompanyEntity.CostIndicationType field instance</summary>
		public static EntityField2 CostIndicationType 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CompanyFieldIndex.CostIndicationType); }
		}
		/// <summary>Creates a new CompanyEntity.CostIndicationValue field instance</summary>
		public static EntityField2 CostIndicationValue 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CompanyFieldIndex.CostIndicationValue); }
		}
		/// <summary>Creates a new CompanyEntity.CreatedBy field instance</summary>
		public static EntityField2 CreatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CompanyFieldIndex.CreatedBy); }
		}
		/// <summary>Creates a new CompanyEntity.UpdatedBy field instance</summary>
		public static EntityField2 UpdatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CompanyFieldIndex.UpdatedBy); }
		}
		/// <summary>Creates a new CompanyEntity.GeoFencingEnabled field instance</summary>
		public static EntityField2 GeoFencingEnabled 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CompanyFieldIndex.GeoFencingEnabled); }
		}
		/// <summary>Creates a new CompanyEntity.GeoFencingRadius field instance</summary>
		public static EntityField2 GeoFencingRadius 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CompanyFieldIndex.GeoFencingRadius); }
		}
		/// <summary>Creates a new CompanyEntity.CometHandlerType field instance</summary>
		public static EntityField2 CometHandlerType 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CompanyFieldIndex.CometHandlerType); }
		}
		/// <summary>Creates a new CompanyEntity.PercentageDownForNotification field instance</summary>
		public static EntityField2 PercentageDownForNotification 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CompanyFieldIndex.PercentageDownForNotification); }
		}
		/// <summary>Creates a new CompanyEntity.PercentageDownIntervalHours field instance</summary>
		public static EntityField2 PercentageDownIntervalHours 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CompanyFieldIndex.PercentageDownIntervalHours); }
		}
		/// <summary>Creates a new CompanyEntity.PercentageDownJumpForNotification field instance</summary>
		public static EntityField2 PercentageDownJumpForNotification 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CompanyFieldIndex.PercentageDownJumpForNotification); }
		}
		/// <summary>Creates a new CompanyEntity.SystemPassword field instance</summary>
		public static EntityField2 SystemPassword 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CompanyFieldIndex.SystemPassword); }
		}
		/// <summary>Creates a new CompanyEntity.TemperatureUnit field instance</summary>
		public static EntityField2 TemperatureUnit 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CompanyFieldIndex.TemperatureUnit); }
		}
		/// <summary>Creates a new CompanyEntity.ClockMode field instance</summary>
		public static EntityField2 ClockMode 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CompanyFieldIndex.ClockMode); }
		}
		/// <summary>Creates a new CompanyEntity.WeatherClockWidgetMode field instance</summary>
		public static EntityField2 WeatherClockWidgetMode 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CompanyFieldIndex.WeatherClockWidgetMode); }
		}
		/// <summary>Creates a new CompanyEntity.DailyReportSendTime field instance</summary>
		public static EntityField2 DailyReportSendTime 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CompanyFieldIndex.DailyReportSendTime); }
		}
		/// <summary>Creates a new CompanyEntity.AlterationDialogMode field instance</summary>
		public static EntityField2 AlterationDialogMode 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CompanyFieldIndex.AlterationDialogMode); }
		}
		/// <summary>Creates a new CompanyEntity.PriceFormatType field instance</summary>
		public static EntityField2 PriceFormatType 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CompanyFieldIndex.PriceFormatType); }
		}
		/// <summary>Creates a new CompanyEntity.IncludeDeliveryTimeInOrderNotes field instance</summary>
		public static EntityField2 IncludeDeliveryTimeInOrderNotes 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CompanyFieldIndex.IncludeDeliveryTimeInOrderNotes); }
		}
		/// <summary>Creates a new CompanyEntity.IncludeMenuItemsNotExternallyLinked field instance</summary>
		public static EntityField2 IncludeMenuItemsNotExternallyLinked 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CompanyFieldIndex.IncludeMenuItemsNotExternallyLinked); }
		}
		/// <summary>Creates a new CompanyEntity.CreatedUTC field instance</summary>
		public static EntityField2 CreatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CompanyFieldIndex.CreatedUTC); }
		}
		/// <summary>Creates a new CompanyEntity.UpdatedUTC field instance</summary>
		public static EntityField2 UpdatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CompanyFieldIndex.UpdatedUTC); }
		}
		/// <summary>Creates a new CompanyEntity.PercentageDownNotificationLastSentUTC field instance</summary>
		public static EntityField2 PercentageDownNotificationLastSentUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CompanyFieldIndex.PercentageDownNotificationLastSentUTC); }
		}
		/// <summary>Creates a new CompanyEntity.PercentageDownJumpNotificationLastSentUTC field instance</summary>
		public static EntityField2 PercentageDownJumpNotificationLastSentUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CompanyFieldIndex.PercentageDownJumpNotificationLastSentUTC); }
		}
		/// <summary>Creates a new CompanyEntity.LastDailyReportSendUTC field instance</summary>
		public static EntityField2 LastDailyReportSendUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CompanyFieldIndex.LastDailyReportSendUTC); }
		}
		/// <summary>Creates a new CompanyEntity.CompanyDataLastModifiedUTC field instance</summary>
		public static EntityField2 CompanyDataLastModifiedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CompanyFieldIndex.CompanyDataLastModifiedUTC); }
		}
		/// <summary>Creates a new CompanyEntity.CompanyMediaLastModifiedUTC field instance</summary>
		public static EntityField2 CompanyMediaLastModifiedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CompanyFieldIndex.CompanyMediaLastModifiedUTC); }
		}
		/// <summary>Creates a new CompanyEntity.MenuDataLastModifiedUTC field instance</summary>
		public static EntityField2 MenuDataLastModifiedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CompanyFieldIndex.MenuDataLastModifiedUTC); }
		}
		/// <summary>Creates a new CompanyEntity.MenuMediaLastModifiedUTC field instance</summary>
		public static EntityField2 MenuMediaLastModifiedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CompanyFieldIndex.MenuMediaLastModifiedUTC); }
		}
		/// <summary>Creates a new CompanyEntity.PosIntegrationInfoLastModifiedUTC field instance</summary>
		public static EntityField2 PosIntegrationInfoLastModifiedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CompanyFieldIndex.PosIntegrationInfoLastModifiedUTC); }
		}
		/// <summary>Creates a new CompanyEntity.DeliverypointDataLastModifiedUTC field instance</summary>
		public static EntityField2 DeliverypointDataLastModifiedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CompanyFieldIndex.DeliverypointDataLastModifiedUTC); }
		}
		/// <summary>Creates a new CompanyEntity.SurveyDataLastModifiedUTC field instance</summary>
		public static EntityField2 SurveyDataLastModifiedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CompanyFieldIndex.SurveyDataLastModifiedUTC); }
		}
		/// <summary>Creates a new CompanyEntity.SurveyMediaLastModifiedUTC field instance</summary>
		public static EntityField2 SurveyMediaLastModifiedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CompanyFieldIndex.SurveyMediaLastModifiedUTC); }
		}
		/// <summary>Creates a new CompanyEntity.AnnouncementDataLastModifiedUTC field instance</summary>
		public static EntityField2 AnnouncementDataLastModifiedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CompanyFieldIndex.AnnouncementDataLastModifiedUTC); }
		}
		/// <summary>Creates a new CompanyEntity.AnnouncementMediaLastModifiedUTC field instance</summary>
		public static EntityField2 AnnouncementMediaLastModifiedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CompanyFieldIndex.AnnouncementMediaLastModifiedUTC); }
		}
		/// <summary>Creates a new CompanyEntity.EntertainmentDataLastModifiedUTC field instance</summary>
		public static EntityField2 EntertainmentDataLastModifiedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CompanyFieldIndex.EntertainmentDataLastModifiedUTC); }
		}
		/// <summary>Creates a new CompanyEntity.EntertainmentMediaLastModifiedUTC field instance</summary>
		public static EntityField2 EntertainmentMediaLastModifiedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CompanyFieldIndex.EntertainmentMediaLastModifiedUTC); }
		}
		/// <summary>Creates a new CompanyEntity.AdvertisementDataLastModifiedUTC field instance</summary>
		public static EntityField2 AdvertisementDataLastModifiedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CompanyFieldIndex.AdvertisementDataLastModifiedUTC); }
		}
		/// <summary>Creates a new CompanyEntity.AdvertisementMediaLastModifiedUTC field instance</summary>
		public static EntityField2 AdvertisementMediaLastModifiedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CompanyFieldIndex.AdvertisementMediaLastModifiedUTC); }
		}
		/// <summary>Creates a new CompanyEntity.GoogleAnalyticsProfileId field instance</summary>
		public static EntityField2 GoogleAnalyticsProfileId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CompanyFieldIndex.GoogleAnalyticsProfileId); }
		}
		/// <summary>Creates a new CompanyEntity.OrganizationId field instance</summary>
		public static EntityField2 OrganizationId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CompanyFieldIndex.OrganizationId); }
		}
		/// <summary>Creates a new CompanyEntity.ApiVersion field instance</summary>
		public static EntityField2 ApiVersion 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CompanyFieldIndex.ApiVersion); }
		}
		/// <summary>Creates a new CompanyEntity.MessagingVersion field instance</summary>
		public static EntityField2 MessagingVersion 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CompanyFieldIndex.MessagingVersion); }
		}
		/// <summary>Creates a new CompanyEntity.LinkDefaultRoomControlArea field instance</summary>
		public static EntityField2 LinkDefaultRoomControlArea 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CompanyFieldIndex.LinkDefaultRoomControlArea); }
		}
		/// <summary>Creates a new CompanyEntity.PricesIncludeTaxes field instance</summary>
		public static EntityField2 PricesIncludeTaxes 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CompanyFieldIndex.PricesIncludeTaxes); }
		}
		/// <summary>Creates a new CompanyEntity.UnitSystem field instance</summary>
		public static EntityField2 UnitSystem 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CompanyFieldIndex.UnitSystem); }
		}
		/// <summary>Creates a new CompanyEntity.ReleaseGroup field instance</summary>
		public static EntityField2 ReleaseGroup 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CompanyFieldIndex.ReleaseGroup); }
		}
		/// <summary>Creates a new CompanyEntity.Notes field instance</summary>
		public static EntityField2 Notes 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CompanyFieldIndex.Notes); }
		}
	}

	/// <summary>Field Creation Class for entity CustomerEntity</summary>
	public partial class CustomerFields
	{
		/// <summary>Creates a new CustomerEntity.CustomerId field instance</summary>
		public static EntityField2 CustomerId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CustomerFieldIndex.CustomerId); }
		}
		/// <summary>Creates a new CustomerEntity.Email field instance</summary>
		public static EntityField2 Email 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CustomerFieldIndex.Email); }
		}
		/// <summary>Creates a new CustomerEntity.Phonenumber field instance</summary>
		public static EntityField2 Phonenumber 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CustomerFieldIndex.Phonenumber); }
		}
		/// <summary>Creates a new CustomerEntity.Password field instance</summary>
		public static EntityField2 Password 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CustomerFieldIndex.Password); }
		}
		/// <summary>Creates a new CustomerEntity.PasswordSalt field instance</summary>
		public static EntityField2 PasswordSalt 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CustomerFieldIndex.PasswordSalt); }
		}
		/// <summary>Creates a new CustomerEntity.CommunicationSalt field instance</summary>
		public static EntityField2 CommunicationSalt 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CustomerFieldIndex.CommunicationSalt); }
		}
		/// <summary>Creates a new CustomerEntity.Verified field instance</summary>
		public static EntityField2 Verified 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CustomerFieldIndex.Verified); }
		}
		/// <summary>Creates a new CustomerEntity.Blacklisted field instance</summary>
		public static EntityField2 Blacklisted 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CustomerFieldIndex.Blacklisted); }
		}
		/// <summary>Creates a new CustomerEntity.BlacklistedNotes field instance</summary>
		public static EntityField2 BlacklistedNotes 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CustomerFieldIndex.BlacklistedNotes); }
		}
		/// <summary>Creates a new CustomerEntity.Firstname field instance</summary>
		public static EntityField2 Firstname 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CustomerFieldIndex.Firstname); }
		}
		/// <summary>Creates a new CustomerEntity.Lastname field instance</summary>
		public static EntityField2 Lastname 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CustomerFieldIndex.Lastname); }
		}
		/// <summary>Creates a new CustomerEntity.LastnamePrefix field instance</summary>
		public static EntityField2 LastnamePrefix 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CustomerFieldIndex.LastnamePrefix); }
		}
		/// <summary>Creates a new CustomerEntity.RandomValue field instance</summary>
		public static EntityField2 RandomValue 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CustomerFieldIndex.RandomValue); }
		}
		/// <summary>Creates a new CustomerEntity.Gender field instance</summary>
		public static EntityField2 Gender 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CustomerFieldIndex.Gender); }
		}
		/// <summary>Creates a new CustomerEntity.Addressline1 field instance</summary>
		public static EntityField2 Addressline1 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CustomerFieldIndex.Addressline1); }
		}
		/// <summary>Creates a new CustomerEntity.Addressline2 field instance</summary>
		public static EntityField2 Addressline2 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CustomerFieldIndex.Addressline2); }
		}
		/// <summary>Creates a new CustomerEntity.Zipcode field instance</summary>
		public static EntityField2 Zipcode 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CustomerFieldIndex.Zipcode); }
		}
		/// <summary>Creates a new CustomerEntity.City field instance</summary>
		public static EntityField2 City 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CustomerFieldIndex.City); }
		}
		/// <summary>Creates a new CustomerEntity.CountryCode field instance</summary>
		public static EntityField2 CountryCode 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CustomerFieldIndex.CountryCode); }
		}
		/// <summary>Creates a new CustomerEntity.Notes field instance</summary>
		public static EntityField2 Notes 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CustomerFieldIndex.Notes); }
		}
		/// <summary>Creates a new CustomerEntity.SmsRequestsSent field instance</summary>
		public static EntityField2 SmsRequestsSent 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CustomerFieldIndex.SmsRequestsSent); }
		}
		/// <summary>Creates a new CustomerEntity.SmsCreateRequests field instance</summary>
		public static EntityField2 SmsCreateRequests 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CustomerFieldIndex.SmsCreateRequests); }
		}
		/// <summary>Creates a new CustomerEntity.SmsResetPincodeRequests field instance</summary>
		public static EntityField2 SmsResetPincodeRequests 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CustomerFieldIndex.SmsResetPincodeRequests); }
		}
		/// <summary>Creates a new CustomerEntity.SmsUnlockAccountRequests field instance</summary>
		public static EntityField2 SmsUnlockAccountRequests 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CustomerFieldIndex.SmsUnlockAccountRequests); }
		}
		/// <summary>Creates a new CustomerEntity.SmsNonProcessableRequests field instance</summary>
		public static EntityField2 SmsNonProcessableRequests 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CustomerFieldIndex.SmsNonProcessableRequests); }
		}
		/// <summary>Creates a new CustomerEntity.HasChangedInititialPincode field instance</summary>
		public static EntityField2 HasChangedInititialPincode 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CustomerFieldIndex.HasChangedInititialPincode); }
		}
		/// <summary>Creates a new CustomerEntity.InitialLinkIdentifier field instance</summary>
		public static EntityField2 InitialLinkIdentifier 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CustomerFieldIndex.InitialLinkIdentifier); }
		}
		/// <summary>Creates a new CustomerEntity.PasswordResetLinkIdentifier field instance</summary>
		public static EntityField2 PasswordResetLinkIdentifier 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CustomerFieldIndex.PasswordResetLinkIdentifier); }
		}
		/// <summary>Creates a new CustomerEntity.PasswordResetLinkFailedAtttempts field instance</summary>
		public static EntityField2 PasswordResetLinkFailedAtttempts 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CustomerFieldIndex.PasswordResetLinkFailedAtttempts); }
		}
		/// <summary>Creates a new CustomerEntity.FailedPasswordAttemptCount field instance</summary>
		public static EntityField2 FailedPasswordAttemptCount 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CustomerFieldIndex.FailedPasswordAttemptCount); }
		}
		/// <summary>Creates a new CustomerEntity.RememberOnMobileWebsite field instance</summary>
		public static EntityField2 RememberOnMobileWebsite 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CustomerFieldIndex.RememberOnMobileWebsite); }
		}
		/// <summary>Creates a new CustomerEntity.AnonymousAccount field instance</summary>
		public static EntityField2 AnonymousAccount 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CustomerFieldIndex.AnonymousAccount); }
		}
		/// <summary>Creates a new CustomerEntity.ClientId field instance</summary>
		public static EntityField2 ClientId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CustomerFieldIndex.ClientId); }
		}
		/// <summary>Creates a new CustomerEntity.NewEmailAddress field instance</summary>
		public static EntityField2 NewEmailAddress 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CustomerFieldIndex.NewEmailAddress); }
		}
		/// <summary>Creates a new CustomerEntity.NewEmailAddressConfirmationGuid field instance</summary>
		public static EntityField2 NewEmailAddressConfirmationGuid 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CustomerFieldIndex.NewEmailAddressConfirmationGuid); }
		}
		/// <summary>Creates a new CustomerEntity.NewEmailAddressVerified field instance</summary>
		public static EntityField2 NewEmailAddressVerified 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CustomerFieldIndex.NewEmailAddressVerified); }
		}
		/// <summary>Creates a new CustomerEntity.CampaignName field instance</summary>
		public static EntityField2 CampaignName 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CustomerFieldIndex.CampaignName); }
		}
		/// <summary>Creates a new CustomerEntity.CampaignSource field instance</summary>
		public static EntityField2 CampaignSource 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CustomerFieldIndex.CampaignSource); }
		}
		/// <summary>Creates a new CustomerEntity.CampaignMedium field instance</summary>
		public static EntityField2 CampaignMedium 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CustomerFieldIndex.CampaignMedium); }
		}
		/// <summary>Creates a new CustomerEntity.CampaignContent field instance</summary>
		public static EntityField2 CampaignContent 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CustomerFieldIndex.CampaignContent); }
		}
		/// <summary>Creates a new CustomerEntity.CreatedBy field instance</summary>
		public static EntityField2 CreatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CustomerFieldIndex.CreatedBy); }
		}
		/// <summary>Creates a new CustomerEntity.UpdatedBy field instance</summary>
		public static EntityField2 UpdatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CustomerFieldIndex.UpdatedBy); }
		}
		/// <summary>Creates a new CustomerEntity.GUID field instance</summary>
		public static EntityField2 GUID 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CustomerFieldIndex.GUID); }
		}
		/// <summary>Creates a new CustomerEntity.CanSingleSignOn field instance</summary>
		public static EntityField2 CanSingleSignOn 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CustomerFieldIndex.CanSingleSignOn); }
		}
		/// <summary>Creates a new CustomerEntity.CreatedUTC field instance</summary>
		public static EntityField2 CreatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CustomerFieldIndex.CreatedUTC); }
		}
		/// <summary>Creates a new CustomerEntity.UpdatedUTC field instance</summary>
		public static EntityField2 UpdatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CustomerFieldIndex.UpdatedUTC); }
		}
		/// <summary>Creates a new CustomerEntity.PasswordResetLinkGeneratedUTC field instance</summary>
		public static EntityField2 PasswordResetLinkGeneratedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CustomerFieldIndex.PasswordResetLinkGeneratedUTC); }
		}
		/// <summary>Creates a new CustomerEntity.LastFailedPasswordAttemptUTC field instance</summary>
		public static EntityField2 LastFailedPasswordAttemptUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(CustomerFieldIndex.LastFailedPasswordAttemptUTC); }
		}
	}

	/// <summary>Field Creation Class for entity DeliveryDistanceEntity</summary>
	public partial class DeliveryDistanceFields
	{
		/// <summary>Creates a new DeliveryDistanceEntity.DeliveryDistanceId field instance</summary>
		public static EntityField2 DeliveryDistanceId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliveryDistanceFieldIndex.DeliveryDistanceId); }
		}
		/// <summary>Creates a new DeliveryDistanceEntity.ServiceMethodId field instance</summary>
		public static EntityField2 ServiceMethodId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliveryDistanceFieldIndex.ServiceMethodId); }
		}
		/// <summary>Creates a new DeliveryDistanceEntity.MaximumInMetres field instance</summary>
		public static EntityField2 MaximumInMetres 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliveryDistanceFieldIndex.MaximumInMetres); }
		}
		/// <summary>Creates a new DeliveryDistanceEntity.Price field instance</summary>
		public static EntityField2 Price 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliveryDistanceFieldIndex.Price); }
		}
		/// <summary>Creates a new DeliveryDistanceEntity.CreatedUTC field instance</summary>
		public static EntityField2 CreatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliveryDistanceFieldIndex.CreatedUTC); }
		}
		/// <summary>Creates a new DeliveryDistanceEntity.CreatedBy field instance</summary>
		public static EntityField2 CreatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliveryDistanceFieldIndex.CreatedBy); }
		}
		/// <summary>Creates a new DeliveryDistanceEntity.UpdatedUTC field instance</summary>
		public static EntityField2 UpdatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliveryDistanceFieldIndex.UpdatedUTC); }
		}
		/// <summary>Creates a new DeliveryDistanceEntity.UpdatedBy field instance</summary>
		public static EntityField2 UpdatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliveryDistanceFieldIndex.UpdatedBy); }
		}
	}

	/// <summary>Field Creation Class for entity DeliveryInformationEntity</summary>
	public partial class DeliveryInformationFields
	{
		/// <summary>Creates a new DeliveryInformationEntity.DeliveryInformationId field instance</summary>
		public static EntityField2 DeliveryInformationId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliveryInformationFieldIndex.DeliveryInformationId); }
		}
		/// <summary>Creates a new DeliveryInformationEntity.BuildingName field instance</summary>
		public static EntityField2 BuildingName 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliveryInformationFieldIndex.BuildingName); }
		}
		/// <summary>Creates a new DeliveryInformationEntity.Address field instance</summary>
		public static EntityField2 Address 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliveryInformationFieldIndex.Address); }
		}
		/// <summary>Creates a new DeliveryInformationEntity.Zipcode field instance</summary>
		public static EntityField2 Zipcode 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliveryInformationFieldIndex.Zipcode); }
		}
		/// <summary>Creates a new DeliveryInformationEntity.City field instance</summary>
		public static EntityField2 City 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliveryInformationFieldIndex.City); }
		}
		/// <summary>Creates a new DeliveryInformationEntity.Instructions field instance</summary>
		public static EntityField2 Instructions 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliveryInformationFieldIndex.Instructions); }
		}
		/// <summary>Creates a new DeliveryInformationEntity.CreatedUTC field instance</summary>
		public static EntityField2 CreatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliveryInformationFieldIndex.CreatedUTC); }
		}
		/// <summary>Creates a new DeliveryInformationEntity.UpdatedUTC field instance</summary>
		public static EntityField2 UpdatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliveryInformationFieldIndex.UpdatedUTC); }
		}
		/// <summary>Creates a new DeliveryInformationEntity.Latitude field instance</summary>
		public static EntityField2 Latitude 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliveryInformationFieldIndex.Latitude); }
		}
		/// <summary>Creates a new DeliveryInformationEntity.Longitude field instance</summary>
		public static EntityField2 Longitude 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliveryInformationFieldIndex.Longitude); }
		}
		/// <summary>Creates a new DeliveryInformationEntity.DistanceToOutletInMetres field instance</summary>
		public static EntityField2 DistanceToOutletInMetres 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliveryInformationFieldIndex.DistanceToOutletInMetres); }
		}
	}

	/// <summary>Field Creation Class for entity DeliverypointEntity</summary>
	public partial class DeliverypointFields
	{
		/// <summary>Creates a new DeliverypointEntity.DeliverypointId field instance</summary>
		public static EntityField2 DeliverypointId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointFieldIndex.DeliverypointId); }
		}
		/// <summary>Creates a new DeliverypointEntity.DeliverypointgroupId field instance</summary>
		public static EntityField2 DeliverypointgroupId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointFieldIndex.DeliverypointgroupId); }
		}
		/// <summary>Creates a new DeliverypointEntity.DeviceId field instance</summary>
		public static EntityField2 DeviceId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointFieldIndex.DeviceId); }
		}
		/// <summary>Creates a new DeliverypointEntity.Name field instance</summary>
		public static EntityField2 Name 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointFieldIndex.Name); }
		}
		/// <summary>Creates a new DeliverypointEntity.Number field instance</summary>
		public static EntityField2 Number 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointFieldIndex.Number); }
		}
		/// <summary>Creates a new DeliverypointEntity.PosdeliverypointId field instance</summary>
		public static EntityField2 PosdeliverypointId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointFieldIndex.PosdeliverypointId); }
		}
		/// <summary>Creates a new DeliverypointEntity.CompanyId field instance</summary>
		public static EntityField2 CompanyId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointFieldIndex.CompanyId); }
		}
		/// <summary>Creates a new DeliverypointEntity.RoomControllerType field instance</summary>
		public static EntityField2 RoomControllerType 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointFieldIndex.RoomControllerType); }
		}
		/// <summary>Creates a new DeliverypointEntity.RoomControllerIp field instance</summary>
		public static EntityField2 RoomControllerIp 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointFieldIndex.RoomControllerIp); }
		}
		/// <summary>Creates a new DeliverypointEntity.RoomControllerSlaveId field instance</summary>
		public static EntityField2 RoomControllerSlaveId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointFieldIndex.RoomControllerSlaveId); }
		}
		/// <summary>Creates a new DeliverypointEntity.GooglePrinterId field instance</summary>
		public static EntityField2 GooglePrinterId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointFieldIndex.GooglePrinterId); }
		}
		/// <summary>Creates a new DeliverypointEntity.GooglePrinterName field instance</summary>
		public static EntityField2 GooglePrinterName 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointFieldIndex.GooglePrinterName); }
		}
		/// <summary>Creates a new DeliverypointEntity.HotSOSRoomId field instance</summary>
		public static EntityField2 HotSOSRoomId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointFieldIndex.HotSOSRoomId); }
		}
		/// <summary>Creates a new DeliverypointEntity.RoomControlConfigurationId field instance</summary>
		public static EntityField2 RoomControlConfigurationId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointFieldIndex.RoomControlConfigurationId); }
		}
		/// <summary>Creates a new DeliverypointEntity.ClientConfigurationId field instance</summary>
		public static EntityField2 ClientConfigurationId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointFieldIndex.ClientConfigurationId); }
		}
		/// <summary>Creates a new DeliverypointEntity.CreatedBy field instance</summary>
		public static EntityField2 CreatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointFieldIndex.CreatedBy); }
		}
		/// <summary>Creates a new DeliverypointEntity.UpdatedBy field instance</summary>
		public static EntityField2 UpdatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointFieldIndex.UpdatedBy); }
		}
		/// <summary>Creates a new DeliverypointEntity.EnableAnalytics field instance</summary>
		public static EntityField2 EnableAnalytics 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointFieldIndex.EnableAnalytics); }
		}
		/// <summary>Creates a new DeliverypointEntity.CreatedUTC field instance</summary>
		public static EntityField2 CreatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointFieldIndex.CreatedUTC); }
		}
		/// <summary>Creates a new DeliverypointEntity.UpdatedUTC field instance</summary>
		public static EntityField2 UpdatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointFieldIndex.UpdatedUTC); }
		}
	}

	/// <summary>Field Creation Class for entity DeliverypointExternalDeliverypointEntity</summary>
	public partial class DeliverypointExternalDeliverypointFields
	{
		/// <summary>Creates a new DeliverypointExternalDeliverypointEntity.DeliverypointExternalDeliverypointId field instance</summary>
		public static EntityField2 DeliverypointExternalDeliverypointId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointExternalDeliverypointFieldIndex.DeliverypointExternalDeliverypointId); }
		}
		/// <summary>Creates a new DeliverypointExternalDeliverypointEntity.DeliverypointId field instance</summary>
		public static EntityField2 DeliverypointId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointExternalDeliverypointFieldIndex.DeliverypointId); }
		}
		/// <summary>Creates a new DeliverypointExternalDeliverypointEntity.ExternalDeliverypointId field instance</summary>
		public static EntityField2 ExternalDeliverypointId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointExternalDeliverypointFieldIndex.ExternalDeliverypointId); }
		}
		/// <summary>Creates a new DeliverypointExternalDeliverypointEntity.ParentCompanyId field instance</summary>
		public static EntityField2 ParentCompanyId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointExternalDeliverypointFieldIndex.ParentCompanyId); }
		}
		/// <summary>Creates a new DeliverypointExternalDeliverypointEntity.CreatedUTC field instance</summary>
		public static EntityField2 CreatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointExternalDeliverypointFieldIndex.CreatedUTC); }
		}
		/// <summary>Creates a new DeliverypointExternalDeliverypointEntity.UpdatedUTC field instance</summary>
		public static EntityField2 UpdatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointExternalDeliverypointFieldIndex.UpdatedUTC); }
		}
	}

	/// <summary>Field Creation Class for entity DeliverypointgroupEntity</summary>
	public partial class DeliverypointgroupFields
	{
		/// <summary>Creates a new DeliverypointgroupEntity.DeliverypointgroupId field instance</summary>
		public static EntityField2 DeliverypointgroupId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.DeliverypointgroupId); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.TerminalId field instance</summary>
		public static EntityField2 TerminalId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.TerminalId); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.CompanyId field instance</summary>
		public static EntityField2 CompanyId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.CompanyId); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.ClientConfigurationId field instance</summary>
		public static EntityField2 ClientConfigurationId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.ClientConfigurationId); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.RouteId field instance</summary>
		public static EntityField2 RouteId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.RouteId); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.SystemMessageRouteId field instance</summary>
		public static EntityField2 SystemMessageRouteId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.SystemMessageRouteId); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.OrderNotesRouteId field instance</summary>
		public static EntityField2 OrderNotesRouteId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.OrderNotesRouteId); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.MenuId field instance</summary>
		public static EntityField2 MenuId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.MenuId); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.EmailDocumentRouteId field instance</summary>
		public static EntityField2 EmailDocumentRouteId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.EmailDocumentRouteId); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.Name field instance</summary>
		public static EntityField2 Name 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.Name); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.DeliverypointCaption field instance</summary>
		public static EntityField2 DeliverypointCaption 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.DeliverypointCaption); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.ConfirmationCodeDeliveryType field instance</summary>
		public static EntityField2 ConfirmationCodeDeliveryType 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.ConfirmationCodeDeliveryType); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.Active field instance</summary>
		public static EntityField2 Active 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.Active); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.AvailableOnObymobi field instance</summary>
		public static EntityField2 AvailableOnObymobi 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.AvailableOnObymobi); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.WifiSsid field instance</summary>
		public static EntityField2 WifiSsid 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.WifiSsid); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.WifiPassword field instance</summary>
		public static EntityField2 WifiPassword 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.WifiPassword); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.WifiSecurityMethod field instance</summary>
		public static EntityField2 WifiSecurityMethod 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.WifiSecurityMethod); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.WifiEapMode field instance</summary>
		public static EntityField2 WifiEapMode 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.WifiEapMode); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.WifiPhase2Authentication field instance</summary>
		public static EntityField2 WifiPhase2Authentication 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.WifiPhase2Authentication); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.WifiAnonymousIdentify field instance</summary>
		public static EntityField2 WifiAnonymousIdentify 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.WifiAnonymousIdentify); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.WifiIdentity field instance</summary>
		public static EntityField2 WifiIdentity 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.WifiIdentity); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.Locale field instance</summary>
		public static EntityField2 Locale 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.Locale); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.Pincode field instance</summary>
		public static EntityField2 Pincode 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.Pincode); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.UseManualDeliverypoint field instance</summary>
		public static EntityField2 UseManualDeliverypoint 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.UseManualDeliverypoint); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.UseManualDeliverypointEncryption field instance</summary>
		public static EntityField2 UseManualDeliverypointEncryption 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.UseManualDeliverypointEncryption); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.AddDeliverypointToOrder field instance</summary>
		public static EntityField2 AddDeliverypointToOrder 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.AddDeliverypointToOrder); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.EncryptionSalt field instance</summary>
		public static EntityField2 EncryptionSalt 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.EncryptionSalt); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.HideDeliverypointNumber field instance</summary>
		public static EntityField2 HideDeliverypointNumber 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.HideDeliverypointNumber); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.HidePrices field instance</summary>
		public static EntityField2 HidePrices 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.HidePrices); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.ClearSessionOnTimeout field instance</summary>
		public static EntityField2 ClearSessionOnTimeout 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.ClearSessionOnTimeout); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.ResetTimeout field instance</summary>
		public static EntityField2 ResetTimeout 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.ResetTimeout); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.ReorderNotificationEnabled field instance</summary>
		public static EntityField2 ReorderNotificationEnabled 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.ReorderNotificationEnabled); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.ReorderNotificationInterval field instance</summary>
		public static EntityField2 ReorderNotificationInterval 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.ReorderNotificationInterval); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.ReorderNotificationAnnouncementId field instance</summary>
		public static EntityField2 ReorderNotificationAnnouncementId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.ReorderNotificationAnnouncementId); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.ScreenTimeoutInterval field instance</summary>
		public static EntityField2 ScreenTimeoutInterval 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.ScreenTimeoutInterval); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.DeviceActivationRequired field instance</summary>
		public static EntityField2 DeviceActivationRequired 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.DeviceActivationRequired); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.PosdeliverypointgroupId field instance</summary>
		public static EntityField2 PosdeliverypointgroupId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.PosdeliverypointgroupId); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.DefaultProductFullView field instance</summary>
		public static EntityField2 DefaultProductFullView 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.DefaultProductFullView); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.MarketingSurveyUrl field instance</summary>
		public static EntityField2 MarketingSurveyUrl 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.MarketingSurveyUrl); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.HotelUrl1 field instance</summary>
		public static EntityField2 HotelUrl1 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.HotelUrl1); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.HotelUrl1Caption field instance</summary>
		public static EntityField2 HotelUrl1Caption 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.HotelUrl1Caption); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.HotelUrl1Zoom field instance</summary>
		public static EntityField2 HotelUrl1Zoom 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.HotelUrl1Zoom); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.HotelUrl2 field instance</summary>
		public static EntityField2 HotelUrl2 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.HotelUrl2); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.HotelUrl2Caption field instance</summary>
		public static EntityField2 HotelUrl2Caption 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.HotelUrl2Caption); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.HotelUrl2Zoom field instance</summary>
		public static EntityField2 HotelUrl2Zoom 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.HotelUrl2Zoom); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.HotelUrl3 field instance</summary>
		public static EntityField2 HotelUrl3 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.HotelUrl3); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.HotelUrl3Caption field instance</summary>
		public static EntityField2 HotelUrl3Caption 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.HotelUrl3Caption); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.HotelUrl3Zoom field instance</summary>
		public static EntityField2 HotelUrl3Zoom 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.HotelUrl3Zoom); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.DimLevelDull field instance</summary>
		public static EntityField2 DimLevelDull 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.DimLevelDull); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.DimLevelMedium field instance</summary>
		public static EntityField2 DimLevelMedium 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.DimLevelMedium); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.DimLevelBright field instance</summary>
		public static EntityField2 DimLevelBright 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.DimLevelBright); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.PowerSaveTimeout field instance</summary>
		public static EntityField2 PowerSaveTimeout 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.PowerSaveTimeout); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.PowerSaveLevel field instance</summary>
		public static EntityField2 PowerSaveLevel 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.PowerSaveLevel); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.OutOfChargeLevel field instance</summary>
		public static EntityField2 OutOfChargeLevel 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.OutOfChargeLevel); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.OutOfChargeNotificationAmount field instance</summary>
		public static EntityField2 OutOfChargeNotificationAmount 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.OutOfChargeNotificationAmount); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.OutOfChargeTitle field instance</summary>
		public static EntityField2 OutOfChargeTitle 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.OutOfChargeTitle); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.OutOfChargeMessage field instance</summary>
		public static EntityField2 OutOfChargeMessage 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.OutOfChargeMessage); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.OrderProcessedTitle field instance</summary>
		public static EntityField2 OrderProcessedTitle 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.OrderProcessedTitle); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.OrderProcessedMessage field instance</summary>
		public static EntityField2 OrderProcessedMessage 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.OrderProcessedMessage); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.OrderFailedTitle field instance</summary>
		public static EntityField2 OrderFailedTitle 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.OrderFailedTitle); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.OrderFailedMessage field instance</summary>
		public static EntityField2 OrderFailedMessage 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.OrderFailedMessage); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.OrderCompletedTitle field instance</summary>
		public static EntityField2 OrderCompletedTitle 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.OrderCompletedTitle); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.OrderCompletedMessage field instance</summary>
		public static EntityField2 OrderCompletedMessage 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.OrderCompletedMessage); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.OrderCompletedEnabled field instance</summary>
		public static EntityField2 OrderCompletedEnabled 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.OrderCompletedEnabled); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.OrderSavingTitle field instance</summary>
		public static EntityField2 OrderSavingTitle 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.OrderSavingTitle); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.OrderSavingMessage field instance</summary>
		public static EntityField2 OrderSavingMessage 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.OrderSavingMessage); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.ServiceSavingTitle field instance</summary>
		public static EntityField2 ServiceSavingTitle 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.ServiceSavingTitle); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.ServiceSavingMessage field instance</summary>
		public static EntityField2 ServiceSavingMessage 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.ServiceSavingMessage); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.ServiceProcessedTitle field instance</summary>
		public static EntityField2 ServiceProcessedTitle 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.ServiceProcessedTitle); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.ServiceProcessedMessage field instance</summary>
		public static EntityField2 ServiceProcessedMessage 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.ServiceProcessedMessage); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.ServiceFailedTitle field instance</summary>
		public static EntityField2 ServiceFailedTitle 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.ServiceFailedTitle); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.ServiceFailedMessage field instance</summary>
		public static EntityField2 ServiceFailedMessage 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.ServiceFailedMessage); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.OrderingNotAvailableMessage field instance</summary>
		public static EntityField2 OrderingNotAvailableMessage 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.OrderingNotAvailableMessage); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.DailyOrderReset field instance</summary>
		public static EntityField2 DailyOrderReset 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.DailyOrderReset); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.SleepTime field instance</summary>
		public static EntityField2 SleepTime 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.SleepTime); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.WakeUpTime field instance</summary>
		public static EntityField2 WakeUpTime 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.WakeUpTime); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.OrderProcessingNotificationEnabled field instance</summary>
		public static EntityField2 OrderProcessingNotificationEnabled 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.OrderProcessingNotificationEnabled); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.OrderProcessingNotificationTitle field instance</summary>
		public static EntityField2 OrderProcessingNotificationTitle 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.OrderProcessingNotificationTitle); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.OrderProcessingNotification field instance</summary>
		public static EntityField2 OrderProcessingNotification 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.OrderProcessingNotification); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.OrderProcessedNotificationEnabled field instance</summary>
		public static EntityField2 OrderProcessedNotificationEnabled 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.OrderProcessedNotificationEnabled); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.OrderProcessedNotificationTitle field instance</summary>
		public static EntityField2 OrderProcessedNotificationTitle 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.OrderProcessedNotificationTitle); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.OrderProcessedNotification field instance</summary>
		public static EntityField2 OrderProcessedNotification 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.OrderProcessedNotification); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.FreeformMessageTitle field instance</summary>
		public static EntityField2 FreeformMessageTitle 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.FreeformMessageTitle); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.ServiceProcessingNotificationTitle field instance</summary>
		public static EntityField2 ServiceProcessingNotificationTitle 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.ServiceProcessingNotificationTitle); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.ServiceProcessingNotification field instance</summary>
		public static EntityField2 ServiceProcessingNotification 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.ServiceProcessingNotification); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.ServiceProcessedNotificationTitle field instance</summary>
		public static EntityField2 ServiceProcessedNotificationTitle 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.ServiceProcessedNotificationTitle); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.ServiceProcessedNotification field instance</summary>
		public static EntityField2 ServiceProcessedNotification 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.ServiceProcessedNotification); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.PreorderingEnabled field instance</summary>
		public static EntityField2 PreorderingEnabled 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.PreorderingEnabled); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.PreorderingActiveMinutes field instance</summary>
		public static EntityField2 PreorderingActiveMinutes 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.PreorderingActiveMinutes); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.AnalyticsOrderingVisible field instance</summary>
		public static EntityField2 AnalyticsOrderingVisible 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.AnalyticsOrderingVisible); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.AnalyticsBestsellersVisible field instance</summary>
		public static EntityField2 AnalyticsBestsellersVisible 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.AnalyticsBestsellersVisible); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.OrderingEnabled field instance</summary>
		public static EntityField2 OrderingEnabled 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.OrderingEnabled); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.HomepageSlideshowInterval field instance</summary>
		public static EntityField2 HomepageSlideshowInterval 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.HomepageSlideshowInterval); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.GooglePrinterId field instance</summary>
		public static EntityField2 GooglePrinterId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.GooglePrinterId); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.GooglePrinterName field instance</summary>
		public static EntityField2 GooglePrinterName 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.GooglePrinterName); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.UIModeId field instance</summary>
		public static EntityField2 UIModeId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.UIModeId); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.MobileUIModeId field instance</summary>
		public static EntityField2 MobileUIModeId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.MobileUIModeId); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.TabletUIModeId field instance</summary>
		public static EntityField2 TabletUIModeId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.TabletUIModeId); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.PmsIntegration field instance</summary>
		public static EntityField2 PmsIntegration 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.PmsIntegration); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.PmsAllowShowBill field instance</summary>
		public static EntityField2 PmsAllowShowBill 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.PmsAllowShowBill); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.PmsAllowExpressCheckout field instance</summary>
		public static EntityField2 PmsAllowExpressCheckout 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.PmsAllowExpressCheckout); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.PmsAllowShowGuestName field instance</summary>
		public static EntityField2 PmsAllowShowGuestName 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.PmsAllowShowGuestName); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.PmsLockClientWhenNotCheckedIn field instance</summary>
		public static EntityField2 PmsLockClientWhenNotCheckedIn 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.PmsLockClientWhenNotCheckedIn); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.PmsDeviceLockedTitle field instance</summary>
		public static EntityField2 PmsDeviceLockedTitle 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.PmsDeviceLockedTitle); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.PmsDeviceLockedText field instance</summary>
		public static EntityField2 PmsDeviceLockedText 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.PmsDeviceLockedText); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.PmsCheckinFailedTitle field instance</summary>
		public static EntityField2 PmsCheckinFailedTitle 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.PmsCheckinFailedTitle); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.PmsCheckinFailedText field instance</summary>
		public static EntityField2 PmsCheckinFailedText 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.PmsCheckinFailedText); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.PmsRestartTitle field instance</summary>
		public static EntityField2 PmsRestartTitle 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.PmsRestartTitle); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.PmsRestartText field instance</summary>
		public static EntityField2 PmsRestartText 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.PmsRestartText); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.PmsWelcomeTitle field instance</summary>
		public static EntityField2 PmsWelcomeTitle 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.PmsWelcomeTitle); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.PmsWelcomeText field instance</summary>
		public static EntityField2 PmsWelcomeText 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.PmsWelcomeText); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.PmsCheckoutApproveText field instance</summary>
		public static EntityField2 PmsCheckoutApproveText 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.PmsCheckoutApproveText); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.PmsWelcomeTimeoutMinutes field instance</summary>
		public static EntityField2 PmsWelcomeTimeoutMinutes 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.PmsWelcomeTimeoutMinutes); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.PmsCheckoutCompleteTitle field instance</summary>
		public static EntityField2 PmsCheckoutCompleteTitle 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.PmsCheckoutCompleteTitle); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.PmsCheckoutCompleteText field instance</summary>
		public static EntityField2 PmsCheckoutCompleteText 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.PmsCheckoutCompleteText); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.PmsCheckoutCompleteTimeoutMinutes field instance</summary>
		public static EntityField2 PmsCheckoutCompleteTimeoutMinutes 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.PmsCheckoutCompleteTimeoutMinutes); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.PincodeSU field instance</summary>
		public static EntityField2 PincodeSU 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.PincodeSU); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.PincodeGM field instance</summary>
		public static EntityField2 PincodeGM 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.PincodeGM); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.DockedDimLevelDull field instance</summary>
		public static EntityField2 DockedDimLevelDull 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.DockedDimLevelDull); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.DockedDimLevelMedium field instance</summary>
		public static EntityField2 DockedDimLevelMedium 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.DockedDimLevelMedium); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.DockedDimLevelBright field instance</summary>
		public static EntityField2 DockedDimLevelBright 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.DockedDimLevelBright); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.OrderHistoryDialogEnabled field instance</summary>
		public static EntityField2 OrderHistoryDialogEnabled 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.OrderHistoryDialogEnabled); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.IsChargerRemovedDialogEnabled field instance</summary>
		public static EntityField2 IsChargerRemovedDialogEnabled 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.IsChargerRemovedDialogEnabled); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.ChargerRemovedDialogTitle field instance</summary>
		public static EntityField2 ChargerRemovedDialogTitle 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.ChargerRemovedDialogTitle); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.ChargerRemovedDialogText field instance</summary>
		public static EntityField2 ChargerRemovedDialogText 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.ChargerRemovedDialogText); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.IsChargerRemovedReminderDialogEnabled field instance</summary>
		public static EntityField2 IsChargerRemovedReminderDialogEnabled 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.IsChargerRemovedReminderDialogEnabled); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.ChargerRemovedReminderDialogTitle field instance</summary>
		public static EntityField2 ChargerRemovedReminderDialogTitle 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.ChargerRemovedReminderDialogTitle); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.ChargerRemovedReminderDialogText field instance</summary>
		public static EntityField2 ChargerRemovedReminderDialogText 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.ChargerRemovedReminderDialogText); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.PowerButtonHardBehaviour field instance</summary>
		public static EntityField2 PowerButtonHardBehaviour 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.PowerButtonHardBehaviour); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.PowerButtonSoftBehaviour field instance</summary>
		public static EntityField2 PowerButtonSoftBehaviour 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.PowerButtonSoftBehaviour); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.ScreenOffMode field instance</summary>
		public static EntityField2 ScreenOffMode 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.ScreenOffMode); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.UseHardKeyboard field instance</summary>
		public static EntityField2 UseHardKeyboard 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.UseHardKeyboard); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.RoomserviceCharge field instance</summary>
		public static EntityField2 RoomserviceCharge 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.RoomserviceCharge); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.HotSOSBatteryLowProductId field instance</summary>
		public static EntityField2 HotSOSBatteryLowProductId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.HotSOSBatteryLowProductId); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.EnableBatteryLowConsoleNotifications field instance</summary>
		public static EntityField2 EnableBatteryLowConsoleNotifications 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.EnableBatteryLowConsoleNotifications); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.UIThemeId field instance</summary>
		public static EntityField2 UIThemeId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.UIThemeId); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.ScreensaverMode field instance</summary>
		public static EntityField2 ScreensaverMode 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.ScreensaverMode); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.TurnOffPrivacyTitle field instance</summary>
		public static EntityField2 TurnOffPrivacyTitle 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.TurnOffPrivacyTitle); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.TurnOffPrivacyText field instance</summary>
		public static EntityField2 TurnOffPrivacyText 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.TurnOffPrivacyText); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.ItemCurrentlyUnavailableText field instance</summary>
		public static EntityField2 ItemCurrentlyUnavailableText 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.ItemCurrentlyUnavailableText); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.EstimatedDeliveryTime field instance</summary>
		public static EntityField2 EstimatedDeliveryTime 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.EstimatedDeliveryTime); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.UIScheduleId field instance</summary>
		public static EntityField2 UIScheduleId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.UIScheduleId); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.AlarmSetWhileNotChargingTitle field instance</summary>
		public static EntityField2 AlarmSetWhileNotChargingTitle 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.AlarmSetWhileNotChargingTitle); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.AlarmSetWhileNotChargingText field instance</summary>
		public static EntityField2 AlarmSetWhileNotChargingText 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.AlarmSetWhileNotChargingText); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.ClearBasketTitle field instance</summary>
		public static EntityField2 ClearBasketTitle 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.ClearBasketTitle); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.ClearBasketText field instance</summary>
		public static EntityField2 ClearBasketText 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.ClearBasketText); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.PriceScheduleId field instance</summary>
		public static EntityField2 PriceScheduleId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.PriceScheduleId); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.IsOrderitemAddedDialogEnabled field instance</summary>
		public static EntityField2 IsOrderitemAddedDialogEnabled 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.IsOrderitemAddedDialogEnabled); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.IsClearBasketDialogEnabled field instance</summary>
		public static EntityField2 IsClearBasketDialogEnabled 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.IsClearBasketDialogEnabled); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.BrowserAgeVerificationEnabled field instance</summary>
		public static EntityField2 BrowserAgeVerificationEnabled 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.BrowserAgeVerificationEnabled); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.BrowserAgeVerificationLayout field instance</summary>
		public static EntityField2 BrowserAgeVerificationLayout 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.BrowserAgeVerificationLayout); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.RestartApplicationDialogEnabled field instance</summary>
		public static EntityField2 RestartApplicationDialogEnabled 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.RestartApplicationDialogEnabled); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.RebootDeviceDialogEnabled field instance</summary>
		public static EntityField2 RebootDeviceDialogEnabled 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.RebootDeviceDialogEnabled); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.RestartTimeoutSeconds field instance</summary>
		public static EntityField2 RestartTimeoutSeconds 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.RestartTimeoutSeconds); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.CraveAnalytics field instance</summary>
		public static EntityField2 CraveAnalytics 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.CraveAnalytics); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.GeoFencingEnabled field instance</summary>
		public static EntityField2 GeoFencingEnabled 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.GeoFencingEnabled); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.GeoFencingRadius field instance</summary>
		public static EntityField2 GeoFencingRadius 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.GeoFencingRadius); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.HideCompanyDetails field instance</summary>
		public static EntityField2 HideCompanyDetails 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.HideCompanyDetails); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.CreatedBy field instance</summary>
		public static EntityField2 CreatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.CreatedBy); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.UpdatedBy field instance</summary>
		public static EntityField2 UpdatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.UpdatedBy); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.CreatedUTC field instance</summary>
		public static EntityField2 CreatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.CreatedUTC); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.UpdatedUTC field instance</summary>
		public static EntityField2 UpdatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.UpdatedUTC); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.ApiVersion field instance</summary>
		public static EntityField2 ApiVersion 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.ApiVersion); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.MessagingVersion field instance</summary>
		public static EntityField2 MessagingVersion 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.MessagingVersion); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.AffiliateCampaignId field instance</summary>
		public static EntityField2 AffiliateCampaignId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.AffiliateCampaignId); }
		}
		/// <summary>Creates a new DeliverypointgroupEntity.AddressId field instance</summary>
		public static EntityField2 AddressId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeliverypointgroupFieldIndex.AddressId); }
		}
	}

	/// <summary>Field Creation Class for entity DeviceEntity</summary>
	public partial class DeviceFields
	{
		/// <summary>Creates a new DeviceEntity.DeviceId field instance</summary>
		public static EntityField2 DeviceId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeviceFieldIndex.DeviceId); }
		}
		/// <summary>Creates a new DeviceEntity.Identifier field instance</summary>
		public static EntityField2 Identifier 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeviceFieldIndex.Identifier); }
		}
		/// <summary>Creates a new DeviceEntity.Type field instance</summary>
		public static EntityField2 Type 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeviceFieldIndex.Type); }
		}
		/// <summary>Creates a new DeviceEntity.Notes field instance</summary>
		public static EntityField2 Notes 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeviceFieldIndex.Notes); }
		}
		/// <summary>Creates a new DeviceEntity.UpdateStatus field instance</summary>
		public static EntityField2 UpdateStatus 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeviceFieldIndex.UpdateStatus); }
		}
		/// <summary>Creates a new DeviceEntity.CustomerId field instance</summary>
		public static EntityField2 CustomerId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeviceFieldIndex.CustomerId); }
		}
		/// <summary>Creates a new DeviceEntity.DevicePlatform field instance</summary>
		public static EntityField2 DevicePlatform 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeviceFieldIndex.DevicePlatform); }
		}
		/// <summary>Creates a new DeviceEntity.Model field instance</summary>
		public static EntityField2 Model 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeviceFieldIndex.Model); }
		}
		/// <summary>Creates a new DeviceEntity.IsDevelopment field instance</summary>
		public static EntityField2 IsDevelopment 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeviceFieldIndex.IsDevelopment); }
		}
		/// <summary>Creates a new DeviceEntity.LastRequestUTC field instance</summary>
		public static EntityField2 LastRequestUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeviceFieldIndex.LastRequestUTC); }
		}
		/// <summary>Creates a new DeviceEntity.LastRequestNotifiedBySmsUTC field instance</summary>
		public static EntityField2 LastRequestNotifiedBySmsUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeviceFieldIndex.LastRequestNotifiedBySmsUTC); }
		}
		/// <summary>Creates a new DeviceEntity.LastSupportToolsRequestUTC field instance</summary>
		public static EntityField2 LastSupportToolsRequestUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeviceFieldIndex.LastSupportToolsRequestUTC); }
		}
		/// <summary>Creates a new DeviceEntity.IsCharging field instance</summary>
		public static EntityField2 IsCharging 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeviceFieldIndex.IsCharging); }
		}
		/// <summary>Creates a new DeviceEntity.BatteryLevel field instance</summary>
		public static EntityField2 BatteryLevel 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeviceFieldIndex.BatteryLevel); }
		}
		/// <summary>Creates a new DeviceEntity.PublicIpAddress field instance</summary>
		public static EntityField2 PublicIpAddress 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeviceFieldIndex.PublicIpAddress); }
		}
		/// <summary>Creates a new DeviceEntity.PrivateIpAddresses field instance</summary>
		public static EntityField2 PrivateIpAddresses 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeviceFieldIndex.PrivateIpAddresses); }
		}
		/// <summary>Creates a new DeviceEntity.ApplicationVersion field instance</summary>
		public static EntityField2 ApplicationVersion 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeviceFieldIndex.ApplicationVersion); }
		}
		/// <summary>Creates a new DeviceEntity.AgentRunning field instance</summary>
		public static EntityField2 AgentRunning 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeviceFieldIndex.AgentRunning); }
		}
		/// <summary>Creates a new DeviceEntity.AgentVersion field instance</summary>
		public static EntityField2 AgentVersion 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeviceFieldIndex.AgentVersion); }
		}
		/// <summary>Creates a new DeviceEntity.SupportToolsRunning field instance</summary>
		public static EntityField2 SupportToolsRunning 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeviceFieldIndex.SupportToolsRunning); }
		}
		/// <summary>Creates a new DeviceEntity.SupportToolsVersion field instance</summary>
		public static EntityField2 SupportToolsVersion 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeviceFieldIndex.SupportToolsVersion); }
		}
		/// <summary>Creates a new DeviceEntity.OsVersion field instance</summary>
		public static EntityField2 OsVersion 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeviceFieldIndex.OsVersion); }
		}
		/// <summary>Creates a new DeviceEntity.WifiStrength field instance</summary>
		public static EntityField2 WifiStrength 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeviceFieldIndex.WifiStrength); }
		}
		/// <summary>Creates a new DeviceEntity.CloudEnvironment field instance</summary>
		public static EntityField2 CloudEnvironment 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeviceFieldIndex.CloudEnvironment); }
		}
		/// <summary>Creates a new DeviceEntity.CommunicationMethod field instance</summary>
		public static EntityField2 CommunicationMethod 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeviceFieldIndex.CommunicationMethod); }
		}
		/// <summary>Creates a new DeviceEntity.LastUserInteraction field instance</summary>
		public static EntityField2 LastUserInteraction 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeviceFieldIndex.LastUserInteraction); }
		}
		/// <summary>Creates a new DeviceEntity.DeviceInfoUpdatedUTC field instance</summary>
		public static EntityField2 DeviceInfoUpdatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeviceFieldIndex.DeviceInfoUpdatedUTC); }
		}
		/// <summary>Creates a new DeviceEntity.UpdateEmenuDownloaded field instance</summary>
		public static EntityField2 UpdateEmenuDownloaded 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeviceFieldIndex.UpdateEmenuDownloaded); }
		}
		/// <summary>Creates a new DeviceEntity.UpdateConsoleDownloaded field instance</summary>
		public static EntityField2 UpdateConsoleDownloaded 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeviceFieldIndex.UpdateConsoleDownloaded); }
		}
		/// <summary>Creates a new DeviceEntity.UpdateAgentDownloaded field instance</summary>
		public static EntityField2 UpdateAgentDownloaded 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeviceFieldIndex.UpdateAgentDownloaded); }
		}
		/// <summary>Creates a new DeviceEntity.UpdateSupportToolsDownloaded field instance</summary>
		public static EntityField2 UpdateSupportToolsDownloaded 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeviceFieldIndex.UpdateSupportToolsDownloaded); }
		}
		/// <summary>Creates a new DeviceEntity.UpdateOSDownloaded field instance</summary>
		public static EntityField2 UpdateOSDownloaded 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeviceFieldIndex.UpdateOSDownloaded); }
		}
		/// <summary>Creates a new DeviceEntity.CreatedBy field instance</summary>
		public static EntityField2 CreatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeviceFieldIndex.CreatedBy); }
		}
		/// <summary>Creates a new DeviceEntity.UpdatedBy field instance</summary>
		public static EntityField2 UpdatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeviceFieldIndex.UpdatedBy); }
		}
		/// <summary>Creates a new DeviceEntity.CreatedUTC field instance</summary>
		public static EntityField2 CreatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeviceFieldIndex.CreatedUTC); }
		}
		/// <summary>Creates a new DeviceEntity.UpdatedUTC field instance</summary>
		public static EntityField2 UpdatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeviceFieldIndex.UpdatedUTC); }
		}
		/// <summary>Creates a new DeviceEntity.UpdateStatusChangedUTC field instance</summary>
		public static EntityField2 UpdateStatusChangedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeviceFieldIndex.UpdateStatusChangedUTC); }
		}
		/// <summary>Creates a new DeviceEntity.Token field instance</summary>
		public static EntityField2 Token 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeviceFieldIndex.Token); }
		}
		/// <summary>Creates a new DeviceEntity.SandboxMode field instance</summary>
		public static EntityField2 SandboxMode 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeviceFieldIndex.SandboxMode); }
		}
		/// <summary>Creates a new DeviceEntity.MessagingServiceRunning field instance</summary>
		public static EntityField2 MessagingServiceRunning 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeviceFieldIndex.MessagingServiceRunning); }
		}
		/// <summary>Creates a new DeviceEntity.MessagingServiceVersion field instance</summary>
		public static EntityField2 MessagingServiceVersion 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeviceFieldIndex.MessagingServiceVersion); }
		}
		/// <summary>Creates a new DeviceEntity.LastMessagingServiceRequest field instance</summary>
		public static EntityField2 LastMessagingServiceRequest 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(DeviceFieldIndex.LastMessagingServiceRequest); }
		}
	}

	/// <summary>Field Creation Class for entity ExternalDeliverypointEntity</summary>
	public partial class ExternalDeliverypointFields
	{
		/// <summary>Creates a new ExternalDeliverypointEntity.ExternalDeliverypointId field instance</summary>
		public static EntityField2 ExternalDeliverypointId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalDeliverypointFieldIndex.ExternalDeliverypointId); }
		}
		/// <summary>Creates a new ExternalDeliverypointEntity.ExternalSystemId field instance</summary>
		public static EntityField2 ExternalSystemId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalDeliverypointFieldIndex.ExternalSystemId); }
		}
		/// <summary>Creates a new ExternalDeliverypointEntity.ParentCompanyId field instance</summary>
		public static EntityField2 ParentCompanyId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalDeliverypointFieldIndex.ParentCompanyId); }
		}
		/// <summary>Creates a new ExternalDeliverypointEntity.Id field instance</summary>
		public static EntityField2 Id 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalDeliverypointFieldIndex.Id); }
		}
		/// <summary>Creates a new ExternalDeliverypointEntity.Name field instance</summary>
		public static EntityField2 Name 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalDeliverypointFieldIndex.Name); }
		}
		/// <summary>Creates a new ExternalDeliverypointEntity.Visible field instance</summary>
		public static EntityField2 Visible 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalDeliverypointFieldIndex.Visible); }
		}
		/// <summary>Creates a new ExternalDeliverypointEntity.StringValue1 field instance</summary>
		public static EntityField2 StringValue1 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalDeliverypointFieldIndex.StringValue1); }
		}
		/// <summary>Creates a new ExternalDeliverypointEntity.StringValue2 field instance</summary>
		public static EntityField2 StringValue2 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalDeliverypointFieldIndex.StringValue2); }
		}
		/// <summary>Creates a new ExternalDeliverypointEntity.StringValue3 field instance</summary>
		public static EntityField2 StringValue3 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalDeliverypointFieldIndex.StringValue3); }
		}
		/// <summary>Creates a new ExternalDeliverypointEntity.StringValue4 field instance</summary>
		public static EntityField2 StringValue4 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalDeliverypointFieldIndex.StringValue4); }
		}
		/// <summary>Creates a new ExternalDeliverypointEntity.StringValue5 field instance</summary>
		public static EntityField2 StringValue5 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalDeliverypointFieldIndex.StringValue5); }
		}
		/// <summary>Creates a new ExternalDeliverypointEntity.IntValue1 field instance</summary>
		public static EntityField2 IntValue1 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalDeliverypointFieldIndex.IntValue1); }
		}
		/// <summary>Creates a new ExternalDeliverypointEntity.IntValue2 field instance</summary>
		public static EntityField2 IntValue2 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalDeliverypointFieldIndex.IntValue2); }
		}
		/// <summary>Creates a new ExternalDeliverypointEntity.IntValue3 field instance</summary>
		public static EntityField2 IntValue3 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalDeliverypointFieldIndex.IntValue3); }
		}
		/// <summary>Creates a new ExternalDeliverypointEntity.IntValue4 field instance</summary>
		public static EntityField2 IntValue4 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalDeliverypointFieldIndex.IntValue4); }
		}
		/// <summary>Creates a new ExternalDeliverypointEntity.IntValue5 field instance</summary>
		public static EntityField2 IntValue5 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalDeliverypointFieldIndex.IntValue5); }
		}
		/// <summary>Creates a new ExternalDeliverypointEntity.BoolValue1 field instance</summary>
		public static EntityField2 BoolValue1 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalDeliverypointFieldIndex.BoolValue1); }
		}
		/// <summary>Creates a new ExternalDeliverypointEntity.BoolValue2 field instance</summary>
		public static EntityField2 BoolValue2 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalDeliverypointFieldIndex.BoolValue2); }
		}
		/// <summary>Creates a new ExternalDeliverypointEntity.BoolValue3 field instance</summary>
		public static EntityField2 BoolValue3 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalDeliverypointFieldIndex.BoolValue3); }
		}
		/// <summary>Creates a new ExternalDeliverypointEntity.BoolValue4 field instance</summary>
		public static EntityField2 BoolValue4 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalDeliverypointFieldIndex.BoolValue4); }
		}
		/// <summary>Creates a new ExternalDeliverypointEntity.BoolValue5 field instance</summary>
		public static EntityField2 BoolValue5 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalDeliverypointFieldIndex.BoolValue5); }
		}
		/// <summary>Creates a new ExternalDeliverypointEntity.CreatedInBatchId field instance</summary>
		public static EntityField2 CreatedInBatchId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalDeliverypointFieldIndex.CreatedInBatchId); }
		}
		/// <summary>Creates a new ExternalDeliverypointEntity.UpdatedInBatchId field instance</summary>
		public static EntityField2 UpdatedInBatchId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalDeliverypointFieldIndex.UpdatedInBatchId); }
		}
		/// <summary>Creates a new ExternalDeliverypointEntity.SynchronisationBatchId field instance</summary>
		public static EntityField2 SynchronisationBatchId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalDeliverypointFieldIndex.SynchronisationBatchId); }
		}
		/// <summary>Creates a new ExternalDeliverypointEntity.CreatedUTC field instance</summary>
		public static EntityField2 CreatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalDeliverypointFieldIndex.CreatedUTC); }
		}
		/// <summary>Creates a new ExternalDeliverypointEntity.CreatedBy field instance</summary>
		public static EntityField2 CreatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalDeliverypointFieldIndex.CreatedBy); }
		}
		/// <summary>Creates a new ExternalDeliverypointEntity.UpdatedUTC field instance</summary>
		public static EntityField2 UpdatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalDeliverypointFieldIndex.UpdatedUTC); }
		}
		/// <summary>Creates a new ExternalDeliverypointEntity.UpdatedBy field instance</summary>
		public static EntityField2 UpdatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalDeliverypointFieldIndex.UpdatedBy); }
		}
	}

	/// <summary>Field Creation Class for entity ExternalMenuEntity</summary>
	public partial class ExternalMenuFields
	{
		/// <summary>Creates a new ExternalMenuEntity.ExternalMenuId field instance</summary>
		public static EntityField2 ExternalMenuId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalMenuFieldIndex.ExternalMenuId); }
		}
		/// <summary>Creates a new ExternalMenuEntity.ExternalSystemId field instance</summary>
		public static EntityField2 ExternalSystemId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalMenuFieldIndex.ExternalSystemId); }
		}
		/// <summary>Creates a new ExternalMenuEntity.Id field instance</summary>
		public static EntityField2 Id 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalMenuFieldIndex.Id); }
		}
		/// <summary>Creates a new ExternalMenuEntity.Name field instance</summary>
		public static EntityField2 Name 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalMenuFieldIndex.Name); }
		}
		/// <summary>Creates a new ExternalMenuEntity.CreatedUTC field instance</summary>
		public static EntityField2 CreatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalMenuFieldIndex.CreatedUTC); }
		}
		/// <summary>Creates a new ExternalMenuEntity.UpdateUTC field instance</summary>
		public static EntityField2 UpdateUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalMenuFieldIndex.UpdateUTC); }
		}
		/// <summary>Creates a new ExternalMenuEntity.ParentCompanyId field instance</summary>
		public static EntityField2 ParentCompanyId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalMenuFieldIndex.ParentCompanyId); }
		}
	}

	/// <summary>Field Creation Class for entity ExternalProductEntity</summary>
	public partial class ExternalProductFields
	{
		/// <summary>Creates a new ExternalProductEntity.ExternalProductId field instance</summary>
		public static EntityField2 ExternalProductId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalProductFieldIndex.ExternalProductId); }
		}
		/// <summary>Creates a new ExternalProductEntity.ExternalSystemId field instance</summary>
		public static EntityField2 ExternalSystemId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalProductFieldIndex.ExternalSystemId); }
		}
		/// <summary>Creates a new ExternalProductEntity.ParentCompanyId field instance</summary>
		public static EntityField2 ParentCompanyId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalProductFieldIndex.ParentCompanyId); }
		}
		/// <summary>Creates a new ExternalProductEntity.Id field instance</summary>
		public static EntityField2 Id 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalProductFieldIndex.Id); }
		}
		/// <summary>Creates a new ExternalProductEntity.Name field instance</summary>
		public static EntityField2 Name 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalProductFieldIndex.Name); }
		}
		/// <summary>Creates a new ExternalProductEntity.Price field instance</summary>
		public static EntityField2 Price 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalProductFieldIndex.Price); }
		}
		/// <summary>Creates a new ExternalProductEntity.Visible field instance</summary>
		public static EntityField2 Visible 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalProductFieldIndex.Visible); }
		}
		/// <summary>Creates a new ExternalProductEntity.StringValue1 field instance</summary>
		public static EntityField2 StringValue1 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalProductFieldIndex.StringValue1); }
		}
		/// <summary>Creates a new ExternalProductEntity.StringValue2 field instance</summary>
		public static EntityField2 StringValue2 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalProductFieldIndex.StringValue2); }
		}
		/// <summary>Creates a new ExternalProductEntity.StringValue3 field instance</summary>
		public static EntityField2 StringValue3 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalProductFieldIndex.StringValue3); }
		}
		/// <summary>Creates a new ExternalProductEntity.StringValue4 field instance</summary>
		public static EntityField2 StringValue4 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalProductFieldIndex.StringValue4); }
		}
		/// <summary>Creates a new ExternalProductEntity.StringValue5 field instance</summary>
		public static EntityField2 StringValue5 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalProductFieldIndex.StringValue5); }
		}
		/// <summary>Creates a new ExternalProductEntity.IntValue1 field instance</summary>
		public static EntityField2 IntValue1 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalProductFieldIndex.IntValue1); }
		}
		/// <summary>Creates a new ExternalProductEntity.IntValue2 field instance</summary>
		public static EntityField2 IntValue2 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalProductFieldIndex.IntValue2); }
		}
		/// <summary>Creates a new ExternalProductEntity.IntValue3 field instance</summary>
		public static EntityField2 IntValue3 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalProductFieldIndex.IntValue3); }
		}
		/// <summary>Creates a new ExternalProductEntity.IntValue4 field instance</summary>
		public static EntityField2 IntValue4 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalProductFieldIndex.IntValue4); }
		}
		/// <summary>Creates a new ExternalProductEntity.IntValue5 field instance</summary>
		public static EntityField2 IntValue5 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalProductFieldIndex.IntValue5); }
		}
		/// <summary>Creates a new ExternalProductEntity.BoolValue1 field instance</summary>
		public static EntityField2 BoolValue1 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalProductFieldIndex.BoolValue1); }
		}
		/// <summary>Creates a new ExternalProductEntity.BoolValue2 field instance</summary>
		public static EntityField2 BoolValue2 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalProductFieldIndex.BoolValue2); }
		}
		/// <summary>Creates a new ExternalProductEntity.BoolValue3 field instance</summary>
		public static EntityField2 BoolValue3 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalProductFieldIndex.BoolValue3); }
		}
		/// <summary>Creates a new ExternalProductEntity.BoolValue4 field instance</summary>
		public static EntityField2 BoolValue4 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalProductFieldIndex.BoolValue4); }
		}
		/// <summary>Creates a new ExternalProductEntity.BoolValue5 field instance</summary>
		public static EntityField2 BoolValue5 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalProductFieldIndex.BoolValue5); }
		}
		/// <summary>Creates a new ExternalProductEntity.CreatedInBatchId field instance</summary>
		public static EntityField2 CreatedInBatchId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalProductFieldIndex.CreatedInBatchId); }
		}
		/// <summary>Creates a new ExternalProductEntity.UpdatedInBatchId field instance</summary>
		public static EntityField2 UpdatedInBatchId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalProductFieldIndex.UpdatedInBatchId); }
		}
		/// <summary>Creates a new ExternalProductEntity.SynchronisationBatchId field instance</summary>
		public static EntityField2 SynchronisationBatchId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalProductFieldIndex.SynchronisationBatchId); }
		}
		/// <summary>Creates a new ExternalProductEntity.CreatedUTC field instance</summary>
		public static EntityField2 CreatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalProductFieldIndex.CreatedUTC); }
		}
		/// <summary>Creates a new ExternalProductEntity.CreatedBy field instance</summary>
		public static EntityField2 CreatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalProductFieldIndex.CreatedBy); }
		}
		/// <summary>Creates a new ExternalProductEntity.UpdatedUTC field instance</summary>
		public static EntityField2 UpdatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalProductFieldIndex.UpdatedUTC); }
		}
		/// <summary>Creates a new ExternalProductEntity.UpdatedBy field instance</summary>
		public static EntityField2 UpdatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalProductFieldIndex.UpdatedBy); }
		}
		/// <summary>Creates a new ExternalProductEntity.Type field instance</summary>
		public static EntityField2 Type 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalProductFieldIndex.Type); }
		}
		/// <summary>Creates a new ExternalProductEntity.MinOptions field instance</summary>
		public static EntityField2 MinOptions 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalProductFieldIndex.MinOptions); }
		}
		/// <summary>Creates a new ExternalProductEntity.MaxOptions field instance</summary>
		public static EntityField2 MaxOptions 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalProductFieldIndex.MaxOptions); }
		}
		/// <summary>Creates a new ExternalProductEntity.AllowDuplicates field instance</summary>
		public static EntityField2 AllowDuplicates 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalProductFieldIndex.AllowDuplicates); }
		}
		/// <summary>Creates a new ExternalProductEntity.IsEnabled field instance</summary>
		public static EntityField2 IsEnabled 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalProductFieldIndex.IsEnabled); }
		}
		/// <summary>Creates a new ExternalProductEntity.IsSnoozed field instance</summary>
		public static EntityField2 IsSnoozed 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalProductFieldIndex.IsSnoozed); }
		}
		/// <summary>Creates a new ExternalProductEntity.ExternalMenuId field instance</summary>
		public static EntityField2 ExternalMenuId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalProductFieldIndex.ExternalMenuId); }
		}
	}

	/// <summary>Field Creation Class for entity ExternalSubProductEntity</summary>
	public partial class ExternalSubProductFields
	{
		/// <summary>Creates a new ExternalSubProductEntity.ExternalProductExternalProductId field instance</summary>
		public static EntityField2 ExternalProductExternalProductId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalSubProductFieldIndex.ExternalProductExternalProductId); }
		}
		/// <summary>Creates a new ExternalSubProductEntity.ExternalProductId field instance</summary>
		public static EntityField2 ExternalProductId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalSubProductFieldIndex.ExternalProductId); }
		}
		/// <summary>Creates a new ExternalSubProductEntity.ExternalSubProductId field instance</summary>
		public static EntityField2 ExternalSubProductId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalSubProductFieldIndex.ExternalSubProductId); }
		}
		/// <summary>Creates a new ExternalSubProductEntity.CreatedUTC field instance</summary>
		public static EntityField2 CreatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalSubProductFieldIndex.CreatedUTC); }
		}
		/// <summary>Creates a new ExternalSubProductEntity.UpdatedUTC field instance</summary>
		public static EntityField2 UpdatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalSubProductFieldIndex.UpdatedUTC); }
		}
	}

	/// <summary>Field Creation Class for entity ExternalSystemEntity</summary>
	public partial class ExternalSystemFields
	{
		/// <summary>Creates a new ExternalSystemEntity.ExternalSystemId field instance</summary>
		public static EntityField2 ExternalSystemId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalSystemFieldIndex.ExternalSystemId); }
		}
		/// <summary>Creates a new ExternalSystemEntity.CompanyId field instance</summary>
		public static EntityField2 CompanyId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalSystemFieldIndex.CompanyId); }
		}
		/// <summary>Creates a new ExternalSystemEntity.Name field instance</summary>
		public static EntityField2 Name 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalSystemFieldIndex.Name); }
		}
		/// <summary>Creates a new ExternalSystemEntity.Type field instance</summary>
		public static EntityField2 Type 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalSystemFieldIndex.Type); }
		}
		/// <summary>Creates a new ExternalSystemEntity.StringValue1 field instance</summary>
		public static EntityField2 StringValue1 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalSystemFieldIndex.StringValue1); }
		}
		/// <summary>Creates a new ExternalSystemEntity.StringValue2 field instance</summary>
		public static EntityField2 StringValue2 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalSystemFieldIndex.StringValue2); }
		}
		/// <summary>Creates a new ExternalSystemEntity.StringValue3 field instance</summary>
		public static EntityField2 StringValue3 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalSystemFieldIndex.StringValue3); }
		}
		/// <summary>Creates a new ExternalSystemEntity.StringValue4 field instance</summary>
		public static EntityField2 StringValue4 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalSystemFieldIndex.StringValue4); }
		}
		/// <summary>Creates a new ExternalSystemEntity.StringValue5 field instance</summary>
		public static EntityField2 StringValue5 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalSystemFieldIndex.StringValue5); }
		}
		/// <summary>Creates a new ExternalSystemEntity.ExpiryDateUtc field instance</summary>
		public static EntityField2 ExpiryDateUtc 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalSystemFieldIndex.ExpiryDateUtc); }
		}
		/// <summary>Creates a new ExternalSystemEntity.CreatedUTC field instance</summary>
		public static EntityField2 CreatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalSystemFieldIndex.CreatedUTC); }
		}
		/// <summary>Creates a new ExternalSystemEntity.CreatedBy field instance</summary>
		public static EntityField2 CreatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalSystemFieldIndex.CreatedBy); }
		}
		/// <summary>Creates a new ExternalSystemEntity.UpdatedUTC field instance</summary>
		public static EntityField2 UpdatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalSystemFieldIndex.UpdatedUTC); }
		}
		/// <summary>Creates a new ExternalSystemEntity.UpdatedBy field instance</summary>
		public static EntityField2 UpdatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalSystemFieldIndex.UpdatedBy); }
		}
		/// <summary>Creates a new ExternalSystemEntity.Environment field instance</summary>
		public static EntityField2 Environment 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalSystemFieldIndex.Environment); }
		}
		/// <summary>Creates a new ExternalSystemEntity.BoolValue1 field instance</summary>
		public static EntityField2 BoolValue1 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalSystemFieldIndex.BoolValue1); }
		}
		/// <summary>Creates a new ExternalSystemEntity.BoolValue2 field instance</summary>
		public static EntityField2 BoolValue2 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalSystemFieldIndex.BoolValue2); }
		}
		/// <summary>Creates a new ExternalSystemEntity.BoolValue3 field instance</summary>
		public static EntityField2 BoolValue3 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalSystemFieldIndex.BoolValue3); }
		}
		/// <summary>Creates a new ExternalSystemEntity.BoolValue4 field instance</summary>
		public static EntityField2 BoolValue4 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalSystemFieldIndex.BoolValue4); }
		}
		/// <summary>Creates a new ExternalSystemEntity.BoolValue5 field instance</summary>
		public static EntityField2 BoolValue5 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalSystemFieldIndex.BoolValue5); }
		}
		/// <summary>Creates a new ExternalSystemEntity.IsBusy field instance</summary>
		public static EntityField2 IsBusy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalSystemFieldIndex.IsBusy); }
		}
	}

	/// <summary>Field Creation Class for entity ExternalSystemLogEntity</summary>
	public partial class ExternalSystemLogFields
	{
		/// <summary>Creates a new ExternalSystemLogEntity.ExternalSystemLogId field instance</summary>
		public static EntityField2 ExternalSystemLogId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalSystemLogFieldIndex.ExternalSystemLogId); }
		}
		/// <summary>Creates a new ExternalSystemLogEntity.ExternalSystemId field instance</summary>
		public static EntityField2 ExternalSystemId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalSystemLogFieldIndex.ExternalSystemId); }
		}
		/// <summary>Creates a new ExternalSystemLogEntity.Message field instance</summary>
		public static EntityField2 Message 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalSystemLogFieldIndex.Message); }
		}
		/// <summary>Creates a new ExternalSystemLogEntity.LogType field instance</summary>
		public static EntityField2 LogType 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalSystemLogFieldIndex.LogType); }
		}
		/// <summary>Creates a new ExternalSystemLogEntity.Success field instance</summary>
		public static EntityField2 Success 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalSystemLogFieldIndex.Success); }
		}
		/// <summary>Creates a new ExternalSystemLogEntity.Raw field instance</summary>
		public static EntityField2 Raw 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalSystemLogFieldIndex.Raw); }
		}
		/// <summary>Creates a new ExternalSystemLogEntity.CreatedUTC field instance</summary>
		public static EntityField2 CreatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalSystemLogFieldIndex.CreatedUTC); }
		}
		/// <summary>Creates a new ExternalSystemLogEntity.UpdatedUTC field instance</summary>
		public static EntityField2 UpdatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalSystemLogFieldIndex.UpdatedUTC); }
		}
		/// <summary>Creates a new ExternalSystemLogEntity.OrderId field instance</summary>
		public static EntityField2 OrderId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalSystemLogFieldIndex.OrderId); }
		}
		/// <summary>Creates a new ExternalSystemLogEntity.Log field instance</summary>
		public static EntityField2 Log 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ExternalSystemLogFieldIndex.Log); }
		}
	}

	/// <summary>Field Creation Class for entity MediaEntity</summary>
	public partial class MediaFields
	{
		/// <summary>Creates a new MediaEntity.MediaId field instance</summary>
		public static EntityField2 MediaId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(MediaFieldIndex.MediaId); }
		}
		/// <summary>Creates a new MediaEntity.MediaType field instance</summary>
		public static EntityField2 MediaType 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(MediaFieldIndex.MediaType); }
		}
		/// <summary>Creates a new MediaEntity.SortOrder field instance</summary>
		public static EntityField2 SortOrder 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(MediaFieldIndex.SortOrder); }
		}
		/// <summary>Creates a new MediaEntity.DefaultItem field instance</summary>
		public static EntityField2 DefaultItem 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(MediaFieldIndex.DefaultItem); }
		}
		/// <summary>Creates a new MediaEntity.Name field instance</summary>
		public static EntityField2 Name 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(MediaFieldIndex.Name); }
		}
		/// <summary>Creates a new MediaEntity.FilePathRelativeToMediaPath field instance</summary>
		public static EntityField2 FilePathRelativeToMediaPath 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(MediaFieldIndex.FilePathRelativeToMediaPath); }
		}
		/// <summary>Creates a new MediaEntity.Description field instance</summary>
		public static EntityField2 Description 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(MediaFieldIndex.Description); }
		}
		/// <summary>Creates a new MediaEntity.MimeType field instance</summary>
		public static EntityField2 MimeType 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(MediaFieldIndex.MimeType); }
		}
		/// <summary>Creates a new MediaEntity.Extension field instance</summary>
		public static EntityField2 Extension 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(MediaFieldIndex.Extension); }
		}
		/// <summary>Creates a new MediaEntity.SizeKb field instance</summary>
		public static EntityField2 SizeKb 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(MediaFieldIndex.SizeKb); }
		}
		/// <summary>Creates a new MediaEntity.RelatedCompanyId field instance</summary>
		public static EntityField2 RelatedCompanyId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(MediaFieldIndex.RelatedCompanyId); }
		}
		/// <summary>Creates a new MediaEntity.CompanyId field instance</summary>
		public static EntityField2 CompanyId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(MediaFieldIndex.CompanyId); }
		}
		/// <summary>Creates a new MediaEntity.ProductId field instance</summary>
		public static EntityField2 ProductId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(MediaFieldIndex.ProductId); }
		}
		/// <summary>Creates a new MediaEntity.CategoryId field instance</summary>
		public static EntityField2 CategoryId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(MediaFieldIndex.CategoryId); }
		}
		/// <summary>Creates a new MediaEntity.AdvertisementId field instance</summary>
		public static EntityField2 AdvertisementId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(MediaFieldIndex.AdvertisementId); }
		}
		/// <summary>Creates a new MediaEntity.EntertainmentId field instance</summary>
		public static EntityField2 EntertainmentId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(MediaFieldIndex.EntertainmentId); }
		}
		/// <summary>Creates a new MediaEntity.AlterationoptionId field instance</summary>
		public static EntityField2 AlterationoptionId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(MediaFieldIndex.AlterationoptionId); }
		}
		/// <summary>Creates a new MediaEntity.GenericproductId field instance</summary>
		public static EntityField2 GenericproductId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(MediaFieldIndex.GenericproductId); }
		}
		/// <summary>Creates a new MediaEntity.GenericcategoryId field instance</summary>
		public static EntityField2 GenericcategoryId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(MediaFieldIndex.GenericcategoryId); }
		}
		/// <summary>Creates a new MediaEntity.DeliverypointgroupId field instance</summary>
		public static EntityField2 DeliverypointgroupId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(MediaFieldIndex.DeliverypointgroupId); }
		}
		/// <summary>Creates a new MediaEntity.PageElementId field instance</summary>
		public static EntityField2 PageElementId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(MediaFieldIndex.PageElementId); }
		}
		/// <summary>Creates a new MediaEntity.PageTemplateElementId field instance</summary>
		public static EntityField2 PageTemplateElementId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(MediaFieldIndex.PageTemplateElementId); }
		}
		/// <summary>Creates a new MediaEntity.SiteId field instance</summary>
		public static EntityField2 SiteId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(MediaFieldIndex.SiteId); }
		}
		/// <summary>Creates a new MediaEntity.RoutestephandlerId field instance</summary>
		public static EntityField2 RoutestephandlerId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(MediaFieldIndex.RoutestephandlerId); }
		}
		/// <summary>Creates a new MediaEntity.SurveyId field instance</summary>
		public static EntityField2 SurveyId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(MediaFieldIndex.SurveyId); }
		}
		/// <summary>Creates a new MediaEntity.SurveyPageId field instance</summary>
		public static EntityField2 SurveyPageId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(MediaFieldIndex.SurveyPageId); }
		}
		/// <summary>Creates a new MediaEntity.ActionProductId field instance</summary>
		public static EntityField2 ActionProductId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(MediaFieldIndex.ActionProductId); }
		}
		/// <summary>Creates a new MediaEntity.ActionCategoryId field instance</summary>
		public static EntityField2 ActionCategoryId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(MediaFieldIndex.ActionCategoryId); }
		}
		/// <summary>Creates a new MediaEntity.ActionEntertainmentId field instance</summary>
		public static EntityField2 ActionEntertainmentId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(MediaFieldIndex.ActionEntertainmentId); }
		}
		/// <summary>Creates a new MediaEntity.ActionEntertainmentcategoryId field instance</summary>
		public static EntityField2 ActionEntertainmentcategoryId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(MediaFieldIndex.ActionEntertainmentcategoryId); }
		}
		/// <summary>Creates a new MediaEntity.ActionSiteId field instance</summary>
		public static EntityField2 ActionSiteId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(MediaFieldIndex.ActionSiteId); }
		}
		/// <summary>Creates a new MediaEntity.ActionPageId field instance</summary>
		public static EntityField2 ActionPageId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(MediaFieldIndex.ActionPageId); }
		}
		/// <summary>Creates a new MediaEntity.ActionUrl field instance</summary>
		public static EntityField2 ActionUrl 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(MediaFieldIndex.ActionUrl); }
		}
		/// <summary>Creates a new MediaEntity.FromSupplier field instance</summary>
		public static EntityField2 FromSupplier 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(MediaFieldIndex.FromSupplier); }
		}
		/// <summary>Creates a new MediaEntity.AlterationId field instance</summary>
		public static EntityField2 AlterationId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(MediaFieldIndex.AlterationId); }
		}
		/// <summary>Creates a new MediaEntity.PointOfInterestId field instance</summary>
		public static EntityField2 PointOfInterestId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(MediaFieldIndex.PointOfInterestId); }
		}
		/// <summary>Creates a new MediaEntity.PageId field instance</summary>
		public static EntityField2 PageId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(MediaFieldIndex.PageId); }
		}
		/// <summary>Creates a new MediaEntity.PageTemplateId field instance</summary>
		public static EntityField2 PageTemplateId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(MediaFieldIndex.PageTemplateId); }
		}
		/// <summary>Creates a new MediaEntity.AttachmentId field instance</summary>
		public static EntityField2 AttachmentId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(MediaFieldIndex.AttachmentId); }
		}
		/// <summary>Creates a new MediaEntity.UIWidgetId field instance</summary>
		public static EntityField2 UIWidgetId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(MediaFieldIndex.UIWidgetId); }
		}
		/// <summary>Creates a new MediaEntity.UIThemeId field instance</summary>
		public static EntityField2 UIThemeId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(MediaFieldIndex.UIThemeId); }
		}
		/// <summary>Creates a new MediaEntity.UIFooterItemId field instance</summary>
		public static EntityField2 UIFooterItemId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(MediaFieldIndex.UIFooterItemId); }
		}
		/// <summary>Creates a new MediaEntity.RoomControlSectionId field instance</summary>
		public static EntityField2 RoomControlSectionId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(MediaFieldIndex.RoomControlSectionId); }
		}
		/// <summary>Creates a new MediaEntity.RoomControlSectionItemId field instance</summary>
		public static EntityField2 RoomControlSectionItemId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(MediaFieldIndex.RoomControlSectionItemId); }
		}
		/// <summary>Creates a new MediaEntity.StationId field instance</summary>
		public static EntityField2 StationId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(MediaFieldIndex.StationId); }
		}
		/// <summary>Creates a new MediaEntity.ClientConfigurationId field instance</summary>
		public static EntityField2 ClientConfigurationId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(MediaFieldIndex.ClientConfigurationId); }
		}
		/// <summary>Creates a new MediaEntity.ProductgroupId field instance</summary>
		public static EntityField2 ProductgroupId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(MediaFieldIndex.ProductgroupId); }
		}
		/// <summary>Creates a new MediaEntity.LandingPageId field instance</summary>
		public static EntityField2 LandingPageId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(MediaFieldIndex.LandingPageId); }
		}
		/// <summary>Creates a new MediaEntity.CarouselItemId field instance</summary>
		public static EntityField2 CarouselItemId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(MediaFieldIndex.CarouselItemId); }
		}
		/// <summary>Creates a new MediaEntity.WidgetId field instance</summary>
		public static EntityField2 WidgetId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(MediaFieldIndex.WidgetId); }
		}
		/// <summary>Creates a new MediaEntity.ApplicationConfigurationId field instance</summary>
		public static EntityField2 ApplicationConfigurationId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(MediaFieldIndex.ApplicationConfigurationId); }
		}
		/// <summary>Creates a new MediaEntity.JpgQuality field instance</summary>
		public static EntityField2 JpgQuality 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(MediaFieldIndex.JpgQuality); }
		}
		/// <summary>Creates a new MediaEntity.SizeMode field instance</summary>
		public static EntityField2 SizeMode 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(MediaFieldIndex.SizeMode); }
		}
		/// <summary>Creates a new MediaEntity.ZoomLevel field instance</summary>
		public static EntityField2 ZoomLevel 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(MediaFieldIndex.ZoomLevel); }
		}
		/// <summary>Creates a new MediaEntity.MediaFileMd5 field instance</summary>
		public static EntityField2 MediaFileMd5 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(MediaFieldIndex.MediaFileMd5); }
		}
		/// <summary>Creates a new MediaEntity.CreatedBy field instance</summary>
		public static EntityField2 CreatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(MediaFieldIndex.CreatedBy); }
		}
		/// <summary>Creates a new MediaEntity.UpdatedBy field instance</summary>
		public static EntityField2 UpdatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(MediaFieldIndex.UpdatedBy); }
		}
		/// <summary>Creates a new MediaEntity.AgnosticMediaId field instance</summary>
		public static EntityField2 AgnosticMediaId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(MediaFieldIndex.AgnosticMediaId); }
		}
		/// <summary>Creates a new MediaEntity.CreatedUTC field instance</summary>
		public static EntityField2 CreatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(MediaFieldIndex.CreatedUTC); }
		}
		/// <summary>Creates a new MediaEntity.UpdatedUTC field instance</summary>
		public static EntityField2 UpdatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(MediaFieldIndex.UpdatedUTC); }
		}
		/// <summary>Creates a new MediaEntity.LastDistributedVersionTicksAzure field instance</summary>
		public static EntityField2 LastDistributedVersionTicksAzure 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(MediaFieldIndex.LastDistributedVersionTicksAzure); }
		}
		/// <summary>Creates a new MediaEntity.LastDistributedVersionTicksAmazon field instance</summary>
		public static EntityField2 LastDistributedVersionTicksAmazon 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(MediaFieldIndex.LastDistributedVersionTicksAmazon); }
		}
		/// <summary>Creates a new MediaEntity.LastDistributedVersionTicks field instance</summary>
		public static EntityField2 LastDistributedVersionTicks 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(MediaFieldIndex.LastDistributedVersionTicks); }
		}
		/// <summary>Creates a new MediaEntity.SizeWidth field instance</summary>
		public static EntityField2 SizeWidth 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(MediaFieldIndex.SizeWidth); }
		}
		/// <summary>Creates a new MediaEntity.SizeHeight field instance</summary>
		public static EntityField2 SizeHeight 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(MediaFieldIndex.SizeHeight); }
		}
	}

	/// <summary>Field Creation Class for entity MediaRatioTypeMediaEntity</summary>
	public partial class MediaRatioTypeMediaFields
	{
		/// <summary>Creates a new MediaRatioTypeMediaEntity.MediaRatioTypeMediaId field instance</summary>
		public static EntityField2 MediaRatioTypeMediaId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(MediaRatioTypeMediaFieldIndex.MediaRatioTypeMediaId); }
		}
		/// <summary>Creates a new MediaRatioTypeMediaEntity.MediaId field instance</summary>
		public static EntityField2 MediaId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(MediaRatioTypeMediaFieldIndex.MediaId); }
		}
		/// <summary>Creates a new MediaRatioTypeMediaEntity.MediaType field instance</summary>
		public static EntityField2 MediaType 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(MediaRatioTypeMediaFieldIndex.MediaType); }
		}
		/// <summary>Creates a new MediaRatioTypeMediaEntity.Top field instance</summary>
		public static EntityField2 Top 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(MediaRatioTypeMediaFieldIndex.Top); }
		}
		/// <summary>Creates a new MediaRatioTypeMediaEntity.Left field instance</summary>
		public static EntityField2 Left 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(MediaRatioTypeMediaFieldIndex.Left); }
		}
		/// <summary>Creates a new MediaRatioTypeMediaEntity.Width field instance</summary>
		public static EntityField2 Width 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(MediaRatioTypeMediaFieldIndex.Width); }
		}
		/// <summary>Creates a new MediaRatioTypeMediaEntity.Height field instance</summary>
		public static EntityField2 Height 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(MediaRatioTypeMediaFieldIndex.Height); }
		}
		/// <summary>Creates a new MediaRatioTypeMediaEntity.ManuallyVerified field instance</summary>
		public static EntityField2 ManuallyVerified 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(MediaRatioTypeMediaFieldIndex.ManuallyVerified); }
		}
		/// <summary>Creates a new MediaRatioTypeMediaEntity.LastDistributedVersionTicks field instance</summary>
		public static EntityField2 LastDistributedVersionTicks 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(MediaRatioTypeMediaFieldIndex.LastDistributedVersionTicks); }
		}
		/// <summary>Creates a new MediaRatioTypeMediaEntity.LastDistributedVersionTicksAzure field instance</summary>
		public static EntityField2 LastDistributedVersionTicksAzure 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(MediaRatioTypeMediaFieldIndex.LastDistributedVersionTicksAzure); }
		}
		/// <summary>Creates a new MediaRatioTypeMediaEntity.LastDistributedVersionTicksAmazon field instance</summary>
		public static EntityField2 LastDistributedVersionTicksAmazon 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(MediaRatioTypeMediaFieldIndex.LastDistributedVersionTicksAmazon); }
		}
		/// <summary>Creates a new MediaRatioTypeMediaEntity.CreatedBy field instance</summary>
		public static EntityField2 CreatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(MediaRatioTypeMediaFieldIndex.CreatedBy); }
		}
		/// <summary>Creates a new MediaRatioTypeMediaEntity.UpdatedBy field instance</summary>
		public static EntityField2 UpdatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(MediaRatioTypeMediaFieldIndex.UpdatedBy); }
		}
		/// <summary>Creates a new MediaRatioTypeMediaEntity.CreatedUTC field instance</summary>
		public static EntityField2 CreatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(MediaRatioTypeMediaFieldIndex.CreatedUTC); }
		}
		/// <summary>Creates a new MediaRatioTypeMediaEntity.UpdatedUTC field instance</summary>
		public static EntityField2 UpdatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(MediaRatioTypeMediaFieldIndex.UpdatedUTC); }
		}
	}

	/// <summary>Field Creation Class for entity MenuEntity</summary>
	public partial class MenuFields
	{
		/// <summary>Creates a new MenuEntity.MenuId field instance</summary>
		public static EntityField2 MenuId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(MenuFieldIndex.MenuId); }
		}
		/// <summary>Creates a new MenuEntity.CompanyId field instance</summary>
		public static EntityField2 CompanyId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(MenuFieldIndex.CompanyId); }
		}
		/// <summary>Creates a new MenuEntity.Name field instance</summary>
		public static EntityField2 Name 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(MenuFieldIndex.Name); }
		}
		/// <summary>Creates a new MenuEntity.Created field instance</summary>
		public static EntityField2 Created 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(MenuFieldIndex.Created); }
		}
		/// <summary>Creates a new MenuEntity.CreatedBy field instance</summary>
		public static EntityField2 CreatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(MenuFieldIndex.CreatedBy); }
		}
		/// <summary>Creates a new MenuEntity.Updated field instance</summary>
		public static EntityField2 Updated 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(MenuFieldIndex.Updated); }
		}
		/// <summary>Creates a new MenuEntity.UpdatedBy field instance</summary>
		public static EntityField2 UpdatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(MenuFieldIndex.UpdatedBy); }
		}
		/// <summary>Creates a new MenuEntity.CreatedUTC field instance</summary>
		public static EntityField2 CreatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(MenuFieldIndex.CreatedUTC); }
		}
		/// <summary>Creates a new MenuEntity.UpdatedUTC field instance</summary>
		public static EntityField2 UpdatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(MenuFieldIndex.UpdatedUTC); }
		}
	}

	/// <summary>Field Creation Class for entity NetmessageEntity</summary>
	public partial class NetmessageFields
	{
		/// <summary>Creates a new NetmessageEntity.NetmessageId field instance</summary>
		public static EntityField2 NetmessageId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(NetmessageFieldIndex.NetmessageId); }
		}
		/// <summary>Creates a new NetmessageEntity.Guid field instance</summary>
		public static EntityField2 Guid 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(NetmessageFieldIndex.Guid); }
		}
		/// <summary>Creates a new NetmessageEntity.SenderIdentifier field instance</summary>
		public static EntityField2 SenderIdentifier 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(NetmessageFieldIndex.SenderIdentifier); }
		}
		/// <summary>Creates a new NetmessageEntity.ReceiverIdentifier field instance</summary>
		public static EntityField2 ReceiverIdentifier 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(NetmessageFieldIndex.ReceiverIdentifier); }
		}
		/// <summary>Creates a new NetmessageEntity.SenderCustomerId field instance</summary>
		public static EntityField2 SenderCustomerId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(NetmessageFieldIndex.SenderCustomerId); }
		}
		/// <summary>Creates a new NetmessageEntity.SenderClientId field instance</summary>
		public static EntityField2 SenderClientId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(NetmessageFieldIndex.SenderClientId); }
		}
		/// <summary>Creates a new NetmessageEntity.SenderCompanyId field instance</summary>
		public static EntityField2 SenderCompanyId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(NetmessageFieldIndex.SenderCompanyId); }
		}
		/// <summary>Creates a new NetmessageEntity.SenderDeliverypointId field instance</summary>
		public static EntityField2 SenderDeliverypointId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(NetmessageFieldIndex.SenderDeliverypointId); }
		}
		/// <summary>Creates a new NetmessageEntity.SenderTerminalId field instance</summary>
		public static EntityField2 SenderTerminalId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(NetmessageFieldIndex.SenderTerminalId); }
		}
		/// <summary>Creates a new NetmessageEntity.ReceiverCustomerId field instance</summary>
		public static EntityField2 ReceiverCustomerId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(NetmessageFieldIndex.ReceiverCustomerId); }
		}
		/// <summary>Creates a new NetmessageEntity.ReceiverClientId field instance</summary>
		public static EntityField2 ReceiverClientId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(NetmessageFieldIndex.ReceiverClientId); }
		}
		/// <summary>Creates a new NetmessageEntity.ReceiverCompanyId field instance</summary>
		public static EntityField2 ReceiverCompanyId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(NetmessageFieldIndex.ReceiverCompanyId); }
		}
		/// <summary>Creates a new NetmessageEntity.ReceiverDeliverypointId field instance</summary>
		public static EntityField2 ReceiverDeliverypointId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(NetmessageFieldIndex.ReceiverDeliverypointId); }
		}
		/// <summary>Creates a new NetmessageEntity.ReceiverTerminalId field instance</summary>
		public static EntityField2 ReceiverTerminalId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(NetmessageFieldIndex.ReceiverTerminalId); }
		}
		/// <summary>Creates a new NetmessageEntity.MessageType field instance</summary>
		public static EntityField2 MessageType 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(NetmessageFieldIndex.MessageType); }
		}
		/// <summary>Creates a new NetmessageEntity.MessageTypeText field instance</summary>
		public static EntityField2 MessageTypeText 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(NetmessageFieldIndex.MessageTypeText); }
		}
		/// <summary>Creates a new NetmessageEntity.Status field instance</summary>
		public static EntityField2 Status 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(NetmessageFieldIndex.Status); }
		}
		/// <summary>Creates a new NetmessageEntity.StatusText field instance</summary>
		public static EntityField2 StatusText 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(NetmessageFieldIndex.StatusText); }
		}
		/// <summary>Creates a new NetmessageEntity.FieldValue1 field instance</summary>
		public static EntityField2 FieldValue1 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(NetmessageFieldIndex.FieldValue1); }
		}
		/// <summary>Creates a new NetmessageEntity.FieldValue2 field instance</summary>
		public static EntityField2 FieldValue2 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(NetmessageFieldIndex.FieldValue2); }
		}
		/// <summary>Creates a new NetmessageEntity.FieldValue3 field instance</summary>
		public static EntityField2 FieldValue3 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(NetmessageFieldIndex.FieldValue3); }
		}
		/// <summary>Creates a new NetmessageEntity.FieldValue4 field instance</summary>
		public static EntityField2 FieldValue4 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(NetmessageFieldIndex.FieldValue4); }
		}
		/// <summary>Creates a new NetmessageEntity.FieldValue5 field instance</summary>
		public static EntityField2 FieldValue5 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(NetmessageFieldIndex.FieldValue5); }
		}
		/// <summary>Creates a new NetmessageEntity.FieldValue6 field instance</summary>
		public static EntityField2 FieldValue6 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(NetmessageFieldIndex.FieldValue6); }
		}
		/// <summary>Creates a new NetmessageEntity.FieldValue7 field instance</summary>
		public static EntityField2 FieldValue7 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(NetmessageFieldIndex.FieldValue7); }
		}
		/// <summary>Creates a new NetmessageEntity.FieldValue8 field instance</summary>
		public static EntityField2 FieldValue8 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(NetmessageFieldIndex.FieldValue8); }
		}
		/// <summary>Creates a new NetmessageEntity.FieldValue9 field instance</summary>
		public static EntityField2 FieldValue9 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(NetmessageFieldIndex.FieldValue9); }
		}
		/// <summary>Creates a new NetmessageEntity.FieldValue10 field instance</summary>
		public static EntityField2 FieldValue10 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(NetmessageFieldIndex.FieldValue10); }
		}
		/// <summary>Creates a new NetmessageEntity.CreatedBy field instance</summary>
		public static EntityField2 CreatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(NetmessageFieldIndex.CreatedBy); }
		}
		/// <summary>Creates a new NetmessageEntity.UpdatedBy field instance</summary>
		public static EntityField2 UpdatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(NetmessageFieldIndex.UpdatedBy); }
		}
		/// <summary>Creates a new NetmessageEntity.ReceiverDeliverypointgroupId field instance</summary>
		public static EntityField2 ReceiverDeliverypointgroupId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(NetmessageFieldIndex.ReceiverDeliverypointgroupId); }
		}
		/// <summary>Creates a new NetmessageEntity.ErrorMessage field instance</summary>
		public static EntityField2 ErrorMessage 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(NetmessageFieldIndex.ErrorMessage); }
		}
		/// <summary>Creates a new NetmessageEntity.MessageLog field instance</summary>
		public static EntityField2 MessageLog 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(NetmessageFieldIndex.MessageLog); }
		}
		/// <summary>Creates a new NetmessageEntity.CreatedUTC field instance</summary>
		public static EntityField2 CreatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(NetmessageFieldIndex.CreatedUTC); }
		}
		/// <summary>Creates a new NetmessageEntity.UpdatedUTC field instance</summary>
		public static EntityField2 UpdatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(NetmessageFieldIndex.UpdatedUTC); }
		}
	}

	/// <summary>Field Creation Class for entity OptInEntity</summary>
	public partial class OptInFields
	{
		/// <summary>Creates a new OptInEntity.OptInId field instance</summary>
		public static EntityField2 OptInId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OptInFieldIndex.OptInId); }
		}
		/// <summary>Creates a new OptInEntity.ParentCompanyId field instance</summary>
		public static EntityField2 ParentCompanyId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OptInFieldIndex.ParentCompanyId); }
		}
		/// <summary>Creates a new OptInEntity.OutletId field instance</summary>
		public static EntityField2 OutletId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OptInFieldIndex.OutletId); }
		}
		/// <summary>Creates a new OptInEntity.Email field instance</summary>
		public static EntityField2 Email 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OptInFieldIndex.Email); }
		}
		/// <summary>Creates a new OptInEntity.PhoneNumber field instance</summary>
		public static EntityField2 PhoneNumber 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OptInFieldIndex.PhoneNumber); }
		}
		/// <summary>Creates a new OptInEntity.CreatedUTC field instance</summary>
		public static EntityField2 CreatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OptInFieldIndex.CreatedUTC); }
		}
		/// <summary>Creates a new OptInEntity.CreatedBy field instance</summary>
		public static EntityField2 CreatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OptInFieldIndex.CreatedBy); }
		}
		/// <summary>Creates a new OptInEntity.UpdatedUTC field instance</summary>
		public static EntityField2 UpdatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OptInFieldIndex.UpdatedUTC); }
		}
		/// <summary>Creates a new OptInEntity.UpdatedBy field instance</summary>
		public static EntityField2 UpdatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OptInFieldIndex.UpdatedBy); }
		}
		/// <summary>Creates a new OptInEntity.CustomerName field instance</summary>
		public static EntityField2 CustomerName 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OptInFieldIndex.CustomerName); }
		}
	}

	/// <summary>Field Creation Class for entity OrderEntity</summary>
	public partial class OrderFields
	{
		/// <summary>Creates a new OrderEntity.OrderId field instance</summary>
		public static EntityField2 OrderId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderFieldIndex.OrderId); }
		}
		/// <summary>Creates a new OrderEntity.MasterOrderId field instance</summary>
		public static EntityField2 MasterOrderId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderFieldIndex.MasterOrderId); }
		}
		/// <summary>Creates a new OrderEntity.CustomerId field instance</summary>
		public static EntityField2 CustomerId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderFieldIndex.CustomerId); }
		}
		/// <summary>Creates a new OrderEntity.CustomerFirstname field instance</summary>
		public static EntityField2 CustomerFirstname 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderFieldIndex.CustomerFirstname); }
		}
		/// <summary>Creates a new OrderEntity.CustomerLastname field instance</summary>
		public static EntityField2 CustomerLastname 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderFieldIndex.CustomerLastname); }
		}
		/// <summary>Creates a new OrderEntity.CustomerLastnamePrefix field instance</summary>
		public static EntityField2 CustomerLastnamePrefix 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderFieldIndex.CustomerLastnamePrefix); }
		}
		/// <summary>Creates a new OrderEntity.CustomerPhonenumber field instance</summary>
		public static EntityField2 CustomerPhonenumber 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderFieldIndex.CustomerPhonenumber); }
		}
		/// <summary>Creates a new OrderEntity.IsCustomerVip field instance</summary>
		public static EntityField2 IsCustomerVip 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderFieldIndex.IsCustomerVip); }
		}
		/// <summary>Creates a new OrderEntity.ClientId field instance</summary>
		public static EntityField2 ClientId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderFieldIndex.ClientId); }
		}
		/// <summary>Creates a new OrderEntity.ClientMacAddress field instance</summary>
		public static EntityField2 ClientMacAddress 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderFieldIndex.ClientMacAddress); }
		}
		/// <summary>Creates a new OrderEntity.CompanyId field instance</summary>
		public static EntityField2 CompanyId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderFieldIndex.CompanyId); }
		}
		/// <summary>Creates a new OrderEntity.CompanyName field instance</summary>
		public static EntityField2 CompanyName 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderFieldIndex.CompanyName); }
		}
		/// <summary>Creates a new OrderEntity.TimeZoneOlsonId field instance</summary>
		public static EntityField2 TimeZoneOlsonId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderFieldIndex.TimeZoneOlsonId); }
		}
		/// <summary>Creates a new OrderEntity.CurrencyCode field instance</summary>
		public static EntityField2 CurrencyCode 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderFieldIndex.CurrencyCode); }
		}
		/// <summary>Creates a new OrderEntity.DeliverypointId field instance</summary>
		public static EntityField2 DeliverypointId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderFieldIndex.DeliverypointId); }
		}
		/// <summary>Creates a new OrderEntity.DeliverypointName field instance</summary>
		public static EntityField2 DeliverypointName 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderFieldIndex.DeliverypointName); }
		}
		/// <summary>Creates a new OrderEntity.DeliverypointNumber field instance</summary>
		public static EntityField2 DeliverypointNumber 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderFieldIndex.DeliverypointNumber); }
		}
		/// <summary>Creates a new OrderEntity.DeliverypointEnableAnalytics field instance</summary>
		public static EntityField2 DeliverypointEnableAnalytics 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderFieldIndex.DeliverypointEnableAnalytics); }
		}
		/// <summary>Creates a new OrderEntity.DeliverypointgroupName field instance</summary>
		public static EntityField2 DeliverypointgroupName 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderFieldIndex.DeliverypointgroupName); }
		}
		/// <summary>Creates a new OrderEntity.Notes field instance</summary>
		public static EntityField2 Notes 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderFieldIndex.Notes); }
		}
		/// <summary>Creates a new OrderEntity.Type field instance</summary>
		public static EntityField2 Type 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderFieldIndex.Type); }
		}
		/// <summary>Creates a new OrderEntity.TypeText field instance</summary>
		public static EntityField2 TypeText 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderFieldIndex.TypeText); }
		}
		/// <summary>Creates a new OrderEntity.Status field instance</summary>
		public static EntityField2 Status 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderFieldIndex.Status); }
		}
		/// <summary>Creates a new OrderEntity.StatusText field instance</summary>
		public static EntityField2 StatusText 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderFieldIndex.StatusText); }
		}
		/// <summary>Creates a new OrderEntity.ErrorCode field instance</summary>
		public static EntityField2 ErrorCode 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderFieldIndex.ErrorCode); }
		}
		/// <summary>Creates a new OrderEntity.ErrorText field instance</summary>
		public static EntityField2 ErrorText 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderFieldIndex.ErrorText); }
		}
		/// <summary>Creates a new OrderEntity.ErrorSentByEmail field instance</summary>
		public static EntityField2 ErrorSentByEmail 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderFieldIndex.ErrorSentByEmail); }
		}
		/// <summary>Creates a new OrderEntity.ErrorSentBySMS field instance</summary>
		public static EntityField2 ErrorSentBySMS 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderFieldIndex.ErrorSentBySMS); }
		}
		/// <summary>Creates a new OrderEntity.Guid field instance</summary>
		public static EntityField2 Guid 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderFieldIndex.Guid); }
		}
		/// <summary>Creates a new OrderEntity.RoutingLog field instance</summary>
		public static EntityField2 RoutingLog 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderFieldIndex.RoutingLog); }
		}
		/// <summary>Creates a new OrderEntity.Printed field instance</summary>
		public static EntityField2 Printed 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderFieldIndex.Printed); }
		}
		/// <summary>Creates a new OrderEntity.MobileOrder field instance</summary>
		public static EntityField2 MobileOrder 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderFieldIndex.MobileOrder); }
		}
		/// <summary>Creates a new OrderEntity.AnalyticsTrackingIds field instance</summary>
		public static EntityField2 AnalyticsTrackingIds 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderFieldIndex.AnalyticsTrackingIds); }
		}
		/// <summary>Creates a new OrderEntity.CreatedBy field instance</summary>
		public static EntityField2 CreatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderFieldIndex.CreatedBy); }
		}
		/// <summary>Creates a new OrderEntity.UpdatedBy field instance</summary>
		public static EntityField2 UpdatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderFieldIndex.UpdatedBy); }
		}
		/// <summary>Creates a new OrderEntity.Email field instance</summary>
		public static EntityField2 Email 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderFieldIndex.Email); }
		}
		/// <summary>Creates a new OrderEntity.AnalyticsPayLoad field instance</summary>
		public static EntityField2 AnalyticsPayLoad 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderFieldIndex.AnalyticsPayLoad); }
		}
		/// <summary>Creates a new OrderEntity.HotSOSServiceOrderId field instance</summary>
		public static EntityField2 HotSOSServiceOrderId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderFieldIndex.HotSOSServiceOrderId); }
		}
		/// <summary>Creates a new OrderEntity.ExternalOrderId field instance</summary>
		public static EntityField2 ExternalOrderId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderFieldIndex.ExternalOrderId); }
		}
		/// <summary>Creates a new OrderEntity.CreatedUTC field instance</summary>
		public static EntityField2 CreatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderFieldIndex.CreatedUTC); }
		}
		/// <summary>Creates a new OrderEntity.UpdatedUTC field instance</summary>
		public static EntityField2 UpdatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderFieldIndex.UpdatedUTC); }
		}
		/// <summary>Creates a new OrderEntity.ProcessedUTC field instance</summary>
		public static EntityField2 ProcessedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderFieldIndex.ProcessedUTC); }
		}
		/// <summary>Creates a new OrderEntity.PlaceTimeUTC field instance</summary>
		public static EntityField2 PlaceTimeUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderFieldIndex.PlaceTimeUTC); }
		}
		/// <summary>Creates a new OrderEntity.CheckoutMethodId field instance</summary>
		public static EntityField2 CheckoutMethodId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderFieldIndex.CheckoutMethodId); }
		}
		/// <summary>Creates a new OrderEntity.ServiceMethodId field instance</summary>
		public static EntityField2 ServiceMethodId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderFieldIndex.ServiceMethodId); }
		}
		/// <summary>Creates a new OrderEntity.CheckoutMethodName field instance</summary>
		public static EntityField2 CheckoutMethodName 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderFieldIndex.CheckoutMethodName); }
		}
		/// <summary>Creates a new OrderEntity.CheckoutMethodType field instance</summary>
		public static EntityField2 CheckoutMethodType 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderFieldIndex.CheckoutMethodType); }
		}
		/// <summary>Creates a new OrderEntity.ServiceMethodName field instance</summary>
		public static EntityField2 ServiceMethodName 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderFieldIndex.ServiceMethodName); }
		}
		/// <summary>Creates a new OrderEntity.ServiceMethodType field instance</summary>
		public static EntityField2 ServiceMethodType 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderFieldIndex.ServiceMethodType); }
		}
		/// <summary>Creates a new OrderEntity.CultureCode field instance</summary>
		public static EntityField2 CultureCode 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderFieldIndex.CultureCode); }
		}
		/// <summary>Creates a new OrderEntity.Total field instance</summary>
		public static EntityField2 Total 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderFieldIndex.Total); }
		}
		/// <summary>Creates a new OrderEntity.SubTotal field instance</summary>
		public static EntityField2 SubTotal 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderFieldIndex.SubTotal); }
		}
		/// <summary>Creates a new OrderEntity.DeliveryInformationId field instance</summary>
		public static EntityField2 DeliveryInformationId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderFieldIndex.DeliveryInformationId); }
		}
		/// <summary>Creates a new OrderEntity.ChargeToDeliverypointId field instance</summary>
		public static EntityField2 ChargeToDeliverypointId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderFieldIndex.ChargeToDeliverypointId); }
		}
		/// <summary>Creates a new OrderEntity.ChargeToDeliverypointName field instance</summary>
		public static EntityField2 ChargeToDeliverypointName 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderFieldIndex.ChargeToDeliverypointName); }
		}
		/// <summary>Creates a new OrderEntity.ChargeToDeliverypointNumber field instance</summary>
		public static EntityField2 ChargeToDeliverypointNumber 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderFieldIndex.ChargeToDeliverypointNumber); }
		}
		/// <summary>Creates a new OrderEntity.ChargeToDeliverypointgroupName field instance</summary>
		public static EntityField2 ChargeToDeliverypointgroupName 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderFieldIndex.ChargeToDeliverypointgroupName); }
		}
		/// <summary>Creates a new OrderEntity.OutletId field instance</summary>
		public static EntityField2 OutletId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderFieldIndex.OutletId); }
		}
		/// <summary>Creates a new OrderEntity.PricesIncludeTaxes field instance</summary>
		public static EntityField2 PricesIncludeTaxes 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderFieldIndex.PricesIncludeTaxes); }
		}
		/// <summary>Creates a new OrderEntity.OptInStatus field instance</summary>
		public static EntityField2 OptInStatus 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderFieldIndex.OptInStatus); }
		}
		/// <summary>Creates a new OrderEntity.TaxBreakdown field instance</summary>
		public static EntityField2 TaxBreakdown 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderFieldIndex.TaxBreakdown); }
		}
		/// <summary>Creates a new OrderEntity.CountryCode field instance</summary>
		public static EntityField2 CountryCode 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderFieldIndex.CountryCode); }
		}
		/// <summary>Creates a new OrderEntity.CoversCount field instance</summary>
		public static EntityField2 CoversCount 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderFieldIndex.CoversCount); }
		}
	}

	/// <summary>Field Creation Class for entity OrderitemEntity</summary>
	public partial class OrderitemFields
	{
		/// <summary>Creates a new OrderitemEntity.OrderitemId field instance</summary>
		public static EntityField2 OrderitemId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderitemFieldIndex.OrderitemId); }
		}
		/// <summary>Creates a new OrderitemEntity.ParentCompanyId field instance</summary>
		public static EntityField2 ParentCompanyId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderitemFieldIndex.ParentCompanyId); }
		}
		/// <summary>Creates a new OrderitemEntity.OrderId field instance</summary>
		public static EntityField2 OrderId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderitemFieldIndex.OrderId); }
		}
		/// <summary>Creates a new OrderitemEntity.CategoryId field instance</summary>
		public static EntityField2 CategoryId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderitemFieldIndex.CategoryId); }
		}
		/// <summary>Creates a new OrderitemEntity.CategoryName field instance</summary>
		public static EntityField2 CategoryName 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderitemFieldIndex.CategoryName); }
		}
		/// <summary>Creates a new OrderitemEntity.CategoryPath field instance</summary>
		public static EntityField2 CategoryPath 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderitemFieldIndex.CategoryPath); }
		}
		/// <summary>Creates a new OrderitemEntity.ProductId field instance</summary>
		public static EntityField2 ProductId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderitemFieldIndex.ProductId); }
		}
		/// <summary>Creates a new OrderitemEntity.ProductDescription field instance</summary>
		public static EntityField2 ProductDescription 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderitemFieldIndex.ProductDescription); }
		}
		/// <summary>Creates a new OrderitemEntity.ProductName field instance</summary>
		public static EntityField2 ProductName 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderitemFieldIndex.ProductName); }
		}
		/// <summary>Creates a new OrderitemEntity.ProductPriceIn field instance</summary>
		public static EntityField2 ProductPriceIn 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderitemFieldIndex.ProductPriceIn); }
		}
		/// <summary>Creates a new OrderitemEntity.Quantity field instance</summary>
		public static EntityField2 Quantity 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderitemFieldIndex.Quantity); }
		}
		/// <summary>Creates a new OrderitemEntity.VatPercentage field instance</summary>
		public static EntityField2 VatPercentage 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderitemFieldIndex.VatPercentage); }
		}
		/// <summary>Creates a new OrderitemEntity.Notes field instance</summary>
		public static EntityField2 Notes 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderitemFieldIndex.Notes); }
		}
		/// <summary>Creates a new OrderitemEntity.CreatedBy field instance</summary>
		public static EntityField2 CreatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderitemFieldIndex.CreatedBy); }
		}
		/// <summary>Creates a new OrderitemEntity.UpdatedBy field instance</summary>
		public static EntityField2 UpdatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderitemFieldIndex.UpdatedBy); }
		}
		/// <summary>Creates a new OrderitemEntity.Guid field instance</summary>
		public static EntityField2 Guid 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderitemFieldIndex.Guid); }
		}
		/// <summary>Creates a new OrderitemEntity.Color field instance</summary>
		public static EntityField2 Color 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderitemFieldIndex.Color); }
		}
		/// <summary>Creates a new OrderitemEntity.OrderSource field instance</summary>
		public static EntityField2 OrderSource 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderitemFieldIndex.OrderSource); }
		}
		/// <summary>Creates a new OrderitemEntity.PriceLevelItemId field instance</summary>
		public static EntityField2 PriceLevelItemId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderitemFieldIndex.PriceLevelItemId); }
		}
		/// <summary>Creates a new OrderitemEntity.PriceLevelLog field instance</summary>
		public static EntityField2 PriceLevelLog 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderitemFieldIndex.PriceLevelLog); }
		}
		/// <summary>Creates a new OrderitemEntity.CreatedUTC field instance</summary>
		public static EntityField2 CreatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderitemFieldIndex.CreatedUTC); }
		}
		/// <summary>Creates a new OrderitemEntity.UpdatedUTC field instance</summary>
		public static EntityField2 UpdatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderitemFieldIndex.UpdatedUTC); }
		}
		/// <summary>Creates a new OrderitemEntity.Type field instance</summary>
		public static EntityField2 Type 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderitemFieldIndex.Type); }
		}
		/// <summary>Creates a new OrderitemEntity.PriceInTax field instance</summary>
		public static EntityField2 PriceInTax 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderitemFieldIndex.PriceInTax); }
		}
		/// <summary>Creates a new OrderitemEntity.PriceExTax field instance</summary>
		public static EntityField2 PriceExTax 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderitemFieldIndex.PriceExTax); }
		}
		/// <summary>Creates a new OrderitemEntity.PriceTotalInTax field instance</summary>
		public static EntityField2 PriceTotalInTax 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderitemFieldIndex.PriceTotalInTax); }
		}
		/// <summary>Creates a new OrderitemEntity.PriceTotalExTax field instance</summary>
		public static EntityField2 PriceTotalExTax 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderitemFieldIndex.PriceTotalExTax); }
		}
		/// <summary>Creates a new OrderitemEntity.TaxPercentage field instance</summary>
		public static EntityField2 TaxPercentage 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderitemFieldIndex.TaxPercentage); }
		}
		/// <summary>Creates a new OrderitemEntity.TaxTariffId field instance</summary>
		public static EntityField2 TaxTariffId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderitemFieldIndex.TaxTariffId); }
		}
		/// <summary>Creates a new OrderitemEntity.Tax field instance</summary>
		public static EntityField2 Tax 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderitemFieldIndex.Tax); }
		}
		/// <summary>Creates a new OrderitemEntity.TaxTotal field instance</summary>
		public static EntityField2 TaxTotal 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderitemFieldIndex.TaxTotal); }
		}
		/// <summary>Creates a new OrderitemEntity.TaxName field instance</summary>
		public static EntityField2 TaxName 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderitemFieldIndex.TaxName); }
		}
		/// <summary>Creates a new OrderitemEntity.ExternalIdentifier field instance</summary>
		public static EntityField2 ExternalIdentifier 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderitemFieldIndex.ExternalIdentifier); }
		}
	}

	/// <summary>Field Creation Class for entity OrderitemAlterationitemEntity</summary>
	public partial class OrderitemAlterationitemFields
	{
		/// <summary>Creates a new OrderitemAlterationitemEntity.OrderitemAlterationitemId field instance</summary>
		public static EntityField2 OrderitemAlterationitemId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderitemAlterationitemFieldIndex.OrderitemAlterationitemId); }
		}
		/// <summary>Creates a new OrderitemAlterationitemEntity.ParentCompanyId field instance</summary>
		public static EntityField2 ParentCompanyId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderitemAlterationitemFieldIndex.ParentCompanyId); }
		}
		/// <summary>Creates a new OrderitemAlterationitemEntity.OrderitemId field instance</summary>
		public static EntityField2 OrderitemId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderitemAlterationitemFieldIndex.OrderitemId); }
		}
		/// <summary>Creates a new OrderitemAlterationitemEntity.AlterationId field instance</summary>
		public static EntityField2 AlterationId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderitemAlterationitemFieldIndex.AlterationId); }
		}
		/// <summary>Creates a new OrderitemAlterationitemEntity.AlterationName field instance</summary>
		public static EntityField2 AlterationName 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderitemAlterationitemFieldIndex.AlterationName); }
		}
		/// <summary>Creates a new OrderitemAlterationitemEntity.AlterationType field instance</summary>
		public static EntityField2 AlterationType 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderitemAlterationitemFieldIndex.AlterationType); }
		}
		/// <summary>Creates a new OrderitemAlterationitemEntity.AlterationitemId field instance</summary>
		public static EntityField2 AlterationitemId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderitemAlterationitemFieldIndex.AlterationitemId); }
		}
		/// <summary>Creates a new OrderitemAlterationitemEntity.AlterationoptionName field instance</summary>
		public static EntityField2 AlterationoptionName 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderitemAlterationitemFieldIndex.AlterationoptionName); }
		}
		/// <summary>Creates a new OrderitemAlterationitemEntity.AlterationoptionType field instance</summary>
		public static EntityField2 AlterationoptionType 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderitemAlterationitemFieldIndex.AlterationoptionType); }
		}
		/// <summary>Creates a new OrderitemAlterationitemEntity.AlterationoptionPriceIn field instance</summary>
		public static EntityField2 AlterationoptionPriceIn 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderitemAlterationitemFieldIndex.AlterationoptionPriceIn); }
		}
		/// <summary>Creates a new OrderitemAlterationitemEntity.Time field instance</summary>
		public static EntityField2 Time 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderitemAlterationitemFieldIndex.Time); }
		}
		/// <summary>Creates a new OrderitemAlterationitemEntity.Value field instance</summary>
		public static EntityField2 Value 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderitemAlterationitemFieldIndex.Value); }
		}
		/// <summary>Creates a new OrderitemAlterationitemEntity.FormerAlterationitemId field instance</summary>
		public static EntityField2 FormerAlterationitemId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderitemAlterationitemFieldIndex.FormerAlterationitemId); }
		}
		/// <summary>Creates a new OrderitemAlterationitemEntity.Guid field instance</summary>
		public static EntityField2 Guid 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderitemAlterationitemFieldIndex.Guid); }
		}
		/// <summary>Creates a new OrderitemAlterationitemEntity.PriceLevelItemId field instance</summary>
		public static EntityField2 PriceLevelItemId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderitemAlterationitemFieldIndex.PriceLevelItemId); }
		}
		/// <summary>Creates a new OrderitemAlterationitemEntity.PriceLevelLog field instance</summary>
		public static EntityField2 PriceLevelLog 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderitemAlterationitemFieldIndex.PriceLevelLog); }
		}
		/// <summary>Creates a new OrderitemAlterationitemEntity.TimeUTC field instance</summary>
		public static EntityField2 TimeUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderitemAlterationitemFieldIndex.TimeUTC); }
		}
		/// <summary>Creates a new OrderitemAlterationitemEntity.CreatedBy field instance</summary>
		public static EntityField2 CreatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderitemAlterationitemFieldIndex.CreatedBy); }
		}
		/// <summary>Creates a new OrderitemAlterationitemEntity.UpdatedBy field instance</summary>
		public static EntityField2 UpdatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderitemAlterationitemFieldIndex.UpdatedBy); }
		}
		/// <summary>Creates a new OrderitemAlterationitemEntity.CreatedUTC field instance</summary>
		public static EntityField2 CreatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderitemAlterationitemFieldIndex.CreatedUTC); }
		}
		/// <summary>Creates a new OrderitemAlterationitemEntity.UpdatedUTC field instance</summary>
		public static EntityField2 UpdatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderitemAlterationitemFieldIndex.UpdatedUTC); }
		}
		/// <summary>Creates a new OrderitemAlterationitemEntity.OrderLevelEnabled field instance</summary>
		public static EntityField2 OrderLevelEnabled 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderitemAlterationitemFieldIndex.OrderLevelEnabled); }
		}
		/// <summary>Creates a new OrderitemAlterationitemEntity.PriceInTax field instance</summary>
		public static EntityField2 PriceInTax 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderitemAlterationitemFieldIndex.PriceInTax); }
		}
		/// <summary>Creates a new OrderitemAlterationitemEntity.PriceExTax field instance</summary>
		public static EntityField2 PriceExTax 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderitemAlterationitemFieldIndex.PriceExTax); }
		}
		/// <summary>Creates a new OrderitemAlterationitemEntity.PriceTotalInTax field instance</summary>
		public static EntityField2 PriceTotalInTax 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderitemAlterationitemFieldIndex.PriceTotalInTax); }
		}
		/// <summary>Creates a new OrderitemAlterationitemEntity.PriceTotalExTax field instance</summary>
		public static EntityField2 PriceTotalExTax 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderitemAlterationitemFieldIndex.PriceTotalExTax); }
		}
		/// <summary>Creates a new OrderitemAlterationitemEntity.TaxPercentage field instance</summary>
		public static EntityField2 TaxPercentage 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderitemAlterationitemFieldIndex.TaxPercentage); }
		}
		/// <summary>Creates a new OrderitemAlterationitemEntity.TaxTariffId field instance</summary>
		public static EntityField2 TaxTariffId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderitemAlterationitemFieldIndex.TaxTariffId); }
		}
		/// <summary>Creates a new OrderitemAlterationitemEntity.Tax field instance</summary>
		public static EntityField2 Tax 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderitemAlterationitemFieldIndex.Tax); }
		}
		/// <summary>Creates a new OrderitemAlterationitemEntity.TaxTotal field instance</summary>
		public static EntityField2 TaxTotal 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderitemAlterationitemFieldIndex.TaxTotal); }
		}
		/// <summary>Creates a new OrderitemAlterationitemEntity.TaxName field instance</summary>
		public static EntityField2 TaxName 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderitemAlterationitemFieldIndex.TaxName); }
		}
		/// <summary>Creates a new OrderitemAlterationitemEntity.AlterationoptionProductId field instance</summary>
		public static EntityField2 AlterationoptionProductId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderitemAlterationitemFieldIndex.AlterationoptionProductId); }
		}
		/// <summary>Creates a new OrderitemAlterationitemEntity.ExternalIdentifier field instance</summary>
		public static EntityField2 ExternalIdentifier 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderitemAlterationitemFieldIndex.ExternalIdentifier); }
		}
	}

	/// <summary>Field Creation Class for entity OrderitemAlterationitemTagEntity</summary>
	public partial class OrderitemAlterationitemTagFields
	{
		/// <summary>Creates a new OrderitemAlterationitemTagEntity.OrderitemAlterationitemTagId field instance</summary>
		public static EntityField2 OrderitemAlterationitemTagId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderitemAlterationitemTagFieldIndex.OrderitemAlterationitemTagId); }
		}
		/// <summary>Creates a new OrderitemAlterationitemTagEntity.OrderitemAlterationitemId field instance</summary>
		public static EntityField2 OrderitemAlterationitemId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderitemAlterationitemTagFieldIndex.OrderitemAlterationitemId); }
		}
		/// <summary>Creates a new OrderitemAlterationitemTagEntity.AlterationoptionId field instance</summary>
		public static EntityField2 AlterationoptionId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderitemAlterationitemTagFieldIndex.AlterationoptionId); }
		}
		/// <summary>Creates a new OrderitemAlterationitemTagEntity.TagId field instance</summary>
		public static EntityField2 TagId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderitemAlterationitemTagFieldIndex.TagId); }
		}
		/// <summary>Creates a new OrderitemAlterationitemTagEntity.ParentCompanyId field instance</summary>
		public static EntityField2 ParentCompanyId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderitemAlterationitemTagFieldIndex.ParentCompanyId); }
		}
		/// <summary>Creates a new OrderitemAlterationitemTagEntity.CreatedUTC field instance</summary>
		public static EntityField2 CreatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderitemAlterationitemTagFieldIndex.CreatedUTC); }
		}
		/// <summary>Creates a new OrderitemAlterationitemTagEntity.UpdatedUTC field instance</summary>
		public static EntityField2 UpdatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderitemAlterationitemTagFieldIndex.UpdatedUTC); }
		}
	}

	/// <summary>Field Creation Class for entity OrderitemTagEntity</summary>
	public partial class OrderitemTagFields
	{
		/// <summary>Creates a new OrderitemTagEntity.OrderitemTagId field instance</summary>
		public static EntityField2 OrderitemTagId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderitemTagFieldIndex.OrderitemTagId); }
		}
		/// <summary>Creates a new OrderitemTagEntity.OrderitemId field instance</summary>
		public static EntityField2 OrderitemId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderitemTagFieldIndex.OrderitemId); }
		}
		/// <summary>Creates a new OrderitemTagEntity.ProductCategoryTagId field instance</summary>
		public static EntityField2 ProductCategoryTagId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderitemTagFieldIndex.ProductCategoryTagId); }
		}
		/// <summary>Creates a new OrderitemTagEntity.ProductId field instance</summary>
		public static EntityField2 ProductId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderitemTagFieldIndex.ProductId); }
		}
		/// <summary>Creates a new OrderitemTagEntity.TagId field instance</summary>
		public static EntityField2 TagId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderitemTagFieldIndex.TagId); }
		}
		/// <summary>Creates a new OrderitemTagEntity.ParentCompanyId field instance</summary>
		public static EntityField2 ParentCompanyId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderitemTagFieldIndex.ParentCompanyId); }
		}
		/// <summary>Creates a new OrderitemTagEntity.CreatedUTC field instance</summary>
		public static EntityField2 CreatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderitemTagFieldIndex.CreatedUTC); }
		}
		/// <summary>Creates a new OrderitemTagEntity.UpdatedUTC field instance</summary>
		public static EntityField2 UpdatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderitemTagFieldIndex.UpdatedUTC); }
		}
	}

	/// <summary>Field Creation Class for entity OrderNotificationLogEntity</summary>
	public partial class OrderNotificationLogFields
	{
		/// <summary>Creates a new OrderNotificationLogEntity.OrderNotificationLogId field instance</summary>
		public static EntityField2 OrderNotificationLogId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderNotificationLogFieldIndex.OrderNotificationLogId); }
		}
		/// <summary>Creates a new OrderNotificationLogEntity.OrderId field instance</summary>
		public static EntityField2 OrderId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderNotificationLogFieldIndex.OrderId); }
		}
		/// <summary>Creates a new OrderNotificationLogEntity.ParentCompanyId field instance</summary>
		public static EntityField2 ParentCompanyId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderNotificationLogFieldIndex.ParentCompanyId); }
		}
		/// <summary>Creates a new OrderNotificationLogEntity.Type field instance</summary>
		public static EntityField2 Type 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderNotificationLogFieldIndex.Type); }
		}
		/// <summary>Creates a new OrderNotificationLogEntity.Method field instance</summary>
		public static EntityField2 Method 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderNotificationLogFieldIndex.Method); }
		}
		/// <summary>Creates a new OrderNotificationLogEntity.Status field instance</summary>
		public static EntityField2 Status 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderNotificationLogFieldIndex.Status); }
		}
		/// <summary>Creates a new OrderNotificationLogEntity.Receiver field instance</summary>
		public static EntityField2 Receiver 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderNotificationLogFieldIndex.Receiver); }
		}
		/// <summary>Creates a new OrderNotificationLogEntity.Subject field instance</summary>
		public static EntityField2 Subject 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderNotificationLogFieldIndex.Subject); }
		}
		/// <summary>Creates a new OrderNotificationLogEntity.Message field instance</summary>
		public static EntityField2 Message 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderNotificationLogFieldIndex.Message); }
		}
		/// <summary>Creates a new OrderNotificationLogEntity.Log field instance</summary>
		public static EntityField2 Log 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderNotificationLogFieldIndex.Log); }
		}
		/// <summary>Creates a new OrderNotificationLogEntity.CreatedUTC field instance</summary>
		public static EntityField2 CreatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderNotificationLogFieldIndex.CreatedUTC); }
		}
		/// <summary>Creates a new OrderNotificationLogEntity.SentUTC field instance</summary>
		public static EntityField2 SentUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderNotificationLogFieldIndex.SentUTC); }
		}
	}

	/// <summary>Field Creation Class for entity OrderRoutestephandlerEntity</summary>
	public partial class OrderRoutestephandlerFields
	{
		/// <summary>Creates a new OrderRoutestephandlerEntity.OrderRoutestephandlerId field instance</summary>
		public static EntityField2 OrderRoutestephandlerId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderRoutestephandlerFieldIndex.OrderRoutestephandlerId); }
		}
		/// <summary>Creates a new OrderRoutestephandlerEntity.ParentCompanyId field instance</summary>
		public static EntityField2 ParentCompanyId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderRoutestephandlerFieldIndex.ParentCompanyId); }
		}
		/// <summary>Creates a new OrderRoutestephandlerEntity.Guid field instance</summary>
		public static EntityField2 Guid 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderRoutestephandlerFieldIndex.Guid); }
		}
		/// <summary>Creates a new OrderRoutestephandlerEntity.OrderId field instance</summary>
		public static EntityField2 OrderId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderRoutestephandlerFieldIndex.OrderId); }
		}
		/// <summary>Creates a new OrderRoutestephandlerEntity.TerminalId field instance</summary>
		public static EntityField2 TerminalId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderRoutestephandlerFieldIndex.TerminalId); }
		}
		/// <summary>Creates a new OrderRoutestephandlerEntity.ForwardedFromTerminalId field instance</summary>
		public static EntityField2 ForwardedFromTerminalId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderRoutestephandlerFieldIndex.ForwardedFromTerminalId); }
		}
		/// <summary>Creates a new OrderRoutestephandlerEntity.Number field instance</summary>
		public static EntityField2 Number 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderRoutestephandlerFieldIndex.Number); }
		}
		/// <summary>Creates a new OrderRoutestephandlerEntity.ContinueOnFailure field instance</summary>
		public static EntityField2 ContinueOnFailure 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderRoutestephandlerFieldIndex.ContinueOnFailure); }
		}
		/// <summary>Creates a new OrderRoutestephandlerEntity.CompleteRouteOnComplete field instance</summary>
		public static EntityField2 CompleteRouteOnComplete 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderRoutestephandlerFieldIndex.CompleteRouteOnComplete); }
		}
		/// <summary>Creates a new OrderRoutestephandlerEntity.HandlerType field instance</summary>
		public static EntityField2 HandlerType 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderRoutestephandlerFieldIndex.HandlerType); }
		}
		/// <summary>Creates a new OrderRoutestephandlerEntity.HandlerTypeText field instance</summary>
		public static EntityField2 HandlerTypeText 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderRoutestephandlerFieldIndex.HandlerTypeText); }
		}
		/// <summary>Creates a new OrderRoutestephandlerEntity.PrintReportType field instance</summary>
		public static EntityField2 PrintReportType 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderRoutestephandlerFieldIndex.PrintReportType); }
		}
		/// <summary>Creates a new OrderRoutestephandlerEntity.EscalationRouteId field instance</summary>
		public static EntityField2 EscalationRouteId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderRoutestephandlerFieldIndex.EscalationRouteId); }
		}
		/// <summary>Creates a new OrderRoutestephandlerEntity.Status field instance</summary>
		public static EntityField2 Status 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderRoutestephandlerFieldIndex.Status); }
		}
		/// <summary>Creates a new OrderRoutestephandlerEntity.StatusText field instance</summary>
		public static EntityField2 StatusText 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderRoutestephandlerFieldIndex.StatusText); }
		}
		/// <summary>Creates a new OrderRoutestephandlerEntity.Timeout field instance</summary>
		public static EntityField2 Timeout 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderRoutestephandlerFieldIndex.Timeout); }
		}
		/// <summary>Creates a new OrderRoutestephandlerEntity.RetrievalSupportNotificationTimeoutMinutes field instance</summary>
		public static EntityField2 RetrievalSupportNotificationTimeoutMinutes 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderRoutestephandlerFieldIndex.RetrievalSupportNotificationTimeoutMinutes); }
		}
		/// <summary>Creates a new OrderRoutestephandlerEntity.RetrievalSupportNotificationSent field instance</summary>
		public static EntityField2 RetrievalSupportNotificationSent 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderRoutestephandlerFieldIndex.RetrievalSupportNotificationSent); }
		}
		/// <summary>Creates a new OrderRoutestephandlerEntity.BeingHandledSupportNotificationTimeoutMinutes field instance</summary>
		public static EntityField2 BeingHandledSupportNotificationTimeoutMinutes 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderRoutestephandlerFieldIndex.BeingHandledSupportNotificationTimeoutMinutes); }
		}
		/// <summary>Creates a new OrderRoutestephandlerEntity.BeingHandledSupportNotificationSent field instance</summary>
		public static EntityField2 BeingHandledSupportNotificationSent 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderRoutestephandlerFieldIndex.BeingHandledSupportNotificationSent); }
		}
		/// <summary>Creates a new OrderRoutestephandlerEntity.ErrorCode field instance</summary>
		public static EntityField2 ErrorCode 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderRoutestephandlerFieldIndex.ErrorCode); }
		}
		/// <summary>Creates a new OrderRoutestephandlerEntity.ErrorText field instance</summary>
		public static EntityField2 ErrorText 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderRoutestephandlerFieldIndex.ErrorText); }
		}
		/// <summary>Creates a new OrderRoutestephandlerEntity.EscalationStep field instance</summary>
		public static EntityField2 EscalationStep 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderRoutestephandlerFieldIndex.EscalationStep); }
		}
		/// <summary>Creates a new OrderRoutestephandlerEntity.FieldValue1 field instance</summary>
		public static EntityField2 FieldValue1 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderRoutestephandlerFieldIndex.FieldValue1); }
		}
		/// <summary>Creates a new OrderRoutestephandlerEntity.FieldValue2 field instance</summary>
		public static EntityField2 FieldValue2 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderRoutestephandlerFieldIndex.FieldValue2); }
		}
		/// <summary>Creates a new OrderRoutestephandlerEntity.FieldValue3 field instance</summary>
		public static EntityField2 FieldValue3 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderRoutestephandlerFieldIndex.FieldValue3); }
		}
		/// <summary>Creates a new OrderRoutestephandlerEntity.FieldValue4 field instance</summary>
		public static EntityField2 FieldValue4 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderRoutestephandlerFieldIndex.FieldValue4); }
		}
		/// <summary>Creates a new OrderRoutestephandlerEntity.FieldValue5 field instance</summary>
		public static EntityField2 FieldValue5 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderRoutestephandlerFieldIndex.FieldValue5); }
		}
		/// <summary>Creates a new OrderRoutestephandlerEntity.FieldValue6 field instance</summary>
		public static EntityField2 FieldValue6 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderRoutestephandlerFieldIndex.FieldValue6); }
		}
		/// <summary>Creates a new OrderRoutestephandlerEntity.FieldValue7 field instance</summary>
		public static EntityField2 FieldValue7 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderRoutestephandlerFieldIndex.FieldValue7); }
		}
		/// <summary>Creates a new OrderRoutestephandlerEntity.FieldValue8 field instance</summary>
		public static EntityField2 FieldValue8 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderRoutestephandlerFieldIndex.FieldValue8); }
		}
		/// <summary>Creates a new OrderRoutestephandlerEntity.FieldValue9 field instance</summary>
		public static EntityField2 FieldValue9 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderRoutestephandlerFieldIndex.FieldValue9); }
		}
		/// <summary>Creates a new OrderRoutestephandlerEntity.FieldValue10 field instance</summary>
		public static EntityField2 FieldValue10 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderRoutestephandlerFieldIndex.FieldValue10); }
		}
		/// <summary>Creates a new OrderRoutestephandlerEntity.LogAlways field instance</summary>
		public static EntityField2 LogAlways 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderRoutestephandlerFieldIndex.LogAlways); }
		}
		/// <summary>Creates a new OrderRoutestephandlerEntity.SupportpoolId field instance</summary>
		public static EntityField2 SupportpoolId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderRoutestephandlerFieldIndex.SupportpoolId); }
		}
		/// <summary>Creates a new OrderRoutestephandlerEntity.OriginatedFromRoutestepHandlerId field instance</summary>
		public static EntityField2 OriginatedFromRoutestepHandlerId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderRoutestephandlerFieldIndex.OriginatedFromRoutestepHandlerId); }
		}
		/// <summary>Creates a new OrderRoutestephandlerEntity.CreatedBy field instance</summary>
		public static EntityField2 CreatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderRoutestephandlerFieldIndex.CreatedBy); }
		}
		/// <summary>Creates a new OrderRoutestephandlerEntity.UpdatedBy field instance</summary>
		public static EntityField2 UpdatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderRoutestephandlerFieldIndex.UpdatedBy); }
		}
		/// <summary>Creates a new OrderRoutestephandlerEntity.CreatedUTC field instance</summary>
		public static EntityField2 CreatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderRoutestephandlerFieldIndex.CreatedUTC); }
		}
		/// <summary>Creates a new OrderRoutestephandlerEntity.UpdatedUTC field instance</summary>
		public static EntityField2 UpdatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderRoutestephandlerFieldIndex.UpdatedUTC); }
		}
		/// <summary>Creates a new OrderRoutestephandlerEntity.TimeoutExpiresUTC field instance</summary>
		public static EntityField2 TimeoutExpiresUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderRoutestephandlerFieldIndex.TimeoutExpiresUTC); }
		}
		/// <summary>Creates a new OrderRoutestephandlerEntity.RetrievalSupportNotificationTimeoutUTC field instance</summary>
		public static EntityField2 RetrievalSupportNotificationTimeoutUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderRoutestephandlerFieldIndex.RetrievalSupportNotificationTimeoutUTC); }
		}
		/// <summary>Creates a new OrderRoutestephandlerEntity.BeingHandledSupportNotificationTimeoutUTC field instance</summary>
		public static EntityField2 BeingHandledSupportNotificationTimeoutUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderRoutestephandlerFieldIndex.BeingHandledSupportNotificationTimeoutUTC); }
		}
		/// <summary>Creates a new OrderRoutestephandlerEntity.WaitingToBeRetrievedUTC field instance</summary>
		public static EntityField2 WaitingToBeRetrievedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderRoutestephandlerFieldIndex.WaitingToBeRetrievedUTC); }
		}
		/// <summary>Creates a new OrderRoutestephandlerEntity.RetrievedByHandlerUTC field instance</summary>
		public static EntityField2 RetrievedByHandlerUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderRoutestephandlerFieldIndex.RetrievedByHandlerUTC); }
		}
		/// <summary>Creates a new OrderRoutestephandlerEntity.BeingHandledUTC field instance</summary>
		public static EntityField2 BeingHandledUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderRoutestephandlerFieldIndex.BeingHandledUTC); }
		}
		/// <summary>Creates a new OrderRoutestephandlerEntity.CompletedUTC field instance</summary>
		public static EntityField2 CompletedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderRoutestephandlerFieldIndex.CompletedUTC); }
		}
		/// <summary>Creates a new OrderRoutestephandlerEntity.ExternalSystemId field instance</summary>
		public static EntityField2 ExternalSystemId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderRoutestephandlerFieldIndex.ExternalSystemId); }
		}
	}

	/// <summary>Field Creation Class for entity OrderRoutestephandlerHistoryEntity</summary>
	public partial class OrderRoutestephandlerHistoryFields
	{
		/// <summary>Creates a new OrderRoutestephandlerHistoryEntity.OrderRoutestephandlerHistoryId field instance</summary>
		public static EntityField2 OrderRoutestephandlerHistoryId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderRoutestephandlerHistoryFieldIndex.OrderRoutestephandlerHistoryId); }
		}
		/// <summary>Creates a new OrderRoutestephandlerHistoryEntity.ParentCompanyId field instance</summary>
		public static EntityField2 ParentCompanyId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderRoutestephandlerHistoryFieldIndex.ParentCompanyId); }
		}
		/// <summary>Creates a new OrderRoutestephandlerHistoryEntity.Guid field instance</summary>
		public static EntityField2 Guid 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderRoutestephandlerHistoryFieldIndex.Guid); }
		}
		/// <summary>Creates a new OrderRoutestephandlerHistoryEntity.OrderId field instance</summary>
		public static EntityField2 OrderId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderRoutestephandlerHistoryFieldIndex.OrderId); }
		}
		/// <summary>Creates a new OrderRoutestephandlerHistoryEntity.TerminalId field instance</summary>
		public static EntityField2 TerminalId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderRoutestephandlerHistoryFieldIndex.TerminalId); }
		}
		/// <summary>Creates a new OrderRoutestephandlerHistoryEntity.ForwardedFromTerminalId field instance</summary>
		public static EntityField2 ForwardedFromTerminalId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderRoutestephandlerHistoryFieldIndex.ForwardedFromTerminalId); }
		}
		/// <summary>Creates a new OrderRoutestephandlerHistoryEntity.Number field instance</summary>
		public static EntityField2 Number 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderRoutestephandlerHistoryFieldIndex.Number); }
		}
		/// <summary>Creates a new OrderRoutestephandlerHistoryEntity.ContinueOnFailure field instance</summary>
		public static EntityField2 ContinueOnFailure 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderRoutestephandlerHistoryFieldIndex.ContinueOnFailure); }
		}
		/// <summary>Creates a new OrderRoutestephandlerHistoryEntity.CompleteRouteOnComplete field instance</summary>
		public static EntityField2 CompleteRouteOnComplete 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderRoutestephandlerHistoryFieldIndex.CompleteRouteOnComplete); }
		}
		/// <summary>Creates a new OrderRoutestephandlerHistoryEntity.HandlerType field instance</summary>
		public static EntityField2 HandlerType 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderRoutestephandlerHistoryFieldIndex.HandlerType); }
		}
		/// <summary>Creates a new OrderRoutestephandlerHistoryEntity.HandlerTypeText field instance</summary>
		public static EntityField2 HandlerTypeText 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderRoutestephandlerHistoryFieldIndex.HandlerTypeText); }
		}
		/// <summary>Creates a new OrderRoutestephandlerHistoryEntity.PrintReportType field instance</summary>
		public static EntityField2 PrintReportType 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderRoutestephandlerHistoryFieldIndex.PrintReportType); }
		}
		/// <summary>Creates a new OrderRoutestephandlerHistoryEntity.EscalationRouteId field instance</summary>
		public static EntityField2 EscalationRouteId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderRoutestephandlerHistoryFieldIndex.EscalationRouteId); }
		}
		/// <summary>Creates a new OrderRoutestephandlerHistoryEntity.Status field instance</summary>
		public static EntityField2 Status 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderRoutestephandlerHistoryFieldIndex.Status); }
		}
		/// <summary>Creates a new OrderRoutestephandlerHistoryEntity.StatusText field instance</summary>
		public static EntityField2 StatusText 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderRoutestephandlerHistoryFieldIndex.StatusText); }
		}
		/// <summary>Creates a new OrderRoutestephandlerHistoryEntity.Timeout field instance</summary>
		public static EntityField2 Timeout 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderRoutestephandlerHistoryFieldIndex.Timeout); }
		}
		/// <summary>Creates a new OrderRoutestephandlerHistoryEntity.RetrievalSupportNotificationTimeoutMinutes field instance</summary>
		public static EntityField2 RetrievalSupportNotificationTimeoutMinutes 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderRoutestephandlerHistoryFieldIndex.RetrievalSupportNotificationTimeoutMinutes); }
		}
		/// <summary>Creates a new OrderRoutestephandlerHistoryEntity.RetrievalSupportNotificationSent field instance</summary>
		public static EntityField2 RetrievalSupportNotificationSent 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderRoutestephandlerHistoryFieldIndex.RetrievalSupportNotificationSent); }
		}
		/// <summary>Creates a new OrderRoutestephandlerHistoryEntity.BeingHandledSupportNotificationTimeoutMinutes field instance</summary>
		public static EntityField2 BeingHandledSupportNotificationTimeoutMinutes 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderRoutestephandlerHistoryFieldIndex.BeingHandledSupportNotificationTimeoutMinutes); }
		}
		/// <summary>Creates a new OrderRoutestephandlerHistoryEntity.BeingHandledSupportNotificationSent field instance</summary>
		public static EntityField2 BeingHandledSupportNotificationSent 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderRoutestephandlerHistoryFieldIndex.BeingHandledSupportNotificationSent); }
		}
		/// <summary>Creates a new OrderRoutestephandlerHistoryEntity.ErrorCode field instance</summary>
		public static EntityField2 ErrorCode 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderRoutestephandlerHistoryFieldIndex.ErrorCode); }
		}
		/// <summary>Creates a new OrderRoutestephandlerHistoryEntity.ErrorText field instance</summary>
		public static EntityField2 ErrorText 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderRoutestephandlerHistoryFieldIndex.ErrorText); }
		}
		/// <summary>Creates a new OrderRoutestephandlerHistoryEntity.EscalationStep field instance</summary>
		public static EntityField2 EscalationStep 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderRoutestephandlerHistoryFieldIndex.EscalationStep); }
		}
		/// <summary>Creates a new OrderRoutestephandlerHistoryEntity.FieldValue1 field instance</summary>
		public static EntityField2 FieldValue1 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderRoutestephandlerHistoryFieldIndex.FieldValue1); }
		}
		/// <summary>Creates a new OrderRoutestephandlerHistoryEntity.FieldValue2 field instance</summary>
		public static EntityField2 FieldValue2 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderRoutestephandlerHistoryFieldIndex.FieldValue2); }
		}
		/// <summary>Creates a new OrderRoutestephandlerHistoryEntity.FieldValue3 field instance</summary>
		public static EntityField2 FieldValue3 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderRoutestephandlerHistoryFieldIndex.FieldValue3); }
		}
		/// <summary>Creates a new OrderRoutestephandlerHistoryEntity.FieldValue4 field instance</summary>
		public static EntityField2 FieldValue4 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderRoutestephandlerHistoryFieldIndex.FieldValue4); }
		}
		/// <summary>Creates a new OrderRoutestephandlerHistoryEntity.FieldValue5 field instance</summary>
		public static EntityField2 FieldValue5 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderRoutestephandlerHistoryFieldIndex.FieldValue5); }
		}
		/// <summary>Creates a new OrderRoutestephandlerHistoryEntity.FieldValue6 field instance</summary>
		public static EntityField2 FieldValue6 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderRoutestephandlerHistoryFieldIndex.FieldValue6); }
		}
		/// <summary>Creates a new OrderRoutestephandlerHistoryEntity.FieldValue7 field instance</summary>
		public static EntityField2 FieldValue7 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderRoutestephandlerHistoryFieldIndex.FieldValue7); }
		}
		/// <summary>Creates a new OrderRoutestephandlerHistoryEntity.FieldValue8 field instance</summary>
		public static EntityField2 FieldValue8 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderRoutestephandlerHistoryFieldIndex.FieldValue8); }
		}
		/// <summary>Creates a new OrderRoutestephandlerHistoryEntity.FieldValue9 field instance</summary>
		public static EntityField2 FieldValue9 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderRoutestephandlerHistoryFieldIndex.FieldValue9); }
		}
		/// <summary>Creates a new OrderRoutestephandlerHistoryEntity.FieldValue10 field instance</summary>
		public static EntityField2 FieldValue10 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderRoutestephandlerHistoryFieldIndex.FieldValue10); }
		}
		/// <summary>Creates a new OrderRoutestephandlerHistoryEntity.LogAlways field instance</summary>
		public static EntityField2 LogAlways 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderRoutestephandlerHistoryFieldIndex.LogAlways); }
		}
		/// <summary>Creates a new OrderRoutestephandlerHistoryEntity.SupportpoolId field instance</summary>
		public static EntityField2 SupportpoolId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderRoutestephandlerHistoryFieldIndex.SupportpoolId); }
		}
		/// <summary>Creates a new OrderRoutestephandlerHistoryEntity.OriginatedFromRoutestepHandlerId field instance</summary>
		public static EntityField2 OriginatedFromRoutestepHandlerId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderRoutestephandlerHistoryFieldIndex.OriginatedFromRoutestepHandlerId); }
		}
		/// <summary>Creates a new OrderRoutestephandlerHistoryEntity.CreatedBy field instance</summary>
		public static EntityField2 CreatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderRoutestephandlerHistoryFieldIndex.CreatedBy); }
		}
		/// <summary>Creates a new OrderRoutestephandlerHistoryEntity.UpdatedBy field instance</summary>
		public static EntityField2 UpdatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderRoutestephandlerHistoryFieldIndex.UpdatedBy); }
		}
		/// <summary>Creates a new OrderRoutestephandlerHistoryEntity.CreatedUTC field instance</summary>
		public static EntityField2 CreatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderRoutestephandlerHistoryFieldIndex.CreatedUTC); }
		}
		/// <summary>Creates a new OrderRoutestephandlerHistoryEntity.UpdatedUTC field instance</summary>
		public static EntityField2 UpdatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderRoutestephandlerHistoryFieldIndex.UpdatedUTC); }
		}
		/// <summary>Creates a new OrderRoutestephandlerHistoryEntity.TimeoutExpiresUTC field instance</summary>
		public static EntityField2 TimeoutExpiresUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderRoutestephandlerHistoryFieldIndex.TimeoutExpiresUTC); }
		}
		/// <summary>Creates a new OrderRoutestephandlerHistoryEntity.RetrievalSupportNotificationTimeoutUTC field instance</summary>
		public static EntityField2 RetrievalSupportNotificationTimeoutUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderRoutestephandlerHistoryFieldIndex.RetrievalSupportNotificationTimeoutUTC); }
		}
		/// <summary>Creates a new OrderRoutestephandlerHistoryEntity.BeingHandledSupportNotificationTimeoutUTC field instance</summary>
		public static EntityField2 BeingHandledSupportNotificationTimeoutUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderRoutestephandlerHistoryFieldIndex.BeingHandledSupportNotificationTimeoutUTC); }
		}
		/// <summary>Creates a new OrderRoutestephandlerHistoryEntity.WaitingToBeRetrievedUTC field instance</summary>
		public static EntityField2 WaitingToBeRetrievedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderRoutestephandlerHistoryFieldIndex.WaitingToBeRetrievedUTC); }
		}
		/// <summary>Creates a new OrderRoutestephandlerHistoryEntity.RetrievedByHandlerUTC field instance</summary>
		public static EntityField2 RetrievedByHandlerUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderRoutestephandlerHistoryFieldIndex.RetrievedByHandlerUTC); }
		}
		/// <summary>Creates a new OrderRoutestephandlerHistoryEntity.BeingHandledUTC field instance</summary>
		public static EntityField2 BeingHandledUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderRoutestephandlerHistoryFieldIndex.BeingHandledUTC); }
		}
		/// <summary>Creates a new OrderRoutestephandlerHistoryEntity.CompletedUTC field instance</summary>
		public static EntityField2 CompletedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderRoutestephandlerHistoryFieldIndex.CompletedUTC); }
		}
		/// <summary>Creates a new OrderRoutestephandlerHistoryEntity.ExternalSystemId field instance</summary>
		public static EntityField2 ExternalSystemId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OrderRoutestephandlerHistoryFieldIndex.ExternalSystemId); }
		}
	}

	/// <summary>Field Creation Class for entity OutletEntity</summary>
	public partial class OutletFields
	{
		/// <summary>Creates a new OutletEntity.OutletId field instance</summary>
		public static EntityField2 OutletId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OutletFieldIndex.OutletId); }
		}
		/// <summary>Creates a new OutletEntity.CompanyId field instance</summary>
		public static EntityField2 CompanyId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OutletFieldIndex.CompanyId); }
		}
		/// <summary>Creates a new OutletEntity.Name field instance</summary>
		public static EntityField2 Name 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OutletFieldIndex.Name); }
		}
		/// <summary>Creates a new OutletEntity.TippingActive field instance</summary>
		public static EntityField2 TippingActive 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OutletFieldIndex.TippingActive); }
		}
		/// <summary>Creates a new OutletEntity.TippingDefaultPercentage field instance</summary>
		public static EntityField2 TippingDefaultPercentage 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OutletFieldIndex.TippingDefaultPercentage); }
		}
		/// <summary>Creates a new OutletEntity.TippingMinimumPercentage field instance</summary>
		public static EntityField2 TippingMinimumPercentage 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OutletFieldIndex.TippingMinimumPercentage); }
		}
		/// <summary>Creates a new OutletEntity.TippingStepSize field instance</summary>
		public static EntityField2 TippingStepSize 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OutletFieldIndex.TippingStepSize); }
		}
		/// <summary>Creates a new OutletEntity.TippingProductId field instance</summary>
		public static EntityField2 TippingProductId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OutletFieldIndex.TippingProductId); }
		}
		/// <summary>Creates a new OutletEntity.CustomerDetailsSectionActive field instance</summary>
		public static EntityField2 CustomerDetailsSectionActive 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OutletFieldIndex.CustomerDetailsSectionActive); }
		}
		/// <summary>Creates a new OutletEntity.CustomerDetailsPhoneRequired field instance</summary>
		public static EntityField2 CustomerDetailsPhoneRequired 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OutletFieldIndex.CustomerDetailsPhoneRequired); }
		}
		/// <summary>Creates a new OutletEntity.CustomerDetailsEmailRequired field instance</summary>
		public static EntityField2 CustomerDetailsEmailRequired 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OutletFieldIndex.CustomerDetailsEmailRequired); }
		}
		/// <summary>Creates a new OutletEntity.ShowPricesIncludingTax field instance</summary>
		public static EntityField2 ShowPricesIncludingTax 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OutletFieldIndex.ShowPricesIncludingTax); }
		}
		/// <summary>Creates a new OutletEntity.ShowTaxBreakDownOnBasket field instance</summary>
		public static EntityField2 ShowTaxBreakDownOnBasket 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OutletFieldIndex.ShowTaxBreakDownOnBasket); }
		}
		/// <summary>Creates a new OutletEntity.ShowTaxBreakDownOnCheckout field instance</summary>
		public static EntityField2 ShowTaxBreakDownOnCheckout 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OutletFieldIndex.ShowTaxBreakDownOnCheckout); }
		}
		/// <summary>Creates a new OutletEntity.ReceiptEmail field instance</summary>
		public static EntityField2 ReceiptEmail 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OutletFieldIndex.ReceiptEmail); }
		}
		/// <summary>Creates a new OutletEntity.OutletOperationalStateId field instance</summary>
		public static EntityField2 OutletOperationalStateId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OutletFieldIndex.OutletOperationalStateId); }
		}
		/// <summary>Creates a new OutletEntity.CreatedBy field instance</summary>
		public static EntityField2 CreatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OutletFieldIndex.CreatedBy); }
		}
		/// <summary>Creates a new OutletEntity.UpdatedBy field instance</summary>
		public static EntityField2 UpdatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OutletFieldIndex.UpdatedBy); }
		}
		/// <summary>Creates a new OutletEntity.CreatedUTC field instance</summary>
		public static EntityField2 CreatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OutletFieldIndex.CreatedUTC); }
		}
		/// <summary>Creates a new OutletEntity.UpdatedUTC field instance</summary>
		public static EntityField2 UpdatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OutletFieldIndex.UpdatedUTC); }
		}
		/// <summary>Creates a new OutletEntity.DeliveryChargeProductId field instance</summary>
		public static EntityField2 DeliveryChargeProductId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OutletFieldIndex.DeliveryChargeProductId); }
		}
		/// <summary>Creates a new OutletEntity.Latitude field instance</summary>
		public static EntityField2 Latitude 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OutletFieldIndex.Latitude); }
		}
		/// <summary>Creates a new OutletEntity.Longitude field instance</summary>
		public static EntityField2 Longitude 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OutletFieldIndex.Longitude); }
		}
		/// <summary>Creates a new OutletEntity.ServiceChargeProductId field instance</summary>
		public static EntityField2 ServiceChargeProductId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OutletFieldIndex.ServiceChargeProductId); }
		}
		/// <summary>Creates a new OutletEntity.CustomerDetailsNameRequired field instance</summary>
		public static EntityField2 CustomerDetailsNameRequired 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OutletFieldIndex.CustomerDetailsNameRequired); }
		}
		/// <summary>Creates a new OutletEntity.OutletSellerInformationId field instance</summary>
		public static EntityField2 OutletSellerInformationId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OutletFieldIndex.OutletSellerInformationId); }
		}
		/// <summary>Creates a new OutletEntity.ShowTaxBreakdown field instance</summary>
		public static EntityField2 ShowTaxBreakdown 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OutletFieldIndex.ShowTaxBreakdown); }
		}
		/// <summary>Creates a new OutletEntity.OptInType field instance</summary>
		public static EntityField2 OptInType 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OutletFieldIndex.OptInType); }
		}
		/// <summary>Creates a new OutletEntity.TaxDisclaimer field instance</summary>
		public static EntityField2 TaxDisclaimer 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OutletFieldIndex.TaxDisclaimer); }
		}
		/// <summary>Creates a new OutletEntity.TaxNumberTitle field instance</summary>
		public static EntityField2 TaxNumberTitle 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OutletFieldIndex.TaxNumberTitle); }
		}
	}

	/// <summary>Field Creation Class for entity OutletOperationalStateEntity</summary>
	public partial class OutletOperationalStateFields
	{
		/// <summary>Creates a new OutletOperationalStateEntity.OutletOperationalStateId field instance</summary>
		public static EntityField2 OutletOperationalStateId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OutletOperationalStateFieldIndex.OutletOperationalStateId); }
		}
		/// <summary>Creates a new OutletOperationalStateEntity.OutletId field instance</summary>
		public static EntityField2 OutletId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OutletOperationalStateFieldIndex.OutletId); }
		}
		/// <summary>Creates a new OutletOperationalStateEntity.ParentCompanyId field instance</summary>
		public static EntityField2 ParentCompanyId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OutletOperationalStateFieldIndex.ParentCompanyId); }
		}
		/// <summary>Creates a new OutletOperationalStateEntity.WaitTime field instance</summary>
		public static EntityField2 WaitTime 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OutletOperationalStateFieldIndex.WaitTime); }
		}
		/// <summary>Creates a new OutletOperationalStateEntity.OrderIntakeDisabled field instance</summary>
		public static EntityField2 OrderIntakeDisabled 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OutletOperationalStateFieldIndex.OrderIntakeDisabled); }
		}
		/// <summary>Creates a new OutletOperationalStateEntity.CreatedUTC field instance</summary>
		public static EntityField2 CreatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OutletOperationalStateFieldIndex.CreatedUTC); }
		}
		/// <summary>Creates a new OutletOperationalStateEntity.CreatedBy field instance</summary>
		public static EntityField2 CreatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OutletOperationalStateFieldIndex.CreatedBy); }
		}
		/// <summary>Creates a new OutletOperationalStateEntity.UpdatedUTC field instance</summary>
		public static EntityField2 UpdatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OutletOperationalStateFieldIndex.UpdatedUTC); }
		}
		/// <summary>Creates a new OutletOperationalStateEntity.UpdatedBy field instance</summary>
		public static EntityField2 UpdatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OutletOperationalStateFieldIndex.UpdatedBy); }
		}
	}

	/// <summary>Field Creation Class for entity OutletSellerInformationEntity</summary>
	public partial class OutletSellerInformationFields
	{
		/// <summary>Creates a new OutletSellerInformationEntity.OutletSellerInformationId field instance</summary>
		public static EntityField2 OutletSellerInformationId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OutletSellerInformationFieldIndex.OutletSellerInformationId); }
		}
		/// <summary>Creates a new OutletSellerInformationEntity.OutletId field instance</summary>
		public static EntityField2 OutletId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OutletSellerInformationFieldIndex.OutletId); }
		}
		/// <summary>Creates a new OutletSellerInformationEntity.ParentCompanyId field instance</summary>
		public static EntityField2 ParentCompanyId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OutletSellerInformationFieldIndex.ParentCompanyId); }
		}
		/// <summary>Creates a new OutletSellerInformationEntity.SellerName field instance</summary>
		public static EntityField2 SellerName 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OutletSellerInformationFieldIndex.SellerName); }
		}
		/// <summary>Creates a new OutletSellerInformationEntity.Address field instance</summary>
		public static EntityField2 Address 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OutletSellerInformationFieldIndex.Address); }
		}
		/// <summary>Creates a new OutletSellerInformationEntity.Phonenumber field instance</summary>
		public static EntityField2 Phonenumber 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OutletSellerInformationFieldIndex.Phonenumber); }
		}
		/// <summary>Creates a new OutletSellerInformationEntity.Email field instance</summary>
		public static EntityField2 Email 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OutletSellerInformationFieldIndex.Email); }
		}
		/// <summary>Creates a new OutletSellerInformationEntity.VatNumber field instance</summary>
		public static EntityField2 VatNumber 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OutletSellerInformationFieldIndex.VatNumber); }
		}
		/// <summary>Creates a new OutletSellerInformationEntity.ReceiptReceiversEmail field instance</summary>
		public static EntityField2 ReceiptReceiversEmail 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OutletSellerInformationFieldIndex.ReceiptReceiversEmail); }
		}
		/// <summary>Creates a new OutletSellerInformationEntity.ReceiptReceiversSms field instance</summary>
		public static EntityField2 ReceiptReceiversSms 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OutletSellerInformationFieldIndex.ReceiptReceiversSms); }
		}
		/// <summary>Creates a new OutletSellerInformationEntity.SmsOriginator field instance</summary>
		public static EntityField2 SmsOriginator 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OutletSellerInformationFieldIndex.SmsOriginator); }
		}
		/// <summary>Creates a new OutletSellerInformationEntity.TaxBreakdown field instance</summary>
		public static EntityField2 TaxBreakdown 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OutletSellerInformationFieldIndex.TaxBreakdown); }
		}
		/// <summary>Creates a new OutletSellerInformationEntity.CreatedUTC field instance</summary>
		public static EntityField2 CreatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OutletSellerInformationFieldIndex.CreatedUTC); }
		}
		/// <summary>Creates a new OutletSellerInformationEntity.CreatedBy field instance</summary>
		public static EntityField2 CreatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OutletSellerInformationFieldIndex.CreatedBy); }
		}
		/// <summary>Creates a new OutletSellerInformationEntity.UpdatedUTC field instance</summary>
		public static EntityField2 UpdatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OutletSellerInformationFieldIndex.UpdatedUTC); }
		}
		/// <summary>Creates a new OutletSellerInformationEntity.UpdatedBy field instance</summary>
		public static EntityField2 UpdatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OutletSellerInformationFieldIndex.UpdatedBy); }
		}
		/// <summary>Creates a new OutletSellerInformationEntity.ReceiveOrderReceiptNotifications field instance</summary>
		public static EntityField2 ReceiveOrderReceiptNotifications 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OutletSellerInformationFieldIndex.ReceiveOrderReceiptNotifications); }
		}
		/// <summary>Creates a new OutletSellerInformationEntity.ReceiveOrderConfirmationNotifications field instance</summary>
		public static EntityField2 ReceiveOrderConfirmationNotifications 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(OutletSellerInformationFieldIndex.ReceiveOrderConfirmationNotifications); }
		}
	}

	/// <summary>Field Creation Class for entity PaymentIntegrationConfigurationEntity</summary>
	public partial class PaymentIntegrationConfigurationFields
	{
		/// <summary>Creates a new PaymentIntegrationConfigurationEntity.PaymentIntegrationConfigurationId field instance</summary>
		public static EntityField2 PaymentIntegrationConfigurationId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PaymentIntegrationConfigurationFieldIndex.PaymentIntegrationConfigurationId); }
		}
		/// <summary>Creates a new PaymentIntegrationConfigurationEntity.PaymentProviderId field instance</summary>
		public static EntityField2 PaymentProviderId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PaymentIntegrationConfigurationFieldIndex.PaymentProviderId); }
		}
		/// <summary>Creates a new PaymentIntegrationConfigurationEntity.CompanyId field instance</summary>
		public static EntityField2 CompanyId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PaymentIntegrationConfigurationFieldIndex.CompanyId); }
		}
		/// <summary>Creates a new PaymentIntegrationConfigurationEntity.DisplayName field instance</summary>
		public static EntityField2 DisplayName 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PaymentIntegrationConfigurationFieldIndex.DisplayName); }
		}
		/// <summary>Creates a new PaymentIntegrationConfigurationEntity.MerchantName field instance</summary>
		public static EntityField2 MerchantName 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PaymentIntegrationConfigurationFieldIndex.MerchantName); }
		}
		/// <summary>Creates a new PaymentIntegrationConfigurationEntity.AdyenMerchantCode field instance</summary>
		public static EntityField2 AdyenMerchantCode 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PaymentIntegrationConfigurationFieldIndex.AdyenMerchantCode); }
		}
		/// <summary>Creates a new PaymentIntegrationConfigurationEntity.GooglePayMerchantIdentifier field instance</summary>
		public static EntityField2 GooglePayMerchantIdentifier 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PaymentIntegrationConfigurationFieldIndex.GooglePayMerchantIdentifier); }
		}
		/// <summary>Creates a new PaymentIntegrationConfigurationEntity.ApplePayMerchantIdentifier field instance</summary>
		public static EntityField2 ApplePayMerchantIdentifier 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PaymentIntegrationConfigurationFieldIndex.ApplePayMerchantIdentifier); }
		}
		/// <summary>Creates a new PaymentIntegrationConfigurationEntity.CreatedUTC field instance</summary>
		public static EntityField2 CreatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PaymentIntegrationConfigurationFieldIndex.CreatedUTC); }
		}
		/// <summary>Creates a new PaymentIntegrationConfigurationEntity.UpdatedUTC field instance</summary>
		public static EntityField2 UpdatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PaymentIntegrationConfigurationFieldIndex.UpdatedUTC); }
		}
		/// <summary>Creates a new PaymentIntegrationConfigurationEntity.PaymentProcessorInfo field instance</summary>
		public static EntityField2 PaymentProcessorInfo 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PaymentIntegrationConfigurationFieldIndex.PaymentProcessorInfo); }
		}
		/// <summary>Creates a new PaymentIntegrationConfigurationEntity.PaymentProcessorInfoFooter field instance</summary>
		public static EntityField2 PaymentProcessorInfoFooter 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PaymentIntegrationConfigurationFieldIndex.PaymentProcessorInfoFooter); }
		}
		/// <summary>Creates a new PaymentIntegrationConfigurationEntity.AdyenAccountHolderCode field instance</summary>
		public static EntityField2 AdyenAccountHolderCode 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PaymentIntegrationConfigurationFieldIndex.AdyenAccountHolderCode); }
		}
		/// <summary>Creates a new PaymentIntegrationConfigurationEntity.AdyenAccountHolderName field instance</summary>
		public static EntityField2 AdyenAccountHolderName 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PaymentIntegrationConfigurationFieldIndex.AdyenAccountHolderName); }
		}
		/// <summary>Creates a new PaymentIntegrationConfigurationEntity.AdyenAccountCode field instance</summary>
		public static EntityField2 AdyenAccountCode 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PaymentIntegrationConfigurationFieldIndex.AdyenAccountCode); }
		}
		/// <summary>Creates a new PaymentIntegrationConfigurationEntity.AdyenAccountName field instance</summary>
		public static EntityField2 AdyenAccountName 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PaymentIntegrationConfigurationFieldIndex.AdyenAccountName); }
		}
	}

	/// <summary>Field Creation Class for entity PaymentProviderEntity</summary>
	public partial class PaymentProviderFields
	{
		/// <summary>Creates a new PaymentProviderEntity.PaymentProviderId field instance</summary>
		public static EntityField2 PaymentProviderId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PaymentProviderFieldIndex.PaymentProviderId); }
		}
		/// <summary>Creates a new PaymentProviderEntity.CompanyId field instance</summary>
		public static EntityField2 CompanyId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PaymentProviderFieldIndex.CompanyId); }
		}
		/// <summary>Creates a new PaymentProviderEntity.Type field instance</summary>
		public static EntityField2 Type 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PaymentProviderFieldIndex.Type); }
		}
		/// <summary>Creates a new PaymentProviderEntity.Name field instance</summary>
		public static EntityField2 Name 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PaymentProviderFieldIndex.Name); }
		}
		/// <summary>Creates a new PaymentProviderEntity.Environment field instance</summary>
		public static EntityField2 Environment 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PaymentProviderFieldIndex.Environment); }
		}
		/// <summary>Creates a new PaymentProviderEntity.ApiKey field instance</summary>
		public static EntityField2 ApiKey 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PaymentProviderFieldIndex.ApiKey); }
		}
		/// <summary>Creates a new PaymentProviderEntity.LiveEndpointUrlPrefix field instance</summary>
		public static EntityField2 LiveEndpointUrlPrefix 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PaymentProviderFieldIndex.LiveEndpointUrlPrefix); }
		}
		/// <summary>Creates a new PaymentProviderEntity.OriginDomain field instance</summary>
		public static EntityField2 OriginDomain 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PaymentProviderFieldIndex.OriginDomain); }
		}
		/// <summary>Creates a new PaymentProviderEntity.CreatedUTC field instance</summary>
		public static EntityField2 CreatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PaymentProviderFieldIndex.CreatedUTC); }
		}
		/// <summary>Creates a new PaymentProviderEntity.UpdatedUTC field instance</summary>
		public static EntityField2 UpdatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PaymentProviderFieldIndex.UpdatedUTC); }
		}
		/// <summary>Creates a new PaymentProviderEntity.PaymentProcessorInfo field instance</summary>
		public static EntityField2 PaymentProcessorInfo 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PaymentProviderFieldIndex.PaymentProcessorInfo); }
		}
		/// <summary>Creates a new PaymentProviderEntity.PaymentProcessorInfoFooter field instance</summary>
		public static EntityField2 PaymentProcessorInfoFooter 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PaymentProviderFieldIndex.PaymentProcessorInfoFooter); }
		}
		/// <summary>Creates a new PaymentProviderEntity.AdyenUsername field instance</summary>
		public static EntityField2 AdyenUsername 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PaymentProviderFieldIndex.AdyenUsername); }
		}
		/// <summary>Creates a new PaymentProviderEntity.AdyenPassword field instance</summary>
		public static EntityField2 AdyenPassword 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PaymentProviderFieldIndex.AdyenPassword); }
		}
		/// <summary>Creates a new PaymentProviderEntity.OmisePublicKey field instance</summary>
		public static EntityField2 OmisePublicKey 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PaymentProviderFieldIndex.OmisePublicKey); }
		}
		/// <summary>Creates a new PaymentProviderEntity.OmiseSecretKey field instance</summary>
		public static EntityField2 OmiseSecretKey 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PaymentProviderFieldIndex.OmiseSecretKey); }
		}
		/// <summary>Creates a new PaymentProviderEntity.SkinCode field instance</summary>
		public static EntityField2 SkinCode 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PaymentProviderFieldIndex.SkinCode); }
		}
		/// <summary>Creates a new PaymentProviderEntity.HmacKey field instance</summary>
		public static EntityField2 HmacKey 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PaymentProviderFieldIndex.HmacKey); }
		}
		/// <summary>Creates a new PaymentProviderEntity.ClientKey field instance</summary>
		public static EntityField2 ClientKey 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PaymentProviderFieldIndex.ClientKey); }
		}
		/// <summary>Creates a new PaymentProviderEntity.AdyenMerchantCode field instance</summary>
		public static EntityField2 AdyenMerchantCode 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PaymentProviderFieldIndex.AdyenMerchantCode); }
		}
		/// <summary>Creates a new PaymentProviderEntity.GooglePayMerchantIdentifier field instance</summary>
		public static EntityField2 GooglePayMerchantIdentifier 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PaymentProviderFieldIndex.GooglePayMerchantIdentifier); }
		}
		/// <summary>Creates a new PaymentProviderEntity.ApplePayMerchantIdentifier field instance</summary>
		public static EntityField2 ApplePayMerchantIdentifier 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PaymentProviderFieldIndex.ApplePayMerchantIdentifier); }
		}
	}

	/// <summary>Field Creation Class for entity PaymentTransactionEntity</summary>
	public partial class PaymentTransactionFields
	{
		/// <summary>Creates a new PaymentTransactionEntity.PaymentTransactionId field instance</summary>
		public static EntityField2 PaymentTransactionId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PaymentTransactionFieldIndex.PaymentTransactionId); }
		}
		/// <summary>Creates a new PaymentTransactionEntity.OrderId field instance</summary>
		public static EntityField2 OrderId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PaymentTransactionFieldIndex.OrderId); }
		}
		/// <summary>Creates a new PaymentTransactionEntity.ReferenceId field instance</summary>
		public static EntityField2 ReferenceId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PaymentTransactionFieldIndex.ReferenceId); }
		}
		/// <summary>Creates a new PaymentTransactionEntity.MerchantReference field instance</summary>
		public static EntityField2 MerchantReference 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PaymentTransactionFieldIndex.MerchantReference); }
		}
		/// <summary>Creates a new PaymentTransactionEntity.MerchantAccount field instance</summary>
		public static EntityField2 MerchantAccount 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PaymentTransactionFieldIndex.MerchantAccount); }
		}
		/// <summary>Creates a new PaymentTransactionEntity.PaymentMethod field instance</summary>
		public static EntityField2 PaymentMethod 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PaymentTransactionFieldIndex.PaymentMethod); }
		}
		/// <summary>Creates a new PaymentTransactionEntity.Status field instance</summary>
		public static EntityField2 Status 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PaymentTransactionFieldIndex.Status); }
		}
		/// <summary>Creates a new PaymentTransactionEntity.CardSummary field instance</summary>
		public static EntityField2 CardSummary 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PaymentTransactionFieldIndex.CardSummary); }
		}
		/// <summary>Creates a new PaymentTransactionEntity.AuthCode field instance</summary>
		public static EntityField2 AuthCode 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PaymentTransactionFieldIndex.AuthCode); }
		}
		/// <summary>Creates a new PaymentTransactionEntity.PaymentData field instance</summary>
		public static EntityField2 PaymentData 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PaymentTransactionFieldIndex.PaymentData); }
		}
		/// <summary>Creates a new PaymentTransactionEntity.CreatedUTC field instance</summary>
		public static EntityField2 CreatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PaymentTransactionFieldIndex.CreatedUTC); }
		}
		/// <summary>Creates a new PaymentTransactionEntity.UpdatedUTC field instance</summary>
		public static EntityField2 UpdatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PaymentTransactionFieldIndex.UpdatedUTC); }
		}
		/// <summary>Creates a new PaymentTransactionEntity.PaymentEnvironment field instance</summary>
		public static EntityField2 PaymentEnvironment 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PaymentTransactionFieldIndex.PaymentEnvironment); }
		}
		/// <summary>Creates a new PaymentTransactionEntity.PaymentProviderType field instance</summary>
		public static EntityField2 PaymentProviderType 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PaymentTransactionFieldIndex.PaymentProviderType); }
		}
		/// <summary>Creates a new PaymentTransactionEntity.PaymentProviderName field instance</summary>
		public static EntityField2 PaymentProviderName 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PaymentTransactionFieldIndex.PaymentProviderName); }
		}
		/// <summary>Creates a new PaymentTransactionEntity.PaymentProviderId field instance</summary>
		public static EntityField2 PaymentProviderId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PaymentTransactionFieldIndex.PaymentProviderId); }
		}
		/// <summary>Creates a new PaymentTransactionEntity.PaymentIntegrationConfigurationId field instance</summary>
		public static EntityField2 PaymentIntegrationConfigurationId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PaymentTransactionFieldIndex.PaymentIntegrationConfigurationId); }
		}
	}

	/// <summary>Field Creation Class for entity PriceLevelEntity</summary>
	public partial class PriceLevelFields
	{
		/// <summary>Creates a new PriceLevelEntity.PriceLevelId field instance</summary>
		public static EntityField2 PriceLevelId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PriceLevelFieldIndex.PriceLevelId); }
		}
		/// <summary>Creates a new PriceLevelEntity.CompanyId field instance</summary>
		public static EntityField2 CompanyId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PriceLevelFieldIndex.CompanyId); }
		}
		/// <summary>Creates a new PriceLevelEntity.Name field instance</summary>
		public static EntityField2 Name 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PriceLevelFieldIndex.Name); }
		}
		/// <summary>Creates a new PriceLevelEntity.CreatedUTC field instance</summary>
		public static EntityField2 CreatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PriceLevelFieldIndex.CreatedUTC); }
		}
		/// <summary>Creates a new PriceLevelEntity.CreatedBy field instance</summary>
		public static EntityField2 CreatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PriceLevelFieldIndex.CreatedBy); }
		}
		/// <summary>Creates a new PriceLevelEntity.UpdatedUTC field instance</summary>
		public static EntityField2 UpdatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PriceLevelFieldIndex.UpdatedUTC); }
		}
		/// <summary>Creates a new PriceLevelEntity.UpdatedBy field instance</summary>
		public static EntityField2 UpdatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PriceLevelFieldIndex.UpdatedBy); }
		}
	}

	/// <summary>Field Creation Class for entity PriceLevelItemEntity</summary>
	public partial class PriceLevelItemFields
	{
		/// <summary>Creates a new PriceLevelItemEntity.PriceLevelItemId field instance</summary>
		public static EntityField2 PriceLevelItemId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PriceLevelItemFieldIndex.PriceLevelItemId); }
		}
		/// <summary>Creates a new PriceLevelItemEntity.ParentCompanyId field instance</summary>
		public static EntityField2 ParentCompanyId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PriceLevelItemFieldIndex.ParentCompanyId); }
		}
		/// <summary>Creates a new PriceLevelItemEntity.PriceLevelId field instance</summary>
		public static EntityField2 PriceLevelId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PriceLevelItemFieldIndex.PriceLevelId); }
		}
		/// <summary>Creates a new PriceLevelItemEntity.Price field instance</summary>
		public static EntityField2 Price 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PriceLevelItemFieldIndex.Price); }
		}
		/// <summary>Creates a new PriceLevelItemEntity.ProductId field instance</summary>
		public static EntityField2 ProductId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PriceLevelItemFieldIndex.ProductId); }
		}
		/// <summary>Creates a new PriceLevelItemEntity.AlterationId field instance</summary>
		public static EntityField2 AlterationId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PriceLevelItemFieldIndex.AlterationId); }
		}
		/// <summary>Creates a new PriceLevelItemEntity.AlterationoptionId field instance</summary>
		public static EntityField2 AlterationoptionId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PriceLevelItemFieldIndex.AlterationoptionId); }
		}
		/// <summary>Creates a new PriceLevelItemEntity.CreatedUTC field instance</summary>
		public static EntityField2 CreatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PriceLevelItemFieldIndex.CreatedUTC); }
		}
		/// <summary>Creates a new PriceLevelItemEntity.CreatedBy field instance</summary>
		public static EntityField2 CreatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PriceLevelItemFieldIndex.CreatedBy); }
		}
		/// <summary>Creates a new PriceLevelItemEntity.UpdatedUTC field instance</summary>
		public static EntityField2 UpdatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PriceLevelItemFieldIndex.UpdatedUTC); }
		}
		/// <summary>Creates a new PriceLevelItemEntity.UpdatedBy field instance</summary>
		public static EntityField2 UpdatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PriceLevelItemFieldIndex.UpdatedBy); }
		}
	}

	/// <summary>Field Creation Class for entity PriceScheduleEntity</summary>
	public partial class PriceScheduleFields
	{
		/// <summary>Creates a new PriceScheduleEntity.PriceScheduleId field instance</summary>
		public static EntityField2 PriceScheduleId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PriceScheduleFieldIndex.PriceScheduleId); }
		}
		/// <summary>Creates a new PriceScheduleEntity.CompanyId field instance</summary>
		public static EntityField2 CompanyId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PriceScheduleFieldIndex.CompanyId); }
		}
		/// <summary>Creates a new PriceScheduleEntity.Name field instance</summary>
		public static EntityField2 Name 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PriceScheduleFieldIndex.Name); }
		}
		/// <summary>Creates a new PriceScheduleEntity.CreatedUTC field instance</summary>
		public static EntityField2 CreatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PriceScheduleFieldIndex.CreatedUTC); }
		}
		/// <summary>Creates a new PriceScheduleEntity.CreatedBy field instance</summary>
		public static EntityField2 CreatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PriceScheduleFieldIndex.CreatedBy); }
		}
		/// <summary>Creates a new PriceScheduleEntity.UpdatedUTC field instance</summary>
		public static EntityField2 UpdatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PriceScheduleFieldIndex.UpdatedUTC); }
		}
		/// <summary>Creates a new PriceScheduleEntity.UpdatedBy field instance</summary>
		public static EntityField2 UpdatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PriceScheduleFieldIndex.UpdatedBy); }
		}
	}

	/// <summary>Field Creation Class for entity PriceScheduleItemEntity</summary>
	public partial class PriceScheduleItemFields
	{
		/// <summary>Creates a new PriceScheduleItemEntity.PriceScheduleItemId field instance</summary>
		public static EntityField2 PriceScheduleItemId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PriceScheduleItemFieldIndex.PriceScheduleItemId); }
		}
		/// <summary>Creates a new PriceScheduleItemEntity.ParentCompanyId field instance</summary>
		public static EntityField2 ParentCompanyId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PriceScheduleItemFieldIndex.ParentCompanyId); }
		}
		/// <summary>Creates a new PriceScheduleItemEntity.PriceScheduleId field instance</summary>
		public static EntityField2 PriceScheduleId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PriceScheduleItemFieldIndex.PriceScheduleId); }
		}
		/// <summary>Creates a new PriceScheduleItemEntity.PriceLevelId field instance</summary>
		public static EntityField2 PriceLevelId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PriceScheduleItemFieldIndex.PriceLevelId); }
		}
		/// <summary>Creates a new PriceScheduleItemEntity.CreatedUTC field instance</summary>
		public static EntityField2 CreatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PriceScheduleItemFieldIndex.CreatedUTC); }
		}
		/// <summary>Creates a new PriceScheduleItemEntity.CreatedBy field instance</summary>
		public static EntityField2 CreatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PriceScheduleItemFieldIndex.CreatedBy); }
		}
		/// <summary>Creates a new PriceScheduleItemEntity.UpdatedUTC field instance</summary>
		public static EntityField2 UpdatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PriceScheduleItemFieldIndex.UpdatedUTC); }
		}
		/// <summary>Creates a new PriceScheduleItemEntity.UpdatedBy field instance</summary>
		public static EntityField2 UpdatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PriceScheduleItemFieldIndex.UpdatedBy); }
		}
	}

	/// <summary>Field Creation Class for entity PriceScheduleItemOccurrenceEntity</summary>
	public partial class PriceScheduleItemOccurrenceFields
	{
		/// <summary>Creates a new PriceScheduleItemOccurrenceEntity.PriceScheduleItemOccurrenceId field instance</summary>
		public static EntityField2 PriceScheduleItemOccurrenceId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PriceScheduleItemOccurrenceFieldIndex.PriceScheduleItemOccurrenceId); }
		}
		/// <summary>Creates a new PriceScheduleItemOccurrenceEntity.ParentCompanyId field instance</summary>
		public static EntityField2 ParentCompanyId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PriceScheduleItemOccurrenceFieldIndex.ParentCompanyId); }
		}
		/// <summary>Creates a new PriceScheduleItemOccurrenceEntity.PriceScheduleItemId field instance</summary>
		public static EntityField2 PriceScheduleItemId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PriceScheduleItemOccurrenceFieldIndex.PriceScheduleItemId); }
		}
		/// <summary>Creates a new PriceScheduleItemOccurrenceEntity.StartTime field instance</summary>
		public static EntityField2 StartTime 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PriceScheduleItemOccurrenceFieldIndex.StartTime); }
		}
		/// <summary>Creates a new PriceScheduleItemOccurrenceEntity.EndTime field instance</summary>
		public static EntityField2 EndTime 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PriceScheduleItemOccurrenceFieldIndex.EndTime); }
		}
		/// <summary>Creates a new PriceScheduleItemOccurrenceEntity.Recurring field instance</summary>
		public static EntityField2 Recurring 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PriceScheduleItemOccurrenceFieldIndex.Recurring); }
		}
		/// <summary>Creates a new PriceScheduleItemOccurrenceEntity.RecurrenceType field instance</summary>
		public static EntityField2 RecurrenceType 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PriceScheduleItemOccurrenceFieldIndex.RecurrenceType); }
		}
		/// <summary>Creates a new PriceScheduleItemOccurrenceEntity.RecurrenceRange field instance</summary>
		public static EntityField2 RecurrenceRange 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PriceScheduleItemOccurrenceFieldIndex.RecurrenceRange); }
		}
		/// <summary>Creates a new PriceScheduleItemOccurrenceEntity.RecurrenceStart field instance</summary>
		public static EntityField2 RecurrenceStart 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PriceScheduleItemOccurrenceFieldIndex.RecurrenceStart); }
		}
		/// <summary>Creates a new PriceScheduleItemOccurrenceEntity.RecurrenceEnd field instance</summary>
		public static EntityField2 RecurrenceEnd 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PriceScheduleItemOccurrenceFieldIndex.RecurrenceEnd); }
		}
		/// <summary>Creates a new PriceScheduleItemOccurrenceEntity.RecurrenceOccurrenceCount field instance</summary>
		public static EntityField2 RecurrenceOccurrenceCount 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PriceScheduleItemOccurrenceFieldIndex.RecurrenceOccurrenceCount); }
		}
		/// <summary>Creates a new PriceScheduleItemOccurrenceEntity.RecurrencePeriodicity field instance</summary>
		public static EntityField2 RecurrencePeriodicity 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PriceScheduleItemOccurrenceFieldIndex.RecurrencePeriodicity); }
		}
		/// <summary>Creates a new PriceScheduleItemOccurrenceEntity.RecurrenceDayNumber field instance</summary>
		public static EntityField2 RecurrenceDayNumber 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PriceScheduleItemOccurrenceFieldIndex.RecurrenceDayNumber); }
		}
		/// <summary>Creates a new PriceScheduleItemOccurrenceEntity.RecurrenceWeekDays field instance</summary>
		public static EntityField2 RecurrenceWeekDays 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PriceScheduleItemOccurrenceFieldIndex.RecurrenceWeekDays); }
		}
		/// <summary>Creates a new PriceScheduleItemOccurrenceEntity.RecurrenceWeekOfMonth field instance</summary>
		public static EntityField2 RecurrenceWeekOfMonth 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PriceScheduleItemOccurrenceFieldIndex.RecurrenceWeekOfMonth); }
		}
		/// <summary>Creates a new PriceScheduleItemOccurrenceEntity.RecurrenceMonth field instance</summary>
		public static EntityField2 RecurrenceMonth 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PriceScheduleItemOccurrenceFieldIndex.RecurrenceMonth); }
		}
		/// <summary>Creates a new PriceScheduleItemOccurrenceEntity.BackgroundColor field instance</summary>
		public static EntityField2 BackgroundColor 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PriceScheduleItemOccurrenceFieldIndex.BackgroundColor); }
		}
		/// <summary>Creates a new PriceScheduleItemOccurrenceEntity.TextColor field instance</summary>
		public static EntityField2 TextColor 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PriceScheduleItemOccurrenceFieldIndex.TextColor); }
		}
		/// <summary>Creates a new PriceScheduleItemOccurrenceEntity.StartTimeUTC field instance</summary>
		public static EntityField2 StartTimeUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PriceScheduleItemOccurrenceFieldIndex.StartTimeUTC); }
		}
		/// <summary>Creates a new PriceScheduleItemOccurrenceEntity.EndTimeUTC field instance</summary>
		public static EntityField2 EndTimeUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PriceScheduleItemOccurrenceFieldIndex.EndTimeUTC); }
		}
		/// <summary>Creates a new PriceScheduleItemOccurrenceEntity.RecurrenceStartUTC field instance</summary>
		public static EntityField2 RecurrenceStartUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PriceScheduleItemOccurrenceFieldIndex.RecurrenceStartUTC); }
		}
		/// <summary>Creates a new PriceScheduleItemOccurrenceEntity.RecurrenceEndUTC field instance</summary>
		public static EntityField2 RecurrenceEndUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PriceScheduleItemOccurrenceFieldIndex.RecurrenceEndUTC); }
		}
		/// <summary>Creates a new PriceScheduleItemOccurrenceEntity.CreatedUTC field instance</summary>
		public static EntityField2 CreatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PriceScheduleItemOccurrenceFieldIndex.CreatedUTC); }
		}
		/// <summary>Creates a new PriceScheduleItemOccurrenceEntity.CreatedBy field instance</summary>
		public static EntityField2 CreatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PriceScheduleItemOccurrenceFieldIndex.CreatedBy); }
		}
		/// <summary>Creates a new PriceScheduleItemOccurrenceEntity.UpdatedUTC field instance</summary>
		public static EntityField2 UpdatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PriceScheduleItemOccurrenceFieldIndex.UpdatedUTC); }
		}
		/// <summary>Creates a new PriceScheduleItemOccurrenceEntity.UpdatedBy field instance</summary>
		public static EntityField2 UpdatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(PriceScheduleItemOccurrenceFieldIndex.UpdatedBy); }
		}
	}

	/// <summary>Field Creation Class for entity ProductEntity</summary>
	public partial class ProductFields
	{
		/// <summary>Creates a new ProductEntity.ProductId field instance</summary>
		public static EntityField2 ProductId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ProductFieldIndex.ProductId); }
		}
		/// <summary>Creates a new ProductEntity.CompanyId field instance</summary>
		public static EntityField2 CompanyId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ProductFieldIndex.CompanyId); }
		}
		/// <summary>Creates a new ProductEntity.GenericproductId field instance</summary>
		public static EntityField2 GenericproductId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ProductFieldIndex.GenericproductId); }
		}
		/// <summary>Creates a new ProductEntity.BrandProductId field instance</summary>
		public static EntityField2 BrandProductId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ProductFieldIndex.BrandProductId); }
		}
		/// <summary>Creates a new ProductEntity.RouteId field instance</summary>
		public static EntityField2 RouteId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ProductFieldIndex.RouteId); }
		}
		/// <summary>Creates a new ProductEntity.ScheduleId field instance</summary>
		public static EntityField2 ScheduleId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ProductFieldIndex.ScheduleId); }
		}
		/// <summary>Creates a new ProductEntity.Name field instance</summary>
		public static EntityField2 Name 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ProductFieldIndex.Name); }
		}
		/// <summary>Creates a new ProductEntity.Description field instance</summary>
		public static EntityField2 Description 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ProductFieldIndex.Description); }
		}
		/// <summary>Creates a new ProductEntity.Type field instance</summary>
		public static EntityField2 Type 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ProductFieldIndex.Type); }
		}
		/// <summary>Creates a new ProductEntity.SubType field instance</summary>
		public static EntityField2 SubType 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ProductFieldIndex.SubType); }
		}
		/// <summary>Creates a new ProductEntity.ButtonText field instance</summary>
		public static EntityField2 ButtonText 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ProductFieldIndex.ButtonText); }
		}
		/// <summary>Creates a new ProductEntity.CustomizeButtonText field instance</summary>
		public static EntityField2 CustomizeButtonText 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ProductFieldIndex.CustomizeButtonText); }
		}
		/// <summary>Creates a new ProductEntity.PriceIn field instance</summary>
		public static EntityField2 PriceIn 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ProductFieldIndex.PriceIn); }
		}
		/// <summary>Creates a new ProductEntity.VattariffId field instance</summary>
		public static EntityField2 VattariffId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ProductFieldIndex.VattariffId); }
		}
		/// <summary>Creates a new ProductEntity.VattariffPercentage field instance</summary>
		public static EntityField2 VattariffPercentage 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ProductFieldIndex.VattariffPercentage); }
		}
		/// <summary>Creates a new ProductEntity.SortOrder field instance</summary>
		public static EntityField2 SortOrder 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ProductFieldIndex.SortOrder); }
		}
		/// <summary>Creates a new ProductEntity.PosproductId field instance</summary>
		public static EntityField2 PosproductId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ProductFieldIndex.PosproductId); }
		}
		/// <summary>Creates a new ProductEntity.ExternalProductId field instance</summary>
		public static EntityField2 ExternalProductId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ProductFieldIndex.ExternalProductId); }
		}
		/// <summary>Creates a new ProductEntity.TextColor field instance</summary>
		public static EntityField2 TextColor 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ProductFieldIndex.TextColor); }
		}
		/// <summary>Creates a new ProductEntity.BackgroundColor field instance</summary>
		public static EntityField2 BackgroundColor 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ProductFieldIndex.BackgroundColor); }
		}
		/// <summary>Creates a new ProductEntity.DisplayOnHomepage field instance</summary>
		public static EntityField2 DisplayOnHomepage 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ProductFieldIndex.DisplayOnHomepage); }
		}
		/// <summary>Creates a new ProductEntity.AvailableOnOtoucho field instance</summary>
		public static EntityField2 AvailableOnOtoucho 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ProductFieldIndex.AvailableOnOtoucho); }
		}
		/// <summary>Creates a new ProductEntity.AvailableOnObymobi field instance</summary>
		public static EntityField2 AvailableOnObymobi 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ProductFieldIndex.AvailableOnObymobi); }
		}
		/// <summary>Creates a new ProductEntity.SoldOut field instance</summary>
		public static EntityField2 SoldOut 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ProductFieldIndex.SoldOut); }
		}
		/// <summary>Creates a new ProductEntity.Rateable field instance</summary>
		public static EntityField2 Rateable 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ProductFieldIndex.Rateable); }
		}
		/// <summary>Creates a new ProductEntity.AllowFreeText field instance</summary>
		public static EntityField2 AllowFreeText 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ProductFieldIndex.AllowFreeText); }
		}
		/// <summary>Creates a new ProductEntity.AnnouncementAction field instance</summary>
		public static EntityField2 AnnouncementAction 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ProductFieldIndex.AnnouncementAction); }
		}
		/// <summary>Creates a new ProductEntity.Color field instance</summary>
		public static EntityField2 Color 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ProductFieldIndex.Color); }
		}
		/// <summary>Creates a new ProductEntity.ManualDescriptionEnabled field instance</summary>
		public static EntityField2 ManualDescriptionEnabled 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ProductFieldIndex.ManualDescriptionEnabled); }
		}
		/// <summary>Creates a new ProductEntity.WebTypeSmartphoneUrl field instance</summary>
		public static EntityField2 WebTypeSmartphoneUrl 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ProductFieldIndex.WebTypeSmartphoneUrl); }
		}
		/// <summary>Creates a new ProductEntity.WebTypeTabletUrl field instance</summary>
		public static EntityField2 WebTypeTabletUrl 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ProductFieldIndex.WebTypeTabletUrl); }
		}
		/// <summary>Creates a new ProductEntity.Geofencing field instance</summary>
		public static EntityField2 Geofencing 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ProductFieldIndex.Geofencing); }
		}
		/// <summary>Creates a new ProductEntity.HotSOSIssueId field instance</summary>
		public static EntityField2 HotSOSIssueId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ProductFieldIndex.HotSOSIssueId); }
		}
		/// <summary>Creates a new ProductEntity.ViewLayoutType field instance</summary>
		public static EntityField2 ViewLayoutType 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ProductFieldIndex.ViewLayoutType); }
		}
		/// <summary>Creates a new ProductEntity.DeliveryLocationType field instance</summary>
		public static EntityField2 DeliveryLocationType 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ProductFieldIndex.DeliveryLocationType); }
		}
		/// <summary>Creates a new ProductEntity.SupportNotificationType field instance</summary>
		public static EntityField2 SupportNotificationType 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ProductFieldIndex.SupportNotificationType); }
		}
		/// <summary>Creates a new ProductEntity.HidePrice field instance</summary>
		public static EntityField2 HidePrice 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ProductFieldIndex.HidePrice); }
		}
		/// <summary>Creates a new ProductEntity.VisibilityType field instance</summary>
		public static EntityField2 VisibilityType 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ProductFieldIndex.VisibilityType); }
		}
		/// <summary>Creates a new ProductEntity.InheritAlterations field instance</summary>
		public static EntityField2 InheritAlterations 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ProductFieldIndex.InheritAlterations); }
		}
		/// <summary>Creates a new ProductEntity.InheritAlterationsFromBrand field instance</summary>
		public static EntityField2 InheritAlterationsFromBrand 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ProductFieldIndex.InheritAlterationsFromBrand); }
		}
		/// <summary>Creates a new ProductEntity.InheritAttachmentsFromBrand field instance</summary>
		public static EntityField2 InheritAttachmentsFromBrand 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ProductFieldIndex.InheritAttachmentsFromBrand); }
		}
		/// <summary>Creates a new ProductEntity.CreatedBy field instance</summary>
		public static EntityField2 CreatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ProductFieldIndex.CreatedBy); }
		}
		/// <summary>Creates a new ProductEntity.UpdatedBy field instance</summary>
		public static EntityField2 UpdatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ProductFieldIndex.UpdatedBy); }
		}
		/// <summary>Creates a new ProductEntity.CreatedUTC field instance</summary>
		public static EntityField2 CreatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ProductFieldIndex.CreatedUTC); }
		}
		/// <summary>Creates a new ProductEntity.UpdatedUTC field instance</summary>
		public static EntityField2 UpdatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ProductFieldIndex.UpdatedUTC); }
		}
		/// <summary>Creates a new ProductEntity.GenericProductChangedUTC field instance</summary>
		public static EntityField2 GenericProductChangedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ProductFieldIndex.GenericProductChangedUTC); }
		}
		/// <summary>Creates a new ProductEntity.IsLinkedToGeneric field instance</summary>
		public static EntityField2 IsLinkedToGeneric 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ProductFieldIndex.IsLinkedToGeneric); }
		}
		/// <summary>Creates a new ProductEntity.OverrideSubType field instance</summary>
		public static EntityField2 OverrideSubType 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ProductFieldIndex.OverrideSubType); }
		}
		/// <summary>Creates a new ProductEntity.ShortDescription field instance</summary>
		public static EntityField2 ShortDescription 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ProductFieldIndex.ShortDescription); }
		}
		/// <summary>Creates a new ProductEntity.InheritName field instance</summary>
		public static EntityField2 InheritName 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ProductFieldIndex.InheritName); }
		}
		/// <summary>Creates a new ProductEntity.InheritPrice field instance</summary>
		public static EntityField2 InheritPrice 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ProductFieldIndex.InheritPrice); }
		}
		/// <summary>Creates a new ProductEntity.InheritButtonText field instance</summary>
		public static EntityField2 InheritButtonText 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ProductFieldIndex.InheritButtonText); }
		}
		/// <summary>Creates a new ProductEntity.InheritCustomizeButtonText field instance</summary>
		public static EntityField2 InheritCustomizeButtonText 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ProductFieldIndex.InheritCustomizeButtonText); }
		}
		/// <summary>Creates a new ProductEntity.InheritWebTypeTabletUrl field instance</summary>
		public static EntityField2 InheritWebTypeTabletUrl 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ProductFieldIndex.InheritWebTypeTabletUrl); }
		}
		/// <summary>Creates a new ProductEntity.InheritWebTypeSmartphoneUrl field instance</summary>
		public static EntityField2 InheritWebTypeSmartphoneUrl 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ProductFieldIndex.InheritWebTypeSmartphoneUrl); }
		}
		/// <summary>Creates a new ProductEntity.InheritDescription field instance</summary>
		public static EntityField2 InheritDescription 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ProductFieldIndex.InheritDescription); }
		}
		/// <summary>Creates a new ProductEntity.InheritColor field instance</summary>
		public static EntityField2 InheritColor 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ProductFieldIndex.InheritColor); }
		}
		/// <summary>Creates a new ProductEntity.InheritMedia field instance</summary>
		public static EntityField2 InheritMedia 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ProductFieldIndex.InheritMedia); }
		}
		/// <summary>Creates a new ProductEntity.TaxTariffId field instance</summary>
		public static EntityField2 TaxTariffId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ProductFieldIndex.TaxTariffId); }
		}
		/// <summary>Creates a new ProductEntity.IsAlcoholic field instance</summary>
		public static EntityField2 IsAlcoholic 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ProductFieldIndex.IsAlcoholic); }
		}
		/// <summary>Creates a new ProductEntity.IsAvailable field instance</summary>
		public static EntityField2 IsAvailable 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ProductFieldIndex.IsAvailable); }
		}
		/// <summary>Creates a new ProductEntity.SystemName field instance</summary>
		public static EntityField2 SystemName 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ProductFieldIndex.SystemName); }
		}
		/// <summary>Creates a new ProductEntity.InheritSystemName field instance</summary>
		public static EntityField2 InheritSystemName 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ProductFieldIndex.InheritSystemName); }
		}
		/// <summary>Creates a new ProductEntity.ExternalIdentifier field instance</summary>
		public static EntityField2 ExternalIdentifier 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ProductFieldIndex.ExternalIdentifier); }
		}
		/// <summary>Creates a new ProductEntity.InheritExternalIdentifier field instance</summary>
		public static EntityField2 InheritExternalIdentifier 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ProductFieldIndex.InheritExternalIdentifier); }
		}
		/// <summary>Creates a new ProductEntity.PointOfInterestId field instance</summary>
		public static EntityField2 PointOfInterestId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ProductFieldIndex.PointOfInterestId); }
		}
		/// <summary>Creates a new ProductEntity.CoversType field instance</summary>
		public static EntityField2 CoversType 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ProductFieldIndex.CoversType); }
		}
	}

	/// <summary>Field Creation Class for entity ProductAlterationEntity</summary>
	public partial class ProductAlterationFields
	{
		/// <summary>Creates a new ProductAlterationEntity.ProductAlterationId field instance</summary>
		public static EntityField2 ProductAlterationId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ProductAlterationFieldIndex.ProductAlterationId); }
		}
		/// <summary>Creates a new ProductAlterationEntity.ParentCompanyId field instance</summary>
		public static EntityField2 ParentCompanyId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ProductAlterationFieldIndex.ParentCompanyId); }
		}
		/// <summary>Creates a new ProductAlterationEntity.Version field instance</summary>
		public static EntityField2 Version 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ProductAlterationFieldIndex.Version); }
		}
		/// <summary>Creates a new ProductAlterationEntity.ProductId field instance</summary>
		public static EntityField2 ProductId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ProductAlterationFieldIndex.ProductId); }
		}
		/// <summary>Creates a new ProductAlterationEntity.AlterationId field instance</summary>
		public static EntityField2 AlterationId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ProductAlterationFieldIndex.AlterationId); }
		}
		/// <summary>Creates a new ProductAlterationEntity.SortOrder field instance</summary>
		public static EntityField2 SortOrder 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ProductAlterationFieldIndex.SortOrder); }
		}
		/// <summary>Creates a new ProductAlterationEntity.PosproductPosalterationId field instance</summary>
		public static EntityField2 PosproductPosalterationId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ProductAlterationFieldIndex.PosproductPosalterationId); }
		}
		/// <summary>Creates a new ProductAlterationEntity.GenericproductGenericalterationId field instance</summary>
		public static EntityField2 GenericproductGenericalterationId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ProductAlterationFieldIndex.GenericproductGenericalterationId); }
		}
		/// <summary>Creates a new ProductAlterationEntity.CreatedBy field instance</summary>
		public static EntityField2 CreatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ProductAlterationFieldIndex.CreatedBy); }
		}
		/// <summary>Creates a new ProductAlterationEntity.UpdatedBy field instance</summary>
		public static EntityField2 UpdatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ProductAlterationFieldIndex.UpdatedBy); }
		}
		/// <summary>Creates a new ProductAlterationEntity.CreatedUTC field instance</summary>
		public static EntityField2 CreatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ProductAlterationFieldIndex.CreatedUTC); }
		}
		/// <summary>Creates a new ProductAlterationEntity.UpdatedUTC field instance</summary>
		public static EntityField2 UpdatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ProductAlterationFieldIndex.UpdatedUTC); }
		}
	}

	/// <summary>Field Creation Class for entity ProductCategoryEntity</summary>
	public partial class ProductCategoryFields
	{
		/// <summary>Creates a new ProductCategoryEntity.ProductCategoryId field instance</summary>
		public static EntityField2 ProductCategoryId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ProductCategoryFieldIndex.ProductCategoryId); }
		}
		/// <summary>Creates a new ProductCategoryEntity.ParentCompanyId field instance</summary>
		public static EntityField2 ParentCompanyId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ProductCategoryFieldIndex.ParentCompanyId); }
		}
		/// <summary>Creates a new ProductCategoryEntity.ProductId field instance</summary>
		public static EntityField2 ProductId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ProductCategoryFieldIndex.ProductId); }
		}
		/// <summary>Creates a new ProductCategoryEntity.CategoryId field instance</summary>
		public static EntityField2 CategoryId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ProductCategoryFieldIndex.CategoryId); }
		}
		/// <summary>Creates a new ProductCategoryEntity.SortOrder field instance</summary>
		public static EntityField2 SortOrder 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ProductCategoryFieldIndex.SortOrder); }
		}
		/// <summary>Creates a new ProductCategoryEntity.InheritSuggestions field instance</summary>
		public static EntityField2 InheritSuggestions 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ProductCategoryFieldIndex.InheritSuggestions); }
		}
		/// <summary>Creates a new ProductCategoryEntity.CreatedBy field instance</summary>
		public static EntityField2 CreatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ProductCategoryFieldIndex.CreatedBy); }
		}
		/// <summary>Creates a new ProductCategoryEntity.UpdatedBy field instance</summary>
		public static EntityField2 UpdatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ProductCategoryFieldIndex.UpdatedBy); }
		}
		/// <summary>Creates a new ProductCategoryEntity.CreatedUTC field instance</summary>
		public static EntityField2 CreatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ProductCategoryFieldIndex.CreatedUTC); }
		}
		/// <summary>Creates a new ProductCategoryEntity.UpdatedUTC field instance</summary>
		public static EntityField2 UpdatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ProductCategoryFieldIndex.UpdatedUTC); }
		}
	}

	/// <summary>Field Creation Class for entity ProductCategoryTagEntity</summary>
	public partial class ProductCategoryTagFields
	{
		/// <summary>Creates a new ProductCategoryTagEntity.ProductCategoryTagId field instance</summary>
		public static EntityField2 ProductCategoryTagId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ProductCategoryTagFieldIndex.ProductCategoryTagId); }
		}
		/// <summary>Creates a new ProductCategoryTagEntity.ProductCategoryId field instance</summary>
		public static EntityField2 ProductCategoryId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ProductCategoryTagFieldIndex.ProductCategoryId); }
		}
		/// <summary>Creates a new ProductCategoryTagEntity.CategoryId field instance</summary>
		public static EntityField2 CategoryId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ProductCategoryTagFieldIndex.CategoryId); }
		}
		/// <summary>Creates a new ProductCategoryTagEntity.TagId field instance</summary>
		public static EntityField2 TagId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ProductCategoryTagFieldIndex.TagId); }
		}
		/// <summary>Creates a new ProductCategoryTagEntity.ParentCompanyId field instance</summary>
		public static EntityField2 ParentCompanyId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ProductCategoryTagFieldIndex.ParentCompanyId); }
		}
		/// <summary>Creates a new ProductCategoryTagEntity.CreatedUTC field instance</summary>
		public static EntityField2 CreatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ProductCategoryTagFieldIndex.CreatedUTC); }
		}
		/// <summary>Creates a new ProductCategoryTagEntity.UpdatedUTC field instance</summary>
		public static EntityField2 UpdatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ProductCategoryTagFieldIndex.UpdatedUTC); }
		}
	}

	/// <summary>Field Creation Class for entity ProductTagEntity</summary>
	public partial class ProductTagFields
	{
		/// <summary>Creates a new ProductTagEntity.ProductTagId field instance</summary>
		public static EntityField2 ProductTagId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ProductTagFieldIndex.ProductTagId); }
		}
		/// <summary>Creates a new ProductTagEntity.ProductId field instance</summary>
		public static EntityField2 ProductId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ProductTagFieldIndex.ProductId); }
		}
		/// <summary>Creates a new ProductTagEntity.TagId field instance</summary>
		public static EntityField2 TagId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ProductTagFieldIndex.TagId); }
		}
		/// <summary>Creates a new ProductTagEntity.ParentCompanyId field instance</summary>
		public static EntityField2 ParentCompanyId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ProductTagFieldIndex.ParentCompanyId); }
		}
		/// <summary>Creates a new ProductTagEntity.CreatedUTC field instance</summary>
		public static EntityField2 CreatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ProductTagFieldIndex.CreatedUTC); }
		}
		/// <summary>Creates a new ProductTagEntity.UpdatedUTC field instance</summary>
		public static EntityField2 UpdatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ProductTagFieldIndex.UpdatedUTC); }
		}
	}

	/// <summary>Field Creation Class for entity ReceiptEntity</summary>
	public partial class ReceiptFields
	{
		/// <summary>Creates a new ReceiptEntity.ReceiptId field instance</summary>
		public static EntityField2 ReceiptId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ReceiptFieldIndex.ReceiptId); }
		}
		/// <summary>Creates a new ReceiptEntity.ReceiptTemplateId field instance</summary>
		public static EntityField2 ReceiptTemplateId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ReceiptFieldIndex.ReceiptTemplateId); }
		}
		/// <summary>Creates a new ReceiptEntity.CompanyId field instance</summary>
		public static EntityField2 CompanyId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ReceiptFieldIndex.CompanyId); }
		}
		/// <summary>Creates a new ReceiptEntity.OrderId field instance</summary>
		public static EntityField2 OrderId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ReceiptFieldIndex.OrderId); }
		}
		/// <summary>Creates a new ReceiptEntity.Number field instance</summary>
		public static EntityField2 Number 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ReceiptFieldIndex.Number); }
		}
		/// <summary>Creates a new ReceiptEntity.SellerName field instance</summary>
		public static EntityField2 SellerName 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ReceiptFieldIndex.SellerName); }
		}
		/// <summary>Creates a new ReceiptEntity.SellerAddress field instance</summary>
		public static EntityField2 SellerAddress 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ReceiptFieldIndex.SellerAddress); }
		}
		/// <summary>Creates a new ReceiptEntity.SellerContactInfo field instance</summary>
		public static EntityField2 SellerContactInfo 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ReceiptFieldIndex.SellerContactInfo); }
		}
		/// <summary>Creates a new ReceiptEntity.ServiceMethodName field instance</summary>
		public static EntityField2 ServiceMethodName 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ReceiptFieldIndex.ServiceMethodName); }
		}
		/// <summary>Creates a new ReceiptEntity.CheckoutMethodName field instance</summary>
		public static EntityField2 CheckoutMethodName 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ReceiptFieldIndex.CheckoutMethodName); }
		}
		/// <summary>Creates a new ReceiptEntity.CreatedBy field instance</summary>
		public static EntityField2 CreatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ReceiptFieldIndex.CreatedBy); }
		}
		/// <summary>Creates a new ReceiptEntity.UpdatedBy field instance</summary>
		public static EntityField2 UpdatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ReceiptFieldIndex.UpdatedBy); }
		}
		/// <summary>Creates a new ReceiptEntity.CreatedUTC field instance</summary>
		public static EntityField2 CreatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ReceiptFieldIndex.CreatedUTC); }
		}
		/// <summary>Creates a new ReceiptEntity.UpdatedUTC field instance</summary>
		public static EntityField2 UpdatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ReceiptFieldIndex.UpdatedUTC); }
		}
		/// <summary>Creates a new ReceiptEntity.TaxBreakdown field instance</summary>
		public static EntityField2 TaxBreakdown 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ReceiptFieldIndex.TaxBreakdown); }
		}
		/// <summary>Creates a new ReceiptEntity.Email field instance</summary>
		public static EntityField2 Email 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ReceiptFieldIndex.Email); }
		}
		/// <summary>Creates a new ReceiptEntity.Phonenumber field instance</summary>
		public static EntityField2 Phonenumber 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ReceiptFieldIndex.Phonenumber); }
		}
		/// <summary>Creates a new ReceiptEntity.VatNumber field instance</summary>
		public static EntityField2 VatNumber 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ReceiptFieldIndex.VatNumber); }
		}
		/// <summary>Creates a new ReceiptEntity.PaymentProcessorInfo field instance</summary>
		public static EntityField2 PaymentProcessorInfo 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ReceiptFieldIndex.PaymentProcessorInfo); }
		}
		/// <summary>Creates a new ReceiptEntity.SellerContactEmail field instance</summary>
		public static EntityField2 SellerContactEmail 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ReceiptFieldIndex.SellerContactEmail); }
		}
		/// <summary>Creates a new ReceiptEntity.OutletSellerInformationId field instance</summary>
		public static EntityField2 OutletSellerInformationId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ReceiptFieldIndex.OutletSellerInformationId); }
		}
	}

	/// <summary>Field Creation Class for entity ReceiptTemplateEntity</summary>
	public partial class ReceiptTemplateFields
	{
		/// <summary>Creates a new ReceiptTemplateEntity.ReceiptTemplateId field instance</summary>
		public static EntityField2 ReceiptTemplateId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ReceiptTemplateFieldIndex.ReceiptTemplateId); }
		}
		/// <summary>Creates a new ReceiptTemplateEntity.CompanyId field instance</summary>
		public static EntityField2 CompanyId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ReceiptTemplateFieldIndex.CompanyId); }
		}
		/// <summary>Creates a new ReceiptTemplateEntity.SellerName field instance</summary>
		public static EntityField2 SellerName 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ReceiptTemplateFieldIndex.SellerName); }
		}
		/// <summary>Creates a new ReceiptTemplateEntity.SellerAddress field instance</summary>
		public static EntityField2 SellerAddress 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ReceiptTemplateFieldIndex.SellerAddress); }
		}
		/// <summary>Creates a new ReceiptTemplateEntity.SellerContactInfo field instance</summary>
		public static EntityField2 SellerContactInfo 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ReceiptTemplateFieldIndex.SellerContactInfo); }
		}
		/// <summary>Creates a new ReceiptTemplateEntity.TaxBreakdown field instance</summary>
		public static EntityField2 TaxBreakdown 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ReceiptTemplateFieldIndex.TaxBreakdown); }
		}
		/// <summary>Creates a new ReceiptTemplateEntity.CreatedBy field instance</summary>
		public static EntityField2 CreatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ReceiptTemplateFieldIndex.CreatedBy); }
		}
		/// <summary>Creates a new ReceiptTemplateEntity.UpdatedBy field instance</summary>
		public static EntityField2 UpdatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ReceiptTemplateFieldIndex.UpdatedBy); }
		}
		/// <summary>Creates a new ReceiptTemplateEntity.CreatedUTC field instance</summary>
		public static EntityField2 CreatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ReceiptTemplateFieldIndex.CreatedUTC); }
		}
		/// <summary>Creates a new ReceiptTemplateEntity.UpdatedUTC field instance</summary>
		public static EntityField2 UpdatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ReceiptTemplateFieldIndex.UpdatedUTC); }
		}
		/// <summary>Creates a new ReceiptTemplateEntity.Name field instance</summary>
		public static EntityField2 Name 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ReceiptTemplateFieldIndex.Name); }
		}
		/// <summary>Creates a new ReceiptTemplateEntity.Email field instance</summary>
		public static EntityField2 Email 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ReceiptTemplateFieldIndex.Email); }
		}
		/// <summary>Creates a new ReceiptTemplateEntity.Phonenumber field instance</summary>
		public static EntityField2 Phonenumber 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ReceiptTemplateFieldIndex.Phonenumber); }
		}
		/// <summary>Creates a new ReceiptTemplateEntity.VatNumber field instance</summary>
		public static EntityField2 VatNumber 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ReceiptTemplateFieldIndex.VatNumber); }
		}
		/// <summary>Creates a new ReceiptTemplateEntity.SellerContactEmail field instance</summary>
		public static EntityField2 SellerContactEmail 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ReceiptTemplateFieldIndex.SellerContactEmail); }
		}
	}

	/// <summary>Field Creation Class for entity RequestlogEntity</summary>
	public partial class RequestlogFields
	{
		/// <summary>Creates a new RequestlogEntity.RequestlogId field instance</summary>
		public static EntityField2 RequestlogId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(RequestlogFieldIndex.RequestlogId); }
		}
		/// <summary>Creates a new RequestlogEntity.ServerName field instance</summary>
		public static EntityField2 ServerName 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(RequestlogFieldIndex.ServerName); }
		}
		/// <summary>Creates a new RequestlogEntity.UserAgent field instance</summary>
		public static EntityField2 UserAgent 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(RequestlogFieldIndex.UserAgent); }
		}
		/// <summary>Creates a new RequestlogEntity.SourceApplication field instance</summary>
		public static EntityField2 SourceApplication 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(RequestlogFieldIndex.SourceApplication); }
		}
		/// <summary>Creates a new RequestlogEntity.Identifier field instance</summary>
		public static EntityField2 Identifier 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(RequestlogFieldIndex.Identifier); }
		}
		/// <summary>Creates a new RequestlogEntity.CustomerId field instance</summary>
		public static EntityField2 CustomerId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(RequestlogFieldIndex.CustomerId); }
		}
		/// <summary>Creates a new RequestlogEntity.TerminalId field instance</summary>
		public static EntityField2 TerminalId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(RequestlogFieldIndex.TerminalId); }
		}
		/// <summary>Creates a new RequestlogEntity.Phonenumber field instance</summary>
		public static EntityField2 Phonenumber 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(RequestlogFieldIndex.Phonenumber); }
		}
		/// <summary>Creates a new RequestlogEntity.MethodName field instance</summary>
		public static EntityField2 MethodName 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(RequestlogFieldIndex.MethodName); }
		}
		/// <summary>Creates a new RequestlogEntity.ResultEnumTypeName field instance</summary>
		public static EntityField2 ResultEnumTypeName 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(RequestlogFieldIndex.ResultEnumTypeName); }
		}
		/// <summary>Creates a new RequestlogEntity.ResultEnumValueName field instance</summary>
		public static EntityField2 ResultEnumValueName 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(RequestlogFieldIndex.ResultEnumValueName); }
		}
		/// <summary>Creates a new RequestlogEntity.ResultCode field instance</summary>
		public static EntityField2 ResultCode 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(RequestlogFieldIndex.ResultCode); }
		}
		/// <summary>Creates a new RequestlogEntity.ResultMessage field instance</summary>
		public static EntityField2 ResultMessage 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(RequestlogFieldIndex.ResultMessage); }
		}
		/// <summary>Creates a new RequestlogEntity.Parameter1 field instance</summary>
		public static EntityField2 Parameter1 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(RequestlogFieldIndex.Parameter1); }
		}
		/// <summary>Creates a new RequestlogEntity.Parameter2 field instance</summary>
		public static EntityField2 Parameter2 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(RequestlogFieldIndex.Parameter2); }
		}
		/// <summary>Creates a new RequestlogEntity.Parameter3 field instance</summary>
		public static EntityField2 Parameter3 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(RequestlogFieldIndex.Parameter3); }
		}
		/// <summary>Creates a new RequestlogEntity.Parameter4 field instance</summary>
		public static EntityField2 Parameter4 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(RequestlogFieldIndex.Parameter4); }
		}
		/// <summary>Creates a new RequestlogEntity.Parameter5 field instance</summary>
		public static EntityField2 Parameter5 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(RequestlogFieldIndex.Parameter5); }
		}
		/// <summary>Creates a new RequestlogEntity.Parameter6 field instance</summary>
		public static EntityField2 Parameter6 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(RequestlogFieldIndex.Parameter6); }
		}
		/// <summary>Creates a new RequestlogEntity.ResultBody field instance</summary>
		public static EntityField2 ResultBody 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(RequestlogFieldIndex.ResultBody); }
		}
		/// <summary>Creates a new RequestlogEntity.ErrorMessage field instance</summary>
		public static EntityField2 ErrorMessage 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(RequestlogFieldIndex.ErrorMessage); }
		}
		/// <summary>Creates a new RequestlogEntity.ErrorStackTrace field instance</summary>
		public static EntityField2 ErrorStackTrace 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(RequestlogFieldIndex.ErrorStackTrace); }
		}
		/// <summary>Creates a new RequestlogEntity.Xml field instance</summary>
		public static EntityField2 Xml 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(RequestlogFieldIndex.Xml); }
		}
		/// <summary>Creates a new RequestlogEntity.RawRequest field instance</summary>
		public static EntityField2 RawRequest 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(RequestlogFieldIndex.RawRequest); }
		}
		/// <summary>Creates a new RequestlogEntity.TraceFromMobile field instance</summary>
		public static EntityField2 TraceFromMobile 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(RequestlogFieldIndex.TraceFromMobile); }
		}
		/// <summary>Creates a new RequestlogEntity.MobileVendor field instance</summary>
		public static EntityField2 MobileVendor 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(RequestlogFieldIndex.MobileVendor); }
		}
		/// <summary>Creates a new RequestlogEntity.MobileIdentifier field instance</summary>
		public static EntityField2 MobileIdentifier 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(RequestlogFieldIndex.MobileIdentifier); }
		}
		/// <summary>Creates a new RequestlogEntity.MobilePlatform field instance</summary>
		public static EntityField2 MobilePlatform 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(RequestlogFieldIndex.MobilePlatform); }
		}
		/// <summary>Creates a new RequestlogEntity.MobileOs field instance</summary>
		public static EntityField2 MobileOs 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(RequestlogFieldIndex.MobileOs); }
		}
		/// <summary>Creates a new RequestlogEntity.MobileName field instance</summary>
		public static EntityField2 MobileName 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(RequestlogFieldIndex.MobileName); }
		}
		/// <summary>Creates a new RequestlogEntity.DeviceInfo field instance</summary>
		public static EntityField2 DeviceInfo 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(RequestlogFieldIndex.DeviceInfo); }
		}
		/// <summary>Creates a new RequestlogEntity.Log field instance</summary>
		public static EntityField2 Log 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(RequestlogFieldIndex.Log); }
		}
		/// <summary>Creates a new RequestlogEntity.CreatedBy field instance</summary>
		public static EntityField2 CreatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(RequestlogFieldIndex.CreatedBy); }
		}
		/// <summary>Creates a new RequestlogEntity.UpdatedBy field instance</summary>
		public static EntityField2 UpdatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(RequestlogFieldIndex.UpdatedBy); }
		}
		/// <summary>Creates a new RequestlogEntity.CreatedUTC field instance</summary>
		public static EntityField2 CreatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(RequestlogFieldIndex.CreatedUTC); }
		}
		/// <summary>Creates a new RequestlogEntity.UpdatedUTC field instance</summary>
		public static EntityField2 UpdatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(RequestlogFieldIndex.UpdatedUTC); }
		}
	}

	/// <summary>Field Creation Class for entity RouteEntity</summary>
	public partial class RouteFields
	{
		/// <summary>Creates a new RouteEntity.RouteId field instance</summary>
		public static EntityField2 RouteId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(RouteFieldIndex.RouteId); }
		}
		/// <summary>Creates a new RouteEntity.CompanyId field instance</summary>
		public static EntityField2 CompanyId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(RouteFieldIndex.CompanyId); }
		}
		/// <summary>Creates a new RouteEntity.Name field instance</summary>
		public static EntityField2 Name 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(RouteFieldIndex.Name); }
		}
		/// <summary>Creates a new RouteEntity.StepTimeoutMinutes field instance</summary>
		public static EntityField2 StepTimeoutMinutes 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(RouteFieldIndex.StepTimeoutMinutes); }
		}
		/// <summary>Creates a new RouteEntity.RetrievalSupportNotificationTimeoutMinutes field instance</summary>
		public static EntityField2 RetrievalSupportNotificationTimeoutMinutes 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(RouteFieldIndex.RetrievalSupportNotificationTimeoutMinutes); }
		}
		/// <summary>Creates a new RouteEntity.BeingHandledSupportNotificationTimeoutMinutes field instance</summary>
		public static EntityField2 BeingHandledSupportNotificationTimeoutMinutes 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(RouteFieldIndex.BeingHandledSupportNotificationTimeoutMinutes); }
		}
		/// <summary>Creates a new RouteEntity.LogAlways field instance</summary>
		public static EntityField2 LogAlways 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(RouteFieldIndex.LogAlways); }
		}
		/// <summary>Creates a new RouteEntity.EscalationRouteId field instance</summary>
		public static EntityField2 EscalationRouteId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(RouteFieldIndex.EscalationRouteId); }
		}
		/// <summary>Creates a new RouteEntity.CreatedBy field instance</summary>
		public static EntityField2 CreatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(RouteFieldIndex.CreatedBy); }
		}
		/// <summary>Creates a new RouteEntity.UpdatedBy field instance</summary>
		public static EntityField2 UpdatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(RouteFieldIndex.UpdatedBy); }
		}
		/// <summary>Creates a new RouteEntity.CreatedUTC field instance</summary>
		public static EntityField2 CreatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(RouteFieldIndex.CreatedUTC); }
		}
		/// <summary>Creates a new RouteEntity.UpdatedUTC field instance</summary>
		public static EntityField2 UpdatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(RouteFieldIndex.UpdatedUTC); }
		}
	}

	/// <summary>Field Creation Class for entity RoutestepEntity</summary>
	public partial class RoutestepFields
	{
		/// <summary>Creates a new RoutestepEntity.RoutestepId field instance</summary>
		public static EntityField2 RoutestepId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(RoutestepFieldIndex.RoutestepId); }
		}
		/// <summary>Creates a new RoutestepEntity.ParentCompanyId field instance</summary>
		public static EntityField2 ParentCompanyId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(RoutestepFieldIndex.ParentCompanyId); }
		}
		/// <summary>Creates a new RoutestepEntity.RouteId field instance</summary>
		public static EntityField2 RouteId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(RoutestepFieldIndex.RouteId); }
		}
		/// <summary>Creates a new RoutestepEntity.Number field instance</summary>
		public static EntityField2 Number 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(RoutestepFieldIndex.Number); }
		}
		/// <summary>Creates a new RoutestepEntity.TimeoutMinutes field instance</summary>
		public static EntityField2 TimeoutMinutes 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(RoutestepFieldIndex.TimeoutMinutes); }
		}
		/// <summary>Creates a new RoutestepEntity.ContinueOnFailure field instance</summary>
		public static EntityField2 ContinueOnFailure 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(RoutestepFieldIndex.ContinueOnFailure); }
		}
		/// <summary>Creates a new RoutestepEntity.CompleteRouteOnComplete field instance</summary>
		public static EntityField2 CompleteRouteOnComplete 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(RoutestepFieldIndex.CompleteRouteOnComplete); }
		}
		/// <summary>Creates a new RoutestepEntity.CreatedBy field instance</summary>
		public static EntityField2 CreatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(RoutestepFieldIndex.CreatedBy); }
		}
		/// <summary>Creates a new RoutestepEntity.UpdatedBy field instance</summary>
		public static EntityField2 UpdatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(RoutestepFieldIndex.UpdatedBy); }
		}
		/// <summary>Creates a new RoutestepEntity.CreatedUTC field instance</summary>
		public static EntityField2 CreatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(RoutestepFieldIndex.CreatedUTC); }
		}
		/// <summary>Creates a new RoutestepEntity.UpdatedUTC field instance</summary>
		public static EntityField2 UpdatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(RoutestepFieldIndex.UpdatedUTC); }
		}
	}

	/// <summary>Field Creation Class for entity RoutestephandlerEntity</summary>
	public partial class RoutestephandlerFields
	{
		/// <summary>Creates a new RoutestephandlerEntity.RoutestephandlerId field instance</summary>
		public static EntityField2 RoutestephandlerId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(RoutestephandlerFieldIndex.RoutestephandlerId); }
		}
		/// <summary>Creates a new RoutestephandlerEntity.ParentCompanyId field instance</summary>
		public static EntityField2 ParentCompanyId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(RoutestephandlerFieldIndex.ParentCompanyId); }
		}
		/// <summary>Creates a new RoutestephandlerEntity.RoutestepId field instance</summary>
		public static EntityField2 RoutestepId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(RoutestephandlerFieldIndex.RoutestepId); }
		}
		/// <summary>Creates a new RoutestephandlerEntity.TerminalId field instance</summary>
		public static EntityField2 TerminalId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(RoutestephandlerFieldIndex.TerminalId); }
		}
		/// <summary>Creates a new RoutestephandlerEntity.HandlerType field instance</summary>
		public static EntityField2 HandlerType 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(RoutestephandlerFieldIndex.HandlerType); }
		}
		/// <summary>Creates a new RoutestephandlerEntity.PrintReportType field instance</summary>
		public static EntityField2 PrintReportType 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(RoutestephandlerFieldIndex.PrintReportType); }
		}
		/// <summary>Creates a new RoutestephandlerEntity.FieldValue1 field instance</summary>
		public static EntityField2 FieldValue1 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(RoutestephandlerFieldIndex.FieldValue1); }
		}
		/// <summary>Creates a new RoutestephandlerEntity.FieldValue2 field instance</summary>
		public static EntityField2 FieldValue2 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(RoutestephandlerFieldIndex.FieldValue2); }
		}
		/// <summary>Creates a new RoutestephandlerEntity.FieldValue3 field instance</summary>
		public static EntityField2 FieldValue3 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(RoutestephandlerFieldIndex.FieldValue3); }
		}
		/// <summary>Creates a new RoutestephandlerEntity.FieldValue4 field instance</summary>
		public static EntityField2 FieldValue4 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(RoutestephandlerFieldIndex.FieldValue4); }
		}
		/// <summary>Creates a new RoutestephandlerEntity.FieldValue5 field instance</summary>
		public static EntityField2 FieldValue5 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(RoutestephandlerFieldIndex.FieldValue5); }
		}
		/// <summary>Creates a new RoutestephandlerEntity.FieldValue6 field instance</summary>
		public static EntityField2 FieldValue6 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(RoutestephandlerFieldIndex.FieldValue6); }
		}
		/// <summary>Creates a new RoutestephandlerEntity.FieldValue7 field instance</summary>
		public static EntityField2 FieldValue7 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(RoutestephandlerFieldIndex.FieldValue7); }
		}
		/// <summary>Creates a new RoutestephandlerEntity.FieldValue8 field instance</summary>
		public static EntityField2 FieldValue8 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(RoutestephandlerFieldIndex.FieldValue8); }
		}
		/// <summary>Creates a new RoutestephandlerEntity.FieldValue9 field instance</summary>
		public static EntityField2 FieldValue9 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(RoutestephandlerFieldIndex.FieldValue9); }
		}
		/// <summary>Creates a new RoutestephandlerEntity.FieldValue10 field instance</summary>
		public static EntityField2 FieldValue10 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(RoutestephandlerFieldIndex.FieldValue10); }
		}
		/// <summary>Creates a new RoutestephandlerEntity.SupportpoolId field instance</summary>
		public static EntityField2 SupportpoolId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(RoutestephandlerFieldIndex.SupportpoolId); }
		}
		/// <summary>Creates a new RoutestephandlerEntity.CreatedBy field instance</summary>
		public static EntityField2 CreatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(RoutestephandlerFieldIndex.CreatedBy); }
		}
		/// <summary>Creates a new RoutestephandlerEntity.UpdatedBy field instance</summary>
		public static EntityField2 UpdatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(RoutestephandlerFieldIndex.UpdatedBy); }
		}
		/// <summary>Creates a new RoutestephandlerEntity.CreatedUTC field instance</summary>
		public static EntityField2 CreatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(RoutestephandlerFieldIndex.CreatedUTC); }
		}
		/// <summary>Creates a new RoutestephandlerEntity.UpdatedUTC field instance</summary>
		public static EntityField2 UpdatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(RoutestephandlerFieldIndex.UpdatedUTC); }
		}
		/// <summary>Creates a new RoutestephandlerEntity.ExternalSystemId field instance</summary>
		public static EntityField2 ExternalSystemId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(RoutestephandlerFieldIndex.ExternalSystemId); }
		}
	}

	/// <summary>Field Creation Class for entity ScheduleEntity</summary>
	public partial class ScheduleFields
	{
		/// <summary>Creates a new ScheduleEntity.ScheduleId field instance</summary>
		public static EntityField2 ScheduleId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ScheduleFieldIndex.ScheduleId); }
		}
		/// <summary>Creates a new ScheduleEntity.CompanyId field instance</summary>
		public static EntityField2 CompanyId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ScheduleFieldIndex.CompanyId); }
		}
		/// <summary>Creates a new ScheduleEntity.Name field instance</summary>
		public static EntityField2 Name 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ScheduleFieldIndex.Name); }
		}
		/// <summary>Creates a new ScheduleEntity.CreatedUTC field instance</summary>
		public static EntityField2 CreatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ScheduleFieldIndex.CreatedUTC); }
		}
		/// <summary>Creates a new ScheduleEntity.CreatedBy field instance</summary>
		public static EntityField2 CreatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ScheduleFieldIndex.CreatedBy); }
		}
		/// <summary>Creates a new ScheduleEntity.UpdatedUTC field instance</summary>
		public static EntityField2 UpdatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ScheduleFieldIndex.UpdatedUTC); }
		}
		/// <summary>Creates a new ScheduleEntity.UpdatedBy field instance</summary>
		public static EntityField2 UpdatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ScheduleFieldIndex.UpdatedBy); }
		}
	}

	/// <summary>Field Creation Class for entity ScheduleitemEntity</summary>
	public partial class ScheduleitemFields
	{
		/// <summary>Creates a new ScheduleitemEntity.ScheduleitemId field instance</summary>
		public static EntityField2 ScheduleitemId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ScheduleitemFieldIndex.ScheduleitemId); }
		}
		/// <summary>Creates a new ScheduleitemEntity.ParentCompanyId field instance</summary>
		public static EntityField2 ParentCompanyId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ScheduleitemFieldIndex.ParentCompanyId); }
		}
		/// <summary>Creates a new ScheduleitemEntity.ScheduleId field instance</summary>
		public static EntityField2 ScheduleId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ScheduleitemFieldIndex.ScheduleId); }
		}
		/// <summary>Creates a new ScheduleitemEntity.SortOrder field instance</summary>
		public static EntityField2 SortOrder 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ScheduleitemFieldIndex.SortOrder); }
		}
		/// <summary>Creates a new ScheduleitemEntity.DayOfWeek field instance</summary>
		public static EntityField2 DayOfWeek 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ScheduleitemFieldIndex.DayOfWeek); }
		}
		/// <summary>Creates a new ScheduleitemEntity.TimeStart field instance</summary>
		public static EntityField2 TimeStart 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ScheduleitemFieldIndex.TimeStart); }
		}
		/// <summary>Creates a new ScheduleitemEntity.TimeEnd field instance</summary>
		public static EntityField2 TimeEnd 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ScheduleitemFieldIndex.TimeEnd); }
		}
		/// <summary>Creates a new ScheduleitemEntity.CreatedUTC field instance</summary>
		public static EntityField2 CreatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ScheduleitemFieldIndex.CreatedUTC); }
		}
		/// <summary>Creates a new ScheduleitemEntity.CreatedBy field instance</summary>
		public static EntityField2 CreatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ScheduleitemFieldIndex.CreatedBy); }
		}
		/// <summary>Creates a new ScheduleitemEntity.UpdatedUTC field instance</summary>
		public static EntityField2 UpdatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ScheduleitemFieldIndex.UpdatedUTC); }
		}
		/// <summary>Creates a new ScheduleitemEntity.UpdatedBy field instance</summary>
		public static EntityField2 UpdatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ScheduleitemFieldIndex.UpdatedBy); }
		}
	}

	/// <summary>Field Creation Class for entity ServiceMethodEntity</summary>
	public partial class ServiceMethodFields
	{
		/// <summary>Creates a new ServiceMethodEntity.ServiceMethodId field instance</summary>
		public static EntityField2 ServiceMethodId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ServiceMethodFieldIndex.ServiceMethodId); }
		}
		/// <summary>Creates a new ServiceMethodEntity.CompanyId field instance</summary>
		public static EntityField2 CompanyId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ServiceMethodFieldIndex.CompanyId); }
		}
		/// <summary>Creates a new ServiceMethodEntity.OutletId field instance</summary>
		public static EntityField2 OutletId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ServiceMethodFieldIndex.OutletId); }
		}
		/// <summary>Creates a new ServiceMethodEntity.Active field instance</summary>
		public static EntityField2 Active 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ServiceMethodFieldIndex.Active); }
		}
		/// <summary>Creates a new ServiceMethodEntity.Type field instance</summary>
		public static EntityField2 Type 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ServiceMethodFieldIndex.Type); }
		}
		/// <summary>Creates a new ServiceMethodEntity.Name field instance</summary>
		public static EntityField2 Name 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ServiceMethodFieldIndex.Name); }
		}
		/// <summary>Creates a new ServiceMethodEntity.Label field instance</summary>
		public static EntityField2 Label 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ServiceMethodFieldIndex.Label); }
		}
		/// <summary>Creates a new ServiceMethodEntity.Description field instance</summary>
		public static EntityField2 Description 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ServiceMethodFieldIndex.Description); }
		}
		/// <summary>Creates a new ServiceMethodEntity.AskEntryPermission field instance</summary>
		public static EntityField2 AskEntryPermission 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ServiceMethodFieldIndex.AskEntryPermission); }
		}
		/// <summary>Creates a new ServiceMethodEntity.ServiceChargePercentage field instance</summary>
		public static EntityField2 ServiceChargePercentage 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ServiceMethodFieldIndex.ServiceChargePercentage); }
		}
		/// <summary>Creates a new ServiceMethodEntity.ServiceChargeAmount field instance</summary>
		public static EntityField2 ServiceChargeAmount 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ServiceMethodFieldIndex.ServiceChargeAmount); }
		}
		/// <summary>Creates a new ServiceMethodEntity.ServiceChargeProductId field instance</summary>
		public static EntityField2 ServiceChargeProductId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ServiceMethodFieldIndex.ServiceChargeProductId); }
		}
		/// <summary>Creates a new ServiceMethodEntity.ConfirmationActive field instance</summary>
		public static EntityField2 ConfirmationActive 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ServiceMethodFieldIndex.ConfirmationActive); }
		}
		/// <summary>Creates a new ServiceMethodEntity.CreatedUTC field instance</summary>
		public static EntityField2 CreatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ServiceMethodFieldIndex.CreatedUTC); }
		}
		/// <summary>Creates a new ServiceMethodEntity.CreatedBy field instance</summary>
		public static EntityField2 CreatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ServiceMethodFieldIndex.CreatedBy); }
		}
		/// <summary>Creates a new ServiceMethodEntity.UpdatedUTC field instance</summary>
		public static EntityField2 UpdatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ServiceMethodFieldIndex.UpdatedUTC); }
		}
		/// <summary>Creates a new ServiceMethodEntity.UpdatedBy field instance</summary>
		public static EntityField2 UpdatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ServiceMethodFieldIndex.UpdatedBy); }
		}
		/// <summary>Creates a new ServiceMethodEntity.MinimumOrderAmount field instance</summary>
		public static EntityField2 MinimumOrderAmount 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ServiceMethodFieldIndex.MinimumOrderAmount); }
		}
		/// <summary>Creates a new ServiceMethodEntity.FreeDeliveryAmount field instance</summary>
		public static EntityField2 FreeDeliveryAmount 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ServiceMethodFieldIndex.FreeDeliveryAmount); }
		}
		/// <summary>Creates a new ServiceMethodEntity.ServiceChargeType field instance</summary>
		public static EntityField2 ServiceChargeType 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ServiceMethodFieldIndex.ServiceChargeType); }
		}
		/// <summary>Creates a new ServiceMethodEntity.MinimumAmountForFreeDelivery field instance</summary>
		public static EntityField2 MinimumAmountForFreeDelivery 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ServiceMethodFieldIndex.MinimumAmountForFreeDelivery); }
		}
		/// <summary>Creates a new ServiceMethodEntity.MinimumAmountForFreeServiceCharge field instance</summary>
		public static EntityField2 MinimumAmountForFreeServiceCharge 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ServiceMethodFieldIndex.MinimumAmountForFreeServiceCharge); }
		}
		/// <summary>Creates a new ServiceMethodEntity.OrderCompleteNotificationMethod field instance</summary>
		public static EntityField2 OrderCompleteNotificationMethod 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ServiceMethodFieldIndex.OrderCompleteNotificationMethod); }
		}
		/// <summary>Creates a new ServiceMethodEntity.OrderCompleteTextMessage field instance</summary>
		public static EntityField2 OrderCompleteTextMessage 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ServiceMethodFieldIndex.OrderCompleteTextMessage); }
		}
		/// <summary>Creates a new ServiceMethodEntity.OrderCompleteMailSubject field instance</summary>
		public static EntityField2 OrderCompleteMailSubject 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ServiceMethodFieldIndex.OrderCompleteMailSubject); }
		}
		/// <summary>Creates a new ServiceMethodEntity.OrderCompleteMailMessage field instance</summary>
		public static EntityField2 OrderCompleteMailMessage 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ServiceMethodFieldIndex.OrderCompleteMailMessage); }
		}
		/// <summary>Creates a new ServiceMethodEntity.MaxCovers field instance</summary>
		public static EntityField2 MaxCovers 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ServiceMethodFieldIndex.MaxCovers); }
		}
		/// <summary>Creates a new ServiceMethodEntity.CoversType field instance</summary>
		public static EntityField2 CoversType 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ServiceMethodFieldIndex.CoversType); }
		}
	}

	/// <summary>Field Creation Class for entity ServiceMethodDeliverypointgroupEntity</summary>
	public partial class ServiceMethodDeliverypointgroupFields
	{
		/// <summary>Creates a new ServiceMethodDeliverypointgroupEntity.ServiceMethodId field instance</summary>
		public static EntityField2 ServiceMethodId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ServiceMethodDeliverypointgroupFieldIndex.ServiceMethodId); }
		}
		/// <summary>Creates a new ServiceMethodDeliverypointgroupEntity.DeliverypointgroupId field instance</summary>
		public static EntityField2 DeliverypointgroupId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ServiceMethodDeliverypointgroupFieldIndex.DeliverypointgroupId); }
		}
		/// <summary>Creates a new ServiceMethodDeliverypointgroupEntity.ParentCompanyId field instance</summary>
		public static EntityField2 ParentCompanyId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ServiceMethodDeliverypointgroupFieldIndex.ParentCompanyId); }
		}
		/// <summary>Creates a new ServiceMethodDeliverypointgroupEntity.CreatedUTC field instance</summary>
		public static EntityField2 CreatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ServiceMethodDeliverypointgroupFieldIndex.CreatedUTC); }
		}
		/// <summary>Creates a new ServiceMethodDeliverypointgroupEntity.CreatedBy field instance</summary>
		public static EntityField2 CreatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ServiceMethodDeliverypointgroupFieldIndex.CreatedBy); }
		}
		/// <summary>Creates a new ServiceMethodDeliverypointgroupEntity.UpdatedUTC field instance</summary>
		public static EntityField2 UpdatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ServiceMethodDeliverypointgroupFieldIndex.UpdatedUTC); }
		}
		/// <summary>Creates a new ServiceMethodDeliverypointgroupEntity.UpdatedBy field instance</summary>
		public static EntityField2 UpdatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ServiceMethodDeliverypointgroupFieldIndex.UpdatedBy); }
		}
	}

	/// <summary>Field Creation Class for entity SupportagentEntity</summary>
	public partial class SupportagentFields
	{
		/// <summary>Creates a new SupportagentEntity.SupportagentId field instance</summary>
		public static EntityField2 SupportagentId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(SupportagentFieldIndex.SupportagentId); }
		}
		/// <summary>Creates a new SupportagentEntity.Firstname field instance</summary>
		public static EntityField2 Firstname 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(SupportagentFieldIndex.Firstname); }
		}
		/// <summary>Creates a new SupportagentEntity.Lastname field instance</summary>
		public static EntityField2 Lastname 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(SupportagentFieldIndex.Lastname); }
		}
		/// <summary>Creates a new SupportagentEntity.LastnamePrefix field instance</summary>
		public static EntityField2 LastnamePrefix 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(SupportagentFieldIndex.LastnamePrefix); }
		}
		/// <summary>Creates a new SupportagentEntity.Email field instance</summary>
		public static EntityField2 Email 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(SupportagentFieldIndex.Email); }
		}
		/// <summary>Creates a new SupportagentEntity.Phonenumber field instance</summary>
		public static EntityField2 Phonenumber 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(SupportagentFieldIndex.Phonenumber); }
		}
		/// <summary>Creates a new SupportagentEntity.SlackUsername field instance</summary>
		public static EntityField2 SlackUsername 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(SupportagentFieldIndex.SlackUsername); }
		}
		/// <summary>Creates a new SupportagentEntity.Active field instance</summary>
		public static EntityField2 Active 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(SupportagentFieldIndex.Active); }
		}
		/// <summary>Creates a new SupportagentEntity.CreatedUTC field instance</summary>
		public static EntityField2 CreatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(SupportagentFieldIndex.CreatedUTC); }
		}
		/// <summary>Creates a new SupportagentEntity.CreatedBy field instance</summary>
		public static EntityField2 CreatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(SupportagentFieldIndex.CreatedBy); }
		}
		/// <summary>Creates a new SupportagentEntity.UpdatedUTC field instance</summary>
		public static EntityField2 UpdatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(SupportagentFieldIndex.UpdatedUTC); }
		}
		/// <summary>Creates a new SupportagentEntity.UpdatedBy field instance</summary>
		public static EntityField2 UpdatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(SupportagentFieldIndex.UpdatedBy); }
		}
	}

	/// <summary>Field Creation Class for entity SupportpoolEntity</summary>
	public partial class SupportpoolFields
	{
		/// <summary>Creates a new SupportpoolEntity.SupportpoolId field instance</summary>
		public static EntityField2 SupportpoolId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(SupportpoolFieldIndex.SupportpoolId); }
		}
		/// <summary>Creates a new SupportpoolEntity.Name field instance</summary>
		public static EntityField2 Name 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(SupportpoolFieldIndex.Name); }
		}
		/// <summary>Creates a new SupportpoolEntity.Phonenumber field instance</summary>
		public static EntityField2 Phonenumber 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(SupportpoolFieldIndex.Phonenumber); }
		}
		/// <summary>Creates a new SupportpoolEntity.Email field instance</summary>
		public static EntityField2 Email 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(SupportpoolFieldIndex.Email); }
		}
		/// <summary>Creates a new SupportpoolEntity.SystemSupportPool field instance</summary>
		public static EntityField2 SystemSupportPool 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(SupportpoolFieldIndex.SystemSupportPool); }
		}
		/// <summary>Creates a new SupportpoolEntity.ContentSupportPool field instance</summary>
		public static EntityField2 ContentSupportPool 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(SupportpoolFieldIndex.ContentSupportPool); }
		}
		/// <summary>Creates a new SupportpoolEntity.Created field instance</summary>
		public static EntityField2 Created 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(SupportpoolFieldIndex.Created); }
		}
		/// <summary>Creates a new SupportpoolEntity.CreatedBy field instance</summary>
		public static EntityField2 CreatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(SupportpoolFieldIndex.CreatedBy); }
		}
		/// <summary>Creates a new SupportpoolEntity.Updated field instance</summary>
		public static EntityField2 Updated 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(SupportpoolFieldIndex.Updated); }
		}
		/// <summary>Creates a new SupportpoolEntity.UpdatedBy field instance</summary>
		public static EntityField2 UpdatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(SupportpoolFieldIndex.UpdatedBy); }
		}
		/// <summary>Creates a new SupportpoolEntity.CreatedUTC field instance</summary>
		public static EntityField2 CreatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(SupportpoolFieldIndex.CreatedUTC); }
		}
		/// <summary>Creates a new SupportpoolEntity.UpdatedUTC field instance</summary>
		public static EntityField2 UpdatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(SupportpoolFieldIndex.UpdatedUTC); }
		}
	}

	/// <summary>Field Creation Class for entity SupportpoolNotificationRecipientEntity</summary>
	public partial class SupportpoolNotificationRecipientFields
	{
		/// <summary>Creates a new SupportpoolNotificationRecipientEntity.SupportpoolNotificationRecipientId field instance</summary>
		public static EntityField2 SupportpoolNotificationRecipientId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(SupportpoolNotificationRecipientFieldIndex.SupportpoolNotificationRecipientId); }
		}
		/// <summary>Creates a new SupportpoolNotificationRecipientEntity.SupportpoolId field instance</summary>
		public static EntityField2 SupportpoolId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(SupportpoolNotificationRecipientFieldIndex.SupportpoolId); }
		}
		/// <summary>Creates a new SupportpoolNotificationRecipientEntity.Name field instance</summary>
		public static EntityField2 Name 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(SupportpoolNotificationRecipientFieldIndex.Name); }
		}
		/// <summary>Creates a new SupportpoolNotificationRecipientEntity.Phonenumber field instance</summary>
		public static EntityField2 Phonenumber 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(SupportpoolNotificationRecipientFieldIndex.Phonenumber); }
		}
		/// <summary>Creates a new SupportpoolNotificationRecipientEntity.Email field instance</summary>
		public static EntityField2 Email 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(SupportpoolNotificationRecipientFieldIndex.Email); }
		}
		/// <summary>Creates a new SupportpoolNotificationRecipientEntity.NotifyOfflineTerminals field instance</summary>
		public static EntityField2 NotifyOfflineTerminals 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(SupportpoolNotificationRecipientFieldIndex.NotifyOfflineTerminals); }
		}
		/// <summary>Creates a new SupportpoolNotificationRecipientEntity.NotifyOfflineClients field instance</summary>
		public static EntityField2 NotifyOfflineClients 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(SupportpoolNotificationRecipientFieldIndex.NotifyOfflineClients); }
		}
		/// <summary>Creates a new SupportpoolNotificationRecipientEntity.NotifyUnprocessableOrders field instance</summary>
		public static EntityField2 NotifyUnprocessableOrders 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(SupportpoolNotificationRecipientFieldIndex.NotifyUnprocessableOrders); }
		}
		/// <summary>Creates a new SupportpoolNotificationRecipientEntity.NotifyExpiredSteps field instance</summary>
		public static EntityField2 NotifyExpiredSteps 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(SupportpoolNotificationRecipientFieldIndex.NotifyExpiredSteps); }
		}
		/// <summary>Creates a new SupportpoolNotificationRecipientEntity.NotifyTooMuchOfflineClientsJump field instance</summary>
		public static EntityField2 NotifyTooMuchOfflineClientsJump 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(SupportpoolNotificationRecipientFieldIndex.NotifyTooMuchOfflineClientsJump); }
		}
		/// <summary>Creates a new SupportpoolNotificationRecipientEntity.NotifyBouncedEmails field instance</summary>
		public static EntityField2 NotifyBouncedEmails 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(SupportpoolNotificationRecipientFieldIndex.NotifyBouncedEmails); }
		}
		/// <summary>Creates a new SupportpoolNotificationRecipientEntity.CreatedUTC field instance</summary>
		public static EntityField2 CreatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(SupportpoolNotificationRecipientFieldIndex.CreatedUTC); }
		}
		/// <summary>Creates a new SupportpoolNotificationRecipientEntity.CreatedBy field instance</summary>
		public static EntityField2 CreatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(SupportpoolNotificationRecipientFieldIndex.CreatedBy); }
		}
		/// <summary>Creates a new SupportpoolNotificationRecipientEntity.UpdatedUTC field instance</summary>
		public static EntityField2 UpdatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(SupportpoolNotificationRecipientFieldIndex.UpdatedUTC); }
		}
		/// <summary>Creates a new SupportpoolNotificationRecipientEntity.UpdatedBy field instance</summary>
		public static EntityField2 UpdatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(SupportpoolNotificationRecipientFieldIndex.UpdatedBy); }
		}
	}

	/// <summary>Field Creation Class for entity SupportpoolSupportagentEntity</summary>
	public partial class SupportpoolSupportagentFields
	{
		/// <summary>Creates a new SupportpoolSupportagentEntity.SupportpoolSupportagentId field instance</summary>
		public static EntityField2 SupportpoolSupportagentId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(SupportpoolSupportagentFieldIndex.SupportpoolSupportagentId); }
		}
		/// <summary>Creates a new SupportpoolSupportagentEntity.SupportpoolId field instance</summary>
		public static EntityField2 SupportpoolId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(SupportpoolSupportagentFieldIndex.SupportpoolId); }
		}
		/// <summary>Creates a new SupportpoolSupportagentEntity.SupportagentId field instance</summary>
		public static EntityField2 SupportagentId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(SupportpoolSupportagentFieldIndex.SupportagentId); }
		}
		/// <summary>Creates a new SupportpoolSupportagentEntity.OnSupport field instance</summary>
		public static EntityField2 OnSupport 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(SupportpoolSupportagentFieldIndex.OnSupport); }
		}
		/// <summary>Creates a new SupportpoolSupportagentEntity.NotifyOfflineTerminals field instance</summary>
		public static EntityField2 NotifyOfflineTerminals 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(SupportpoolSupportagentFieldIndex.NotifyOfflineTerminals); }
		}
		/// <summary>Creates a new SupportpoolSupportagentEntity.NotifyOfflineClients field instance</summary>
		public static EntityField2 NotifyOfflineClients 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(SupportpoolSupportagentFieldIndex.NotifyOfflineClients); }
		}
		/// <summary>Creates a new SupportpoolSupportagentEntity.NotifyUnprocessableOrders field instance</summary>
		public static EntityField2 NotifyUnprocessableOrders 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(SupportpoolSupportagentFieldIndex.NotifyUnprocessableOrders); }
		}
		/// <summary>Creates a new SupportpoolSupportagentEntity.NotifyExpiredSteps field instance</summary>
		public static EntityField2 NotifyExpiredSteps 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(SupportpoolSupportagentFieldIndex.NotifyExpiredSteps); }
		}
		/// <summary>Creates a new SupportpoolSupportagentEntity.NotifyTooMuchOfflineClientsJump field instance</summary>
		public static EntityField2 NotifyTooMuchOfflineClientsJump 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(SupportpoolSupportagentFieldIndex.NotifyTooMuchOfflineClientsJump); }
		}
		/// <summary>Creates a new SupportpoolSupportagentEntity.NotifyBouncedEmails field instance</summary>
		public static EntityField2 NotifyBouncedEmails 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(SupportpoolSupportagentFieldIndex.NotifyBouncedEmails); }
		}
		/// <summary>Creates a new SupportpoolSupportagentEntity.CreatedUTC field instance</summary>
		public static EntityField2 CreatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(SupportpoolSupportagentFieldIndex.CreatedUTC); }
		}
		/// <summary>Creates a new SupportpoolSupportagentEntity.CreatedBy field instance</summary>
		public static EntityField2 CreatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(SupportpoolSupportagentFieldIndex.CreatedBy); }
		}
		/// <summary>Creates a new SupportpoolSupportagentEntity.UpdatedUTC field instance</summary>
		public static EntityField2 UpdatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(SupportpoolSupportagentFieldIndex.UpdatedUTC); }
		}
		/// <summary>Creates a new SupportpoolSupportagentEntity.UpdatedBy field instance</summary>
		public static EntityField2 UpdatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(SupportpoolSupportagentFieldIndex.UpdatedBy); }
		}
	}

	/// <summary>Field Creation Class for entity TagEntity</summary>
	public partial class TagFields
	{
		/// <summary>Creates a new TagEntity.TagId field instance</summary>
		public static EntityField2 TagId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(TagFieldIndex.TagId); }
		}
		/// <summary>Creates a new TagEntity.CompanyId field instance</summary>
		public static EntityField2 CompanyId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(TagFieldIndex.CompanyId); }
		}
		/// <summary>Creates a new TagEntity.Name field instance</summary>
		public static EntityField2 Name 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(TagFieldIndex.Name); }
		}
		/// <summary>Creates a new TagEntity.CreatedUTC field instance</summary>
		public static EntityField2 CreatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(TagFieldIndex.CreatedUTC); }
		}
		/// <summary>Creates a new TagEntity.UpdatedUTC field instance</summary>
		public static EntityField2 UpdatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(TagFieldIndex.UpdatedUTC); }
		}
	}

	/// <summary>Field Creation Class for entity TaxTariffEntity</summary>
	public partial class TaxTariffFields
	{
		/// <summary>Creates a new TaxTariffEntity.TaxTariffId field instance</summary>
		public static EntityField2 TaxTariffId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(TaxTariffFieldIndex.TaxTariffId); }
		}
		/// <summary>Creates a new TaxTariffEntity.CompanyId field instance</summary>
		public static EntityField2 CompanyId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(TaxTariffFieldIndex.CompanyId); }
		}
		/// <summary>Creates a new TaxTariffEntity.Name field instance</summary>
		public static EntityField2 Name 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(TaxTariffFieldIndex.Name); }
		}
		/// <summary>Creates a new TaxTariffEntity.Percentage field instance</summary>
		public static EntityField2 Percentage 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(TaxTariffFieldIndex.Percentage); }
		}
		/// <summary>Creates a new TaxTariffEntity.CreatedBy field instance</summary>
		public static EntityField2 CreatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(TaxTariffFieldIndex.CreatedBy); }
		}
		/// <summary>Creates a new TaxTariffEntity.UpdatedBy field instance</summary>
		public static EntityField2 UpdatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(TaxTariffFieldIndex.UpdatedBy); }
		}
		/// <summary>Creates a new TaxTariffEntity.CreatedUTC field instance</summary>
		public static EntityField2 CreatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(TaxTariffFieldIndex.CreatedUTC); }
		}
		/// <summary>Creates a new TaxTariffEntity.UpdatedUTC field instance</summary>
		public static EntityField2 UpdatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(TaxTariffFieldIndex.UpdatedUTC); }
		}
	}

	/// <summary>Field Creation Class for entity TerminalEntity</summary>
	public partial class TerminalFields
	{
		/// <summary>Creates a new TerminalEntity.TerminalId field instance</summary>
		public static EntityField2 TerminalId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(TerminalFieldIndex.TerminalId); }
		}
		/// <summary>Creates a new TerminalEntity.CompanyId field instance</summary>
		public static EntityField2 CompanyId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(TerminalFieldIndex.CompanyId); }
		}
		/// <summary>Creates a new TerminalEntity.UIModeId field instance</summary>
		public static EntityField2 UIModeId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(TerminalFieldIndex.UIModeId); }
		}
		/// <summary>Creates a new TerminalEntity.Type field instance</summary>
		public static EntityField2 Type 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(TerminalFieldIndex.Type); }
		}
		/// <summary>Creates a new TerminalEntity.OnsiteServerDeliverypointgroupId field instance</summary>
		public static EntityField2 OnsiteServerDeliverypointgroupId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(TerminalFieldIndex.OnsiteServerDeliverypointgroupId); }
		}
		/// <summary>Creates a new TerminalEntity.Name field instance</summary>
		public static EntityField2 Name 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(TerminalFieldIndex.Name); }
		}
		/// <summary>Creates a new TerminalEntity.LastStatus field instance</summary>
		public static EntityField2 LastStatus 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(TerminalFieldIndex.LastStatus); }
		}
		/// <summary>Creates a new TerminalEntity.LastStatusText field instance</summary>
		public static EntityField2 LastStatusText 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(TerminalFieldIndex.LastStatusText); }
		}
		/// <summary>Creates a new TerminalEntity.LastStatusMessage field instance</summary>
		public static EntityField2 LastStatusMessage 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(TerminalFieldIndex.LastStatusMessage); }
		}
		/// <summary>Creates a new TerminalEntity.OutOfChargeNotificationsSent field instance</summary>
		public static EntityField2 OutOfChargeNotificationsSent 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(TerminalFieldIndex.OutOfChargeNotificationsSent); }
		}
		/// <summary>Creates a new TerminalEntity.RequestInterval field instance</summary>
		public static EntityField2 RequestInterval 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(TerminalFieldIndex.RequestInterval); }
		}
		/// <summary>Creates a new TerminalEntity.DiagnoseInterval field instance</summary>
		public static EntityField2 DiagnoseInterval 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(TerminalFieldIndex.DiagnoseInterval); }
		}
		/// <summary>Creates a new TerminalEntity.PrintingEnabled field instance</summary>
		public static EntityField2 PrintingEnabled 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(TerminalFieldIndex.PrintingEnabled); }
		}
		/// <summary>Creates a new TerminalEntity.PrinterName field instance</summary>
		public static EntityField2 PrinterName 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(TerminalFieldIndex.PrinterName); }
		}
		/// <summary>Creates a new TerminalEntity.RecordAllRequestsToRequestLog field instance</summary>
		public static EntityField2 RecordAllRequestsToRequestLog 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(TerminalFieldIndex.RecordAllRequestsToRequestLog); }
		}
		/// <summary>Creates a new TerminalEntity.TeamviewerId field instance</summary>
		public static EntityField2 TeamviewerId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(TerminalFieldIndex.TeamviewerId); }
		}
		/// <summary>Creates a new TerminalEntity.MaxDaysLogHistory field instance</summary>
		public static EntityField2 MaxDaysLogHistory 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(TerminalFieldIndex.MaxDaysLogHistory); }
		}
		/// <summary>Creates a new TerminalEntity.POSConnectorType field instance</summary>
		public static EntityField2 POSConnectorType 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(TerminalFieldIndex.POSConnectorType); }
		}
		/// <summary>Creates a new TerminalEntity.PMSConnectorType field instance</summary>
		public static EntityField2 PMSConnectorType 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(TerminalFieldIndex.PMSConnectorType); }
		}
		/// <summary>Creates a new TerminalEntity.SynchronizeWithPOSOnStart field instance</summary>
		public static EntityField2 SynchronizeWithPOSOnStart 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(TerminalFieldIndex.SynchronizeWithPOSOnStart); }
		}
		/// <summary>Creates a new TerminalEntity.MaxStatusReports field instance</summary>
		public static EntityField2 MaxStatusReports 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(TerminalFieldIndex.MaxStatusReports); }
		}
		/// <summary>Creates a new TerminalEntity.Active field instance</summary>
		public static EntityField2 Active 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(TerminalFieldIndex.Active); }
		}
		/// <summary>Creates a new TerminalEntity.LanguageCode field instance</summary>
		public static EntityField2 LanguageCode 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(TerminalFieldIndex.LanguageCode); }
		}
		/// <summary>Creates a new TerminalEntity.UnlockDeliverypointProductId field instance</summary>
		public static EntityField2 UnlockDeliverypointProductId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(TerminalFieldIndex.UnlockDeliverypointProductId); }
		}
		/// <summary>Creates a new TerminalEntity.BatteryLowProductId field instance</summary>
		public static EntityField2 BatteryLowProductId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(TerminalFieldIndex.BatteryLowProductId); }
		}
		/// <summary>Creates a new TerminalEntity.ClientDisconnectedProductId field instance</summary>
		public static EntityField2 ClientDisconnectedProductId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(TerminalFieldIndex.ClientDisconnectedProductId); }
		}
		/// <summary>Creates a new TerminalEntity.OrderFailedProductId field instance</summary>
		public static EntityField2 OrderFailedProductId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(TerminalFieldIndex.OrderFailedProductId); }
		}
		/// <summary>Creates a new TerminalEntity.SystemMessagesDeliverypointId field instance</summary>
		public static EntityField2 SystemMessagesDeliverypointId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(TerminalFieldIndex.SystemMessagesDeliverypointId); }
		}
		/// <summary>Creates a new TerminalEntity.AltSystemMessagesDeliverypointId field instance</summary>
		public static EntityField2 AltSystemMessagesDeliverypointId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(TerminalFieldIndex.AltSystemMessagesDeliverypointId); }
		}
		/// <summary>Creates a new TerminalEntity.IcrtouchprintermappingId field instance</summary>
		public static EntityField2 IcrtouchprintermappingId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(TerminalFieldIndex.IcrtouchprintermappingId); }
		}
		/// <summary>Creates a new TerminalEntity.OperationMode field instance</summary>
		public static EntityField2 OperationMode 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(TerminalFieldIndex.OperationMode); }
		}
		/// <summary>Creates a new TerminalEntity.OperationState field instance</summary>
		public static EntityField2 OperationState 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(TerminalFieldIndex.OperationState); }
		}
		/// <summary>Creates a new TerminalEntity.HandlingMethod field instance</summary>
		public static EntityField2 HandlingMethod 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(TerminalFieldIndex.HandlingMethod); }
		}
		/// <summary>Creates a new TerminalEntity.NotificationForNewOrder field instance</summary>
		public static EntityField2 NotificationForNewOrder 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(TerminalFieldIndex.NotificationForNewOrder); }
		}
		/// <summary>Creates a new TerminalEntity.NotificationForOverdueOrder field instance</summary>
		public static EntityField2 NotificationForOverdueOrder 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(TerminalFieldIndex.NotificationForOverdueOrder); }
		}
		/// <summary>Creates a new TerminalEntity.DeliverypointCaption field instance</summary>
		public static EntityField2 DeliverypointCaption 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(TerminalFieldIndex.DeliverypointCaption); }
		}
		/// <summary>Creates a new TerminalEntity.DeliverypointgroupId field instance</summary>
		public static EntityField2 DeliverypointgroupId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(TerminalFieldIndex.DeliverypointgroupId); }
		}
		/// <summary>Creates a new TerminalEntity.ForwardToTerminalId field instance</summary>
		public static EntityField2 ForwardToTerminalId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(TerminalFieldIndex.ForwardToTerminalId); }
		}
		/// <summary>Creates a new TerminalEntity.PosValue1 field instance</summary>
		public static EntityField2 PosValue1 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(TerminalFieldIndex.PosValue1); }
		}
		/// <summary>Creates a new TerminalEntity.PosValue2 field instance</summary>
		public static EntityField2 PosValue2 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(TerminalFieldIndex.PosValue2); }
		}
		/// <summary>Creates a new TerminalEntity.PosValue3 field instance</summary>
		public static EntityField2 PosValue3 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(TerminalFieldIndex.PosValue3); }
		}
		/// <summary>Creates a new TerminalEntity.PosValue4 field instance</summary>
		public static EntityField2 PosValue4 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(TerminalFieldIndex.PosValue4); }
		}
		/// <summary>Creates a new TerminalEntity.PosValue5 field instance</summary>
		public static EntityField2 PosValue5 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(TerminalFieldIndex.PosValue5); }
		}
		/// <summary>Creates a new TerminalEntity.PosValue6 field instance</summary>
		public static EntityField2 PosValue6 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(TerminalFieldIndex.PosValue6); }
		}
		/// <summary>Creates a new TerminalEntity.PosValue7 field instance</summary>
		public static EntityField2 PosValue7 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(TerminalFieldIndex.PosValue7); }
		}
		/// <summary>Creates a new TerminalEntity.PosValue8 field instance</summary>
		public static EntityField2 PosValue8 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(TerminalFieldIndex.PosValue8); }
		}
		/// <summary>Creates a new TerminalEntity.PosValue9 field instance</summary>
		public static EntityField2 PosValue9 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(TerminalFieldIndex.PosValue9); }
		}
		/// <summary>Creates a new TerminalEntity.PosValue10 field instance</summary>
		public static EntityField2 PosValue10 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(TerminalFieldIndex.PosValue10); }
		}
		/// <summary>Creates a new TerminalEntity.PosValue11 field instance</summary>
		public static EntityField2 PosValue11 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(TerminalFieldIndex.PosValue11); }
		}
		/// <summary>Creates a new TerminalEntity.PosValue12 field instance</summary>
		public static EntityField2 PosValue12 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(TerminalFieldIndex.PosValue12); }
		}
		/// <summary>Creates a new TerminalEntity.PosValue13 field instance</summary>
		public static EntityField2 PosValue13 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(TerminalFieldIndex.PosValue13); }
		}
		/// <summary>Creates a new TerminalEntity.PosValue14 field instance</summary>
		public static EntityField2 PosValue14 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(TerminalFieldIndex.PosValue14); }
		}
		/// <summary>Creates a new TerminalEntity.PosValue15 field instance</summary>
		public static EntityField2 PosValue15 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(TerminalFieldIndex.PosValue15); }
		}
		/// <summary>Creates a new TerminalEntity.PosValue16 field instance</summary>
		public static EntityField2 PosValue16 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(TerminalFieldIndex.PosValue16); }
		}
		/// <summary>Creates a new TerminalEntity.PosValue17 field instance</summary>
		public static EntityField2 PosValue17 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(TerminalFieldIndex.PosValue17); }
		}
		/// <summary>Creates a new TerminalEntity.PosValue18 field instance</summary>
		public static EntityField2 PosValue18 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(TerminalFieldIndex.PosValue18); }
		}
		/// <summary>Creates a new TerminalEntity.PosValue19 field instance</summary>
		public static EntityField2 PosValue19 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(TerminalFieldIndex.PosValue19); }
		}
		/// <summary>Creates a new TerminalEntity.PosValue20 field instance</summary>
		public static EntityField2 PosValue20 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(TerminalFieldIndex.PosValue20); }
		}
		/// <summary>Creates a new TerminalEntity.DeviceId field instance</summary>
		public static EntityField2 DeviceId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(TerminalFieldIndex.DeviceId); }
		}
		/// <summary>Creates a new TerminalEntity.AutomaticSignOnUserId field instance</summary>
		public static EntityField2 AutomaticSignOnUserId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(TerminalFieldIndex.AutomaticSignOnUserId); }
		}
		/// <summary>Creates a new TerminalEntity.SurveyResultNotifications field instance</summary>
		public static EntityField2 SurveyResultNotifications 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(TerminalFieldIndex.SurveyResultNotifications); }
		}
		/// <summary>Creates a new TerminalEntity.LastCommunicationMethod field instance</summary>
		public static EntityField2 LastCommunicationMethod 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(TerminalFieldIndex.LastCommunicationMethod); }
		}
		/// <summary>Creates a new TerminalEntity.LastCloudEnvironment field instance</summary>
		public static EntityField2 LastCloudEnvironment 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(TerminalFieldIndex.LastCloudEnvironment); }
		}
		/// <summary>Creates a new TerminalEntity.LastStateOnline field instance</summary>
		public static EntityField2 LastStateOnline 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(TerminalFieldIndex.LastStateOnline); }
		}
		/// <summary>Creates a new TerminalEntity.LastStateOperationMode field instance</summary>
		public static EntityField2 LastStateOperationMode 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(TerminalFieldIndex.LastStateOperationMode); }
		}
		/// <summary>Creates a new TerminalEntity.UseMonitoring field instance</summary>
		public static EntityField2 UseMonitoring 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(TerminalFieldIndex.UseMonitoring); }
		}
		/// <summary>Creates a new TerminalEntity.MasterTab field instance</summary>
		public static EntityField2 MasterTab 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(TerminalFieldIndex.MasterTab); }
		}
		/// <summary>Creates a new TerminalEntity.ResetTimeout field instance</summary>
		public static EntityField2 ResetTimeout 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(TerminalFieldIndex.ResetTimeout); }
		}
		/// <summary>Creates a new TerminalEntity.PrinterConnected field instance</summary>
		public static EntityField2 PrinterConnected 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(TerminalFieldIndex.PrinterConnected); }
		}
		/// <summary>Creates a new TerminalEntity.OfflineNotificationEnabled field instance</summary>
		public static EntityField2 OfflineNotificationEnabled 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(TerminalFieldIndex.OfflineNotificationEnabled); }
		}
		/// <summary>Creates a new TerminalEntity.LoadedSuccessfully field instance</summary>
		public static EntityField2 LoadedSuccessfully 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(TerminalFieldIndex.LoadedSuccessfully); }
		}
		/// <summary>Creates a new TerminalEntity.SynchronizeWithPMSOnStart field instance</summary>
		public static EntityField2 SynchronizeWithPMSOnStart 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(TerminalFieldIndex.SynchronizeWithPMSOnStart); }
		}
		/// <summary>Creates a new TerminalEntity.StatusUpdatedUTC field instance</summary>
		public static EntityField2 StatusUpdatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(TerminalFieldIndex.StatusUpdatedUTC); }
		}
		/// <summary>Creates a new TerminalEntity.LastSyncUTC field instance</summary>
		public static EntityField2 LastSyncUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(TerminalFieldIndex.LastSyncUTC); }
		}
		/// <summary>Creates a new TerminalEntity.PmsTerminalOfflineNotificationEnabled field instance</summary>
		public static EntityField2 PmsTerminalOfflineNotificationEnabled 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(TerminalFieldIndex.PmsTerminalOfflineNotificationEnabled); }
		}
		/// <summary>Creates a new TerminalEntity.CreatedBy field instance</summary>
		public static EntityField2 CreatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(TerminalFieldIndex.CreatedBy); }
		}
		/// <summary>Creates a new TerminalEntity.UpdatedBy field instance</summary>
		public static EntityField2 UpdatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(TerminalFieldIndex.UpdatedBy); }
		}
		/// <summary>Creates a new TerminalEntity.CreatedUTC field instance</summary>
		public static EntityField2 CreatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(TerminalFieldIndex.CreatedUTC); }
		}
		/// <summary>Creates a new TerminalEntity.UpdatedUTC field instance</summary>
		public static EntityField2 UpdatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(TerminalFieldIndex.UpdatedUTC); }
		}
		/// <summary>Creates a new TerminalEntity.OutOfChargeNotificationLastSentUTC field instance</summary>
		public static EntityField2 OutOfChargeNotificationLastSentUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(TerminalFieldIndex.OutOfChargeNotificationLastSentUTC); }
		}
		/// <summary>Creates a new TerminalEntity.UseHardKeyboard field instance</summary>
		public static EntityField2 UseHardKeyboard 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(TerminalFieldIndex.UseHardKeyboard); }
		}
		/// <summary>Creates a new TerminalEntity.MaxVolume field instance</summary>
		public static EntityField2 MaxVolume 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(TerminalFieldIndex.MaxVolume); }
		}
		/// <summary>Creates a new TerminalEntity.MaxProcessTime field instance</summary>
		public static EntityField2 MaxProcessTime 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(TerminalFieldIndex.MaxProcessTime); }
		}
	}

	/// <summary>Field Creation Class for entity ApplicationConfigurationEntity</summary>
	public partial class ApplicationConfigurationFields
	{
		/// <summary>Creates a new ApplicationConfigurationEntity.ApplicationConfigurationId field instance</summary>
		public static EntityField2 ApplicationConfigurationId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ApplicationConfigurationFieldIndex.ApplicationConfigurationId); }
		}
		/// <summary>Creates a new ApplicationConfigurationEntity.ExternalId field instance</summary>
		public static EntityField2 ExternalId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ApplicationConfigurationFieldIndex.ExternalId); }
		}
		/// <summary>Creates a new ApplicationConfigurationEntity.CompanyId field instance</summary>
		public static EntityField2 CompanyId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ApplicationConfigurationFieldIndex.CompanyId); }
		}
		/// <summary>Creates a new ApplicationConfigurationEntity.LandingPageId field instance</summary>
		public static EntityField2 LandingPageId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ApplicationConfigurationFieldIndex.LandingPageId); }
		}
		/// <summary>Creates a new ApplicationConfigurationEntity.Name field instance</summary>
		public static EntityField2 Name 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ApplicationConfigurationFieldIndex.Name); }
		}
		/// <summary>Creates a new ApplicationConfigurationEntity.ShortName field instance</summary>
		public static EntityField2 ShortName 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ApplicationConfigurationFieldIndex.ShortName); }
		}
		/// <summary>Creates a new ApplicationConfigurationEntity.Description field instance</summary>
		public static EntityField2 Description 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ApplicationConfigurationFieldIndex.Description); }
		}
		/// <summary>Creates a new ApplicationConfigurationEntity.CreatedUTC field instance</summary>
		public static EntityField2 CreatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ApplicationConfigurationFieldIndex.CreatedUTC); }
		}
		/// <summary>Creates a new ApplicationConfigurationEntity.CreatedBy field instance</summary>
		public static EntityField2 CreatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ApplicationConfigurationFieldIndex.CreatedBy); }
		}
		/// <summary>Creates a new ApplicationConfigurationEntity.Updated field instance</summary>
		public static EntityField2 Updated 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ApplicationConfigurationFieldIndex.Updated); }
		}
		/// <summary>Creates a new ApplicationConfigurationEntity.UpdatedBy field instance</summary>
		public static EntityField2 UpdatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ApplicationConfigurationFieldIndex.UpdatedBy); }
		}
		/// <summary>Creates a new ApplicationConfigurationEntity.UpdatedUTC field instance</summary>
		public static EntityField2 UpdatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ApplicationConfigurationFieldIndex.UpdatedUTC); }
		}
		/// <summary>Creates a new ApplicationConfigurationEntity.CultureCode field instance</summary>
		public static EntityField2 CultureCode 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ApplicationConfigurationFieldIndex.CultureCode); }
		}
		/// <summary>Creates a new ApplicationConfigurationEntity.AnalyticsEnvironment field instance</summary>
		public static EntityField2 AnalyticsEnvironment 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ApplicationConfigurationFieldIndex.AnalyticsEnvironment); }
		}
		/// <summary>Creates a new ApplicationConfigurationEntity.GoogleAnalyticsId field instance</summary>
		public static EntityField2 GoogleAnalyticsId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ApplicationConfigurationFieldIndex.GoogleAnalyticsId); }
		}
		/// <summary>Creates a new ApplicationConfigurationEntity.GoogleTagManagerId field instance</summary>
		public static EntityField2 GoogleTagManagerId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ApplicationConfigurationFieldIndex.GoogleTagManagerId); }
		}
		/// <summary>Creates a new ApplicationConfigurationEntity.EnableCookieWall field instance</summary>
		public static EntityField2 EnableCookieWall 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ApplicationConfigurationFieldIndex.EnableCookieWall); }
		}
		/// <summary>Creates a new ApplicationConfigurationEntity.ProductSwipingEnabled field instance</summary>
		public static EntityField2 ProductSwipingEnabled 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(ApplicationConfigurationFieldIndex.ProductSwipingEnabled); }
		}
	}

	/// <summary>Field Creation Class for entity FeatureFlagEntity</summary>
	public partial class FeatureFlagFields
	{
		/// <summary>Creates a new FeatureFlagEntity.FeatureFlagId field instance</summary>
		public static EntityField2 FeatureFlagId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(FeatureFlagFieldIndex.FeatureFlagId); }
		}
		/// <summary>Creates a new FeatureFlagEntity.ApplicationConfigurationId field instance</summary>
		public static EntityField2 ApplicationConfigurationId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(FeatureFlagFieldIndex.ApplicationConfigurationId); }
		}
		/// <summary>Creates a new FeatureFlagEntity.ApplessFeatureFlags field instance</summary>
		public static EntityField2 ApplessFeatureFlags 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(FeatureFlagFieldIndex.ApplessFeatureFlags); }
		}
		/// <summary>Creates a new FeatureFlagEntity.CreatedUTC field instance</summary>
		public static EntityField2 CreatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(FeatureFlagFieldIndex.CreatedUTC); }
		}
		/// <summary>Creates a new FeatureFlagEntity.CreatedBy field instance</summary>
		public static EntityField2 CreatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(FeatureFlagFieldIndex.CreatedBy); }
		}
		/// <summary>Creates a new FeatureFlagEntity.UpdatedUTC field instance</summary>
		public static EntityField2 UpdatedUTC 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(FeatureFlagFieldIndex.UpdatedUTC); }
		}
		/// <summary>Creates a new FeatureFlagEntity.UpdatedBy field instance</summary>
		public static EntityField2 UpdatedBy 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(FeatureFlagFieldIndex.UpdatedBy); }
		}
		/// <summary>Creates a new FeatureFlagEntity.CompanyId field instance</summary>
		public static EntityField2 CompanyId 
		{ 
			get { return ModelInfoProviderSingleton.GetInstance().CreateField2(FeatureFlagFieldIndex.CompanyId); }
		}
	}
	

}