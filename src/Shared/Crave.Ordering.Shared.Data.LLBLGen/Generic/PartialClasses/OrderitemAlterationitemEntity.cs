﻿using System;
using System.Xml.Serialization;
using Crave.Ordering.Shared.Data.LLBLGen.Extensions;
using Crave.Ordering.Shared.Extensions;

namespace Crave.Ordering.Shared.Data.LLBLGen.EntityClasses
{
    public partial class OrderitemAlterationitemEntity
    {
        /// <summary>
        /// Gets or sets a value to override the change protection on an order item.
        /// </summary>
        public bool OverrideChangeProtection { get; set; }

        /// <summary>
        /// Gets the quantity of the orderitem, returns 1 if the order level enabled property is set to True.
        /// </summary>
        [XmlIgnore]
        public int Quantity => this.OrderLevelEnabled ? 1 : this.Orderitem.Quantity;

        /// <summary>
        /// Gets the price as shown to the customer (with or without tax).
        /// </summary>
        [XmlIgnore]
        public decimal DisplayPrice => this.Orderitem.Order.DisplayPriceIncludingTaxes ? this.PriceInTax : this.PriceExTax;

        /// <summary>
        /// Gets the total price as shown to the customer (with or without tax).
        /// </summary>
        [XmlIgnore]
        public decimal TotalDisplayPrice => this.Orderitem.Order.DisplayPriceIncludingTaxes ? this.PriceTotalInTax : this.PriceTotalExTax;

        /// <summary>
        /// Gets the price as shown to the customer (with or without tax) formatted based on company culture.
        /// </summary>
        [XmlIgnore]
        public string DisplayPriceFormatted => this.DisplayPrice.Format(this.Orderitem.Order.CultureCode);

        /// <summary>
        /// Gets the total price as shown to the customer (with or without tax) formatted based on company culture.
        /// </summary>
        [XmlIgnore]
        public string TotalDisplayPriceFormatted => this.TotalDisplayPrice.Format(this.Orderitem.Order.CultureCode);

        /// <summary>
        /// Gets the time in company timezone.
        /// </summary>
        [XmlIgnore]
        public DateTime? TimeLocal => this.TimeUTC.HasValue ? TimeZoneInfo.ConvertTimeFromUtc(this.TimeUTC.Value, this.Orderitem.Order.GetTimeZone().TimeZoneInfo) : (DateTime?)null;
    }
}
