﻿using System;
using System.Xml.Serialization;

namespace Crave.Ordering.Shared.Data.LLBLGen.EntityClasses
{
    public partial class OrderRoutestephandlerEntity
    {
        /// <summary>
        /// Gets or sets the file (i.e. document) that is attached to an order.
        /// </summary>
        [XmlIgnore]
        public byte[] File { get; set; }

        /// <summary>
        /// Gets or sets whether the receipt should also be send to the seller.
        /// </summary>
        [XmlIgnore]
        public bool ToSeller => this.FieldValue1.Equals("1", StringComparison.InvariantCultureIgnoreCase);

        /// <summary>
        /// Gets or sets whether the receipt should be send through email.
        /// </summary>
        [XmlIgnore]
        public bool EmailEnabled => this.FieldValue2.Equals("1", StringComparison.InvariantCultureIgnoreCase);

        /// <summary>
        /// Gets or sets whether the receipt should be send through sms.
        /// </summary>
        [XmlIgnore]
        public bool SmsEnabled => this.FieldValue3.Equals("1", StringComparison.InvariantCultureIgnoreCase);
    }
}
