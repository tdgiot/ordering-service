﻿using System;
using System.IO;
using System.Xml.Serialization;

namespace Crave.Ordering.Shared.Data.LLBLGen.EntityClasses
{
    public partial class MediaRatioTypeMediaEntity
    {
        public enum FileNameType
        {
            Cdn,
            Format,
            DeletionWildcard
        }

        public const string FILENAME_FORMAT = "{0}-{1}w-{2}h-{3}l-{4}t-";

        public const string FILENAME_FORMAT_TICKS = "[ticks]"; // MUST BE LOWER CASE!

        public string GetFileName(FileNameType type)
        {
            string fileName = string.Empty;

            switch (type)
            {
                case FileNameType.Cdn:
                    fileName = this.GetFileNameForCdn();
                    break;
                case FileNameType.Format:
                    fileName = this.GetFileNameForFormat();
                    break;
                case FileNameType.DeletionWildcard:
                    fileName = this.GetFileNameForDeletion();
                    break;
            }

            return fileName.ToLower();
        }

        private string GetFileNameForCdn()
        {
            string fileName = string.Empty;

            // Only return the file name if the file was distributed.
            if (this.LastDistributedVersionTicks.HasValue)
            {
                fileName = this.GetFileNameForFormat();
                fileName = fileName.Replace(MediaRatioTypeMediaEntity.FILENAME_FORMAT_TICKS, this.LastDistributedVersionTicks.ToString());
            }

            return fileName;
        }

        private string GetFileNameForFormat()
        {
            string fileName = string.Empty;

            // Add the id to the file name
            fileName += string.Format("{0}-", this.MediaRatioTypeMediaId);

            // Add the details of the media ratio type to the file name
            MediaRatioType mediaRatioType = this.MediaRatioType;
            if (mediaRatioType != null)
            {
                fileName += string.Format(MediaRatioTypeMediaEntity.FILENAME_FORMAT, mediaRatioType.MediaType.ToString(), this.Width, this.Height, this.Left, this.Top);
            }

            // Add the ticks format to the file name
            fileName += MediaRatioTypeMediaEntity.FILENAME_FORMAT_TICKS;

            // Add the extension if found
            if (this.Media.FilePathRelativeToMediaPath.IsNullOrWhiteSpace())
            {
                fileName += this.MediaRatioTypeMediaId;
            }
            else
            {
                fileName += Path.GetExtension(this.Media.FilePathRelativeToMediaPath);
            }

            return fileName;
        }

        private string GetFileNameForDeletion()
        {
            return string.Format("{0}-", this.MediaRatioTypeMediaId); // It's important that we have the dash, otherwise 1 would also delete 100 and 1000 and 1414, now just 1-
        }

        [XmlIgnore]
        private MediaRatioType mediaRatioType = null;

        [XmlIgnore]
        public MediaRatioType MediaRatioType
        {
            get
            {
                if (this.mediaRatioType == null && this.MediaType.HasValue && this.MediaType.Value != Crave.Enums.MediaType.NoMediaTypeSpecified)
                {
                    this.mediaRatioType = MediaRatioTypes.GetMediaRatioType(this.MediaType.Value);
                }

                return this.mediaRatioType;
            }
        }
    }
}
