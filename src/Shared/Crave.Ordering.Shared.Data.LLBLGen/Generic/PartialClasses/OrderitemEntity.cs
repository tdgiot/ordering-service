﻿using System;
using System.Linq;
using System.Xml.Serialization;
using Crave.Ordering.Shared.Extensions;

namespace Crave.Ordering.Shared.Data.LLBLGen.EntityClasses
{
    public partial class OrderitemEntity
    {
        /// <summary>
        /// Gets or sets a value to override the change protection on an order item.
        /// </summary>
        public bool OverrideChangeProtection { get; set; }

        public Guid TempInternalGuid { get; set; }

        /// <summary>
        /// Gets the sub total price (excluding tax).
        /// </summary>
        [XmlIgnore]
        public decimal SubTotal => this.PriceTotalExTax + this.OrderitemAlterationitemCollection.Sum(o => o.PriceTotalExTax);

        /// <summary>
        /// Gets the total price (including tax)
        /// </summary>
        [XmlIgnore]
        public decimal Total => this.PriceTotalInTax + this.OrderitemAlterationitemCollection.Sum(o => o.PriceTotalInTax);

        /// <summary>
        /// Gets the price as shown to the customer (with or without tax).
        /// </summary>
        [XmlIgnore]
        public decimal DisplayPrice => this.Order.DisplayPriceIncludingTaxes ? this.PriceInTax : this.PriceExTax;

        /// <summary>
        /// Gets the total price as shown to the customer (with or without tax) including alteration items.
        /// </summary>
        [XmlIgnore]
        public decimal TotalDisplayPrice => this.Order.DisplayPriceIncludingTaxes ? this.Total : this.SubTotal;

        /// <summary>
        /// Gets the total price as shown to the customer (with or without tax) excluding alteration items.
        /// </summary>
        [XmlIgnore]
        public decimal TotalDisplayPriceExcludingAlterationitems => this.Order.DisplayPriceIncludingTaxes ? this.PriceTotalInTax : this.PriceTotalExTax;

        /// <summary>
        /// Gets the price as shown to the customer (with or without tax) formatted based on company culture.
        /// </summary>
        [XmlIgnore]
        public string DisplayPriceFormatted => this.DisplayPrice.Format(this.Order.CultureCode);

        /// <summary>
        /// Gets the total price as shown to the customer (with or without tax) formatted based on company culture.
        /// </summary>
        [XmlIgnore]
        public string TotalDisplayPriceExcludingAlterationitemsFormatted => this.TotalDisplayPriceExcludingAlterationitems.Format(this.Order.CultureCode);

        /// <summary>
        /// Gets the tax total (including alteration items).
        /// </summary>
        [XmlIgnore]
        public decimal TaxTotalIncludingAlterationitems => this.TaxTotal + this.OrderitemAlterationitemCollection.Sum(o => o.TaxTotal);
    }
}
