﻿namespace Crave.Ordering.Shared.Data.LLBLGen.EntityClasses
{
    public partial class BusinesshourEntity
    {
        public int GetDay()
        {
            return int.Parse(this.DayOfWeekAndTime.Substring(0, 1));
        }

        public int GetTime()
        {
            return int.Parse(this.DayOfWeekAndTime.Substring(1, 4));
        }
    }
}
