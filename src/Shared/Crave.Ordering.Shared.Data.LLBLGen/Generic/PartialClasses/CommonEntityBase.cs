﻿using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Crave.Ordering.Shared.Data.LLBLGen.EntityClasses
{
    public partial class CommonEntityBase
    {
        public IDataAccessAdapter Adapter { get; set; }
    }
}
