﻿using System;
using System.Xml.Serialization;

namespace Crave.Ordering.Shared.Data.LLBLGen.EntityClasses
{
    public partial class MediaEntity
    {
        /// <summary>
        /// Gets the EntityName this MediaEntity is related to (eg. Product, Customer, etc.).
        /// </summary>
        [XmlIgnore]
        public string RelatedEntityName
        {
            get
            {
                // Loop through fields and find which Related Id is filled
                string relatedEntityName = null;
                for (int i = 0; i < (int)MediaFieldIndex.AmountOfFields; i++)
                {
                    if (this.Fields[i].Name.EndsWith("Id") &&
                        this.Fields[i].Name != "MediaId" &&
                        this.Fields[i].Name != "MediaTypeId" &&
                        this.Fields[i].Name != "AgnosticMediaId" &&
                        this.Fields[i].Name != "RelatedCompanyId" &&
                        !this.Fields[i].Name.StartsWith("Action") && 
                        this.Fields[i].CurrentValue != null && 
                        this.Fields[i].CurrentValue != DBNull.Value)
                    {
                        // We have an Id field which is not the MediaId
                        // We have a value in the ID field
                        relatedEntityName = this.Fields[i].Name.Replace("Id", string.Empty);
                        break;
                    }
                }

                return relatedEntityName;
            }
        }

        [XmlIgnore]
        public EntityType RelatedEntityType
        {
            get
            {
                return (this.RelatedEntityName + "Entity").ToEnum<EntityType>();
            }
        }

        [XmlIgnore]
        public int? RelatedEntityId
        {
            get
            {
                int? toReturn = null;
                if (this.RelatedEntityName != null)
                {
                    toReturn = (int)this.Fields[this.RelatedEntityName + "Id"].CurrentValue;
                }
                return toReturn;
            }

        }
    }
}
