﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using Crave.Enums;
using Crave.Ordering.Shared.Extensions;

namespace Crave.Ordering.Shared.Data.LLBLGen.EntityClasses
{
    public partial class OrderEntity
    {
        /// <summary>
        /// Gets or sets the file (i.e. document) that is attached to an order.
        /// </summary>
        [XmlIgnore]
        public byte[] File { get; set; }

        /// <summary>
        /// Gets or sets a value to override the change protection on an order item.
        /// </summary>
        public bool OverrideChangeProtection { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the order is validated.
        /// </summary>
        public bool SavedThroughUseCase { get; set; } = false;

        /// <summary>
        /// Gets or sets a value indicating whether to skip validation (eg, when saved via trigger)
        /// </summary>
        public bool SkipValidation { get; set; } = false;

        /// <summary>
        /// Gets the sub total price (excluding tax) formatted based on company culture.
        /// </summary>
        [XmlIgnore]
        public string SubTotalFormatted => this.SubTotal.Format(this.CultureCode);

        /// <summary>
        /// Gets the total price (including tax) formatted based on company culture.
        /// </summary>
        [XmlIgnore]
        public string TotalFormatted => !string.IsNullOrWhiteSpace(this.CultureCode) ? this.Total.Format(this.CultureCode) : this.Total.ToString("0.00");

        /// <summary>
        /// Gets the tax total price (including alteration items).
        /// </summary>
        [XmlIgnore]
        public decimal TaxTotalIncludingAlterationitems => this.OrderitemCollection.Sum(o => o.TaxTotalIncludingAlterationitems);

        /// <summary>
        /// Gets the tax total price (including alteration items) formatted based on company culture.
        /// </summary>
        [XmlIgnore]
        public string TaxTotalIncludingAlterationitemsFormatted => this.TaxTotalIncludingAlterationitems.Format(this.CultureCode);

        /// <summary>
        /// Gets whether the price shown the customer includes taxes.
        /// </summary>
        [XmlIgnore]
        public bool DisplayPriceIncludingTaxes => this.PricesIncludeTaxes;

        /// <summary>
        /// Gets the total price of the order items of type 'Product' as shown to the customer.
        /// </summary>
        [XmlIgnore]
        public decimal OrderitemTotalDisplayPrice => Math.Round(this.OrderitemCollection.Where(x => x.Type == OrderitemType.Product).Sum(o => o.TotalDisplayPrice), 2, MidpointRounding.AwayFromZero);

        /// <summary>
        /// Gets the tax tariffs of the order items in the order ordered by percentage.
        /// </summary>
        [XmlIgnore]
        public IEnumerable<int> TaxTariffs
        {
            get
            {
                return this.OrderitemCollection
                           .OrderByDescending(x => x.TaxPercentage)
                           .Where(x => x.TaxTariffId.HasValue)
                           .Select(x => x.TaxTariffId.Value)
                           .Distinct();
            }
        }

        public bool IsProcessedWithoutManualIntervention => this.Status == OrderStatus.Processed && this.ErrorCode != OrderErrorCode.ManuallyProcessed;
    }
}
