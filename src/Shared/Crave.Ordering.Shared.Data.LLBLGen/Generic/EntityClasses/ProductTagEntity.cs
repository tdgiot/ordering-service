﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 5.8
// Code is generated on: 
// Code is generated using templates: Crave.TemplateBindings
// Templates vendor: Crave Interactive
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.FactoryClasses;
using Crave.Ordering.Shared.Data.LLBLGen.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

// ReSharper disable All
namespace Crave.Ordering.Shared.Data.LLBLGen.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'ProductTag'.<br/><br/></summary>
	[Serializable]
	public partial class ProductTagEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END
	
	{
		private ProductEntity _product;
		private TagEntity _tag;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		private static ProductTagEntityStaticMetaData _staticMetaData = new ProductTagEntityStaticMetaData();
		private static ProductTagRelations _relationsFactory = new ProductTagRelations();		

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Product</summary>
			public static readonly string Product = "Product";
			/// <summary>Member name Tag</summary>
			public static readonly string Tag = "Tag";
		}

		/// <summary>Static meta-data storage for navigator related information</summary>
		protected class ProductTagEntityStaticMetaData : EntityStaticMetaDataBase
		{
			public ProductTagEntityStaticMetaData()
			{
				SetEntityCoreInfo("ProductTagEntity", InheritanceHierarchyType.None, false, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ProductTagEntity, typeof(ProductTagEntity), typeof(ProductTagEntityFactory), false);
				AddNavigatorMetaData<ProductTagEntity, ProductEntity>("Product", "ProductTagCollection", (a, b) => a._product = b, a => a._product, (a, b) => a.Product = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticProductTagRelations.ProductEntityUsingProductIdStatic, ()=>new ProductTagRelations().ProductEntityUsingProductId, null, new int[] { (int)ProductTagFieldIndex.ProductId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ProductEntity);
				AddNavigatorMetaData<ProductTagEntity, TagEntity>("Tag", "ProductTagCollection", (a, b) => a._tag = b, a => a._tag, (a, b) => a.Tag = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticProductTagRelations.TagEntityUsingTagIdStatic, ()=>new ProductTagRelations().TagEntityUsingTagId, null, new int[] { (int)ProductTagFieldIndex.TagId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.TagEntity);
			}
		}

		/// <summary>Static ctor</summary>
		static ProductTagEntity()
		{
		}

		/// <summary> CTor</summary>
		public ProductTagEntity()
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public ProductTagEntity(IEntityFields2 fields)
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this ProductTagEntity</param>
		public ProductTagEntity(IValidator validator)
		{
			InitClassEmpty(validator, null);
		}

		/// <summary> CTor</summary>
		/// <param name="productTagId">PK value for ProductTag which data should be fetched into this ProductTag object</param>
		public ProductTagEntity(System.Int32 productTagId) : this(productTagId, null)
		{
		}

		/// <summary> CTor</summary>
		/// <param name="productTagId">PK value for ProductTag which data should be fetched into this ProductTag object</param>
		/// <param name="validator">The custom validator object for this ProductTagEntity</param>
		public ProductTagEntity(System.Int32 productTagId, IValidator validator)
		{
			InitClassEmpty(validator, null);
			this.ProductTagId = productTagId;
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ProductTagEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Product' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoProduct() { return CreateRelationInfoForNavigator("Product"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Tag' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoTag() { return CreateRelationInfoForNavigator("Tag"); }
		
		/// <inheritdoc/>
		protected override EntityStaticMetaDataBase GetEntityStaticMetaData() {	return _staticMetaData; }

		/// <summary>Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		/// <summary>Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this ProductTagEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary>The relations object holding all relations of this entity with other entity classes.</summary>
		public static ProductTagRelations Relations { get { return _relationsFactory; } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Product' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathProductEntity { get { return _staticMetaData.GetPrefetchPathElement("Product", CommonEntityBase.CreateEntityCollection<ProductEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Tag' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathTagEntity { get { return _staticMetaData.GetPrefetchPathElement("Tag", CommonEntityBase.CreateEntityCollection<TagEntity>()); } }

		/// <summary>The ProductTagId property of the Entity ProductTag<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ProductTag"."ProductTagId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 ProductTagId
		{
			get { return (System.Int32)GetValue((int)ProductTagFieldIndex.ProductTagId, true); }
			set { SetValue((int)ProductTagFieldIndex.ProductTagId, value); }		}

		/// <summary>The ProductId property of the Entity ProductTag<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ProductTag"."ProductId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ProductId
		{
			get { return (System.Int32)GetValue((int)ProductTagFieldIndex.ProductId, true); }
			set	{ SetValue((int)ProductTagFieldIndex.ProductId, value); }
		}

		/// <summary>The TagId property of the Entity ProductTag<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ProductTag"."TagId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 TagId
		{
			get { return (System.Int32)GetValue((int)ProductTagFieldIndex.TagId, true); }
			set	{ SetValue((int)ProductTagFieldIndex.TagId, value); }
		}

		/// <summary>The ParentCompanyId property of the Entity ProductTag<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ProductTag"."ParentCompanyId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ParentCompanyId
		{
			get { return (Nullable<System.Int32>)GetValue((int)ProductTagFieldIndex.ParentCompanyId, false); }
			set	{ SetValue((int)ProductTagFieldIndex.ParentCompanyId, value); }
		}

		/// <summary>The CreatedUTC property of the Entity ProductTag<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ProductTag"."CreatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime CreatedUTC
		{
			get { return (System.DateTime)GetValue((int)ProductTagFieldIndex.CreatedUTC, true); }
			set	{ SetValue((int)ProductTagFieldIndex.CreatedUTC, value); }
		}

		/// <summary>The UpdatedUTC property of the Entity ProductTag<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ProductTag"."UpdatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ProductTagFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)ProductTagFieldIndex.UpdatedUTC, value); }
		}

		/// <summary>Gets / sets related entity of type 'ProductEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual ProductEntity Product
		{
			get { return _product; }
			set { SetSingleRelatedEntityNavigator(value, "Product"); }
		}

		/// <summary>Gets / sets related entity of type 'TagEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual TagEntity Tag
		{
			get { return _tag; }
			set { SetSingleRelatedEntityNavigator(value, "Tag"); }
		}


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen
{
	public enum ProductTagFieldIndex
	{
		///<summary>ProductTagId. </summary>
		ProductTagId,
		///<summary>ProductId. </summary>
		ProductId,
		///<summary>TagId. </summary>
		TagId,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: ProductTag. </summary>
	public partial class ProductTagRelations: RelationFactory
	{
		#region Custom Relation code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomRelationCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		


		/// <summary>Gets all relations and delete rules of the ProductTagEntity as a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects.</summary>
		/// <returns>a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects</returns>
		public virtual Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> GetRelationsAndDeleteRules()
		{
			Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> toReturn = new Dictionary<IEntityRelation, ReferentialConstraintDeleteRule>();
			return toReturn;
		}		
	
	

		/// <summary>Returns a new IEntityRelation object, between ProductTagEntity and ProductEntity over the m:1 relation they have, using the relation between the fields: ProductTag.ProductId - Product.ProductId</summary>
		public virtual IEntityRelation ProductEntityUsingProductId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Product", false, new[] { ProductFields.ProductId, ProductTagFields.ProductId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between ProductTagEntity and TagEntity over the m:1 relation they have, using the relation between the fields: ProductTag.TagId - Tag.TagId</summary>
		public virtual IEntityRelation TagEntityUsingTagId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Tag", false, new[] { TagFields.TagId, ProductTagFields.TagId }); }
		}

	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticProductTagRelations
	{
		internal static readonly IEntityRelation ProductEntityUsingProductIdStatic = new ProductTagRelations().ProductEntityUsingProductId;
		internal static readonly IEntityRelation TagEntityUsingTagIdStatic = new ProductTagRelations().TagEntityUsingTagId;

		/// <summary>CTor</summary>
		static StaticProductTagRelations() { }
	}
}

