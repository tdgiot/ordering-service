﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 5.8
// Code is generated on: 
// Code is generated using templates: Crave.TemplateBindings
// Templates vendor: Crave Interactive
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.FactoryClasses;
using Crave.Ordering.Shared.Data.LLBLGen.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

// ReSharper disable All
namespace Crave.Ordering.Shared.Data.LLBLGen.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'DeliveryInformation'.<br/><br/></summary>
	[Serializable]
	public partial class DeliveryInformationEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END
	
	{
		private EntityCollection<OrderEntity> _orderCollection;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		private static DeliveryInformationEntityStaticMetaData _staticMetaData = new DeliveryInformationEntityStaticMetaData();
		private static DeliveryInformationRelations _relationsFactory = new DeliveryInformationRelations();		

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name OrderCollection</summary>
			public static readonly string OrderCollection = "OrderCollection";
		}

		/// <summary>Static meta-data storage for navigator related information</summary>
		protected class DeliveryInformationEntityStaticMetaData : EntityStaticMetaDataBase
		{
			public DeliveryInformationEntityStaticMetaData()
			{
				SetEntityCoreInfo("DeliveryInformationEntity", InheritanceHierarchyType.None, false, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.DeliveryInformationEntity, typeof(DeliveryInformationEntity), typeof(DeliveryInformationEntityFactory), false);
				AddNavigatorMetaData<DeliveryInformationEntity, EntityCollection<OrderEntity>>("OrderCollection", a => a._orderCollection, (a, b) => a._orderCollection = b, a => a.OrderCollection, () => new DeliveryInformationRelations().OrderEntityUsingDeliveryInformationId, typeof(OrderEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.OrderEntity);
			}
		}

		/// <summary>Static ctor</summary>
		static DeliveryInformationEntity()
		{
		}

		/// <summary> CTor</summary>
		public DeliveryInformationEntity()
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public DeliveryInformationEntity(IEntityFields2 fields)
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this DeliveryInformationEntity</param>
		public DeliveryInformationEntity(IValidator validator)
		{
			InitClassEmpty(validator, null);
		}

		/// <summary> CTor</summary>
		/// <param name="deliveryInformationId">PK value for DeliveryInformation which data should be fetched into this DeliveryInformation object</param>
		public DeliveryInformationEntity(System.Int32 deliveryInformationId) : this(deliveryInformationId, null)
		{
		}

		/// <summary> CTor</summary>
		/// <param name="deliveryInformationId">PK value for DeliveryInformation which data should be fetched into this DeliveryInformation object</param>
		/// <param name="validator">The custom validator object for this DeliveryInformationEntity</param>
		public DeliveryInformationEntity(System.Int32 deliveryInformationId, IValidator validator)
		{
			InitClassEmpty(validator, null);
			this.DeliveryInformationId = deliveryInformationId;
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected DeliveryInformationEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Order' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoOrderCollection() { return CreateRelationInfoForNavigator("OrderCollection"); }
		
		/// <inheritdoc/>
		protected override EntityStaticMetaDataBase GetEntityStaticMetaData() {	return _staticMetaData; }

		/// <summary>Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		/// <summary>Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this DeliveryInformationEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary>The relations object holding all relations of this entity with other entity classes.</summary>
		public static DeliveryInformationRelations Relations { get { return _relationsFactory; } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Order' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathOrderCollection { get { return _staticMetaData.GetPrefetchPathElement("OrderCollection", CommonEntityBase.CreateEntityCollection<OrderEntity>()); } }

		/// <summary>The DeliveryInformationId property of the Entity DeliveryInformation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliveryInformation"."DeliveryInformationId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 DeliveryInformationId
		{
			get { return (System.Int32)GetValue((int)DeliveryInformationFieldIndex.DeliveryInformationId, true); }
			set { SetValue((int)DeliveryInformationFieldIndex.DeliveryInformationId, value); }		}

		/// <summary>The BuildingName property of the Entity DeliveryInformation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliveryInformation"."BuildingName".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String BuildingName
		{
			get { return (System.String)GetValue((int)DeliveryInformationFieldIndex.BuildingName, true); }
			set	{ SetValue((int)DeliveryInformationFieldIndex.BuildingName, value); }
		}

		/// <summary>The Address property of the Entity DeliveryInformation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliveryInformation"."Address".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Address
		{
			get { return (System.String)GetValue((int)DeliveryInformationFieldIndex.Address, true); }
			set	{ SetValue((int)DeliveryInformationFieldIndex.Address, value); }
		}

		/// <summary>The Zipcode property of the Entity DeliveryInformation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliveryInformation"."Zipcode".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Zipcode
		{
			get { return (System.String)GetValue((int)DeliveryInformationFieldIndex.Zipcode, true); }
			set	{ SetValue((int)DeliveryInformationFieldIndex.Zipcode, value); }
		}

		/// <summary>The City property of the Entity DeliveryInformation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliveryInformation"."City".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String City
		{
			get { return (System.String)GetValue((int)DeliveryInformationFieldIndex.City, true); }
			set	{ SetValue((int)DeliveryInformationFieldIndex.City, value); }
		}

		/// <summary>The Instructions property of the Entity DeliveryInformation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliveryInformation"."Instructions".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 500.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Instructions
		{
			get { return (System.String)GetValue((int)DeliveryInformationFieldIndex.Instructions, true); }
			set	{ SetValue((int)DeliveryInformationFieldIndex.Instructions, value); }
		}

		/// <summary>The CreatedUTC property of the Entity DeliveryInformation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliveryInformation"."CreatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)DeliveryInformationFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)DeliveryInformationFieldIndex.CreatedUTC, value); }
		}

		/// <summary>The UpdatedUTC property of the Entity DeliveryInformation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliveryInformation"."UpdatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)DeliveryInformationFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)DeliveryInformationFieldIndex.UpdatedUTC, value); }
		}

		/// <summary>The Latitude property of the Entity DeliveryInformation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliveryInformation"."Latitude".<br/>Table field type characteristics (type, precision, scale, length): Float, 38, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Double Latitude
		{
			get { return (System.Double)GetValue((int)DeliveryInformationFieldIndex.Latitude, true); }
			set	{ SetValue((int)DeliveryInformationFieldIndex.Latitude, value); }
		}

		/// <summary>The Longitude property of the Entity DeliveryInformation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliveryInformation"."Longitude".<br/>Table field type characteristics (type, precision, scale, length): Float, 38, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Double Longitude
		{
			get { return (System.Double)GetValue((int)DeliveryInformationFieldIndex.Longitude, true); }
			set	{ SetValue((int)DeliveryInformationFieldIndex.Longitude, value); }
		}

		/// <summary>The DistanceToOutletInMetres property of the Entity DeliveryInformation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliveryInformation"."DistanceToOutletInMetres".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 DistanceToOutletInMetres
		{
			get { return (System.Int32)GetValue((int)DeliveryInformationFieldIndex.DistanceToOutletInMetres, true); }
			set	{ SetValue((int)DeliveryInformationFieldIndex.DistanceToOutletInMetres, value); }
		}

		/// <summary>Gets the EntityCollection with the related entities of type 'OrderEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(OrderEntity))]
		public virtual EntityCollection<OrderEntity> OrderCollection { get { return GetOrCreateEntityCollection<OrderEntity, OrderEntityFactory>("DeliveryInformation", true, false, ref _orderCollection); } }


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen
{
	public enum DeliveryInformationFieldIndex
	{
		///<summary>DeliveryInformationId. </summary>
		DeliveryInformationId,
		///<summary>BuildingName. </summary>
		BuildingName,
		///<summary>Address. </summary>
		Address,
		///<summary>Zipcode. </summary>
		Zipcode,
		///<summary>City. </summary>
		City,
		///<summary>Instructions. </summary>
		Instructions,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>Latitude. </summary>
		Latitude,
		///<summary>Longitude. </summary>
		Longitude,
		///<summary>DistanceToOutletInMetres. </summary>
		DistanceToOutletInMetres,
		/// <summary></summary>
		AmountOfFields
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: DeliveryInformation. </summary>
	public partial class DeliveryInformationRelations: RelationFactory
	{
		#region Custom Relation code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomRelationCode
        private const ReferentialConstraintDeleteRule DeleteRuleForOrderEntityUsingDeliveryInformationId = ReferentialConstraintDeleteRule.SetNull;
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		
		// private const ReferentialConstraintDeleteRule DeleteRuleForOrderEntityUsingDeliveryInformationId = ReferentialConstraintDeleteRule


		/// <summary>Gets all relations and delete rules of the DeliveryInformationEntity as a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects.</summary>
		/// <returns>a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects</returns>
		public virtual Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> GetRelationsAndDeleteRules()
		{
			Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> toReturn = new Dictionary<IEntityRelation, ReferentialConstraintDeleteRule>();
			toReturn.Add(this.OrderEntityUsingDeliveryInformationId, DeliveryInformationRelations.DeleteRuleForOrderEntityUsingDeliveryInformationId);
			return toReturn;
		}		
	
	
		/// <summary>Returns a new IEntityRelation object, between DeliveryInformationEntity and OrderEntity over the 1:n relation they have, using the relation between the fields: DeliveryInformation.DeliveryInformationId - Order.DeliveryInformationId</summary>
		public virtual IEntityRelation OrderEntityUsingDeliveryInformationId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "OrderCollection", true, new[] { DeliveryInformationFields.DeliveryInformationId, OrderFields.DeliveryInformationId }); }
		}

	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticDeliveryInformationRelations
	{
		internal static readonly IEntityRelation OrderEntityUsingDeliveryInformationIdStatic = new DeliveryInformationRelations().OrderEntityUsingDeliveryInformationId;

		/// <summary>CTor</summary>
		static StaticDeliveryInformationRelations() { }
	}
}

