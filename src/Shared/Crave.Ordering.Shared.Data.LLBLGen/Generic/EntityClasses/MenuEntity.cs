﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 5.8
// Code is generated on: 
// Code is generated using templates: Crave.TemplateBindings
// Templates vendor: Crave Interactive
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.FactoryClasses;
using Crave.Ordering.Shared.Data.LLBLGen.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

// ReSharper disable All
namespace Crave.Ordering.Shared.Data.LLBLGen.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'Menu'.<br/><br/></summary>
	[Serializable]
	public partial class MenuEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END
	
	{
		private EntityCollection<CategoryEntity> _categoryCollection;
		private EntityCollection<ClientConfigurationEntity> _clientConfigurationCollection;
		private EntityCollection<DeliverypointgroupEntity> _deliverypointgroupCollection;
		private CompanyEntity _company;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		private static MenuEntityStaticMetaData _staticMetaData = new MenuEntityStaticMetaData();
		private static MenuRelations _relationsFactory = new MenuRelations();		

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Company</summary>
			public static readonly string Company = "Company";
			/// <summary>Member name CategoryCollection</summary>
			public static readonly string CategoryCollection = "CategoryCollection";
			/// <summary>Member name ClientConfigurationCollection</summary>
			public static readonly string ClientConfigurationCollection = "ClientConfigurationCollection";
			/// <summary>Member name DeliverypointgroupCollection</summary>
			public static readonly string DeliverypointgroupCollection = "DeliverypointgroupCollection";
		}

		/// <summary>Static meta-data storage for navigator related information</summary>
		protected class MenuEntityStaticMetaData : EntityStaticMetaDataBase
		{
			public MenuEntityStaticMetaData()
			{
				SetEntityCoreInfo("MenuEntity", InheritanceHierarchyType.None, false, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.MenuEntity, typeof(MenuEntity), typeof(MenuEntityFactory), false);
				AddNavigatorMetaData<MenuEntity, EntityCollection<CategoryEntity>>("CategoryCollection", a => a._categoryCollection, (a, b) => a._categoryCollection = b, a => a.CategoryCollection, () => new MenuRelations().CategoryEntityUsingMenuId, typeof(CategoryEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.CategoryEntity);
				AddNavigatorMetaData<MenuEntity, EntityCollection<ClientConfigurationEntity>>("ClientConfigurationCollection", a => a._clientConfigurationCollection, (a, b) => a._clientConfigurationCollection = b, a => a.ClientConfigurationCollection, () => new MenuRelations().ClientConfigurationEntityUsingMenuId, typeof(ClientConfigurationEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ClientConfigurationEntity);
				AddNavigatorMetaData<MenuEntity, EntityCollection<DeliverypointgroupEntity>>("DeliverypointgroupCollection", a => a._deliverypointgroupCollection, (a, b) => a._deliverypointgroupCollection = b, a => a.DeliverypointgroupCollection, () => new MenuRelations().DeliverypointgroupEntityUsingMenuId, typeof(DeliverypointgroupEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.DeliverypointgroupEntity);
				AddNavigatorMetaData<MenuEntity, CompanyEntity>("Company", "MenuCollection", (a, b) => a._company = b, a => a._company, (a, b) => a.Company = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticMenuRelations.CompanyEntityUsingCompanyIdStatic, ()=>new MenuRelations().CompanyEntityUsingCompanyId, null, new int[] { (int)MenuFieldIndex.CompanyId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.CompanyEntity);
			}
		}

		/// <summary>Static ctor</summary>
		static MenuEntity()
		{
		}

		/// <summary> CTor</summary>
		public MenuEntity()
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public MenuEntity(IEntityFields2 fields)
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this MenuEntity</param>
		public MenuEntity(IValidator validator)
		{
			InitClassEmpty(validator, null);
		}

		/// <summary> CTor</summary>
		/// <param name="menuId">PK value for Menu which data should be fetched into this Menu object</param>
		public MenuEntity(System.Int32 menuId) : this(menuId, null)
		{
		}

		/// <summary> CTor</summary>
		/// <param name="menuId">PK value for Menu which data should be fetched into this Menu object</param>
		/// <param name="validator">The custom validator object for this MenuEntity</param>
		public MenuEntity(System.Int32 menuId, IValidator validator)
		{
			InitClassEmpty(validator, null);
			this.MenuId = menuId;
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected MenuEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Category' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoCategoryCollection() { return CreateRelationInfoForNavigator("CategoryCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'ClientConfiguration' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoClientConfigurationCollection() { return CreateRelationInfoForNavigator("ClientConfigurationCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Deliverypointgroup' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoDeliverypointgroupCollection() { return CreateRelationInfoForNavigator("DeliverypointgroupCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Company' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoCompany() { return CreateRelationInfoForNavigator("Company"); }
		
		/// <inheritdoc/>
		protected override EntityStaticMetaDataBase GetEntityStaticMetaData() {	return _staticMetaData; }

		/// <summary>Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		/// <summary>Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this MenuEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary>The relations object holding all relations of this entity with other entity classes.</summary>
		public static MenuRelations Relations { get { return _relationsFactory; } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Category' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathCategoryCollection { get { return _staticMetaData.GetPrefetchPathElement("CategoryCollection", CommonEntityBase.CreateEntityCollection<CategoryEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'ClientConfiguration' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathClientConfigurationCollection { get { return _staticMetaData.GetPrefetchPathElement("ClientConfigurationCollection", CommonEntityBase.CreateEntityCollection<ClientConfigurationEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Deliverypointgroup' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathDeliverypointgroupCollection { get { return _staticMetaData.GetPrefetchPathElement("DeliverypointgroupCollection", CommonEntityBase.CreateEntityCollection<DeliverypointgroupEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Company' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathCompanyEntity { get { return _staticMetaData.GetPrefetchPathElement("Company", CommonEntityBase.CreateEntityCollection<CompanyEntity>()); } }

		/// <summary>The MenuId property of the Entity Menu<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Menu"."MenuId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 MenuId
		{
			get { return (System.Int32)GetValue((int)MenuFieldIndex.MenuId, true); }
			set { SetValue((int)MenuFieldIndex.MenuId, value); }		}

		/// <summary>The CompanyId property of the Entity Menu<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Menu"."CompanyId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CompanyId
		{
			get { return (System.Int32)GetValue((int)MenuFieldIndex.CompanyId, true); }
			set	{ SetValue((int)MenuFieldIndex.CompanyId, value); }
		}

		/// <summary>The Name property of the Entity Menu<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Menu"."Name".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)MenuFieldIndex.Name, true); }
			set	{ SetValue((int)MenuFieldIndex.Name, value); }
		}

		/// <summary>The Created property of the Entity Menu<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Menu"."Created".<br/>Table field type characteristics (type, precision, scale, length): DateTime, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> Created
		{
			get { return (Nullable<System.DateTime>)GetValue((int)MenuFieldIndex.Created, false); }
			set	{ SetValue((int)MenuFieldIndex.Created, value); }
		}

		/// <summary>The CreatedBy property of the Entity Menu<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Menu"."CreatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)MenuFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)MenuFieldIndex.CreatedBy, value); }
		}

		/// <summary>The Updated property of the Entity Menu<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Menu"."Updated".<br/>Table field type characteristics (type, precision, scale, length): DateTime, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> Updated
		{
			get { return (Nullable<System.DateTime>)GetValue((int)MenuFieldIndex.Updated, false); }
			set	{ SetValue((int)MenuFieldIndex.Updated, value); }
		}

		/// <summary>The UpdatedBy property of the Entity Menu<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Menu"."UpdatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)MenuFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)MenuFieldIndex.UpdatedBy, value); }
		}

		/// <summary>The CreatedUTC property of the Entity Menu<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Menu"."CreatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)MenuFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)MenuFieldIndex.CreatedUTC, value); }
		}

		/// <summary>The UpdatedUTC property of the Entity Menu<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Menu"."UpdatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)MenuFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)MenuFieldIndex.UpdatedUTC, value); }
		}

		/// <summary>Gets the EntityCollection with the related entities of type 'CategoryEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(CategoryEntity))]
		public virtual EntityCollection<CategoryEntity> CategoryCollection { get { return GetOrCreateEntityCollection<CategoryEntity, CategoryEntityFactory>("Menu", true, false, ref _categoryCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'ClientConfigurationEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(ClientConfigurationEntity))]
		public virtual EntityCollection<ClientConfigurationEntity> ClientConfigurationCollection { get { return GetOrCreateEntityCollection<ClientConfigurationEntity, ClientConfigurationEntityFactory>("Menu", true, false, ref _clientConfigurationCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'DeliverypointgroupEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(DeliverypointgroupEntity))]
		public virtual EntityCollection<DeliverypointgroupEntity> DeliverypointgroupCollection { get { return GetOrCreateEntityCollection<DeliverypointgroupEntity, DeliverypointgroupEntityFactory>("Menu", true, false, ref _deliverypointgroupCollection); } }

		/// <summary>Gets / sets related entity of type 'CompanyEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual CompanyEntity Company
		{
			get { return _company; }
			set { SetSingleRelatedEntityNavigator(value, "Company"); }
		}


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen
{
	public enum MenuFieldIndex
	{
		///<summary>MenuId. </summary>
		MenuId,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>Name. </summary>
		Name,
		///<summary>Created. </summary>
		Created,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>Updated. </summary>
		Updated,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Menu. </summary>
	public partial class MenuRelations: RelationFactory
	{
		#region Custom Relation code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomRelationCode
        private const ReferentialConstraintDeleteRule DeleteRuleForCategoryEntityUsingMenuId = ReferentialConstraintDeleteRule.Cascade;
        private const ReferentialConstraintDeleteRule DeleteRuleForClientConfigurationEntityUsingMenuId = ReferentialConstraintDeleteRule.NoAction;
        private const ReferentialConstraintDeleteRule DeleteRuleForDeliverypointgroupEntityUsingMenuId = ReferentialConstraintDeleteRule.NoAction;
        // __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		
		// private const ReferentialConstraintDeleteRule DeleteRuleForCategoryEntityUsingMenuId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForClientConfigurationEntityUsingMenuId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForDeliverypointgroupEntityUsingMenuId = ReferentialConstraintDeleteRule


		/// <summary>Gets all relations and delete rules of the MenuEntity as a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects.</summary>
		/// <returns>a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects</returns>
		public virtual Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> GetRelationsAndDeleteRules()
		{
			Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> toReturn = new Dictionary<IEntityRelation, ReferentialConstraintDeleteRule>();
			toReturn.Add(this.CategoryEntityUsingMenuId, MenuRelations.DeleteRuleForCategoryEntityUsingMenuId);
			toReturn.Add(this.ClientConfigurationEntityUsingMenuId, MenuRelations.DeleteRuleForClientConfigurationEntityUsingMenuId);
			toReturn.Add(this.DeliverypointgroupEntityUsingMenuId, MenuRelations.DeleteRuleForDeliverypointgroupEntityUsingMenuId);
			return toReturn;
		}		
	
	
		/// <summary>Returns a new IEntityRelation object, between MenuEntity and CategoryEntity over the 1:n relation they have, using the relation between the fields: Menu.MenuId - Category.MenuId</summary>
		public virtual IEntityRelation CategoryEntityUsingMenuId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "CategoryCollection", true, new[] { MenuFields.MenuId, CategoryFields.MenuId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between MenuEntity and ClientConfigurationEntity over the 1:n relation they have, using the relation between the fields: Menu.MenuId - ClientConfiguration.MenuId</summary>
		public virtual IEntityRelation ClientConfigurationEntityUsingMenuId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "ClientConfigurationCollection", true, new[] { MenuFields.MenuId, ClientConfigurationFields.MenuId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between MenuEntity and DeliverypointgroupEntity over the 1:n relation they have, using the relation between the fields: Menu.MenuId - Deliverypointgroup.MenuId</summary>
		public virtual IEntityRelation DeliverypointgroupEntityUsingMenuId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "DeliverypointgroupCollection", true, new[] { MenuFields.MenuId, DeliverypointgroupFields.MenuId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between MenuEntity and CompanyEntity over the m:1 relation they have, using the relation between the fields: Menu.CompanyId - Company.CompanyId</summary>
		public virtual IEntityRelation CompanyEntityUsingCompanyId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Company", false, new[] { CompanyFields.CompanyId, MenuFields.CompanyId }); }
		}

	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticMenuRelations
	{
		internal static readonly IEntityRelation CategoryEntityUsingMenuIdStatic = new MenuRelations().CategoryEntityUsingMenuId;
		internal static readonly IEntityRelation ClientConfigurationEntityUsingMenuIdStatic = new MenuRelations().ClientConfigurationEntityUsingMenuId;
		internal static readonly IEntityRelation DeliverypointgroupEntityUsingMenuIdStatic = new MenuRelations().DeliverypointgroupEntityUsingMenuId;
		internal static readonly IEntityRelation CompanyEntityUsingCompanyIdStatic = new MenuRelations().CompanyEntityUsingCompanyId;

		/// <summary>CTor</summary>
		static StaticMenuRelations() { }
	}
}

