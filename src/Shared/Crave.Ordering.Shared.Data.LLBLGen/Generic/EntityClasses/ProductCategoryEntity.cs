﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 5.8
// Code is generated on: 
// Code is generated using templates: Crave.TemplateBindings
// Templates vendor: Crave Interactive
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.FactoryClasses;
using Crave.Ordering.Shared.Data.LLBLGen.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

// ReSharper disable All
namespace Crave.Ordering.Shared.Data.LLBLGen.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'ProductCategory'.<br/><br/></summary>
	[Serializable]
	public partial class ProductCategoryEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END
	
	{
		private EntityCollection<ProductCategoryTagEntity> _productCategoryTagCollection;
		private CategoryEntity _category;
		private ProductEntity _product;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		private static ProductCategoryEntityStaticMetaData _staticMetaData = new ProductCategoryEntityStaticMetaData();
		private static ProductCategoryRelations _relationsFactory = new ProductCategoryRelations();		

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Category</summary>
			public static readonly string Category = "Category";
			/// <summary>Member name Product</summary>
			public static readonly string Product = "Product";
			/// <summary>Member name ProductCategoryTagCollection</summary>
			public static readonly string ProductCategoryTagCollection = "ProductCategoryTagCollection";
		}

		/// <summary>Static meta-data storage for navigator related information</summary>
		protected class ProductCategoryEntityStaticMetaData : EntityStaticMetaDataBase
		{
			public ProductCategoryEntityStaticMetaData()
			{
				SetEntityCoreInfo("ProductCategoryEntity", InheritanceHierarchyType.None, false, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ProductCategoryEntity, typeof(ProductCategoryEntity), typeof(ProductCategoryEntityFactory), false);
				AddNavigatorMetaData<ProductCategoryEntity, EntityCollection<ProductCategoryTagEntity>>("ProductCategoryTagCollection", a => a._productCategoryTagCollection, (a, b) => a._productCategoryTagCollection = b, a => a.ProductCategoryTagCollection, () => new ProductCategoryRelations().ProductCategoryTagEntityUsingProductCategoryId, typeof(ProductCategoryTagEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ProductCategoryTagEntity);
				AddNavigatorMetaData<ProductCategoryEntity, CategoryEntity>("Category", "ProductCategoryCollection", (a, b) => a._category = b, a => a._category, (a, b) => a.Category = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticProductCategoryRelations.CategoryEntityUsingCategoryIdStatic, ()=>new ProductCategoryRelations().CategoryEntityUsingCategoryId, null, new int[] { (int)ProductCategoryFieldIndex.CategoryId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.CategoryEntity);
				AddNavigatorMetaData<ProductCategoryEntity, ProductEntity>("Product", "ProductCategoryCollection", (a, b) => a._product = b, a => a._product, (a, b) => a.Product = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticProductCategoryRelations.ProductEntityUsingProductIdStatic, ()=>new ProductCategoryRelations().ProductEntityUsingProductId, null, new int[] { (int)ProductCategoryFieldIndex.ProductId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ProductEntity);
			}
		}

		/// <summary>Static ctor</summary>
		static ProductCategoryEntity()
		{
		}

		/// <summary> CTor</summary>
		public ProductCategoryEntity()
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public ProductCategoryEntity(IEntityFields2 fields)
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this ProductCategoryEntity</param>
		public ProductCategoryEntity(IValidator validator)
		{
			InitClassEmpty(validator, null);
		}

		/// <summary> CTor</summary>
		/// <param name="productCategoryId">PK value for ProductCategory which data should be fetched into this ProductCategory object</param>
		public ProductCategoryEntity(System.Int32 productCategoryId) : this(productCategoryId, null)
		{
		}

		/// <summary> CTor</summary>
		/// <param name="productCategoryId">PK value for ProductCategory which data should be fetched into this ProductCategory object</param>
		/// <param name="validator">The custom validator object for this ProductCategoryEntity</param>
		public ProductCategoryEntity(System.Int32 productCategoryId, IValidator validator)
		{
			InitClassEmpty(validator, null);
			this.ProductCategoryId = productCategoryId;
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ProductCategoryEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary>Method which will construct a filter (predicate expression) for the unique constraint defined on the fields: ProductId , CategoryId .</summary>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public IPredicateExpression ConstructFilterForUCProductIdCategoryId()
		{
			var filter = new PredicateExpression();
			filter.Add(Crave.Ordering.Shared.Data.LLBLGen.HelperClasses.ProductCategoryFields.ProductId == this.Fields.GetCurrentValue((int)ProductCategoryFieldIndex.ProductId));
			filter.Add(Crave.Ordering.Shared.Data.LLBLGen.HelperClasses.ProductCategoryFields.CategoryId == this.Fields.GetCurrentValue((int)ProductCategoryFieldIndex.CategoryId));
 			return filter;
		}

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'ProductCategoryTag' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoProductCategoryTagCollection() { return CreateRelationInfoForNavigator("ProductCategoryTagCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Category' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoCategory() { return CreateRelationInfoForNavigator("Category"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Product' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoProduct() { return CreateRelationInfoForNavigator("Product"); }
		
		/// <inheritdoc/>
		protected override EntityStaticMetaDataBase GetEntityStaticMetaData() {	return _staticMetaData; }

		/// <summary>Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		/// <summary>Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this ProductCategoryEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary>The relations object holding all relations of this entity with other entity classes.</summary>
		public static ProductCategoryRelations Relations { get { return _relationsFactory; } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'ProductCategoryTag' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathProductCategoryTagCollection { get { return _staticMetaData.GetPrefetchPathElement("ProductCategoryTagCollection", CommonEntityBase.CreateEntityCollection<ProductCategoryTagEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Category' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathCategoryEntity { get { return _staticMetaData.GetPrefetchPathElement("Category", CommonEntityBase.CreateEntityCollection<CategoryEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Product' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathProductEntity { get { return _staticMetaData.GetPrefetchPathElement("Product", CommonEntityBase.CreateEntityCollection<ProductEntity>()); } }

		/// <summary>The ProductCategoryId property of the Entity ProductCategory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ProductCategory"."ProductCategoryId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 ProductCategoryId
		{
			get { return (System.Int32)GetValue((int)ProductCategoryFieldIndex.ProductCategoryId, true); }
			set { SetValue((int)ProductCategoryFieldIndex.ProductCategoryId, value); }		}

		/// <summary>The ParentCompanyId property of the Entity ProductCategory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ProductCategory"."ParentCompanyId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ParentCompanyId
		{
			get { return (Nullable<System.Int32>)GetValue((int)ProductCategoryFieldIndex.ParentCompanyId, false); }
			set	{ SetValue((int)ProductCategoryFieldIndex.ParentCompanyId, value); }
		}

		/// <summary>The ProductId property of the Entity ProductCategory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ProductCategory"."ProductId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ProductId
		{
			get { return (System.Int32)GetValue((int)ProductCategoryFieldIndex.ProductId, true); }
			set	{ SetValue((int)ProductCategoryFieldIndex.ProductId, value); }
		}

		/// <summary>The CategoryId property of the Entity ProductCategory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ProductCategory"."CategoryId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CategoryId
		{
			get { return (System.Int32)GetValue((int)ProductCategoryFieldIndex.CategoryId, true); }
			set	{ SetValue((int)ProductCategoryFieldIndex.CategoryId, value); }
		}

		/// <summary>The SortOrder property of the Entity ProductCategory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ProductCategory"."SortOrder".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 SortOrder
		{
			get { return (System.Int32)GetValue((int)ProductCategoryFieldIndex.SortOrder, true); }
			set	{ SetValue((int)ProductCategoryFieldIndex.SortOrder, value); }
		}

		/// <summary>The InheritSuggestions property of the Entity ProductCategory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ProductCategory"."InheritSuggestions".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean InheritSuggestions
		{
			get { return (System.Boolean)GetValue((int)ProductCategoryFieldIndex.InheritSuggestions, true); }
			set	{ SetValue((int)ProductCategoryFieldIndex.InheritSuggestions, value); }
		}

		/// <summary>The CreatedBy property of the Entity ProductCategory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ProductCategory"."CreatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)ProductCategoryFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)ProductCategoryFieldIndex.CreatedBy, value); }
		}

		/// <summary>The UpdatedBy property of the Entity ProductCategory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ProductCategory"."UpdatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)ProductCategoryFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)ProductCategoryFieldIndex.UpdatedBy, value); }
		}

		/// <summary>The CreatedUTC property of the Entity ProductCategory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ProductCategory"."CreatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ProductCategoryFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)ProductCategoryFieldIndex.CreatedUTC, value); }
		}

		/// <summary>The UpdatedUTC property of the Entity ProductCategory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ProductCategory"."UpdatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ProductCategoryFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)ProductCategoryFieldIndex.UpdatedUTC, value); }
		}

		/// <summary>Gets the EntityCollection with the related entities of type 'ProductCategoryTagEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(ProductCategoryTagEntity))]
		public virtual EntityCollection<ProductCategoryTagEntity> ProductCategoryTagCollection { get { return GetOrCreateEntityCollection<ProductCategoryTagEntity, ProductCategoryTagEntityFactory>("ProductCategory", true, false, ref _productCategoryTagCollection); } }

		/// <summary>Gets / sets related entity of type 'CategoryEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual CategoryEntity Category
		{
			get { return _category; }
			set { SetSingleRelatedEntityNavigator(value, "Category"); }
		}

		/// <summary>Gets / sets related entity of type 'ProductEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual ProductEntity Product
		{
			get { return _product; }
			set { SetSingleRelatedEntityNavigator(value, "Product"); }
		}


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen
{
	public enum ProductCategoryFieldIndex
	{
		///<summary>ProductCategoryId. </summary>
		ProductCategoryId,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>ProductId. </summary>
		ProductId,
		///<summary>CategoryId. </summary>
		CategoryId,
		///<summary>SortOrder. </summary>
		SortOrder,
		///<summary>InheritSuggestions. </summary>
		InheritSuggestions,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: ProductCategory. </summary>
	public partial class ProductCategoryRelations: RelationFactory
	{
		#region Custom Relation code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomRelationCode
        private const ReferentialConstraintDeleteRule DeleteRuleForProductCategoryTagEntityUsingProductCategoryId = ReferentialConstraintDeleteRule.NoAction;
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		
		// private const ReferentialConstraintDeleteRule DeleteRuleForProductCategoryTagEntityUsingProductCategoryId = ReferentialConstraintDeleteRule


		/// <summary>Gets all relations and delete rules of the ProductCategoryEntity as a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects.</summary>
		/// <returns>a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects</returns>
		public virtual Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> GetRelationsAndDeleteRules()
		{
			Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> toReturn = new Dictionary<IEntityRelation, ReferentialConstraintDeleteRule>();
			toReturn.Add(this.ProductCategoryTagEntityUsingProductCategoryId, ProductCategoryRelations.DeleteRuleForProductCategoryTagEntityUsingProductCategoryId);
			return toReturn;
		}		
	
	
		/// <summary>Returns a new IEntityRelation object, between ProductCategoryEntity and ProductCategoryTagEntity over the 1:n relation they have, using the relation between the fields: ProductCategory.ProductCategoryId - ProductCategoryTag.ProductCategoryId</summary>
		public virtual IEntityRelation ProductCategoryTagEntityUsingProductCategoryId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "ProductCategoryTagCollection", true, new[] { ProductCategoryFields.ProductCategoryId, ProductCategoryTagFields.ProductCategoryId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between ProductCategoryEntity and CategoryEntity over the m:1 relation they have, using the relation between the fields: ProductCategory.CategoryId - Category.CategoryId</summary>
		public virtual IEntityRelation CategoryEntityUsingCategoryId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Category", false, new[] { CategoryFields.CategoryId, ProductCategoryFields.CategoryId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between ProductCategoryEntity and ProductEntity over the m:1 relation they have, using the relation between the fields: ProductCategory.ProductId - Product.ProductId</summary>
		public virtual IEntityRelation ProductEntityUsingProductId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Product", false, new[] { ProductFields.ProductId, ProductCategoryFields.ProductId }); }
		}

	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticProductCategoryRelations
	{
		internal static readonly IEntityRelation ProductCategoryTagEntityUsingProductCategoryIdStatic = new ProductCategoryRelations().ProductCategoryTagEntityUsingProductCategoryId;
		internal static readonly IEntityRelation CategoryEntityUsingCategoryIdStatic = new ProductCategoryRelations().CategoryEntityUsingCategoryId;
		internal static readonly IEntityRelation ProductEntityUsingProductIdStatic = new ProductCategoryRelations().ProductEntityUsingProductId;

		/// <summary>CTor</summary>
		static StaticProductCategoryRelations() { }
	}
}

