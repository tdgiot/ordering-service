﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 5.8
// Code is generated on: 
// Code is generated using templates: Crave.TemplateBindings
// Templates vendor: Crave Interactive
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.FactoryClasses;
using Crave.Ordering.Shared.Data.LLBLGen.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

// ReSharper disable All
namespace Crave.Ordering.Shared.Data.LLBLGen.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'PaymentTransaction'.<br/><br/></summary>
	[Serializable]
	public partial class PaymentTransactionEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END
	
	{
		private OrderEntity _order;
		private PaymentIntegrationConfigurationEntity _paymentIntegrationConfiguration;
		private PaymentProviderEntity _paymentProvider;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		private static PaymentTransactionEntityStaticMetaData _staticMetaData = new PaymentTransactionEntityStaticMetaData();
		private static PaymentTransactionRelations _relationsFactory = new PaymentTransactionRelations();		

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Order</summary>
			public static readonly string Order = "Order";
			/// <summary>Member name PaymentIntegrationConfiguration</summary>
			public static readonly string PaymentIntegrationConfiguration = "PaymentIntegrationConfiguration";
			/// <summary>Member name PaymentProvider</summary>
			public static readonly string PaymentProvider = "PaymentProvider";
		}

		/// <summary>Static meta-data storage for navigator related information</summary>
		protected class PaymentTransactionEntityStaticMetaData : EntityStaticMetaDataBase
		{
			public PaymentTransactionEntityStaticMetaData()
			{
				SetEntityCoreInfo("PaymentTransactionEntity", InheritanceHierarchyType.None, false, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.PaymentTransactionEntity, typeof(PaymentTransactionEntity), typeof(PaymentTransactionEntityFactory), false);
				AddNavigatorMetaData<PaymentTransactionEntity, OrderEntity>("Order", "PaymentTransactionCollection", (a, b) => a._order = b, a => a._order, (a, b) => a.Order = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticPaymentTransactionRelations.OrderEntityUsingOrderIdStatic, ()=>new PaymentTransactionRelations().OrderEntityUsingOrderId, null, new int[] { (int)PaymentTransactionFieldIndex.OrderId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.OrderEntity);
				AddNavigatorMetaData<PaymentTransactionEntity, PaymentIntegrationConfigurationEntity>("PaymentIntegrationConfiguration", "PaymentTransactionCollection", (a, b) => a._paymentIntegrationConfiguration = b, a => a._paymentIntegrationConfiguration, (a, b) => a.PaymentIntegrationConfiguration = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticPaymentTransactionRelations.PaymentIntegrationConfigurationEntityUsingPaymentIntegrationConfigurationIdStatic, ()=>new PaymentTransactionRelations().PaymentIntegrationConfigurationEntityUsingPaymentIntegrationConfigurationId, null, new int[] { (int)PaymentTransactionFieldIndex.PaymentIntegrationConfigurationId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.PaymentIntegrationConfigurationEntity);
				AddNavigatorMetaData<PaymentTransactionEntity, PaymentProviderEntity>("PaymentProvider", "PaymentTransactionCollection", (a, b) => a._paymentProvider = b, a => a._paymentProvider, (a, b) => a.PaymentProvider = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticPaymentTransactionRelations.PaymentProviderEntityUsingPaymentProviderIdStatic, ()=>new PaymentTransactionRelations().PaymentProviderEntityUsingPaymentProviderId, null, new int[] { (int)PaymentTransactionFieldIndex.PaymentProviderId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.PaymentProviderEntity);
			}
		}

		/// <summary>Static ctor</summary>
		static PaymentTransactionEntity()
		{
		}

		/// <summary> CTor</summary>
		public PaymentTransactionEntity()
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public PaymentTransactionEntity(IEntityFields2 fields)
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this PaymentTransactionEntity</param>
		public PaymentTransactionEntity(IValidator validator)
		{
			InitClassEmpty(validator, null);
		}

		/// <summary> CTor</summary>
		/// <param name="paymentTransactionId">PK value for PaymentTransaction which data should be fetched into this PaymentTransaction object</param>
		public PaymentTransactionEntity(System.Int32 paymentTransactionId) : this(paymentTransactionId, null)
		{
		}

		/// <summary> CTor</summary>
		/// <param name="paymentTransactionId">PK value for PaymentTransaction which data should be fetched into this PaymentTransaction object</param>
		/// <param name="validator">The custom validator object for this PaymentTransactionEntity</param>
		public PaymentTransactionEntity(System.Int32 paymentTransactionId, IValidator validator)
		{
			InitClassEmpty(validator, null);
			this.PaymentTransactionId = paymentTransactionId;
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected PaymentTransactionEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Order' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoOrder() { return CreateRelationInfoForNavigator("Order"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'PaymentIntegrationConfiguration' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoPaymentIntegrationConfiguration() { return CreateRelationInfoForNavigator("PaymentIntegrationConfiguration"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'PaymentProvider' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoPaymentProvider() { return CreateRelationInfoForNavigator("PaymentProvider"); }
		
		/// <inheritdoc/>
		protected override EntityStaticMetaDataBase GetEntityStaticMetaData() {	return _staticMetaData; }

		/// <summary>Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		/// <summary>Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this PaymentTransactionEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary>The relations object holding all relations of this entity with other entity classes.</summary>
		public static PaymentTransactionRelations Relations { get { return _relationsFactory; } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Order' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathOrderEntity { get { return _staticMetaData.GetPrefetchPathElement("Order", CommonEntityBase.CreateEntityCollection<OrderEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'PaymentIntegrationConfiguration' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathPaymentIntegrationConfigurationEntity { get { return _staticMetaData.GetPrefetchPathElement("PaymentIntegrationConfiguration", CommonEntityBase.CreateEntityCollection<PaymentIntegrationConfigurationEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'PaymentProvider' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathPaymentProviderEntity { get { return _staticMetaData.GetPrefetchPathElement("PaymentProvider", CommonEntityBase.CreateEntityCollection<PaymentProviderEntity>()); } }

		/// <summary>The PaymentTransactionId property of the Entity PaymentTransaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentTransaction"."PaymentTransactionId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 PaymentTransactionId
		{
			get { return (System.Int32)GetValue((int)PaymentTransactionFieldIndex.PaymentTransactionId, true); }
			set { SetValue((int)PaymentTransactionFieldIndex.PaymentTransactionId, value); }		}

		/// <summary>The OrderId property of the Entity PaymentTransaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentTransaction"."OrderId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 OrderId
		{
			get { return (System.Int32)GetValue((int)PaymentTransactionFieldIndex.OrderId, true); }
			set	{ SetValue((int)PaymentTransactionFieldIndex.OrderId, value); }
		}

		/// <summary>The ReferenceId property of the Entity PaymentTransaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentTransaction"."ReferenceId".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ReferenceId
		{
			get { return (System.String)GetValue((int)PaymentTransactionFieldIndex.ReferenceId, true); }
			set	{ SetValue((int)PaymentTransactionFieldIndex.ReferenceId, value); }
		}

		/// <summary>The MerchantReference property of the Entity PaymentTransaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentTransaction"."MerchantReference".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String MerchantReference
		{
			get { return (System.String)GetValue((int)PaymentTransactionFieldIndex.MerchantReference, true); }
			set	{ SetValue((int)PaymentTransactionFieldIndex.MerchantReference, value); }
		}

		/// <summary>The MerchantAccount property of the Entity PaymentTransaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentTransaction"."MerchantAccount".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String MerchantAccount
		{
			get { return (System.String)GetValue((int)PaymentTransactionFieldIndex.MerchantAccount, true); }
			set	{ SetValue((int)PaymentTransactionFieldIndex.MerchantAccount, value); }
		}

		/// <summary>The PaymentMethod property of the Entity PaymentTransaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentTransaction"."PaymentMethod".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String PaymentMethod
		{
			get { return (System.String)GetValue((int)PaymentTransactionFieldIndex.PaymentMethod, true); }
			set	{ SetValue((int)PaymentTransactionFieldIndex.PaymentMethod, value); }
		}

		/// <summary>The Status property of the Entity PaymentTransaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentTransaction"."Status".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Crave.Enums.PaymentTransactionStatus Status
		{
			get { return (Crave.Enums.PaymentTransactionStatus)GetValue((int)PaymentTransactionFieldIndex.Status, true); }
			set	{ SetValue((int)PaymentTransactionFieldIndex.Status, value); }
		}

		/// <summary>The CardSummary property of the Entity PaymentTransaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentTransaction"."CardSummary".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 25.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CardSummary
		{
			get { return (System.String)GetValue((int)PaymentTransactionFieldIndex.CardSummary, true); }
			set	{ SetValue((int)PaymentTransactionFieldIndex.CardSummary, value); }
		}

		/// <summary>The AuthCode property of the Entity PaymentTransaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentTransaction"."AuthCode".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 25.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String AuthCode
		{
			get { return (System.String)GetValue((int)PaymentTransactionFieldIndex.AuthCode, true); }
			set	{ SetValue((int)PaymentTransactionFieldIndex.AuthCode, value); }
		}

		/// <summary>The PaymentData property of the Entity PaymentTransaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentTransaction"."PaymentData".<br/>Table field type characteristics (type, precision, scale, length): Text, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PaymentData
		{
			get { return (System.String)GetValue((int)PaymentTransactionFieldIndex.PaymentData, true); }
			set	{ SetValue((int)PaymentTransactionFieldIndex.PaymentData, value); }
		}

		/// <summary>The CreatedUTC property of the Entity PaymentTransaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentTransaction"."CreatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime CreatedUTC
		{
			get { return (System.DateTime)GetValue((int)PaymentTransactionFieldIndex.CreatedUTC, true); }
			set	{ SetValue((int)PaymentTransactionFieldIndex.CreatedUTC, value); }
		}

		/// <summary>The UpdatedUTC property of the Entity PaymentTransaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentTransaction"."UpdatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)PaymentTransactionFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)PaymentTransactionFieldIndex.UpdatedUTC, value); }
		}

		/// <summary>The PaymentEnvironment property of the Entity PaymentTransaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentTransaction"."PaymentEnvironment".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 PaymentEnvironment
		{
			get { return (System.Int32)GetValue((int)PaymentTransactionFieldIndex.PaymentEnvironment, true); }
			set	{ SetValue((int)PaymentTransactionFieldIndex.PaymentEnvironment, value); }
		}

		/// <summary>The PaymentProviderType property of the Entity PaymentTransaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentTransaction"."PaymentProviderType".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 PaymentProviderType
		{
			get { return (System.Int32)GetValue((int)PaymentTransactionFieldIndex.PaymentProviderType, true); }
			set	{ SetValue((int)PaymentTransactionFieldIndex.PaymentProviderType, value); }
		}

		/// <summary>The PaymentProviderName property of the Entity PaymentTransaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentTransaction"."PaymentProviderName".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 100.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PaymentProviderName
		{
			get { return (System.String)GetValue((int)PaymentTransactionFieldIndex.PaymentProviderName, true); }
			set	{ SetValue((int)PaymentTransactionFieldIndex.PaymentProviderName, value); }
		}

		/// <summary>The PaymentProviderId property of the Entity PaymentTransaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentTransaction"."PaymentProviderId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> PaymentProviderId
		{
			get { return (Nullable<System.Int32>)GetValue((int)PaymentTransactionFieldIndex.PaymentProviderId, false); }
			set	{ SetValue((int)PaymentTransactionFieldIndex.PaymentProviderId, value); }
		}

		/// <summary>The PaymentIntegrationConfigurationId property of the Entity PaymentTransaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentTransaction"."PaymentIntegrationConfigurationId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> PaymentIntegrationConfigurationId
		{
			get { return (Nullable<System.Int32>)GetValue((int)PaymentTransactionFieldIndex.PaymentIntegrationConfigurationId, false); }
			set	{ SetValue((int)PaymentTransactionFieldIndex.PaymentIntegrationConfigurationId, value); }
		}

		/// <summary>Gets / sets related entity of type 'OrderEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual OrderEntity Order
		{
			get { return _order; }
			set { SetSingleRelatedEntityNavigator(value, "Order"); }
		}

		/// <summary>Gets / sets related entity of type 'PaymentIntegrationConfigurationEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual PaymentIntegrationConfigurationEntity PaymentIntegrationConfiguration
		{
			get { return _paymentIntegrationConfiguration; }
			set { SetSingleRelatedEntityNavigator(value, "PaymentIntegrationConfiguration"); }
		}

		/// <summary>Gets / sets related entity of type 'PaymentProviderEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual PaymentProviderEntity PaymentProvider
		{
			get { return _paymentProvider; }
			set { SetSingleRelatedEntityNavigator(value, "PaymentProvider"); }
		}


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen
{
	public enum PaymentTransactionFieldIndex
	{
		///<summary>PaymentTransactionId. </summary>
		PaymentTransactionId,
		///<summary>OrderId. </summary>
		OrderId,
		///<summary>ReferenceId. </summary>
		ReferenceId,
		///<summary>MerchantReference. </summary>
		MerchantReference,
		///<summary>MerchantAccount. </summary>
		MerchantAccount,
		///<summary>PaymentMethod. </summary>
		PaymentMethod,
		///<summary>Status. </summary>
		Status,
		///<summary>CardSummary. </summary>
		CardSummary,
		///<summary>AuthCode. </summary>
		AuthCode,
		///<summary>PaymentData. </summary>
		PaymentData,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>PaymentEnvironment. </summary>
		PaymentEnvironment,
		///<summary>PaymentProviderType. </summary>
		PaymentProviderType,
		///<summary>PaymentProviderName. </summary>
		PaymentProviderName,
		///<summary>PaymentProviderId. </summary>
		PaymentProviderId,
		///<summary>PaymentIntegrationConfigurationId. </summary>
		PaymentIntegrationConfigurationId,
		/// <summary></summary>
		AmountOfFields
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: PaymentTransaction. </summary>
	public partial class PaymentTransactionRelations: RelationFactory
	{
		#region Custom Relation code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomRelationCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		


		/// <summary>Gets all relations and delete rules of the PaymentTransactionEntity as a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects.</summary>
		/// <returns>a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects</returns>
		public virtual Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> GetRelationsAndDeleteRules()
		{
			Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> toReturn = new Dictionary<IEntityRelation, ReferentialConstraintDeleteRule>();
			return toReturn;
		}		
	
	

		/// <summary>Returns a new IEntityRelation object, between PaymentTransactionEntity and OrderEntity over the m:1 relation they have, using the relation between the fields: PaymentTransaction.OrderId - Order.OrderId</summary>
		public virtual IEntityRelation OrderEntityUsingOrderId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Order", false, new[] { OrderFields.OrderId, PaymentTransactionFields.OrderId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between PaymentTransactionEntity and PaymentIntegrationConfigurationEntity over the m:1 relation they have, using the relation between the fields: PaymentTransaction.PaymentIntegrationConfigurationId - PaymentIntegrationConfiguration.PaymentIntegrationConfigurationId</summary>
		public virtual IEntityRelation PaymentIntegrationConfigurationEntityUsingPaymentIntegrationConfigurationId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "PaymentIntegrationConfiguration", false, new[] { PaymentIntegrationConfigurationFields.PaymentIntegrationConfigurationId, PaymentTransactionFields.PaymentIntegrationConfigurationId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between PaymentTransactionEntity and PaymentProviderEntity over the m:1 relation they have, using the relation between the fields: PaymentTransaction.PaymentProviderId - PaymentProvider.PaymentProviderId</summary>
		public virtual IEntityRelation PaymentProviderEntityUsingPaymentProviderId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "PaymentProvider", false, new[] { PaymentProviderFields.PaymentProviderId, PaymentTransactionFields.PaymentProviderId }); }
		}

	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticPaymentTransactionRelations
	{
		internal static readonly IEntityRelation OrderEntityUsingOrderIdStatic = new PaymentTransactionRelations().OrderEntityUsingOrderId;
		internal static readonly IEntityRelation PaymentIntegrationConfigurationEntityUsingPaymentIntegrationConfigurationIdStatic = new PaymentTransactionRelations().PaymentIntegrationConfigurationEntityUsingPaymentIntegrationConfigurationId;
		internal static readonly IEntityRelation PaymentProviderEntityUsingPaymentProviderIdStatic = new PaymentTransactionRelations().PaymentProviderEntityUsingPaymentProviderId;

		/// <summary>CTor</summary>
		static StaticPaymentTransactionRelations() { }
	}
}

