﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 5.8
// Code is generated on: 
// Code is generated using templates: Crave.TemplateBindings
// Templates vendor: Crave Interactive
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.FactoryClasses;
using Crave.Ordering.Shared.Data.LLBLGen.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

// ReSharper disable All
namespace Crave.Ordering.Shared.Data.LLBLGen.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'SupportpoolNotificationRecipient'.<br/><br/></summary>
	[Serializable]
	public partial class SupportpoolNotificationRecipientEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END
	
	{
		private SupportpoolEntity _supportpool;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		private static SupportpoolNotificationRecipientEntityStaticMetaData _staticMetaData = new SupportpoolNotificationRecipientEntityStaticMetaData();
		private static SupportpoolNotificationRecipientRelations _relationsFactory = new SupportpoolNotificationRecipientRelations();		

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Supportpool</summary>
			public static readonly string Supportpool = "Supportpool";
		}

		/// <summary>Static meta-data storage for navigator related information</summary>
		protected class SupportpoolNotificationRecipientEntityStaticMetaData : EntityStaticMetaDataBase
		{
			public SupportpoolNotificationRecipientEntityStaticMetaData()
			{
				SetEntityCoreInfo("SupportpoolNotificationRecipientEntity", InheritanceHierarchyType.None, false, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.SupportpoolNotificationRecipientEntity, typeof(SupportpoolNotificationRecipientEntity), typeof(SupportpoolNotificationRecipientEntityFactory), false);
				AddNavigatorMetaData<SupportpoolNotificationRecipientEntity, SupportpoolEntity>("Supportpool", "SupportpoolNotificationRecipientCollection", (a, b) => a._supportpool = b, a => a._supportpool, (a, b) => a.Supportpool = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticSupportpoolNotificationRecipientRelations.SupportpoolEntityUsingSupportpoolIdStatic, ()=>new SupportpoolNotificationRecipientRelations().SupportpoolEntityUsingSupportpoolId, null, new int[] { (int)SupportpoolNotificationRecipientFieldIndex.SupportpoolId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.SupportpoolEntity);
			}
		}

		/// <summary>Static ctor</summary>
		static SupportpoolNotificationRecipientEntity()
		{
		}

		/// <summary> CTor</summary>
		public SupportpoolNotificationRecipientEntity()
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public SupportpoolNotificationRecipientEntity(IEntityFields2 fields)
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this SupportpoolNotificationRecipientEntity</param>
		public SupportpoolNotificationRecipientEntity(IValidator validator)
		{
			InitClassEmpty(validator, null);
		}

		/// <summary> CTor</summary>
		/// <param name="supportpoolNotificationRecipientId">PK value for SupportpoolNotificationRecipient which data should be fetched into this SupportpoolNotificationRecipient object</param>
		public SupportpoolNotificationRecipientEntity(System.Int32 supportpoolNotificationRecipientId) : this(supportpoolNotificationRecipientId, null)
		{
		}

		/// <summary> CTor</summary>
		/// <param name="supportpoolNotificationRecipientId">PK value for SupportpoolNotificationRecipient which data should be fetched into this SupportpoolNotificationRecipient object</param>
		/// <param name="validator">The custom validator object for this SupportpoolNotificationRecipientEntity</param>
		public SupportpoolNotificationRecipientEntity(System.Int32 supportpoolNotificationRecipientId, IValidator validator)
		{
			InitClassEmpty(validator, null);
			this.SupportpoolNotificationRecipientId = supportpoolNotificationRecipientId;
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected SupportpoolNotificationRecipientEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Supportpool' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoSupportpool() { return CreateRelationInfoForNavigator("Supportpool"); }
		
		/// <inheritdoc/>
		protected override EntityStaticMetaDataBase GetEntityStaticMetaData() {	return _staticMetaData; }

		/// <summary>Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		/// <summary>Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this SupportpoolNotificationRecipientEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary>The relations object holding all relations of this entity with other entity classes.</summary>
		public static SupportpoolNotificationRecipientRelations Relations { get { return _relationsFactory; } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Supportpool' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathSupportpoolEntity { get { return _staticMetaData.GetPrefetchPathElement("Supportpool", CommonEntityBase.CreateEntityCollection<SupportpoolEntity>()); } }

		/// <summary>The SupportpoolNotificationRecipientId property of the Entity SupportpoolNotificationRecipient<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SupportpoolNotificationRecipient"."SupportpoolNotificationRecipientId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 SupportpoolNotificationRecipientId
		{
			get { return (System.Int32)GetValue((int)SupportpoolNotificationRecipientFieldIndex.SupportpoolNotificationRecipientId, true); }
			set { SetValue((int)SupportpoolNotificationRecipientFieldIndex.SupportpoolNotificationRecipientId, value); }		}

		/// <summary>The SupportpoolId property of the Entity SupportpoolNotificationRecipient<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SupportpoolNotificationRecipient"."SupportpoolId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> SupportpoolId
		{
			get { return (Nullable<System.Int32>)GetValue((int)SupportpoolNotificationRecipientFieldIndex.SupportpoolId, false); }
			set	{ SetValue((int)SupportpoolNotificationRecipientFieldIndex.SupportpoolId, value); }
		}

		/// <summary>The Name property of the Entity SupportpoolNotificationRecipient<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SupportpoolNotificationRecipient"."Name".<br/>Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 255.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)SupportpoolNotificationRecipientFieldIndex.Name, true); }
			set	{ SetValue((int)SupportpoolNotificationRecipientFieldIndex.Name, value); }
		}

		/// <summary>The Phonenumber property of the Entity SupportpoolNotificationRecipient<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SupportpoolNotificationRecipient"."Phonenumber".<br/>Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 255.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Phonenumber
		{
			get { return (System.String)GetValue((int)SupportpoolNotificationRecipientFieldIndex.Phonenumber, true); }
			set	{ SetValue((int)SupportpoolNotificationRecipientFieldIndex.Phonenumber, value); }
		}

		/// <summary>The Email property of the Entity SupportpoolNotificationRecipient<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SupportpoolNotificationRecipient"."Email".<br/>Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 255.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Email
		{
			get { return (System.String)GetValue((int)SupportpoolNotificationRecipientFieldIndex.Email, true); }
			set	{ SetValue((int)SupportpoolNotificationRecipientFieldIndex.Email, value); }
		}

		/// <summary>The NotifyOfflineTerminals property of the Entity SupportpoolNotificationRecipient<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SupportpoolNotificationRecipient"."NotifyOfflineTerminals".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean NotifyOfflineTerminals
		{
			get { return (System.Boolean)GetValue((int)SupportpoolNotificationRecipientFieldIndex.NotifyOfflineTerminals, true); }
			set	{ SetValue((int)SupportpoolNotificationRecipientFieldIndex.NotifyOfflineTerminals, value); }
		}

		/// <summary>The NotifyOfflineClients property of the Entity SupportpoolNotificationRecipient<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SupportpoolNotificationRecipient"."NotifyOfflineClients".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean NotifyOfflineClients
		{
			get { return (System.Boolean)GetValue((int)SupportpoolNotificationRecipientFieldIndex.NotifyOfflineClients, true); }
			set	{ SetValue((int)SupportpoolNotificationRecipientFieldIndex.NotifyOfflineClients, value); }
		}

		/// <summary>The NotifyUnprocessableOrders property of the Entity SupportpoolNotificationRecipient<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SupportpoolNotificationRecipient"."NotifyUnprocessableOrders".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean NotifyUnprocessableOrders
		{
			get { return (System.Boolean)GetValue((int)SupportpoolNotificationRecipientFieldIndex.NotifyUnprocessableOrders, true); }
			set	{ SetValue((int)SupportpoolNotificationRecipientFieldIndex.NotifyUnprocessableOrders, value); }
		}

		/// <summary>The NotifyExpiredSteps property of the Entity SupportpoolNotificationRecipient<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SupportpoolNotificationRecipient"."NotifyExpiredSteps".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean NotifyExpiredSteps
		{
			get { return (System.Boolean)GetValue((int)SupportpoolNotificationRecipientFieldIndex.NotifyExpiredSteps, true); }
			set	{ SetValue((int)SupportpoolNotificationRecipientFieldIndex.NotifyExpiredSteps, value); }
		}

		/// <summary>The NotifyTooMuchOfflineClientsJump property of the Entity SupportpoolNotificationRecipient<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SupportpoolNotificationRecipient"."NotifyTooMuchOfflineClientsJump".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean NotifyTooMuchOfflineClientsJump
		{
			get { return (System.Boolean)GetValue((int)SupportpoolNotificationRecipientFieldIndex.NotifyTooMuchOfflineClientsJump, true); }
			set	{ SetValue((int)SupportpoolNotificationRecipientFieldIndex.NotifyTooMuchOfflineClientsJump, value); }
		}

		/// <summary>The NotifyBouncedEmails property of the Entity SupportpoolNotificationRecipient<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SupportpoolNotificationRecipient"."NotifyBouncedEmails".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean NotifyBouncedEmails
		{
			get { return (System.Boolean)GetValue((int)SupportpoolNotificationRecipientFieldIndex.NotifyBouncedEmails, true); }
			set	{ SetValue((int)SupportpoolNotificationRecipientFieldIndex.NotifyBouncedEmails, value); }
		}

		/// <summary>The CreatedUTC property of the Entity SupportpoolNotificationRecipient<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SupportpoolNotificationRecipient"."CreatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)SupportpoolNotificationRecipientFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)SupportpoolNotificationRecipientFieldIndex.CreatedUTC, value); }
		}

		/// <summary>The CreatedBy property of the Entity SupportpoolNotificationRecipient<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SupportpoolNotificationRecipient"."CreatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CreatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)SupportpoolNotificationRecipientFieldIndex.CreatedBy, false); }
			set	{ SetValue((int)SupportpoolNotificationRecipientFieldIndex.CreatedBy, value); }
		}

		/// <summary>The UpdatedUTC property of the Entity SupportpoolNotificationRecipient<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SupportpoolNotificationRecipient"."UpdatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)SupportpoolNotificationRecipientFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)SupportpoolNotificationRecipientFieldIndex.UpdatedUTC, value); }
		}

		/// <summary>The UpdatedBy property of the Entity SupportpoolNotificationRecipient<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SupportpoolNotificationRecipient"."UpdatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UpdatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)SupportpoolNotificationRecipientFieldIndex.UpdatedBy, false); }
			set	{ SetValue((int)SupportpoolNotificationRecipientFieldIndex.UpdatedBy, value); }
		}

		/// <summary>Gets / sets related entity of type 'SupportpoolEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual SupportpoolEntity Supportpool
		{
			get { return _supportpool; }
			set { SetSingleRelatedEntityNavigator(value, "Supportpool"); }
		}


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen
{
	public enum SupportpoolNotificationRecipientFieldIndex
	{
		///<summary>SupportpoolNotificationRecipientId. </summary>
		SupportpoolNotificationRecipientId,
		///<summary>SupportpoolId. </summary>
		SupportpoolId,
		///<summary>Name. </summary>
		Name,
		///<summary>Phonenumber. </summary>
		Phonenumber,
		///<summary>Email. </summary>
		Email,
		///<summary>NotifyOfflineTerminals. </summary>
		NotifyOfflineTerminals,
		///<summary>NotifyOfflineClients. </summary>
		NotifyOfflineClients,
		///<summary>NotifyUnprocessableOrders. </summary>
		NotifyUnprocessableOrders,
		///<summary>NotifyExpiredSteps. </summary>
		NotifyExpiredSteps,
		///<summary>NotifyTooMuchOfflineClientsJump. </summary>
		NotifyTooMuchOfflineClientsJump,
		///<summary>NotifyBouncedEmails. </summary>
		NotifyBouncedEmails,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		/// <summary></summary>
		AmountOfFields
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: SupportpoolNotificationRecipient. </summary>
	public partial class SupportpoolNotificationRecipientRelations: RelationFactory
	{
		#region Custom Relation code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomRelationCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		


		/// <summary>Gets all relations and delete rules of the SupportpoolNotificationRecipientEntity as a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects.</summary>
		/// <returns>a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects</returns>
		public virtual Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> GetRelationsAndDeleteRules()
		{
			Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> toReturn = new Dictionary<IEntityRelation, ReferentialConstraintDeleteRule>();
			return toReturn;
		}		
	
	

		/// <summary>Returns a new IEntityRelation object, between SupportpoolNotificationRecipientEntity and SupportpoolEntity over the m:1 relation they have, using the relation between the fields: SupportpoolNotificationRecipient.SupportpoolId - Supportpool.SupportpoolId</summary>
		public virtual IEntityRelation SupportpoolEntityUsingSupportpoolId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Supportpool", false, new[] { SupportpoolFields.SupportpoolId, SupportpoolNotificationRecipientFields.SupportpoolId }); }
		}

	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticSupportpoolNotificationRecipientRelations
	{
		internal static readonly IEntityRelation SupportpoolEntityUsingSupportpoolIdStatic = new SupportpoolNotificationRecipientRelations().SupportpoolEntityUsingSupportpoolId;

		/// <summary>CTor</summary>
		static StaticSupportpoolNotificationRecipientRelations() { }
	}
}

