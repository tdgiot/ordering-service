﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 5.8
// Code is generated on: 
// Code is generated using templates: Crave.TemplateBindings
// Templates vendor: Crave Interactive
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.FactoryClasses;
using Crave.Ordering.Shared.Data.LLBLGen.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

// ReSharper disable All
namespace Crave.Ordering.Shared.Data.LLBLGen.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'ExternalDeliverypoint'.<br/><br/></summary>
	[Serializable]
	public partial class ExternalDeliverypointEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END
	
	{
		private EntityCollection<DeliverypointExternalDeliverypointEntity> _deliverypointExternalDeliverypointCollection;
		private ExternalSystemEntity _externalSystem;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		private static ExternalDeliverypointEntityStaticMetaData _staticMetaData = new ExternalDeliverypointEntityStaticMetaData();
		private static ExternalDeliverypointRelations _relationsFactory = new ExternalDeliverypointRelations();		

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name ExternalSystem</summary>
			public static readonly string ExternalSystem = "ExternalSystem";
			/// <summary>Member name DeliverypointExternalDeliverypointCollection</summary>
			public static readonly string DeliverypointExternalDeliverypointCollection = "DeliverypointExternalDeliverypointCollection";
		}

		/// <summary>Static meta-data storage for navigator related information</summary>
		protected class ExternalDeliverypointEntityStaticMetaData : EntityStaticMetaDataBase
		{
			public ExternalDeliverypointEntityStaticMetaData()
			{
				SetEntityCoreInfo("ExternalDeliverypointEntity", InheritanceHierarchyType.None, false, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ExternalDeliverypointEntity, typeof(ExternalDeliverypointEntity), typeof(ExternalDeliverypointEntityFactory), false);
				AddNavigatorMetaData<ExternalDeliverypointEntity, EntityCollection<DeliverypointExternalDeliverypointEntity>>("DeliverypointExternalDeliverypointCollection", a => a._deliverypointExternalDeliverypointCollection, (a, b) => a._deliverypointExternalDeliverypointCollection = b, a => a.DeliverypointExternalDeliverypointCollection, () => new ExternalDeliverypointRelations().DeliverypointExternalDeliverypointEntityUsingExternalDeliverypointId, typeof(DeliverypointExternalDeliverypointEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.DeliverypointExternalDeliverypointEntity);
				AddNavigatorMetaData<ExternalDeliverypointEntity, ExternalSystemEntity>("ExternalSystem", "ExternalDeliverypointCollection", (a, b) => a._externalSystem = b, a => a._externalSystem, (a, b) => a.ExternalSystem = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticExternalDeliverypointRelations.ExternalSystemEntityUsingExternalSystemIdStatic, ()=>new ExternalDeliverypointRelations().ExternalSystemEntityUsingExternalSystemId, null, new int[] { (int)ExternalDeliverypointFieldIndex.ExternalSystemId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ExternalSystemEntity);
			}
		}

		/// <summary>Static ctor</summary>
		static ExternalDeliverypointEntity()
		{
		}

		/// <summary> CTor</summary>
		public ExternalDeliverypointEntity()
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public ExternalDeliverypointEntity(IEntityFields2 fields)
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this ExternalDeliverypointEntity</param>
		public ExternalDeliverypointEntity(IValidator validator)
		{
			InitClassEmpty(validator, null);
		}

		/// <summary> CTor</summary>
		/// <param name="externalDeliverypointId">PK value for ExternalDeliverypoint which data should be fetched into this ExternalDeliverypoint object</param>
		public ExternalDeliverypointEntity(System.Int32 externalDeliverypointId) : this(externalDeliverypointId, null)
		{
		}

		/// <summary> CTor</summary>
		/// <param name="externalDeliverypointId">PK value for ExternalDeliverypoint which data should be fetched into this ExternalDeliverypoint object</param>
		/// <param name="validator">The custom validator object for this ExternalDeliverypointEntity</param>
		public ExternalDeliverypointEntity(System.Int32 externalDeliverypointId, IValidator validator)
		{
			InitClassEmpty(validator, null);
			this.ExternalDeliverypointId = externalDeliverypointId;
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ExternalDeliverypointEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'DeliverypointExternalDeliverypoint' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoDeliverypointExternalDeliverypointCollection() { return CreateRelationInfoForNavigator("DeliverypointExternalDeliverypointCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'ExternalSystem' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoExternalSystem() { return CreateRelationInfoForNavigator("ExternalSystem"); }
		
		/// <inheritdoc/>
		protected override EntityStaticMetaDataBase GetEntityStaticMetaData() {	return _staticMetaData; }

		/// <summary>Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		/// <summary>Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this ExternalDeliverypointEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary>The relations object holding all relations of this entity with other entity classes.</summary>
		public static ExternalDeliverypointRelations Relations { get { return _relationsFactory; } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'DeliverypointExternalDeliverypoint' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathDeliverypointExternalDeliverypointCollection { get { return _staticMetaData.GetPrefetchPathElement("DeliverypointExternalDeliverypointCollection", CommonEntityBase.CreateEntityCollection<DeliverypointExternalDeliverypointEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'ExternalSystem' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathExternalSystemEntity { get { return _staticMetaData.GetPrefetchPathElement("ExternalSystem", CommonEntityBase.CreateEntityCollection<ExternalSystemEntity>()); } }

		/// <summary>The ExternalDeliverypointId property of the Entity ExternalDeliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalDeliverypoint"."ExternalDeliverypointId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 ExternalDeliverypointId
		{
			get { return (System.Int32)GetValue((int)ExternalDeliverypointFieldIndex.ExternalDeliverypointId, true); }
			set { SetValue((int)ExternalDeliverypointFieldIndex.ExternalDeliverypointId, value); }		}

		/// <summary>The ExternalSystemId property of the Entity ExternalDeliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalDeliverypoint"."ExternalSystemId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ExternalSystemId
		{
			get { return (System.Int32)GetValue((int)ExternalDeliverypointFieldIndex.ExternalSystemId, true); }
			set	{ SetValue((int)ExternalDeliverypointFieldIndex.ExternalSystemId, value); }
		}

		/// <summary>The ParentCompanyId property of the Entity ExternalDeliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalDeliverypoint"."ParentCompanyId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ParentCompanyId
		{
			get { return (System.Int32)GetValue((int)ExternalDeliverypointFieldIndex.ParentCompanyId, true); }
			set	{ SetValue((int)ExternalDeliverypointFieldIndex.ParentCompanyId, value); }
		}

		/// <summary>The Id property of the Entity ExternalDeliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalDeliverypoint"."Id".<br/>Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 50.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Id
		{
			get { return (System.String)GetValue((int)ExternalDeliverypointFieldIndex.Id, true); }
			set	{ SetValue((int)ExternalDeliverypointFieldIndex.Id, value); }
		}

		/// <summary>The Name property of the Entity ExternalDeliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalDeliverypoint"."Name".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 100.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)ExternalDeliverypointFieldIndex.Name, true); }
			set	{ SetValue((int)ExternalDeliverypointFieldIndex.Name, value); }
		}

		/// <summary>The Visible property of the Entity ExternalDeliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalDeliverypoint"."Visible".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean Visible
		{
			get { return (System.Boolean)GetValue((int)ExternalDeliverypointFieldIndex.Visible, true); }
			set	{ SetValue((int)ExternalDeliverypointFieldIndex.Visible, value); }
		}

		/// <summary>The StringValue1 property of the Entity ExternalDeliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalDeliverypoint"."StringValue1".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String StringValue1
		{
			get { return (System.String)GetValue((int)ExternalDeliverypointFieldIndex.StringValue1, true); }
			set	{ SetValue((int)ExternalDeliverypointFieldIndex.StringValue1, value); }
		}

		/// <summary>The StringValue2 property of the Entity ExternalDeliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalDeliverypoint"."StringValue2".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String StringValue2
		{
			get { return (System.String)GetValue((int)ExternalDeliverypointFieldIndex.StringValue2, true); }
			set	{ SetValue((int)ExternalDeliverypointFieldIndex.StringValue2, value); }
		}

		/// <summary>The StringValue3 property of the Entity ExternalDeliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalDeliverypoint"."StringValue3".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String StringValue3
		{
			get { return (System.String)GetValue((int)ExternalDeliverypointFieldIndex.StringValue3, true); }
			set	{ SetValue((int)ExternalDeliverypointFieldIndex.StringValue3, value); }
		}

		/// <summary>The StringValue4 property of the Entity ExternalDeliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalDeliverypoint"."StringValue4".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String StringValue4
		{
			get { return (System.String)GetValue((int)ExternalDeliverypointFieldIndex.StringValue4, true); }
			set	{ SetValue((int)ExternalDeliverypointFieldIndex.StringValue4, value); }
		}

		/// <summary>The StringValue5 property of the Entity ExternalDeliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalDeliverypoint"."StringValue5".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String StringValue5
		{
			get { return (System.String)GetValue((int)ExternalDeliverypointFieldIndex.StringValue5, true); }
			set	{ SetValue((int)ExternalDeliverypointFieldIndex.StringValue5, value); }
		}

		/// <summary>The IntValue1 property of the Entity ExternalDeliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalDeliverypoint"."IntValue1".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> IntValue1
		{
			get { return (Nullable<System.Int32>)GetValue((int)ExternalDeliverypointFieldIndex.IntValue1, false); }
			set	{ SetValue((int)ExternalDeliverypointFieldIndex.IntValue1, value); }
		}

		/// <summary>The IntValue2 property of the Entity ExternalDeliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalDeliverypoint"."IntValue2".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> IntValue2
		{
			get { return (Nullable<System.Int32>)GetValue((int)ExternalDeliverypointFieldIndex.IntValue2, false); }
			set	{ SetValue((int)ExternalDeliverypointFieldIndex.IntValue2, value); }
		}

		/// <summary>The IntValue3 property of the Entity ExternalDeliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalDeliverypoint"."IntValue3".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> IntValue3
		{
			get { return (Nullable<System.Int32>)GetValue((int)ExternalDeliverypointFieldIndex.IntValue3, false); }
			set	{ SetValue((int)ExternalDeliverypointFieldIndex.IntValue3, value); }
		}

		/// <summary>The IntValue4 property of the Entity ExternalDeliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalDeliverypoint"."IntValue4".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> IntValue4
		{
			get { return (Nullable<System.Int32>)GetValue((int)ExternalDeliverypointFieldIndex.IntValue4, false); }
			set	{ SetValue((int)ExternalDeliverypointFieldIndex.IntValue4, value); }
		}

		/// <summary>The IntValue5 property of the Entity ExternalDeliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalDeliverypoint"."IntValue5".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> IntValue5
		{
			get { return (Nullable<System.Int32>)GetValue((int)ExternalDeliverypointFieldIndex.IntValue5, false); }
			set	{ SetValue((int)ExternalDeliverypointFieldIndex.IntValue5, value); }
		}

		/// <summary>The BoolValue1 property of the Entity ExternalDeliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalDeliverypoint"."BoolValue1".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> BoolValue1
		{
			get { return (Nullable<System.Boolean>)GetValue((int)ExternalDeliverypointFieldIndex.BoolValue1, false); }
			set	{ SetValue((int)ExternalDeliverypointFieldIndex.BoolValue1, value); }
		}

		/// <summary>The BoolValue2 property of the Entity ExternalDeliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalDeliverypoint"."BoolValue2".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> BoolValue2
		{
			get { return (Nullable<System.Boolean>)GetValue((int)ExternalDeliverypointFieldIndex.BoolValue2, false); }
			set	{ SetValue((int)ExternalDeliverypointFieldIndex.BoolValue2, value); }
		}

		/// <summary>The BoolValue3 property of the Entity ExternalDeliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalDeliverypoint"."BoolValue3".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> BoolValue3
		{
			get { return (Nullable<System.Boolean>)GetValue((int)ExternalDeliverypointFieldIndex.BoolValue3, false); }
			set	{ SetValue((int)ExternalDeliverypointFieldIndex.BoolValue3, value); }
		}

		/// <summary>The BoolValue4 property of the Entity ExternalDeliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalDeliverypoint"."BoolValue4".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> BoolValue4
		{
			get { return (Nullable<System.Boolean>)GetValue((int)ExternalDeliverypointFieldIndex.BoolValue4, false); }
			set	{ SetValue((int)ExternalDeliverypointFieldIndex.BoolValue4, value); }
		}

		/// <summary>The BoolValue5 property of the Entity ExternalDeliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalDeliverypoint"."BoolValue5".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> BoolValue5
		{
			get { return (Nullable<System.Boolean>)GetValue((int)ExternalDeliverypointFieldIndex.BoolValue5, false); }
			set	{ SetValue((int)ExternalDeliverypointFieldIndex.BoolValue5, value); }
		}

		/// <summary>The CreatedInBatchId property of the Entity ExternalDeliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalDeliverypoint"."CreatedInBatchId".<br/>Table field type characteristics (type, precision, scale, length): BigInt, 19, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CreatedInBatchId
		{
			get { return (Nullable<System.Int64>)GetValue((int)ExternalDeliverypointFieldIndex.CreatedInBatchId, false); }
			set	{ SetValue((int)ExternalDeliverypointFieldIndex.CreatedInBatchId, value); }
		}

		/// <summary>The UpdatedInBatchId property of the Entity ExternalDeliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalDeliverypoint"."UpdatedInBatchId".<br/>Table field type characteristics (type, precision, scale, length): BigInt, 19, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> UpdatedInBatchId
		{
			get { return (Nullable<System.Int64>)GetValue((int)ExternalDeliverypointFieldIndex.UpdatedInBatchId, false); }
			set	{ SetValue((int)ExternalDeliverypointFieldIndex.UpdatedInBatchId, value); }
		}

		/// <summary>The SynchronisationBatchId property of the Entity ExternalDeliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalDeliverypoint"."SynchronisationBatchId".<br/>Table field type characteristics (type, precision, scale, length): BigInt, 19, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> SynchronisationBatchId
		{
			get { return (Nullable<System.Int64>)GetValue((int)ExternalDeliverypointFieldIndex.SynchronisationBatchId, false); }
			set	{ SetValue((int)ExternalDeliverypointFieldIndex.SynchronisationBatchId, value); }
		}

		/// <summary>The CreatedUTC property of the Entity ExternalDeliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalDeliverypoint"."CreatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ExternalDeliverypointFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)ExternalDeliverypointFieldIndex.CreatedUTC, value); }
		}

		/// <summary>The CreatedBy property of the Entity ExternalDeliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalDeliverypoint"."CreatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CreatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)ExternalDeliverypointFieldIndex.CreatedBy, false); }
			set	{ SetValue((int)ExternalDeliverypointFieldIndex.CreatedBy, value); }
		}

		/// <summary>The UpdatedUTC property of the Entity ExternalDeliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalDeliverypoint"."UpdatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ExternalDeliverypointFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)ExternalDeliverypointFieldIndex.UpdatedUTC, value); }
		}

		/// <summary>The UpdatedBy property of the Entity ExternalDeliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalDeliverypoint"."UpdatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UpdatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)ExternalDeliverypointFieldIndex.UpdatedBy, false); }
			set	{ SetValue((int)ExternalDeliverypointFieldIndex.UpdatedBy, value); }
		}

		/// <summary>Gets the EntityCollection with the related entities of type 'DeliverypointExternalDeliverypointEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(DeliverypointExternalDeliverypointEntity))]
		public virtual EntityCollection<DeliverypointExternalDeliverypointEntity> DeliverypointExternalDeliverypointCollection { get { return GetOrCreateEntityCollection<DeliverypointExternalDeliverypointEntity, DeliverypointExternalDeliverypointEntityFactory>("ExternalDeliverypoint", true, false, ref _deliverypointExternalDeliverypointCollection); } }

		/// <summary>Gets / sets related entity of type 'ExternalSystemEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual ExternalSystemEntity ExternalSystem
		{
			get { return _externalSystem; }
			set { SetSingleRelatedEntityNavigator(value, "ExternalSystem"); }
		}


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen
{
	public enum ExternalDeliverypointFieldIndex
	{
		///<summary>ExternalDeliverypointId. </summary>
		ExternalDeliverypointId,
		///<summary>ExternalSystemId. </summary>
		ExternalSystemId,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>Id. </summary>
		Id,
		///<summary>Name. </summary>
		Name,
		///<summary>Visible. </summary>
		Visible,
		///<summary>StringValue1. </summary>
		StringValue1,
		///<summary>StringValue2. </summary>
		StringValue2,
		///<summary>StringValue3. </summary>
		StringValue3,
		///<summary>StringValue4. </summary>
		StringValue4,
		///<summary>StringValue5. </summary>
		StringValue5,
		///<summary>IntValue1. </summary>
		IntValue1,
		///<summary>IntValue2. </summary>
		IntValue2,
		///<summary>IntValue3. </summary>
		IntValue3,
		///<summary>IntValue4. </summary>
		IntValue4,
		///<summary>IntValue5. </summary>
		IntValue5,
		///<summary>BoolValue1. </summary>
		BoolValue1,
		///<summary>BoolValue2. </summary>
		BoolValue2,
		///<summary>BoolValue3. </summary>
		BoolValue3,
		///<summary>BoolValue4. </summary>
		BoolValue4,
		///<summary>BoolValue5. </summary>
		BoolValue5,
		///<summary>CreatedInBatchId. </summary>
		CreatedInBatchId,
		///<summary>UpdatedInBatchId. </summary>
		UpdatedInBatchId,
		///<summary>SynchronisationBatchId. </summary>
		SynchronisationBatchId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		/// <summary></summary>
		AmountOfFields
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: ExternalDeliverypoint. </summary>
	public partial class ExternalDeliverypointRelations: RelationFactory
	{
		#region Custom Relation code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomRelationCode
        private const ReferentialConstraintDeleteRule DeleteRuleForDeliverypointExternalDeliverypointEntityUsingExternalDeliverypointId = ReferentialConstraintDeleteRule.Cascade;
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		
		// private const ReferentialConstraintDeleteRule DeleteRuleForDeliverypointExternalDeliverypointEntityUsingExternalDeliverypointId = ReferentialConstraintDeleteRule


		/// <summary>Gets all relations and delete rules of the ExternalDeliverypointEntity as a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects.</summary>
		/// <returns>a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects</returns>
		public virtual Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> GetRelationsAndDeleteRules()
		{
			Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> toReturn = new Dictionary<IEntityRelation, ReferentialConstraintDeleteRule>();
			toReturn.Add(this.DeliverypointExternalDeliverypointEntityUsingExternalDeliverypointId, ExternalDeliverypointRelations.DeleteRuleForDeliverypointExternalDeliverypointEntityUsingExternalDeliverypointId);
			return toReturn;
		}		
	
	
		/// <summary>Returns a new IEntityRelation object, between ExternalDeliverypointEntity and DeliverypointExternalDeliverypointEntity over the 1:n relation they have, using the relation between the fields: ExternalDeliverypoint.ExternalDeliverypointId - DeliverypointExternalDeliverypoint.ExternalDeliverypointId</summary>
		public virtual IEntityRelation DeliverypointExternalDeliverypointEntityUsingExternalDeliverypointId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "DeliverypointExternalDeliverypointCollection", true, new[] { ExternalDeliverypointFields.ExternalDeliverypointId, DeliverypointExternalDeliverypointFields.ExternalDeliverypointId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between ExternalDeliverypointEntity and ExternalSystemEntity over the m:1 relation they have, using the relation between the fields: ExternalDeliverypoint.ExternalSystemId - ExternalSystem.ExternalSystemId</summary>
		public virtual IEntityRelation ExternalSystemEntityUsingExternalSystemId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "ExternalSystem", false, new[] { ExternalSystemFields.ExternalSystemId, ExternalDeliverypointFields.ExternalSystemId }); }
		}

	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticExternalDeliverypointRelations
	{
		internal static readonly IEntityRelation DeliverypointExternalDeliverypointEntityUsingExternalDeliverypointIdStatic = new ExternalDeliverypointRelations().DeliverypointExternalDeliverypointEntityUsingExternalDeliverypointId;
		internal static readonly IEntityRelation ExternalSystemEntityUsingExternalSystemIdStatic = new ExternalDeliverypointRelations().ExternalSystemEntityUsingExternalSystemId;

		/// <summary>CTor</summary>
		static StaticExternalDeliverypointRelations() { }
	}
}

