﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 5.8
// Code is generated on: 
// Code is generated using templates: Crave.TemplateBindings
// Templates vendor: Crave Interactive
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.FactoryClasses;
using Crave.Ordering.Shared.Data.LLBLGen.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

// ReSharper disable All
namespace Crave.Ordering.Shared.Data.LLBLGen.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'Alteration'.<br/><br/></summary>
	[Serializable]
	public partial class AlterationEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END
	
	{
		private EntityCollection<AlterationEntity> _alterationCollection;
		private EntityCollection<AlterationitemEntity> _alterationitemCollection;
		private EntityCollection<AlterationitemAlterationEntity> _alterationitemAlterationCollection;
		private EntityCollection<AlterationProductEntity> _alterationProductCollection;
		private EntityCollection<MediaEntity> _mediaCollection;
		private EntityCollection<OrderitemAlterationitemEntity> _orderitemAlterationitemCollection;
		private EntityCollection<PriceLevelItemEntity> _priceLevelItemCollection;
		private EntityCollection<ProductAlterationEntity> _productAlterationCollection;
		private AlterationEntity _parentAlteration;
		private CompanyEntity _company;
		private ExternalProductEntity _externalProduct;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		private static AlterationEntityStaticMetaData _staticMetaData = new AlterationEntityStaticMetaData();
		private static AlterationRelations _relationsFactory = new AlterationRelations();		

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name ParentAlteration</summary>
			public static readonly string ParentAlteration = "ParentAlteration";
			/// <summary>Member name Company</summary>
			public static readonly string Company = "Company";
			/// <summary>Member name ExternalProduct</summary>
			public static readonly string ExternalProduct = "ExternalProduct";
			/// <summary>Member name AlterationCollection</summary>
			public static readonly string AlterationCollection = "AlterationCollection";
			/// <summary>Member name AlterationitemCollection</summary>
			public static readonly string AlterationitemCollection = "AlterationitemCollection";
			/// <summary>Member name AlterationitemAlterationCollection</summary>
			public static readonly string AlterationitemAlterationCollection = "AlterationitemAlterationCollection";
			/// <summary>Member name AlterationProductCollection</summary>
			public static readonly string AlterationProductCollection = "AlterationProductCollection";
			/// <summary>Member name MediaCollection</summary>
			public static readonly string MediaCollection = "MediaCollection";
			/// <summary>Member name OrderitemAlterationitemCollection</summary>
			public static readonly string OrderitemAlterationitemCollection = "OrderitemAlterationitemCollection";
			/// <summary>Member name PriceLevelItemCollection</summary>
			public static readonly string PriceLevelItemCollection = "PriceLevelItemCollection";
			/// <summary>Member name ProductAlterationCollection</summary>
			public static readonly string ProductAlterationCollection = "ProductAlterationCollection";
		}

		/// <summary>Static meta-data storage for navigator related information</summary>
		protected class AlterationEntityStaticMetaData : EntityStaticMetaDataBase
		{
			public AlterationEntityStaticMetaData()
			{
				SetEntityCoreInfo("AlterationEntity", InheritanceHierarchyType.None, false, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.AlterationEntity, typeof(AlterationEntity), typeof(AlterationEntityFactory), false);
				AddNavigatorMetaData<AlterationEntity, EntityCollection<AlterationEntity>>("AlterationCollection", a => a._alterationCollection, (a, b) => a._alterationCollection = b, a => a.AlterationCollection, () => new AlterationRelations().AlterationEntityUsingParentAlterationId, typeof(AlterationEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.AlterationEntity);
				AddNavigatorMetaData<AlterationEntity, EntityCollection<AlterationitemEntity>>("AlterationitemCollection", a => a._alterationitemCollection, (a, b) => a._alterationitemCollection = b, a => a.AlterationitemCollection, () => new AlterationRelations().AlterationitemEntityUsingAlterationId, typeof(AlterationitemEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.AlterationitemEntity);
				AddNavigatorMetaData<AlterationEntity, EntityCollection<AlterationitemAlterationEntity>>("AlterationitemAlterationCollection", a => a._alterationitemAlterationCollection, (a, b) => a._alterationitemAlterationCollection = b, a => a.AlterationitemAlterationCollection, () => new AlterationRelations().AlterationitemAlterationEntityUsingAlterationId, typeof(AlterationitemAlterationEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.AlterationitemAlterationEntity);
				AddNavigatorMetaData<AlterationEntity, EntityCollection<AlterationProductEntity>>("AlterationProductCollection", a => a._alterationProductCollection, (a, b) => a._alterationProductCollection = b, a => a.AlterationProductCollection, () => new AlterationRelations().AlterationProductEntityUsingAlterationId, typeof(AlterationProductEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.AlterationProductEntity);
				AddNavigatorMetaData<AlterationEntity, EntityCollection<MediaEntity>>("MediaCollection", a => a._mediaCollection, (a, b) => a._mediaCollection = b, a => a.MediaCollection, () => new AlterationRelations().MediaEntityUsingAlterationId, typeof(MediaEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.MediaEntity);
				AddNavigatorMetaData<AlterationEntity, EntityCollection<OrderitemAlterationitemEntity>>("OrderitemAlterationitemCollection", a => a._orderitemAlterationitemCollection, (a, b) => a._orderitemAlterationitemCollection = b, a => a.OrderitemAlterationitemCollection, () => new AlterationRelations().OrderitemAlterationitemEntityUsingAlterationId, typeof(OrderitemAlterationitemEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.OrderitemAlterationitemEntity);
				AddNavigatorMetaData<AlterationEntity, EntityCollection<PriceLevelItemEntity>>("PriceLevelItemCollection", a => a._priceLevelItemCollection, (a, b) => a._priceLevelItemCollection = b, a => a.PriceLevelItemCollection, () => new AlterationRelations().PriceLevelItemEntityUsingAlterationId, typeof(PriceLevelItemEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.PriceLevelItemEntity);
				AddNavigatorMetaData<AlterationEntity, EntityCollection<ProductAlterationEntity>>("ProductAlterationCollection", a => a._productAlterationCollection, (a, b) => a._productAlterationCollection = b, a => a.ProductAlterationCollection, () => new AlterationRelations().ProductAlterationEntityUsingAlterationId, typeof(ProductAlterationEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ProductAlterationEntity);
				AddNavigatorMetaData<AlterationEntity, AlterationEntity>("ParentAlteration", "AlterationCollection", (a, b) => a._parentAlteration = b, a => a._parentAlteration, (a, b) => a.ParentAlteration = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticAlterationRelations.AlterationEntityUsingAlterationIdParentAlterationIdStatic, ()=>new AlterationRelations().AlterationEntityUsingAlterationIdParentAlterationId, null, new int[] { (int)AlterationFieldIndex.ParentAlterationId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.AlterationEntity);
				AddNavigatorMetaData<AlterationEntity, CompanyEntity>("Company", "AlterationCollection", (a, b) => a._company = b, a => a._company, (a, b) => a.Company = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticAlterationRelations.CompanyEntityUsingCompanyIdStatic, ()=>new AlterationRelations().CompanyEntityUsingCompanyId, null, new int[] { (int)AlterationFieldIndex.CompanyId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.CompanyEntity);
				AddNavigatorMetaData<AlterationEntity, ExternalProductEntity>("ExternalProduct", "AlterationCollection", (a, b) => a._externalProduct = b, a => a._externalProduct, (a, b) => a.ExternalProduct = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticAlterationRelations.ExternalProductEntityUsingExternalProductIdStatic, ()=>new AlterationRelations().ExternalProductEntityUsingExternalProductId, null, new int[] { (int)AlterationFieldIndex.ExternalProductId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ExternalProductEntity);
			}
		}

		/// <summary>Static ctor</summary>
		static AlterationEntity()
		{
		}

		/// <summary> CTor</summary>
		public AlterationEntity()
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public AlterationEntity(IEntityFields2 fields)
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this AlterationEntity</param>
		public AlterationEntity(IValidator validator)
		{
			InitClassEmpty(validator, null);
		}

		/// <summary> CTor</summary>
		/// <param name="alterationId">PK value for Alteration which data should be fetched into this Alteration object</param>
		public AlterationEntity(System.Int32 alterationId) : this(alterationId, null)
		{
		}

		/// <summary> CTor</summary>
		/// <param name="alterationId">PK value for Alteration which data should be fetched into this Alteration object</param>
		/// <param name="validator">The custom validator object for this AlterationEntity</param>
		public AlterationEntity(System.Int32 alterationId, IValidator validator)
		{
			InitClassEmpty(validator, null);
			this.AlterationId = alterationId;
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected AlterationEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Alteration' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoAlterationCollection() { return CreateRelationInfoForNavigator("AlterationCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Alterationitem' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoAlterationitemCollection() { return CreateRelationInfoForNavigator("AlterationitemCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'AlterationitemAlteration' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoAlterationitemAlterationCollection() { return CreateRelationInfoForNavigator("AlterationitemAlterationCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'AlterationProduct' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoAlterationProductCollection() { return CreateRelationInfoForNavigator("AlterationProductCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Media' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoMediaCollection() { return CreateRelationInfoForNavigator("MediaCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'OrderitemAlterationitem' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoOrderitemAlterationitemCollection() { return CreateRelationInfoForNavigator("OrderitemAlterationitemCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'PriceLevelItem' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoPriceLevelItemCollection() { return CreateRelationInfoForNavigator("PriceLevelItemCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'ProductAlteration' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoProductAlterationCollection() { return CreateRelationInfoForNavigator("ProductAlterationCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Alteration' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoParentAlteration() { return CreateRelationInfoForNavigator("ParentAlteration"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Company' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoCompany() { return CreateRelationInfoForNavigator("Company"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'ExternalProduct' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoExternalProduct() { return CreateRelationInfoForNavigator("ExternalProduct"); }
		
		/// <inheritdoc/>
		protected override EntityStaticMetaDataBase GetEntityStaticMetaData() {	return _staticMetaData; }

		/// <summary>Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		/// <summary>Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this AlterationEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary>The relations object holding all relations of this entity with other entity classes.</summary>
		public static AlterationRelations Relations { get { return _relationsFactory; } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Alteration' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathAlterationCollection { get { return _staticMetaData.GetPrefetchPathElement("AlterationCollection", CommonEntityBase.CreateEntityCollection<AlterationEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Alterationitem' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathAlterationitemCollection { get { return _staticMetaData.GetPrefetchPathElement("AlterationitemCollection", CommonEntityBase.CreateEntityCollection<AlterationitemEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'AlterationitemAlteration' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathAlterationitemAlterationCollection { get { return _staticMetaData.GetPrefetchPathElement("AlterationitemAlterationCollection", CommonEntityBase.CreateEntityCollection<AlterationitemAlterationEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'AlterationProduct' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathAlterationProductCollection { get { return _staticMetaData.GetPrefetchPathElement("AlterationProductCollection", CommonEntityBase.CreateEntityCollection<AlterationProductEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Media' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathMediaCollection { get { return _staticMetaData.GetPrefetchPathElement("MediaCollection", CommonEntityBase.CreateEntityCollection<MediaEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'OrderitemAlterationitem' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathOrderitemAlterationitemCollection { get { return _staticMetaData.GetPrefetchPathElement("OrderitemAlterationitemCollection", CommonEntityBase.CreateEntityCollection<OrderitemAlterationitemEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'PriceLevelItem' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathPriceLevelItemCollection { get { return _staticMetaData.GetPrefetchPathElement("PriceLevelItemCollection", CommonEntityBase.CreateEntityCollection<PriceLevelItemEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'ProductAlteration' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathProductAlterationCollection { get { return _staticMetaData.GetPrefetchPathElement("ProductAlterationCollection", CommonEntityBase.CreateEntityCollection<ProductAlterationEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Alteration' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathParentAlterationEntity { get { return _staticMetaData.GetPrefetchPathElement("ParentAlteration", CommonEntityBase.CreateEntityCollection<AlterationEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Company' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathCompanyEntity { get { return _staticMetaData.GetPrefetchPathElement("Company", CommonEntityBase.CreateEntityCollection<CompanyEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'ExternalProduct' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathExternalProductEntity { get { return _staticMetaData.GetPrefetchPathElement("ExternalProduct", CommonEntityBase.CreateEntityCollection<ExternalProductEntity>()); } }

		/// <summary>The AlterationId property of the Entity Alteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alteration"."AlterationId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 AlterationId
		{
			get { return (System.Int32)GetValue((int)AlterationFieldIndex.AlterationId, true); }
			set { SetValue((int)AlterationFieldIndex.AlterationId, value); }		}

		/// <summary>The ParentAlterationId property of the Entity Alteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alteration"."ParentAlterationId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ParentAlterationId
		{
			get { return (Nullable<System.Int32>)GetValue((int)AlterationFieldIndex.ParentAlterationId, false); }
			set	{ SetValue((int)AlterationFieldIndex.ParentAlterationId, value); }
		}

		/// <summary>The Version property of the Entity Alteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alteration"."Version".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 Version
		{
			get { return (System.Int32)GetValue((int)AlterationFieldIndex.Version, true); }
			set	{ SetValue((int)AlterationFieldIndex.Version, value); }
		}

		/// <summary>The Name property of the Entity Alteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alteration"."Name".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)AlterationFieldIndex.Name, true); }
			set	{ SetValue((int)AlterationFieldIndex.Name, value); }
		}

		/// <summary>The FriendlyName property of the Entity Alteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alteration"."FriendlyName".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FriendlyName
		{
			get { return (System.String)GetValue((int)AlterationFieldIndex.FriendlyName, true); }
			set	{ SetValue((int)AlterationFieldIndex.FriendlyName, value); }
		}

		/// <summary>The Description property of the Entity Alteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alteration"."Description".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)AlterationFieldIndex.Description, true); }
			set	{ SetValue((int)AlterationFieldIndex.Description, value); }
		}

		/// <summary>The Type property of the Entity Alteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alteration"."Type".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Crave.Enums.AlterationType Type
		{
			get { return (Crave.Enums.AlterationType)GetValue((int)AlterationFieldIndex.Type, true); }
			set	{ SetValue((int)AlterationFieldIndex.Type, value); }
		}

		/// <summary>The LayoutType property of the Entity Alteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alteration"."LayoutType".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Crave.Enums.AlterationLayoutType LayoutType
		{
			get { return (Crave.Enums.AlterationLayoutType)GetValue((int)AlterationFieldIndex.LayoutType, true); }
			set	{ SetValue((int)AlterationFieldIndex.LayoutType, value); }
		}

		/// <summary>The MinOptions property of the Entity Alteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alteration"."MinOptions".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 MinOptions
		{
			get { return (System.Int32)GetValue((int)AlterationFieldIndex.MinOptions, true); }
			set	{ SetValue((int)AlterationFieldIndex.MinOptions, value); }
		}

		/// <summary>The MaxOptions property of the Entity Alteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alteration"."MaxOptions".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 MaxOptions
		{
			get { return (System.Int32)GetValue((int)AlterationFieldIndex.MaxOptions, true); }
			set	{ SetValue((int)AlterationFieldIndex.MaxOptions, value); }
		}

		/// <summary>The GenericalterationId property of the Entity Alteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alteration"."GenericalterationId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> GenericalterationId
		{
			get { return (Nullable<System.Int32>)GetValue((int)AlterationFieldIndex.GenericalterationId, false); }
			set	{ SetValue((int)AlterationFieldIndex.GenericalterationId, value); }
		}

		/// <summary>The PosalterationId property of the Entity Alteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alteration"."PosalterationId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> PosalterationId
		{
			get { return (Nullable<System.Int32>)GetValue((int)AlterationFieldIndex.PosalterationId, false); }
			set	{ SetValue((int)AlterationFieldIndex.PosalterationId, value); }
		}

		/// <summary>The CompanyId property of the Entity Alteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alteration"."CompanyId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CompanyId
		{
			get { return (Nullable<System.Int32>)GetValue((int)AlterationFieldIndex.CompanyId, false); }
			set	{ SetValue((int)AlterationFieldIndex.CompanyId, value); }
		}

		/// <summary>The AvailableOnOtoucho property of the Entity Alteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alteration"."AvailableOnOtoucho".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean AvailableOnOtoucho
		{
			get { return (System.Boolean)GetValue((int)AlterationFieldIndex.AvailableOnOtoucho, true); }
			set	{ SetValue((int)AlterationFieldIndex.AvailableOnOtoucho, value); }
		}

		/// <summary>The AvailableOnObymobi property of the Entity Alteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alteration"."AvailableOnObymobi".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean AvailableOnObymobi
		{
			get { return (System.Boolean)GetValue((int)AlterationFieldIndex.AvailableOnObymobi, true); }
			set	{ SetValue((int)AlterationFieldIndex.AvailableOnObymobi, value); }
		}

		/// <summary>The StartTime property of the Entity Alteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alteration"."StartTime".<br/>Table field type characteristics (type, precision, scale, length): DateTime, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> StartTime
		{
			get { return (Nullable<System.DateTime>)GetValue((int)AlterationFieldIndex.StartTime, false); }
			set	{ SetValue((int)AlterationFieldIndex.StartTime, value); }
		}

		/// <summary>The EndTime property of the Entity Alteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alteration"."EndTime".<br/>Table field type characteristics (type, precision, scale, length): DateTime, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> EndTime
		{
			get { return (Nullable<System.DateTime>)GetValue((int)AlterationFieldIndex.EndTime, false); }
			set	{ SetValue((int)AlterationFieldIndex.EndTime, value); }
		}

		/// <summary>The MinLeadMinutes property of the Entity Alteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alteration"."MinLeadMinutes".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 MinLeadMinutes
		{
			get { return (System.Int32)GetValue((int)AlterationFieldIndex.MinLeadMinutes, true); }
			set	{ SetValue((int)AlterationFieldIndex.MinLeadMinutes, value); }
		}

		/// <summary>The MaxLeadHours property of the Entity Alteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alteration"."MaxLeadHours".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 MaxLeadHours
		{
			get { return (System.Int32)GetValue((int)AlterationFieldIndex.MaxLeadHours, true); }
			set	{ SetValue((int)AlterationFieldIndex.MaxLeadHours, value); }
		}

		/// <summary>The IntervalMinutes property of the Entity Alteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alteration"."IntervalMinutes".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 IntervalMinutes
		{
			get { return (System.Int32)GetValue((int)AlterationFieldIndex.IntervalMinutes, true); }
			set	{ SetValue((int)AlterationFieldIndex.IntervalMinutes, value); }
		}

		/// <summary>The ShowDatePicker property of the Entity Alteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alteration"."ShowDatePicker".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean ShowDatePicker
		{
			get { return (System.Boolean)GetValue((int)AlterationFieldIndex.ShowDatePicker, true); }
			set	{ SetValue((int)AlterationFieldIndex.ShowDatePicker, value); }
		}

		/// <summary>The Value property of the Entity Alteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alteration"."Value".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Value
		{
			get { return (System.String)GetValue((int)AlterationFieldIndex.Value, true); }
			set	{ SetValue((int)AlterationFieldIndex.Value, value); }
		}

		/// <summary>The OrderLevelEnabled property of the Entity Alteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alteration"."OrderLevelEnabled".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean OrderLevelEnabled
		{
			get { return (System.Boolean)GetValue((int)AlterationFieldIndex.OrderLevelEnabled, true); }
			set	{ SetValue((int)AlterationFieldIndex.OrderLevelEnabled, value); }
		}

		/// <summary>The SortOrder property of the Entity Alteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alteration"."SortOrder".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 SortOrder
		{
			get { return (System.Int32)GetValue((int)AlterationFieldIndex.SortOrder, true); }
			set	{ SetValue((int)AlterationFieldIndex.SortOrder, value); }
		}

		/// <summary>The Visible property of the Entity Alteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alteration"."Visible".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean Visible
		{
			get { return (System.Boolean)GetValue((int)AlterationFieldIndex.Visible, true); }
			set	{ SetValue((int)AlterationFieldIndex.Visible, value); }
		}

		/// <summary>The PriceAddition property of the Entity Alteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alteration"."PriceAddition".<br/>Table field type characteristics (type, precision, scale, length): Money, 19, 4, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal PriceAddition
		{
			get { return (System.Decimal)GetValue((int)AlterationFieldIndex.PriceAddition, true); }
			set	{ SetValue((int)AlterationFieldIndex.PriceAddition, value); }
		}

		/// <summary>The OriginalAlterationId property of the Entity Alteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alteration"."OriginalAlterationId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> OriginalAlterationId
		{
			get { return (Nullable<System.Int32>)GetValue((int)AlterationFieldIndex.OriginalAlterationId, false); }
			set	{ SetValue((int)AlterationFieldIndex.OriginalAlterationId, value); }
		}

		/// <summary>The AllowDuplicates property of the Entity Alteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alteration"."AllowDuplicates".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean AllowDuplicates
		{
			get { return (System.Boolean)GetValue((int)AlterationFieldIndex.AllowDuplicates, true); }
			set	{ SetValue((int)AlterationFieldIndex.AllowDuplicates, value); }
		}

		/// <summary>The StartTimeUTC property of the Entity Alteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alteration"."StartTimeUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> StartTimeUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)AlterationFieldIndex.StartTimeUTC, false); }
			set	{ SetValue((int)AlterationFieldIndex.StartTimeUTC, value); }
		}

		/// <summary>The EndTimeUTC property of the Entity Alteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alteration"."EndTimeUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> EndTimeUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)AlterationFieldIndex.EndTimeUTC, false); }
			set	{ SetValue((int)AlterationFieldIndex.EndTimeUTC, value); }
		}

		/// <summary>The CreatedUTC property of the Entity Alteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alteration"."CreatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)AlterationFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)AlterationFieldIndex.CreatedUTC, value); }
		}

		/// <summary>The CreatedBy property of the Entity Alteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alteration"."CreatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CreatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)AlterationFieldIndex.CreatedBy, false); }
			set	{ SetValue((int)AlterationFieldIndex.CreatedBy, value); }
		}

		/// <summary>The UpdatedUTC property of the Entity Alteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alteration"."UpdatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)AlterationFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)AlterationFieldIndex.UpdatedUTC, value); }
		}

		/// <summary>The UpdatedBy property of the Entity Alteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alteration"."UpdatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UpdatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)AlterationFieldIndex.UpdatedBy, false); }
			set	{ SetValue((int)AlterationFieldIndex.UpdatedBy, value); }
		}

		/// <summary>The ExternalProductId property of the Entity Alteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alteration"."ExternalProductId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ExternalProductId
		{
			get { return (Nullable<System.Int32>)GetValue((int)AlterationFieldIndex.ExternalProductId, false); }
			set	{ SetValue((int)AlterationFieldIndex.ExternalProductId, value); }
		}

		/// <summary>The IsAvailable property of the Entity Alteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alteration"."IsAvailable".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsAvailable
		{
			get { return (System.Boolean)GetValue((int)AlterationFieldIndex.IsAvailable, true); }
			set	{ SetValue((int)AlterationFieldIndex.IsAvailable, value); }
		}

		/// <summary>Gets the EntityCollection with the related entities of type 'AlterationEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(AlterationEntity))]
		public virtual EntityCollection<AlterationEntity> AlterationCollection { get { return GetOrCreateEntityCollection<AlterationEntity, AlterationEntityFactory>("ParentAlteration", true, false, ref _alterationCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'AlterationitemEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(AlterationitemEntity))]
		public virtual EntityCollection<AlterationitemEntity> AlterationitemCollection { get { return GetOrCreateEntityCollection<AlterationitemEntity, AlterationitemEntityFactory>("Alteration", true, false, ref _alterationitemCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'AlterationitemAlterationEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(AlterationitemAlterationEntity))]
		public virtual EntityCollection<AlterationitemAlterationEntity> AlterationitemAlterationCollection { get { return GetOrCreateEntityCollection<AlterationitemAlterationEntity, AlterationitemAlterationEntityFactory>("Alteration", true, false, ref _alterationitemAlterationCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'AlterationProductEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(AlterationProductEntity))]
		public virtual EntityCollection<AlterationProductEntity> AlterationProductCollection { get { return GetOrCreateEntityCollection<AlterationProductEntity, AlterationProductEntityFactory>("Alteration", true, false, ref _alterationProductCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'MediaEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(MediaEntity))]
		public virtual EntityCollection<MediaEntity> MediaCollection { get { return GetOrCreateEntityCollection<MediaEntity, MediaEntityFactory>("Alteration", true, false, ref _mediaCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'OrderitemAlterationitemEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(OrderitemAlterationitemEntity))]
		public virtual EntityCollection<OrderitemAlterationitemEntity> OrderitemAlterationitemCollection { get { return GetOrCreateEntityCollection<OrderitemAlterationitemEntity, OrderitemAlterationitemEntityFactory>("Alteration", true, false, ref _orderitemAlterationitemCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'PriceLevelItemEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(PriceLevelItemEntity))]
		public virtual EntityCollection<PriceLevelItemEntity> PriceLevelItemCollection { get { return GetOrCreateEntityCollection<PriceLevelItemEntity, PriceLevelItemEntityFactory>("Alteration", true, false, ref _priceLevelItemCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'ProductAlterationEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(ProductAlterationEntity))]
		public virtual EntityCollection<ProductAlterationEntity> ProductAlterationCollection { get { return GetOrCreateEntityCollection<ProductAlterationEntity, ProductAlterationEntityFactory>("Alteration", true, false, ref _productAlterationCollection); } }

		/// <summary>Gets / sets related entity of type 'AlterationEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual AlterationEntity ParentAlteration
		{
			get { return _parentAlteration; }
			set { SetSingleRelatedEntityNavigator(value, "ParentAlteration"); }
		}

		/// <summary>Gets / sets related entity of type 'CompanyEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual CompanyEntity Company
		{
			get { return _company; }
			set { SetSingleRelatedEntityNavigator(value, "Company"); }
		}

		/// <summary>Gets / sets related entity of type 'ExternalProductEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual ExternalProductEntity ExternalProduct
		{
			get { return _externalProduct; }
			set { SetSingleRelatedEntityNavigator(value, "ExternalProduct"); }
		}


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen
{
	public enum AlterationFieldIndex
	{
		///<summary>AlterationId. </summary>
		AlterationId,
		///<summary>ParentAlterationId. </summary>
		ParentAlterationId,
		///<summary>Version. </summary>
		Version,
		///<summary>Name. </summary>
		Name,
		///<summary>FriendlyName. </summary>
		FriendlyName,
		///<summary>Description. </summary>
		Description,
		///<summary>Type. </summary>
		Type,
		///<summary>LayoutType. </summary>
		LayoutType,
		///<summary>MinOptions. </summary>
		MinOptions,
		///<summary>MaxOptions. </summary>
		MaxOptions,
		///<summary>GenericalterationId. </summary>
		GenericalterationId,
		///<summary>PosalterationId. </summary>
		PosalterationId,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>AvailableOnOtoucho. </summary>
		AvailableOnOtoucho,
		///<summary>AvailableOnObymobi. </summary>
		AvailableOnObymobi,
		///<summary>StartTime. </summary>
		StartTime,
		///<summary>EndTime. </summary>
		EndTime,
		///<summary>MinLeadMinutes. </summary>
		MinLeadMinutes,
		///<summary>MaxLeadHours. </summary>
		MaxLeadHours,
		///<summary>IntervalMinutes. </summary>
		IntervalMinutes,
		///<summary>ShowDatePicker. </summary>
		ShowDatePicker,
		///<summary>Value. </summary>
		Value,
		///<summary>OrderLevelEnabled. </summary>
		OrderLevelEnabled,
		///<summary>SortOrder. </summary>
		SortOrder,
		///<summary>Visible. </summary>
		Visible,
		///<summary>PriceAddition. </summary>
		PriceAddition,
		///<summary>OriginalAlterationId. </summary>
		OriginalAlterationId,
		///<summary>AllowDuplicates. </summary>
		AllowDuplicates,
		///<summary>StartTimeUTC. </summary>
		StartTimeUTC,
		///<summary>EndTimeUTC. </summary>
		EndTimeUTC,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>ExternalProductId. </summary>
		ExternalProductId,
		///<summary>IsAvailable. </summary>
		IsAvailable,
		/// <summary></summary>
		AmountOfFields
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Alteration. </summary>
	public partial class AlterationRelations: RelationFactory
	{
		#region Custom Relation code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomRelationCode
        private const ReferentialConstraintDeleteRule DeleteRuleForAlterationEntityUsingParentAlterationId = ReferentialConstraintDeleteRule.SetNull;
        private const ReferentialConstraintDeleteRule DeleteRuleForAlterationitemEntityUsingAlterationId = ReferentialConstraintDeleteRule.Cascade;
        private const ReferentialConstraintDeleteRule DeleteRuleForAlterationitemAlterationEntityUsingAlterationId = ReferentialConstraintDeleteRule.Cascade;
        private const ReferentialConstraintDeleteRule DeleteRuleForAlterationProductEntityUsingAlterationId = ReferentialConstraintDeleteRule.Cascade;
        private const ReferentialConstraintDeleteRule DeleteRuleForMediaEntityUsingAlterationId = ReferentialConstraintDeleteRule.Cascade;
		private const ReferentialConstraintDeleteRule DeleteRuleForPriceLevelItemEntityUsingAlterationId = ReferentialConstraintDeleteRule.Cascade;
        private const ReferentialConstraintDeleteRule DeleteRuleForProductAlterationEntityUsingAlterationId = ReferentialConstraintDeleteRule.Cascade;
        private const ReferentialConstraintDeleteRule DeleteRuleForOrderitemAlterationitemEntityUsingAlterationId = ReferentialConstraintDeleteRule.SetNull;
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		
		// private const ReferentialConstraintDeleteRule DeleteRuleForAlterationEntityUsingParentAlterationId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForAlterationitemEntityUsingAlterationId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForAlterationitemAlterationEntityUsingAlterationId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForAlterationProductEntityUsingAlterationId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForMediaEntityUsingAlterationId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForOrderitemAlterationitemEntityUsingAlterationId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForPriceLevelItemEntityUsingAlterationId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForProductAlterationEntityUsingAlterationId = ReferentialConstraintDeleteRule


		/// <summary>Gets all relations and delete rules of the AlterationEntity as a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects.</summary>
		/// <returns>a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects</returns>
		public virtual Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> GetRelationsAndDeleteRules()
		{
			Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> toReturn = new Dictionary<IEntityRelation, ReferentialConstraintDeleteRule>();
			toReturn.Add(this.AlterationEntityUsingParentAlterationId, AlterationRelations.DeleteRuleForAlterationEntityUsingParentAlterationId);
			toReturn.Add(this.AlterationitemEntityUsingAlterationId, AlterationRelations.DeleteRuleForAlterationitemEntityUsingAlterationId);
			toReturn.Add(this.AlterationitemAlterationEntityUsingAlterationId, AlterationRelations.DeleteRuleForAlterationitemAlterationEntityUsingAlterationId);
			toReturn.Add(this.AlterationProductEntityUsingAlterationId, AlterationRelations.DeleteRuleForAlterationProductEntityUsingAlterationId);
			toReturn.Add(this.MediaEntityUsingAlterationId, AlterationRelations.DeleteRuleForMediaEntityUsingAlterationId);
			toReturn.Add(this.OrderitemAlterationitemEntityUsingAlterationId, AlterationRelations.DeleteRuleForOrderitemAlterationitemEntityUsingAlterationId);
			toReturn.Add(this.PriceLevelItemEntityUsingAlterationId, AlterationRelations.DeleteRuleForPriceLevelItemEntityUsingAlterationId);
			toReturn.Add(this.ProductAlterationEntityUsingAlterationId, AlterationRelations.DeleteRuleForProductAlterationEntityUsingAlterationId);
			return toReturn;
		}		
	
	
		/// <summary>Returns a new IEntityRelation object, between AlterationEntity and AlterationEntity over the 1:n relation they have, using the relation between the fields: Alteration.AlterationId - Alteration.ParentAlterationId</summary>
		public virtual IEntityRelation AlterationEntityUsingParentAlterationId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "AlterationCollection", true, new[] { AlterationFields.AlterationId, AlterationFields.ParentAlterationId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between AlterationEntity and AlterationitemEntity over the 1:n relation they have, using the relation between the fields: Alteration.AlterationId - Alterationitem.AlterationId</summary>
		public virtual IEntityRelation AlterationitemEntityUsingAlterationId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "AlterationitemCollection", true, new[] { AlterationFields.AlterationId, AlterationitemFields.AlterationId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between AlterationEntity and AlterationitemAlterationEntity over the 1:n relation they have, using the relation between the fields: Alteration.AlterationId - AlterationitemAlteration.AlterationId</summary>
		public virtual IEntityRelation AlterationitemAlterationEntityUsingAlterationId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "AlterationitemAlterationCollection", true, new[] { AlterationFields.AlterationId, AlterationitemAlterationFields.AlterationId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between AlterationEntity and AlterationProductEntity over the 1:n relation they have, using the relation between the fields: Alteration.AlterationId - AlterationProduct.AlterationId</summary>
		public virtual IEntityRelation AlterationProductEntityUsingAlterationId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "AlterationProductCollection", true, new[] { AlterationFields.AlterationId, AlterationProductFields.AlterationId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between AlterationEntity and MediaEntity over the 1:n relation they have, using the relation between the fields: Alteration.AlterationId - Media.AlterationId</summary>
		public virtual IEntityRelation MediaEntityUsingAlterationId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "MediaCollection", true, new[] { AlterationFields.AlterationId, MediaFields.AlterationId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between AlterationEntity and OrderitemAlterationitemEntity over the 1:n relation they have, using the relation between the fields: Alteration.AlterationId - OrderitemAlterationitem.AlterationId</summary>
		public virtual IEntityRelation OrderitemAlterationitemEntityUsingAlterationId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "OrderitemAlterationitemCollection", true, new[] { AlterationFields.AlterationId, OrderitemAlterationitemFields.AlterationId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between AlterationEntity and PriceLevelItemEntity over the 1:n relation they have, using the relation between the fields: Alteration.AlterationId - PriceLevelItem.AlterationId</summary>
		public virtual IEntityRelation PriceLevelItemEntityUsingAlterationId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "PriceLevelItemCollection", true, new[] { AlterationFields.AlterationId, PriceLevelItemFields.AlterationId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between AlterationEntity and ProductAlterationEntity over the 1:n relation they have, using the relation between the fields: Alteration.AlterationId - ProductAlteration.AlterationId</summary>
		public virtual IEntityRelation ProductAlterationEntityUsingAlterationId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "ProductAlterationCollection", true, new[] { AlterationFields.AlterationId, ProductAlterationFields.AlterationId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between AlterationEntity and AlterationEntity over the m:1 relation they have, using the relation between the fields: Alteration.ParentAlterationId - Alteration.AlterationId</summary>
		public virtual IEntityRelation AlterationEntityUsingAlterationIdParentAlterationId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "ParentAlteration", false, new[] { AlterationFields.AlterationId, AlterationFields.ParentAlterationId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between AlterationEntity and CompanyEntity over the m:1 relation they have, using the relation between the fields: Alteration.CompanyId - Company.CompanyId</summary>
		public virtual IEntityRelation CompanyEntityUsingCompanyId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Company", false, new[] { CompanyFields.CompanyId, AlterationFields.CompanyId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between AlterationEntity and ExternalProductEntity over the m:1 relation they have, using the relation between the fields: Alteration.ExternalProductId - ExternalProduct.ExternalProductId</summary>
		public virtual IEntityRelation ExternalProductEntityUsingExternalProductId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "ExternalProduct", false, new[] { ExternalProductFields.ExternalProductId, AlterationFields.ExternalProductId }); }
		}

	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticAlterationRelations
	{
		internal static readonly IEntityRelation AlterationEntityUsingParentAlterationIdStatic = new AlterationRelations().AlterationEntityUsingParentAlterationId;
		internal static readonly IEntityRelation AlterationitemEntityUsingAlterationIdStatic = new AlterationRelations().AlterationitemEntityUsingAlterationId;
		internal static readonly IEntityRelation AlterationitemAlterationEntityUsingAlterationIdStatic = new AlterationRelations().AlterationitemAlterationEntityUsingAlterationId;
		internal static readonly IEntityRelation AlterationProductEntityUsingAlterationIdStatic = new AlterationRelations().AlterationProductEntityUsingAlterationId;
		internal static readonly IEntityRelation MediaEntityUsingAlterationIdStatic = new AlterationRelations().MediaEntityUsingAlterationId;
		internal static readonly IEntityRelation OrderitemAlterationitemEntityUsingAlterationIdStatic = new AlterationRelations().OrderitemAlterationitemEntityUsingAlterationId;
		internal static readonly IEntityRelation PriceLevelItemEntityUsingAlterationIdStatic = new AlterationRelations().PriceLevelItemEntityUsingAlterationId;
		internal static readonly IEntityRelation ProductAlterationEntityUsingAlterationIdStatic = new AlterationRelations().ProductAlterationEntityUsingAlterationId;
		internal static readonly IEntityRelation AlterationEntityUsingAlterationIdParentAlterationIdStatic = new AlterationRelations().AlterationEntityUsingAlterationIdParentAlterationId;
		internal static readonly IEntityRelation CompanyEntityUsingCompanyIdStatic = new AlterationRelations().CompanyEntityUsingCompanyId;
		internal static readonly IEntityRelation ExternalProductEntityUsingExternalProductIdStatic = new AlterationRelations().ExternalProductEntityUsingExternalProductId;

		/// <summary>CTor</summary>
		static StaticAlterationRelations() { }
	}
}

