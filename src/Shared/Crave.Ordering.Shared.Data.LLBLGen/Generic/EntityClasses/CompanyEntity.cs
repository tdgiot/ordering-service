﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 5.8
// Code is generated on: 
// Code is generated using templates: Crave.TemplateBindings
// Templates vendor: Crave Interactive
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.FactoryClasses;
using Crave.Ordering.Shared.Data.LLBLGen.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

// ReSharper disable All
namespace Crave.Ordering.Shared.Data.LLBLGen.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'Company'.<br/><br/></summary>
	[Serializable]
	public partial class CompanyEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END
	
	{
		private EntityCollection<AlterationEntity> _alterationCollection;
		private EntityCollection<AlterationoptionEntity> _alterationoptionCollection;
		private EntityCollection<ApplicationConfigurationEntity> _applicationConfigurationCollection;
		private EntityCollection<FeatureFlagEntity> _featureFlagCollection;
		private EntityCollection<BusinesshourEntity> _businesshourCollection;
		private EntityCollection<BusinesshoursexceptionEntity> _businesshoursexceptionCollection;
		private EntityCollection<CategoryEntity> _categoryCollection;
		private EntityCollection<CheckoutMethodEntity> _checkoutMethodCollection;
		private EntityCollection<CheckoutMethodDeliverypointgroupEntity> _checkoutMethodDeliverypointgroupCollection;
		private EntityCollection<ClientEntity> _clientCollection;
		private EntityCollection<ClientConfigurationEntity> _clientConfigurationCollection;
		private EntityCollection<DeliverypointEntity> _deliverypointCollection;
		private EntityCollection<DeliverypointgroupEntity> _deliverypointgroupCollection;
		private EntityCollection<ExternalMenuEntity> _externalMenuCollection;
		private EntityCollection<ExternalSystemEntity> _externalSystemCollection;
		private EntityCollection<MediaEntity> _mediaCollection;
		private EntityCollection<MenuEntity> _menuCollection;
		private EntityCollection<NetmessageEntity> _netmessageCollection;
		private EntityCollection<NetmessageEntity> _netmessageCollection1;
		private EntityCollection<OptInEntity> _optInCollection;
		private EntityCollection<OrderEntity> _orderCollection;
		private EntityCollection<OutletEntity> _outletCollection;
		private EntityCollection<PaymentIntegrationConfigurationEntity> _paymentIntegrationConfigurationCollection;
		private EntityCollection<PaymentProviderEntity> _paymentProviderCollection;
		private EntityCollection<PriceLevelEntity> _priceLevelCollection;
		private EntityCollection<PriceScheduleEntity> _priceScheduleCollection;
		private EntityCollection<ProductEntity> _productCollection;
		private EntityCollection<ReceiptEntity> _receiptCollection;
		private EntityCollection<ReceiptTemplateEntity> _receiptTemplateCollection;
		private EntityCollection<RouteEntity> _routeCollection;
		private EntityCollection<ScheduleEntity> _scheduleCollection;
		private EntityCollection<ServiceMethodEntity> _serviceMethodCollection;
		private EntityCollection<ServiceMethodDeliverypointgroupEntity> _serviceMethodDeliverypointgroupCollection;
		private EntityCollection<TagEntity> _tagCollection;
		private EntityCollection<TaxTariffEntity> _taxTariffCollection;
		private EntityCollection<TerminalEntity> _terminalCollection;
		private EntityCollection<ClientConfigurationEntity> _clientConfigurationCollectionViaDeliverypoint;
		private RouteEntity _route;
		private SupportpoolEntity _supportpool;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		private static CompanyEntityStaticMetaData _staticMetaData = new CompanyEntityStaticMetaData();
		private static CompanyRelations _relationsFactory = new CompanyRelations();		

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Route</summary>
			public static readonly string Route = "Route";
			/// <summary>Member name Supportpool</summary>
			public static readonly string Supportpool = "Supportpool";
			/// <summary>Member name AlterationCollection</summary>
			public static readonly string AlterationCollection = "AlterationCollection";
			/// <summary>Member name AlterationoptionCollection</summary>
			public static readonly string AlterationoptionCollection = "AlterationoptionCollection";
			/// <summary>Member name ApplicationConfigurationCollection</summary>
			public static readonly string ApplicationConfigurationCollection = "ApplicationConfigurationCollection";
			/// <summary>Member name FeatureFlagCollection</summary>
			public static readonly string FeatureFlagCollection = "FeatureFlagCollection";
			/// <summary>Member name BusinesshourCollection</summary>
			public static readonly string BusinesshourCollection = "BusinesshourCollection";
			/// <summary>Member name BusinesshoursexceptionCollection</summary>
			public static readonly string BusinesshoursexceptionCollection = "BusinesshoursexceptionCollection";
			/// <summary>Member name CategoryCollection</summary>
			public static readonly string CategoryCollection = "CategoryCollection";
			/// <summary>Member name CheckoutMethodCollection</summary>
			public static readonly string CheckoutMethodCollection = "CheckoutMethodCollection";
			/// <summary>Member name CheckoutMethodDeliverypointgroupCollection</summary>
			public static readonly string CheckoutMethodDeliverypointgroupCollection = "CheckoutMethodDeliverypointgroupCollection";
			/// <summary>Member name ClientCollection</summary>
			public static readonly string ClientCollection = "ClientCollection";
			/// <summary>Member name ClientConfigurationCollection</summary>
			public static readonly string ClientConfigurationCollection = "ClientConfigurationCollection";
			/// <summary>Member name DeliverypointCollection</summary>
			public static readonly string DeliverypointCollection = "DeliverypointCollection";
			/// <summary>Member name DeliverypointgroupCollection</summary>
			public static readonly string DeliverypointgroupCollection = "DeliverypointgroupCollection";
			/// <summary>Member name ExternalMenuCollection</summary>
			public static readonly string ExternalMenuCollection = "ExternalMenuCollection";
			/// <summary>Member name ExternalSystemCollection</summary>
			public static readonly string ExternalSystemCollection = "ExternalSystemCollection";
			/// <summary>Member name MediaCollection</summary>
			public static readonly string MediaCollection = "MediaCollection";
			/// <summary>Member name MenuCollection</summary>
			public static readonly string MenuCollection = "MenuCollection";
			/// <summary>Member name NetmessageCollection</summary>
			public static readonly string NetmessageCollection = "NetmessageCollection";
			/// <summary>Member name NetmessageCollection1</summary>
			public static readonly string NetmessageCollection1 = "NetmessageCollection1";
			/// <summary>Member name OptInCollection</summary>
			public static readonly string OptInCollection = "OptInCollection";
			/// <summary>Member name OrderCollection</summary>
			public static readonly string OrderCollection = "OrderCollection";
			/// <summary>Member name OutletCollection</summary>
			public static readonly string OutletCollection = "OutletCollection";
			/// <summary>Member name PaymentIntegrationConfigurationCollection</summary>
			public static readonly string PaymentIntegrationConfigurationCollection = "PaymentIntegrationConfigurationCollection";
			/// <summary>Member name PaymentProviderCollection</summary>
			public static readonly string PaymentProviderCollection = "PaymentProviderCollection";
			/// <summary>Member name PriceLevelCollection</summary>
			public static readonly string PriceLevelCollection = "PriceLevelCollection";
			/// <summary>Member name PriceScheduleCollection</summary>
			public static readonly string PriceScheduleCollection = "PriceScheduleCollection";
			/// <summary>Member name ProductCollection</summary>
			public static readonly string ProductCollection = "ProductCollection";
			/// <summary>Member name ReceiptCollection</summary>
			public static readonly string ReceiptCollection = "ReceiptCollection";
			/// <summary>Member name ReceiptTemplateCollection</summary>
			public static readonly string ReceiptTemplateCollection = "ReceiptTemplateCollection";
			/// <summary>Member name RouteCollection</summary>
			public static readonly string RouteCollection = "RouteCollection";
			/// <summary>Member name ScheduleCollection</summary>
			public static readonly string ScheduleCollection = "ScheduleCollection";
			/// <summary>Member name ServiceMethodCollection</summary>
			public static readonly string ServiceMethodCollection = "ServiceMethodCollection";
			/// <summary>Member name ServiceMethodDeliverypointgroupCollection</summary>
			public static readonly string ServiceMethodDeliverypointgroupCollection = "ServiceMethodDeliverypointgroupCollection";
			/// <summary>Member name TagCollection</summary>
			public static readonly string TagCollection = "TagCollection";
			/// <summary>Member name TaxTariffCollection</summary>
			public static readonly string TaxTariffCollection = "TaxTariffCollection";
			/// <summary>Member name TerminalCollection</summary>
			public static readonly string TerminalCollection = "TerminalCollection";
			/// <summary>Member name ClientConfigurationCollectionViaDeliverypoint</summary>
			public static readonly string ClientConfigurationCollectionViaDeliverypoint = "ClientConfigurationCollectionViaDeliverypoint";
		}

		/// <summary>Static meta-data storage for navigator related information</summary>
		protected class CompanyEntityStaticMetaData : EntityStaticMetaDataBase
		{
			public CompanyEntityStaticMetaData()
			{
				SetEntityCoreInfo("CompanyEntity", InheritanceHierarchyType.None, false, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.CompanyEntity, typeof(CompanyEntity), typeof(CompanyEntityFactory), false);
				AddNavigatorMetaData<CompanyEntity, EntityCollection<AlterationEntity>>("AlterationCollection", a => a._alterationCollection, (a, b) => a._alterationCollection = b, a => a.AlterationCollection, () => new CompanyRelations().AlterationEntityUsingCompanyId, typeof(AlterationEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.AlterationEntity);
				AddNavigatorMetaData<CompanyEntity, EntityCollection<AlterationoptionEntity>>("AlterationoptionCollection", a => a._alterationoptionCollection, (a, b) => a._alterationoptionCollection = b, a => a.AlterationoptionCollection, () => new CompanyRelations().AlterationoptionEntityUsingCompanyId, typeof(AlterationoptionEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.AlterationoptionEntity);
				AddNavigatorMetaData<CompanyEntity, EntityCollection<ApplicationConfigurationEntity>>("ApplicationConfigurationCollection", a => a._applicationConfigurationCollection, (a, b) => a._applicationConfigurationCollection = b, a => a.ApplicationConfigurationCollection, () => new CompanyRelations().ApplicationConfigurationEntityUsingCompanyId, typeof(ApplicationConfigurationEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ApplicationConfigurationEntity);
				AddNavigatorMetaData<CompanyEntity, EntityCollection<FeatureFlagEntity>>("FeatureFlagCollection", a => a._featureFlagCollection, (a, b) => a._featureFlagCollection = b, a => a.FeatureFlagCollection, () => new CompanyRelations().FeatureFlagEntityUsingCompanyId, typeof(FeatureFlagEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.FeatureFlagEntity);
				AddNavigatorMetaData<CompanyEntity, EntityCollection<BusinesshourEntity>>("BusinesshourCollection", a => a._businesshourCollection, (a, b) => a._businesshourCollection = b, a => a.BusinesshourCollection, () => new CompanyRelations().BusinesshourEntityUsingCompanyId, typeof(BusinesshourEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.BusinesshourEntity);
				AddNavigatorMetaData<CompanyEntity, EntityCollection<BusinesshoursexceptionEntity>>("BusinesshoursexceptionCollection", a => a._businesshoursexceptionCollection, (a, b) => a._businesshoursexceptionCollection = b, a => a.BusinesshoursexceptionCollection, () => new CompanyRelations().BusinesshoursexceptionEntityUsingCompanyId, typeof(BusinesshoursexceptionEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.BusinesshoursexceptionEntity);
				AddNavigatorMetaData<CompanyEntity, EntityCollection<CategoryEntity>>("CategoryCollection", a => a._categoryCollection, (a, b) => a._categoryCollection = b, a => a.CategoryCollection, () => new CompanyRelations().CategoryEntityUsingCompanyId, typeof(CategoryEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.CategoryEntity);
				AddNavigatorMetaData<CompanyEntity, EntityCollection<CheckoutMethodEntity>>("CheckoutMethodCollection", a => a._checkoutMethodCollection, (a, b) => a._checkoutMethodCollection = b, a => a.CheckoutMethodCollection, () => new CompanyRelations().CheckoutMethodEntityUsingCompanyId, typeof(CheckoutMethodEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.CheckoutMethodEntity);
				AddNavigatorMetaData<CompanyEntity, EntityCollection<CheckoutMethodDeliverypointgroupEntity>>("CheckoutMethodDeliverypointgroupCollection", a => a._checkoutMethodDeliverypointgroupCollection, (a, b) => a._checkoutMethodDeliverypointgroupCollection = b, a => a.CheckoutMethodDeliverypointgroupCollection, () => new CompanyRelations().CheckoutMethodDeliverypointgroupEntityUsingParentCompanyId, typeof(CheckoutMethodDeliverypointgroupEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.CheckoutMethodDeliverypointgroupEntity);
				AddNavigatorMetaData<CompanyEntity, EntityCollection<ClientEntity>>("ClientCollection", a => a._clientCollection, (a, b) => a._clientCollection = b, a => a.ClientCollection, () => new CompanyRelations().ClientEntityUsingCompanyId, typeof(ClientEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ClientEntity);
				AddNavigatorMetaData<CompanyEntity, EntityCollection<ClientConfigurationEntity>>("ClientConfigurationCollection", a => a._clientConfigurationCollection, (a, b) => a._clientConfigurationCollection = b, a => a.ClientConfigurationCollection, () => new CompanyRelations().ClientConfigurationEntityUsingCompanyId, typeof(ClientConfigurationEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ClientConfigurationEntity);
				AddNavigatorMetaData<CompanyEntity, EntityCollection<DeliverypointEntity>>("DeliverypointCollection", a => a._deliverypointCollection, (a, b) => a._deliverypointCollection = b, a => a.DeliverypointCollection, () => new CompanyRelations().DeliverypointEntityUsingCompanyId, typeof(DeliverypointEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.DeliverypointEntity);
				AddNavigatorMetaData<CompanyEntity, EntityCollection<DeliverypointgroupEntity>>("DeliverypointgroupCollection", a => a._deliverypointgroupCollection, (a, b) => a._deliverypointgroupCollection = b, a => a.DeliverypointgroupCollection, () => new CompanyRelations().DeliverypointgroupEntityUsingCompanyId, typeof(DeliverypointgroupEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.DeliverypointgroupEntity);
				AddNavigatorMetaData<CompanyEntity, EntityCollection<ExternalMenuEntity>>("ExternalMenuCollection", a => a._externalMenuCollection, (a, b) => a._externalMenuCollection = b, a => a.ExternalMenuCollection, () => new CompanyRelations().ExternalMenuEntityUsingParentCompanyId, typeof(ExternalMenuEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ExternalMenuEntity);
				AddNavigatorMetaData<CompanyEntity, EntityCollection<ExternalSystemEntity>>("ExternalSystemCollection", a => a._externalSystemCollection, (a, b) => a._externalSystemCollection = b, a => a.ExternalSystemCollection, () => new CompanyRelations().ExternalSystemEntityUsingCompanyId, typeof(ExternalSystemEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ExternalSystemEntity);
				AddNavigatorMetaData<CompanyEntity, EntityCollection<MediaEntity>>("MediaCollection", a => a._mediaCollection, (a, b) => a._mediaCollection = b, a => a.MediaCollection, () => new CompanyRelations().MediaEntityUsingCompanyId, typeof(MediaEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.MediaEntity);
				AddNavigatorMetaData<CompanyEntity, EntityCollection<MenuEntity>>("MenuCollection", a => a._menuCollection, (a, b) => a._menuCollection = b, a => a.MenuCollection, () => new CompanyRelations().MenuEntityUsingCompanyId, typeof(MenuEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.MenuEntity);
				AddNavigatorMetaData<CompanyEntity, EntityCollection<NetmessageEntity>>("NetmessageCollection", a => a._netmessageCollection, (a, b) => a._netmessageCollection = b, a => a.NetmessageCollection, () => new CompanyRelations().NetmessageEntityUsingSenderCompanyId, typeof(NetmessageEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.NetmessageEntity);
				AddNavigatorMetaData<CompanyEntity, EntityCollection<NetmessageEntity>>("NetmessageCollection1", a => a._netmessageCollection1, (a, b) => a._netmessageCollection1 = b, a => a.NetmessageCollection1, () => new CompanyRelations().NetmessageEntityUsingReceiverCompanyId, typeof(NetmessageEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.NetmessageEntity);
				AddNavigatorMetaData<CompanyEntity, EntityCollection<OptInEntity>>("OptInCollection", a => a._optInCollection, (a, b) => a._optInCollection = b, a => a.OptInCollection, () => new CompanyRelations().OptInEntityUsingParentCompanyId, typeof(OptInEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.OptInEntity);
				AddNavigatorMetaData<CompanyEntity, EntityCollection<OrderEntity>>("OrderCollection", a => a._orderCollection, (a, b) => a._orderCollection = b, a => a.OrderCollection, () => new CompanyRelations().OrderEntityUsingCompanyId, typeof(OrderEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.OrderEntity);
				AddNavigatorMetaData<CompanyEntity, EntityCollection<OutletEntity>>("OutletCollection", a => a._outletCollection, (a, b) => a._outletCollection = b, a => a.OutletCollection, () => new CompanyRelations().OutletEntityUsingCompanyId, typeof(OutletEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.OutletEntity);
				AddNavigatorMetaData<CompanyEntity, EntityCollection<PaymentIntegrationConfigurationEntity>>("PaymentIntegrationConfigurationCollection", a => a._paymentIntegrationConfigurationCollection, (a, b) => a._paymentIntegrationConfigurationCollection = b, a => a.PaymentIntegrationConfigurationCollection, () => new CompanyRelations().PaymentIntegrationConfigurationEntityUsingCompanyId, typeof(PaymentIntegrationConfigurationEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.PaymentIntegrationConfigurationEntity);
				AddNavigatorMetaData<CompanyEntity, EntityCollection<PaymentProviderEntity>>("PaymentProviderCollection", a => a._paymentProviderCollection, (a, b) => a._paymentProviderCollection = b, a => a.PaymentProviderCollection, () => new CompanyRelations().PaymentProviderEntityUsingCompanyId, typeof(PaymentProviderEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.PaymentProviderEntity);
				AddNavigatorMetaData<CompanyEntity, EntityCollection<PriceLevelEntity>>("PriceLevelCollection", a => a._priceLevelCollection, (a, b) => a._priceLevelCollection = b, a => a.PriceLevelCollection, () => new CompanyRelations().PriceLevelEntityUsingCompanyId, typeof(PriceLevelEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.PriceLevelEntity);
				AddNavigatorMetaData<CompanyEntity, EntityCollection<PriceScheduleEntity>>("PriceScheduleCollection", a => a._priceScheduleCollection, (a, b) => a._priceScheduleCollection = b, a => a.PriceScheduleCollection, () => new CompanyRelations().PriceScheduleEntityUsingCompanyId, typeof(PriceScheduleEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.PriceScheduleEntity);
				AddNavigatorMetaData<CompanyEntity, EntityCollection<ProductEntity>>("ProductCollection", a => a._productCollection, (a, b) => a._productCollection = b, a => a.ProductCollection, () => new CompanyRelations().ProductEntityUsingCompanyId, typeof(ProductEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ProductEntity);
				AddNavigatorMetaData<CompanyEntity, EntityCollection<ReceiptEntity>>("ReceiptCollection", a => a._receiptCollection, (a, b) => a._receiptCollection = b, a => a.ReceiptCollection, () => new CompanyRelations().ReceiptEntityUsingCompanyId, typeof(ReceiptEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ReceiptEntity);
				AddNavigatorMetaData<CompanyEntity, EntityCollection<ReceiptTemplateEntity>>("ReceiptTemplateCollection", a => a._receiptTemplateCollection, (a, b) => a._receiptTemplateCollection = b, a => a.ReceiptTemplateCollection, () => new CompanyRelations().ReceiptTemplateEntityUsingCompanyId, typeof(ReceiptTemplateEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ReceiptTemplateEntity);
				AddNavigatorMetaData<CompanyEntity, EntityCollection<RouteEntity>>("RouteCollection", a => a._routeCollection, (a, b) => a._routeCollection = b, a => a.RouteCollection, () => new CompanyRelations().RouteEntityUsingCompanyId, typeof(RouteEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.RouteEntity);
				AddNavigatorMetaData<CompanyEntity, EntityCollection<ScheduleEntity>>("ScheduleCollection", a => a._scheduleCollection, (a, b) => a._scheduleCollection = b, a => a.ScheduleCollection, () => new CompanyRelations().ScheduleEntityUsingCompanyId, typeof(ScheduleEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ScheduleEntity);
				AddNavigatorMetaData<CompanyEntity, EntityCollection<ServiceMethodEntity>>("ServiceMethodCollection", a => a._serviceMethodCollection, (a, b) => a._serviceMethodCollection = b, a => a.ServiceMethodCollection, () => new CompanyRelations().ServiceMethodEntityUsingCompanyId, typeof(ServiceMethodEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ServiceMethodEntity);
				AddNavigatorMetaData<CompanyEntity, EntityCollection<ServiceMethodDeliverypointgroupEntity>>("ServiceMethodDeliverypointgroupCollection", a => a._serviceMethodDeliverypointgroupCollection, (a, b) => a._serviceMethodDeliverypointgroupCollection = b, a => a.ServiceMethodDeliverypointgroupCollection, () => new CompanyRelations().ServiceMethodDeliverypointgroupEntityUsingParentCompanyId, typeof(ServiceMethodDeliverypointgroupEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ServiceMethodDeliverypointgroupEntity);
				AddNavigatorMetaData<CompanyEntity, EntityCollection<TagEntity>>("TagCollection", a => a._tagCollection, (a, b) => a._tagCollection = b, a => a.TagCollection, () => new CompanyRelations().TagEntityUsingCompanyId, typeof(TagEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.TagEntity);
				AddNavigatorMetaData<CompanyEntity, EntityCollection<TaxTariffEntity>>("TaxTariffCollection", a => a._taxTariffCollection, (a, b) => a._taxTariffCollection = b, a => a.TaxTariffCollection, () => new CompanyRelations().TaxTariffEntityUsingCompanyId, typeof(TaxTariffEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.TaxTariffEntity);
				AddNavigatorMetaData<CompanyEntity, EntityCollection<TerminalEntity>>("TerminalCollection", a => a._terminalCollection, (a, b) => a._terminalCollection = b, a => a.TerminalCollection, () => new CompanyRelations().TerminalEntityUsingCompanyId, typeof(TerminalEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.TerminalEntity);
				AddNavigatorMetaData<CompanyEntity, RouteEntity>("Route", "CompanyCollection", (a, b) => a._route = b, a => a._route, (a, b) => a.Route = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticCompanyRelations.RouteEntityUsingRouteIdStatic, ()=>new CompanyRelations().RouteEntityUsingRouteId, null, new int[] { (int)CompanyFieldIndex.RouteId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.RouteEntity);
				AddNavigatorMetaData<CompanyEntity, SupportpoolEntity>("Supportpool", "CompanyCollection", (a, b) => a._supportpool = b, a => a._supportpool, (a, b) => a.Supportpool = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticCompanyRelations.SupportpoolEntityUsingSupportpoolIdStatic, ()=>new CompanyRelations().SupportpoolEntityUsingSupportpoolId, null, new int[] { (int)CompanyFieldIndex.SupportpoolId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.SupportpoolEntity);
				AddNavigatorMetaData<CompanyEntity, EntityCollection<ClientConfigurationEntity>>("ClientConfigurationCollectionViaDeliverypoint", a => a._clientConfigurationCollectionViaDeliverypoint, (a, b) => a._clientConfigurationCollectionViaDeliverypoint = b, a => a.ClientConfigurationCollectionViaDeliverypoint, () => new CompanyRelations().DeliverypointEntityUsingCompanyId, () => new DeliverypointRelations().ClientConfigurationEntityUsingClientConfigurationId, "CompanyEntity__", "Deliverypoint_", typeof(ClientConfigurationEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ClientConfigurationEntity);
			}
		}

		/// <summary>Static ctor</summary>
		static CompanyEntity()
		{
		}

		/// <summary> CTor</summary>
		public CompanyEntity()
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public CompanyEntity(IEntityFields2 fields)
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this CompanyEntity</param>
		public CompanyEntity(IValidator validator)
		{
			InitClassEmpty(validator, null);
		}

		/// <summary> CTor</summary>
		/// <param name="companyId">PK value for Company which data should be fetched into this Company object</param>
		public CompanyEntity(System.Int32 companyId) : this(companyId, null)
		{
		}

		/// <summary> CTor</summary>
		/// <param name="companyId">PK value for Company which data should be fetched into this Company object</param>
		/// <param name="validator">The custom validator object for this CompanyEntity</param>
		public CompanyEntity(System.Int32 companyId, IValidator validator)
		{
			InitClassEmpty(validator, null);
			this.CompanyId = companyId;
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected CompanyEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Alteration' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoAlterationCollection() { return CreateRelationInfoForNavigator("AlterationCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Alterationoption' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoAlterationoptionCollection() { return CreateRelationInfoForNavigator("AlterationoptionCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'ApplicationConfiguration' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoApplicationConfigurationCollection() { return CreateRelationInfoForNavigator("ApplicationConfigurationCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'FeatureFlag' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoFeatureFlagCollection() { return CreateRelationInfoForNavigator("FeatureFlagCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Businesshour' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoBusinesshourCollection() { return CreateRelationInfoForNavigator("BusinesshourCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Businesshoursexception' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoBusinesshoursexceptionCollection() { return CreateRelationInfoForNavigator("BusinesshoursexceptionCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Category' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoCategoryCollection() { return CreateRelationInfoForNavigator("CategoryCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'CheckoutMethod' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoCheckoutMethodCollection() { return CreateRelationInfoForNavigator("CheckoutMethodCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'CheckoutMethodDeliverypointgroup' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoCheckoutMethodDeliverypointgroupCollection() { return CreateRelationInfoForNavigator("CheckoutMethodDeliverypointgroupCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Client' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoClientCollection() { return CreateRelationInfoForNavigator("ClientCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'ClientConfiguration' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoClientConfigurationCollection() { return CreateRelationInfoForNavigator("ClientConfigurationCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Deliverypoint' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoDeliverypointCollection() { return CreateRelationInfoForNavigator("DeliverypointCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Deliverypointgroup' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoDeliverypointgroupCollection() { return CreateRelationInfoForNavigator("DeliverypointgroupCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'ExternalMenu' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoExternalMenuCollection() { return CreateRelationInfoForNavigator("ExternalMenuCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'ExternalSystem' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoExternalSystemCollection() { return CreateRelationInfoForNavigator("ExternalSystemCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Media' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoMediaCollection() { return CreateRelationInfoForNavigator("MediaCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Menu' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoMenuCollection() { return CreateRelationInfoForNavigator("MenuCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Netmessage' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoNetmessageCollection() { return CreateRelationInfoForNavigator("NetmessageCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Netmessage' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoNetmessageCollection1() { return CreateRelationInfoForNavigator("NetmessageCollection1"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'OptIn' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoOptInCollection() { return CreateRelationInfoForNavigator("OptInCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Order' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoOrderCollection() { return CreateRelationInfoForNavigator("OrderCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Outlet' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoOutletCollection() { return CreateRelationInfoForNavigator("OutletCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'PaymentIntegrationConfiguration' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoPaymentIntegrationConfigurationCollection() { return CreateRelationInfoForNavigator("PaymentIntegrationConfigurationCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'PaymentProvider' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoPaymentProviderCollection() { return CreateRelationInfoForNavigator("PaymentProviderCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'PriceLevel' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoPriceLevelCollection() { return CreateRelationInfoForNavigator("PriceLevelCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'PriceSchedule' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoPriceScheduleCollection() { return CreateRelationInfoForNavigator("PriceScheduleCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Product' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoProductCollection() { return CreateRelationInfoForNavigator("ProductCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Receipt' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoReceiptCollection() { return CreateRelationInfoForNavigator("ReceiptCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'ReceiptTemplate' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoReceiptTemplateCollection() { return CreateRelationInfoForNavigator("ReceiptTemplateCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Route' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoRouteCollection() { return CreateRelationInfoForNavigator("RouteCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Schedule' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoScheduleCollection() { return CreateRelationInfoForNavigator("ScheduleCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'ServiceMethod' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoServiceMethodCollection() { return CreateRelationInfoForNavigator("ServiceMethodCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'ServiceMethodDeliverypointgroup' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoServiceMethodDeliverypointgroupCollection() { return CreateRelationInfoForNavigator("ServiceMethodDeliverypointgroupCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Tag' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoTagCollection() { return CreateRelationInfoForNavigator("TagCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'TaxTariff' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoTaxTariffCollection() { return CreateRelationInfoForNavigator("TaxTariffCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Terminal' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoTerminalCollection() { return CreateRelationInfoForNavigator("TerminalCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'ClientConfiguration' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoClientConfigurationCollectionViaDeliverypoint() { return CreateRelationInfoForNavigator("ClientConfigurationCollectionViaDeliverypoint"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Route' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoRoute() { return CreateRelationInfoForNavigator("Route"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Supportpool' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoSupportpool() { return CreateRelationInfoForNavigator("Supportpool"); }
		
		/// <inheritdoc/>
		protected override EntityStaticMetaDataBase GetEntityStaticMetaData() {	return _staticMetaData; }

		/// <summary>Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		/// <summary>Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this CompanyEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary>The relations object holding all relations of this entity with other entity classes.</summary>
		public static CompanyRelations Relations { get { return _relationsFactory; } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Alteration' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathAlterationCollection { get { return _staticMetaData.GetPrefetchPathElement("AlterationCollection", CommonEntityBase.CreateEntityCollection<AlterationEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Alterationoption' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathAlterationoptionCollection { get { return _staticMetaData.GetPrefetchPathElement("AlterationoptionCollection", CommonEntityBase.CreateEntityCollection<AlterationoptionEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'ApplicationConfiguration' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathApplicationConfigurationCollection { get { return _staticMetaData.GetPrefetchPathElement("ApplicationConfigurationCollection", CommonEntityBase.CreateEntityCollection<ApplicationConfigurationEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'FeatureFlag' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathFeatureFlagCollection { get { return _staticMetaData.GetPrefetchPathElement("FeatureFlagCollection", CommonEntityBase.CreateEntityCollection<FeatureFlagEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Businesshour' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathBusinesshourCollection { get { return _staticMetaData.GetPrefetchPathElement("BusinesshourCollection", CommonEntityBase.CreateEntityCollection<BusinesshourEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Businesshoursexception' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathBusinesshoursexceptionCollection { get { return _staticMetaData.GetPrefetchPathElement("BusinesshoursexceptionCollection", CommonEntityBase.CreateEntityCollection<BusinesshoursexceptionEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Category' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathCategoryCollection { get { return _staticMetaData.GetPrefetchPathElement("CategoryCollection", CommonEntityBase.CreateEntityCollection<CategoryEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'CheckoutMethod' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathCheckoutMethodCollection { get { return _staticMetaData.GetPrefetchPathElement("CheckoutMethodCollection", CommonEntityBase.CreateEntityCollection<CheckoutMethodEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'CheckoutMethodDeliverypointgroup' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathCheckoutMethodDeliverypointgroupCollection { get { return _staticMetaData.GetPrefetchPathElement("CheckoutMethodDeliverypointgroupCollection", CommonEntityBase.CreateEntityCollection<CheckoutMethodDeliverypointgroupEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Client' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathClientCollection { get { return _staticMetaData.GetPrefetchPathElement("ClientCollection", CommonEntityBase.CreateEntityCollection<ClientEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'ClientConfiguration' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathClientConfigurationCollection { get { return _staticMetaData.GetPrefetchPathElement("ClientConfigurationCollection", CommonEntityBase.CreateEntityCollection<ClientConfigurationEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Deliverypoint' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathDeliverypointCollection { get { return _staticMetaData.GetPrefetchPathElement("DeliverypointCollection", CommonEntityBase.CreateEntityCollection<DeliverypointEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Deliverypointgroup' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathDeliverypointgroupCollection { get { return _staticMetaData.GetPrefetchPathElement("DeliverypointgroupCollection", CommonEntityBase.CreateEntityCollection<DeliverypointgroupEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'ExternalMenu' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathExternalMenuCollection { get { return _staticMetaData.GetPrefetchPathElement("ExternalMenuCollection", CommonEntityBase.CreateEntityCollection<ExternalMenuEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'ExternalSystem' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathExternalSystemCollection { get { return _staticMetaData.GetPrefetchPathElement("ExternalSystemCollection", CommonEntityBase.CreateEntityCollection<ExternalSystemEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Media' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathMediaCollection { get { return _staticMetaData.GetPrefetchPathElement("MediaCollection", CommonEntityBase.CreateEntityCollection<MediaEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Menu' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathMenuCollection { get { return _staticMetaData.GetPrefetchPathElement("MenuCollection", CommonEntityBase.CreateEntityCollection<MenuEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Netmessage' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathNetmessageCollection { get { return _staticMetaData.GetPrefetchPathElement("NetmessageCollection", CommonEntityBase.CreateEntityCollection<NetmessageEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Netmessage' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathNetmessageCollection1 { get { return _staticMetaData.GetPrefetchPathElement("NetmessageCollection1", CommonEntityBase.CreateEntityCollection<NetmessageEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'OptIn' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathOptInCollection { get { return _staticMetaData.GetPrefetchPathElement("OptInCollection", CommonEntityBase.CreateEntityCollection<OptInEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Order' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathOrderCollection { get { return _staticMetaData.GetPrefetchPathElement("OrderCollection", CommonEntityBase.CreateEntityCollection<OrderEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Outlet' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathOutletCollection { get { return _staticMetaData.GetPrefetchPathElement("OutletCollection", CommonEntityBase.CreateEntityCollection<OutletEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'PaymentIntegrationConfiguration' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathPaymentIntegrationConfigurationCollection { get { return _staticMetaData.GetPrefetchPathElement("PaymentIntegrationConfigurationCollection", CommonEntityBase.CreateEntityCollection<PaymentIntegrationConfigurationEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'PaymentProvider' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathPaymentProviderCollection { get { return _staticMetaData.GetPrefetchPathElement("PaymentProviderCollection", CommonEntityBase.CreateEntityCollection<PaymentProviderEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'PriceLevel' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathPriceLevelCollection { get { return _staticMetaData.GetPrefetchPathElement("PriceLevelCollection", CommonEntityBase.CreateEntityCollection<PriceLevelEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'PriceSchedule' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathPriceScheduleCollection { get { return _staticMetaData.GetPrefetchPathElement("PriceScheduleCollection", CommonEntityBase.CreateEntityCollection<PriceScheduleEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Product' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathProductCollection { get { return _staticMetaData.GetPrefetchPathElement("ProductCollection", CommonEntityBase.CreateEntityCollection<ProductEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Receipt' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathReceiptCollection { get { return _staticMetaData.GetPrefetchPathElement("ReceiptCollection", CommonEntityBase.CreateEntityCollection<ReceiptEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'ReceiptTemplate' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathReceiptTemplateCollection { get { return _staticMetaData.GetPrefetchPathElement("ReceiptTemplateCollection", CommonEntityBase.CreateEntityCollection<ReceiptTemplateEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Route' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathRouteCollection { get { return _staticMetaData.GetPrefetchPathElement("RouteCollection", CommonEntityBase.CreateEntityCollection<RouteEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Schedule' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathScheduleCollection { get { return _staticMetaData.GetPrefetchPathElement("ScheduleCollection", CommonEntityBase.CreateEntityCollection<ScheduleEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'ServiceMethod' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathServiceMethodCollection { get { return _staticMetaData.GetPrefetchPathElement("ServiceMethodCollection", CommonEntityBase.CreateEntityCollection<ServiceMethodEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'ServiceMethodDeliverypointgroup' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathServiceMethodDeliverypointgroupCollection { get { return _staticMetaData.GetPrefetchPathElement("ServiceMethodDeliverypointgroupCollection", CommonEntityBase.CreateEntityCollection<ServiceMethodDeliverypointgroupEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Tag' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathTagCollection { get { return _staticMetaData.GetPrefetchPathElement("TagCollection", CommonEntityBase.CreateEntityCollection<TagEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'TaxTariff' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathTaxTariffCollection { get { return _staticMetaData.GetPrefetchPathElement("TaxTariffCollection", CommonEntityBase.CreateEntityCollection<TaxTariffEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Terminal' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathTerminalCollection { get { return _staticMetaData.GetPrefetchPathElement("TerminalCollection", CommonEntityBase.CreateEntityCollection<TerminalEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'ClientConfiguration' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathClientConfigurationCollectionViaDeliverypoint { get { return _staticMetaData.GetPrefetchPathElement("ClientConfigurationCollectionViaDeliverypoint", CommonEntityBase.CreateEntityCollection<ClientConfigurationEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Route' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathRouteEntity { get { return _staticMetaData.GetPrefetchPathElement("Route", CommonEntityBase.CreateEntityCollection<RouteEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Supportpool' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathSupportpoolEntity { get { return _staticMetaData.GetPrefetchPathElement("Supportpool", CommonEntityBase.CreateEntityCollection<SupportpoolEntity>()); } }

		/// <summary>The CompanyId property of the Entity Company<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Company"."CompanyId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 CompanyId
		{
			get { return (System.Int32)GetValue((int)CompanyFieldIndex.CompanyId, true); }
			set { SetValue((int)CompanyFieldIndex.CompanyId, value); }		}

		/// <summary>The CompanyOwnerId property of the Entity Company<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Company"."CompanyOwnerId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CompanyOwnerId
		{
			get { return (System.Int32)GetValue((int)CompanyFieldIndex.CompanyOwnerId, true); }
			set	{ SetValue((int)CompanyFieldIndex.CompanyOwnerId, value); }
		}

		/// <summary>The RouteId property of the Entity Company<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Company"."RouteId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> RouteId
		{
			get { return (Nullable<System.Int32>)GetValue((int)CompanyFieldIndex.RouteId, false); }
			set	{ SetValue((int)CompanyFieldIndex.RouteId, value); }
		}

		/// <summary>The SupportpoolId property of the Entity Company<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Company"."SupportpoolId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> SupportpoolId
		{
			get { return (Nullable<System.Int32>)GetValue((int)CompanyFieldIndex.SupportpoolId, false); }
			set	{ SetValue((int)CompanyFieldIndex.SupportpoolId, value); }
		}

		/// <summary>The Name property of the Entity Company<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Company"."Name".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 100.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)CompanyFieldIndex.Name, true); }
			set	{ SetValue((int)CompanyFieldIndex.Name, value); }
		}

		/// <summary>The NameWithoutDiacritics property of the Entity Company<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Company"."NameWithoutDiacritics".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 100.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String NameWithoutDiacritics
		{
			get { return (System.String)GetValue((int)CompanyFieldIndex.NameWithoutDiacritics, true); }
			set	{ SetValue((int)CompanyFieldIndex.NameWithoutDiacritics, value); }
		}

		/// <summary>The Code property of the Entity Company<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Company"."Code".<br/>Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 10.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Code
		{
			get { return (System.String)GetValue((int)CompanyFieldIndex.Code, true); }
			set	{ SetValue((int)CompanyFieldIndex.Code, value); }
		}

		/// <summary>The Addressline1 property of the Entity Company<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Company"."Addressline1".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 200.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Addressline1
		{
			get { return (System.String)GetValue((int)CompanyFieldIndex.Addressline1, true); }
			set	{ SetValue((int)CompanyFieldIndex.Addressline1, value); }
		}

		/// <summary>The Addressline2 property of the Entity Company<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Company"."Addressline2".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 200.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Addressline2
		{
			get { return (System.String)GetValue((int)CompanyFieldIndex.Addressline2, true); }
			set	{ SetValue((int)CompanyFieldIndex.Addressline2, value); }
		}

		/// <summary>The Addressline3 property of the Entity Company<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Company"."Addressline3".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 200.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Addressline3
		{
			get { return (System.String)GetValue((int)CompanyFieldIndex.Addressline3, true); }
			set	{ SetValue((int)CompanyFieldIndex.Addressline3, value); }
		}

		/// <summary>The Zipcode property of the Entity Company<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Company"."Zipcode".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Zipcode
		{
			get { return (System.String)GetValue((int)CompanyFieldIndex.Zipcode, true); }
			set	{ SetValue((int)CompanyFieldIndex.Zipcode, value); }
		}

		/// <summary>The City property of the Entity Company<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Company"."City".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String City
		{
			get { return (System.String)GetValue((int)CompanyFieldIndex.City, true); }
			set	{ SetValue((int)CompanyFieldIndex.City, value); }
		}

		/// <summary>The CountryCode property of the Entity Company<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Company"."CountryCode".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 3.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CountryCode
		{
			get { return (System.String)GetValue((int)CompanyFieldIndex.CountryCode, true); }
			set	{ SetValue((int)CompanyFieldIndex.CountryCode, value); }
		}

		/// <summary>The TimeZoneOlsonId property of the Entity Company<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Company"."TimeZoneOlsonId".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String TimeZoneOlsonId
		{
			get { return (System.String)GetValue((int)CompanyFieldIndex.TimeZoneOlsonId, true); }
			set	{ SetValue((int)CompanyFieldIndex.TimeZoneOlsonId, value); }
		}

		/// <summary>The CultureCode property of the Entity Company<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Company"."CultureCode".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 10.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CultureCode
		{
			get { return (System.String)GetValue((int)CompanyFieldIndex.CultureCode, true); }
			set	{ SetValue((int)CompanyFieldIndex.CultureCode, value); }
		}

		/// <summary>The CurrencyCode property of the Entity Company<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Company"."CurrencyCode".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 3.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CurrencyCode
		{
			get { return (System.String)GetValue((int)CompanyFieldIndex.CurrencyCode, true); }
			set	{ SetValue((int)CompanyFieldIndex.CurrencyCode, value); }
		}

		/// <summary>The Latitude property of the Entity Company<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Company"."Latitude".<br/>Table field type characteristics (type, precision, scale, length): Float, 38, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Double> Latitude
		{
			get { return (Nullable<System.Double>)GetValue((int)CompanyFieldIndex.Latitude, false); }
			set	{ SetValue((int)CompanyFieldIndex.Latitude, value); }
		}

		/// <summary>The LatitudeOverridden property of the Entity Company<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Company"."LatitudeOverridden".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean LatitudeOverridden
		{
			get { return (System.Boolean)GetValue((int)CompanyFieldIndex.LatitudeOverridden, true); }
			set	{ SetValue((int)CompanyFieldIndex.LatitudeOverridden, value); }
		}

		/// <summary>The Longitude property of the Entity Company<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Company"."Longitude".<br/>Table field type characteristics (type, precision, scale, length): Float, 38, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Double> Longitude
		{
			get { return (Nullable<System.Double>)GetValue((int)CompanyFieldIndex.Longitude, false); }
			set	{ SetValue((int)CompanyFieldIndex.Longitude, value); }
		}

		/// <summary>The LongitudeOverriden property of the Entity Company<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Company"."LongitudeOverriden".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean LongitudeOverriden
		{
			get { return (System.Boolean)GetValue((int)CompanyFieldIndex.LongitudeOverriden, true); }
			set	{ SetValue((int)CompanyFieldIndex.LongitudeOverriden, value); }
		}

		/// <summary>The Telephone property of the Entity Company<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Company"."Telephone".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Telephone
		{
			get { return (System.String)GetValue((int)CompanyFieldIndex.Telephone, true); }
			set	{ SetValue((int)CompanyFieldIndex.Telephone, value); }
		}

		/// <summary>The Fax property of the Entity Company<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Company"."Fax".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Fax
		{
			get { return (System.String)GetValue((int)CompanyFieldIndex.Fax, true); }
			set	{ SetValue((int)CompanyFieldIndex.Fax, value); }
		}

		/// <summary>The Website property of the Entity Company<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Company"."Website".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Website
		{
			get { return (System.String)GetValue((int)CompanyFieldIndex.Website, true); }
			set	{ SetValue((int)CompanyFieldIndex.Website, value); }
		}

		/// <summary>The Email property of the Entity Company<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Company"."Email".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 100.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Email
		{
			get { return (System.String)GetValue((int)CompanyFieldIndex.Email, true); }
			set	{ SetValue((int)CompanyFieldIndex.Email, value); }
		}

		/// <summary>The GoogleAnalyticsId property of the Entity Company<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Company"."GoogleAnalyticsId".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String GoogleAnalyticsId
		{
			get { return (System.String)GetValue((int)CompanyFieldIndex.GoogleAnalyticsId, true); }
			set	{ SetValue((int)CompanyFieldIndex.GoogleAnalyticsId, value); }
		}

		/// <summary>The StandardCheckInterval property of the Entity Company<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Company"."StandardCheckInterval".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 StandardCheckInterval
		{
			get { return (System.Int32)GetValue((int)CompanyFieldIndex.StandardCheckInterval, true); }
			set	{ SetValue((int)CompanyFieldIndex.StandardCheckInterval, value); }
		}

		/// <summary>The BrowserAgeVerification property of the Entity Company<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Company"."SMSOrdering".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean BrowserAgeVerification
		{
			get { return (System.Boolean)GetValue((int)CompanyFieldIndex.BrowserAgeVerification, true); }
			set	{ SetValue((int)CompanyFieldIndex.BrowserAgeVerification, value); }
		}

		/// <summary>The Description property of the Entity Company<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Company"."Description".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)CompanyFieldIndex.Description, true); }
			set	{ SetValue((int)CompanyFieldIndex.Description, value); }
		}

		/// <summary>The DescriptionSingleLine property of the Entity Company<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Company"."DescriptionSingleLine".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 250.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String DescriptionSingleLine
		{
			get { return (System.String)GetValue((int)CompanyFieldIndex.DescriptionSingleLine, true); }
			set	{ SetValue((int)CompanyFieldIndex.DescriptionSingleLine, value); }
		}

		/// <summary>The AutomaticClientOperationModes property of the Entity Company<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Company"."AutomaticClientOperationModes".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean AutomaticClientOperationModes
		{
			get { return (System.Boolean)GetValue((int)CompanyFieldIndex.AutomaticClientOperationModes, true); }
			set	{ SetValue((int)CompanyFieldIndex.AutomaticClientOperationModes, value); }
		}

		/// <summary>The AvailableOnOtoucho property of the Entity Company<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Company"."AvailableOnOtoucho".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean AvailableOnOtoucho
		{
			get { return (System.Boolean)GetValue((int)CompanyFieldIndex.AvailableOnOtoucho, true); }
			set	{ SetValue((int)CompanyFieldIndex.AvailableOnOtoucho, value); }
		}

		/// <summary>The AvailableOnObymobi property of the Entity Company<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Company"."AvailableOnObymobi".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean AvailableOnObymobi
		{
			get { return (System.Boolean)GetValue((int)CompanyFieldIndex.AvailableOnObymobi, true); }
			set	{ SetValue((int)CompanyFieldIndex.AvailableOnObymobi, value); }
		}

		/// <summary>The MobileOrderingDisabled property of the Entity Company<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Company"."MobileOrderingDisabled".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean MobileOrderingDisabled
		{
			get { return (System.Boolean)GetValue((int)CompanyFieldIndex.MobileOrderingDisabled, true); }
			set	{ SetValue((int)CompanyFieldIndex.MobileOrderingDisabled, value); }
		}

		/// <summary>The UseMonitoring property of the Entity Company<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Company"."UseMonitoring".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean UseMonitoring
		{
			get { return (System.Boolean)GetValue((int)CompanyFieldIndex.UseMonitoring, true); }
			set	{ SetValue((int)CompanyFieldIndex.UseMonitoring, value); }
		}

		/// <summary>The SystemType property of the Entity Company<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Company"."SystemType".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Crave.Enums.SystemType SystemType
		{
			get { return (Crave.Enums.SystemType)GetValue((int)CompanyFieldIndex.SystemType, true); }
			set	{ SetValue((int)CompanyFieldIndex.SystemType, value); }
		}

		/// <summary>The AllowRequestService property of the Entity Company<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Company"."AllowRequestService".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> AllowRequestService
		{
			get { return (Nullable<System.Boolean>)GetValue((int)CompanyFieldIndex.AllowRequestService, false); }
			set	{ SetValue((int)CompanyFieldIndex.AllowRequestService, value); }
		}

		/// <summary>The AllowRateProduct property of the Entity Company<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Company"."AllowRateProduct".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> AllowRateProduct
		{
			get { return (Nullable<System.Boolean>)GetValue((int)CompanyFieldIndex.AllowRateProduct, false); }
			set	{ SetValue((int)CompanyFieldIndex.AllowRateProduct, value); }
		}

		/// <summary>The AllowRateService property of the Entity Company<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Company"."AllowRateService".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> AllowRateService
		{
			get { return (Nullable<System.Boolean>)GetValue((int)CompanyFieldIndex.AllowRateService, false); }
			set	{ SetValue((int)CompanyFieldIndex.AllowRateService, value); }
		}

		/// <summary>The AllowFreeText property of the Entity Company<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Company"."AllowFreeText".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean AllowFreeText
		{
			get { return (System.Boolean)GetValue((int)CompanyFieldIndex.AllowFreeText, true); }
			set	{ SetValue((int)CompanyFieldIndex.AllowFreeText, value); }
		}

		/// <summary>The UseBillButton property of the Entity Company<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Company"."UseBillButton".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean UseBillButton
		{
			get { return (System.Boolean)GetValue((int)CompanyFieldIndex.UseBillButton, true); }
			set	{ SetValue((int)CompanyFieldIndex.UseBillButton, value); }
		}

		/// <summary>The Salt property of the Entity Company<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Company"."Salt".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Salt
		{
			get { return (System.String)GetValue((int)CompanyFieldIndex.Salt, true); }
			set	{ SetValue((int)CompanyFieldIndex.Salt, value); }
		}

		/// <summary>The SaltPms property of the Entity Company<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Company"."SaltPms".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String SaltPms
		{
			get { return (System.String)GetValue((int)CompanyFieldIndex.SaltPms, true); }
			set	{ SetValue((int)CompanyFieldIndex.SaltPms, value); }
		}

		/// <summary>The MaxDownloadConnections property of the Entity Company<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Company"."MaxDownloadConnections".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 MaxDownloadConnections
		{
			get { return (System.Int32)GetValue((int)CompanyFieldIndex.MaxDownloadConnections, true); }
			set	{ SetValue((int)CompanyFieldIndex.MaxDownloadConnections, value); }
		}

		/// <summary>The ClientApplicationVersion property of the Entity Company<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Company"."ClientApplicationVersion".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 250.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ClientApplicationVersion
		{
			get { return (System.String)GetValue((int)CompanyFieldIndex.ClientApplicationVersion, true); }
			set	{ SetValue((int)CompanyFieldIndex.ClientApplicationVersion, value); }
		}

		/// <summary>The TerminalApplicationVersion property of the Entity Company<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Company"."TerminalApplicationVersion".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 250.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String TerminalApplicationVersion
		{
			get { return (System.String)GetValue((int)CompanyFieldIndex.TerminalApplicationVersion, true); }
			set	{ SetValue((int)CompanyFieldIndex.TerminalApplicationVersion, value); }
		}

		/// <summary>The OsVersion property of the Entity Company<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Company"."OsVersion".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 250.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String OsVersion
		{
			get { return (System.String)GetValue((int)CompanyFieldIndex.OsVersion, true); }
			set	{ SetValue((int)CompanyFieldIndex.OsVersion, value); }
		}

		/// <summary>The AgentVersion property of the Entity Company<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Company"."AgentVersion".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 250.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String AgentVersion
		{
			get { return (System.String)GetValue((int)CompanyFieldIndex.AgentVersion, true); }
			set	{ SetValue((int)CompanyFieldIndex.AgentVersion, value); }
		}

		/// <summary>The SupportToolsVersion property of the Entity Company<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Company"."SupportToolsVersion".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 250.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String SupportToolsVersion
		{
			get { return (System.String)GetValue((int)CompanyFieldIndex.SupportToolsVersion, true); }
			set	{ SetValue((int)CompanyFieldIndex.SupportToolsVersion, value); }
		}

		/// <summary>The DeviceRebootMethod property of the Entity Company<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Company"."DeviceRebootMethod".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Crave.Enums.DeviceRebootMethod DeviceRebootMethod
		{
			get { return (Crave.Enums.DeviceRebootMethod)GetValue((int)CompanyFieldIndex.DeviceRebootMethod, true); }
			set	{ SetValue((int)CompanyFieldIndex.DeviceRebootMethod, value); }
		}

		/// <summary>The CorrespondenceEmail property of the Entity Company<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Company"."CorrespondenceEmail".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CorrespondenceEmail
		{
			get { return (System.String)GetValue((int)CompanyFieldIndex.CorrespondenceEmail, true); }
			set	{ SetValue((int)CompanyFieldIndex.CorrespondenceEmail, value); }
		}

		/// <summary>The ShowCurrencySymbol property of the Entity Company<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Company"."ShowCurrencySymbol".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean ShowCurrencySymbol
		{
			get { return (System.Boolean)GetValue((int)CompanyFieldIndex.ShowCurrencySymbol, true); }
			set	{ SetValue((int)CompanyFieldIndex.ShowCurrencySymbol, value); }
		}

		/// <summary>The Pincode property of the Entity Company<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Company"."Pincode".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Pincode
		{
			get { return (System.String)GetValue((int)CompanyFieldIndex.Pincode, true); }
			set	{ SetValue((int)CompanyFieldIndex.Pincode, value); }
		}

		/// <summary>The PincodeSU property of the Entity Company<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Company"."PincodeSU".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PincodeSU
		{
			get { return (System.String)GetValue((int)CompanyFieldIndex.PincodeSU, true); }
			set	{ SetValue((int)CompanyFieldIndex.PincodeSU, value); }
		}

		/// <summary>The PincodeGM property of the Entity Company<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Company"."PincodeGM".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PincodeGM
		{
			get { return (System.String)GetValue((int)CompanyFieldIndex.PincodeGM, true); }
			set	{ SetValue((int)CompanyFieldIndex.PincodeGM, value); }
		}

		/// <summary>The ActionButtonId property of the Entity Company<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Company"."ActionButtonId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ActionButtonId
		{
			get { return (Nullable<System.Int32>)GetValue((int)CompanyFieldIndex.ActionButtonId, false); }
			set	{ SetValue((int)CompanyFieldIndex.ActionButtonId, value); }
		}

		/// <summary>The ActionButtonUrlTablet property of the Entity Company<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Company"."ActionButtonUrlTablet".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ActionButtonUrlTablet
		{
			get { return (System.String)GetValue((int)CompanyFieldIndex.ActionButtonUrlTablet, true); }
			set	{ SetValue((int)CompanyFieldIndex.ActionButtonUrlTablet, value); }
		}

		/// <summary>The ActionButtonUrlMobile property of the Entity Company<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Company"."ActionButtonUrlMobile".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ActionButtonUrlMobile
		{
			get { return (System.String)GetValue((int)CompanyFieldIndex.ActionButtonUrlMobile, true); }
			set	{ SetValue((int)CompanyFieldIndex.ActionButtonUrlMobile, value); }
		}

		/// <summary>The CostIndicationType property of the Entity Company<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Company"."CostIndicationType".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Crave.Enums.CostIndicationType CostIndicationType
		{
			get { return (Crave.Enums.CostIndicationType)GetValue((int)CompanyFieldIndex.CostIndicationType, true); }
			set	{ SetValue((int)CompanyFieldIndex.CostIndicationType, value); }
		}

		/// <summary>The CostIndicationValue property of the Entity Company<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Company"."CostIndicationValue".<br/>Table field type characteristics (type, precision, scale, length): Money, 19, 4, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Decimal> CostIndicationValue
		{
			get { return (Nullable<System.Decimal>)GetValue((int)CompanyFieldIndex.CostIndicationValue, false); }
			set	{ SetValue((int)CompanyFieldIndex.CostIndicationValue, value); }
		}

		/// <summary>The CreatedBy property of the Entity Company<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Company"."CreatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)CompanyFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)CompanyFieldIndex.CreatedBy, value); }
		}

		/// <summary>The UpdatedBy property of the Entity Company<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Company"."UpdatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)CompanyFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)CompanyFieldIndex.UpdatedBy, value); }
		}

		/// <summary>The GeoFencingEnabled property of the Entity Company<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Company"."GeoFencingEnabled".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean GeoFencingEnabled
		{
			get { return (System.Boolean)GetValue((int)CompanyFieldIndex.GeoFencingEnabled, true); }
			set	{ SetValue((int)CompanyFieldIndex.GeoFencingEnabled, value); }
		}

		/// <summary>The GeoFencingRadius property of the Entity Company<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Company"."GeoFencingRadius".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 GeoFencingRadius
		{
			get { return (System.Int32)GetValue((int)CompanyFieldIndex.GeoFencingRadius, true); }
			set	{ SetValue((int)CompanyFieldIndex.GeoFencingRadius, value); }
		}

		/// <summary>The CometHandlerType property of the Entity Company<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Company"."CometHandlerType".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CometHandlerType
		{
			get { return (System.Int32)GetValue((int)CompanyFieldIndex.CometHandlerType, true); }
			set	{ SetValue((int)CompanyFieldIndex.CometHandlerType, value); }
		}

		/// <summary>The PercentageDownForNotification property of the Entity Company<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Company"."PercentageDownForNotification".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 PercentageDownForNotification
		{
			get { return (System.Int32)GetValue((int)CompanyFieldIndex.PercentageDownForNotification, true); }
			set	{ SetValue((int)CompanyFieldIndex.PercentageDownForNotification, value); }
		}

		/// <summary>The PercentageDownIntervalHours property of the Entity Company<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Company"."PercentageDownIntervalHours".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 PercentageDownIntervalHours
		{
			get { return (System.Int32)GetValue((int)CompanyFieldIndex.PercentageDownIntervalHours, true); }
			set	{ SetValue((int)CompanyFieldIndex.PercentageDownIntervalHours, value); }
		}

		/// <summary>The PercentageDownJumpForNotification property of the Entity Company<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Company"."PercentageDownJumpForNotification".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 PercentageDownJumpForNotification
		{
			get { return (System.Int32)GetValue((int)CompanyFieldIndex.PercentageDownJumpForNotification, true); }
			set	{ SetValue((int)CompanyFieldIndex.PercentageDownJumpForNotification, value); }
		}

		/// <summary>The SystemPassword property of the Entity Company<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Company"."SystemPassword".<br/>Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 50.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String SystemPassword
		{
			get { return (System.String)GetValue((int)CompanyFieldIndex.SystemPassword, true); }
			set	{ SetValue((int)CompanyFieldIndex.SystemPassword, value); }
		}

		/// <summary>The TemperatureUnit property of the Entity Company<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Company"."TemperatureUnit".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Crave.Enums.TemperatureUnit TemperatureUnit
		{
			get { return (Crave.Enums.TemperatureUnit)GetValue((int)CompanyFieldIndex.TemperatureUnit, true); }
			set	{ SetValue((int)CompanyFieldIndex.TemperatureUnit, value); }
		}

		/// <summary>The ClockMode property of the Entity Company<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Company"."ClockMode".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Crave.Enums.ClockMode ClockMode
		{
			get { return (Crave.Enums.ClockMode)GetValue((int)CompanyFieldIndex.ClockMode, true); }
			set	{ SetValue((int)CompanyFieldIndex.ClockMode, value); }
		}

		/// <summary>The WeatherClockWidgetMode property of the Entity Company<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Company"."WeatherClockWidgetMode".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Crave.Enums.WeatherClockWidgetMode WeatherClockWidgetMode
		{
			get { return (Crave.Enums.WeatherClockWidgetMode)GetValue((int)CompanyFieldIndex.WeatherClockWidgetMode, true); }
			set	{ SetValue((int)CompanyFieldIndex.WeatherClockWidgetMode, value); }
		}

		/// <summary>The DailyReportSendTime property of the Entity Company<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Company"."DailyReportSendTime".<br/>Table field type characteristics (type, precision, scale, length): DateTime, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> DailyReportSendTime
		{
			get { return (Nullable<System.DateTime>)GetValue((int)CompanyFieldIndex.DailyReportSendTime, false); }
			set	{ SetValue((int)CompanyFieldIndex.DailyReportSendTime, value); }
		}

		/// <summary>The AlterationDialogMode property of the Entity Company<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Company"."AlterationDialogMode".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Crave.Enums.AlterationDialogMode AlterationDialogMode
		{
			get { return (Crave.Enums.AlterationDialogMode)GetValue((int)CompanyFieldIndex.AlterationDialogMode, true); }
			set	{ SetValue((int)CompanyFieldIndex.AlterationDialogMode, value); }
		}

		/// <summary>The PriceFormatType property of the Entity Company<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Company"."PriceFormatType".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Crave.Enums.PriceFormatType PriceFormatType
		{
			get { return (Crave.Enums.PriceFormatType)GetValue((int)CompanyFieldIndex.PriceFormatType, true); }
			set	{ SetValue((int)CompanyFieldIndex.PriceFormatType, value); }
		}

		/// <summary>The IncludeDeliveryTimeInOrderNotes property of the Entity Company<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Company"."IncludeDeliveryTimeInOrderNotes".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IncludeDeliveryTimeInOrderNotes
		{
			get { return (System.Boolean)GetValue((int)CompanyFieldIndex.IncludeDeliveryTimeInOrderNotes, true); }
			set	{ SetValue((int)CompanyFieldIndex.IncludeDeliveryTimeInOrderNotes, value); }
		}

		/// <summary>The IncludeMenuItemsNotExternallyLinked property of the Entity Company<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Company"."IncludeMenuItemsNotExternallyLinked".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IncludeMenuItemsNotExternallyLinked
		{
			get { return (System.Boolean)GetValue((int)CompanyFieldIndex.IncludeMenuItemsNotExternallyLinked, true); }
			set	{ SetValue((int)CompanyFieldIndex.IncludeMenuItemsNotExternallyLinked, value); }
		}

		/// <summary>The CreatedUTC property of the Entity Company<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Company"."CreatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)CompanyFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)CompanyFieldIndex.CreatedUTC, value); }
		}

		/// <summary>The UpdatedUTC property of the Entity Company<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Company"."UpdatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)CompanyFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)CompanyFieldIndex.UpdatedUTC, value); }
		}

		/// <summary>The PercentageDownNotificationLastSentUTC property of the Entity Company<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Company"."PercentageDownNotificationLastSentUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> PercentageDownNotificationLastSentUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)CompanyFieldIndex.PercentageDownNotificationLastSentUTC, false); }
			set	{ SetValue((int)CompanyFieldIndex.PercentageDownNotificationLastSentUTC, value); }
		}

		/// <summary>The PercentageDownJumpNotificationLastSentUTC property of the Entity Company<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Company"."PercentageDownJumpNotificationLastSentUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> PercentageDownJumpNotificationLastSentUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)CompanyFieldIndex.PercentageDownJumpNotificationLastSentUTC, false); }
			set	{ SetValue((int)CompanyFieldIndex.PercentageDownJumpNotificationLastSentUTC, value); }
		}

		/// <summary>The LastDailyReportSendUTC property of the Entity Company<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Company"."LastDailyReportSendUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> LastDailyReportSendUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)CompanyFieldIndex.LastDailyReportSendUTC, false); }
			set	{ SetValue((int)CompanyFieldIndex.LastDailyReportSendUTC, value); }
		}

		/// <summary>The CompanyDataLastModifiedUTC property of the Entity Company<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Company"."CompanyDataLastModifiedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime CompanyDataLastModifiedUTC
		{
			get { return (System.DateTime)GetValue((int)CompanyFieldIndex.CompanyDataLastModifiedUTC, true); }
			set	{ SetValue((int)CompanyFieldIndex.CompanyDataLastModifiedUTC, value); }
		}

		/// <summary>The CompanyMediaLastModifiedUTC property of the Entity Company<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Company"."CompanyMediaLastModifiedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime CompanyMediaLastModifiedUTC
		{
			get { return (System.DateTime)GetValue((int)CompanyFieldIndex.CompanyMediaLastModifiedUTC, true); }
			set	{ SetValue((int)CompanyFieldIndex.CompanyMediaLastModifiedUTC, value); }
		}

		/// <summary>The MenuDataLastModifiedUTC property of the Entity Company<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Company"."MenuDataLastModifiedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime MenuDataLastModifiedUTC
		{
			get { return (System.DateTime)GetValue((int)CompanyFieldIndex.MenuDataLastModifiedUTC, true); }
			set	{ SetValue((int)CompanyFieldIndex.MenuDataLastModifiedUTC, value); }
		}

		/// <summary>The MenuMediaLastModifiedUTC property of the Entity Company<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Company"."MenuMediaLastModifiedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime MenuMediaLastModifiedUTC
		{
			get { return (System.DateTime)GetValue((int)CompanyFieldIndex.MenuMediaLastModifiedUTC, true); }
			set	{ SetValue((int)CompanyFieldIndex.MenuMediaLastModifiedUTC, value); }
		}

		/// <summary>The PosIntegrationInfoLastModifiedUTC property of the Entity Company<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Company"."PosIntegrationInfoLastModifiedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime PosIntegrationInfoLastModifiedUTC
		{
			get { return (System.DateTime)GetValue((int)CompanyFieldIndex.PosIntegrationInfoLastModifiedUTC, true); }
			set	{ SetValue((int)CompanyFieldIndex.PosIntegrationInfoLastModifiedUTC, value); }
		}

		/// <summary>The DeliverypointDataLastModifiedUTC property of the Entity Company<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Company"."DeliverypointDataLastModifiedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime DeliverypointDataLastModifiedUTC
		{
			get { return (System.DateTime)GetValue((int)CompanyFieldIndex.DeliverypointDataLastModifiedUTC, true); }
			set	{ SetValue((int)CompanyFieldIndex.DeliverypointDataLastModifiedUTC, value); }
		}

		/// <summary>The SurveyDataLastModifiedUTC property of the Entity Company<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Company"."SurveyDataLastModifiedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime SurveyDataLastModifiedUTC
		{
			get { return (System.DateTime)GetValue((int)CompanyFieldIndex.SurveyDataLastModifiedUTC, true); }
			set	{ SetValue((int)CompanyFieldIndex.SurveyDataLastModifiedUTC, value); }
		}

		/// <summary>The SurveyMediaLastModifiedUTC property of the Entity Company<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Company"."SurveyMediaLastModifiedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime SurveyMediaLastModifiedUTC
		{
			get { return (System.DateTime)GetValue((int)CompanyFieldIndex.SurveyMediaLastModifiedUTC, true); }
			set	{ SetValue((int)CompanyFieldIndex.SurveyMediaLastModifiedUTC, value); }
		}

		/// <summary>The AnnouncementDataLastModifiedUTC property of the Entity Company<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Company"."AnnouncementDataLastModifiedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime AnnouncementDataLastModifiedUTC
		{
			get { return (System.DateTime)GetValue((int)CompanyFieldIndex.AnnouncementDataLastModifiedUTC, true); }
			set	{ SetValue((int)CompanyFieldIndex.AnnouncementDataLastModifiedUTC, value); }
		}

		/// <summary>The AnnouncementMediaLastModifiedUTC property of the Entity Company<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Company"."AnnouncementMediaLastModifiedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime AnnouncementMediaLastModifiedUTC
		{
			get { return (System.DateTime)GetValue((int)CompanyFieldIndex.AnnouncementMediaLastModifiedUTC, true); }
			set	{ SetValue((int)CompanyFieldIndex.AnnouncementMediaLastModifiedUTC, value); }
		}

		/// <summary>The EntertainmentDataLastModifiedUTC property of the Entity Company<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Company"."EntertainmentDataLastModifiedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime EntertainmentDataLastModifiedUTC
		{
			get { return (System.DateTime)GetValue((int)CompanyFieldIndex.EntertainmentDataLastModifiedUTC, true); }
			set	{ SetValue((int)CompanyFieldIndex.EntertainmentDataLastModifiedUTC, value); }
		}

		/// <summary>The EntertainmentMediaLastModifiedUTC property of the Entity Company<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Company"."EntertainmentMediaLastModifiedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime EntertainmentMediaLastModifiedUTC
		{
			get { return (System.DateTime)GetValue((int)CompanyFieldIndex.EntertainmentMediaLastModifiedUTC, true); }
			set	{ SetValue((int)CompanyFieldIndex.EntertainmentMediaLastModifiedUTC, value); }
		}

		/// <summary>The AdvertisementDataLastModifiedUTC property of the Entity Company<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Company"."AdvertisementDataLastModifiedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime AdvertisementDataLastModifiedUTC
		{
			get { return (System.DateTime)GetValue((int)CompanyFieldIndex.AdvertisementDataLastModifiedUTC, true); }
			set	{ SetValue((int)CompanyFieldIndex.AdvertisementDataLastModifiedUTC, value); }
		}

		/// <summary>The AdvertisementMediaLastModifiedUTC property of the Entity Company<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Company"."AdvertisementMediaLastModifiedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime AdvertisementMediaLastModifiedUTC
		{
			get { return (System.DateTime)GetValue((int)CompanyFieldIndex.AdvertisementMediaLastModifiedUTC, true); }
			set	{ SetValue((int)CompanyFieldIndex.AdvertisementMediaLastModifiedUTC, value); }
		}

		/// <summary>The GoogleAnalyticsProfileId property of the Entity Company<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Company"."GoogleAnalyticsProfileId".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String GoogleAnalyticsProfileId
		{
			get { return (System.String)GetValue((int)CompanyFieldIndex.GoogleAnalyticsProfileId, true); }
			set	{ SetValue((int)CompanyFieldIndex.GoogleAnalyticsProfileId, value); }
		}

		/// <summary>The OrganizationId property of the Entity Company<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Company"."OrganizationId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> OrganizationId
		{
			get { return (Nullable<System.Int32>)GetValue((int)CompanyFieldIndex.OrganizationId, false); }
			set	{ SetValue((int)CompanyFieldIndex.OrganizationId, value); }
		}

		/// <summary>The ApiVersion property of the Entity Company<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Company"."ApiVersion".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ApiVersion
		{
			get { return (System.Int32)GetValue((int)CompanyFieldIndex.ApiVersion, true); }
			set	{ SetValue((int)CompanyFieldIndex.ApiVersion, value); }
		}

		/// <summary>The MessagingVersion property of the Entity Company<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Company"."MessagingVersion".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 MessagingVersion
		{
			get { return (System.Int32)GetValue((int)CompanyFieldIndex.MessagingVersion, true); }
			set	{ SetValue((int)CompanyFieldIndex.MessagingVersion, value); }
		}

		/// <summary>The LinkDefaultRoomControlArea property of the Entity Company<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Company"."LinkDefaultRoomControlArea".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean LinkDefaultRoomControlArea
		{
			get { return (System.Boolean)GetValue((int)CompanyFieldIndex.LinkDefaultRoomControlArea, true); }
			set	{ SetValue((int)CompanyFieldIndex.LinkDefaultRoomControlArea, value); }
		}

		/// <summary>The PricesIncludeTaxes property of the Entity Company<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Company"."PricesIncludeTaxes".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean PricesIncludeTaxes
		{
			get { return (System.Boolean)GetValue((int)CompanyFieldIndex.PricesIncludeTaxes, true); }
			set	{ SetValue((int)CompanyFieldIndex.PricesIncludeTaxes, value); }
		}

		/// <summary>The UnitSystem property of the Entity Company<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Company"."UnitSystem".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UnitSystem
		{
			get { return (System.Int32)GetValue((int)CompanyFieldIndex.UnitSystem, true); }
			set	{ SetValue((int)CompanyFieldIndex.UnitSystem, value); }
		}

		/// <summary>The ReleaseGroup property of the Entity Company<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Company"."ReleaseGroup".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ReleaseGroup
		{
			get { return (Nullable<System.Int32>)GetValue((int)CompanyFieldIndex.ReleaseGroup, false); }
			set	{ SetValue((int)CompanyFieldIndex.ReleaseGroup, value); }
		}

		/// <summary>The Notes property of the Entity Company<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Company"."Notes".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Notes
		{
			get { return (System.String)GetValue((int)CompanyFieldIndex.Notes, true); }
			set	{ SetValue((int)CompanyFieldIndex.Notes, value); }
		}

		/// <summary>Gets the EntityCollection with the related entities of type 'AlterationEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(AlterationEntity))]
		public virtual EntityCollection<AlterationEntity> AlterationCollection { get { return GetOrCreateEntityCollection<AlterationEntity, AlterationEntityFactory>("Company", true, false, ref _alterationCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'AlterationoptionEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(AlterationoptionEntity))]
		public virtual EntityCollection<AlterationoptionEntity> AlterationoptionCollection { get { return GetOrCreateEntityCollection<AlterationoptionEntity, AlterationoptionEntityFactory>("Company", true, false, ref _alterationoptionCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'ApplicationConfigurationEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(ApplicationConfigurationEntity))]
		public virtual EntityCollection<ApplicationConfigurationEntity> ApplicationConfigurationCollection { get { return GetOrCreateEntityCollection<ApplicationConfigurationEntity, ApplicationConfigurationEntityFactory>("Company", true, false, ref _applicationConfigurationCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'FeatureFlagEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(FeatureFlagEntity))]
		public virtual EntityCollection<FeatureFlagEntity> FeatureFlagCollection { get { return GetOrCreateEntityCollection<FeatureFlagEntity, FeatureFlagEntityFactory>("Company", true, false, ref _featureFlagCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'BusinesshourEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(BusinesshourEntity))]
		public virtual EntityCollection<BusinesshourEntity> BusinesshourCollection { get { return GetOrCreateEntityCollection<BusinesshourEntity, BusinesshourEntityFactory>("Company", true, false, ref _businesshourCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'BusinesshoursexceptionEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(BusinesshoursexceptionEntity))]
		public virtual EntityCollection<BusinesshoursexceptionEntity> BusinesshoursexceptionCollection { get { return GetOrCreateEntityCollection<BusinesshoursexceptionEntity, BusinesshoursexceptionEntityFactory>("Company", true, false, ref _businesshoursexceptionCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'CategoryEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(CategoryEntity))]
		public virtual EntityCollection<CategoryEntity> CategoryCollection { get { return GetOrCreateEntityCollection<CategoryEntity, CategoryEntityFactory>("Company", true, false, ref _categoryCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'CheckoutMethodEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(CheckoutMethodEntity))]
		public virtual EntityCollection<CheckoutMethodEntity> CheckoutMethodCollection { get { return GetOrCreateEntityCollection<CheckoutMethodEntity, CheckoutMethodEntityFactory>("Company", true, false, ref _checkoutMethodCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'CheckoutMethodDeliverypointgroupEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(CheckoutMethodDeliverypointgroupEntity))]
		public virtual EntityCollection<CheckoutMethodDeliverypointgroupEntity> CheckoutMethodDeliverypointgroupCollection { get { return GetOrCreateEntityCollection<CheckoutMethodDeliverypointgroupEntity, CheckoutMethodDeliverypointgroupEntityFactory>("Company", true, false, ref _checkoutMethodDeliverypointgroupCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'ClientEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(ClientEntity))]
		public virtual EntityCollection<ClientEntity> ClientCollection { get { return GetOrCreateEntityCollection<ClientEntity, ClientEntityFactory>("Company", true, false, ref _clientCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'ClientConfigurationEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(ClientConfigurationEntity))]
		public virtual EntityCollection<ClientConfigurationEntity> ClientConfigurationCollection { get { return GetOrCreateEntityCollection<ClientConfigurationEntity, ClientConfigurationEntityFactory>("Company", true, false, ref _clientConfigurationCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'DeliverypointEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(DeliverypointEntity))]
		public virtual EntityCollection<DeliverypointEntity> DeliverypointCollection { get { return GetOrCreateEntityCollection<DeliverypointEntity, DeliverypointEntityFactory>("Company", true, false, ref _deliverypointCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'DeliverypointgroupEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(DeliverypointgroupEntity))]
		public virtual EntityCollection<DeliverypointgroupEntity> DeliverypointgroupCollection { get { return GetOrCreateEntityCollection<DeliverypointgroupEntity, DeliverypointgroupEntityFactory>("Company", true, false, ref _deliverypointgroupCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'ExternalMenuEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(ExternalMenuEntity))]
		public virtual EntityCollection<ExternalMenuEntity> ExternalMenuCollection { get { return GetOrCreateEntityCollection<ExternalMenuEntity, ExternalMenuEntityFactory>("Company", true, false, ref _externalMenuCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'ExternalSystemEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(ExternalSystemEntity))]
		public virtual EntityCollection<ExternalSystemEntity> ExternalSystemCollection { get { return GetOrCreateEntityCollection<ExternalSystemEntity, ExternalSystemEntityFactory>("Company", true, false, ref _externalSystemCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'MediaEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(MediaEntity))]
		public virtual EntityCollection<MediaEntity> MediaCollection { get { return GetOrCreateEntityCollection<MediaEntity, MediaEntityFactory>("Company", true, false, ref _mediaCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'MenuEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(MenuEntity))]
		public virtual EntityCollection<MenuEntity> MenuCollection { get { return GetOrCreateEntityCollection<MenuEntity, MenuEntityFactory>("Company", true, false, ref _menuCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'NetmessageEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(NetmessageEntity))]
		public virtual EntityCollection<NetmessageEntity> NetmessageCollection { get { return GetOrCreateEntityCollection<NetmessageEntity, NetmessageEntityFactory>("Company", true, false, ref _netmessageCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'NetmessageEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(NetmessageEntity))]
		public virtual EntityCollection<NetmessageEntity> NetmessageCollection1 { get { return GetOrCreateEntityCollection<NetmessageEntity, NetmessageEntityFactory>("CompanyEntity1", true, false, ref _netmessageCollection1); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'OptInEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(OptInEntity))]
		public virtual EntityCollection<OptInEntity> OptInCollection { get { return GetOrCreateEntityCollection<OptInEntity, OptInEntityFactory>("Company", true, false, ref _optInCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'OrderEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(OrderEntity))]
		public virtual EntityCollection<OrderEntity> OrderCollection { get { return GetOrCreateEntityCollection<OrderEntity, OrderEntityFactory>("Company", true, false, ref _orderCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'OutletEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(OutletEntity))]
		public virtual EntityCollection<OutletEntity> OutletCollection { get { return GetOrCreateEntityCollection<OutletEntity, OutletEntityFactory>("Company", true, false, ref _outletCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'PaymentIntegrationConfigurationEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(PaymentIntegrationConfigurationEntity))]
		public virtual EntityCollection<PaymentIntegrationConfigurationEntity> PaymentIntegrationConfigurationCollection { get { return GetOrCreateEntityCollection<PaymentIntegrationConfigurationEntity, PaymentIntegrationConfigurationEntityFactory>("Company", true, false, ref _paymentIntegrationConfigurationCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'PaymentProviderEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(PaymentProviderEntity))]
		public virtual EntityCollection<PaymentProviderEntity> PaymentProviderCollection { get { return GetOrCreateEntityCollection<PaymentProviderEntity, PaymentProviderEntityFactory>("Company", true, false, ref _paymentProviderCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'PriceLevelEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(PriceLevelEntity))]
		public virtual EntityCollection<PriceLevelEntity> PriceLevelCollection { get { return GetOrCreateEntityCollection<PriceLevelEntity, PriceLevelEntityFactory>("Company", true, false, ref _priceLevelCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'PriceScheduleEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(PriceScheduleEntity))]
		public virtual EntityCollection<PriceScheduleEntity> PriceScheduleCollection { get { return GetOrCreateEntityCollection<PriceScheduleEntity, PriceScheduleEntityFactory>("Company", true, false, ref _priceScheduleCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'ProductEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(ProductEntity))]
		public virtual EntityCollection<ProductEntity> ProductCollection { get { return GetOrCreateEntityCollection<ProductEntity, ProductEntityFactory>("Company", true, false, ref _productCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'ReceiptEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(ReceiptEntity))]
		public virtual EntityCollection<ReceiptEntity> ReceiptCollection { get { return GetOrCreateEntityCollection<ReceiptEntity, ReceiptEntityFactory>("Company", true, false, ref _receiptCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'ReceiptTemplateEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(ReceiptTemplateEntity))]
		public virtual EntityCollection<ReceiptTemplateEntity> ReceiptTemplateCollection { get { return GetOrCreateEntityCollection<ReceiptTemplateEntity, ReceiptTemplateEntityFactory>("Company", true, false, ref _receiptTemplateCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'RouteEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(RouteEntity))]
		public virtual EntityCollection<RouteEntity> RouteCollection { get { return GetOrCreateEntityCollection<RouteEntity, RouteEntityFactory>("Company", true, false, ref _routeCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'ScheduleEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(ScheduleEntity))]
		public virtual EntityCollection<ScheduleEntity> ScheduleCollection { get { return GetOrCreateEntityCollection<ScheduleEntity, ScheduleEntityFactory>("Company", true, false, ref _scheduleCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'ServiceMethodEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(ServiceMethodEntity))]
		public virtual EntityCollection<ServiceMethodEntity> ServiceMethodCollection { get { return GetOrCreateEntityCollection<ServiceMethodEntity, ServiceMethodEntityFactory>("Company", true, false, ref _serviceMethodCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'ServiceMethodDeliverypointgroupEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(ServiceMethodDeliverypointgroupEntity))]
		public virtual EntityCollection<ServiceMethodDeliverypointgroupEntity> ServiceMethodDeliverypointgroupCollection { get { return GetOrCreateEntityCollection<ServiceMethodDeliverypointgroupEntity, ServiceMethodDeliverypointgroupEntityFactory>("Company", true, false, ref _serviceMethodDeliverypointgroupCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'TagEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(TagEntity))]
		public virtual EntityCollection<TagEntity> TagCollection { get { return GetOrCreateEntityCollection<TagEntity, TagEntityFactory>("Company", true, false, ref _tagCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'TaxTariffEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(TaxTariffEntity))]
		public virtual EntityCollection<TaxTariffEntity> TaxTariffCollection { get { return GetOrCreateEntityCollection<TaxTariffEntity, TaxTariffEntityFactory>("Company", true, false, ref _taxTariffCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'TerminalEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(TerminalEntity))]
		public virtual EntityCollection<TerminalEntity> TerminalCollection { get { return GetOrCreateEntityCollection<TerminalEntity, TerminalEntityFactory>("Company", true, false, ref _terminalCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'ClientConfigurationEntity' which are related to this entity via a relation of type 'm:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(ClientConfigurationEntity))]
		public virtual EntityCollection<ClientConfigurationEntity> ClientConfigurationCollectionViaDeliverypoint { get { return GetOrCreateEntityCollection<ClientConfigurationEntity, ClientConfigurationEntityFactory>("", false, true, ref _clientConfigurationCollectionViaDeliverypoint); } }

		/// <summary>Gets / sets related entity of type 'RouteEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual RouteEntity Route
		{
			get { return _route; }
			set { SetSingleRelatedEntityNavigator(value, "Route"); }
		}

		/// <summary>Gets / sets related entity of type 'SupportpoolEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual SupportpoolEntity Supportpool
		{
			get { return _supportpool; }
			set { SetSingleRelatedEntityNavigator(value, "Supportpool"); }
		}


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen
{
	public enum CompanyFieldIndex
	{
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>CompanyOwnerId. </summary>
		CompanyOwnerId,
		///<summary>RouteId. </summary>
		RouteId,
		///<summary>SupportpoolId. </summary>
		SupportpoolId,
		///<summary>Name. </summary>
		Name,
		///<summary>NameWithoutDiacritics. </summary>
		NameWithoutDiacritics,
		///<summary>Code. </summary>
		Code,
		///<summary>Addressline1. </summary>
		Addressline1,
		///<summary>Addressline2. </summary>
		Addressline2,
		///<summary>Addressline3. </summary>
		Addressline3,
		///<summary>Zipcode. </summary>
		Zipcode,
		///<summary>City. </summary>
		City,
		///<summary>CountryCode. </summary>
		CountryCode,
		///<summary>TimeZoneOlsonId. </summary>
		TimeZoneOlsonId,
		///<summary>CultureCode. </summary>
		CultureCode,
		///<summary>CurrencyCode. </summary>
		CurrencyCode,
		///<summary>Latitude. </summary>
		Latitude,
		///<summary>LatitudeOverridden. </summary>
		LatitudeOverridden,
		///<summary>Longitude. </summary>
		Longitude,
		///<summary>LongitudeOverriden. </summary>
		LongitudeOverriden,
		///<summary>Telephone. </summary>
		Telephone,
		///<summary>Fax. </summary>
		Fax,
		///<summary>Website. </summary>
		Website,
		///<summary>Email. </summary>
		Email,
		///<summary>GoogleAnalyticsId. </summary>
		GoogleAnalyticsId,
		///<summary>StandardCheckInterval. </summary>
		StandardCheckInterval,
		///<summary>BrowserAgeVerification. </summary>
		BrowserAgeVerification,
		///<summary>Description. </summary>
		Description,
		///<summary>DescriptionSingleLine. </summary>
		DescriptionSingleLine,
		///<summary>AutomaticClientOperationModes. </summary>
		AutomaticClientOperationModes,
		///<summary>AvailableOnOtoucho. </summary>
		AvailableOnOtoucho,
		///<summary>AvailableOnObymobi. </summary>
		AvailableOnObymobi,
		///<summary>MobileOrderingDisabled. </summary>
		MobileOrderingDisabled,
		///<summary>UseMonitoring. </summary>
		UseMonitoring,
		///<summary>SystemType. </summary>
		SystemType,
		///<summary>AllowRequestService. </summary>
		AllowRequestService,
		///<summary>AllowRateProduct. </summary>
		AllowRateProduct,
		///<summary>AllowRateService. </summary>
		AllowRateService,
		///<summary>AllowFreeText. </summary>
		AllowFreeText,
		///<summary>UseBillButton. </summary>
		UseBillButton,
		///<summary>Salt. </summary>
		Salt,
		///<summary>SaltPms. </summary>
		SaltPms,
		///<summary>MaxDownloadConnections. </summary>
		MaxDownloadConnections,
		///<summary>ClientApplicationVersion. </summary>
		ClientApplicationVersion,
		///<summary>TerminalApplicationVersion. </summary>
		TerminalApplicationVersion,
		///<summary>OsVersion. </summary>
		OsVersion,
		///<summary>AgentVersion. </summary>
		AgentVersion,
		///<summary>SupportToolsVersion. </summary>
		SupportToolsVersion,
		///<summary>DeviceRebootMethod. </summary>
		DeviceRebootMethod,
		///<summary>CorrespondenceEmail. </summary>
		CorrespondenceEmail,
		///<summary>ShowCurrencySymbol. </summary>
		ShowCurrencySymbol,
		///<summary>Pincode. </summary>
		Pincode,
		///<summary>PincodeSU. </summary>
		PincodeSU,
		///<summary>PincodeGM. </summary>
		PincodeGM,
		///<summary>ActionButtonId. </summary>
		ActionButtonId,
		///<summary>ActionButtonUrlTablet. </summary>
		ActionButtonUrlTablet,
		///<summary>ActionButtonUrlMobile. </summary>
		ActionButtonUrlMobile,
		///<summary>CostIndicationType. </summary>
		CostIndicationType,
		///<summary>CostIndicationValue. </summary>
		CostIndicationValue,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>GeoFencingEnabled. </summary>
		GeoFencingEnabled,
		///<summary>GeoFencingRadius. </summary>
		GeoFencingRadius,
		///<summary>CometHandlerType. </summary>
		CometHandlerType,
		///<summary>PercentageDownForNotification. </summary>
		PercentageDownForNotification,
		///<summary>PercentageDownIntervalHours. </summary>
		PercentageDownIntervalHours,
		///<summary>PercentageDownJumpForNotification. </summary>
		PercentageDownJumpForNotification,
		///<summary>SystemPassword. </summary>
		SystemPassword,
		///<summary>TemperatureUnit. </summary>
		TemperatureUnit,
		///<summary>ClockMode. </summary>
		ClockMode,
		///<summary>WeatherClockWidgetMode. </summary>
		WeatherClockWidgetMode,
		///<summary>DailyReportSendTime. </summary>
		DailyReportSendTime,
		///<summary>AlterationDialogMode. </summary>
		AlterationDialogMode,
		///<summary>PriceFormatType. </summary>
		PriceFormatType,
		///<summary>IncludeDeliveryTimeInOrderNotes. </summary>
		IncludeDeliveryTimeInOrderNotes,
		///<summary>IncludeMenuItemsNotExternallyLinked. </summary>
		IncludeMenuItemsNotExternallyLinked,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>PercentageDownNotificationLastSentUTC. </summary>
		PercentageDownNotificationLastSentUTC,
		///<summary>PercentageDownJumpNotificationLastSentUTC. </summary>
		PercentageDownJumpNotificationLastSentUTC,
		///<summary>LastDailyReportSendUTC. </summary>
		LastDailyReportSendUTC,
		///<summary>CompanyDataLastModifiedUTC. </summary>
		CompanyDataLastModifiedUTC,
		///<summary>CompanyMediaLastModifiedUTC. </summary>
		CompanyMediaLastModifiedUTC,
		///<summary>MenuDataLastModifiedUTC. </summary>
		MenuDataLastModifiedUTC,
		///<summary>MenuMediaLastModifiedUTC. </summary>
		MenuMediaLastModifiedUTC,
		///<summary>PosIntegrationInfoLastModifiedUTC. </summary>
		PosIntegrationInfoLastModifiedUTC,
		///<summary>DeliverypointDataLastModifiedUTC. </summary>
		DeliverypointDataLastModifiedUTC,
		///<summary>SurveyDataLastModifiedUTC. </summary>
		SurveyDataLastModifiedUTC,
		///<summary>SurveyMediaLastModifiedUTC. </summary>
		SurveyMediaLastModifiedUTC,
		///<summary>AnnouncementDataLastModifiedUTC. </summary>
		AnnouncementDataLastModifiedUTC,
		///<summary>AnnouncementMediaLastModifiedUTC. </summary>
		AnnouncementMediaLastModifiedUTC,
		///<summary>EntertainmentDataLastModifiedUTC. </summary>
		EntertainmentDataLastModifiedUTC,
		///<summary>EntertainmentMediaLastModifiedUTC. </summary>
		EntertainmentMediaLastModifiedUTC,
		///<summary>AdvertisementDataLastModifiedUTC. </summary>
		AdvertisementDataLastModifiedUTC,
		///<summary>AdvertisementMediaLastModifiedUTC. </summary>
		AdvertisementMediaLastModifiedUTC,
		///<summary>GoogleAnalyticsProfileId. </summary>
		GoogleAnalyticsProfileId,
		///<summary>OrganizationId. </summary>
		OrganizationId,
		///<summary>ApiVersion. </summary>
		ApiVersion,
		///<summary>MessagingVersion. </summary>
		MessagingVersion,
		///<summary>LinkDefaultRoomControlArea. </summary>
		LinkDefaultRoomControlArea,
		///<summary>PricesIncludeTaxes. </summary>
		PricesIncludeTaxes,
		///<summary>UnitSystem. </summary>
		UnitSystem,
		///<summary>ReleaseGroup. </summary>
		ReleaseGroup,
		///<summary>Notes. </summary>
		Notes,
		/// <summary></summary>
		AmountOfFields
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Company. </summary>
	public partial class CompanyRelations: RelationFactory
	{
		#region Custom Relation code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomRelationCode
		private const ReferentialConstraintDeleteRule DeleteRuleForAlterationEntityUsingCompanyId = ReferentialConstraintDeleteRule.Cascade;
		private const ReferentialConstraintDeleteRule DeleteRuleForAlterationoptionEntityUsingCompanyId = ReferentialConstraintDeleteRule.Cascade;
		private const ReferentialConstraintDeleteRule DeleteRuleForApplicationConfigurationEntityUsingCompanyId = ReferentialConstraintDeleteRule.Cascade;
		private const ReferentialConstraintDeleteRule DeleteRuleForFeatureFlagEntityUsingCompanyId = ReferentialConstraintDeleteRule.Cascade;
		private const ReferentialConstraintDeleteRule DeleteRuleForBusinesshourEntityUsingCompanyId = ReferentialConstraintDeleteRule.Cascade;
		private const ReferentialConstraintDeleteRule DeleteRuleForBusinesshoursexceptionEntityUsingCompanyId = ReferentialConstraintDeleteRule.Cascade;
		private const ReferentialConstraintDeleteRule DeleteRuleForCategoryEntityUsingCompanyId = ReferentialConstraintDeleteRule.Cascade;
		private const ReferentialConstraintDeleteRule DeleteRuleForCheckoutMethodEntityUsingCompanyId = ReferentialConstraintDeleteRule.Cascade;
		private const ReferentialConstraintDeleteRule DeleteRuleForClientEntityUsingCompanyId = ReferentialConstraintDeleteRule.Cascade;
		private const ReferentialConstraintDeleteRule DeleteRuleForClientConfigurationEntityUsingCompanyId = ReferentialConstraintDeleteRule.Cascade;
		private const ReferentialConstraintDeleteRule DeleteRuleForDeliverypointEntityUsingCompanyId = ReferentialConstraintDeleteRule.Cascade;
		private const ReferentialConstraintDeleteRule DeleteRuleForDeliverypointgroupEntityUsingCompanyId = ReferentialConstraintDeleteRule.Cascade;
		private const ReferentialConstraintDeleteRule DeleteRuleForMediaEntityUsingCompanyId = ReferentialConstraintDeleteRule.Cascade;
		private const ReferentialConstraintDeleteRule DeleteRuleForMenuEntityUsingCompanyId = ReferentialConstraintDeleteRule.Cascade;
		private const ReferentialConstraintDeleteRule DeleteRuleForNetmessageEntityUsingSenderCompanyId = ReferentialConstraintDeleteRule.Cascade;
		private const ReferentialConstraintDeleteRule DeleteRuleForNetmessageEntityUsingReceiverCompanyId = ReferentialConstraintDeleteRule.Cascade;
		private const ReferentialConstraintDeleteRule DeleteRuleForOptInEntityUsingParentCompanyId = ReferentialConstraintDeleteRule.Cascade;
		private const ReferentialConstraintDeleteRule DeleteRuleForOrderEntityUsingCompanyId = ReferentialConstraintDeleteRule.NoAction;
		private const ReferentialConstraintDeleteRule DeleteRuleForOutletEntityUsingCompanyId = ReferentialConstraintDeleteRule.Cascade;
		private const ReferentialConstraintDeleteRule DeleteRuleForPaymentIntegrationConfigurationEntityUsingCompanyId = ReferentialConstraintDeleteRule.Cascade;
		private const ReferentialConstraintDeleteRule DeleteRuleForPaymentProviderEntityUsingCompanyId = ReferentialConstraintDeleteRule.NoAction;
		private const ReferentialConstraintDeleteRule DeleteRuleForPriceLevelEntityUsingCompanyId = ReferentialConstraintDeleteRule.Cascade;
		private const ReferentialConstraintDeleteRule DeleteRuleForPriceScheduleEntityUsingCompanyId = ReferentialConstraintDeleteRule.Cascade;
		private const ReferentialConstraintDeleteRule DeleteRuleForProductEntityUsingCompanyId = ReferentialConstraintDeleteRule.Cascade;
		private const ReferentialConstraintDeleteRule DeleteRuleForReceiptEntityUsingCompanyId = ReferentialConstraintDeleteRule.Cascade;
		private const ReferentialConstraintDeleteRule DeleteRuleForReceiptTemplateEntityUsingCompanyId = ReferentialConstraintDeleteRule.Cascade;
		private const ReferentialConstraintDeleteRule DeleteRuleForRouteEntityUsingCompanyId = ReferentialConstraintDeleteRule.Cascade;
		private const ReferentialConstraintDeleteRule DeleteRuleForScheduleEntityUsingCompanyId = ReferentialConstraintDeleteRule.Cascade;
		private const ReferentialConstraintDeleteRule DeleteRuleForServiceMethodEntityUsingCompanyId = ReferentialConstraintDeleteRule.Cascade;
		private const ReferentialConstraintDeleteRule DeleteRuleForTaxTariffEntityUsingCompanyId = ReferentialConstraintDeleteRule.Cascade;
		private const ReferentialConstraintDeleteRule DeleteRuleForServiceMethodDeliverypointgroupEntityUsingParentCompanyId = ReferentialConstraintDeleteRule.Cascade;
		private const ReferentialConstraintDeleteRule DeleteRuleForTerminalEntityUsingCompanyId = ReferentialConstraintDeleteRule.Cascade;
		private const ReferentialConstraintDeleteRule DeleteRuleForTagEntityUsingCompanyId = ReferentialConstraintDeleteRule.NoAction;
		private const ReferentialConstraintDeleteRule DeleteRuleForCheckoutMethodDeliverypointgroupEntityUsingParentCompanyId = ReferentialConstraintDeleteRule.Cascade;
		private const ReferentialConstraintDeleteRule DeleteRuleForExternalSystemEntityUsingCompanyId = ReferentialConstraintDeleteRule.Cascade;
        private const ReferentialConstraintDeleteRule DeleteRuleForExternalMenuEntityUsingParentCompanyId = ReferentialConstraintDeleteRule.Cascade;
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		
		// private const ReferentialConstraintDeleteRule DeleteRuleForAlterationEntityUsingCompanyId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForAlterationoptionEntityUsingCompanyId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForApplicationConfigurationEntityUsingCompanyId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForFeatureFlagEntityUsingCompanyId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForBusinesshourEntityUsingCompanyId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForBusinesshoursexceptionEntityUsingCompanyId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForCategoryEntityUsingCompanyId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForCheckoutMethodEntityUsingCompanyId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForCheckoutMethodDeliverypointgroupEntityUsingParentCompanyId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForClientEntityUsingCompanyId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForClientConfigurationEntityUsingCompanyId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForDeliverypointEntityUsingCompanyId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForDeliverypointgroupEntityUsingCompanyId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForExternalMenuEntityUsingParentCompanyId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForExternalSystemEntityUsingCompanyId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForMediaEntityUsingCompanyId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForMenuEntityUsingCompanyId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForNetmessageEntityUsingSenderCompanyId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForNetmessageEntityUsingReceiverCompanyId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForOptInEntityUsingParentCompanyId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForOrderEntityUsingCompanyId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForOutletEntityUsingCompanyId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForPaymentIntegrationConfigurationEntityUsingCompanyId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForPaymentProviderEntityUsingCompanyId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForPriceLevelEntityUsingCompanyId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForPriceScheduleEntityUsingCompanyId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForProductEntityUsingCompanyId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForReceiptEntityUsingCompanyId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForReceiptTemplateEntityUsingCompanyId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForRouteEntityUsingCompanyId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForScheduleEntityUsingCompanyId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForServiceMethodEntityUsingCompanyId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForServiceMethodDeliverypointgroupEntityUsingParentCompanyId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForTagEntityUsingCompanyId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForTaxTariffEntityUsingCompanyId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForTerminalEntityUsingCompanyId = ReferentialConstraintDeleteRule


		/// <summary>Gets all relations and delete rules of the CompanyEntity as a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects.</summary>
		/// <returns>a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects</returns>
		public virtual Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> GetRelationsAndDeleteRules()
		{
			Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> toReturn = new Dictionary<IEntityRelation, ReferentialConstraintDeleteRule>();
			toReturn.Add(this.AlterationEntityUsingCompanyId, CompanyRelations.DeleteRuleForAlterationEntityUsingCompanyId);
			toReturn.Add(this.AlterationoptionEntityUsingCompanyId, CompanyRelations.DeleteRuleForAlterationoptionEntityUsingCompanyId);
			toReturn.Add(this.ApplicationConfigurationEntityUsingCompanyId, CompanyRelations.DeleteRuleForApplicationConfigurationEntityUsingCompanyId);
			toReturn.Add(this.FeatureFlagEntityUsingCompanyId, CompanyRelations.DeleteRuleForFeatureFlagEntityUsingCompanyId);
			toReturn.Add(this.BusinesshourEntityUsingCompanyId, CompanyRelations.DeleteRuleForBusinesshourEntityUsingCompanyId);
			toReturn.Add(this.BusinesshoursexceptionEntityUsingCompanyId, CompanyRelations.DeleteRuleForBusinesshoursexceptionEntityUsingCompanyId);
			toReturn.Add(this.CategoryEntityUsingCompanyId, CompanyRelations.DeleteRuleForCategoryEntityUsingCompanyId);
			toReturn.Add(this.CheckoutMethodEntityUsingCompanyId, CompanyRelations.DeleteRuleForCheckoutMethodEntityUsingCompanyId);
			toReturn.Add(this.CheckoutMethodDeliverypointgroupEntityUsingParentCompanyId, CompanyRelations.DeleteRuleForCheckoutMethodDeliverypointgroupEntityUsingParentCompanyId);
			toReturn.Add(this.ClientEntityUsingCompanyId, CompanyRelations.DeleteRuleForClientEntityUsingCompanyId);
			toReturn.Add(this.ClientConfigurationEntityUsingCompanyId, CompanyRelations.DeleteRuleForClientConfigurationEntityUsingCompanyId);
			toReturn.Add(this.DeliverypointEntityUsingCompanyId, CompanyRelations.DeleteRuleForDeliverypointEntityUsingCompanyId);
			toReturn.Add(this.DeliverypointgroupEntityUsingCompanyId, CompanyRelations.DeleteRuleForDeliverypointgroupEntityUsingCompanyId);
			toReturn.Add(this.ExternalMenuEntityUsingParentCompanyId, CompanyRelations.DeleteRuleForExternalMenuEntityUsingParentCompanyId);
			toReturn.Add(this.ExternalSystemEntityUsingCompanyId, CompanyRelations.DeleteRuleForExternalSystemEntityUsingCompanyId);
			toReturn.Add(this.MediaEntityUsingCompanyId, CompanyRelations.DeleteRuleForMediaEntityUsingCompanyId);
			toReturn.Add(this.MenuEntityUsingCompanyId, CompanyRelations.DeleteRuleForMenuEntityUsingCompanyId);
			toReturn.Add(this.NetmessageEntityUsingSenderCompanyId, CompanyRelations.DeleteRuleForNetmessageEntityUsingSenderCompanyId);
			toReturn.Add(this.NetmessageEntityUsingReceiverCompanyId, CompanyRelations.DeleteRuleForNetmessageEntityUsingReceiverCompanyId);
			toReturn.Add(this.OptInEntityUsingParentCompanyId, CompanyRelations.DeleteRuleForOptInEntityUsingParentCompanyId);
			toReturn.Add(this.OrderEntityUsingCompanyId, CompanyRelations.DeleteRuleForOrderEntityUsingCompanyId);
			toReturn.Add(this.OutletEntityUsingCompanyId, CompanyRelations.DeleteRuleForOutletEntityUsingCompanyId);
			toReturn.Add(this.PaymentIntegrationConfigurationEntityUsingCompanyId, CompanyRelations.DeleteRuleForPaymentIntegrationConfigurationEntityUsingCompanyId);
			toReturn.Add(this.PaymentProviderEntityUsingCompanyId, CompanyRelations.DeleteRuleForPaymentProviderEntityUsingCompanyId);
			toReturn.Add(this.PriceLevelEntityUsingCompanyId, CompanyRelations.DeleteRuleForPriceLevelEntityUsingCompanyId);
			toReturn.Add(this.PriceScheduleEntityUsingCompanyId, CompanyRelations.DeleteRuleForPriceScheduleEntityUsingCompanyId);
			toReturn.Add(this.ProductEntityUsingCompanyId, CompanyRelations.DeleteRuleForProductEntityUsingCompanyId);
			toReturn.Add(this.ReceiptEntityUsingCompanyId, CompanyRelations.DeleteRuleForReceiptEntityUsingCompanyId);
			toReturn.Add(this.ReceiptTemplateEntityUsingCompanyId, CompanyRelations.DeleteRuleForReceiptTemplateEntityUsingCompanyId);
			toReturn.Add(this.RouteEntityUsingCompanyId, CompanyRelations.DeleteRuleForRouteEntityUsingCompanyId);
			toReturn.Add(this.ScheduleEntityUsingCompanyId, CompanyRelations.DeleteRuleForScheduleEntityUsingCompanyId);
			toReturn.Add(this.ServiceMethodEntityUsingCompanyId, CompanyRelations.DeleteRuleForServiceMethodEntityUsingCompanyId);
			toReturn.Add(this.ServiceMethodDeliverypointgroupEntityUsingParentCompanyId, CompanyRelations.DeleteRuleForServiceMethodDeliverypointgroupEntityUsingParentCompanyId);
			toReturn.Add(this.TagEntityUsingCompanyId, CompanyRelations.DeleteRuleForTagEntityUsingCompanyId);
			toReturn.Add(this.TaxTariffEntityUsingCompanyId, CompanyRelations.DeleteRuleForTaxTariffEntityUsingCompanyId);
			toReturn.Add(this.TerminalEntityUsingCompanyId, CompanyRelations.DeleteRuleForTerminalEntityUsingCompanyId);
			return toReturn;
		}		
	
	
		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and AlterationEntity over the 1:n relation they have, using the relation between the fields: Company.CompanyId - Alteration.CompanyId</summary>
		public virtual IEntityRelation AlterationEntityUsingCompanyId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "AlterationCollection", true, new[] { CompanyFields.CompanyId, AlterationFields.CompanyId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and AlterationoptionEntity over the 1:n relation they have, using the relation between the fields: Company.CompanyId - Alterationoption.CompanyId</summary>
		public virtual IEntityRelation AlterationoptionEntityUsingCompanyId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "AlterationoptionCollection", true, new[] { CompanyFields.CompanyId, AlterationoptionFields.CompanyId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and ApplicationConfigurationEntity over the 1:n relation they have, using the relation between the fields: Company.CompanyId - ApplicationConfiguration.CompanyId</summary>
		public virtual IEntityRelation ApplicationConfigurationEntityUsingCompanyId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "ApplicationConfigurationCollection", true, new[] { CompanyFields.CompanyId, ApplicationConfigurationFields.CompanyId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and FeatureFlagEntity over the 1:n relation they have, using the relation between the fields: Company.CompanyId - FeatureFlag.CompanyId</summary>
		public virtual IEntityRelation FeatureFlagEntityUsingCompanyId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "FeatureFlagCollection", true, new[] { CompanyFields.CompanyId, FeatureFlagFields.CompanyId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and BusinesshourEntity over the 1:n relation they have, using the relation between the fields: Company.CompanyId - Businesshour.CompanyId</summary>
		public virtual IEntityRelation BusinesshourEntityUsingCompanyId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "BusinesshourCollection", true, new[] { CompanyFields.CompanyId, BusinesshourFields.CompanyId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and BusinesshoursexceptionEntity over the 1:n relation they have, using the relation between the fields: Company.CompanyId - Businesshoursexception.CompanyId</summary>
		public virtual IEntityRelation BusinesshoursexceptionEntityUsingCompanyId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "BusinesshoursexceptionCollection", true, new[] { CompanyFields.CompanyId, BusinesshoursexceptionFields.CompanyId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and CategoryEntity over the 1:n relation they have, using the relation between the fields: Company.CompanyId - Category.CompanyId</summary>
		public virtual IEntityRelation CategoryEntityUsingCompanyId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "CategoryCollection", true, new[] { CompanyFields.CompanyId, CategoryFields.CompanyId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and CheckoutMethodEntity over the 1:n relation they have, using the relation between the fields: Company.CompanyId - CheckoutMethod.CompanyId</summary>
		public virtual IEntityRelation CheckoutMethodEntityUsingCompanyId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "CheckoutMethodCollection", true, new[] { CompanyFields.CompanyId, CheckoutMethodFields.CompanyId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and CheckoutMethodDeliverypointgroupEntity over the 1:n relation they have, using the relation between the fields: Company.CompanyId - CheckoutMethodDeliverypointgroup.ParentCompanyId</summary>
		public virtual IEntityRelation CheckoutMethodDeliverypointgroupEntityUsingParentCompanyId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "CheckoutMethodDeliverypointgroupCollection", true, new[] { CompanyFields.CompanyId, CheckoutMethodDeliverypointgroupFields.ParentCompanyId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and ClientEntity over the 1:n relation they have, using the relation between the fields: Company.CompanyId - Client.CompanyId</summary>
		public virtual IEntityRelation ClientEntityUsingCompanyId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "ClientCollection", true, new[] { CompanyFields.CompanyId, ClientFields.CompanyId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and ClientConfigurationEntity over the 1:n relation they have, using the relation between the fields: Company.CompanyId - ClientConfiguration.CompanyId</summary>
		public virtual IEntityRelation ClientConfigurationEntityUsingCompanyId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "ClientConfigurationCollection", true, new[] { CompanyFields.CompanyId, ClientConfigurationFields.CompanyId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and DeliverypointEntity over the 1:n relation they have, using the relation between the fields: Company.CompanyId - Deliverypoint.CompanyId</summary>
		public virtual IEntityRelation DeliverypointEntityUsingCompanyId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "DeliverypointCollection", true, new[] { CompanyFields.CompanyId, DeliverypointFields.CompanyId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and DeliverypointgroupEntity over the 1:n relation they have, using the relation between the fields: Company.CompanyId - Deliverypointgroup.CompanyId</summary>
		public virtual IEntityRelation DeliverypointgroupEntityUsingCompanyId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "DeliverypointgroupCollection", true, new[] { CompanyFields.CompanyId, DeliverypointgroupFields.CompanyId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and ExternalMenuEntity over the 1:n relation they have, using the relation between the fields: Company.CompanyId - ExternalMenu.ParentCompanyId</summary>
		public virtual IEntityRelation ExternalMenuEntityUsingParentCompanyId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "ExternalMenuCollection", true, new[] { CompanyFields.CompanyId, ExternalMenuFields.ParentCompanyId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and ExternalSystemEntity over the 1:n relation they have, using the relation between the fields: Company.CompanyId - ExternalSystem.CompanyId</summary>
		public virtual IEntityRelation ExternalSystemEntityUsingCompanyId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "ExternalSystemCollection", true, new[] { CompanyFields.CompanyId, ExternalSystemFields.CompanyId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and MediaEntity over the 1:n relation they have, using the relation between the fields: Company.CompanyId - Media.CompanyId</summary>
		public virtual IEntityRelation MediaEntityUsingCompanyId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "MediaCollection", true, new[] { CompanyFields.CompanyId, MediaFields.CompanyId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and MenuEntity over the 1:n relation they have, using the relation between the fields: Company.CompanyId - Menu.CompanyId</summary>
		public virtual IEntityRelation MenuEntityUsingCompanyId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "MenuCollection", true, new[] { CompanyFields.CompanyId, MenuFields.CompanyId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and NetmessageEntity over the 1:n relation they have, using the relation between the fields: Company.CompanyId - Netmessage.SenderCompanyId</summary>
		public virtual IEntityRelation NetmessageEntityUsingSenderCompanyId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "NetmessageCollection", true, new[] { CompanyFields.CompanyId, NetmessageFields.SenderCompanyId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and NetmessageEntity over the 1:n relation they have, using the relation between the fields: Company.CompanyId - Netmessage.ReceiverCompanyId</summary>
		public virtual IEntityRelation NetmessageEntityUsingReceiverCompanyId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "NetmessageCollection1", true, new[] { CompanyFields.CompanyId, NetmessageFields.ReceiverCompanyId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and OptInEntity over the 1:n relation they have, using the relation between the fields: Company.CompanyId - OptIn.ParentCompanyId</summary>
		public virtual IEntityRelation OptInEntityUsingParentCompanyId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "OptInCollection", true, new[] { CompanyFields.CompanyId, OptInFields.ParentCompanyId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and OrderEntity over the 1:n relation they have, using the relation between the fields: Company.CompanyId - Order.CompanyId</summary>
		public virtual IEntityRelation OrderEntityUsingCompanyId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "OrderCollection", true, new[] { CompanyFields.CompanyId, OrderFields.CompanyId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and OutletEntity over the 1:n relation they have, using the relation between the fields: Company.CompanyId - Outlet.CompanyId</summary>
		public virtual IEntityRelation OutletEntityUsingCompanyId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "OutletCollection", true, new[] { CompanyFields.CompanyId, OutletFields.CompanyId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and PaymentIntegrationConfigurationEntity over the 1:n relation they have, using the relation between the fields: Company.CompanyId - PaymentIntegrationConfiguration.CompanyId</summary>
		public virtual IEntityRelation PaymentIntegrationConfigurationEntityUsingCompanyId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "PaymentIntegrationConfigurationCollection", true, new[] { CompanyFields.CompanyId, PaymentIntegrationConfigurationFields.CompanyId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and PaymentProviderEntity over the 1:n relation they have, using the relation between the fields: Company.CompanyId - PaymentProvider.CompanyId</summary>
		public virtual IEntityRelation PaymentProviderEntityUsingCompanyId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "PaymentProviderCollection", true, new[] { CompanyFields.CompanyId, PaymentProviderFields.CompanyId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and PriceLevelEntity over the 1:n relation they have, using the relation between the fields: Company.CompanyId - PriceLevel.CompanyId</summary>
		public virtual IEntityRelation PriceLevelEntityUsingCompanyId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "PriceLevelCollection", true, new[] { CompanyFields.CompanyId, PriceLevelFields.CompanyId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and PriceScheduleEntity over the 1:n relation they have, using the relation between the fields: Company.CompanyId - PriceSchedule.CompanyId</summary>
		public virtual IEntityRelation PriceScheduleEntityUsingCompanyId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "PriceScheduleCollection", true, new[] { CompanyFields.CompanyId, PriceScheduleFields.CompanyId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and ProductEntity over the 1:n relation they have, using the relation between the fields: Company.CompanyId - Product.CompanyId</summary>
		public virtual IEntityRelation ProductEntityUsingCompanyId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "ProductCollection", true, new[] { CompanyFields.CompanyId, ProductFields.CompanyId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and ReceiptEntity over the 1:n relation they have, using the relation between the fields: Company.CompanyId - Receipt.CompanyId</summary>
		public virtual IEntityRelation ReceiptEntityUsingCompanyId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "ReceiptCollection", true, new[] { CompanyFields.CompanyId, ReceiptFields.CompanyId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and ReceiptTemplateEntity over the 1:n relation they have, using the relation between the fields: Company.CompanyId - ReceiptTemplate.CompanyId</summary>
		public virtual IEntityRelation ReceiptTemplateEntityUsingCompanyId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "ReceiptTemplateCollection", true, new[] { CompanyFields.CompanyId, ReceiptTemplateFields.CompanyId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and RouteEntity over the 1:n relation they have, using the relation between the fields: Company.CompanyId - Route.CompanyId</summary>
		public virtual IEntityRelation RouteEntityUsingCompanyId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "RouteCollection", true, new[] { CompanyFields.CompanyId, RouteFields.CompanyId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and ScheduleEntity over the 1:n relation they have, using the relation between the fields: Company.CompanyId - Schedule.CompanyId</summary>
		public virtual IEntityRelation ScheduleEntityUsingCompanyId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "ScheduleCollection", true, new[] { CompanyFields.CompanyId, ScheduleFields.CompanyId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and ServiceMethodEntity over the 1:n relation they have, using the relation between the fields: Company.CompanyId - ServiceMethod.CompanyId</summary>
		public virtual IEntityRelation ServiceMethodEntityUsingCompanyId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "ServiceMethodCollection", true, new[] { CompanyFields.CompanyId, ServiceMethodFields.CompanyId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and ServiceMethodDeliverypointgroupEntity over the 1:n relation they have, using the relation between the fields: Company.CompanyId - ServiceMethodDeliverypointgroup.ParentCompanyId</summary>
		public virtual IEntityRelation ServiceMethodDeliverypointgroupEntityUsingParentCompanyId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "ServiceMethodDeliverypointgroupCollection", true, new[] { CompanyFields.CompanyId, ServiceMethodDeliverypointgroupFields.ParentCompanyId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and TagEntity over the 1:n relation they have, using the relation between the fields: Company.CompanyId - Tag.CompanyId</summary>
		public virtual IEntityRelation TagEntityUsingCompanyId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "TagCollection", true, new[] { CompanyFields.CompanyId, TagFields.CompanyId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and TaxTariffEntity over the 1:n relation they have, using the relation between the fields: Company.CompanyId - TaxTariff.CompanyId</summary>
		public virtual IEntityRelation TaxTariffEntityUsingCompanyId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "TaxTariffCollection", true, new[] { CompanyFields.CompanyId, TaxTariffFields.CompanyId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and TerminalEntity over the 1:n relation they have, using the relation between the fields: Company.CompanyId - Terminal.CompanyId</summary>
		public virtual IEntityRelation TerminalEntityUsingCompanyId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "TerminalCollection", true, new[] { CompanyFields.CompanyId, TerminalFields.CompanyId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and RouteEntity over the m:1 relation they have, using the relation between the fields: Company.RouteId - Route.RouteId</summary>
		public virtual IEntityRelation RouteEntityUsingRouteId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Route", false, new[] { RouteFields.RouteId, CompanyFields.RouteId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and SupportpoolEntity over the m:1 relation they have, using the relation between the fields: Company.SupportpoolId - Supportpool.SupportpoolId</summary>
		public virtual IEntityRelation SupportpoolEntityUsingSupportpoolId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Supportpool", false, new[] { SupportpoolFields.SupportpoolId, CompanyFields.SupportpoolId }); }
		}

	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticCompanyRelations
	{
		internal static readonly IEntityRelation AlterationEntityUsingCompanyIdStatic = new CompanyRelations().AlterationEntityUsingCompanyId;
		internal static readonly IEntityRelation AlterationoptionEntityUsingCompanyIdStatic = new CompanyRelations().AlterationoptionEntityUsingCompanyId;
		internal static readonly IEntityRelation ApplicationConfigurationEntityUsingCompanyIdStatic = new CompanyRelations().ApplicationConfigurationEntityUsingCompanyId;
		internal static readonly IEntityRelation FeatureFlagEntityUsingCompanyIdStatic = new CompanyRelations().FeatureFlagEntityUsingCompanyId;
		internal static readonly IEntityRelation BusinesshourEntityUsingCompanyIdStatic = new CompanyRelations().BusinesshourEntityUsingCompanyId;
		internal static readonly IEntityRelation BusinesshoursexceptionEntityUsingCompanyIdStatic = new CompanyRelations().BusinesshoursexceptionEntityUsingCompanyId;
		internal static readonly IEntityRelation CategoryEntityUsingCompanyIdStatic = new CompanyRelations().CategoryEntityUsingCompanyId;
		internal static readonly IEntityRelation CheckoutMethodEntityUsingCompanyIdStatic = new CompanyRelations().CheckoutMethodEntityUsingCompanyId;
		internal static readonly IEntityRelation CheckoutMethodDeliverypointgroupEntityUsingParentCompanyIdStatic = new CompanyRelations().CheckoutMethodDeliverypointgroupEntityUsingParentCompanyId;
		internal static readonly IEntityRelation ClientEntityUsingCompanyIdStatic = new CompanyRelations().ClientEntityUsingCompanyId;
		internal static readonly IEntityRelation ClientConfigurationEntityUsingCompanyIdStatic = new CompanyRelations().ClientConfigurationEntityUsingCompanyId;
		internal static readonly IEntityRelation DeliverypointEntityUsingCompanyIdStatic = new CompanyRelations().DeliverypointEntityUsingCompanyId;
		internal static readonly IEntityRelation DeliverypointgroupEntityUsingCompanyIdStatic = new CompanyRelations().DeliverypointgroupEntityUsingCompanyId;
		internal static readonly IEntityRelation ExternalMenuEntityUsingParentCompanyIdStatic = new CompanyRelations().ExternalMenuEntityUsingParentCompanyId;
		internal static readonly IEntityRelation ExternalSystemEntityUsingCompanyIdStatic = new CompanyRelations().ExternalSystemEntityUsingCompanyId;
		internal static readonly IEntityRelation MediaEntityUsingCompanyIdStatic = new CompanyRelations().MediaEntityUsingCompanyId;
		internal static readonly IEntityRelation MenuEntityUsingCompanyIdStatic = new CompanyRelations().MenuEntityUsingCompanyId;
		internal static readonly IEntityRelation NetmessageEntityUsingSenderCompanyIdStatic = new CompanyRelations().NetmessageEntityUsingSenderCompanyId;
		internal static readonly IEntityRelation NetmessageEntityUsingReceiverCompanyIdStatic = new CompanyRelations().NetmessageEntityUsingReceiverCompanyId;
		internal static readonly IEntityRelation OptInEntityUsingParentCompanyIdStatic = new CompanyRelations().OptInEntityUsingParentCompanyId;
		internal static readonly IEntityRelation OrderEntityUsingCompanyIdStatic = new CompanyRelations().OrderEntityUsingCompanyId;
		internal static readonly IEntityRelation OutletEntityUsingCompanyIdStatic = new CompanyRelations().OutletEntityUsingCompanyId;
		internal static readonly IEntityRelation PaymentIntegrationConfigurationEntityUsingCompanyIdStatic = new CompanyRelations().PaymentIntegrationConfigurationEntityUsingCompanyId;
		internal static readonly IEntityRelation PaymentProviderEntityUsingCompanyIdStatic = new CompanyRelations().PaymentProviderEntityUsingCompanyId;
		internal static readonly IEntityRelation PriceLevelEntityUsingCompanyIdStatic = new CompanyRelations().PriceLevelEntityUsingCompanyId;
		internal static readonly IEntityRelation PriceScheduleEntityUsingCompanyIdStatic = new CompanyRelations().PriceScheduleEntityUsingCompanyId;
		internal static readonly IEntityRelation ProductEntityUsingCompanyIdStatic = new CompanyRelations().ProductEntityUsingCompanyId;
		internal static readonly IEntityRelation ReceiptEntityUsingCompanyIdStatic = new CompanyRelations().ReceiptEntityUsingCompanyId;
		internal static readonly IEntityRelation ReceiptTemplateEntityUsingCompanyIdStatic = new CompanyRelations().ReceiptTemplateEntityUsingCompanyId;
		internal static readonly IEntityRelation RouteEntityUsingCompanyIdStatic = new CompanyRelations().RouteEntityUsingCompanyId;
		internal static readonly IEntityRelation ScheduleEntityUsingCompanyIdStatic = new CompanyRelations().ScheduleEntityUsingCompanyId;
		internal static readonly IEntityRelation ServiceMethodEntityUsingCompanyIdStatic = new CompanyRelations().ServiceMethodEntityUsingCompanyId;
		internal static readonly IEntityRelation ServiceMethodDeliverypointgroupEntityUsingParentCompanyIdStatic = new CompanyRelations().ServiceMethodDeliverypointgroupEntityUsingParentCompanyId;
		internal static readonly IEntityRelation TagEntityUsingCompanyIdStatic = new CompanyRelations().TagEntityUsingCompanyId;
		internal static readonly IEntityRelation TaxTariffEntityUsingCompanyIdStatic = new CompanyRelations().TaxTariffEntityUsingCompanyId;
		internal static readonly IEntityRelation TerminalEntityUsingCompanyIdStatic = new CompanyRelations().TerminalEntityUsingCompanyId;
		internal static readonly IEntityRelation RouteEntityUsingRouteIdStatic = new CompanyRelations().RouteEntityUsingRouteId;
		internal static readonly IEntityRelation SupportpoolEntityUsingSupportpoolIdStatic = new CompanyRelations().SupportpoolEntityUsingSupportpoolId;

		/// <summary>CTor</summary>
		static StaticCompanyRelations() { }
	}
}

