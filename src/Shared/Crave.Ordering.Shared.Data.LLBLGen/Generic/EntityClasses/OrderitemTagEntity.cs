﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 5.8
// Code is generated on: 
// Code is generated using templates: Crave.TemplateBindings
// Templates vendor: Crave Interactive
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.FactoryClasses;
using Crave.Ordering.Shared.Data.LLBLGen.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

// ReSharper disable All
namespace Crave.Ordering.Shared.Data.LLBLGen.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'OrderitemTag'.<br/><br/></summary>
	[Serializable]
	public partial class OrderitemTagEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END
	
	{
		private OrderitemEntity _orderitem;
		private ProductEntity _product;
		private ProductCategoryTagEntity _productCategoryTag;
		private TagEntity _tag;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		private static OrderitemTagEntityStaticMetaData _staticMetaData = new OrderitemTagEntityStaticMetaData();
		private static OrderitemTagRelations _relationsFactory = new OrderitemTagRelations();		

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Orderitem</summary>
			public static readonly string Orderitem = "Orderitem";
			/// <summary>Member name Product</summary>
			public static readonly string Product = "Product";
			/// <summary>Member name ProductCategoryTag</summary>
			public static readonly string ProductCategoryTag = "ProductCategoryTag";
			/// <summary>Member name Tag</summary>
			public static readonly string Tag = "Tag";
		}

		/// <summary>Static meta-data storage for navigator related information</summary>
		protected class OrderitemTagEntityStaticMetaData : EntityStaticMetaDataBase
		{
			public OrderitemTagEntityStaticMetaData()
			{
				SetEntityCoreInfo("OrderitemTagEntity", InheritanceHierarchyType.None, false, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.OrderitemTagEntity, typeof(OrderitemTagEntity), typeof(OrderitemTagEntityFactory), false);
				AddNavigatorMetaData<OrderitemTagEntity, OrderitemEntity>("Orderitem", "OrderitemTagCollection", (a, b) => a._orderitem = b, a => a._orderitem, (a, b) => a.Orderitem = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticOrderitemTagRelations.OrderitemEntityUsingOrderitemIdStatic, ()=>new OrderitemTagRelations().OrderitemEntityUsingOrderitemId, null, new int[] { (int)OrderitemTagFieldIndex.OrderitemId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.OrderitemEntity);
				AddNavigatorMetaData<OrderitemTagEntity, ProductEntity>("Product", "OrderitemTagCollection", (a, b) => a._product = b, a => a._product, (a, b) => a.Product = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticOrderitemTagRelations.ProductEntityUsingProductIdStatic, ()=>new OrderitemTagRelations().ProductEntityUsingProductId, null, new int[] { (int)OrderitemTagFieldIndex.ProductId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ProductEntity);
				AddNavigatorMetaData<OrderitemTagEntity, ProductCategoryTagEntity>("ProductCategoryTag", "OrderitemTagCollection", (a, b) => a._productCategoryTag = b, a => a._productCategoryTag, (a, b) => a.ProductCategoryTag = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticOrderitemTagRelations.ProductCategoryTagEntityUsingProductCategoryTagIdStatic, ()=>new OrderitemTagRelations().ProductCategoryTagEntityUsingProductCategoryTagId, null, new int[] { (int)OrderitemTagFieldIndex.ProductCategoryTagId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ProductCategoryTagEntity);
				AddNavigatorMetaData<OrderitemTagEntity, TagEntity>("Tag", "OrderitemTagCollection", (a, b) => a._tag = b, a => a._tag, (a, b) => a.Tag = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticOrderitemTagRelations.TagEntityUsingTagIdStatic, ()=>new OrderitemTagRelations().TagEntityUsingTagId, null, new int[] { (int)OrderitemTagFieldIndex.TagId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.TagEntity);
			}
		}

		/// <summary>Static ctor</summary>
		static OrderitemTagEntity()
		{
		}

		/// <summary> CTor</summary>
		public OrderitemTagEntity()
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public OrderitemTagEntity(IEntityFields2 fields)
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this OrderitemTagEntity</param>
		public OrderitemTagEntity(IValidator validator)
		{
			InitClassEmpty(validator, null);
		}

		/// <summary> CTor</summary>
		/// <param name="orderitemTagId">PK value for OrderitemTag which data should be fetched into this OrderitemTag object</param>
		public OrderitemTagEntity(System.Int32 orderitemTagId) : this(orderitemTagId, null)
		{
		}

		/// <summary> CTor</summary>
		/// <param name="orderitemTagId">PK value for OrderitemTag which data should be fetched into this OrderitemTag object</param>
		/// <param name="validator">The custom validator object for this OrderitemTagEntity</param>
		public OrderitemTagEntity(System.Int32 orderitemTagId, IValidator validator)
		{
			InitClassEmpty(validator, null);
			this.OrderitemTagId = orderitemTagId;
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected OrderitemTagEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Orderitem' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoOrderitem() { return CreateRelationInfoForNavigator("Orderitem"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Product' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoProduct() { return CreateRelationInfoForNavigator("Product"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'ProductCategoryTag' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoProductCategoryTag() { return CreateRelationInfoForNavigator("ProductCategoryTag"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Tag' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoTag() { return CreateRelationInfoForNavigator("Tag"); }
		
		/// <inheritdoc/>
		protected override EntityStaticMetaDataBase GetEntityStaticMetaData() {	return _staticMetaData; }

		/// <summary>Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		/// <summary>Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this OrderitemTagEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary>The relations object holding all relations of this entity with other entity classes.</summary>
		public static OrderitemTagRelations Relations { get { return _relationsFactory; } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Orderitem' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathOrderitemEntity { get { return _staticMetaData.GetPrefetchPathElement("Orderitem", CommonEntityBase.CreateEntityCollection<OrderitemEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Product' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathProductEntity { get { return _staticMetaData.GetPrefetchPathElement("Product", CommonEntityBase.CreateEntityCollection<ProductEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'ProductCategoryTag' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathProductCategoryTagEntity { get { return _staticMetaData.GetPrefetchPathElement("ProductCategoryTag", CommonEntityBase.CreateEntityCollection<ProductCategoryTagEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Tag' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathTagEntity { get { return _staticMetaData.GetPrefetchPathElement("Tag", CommonEntityBase.CreateEntityCollection<TagEntity>()); } }

		/// <summary>The OrderitemTagId property of the Entity OrderitemTag<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderitemTag"."OrderitemTagId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 OrderitemTagId
		{
			get { return (System.Int32)GetValue((int)OrderitemTagFieldIndex.OrderitemTagId, true); }
			set { SetValue((int)OrderitemTagFieldIndex.OrderitemTagId, value); }		}

		/// <summary>The OrderitemId property of the Entity OrderitemTag<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderitemTag"."OrderitemId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 OrderitemId
		{
			get { return (System.Int32)GetValue((int)OrderitemTagFieldIndex.OrderitemId, true); }
			set	{ SetValue((int)OrderitemTagFieldIndex.OrderitemId, value); }
		}

		/// <summary>The ProductCategoryTagId property of the Entity OrderitemTag<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderitemTag"."ProductCategoryTagId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ProductCategoryTagId
		{
			get { return (Nullable<System.Int32>)GetValue((int)OrderitemTagFieldIndex.ProductCategoryTagId, false); }
			set	{ SetValue((int)OrderitemTagFieldIndex.ProductCategoryTagId, value); }
		}

		/// <summary>The ProductId property of the Entity OrderitemTag<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderitemTag"."ProductId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ProductId
		{
			get { return (Nullable<System.Int32>)GetValue((int)OrderitemTagFieldIndex.ProductId, false); }
			set	{ SetValue((int)OrderitemTagFieldIndex.ProductId, value); }
		}

		/// <summary>The TagId property of the Entity OrderitemTag<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderitemTag"."TagId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 TagId
		{
			get { return (System.Int32)GetValue((int)OrderitemTagFieldIndex.TagId, true); }
			set	{ SetValue((int)OrderitemTagFieldIndex.TagId, value); }
		}

		/// <summary>The ParentCompanyId property of the Entity OrderitemTag<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderitemTag"."ParentCompanyId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ParentCompanyId
		{
			get { return (Nullable<System.Int32>)GetValue((int)OrderitemTagFieldIndex.ParentCompanyId, false); }
			set	{ SetValue((int)OrderitemTagFieldIndex.ParentCompanyId, value); }
		}

		/// <summary>The CreatedUTC property of the Entity OrderitemTag<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderitemTag"."CreatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime CreatedUTC
		{
			get { return (System.DateTime)GetValue((int)OrderitemTagFieldIndex.CreatedUTC, true); }
			set	{ SetValue((int)OrderitemTagFieldIndex.CreatedUTC, value); }
		}

		/// <summary>The UpdatedUTC property of the Entity OrderitemTag<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderitemTag"."UpdatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)OrderitemTagFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)OrderitemTagFieldIndex.UpdatedUTC, value); }
		}

		/// <summary>Gets / sets related entity of type 'OrderitemEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual OrderitemEntity Orderitem
		{
			get { return _orderitem; }
			set { SetSingleRelatedEntityNavigator(value, "Orderitem"); }
		}

		/// <summary>Gets / sets related entity of type 'ProductEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual ProductEntity Product
		{
			get { return _product; }
			set { SetSingleRelatedEntityNavigator(value, "Product"); }
		}

		/// <summary>Gets / sets related entity of type 'ProductCategoryTagEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual ProductCategoryTagEntity ProductCategoryTag
		{
			get { return _productCategoryTag; }
			set { SetSingleRelatedEntityNavigator(value, "ProductCategoryTag"); }
		}

		/// <summary>Gets / sets related entity of type 'TagEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual TagEntity Tag
		{
			get { return _tag; }
			set { SetSingleRelatedEntityNavigator(value, "Tag"); }
		}


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen
{
	public enum OrderitemTagFieldIndex
	{
		///<summary>OrderitemTagId. </summary>
		OrderitemTagId,
		///<summary>OrderitemId. </summary>
		OrderitemId,
		///<summary>ProductCategoryTagId. </summary>
		ProductCategoryTagId,
		///<summary>ProductId. </summary>
		ProductId,
		///<summary>TagId. </summary>
		TagId,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: OrderitemTag. </summary>
	public partial class OrderitemTagRelations: RelationFactory
	{
		#region Custom Relation code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomRelationCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		


		/// <summary>Gets all relations and delete rules of the OrderitemTagEntity as a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects.</summary>
		/// <returns>a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects</returns>
		public virtual Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> GetRelationsAndDeleteRules()
		{
			Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> toReturn = new Dictionary<IEntityRelation, ReferentialConstraintDeleteRule>();
			return toReturn;
		}		
	
	

		/// <summary>Returns a new IEntityRelation object, between OrderitemTagEntity and OrderitemEntity over the m:1 relation they have, using the relation between the fields: OrderitemTag.OrderitemId - Orderitem.OrderitemId</summary>
		public virtual IEntityRelation OrderitemEntityUsingOrderitemId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Orderitem", false, new[] { OrderitemFields.OrderitemId, OrderitemTagFields.OrderitemId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between OrderitemTagEntity and ProductEntity over the m:1 relation they have, using the relation between the fields: OrderitemTag.ProductId - Product.ProductId</summary>
		public virtual IEntityRelation ProductEntityUsingProductId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Product", false, new[] { ProductFields.ProductId, OrderitemTagFields.ProductId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between OrderitemTagEntity and ProductCategoryTagEntity over the m:1 relation they have, using the relation between the fields: OrderitemTag.ProductCategoryTagId - ProductCategoryTag.ProductCategoryTagId</summary>
		public virtual IEntityRelation ProductCategoryTagEntityUsingProductCategoryTagId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "ProductCategoryTag", false, new[] { ProductCategoryTagFields.ProductCategoryTagId, OrderitemTagFields.ProductCategoryTagId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between OrderitemTagEntity and TagEntity over the m:1 relation they have, using the relation between the fields: OrderitemTag.TagId - Tag.TagId</summary>
		public virtual IEntityRelation TagEntityUsingTagId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Tag", false, new[] { TagFields.TagId, OrderitemTagFields.TagId }); }
		}

	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticOrderitemTagRelations
	{
		internal static readonly IEntityRelation OrderitemEntityUsingOrderitemIdStatic = new OrderitemTagRelations().OrderitemEntityUsingOrderitemId;
		internal static readonly IEntityRelation ProductEntityUsingProductIdStatic = new OrderitemTagRelations().ProductEntityUsingProductId;
		internal static readonly IEntityRelation ProductCategoryTagEntityUsingProductCategoryTagIdStatic = new OrderitemTagRelations().ProductCategoryTagEntityUsingProductCategoryTagId;
		internal static readonly IEntityRelation TagEntityUsingTagIdStatic = new OrderitemTagRelations().TagEntityUsingTagId;

		/// <summary>CTor</summary>
		static StaticOrderitemTagRelations() { }
	}
}

