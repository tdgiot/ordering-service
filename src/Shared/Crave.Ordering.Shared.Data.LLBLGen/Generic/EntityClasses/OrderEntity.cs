﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 5.8
// Code is generated on: 
// Code is generated using templates: Crave.TemplateBindings
// Templates vendor: Crave Interactive
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.FactoryClasses;
using Crave.Ordering.Shared.Data.LLBLGen.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

// ReSharper disable All
namespace Crave.Ordering.Shared.Data.LLBLGen.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'Order'.<br/><br/></summary>
	[Serializable]
	public partial class OrderEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END
	
	{
		private EntityCollection<AnalyticsProcessingTaskEntity> _analyticsProcessingTaskCollection;
		private EntityCollection<ExternalSystemLogEntity> _externalSystemLogCollection;
		private EntityCollection<OrderEntity> _orderCollection;
		private EntityCollection<OrderitemEntity> _orderitemCollection;
		private EntityCollection<OrderNotificationLogEntity> _orderNotificationLogCollection;
		private EntityCollection<OrderRoutestephandlerEntity> _orderRoutestephandlerCollection;
		private EntityCollection<OrderRoutestephandlerHistoryEntity> _orderRoutestephandlerHistoryCollection;
		private EntityCollection<PaymentTransactionEntity> _paymentTransactionCollection;
		private EntityCollection<ReceiptEntity> _receiptCollection;
		private CheckoutMethodEntity _checkoutMethod;
		private ClientEntity _client;
		private CompanyEntity _company;
		private CustomerEntity _customer;
		private DeliveryInformationEntity _deliveryInformation;
		private DeliverypointEntity _chargeToDeliverypoint;
		private DeliverypointEntity _deliverypoint;
		private OrderEntity _masterOrder;
		private OutletEntity _outlet;
		private ServiceMethodEntity _serviceMethod;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		private static OrderEntityStaticMetaData _staticMetaData = new OrderEntityStaticMetaData();
		private static OrderRelations _relationsFactory = new OrderRelations();		

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name CheckoutMethod</summary>
			public static readonly string CheckoutMethod = "CheckoutMethod";
			/// <summary>Member name Client</summary>
			public static readonly string Client = "Client";
			/// <summary>Member name Company</summary>
			public static readonly string Company = "Company";
			/// <summary>Member name Customer</summary>
			public static readonly string Customer = "Customer";
			/// <summary>Member name DeliveryInformation</summary>
			public static readonly string DeliveryInformation = "DeliveryInformation";
			/// <summary>Member name ChargeToDeliverypoint</summary>
			public static readonly string ChargeToDeliverypoint = "ChargeToDeliverypoint";
			/// <summary>Member name Deliverypoint</summary>
			public static readonly string Deliverypoint = "Deliverypoint";
			/// <summary>Member name MasterOrder</summary>
			public static readonly string MasterOrder = "MasterOrder";
			/// <summary>Member name Outlet</summary>
			public static readonly string Outlet = "Outlet";
			/// <summary>Member name ServiceMethod</summary>
			public static readonly string ServiceMethod = "ServiceMethod";
			/// <summary>Member name AnalyticsProcessingTaskCollection</summary>
			public static readonly string AnalyticsProcessingTaskCollection = "AnalyticsProcessingTaskCollection";
			/// <summary>Member name ExternalSystemLogCollection</summary>
			public static readonly string ExternalSystemLogCollection = "ExternalSystemLogCollection";
			/// <summary>Member name OrderCollection</summary>
			public static readonly string OrderCollection = "OrderCollection";
			/// <summary>Member name OrderitemCollection</summary>
			public static readonly string OrderitemCollection = "OrderitemCollection";
			/// <summary>Member name OrderNotificationLogCollection</summary>
			public static readonly string OrderNotificationLogCollection = "OrderNotificationLogCollection";
			/// <summary>Member name OrderRoutestephandlerCollection</summary>
			public static readonly string OrderRoutestephandlerCollection = "OrderRoutestephandlerCollection";
			/// <summary>Member name OrderRoutestephandlerHistoryCollection</summary>
			public static readonly string OrderRoutestephandlerHistoryCollection = "OrderRoutestephandlerHistoryCollection";
			/// <summary>Member name PaymentTransactionCollection</summary>
			public static readonly string PaymentTransactionCollection = "PaymentTransactionCollection";
			/// <summary>Member name ReceiptCollection</summary>
			public static readonly string ReceiptCollection = "ReceiptCollection";
		}

		/// <summary>Static meta-data storage for navigator related information</summary>
		protected class OrderEntityStaticMetaData : EntityStaticMetaDataBase
		{
			public OrderEntityStaticMetaData()
			{
				SetEntityCoreInfo("OrderEntity", InheritanceHierarchyType.None, false, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.OrderEntity, typeof(OrderEntity), typeof(OrderEntityFactory), false);
				AddNavigatorMetaData<OrderEntity, EntityCollection<AnalyticsProcessingTaskEntity>>("AnalyticsProcessingTaskCollection", a => a._analyticsProcessingTaskCollection, (a, b) => a._analyticsProcessingTaskCollection = b, a => a.AnalyticsProcessingTaskCollection, () => new OrderRelations().AnalyticsProcessingTaskEntityUsingOrderId, typeof(AnalyticsProcessingTaskEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.AnalyticsProcessingTaskEntity);
				AddNavigatorMetaData<OrderEntity, EntityCollection<ExternalSystemLogEntity>>("ExternalSystemLogCollection", a => a._externalSystemLogCollection, (a, b) => a._externalSystemLogCollection = b, a => a.ExternalSystemLogCollection, () => new OrderRelations().ExternalSystemLogEntityUsingOrderId, typeof(ExternalSystemLogEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ExternalSystemLogEntity);
				AddNavigatorMetaData<OrderEntity, EntityCollection<OrderEntity>>("OrderCollection", a => a._orderCollection, (a, b) => a._orderCollection = b, a => a.OrderCollection, () => new OrderRelations().OrderEntityUsingMasterOrderId, typeof(OrderEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.OrderEntity);
				AddNavigatorMetaData<OrderEntity, EntityCollection<OrderitemEntity>>("OrderitemCollection", a => a._orderitemCollection, (a, b) => a._orderitemCollection = b, a => a.OrderitemCollection, () => new OrderRelations().OrderitemEntityUsingOrderId, typeof(OrderitemEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.OrderitemEntity);
				AddNavigatorMetaData<OrderEntity, EntityCollection<OrderNotificationLogEntity>>("OrderNotificationLogCollection", a => a._orderNotificationLogCollection, (a, b) => a._orderNotificationLogCollection = b, a => a.OrderNotificationLogCollection, () => new OrderRelations().OrderNotificationLogEntityUsingOrderId, typeof(OrderNotificationLogEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.OrderNotificationLogEntity);
				AddNavigatorMetaData<OrderEntity, EntityCollection<OrderRoutestephandlerEntity>>("OrderRoutestephandlerCollection", a => a._orderRoutestephandlerCollection, (a, b) => a._orderRoutestephandlerCollection = b, a => a.OrderRoutestephandlerCollection, () => new OrderRelations().OrderRoutestephandlerEntityUsingOrderId, typeof(OrderRoutestephandlerEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.OrderRoutestephandlerEntity);
				AddNavigatorMetaData<OrderEntity, EntityCollection<OrderRoutestephandlerHistoryEntity>>("OrderRoutestephandlerHistoryCollection", a => a._orderRoutestephandlerHistoryCollection, (a, b) => a._orderRoutestephandlerHistoryCollection = b, a => a.OrderRoutestephandlerHistoryCollection, () => new OrderRelations().OrderRoutestephandlerHistoryEntityUsingOrderId, typeof(OrderRoutestephandlerHistoryEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.OrderRoutestephandlerHistoryEntity);
				AddNavigatorMetaData<OrderEntity, EntityCollection<PaymentTransactionEntity>>("PaymentTransactionCollection", a => a._paymentTransactionCollection, (a, b) => a._paymentTransactionCollection = b, a => a.PaymentTransactionCollection, () => new OrderRelations().PaymentTransactionEntityUsingOrderId, typeof(PaymentTransactionEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.PaymentTransactionEntity);
				AddNavigatorMetaData<OrderEntity, EntityCollection<ReceiptEntity>>("ReceiptCollection", a => a._receiptCollection, (a, b) => a._receiptCollection = b, a => a.ReceiptCollection, () => new OrderRelations().ReceiptEntityUsingOrderId, typeof(ReceiptEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ReceiptEntity);
				AddNavigatorMetaData<OrderEntity, CheckoutMethodEntity>("CheckoutMethod", "OrderCollection", (a, b) => a._checkoutMethod = b, a => a._checkoutMethod, (a, b) => a.CheckoutMethod = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticOrderRelations.CheckoutMethodEntityUsingCheckoutMethodIdStatic, ()=>new OrderRelations().CheckoutMethodEntityUsingCheckoutMethodId, null, new int[] { (int)OrderFieldIndex.CheckoutMethodId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.CheckoutMethodEntity);
				AddNavigatorMetaData<OrderEntity, ClientEntity>("Client", "OrderCollection", (a, b) => a._client = b, a => a._client, (a, b) => a.Client = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticOrderRelations.ClientEntityUsingClientIdStatic, ()=>new OrderRelations().ClientEntityUsingClientId, null, new int[] { (int)OrderFieldIndex.ClientId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ClientEntity);
				AddNavigatorMetaData<OrderEntity, CompanyEntity>("Company", "OrderCollection", (a, b) => a._company = b, a => a._company, (a, b) => a.Company = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticOrderRelations.CompanyEntityUsingCompanyIdStatic, ()=>new OrderRelations().CompanyEntityUsingCompanyId, null, new int[] { (int)OrderFieldIndex.CompanyId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.CompanyEntity);
				AddNavigatorMetaData<OrderEntity, CustomerEntity>("Customer", "OrderCollection", (a, b) => a._customer = b, a => a._customer, (a, b) => a.Customer = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticOrderRelations.CustomerEntityUsingCustomerIdStatic, ()=>new OrderRelations().CustomerEntityUsingCustomerId, null, new int[] { (int)OrderFieldIndex.CustomerId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.CustomerEntity);
				AddNavigatorMetaData<OrderEntity, DeliveryInformationEntity>("DeliveryInformation", "OrderCollection", (a, b) => a._deliveryInformation = b, a => a._deliveryInformation, (a, b) => a.DeliveryInformation = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticOrderRelations.DeliveryInformationEntityUsingDeliveryInformationIdStatic, ()=>new OrderRelations().DeliveryInformationEntityUsingDeliveryInformationId, null, new int[] { (int)OrderFieldIndex.DeliveryInformationId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.DeliveryInformationEntity);
				AddNavigatorMetaData<OrderEntity, DeliverypointEntity>("ChargeToDeliverypoint", "OrderCollectionViaCharge", (a, b) => a._chargeToDeliverypoint = b, a => a._chargeToDeliverypoint, (a, b) => a.ChargeToDeliverypoint = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticOrderRelations.DeliverypointEntityUsingChargeToDeliverypointIdStatic, ()=>new OrderRelations().DeliverypointEntityUsingChargeToDeliverypointId, null, new int[] { (int)OrderFieldIndex.ChargeToDeliverypointId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.DeliverypointEntity);
				AddNavigatorMetaData<OrderEntity, DeliverypointEntity>("Deliverypoint", "OrderCollection", (a, b) => a._deliverypoint = b, a => a._deliverypoint, (a, b) => a.Deliverypoint = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticOrderRelations.DeliverypointEntityUsingDeliverypointIdStatic, ()=>new OrderRelations().DeliverypointEntityUsingDeliverypointId, null, new int[] { (int)OrderFieldIndex.DeliverypointId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.DeliverypointEntity);
				AddNavigatorMetaData<OrderEntity, OrderEntity>("MasterOrder", "OrderCollection", (a, b) => a._masterOrder = b, a => a._masterOrder, (a, b) => a.MasterOrder = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticOrderRelations.OrderEntityUsingOrderIdMasterOrderIdStatic, ()=>new OrderRelations().OrderEntityUsingOrderIdMasterOrderId, null, new int[] { (int)OrderFieldIndex.MasterOrderId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.OrderEntity);
				AddNavigatorMetaData<OrderEntity, OutletEntity>("Outlet", "OrderCollection", (a, b) => a._outlet = b, a => a._outlet, (a, b) => a.Outlet = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticOrderRelations.OutletEntityUsingOutletIdStatic, ()=>new OrderRelations().OutletEntityUsingOutletId, null, new int[] { (int)OrderFieldIndex.OutletId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.OutletEntity);
				AddNavigatorMetaData<OrderEntity, ServiceMethodEntity>("ServiceMethod", "OrderCollection", (a, b) => a._serviceMethod = b, a => a._serviceMethod, (a, b) => a.ServiceMethod = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticOrderRelations.ServiceMethodEntityUsingServiceMethodIdStatic, ()=>new OrderRelations().ServiceMethodEntityUsingServiceMethodId, null, new int[] { (int)OrderFieldIndex.ServiceMethodId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ServiceMethodEntity);
			}
		}

		/// <summary>Static ctor</summary>
		static OrderEntity()
		{
		}

		/// <summary> CTor</summary>
		public OrderEntity()
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public OrderEntity(IEntityFields2 fields)
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this OrderEntity</param>
		public OrderEntity(IValidator validator)
		{
			InitClassEmpty(validator, null);
		}

		/// <summary> CTor</summary>
		/// <param name="orderId">PK value for Order which data should be fetched into this Order object</param>
		public OrderEntity(System.Int32 orderId) : this(orderId, null)
		{
		}

		/// <summary> CTor</summary>
		/// <param name="orderId">PK value for Order which data should be fetched into this Order object</param>
		/// <param name="validator">The custom validator object for this OrderEntity</param>
		public OrderEntity(System.Int32 orderId, IValidator validator)
		{
			InitClassEmpty(validator, null);
			this.OrderId = orderId;
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected OrderEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'AnalyticsProcessingTask' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoAnalyticsProcessingTaskCollection() { return CreateRelationInfoForNavigator("AnalyticsProcessingTaskCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'ExternalSystemLog' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoExternalSystemLogCollection() { return CreateRelationInfoForNavigator("ExternalSystemLogCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Order' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoOrderCollection() { return CreateRelationInfoForNavigator("OrderCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Orderitem' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoOrderitemCollection() { return CreateRelationInfoForNavigator("OrderitemCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'OrderNotificationLog' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoOrderNotificationLogCollection() { return CreateRelationInfoForNavigator("OrderNotificationLogCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'OrderRoutestephandler' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoOrderRoutestephandlerCollection() { return CreateRelationInfoForNavigator("OrderRoutestephandlerCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'OrderRoutestephandlerHistory' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoOrderRoutestephandlerHistoryCollection() { return CreateRelationInfoForNavigator("OrderRoutestephandlerHistoryCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'PaymentTransaction' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoPaymentTransactionCollection() { return CreateRelationInfoForNavigator("PaymentTransactionCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Receipt' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoReceiptCollection() { return CreateRelationInfoForNavigator("ReceiptCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'CheckoutMethod' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoCheckoutMethod() { return CreateRelationInfoForNavigator("CheckoutMethod"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Client' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoClient() { return CreateRelationInfoForNavigator("Client"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Company' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoCompany() { return CreateRelationInfoForNavigator("Company"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Customer' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoCustomer() { return CreateRelationInfoForNavigator("Customer"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'DeliveryInformation' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoDeliveryInformation() { return CreateRelationInfoForNavigator("DeliveryInformation"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Deliverypoint' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoChargeToDeliverypoint() { return CreateRelationInfoForNavigator("ChargeToDeliverypoint"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Deliverypoint' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoDeliverypoint() { return CreateRelationInfoForNavigator("Deliverypoint"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Order' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoMasterOrder() { return CreateRelationInfoForNavigator("MasterOrder"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Outlet' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoOutlet() { return CreateRelationInfoForNavigator("Outlet"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'ServiceMethod' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoServiceMethod() { return CreateRelationInfoForNavigator("ServiceMethod"); }
		
		/// <inheritdoc/>
		protected override EntityStaticMetaDataBase GetEntityStaticMetaData() {	return _staticMetaData; }

		/// <summary>Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		/// <summary>Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this OrderEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary>The relations object holding all relations of this entity with other entity classes.</summary>
		public static OrderRelations Relations { get { return _relationsFactory; } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'AnalyticsProcessingTask' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathAnalyticsProcessingTaskCollection { get { return _staticMetaData.GetPrefetchPathElement("AnalyticsProcessingTaskCollection", CommonEntityBase.CreateEntityCollection<AnalyticsProcessingTaskEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'ExternalSystemLog' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathExternalSystemLogCollection { get { return _staticMetaData.GetPrefetchPathElement("ExternalSystemLogCollection", CommonEntityBase.CreateEntityCollection<ExternalSystemLogEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Order' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathOrderCollection { get { return _staticMetaData.GetPrefetchPathElement("OrderCollection", CommonEntityBase.CreateEntityCollection<OrderEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Orderitem' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathOrderitemCollection { get { return _staticMetaData.GetPrefetchPathElement("OrderitemCollection", CommonEntityBase.CreateEntityCollection<OrderitemEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'OrderNotificationLog' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathOrderNotificationLogCollection { get { return _staticMetaData.GetPrefetchPathElement("OrderNotificationLogCollection", CommonEntityBase.CreateEntityCollection<OrderNotificationLogEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'OrderRoutestephandler' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathOrderRoutestephandlerCollection { get { return _staticMetaData.GetPrefetchPathElement("OrderRoutestephandlerCollection", CommonEntityBase.CreateEntityCollection<OrderRoutestephandlerEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'OrderRoutestephandlerHistory' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathOrderRoutestephandlerHistoryCollection { get { return _staticMetaData.GetPrefetchPathElement("OrderRoutestephandlerHistoryCollection", CommonEntityBase.CreateEntityCollection<OrderRoutestephandlerHistoryEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'PaymentTransaction' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathPaymentTransactionCollection { get { return _staticMetaData.GetPrefetchPathElement("PaymentTransactionCollection", CommonEntityBase.CreateEntityCollection<PaymentTransactionEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Receipt' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathReceiptCollection { get { return _staticMetaData.GetPrefetchPathElement("ReceiptCollection", CommonEntityBase.CreateEntityCollection<ReceiptEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'CheckoutMethod' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathCheckoutMethodEntity { get { return _staticMetaData.GetPrefetchPathElement("CheckoutMethod", CommonEntityBase.CreateEntityCollection<CheckoutMethodEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Client' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathClientEntity { get { return _staticMetaData.GetPrefetchPathElement("Client", CommonEntityBase.CreateEntityCollection<ClientEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Company' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathCompanyEntity { get { return _staticMetaData.GetPrefetchPathElement("Company", CommonEntityBase.CreateEntityCollection<CompanyEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Customer' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathCustomerEntity { get { return _staticMetaData.GetPrefetchPathElement("Customer", CommonEntityBase.CreateEntityCollection<CustomerEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'DeliveryInformation' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathDeliveryInformationEntity { get { return _staticMetaData.GetPrefetchPathElement("DeliveryInformation", CommonEntityBase.CreateEntityCollection<DeliveryInformationEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Deliverypoint' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathChargeToDeliverypointEntity { get { return _staticMetaData.GetPrefetchPathElement("ChargeToDeliverypoint", CommonEntityBase.CreateEntityCollection<DeliverypointEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Deliverypoint' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathDeliverypointEntity { get { return _staticMetaData.GetPrefetchPathElement("Deliverypoint", CommonEntityBase.CreateEntityCollection<DeliverypointEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Order' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathMasterOrderEntity { get { return _staticMetaData.GetPrefetchPathElement("MasterOrder", CommonEntityBase.CreateEntityCollection<OrderEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Outlet' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathOutletEntity { get { return _staticMetaData.GetPrefetchPathElement("Outlet", CommonEntityBase.CreateEntityCollection<OutletEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'ServiceMethod' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathServiceMethodEntity { get { return _staticMetaData.GetPrefetchPathElement("ServiceMethod", CommonEntityBase.CreateEntityCollection<ServiceMethodEntity>()); } }

		/// <summary>The OrderId property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."OrderId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 OrderId
		{
			get { return (System.Int32)GetValue((int)OrderFieldIndex.OrderId, true); }
			set { SetValue((int)OrderFieldIndex.OrderId, value); }		}

		/// <summary>The MasterOrderId property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."MasterOrderId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> MasterOrderId
		{
			get { return (Nullable<System.Int32>)GetValue((int)OrderFieldIndex.MasterOrderId, false); }
			set	{ SetValue((int)OrderFieldIndex.MasterOrderId, value); }
		}

		/// <summary>The CustomerId property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."CustomerId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CustomerId
		{
			get { return (Nullable<System.Int32>)GetValue((int)OrderFieldIndex.CustomerId, false); }
			set	{ SetValue((int)OrderFieldIndex.CustomerId, value); }
		}

		/// <summary>The CustomerFirstname property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."CustomerFirstname".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CustomerFirstname
		{
			get { return (System.String)GetValue((int)OrderFieldIndex.CustomerFirstname, true); }
			set	{ SetValue((int)OrderFieldIndex.CustomerFirstname, value); }
		}

		/// <summary>The CustomerLastname property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."CustomerLastname".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CustomerLastname
		{
			get { return (System.String)GetValue((int)OrderFieldIndex.CustomerLastname, true); }
			set	{ SetValue((int)OrderFieldIndex.CustomerLastname, value); }
		}

		/// <summary>The CustomerLastnamePrefix property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."CustomerLastnamePrefix".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CustomerLastnamePrefix
		{
			get { return (System.String)GetValue((int)OrderFieldIndex.CustomerLastnamePrefix, true); }
			set	{ SetValue((int)OrderFieldIndex.CustomerLastnamePrefix, value); }
		}

		/// <summary>The CustomerPhonenumber property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."CustomerPhonenumber".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CustomerPhonenumber
		{
			get { return (System.String)GetValue((int)OrderFieldIndex.CustomerPhonenumber, true); }
			set	{ SetValue((int)OrderFieldIndex.CustomerPhonenumber, value); }
		}

		/// <summary>The IsCustomerVip property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."IsCustomerVip".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsCustomerVip
		{
			get { return (System.Boolean)GetValue((int)OrderFieldIndex.IsCustomerVip, true); }
			set	{ SetValue((int)OrderFieldIndex.IsCustomerVip, value); }
		}

		/// <summary>The ClientId property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."ClientId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ClientId
		{
			get { return (Nullable<System.Int32>)GetValue((int)OrderFieldIndex.ClientId, false); }
			set	{ SetValue((int)OrderFieldIndex.ClientId, value); }
		}

		/// <summary>The ClientMacAddress property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."ClientMacAddress".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ClientMacAddress
		{
			get { return (System.String)GetValue((int)OrderFieldIndex.ClientMacAddress, true); }
			set	{ SetValue((int)OrderFieldIndex.ClientMacAddress, value); }
		}

		/// <summary>The CompanyId property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."CompanyId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CompanyId
		{
			get { return (System.Int32)GetValue((int)OrderFieldIndex.CompanyId, true); }
			set	{ SetValue((int)OrderFieldIndex.CompanyId, value); }
		}

		/// <summary>The CompanyName property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."CompanyName".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CompanyName
		{
			get { return (System.String)GetValue((int)OrderFieldIndex.CompanyName, true); }
			set	{ SetValue((int)OrderFieldIndex.CompanyName, value); }
		}

		/// <summary>The TimeZoneOlsonId property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."TimeZoneOlsonId".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String TimeZoneOlsonId
		{
			get { return (System.String)GetValue((int)OrderFieldIndex.TimeZoneOlsonId, true); }
			set	{ SetValue((int)OrderFieldIndex.TimeZoneOlsonId, value); }
		}

		/// <summary>The CurrencyCode property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."CurrencyCode".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 10.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CurrencyCode
		{
			get { return (System.String)GetValue((int)OrderFieldIndex.CurrencyCode, true); }
			set	{ SetValue((int)OrderFieldIndex.CurrencyCode, value); }
		}

		/// <summary>The DeliverypointId property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."DeliverypointId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> DeliverypointId
		{
			get { return (Nullable<System.Int32>)GetValue((int)OrderFieldIndex.DeliverypointId, false); }
			set	{ SetValue((int)OrderFieldIndex.DeliverypointId, value); }
		}

		/// <summary>The DeliverypointName property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."DeliverypointName".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String DeliverypointName
		{
			get { return (System.String)GetValue((int)OrderFieldIndex.DeliverypointName, true); }
			set	{ SetValue((int)OrderFieldIndex.DeliverypointName, value); }
		}

		/// <summary>The DeliverypointNumber property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."DeliverypointNumber".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 20.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String DeliverypointNumber
		{
			get { return (System.String)GetValue((int)OrderFieldIndex.DeliverypointNumber, true); }
			set	{ SetValue((int)OrderFieldIndex.DeliverypointNumber, value); }
		}

		/// <summary>The DeliverypointEnableAnalytics property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."DeliverypointEnableAnalytics".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> DeliverypointEnableAnalytics
		{
			get { return (Nullable<System.Boolean>)GetValue((int)OrderFieldIndex.DeliverypointEnableAnalytics, false); }
			set	{ SetValue((int)OrderFieldIndex.DeliverypointEnableAnalytics, value); }
		}

		/// <summary>The DeliverypointgroupName property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."DeliverypointgroupName".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String DeliverypointgroupName
		{
			get { return (System.String)GetValue((int)OrderFieldIndex.DeliverypointgroupName, true); }
			set	{ SetValue((int)OrderFieldIndex.DeliverypointgroupName, value); }
		}

		/// <summary>The Notes property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."Notes".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Notes
		{
			get { return (System.String)GetValue((int)OrderFieldIndex.Notes, true); }
			set	{ SetValue((int)OrderFieldIndex.Notes, value); }
		}

		/// <summary>The Type property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."Type".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Crave.Enums.OrderType Type
		{
			get { return (Crave.Enums.OrderType)GetValue((int)OrderFieldIndex.Type, true); }
			set	{ SetValue((int)OrderFieldIndex.Type, value); }
		}

		/// <summary>The TypeText property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."TypeText".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 100.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String TypeText
		{
			get { return (System.String)GetValue((int)OrderFieldIndex.TypeText, true); }
			set	{ SetValue((int)OrderFieldIndex.TypeText, value); }
		}

		/// <summary>The Status property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."Status".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Crave.Enums.OrderStatus Status
		{
			get { return (Crave.Enums.OrderStatus)GetValue((int)OrderFieldIndex.Status, true); }
			set	{ SetValue((int)OrderFieldIndex.Status, value); }
		}

		/// <summary>The StatusText property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."StatusText".<br/>Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 100.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String StatusText
		{
			get { return (System.String)GetValue((int)OrderFieldIndex.StatusText, true); }
			set	{ SetValue((int)OrderFieldIndex.StatusText, value); }
		}

		/// <summary>The ErrorCode property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."ErrorCode".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Crave.Enums.OrderErrorCode ErrorCode
		{
			get { return (Crave.Enums.OrderErrorCode)GetValue((int)OrderFieldIndex.ErrorCode, true); }
			set	{ SetValue((int)OrderFieldIndex.ErrorCode, value); }
		}

		/// <summary>The ErrorText property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."ErrorText".<br/>Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ErrorText
		{
			get { return (System.String)GetValue((int)OrderFieldIndex.ErrorText, true); }
			set	{ SetValue((int)OrderFieldIndex.ErrorText, value); }
		}

		/// <summary>The ErrorSentByEmail property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."ErrorSentByEmail".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean ErrorSentByEmail
		{
			get { return (System.Boolean)GetValue((int)OrderFieldIndex.ErrorSentByEmail, true); }
			set	{ SetValue((int)OrderFieldIndex.ErrorSentByEmail, value); }
		}

		/// <summary>The ErrorSentBySMS property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."ErrorSentBySMS".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean ErrorSentBySMS
		{
			get { return (System.Boolean)GetValue((int)OrderFieldIndex.ErrorSentBySMS, true); }
			set	{ SetValue((int)OrderFieldIndex.ErrorSentBySMS, value); }
		}

		/// <summary>The Guid property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."Guid".<br/>Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 128.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Guid
		{
			get { return (System.String)GetValue((int)OrderFieldIndex.Guid, true); }
			set	{ SetValue((int)OrderFieldIndex.Guid, value); }
		}

		/// <summary>The RoutingLog property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."RoutingLog".<br/>Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String RoutingLog
		{
			get { return (System.String)GetValue((int)OrderFieldIndex.RoutingLog, true); }
			set	{ SetValue((int)OrderFieldIndex.RoutingLog, value); }
		}

		/// <summary>The Printed property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."Printed".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean Printed
		{
			get { return (System.Boolean)GetValue((int)OrderFieldIndex.Printed, true); }
			set	{ SetValue((int)OrderFieldIndex.Printed, value); }
		}

		/// <summary>The MobileOrder property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."MobileOrder".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean MobileOrder
		{
			get { return (System.Boolean)GetValue((int)OrderFieldIndex.MobileOrder, true); }
			set	{ SetValue((int)OrderFieldIndex.MobileOrder, value); }
		}

		/// <summary>The AnalyticsTrackingIds property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."AnalyticsTrackingIds".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 100.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String AnalyticsTrackingIds
		{
			get { return (System.String)GetValue((int)OrderFieldIndex.AnalyticsTrackingIds, true); }
			set	{ SetValue((int)OrderFieldIndex.AnalyticsTrackingIds, value); }
		}

		/// <summary>The CreatedBy property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."CreatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)OrderFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)OrderFieldIndex.CreatedBy, value); }
		}

		/// <summary>The UpdatedBy property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."UpdatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)OrderFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)OrderFieldIndex.UpdatedBy, value); }
		}

		/// <summary>The Email property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."Email".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Email
		{
			get { return (System.String)GetValue((int)OrderFieldIndex.Email, true); }
			set	{ SetValue((int)OrderFieldIndex.Email, value); }
		}

		/// <summary>The AnalyticsPayLoad property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."AnalyticsPayLoad".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String AnalyticsPayLoad
		{
			get { return (System.String)GetValue((int)OrderFieldIndex.AnalyticsPayLoad, true); }
			set	{ SetValue((int)OrderFieldIndex.AnalyticsPayLoad, value); }
		}

		/// <summary>The HotSOSServiceOrderId property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."HotSOSServiceOrderId".<br/>Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 50.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String HotSOSServiceOrderId
		{
			get { return (System.String)GetValue((int)OrderFieldIndex.HotSOSServiceOrderId, true); }
			set	{ SetValue((int)OrderFieldIndex.HotSOSServiceOrderId, value); }
		}

		/// <summary>The ExternalOrderId property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."ExternalOrderId".<br/>Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 50.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ExternalOrderId
		{
			get { return (System.String)GetValue((int)OrderFieldIndex.ExternalOrderId, true); }
			set	{ SetValue((int)OrderFieldIndex.ExternalOrderId, value); }
		}

		/// <summary>The CreatedUTC property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."CreatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)OrderFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)OrderFieldIndex.CreatedUTC, value); }
		}

		/// <summary>The UpdatedUTC property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."UpdatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)OrderFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)OrderFieldIndex.UpdatedUTC, value); }
		}

		/// <summary>The ProcessedUTC property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."ProcessedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ProcessedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)OrderFieldIndex.ProcessedUTC, false); }
			set	{ SetValue((int)OrderFieldIndex.ProcessedUTC, value); }
		}

		/// <summary>The PlaceTimeUTC property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."PlaceTimeUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> PlaceTimeUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)OrderFieldIndex.PlaceTimeUTC, false); }
			set	{ SetValue((int)OrderFieldIndex.PlaceTimeUTC, value); }
		}

		/// <summary>The CheckoutMethodId property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."CheckoutMethodId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CheckoutMethodId
		{
			get { return (Nullable<System.Int32>)GetValue((int)OrderFieldIndex.CheckoutMethodId, false); }
			set	{ SetValue((int)OrderFieldIndex.CheckoutMethodId, value); }
		}

		/// <summary>The ServiceMethodId property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."ServiceMethodId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ServiceMethodId
		{
			get { return (Nullable<System.Int32>)GetValue((int)OrderFieldIndex.ServiceMethodId, false); }
			set	{ SetValue((int)OrderFieldIndex.ServiceMethodId, value); }
		}

		/// <summary>The CheckoutMethodName property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."CheckoutMethodName".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 256.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CheckoutMethodName
		{
			get { return (System.String)GetValue((int)OrderFieldIndex.CheckoutMethodName, true); }
			set	{ SetValue((int)OrderFieldIndex.CheckoutMethodName, value); }
		}

		/// <summary>The CheckoutMethodType property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."CheckoutMethodType".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<Crave.Enums.CheckoutType> CheckoutMethodType
		{
			get { return (Nullable<Crave.Enums.CheckoutType>)GetValue((int)OrderFieldIndex.CheckoutMethodType, false); }
			set	{ SetValue((int)OrderFieldIndex.CheckoutMethodType, value); }
		}

		/// <summary>The ServiceMethodName property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."ServiceMethodName".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 256.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ServiceMethodName
		{
			get { return (System.String)GetValue((int)OrderFieldIndex.ServiceMethodName, true); }
			set	{ SetValue((int)OrderFieldIndex.ServiceMethodName, value); }
		}

		/// <summary>The ServiceMethodType property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."ServiceMethodType".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<Crave.Enums.ServiceMethodType> ServiceMethodType
		{
			get { return (Nullable<Crave.Enums.ServiceMethodType>)GetValue((int)OrderFieldIndex.ServiceMethodType, false); }
			set	{ SetValue((int)OrderFieldIndex.ServiceMethodType, value); }
		}

		/// <summary>The CultureCode property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."CultureCode".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 10.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CultureCode
		{
			get { return (System.String)GetValue((int)OrderFieldIndex.CultureCode, true); }
			set	{ SetValue((int)OrderFieldIndex.CultureCode, value); }
		}

		/// <summary>The Total property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."Total".<br/>Table field type characteristics (type, precision, scale, length): Money, 19, 4, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal Total
		{
			get { return (System.Decimal)GetValue((int)OrderFieldIndex.Total, true); }
			set	{ SetValue((int)OrderFieldIndex.Total, value); }
		}

		/// <summary>The SubTotal property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."SubTotal".<br/>Table field type characteristics (type, precision, scale, length): Money, 19, 4, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal SubTotal
		{
			get { return (System.Decimal)GetValue((int)OrderFieldIndex.SubTotal, true); }
			set	{ SetValue((int)OrderFieldIndex.SubTotal, value); }
		}

		/// <summary>The DeliveryInformationId property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."DeliveryInformationId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> DeliveryInformationId
		{
			get { return (Nullable<System.Int32>)GetValue((int)OrderFieldIndex.DeliveryInformationId, false); }
			set	{ SetValue((int)OrderFieldIndex.DeliveryInformationId, value); }
		}

		/// <summary>The ChargeToDeliverypointId property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."ChargeToDeliverypointId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ChargeToDeliverypointId
		{
			get { return (Nullable<System.Int32>)GetValue((int)OrderFieldIndex.ChargeToDeliverypointId, false); }
			set	{ SetValue((int)OrderFieldIndex.ChargeToDeliverypointId, value); }
		}

		/// <summary>The ChargeToDeliverypointName property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."ChargeToDeliverypointName".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ChargeToDeliverypointName
		{
			get { return (System.String)GetValue((int)OrderFieldIndex.ChargeToDeliverypointName, true); }
			set	{ SetValue((int)OrderFieldIndex.ChargeToDeliverypointName, value); }
		}

		/// <summary>The ChargeToDeliverypointNumber property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."ChargeToDeliverypointNumber".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 20.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ChargeToDeliverypointNumber
		{
			get { return (System.String)GetValue((int)OrderFieldIndex.ChargeToDeliverypointNumber, true); }
			set	{ SetValue((int)OrderFieldIndex.ChargeToDeliverypointNumber, value); }
		}

		/// <summary>The ChargeToDeliverypointgroupName property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."ChargeToDeliverypointgroupName".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ChargeToDeliverypointgroupName
		{
			get { return (System.String)GetValue((int)OrderFieldIndex.ChargeToDeliverypointgroupName, true); }
			set	{ SetValue((int)OrderFieldIndex.ChargeToDeliverypointgroupName, value); }
		}

		/// <summary>The OutletId property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."OutletId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> OutletId
		{
			get { return (Nullable<System.Int32>)GetValue((int)OrderFieldIndex.OutletId, false); }
			set	{ SetValue((int)OrderFieldIndex.OutletId, value); }
		}

		/// <summary>The PricesIncludeTaxes property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."PricesIncludeTaxes".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean PricesIncludeTaxes
		{
			get { return (System.Boolean)GetValue((int)OrderFieldIndex.PricesIncludeTaxes, true); }
			set	{ SetValue((int)OrderFieldIndex.PricesIncludeTaxes, value); }
		}

		/// <summary>The OptInStatus property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."OptInStatus".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Crave.Enums.OptInStatus OptInStatus
		{
			get { return (Crave.Enums.OptInStatus)GetValue((int)OrderFieldIndex.OptInStatus, true); }
			set	{ SetValue((int)OrderFieldIndex.OptInStatus, value); }
		}

		/// <summary>The TaxBreakdown property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."TaxBreakdown".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Crave.Enums.TaxBreakdown TaxBreakdown
		{
			get { return (Crave.Enums.TaxBreakdown)GetValue((int)OrderFieldIndex.TaxBreakdown, true); }
			set	{ SetValue((int)OrderFieldIndex.TaxBreakdown, value); }
		}

		/// <summary>The CountryCode property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."CountryCode".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 10.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CountryCode
		{
			get { return (System.String)GetValue((int)OrderFieldIndex.CountryCode, true); }
			set	{ SetValue((int)OrderFieldIndex.CountryCode, value); }
		}

		/// <summary>The CoversCount property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."CoversCount".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CoversCount
		{
			get { return (Nullable<System.Int32>)GetValue((int)OrderFieldIndex.CoversCount, false); }
			set	{ SetValue((int)OrderFieldIndex.CoversCount, value); }
		}

		/// <summary>Gets the EntityCollection with the related entities of type 'AnalyticsProcessingTaskEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(AnalyticsProcessingTaskEntity))]
		public virtual EntityCollection<AnalyticsProcessingTaskEntity> AnalyticsProcessingTaskCollection { get { return GetOrCreateEntityCollection<AnalyticsProcessingTaskEntity, AnalyticsProcessingTaskEntityFactory>("Order", true, false, ref _analyticsProcessingTaskCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'ExternalSystemLogEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(ExternalSystemLogEntity))]
		public virtual EntityCollection<ExternalSystemLogEntity> ExternalSystemLogCollection { get { return GetOrCreateEntityCollection<ExternalSystemLogEntity, ExternalSystemLogEntityFactory>("Order", true, false, ref _externalSystemLogCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'OrderEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(OrderEntity))]
		public virtual EntityCollection<OrderEntity> OrderCollection { get { return GetOrCreateEntityCollection<OrderEntity, OrderEntityFactory>("MasterOrder", true, false, ref _orderCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'OrderitemEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(OrderitemEntity))]
		public virtual EntityCollection<OrderitemEntity> OrderitemCollection { get { return GetOrCreateEntityCollection<OrderitemEntity, OrderitemEntityFactory>("Order", true, false, ref _orderitemCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'OrderNotificationLogEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(OrderNotificationLogEntity))]
		public virtual EntityCollection<OrderNotificationLogEntity> OrderNotificationLogCollection { get { return GetOrCreateEntityCollection<OrderNotificationLogEntity, OrderNotificationLogEntityFactory>("Order", true, false, ref _orderNotificationLogCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'OrderRoutestephandlerEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(OrderRoutestephandlerEntity))]
		public virtual EntityCollection<OrderRoutestephandlerEntity> OrderRoutestephandlerCollection { get { return GetOrCreateEntityCollection<OrderRoutestephandlerEntity, OrderRoutestephandlerEntityFactory>("Order", true, false, ref _orderRoutestephandlerCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'OrderRoutestephandlerHistoryEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(OrderRoutestephandlerHistoryEntity))]
		public virtual EntityCollection<OrderRoutestephandlerHistoryEntity> OrderRoutestephandlerHistoryCollection { get { return GetOrCreateEntityCollection<OrderRoutestephandlerHistoryEntity, OrderRoutestephandlerHistoryEntityFactory>("Order", true, false, ref _orderRoutestephandlerHistoryCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'PaymentTransactionEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(PaymentTransactionEntity))]
		public virtual EntityCollection<PaymentTransactionEntity> PaymentTransactionCollection { get { return GetOrCreateEntityCollection<PaymentTransactionEntity, PaymentTransactionEntityFactory>("Order", true, false, ref _paymentTransactionCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'ReceiptEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(ReceiptEntity))]
		public virtual EntityCollection<ReceiptEntity> ReceiptCollection { get { return GetOrCreateEntityCollection<ReceiptEntity, ReceiptEntityFactory>("Order", true, false, ref _receiptCollection); } }

		/// <summary>Gets / sets related entity of type 'CheckoutMethodEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual CheckoutMethodEntity CheckoutMethod
		{
			get { return _checkoutMethod; }
			set { SetSingleRelatedEntityNavigator(value, "CheckoutMethod"); }
		}

		/// <summary>Gets / sets related entity of type 'ClientEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual ClientEntity Client
		{
			get { return _client; }
			set { SetSingleRelatedEntityNavigator(value, "Client"); }
		}

		/// <summary>Gets / sets related entity of type 'CompanyEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual CompanyEntity Company
		{
			get { return _company; }
			set { SetSingleRelatedEntityNavigator(value, "Company"); }
		}

		/// <summary>Gets / sets related entity of type 'CustomerEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual CustomerEntity Customer
		{
			get { return _customer; }
			set { SetSingleRelatedEntityNavigator(value, "Customer"); }
		}

		/// <summary>Gets / sets related entity of type 'DeliveryInformationEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual DeliveryInformationEntity DeliveryInformation
		{
			get { return _deliveryInformation; }
			set { SetSingleRelatedEntityNavigator(value, "DeliveryInformation"); }
		}

		/// <summary>Gets / sets related entity of type 'DeliverypointEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual DeliverypointEntity ChargeToDeliverypoint
		{
			get { return _chargeToDeliverypoint; }
			set { SetSingleRelatedEntityNavigator(value, "ChargeToDeliverypoint"); }
		}

		/// <summary>Gets / sets related entity of type 'DeliverypointEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual DeliverypointEntity Deliverypoint
		{
			get { return _deliverypoint; }
			set { SetSingleRelatedEntityNavigator(value, "Deliverypoint"); }
		}

		/// <summary>Gets / sets related entity of type 'OrderEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual OrderEntity MasterOrder
		{
			get { return _masterOrder; }
			set { SetSingleRelatedEntityNavigator(value, "MasterOrder"); }
		}

		/// <summary>Gets / sets related entity of type 'OutletEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual OutletEntity Outlet
		{
			get { return _outlet; }
			set { SetSingleRelatedEntityNavigator(value, "Outlet"); }
		}

		/// <summary>Gets / sets related entity of type 'ServiceMethodEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual ServiceMethodEntity ServiceMethod
		{
			get { return _serviceMethod; }
			set { SetSingleRelatedEntityNavigator(value, "ServiceMethod"); }
		}


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen
{
	public enum OrderFieldIndex
	{
		///<summary>OrderId. </summary>
		OrderId,
		///<summary>MasterOrderId. </summary>
		MasterOrderId,
		///<summary>CustomerId. </summary>
		CustomerId,
		///<summary>CustomerFirstname. </summary>
		CustomerFirstname,
		///<summary>CustomerLastname. </summary>
		CustomerLastname,
		///<summary>CustomerLastnamePrefix. </summary>
		CustomerLastnamePrefix,
		///<summary>CustomerPhonenumber. </summary>
		CustomerPhonenumber,
		///<summary>IsCustomerVip. </summary>
		IsCustomerVip,
		///<summary>ClientId. </summary>
		ClientId,
		///<summary>ClientMacAddress. </summary>
		ClientMacAddress,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>CompanyName. </summary>
		CompanyName,
		///<summary>TimeZoneOlsonId. </summary>
		TimeZoneOlsonId,
		///<summary>CurrencyCode. </summary>
		CurrencyCode,
		///<summary>DeliverypointId. </summary>
		DeliverypointId,
		///<summary>DeliverypointName. </summary>
		DeliverypointName,
		///<summary>DeliverypointNumber. </summary>
		DeliverypointNumber,
		///<summary>DeliverypointEnableAnalytics. </summary>
		DeliverypointEnableAnalytics,
		///<summary>DeliverypointgroupName. </summary>
		DeliverypointgroupName,
		///<summary>Notes. </summary>
		Notes,
		///<summary>Type. </summary>
		Type,
		///<summary>TypeText. </summary>
		TypeText,
		///<summary>Status. </summary>
		Status,
		///<summary>StatusText. </summary>
		StatusText,
		///<summary>ErrorCode. </summary>
		ErrorCode,
		///<summary>ErrorText. </summary>
		ErrorText,
		///<summary>ErrorSentByEmail. </summary>
		ErrorSentByEmail,
		///<summary>ErrorSentBySMS. </summary>
		ErrorSentBySMS,
		///<summary>Guid. </summary>
		Guid,
		///<summary>RoutingLog. </summary>
		RoutingLog,
		///<summary>Printed. </summary>
		Printed,
		///<summary>MobileOrder. </summary>
		MobileOrder,
		///<summary>AnalyticsTrackingIds. </summary>
		AnalyticsTrackingIds,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>Email. </summary>
		Email,
		///<summary>AnalyticsPayLoad. </summary>
		AnalyticsPayLoad,
		///<summary>HotSOSServiceOrderId. </summary>
		HotSOSServiceOrderId,
		///<summary>ExternalOrderId. </summary>
		ExternalOrderId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>ProcessedUTC. </summary>
		ProcessedUTC,
		///<summary>PlaceTimeUTC. </summary>
		PlaceTimeUTC,
		///<summary>CheckoutMethodId. </summary>
		CheckoutMethodId,
		///<summary>ServiceMethodId. </summary>
		ServiceMethodId,
		///<summary>CheckoutMethodName. </summary>
		CheckoutMethodName,
		///<summary>CheckoutMethodType. </summary>
		CheckoutMethodType,
		///<summary>ServiceMethodName. </summary>
		ServiceMethodName,
		///<summary>ServiceMethodType. </summary>
		ServiceMethodType,
		///<summary>CultureCode. </summary>
		CultureCode,
		///<summary>Total. </summary>
		Total,
		///<summary>SubTotal. </summary>
		SubTotal,
		///<summary>DeliveryInformationId. </summary>
		DeliveryInformationId,
		///<summary>ChargeToDeliverypointId. </summary>
		ChargeToDeliverypointId,
		///<summary>ChargeToDeliverypointName. </summary>
		ChargeToDeliverypointName,
		///<summary>ChargeToDeliverypointNumber. </summary>
		ChargeToDeliverypointNumber,
		///<summary>ChargeToDeliverypointgroupName. </summary>
		ChargeToDeliverypointgroupName,
		///<summary>OutletId. </summary>
		OutletId,
		///<summary>PricesIncludeTaxes. </summary>
		PricesIncludeTaxes,
		///<summary>OptInStatus. </summary>
		OptInStatus,
		///<summary>TaxBreakdown. </summary>
		TaxBreakdown,
		///<summary>CountryCode. </summary>
		CountryCode,
		///<summary>CoversCount. </summary>
		CoversCount,
		/// <summary></summary>
		AmountOfFields
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Order. </summary>
	public partial class OrderRelations: RelationFactory
	{
		#region Custom Relation code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomRelationCode
        private const ReferentialConstraintDeleteRule DeleteRuleForAnalyticsProcessingTaskEntityUsingOrderId = ReferentialConstraintDeleteRule.NoAction;
        private const ReferentialConstraintDeleteRule DeleteRuleForExternalSystemLogEntityUsingOrderId = ReferentialConstraintDeleteRule.SetNull;
        private const ReferentialConstraintDeleteRule DeleteRuleForOrderEntityUsingMasterOrderId = ReferentialConstraintDeleteRule.NoAction;
        private const ReferentialConstraintDeleteRule DeleteRuleForOrderitemEntityUsingOrderId = ReferentialConstraintDeleteRule.NoAction;
        private const ReferentialConstraintDeleteRule DeleteRuleForOrderNotificationLogEntityUsingOrderId = ReferentialConstraintDeleteRule.NoAction;
        private const ReferentialConstraintDeleteRule DeleteRuleForOrderRoutestephandlerEntityUsingOrderId = ReferentialConstraintDeleteRule.NoAction;
        private const ReferentialConstraintDeleteRule DeleteRuleForOrderRoutestephandlerHistoryEntityUsingOrderId = ReferentialConstraintDeleteRule.NoAction;
        private const ReferentialConstraintDeleteRule DeleteRuleForReceiptEntityUsingOrderId = ReferentialConstraintDeleteRule.NoAction;
        private const ReferentialConstraintDeleteRule DeleteRuleForPaymentTransactionEntityUsingOrderId = ReferentialConstraintDeleteRule.NoAction;
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		
		// private const ReferentialConstraintDeleteRule DeleteRuleForAnalyticsProcessingTaskEntityUsingOrderId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForExternalSystemLogEntityUsingOrderId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForOrderEntityUsingMasterOrderId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForOrderitemEntityUsingOrderId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForOrderNotificationLogEntityUsingOrderId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForOrderRoutestephandlerEntityUsingOrderId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForOrderRoutestephandlerHistoryEntityUsingOrderId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForPaymentTransactionEntityUsingOrderId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForReceiptEntityUsingOrderId = ReferentialConstraintDeleteRule


		/// <summary>Gets all relations and delete rules of the OrderEntity as a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects.</summary>
		/// <returns>a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects</returns>
		public virtual Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> GetRelationsAndDeleteRules()
		{
			Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> toReturn = new Dictionary<IEntityRelation, ReferentialConstraintDeleteRule>();
			toReturn.Add(this.AnalyticsProcessingTaskEntityUsingOrderId, OrderRelations.DeleteRuleForAnalyticsProcessingTaskEntityUsingOrderId);
			toReturn.Add(this.ExternalSystemLogEntityUsingOrderId, OrderRelations.DeleteRuleForExternalSystemLogEntityUsingOrderId);
			toReturn.Add(this.OrderEntityUsingMasterOrderId, OrderRelations.DeleteRuleForOrderEntityUsingMasterOrderId);
			toReturn.Add(this.OrderitemEntityUsingOrderId, OrderRelations.DeleteRuleForOrderitemEntityUsingOrderId);
			toReturn.Add(this.OrderNotificationLogEntityUsingOrderId, OrderRelations.DeleteRuleForOrderNotificationLogEntityUsingOrderId);
			toReturn.Add(this.OrderRoutestephandlerEntityUsingOrderId, OrderRelations.DeleteRuleForOrderRoutestephandlerEntityUsingOrderId);
			toReturn.Add(this.OrderRoutestephandlerHistoryEntityUsingOrderId, OrderRelations.DeleteRuleForOrderRoutestephandlerHistoryEntityUsingOrderId);
			toReturn.Add(this.PaymentTransactionEntityUsingOrderId, OrderRelations.DeleteRuleForPaymentTransactionEntityUsingOrderId);
			toReturn.Add(this.ReceiptEntityUsingOrderId, OrderRelations.DeleteRuleForReceiptEntityUsingOrderId);
			return toReturn;
		}		
	
	
		/// <summary>Returns a new IEntityRelation object, between OrderEntity and AnalyticsProcessingTaskEntity over the 1:n relation they have, using the relation between the fields: Order.OrderId - AnalyticsProcessingTask.OrderId</summary>
		public virtual IEntityRelation AnalyticsProcessingTaskEntityUsingOrderId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "AnalyticsProcessingTaskCollection", true, new[] { OrderFields.OrderId, AnalyticsProcessingTaskFields.OrderId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between OrderEntity and ExternalSystemLogEntity over the 1:n relation they have, using the relation between the fields: Order.OrderId - ExternalSystemLog.OrderId</summary>
		public virtual IEntityRelation ExternalSystemLogEntityUsingOrderId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "ExternalSystemLogCollection", true, new[] { OrderFields.OrderId, ExternalSystemLogFields.OrderId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between OrderEntity and OrderEntity over the 1:n relation they have, using the relation between the fields: Order.OrderId - Order.MasterOrderId</summary>
		public virtual IEntityRelation OrderEntityUsingMasterOrderId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "OrderCollection", true, new[] { OrderFields.OrderId, OrderFields.MasterOrderId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between OrderEntity and OrderitemEntity over the 1:n relation they have, using the relation between the fields: Order.OrderId - Orderitem.OrderId</summary>
		public virtual IEntityRelation OrderitemEntityUsingOrderId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "OrderitemCollection", true, new[] { OrderFields.OrderId, OrderitemFields.OrderId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between OrderEntity and OrderNotificationLogEntity over the 1:n relation they have, using the relation between the fields: Order.OrderId - OrderNotificationLog.OrderId</summary>
		public virtual IEntityRelation OrderNotificationLogEntityUsingOrderId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "OrderNotificationLogCollection", true, new[] { OrderFields.OrderId, OrderNotificationLogFields.OrderId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between OrderEntity and OrderRoutestephandlerEntity over the 1:n relation they have, using the relation between the fields: Order.OrderId - OrderRoutestephandler.OrderId</summary>
		public virtual IEntityRelation OrderRoutestephandlerEntityUsingOrderId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "OrderRoutestephandlerCollection", true, new[] { OrderFields.OrderId, OrderRoutestephandlerFields.OrderId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between OrderEntity and OrderRoutestephandlerHistoryEntity over the 1:n relation they have, using the relation between the fields: Order.OrderId - OrderRoutestephandlerHistory.OrderId</summary>
		public virtual IEntityRelation OrderRoutestephandlerHistoryEntityUsingOrderId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "OrderRoutestephandlerHistoryCollection", true, new[] { OrderFields.OrderId, OrderRoutestephandlerHistoryFields.OrderId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between OrderEntity and PaymentTransactionEntity over the 1:n relation they have, using the relation between the fields: Order.OrderId - PaymentTransaction.OrderId</summary>
		public virtual IEntityRelation PaymentTransactionEntityUsingOrderId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "PaymentTransactionCollection", true, new[] { OrderFields.OrderId, PaymentTransactionFields.OrderId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between OrderEntity and ReceiptEntity over the 1:n relation they have, using the relation between the fields: Order.OrderId - Receipt.OrderId</summary>
		public virtual IEntityRelation ReceiptEntityUsingOrderId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "ReceiptCollection", true, new[] { OrderFields.OrderId, ReceiptFields.OrderId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between OrderEntity and CheckoutMethodEntity over the m:1 relation they have, using the relation between the fields: Order.CheckoutMethodId - CheckoutMethod.CheckoutMethodId</summary>
		public virtual IEntityRelation CheckoutMethodEntityUsingCheckoutMethodId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "CheckoutMethod", false, new[] { CheckoutMethodFields.CheckoutMethodId, OrderFields.CheckoutMethodId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between OrderEntity and ClientEntity over the m:1 relation they have, using the relation between the fields: Order.ClientId - Client.ClientId</summary>
		public virtual IEntityRelation ClientEntityUsingClientId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Client", false, new[] { ClientFields.ClientId, OrderFields.ClientId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between OrderEntity and CompanyEntity over the m:1 relation they have, using the relation between the fields: Order.CompanyId - Company.CompanyId</summary>
		public virtual IEntityRelation CompanyEntityUsingCompanyId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Company", false, new[] { CompanyFields.CompanyId, OrderFields.CompanyId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between OrderEntity and CustomerEntity over the m:1 relation they have, using the relation between the fields: Order.CustomerId - Customer.CustomerId</summary>
		public virtual IEntityRelation CustomerEntityUsingCustomerId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Customer", false, new[] { CustomerFields.CustomerId, OrderFields.CustomerId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between OrderEntity and DeliveryInformationEntity over the m:1 relation they have, using the relation between the fields: Order.DeliveryInformationId - DeliveryInformation.DeliveryInformationId</summary>
		public virtual IEntityRelation DeliveryInformationEntityUsingDeliveryInformationId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "DeliveryInformation", false, new[] { DeliveryInformationFields.DeliveryInformationId, OrderFields.DeliveryInformationId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between OrderEntity and DeliverypointEntity over the m:1 relation they have, using the relation between the fields: Order.ChargeToDeliverypointId - Deliverypoint.DeliverypointId</summary>
		public virtual IEntityRelation DeliverypointEntityUsingChargeToDeliverypointId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "ChargeToDeliverypoint", false, new[] { DeliverypointFields.DeliverypointId, OrderFields.ChargeToDeliverypointId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between OrderEntity and DeliverypointEntity over the m:1 relation they have, using the relation between the fields: Order.DeliverypointId - Deliverypoint.DeliverypointId</summary>
		public virtual IEntityRelation DeliverypointEntityUsingDeliverypointId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Deliverypoint", false, new[] { DeliverypointFields.DeliverypointId, OrderFields.DeliverypointId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between OrderEntity and OrderEntity over the m:1 relation they have, using the relation between the fields: Order.MasterOrderId - Order.OrderId</summary>
		public virtual IEntityRelation OrderEntityUsingOrderIdMasterOrderId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "MasterOrder", false, new[] { OrderFields.OrderId, OrderFields.MasterOrderId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between OrderEntity and OutletEntity over the m:1 relation they have, using the relation between the fields: Order.OutletId - Outlet.OutletId</summary>
		public virtual IEntityRelation OutletEntityUsingOutletId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Outlet", false, new[] { OutletFields.OutletId, OrderFields.OutletId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between OrderEntity and ServiceMethodEntity over the m:1 relation they have, using the relation between the fields: Order.ServiceMethodId - ServiceMethod.ServiceMethodId</summary>
		public virtual IEntityRelation ServiceMethodEntityUsingServiceMethodId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "ServiceMethod", false, new[] { ServiceMethodFields.ServiceMethodId, OrderFields.ServiceMethodId }); }
		}

	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticOrderRelations
	{
		internal static readonly IEntityRelation AnalyticsProcessingTaskEntityUsingOrderIdStatic = new OrderRelations().AnalyticsProcessingTaskEntityUsingOrderId;
		internal static readonly IEntityRelation ExternalSystemLogEntityUsingOrderIdStatic = new OrderRelations().ExternalSystemLogEntityUsingOrderId;
		internal static readonly IEntityRelation OrderEntityUsingMasterOrderIdStatic = new OrderRelations().OrderEntityUsingMasterOrderId;
		internal static readonly IEntityRelation OrderitemEntityUsingOrderIdStatic = new OrderRelations().OrderitemEntityUsingOrderId;
		internal static readonly IEntityRelation OrderNotificationLogEntityUsingOrderIdStatic = new OrderRelations().OrderNotificationLogEntityUsingOrderId;
		internal static readonly IEntityRelation OrderRoutestephandlerEntityUsingOrderIdStatic = new OrderRelations().OrderRoutestephandlerEntityUsingOrderId;
		internal static readonly IEntityRelation OrderRoutestephandlerHistoryEntityUsingOrderIdStatic = new OrderRelations().OrderRoutestephandlerHistoryEntityUsingOrderId;
		internal static readonly IEntityRelation PaymentTransactionEntityUsingOrderIdStatic = new OrderRelations().PaymentTransactionEntityUsingOrderId;
		internal static readonly IEntityRelation ReceiptEntityUsingOrderIdStatic = new OrderRelations().ReceiptEntityUsingOrderId;
		internal static readonly IEntityRelation CheckoutMethodEntityUsingCheckoutMethodIdStatic = new OrderRelations().CheckoutMethodEntityUsingCheckoutMethodId;
		internal static readonly IEntityRelation ClientEntityUsingClientIdStatic = new OrderRelations().ClientEntityUsingClientId;
		internal static readonly IEntityRelation CompanyEntityUsingCompanyIdStatic = new OrderRelations().CompanyEntityUsingCompanyId;
		internal static readonly IEntityRelation CustomerEntityUsingCustomerIdStatic = new OrderRelations().CustomerEntityUsingCustomerId;
		internal static readonly IEntityRelation DeliveryInformationEntityUsingDeliveryInformationIdStatic = new OrderRelations().DeliveryInformationEntityUsingDeliveryInformationId;
		internal static readonly IEntityRelation DeliverypointEntityUsingChargeToDeliverypointIdStatic = new OrderRelations().DeliverypointEntityUsingChargeToDeliverypointId;
		internal static readonly IEntityRelation DeliverypointEntityUsingDeliverypointIdStatic = new OrderRelations().DeliverypointEntityUsingDeliverypointId;
		internal static readonly IEntityRelation OrderEntityUsingOrderIdMasterOrderIdStatic = new OrderRelations().OrderEntityUsingOrderIdMasterOrderId;
		internal static readonly IEntityRelation OutletEntityUsingOutletIdStatic = new OrderRelations().OutletEntityUsingOutletId;
		internal static readonly IEntityRelation ServiceMethodEntityUsingServiceMethodIdStatic = new OrderRelations().ServiceMethodEntityUsingServiceMethodId;

		/// <summary>CTor</summary>
		static StaticOrderRelations() { }
	}
}

