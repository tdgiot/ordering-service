﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 5.8
// Code is generated on: 
// Code is generated using templates: Crave.TemplateBindings
// Templates vendor: Crave Interactive
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.FactoryClasses;
using Crave.Ordering.Shared.Data.LLBLGen.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

// ReSharper disable All
namespace Crave.Ordering.Shared.Data.LLBLGen.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'PriceLevel'.<br/><br/></summary>
	[Serializable]
	public partial class PriceLevelEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END
	
	{
		private EntityCollection<PriceLevelItemEntity> _priceLevelItemCollection;
		private EntityCollection<PriceScheduleItemEntity> _priceScheduleItemCollection;
		private CompanyEntity _company;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		private static PriceLevelEntityStaticMetaData _staticMetaData = new PriceLevelEntityStaticMetaData();
		private static PriceLevelRelations _relationsFactory = new PriceLevelRelations();		

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Company</summary>
			public static readonly string Company = "Company";
			/// <summary>Member name PriceLevelItemCollection</summary>
			public static readonly string PriceLevelItemCollection = "PriceLevelItemCollection";
			/// <summary>Member name PriceScheduleItemCollection</summary>
			public static readonly string PriceScheduleItemCollection = "PriceScheduleItemCollection";
		}

		/// <summary>Static meta-data storage for navigator related information</summary>
		protected class PriceLevelEntityStaticMetaData : EntityStaticMetaDataBase
		{
			public PriceLevelEntityStaticMetaData()
			{
				SetEntityCoreInfo("PriceLevelEntity", InheritanceHierarchyType.None, false, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.PriceLevelEntity, typeof(PriceLevelEntity), typeof(PriceLevelEntityFactory), false);
				AddNavigatorMetaData<PriceLevelEntity, EntityCollection<PriceLevelItemEntity>>("PriceLevelItemCollection", a => a._priceLevelItemCollection, (a, b) => a._priceLevelItemCollection = b, a => a.PriceLevelItemCollection, () => new PriceLevelRelations().PriceLevelItemEntityUsingPriceLevelId, typeof(PriceLevelItemEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.PriceLevelItemEntity);
				AddNavigatorMetaData<PriceLevelEntity, EntityCollection<PriceScheduleItemEntity>>("PriceScheduleItemCollection", a => a._priceScheduleItemCollection, (a, b) => a._priceScheduleItemCollection = b, a => a.PriceScheduleItemCollection, () => new PriceLevelRelations().PriceScheduleItemEntityUsingPriceLevelId, typeof(PriceScheduleItemEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.PriceScheduleItemEntity);
				AddNavigatorMetaData<PriceLevelEntity, CompanyEntity>("Company", "PriceLevelCollection", (a, b) => a._company = b, a => a._company, (a, b) => a.Company = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticPriceLevelRelations.CompanyEntityUsingCompanyIdStatic, ()=>new PriceLevelRelations().CompanyEntityUsingCompanyId, null, new int[] { (int)PriceLevelFieldIndex.CompanyId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.CompanyEntity);
			}
		}

		/// <summary>Static ctor</summary>
		static PriceLevelEntity()
		{
		}

		/// <summary> CTor</summary>
		public PriceLevelEntity()
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public PriceLevelEntity(IEntityFields2 fields)
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this PriceLevelEntity</param>
		public PriceLevelEntity(IValidator validator)
		{
			InitClassEmpty(validator, null);
		}

		/// <summary> CTor</summary>
		/// <param name="priceLevelId">PK value for PriceLevel which data should be fetched into this PriceLevel object</param>
		public PriceLevelEntity(System.Int32 priceLevelId) : this(priceLevelId, null)
		{
		}

		/// <summary> CTor</summary>
		/// <param name="priceLevelId">PK value for PriceLevel which data should be fetched into this PriceLevel object</param>
		/// <param name="validator">The custom validator object for this PriceLevelEntity</param>
		public PriceLevelEntity(System.Int32 priceLevelId, IValidator validator)
		{
			InitClassEmpty(validator, null);
			this.PriceLevelId = priceLevelId;
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected PriceLevelEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'PriceLevelItem' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoPriceLevelItemCollection() { return CreateRelationInfoForNavigator("PriceLevelItemCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'PriceScheduleItem' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoPriceScheduleItemCollection() { return CreateRelationInfoForNavigator("PriceScheduleItemCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Company' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoCompany() { return CreateRelationInfoForNavigator("Company"); }
		
		/// <inheritdoc/>
		protected override EntityStaticMetaDataBase GetEntityStaticMetaData() {	return _staticMetaData; }

		/// <summary>Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		/// <summary>Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this PriceLevelEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary>The relations object holding all relations of this entity with other entity classes.</summary>
		public static PriceLevelRelations Relations { get { return _relationsFactory; } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'PriceLevelItem' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathPriceLevelItemCollection { get { return _staticMetaData.GetPrefetchPathElement("PriceLevelItemCollection", CommonEntityBase.CreateEntityCollection<PriceLevelItemEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'PriceScheduleItem' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathPriceScheduleItemCollection { get { return _staticMetaData.GetPrefetchPathElement("PriceScheduleItemCollection", CommonEntityBase.CreateEntityCollection<PriceScheduleItemEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Company' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathCompanyEntity { get { return _staticMetaData.GetPrefetchPathElement("Company", CommonEntityBase.CreateEntityCollection<CompanyEntity>()); } }

		/// <summary>The PriceLevelId property of the Entity PriceLevel<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceLevel"."PriceLevelId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 PriceLevelId
		{
			get { return (System.Int32)GetValue((int)PriceLevelFieldIndex.PriceLevelId, true); }
			set { SetValue((int)PriceLevelFieldIndex.PriceLevelId, value); }		}

		/// <summary>The CompanyId property of the Entity PriceLevel<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceLevel"."CompanyId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CompanyId
		{
			get { return (System.Int32)GetValue((int)PriceLevelFieldIndex.CompanyId, true); }
			set	{ SetValue((int)PriceLevelFieldIndex.CompanyId, value); }
		}

		/// <summary>The Name property of the Entity PriceLevel<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceLevel"."Name".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)PriceLevelFieldIndex.Name, true); }
			set	{ SetValue((int)PriceLevelFieldIndex.Name, value); }
		}

		/// <summary>The CreatedUTC property of the Entity PriceLevel<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceLevel"."CreatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)PriceLevelFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)PriceLevelFieldIndex.CreatedUTC, value); }
		}

		/// <summary>The CreatedBy property of the Entity PriceLevel<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceLevel"."CreatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CreatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)PriceLevelFieldIndex.CreatedBy, false); }
			set	{ SetValue((int)PriceLevelFieldIndex.CreatedBy, value); }
		}

		/// <summary>The UpdatedUTC property of the Entity PriceLevel<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceLevel"."UpdatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)PriceLevelFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)PriceLevelFieldIndex.UpdatedUTC, value); }
		}

		/// <summary>The UpdatedBy property of the Entity PriceLevel<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceLevel"."UpdatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UpdatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)PriceLevelFieldIndex.UpdatedBy, false); }
			set	{ SetValue((int)PriceLevelFieldIndex.UpdatedBy, value); }
		}

		/// <summary>Gets the EntityCollection with the related entities of type 'PriceLevelItemEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(PriceLevelItemEntity))]
		public virtual EntityCollection<PriceLevelItemEntity> PriceLevelItemCollection { get { return GetOrCreateEntityCollection<PriceLevelItemEntity, PriceLevelItemEntityFactory>("PriceLevel", true, false, ref _priceLevelItemCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'PriceScheduleItemEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(PriceScheduleItemEntity))]
		public virtual EntityCollection<PriceScheduleItemEntity> PriceScheduleItemCollection { get { return GetOrCreateEntityCollection<PriceScheduleItemEntity, PriceScheduleItemEntityFactory>("PriceLevel", true, false, ref _priceScheduleItemCollection); } }

		/// <summary>Gets / sets related entity of type 'CompanyEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual CompanyEntity Company
		{
			get { return _company; }
			set { SetSingleRelatedEntityNavigator(value, "Company"); }
		}


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen
{
	public enum PriceLevelFieldIndex
	{
		///<summary>PriceLevelId. </summary>
		PriceLevelId,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>Name. </summary>
		Name,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		/// <summary></summary>
		AmountOfFields
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: PriceLevel. </summary>
	public partial class PriceLevelRelations: RelationFactory
	{
		#region Custom Relation code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomRelationCode
        private const ReferentialConstraintDeleteRule DeleteRuleForPriceLevelItemEntityUsingPriceLevelId = ReferentialConstraintDeleteRule.Cascade;
        private const ReferentialConstraintDeleteRule DeleteRuleForPriceScheduleItemEntityUsingPriceLevelId = ReferentialConstraintDeleteRule.NoAction;
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		
		// private const ReferentialConstraintDeleteRule DeleteRuleForPriceLevelItemEntityUsingPriceLevelId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForPriceScheduleItemEntityUsingPriceLevelId = ReferentialConstraintDeleteRule


		/// <summary>Gets all relations and delete rules of the PriceLevelEntity as a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects.</summary>
		/// <returns>a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects</returns>
		public virtual Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> GetRelationsAndDeleteRules()
		{
			Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> toReturn = new Dictionary<IEntityRelation, ReferentialConstraintDeleteRule>();
			toReturn.Add(this.PriceLevelItemEntityUsingPriceLevelId, PriceLevelRelations.DeleteRuleForPriceLevelItemEntityUsingPriceLevelId);
			toReturn.Add(this.PriceScheduleItemEntityUsingPriceLevelId, PriceLevelRelations.DeleteRuleForPriceScheduleItemEntityUsingPriceLevelId);
			return toReturn;
		}		
	
	
		/// <summary>Returns a new IEntityRelation object, between PriceLevelEntity and PriceLevelItemEntity over the 1:n relation they have, using the relation between the fields: PriceLevel.PriceLevelId - PriceLevelItem.PriceLevelId</summary>
		public virtual IEntityRelation PriceLevelItemEntityUsingPriceLevelId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "PriceLevelItemCollection", true, new[] { PriceLevelFields.PriceLevelId, PriceLevelItemFields.PriceLevelId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between PriceLevelEntity and PriceScheduleItemEntity over the 1:n relation they have, using the relation between the fields: PriceLevel.PriceLevelId - PriceScheduleItem.PriceLevelId</summary>
		public virtual IEntityRelation PriceScheduleItemEntityUsingPriceLevelId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "PriceScheduleItemCollection", true, new[] { PriceLevelFields.PriceLevelId, PriceScheduleItemFields.PriceLevelId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between PriceLevelEntity and CompanyEntity over the m:1 relation they have, using the relation between the fields: PriceLevel.CompanyId - Company.CompanyId</summary>
		public virtual IEntityRelation CompanyEntityUsingCompanyId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Company", false, new[] { CompanyFields.CompanyId, PriceLevelFields.CompanyId }); }
		}

	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticPriceLevelRelations
	{
		internal static readonly IEntityRelation PriceLevelItemEntityUsingPriceLevelIdStatic = new PriceLevelRelations().PriceLevelItemEntityUsingPriceLevelId;
		internal static readonly IEntityRelation PriceScheduleItemEntityUsingPriceLevelIdStatic = new PriceLevelRelations().PriceScheduleItemEntityUsingPriceLevelId;
		internal static readonly IEntityRelation CompanyEntityUsingCompanyIdStatic = new PriceLevelRelations().CompanyEntityUsingCompanyId;

		/// <summary>CTor</summary>
		static StaticPriceLevelRelations() { }
	}
}

