﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 5.8
// Code is generated on: 
// Code is generated using templates: Crave.TemplateBindings
// Templates vendor: Crave Interactive
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.FactoryClasses;
using Crave.Ordering.Shared.Data.LLBLGen.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

// ReSharper disable All
namespace Crave.Ordering.Shared.Data.LLBLGen.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'OrderRoutestephandlerHistory'.<br/><br/></summary>
	[Serializable]
	public partial class OrderRoutestephandlerHistoryEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END
	
	{
		private ExternalSystemEntity _externalSystem;
		private OrderEntity _order;
		private SupportpoolEntity _supportpool;
		private TerminalEntity _terminal;
		private TerminalEntity _terminalEntity1;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		private static OrderRoutestephandlerHistoryEntityStaticMetaData _staticMetaData = new OrderRoutestephandlerHistoryEntityStaticMetaData();
		private static OrderRoutestephandlerHistoryRelations _relationsFactory = new OrderRoutestephandlerHistoryRelations();		

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name ExternalSystem</summary>
			public static readonly string ExternalSystem = "ExternalSystem";
			/// <summary>Member name Order</summary>
			public static readonly string Order = "Order";
			/// <summary>Member name Supportpool</summary>
			public static readonly string Supportpool = "Supportpool";
			/// <summary>Member name Terminal</summary>
			public static readonly string Terminal = "Terminal";
			/// <summary>Member name TerminalEntity1</summary>
			public static readonly string TerminalEntity1 = "TerminalEntity1";
		}

		/// <summary>Static meta-data storage for navigator related information</summary>
		protected class OrderRoutestephandlerHistoryEntityStaticMetaData : EntityStaticMetaDataBase
		{
			public OrderRoutestephandlerHistoryEntityStaticMetaData()
			{
				SetEntityCoreInfo("OrderRoutestephandlerHistoryEntity", InheritanceHierarchyType.None, false, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.OrderRoutestephandlerHistoryEntity, typeof(OrderRoutestephandlerHistoryEntity), typeof(OrderRoutestephandlerHistoryEntityFactory), false);
				AddNavigatorMetaData<OrderRoutestephandlerHistoryEntity, ExternalSystemEntity>("ExternalSystem", "OrderRoutestephandlerHistoryCollection", (a, b) => a._externalSystem = b, a => a._externalSystem, (a, b) => a.ExternalSystem = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticOrderRoutestephandlerHistoryRelations.ExternalSystemEntityUsingExternalSystemIdStatic, ()=>new OrderRoutestephandlerHistoryRelations().ExternalSystemEntityUsingExternalSystemId, null, new int[] { (int)OrderRoutestephandlerHistoryFieldIndex.ExternalSystemId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ExternalSystemEntity);
				AddNavigatorMetaData<OrderRoutestephandlerHistoryEntity, OrderEntity>("Order", "OrderRoutestephandlerHistoryCollection", (a, b) => a._order = b, a => a._order, (a, b) => a.Order = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticOrderRoutestephandlerHistoryRelations.OrderEntityUsingOrderIdStatic, ()=>new OrderRoutestephandlerHistoryRelations().OrderEntityUsingOrderId, null, new int[] { (int)OrderRoutestephandlerHistoryFieldIndex.OrderId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.OrderEntity);
				AddNavigatorMetaData<OrderRoutestephandlerHistoryEntity, SupportpoolEntity>("Supportpool", "OrderRoutestephandlerHistoryCollection", (a, b) => a._supportpool = b, a => a._supportpool, (a, b) => a.Supportpool = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticOrderRoutestephandlerHistoryRelations.SupportpoolEntityUsingSupportpoolIdStatic, ()=>new OrderRoutestephandlerHistoryRelations().SupportpoolEntityUsingSupportpoolId, null, new int[] { (int)OrderRoutestephandlerHistoryFieldIndex.SupportpoolId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.SupportpoolEntity);
				AddNavigatorMetaData<OrderRoutestephandlerHistoryEntity, TerminalEntity>("Terminal", "OrderRoutestephandlerHistoryCollection", (a, b) => a._terminal = b, a => a._terminal, (a, b) => a.Terminal = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticOrderRoutestephandlerHistoryRelations.TerminalEntityUsingTerminalIdStatic, ()=>new OrderRoutestephandlerHistoryRelations().TerminalEntityUsingTerminalId, null, new int[] { (int)OrderRoutestephandlerHistoryFieldIndex.TerminalId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.TerminalEntity);
				AddNavigatorMetaData<OrderRoutestephandlerHistoryEntity, TerminalEntity>("TerminalEntity1", "OrderRoutestephandlerHistoryCollection1", (a, b) => a._terminalEntity1 = b, a => a._terminalEntity1, (a, b) => a.TerminalEntity1 = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticOrderRoutestephandlerHistoryRelations.TerminalEntityUsingForwardedFromTerminalIdStatic, ()=>new OrderRoutestephandlerHistoryRelations().TerminalEntityUsingForwardedFromTerminalId, null, new int[] { (int)OrderRoutestephandlerHistoryFieldIndex.ForwardedFromTerminalId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.TerminalEntity);
			}
		}

		/// <summary>Static ctor</summary>
		static OrderRoutestephandlerHistoryEntity()
		{
		}

		/// <summary> CTor</summary>
		public OrderRoutestephandlerHistoryEntity()
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public OrderRoutestephandlerHistoryEntity(IEntityFields2 fields)
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this OrderRoutestephandlerHistoryEntity</param>
		public OrderRoutestephandlerHistoryEntity(IValidator validator)
		{
			InitClassEmpty(validator, null);
		}

		/// <summary> CTor</summary>
		/// <param name="orderRoutestephandlerHistoryId">PK value for OrderRoutestephandlerHistory which data should be fetched into this OrderRoutestephandlerHistory object</param>
		public OrderRoutestephandlerHistoryEntity(System.Int32 orderRoutestephandlerHistoryId) : this(orderRoutestephandlerHistoryId, null)
		{
		}

		/// <summary> CTor</summary>
		/// <param name="orderRoutestephandlerHistoryId">PK value for OrderRoutestephandlerHistory which data should be fetched into this OrderRoutestephandlerHistory object</param>
		/// <param name="validator">The custom validator object for this OrderRoutestephandlerHistoryEntity</param>
		public OrderRoutestephandlerHistoryEntity(System.Int32 orderRoutestephandlerHistoryId, IValidator validator)
		{
			InitClassEmpty(validator, null);
			this.OrderRoutestephandlerHistoryId = orderRoutestephandlerHistoryId;
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected OrderRoutestephandlerHistoryEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'ExternalSystem' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoExternalSystem() { return CreateRelationInfoForNavigator("ExternalSystem"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Order' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoOrder() { return CreateRelationInfoForNavigator("Order"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Supportpool' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoSupportpool() { return CreateRelationInfoForNavigator("Supportpool"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Terminal' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoTerminal() { return CreateRelationInfoForNavigator("Terminal"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Terminal' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoTerminalEntity1() { return CreateRelationInfoForNavigator("TerminalEntity1"); }
		
		/// <inheritdoc/>
		protected override EntityStaticMetaDataBase GetEntityStaticMetaData() {	return _staticMetaData; }

		/// <summary>Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		/// <summary>Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this OrderRoutestephandlerHistoryEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary>The relations object holding all relations of this entity with other entity classes.</summary>
		public static OrderRoutestephandlerHistoryRelations Relations { get { return _relationsFactory; } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'ExternalSystem' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathExternalSystemEntity { get { return _staticMetaData.GetPrefetchPathElement("ExternalSystem", CommonEntityBase.CreateEntityCollection<ExternalSystemEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Order' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathOrderEntity { get { return _staticMetaData.GetPrefetchPathElement("Order", CommonEntityBase.CreateEntityCollection<OrderEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Supportpool' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathSupportpoolEntity { get { return _staticMetaData.GetPrefetchPathElement("Supportpool", CommonEntityBase.CreateEntityCollection<SupportpoolEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Terminal' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathTerminalEntity { get { return _staticMetaData.GetPrefetchPathElement("Terminal", CommonEntityBase.CreateEntityCollection<TerminalEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Terminal' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathTerminalEntity1Entity { get { return _staticMetaData.GetPrefetchPathElement("TerminalEntity1", CommonEntityBase.CreateEntityCollection<TerminalEntity>()); } }

		/// <summary>The OrderRoutestephandlerHistoryId property of the Entity OrderRoutestephandlerHistory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandlerHistory"."OrderRoutestephandlerHistoryId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 OrderRoutestephandlerHistoryId
		{
			get { return (System.Int32)GetValue((int)OrderRoutestephandlerHistoryFieldIndex.OrderRoutestephandlerHistoryId, true); }
			set { SetValue((int)OrderRoutestephandlerHistoryFieldIndex.OrderRoutestephandlerHistoryId, value); }		}

		/// <summary>The ParentCompanyId property of the Entity OrderRoutestephandlerHistory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandlerHistory"."ParentCompanyId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ParentCompanyId
		{
			get { return (System.Int32)GetValue((int)OrderRoutestephandlerHistoryFieldIndex.ParentCompanyId, true); }
			set	{ SetValue((int)OrderRoutestephandlerHistoryFieldIndex.ParentCompanyId, value); }
		}

		/// <summary>The Guid property of the Entity OrderRoutestephandlerHistory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandlerHistory"."Guid".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 128.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Guid
		{
			get { return (System.String)GetValue((int)OrderRoutestephandlerHistoryFieldIndex.Guid, true); }
			set	{ SetValue((int)OrderRoutestephandlerHistoryFieldIndex.Guid, value); }
		}

		/// <summary>The OrderId property of the Entity OrderRoutestephandlerHistory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandlerHistory"."OrderId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 OrderId
		{
			get { return (System.Int32)GetValue((int)OrderRoutestephandlerHistoryFieldIndex.OrderId, true); }
			set	{ SetValue((int)OrderRoutestephandlerHistoryFieldIndex.OrderId, value); }
		}

		/// <summary>The TerminalId property of the Entity OrderRoutestephandlerHistory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandlerHistory"."TerminalId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> TerminalId
		{
			get { return (Nullable<System.Int32>)GetValue((int)OrderRoutestephandlerHistoryFieldIndex.TerminalId, false); }
			set	{ SetValue((int)OrderRoutestephandlerHistoryFieldIndex.TerminalId, value); }
		}

		/// <summary>The ForwardedFromTerminalId property of the Entity OrderRoutestephandlerHistory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandlerHistory"."ForwardedFromTerminalId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ForwardedFromTerminalId
		{
			get { return (Nullable<System.Int32>)GetValue((int)OrderRoutestephandlerHistoryFieldIndex.ForwardedFromTerminalId, false); }
			set	{ SetValue((int)OrderRoutestephandlerHistoryFieldIndex.ForwardedFromTerminalId, value); }
		}

		/// <summary>The Number property of the Entity OrderRoutestephandlerHistory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandlerHistory"."Number".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 Number
		{
			get { return (System.Int32)GetValue((int)OrderRoutestephandlerHistoryFieldIndex.Number, true); }
			set	{ SetValue((int)OrderRoutestephandlerHistoryFieldIndex.Number, value); }
		}

		/// <summary>The ContinueOnFailure property of the Entity OrderRoutestephandlerHistory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandlerHistory"."ContinueOnFailure".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean ContinueOnFailure
		{
			get { return (System.Boolean)GetValue((int)OrderRoutestephandlerHistoryFieldIndex.ContinueOnFailure, true); }
			set	{ SetValue((int)OrderRoutestephandlerHistoryFieldIndex.ContinueOnFailure, value); }
		}

		/// <summary>The CompleteRouteOnComplete property of the Entity OrderRoutestephandlerHistory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandlerHistory"."CompleteRouteOnComplete".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean CompleteRouteOnComplete
		{
			get { return (System.Boolean)GetValue((int)OrderRoutestephandlerHistoryFieldIndex.CompleteRouteOnComplete, true); }
			set	{ SetValue((int)OrderRoutestephandlerHistoryFieldIndex.CompleteRouteOnComplete, value); }
		}

		/// <summary>The HandlerType property of the Entity OrderRoutestephandlerHistory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandlerHistory"."HandlerType".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Crave.Enums.RoutestephandlerType HandlerType
		{
			get { return (Crave.Enums.RoutestephandlerType)GetValue((int)OrderRoutestephandlerHistoryFieldIndex.HandlerType, true); }
			set	{ SetValue((int)OrderRoutestephandlerHistoryFieldIndex.HandlerType, value); }
		}

		/// <summary>The HandlerTypeText property of the Entity OrderRoutestephandlerHistory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandlerHistory"."HandlerTypeText".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 100.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String HandlerTypeText
		{
			get { return (System.String)GetValue((int)OrderRoutestephandlerHistoryFieldIndex.HandlerTypeText, true); }
			set	{ SetValue((int)OrderRoutestephandlerHistoryFieldIndex.HandlerTypeText, value); }
		}

		/// <summary>The PrintReportType property of the Entity OrderRoutestephandlerHistory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandlerHistory"."PrintReportType".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<Crave.Enums.PrintReportType> PrintReportType
		{
			get { return (Nullable<Crave.Enums.PrintReportType>)GetValue((int)OrderRoutestephandlerHistoryFieldIndex.PrintReportType, false); }
			set	{ SetValue((int)OrderRoutestephandlerHistoryFieldIndex.PrintReportType, value); }
		}

		/// <summary>The EscalationRouteId property of the Entity OrderRoutestephandlerHistory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandlerHistory"."EscalationRouteId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> EscalationRouteId
		{
			get { return (Nullable<System.Int32>)GetValue((int)OrderRoutestephandlerHistoryFieldIndex.EscalationRouteId, false); }
			set	{ SetValue((int)OrderRoutestephandlerHistoryFieldIndex.EscalationRouteId, value); }
		}

		/// <summary>The Status property of the Entity OrderRoutestephandlerHistory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandlerHistory"."Status".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Crave.Enums.OrderRoutestephandlerStatus Status
		{
			get { return (Crave.Enums.OrderRoutestephandlerStatus)GetValue((int)OrderRoutestephandlerHistoryFieldIndex.Status, true); }
			set	{ SetValue((int)OrderRoutestephandlerHistoryFieldIndex.Status, value); }
		}

		/// <summary>The StatusText property of the Entity OrderRoutestephandlerHistory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandlerHistory"."StatusText".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 100.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String StatusText
		{
			get { return (System.String)GetValue((int)OrderRoutestephandlerHistoryFieldIndex.StatusText, true); }
			set	{ SetValue((int)OrderRoutestephandlerHistoryFieldIndex.StatusText, value); }
		}

		/// <summary>The Timeout property of the Entity OrderRoutestephandlerHistory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandlerHistory"."Timeout".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 Timeout
		{
			get { return (System.Int32)GetValue((int)OrderRoutestephandlerHistoryFieldIndex.Timeout, true); }
			set	{ SetValue((int)OrderRoutestephandlerHistoryFieldIndex.Timeout, value); }
		}

		/// <summary>The RetrievalSupportNotificationTimeoutMinutes property of the Entity OrderRoutestephandlerHistory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandlerHistory"."RetrievalSupportNotificationTimeoutMinutes".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 RetrievalSupportNotificationTimeoutMinutes
		{
			get { return (System.Int32)GetValue((int)OrderRoutestephandlerHistoryFieldIndex.RetrievalSupportNotificationTimeoutMinutes, true); }
			set	{ SetValue((int)OrderRoutestephandlerHistoryFieldIndex.RetrievalSupportNotificationTimeoutMinutes, value); }
		}

		/// <summary>The RetrievalSupportNotificationSent property of the Entity OrderRoutestephandlerHistory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandlerHistory"."RetrievalSupportNotificationSent".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean RetrievalSupportNotificationSent
		{
			get { return (System.Boolean)GetValue((int)OrderRoutestephandlerHistoryFieldIndex.RetrievalSupportNotificationSent, true); }
			set	{ SetValue((int)OrderRoutestephandlerHistoryFieldIndex.RetrievalSupportNotificationSent, value); }
		}

		/// <summary>The BeingHandledSupportNotificationTimeoutMinutes property of the Entity OrderRoutestephandlerHistory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandlerHistory"."BeingHandledSupportNotificationTimeoutMinutes".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 BeingHandledSupportNotificationTimeoutMinutes
		{
			get { return (System.Int32)GetValue((int)OrderRoutestephandlerHistoryFieldIndex.BeingHandledSupportNotificationTimeoutMinutes, true); }
			set	{ SetValue((int)OrderRoutestephandlerHistoryFieldIndex.BeingHandledSupportNotificationTimeoutMinutes, value); }
		}

		/// <summary>The BeingHandledSupportNotificationSent property of the Entity OrderRoutestephandlerHistory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandlerHistory"."BeingHandledSupportNotificationSent".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean BeingHandledSupportNotificationSent
		{
			get { return (System.Boolean)GetValue((int)OrderRoutestephandlerHistoryFieldIndex.BeingHandledSupportNotificationSent, true); }
			set	{ SetValue((int)OrderRoutestephandlerHistoryFieldIndex.BeingHandledSupportNotificationSent, value); }
		}

		/// <summary>The ErrorCode property of the Entity OrderRoutestephandlerHistory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandlerHistory"."ErrorCode".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Crave.Enums.OrderErrorCode ErrorCode
		{
			get { return (Crave.Enums.OrderErrorCode)GetValue((int)OrderRoutestephandlerHistoryFieldIndex.ErrorCode, true); }
			set	{ SetValue((int)OrderRoutestephandlerHistoryFieldIndex.ErrorCode, value); }
		}

		/// <summary>The ErrorText property of the Entity OrderRoutestephandlerHistory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandlerHistory"."ErrorText".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ErrorText
		{
			get { return (System.String)GetValue((int)OrderRoutestephandlerHistoryFieldIndex.ErrorText, true); }
			set	{ SetValue((int)OrderRoutestephandlerHistoryFieldIndex.ErrorText, value); }
		}

		/// <summary>The EscalationStep property of the Entity OrderRoutestephandlerHistory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandlerHistory"."EscalationStep".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean EscalationStep
		{
			get { return (System.Boolean)GetValue((int)OrderRoutestephandlerHistoryFieldIndex.EscalationStep, true); }
			set	{ SetValue((int)OrderRoutestephandlerHistoryFieldIndex.EscalationStep, value); }
		}

		/// <summary>The FieldValue1 property of the Entity OrderRoutestephandlerHistory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandlerHistory"."FieldValue1".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue1
		{
			get { return (System.String)GetValue((int)OrderRoutestephandlerHistoryFieldIndex.FieldValue1, true); }
			set	{ SetValue((int)OrderRoutestephandlerHistoryFieldIndex.FieldValue1, value); }
		}

		/// <summary>The FieldValue2 property of the Entity OrderRoutestephandlerHistory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandlerHistory"."FieldValue2".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue2
		{
			get { return (System.String)GetValue((int)OrderRoutestephandlerHistoryFieldIndex.FieldValue2, true); }
			set	{ SetValue((int)OrderRoutestephandlerHistoryFieldIndex.FieldValue2, value); }
		}

		/// <summary>The FieldValue3 property of the Entity OrderRoutestephandlerHistory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandlerHistory"."FieldValue3".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue3
		{
			get { return (System.String)GetValue((int)OrderRoutestephandlerHistoryFieldIndex.FieldValue3, true); }
			set	{ SetValue((int)OrderRoutestephandlerHistoryFieldIndex.FieldValue3, value); }
		}

		/// <summary>The FieldValue4 property of the Entity OrderRoutestephandlerHistory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandlerHistory"."FieldValue4".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue4
		{
			get { return (System.String)GetValue((int)OrderRoutestephandlerHistoryFieldIndex.FieldValue4, true); }
			set	{ SetValue((int)OrderRoutestephandlerHistoryFieldIndex.FieldValue4, value); }
		}

		/// <summary>The FieldValue5 property of the Entity OrderRoutestephandlerHistory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandlerHistory"."FieldValue5".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue5
		{
			get { return (System.String)GetValue((int)OrderRoutestephandlerHistoryFieldIndex.FieldValue5, true); }
			set	{ SetValue((int)OrderRoutestephandlerHistoryFieldIndex.FieldValue5, value); }
		}

		/// <summary>The FieldValue6 property of the Entity OrderRoutestephandlerHistory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandlerHistory"."FieldValue6".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue6
		{
			get { return (System.String)GetValue((int)OrderRoutestephandlerHistoryFieldIndex.FieldValue6, true); }
			set	{ SetValue((int)OrderRoutestephandlerHistoryFieldIndex.FieldValue6, value); }
		}

		/// <summary>The FieldValue7 property of the Entity OrderRoutestephandlerHistory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandlerHistory"."FieldValue7".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue7
		{
			get { return (System.String)GetValue((int)OrderRoutestephandlerHistoryFieldIndex.FieldValue7, true); }
			set	{ SetValue((int)OrderRoutestephandlerHistoryFieldIndex.FieldValue7, value); }
		}

		/// <summary>The FieldValue8 property of the Entity OrderRoutestephandlerHistory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandlerHistory"."FieldValue8".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue8
		{
			get { return (System.String)GetValue((int)OrderRoutestephandlerHistoryFieldIndex.FieldValue8, true); }
			set	{ SetValue((int)OrderRoutestephandlerHistoryFieldIndex.FieldValue8, value); }
		}

		/// <summary>The FieldValue9 property of the Entity OrderRoutestephandlerHistory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandlerHistory"."FieldValue9".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue9
		{
			get { return (System.String)GetValue((int)OrderRoutestephandlerHistoryFieldIndex.FieldValue9, true); }
			set	{ SetValue((int)OrderRoutestephandlerHistoryFieldIndex.FieldValue9, value); }
		}

		/// <summary>The FieldValue10 property of the Entity OrderRoutestephandlerHistory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandlerHistory"."FieldValue10".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue10
		{
			get { return (System.String)GetValue((int)OrderRoutestephandlerHistoryFieldIndex.FieldValue10, true); }
			set	{ SetValue((int)OrderRoutestephandlerHistoryFieldIndex.FieldValue10, value); }
		}

		/// <summary>The LogAlways property of the Entity OrderRoutestephandlerHistory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandlerHistory"."LogAlways".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean LogAlways
		{
			get { return (System.Boolean)GetValue((int)OrderRoutestephandlerHistoryFieldIndex.LogAlways, true); }
			set	{ SetValue((int)OrderRoutestephandlerHistoryFieldIndex.LogAlways, value); }
		}

		/// <summary>The SupportpoolId property of the Entity OrderRoutestephandlerHistory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandlerHistory"."SupportpoolId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> SupportpoolId
		{
			get { return (Nullable<System.Int32>)GetValue((int)OrderRoutestephandlerHistoryFieldIndex.SupportpoolId, false); }
			set	{ SetValue((int)OrderRoutestephandlerHistoryFieldIndex.SupportpoolId, value); }
		}

		/// <summary>The OriginatedFromRoutestepHandlerId property of the Entity OrderRoutestephandlerHistory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandlerHistory"."OriginatedFromRoutestepHandlerId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> OriginatedFromRoutestepHandlerId
		{
			get { return (Nullable<System.Int32>)GetValue((int)OrderRoutestephandlerHistoryFieldIndex.OriginatedFromRoutestepHandlerId, false); }
			set	{ SetValue((int)OrderRoutestephandlerHistoryFieldIndex.OriginatedFromRoutestepHandlerId, value); }
		}

		/// <summary>The CreatedBy property of the Entity OrderRoutestephandlerHistory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandlerHistory"."CreatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)OrderRoutestephandlerHistoryFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)OrderRoutestephandlerHistoryFieldIndex.CreatedBy, value); }
		}

		/// <summary>The UpdatedBy property of the Entity OrderRoutestephandlerHistory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandlerHistory"."UpdatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)OrderRoutestephandlerHistoryFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)OrderRoutestephandlerHistoryFieldIndex.UpdatedBy, value); }
		}

		/// <summary>The CreatedUTC property of the Entity OrderRoutestephandlerHistory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandlerHistory"."CreatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)OrderRoutestephandlerHistoryFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)OrderRoutestephandlerHistoryFieldIndex.CreatedUTC, value); }
		}

		/// <summary>The UpdatedUTC property of the Entity OrderRoutestephandlerHistory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandlerHistory"."UpdatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)OrderRoutestephandlerHistoryFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)OrderRoutestephandlerHistoryFieldIndex.UpdatedUTC, value); }
		}

		/// <summary>The TimeoutExpiresUTC property of the Entity OrderRoutestephandlerHistory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandlerHistory"."TimeoutExpiresUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> TimeoutExpiresUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)OrderRoutestephandlerHistoryFieldIndex.TimeoutExpiresUTC, false); }
			set	{ SetValue((int)OrderRoutestephandlerHistoryFieldIndex.TimeoutExpiresUTC, value); }
		}

		/// <summary>The RetrievalSupportNotificationTimeoutUTC property of the Entity OrderRoutestephandlerHistory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandlerHistory"."RetrievalSupportNotificationTimeoutUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> RetrievalSupportNotificationTimeoutUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)OrderRoutestephandlerHistoryFieldIndex.RetrievalSupportNotificationTimeoutUTC, false); }
			set	{ SetValue((int)OrderRoutestephandlerHistoryFieldIndex.RetrievalSupportNotificationTimeoutUTC, value); }
		}

		/// <summary>The BeingHandledSupportNotificationTimeoutUTC property of the Entity OrderRoutestephandlerHistory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandlerHistory"."BeingHandledSupportNotificationTimeoutUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> BeingHandledSupportNotificationTimeoutUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)OrderRoutestephandlerHistoryFieldIndex.BeingHandledSupportNotificationTimeoutUTC, false); }
			set	{ SetValue((int)OrderRoutestephandlerHistoryFieldIndex.BeingHandledSupportNotificationTimeoutUTC, value); }
		}

		/// <summary>The WaitingToBeRetrievedUTC property of the Entity OrderRoutestephandlerHistory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandlerHistory"."WaitingToBeRetrievedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> WaitingToBeRetrievedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)OrderRoutestephandlerHistoryFieldIndex.WaitingToBeRetrievedUTC, false); }
			set	{ SetValue((int)OrderRoutestephandlerHistoryFieldIndex.WaitingToBeRetrievedUTC, value); }
		}

		/// <summary>The RetrievedByHandlerUTC property of the Entity OrderRoutestephandlerHistory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandlerHistory"."RetrievedByHandlerUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> RetrievedByHandlerUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)OrderRoutestephandlerHistoryFieldIndex.RetrievedByHandlerUTC, false); }
			set	{ SetValue((int)OrderRoutestephandlerHistoryFieldIndex.RetrievedByHandlerUTC, value); }
		}

		/// <summary>The BeingHandledUTC property of the Entity OrderRoutestephandlerHistory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandlerHistory"."BeingHandledUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> BeingHandledUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)OrderRoutestephandlerHistoryFieldIndex.BeingHandledUTC, false); }
			set	{ SetValue((int)OrderRoutestephandlerHistoryFieldIndex.BeingHandledUTC, value); }
		}

		/// <summary>The CompletedUTC property of the Entity OrderRoutestephandlerHistory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandlerHistory"."CompletedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CompletedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)OrderRoutestephandlerHistoryFieldIndex.CompletedUTC, false); }
			set	{ SetValue((int)OrderRoutestephandlerHistoryFieldIndex.CompletedUTC, value); }
		}

		/// <summary>The ExternalSystemId property of the Entity OrderRoutestephandlerHistory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandlerHistory"."ExternalSystemId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ExternalSystemId
		{
			get { return (Nullable<System.Int32>)GetValue((int)OrderRoutestephandlerHistoryFieldIndex.ExternalSystemId, false); }
			set	{ SetValue((int)OrderRoutestephandlerHistoryFieldIndex.ExternalSystemId, value); }
		}

		/// <summary>Gets / sets related entity of type 'ExternalSystemEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual ExternalSystemEntity ExternalSystem
		{
			get { return _externalSystem; }
			set { SetSingleRelatedEntityNavigator(value, "ExternalSystem"); }
		}

		/// <summary>Gets / sets related entity of type 'OrderEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual OrderEntity Order
		{
			get { return _order; }
			set { SetSingleRelatedEntityNavigator(value, "Order"); }
		}

		/// <summary>Gets / sets related entity of type 'SupportpoolEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual SupportpoolEntity Supportpool
		{
			get { return _supportpool; }
			set { SetSingleRelatedEntityNavigator(value, "Supportpool"); }
		}

		/// <summary>Gets / sets related entity of type 'TerminalEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual TerminalEntity Terminal
		{
			get { return _terminal; }
			set { SetSingleRelatedEntityNavigator(value, "Terminal"); }
		}

		/// <summary>Gets / sets related entity of type 'TerminalEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual TerminalEntity TerminalEntity1
		{
			get { return _terminalEntity1; }
			set { SetSingleRelatedEntityNavigator(value, "TerminalEntity1"); }
		}


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen
{
	public enum OrderRoutestephandlerHistoryFieldIndex
	{
		///<summary>OrderRoutestephandlerHistoryId. </summary>
		OrderRoutestephandlerHistoryId,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>Guid. </summary>
		Guid,
		///<summary>OrderId. </summary>
		OrderId,
		///<summary>TerminalId. </summary>
		TerminalId,
		///<summary>ForwardedFromTerminalId. </summary>
		ForwardedFromTerminalId,
		///<summary>Number. </summary>
		Number,
		///<summary>ContinueOnFailure. </summary>
		ContinueOnFailure,
		///<summary>CompleteRouteOnComplete. </summary>
		CompleteRouteOnComplete,
		///<summary>HandlerType. </summary>
		HandlerType,
		///<summary>HandlerTypeText. </summary>
		HandlerTypeText,
		///<summary>PrintReportType. </summary>
		PrintReportType,
		///<summary>EscalationRouteId. </summary>
		EscalationRouteId,
		///<summary>Status. </summary>
		Status,
		///<summary>StatusText. </summary>
		StatusText,
		///<summary>Timeout. </summary>
		Timeout,
		///<summary>RetrievalSupportNotificationTimeoutMinutes. </summary>
		RetrievalSupportNotificationTimeoutMinutes,
		///<summary>RetrievalSupportNotificationSent. </summary>
		RetrievalSupportNotificationSent,
		///<summary>BeingHandledSupportNotificationTimeoutMinutes. </summary>
		BeingHandledSupportNotificationTimeoutMinutes,
		///<summary>BeingHandledSupportNotificationSent. </summary>
		BeingHandledSupportNotificationSent,
		///<summary>ErrorCode. </summary>
		ErrorCode,
		///<summary>ErrorText. </summary>
		ErrorText,
		///<summary>EscalationStep. </summary>
		EscalationStep,
		///<summary>FieldValue1. </summary>
		FieldValue1,
		///<summary>FieldValue2. </summary>
		FieldValue2,
		///<summary>FieldValue3. </summary>
		FieldValue3,
		///<summary>FieldValue4. </summary>
		FieldValue4,
		///<summary>FieldValue5. </summary>
		FieldValue5,
		///<summary>FieldValue6. </summary>
		FieldValue6,
		///<summary>FieldValue7. </summary>
		FieldValue7,
		///<summary>FieldValue8. </summary>
		FieldValue8,
		///<summary>FieldValue9. </summary>
		FieldValue9,
		///<summary>FieldValue10. </summary>
		FieldValue10,
		///<summary>LogAlways. </summary>
		LogAlways,
		///<summary>SupportpoolId. </summary>
		SupportpoolId,
		///<summary>OriginatedFromRoutestepHandlerId. </summary>
		OriginatedFromRoutestepHandlerId,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>TimeoutExpiresUTC. </summary>
		TimeoutExpiresUTC,
		///<summary>RetrievalSupportNotificationTimeoutUTC. </summary>
		RetrievalSupportNotificationTimeoutUTC,
		///<summary>BeingHandledSupportNotificationTimeoutUTC. </summary>
		BeingHandledSupportNotificationTimeoutUTC,
		///<summary>WaitingToBeRetrievedUTC. </summary>
		WaitingToBeRetrievedUTC,
		///<summary>RetrievedByHandlerUTC. </summary>
		RetrievedByHandlerUTC,
		///<summary>BeingHandledUTC. </summary>
		BeingHandledUTC,
		///<summary>CompletedUTC. </summary>
		CompletedUTC,
		///<summary>ExternalSystemId. </summary>
		ExternalSystemId,
		/// <summary></summary>
		AmountOfFields
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: OrderRoutestephandlerHistory. </summary>
	public partial class OrderRoutestephandlerHistoryRelations: RelationFactory
	{
		#region Custom Relation code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomRelationCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		


		/// <summary>Gets all relations and delete rules of the OrderRoutestephandlerHistoryEntity as a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects.</summary>
		/// <returns>a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects</returns>
		public virtual Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> GetRelationsAndDeleteRules()
		{
			Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> toReturn = new Dictionary<IEntityRelation, ReferentialConstraintDeleteRule>();
			return toReturn;
		}		
	
	

		/// <summary>Returns a new IEntityRelation object, between OrderRoutestephandlerHistoryEntity and ExternalSystemEntity over the m:1 relation they have, using the relation between the fields: OrderRoutestephandlerHistory.ExternalSystemId - ExternalSystem.ExternalSystemId</summary>
		public virtual IEntityRelation ExternalSystemEntityUsingExternalSystemId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "ExternalSystem", false, new[] { ExternalSystemFields.ExternalSystemId, OrderRoutestephandlerHistoryFields.ExternalSystemId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between OrderRoutestephandlerHistoryEntity and OrderEntity over the m:1 relation they have, using the relation between the fields: OrderRoutestephandlerHistory.OrderId - Order.OrderId</summary>
		public virtual IEntityRelation OrderEntityUsingOrderId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Order", false, new[] { OrderFields.OrderId, OrderRoutestephandlerHistoryFields.OrderId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between OrderRoutestephandlerHistoryEntity and SupportpoolEntity over the m:1 relation they have, using the relation between the fields: OrderRoutestephandlerHistory.SupportpoolId - Supportpool.SupportpoolId</summary>
		public virtual IEntityRelation SupportpoolEntityUsingSupportpoolId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Supportpool", false, new[] { SupportpoolFields.SupportpoolId, OrderRoutestephandlerHistoryFields.SupportpoolId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between OrderRoutestephandlerHistoryEntity and TerminalEntity over the m:1 relation they have, using the relation between the fields: OrderRoutestephandlerHistory.TerminalId - Terminal.TerminalId</summary>
		public virtual IEntityRelation TerminalEntityUsingTerminalId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Terminal", false, new[] { TerminalFields.TerminalId, OrderRoutestephandlerHistoryFields.TerminalId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between OrderRoutestephandlerHistoryEntity and TerminalEntity over the m:1 relation they have, using the relation between the fields: OrderRoutestephandlerHistory.ForwardedFromTerminalId - Terminal.TerminalId</summary>
		public virtual IEntityRelation TerminalEntityUsingForwardedFromTerminalId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "TerminalEntity1", false, new[] { TerminalFields.TerminalId, OrderRoutestephandlerHistoryFields.ForwardedFromTerminalId }); }
		}

	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticOrderRoutestephandlerHistoryRelations
	{
		internal static readonly IEntityRelation ExternalSystemEntityUsingExternalSystemIdStatic = new OrderRoutestephandlerHistoryRelations().ExternalSystemEntityUsingExternalSystemId;
		internal static readonly IEntityRelation OrderEntityUsingOrderIdStatic = new OrderRoutestephandlerHistoryRelations().OrderEntityUsingOrderId;
		internal static readonly IEntityRelation SupportpoolEntityUsingSupportpoolIdStatic = new OrderRoutestephandlerHistoryRelations().SupportpoolEntityUsingSupportpoolId;
		internal static readonly IEntityRelation TerminalEntityUsingTerminalIdStatic = new OrderRoutestephandlerHistoryRelations().TerminalEntityUsingTerminalId;
		internal static readonly IEntityRelation TerminalEntityUsingForwardedFromTerminalIdStatic = new OrderRoutestephandlerHistoryRelations().TerminalEntityUsingForwardedFromTerminalId;

		/// <summary>CTor</summary>
		static StaticOrderRoutestephandlerHistoryRelations() { }
	}
}

