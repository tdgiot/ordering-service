﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 5.8
// Code is generated on: 
// Code is generated using templates: Crave.TemplateBindings
// Templates vendor: Crave Interactive
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.FactoryClasses;
using Crave.Ordering.Shared.Data.LLBLGen.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

// ReSharper disable All
namespace Crave.Ordering.Shared.Data.LLBLGen.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'Outlet'.<br/><br/></summary>
	[Serializable]
	public partial class OutletEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END
	
	{
		private EntityCollection<BusinesshourEntity> _businesshourCollection;
		private EntityCollection<CheckoutMethodEntity> _checkoutMethodCollection;
		private EntityCollection<OptInEntity> _optInCollection;
		private EntityCollection<OrderEntity> _orderCollection;
		private EntityCollection<ServiceMethodEntity> _serviceMethodCollection;
		private CompanyEntity _company;
		private OutletOperationalStateEntity _outletOperationalState;
		private OutletSellerInformationEntity _outletSellerInformation;
		private ProductEntity _deliveryChargeProduct;
		private ProductEntity _serviceChargeProduct;
		private ProductEntity _tippingProduct;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		private static OutletEntityStaticMetaData _staticMetaData = new OutletEntityStaticMetaData();
		private static OutletRelations _relationsFactory = new OutletRelations();		

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Company</summary>
			public static readonly string Company = "Company";
			/// <summary>Member name OutletOperationalState</summary>
			public static readonly string OutletOperationalState = "OutletOperationalState";
			/// <summary>Member name OutletSellerInformation</summary>
			public static readonly string OutletSellerInformation = "OutletSellerInformation";
			/// <summary>Member name DeliveryChargeProduct</summary>
			public static readonly string DeliveryChargeProduct = "DeliveryChargeProduct";
			/// <summary>Member name ServiceChargeProduct</summary>
			public static readonly string ServiceChargeProduct = "ServiceChargeProduct";
			/// <summary>Member name TippingProduct</summary>
			public static readonly string TippingProduct = "TippingProduct";
			/// <summary>Member name BusinesshourCollection</summary>
			public static readonly string BusinesshourCollection = "BusinesshourCollection";
			/// <summary>Member name CheckoutMethodCollection</summary>
			public static readonly string CheckoutMethodCollection = "CheckoutMethodCollection";
			/// <summary>Member name OptInCollection</summary>
			public static readonly string OptInCollection = "OptInCollection";
			/// <summary>Member name OrderCollection</summary>
			public static readonly string OrderCollection = "OrderCollection";
			/// <summary>Member name ServiceMethodCollection</summary>
			public static readonly string ServiceMethodCollection = "ServiceMethodCollection";
		}

		/// <summary>Static meta-data storage for navigator related information</summary>
		protected class OutletEntityStaticMetaData : EntityStaticMetaDataBase
		{
			public OutletEntityStaticMetaData()
			{
				SetEntityCoreInfo("OutletEntity", InheritanceHierarchyType.None, false, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.OutletEntity, typeof(OutletEntity), typeof(OutletEntityFactory), false);
				AddNavigatorMetaData<OutletEntity, EntityCollection<BusinesshourEntity>>("BusinesshourCollection", a => a._businesshourCollection, (a, b) => a._businesshourCollection = b, a => a.BusinesshourCollection, () => new OutletRelations().BusinesshourEntityUsingOutletId, typeof(BusinesshourEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.BusinesshourEntity);
				AddNavigatorMetaData<OutletEntity, EntityCollection<CheckoutMethodEntity>>("CheckoutMethodCollection", a => a._checkoutMethodCollection, (a, b) => a._checkoutMethodCollection = b, a => a.CheckoutMethodCollection, () => new OutletRelations().CheckoutMethodEntityUsingOutletId, typeof(CheckoutMethodEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.CheckoutMethodEntity);
				AddNavigatorMetaData<OutletEntity, EntityCollection<OptInEntity>>("OptInCollection", a => a._optInCollection, (a, b) => a._optInCollection = b, a => a.OptInCollection, () => new OutletRelations().OptInEntityUsingOutletId, typeof(OptInEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.OptInEntity);
				AddNavigatorMetaData<OutletEntity, EntityCollection<OrderEntity>>("OrderCollection", a => a._orderCollection, (a, b) => a._orderCollection = b, a => a.OrderCollection, () => new OutletRelations().OrderEntityUsingOutletId, typeof(OrderEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.OrderEntity);
				AddNavigatorMetaData<OutletEntity, EntityCollection<ServiceMethodEntity>>("ServiceMethodCollection", a => a._serviceMethodCollection, (a, b) => a._serviceMethodCollection = b, a => a.ServiceMethodCollection, () => new OutletRelations().ServiceMethodEntityUsingOutletId, typeof(ServiceMethodEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ServiceMethodEntity);
				AddNavigatorMetaData<OutletEntity, CompanyEntity>("Company", "OutletCollection", (a, b) => a._company = b, a => a._company, (a, b) => a.Company = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticOutletRelations.CompanyEntityUsingCompanyIdStatic, ()=>new OutletRelations().CompanyEntityUsingCompanyId, null, new int[] { (int)OutletFieldIndex.CompanyId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.CompanyEntity);
				AddNavigatorMetaData<OutletEntity, OutletOperationalStateEntity>("OutletOperationalState", "OutletCollection", (a, b) => a._outletOperationalState = b, a => a._outletOperationalState, (a, b) => a.OutletOperationalState = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticOutletRelations.OutletOperationalStateEntityUsingOutletOperationalStateIdStatic, ()=>new OutletRelations().OutletOperationalStateEntityUsingOutletOperationalStateId, null, new int[] { (int)OutletFieldIndex.OutletOperationalStateId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.OutletOperationalStateEntity);
				AddNavigatorMetaData<OutletEntity, OutletSellerInformationEntity>("OutletSellerInformation", "OutletCollection", (a, b) => a._outletSellerInformation = b, a => a._outletSellerInformation, (a, b) => a.OutletSellerInformation = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticOutletRelations.OutletSellerInformationEntityUsingOutletSellerInformationIdStatic, ()=>new OutletRelations().OutletSellerInformationEntityUsingOutletSellerInformationId, null, new int[] { (int)OutletFieldIndex.OutletSellerInformationId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.OutletSellerInformationEntity);
				AddNavigatorMetaData<OutletEntity, ProductEntity>("DeliveryChargeProduct", "OutletCollection__", (a, b) => a._deliveryChargeProduct = b, a => a._deliveryChargeProduct, (a, b) => a.DeliveryChargeProduct = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticOutletRelations.ProductEntityUsingDeliveryChargeProductIdStatic, ()=>new OutletRelations().ProductEntityUsingDeliveryChargeProductId, null, new int[] { (int)OutletFieldIndex.DeliveryChargeProductId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ProductEntity);
				AddNavigatorMetaData<OutletEntity, ProductEntity>("ServiceChargeProduct", "OutletCollection_", (a, b) => a._serviceChargeProduct = b, a => a._serviceChargeProduct, (a, b) => a.ServiceChargeProduct = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticOutletRelations.ProductEntityUsingServiceChargeProductIdStatic, ()=>new OutletRelations().ProductEntityUsingServiceChargeProductId, null, new int[] { (int)OutletFieldIndex.ServiceChargeProductId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ProductEntity);
				AddNavigatorMetaData<OutletEntity, ProductEntity>("TippingProduct", "OutletCollection", (a, b) => a._tippingProduct = b, a => a._tippingProduct, (a, b) => a.TippingProduct = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticOutletRelations.ProductEntityUsingTippingProductIdStatic, ()=>new OutletRelations().ProductEntityUsingTippingProductId, null, new int[] { (int)OutletFieldIndex.TippingProductId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ProductEntity);
			}
		}

		/// <summary>Static ctor</summary>
		static OutletEntity()
		{
		}

		/// <summary> CTor</summary>
		public OutletEntity()
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public OutletEntity(IEntityFields2 fields)
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this OutletEntity</param>
		public OutletEntity(IValidator validator)
		{
			InitClassEmpty(validator, null);
		}

		/// <summary> CTor</summary>
		/// <param name="outletId">PK value for Outlet which data should be fetched into this Outlet object</param>
		public OutletEntity(System.Int32 outletId) : this(outletId, null)
		{
		}

		/// <summary> CTor</summary>
		/// <param name="outletId">PK value for Outlet which data should be fetched into this Outlet object</param>
		/// <param name="validator">The custom validator object for this OutletEntity</param>
		public OutletEntity(System.Int32 outletId, IValidator validator)
		{
			InitClassEmpty(validator, null);
			this.OutletId = outletId;
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected OutletEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Businesshour' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoBusinesshourCollection() { return CreateRelationInfoForNavigator("BusinesshourCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'CheckoutMethod' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoCheckoutMethodCollection() { return CreateRelationInfoForNavigator("CheckoutMethodCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'OptIn' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoOptInCollection() { return CreateRelationInfoForNavigator("OptInCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Order' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoOrderCollection() { return CreateRelationInfoForNavigator("OrderCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'ServiceMethod' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoServiceMethodCollection() { return CreateRelationInfoForNavigator("ServiceMethodCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Company' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoCompany() { return CreateRelationInfoForNavigator("Company"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'OutletOperationalState' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoOutletOperationalState() { return CreateRelationInfoForNavigator("OutletOperationalState"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'OutletSellerInformation' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoOutletSellerInformation() { return CreateRelationInfoForNavigator("OutletSellerInformation"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Product' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoDeliveryChargeProduct() { return CreateRelationInfoForNavigator("DeliveryChargeProduct"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Product' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoServiceChargeProduct() { return CreateRelationInfoForNavigator("ServiceChargeProduct"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Product' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoTippingProduct() { return CreateRelationInfoForNavigator("TippingProduct"); }
		
		/// <inheritdoc/>
		protected override EntityStaticMetaDataBase GetEntityStaticMetaData() {	return _staticMetaData; }

		/// <summary>Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		/// <summary>Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this OutletEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary>The relations object holding all relations of this entity with other entity classes.</summary>
		public static OutletRelations Relations { get { return _relationsFactory; } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Businesshour' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathBusinesshourCollection { get { return _staticMetaData.GetPrefetchPathElement("BusinesshourCollection", CommonEntityBase.CreateEntityCollection<BusinesshourEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'CheckoutMethod' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathCheckoutMethodCollection { get { return _staticMetaData.GetPrefetchPathElement("CheckoutMethodCollection", CommonEntityBase.CreateEntityCollection<CheckoutMethodEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'OptIn' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathOptInCollection { get { return _staticMetaData.GetPrefetchPathElement("OptInCollection", CommonEntityBase.CreateEntityCollection<OptInEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Order' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathOrderCollection { get { return _staticMetaData.GetPrefetchPathElement("OrderCollection", CommonEntityBase.CreateEntityCollection<OrderEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'ServiceMethod' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathServiceMethodCollection { get { return _staticMetaData.GetPrefetchPathElement("ServiceMethodCollection", CommonEntityBase.CreateEntityCollection<ServiceMethodEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Company' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathCompanyEntity { get { return _staticMetaData.GetPrefetchPathElement("Company", CommonEntityBase.CreateEntityCollection<CompanyEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'OutletOperationalState' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathOutletOperationalStateEntity { get { return _staticMetaData.GetPrefetchPathElement("OutletOperationalState", CommonEntityBase.CreateEntityCollection<OutletOperationalStateEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'OutletSellerInformation' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathOutletSellerInformationEntity { get { return _staticMetaData.GetPrefetchPathElement("OutletSellerInformation", CommonEntityBase.CreateEntityCollection<OutletSellerInformationEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Product' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathDeliveryChargeProductEntity { get { return _staticMetaData.GetPrefetchPathElement("DeliveryChargeProduct", CommonEntityBase.CreateEntityCollection<ProductEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Product' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathServiceChargeProductEntity { get { return _staticMetaData.GetPrefetchPathElement("ServiceChargeProduct", CommonEntityBase.CreateEntityCollection<ProductEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Product' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathTippingProductEntity { get { return _staticMetaData.GetPrefetchPathElement("TippingProduct", CommonEntityBase.CreateEntityCollection<ProductEntity>()); } }

		/// <summary>The OutletId property of the Entity Outlet<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Outlet"."OutletId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 OutletId
		{
			get { return (System.Int32)GetValue((int)OutletFieldIndex.OutletId, true); }
			set { SetValue((int)OutletFieldIndex.OutletId, value); }		}

		/// <summary>The CompanyId property of the Entity Outlet<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Outlet"."CompanyId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CompanyId
		{
			get { return (System.Int32)GetValue((int)OutletFieldIndex.CompanyId, true); }
			set	{ SetValue((int)OutletFieldIndex.CompanyId, value); }
		}

		/// <summary>The Name property of the Entity Outlet<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Outlet"."Name".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 256.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)OutletFieldIndex.Name, true); }
			set	{ SetValue((int)OutletFieldIndex.Name, value); }
		}

		/// <summary>The TippingActive property of the Entity Outlet<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Outlet"."TippingActive".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean TippingActive
		{
			get { return (System.Boolean)GetValue((int)OutletFieldIndex.TippingActive, true); }
			set	{ SetValue((int)OutletFieldIndex.TippingActive, value); }
		}

		/// <summary>The TippingDefaultPercentage property of the Entity Outlet<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Outlet"."TippingDefaultPercentage".<br/>Table field type characteristics (type, precision, scale, length): Float, 38, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Double> TippingDefaultPercentage
		{
			get { return (Nullable<System.Double>)GetValue((int)OutletFieldIndex.TippingDefaultPercentage, false); }
			set	{ SetValue((int)OutletFieldIndex.TippingDefaultPercentage, value); }
		}

		/// <summary>The TippingMinimumPercentage property of the Entity Outlet<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Outlet"."TippingMinimumPercentage".<br/>Table field type characteristics (type, precision, scale, length): Float, 38, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Double> TippingMinimumPercentage
		{
			get { return (Nullable<System.Double>)GetValue((int)OutletFieldIndex.TippingMinimumPercentage, false); }
			set	{ SetValue((int)OutletFieldIndex.TippingMinimumPercentage, value); }
		}

		/// <summary>The TippingStepSize property of the Entity Outlet<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Outlet"."TippingStepSize".<br/>Table field type characteristics (type, precision, scale, length): Float, 38, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Double> TippingStepSize
		{
			get { return (Nullable<System.Double>)GetValue((int)OutletFieldIndex.TippingStepSize, false); }
			set	{ SetValue((int)OutletFieldIndex.TippingStepSize, value); }
		}

		/// <summary>The TippingProductId property of the Entity Outlet<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Outlet"."TippingProductId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> TippingProductId
		{
			get { return (Nullable<System.Int32>)GetValue((int)OutletFieldIndex.TippingProductId, false); }
			set	{ SetValue((int)OutletFieldIndex.TippingProductId, value); }
		}

		/// <summary>The CustomerDetailsSectionActive property of the Entity Outlet<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Outlet"."CustomerDetailsSectionActive".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean CustomerDetailsSectionActive
		{
			get { return (System.Boolean)GetValue((int)OutletFieldIndex.CustomerDetailsSectionActive, true); }
			set	{ SetValue((int)OutletFieldIndex.CustomerDetailsSectionActive, value); }
		}

		/// <summary>The CustomerDetailsPhoneRequired property of the Entity Outlet<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Outlet"."CustomerDetailsPhoneRequired".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean CustomerDetailsPhoneRequired
		{
			get { return (System.Boolean)GetValue((int)OutletFieldIndex.CustomerDetailsPhoneRequired, true); }
			set	{ SetValue((int)OutletFieldIndex.CustomerDetailsPhoneRequired, value); }
		}

		/// <summary>The CustomerDetailsEmailRequired property of the Entity Outlet<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Outlet"."CustomerDetailsEmailRequired".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean CustomerDetailsEmailRequired
		{
			get { return (System.Boolean)GetValue((int)OutletFieldIndex.CustomerDetailsEmailRequired, true); }
			set	{ SetValue((int)OutletFieldIndex.CustomerDetailsEmailRequired, value); }
		}

		/// <summary>The ShowPricesIncludingTax property of the Entity Outlet<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Outlet"."ShowPricesIncludingTax".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean ShowPricesIncludingTax
		{
			get { return (System.Boolean)GetValue((int)OutletFieldIndex.ShowPricesIncludingTax, true); }
			set	{ SetValue((int)OutletFieldIndex.ShowPricesIncludingTax, value); }
		}

		/// <summary>The ShowTaxBreakDownOnBasket property of the Entity Outlet<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Outlet"."ShowTaxBreakDownOnBasket".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean ShowTaxBreakDownOnBasket
		{
			get { return (System.Boolean)GetValue((int)OutletFieldIndex.ShowTaxBreakDownOnBasket, true); }
			set	{ SetValue((int)OutletFieldIndex.ShowTaxBreakDownOnBasket, value); }
		}

		/// <summary>The ShowTaxBreakDownOnCheckout property of the Entity Outlet<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Outlet"."ShowTaxBreakDownOnCheckout".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean ShowTaxBreakDownOnCheckout
		{
			get { return (System.Boolean)GetValue((int)OutletFieldIndex.ShowTaxBreakDownOnCheckout, true); }
			set	{ SetValue((int)OutletFieldIndex.ShowTaxBreakDownOnCheckout, value); }
		}

		/// <summary>The ReceiptEmail property of the Entity Outlet<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Outlet"."ReceiptEmail".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2000.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ReceiptEmail
		{
			get { return (System.String)GetValue((int)OutletFieldIndex.ReceiptEmail, true); }
			set	{ SetValue((int)OutletFieldIndex.ReceiptEmail, value); }
		}

		/// <summary>The OutletOperationalStateId property of the Entity Outlet<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Outlet"."OutletOperationalStateId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> OutletOperationalStateId
		{
			get { return (Nullable<System.Int32>)GetValue((int)OutletFieldIndex.OutletOperationalStateId, false); }
			set	{ SetValue((int)OutletFieldIndex.OutletOperationalStateId, value); }
		}

		/// <summary>The CreatedBy property of the Entity Outlet<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Outlet"."CreatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)OutletFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)OutletFieldIndex.CreatedBy, value); }
		}

		/// <summary>The UpdatedBy property of the Entity Outlet<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Outlet"."UpdatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)OutletFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)OutletFieldIndex.UpdatedBy, value); }
		}

		/// <summary>The CreatedUTC property of the Entity Outlet<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Outlet"."CreatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)OutletFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)OutletFieldIndex.CreatedUTC, value); }
		}

		/// <summary>The UpdatedUTC property of the Entity Outlet<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Outlet"."UpdatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)OutletFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)OutletFieldIndex.UpdatedUTC, value); }
		}

		/// <summary>The DeliveryChargeProductId property of the Entity Outlet<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Outlet"."DeliveryChargeProductId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> DeliveryChargeProductId
		{
			get { return (Nullable<System.Int32>)GetValue((int)OutletFieldIndex.DeliveryChargeProductId, false); }
			set	{ SetValue((int)OutletFieldIndex.DeliveryChargeProductId, value); }
		}

		/// <summary>The Latitude property of the Entity Outlet<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Outlet"."Latitude".<br/>Table field type characteristics (type, precision, scale, length): Float, 38, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Double Latitude
		{
			get { return (System.Double)GetValue((int)OutletFieldIndex.Latitude, true); }
			set	{ SetValue((int)OutletFieldIndex.Latitude, value); }
		}

		/// <summary>The Longitude property of the Entity Outlet<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Outlet"."Longitude".<br/>Table field type characteristics (type, precision, scale, length): Float, 38, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Double Longitude
		{
			get { return (System.Double)GetValue((int)OutletFieldIndex.Longitude, true); }
			set	{ SetValue((int)OutletFieldIndex.Longitude, value); }
		}

		/// <summary>The ServiceChargeProductId property of the Entity Outlet<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Outlet"."ServiceChargeProductId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ServiceChargeProductId
		{
			get { return (Nullable<System.Int32>)GetValue((int)OutletFieldIndex.ServiceChargeProductId, false); }
			set	{ SetValue((int)OutletFieldIndex.ServiceChargeProductId, value); }
		}

		/// <summary>The CustomerDetailsNameRequired property of the Entity Outlet<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Outlet"."CustomerDetailsNameRequired".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean CustomerDetailsNameRequired
		{
			get { return (System.Boolean)GetValue((int)OutletFieldIndex.CustomerDetailsNameRequired, true); }
			set	{ SetValue((int)OutletFieldIndex.CustomerDetailsNameRequired, value); }
		}

		/// <summary>The OutletSellerInformationId property of the Entity Outlet<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Outlet"."OutletSellerInformationId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> OutletSellerInformationId
		{
			get { return (Nullable<System.Int32>)GetValue((int)OutletFieldIndex.OutletSellerInformationId, false); }
			set	{ SetValue((int)OutletFieldIndex.OutletSellerInformationId, value); }
		}

		/// <summary>The ShowTaxBreakdown property of the Entity Outlet<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Outlet"."ShowTaxBreakdown".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Crave.Enums.TaxBreakdown ShowTaxBreakdown
		{
			get { return (Crave.Enums.TaxBreakdown)GetValue((int)OutletFieldIndex.ShowTaxBreakdown, true); }
			set	{ SetValue((int)OutletFieldIndex.ShowTaxBreakdown, value); }
		}

		/// <summary>The OptInType property of the Entity Outlet<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Outlet"."OptInType".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Crave.Enums.OptInType OptInType
		{
			get { return (Crave.Enums.OptInType)GetValue((int)OutletFieldIndex.OptInType, true); }
			set	{ SetValue((int)OutletFieldIndex.OptInType, value); }
		}

		/// <summary>The TaxDisclaimer property of the Entity Outlet<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Outlet"."TaxDisclaimer".<br/>Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 2000.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String TaxDisclaimer
		{
			get { return (System.String)GetValue((int)OutletFieldIndex.TaxDisclaimer, true); }
			set	{ SetValue((int)OutletFieldIndex.TaxDisclaimer, value); }
		}

		/// <summary>The TaxNumberTitle property of the Entity Outlet<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Outlet"."TaxNumberTitle".<br/>Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 100.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String TaxNumberTitle
		{
			get { return (System.String)GetValue((int)OutletFieldIndex.TaxNumberTitle, true); }
			set	{ SetValue((int)OutletFieldIndex.TaxNumberTitle, value); }
		}

		/// <summary>Gets the EntityCollection with the related entities of type 'BusinesshourEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(BusinesshourEntity))]
		public virtual EntityCollection<BusinesshourEntity> BusinesshourCollection { get { return GetOrCreateEntityCollection<BusinesshourEntity, BusinesshourEntityFactory>("Outlet", true, false, ref _businesshourCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'CheckoutMethodEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(CheckoutMethodEntity))]
		public virtual EntityCollection<CheckoutMethodEntity> CheckoutMethodCollection { get { return GetOrCreateEntityCollection<CheckoutMethodEntity, CheckoutMethodEntityFactory>("Outlet", true, false, ref _checkoutMethodCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'OptInEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(OptInEntity))]
		public virtual EntityCollection<OptInEntity> OptInCollection { get { return GetOrCreateEntityCollection<OptInEntity, OptInEntityFactory>("Outlet", true, false, ref _optInCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'OrderEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(OrderEntity))]
		public virtual EntityCollection<OrderEntity> OrderCollection { get { return GetOrCreateEntityCollection<OrderEntity, OrderEntityFactory>("Outlet", true, false, ref _orderCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'ServiceMethodEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(ServiceMethodEntity))]
		public virtual EntityCollection<ServiceMethodEntity> ServiceMethodCollection { get { return GetOrCreateEntityCollection<ServiceMethodEntity, ServiceMethodEntityFactory>("Outlet", true, false, ref _serviceMethodCollection); } }

		/// <summary>Gets / sets related entity of type 'CompanyEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual CompanyEntity Company
		{
			get { return _company; }
			set { SetSingleRelatedEntityNavigator(value, "Company"); }
		}

		/// <summary>Gets / sets related entity of type 'OutletOperationalStateEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual OutletOperationalStateEntity OutletOperationalState
		{
			get { return _outletOperationalState; }
			set { SetSingleRelatedEntityNavigator(value, "OutletOperationalState"); }
		}

		/// <summary>Gets / sets related entity of type 'OutletSellerInformationEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual OutletSellerInformationEntity OutletSellerInformation
		{
			get { return _outletSellerInformation; }
			set { SetSingleRelatedEntityNavigator(value, "OutletSellerInformation"); }
		}

		/// <summary>Gets / sets related entity of type 'ProductEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual ProductEntity DeliveryChargeProduct
		{
			get { return _deliveryChargeProduct; }
			set { SetSingleRelatedEntityNavigator(value, "DeliveryChargeProduct"); }
		}

		/// <summary>Gets / sets related entity of type 'ProductEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual ProductEntity ServiceChargeProduct
		{
			get { return _serviceChargeProduct; }
			set { SetSingleRelatedEntityNavigator(value, "ServiceChargeProduct"); }
		}

		/// <summary>Gets / sets related entity of type 'ProductEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual ProductEntity TippingProduct
		{
			get { return _tippingProduct; }
			set { SetSingleRelatedEntityNavigator(value, "TippingProduct"); }
		}


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen
{
	public enum OutletFieldIndex
	{
		///<summary>OutletId. </summary>
		OutletId,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>Name. </summary>
		Name,
		///<summary>TippingActive. </summary>
		TippingActive,
		///<summary>TippingDefaultPercentage. </summary>
		TippingDefaultPercentage,
		///<summary>TippingMinimumPercentage. </summary>
		TippingMinimumPercentage,
		///<summary>TippingStepSize. </summary>
		TippingStepSize,
		///<summary>TippingProductId. </summary>
		TippingProductId,
		///<summary>CustomerDetailsSectionActive. </summary>
		CustomerDetailsSectionActive,
		///<summary>CustomerDetailsPhoneRequired. </summary>
		CustomerDetailsPhoneRequired,
		///<summary>CustomerDetailsEmailRequired. </summary>
		CustomerDetailsEmailRequired,
		///<summary>ShowPricesIncludingTax. </summary>
		ShowPricesIncludingTax,
		///<summary>ShowTaxBreakDownOnBasket. </summary>
		ShowTaxBreakDownOnBasket,
		///<summary>ShowTaxBreakDownOnCheckout. </summary>
		ShowTaxBreakDownOnCheckout,
		///<summary>ReceiptEmail. </summary>
		ReceiptEmail,
		///<summary>OutletOperationalStateId. </summary>
		OutletOperationalStateId,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>DeliveryChargeProductId. </summary>
		DeliveryChargeProductId,
		///<summary>Latitude. </summary>
		Latitude,
		///<summary>Longitude. </summary>
		Longitude,
		///<summary>ServiceChargeProductId. </summary>
		ServiceChargeProductId,
		///<summary>CustomerDetailsNameRequired. </summary>
		CustomerDetailsNameRequired,
		///<summary>OutletSellerInformationId. </summary>
		OutletSellerInformationId,
		///<summary>ShowTaxBreakdown. </summary>
		ShowTaxBreakdown,
		///<summary>OptInType. </summary>
		OptInType,
		///<summary>TaxDisclaimer. </summary>
		TaxDisclaimer,
		///<summary>TaxNumberTitle. </summary>
		TaxNumberTitle,
		/// <summary></summary>
		AmountOfFields
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Outlet. </summary>
	public partial class OutletRelations: RelationFactory
	{
		#region Custom Relation code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomRelationCode
        private const ReferentialConstraintDeleteRule DeleteRuleForBusinesshourEntityUsingOutletId = ReferentialConstraintDeleteRule.Cascade;
        private const ReferentialConstraintDeleteRule DeleteRuleForCheckoutMethodEntityUsingOutletId = ReferentialConstraintDeleteRule.Cascade;
        private const ReferentialConstraintDeleteRule DeleteRuleForOrderEntityUsingOutletId = ReferentialConstraintDeleteRule.Cascade;
        private const ReferentialConstraintDeleteRule DeleteRuleForOptInEntityUsingOutletId = ReferentialConstraintDeleteRule.Cascade;
		private const ReferentialConstraintDeleteRule DeleteRuleForOutletSellerInformationEntityUsingOutletId = ReferentialConstraintDeleteRule.Cascade;
        private const ReferentialConstraintDeleteRule DeleteRuleForServiceMethodEntityUsingOutletId = ReferentialConstraintDeleteRule.Cascade;
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		
		// private const ReferentialConstraintDeleteRule DeleteRuleForBusinesshourEntityUsingOutletId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForCheckoutMethodEntityUsingOutletId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForOptInEntityUsingOutletId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForOrderEntityUsingOutletId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForServiceMethodEntityUsingOutletId = ReferentialConstraintDeleteRule


		/// <summary>Gets all relations and delete rules of the OutletEntity as a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects.</summary>
		/// <returns>a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects</returns>
		public virtual Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> GetRelationsAndDeleteRules()
		{
			Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> toReturn = new Dictionary<IEntityRelation, ReferentialConstraintDeleteRule>();
			toReturn.Add(this.BusinesshourEntityUsingOutletId, OutletRelations.DeleteRuleForBusinesshourEntityUsingOutletId);
			toReturn.Add(this.CheckoutMethodEntityUsingOutletId, OutletRelations.DeleteRuleForCheckoutMethodEntityUsingOutletId);
			toReturn.Add(this.OptInEntityUsingOutletId, OutletRelations.DeleteRuleForOptInEntityUsingOutletId);
			toReturn.Add(this.OrderEntityUsingOutletId, OutletRelations.DeleteRuleForOrderEntityUsingOutletId);
			toReturn.Add(this.ServiceMethodEntityUsingOutletId, OutletRelations.DeleteRuleForServiceMethodEntityUsingOutletId);
			return toReturn;
		}		
	
	
		/// <summary>Returns a new IEntityRelation object, between OutletEntity and BusinesshourEntity over the 1:n relation they have, using the relation between the fields: Outlet.OutletId - Businesshour.OutletId</summary>
		public virtual IEntityRelation BusinesshourEntityUsingOutletId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "BusinesshourCollection", true, new[] { OutletFields.OutletId, BusinesshourFields.OutletId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between OutletEntity and CheckoutMethodEntity over the 1:n relation they have, using the relation between the fields: Outlet.OutletId - CheckoutMethod.OutletId</summary>
		public virtual IEntityRelation CheckoutMethodEntityUsingOutletId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "CheckoutMethodCollection", true, new[] { OutletFields.OutletId, CheckoutMethodFields.OutletId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between OutletEntity and OptInEntity over the 1:n relation they have, using the relation between the fields: Outlet.OutletId - OptIn.OutletId</summary>
		public virtual IEntityRelation OptInEntityUsingOutletId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "OptInCollection", true, new[] { OutletFields.OutletId, OptInFields.OutletId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between OutletEntity and OrderEntity over the 1:n relation they have, using the relation between the fields: Outlet.OutletId - Order.OutletId</summary>
		public virtual IEntityRelation OrderEntityUsingOutletId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "OrderCollection", true, new[] { OutletFields.OutletId, OrderFields.OutletId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between OutletEntity and ServiceMethodEntity over the 1:n relation they have, using the relation between the fields: Outlet.OutletId - ServiceMethod.OutletId</summary>
		public virtual IEntityRelation ServiceMethodEntityUsingOutletId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "ServiceMethodCollection", true, new[] { OutletFields.OutletId, ServiceMethodFields.OutletId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between OutletEntity and CompanyEntity over the m:1 relation they have, using the relation between the fields: Outlet.CompanyId - Company.CompanyId</summary>
		public virtual IEntityRelation CompanyEntityUsingCompanyId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Company", false, new[] { CompanyFields.CompanyId, OutletFields.CompanyId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between OutletEntity and OutletOperationalStateEntity over the m:1 relation they have, using the relation between the fields: Outlet.OutletOperationalStateId - OutletOperationalState.OutletOperationalStateId</summary>
		public virtual IEntityRelation OutletOperationalStateEntityUsingOutletOperationalStateId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "OutletOperationalState", false, new[] { OutletOperationalStateFields.OutletOperationalStateId, OutletFields.OutletOperationalStateId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between OutletEntity and OutletSellerInformationEntity over the m:1 relation they have, using the relation between the fields: Outlet.OutletSellerInformationId - OutletSellerInformation.OutletSellerInformationId</summary>
		public virtual IEntityRelation OutletSellerInformationEntityUsingOutletSellerInformationId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "OutletSellerInformation", false, new[] { OutletSellerInformationFields.OutletSellerInformationId, OutletFields.OutletSellerInformationId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between OutletEntity and ProductEntity over the m:1 relation they have, using the relation between the fields: Outlet.DeliveryChargeProductId - Product.ProductId</summary>
		public virtual IEntityRelation ProductEntityUsingDeliveryChargeProductId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "DeliveryChargeProduct", false, new[] { ProductFields.ProductId, OutletFields.DeliveryChargeProductId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between OutletEntity and ProductEntity over the m:1 relation they have, using the relation between the fields: Outlet.ServiceChargeProductId - Product.ProductId</summary>
		public virtual IEntityRelation ProductEntityUsingServiceChargeProductId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "ServiceChargeProduct", false, new[] { ProductFields.ProductId, OutletFields.ServiceChargeProductId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between OutletEntity and ProductEntity over the m:1 relation they have, using the relation between the fields: Outlet.TippingProductId - Product.ProductId</summary>
		public virtual IEntityRelation ProductEntityUsingTippingProductId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "TippingProduct", false, new[] { ProductFields.ProductId, OutletFields.TippingProductId }); }
		}

	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticOutletRelations
	{
		internal static readonly IEntityRelation BusinesshourEntityUsingOutletIdStatic = new OutletRelations().BusinesshourEntityUsingOutletId;
		internal static readonly IEntityRelation CheckoutMethodEntityUsingOutletIdStatic = new OutletRelations().CheckoutMethodEntityUsingOutletId;
		internal static readonly IEntityRelation OptInEntityUsingOutletIdStatic = new OutletRelations().OptInEntityUsingOutletId;
		internal static readonly IEntityRelation OrderEntityUsingOutletIdStatic = new OutletRelations().OrderEntityUsingOutletId;
		internal static readonly IEntityRelation ServiceMethodEntityUsingOutletIdStatic = new OutletRelations().ServiceMethodEntityUsingOutletId;
		internal static readonly IEntityRelation CompanyEntityUsingCompanyIdStatic = new OutletRelations().CompanyEntityUsingCompanyId;
		internal static readonly IEntityRelation OutletOperationalStateEntityUsingOutletOperationalStateIdStatic = new OutletRelations().OutletOperationalStateEntityUsingOutletOperationalStateId;
		internal static readonly IEntityRelation OutletSellerInformationEntityUsingOutletSellerInformationIdStatic = new OutletRelations().OutletSellerInformationEntityUsingOutletSellerInformationId;
		internal static readonly IEntityRelation ProductEntityUsingDeliveryChargeProductIdStatic = new OutletRelations().ProductEntityUsingDeliveryChargeProductId;
		internal static readonly IEntityRelation ProductEntityUsingServiceChargeProductIdStatic = new OutletRelations().ProductEntityUsingServiceChargeProductId;
		internal static readonly IEntityRelation ProductEntityUsingTippingProductIdStatic = new OutletRelations().ProductEntityUsingTippingProductId;

		/// <summary>CTor</summary>
		static StaticOutletRelations() { }
	}
}

