﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 5.8
// Code is generated on: 
// Code is generated using templates: Crave.TemplateBindings
// Templates vendor: Crave Interactive
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.FactoryClasses;
using Crave.Ordering.Shared.Data.LLBLGen.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

// ReSharper disable All
namespace Crave.Ordering.Shared.Data.LLBLGen.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'Supportpool'.<br/><br/></summary>
	[Serializable]
	public partial class SupportpoolEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END
	
	{
		private EntityCollection<CompanyEntity> _companyCollection;
		private EntityCollection<OrderRoutestephandlerEntity> _orderRoutestephandlerCollection;
		private EntityCollection<OrderRoutestephandlerHistoryEntity> _orderRoutestephandlerHistoryCollection;
		private EntityCollection<RoutestephandlerEntity> _routestephandlerCollection;
		private EntityCollection<SupportpoolNotificationRecipientEntity> _supportpoolNotificationRecipientCollection;
		private EntityCollection<SupportpoolSupportagentEntity> _supportpoolSupportagentCollection;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		private static SupportpoolEntityStaticMetaData _staticMetaData = new SupportpoolEntityStaticMetaData();
		private static SupportpoolRelations _relationsFactory = new SupportpoolRelations();		

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name CompanyCollection</summary>
			public static readonly string CompanyCollection = "CompanyCollection";
			/// <summary>Member name OrderRoutestephandlerCollection</summary>
			public static readonly string OrderRoutestephandlerCollection = "OrderRoutestephandlerCollection";
			/// <summary>Member name OrderRoutestephandlerHistoryCollection</summary>
			public static readonly string OrderRoutestephandlerHistoryCollection = "OrderRoutestephandlerHistoryCollection";
			/// <summary>Member name RoutestephandlerCollection</summary>
			public static readonly string RoutestephandlerCollection = "RoutestephandlerCollection";
			/// <summary>Member name SupportpoolNotificationRecipientCollection</summary>
			public static readonly string SupportpoolNotificationRecipientCollection = "SupportpoolNotificationRecipientCollection";
			/// <summary>Member name SupportpoolSupportagentCollection</summary>
			public static readonly string SupportpoolSupportagentCollection = "SupportpoolSupportagentCollection";
		}

		/// <summary>Static meta-data storage for navigator related information</summary>
		protected class SupportpoolEntityStaticMetaData : EntityStaticMetaDataBase
		{
			public SupportpoolEntityStaticMetaData()
			{
				SetEntityCoreInfo("SupportpoolEntity", InheritanceHierarchyType.None, false, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.SupportpoolEntity, typeof(SupportpoolEntity), typeof(SupportpoolEntityFactory), false);
				AddNavigatorMetaData<SupportpoolEntity, EntityCollection<CompanyEntity>>("CompanyCollection", a => a._companyCollection, (a, b) => a._companyCollection = b, a => a.CompanyCollection, () => new SupportpoolRelations().CompanyEntityUsingSupportpoolId, typeof(CompanyEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.CompanyEntity);
				AddNavigatorMetaData<SupportpoolEntity, EntityCollection<OrderRoutestephandlerEntity>>("OrderRoutestephandlerCollection", a => a._orderRoutestephandlerCollection, (a, b) => a._orderRoutestephandlerCollection = b, a => a.OrderRoutestephandlerCollection, () => new SupportpoolRelations().OrderRoutestephandlerEntityUsingSupportpoolId, typeof(OrderRoutestephandlerEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.OrderRoutestephandlerEntity);
				AddNavigatorMetaData<SupportpoolEntity, EntityCollection<OrderRoutestephandlerHistoryEntity>>("OrderRoutestephandlerHistoryCollection", a => a._orderRoutestephandlerHistoryCollection, (a, b) => a._orderRoutestephandlerHistoryCollection = b, a => a.OrderRoutestephandlerHistoryCollection, () => new SupportpoolRelations().OrderRoutestephandlerHistoryEntityUsingSupportpoolId, typeof(OrderRoutestephandlerHistoryEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.OrderRoutestephandlerHistoryEntity);
				AddNavigatorMetaData<SupportpoolEntity, EntityCollection<RoutestephandlerEntity>>("RoutestephandlerCollection", a => a._routestephandlerCollection, (a, b) => a._routestephandlerCollection = b, a => a.RoutestephandlerCollection, () => new SupportpoolRelations().RoutestephandlerEntityUsingSupportpoolId, typeof(RoutestephandlerEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.RoutestephandlerEntity);
				AddNavigatorMetaData<SupportpoolEntity, EntityCollection<SupportpoolNotificationRecipientEntity>>("SupportpoolNotificationRecipientCollection", a => a._supportpoolNotificationRecipientCollection, (a, b) => a._supportpoolNotificationRecipientCollection = b, a => a.SupportpoolNotificationRecipientCollection, () => new SupportpoolRelations().SupportpoolNotificationRecipientEntityUsingSupportpoolId, typeof(SupportpoolNotificationRecipientEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.SupportpoolNotificationRecipientEntity);
				AddNavigatorMetaData<SupportpoolEntity, EntityCollection<SupportpoolSupportagentEntity>>("SupportpoolSupportagentCollection", a => a._supportpoolSupportagentCollection, (a, b) => a._supportpoolSupportagentCollection = b, a => a.SupportpoolSupportagentCollection, () => new SupportpoolRelations().SupportpoolSupportagentEntityUsingSupportpoolId, typeof(SupportpoolSupportagentEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.SupportpoolSupportagentEntity);
			}
		}

		/// <summary>Static ctor</summary>
		static SupportpoolEntity()
		{
		}

		/// <summary> CTor</summary>
		public SupportpoolEntity()
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public SupportpoolEntity(IEntityFields2 fields)
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this SupportpoolEntity</param>
		public SupportpoolEntity(IValidator validator)
		{
			InitClassEmpty(validator, null);
		}

		/// <summary> CTor</summary>
		/// <param name="supportpoolId">PK value for Supportpool which data should be fetched into this Supportpool object</param>
		public SupportpoolEntity(System.Int32 supportpoolId) : this(supportpoolId, null)
		{
		}

		/// <summary> CTor</summary>
		/// <param name="supportpoolId">PK value for Supportpool which data should be fetched into this Supportpool object</param>
		/// <param name="validator">The custom validator object for this SupportpoolEntity</param>
		public SupportpoolEntity(System.Int32 supportpoolId, IValidator validator)
		{
			InitClassEmpty(validator, null);
			this.SupportpoolId = supportpoolId;
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected SupportpoolEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Company' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoCompanyCollection() { return CreateRelationInfoForNavigator("CompanyCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'OrderRoutestephandler' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoOrderRoutestephandlerCollection() { return CreateRelationInfoForNavigator("OrderRoutestephandlerCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'OrderRoutestephandlerHistory' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoOrderRoutestephandlerHistoryCollection() { return CreateRelationInfoForNavigator("OrderRoutestephandlerHistoryCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Routestephandler' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoRoutestephandlerCollection() { return CreateRelationInfoForNavigator("RoutestephandlerCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'SupportpoolNotificationRecipient' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoSupportpoolNotificationRecipientCollection() { return CreateRelationInfoForNavigator("SupportpoolNotificationRecipientCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'SupportpoolSupportagent' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoSupportpoolSupportagentCollection() { return CreateRelationInfoForNavigator("SupportpoolSupportagentCollection"); }
		
		/// <inheritdoc/>
		protected override EntityStaticMetaDataBase GetEntityStaticMetaData() {	return _staticMetaData; }

		/// <summary>Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		/// <summary>Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this SupportpoolEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary>The relations object holding all relations of this entity with other entity classes.</summary>
		public static SupportpoolRelations Relations { get { return _relationsFactory; } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Company' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathCompanyCollection { get { return _staticMetaData.GetPrefetchPathElement("CompanyCollection", CommonEntityBase.CreateEntityCollection<CompanyEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'OrderRoutestephandler' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathOrderRoutestephandlerCollection { get { return _staticMetaData.GetPrefetchPathElement("OrderRoutestephandlerCollection", CommonEntityBase.CreateEntityCollection<OrderRoutestephandlerEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'OrderRoutestephandlerHistory' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathOrderRoutestephandlerHistoryCollection { get { return _staticMetaData.GetPrefetchPathElement("OrderRoutestephandlerHistoryCollection", CommonEntityBase.CreateEntityCollection<OrderRoutestephandlerHistoryEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Routestephandler' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathRoutestephandlerCollection { get { return _staticMetaData.GetPrefetchPathElement("RoutestephandlerCollection", CommonEntityBase.CreateEntityCollection<RoutestephandlerEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'SupportpoolNotificationRecipient' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathSupportpoolNotificationRecipientCollection { get { return _staticMetaData.GetPrefetchPathElement("SupportpoolNotificationRecipientCollection", CommonEntityBase.CreateEntityCollection<SupportpoolNotificationRecipientEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'SupportpoolSupportagent' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathSupportpoolSupportagentCollection { get { return _staticMetaData.GetPrefetchPathElement("SupportpoolSupportagentCollection", CommonEntityBase.CreateEntityCollection<SupportpoolSupportagentEntity>()); } }

		/// <summary>The SupportpoolId property of the Entity Supportpool<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Supportpool"."SupportpoolId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 SupportpoolId
		{
			get { return (System.Int32)GetValue((int)SupportpoolFieldIndex.SupportpoolId, true); }
			set { SetValue((int)SupportpoolFieldIndex.SupportpoolId, value); }		}

		/// <summary>The Name property of the Entity Supportpool<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Supportpool"."Name".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)SupportpoolFieldIndex.Name, true); }
			set	{ SetValue((int)SupportpoolFieldIndex.Name, value); }
		}

		/// <summary>The Phonenumber property of the Entity Supportpool<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Supportpool"."Phonenumber".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Phonenumber
		{
			get { return (System.String)GetValue((int)SupportpoolFieldIndex.Phonenumber, true); }
			set	{ SetValue((int)SupportpoolFieldIndex.Phonenumber, value); }
		}

		/// <summary>The Email property of the Entity Supportpool<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Supportpool"."Email".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Email
		{
			get { return (System.String)GetValue((int)SupportpoolFieldIndex.Email, true); }
			set	{ SetValue((int)SupportpoolFieldIndex.Email, value); }
		}

		/// <summary>The SystemSupportPool property of the Entity Supportpool<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Supportpool"."SystemSupportPool".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean SystemSupportPool
		{
			get { return (System.Boolean)GetValue((int)SupportpoolFieldIndex.SystemSupportPool, true); }
			set	{ SetValue((int)SupportpoolFieldIndex.SystemSupportPool, value); }
		}

		/// <summary>The ContentSupportPool property of the Entity Supportpool<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Supportpool"."ContentSupportPool".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean ContentSupportPool
		{
			get { return (System.Boolean)GetValue((int)SupportpoolFieldIndex.ContentSupportPool, true); }
			set	{ SetValue((int)SupportpoolFieldIndex.ContentSupportPool, value); }
		}

		/// <summary>The Created property of the Entity Supportpool<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Supportpool"."Created".<br/>Table field type characteristics (type, precision, scale, length): DateTime, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> Created
		{
			get { return (Nullable<System.DateTime>)GetValue((int)SupportpoolFieldIndex.Created, false); }
			set	{ SetValue((int)SupportpoolFieldIndex.Created, value); }
		}

		/// <summary>The CreatedBy property of the Entity Supportpool<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Supportpool"."CreatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)SupportpoolFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)SupportpoolFieldIndex.CreatedBy, value); }
		}

		/// <summary>The Updated property of the Entity Supportpool<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Supportpool"."Updated".<br/>Table field type characteristics (type, precision, scale, length): DateTime, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> Updated
		{
			get { return (Nullable<System.DateTime>)GetValue((int)SupportpoolFieldIndex.Updated, false); }
			set	{ SetValue((int)SupportpoolFieldIndex.Updated, value); }
		}

		/// <summary>The UpdatedBy property of the Entity Supportpool<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Supportpool"."UpdatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)SupportpoolFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)SupportpoolFieldIndex.UpdatedBy, value); }
		}

		/// <summary>The CreatedUTC property of the Entity Supportpool<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Supportpool"."CreatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)SupportpoolFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)SupportpoolFieldIndex.CreatedUTC, value); }
		}

		/// <summary>The UpdatedUTC property of the Entity Supportpool<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Supportpool"."UpdatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)SupportpoolFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)SupportpoolFieldIndex.UpdatedUTC, value); }
		}

		/// <summary>Gets the EntityCollection with the related entities of type 'CompanyEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(CompanyEntity))]
		public virtual EntityCollection<CompanyEntity> CompanyCollection { get { return GetOrCreateEntityCollection<CompanyEntity, CompanyEntityFactory>("Supportpool", true, false, ref _companyCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'OrderRoutestephandlerEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(OrderRoutestephandlerEntity))]
		public virtual EntityCollection<OrderRoutestephandlerEntity> OrderRoutestephandlerCollection { get { return GetOrCreateEntityCollection<OrderRoutestephandlerEntity, OrderRoutestephandlerEntityFactory>("Supportpool", true, false, ref _orderRoutestephandlerCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'OrderRoutestephandlerHistoryEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(OrderRoutestephandlerHistoryEntity))]
		public virtual EntityCollection<OrderRoutestephandlerHistoryEntity> OrderRoutestephandlerHistoryCollection { get { return GetOrCreateEntityCollection<OrderRoutestephandlerHistoryEntity, OrderRoutestephandlerHistoryEntityFactory>("Supportpool", true, false, ref _orderRoutestephandlerHistoryCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'RoutestephandlerEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(RoutestephandlerEntity))]
		public virtual EntityCollection<RoutestephandlerEntity> RoutestephandlerCollection { get { return GetOrCreateEntityCollection<RoutestephandlerEntity, RoutestephandlerEntityFactory>("Supportpool", true, false, ref _routestephandlerCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'SupportpoolNotificationRecipientEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(SupportpoolNotificationRecipientEntity))]
		public virtual EntityCollection<SupportpoolNotificationRecipientEntity> SupportpoolNotificationRecipientCollection { get { return GetOrCreateEntityCollection<SupportpoolNotificationRecipientEntity, SupportpoolNotificationRecipientEntityFactory>("Supportpool", true, false, ref _supportpoolNotificationRecipientCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'SupportpoolSupportagentEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(SupportpoolSupportagentEntity))]
		public virtual EntityCollection<SupportpoolSupportagentEntity> SupportpoolSupportagentCollection { get { return GetOrCreateEntityCollection<SupportpoolSupportagentEntity, SupportpoolSupportagentEntityFactory>("Supportpool", true, false, ref _supportpoolSupportagentCollection); } }


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen
{
	public enum SupportpoolFieldIndex
	{
		///<summary>SupportpoolId. </summary>
		SupportpoolId,
		///<summary>Name. </summary>
		Name,
		///<summary>Phonenumber. </summary>
		Phonenumber,
		///<summary>Email. </summary>
		Email,
		///<summary>SystemSupportPool. </summary>
		SystemSupportPool,
		///<summary>ContentSupportPool. </summary>
		ContentSupportPool,
		///<summary>Created. </summary>
		Created,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>Updated. </summary>
		Updated,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Supportpool. </summary>
	public partial class SupportpoolRelations: RelationFactory
	{
		#region Custom Relation code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomRelationCode
		private const ReferentialConstraintDeleteRule DeleteRuleForCompanyEntityUsingSupportpoolId = ReferentialConstraintDeleteRule.NoAction;
        private const ReferentialConstraintDeleteRule DeleteRuleForOrderRoutestephandlerEntityUsingSupportpoolId = ReferentialConstraintDeleteRule.NoAction;
        private const ReferentialConstraintDeleteRule DeleteRuleForOrderRoutestephandlerHistoryEntityUsingSupportpoolId = ReferentialConstraintDeleteRule.NoAction;
        private const ReferentialConstraintDeleteRule DeleteRuleForRoutestephandlerEntityUsingSupportpoolId = ReferentialConstraintDeleteRule.NoAction;
        private const ReferentialConstraintDeleteRule DeleteRuleForSupportpoolNotificationRecipientEntityUsingSupportpoolId = ReferentialConstraintDeleteRule.Cascade;
        private const ReferentialConstraintDeleteRule DeleteRuleForSupportpoolSupportagentEntityUsingSupportpoolId = ReferentialConstraintDeleteRule.Cascade;
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		
		// private const ReferentialConstraintDeleteRule DeleteRuleForCompanyEntityUsingSupportpoolId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForOrderRoutestephandlerEntityUsingSupportpoolId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForOrderRoutestephandlerHistoryEntityUsingSupportpoolId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForRoutestephandlerEntityUsingSupportpoolId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForSupportpoolNotificationRecipientEntityUsingSupportpoolId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForSupportpoolSupportagentEntityUsingSupportpoolId = ReferentialConstraintDeleteRule


		/// <summary>Gets all relations and delete rules of the SupportpoolEntity as a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects.</summary>
		/// <returns>a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects</returns>
		public virtual Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> GetRelationsAndDeleteRules()
		{
			Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> toReturn = new Dictionary<IEntityRelation, ReferentialConstraintDeleteRule>();
			toReturn.Add(this.CompanyEntityUsingSupportpoolId, SupportpoolRelations.DeleteRuleForCompanyEntityUsingSupportpoolId);
			toReturn.Add(this.OrderRoutestephandlerEntityUsingSupportpoolId, SupportpoolRelations.DeleteRuleForOrderRoutestephandlerEntityUsingSupportpoolId);
			toReturn.Add(this.OrderRoutestephandlerHistoryEntityUsingSupportpoolId, SupportpoolRelations.DeleteRuleForOrderRoutestephandlerHistoryEntityUsingSupportpoolId);
			toReturn.Add(this.RoutestephandlerEntityUsingSupportpoolId, SupportpoolRelations.DeleteRuleForRoutestephandlerEntityUsingSupportpoolId);
			toReturn.Add(this.SupportpoolNotificationRecipientEntityUsingSupportpoolId, SupportpoolRelations.DeleteRuleForSupportpoolNotificationRecipientEntityUsingSupportpoolId);
			toReturn.Add(this.SupportpoolSupportagentEntityUsingSupportpoolId, SupportpoolRelations.DeleteRuleForSupportpoolSupportagentEntityUsingSupportpoolId);
			return toReturn;
		}		
	
	
		/// <summary>Returns a new IEntityRelation object, between SupportpoolEntity and CompanyEntity over the 1:n relation they have, using the relation between the fields: Supportpool.SupportpoolId - Company.SupportpoolId</summary>
		public virtual IEntityRelation CompanyEntityUsingSupportpoolId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "CompanyCollection", true, new[] { SupportpoolFields.SupportpoolId, CompanyFields.SupportpoolId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between SupportpoolEntity and OrderRoutestephandlerEntity over the 1:n relation they have, using the relation between the fields: Supportpool.SupportpoolId - OrderRoutestephandler.SupportpoolId</summary>
		public virtual IEntityRelation OrderRoutestephandlerEntityUsingSupportpoolId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "OrderRoutestephandlerCollection", true, new[] { SupportpoolFields.SupportpoolId, OrderRoutestephandlerFields.SupportpoolId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between SupportpoolEntity and OrderRoutestephandlerHistoryEntity over the 1:n relation they have, using the relation between the fields: Supportpool.SupportpoolId - OrderRoutestephandlerHistory.SupportpoolId</summary>
		public virtual IEntityRelation OrderRoutestephandlerHistoryEntityUsingSupportpoolId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "OrderRoutestephandlerHistoryCollection", true, new[] { SupportpoolFields.SupportpoolId, OrderRoutestephandlerHistoryFields.SupportpoolId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between SupportpoolEntity and RoutestephandlerEntity over the 1:n relation they have, using the relation between the fields: Supportpool.SupportpoolId - Routestephandler.SupportpoolId</summary>
		public virtual IEntityRelation RoutestephandlerEntityUsingSupportpoolId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "RoutestephandlerCollection", true, new[] { SupportpoolFields.SupportpoolId, RoutestephandlerFields.SupportpoolId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between SupportpoolEntity and SupportpoolNotificationRecipientEntity over the 1:n relation they have, using the relation between the fields: Supportpool.SupportpoolId - SupportpoolNotificationRecipient.SupportpoolId</summary>
		public virtual IEntityRelation SupportpoolNotificationRecipientEntityUsingSupportpoolId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "SupportpoolNotificationRecipientCollection", true, new[] { SupportpoolFields.SupportpoolId, SupportpoolNotificationRecipientFields.SupportpoolId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between SupportpoolEntity and SupportpoolSupportagentEntity over the 1:n relation they have, using the relation between the fields: Supportpool.SupportpoolId - SupportpoolSupportagent.SupportpoolId</summary>
		public virtual IEntityRelation SupportpoolSupportagentEntityUsingSupportpoolId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "SupportpoolSupportagentCollection", true, new[] { SupportpoolFields.SupportpoolId, SupportpoolSupportagentFields.SupportpoolId }); }
		}

	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticSupportpoolRelations
	{
		internal static readonly IEntityRelation CompanyEntityUsingSupportpoolIdStatic = new SupportpoolRelations().CompanyEntityUsingSupportpoolId;
		internal static readonly IEntityRelation OrderRoutestephandlerEntityUsingSupportpoolIdStatic = new SupportpoolRelations().OrderRoutestephandlerEntityUsingSupportpoolId;
		internal static readonly IEntityRelation OrderRoutestephandlerHistoryEntityUsingSupportpoolIdStatic = new SupportpoolRelations().OrderRoutestephandlerHistoryEntityUsingSupportpoolId;
		internal static readonly IEntityRelation RoutestephandlerEntityUsingSupportpoolIdStatic = new SupportpoolRelations().RoutestephandlerEntityUsingSupportpoolId;
		internal static readonly IEntityRelation SupportpoolNotificationRecipientEntityUsingSupportpoolIdStatic = new SupportpoolRelations().SupportpoolNotificationRecipientEntityUsingSupportpoolId;
		internal static readonly IEntityRelation SupportpoolSupportagentEntityUsingSupportpoolIdStatic = new SupportpoolRelations().SupportpoolSupportagentEntityUsingSupportpoolId;

		/// <summary>CTor</summary>
		static StaticSupportpoolRelations() { }
	}
}

