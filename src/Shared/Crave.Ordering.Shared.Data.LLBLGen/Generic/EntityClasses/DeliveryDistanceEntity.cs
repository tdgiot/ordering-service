﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 5.8
// Code is generated on: 
// Code is generated using templates: Crave.TemplateBindings
// Templates vendor: Crave Interactive
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.FactoryClasses;
using Crave.Ordering.Shared.Data.LLBLGen.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

// ReSharper disable All
namespace Crave.Ordering.Shared.Data.LLBLGen.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'DeliveryDistance'.<br/><br/></summary>
	[Serializable]
	public partial class DeliveryDistanceEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END
	
	{
		private ServiceMethodEntity _serviceMethod;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		private static DeliveryDistanceEntityStaticMetaData _staticMetaData = new DeliveryDistanceEntityStaticMetaData();
		private static DeliveryDistanceRelations _relationsFactory = new DeliveryDistanceRelations();		

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name ServiceMethod</summary>
			public static readonly string ServiceMethod = "ServiceMethod";
		}

		/// <summary>Static meta-data storage for navigator related information</summary>
		protected class DeliveryDistanceEntityStaticMetaData : EntityStaticMetaDataBase
		{
			public DeliveryDistanceEntityStaticMetaData()
			{
				SetEntityCoreInfo("DeliveryDistanceEntity", InheritanceHierarchyType.None, false, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.DeliveryDistanceEntity, typeof(DeliveryDistanceEntity), typeof(DeliveryDistanceEntityFactory), false);
				AddNavigatorMetaData<DeliveryDistanceEntity, ServiceMethodEntity>("ServiceMethod", "DeliveryDistanceCollection", (a, b) => a._serviceMethod = b, a => a._serviceMethod, (a, b) => a.ServiceMethod = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticDeliveryDistanceRelations.ServiceMethodEntityUsingServiceMethodIdStatic, ()=>new DeliveryDistanceRelations().ServiceMethodEntityUsingServiceMethodId, null, new int[] { (int)DeliveryDistanceFieldIndex.ServiceMethodId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ServiceMethodEntity);
			}
		}

		/// <summary>Static ctor</summary>
		static DeliveryDistanceEntity()
		{
		}

		/// <summary> CTor</summary>
		public DeliveryDistanceEntity()
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public DeliveryDistanceEntity(IEntityFields2 fields)
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this DeliveryDistanceEntity</param>
		public DeliveryDistanceEntity(IValidator validator)
		{
			InitClassEmpty(validator, null);
		}

		/// <summary> CTor</summary>
		/// <param name="deliveryDistanceId">PK value for DeliveryDistance which data should be fetched into this DeliveryDistance object</param>
		public DeliveryDistanceEntity(System.Int32 deliveryDistanceId) : this(deliveryDistanceId, null)
		{
		}

		/// <summary> CTor</summary>
		/// <param name="deliveryDistanceId">PK value for DeliveryDistance which data should be fetched into this DeliveryDistance object</param>
		/// <param name="validator">The custom validator object for this DeliveryDistanceEntity</param>
		public DeliveryDistanceEntity(System.Int32 deliveryDistanceId, IValidator validator)
		{
			InitClassEmpty(validator, null);
			this.DeliveryDistanceId = deliveryDistanceId;
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected DeliveryDistanceEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'ServiceMethod' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoServiceMethod() { return CreateRelationInfoForNavigator("ServiceMethod"); }
		
		/// <inheritdoc/>
		protected override EntityStaticMetaDataBase GetEntityStaticMetaData() {	return _staticMetaData; }

		/// <summary>Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		/// <summary>Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this DeliveryDistanceEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary>The relations object holding all relations of this entity with other entity classes.</summary>
		public static DeliveryDistanceRelations Relations { get { return _relationsFactory; } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'ServiceMethod' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathServiceMethodEntity { get { return _staticMetaData.GetPrefetchPathElement("ServiceMethod", CommonEntityBase.CreateEntityCollection<ServiceMethodEntity>()); } }

		/// <summary>The DeliveryDistanceId property of the Entity DeliveryDistance<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliveryDistance"."DeliveryDistanceId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 DeliveryDistanceId
		{
			get { return (System.Int32)GetValue((int)DeliveryDistanceFieldIndex.DeliveryDistanceId, true); }
			set { SetValue((int)DeliveryDistanceFieldIndex.DeliveryDistanceId, value); }		}

		/// <summary>The ServiceMethodId property of the Entity DeliveryDistance<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliveryDistance"."ServiceMethodId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ServiceMethodId
		{
			get { return (System.Int32)GetValue((int)DeliveryDistanceFieldIndex.ServiceMethodId, true); }
			set	{ SetValue((int)DeliveryDistanceFieldIndex.ServiceMethodId, value); }
		}

		/// <summary>The MaximumInMetres property of the Entity DeliveryDistance<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliveryDistance"."MaximumInMetres".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 MaximumInMetres
		{
			get { return (System.Int32)GetValue((int)DeliveryDistanceFieldIndex.MaximumInMetres, true); }
			set	{ SetValue((int)DeliveryDistanceFieldIndex.MaximumInMetres, value); }
		}

		/// <summary>The Price property of the Entity DeliveryDistance<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliveryDistance"."Price".<br/>Table field type characteristics (type, precision, scale, length): Money, 19, 4, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal Price
		{
			get { return (System.Decimal)GetValue((int)DeliveryDistanceFieldIndex.Price, true); }
			set	{ SetValue((int)DeliveryDistanceFieldIndex.Price, value); }
		}

		/// <summary>The CreatedUTC property of the Entity DeliveryDistance<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliveryDistance"."CreatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)DeliveryDistanceFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)DeliveryDistanceFieldIndex.CreatedUTC, value); }
		}

		/// <summary>The CreatedBy property of the Entity DeliveryDistance<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliveryDistance"."CreatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)DeliveryDistanceFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)DeliveryDistanceFieldIndex.CreatedBy, value); }
		}

		/// <summary>The UpdatedUTC property of the Entity DeliveryDistance<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliveryDistance"."UpdatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)DeliveryDistanceFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)DeliveryDistanceFieldIndex.UpdatedUTC, value); }
		}

		/// <summary>The UpdatedBy property of the Entity DeliveryDistance<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliveryDistance"."UpdatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)DeliveryDistanceFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)DeliveryDistanceFieldIndex.UpdatedBy, value); }
		}

		/// <summary>Gets / sets related entity of type 'ServiceMethodEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual ServiceMethodEntity ServiceMethod
		{
			get { return _serviceMethod; }
			set { SetSingleRelatedEntityNavigator(value, "ServiceMethod"); }
		}


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen
{
	public enum DeliveryDistanceFieldIndex
	{
		///<summary>DeliveryDistanceId. </summary>
		DeliveryDistanceId,
		///<summary>ServiceMethodId. </summary>
		ServiceMethodId,
		///<summary>MaximumInMetres. </summary>
		MaximumInMetres,
		///<summary>Price. </summary>
		Price,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		/// <summary></summary>
		AmountOfFields
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: DeliveryDistance. </summary>
	public partial class DeliveryDistanceRelations: RelationFactory
	{
		#region Custom Relation code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomRelationCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		


		/// <summary>Gets all relations and delete rules of the DeliveryDistanceEntity as a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects.</summary>
		/// <returns>a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects</returns>
		public virtual Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> GetRelationsAndDeleteRules()
		{
			Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> toReturn = new Dictionary<IEntityRelation, ReferentialConstraintDeleteRule>();
			return toReturn;
		}		
	
	

		/// <summary>Returns a new IEntityRelation object, between DeliveryDistanceEntity and ServiceMethodEntity over the m:1 relation they have, using the relation between the fields: DeliveryDistance.ServiceMethodId - ServiceMethod.ServiceMethodId</summary>
		public virtual IEntityRelation ServiceMethodEntityUsingServiceMethodId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "ServiceMethod", false, new[] { ServiceMethodFields.ServiceMethodId, DeliveryDistanceFields.ServiceMethodId }); }
		}

	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticDeliveryDistanceRelations
	{
		internal static readonly IEntityRelation ServiceMethodEntityUsingServiceMethodIdStatic = new DeliveryDistanceRelations().ServiceMethodEntityUsingServiceMethodId;

		/// <summary>CTor</summary>
		static StaticDeliveryDistanceRelations() { }
	}
}

