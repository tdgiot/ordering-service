﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 5.8
// Code is generated on: 
// Code is generated using templates: Crave.TemplateBindings
// Templates vendor: Crave Interactive
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.FactoryClasses;
using Crave.Ordering.Shared.Data.LLBLGen.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

// ReSharper disable All
namespace Crave.Ordering.Shared.Data.LLBLGen.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'ServiceMethodDeliverypointgroup'.<br/><br/></summary>
	[Serializable]
	public partial class ServiceMethodDeliverypointgroupEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END
	
	{
		private CompanyEntity _company;
		private DeliverypointgroupEntity _deliverypointgroup;
		private ServiceMethodEntity _serviceMethod;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		private static ServiceMethodDeliverypointgroupEntityStaticMetaData _staticMetaData = new ServiceMethodDeliverypointgroupEntityStaticMetaData();
		private static ServiceMethodDeliverypointgroupRelations _relationsFactory = new ServiceMethodDeliverypointgroupRelations();		

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Company</summary>
			public static readonly string Company = "Company";
			/// <summary>Member name Deliverypointgroup</summary>
			public static readonly string Deliverypointgroup = "Deliverypointgroup";
			/// <summary>Member name ServiceMethod</summary>
			public static readonly string ServiceMethod = "ServiceMethod";
		}

		/// <summary>Static meta-data storage for navigator related information</summary>
		protected class ServiceMethodDeliverypointgroupEntityStaticMetaData : EntityStaticMetaDataBase
		{
			public ServiceMethodDeliverypointgroupEntityStaticMetaData()
			{
				SetEntityCoreInfo("ServiceMethodDeliverypointgroupEntity", InheritanceHierarchyType.None, false, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ServiceMethodDeliverypointgroupEntity, typeof(ServiceMethodDeliverypointgroupEntity), typeof(ServiceMethodDeliverypointgroupEntityFactory), false);
				AddNavigatorMetaData<ServiceMethodDeliverypointgroupEntity, CompanyEntity>("Company", "ServiceMethodDeliverypointgroupCollection", (a, b) => a._company = b, a => a._company, (a, b) => a.Company = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticServiceMethodDeliverypointgroupRelations.CompanyEntityUsingParentCompanyIdStatic, ()=>new ServiceMethodDeliverypointgroupRelations().CompanyEntityUsingParentCompanyId, null, new int[] { (int)ServiceMethodDeliverypointgroupFieldIndex.ParentCompanyId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.CompanyEntity);
				AddNavigatorMetaData<ServiceMethodDeliverypointgroupEntity, DeliverypointgroupEntity>("Deliverypointgroup", "ServiceMethodDeliverypointgroupCollection", (a, b) => a._deliverypointgroup = b, a => a._deliverypointgroup, (a, b) => a.Deliverypointgroup = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticServiceMethodDeliverypointgroupRelations.DeliverypointgroupEntityUsingDeliverypointgroupIdStatic, ()=>new ServiceMethodDeliverypointgroupRelations().DeliverypointgroupEntityUsingDeliverypointgroupId, null, new int[] { (int)ServiceMethodDeliverypointgroupFieldIndex.DeliverypointgroupId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.DeliverypointgroupEntity);
				AddNavigatorMetaData<ServiceMethodDeliverypointgroupEntity, ServiceMethodEntity>("ServiceMethod", "ServiceMethodDeliverypointgroupCollection", (a, b) => a._serviceMethod = b, a => a._serviceMethod, (a, b) => a.ServiceMethod = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticServiceMethodDeliverypointgroupRelations.ServiceMethodEntityUsingServiceMethodIdStatic, ()=>new ServiceMethodDeliverypointgroupRelations().ServiceMethodEntityUsingServiceMethodId, null, new int[] { (int)ServiceMethodDeliverypointgroupFieldIndex.ServiceMethodId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ServiceMethodEntity);
			}
		}

		/// <summary>Static ctor</summary>
		static ServiceMethodDeliverypointgroupEntity()
		{
		}

		/// <summary> CTor</summary>
		public ServiceMethodDeliverypointgroupEntity()
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public ServiceMethodDeliverypointgroupEntity(IEntityFields2 fields)
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this ServiceMethodDeliverypointgroupEntity</param>
		public ServiceMethodDeliverypointgroupEntity(IValidator validator)
		{
			InitClassEmpty(validator, null);
		}

		/// <summary> CTor</summary>
		/// <param name="serviceMethodId">PK value for ServiceMethodDeliverypointgroup which data should be fetched into this ServiceMethodDeliverypointgroup object</param>
		/// <param name="deliverypointgroupId">PK value for ServiceMethodDeliverypointgroup which data should be fetched into this ServiceMethodDeliverypointgroup object</param>
		public ServiceMethodDeliverypointgroupEntity(System.Int32 serviceMethodId, System.Int32 deliverypointgroupId) : this(serviceMethodId, deliverypointgroupId, null)
		{
		}

		/// <summary> CTor</summary>
		/// <param name="serviceMethodId">PK value for ServiceMethodDeliverypointgroup which data should be fetched into this ServiceMethodDeliverypointgroup object</param>
		/// <param name="deliverypointgroupId">PK value for ServiceMethodDeliverypointgroup which data should be fetched into this ServiceMethodDeliverypointgroup object</param>
		/// <param name="validator">The custom validator object for this ServiceMethodDeliverypointgroupEntity</param>
		public ServiceMethodDeliverypointgroupEntity(System.Int32 serviceMethodId, System.Int32 deliverypointgroupId, IValidator validator)
		{
			InitClassEmpty(validator, null);
			this.ServiceMethodId = serviceMethodId;
			this.DeliverypointgroupId = deliverypointgroupId;
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ServiceMethodDeliverypointgroupEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Company' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoCompany() { return CreateRelationInfoForNavigator("Company"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Deliverypointgroup' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoDeliverypointgroup() { return CreateRelationInfoForNavigator("Deliverypointgroup"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'ServiceMethod' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoServiceMethod() { return CreateRelationInfoForNavigator("ServiceMethod"); }
		
		/// <inheritdoc/>
		protected override EntityStaticMetaDataBase GetEntityStaticMetaData() {	return _staticMetaData; }

		/// <summary>Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		/// <summary>Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this ServiceMethodDeliverypointgroupEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary>The relations object holding all relations of this entity with other entity classes.</summary>
		public static ServiceMethodDeliverypointgroupRelations Relations { get { return _relationsFactory; } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Company' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathCompanyEntity { get { return _staticMetaData.GetPrefetchPathElement("Company", CommonEntityBase.CreateEntityCollection<CompanyEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Deliverypointgroup' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathDeliverypointgroupEntity { get { return _staticMetaData.GetPrefetchPathElement("Deliverypointgroup", CommonEntityBase.CreateEntityCollection<DeliverypointgroupEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'ServiceMethod' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathServiceMethodEntity { get { return _staticMetaData.GetPrefetchPathElement("ServiceMethod", CommonEntityBase.CreateEntityCollection<ServiceMethodEntity>()); } }

		/// <summary>The ServiceMethodId property of the Entity ServiceMethodDeliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ServiceMethodDeliverypointgroup"."ServiceMethodId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int32 ServiceMethodId
		{
			get { return (System.Int32)GetValue((int)ServiceMethodDeliverypointgroupFieldIndex.ServiceMethodId, true); }
			set	{ SetValue((int)ServiceMethodDeliverypointgroupFieldIndex.ServiceMethodId, value); }
		}

		/// <summary>The DeliverypointgroupId property of the Entity ServiceMethodDeliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ServiceMethodDeliverypointgroup"."DeliverypointgroupId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int32 DeliverypointgroupId
		{
			get { return (System.Int32)GetValue((int)ServiceMethodDeliverypointgroupFieldIndex.DeliverypointgroupId, true); }
			set	{ SetValue((int)ServiceMethodDeliverypointgroupFieldIndex.DeliverypointgroupId, value); }
		}

		/// <summary>The ParentCompanyId property of the Entity ServiceMethodDeliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ServiceMethodDeliverypointgroup"."ParentCompanyId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ParentCompanyId
		{
			get { return (System.Int32)GetValue((int)ServiceMethodDeliverypointgroupFieldIndex.ParentCompanyId, true); }
			set	{ SetValue((int)ServiceMethodDeliverypointgroupFieldIndex.ParentCompanyId, value); }
		}

		/// <summary>The CreatedUTC property of the Entity ServiceMethodDeliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ServiceMethodDeliverypointgroup"."CreatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ServiceMethodDeliverypointgroupFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)ServiceMethodDeliverypointgroupFieldIndex.CreatedUTC, value); }
		}

		/// <summary>The CreatedBy property of the Entity ServiceMethodDeliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ServiceMethodDeliverypointgroup"."CreatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)ServiceMethodDeliverypointgroupFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)ServiceMethodDeliverypointgroupFieldIndex.CreatedBy, value); }
		}

		/// <summary>The UpdatedUTC property of the Entity ServiceMethodDeliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ServiceMethodDeliverypointgroup"."UpdatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ServiceMethodDeliverypointgroupFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)ServiceMethodDeliverypointgroupFieldIndex.UpdatedUTC, value); }
		}

		/// <summary>The UpdatedBy property of the Entity ServiceMethodDeliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ServiceMethodDeliverypointgroup"."UpdatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)ServiceMethodDeliverypointgroupFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)ServiceMethodDeliverypointgroupFieldIndex.UpdatedBy, value); }
		}

		/// <summary>Gets / sets related entity of type 'CompanyEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual CompanyEntity Company
		{
			get { return _company; }
			set { SetSingleRelatedEntityNavigator(value, "Company"); }
		}

		/// <summary>Gets / sets related entity of type 'DeliverypointgroupEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual DeliverypointgroupEntity Deliverypointgroup
		{
			get { return _deliverypointgroup; }
			set { SetSingleRelatedEntityNavigator(value, "Deliverypointgroup"); }
		}

		/// <summary>Gets / sets related entity of type 'ServiceMethodEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual ServiceMethodEntity ServiceMethod
		{
			get { return _serviceMethod; }
			set { SetSingleRelatedEntityNavigator(value, "ServiceMethod"); }
		}


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen
{
	public enum ServiceMethodDeliverypointgroupFieldIndex
	{
		///<summary>ServiceMethodId. </summary>
		ServiceMethodId,
		///<summary>DeliverypointgroupId. </summary>
		DeliverypointgroupId,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		/// <summary></summary>
		AmountOfFields
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: ServiceMethodDeliverypointgroup. </summary>
	public partial class ServiceMethodDeliverypointgroupRelations: RelationFactory
	{
		#region Custom Relation code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomRelationCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		


		/// <summary>Gets all relations and delete rules of the ServiceMethodDeliverypointgroupEntity as a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects.</summary>
		/// <returns>a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects</returns>
		public virtual Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> GetRelationsAndDeleteRules()
		{
			Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> toReturn = new Dictionary<IEntityRelation, ReferentialConstraintDeleteRule>();
			return toReturn;
		}		
	
	

		/// <summary>Returns a new IEntityRelation object, between ServiceMethodDeliverypointgroupEntity and CompanyEntity over the m:1 relation they have, using the relation between the fields: ServiceMethodDeliverypointgroup.ParentCompanyId - Company.CompanyId</summary>
		public virtual IEntityRelation CompanyEntityUsingParentCompanyId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Company", false, new[] { CompanyFields.CompanyId, ServiceMethodDeliverypointgroupFields.ParentCompanyId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between ServiceMethodDeliverypointgroupEntity and DeliverypointgroupEntity over the m:1 relation they have, using the relation between the fields: ServiceMethodDeliverypointgroup.DeliverypointgroupId - Deliverypointgroup.DeliverypointgroupId</summary>
		public virtual IEntityRelation DeliverypointgroupEntityUsingDeliverypointgroupId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Deliverypointgroup", false, new[] { DeliverypointgroupFields.DeliverypointgroupId, ServiceMethodDeliverypointgroupFields.DeliverypointgroupId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between ServiceMethodDeliverypointgroupEntity and ServiceMethodEntity over the m:1 relation they have, using the relation between the fields: ServiceMethodDeliverypointgroup.ServiceMethodId - ServiceMethod.ServiceMethodId</summary>
		public virtual IEntityRelation ServiceMethodEntityUsingServiceMethodId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "ServiceMethod", false, new[] { ServiceMethodFields.ServiceMethodId, ServiceMethodDeliverypointgroupFields.ServiceMethodId }); }
		}

	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticServiceMethodDeliverypointgroupRelations
	{
		internal static readonly IEntityRelation CompanyEntityUsingParentCompanyIdStatic = new ServiceMethodDeliverypointgroupRelations().CompanyEntityUsingParentCompanyId;
		internal static readonly IEntityRelation DeliverypointgroupEntityUsingDeliverypointgroupIdStatic = new ServiceMethodDeliverypointgroupRelations().DeliverypointgroupEntityUsingDeliverypointgroupId;
		internal static readonly IEntityRelation ServiceMethodEntityUsingServiceMethodIdStatic = new ServiceMethodDeliverypointgroupRelations().ServiceMethodEntityUsingServiceMethodId;

		/// <summary>CTor</summary>
		static StaticServiceMethodDeliverypointgroupRelations() { }
	}
}

