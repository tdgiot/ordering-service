﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 5.8
// Code is generated on: 
// Code is generated using templates: Crave.TemplateBindings
// Templates vendor: Crave Interactive
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.FactoryClasses;
using Crave.Ordering.Shared.Data.LLBLGen.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

// ReSharper disable All
namespace Crave.Ordering.Shared.Data.LLBLGen.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'TaxTariff'.<br/><br/></summary>
	[Serializable]
	public partial class TaxTariffEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END
	
	{
		private EntityCollection<AlterationoptionEntity> _alterationoptionCollection;
		private EntityCollection<OrderitemEntity> _orderitemCollection;
		private EntityCollection<OrderitemAlterationitemEntity> _orderitemAlterationitemCollection;
		private EntityCollection<ProductEntity> _productCollection;
		private CompanyEntity _company;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		private static TaxTariffEntityStaticMetaData _staticMetaData = new TaxTariffEntityStaticMetaData();
		private static TaxTariffRelations _relationsFactory = new TaxTariffRelations();		

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Company</summary>
			public static readonly string Company = "Company";
			/// <summary>Member name AlterationoptionCollection</summary>
			public static readonly string AlterationoptionCollection = "AlterationoptionCollection";
			/// <summary>Member name OrderitemCollection</summary>
			public static readonly string OrderitemCollection = "OrderitemCollection";
			/// <summary>Member name OrderitemAlterationitemCollection</summary>
			public static readonly string OrderitemAlterationitemCollection = "OrderitemAlterationitemCollection";
			/// <summary>Member name ProductCollection</summary>
			public static readonly string ProductCollection = "ProductCollection";
		}

		/// <summary>Static meta-data storage for navigator related information</summary>
		protected class TaxTariffEntityStaticMetaData : EntityStaticMetaDataBase
		{
			public TaxTariffEntityStaticMetaData()
			{
				SetEntityCoreInfo("TaxTariffEntity", InheritanceHierarchyType.None, false, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.TaxTariffEntity, typeof(TaxTariffEntity), typeof(TaxTariffEntityFactory), false);
				AddNavigatorMetaData<TaxTariffEntity, EntityCollection<AlterationoptionEntity>>("AlterationoptionCollection", a => a._alterationoptionCollection, (a, b) => a._alterationoptionCollection = b, a => a.AlterationoptionCollection, () => new TaxTariffRelations().AlterationoptionEntityUsingTaxTariffId, typeof(AlterationoptionEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.AlterationoptionEntity);
				AddNavigatorMetaData<TaxTariffEntity, EntityCollection<OrderitemEntity>>("OrderitemCollection", a => a._orderitemCollection, (a, b) => a._orderitemCollection = b, a => a.OrderitemCollection, () => new TaxTariffRelations().OrderitemEntityUsingTaxTariffId, typeof(OrderitemEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.OrderitemEntity);
				AddNavigatorMetaData<TaxTariffEntity, EntityCollection<OrderitemAlterationitemEntity>>("OrderitemAlterationitemCollection", a => a._orderitemAlterationitemCollection, (a, b) => a._orderitemAlterationitemCollection = b, a => a.OrderitemAlterationitemCollection, () => new TaxTariffRelations().OrderitemAlterationitemEntityUsingTaxTariffId, typeof(OrderitemAlterationitemEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.OrderitemAlterationitemEntity);
				AddNavigatorMetaData<TaxTariffEntity, EntityCollection<ProductEntity>>("ProductCollection", a => a._productCollection, (a, b) => a._productCollection = b, a => a.ProductCollection, () => new TaxTariffRelations().ProductEntityUsingTaxTariffId, typeof(ProductEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ProductEntity);
				AddNavigatorMetaData<TaxTariffEntity, CompanyEntity>("Company", "TaxTariffCollection", (a, b) => a._company = b, a => a._company, (a, b) => a.Company = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticTaxTariffRelations.CompanyEntityUsingCompanyIdStatic, ()=>new TaxTariffRelations().CompanyEntityUsingCompanyId, null, new int[] { (int)TaxTariffFieldIndex.CompanyId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.CompanyEntity);
			}
		}

		/// <summary>Static ctor</summary>
		static TaxTariffEntity()
		{
		}

		/// <summary> CTor</summary>
		public TaxTariffEntity()
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public TaxTariffEntity(IEntityFields2 fields)
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this TaxTariffEntity</param>
		public TaxTariffEntity(IValidator validator)
		{
			InitClassEmpty(validator, null);
		}

		/// <summary> CTor</summary>
		/// <param name="taxTariffId">PK value for TaxTariff which data should be fetched into this TaxTariff object</param>
		public TaxTariffEntity(System.Int32 taxTariffId) : this(taxTariffId, null)
		{
		}

		/// <summary> CTor</summary>
		/// <param name="taxTariffId">PK value for TaxTariff which data should be fetched into this TaxTariff object</param>
		/// <param name="validator">The custom validator object for this TaxTariffEntity</param>
		public TaxTariffEntity(System.Int32 taxTariffId, IValidator validator)
		{
			InitClassEmpty(validator, null);
			this.TaxTariffId = taxTariffId;
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected TaxTariffEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Alterationoption' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoAlterationoptionCollection() { return CreateRelationInfoForNavigator("AlterationoptionCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Orderitem' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoOrderitemCollection() { return CreateRelationInfoForNavigator("OrderitemCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'OrderitemAlterationitem' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoOrderitemAlterationitemCollection() { return CreateRelationInfoForNavigator("OrderitemAlterationitemCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Product' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoProductCollection() { return CreateRelationInfoForNavigator("ProductCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Company' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoCompany() { return CreateRelationInfoForNavigator("Company"); }
		
		/// <inheritdoc/>
		protected override EntityStaticMetaDataBase GetEntityStaticMetaData() {	return _staticMetaData; }

		/// <summary>Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		/// <summary>Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this TaxTariffEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary>The relations object holding all relations of this entity with other entity classes.</summary>
		public static TaxTariffRelations Relations { get { return _relationsFactory; } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Alterationoption' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathAlterationoptionCollection { get { return _staticMetaData.GetPrefetchPathElement("AlterationoptionCollection", CommonEntityBase.CreateEntityCollection<AlterationoptionEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Orderitem' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathOrderitemCollection { get { return _staticMetaData.GetPrefetchPathElement("OrderitemCollection", CommonEntityBase.CreateEntityCollection<OrderitemEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'OrderitemAlterationitem' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathOrderitemAlterationitemCollection { get { return _staticMetaData.GetPrefetchPathElement("OrderitemAlterationitemCollection", CommonEntityBase.CreateEntityCollection<OrderitemAlterationitemEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Product' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathProductCollection { get { return _staticMetaData.GetPrefetchPathElement("ProductCollection", CommonEntityBase.CreateEntityCollection<ProductEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Company' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathCompanyEntity { get { return _staticMetaData.GetPrefetchPathElement("Company", CommonEntityBase.CreateEntityCollection<CompanyEntity>()); } }

		/// <summary>The TaxTariffId property of the Entity TaxTariff<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TaxTariff"."TaxTariffId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 TaxTariffId
		{
			get { return (System.Int32)GetValue((int)TaxTariffFieldIndex.TaxTariffId, true); }
			set { SetValue((int)TaxTariffFieldIndex.TaxTariffId, value); }		}

		/// <summary>The CompanyId property of the Entity TaxTariff<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TaxTariff"."CompanyId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CompanyId
		{
			get { return (System.Int32)GetValue((int)TaxTariffFieldIndex.CompanyId, true); }
			set	{ SetValue((int)TaxTariffFieldIndex.CompanyId, value); }
		}

		/// <summary>The Name property of the Entity TaxTariff<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TaxTariff"."Name".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 100.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)TaxTariffFieldIndex.Name, true); }
			set	{ SetValue((int)TaxTariffFieldIndex.Name, value); }
		}

		/// <summary>The Percentage property of the Entity TaxTariff<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TaxTariff"."Percentage".<br/>Table field type characteristics (type, precision, scale, length): Float, 38, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Double Percentage
		{
			get { return (System.Double)GetValue((int)TaxTariffFieldIndex.Percentage, true); }
			set	{ SetValue((int)TaxTariffFieldIndex.Percentage, value); }
		}

		/// <summary>The CreatedBy property of the Entity TaxTariff<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TaxTariff"."CreatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)TaxTariffFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)TaxTariffFieldIndex.CreatedBy, value); }
		}

		/// <summary>The UpdatedBy property of the Entity TaxTariff<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TaxTariff"."UpdatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)TaxTariffFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)TaxTariffFieldIndex.UpdatedBy, value); }
		}

		/// <summary>The CreatedUTC property of the Entity TaxTariff<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TaxTariff"."CreatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TaxTariffFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)TaxTariffFieldIndex.CreatedUTC, value); }
		}

		/// <summary>The UpdatedUTC property of the Entity TaxTariff<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TaxTariff"."UpdatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TaxTariffFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)TaxTariffFieldIndex.UpdatedUTC, value); }
		}

		/// <summary>Gets the EntityCollection with the related entities of type 'AlterationoptionEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(AlterationoptionEntity))]
		public virtual EntityCollection<AlterationoptionEntity> AlterationoptionCollection { get { return GetOrCreateEntityCollection<AlterationoptionEntity, AlterationoptionEntityFactory>("TaxTariff", true, false, ref _alterationoptionCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'OrderitemEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(OrderitemEntity))]
		public virtual EntityCollection<OrderitemEntity> OrderitemCollection { get { return GetOrCreateEntityCollection<OrderitemEntity, OrderitemEntityFactory>("TaxTariff", true, false, ref _orderitemCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'OrderitemAlterationitemEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(OrderitemAlterationitemEntity))]
		public virtual EntityCollection<OrderitemAlterationitemEntity> OrderitemAlterationitemCollection { get { return GetOrCreateEntityCollection<OrderitemAlterationitemEntity, OrderitemAlterationitemEntityFactory>("TaxTariff", true, false, ref _orderitemAlterationitemCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'ProductEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(ProductEntity))]
		public virtual EntityCollection<ProductEntity> ProductCollection { get { return GetOrCreateEntityCollection<ProductEntity, ProductEntityFactory>("TaxTariff", true, false, ref _productCollection); } }

		/// <summary>Gets / sets related entity of type 'CompanyEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual CompanyEntity Company
		{
			get { return _company; }
			set { SetSingleRelatedEntityNavigator(value, "Company"); }
		}


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen
{
	public enum TaxTariffFieldIndex
	{
		///<summary>TaxTariffId. </summary>
		TaxTariffId,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>Name. </summary>
		Name,
		///<summary>Percentage. </summary>
		Percentage,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: TaxTariff. </summary>
	public partial class TaxTariffRelations: RelationFactory
	{
		#region Custom Relation code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomRelationCode
        private const ReferentialConstraintDeleteRule DeleteRuleForAlterationoptionEntityUsingTaxTariffId = ReferentialConstraintDeleteRule.NoAction;
        private const ReferentialConstraintDeleteRule DeleteRuleForOrderitemEntityUsingTaxTariffId = ReferentialConstraintDeleteRule.SetNull;
		private const ReferentialConstraintDeleteRule DeleteRuleForOrderitemAlterationitemEntityUsingTaxTariffId = ReferentialConstraintDeleteRule.SetNull;
		private const ReferentialConstraintDeleteRule DeleteRuleForProductEntityUsingTaxTariffId = ReferentialConstraintDeleteRule.NoAction;
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		
		// private const ReferentialConstraintDeleteRule DeleteRuleForAlterationoptionEntityUsingTaxTariffId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForOrderitemEntityUsingTaxTariffId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForOrderitemAlterationitemEntityUsingTaxTariffId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForProductEntityUsingTaxTariffId = ReferentialConstraintDeleteRule


		/// <summary>Gets all relations and delete rules of the TaxTariffEntity as a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects.</summary>
		/// <returns>a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects</returns>
		public virtual Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> GetRelationsAndDeleteRules()
		{
			Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> toReturn = new Dictionary<IEntityRelation, ReferentialConstraintDeleteRule>();
			toReturn.Add(this.AlterationoptionEntityUsingTaxTariffId, TaxTariffRelations.DeleteRuleForAlterationoptionEntityUsingTaxTariffId);
			toReturn.Add(this.OrderitemEntityUsingTaxTariffId, TaxTariffRelations.DeleteRuleForOrderitemEntityUsingTaxTariffId);
			toReturn.Add(this.OrderitemAlterationitemEntityUsingTaxTariffId, TaxTariffRelations.DeleteRuleForOrderitemAlterationitemEntityUsingTaxTariffId);
			toReturn.Add(this.ProductEntityUsingTaxTariffId, TaxTariffRelations.DeleteRuleForProductEntityUsingTaxTariffId);
			return toReturn;
		}		
	
	
		/// <summary>Returns a new IEntityRelation object, between TaxTariffEntity and AlterationoptionEntity over the 1:n relation they have, using the relation between the fields: TaxTariff.TaxTariffId - Alterationoption.TaxTariffId</summary>
		public virtual IEntityRelation AlterationoptionEntityUsingTaxTariffId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "AlterationoptionCollection", true, new[] { TaxTariffFields.TaxTariffId, AlterationoptionFields.TaxTariffId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between TaxTariffEntity and OrderitemEntity over the 1:n relation they have, using the relation between the fields: TaxTariff.TaxTariffId - Orderitem.TaxTariffId</summary>
		public virtual IEntityRelation OrderitemEntityUsingTaxTariffId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "OrderitemCollection", true, new[] { TaxTariffFields.TaxTariffId, OrderitemFields.TaxTariffId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between TaxTariffEntity and OrderitemAlterationitemEntity over the 1:n relation they have, using the relation between the fields: TaxTariff.TaxTariffId - OrderitemAlterationitem.TaxTariffId</summary>
		public virtual IEntityRelation OrderitemAlterationitemEntityUsingTaxTariffId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "OrderitemAlterationitemCollection", true, new[] { TaxTariffFields.TaxTariffId, OrderitemAlterationitemFields.TaxTariffId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between TaxTariffEntity and ProductEntity over the 1:n relation they have, using the relation between the fields: TaxTariff.TaxTariffId - Product.TaxTariffId</summary>
		public virtual IEntityRelation ProductEntityUsingTaxTariffId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "ProductCollection", true, new[] { TaxTariffFields.TaxTariffId, ProductFields.TaxTariffId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between TaxTariffEntity and CompanyEntity over the m:1 relation they have, using the relation between the fields: TaxTariff.CompanyId - Company.CompanyId</summary>
		public virtual IEntityRelation CompanyEntityUsingCompanyId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Company", false, new[] { CompanyFields.CompanyId, TaxTariffFields.CompanyId }); }
		}

	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticTaxTariffRelations
	{
		internal static readonly IEntityRelation AlterationoptionEntityUsingTaxTariffIdStatic = new TaxTariffRelations().AlterationoptionEntityUsingTaxTariffId;
		internal static readonly IEntityRelation OrderitemEntityUsingTaxTariffIdStatic = new TaxTariffRelations().OrderitemEntityUsingTaxTariffId;
		internal static readonly IEntityRelation OrderitemAlterationitemEntityUsingTaxTariffIdStatic = new TaxTariffRelations().OrderitemAlterationitemEntityUsingTaxTariffId;
		internal static readonly IEntityRelation ProductEntityUsingTaxTariffIdStatic = new TaxTariffRelations().ProductEntityUsingTaxTariffId;
		internal static readonly IEntityRelation CompanyEntityUsingCompanyIdStatic = new TaxTariffRelations().CompanyEntityUsingCompanyId;

		/// <summary>CTor</summary>
		static StaticTaxTariffRelations() { }
	}
}

