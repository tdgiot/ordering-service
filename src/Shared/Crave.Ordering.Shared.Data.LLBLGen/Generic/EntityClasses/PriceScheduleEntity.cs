﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 5.8
// Code is generated on: 
// Code is generated using templates: Crave.TemplateBindings
// Templates vendor: Crave Interactive
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.FactoryClasses;
using Crave.Ordering.Shared.Data.LLBLGen.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

// ReSharper disable All
namespace Crave.Ordering.Shared.Data.LLBLGen.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'PriceSchedule'.<br/><br/></summary>
	[Serializable]
	public partial class PriceScheduleEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END
	
	{
		private EntityCollection<ClientConfigurationEntity> _clientConfigurationCollection;
		private EntityCollection<DeliverypointgroupEntity> _deliverypointgroupCollection;
		private EntityCollection<PriceScheduleItemEntity> _priceScheduleItemCollection;
		private CompanyEntity _company;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		private static PriceScheduleEntityStaticMetaData _staticMetaData = new PriceScheduleEntityStaticMetaData();
		private static PriceScheduleRelations _relationsFactory = new PriceScheduleRelations();		

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Company</summary>
			public static readonly string Company = "Company";
			/// <summary>Member name ClientConfigurationCollection</summary>
			public static readonly string ClientConfigurationCollection = "ClientConfigurationCollection";
			/// <summary>Member name DeliverypointgroupCollection</summary>
			public static readonly string DeliverypointgroupCollection = "DeliverypointgroupCollection";
			/// <summary>Member name PriceScheduleItemCollection</summary>
			public static readonly string PriceScheduleItemCollection = "PriceScheduleItemCollection";
		}

		/// <summary>Static meta-data storage for navigator related information</summary>
		protected class PriceScheduleEntityStaticMetaData : EntityStaticMetaDataBase
		{
			public PriceScheduleEntityStaticMetaData()
			{
				SetEntityCoreInfo("PriceScheduleEntity", InheritanceHierarchyType.None, false, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.PriceScheduleEntity, typeof(PriceScheduleEntity), typeof(PriceScheduleEntityFactory), false);
				AddNavigatorMetaData<PriceScheduleEntity, EntityCollection<ClientConfigurationEntity>>("ClientConfigurationCollection", a => a._clientConfigurationCollection, (a, b) => a._clientConfigurationCollection = b, a => a.ClientConfigurationCollection, () => new PriceScheduleRelations().ClientConfigurationEntityUsingPriceScheduleId, typeof(ClientConfigurationEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ClientConfigurationEntity);
				AddNavigatorMetaData<PriceScheduleEntity, EntityCollection<DeliverypointgroupEntity>>("DeliverypointgroupCollection", a => a._deliverypointgroupCollection, (a, b) => a._deliverypointgroupCollection = b, a => a.DeliverypointgroupCollection, () => new PriceScheduleRelations().DeliverypointgroupEntityUsingPriceScheduleId, typeof(DeliverypointgroupEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.DeliverypointgroupEntity);
				AddNavigatorMetaData<PriceScheduleEntity, EntityCollection<PriceScheduleItemEntity>>("PriceScheduleItemCollection", a => a._priceScheduleItemCollection, (a, b) => a._priceScheduleItemCollection = b, a => a.PriceScheduleItemCollection, () => new PriceScheduleRelations().PriceScheduleItemEntityUsingPriceScheduleId, typeof(PriceScheduleItemEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.PriceScheduleItemEntity);
				AddNavigatorMetaData<PriceScheduleEntity, CompanyEntity>("Company", "PriceScheduleCollection", (a, b) => a._company = b, a => a._company, (a, b) => a.Company = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticPriceScheduleRelations.CompanyEntityUsingCompanyIdStatic, ()=>new PriceScheduleRelations().CompanyEntityUsingCompanyId, null, new int[] { (int)PriceScheduleFieldIndex.CompanyId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.CompanyEntity);
			}
		}

		/// <summary>Static ctor</summary>
		static PriceScheduleEntity()
		{
		}

		/// <summary> CTor</summary>
		public PriceScheduleEntity()
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public PriceScheduleEntity(IEntityFields2 fields)
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this PriceScheduleEntity</param>
		public PriceScheduleEntity(IValidator validator)
		{
			InitClassEmpty(validator, null);
		}

		/// <summary> CTor</summary>
		/// <param name="priceScheduleId">PK value for PriceSchedule which data should be fetched into this PriceSchedule object</param>
		public PriceScheduleEntity(System.Int32 priceScheduleId) : this(priceScheduleId, null)
		{
		}

		/// <summary> CTor</summary>
		/// <param name="priceScheduleId">PK value for PriceSchedule which data should be fetched into this PriceSchedule object</param>
		/// <param name="validator">The custom validator object for this PriceScheduleEntity</param>
		public PriceScheduleEntity(System.Int32 priceScheduleId, IValidator validator)
		{
			InitClassEmpty(validator, null);
			this.PriceScheduleId = priceScheduleId;
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected PriceScheduleEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'ClientConfiguration' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoClientConfigurationCollection() { return CreateRelationInfoForNavigator("ClientConfigurationCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Deliverypointgroup' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoDeliverypointgroupCollection() { return CreateRelationInfoForNavigator("DeliverypointgroupCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'PriceScheduleItem' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoPriceScheduleItemCollection() { return CreateRelationInfoForNavigator("PriceScheduleItemCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Company' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoCompany() { return CreateRelationInfoForNavigator("Company"); }
		
		/// <inheritdoc/>
		protected override EntityStaticMetaDataBase GetEntityStaticMetaData() {	return _staticMetaData; }

		/// <summary>Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		/// <summary>Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this PriceScheduleEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary>The relations object holding all relations of this entity with other entity classes.</summary>
		public static PriceScheduleRelations Relations { get { return _relationsFactory; } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'ClientConfiguration' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathClientConfigurationCollection { get { return _staticMetaData.GetPrefetchPathElement("ClientConfigurationCollection", CommonEntityBase.CreateEntityCollection<ClientConfigurationEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Deliverypointgroup' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathDeliverypointgroupCollection { get { return _staticMetaData.GetPrefetchPathElement("DeliverypointgroupCollection", CommonEntityBase.CreateEntityCollection<DeliverypointgroupEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'PriceScheduleItem' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathPriceScheduleItemCollection { get { return _staticMetaData.GetPrefetchPathElement("PriceScheduleItemCollection", CommonEntityBase.CreateEntityCollection<PriceScheduleItemEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Company' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathCompanyEntity { get { return _staticMetaData.GetPrefetchPathElement("Company", CommonEntityBase.CreateEntityCollection<CompanyEntity>()); } }

		/// <summary>The PriceScheduleId property of the Entity PriceSchedule<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceSchedule"."PriceScheduleId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 PriceScheduleId
		{
			get { return (System.Int32)GetValue((int)PriceScheduleFieldIndex.PriceScheduleId, true); }
			set { SetValue((int)PriceScheduleFieldIndex.PriceScheduleId, value); }		}

		/// <summary>The CompanyId property of the Entity PriceSchedule<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceSchedule"."CompanyId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CompanyId
		{
			get { return (System.Int32)GetValue((int)PriceScheduleFieldIndex.CompanyId, true); }
			set	{ SetValue((int)PriceScheduleFieldIndex.CompanyId, value); }
		}

		/// <summary>The Name property of the Entity PriceSchedule<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceSchedule"."Name".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)PriceScheduleFieldIndex.Name, true); }
			set	{ SetValue((int)PriceScheduleFieldIndex.Name, value); }
		}

		/// <summary>The CreatedUTC property of the Entity PriceSchedule<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceSchedule"."CreatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)PriceScheduleFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)PriceScheduleFieldIndex.CreatedUTC, value); }
		}

		/// <summary>The CreatedBy property of the Entity PriceSchedule<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceSchedule"."CreatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CreatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)PriceScheduleFieldIndex.CreatedBy, false); }
			set	{ SetValue((int)PriceScheduleFieldIndex.CreatedBy, value); }
		}

		/// <summary>The UpdatedUTC property of the Entity PriceSchedule<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceSchedule"."UpdatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)PriceScheduleFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)PriceScheduleFieldIndex.UpdatedUTC, value); }
		}

		/// <summary>The UpdatedBy property of the Entity PriceSchedule<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceSchedule"."UpdatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UpdatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)PriceScheduleFieldIndex.UpdatedBy, false); }
			set	{ SetValue((int)PriceScheduleFieldIndex.UpdatedBy, value); }
		}

		/// <summary>Gets the EntityCollection with the related entities of type 'ClientConfigurationEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(ClientConfigurationEntity))]
		public virtual EntityCollection<ClientConfigurationEntity> ClientConfigurationCollection { get { return GetOrCreateEntityCollection<ClientConfigurationEntity, ClientConfigurationEntityFactory>("PriceSchedule", true, false, ref _clientConfigurationCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'DeliverypointgroupEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(DeliverypointgroupEntity))]
		public virtual EntityCollection<DeliverypointgroupEntity> DeliverypointgroupCollection { get { return GetOrCreateEntityCollection<DeliverypointgroupEntity, DeliverypointgroupEntityFactory>("PriceSchedule", true, false, ref _deliverypointgroupCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'PriceScheduleItemEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(PriceScheduleItemEntity))]
		public virtual EntityCollection<PriceScheduleItemEntity> PriceScheduleItemCollection { get { return GetOrCreateEntityCollection<PriceScheduleItemEntity, PriceScheduleItemEntityFactory>("PriceSchedule", true, false, ref _priceScheduleItemCollection); } }

		/// <summary>Gets / sets related entity of type 'CompanyEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual CompanyEntity Company
		{
			get { return _company; }
			set { SetSingleRelatedEntityNavigator(value, "Company"); }
		}


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen
{
	public enum PriceScheduleFieldIndex
	{
		///<summary>PriceScheduleId. </summary>
		PriceScheduleId,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>Name. </summary>
		Name,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		/// <summary></summary>
		AmountOfFields
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: PriceSchedule. </summary>
	public partial class PriceScheduleRelations: RelationFactory
	{
		#region Custom Relation code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomRelationCode
        private const ReferentialConstraintDeleteRule DeleteRuleForClientConfigurationEntityUsingPriceScheduleId = ReferentialConstraintDeleteRule.NoAction;
        private const ReferentialConstraintDeleteRule DeleteRuleForDeliverypointgroupEntityUsingPriceScheduleId = ReferentialConstraintDeleteRule.NoAction;
        private const ReferentialConstraintDeleteRule DeleteRuleForPriceScheduleItemEntityUsingPriceScheduleId = ReferentialConstraintDeleteRule.Cascade;
        private const ReferentialConstraintDeleteRule DeleteRuleForTimestampEntityUsingPriceScheduleId = ReferentialConstraintDeleteRule.Cascade;
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		
		// private const ReferentialConstraintDeleteRule DeleteRuleForClientConfigurationEntityUsingPriceScheduleId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForDeliverypointgroupEntityUsingPriceScheduleId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForPriceScheduleItemEntityUsingPriceScheduleId = ReferentialConstraintDeleteRule


		/// <summary>Gets all relations and delete rules of the PriceScheduleEntity as a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects.</summary>
		/// <returns>a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects</returns>
		public virtual Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> GetRelationsAndDeleteRules()
		{
			Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> toReturn = new Dictionary<IEntityRelation, ReferentialConstraintDeleteRule>();
			toReturn.Add(this.ClientConfigurationEntityUsingPriceScheduleId, PriceScheduleRelations.DeleteRuleForClientConfigurationEntityUsingPriceScheduleId);
			toReturn.Add(this.DeliverypointgroupEntityUsingPriceScheduleId, PriceScheduleRelations.DeleteRuleForDeliverypointgroupEntityUsingPriceScheduleId);
			toReturn.Add(this.PriceScheduleItemEntityUsingPriceScheduleId, PriceScheduleRelations.DeleteRuleForPriceScheduleItemEntityUsingPriceScheduleId);
			return toReturn;
		}		
	
	
		/// <summary>Returns a new IEntityRelation object, between PriceScheduleEntity and ClientConfigurationEntity over the 1:n relation they have, using the relation between the fields: PriceSchedule.PriceScheduleId - ClientConfiguration.PriceScheduleId</summary>
		public virtual IEntityRelation ClientConfigurationEntityUsingPriceScheduleId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "ClientConfigurationCollection", true, new[] { PriceScheduleFields.PriceScheduleId, ClientConfigurationFields.PriceScheduleId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between PriceScheduleEntity and DeliverypointgroupEntity over the 1:n relation they have, using the relation between the fields: PriceSchedule.PriceScheduleId - Deliverypointgroup.PriceScheduleId</summary>
		public virtual IEntityRelation DeliverypointgroupEntityUsingPriceScheduleId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "DeliverypointgroupCollection", true, new[] { PriceScheduleFields.PriceScheduleId, DeliverypointgroupFields.PriceScheduleId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between PriceScheduleEntity and PriceScheduleItemEntity over the 1:n relation they have, using the relation between the fields: PriceSchedule.PriceScheduleId - PriceScheduleItem.PriceScheduleId</summary>
		public virtual IEntityRelation PriceScheduleItemEntityUsingPriceScheduleId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "PriceScheduleItemCollection", true, new[] { PriceScheduleFields.PriceScheduleId, PriceScheduleItemFields.PriceScheduleId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between PriceScheduleEntity and CompanyEntity over the m:1 relation they have, using the relation between the fields: PriceSchedule.CompanyId - Company.CompanyId</summary>
		public virtual IEntityRelation CompanyEntityUsingCompanyId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Company", false, new[] { CompanyFields.CompanyId, PriceScheduleFields.CompanyId }); }
		}

	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticPriceScheduleRelations
	{
		internal static readonly IEntityRelation ClientConfigurationEntityUsingPriceScheduleIdStatic = new PriceScheduleRelations().ClientConfigurationEntityUsingPriceScheduleId;
		internal static readonly IEntityRelation DeliverypointgroupEntityUsingPriceScheduleIdStatic = new PriceScheduleRelations().DeliverypointgroupEntityUsingPriceScheduleId;
		internal static readonly IEntityRelation PriceScheduleItemEntityUsingPriceScheduleIdStatic = new PriceScheduleRelations().PriceScheduleItemEntityUsingPriceScheduleId;
		internal static readonly IEntityRelation CompanyEntityUsingCompanyIdStatic = new PriceScheduleRelations().CompanyEntityUsingCompanyId;

		/// <summary>CTor</summary>
		static StaticPriceScheduleRelations() { }
	}
}

