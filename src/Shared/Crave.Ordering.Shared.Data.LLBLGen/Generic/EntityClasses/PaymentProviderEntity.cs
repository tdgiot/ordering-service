﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 5.8
// Code is generated on: 
// Code is generated using templates: Crave.TemplateBindings
// Templates vendor: Crave Interactive
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.FactoryClasses;
using Crave.Ordering.Shared.Data.LLBLGen.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

// ReSharper disable All
namespace Crave.Ordering.Shared.Data.LLBLGen.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'PaymentProvider'.<br/><br/></summary>
	[Serializable]
	public partial class PaymentProviderEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END
	
	{
		private EntityCollection<PaymentIntegrationConfigurationEntity> _paymentIntegrationConfigurationCollection;
		private EntityCollection<PaymentTransactionEntity> _paymentTransactionCollection;
		private CompanyEntity _company;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		private static PaymentProviderEntityStaticMetaData _staticMetaData = new PaymentProviderEntityStaticMetaData();
		private static PaymentProviderRelations _relationsFactory = new PaymentProviderRelations();		

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Company</summary>
			public static readonly string Company = "Company";
			/// <summary>Member name PaymentIntegrationConfigurationCollection</summary>
			public static readonly string PaymentIntegrationConfigurationCollection = "PaymentIntegrationConfigurationCollection";
			/// <summary>Member name PaymentTransactionCollection</summary>
			public static readonly string PaymentTransactionCollection = "PaymentTransactionCollection";
		}

		/// <summary>Static meta-data storage for navigator related information</summary>
		protected class PaymentProviderEntityStaticMetaData : EntityStaticMetaDataBase
		{
			public PaymentProviderEntityStaticMetaData()
			{
				SetEntityCoreInfo("PaymentProviderEntity", InheritanceHierarchyType.None, false, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.PaymentProviderEntity, typeof(PaymentProviderEntity), typeof(PaymentProviderEntityFactory), false);
				AddNavigatorMetaData<PaymentProviderEntity, EntityCollection<PaymentIntegrationConfigurationEntity>>("PaymentIntegrationConfigurationCollection", a => a._paymentIntegrationConfigurationCollection, (a, b) => a._paymentIntegrationConfigurationCollection = b, a => a.PaymentIntegrationConfigurationCollection, () => new PaymentProviderRelations().PaymentIntegrationConfigurationEntityUsingPaymentProviderId, typeof(PaymentIntegrationConfigurationEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.PaymentIntegrationConfigurationEntity);
				AddNavigatorMetaData<PaymentProviderEntity, EntityCollection<PaymentTransactionEntity>>("PaymentTransactionCollection", a => a._paymentTransactionCollection, (a, b) => a._paymentTransactionCollection = b, a => a.PaymentTransactionCollection, () => new PaymentProviderRelations().PaymentTransactionEntityUsingPaymentProviderId, typeof(PaymentTransactionEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.PaymentTransactionEntity);
				AddNavigatorMetaData<PaymentProviderEntity, CompanyEntity>("Company", "PaymentProviderCollection", (a, b) => a._company = b, a => a._company, (a, b) => a.Company = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticPaymentProviderRelations.CompanyEntityUsingCompanyIdStatic, ()=>new PaymentProviderRelations().CompanyEntityUsingCompanyId, null, new int[] { (int)PaymentProviderFieldIndex.CompanyId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.CompanyEntity);
			}
		}

		/// <summary>Static ctor</summary>
		static PaymentProviderEntity()
		{
		}

		/// <summary> CTor</summary>
		public PaymentProviderEntity()
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public PaymentProviderEntity(IEntityFields2 fields)
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this PaymentProviderEntity</param>
		public PaymentProviderEntity(IValidator validator)
		{
			InitClassEmpty(validator, null);
		}

		/// <summary> CTor</summary>
		/// <param name="paymentProviderId">PK value for PaymentProvider which data should be fetched into this PaymentProvider object</param>
		public PaymentProviderEntity(System.Int32 paymentProviderId) : this(paymentProviderId, null)
		{
		}

		/// <summary> CTor</summary>
		/// <param name="paymentProviderId">PK value for PaymentProvider which data should be fetched into this PaymentProvider object</param>
		/// <param name="validator">The custom validator object for this PaymentProviderEntity</param>
		public PaymentProviderEntity(System.Int32 paymentProviderId, IValidator validator)
		{
			InitClassEmpty(validator, null);
			this.PaymentProviderId = paymentProviderId;
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected PaymentProviderEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'PaymentIntegrationConfiguration' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoPaymentIntegrationConfigurationCollection() { return CreateRelationInfoForNavigator("PaymentIntegrationConfigurationCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'PaymentTransaction' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoPaymentTransactionCollection() { return CreateRelationInfoForNavigator("PaymentTransactionCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Company' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoCompany() { return CreateRelationInfoForNavigator("Company"); }
		
		/// <inheritdoc/>
		protected override EntityStaticMetaDataBase GetEntityStaticMetaData() {	return _staticMetaData; }

		/// <summary>Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		/// <summary>Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this PaymentProviderEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary>The relations object holding all relations of this entity with other entity classes.</summary>
		public static PaymentProviderRelations Relations { get { return _relationsFactory; } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'PaymentIntegrationConfiguration' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathPaymentIntegrationConfigurationCollection { get { return _staticMetaData.GetPrefetchPathElement("PaymentIntegrationConfigurationCollection", CommonEntityBase.CreateEntityCollection<PaymentIntegrationConfigurationEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'PaymentTransaction' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathPaymentTransactionCollection { get { return _staticMetaData.GetPrefetchPathElement("PaymentTransactionCollection", CommonEntityBase.CreateEntityCollection<PaymentTransactionEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Company' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathCompanyEntity { get { return _staticMetaData.GetPrefetchPathElement("Company", CommonEntityBase.CreateEntityCollection<CompanyEntity>()); } }

		/// <summary>The PaymentProviderId property of the Entity PaymentProvider<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentProvider"."PaymentProviderId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 PaymentProviderId
		{
			get { return (System.Int32)GetValue((int)PaymentProviderFieldIndex.PaymentProviderId, true); }
			set { SetValue((int)PaymentProviderFieldIndex.PaymentProviderId, value); }		}

		/// <summary>The CompanyId property of the Entity PaymentProvider<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentProvider"."CompanyId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CompanyId
		{
			get { return (Nullable<System.Int32>)GetValue((int)PaymentProviderFieldIndex.CompanyId, false); }
			set	{ SetValue((int)PaymentProviderFieldIndex.CompanyId, value); }
		}

		/// <summary>The Type property of the Entity PaymentProvider<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentProvider"."Type".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 Type
		{
			get { return (System.Int32)GetValue((int)PaymentProviderFieldIndex.Type, true); }
			set	{ SetValue((int)PaymentProviderFieldIndex.Type, value); }
		}

		/// <summary>The Name property of the Entity PaymentProvider<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentProvider"."Name".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 100.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)PaymentProviderFieldIndex.Name, true); }
			set	{ SetValue((int)PaymentProviderFieldIndex.Name, value); }
		}

		/// <summary>The Environment property of the Entity PaymentProvider<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentProvider"."Environment".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 Environment
		{
			get { return (System.Int32)GetValue((int)PaymentProviderFieldIndex.Environment, true); }
			set	{ SetValue((int)PaymentProviderFieldIndex.Environment, value); }
		}

		/// <summary>The ApiKey property of the Entity PaymentProvider<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentProvider"."ApiKey".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 512.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ApiKey
		{
			get { return (System.String)GetValue((int)PaymentProviderFieldIndex.ApiKey, true); }
			set	{ SetValue((int)PaymentProviderFieldIndex.ApiKey, value); }
		}

		/// <summary>The LiveEndpointUrlPrefix property of the Entity PaymentProvider<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentProvider"."LiveEndpointUrlPrefix".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 250.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String LiveEndpointUrlPrefix
		{
			get { return (System.String)GetValue((int)PaymentProviderFieldIndex.LiveEndpointUrlPrefix, true); }
			set	{ SetValue((int)PaymentProviderFieldIndex.LiveEndpointUrlPrefix, value); }
		}

		/// <summary>The OriginDomain property of the Entity PaymentProvider<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentProvider"."OriginDomain".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String OriginDomain
		{
			get { return (System.String)GetValue((int)PaymentProviderFieldIndex.OriginDomain, true); }
			set	{ SetValue((int)PaymentProviderFieldIndex.OriginDomain, value); }
		}

		/// <summary>The CreatedUTC property of the Entity PaymentProvider<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentProvider"."CreatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime CreatedUTC
		{
			get { return (System.DateTime)GetValue((int)PaymentProviderFieldIndex.CreatedUTC, true); }
			set	{ SetValue((int)PaymentProviderFieldIndex.CreatedUTC, value); }
		}

		/// <summary>The UpdatedUTC property of the Entity PaymentProvider<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentProvider"."UpdatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)PaymentProviderFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)PaymentProviderFieldIndex.UpdatedUTC, value); }
		}

		/// <summary>The PaymentProcessorInfo property of the Entity PaymentProvider<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentProvider"."PaymentProcessorInfo".<br/>Table field type characteristics (type, precision, scale, length): Text, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PaymentProcessorInfo
		{
			get { return (System.String)GetValue((int)PaymentProviderFieldIndex.PaymentProcessorInfo, true); }
			set	{ SetValue((int)PaymentProviderFieldIndex.PaymentProcessorInfo, value); }
		}

		/// <summary>The PaymentProcessorInfoFooter property of the Entity PaymentProvider<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentProvider"."PaymentProcessorInfoFooter".<br/>Table field type characteristics (type, precision, scale, length): Text, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PaymentProcessorInfoFooter
		{
			get { return (System.String)GetValue((int)PaymentProviderFieldIndex.PaymentProcessorInfoFooter, true); }
			set	{ SetValue((int)PaymentProviderFieldIndex.PaymentProcessorInfoFooter, value); }
		}

		/// <summary>The AdyenUsername property of the Entity PaymentProvider<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentProvider"."AdyenUsername".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 100.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String AdyenUsername
		{
			get { return (System.String)GetValue((int)PaymentProviderFieldIndex.AdyenUsername, true); }
			set	{ SetValue((int)PaymentProviderFieldIndex.AdyenUsername, value); }
		}

		/// <summary>The AdyenPassword property of the Entity PaymentProvider<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentProvider"."AdyenPassword".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 250.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String AdyenPassword
		{
			get { return (System.String)GetValue((int)PaymentProviderFieldIndex.AdyenPassword, true); }
			set	{ SetValue((int)PaymentProviderFieldIndex.AdyenPassword, value); }
		}

		/// <summary>The OmisePublicKey property of the Entity PaymentProvider<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentProvider"."OmisePublicKey".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 100.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String OmisePublicKey
		{
			get { return (System.String)GetValue((int)PaymentProviderFieldIndex.OmisePublicKey, true); }
			set	{ SetValue((int)PaymentProviderFieldIndex.OmisePublicKey, value); }
		}

		/// <summary>The OmiseSecretKey property of the Entity PaymentProvider<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentProvider"."OmiseSecretKey".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 250.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String OmiseSecretKey
		{
			get { return (System.String)GetValue((int)PaymentProviderFieldIndex.OmiseSecretKey, true); }
			set	{ SetValue((int)PaymentProviderFieldIndex.OmiseSecretKey, value); }
		}

		/// <summary>The SkinCode property of the Entity PaymentProvider<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentProvider"."SkinCode".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String SkinCode
		{
			get { return (System.String)GetValue((int)PaymentProviderFieldIndex.SkinCode, true); }
			set	{ SetValue((int)PaymentProviderFieldIndex.SkinCode, value); }
		}

		/// <summary>The HmacKey property of the Entity PaymentProvider<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentProvider"."HmacKey".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 250.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String HmacKey
		{
			get { return (System.String)GetValue((int)PaymentProviderFieldIndex.HmacKey, true); }
			set	{ SetValue((int)PaymentProviderFieldIndex.HmacKey, value); }
		}

		/// <summary>The ClientKey property of the Entity PaymentProvider<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentProvider"."ClientKey".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 250.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ClientKey
		{
			get { return (System.String)GetValue((int)PaymentProviderFieldIndex.ClientKey, true); }
			set	{ SetValue((int)PaymentProviderFieldIndex.ClientKey, value); }
		}

		/// <summary>The AdyenMerchantCode property of the Entity PaymentProvider<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentProvider"."AdyenMerchantCode".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 250.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String AdyenMerchantCode
		{
			get { return (System.String)GetValue((int)PaymentProviderFieldIndex.AdyenMerchantCode, true); }
			set	{ SetValue((int)PaymentProviderFieldIndex.AdyenMerchantCode, value); }
		}

		/// <summary>The GooglePayMerchantIdentifier property of the Entity PaymentProvider<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentProvider"."GooglePayMerchantIdentifier".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 250.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String GooglePayMerchantIdentifier
		{
			get { return (System.String)GetValue((int)PaymentProviderFieldIndex.GooglePayMerchantIdentifier, true); }
			set	{ SetValue((int)PaymentProviderFieldIndex.GooglePayMerchantIdentifier, value); }
		}

		/// <summary>The ApplePayMerchantIdentifier property of the Entity PaymentProvider<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentProvider"."ApplePayMerchantIdentifier".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 250.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ApplePayMerchantIdentifier
		{
			get { return (System.String)GetValue((int)PaymentProviderFieldIndex.ApplePayMerchantIdentifier, true); }
			set	{ SetValue((int)PaymentProviderFieldIndex.ApplePayMerchantIdentifier, value); }
		}

		/// <summary>Gets the EntityCollection with the related entities of type 'PaymentIntegrationConfigurationEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(PaymentIntegrationConfigurationEntity))]
		public virtual EntityCollection<PaymentIntegrationConfigurationEntity> PaymentIntegrationConfigurationCollection { get { return GetOrCreateEntityCollection<PaymentIntegrationConfigurationEntity, PaymentIntegrationConfigurationEntityFactory>("PaymentProvider", true, false, ref _paymentIntegrationConfigurationCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'PaymentTransactionEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(PaymentTransactionEntity))]
		public virtual EntityCollection<PaymentTransactionEntity> PaymentTransactionCollection { get { return GetOrCreateEntityCollection<PaymentTransactionEntity, PaymentTransactionEntityFactory>("PaymentProvider", true, false, ref _paymentTransactionCollection); } }

		/// <summary>Gets / sets related entity of type 'CompanyEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual CompanyEntity Company
		{
			get { return _company; }
			set { SetSingleRelatedEntityNavigator(value, "Company"); }
		}


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen
{
	public enum PaymentProviderFieldIndex
	{
		///<summary>PaymentProviderId. </summary>
		PaymentProviderId,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>Type. </summary>
		Type,
		///<summary>Name. </summary>
		Name,
		///<summary>Environment. </summary>
		Environment,
		///<summary>ApiKey. </summary>
		ApiKey,
		///<summary>LiveEndpointUrlPrefix. </summary>
		LiveEndpointUrlPrefix,
		///<summary>OriginDomain. </summary>
		OriginDomain,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>PaymentProcessorInfo. </summary>
		PaymentProcessorInfo,
		///<summary>PaymentProcessorInfoFooter. </summary>
		PaymentProcessorInfoFooter,
		///<summary>AdyenUsername. </summary>
		AdyenUsername,
		///<summary>AdyenPassword. </summary>
		AdyenPassword,
		///<summary>OmisePublicKey. </summary>
		OmisePublicKey,
		///<summary>OmiseSecretKey. </summary>
		OmiseSecretKey,
		///<summary>SkinCode. </summary>
		SkinCode,
		///<summary>HmacKey. </summary>
		HmacKey,
		///<summary>ClientKey. </summary>
		ClientKey,
		///<summary>AdyenMerchantCode. </summary>
		AdyenMerchantCode,
		///<summary>GooglePayMerchantIdentifier. </summary>
		GooglePayMerchantIdentifier,
		///<summary>ApplePayMerchantIdentifier. </summary>
		ApplePayMerchantIdentifier,
		/// <summary></summary>
		AmountOfFields
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: PaymentProvider. </summary>
	public partial class PaymentProviderRelations: RelationFactory
	{
		#region Custom Relation code
		
        // __LLBLGENPRO_USER_CODE_REGION_START CustomRelationCode
        private const ReferentialConstraintDeleteRule DeleteRuleForPaymentIntegrationConfigurationEntityUsingPaymentProviderId = ReferentialConstraintDeleteRule.SetNull;
        private const ReferentialConstraintDeleteRule DeleteRuleForPaymentTransactionEntityUsingPaymentProviderId = ReferentialConstraintDeleteRule.SetNull;
        // __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		
		// private const ReferentialConstraintDeleteRule DeleteRuleForPaymentIntegrationConfigurationEntityUsingPaymentProviderId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForPaymentTransactionEntityUsingPaymentProviderId = ReferentialConstraintDeleteRule


		/// <summary>Gets all relations and delete rules of the PaymentProviderEntity as a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects.</summary>
		/// <returns>a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects</returns>
		public virtual Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> GetRelationsAndDeleteRules()
		{
			Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> toReturn = new Dictionary<IEntityRelation, ReferentialConstraintDeleteRule>();
			toReturn.Add(this.PaymentIntegrationConfigurationEntityUsingPaymentProviderId, PaymentProviderRelations.DeleteRuleForPaymentIntegrationConfigurationEntityUsingPaymentProviderId);
			toReturn.Add(this.PaymentTransactionEntityUsingPaymentProviderId, PaymentProviderRelations.DeleteRuleForPaymentTransactionEntityUsingPaymentProviderId);
			return toReturn;
		}		
	
	
		/// <summary>Returns a new IEntityRelation object, between PaymentProviderEntity and PaymentIntegrationConfigurationEntity over the 1:n relation they have, using the relation between the fields: PaymentProvider.PaymentProviderId - PaymentIntegrationConfiguration.PaymentProviderId</summary>
		public virtual IEntityRelation PaymentIntegrationConfigurationEntityUsingPaymentProviderId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "PaymentIntegrationConfigurationCollection", true, new[] { PaymentProviderFields.PaymentProviderId, PaymentIntegrationConfigurationFields.PaymentProviderId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between PaymentProviderEntity and PaymentTransactionEntity over the 1:n relation they have, using the relation between the fields: PaymentProvider.PaymentProviderId - PaymentTransaction.PaymentProviderId</summary>
		public virtual IEntityRelation PaymentTransactionEntityUsingPaymentProviderId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "PaymentTransactionCollection", true, new[] { PaymentProviderFields.PaymentProviderId, PaymentTransactionFields.PaymentProviderId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between PaymentProviderEntity and CompanyEntity over the m:1 relation they have, using the relation between the fields: PaymentProvider.CompanyId - Company.CompanyId</summary>
		public virtual IEntityRelation CompanyEntityUsingCompanyId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Company", false, new[] { CompanyFields.CompanyId, PaymentProviderFields.CompanyId }); }
		}

	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticPaymentProviderRelations
	{
		internal static readonly IEntityRelation PaymentIntegrationConfigurationEntityUsingPaymentProviderIdStatic = new PaymentProviderRelations().PaymentIntegrationConfigurationEntityUsingPaymentProviderId;
		internal static readonly IEntityRelation PaymentTransactionEntityUsingPaymentProviderIdStatic = new PaymentProviderRelations().PaymentTransactionEntityUsingPaymentProviderId;
		internal static readonly IEntityRelation CompanyEntityUsingCompanyIdStatic = new PaymentProviderRelations().CompanyEntityUsingCompanyId;

		/// <summary>CTor</summary>
		static StaticPaymentProviderRelations() { }
	}
}

