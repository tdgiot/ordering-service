﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 5.8
// Code is generated on: 
// Code is generated using templates: Crave.TemplateBindings
// Templates vendor: Crave Interactive
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.FactoryClasses;
using Crave.Ordering.Shared.Data.LLBLGen.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

// ReSharper disable All
namespace Crave.Ordering.Shared.Data.LLBLGen.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'AlterationitemAlteration'.<br/><br/></summary>
	[Serializable]
	public partial class AlterationitemAlterationEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END
	
	{
		private AlterationEntity _alteration;
		private AlterationitemEntity _alterationitem;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		private static AlterationitemAlterationEntityStaticMetaData _staticMetaData = new AlterationitemAlterationEntityStaticMetaData();
		private static AlterationitemAlterationRelations _relationsFactory = new AlterationitemAlterationRelations();		

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Alteration</summary>
			public static readonly string Alteration = "Alteration";
			/// <summary>Member name Alterationitem</summary>
			public static readonly string Alterationitem = "Alterationitem";
		}

		/// <summary>Static meta-data storage for navigator related information</summary>
		protected class AlterationitemAlterationEntityStaticMetaData : EntityStaticMetaDataBase
		{
			public AlterationitemAlterationEntityStaticMetaData()
			{
				SetEntityCoreInfo("AlterationitemAlterationEntity", InheritanceHierarchyType.None, false, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.AlterationitemAlterationEntity, typeof(AlterationitemAlterationEntity), typeof(AlterationitemAlterationEntityFactory), false);
				AddNavigatorMetaData<AlterationitemAlterationEntity, AlterationEntity>("Alteration", "AlterationitemAlterationCollection", (a, b) => a._alteration = b, a => a._alteration, (a, b) => a.Alteration = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticAlterationitemAlterationRelations.AlterationEntityUsingAlterationIdStatic, ()=>new AlterationitemAlterationRelations().AlterationEntityUsingAlterationId, null, new int[] { (int)AlterationitemAlterationFieldIndex.AlterationId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.AlterationEntity);
				AddNavigatorMetaData<AlterationitemAlterationEntity, AlterationitemEntity>("Alterationitem", "AlterationitemAlterationCollection", (a, b) => a._alterationitem = b, a => a._alterationitem, (a, b) => a.Alterationitem = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticAlterationitemAlterationRelations.AlterationitemEntityUsingAlterationitemIdStatic, ()=>new AlterationitemAlterationRelations().AlterationitemEntityUsingAlterationitemId, null, new int[] { (int)AlterationitemAlterationFieldIndex.AlterationitemId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.AlterationitemEntity);
			}
		}

		/// <summary>Static ctor</summary>
		static AlterationitemAlterationEntity()
		{
		}

		/// <summary> CTor</summary>
		public AlterationitemAlterationEntity()
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public AlterationitemAlterationEntity(IEntityFields2 fields)
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this AlterationitemAlterationEntity</param>
		public AlterationitemAlterationEntity(IValidator validator)
		{
			InitClassEmpty(validator, null);
		}

		/// <summary> CTor</summary>
		/// <param name="alterationitemAlterationId">PK value for AlterationitemAlteration which data should be fetched into this AlterationitemAlteration object</param>
		public AlterationitemAlterationEntity(System.Int32 alterationitemAlterationId) : this(alterationitemAlterationId, null)
		{
		}

		/// <summary> CTor</summary>
		/// <param name="alterationitemAlterationId">PK value for AlterationitemAlteration which data should be fetched into this AlterationitemAlteration object</param>
		/// <param name="validator">The custom validator object for this AlterationitemAlterationEntity</param>
		public AlterationitemAlterationEntity(System.Int32 alterationitemAlterationId, IValidator validator)
		{
			InitClassEmpty(validator, null);
			this.AlterationitemAlterationId = alterationitemAlterationId;
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected AlterationitemAlterationEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary>Method which will construct a filter (predicate expression) for the unique constraint defined on the fields: AlterationitemId , AlterationId .</summary>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public IPredicateExpression ConstructFilterForUCAlterationitemIdAlterationId()
		{
			var filter = new PredicateExpression();
			filter.Add(Crave.Ordering.Shared.Data.LLBLGen.HelperClasses.AlterationitemAlterationFields.AlterationitemId == this.Fields.GetCurrentValue((int)AlterationitemAlterationFieldIndex.AlterationitemId));
			filter.Add(Crave.Ordering.Shared.Data.LLBLGen.HelperClasses.AlterationitemAlterationFields.AlterationId == this.Fields.GetCurrentValue((int)AlterationitemAlterationFieldIndex.AlterationId));
 			return filter;
		}

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Alteration' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoAlteration() { return CreateRelationInfoForNavigator("Alteration"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Alterationitem' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoAlterationitem() { return CreateRelationInfoForNavigator("Alterationitem"); }
		
		/// <inheritdoc/>
		protected override EntityStaticMetaDataBase GetEntityStaticMetaData() {	return _staticMetaData; }

		/// <summary>Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		/// <summary>Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this AlterationitemAlterationEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary>The relations object holding all relations of this entity with other entity classes.</summary>
		public static AlterationitemAlterationRelations Relations { get { return _relationsFactory; } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Alteration' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathAlterationEntity { get { return _staticMetaData.GetPrefetchPathElement("Alteration", CommonEntityBase.CreateEntityCollection<AlterationEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Alterationitem' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathAlterationitemEntity { get { return _staticMetaData.GetPrefetchPathElement("Alterationitem", CommonEntityBase.CreateEntityCollection<AlterationitemEntity>()); } }

		/// <summary>The AlterationitemAlterationId property of the Entity AlterationitemAlteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AlterationitemAlteration"."AlterationitemAlterationId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 AlterationitemAlterationId
		{
			get { return (System.Int32)GetValue((int)AlterationitemAlterationFieldIndex.AlterationitemAlterationId, true); }
			set { SetValue((int)AlterationitemAlterationFieldIndex.AlterationitemAlterationId, value); }		}

		/// <summary>The ParentCompanyId property of the Entity AlterationitemAlteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AlterationitemAlteration"."ParentCompanyId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ParentCompanyId
		{
			get { return (Nullable<System.Int32>)GetValue((int)AlterationitemAlterationFieldIndex.ParentCompanyId, false); }
			set	{ SetValue((int)AlterationitemAlterationFieldIndex.ParentCompanyId, value); }
		}

		/// <summary>The AlterationitemId property of the Entity AlterationitemAlteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AlterationitemAlteration"."AlterationitemId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 AlterationitemId
		{
			get { return (System.Int32)GetValue((int)AlterationitemAlterationFieldIndex.AlterationitemId, true); }
			set	{ SetValue((int)AlterationitemAlterationFieldIndex.AlterationitemId, value); }
		}

		/// <summary>The AlterationId property of the Entity AlterationitemAlteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AlterationitemAlteration"."AlterationId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 AlterationId
		{
			get { return (System.Int32)GetValue((int)AlterationitemAlterationFieldIndex.AlterationId, true); }
			set	{ SetValue((int)AlterationitemAlterationFieldIndex.AlterationId, value); }
		}

		/// <summary>The SortOrder property of the Entity AlterationitemAlteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AlterationitemAlteration"."SortOrder".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 SortOrder
		{
			get { return (System.Int32)GetValue((int)AlterationitemAlterationFieldIndex.SortOrder, true); }
			set	{ SetValue((int)AlterationitemAlterationFieldIndex.SortOrder, value); }
		}

		/// <summary>The CreatedUTC property of the Entity AlterationitemAlteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AlterationitemAlteration"."CreatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)AlterationitemAlterationFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)AlterationitemAlterationFieldIndex.CreatedUTC, value); }
		}

		/// <summary>The CreatedBy property of the Entity AlterationitemAlteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AlterationitemAlteration"."CreatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CreatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)AlterationitemAlterationFieldIndex.CreatedBy, false); }
			set	{ SetValue((int)AlterationitemAlterationFieldIndex.CreatedBy, value); }
		}

		/// <summary>The UpdatedUTC property of the Entity AlterationitemAlteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AlterationitemAlteration"."UpdatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)AlterationitemAlterationFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)AlterationitemAlterationFieldIndex.UpdatedUTC, value); }
		}

		/// <summary>The UpdatedBy property of the Entity AlterationitemAlteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AlterationitemAlteration"."UpdatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UpdatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)AlterationitemAlterationFieldIndex.UpdatedBy, false); }
			set	{ SetValue((int)AlterationitemAlterationFieldIndex.UpdatedBy, value); }
		}

		/// <summary>Gets / sets related entity of type 'AlterationEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual AlterationEntity Alteration
		{
			get { return _alteration; }
			set { SetSingleRelatedEntityNavigator(value, "Alteration"); }
		}

		/// <summary>Gets / sets related entity of type 'AlterationitemEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual AlterationitemEntity Alterationitem
		{
			get { return _alterationitem; }
			set { SetSingleRelatedEntityNavigator(value, "Alterationitem"); }
		}


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen
{
	public enum AlterationitemAlterationFieldIndex
	{
		///<summary>AlterationitemAlterationId. </summary>
		AlterationitemAlterationId,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>AlterationitemId. </summary>
		AlterationitemId,
		///<summary>AlterationId. </summary>
		AlterationId,
		///<summary>SortOrder. </summary>
		SortOrder,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		/// <summary></summary>
		AmountOfFields
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: AlterationitemAlteration. </summary>
	public partial class AlterationitemAlterationRelations: RelationFactory
	{
		#region Custom Relation code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomRelationCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		


		/// <summary>Gets all relations and delete rules of the AlterationitemAlterationEntity as a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects.</summary>
		/// <returns>a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects</returns>
		public virtual Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> GetRelationsAndDeleteRules()
		{
			Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> toReturn = new Dictionary<IEntityRelation, ReferentialConstraintDeleteRule>();
			return toReturn;
		}		
	
	

		/// <summary>Returns a new IEntityRelation object, between AlterationitemAlterationEntity and AlterationEntity over the m:1 relation they have, using the relation between the fields: AlterationitemAlteration.AlterationId - Alteration.AlterationId</summary>
		public virtual IEntityRelation AlterationEntityUsingAlterationId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Alteration", false, new[] { AlterationFields.AlterationId, AlterationitemAlterationFields.AlterationId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between AlterationitemAlterationEntity and AlterationitemEntity over the m:1 relation they have, using the relation between the fields: AlterationitemAlteration.AlterationitemId - Alterationitem.AlterationitemId</summary>
		public virtual IEntityRelation AlterationitemEntityUsingAlterationitemId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Alterationitem", false, new[] { AlterationitemFields.AlterationitemId, AlterationitemAlterationFields.AlterationitemId }); }
		}

	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticAlterationitemAlterationRelations
	{
		internal static readonly IEntityRelation AlterationEntityUsingAlterationIdStatic = new AlterationitemAlterationRelations().AlterationEntityUsingAlterationId;
		internal static readonly IEntityRelation AlterationitemEntityUsingAlterationitemIdStatic = new AlterationitemAlterationRelations().AlterationitemEntityUsingAlterationitemId;

		/// <summary>CTor</summary>
		static StaticAlterationitemAlterationRelations() { }
	}
}

