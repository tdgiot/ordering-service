﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 5.8
// Code is generated on: 
// Code is generated using templates: Crave.TemplateBindings
// Templates vendor: Crave Interactive
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.FactoryClasses;
using Crave.Ordering.Shared.Data.LLBLGen.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

// ReSharper disable All
namespace Crave.Ordering.Shared.Data.LLBLGen.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'ReceiptTemplate'.<br/><br/></summary>
	[Serializable]
	public partial class ReceiptTemplateEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END
	
	{
		private EntityCollection<CheckoutMethodEntity> _checkoutMethodCollection;
		private EntityCollection<ReceiptEntity> _receiptCollection;
		private CompanyEntity _company;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		private static ReceiptTemplateEntityStaticMetaData _staticMetaData = new ReceiptTemplateEntityStaticMetaData();
		private static ReceiptTemplateRelations _relationsFactory = new ReceiptTemplateRelations();		

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Company</summary>
			public static readonly string Company = "Company";
			/// <summary>Member name CheckoutMethodCollection</summary>
			public static readonly string CheckoutMethodCollection = "CheckoutMethodCollection";
			/// <summary>Member name ReceiptCollection</summary>
			public static readonly string ReceiptCollection = "ReceiptCollection";
		}

		/// <summary>Static meta-data storage for navigator related information</summary>
		protected class ReceiptTemplateEntityStaticMetaData : EntityStaticMetaDataBase
		{
			public ReceiptTemplateEntityStaticMetaData()
			{
				SetEntityCoreInfo("ReceiptTemplateEntity", InheritanceHierarchyType.None, false, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ReceiptTemplateEntity, typeof(ReceiptTemplateEntity), typeof(ReceiptTemplateEntityFactory), false);
				AddNavigatorMetaData<ReceiptTemplateEntity, EntityCollection<CheckoutMethodEntity>>("CheckoutMethodCollection", a => a._checkoutMethodCollection, (a, b) => a._checkoutMethodCollection = b, a => a.CheckoutMethodCollection, () => new ReceiptTemplateRelations().CheckoutMethodEntityUsingReceiptTemplateId, typeof(CheckoutMethodEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.CheckoutMethodEntity);
				AddNavigatorMetaData<ReceiptTemplateEntity, EntityCollection<ReceiptEntity>>("ReceiptCollection", a => a._receiptCollection, (a, b) => a._receiptCollection = b, a => a.ReceiptCollection, () => new ReceiptTemplateRelations().ReceiptEntityUsingReceiptTemplateId, typeof(ReceiptEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ReceiptEntity);
				AddNavigatorMetaData<ReceiptTemplateEntity, CompanyEntity>("Company", "ReceiptTemplateCollection", (a, b) => a._company = b, a => a._company, (a, b) => a.Company = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticReceiptTemplateRelations.CompanyEntityUsingCompanyIdStatic, ()=>new ReceiptTemplateRelations().CompanyEntityUsingCompanyId, null, new int[] { (int)ReceiptTemplateFieldIndex.CompanyId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.CompanyEntity);
			}
		}

		/// <summary>Static ctor</summary>
		static ReceiptTemplateEntity()
		{
		}

		/// <summary> CTor</summary>
		public ReceiptTemplateEntity()
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public ReceiptTemplateEntity(IEntityFields2 fields)
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this ReceiptTemplateEntity</param>
		public ReceiptTemplateEntity(IValidator validator)
		{
			InitClassEmpty(validator, null);
		}

		/// <summary> CTor</summary>
		/// <param name="receiptTemplateId">PK value for ReceiptTemplate which data should be fetched into this ReceiptTemplate object</param>
		public ReceiptTemplateEntity(System.Int32 receiptTemplateId) : this(receiptTemplateId, null)
		{
		}

		/// <summary> CTor</summary>
		/// <param name="receiptTemplateId">PK value for ReceiptTemplate which data should be fetched into this ReceiptTemplate object</param>
		/// <param name="validator">The custom validator object for this ReceiptTemplateEntity</param>
		public ReceiptTemplateEntity(System.Int32 receiptTemplateId, IValidator validator)
		{
			InitClassEmpty(validator, null);
			this.ReceiptTemplateId = receiptTemplateId;
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ReceiptTemplateEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'CheckoutMethod' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoCheckoutMethodCollection() { return CreateRelationInfoForNavigator("CheckoutMethodCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Receipt' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoReceiptCollection() { return CreateRelationInfoForNavigator("ReceiptCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Company' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoCompany() { return CreateRelationInfoForNavigator("Company"); }
		
		/// <inheritdoc/>
		protected override EntityStaticMetaDataBase GetEntityStaticMetaData() {	return _staticMetaData; }

		/// <summary>Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		/// <summary>Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this ReceiptTemplateEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary>The relations object holding all relations of this entity with other entity classes.</summary>
		public static ReceiptTemplateRelations Relations { get { return _relationsFactory; } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'CheckoutMethod' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathCheckoutMethodCollection { get { return _staticMetaData.GetPrefetchPathElement("CheckoutMethodCollection", CommonEntityBase.CreateEntityCollection<CheckoutMethodEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Receipt' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathReceiptCollection { get { return _staticMetaData.GetPrefetchPathElement("ReceiptCollection", CommonEntityBase.CreateEntityCollection<ReceiptEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Company' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathCompanyEntity { get { return _staticMetaData.GetPrefetchPathElement("Company", CommonEntityBase.CreateEntityCollection<CompanyEntity>()); } }

		/// <summary>The ReceiptTemplateId property of the Entity ReceiptTemplate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ReceiptTemplate"."ReceiptTemplateId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 ReceiptTemplateId
		{
			get { return (System.Int32)GetValue((int)ReceiptTemplateFieldIndex.ReceiptTemplateId, true); }
			set { SetValue((int)ReceiptTemplateFieldIndex.ReceiptTemplateId, value); }		}

		/// <summary>The CompanyId property of the Entity ReceiptTemplate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ReceiptTemplate"."CompanyId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CompanyId
		{
			get { return (System.Int32)GetValue((int)ReceiptTemplateFieldIndex.CompanyId, true); }
			set	{ SetValue((int)ReceiptTemplateFieldIndex.CompanyId, value); }
		}

		/// <summary>The SellerName property of the Entity ReceiptTemplate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ReceiptTemplate"."SellerName".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String SellerName
		{
			get { return (System.String)GetValue((int)ReceiptTemplateFieldIndex.SellerName, true); }
			set	{ SetValue((int)ReceiptTemplateFieldIndex.SellerName, value); }
		}

		/// <summary>The SellerAddress property of the Entity ReceiptTemplate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ReceiptTemplate"."SellerAddress".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String SellerAddress
		{
			get { return (System.String)GetValue((int)ReceiptTemplateFieldIndex.SellerAddress, true); }
			set	{ SetValue((int)ReceiptTemplateFieldIndex.SellerAddress, value); }
		}

		/// <summary>The SellerContactInfo property of the Entity ReceiptTemplate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ReceiptTemplate"."SellerContactInfo".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String SellerContactInfo
		{
			get { return (System.String)GetValue((int)ReceiptTemplateFieldIndex.SellerContactInfo, true); }
			set	{ SetValue((int)ReceiptTemplateFieldIndex.SellerContactInfo, value); }
		}

		/// <summary>The TaxBreakdown property of the Entity ReceiptTemplate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ReceiptTemplate"."TaxBreakdown".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Crave.Enums.TaxBreakdown TaxBreakdown
		{
			get { return (Crave.Enums.TaxBreakdown)GetValue((int)ReceiptTemplateFieldIndex.TaxBreakdown, true); }
			set	{ SetValue((int)ReceiptTemplateFieldIndex.TaxBreakdown, value); }
		}

		/// <summary>The CreatedBy property of the Entity ReceiptTemplate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ReceiptTemplate"."CreatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)ReceiptTemplateFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)ReceiptTemplateFieldIndex.CreatedBy, value); }
		}

		/// <summary>The UpdatedBy property of the Entity ReceiptTemplate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ReceiptTemplate"."UpdatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)ReceiptTemplateFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)ReceiptTemplateFieldIndex.UpdatedBy, value); }
		}

		/// <summary>The CreatedUTC property of the Entity ReceiptTemplate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ReceiptTemplate"."CreatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ReceiptTemplateFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)ReceiptTemplateFieldIndex.CreatedUTC, value); }
		}

		/// <summary>The UpdatedUTC property of the Entity ReceiptTemplate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ReceiptTemplate"."UpdatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ReceiptTemplateFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)ReceiptTemplateFieldIndex.UpdatedUTC, value); }
		}

		/// <summary>The Name property of the Entity ReceiptTemplate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ReceiptTemplate"."Name".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 256.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)ReceiptTemplateFieldIndex.Name, true); }
			set	{ SetValue((int)ReceiptTemplateFieldIndex.Name, value); }
		}

		/// <summary>The Email property of the Entity ReceiptTemplate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ReceiptTemplate"."Email".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2000.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Email
		{
			get { return (System.String)GetValue((int)ReceiptTemplateFieldIndex.Email, true); }
			set	{ SetValue((int)ReceiptTemplateFieldIndex.Email, value); }
		}

		/// <summary>The Phonenumber property of the Entity ReceiptTemplate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ReceiptTemplate"."Phonenumber".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2000.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Phonenumber
		{
			get { return (System.String)GetValue((int)ReceiptTemplateFieldIndex.Phonenumber, true); }
			set	{ SetValue((int)ReceiptTemplateFieldIndex.Phonenumber, value); }
		}

		/// <summary>The VatNumber property of the Entity ReceiptTemplate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ReceiptTemplate"."VatNumber".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String VatNumber
		{
			get { return (System.String)GetValue((int)ReceiptTemplateFieldIndex.VatNumber, true); }
			set	{ SetValue((int)ReceiptTemplateFieldIndex.VatNumber, value); }
		}

		/// <summary>The SellerContactEmail property of the Entity ReceiptTemplate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ReceiptTemplate"."SellerContactEmail".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 250.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String SellerContactEmail
		{
			get { return (System.String)GetValue((int)ReceiptTemplateFieldIndex.SellerContactEmail, true); }
			set	{ SetValue((int)ReceiptTemplateFieldIndex.SellerContactEmail, value); }
		}

		/// <summary>Gets the EntityCollection with the related entities of type 'CheckoutMethodEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(CheckoutMethodEntity))]
		public virtual EntityCollection<CheckoutMethodEntity> CheckoutMethodCollection { get { return GetOrCreateEntityCollection<CheckoutMethodEntity, CheckoutMethodEntityFactory>("ReceiptTemplate", true, false, ref _checkoutMethodCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'ReceiptEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(ReceiptEntity))]
		public virtual EntityCollection<ReceiptEntity> ReceiptCollection { get { return GetOrCreateEntityCollection<ReceiptEntity, ReceiptEntityFactory>("ReceiptTemplate", true, false, ref _receiptCollection); } }

		/// <summary>Gets / sets related entity of type 'CompanyEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual CompanyEntity Company
		{
			get { return _company; }
			set { SetSingleRelatedEntityNavigator(value, "Company"); }
		}


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen
{
	public enum ReceiptTemplateFieldIndex
	{
		///<summary>ReceiptTemplateId. </summary>
		ReceiptTemplateId,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>SellerName. </summary>
		SellerName,
		///<summary>SellerAddress. </summary>
		SellerAddress,
		///<summary>SellerContactInfo. </summary>
		SellerContactInfo,
		///<summary>TaxBreakdown. </summary>
		TaxBreakdown,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>Name. </summary>
		Name,
		///<summary>Email. </summary>
		Email,
		///<summary>Phonenumber. </summary>
		Phonenumber,
		///<summary>VatNumber. </summary>
		VatNumber,
		///<summary>SellerContactEmail. </summary>
		SellerContactEmail,
		/// <summary></summary>
		AmountOfFields
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: ReceiptTemplate. </summary>
	public partial class ReceiptTemplateRelations: RelationFactory
	{
		#region Custom Relation code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomRelationCode
        private const ReferentialConstraintDeleteRule DeleteRuleForCheckoutMethodEntityUsingReceiptTemplateId = ReferentialConstraintDeleteRule.NoAction;
        private const ReferentialConstraintDeleteRule DeleteRuleForReceiptEntityUsingReceiptTemplateId = ReferentialConstraintDeleteRule.NoAction;
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		
		// private const ReferentialConstraintDeleteRule DeleteRuleForCheckoutMethodEntityUsingReceiptTemplateId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForReceiptEntityUsingReceiptTemplateId = ReferentialConstraintDeleteRule


		/// <summary>Gets all relations and delete rules of the ReceiptTemplateEntity as a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects.</summary>
		/// <returns>a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects</returns>
		public virtual Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> GetRelationsAndDeleteRules()
		{
			Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> toReturn = new Dictionary<IEntityRelation, ReferentialConstraintDeleteRule>();
			toReturn.Add(this.CheckoutMethodEntityUsingReceiptTemplateId, ReceiptTemplateRelations.DeleteRuleForCheckoutMethodEntityUsingReceiptTemplateId);
			toReturn.Add(this.ReceiptEntityUsingReceiptTemplateId, ReceiptTemplateRelations.DeleteRuleForReceiptEntityUsingReceiptTemplateId);
			return toReturn;
		}		
	
	
		/// <summary>Returns a new IEntityRelation object, between ReceiptTemplateEntity and CheckoutMethodEntity over the 1:n relation they have, using the relation between the fields: ReceiptTemplate.ReceiptTemplateId - CheckoutMethod.ReceiptTemplateId</summary>
		public virtual IEntityRelation CheckoutMethodEntityUsingReceiptTemplateId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "CheckoutMethodCollection", true, new[] { ReceiptTemplateFields.ReceiptTemplateId, CheckoutMethodFields.ReceiptTemplateId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between ReceiptTemplateEntity and ReceiptEntity over the 1:n relation they have, using the relation between the fields: ReceiptTemplate.ReceiptTemplateId - Receipt.ReceiptTemplateId</summary>
		public virtual IEntityRelation ReceiptEntityUsingReceiptTemplateId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "ReceiptCollection", true, new[] { ReceiptTemplateFields.ReceiptTemplateId, ReceiptFields.ReceiptTemplateId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between ReceiptTemplateEntity and CompanyEntity over the m:1 relation they have, using the relation between the fields: ReceiptTemplate.CompanyId - Company.CompanyId</summary>
		public virtual IEntityRelation CompanyEntityUsingCompanyId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Company", false, new[] { CompanyFields.CompanyId, ReceiptTemplateFields.CompanyId }); }
		}

	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticReceiptTemplateRelations
	{
		internal static readonly IEntityRelation CheckoutMethodEntityUsingReceiptTemplateIdStatic = new ReceiptTemplateRelations().CheckoutMethodEntityUsingReceiptTemplateId;
		internal static readonly IEntityRelation ReceiptEntityUsingReceiptTemplateIdStatic = new ReceiptTemplateRelations().ReceiptEntityUsingReceiptTemplateId;
		internal static readonly IEntityRelation CompanyEntityUsingCompanyIdStatic = new ReceiptTemplateRelations().CompanyEntityUsingCompanyId;

		/// <summary>CTor</summary>
		static StaticReceiptTemplateRelations() { }
	}
}

