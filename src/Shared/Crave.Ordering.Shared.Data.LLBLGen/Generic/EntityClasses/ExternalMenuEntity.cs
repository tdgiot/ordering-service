﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 5.8
// Code is generated on: 
// Code is generated using templates: Crave.TemplateBindings
// Templates vendor: Crave Interactive
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.FactoryClasses;
using Crave.Ordering.Shared.Data.LLBLGen.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

// ReSharper disable All
namespace Crave.Ordering.Shared.Data.LLBLGen.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'ExternalMenu'.<br/><br/></summary>
	[Serializable]
	public partial class ExternalMenuEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END
	
	{
		private EntityCollection<ExternalProductEntity> _externalProductCollection;
		private CompanyEntity _company;
		private ExternalSystemEntity _externalSystem;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		private static ExternalMenuEntityStaticMetaData _staticMetaData = new ExternalMenuEntityStaticMetaData();
		private static ExternalMenuRelations _relationsFactory = new ExternalMenuRelations();		

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Company</summary>
			public static readonly string Company = "Company";
			/// <summary>Member name ExternalSystem</summary>
			public static readonly string ExternalSystem = "ExternalSystem";
			/// <summary>Member name ExternalProductCollection</summary>
			public static readonly string ExternalProductCollection = "ExternalProductCollection";
		}

		/// <summary>Static meta-data storage for navigator related information</summary>
		protected class ExternalMenuEntityStaticMetaData : EntityStaticMetaDataBase
		{
			public ExternalMenuEntityStaticMetaData()
			{
				SetEntityCoreInfo("ExternalMenuEntity", InheritanceHierarchyType.None, false, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ExternalMenuEntity, typeof(ExternalMenuEntity), typeof(ExternalMenuEntityFactory), false);
				AddNavigatorMetaData<ExternalMenuEntity, EntityCollection<ExternalProductEntity>>("ExternalProductCollection", a => a._externalProductCollection, (a, b) => a._externalProductCollection = b, a => a.ExternalProductCollection, () => new ExternalMenuRelations().ExternalProductEntityUsingExternalMenuId, typeof(ExternalProductEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ExternalProductEntity);
				AddNavigatorMetaData<ExternalMenuEntity, CompanyEntity>("Company", "ExternalMenuCollection", (a, b) => a._company = b, a => a._company, (a, b) => a.Company = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticExternalMenuRelations.CompanyEntityUsingParentCompanyIdStatic, ()=>new ExternalMenuRelations().CompanyEntityUsingParentCompanyId, null, new int[] { (int)ExternalMenuFieldIndex.ParentCompanyId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.CompanyEntity);
				AddNavigatorMetaData<ExternalMenuEntity, ExternalSystemEntity>("ExternalSystem", "ExternalMenuCollection", (a, b) => a._externalSystem = b, a => a._externalSystem, (a, b) => a.ExternalSystem = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticExternalMenuRelations.ExternalSystemEntityUsingExternalSystemIdStatic, ()=>new ExternalMenuRelations().ExternalSystemEntityUsingExternalSystemId, null, new int[] { (int)ExternalMenuFieldIndex.ExternalSystemId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ExternalSystemEntity);
			}
		}

		/// <summary>Static ctor</summary>
		static ExternalMenuEntity()
		{
		}

		/// <summary> CTor</summary>
		public ExternalMenuEntity()
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public ExternalMenuEntity(IEntityFields2 fields)
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this ExternalMenuEntity</param>
		public ExternalMenuEntity(IValidator validator)
		{
			InitClassEmpty(validator, null);
		}

		/// <summary> CTor</summary>
		/// <param name="externalMenuId">PK value for ExternalMenu which data should be fetched into this ExternalMenu object</param>
		public ExternalMenuEntity(System.Int32 externalMenuId) : this(externalMenuId, null)
		{
		}

		/// <summary> CTor</summary>
		/// <param name="externalMenuId">PK value for ExternalMenu which data should be fetched into this ExternalMenu object</param>
		/// <param name="validator">The custom validator object for this ExternalMenuEntity</param>
		public ExternalMenuEntity(System.Int32 externalMenuId, IValidator validator)
		{
			InitClassEmpty(validator, null);
			this.ExternalMenuId = externalMenuId;
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ExternalMenuEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'ExternalProduct' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoExternalProductCollection() { return CreateRelationInfoForNavigator("ExternalProductCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Company' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoCompany() { return CreateRelationInfoForNavigator("Company"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'ExternalSystem' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoExternalSystem() { return CreateRelationInfoForNavigator("ExternalSystem"); }
		
		/// <inheritdoc/>
		protected override EntityStaticMetaDataBase GetEntityStaticMetaData() {	return _staticMetaData; }

		/// <summary>Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		/// <summary>Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this ExternalMenuEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary>The relations object holding all relations of this entity with other entity classes.</summary>
		public static ExternalMenuRelations Relations { get { return _relationsFactory; } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'ExternalProduct' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathExternalProductCollection { get { return _staticMetaData.GetPrefetchPathElement("ExternalProductCollection", CommonEntityBase.CreateEntityCollection<ExternalProductEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Company' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathCompanyEntity { get { return _staticMetaData.GetPrefetchPathElement("Company", CommonEntityBase.CreateEntityCollection<CompanyEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'ExternalSystem' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathExternalSystemEntity { get { return _staticMetaData.GetPrefetchPathElement("ExternalSystem", CommonEntityBase.CreateEntityCollection<ExternalSystemEntity>()); } }

		/// <summary>The ExternalMenuId property of the Entity ExternalMenu<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalMenu"."ExternalMenuId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 ExternalMenuId
		{
			get { return (System.Int32)GetValue((int)ExternalMenuFieldIndex.ExternalMenuId, true); }
			set { SetValue((int)ExternalMenuFieldIndex.ExternalMenuId, value); }		}

		/// <summary>The ExternalSystemId property of the Entity ExternalMenu<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalMenu"."ExternalSystemId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ExternalSystemId
		{
			get { return (System.Int32)GetValue((int)ExternalMenuFieldIndex.ExternalSystemId, true); }
			set	{ SetValue((int)ExternalMenuFieldIndex.ExternalSystemId, value); }
		}

		/// <summary>The Id property of the Entity ExternalMenu<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalMenu"."Id".<br/>Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 50.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Id
		{
			get { return (System.String)GetValue((int)ExternalMenuFieldIndex.Id, true); }
			set	{ SetValue((int)ExternalMenuFieldIndex.Id, value); }
		}

		/// <summary>The Name property of the Entity ExternalMenu<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalMenu"."Name".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 256.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)ExternalMenuFieldIndex.Name, true); }
			set	{ SetValue((int)ExternalMenuFieldIndex.Name, value); }
		}

		/// <summary>The CreatedUTC property of the Entity ExternalMenu<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalMenu"."CreatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime CreatedUTC
		{
			get { return (System.DateTime)GetValue((int)ExternalMenuFieldIndex.CreatedUTC, true); }
			set	{ SetValue((int)ExternalMenuFieldIndex.CreatedUTC, value); }
		}

		/// <summary>The UpdateUTC property of the Entity ExternalMenu<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalMenu"."UpdateUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdateUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ExternalMenuFieldIndex.UpdateUTC, false); }
			set	{ SetValue((int)ExternalMenuFieldIndex.UpdateUTC, value); }
		}

		/// <summary>The ParentCompanyId property of the Entity ExternalMenu<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalMenu"."ParentCompanyId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ParentCompanyId
		{
			get { return (System.Int32)GetValue((int)ExternalMenuFieldIndex.ParentCompanyId, true); }
			set	{ SetValue((int)ExternalMenuFieldIndex.ParentCompanyId, value); }
		}

		/// <summary>Gets the EntityCollection with the related entities of type 'ExternalProductEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(ExternalProductEntity))]
		public virtual EntityCollection<ExternalProductEntity> ExternalProductCollection { get { return GetOrCreateEntityCollection<ExternalProductEntity, ExternalProductEntityFactory>("ExternalMenu", true, false, ref _externalProductCollection); } }

		/// <summary>Gets / sets related entity of type 'CompanyEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual CompanyEntity Company
		{
			get { return _company; }
			set { SetSingleRelatedEntityNavigator(value, "Company"); }
		}

		/// <summary>Gets / sets related entity of type 'ExternalSystemEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual ExternalSystemEntity ExternalSystem
		{
			get { return _externalSystem; }
			set { SetSingleRelatedEntityNavigator(value, "ExternalSystem"); }
		}


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen
{
	public enum ExternalMenuFieldIndex
	{
		///<summary>ExternalMenuId. </summary>
		ExternalMenuId,
		///<summary>ExternalSystemId. </summary>
		ExternalSystemId,
		///<summary>Id. </summary>
		Id,
		///<summary>Name. </summary>
		Name,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdateUTC. </summary>
		UpdateUTC,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		/// <summary></summary>
		AmountOfFields
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: ExternalMenu. </summary>
	public partial class ExternalMenuRelations: RelationFactory
	{
		#region Custom Relation code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomRelationCode
        private const ReferentialConstraintDeleteRule DeleteRuleForExternalProductEntityUsingExternalMenuId = ReferentialConstraintDeleteRule.Cascade;
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		
		// private const ReferentialConstraintDeleteRule DeleteRuleForExternalProductEntityUsingExternalMenuId = ReferentialConstraintDeleteRule


		/// <summary>Gets all relations and delete rules of the ExternalMenuEntity as a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects.</summary>
		/// <returns>a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects</returns>
		public virtual Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> GetRelationsAndDeleteRules()
		{
			Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> toReturn = new Dictionary<IEntityRelation, ReferentialConstraintDeleteRule>();
			toReturn.Add(this.ExternalProductEntityUsingExternalMenuId, ExternalMenuRelations.DeleteRuleForExternalProductEntityUsingExternalMenuId);
			return toReturn;
		}		
	
	
		/// <summary>Returns a new IEntityRelation object, between ExternalMenuEntity and ExternalProductEntity over the 1:n relation they have, using the relation between the fields: ExternalMenu.ExternalMenuId - ExternalProduct.ExternalMenuId</summary>
		public virtual IEntityRelation ExternalProductEntityUsingExternalMenuId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "ExternalProductCollection", true, new[] { ExternalMenuFields.ExternalMenuId, ExternalProductFields.ExternalMenuId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between ExternalMenuEntity and CompanyEntity over the m:1 relation they have, using the relation between the fields: ExternalMenu.ParentCompanyId - Company.CompanyId</summary>
		public virtual IEntityRelation CompanyEntityUsingParentCompanyId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Company", false, new[] { CompanyFields.CompanyId, ExternalMenuFields.ParentCompanyId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between ExternalMenuEntity and ExternalSystemEntity over the m:1 relation they have, using the relation between the fields: ExternalMenu.ExternalSystemId - ExternalSystem.ExternalSystemId</summary>
		public virtual IEntityRelation ExternalSystemEntityUsingExternalSystemId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "ExternalSystem", false, new[] { ExternalSystemFields.ExternalSystemId, ExternalMenuFields.ExternalSystemId }); }
		}

	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticExternalMenuRelations
	{
		internal static readonly IEntityRelation ExternalProductEntityUsingExternalMenuIdStatic = new ExternalMenuRelations().ExternalProductEntityUsingExternalMenuId;
		internal static readonly IEntityRelation CompanyEntityUsingParentCompanyIdStatic = new ExternalMenuRelations().CompanyEntityUsingParentCompanyId;
		internal static readonly IEntityRelation ExternalSystemEntityUsingExternalSystemIdStatic = new ExternalMenuRelations().ExternalSystemEntityUsingExternalSystemId;

		/// <summary>CTor</summary>
		static StaticExternalMenuRelations() { }
	}
}

