﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 5.8
// Code is generated on: 
// Code is generated using templates: Crave.TemplateBindings
// Templates vendor: Crave Interactive
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.FactoryClasses;
using Crave.Ordering.Shared.Data.LLBLGen.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

// ReSharper disable All
namespace Crave.Ordering.Shared.Data.LLBLGen.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'ExternalSubProduct'.<br/><br/></summary>
	[Serializable]
	public partial class ExternalSubProductEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END
	
	{
		private ExternalProductEntity _externalProduct;
		private ExternalProductEntity _externalProduct1;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		private static ExternalSubProductEntityStaticMetaData _staticMetaData = new ExternalSubProductEntityStaticMetaData();
		private static ExternalSubProductRelations _relationsFactory = new ExternalSubProductRelations();		

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name ExternalProduct</summary>
			public static readonly string ExternalProduct = "ExternalProduct";
			/// <summary>Member name ExternalProduct1</summary>
			public static readonly string ExternalProduct1 = "ExternalProduct1";
		}

		/// <summary>Static meta-data storage for navigator related information</summary>
		protected class ExternalSubProductEntityStaticMetaData : EntityStaticMetaDataBase
		{
			public ExternalSubProductEntityStaticMetaData()
			{
				SetEntityCoreInfo("ExternalSubProductEntity", InheritanceHierarchyType.None, false, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ExternalSubProductEntity, typeof(ExternalSubProductEntity), typeof(ExternalSubProductEntityFactory), false);
				AddNavigatorMetaData<ExternalSubProductEntity, ExternalProductEntity>("ExternalProduct", "ExternalSubProductCollection", (a, b) => a._externalProduct = b, a => a._externalProduct, (a, b) => a.ExternalProduct = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticExternalSubProductRelations.ExternalProductEntityUsingExternalProductIdStatic, ()=>new ExternalSubProductRelations().ExternalProductEntityUsingExternalProductId, null, new int[] { (int)ExternalSubProductFieldIndex.ExternalProductId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ExternalProductEntity);
				AddNavigatorMetaData<ExternalSubProductEntity, ExternalProductEntity>("ExternalProduct1", "ExternalSubProductCollection1", (a, b) => a._externalProduct1 = b, a => a._externalProduct1, (a, b) => a.ExternalProduct1 = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticExternalSubProductRelations.ExternalProductEntityUsingExternalSubProductIdStatic, ()=>new ExternalSubProductRelations().ExternalProductEntityUsingExternalSubProductId, null, new int[] { (int)ExternalSubProductFieldIndex.ExternalSubProductId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ExternalProductEntity);
			}
		}

		/// <summary>Static ctor</summary>
		static ExternalSubProductEntity()
		{
		}

		/// <summary> CTor</summary>
		public ExternalSubProductEntity()
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public ExternalSubProductEntity(IEntityFields2 fields)
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this ExternalSubProductEntity</param>
		public ExternalSubProductEntity(IValidator validator)
		{
			InitClassEmpty(validator, null);
		}

		/// <summary> CTor</summary>
		/// <param name="externalProductExternalProductId">PK value for ExternalSubProduct which data should be fetched into this ExternalSubProduct object</param>
		public ExternalSubProductEntity(System.Int32 externalProductExternalProductId) : this(externalProductExternalProductId, null)
		{
		}

		/// <summary> CTor</summary>
		/// <param name="externalProductExternalProductId">PK value for ExternalSubProduct which data should be fetched into this ExternalSubProduct object</param>
		/// <param name="validator">The custom validator object for this ExternalSubProductEntity</param>
		public ExternalSubProductEntity(System.Int32 externalProductExternalProductId, IValidator validator)
		{
			InitClassEmpty(validator, null);
			this.ExternalProductExternalProductId = externalProductExternalProductId;
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ExternalSubProductEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'ExternalProduct' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoExternalProduct() { return CreateRelationInfoForNavigator("ExternalProduct"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'ExternalProduct' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoExternalProduct1() { return CreateRelationInfoForNavigator("ExternalProduct1"); }
		
		/// <inheritdoc/>
		protected override EntityStaticMetaDataBase GetEntityStaticMetaData() {	return _staticMetaData; }

		/// <summary>Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		/// <summary>Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this ExternalSubProductEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary>The relations object holding all relations of this entity with other entity classes.</summary>
		public static ExternalSubProductRelations Relations { get { return _relationsFactory; } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'ExternalProduct' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathExternalProductEntity { get { return _staticMetaData.GetPrefetchPathElement("ExternalProduct", CommonEntityBase.CreateEntityCollection<ExternalProductEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'ExternalProduct' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathExternalProduct1Entity { get { return _staticMetaData.GetPrefetchPathElement("ExternalProduct1", CommonEntityBase.CreateEntityCollection<ExternalProductEntity>()); } }

		/// <summary>The ExternalProductExternalProductId property of the Entity ExternalSubProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalSubProduct"."ExternalProductExternalProductId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 ExternalProductExternalProductId
		{
			get { return (System.Int32)GetValue((int)ExternalSubProductFieldIndex.ExternalProductExternalProductId, true); }
			set { SetValue((int)ExternalSubProductFieldIndex.ExternalProductExternalProductId, value); }		}

		/// <summary>The ExternalProductId property of the Entity ExternalSubProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalSubProduct"."ExternalProductId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ExternalProductId
		{
			get { return (System.Int32)GetValue((int)ExternalSubProductFieldIndex.ExternalProductId, true); }
			set	{ SetValue((int)ExternalSubProductFieldIndex.ExternalProductId, value); }
		}

		/// <summary>The ExternalSubProductId property of the Entity ExternalSubProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalSubProduct"."ExternalSubProductId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ExternalSubProductId
		{
			get { return (System.Int32)GetValue((int)ExternalSubProductFieldIndex.ExternalSubProductId, true); }
			set	{ SetValue((int)ExternalSubProductFieldIndex.ExternalSubProductId, value); }
		}

		/// <summary>The CreatedUTC property of the Entity ExternalSubProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalSubProduct"."CreatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime CreatedUTC
		{
			get { return (System.DateTime)GetValue((int)ExternalSubProductFieldIndex.CreatedUTC, true); }
			set	{ SetValue((int)ExternalSubProductFieldIndex.CreatedUTC, value); }
		}

		/// <summary>The UpdatedUTC property of the Entity ExternalSubProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalSubProduct"."UpdatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ExternalSubProductFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)ExternalSubProductFieldIndex.UpdatedUTC, value); }
		}

		/// <summary>Gets / sets related entity of type 'ExternalProductEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual ExternalProductEntity ExternalProduct
		{
			get { return _externalProduct; }
			set { SetSingleRelatedEntityNavigator(value, "ExternalProduct"); }
		}

		/// <summary>Gets / sets related entity of type 'ExternalProductEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual ExternalProductEntity ExternalProduct1
		{
			get { return _externalProduct1; }
			set { SetSingleRelatedEntityNavigator(value, "ExternalProduct1"); }
		}


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen
{
	public enum ExternalSubProductFieldIndex
	{
		///<summary>ExternalProductExternalProductId. </summary>
		ExternalProductExternalProductId,
		///<summary>ExternalProductId. </summary>
		ExternalProductId,
		///<summary>ExternalSubProductId. </summary>
		ExternalSubProductId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: ExternalSubProduct. </summary>
	public partial class ExternalSubProductRelations: RelationFactory
	{
		#region Custom Relation code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomRelationCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		


		/// <summary>Gets all relations and delete rules of the ExternalSubProductEntity as a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects.</summary>
		/// <returns>a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects</returns>
		public virtual Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> GetRelationsAndDeleteRules()
		{
			Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> toReturn = new Dictionary<IEntityRelation, ReferentialConstraintDeleteRule>();
			return toReturn;
		}		
	
	

		/// <summary>Returns a new IEntityRelation object, between ExternalSubProductEntity and ExternalProductEntity over the m:1 relation they have, using the relation between the fields: ExternalSubProduct.ExternalProductId - ExternalProduct.ExternalProductId</summary>
		public virtual IEntityRelation ExternalProductEntityUsingExternalProductId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "ExternalProduct", false, new[] { ExternalProductFields.ExternalProductId, ExternalSubProductFields.ExternalProductId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between ExternalSubProductEntity and ExternalProductEntity over the m:1 relation they have, using the relation between the fields: ExternalSubProduct.ExternalSubProductId - ExternalProduct.ExternalProductId</summary>
		public virtual IEntityRelation ExternalProductEntityUsingExternalSubProductId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "ExternalProduct1", false, new[] { ExternalProductFields.ExternalProductId, ExternalSubProductFields.ExternalSubProductId }); }
		}

	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticExternalSubProductRelations
	{
		internal static readonly IEntityRelation ExternalProductEntityUsingExternalProductIdStatic = new ExternalSubProductRelations().ExternalProductEntityUsingExternalProductId;
		internal static readonly IEntityRelation ExternalProductEntityUsingExternalSubProductIdStatic = new ExternalSubProductRelations().ExternalProductEntityUsingExternalSubProductId;

		/// <summary>CTor</summary>
		static StaticExternalSubProductRelations() { }
	}
}

