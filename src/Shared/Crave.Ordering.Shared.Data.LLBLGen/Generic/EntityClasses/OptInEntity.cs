﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 5.8
// Code is generated on: 
// Code is generated using templates: Crave.TemplateBindings
// Templates vendor: Crave Interactive
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.FactoryClasses;
using Crave.Ordering.Shared.Data.LLBLGen.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

// ReSharper disable All
namespace Crave.Ordering.Shared.Data.LLBLGen.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'OptIn'.<br/><br/></summary>
	[Serializable]
	public partial class OptInEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END
	
	{
		private CompanyEntity _company;
		private OutletEntity _outlet;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		private static OptInEntityStaticMetaData _staticMetaData = new OptInEntityStaticMetaData();
		private static OptInRelations _relationsFactory = new OptInRelations();		

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Company</summary>
			public static readonly string Company = "Company";
			/// <summary>Member name Outlet</summary>
			public static readonly string Outlet = "Outlet";
		}

		/// <summary>Static meta-data storage for navigator related information</summary>
		protected class OptInEntityStaticMetaData : EntityStaticMetaDataBase
		{
			public OptInEntityStaticMetaData()
			{
				SetEntityCoreInfo("OptInEntity", InheritanceHierarchyType.None, false, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.OptInEntity, typeof(OptInEntity), typeof(OptInEntityFactory), false);
				AddNavigatorMetaData<OptInEntity, CompanyEntity>("Company", "OptInCollection", (a, b) => a._company = b, a => a._company, (a, b) => a.Company = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticOptInRelations.CompanyEntityUsingParentCompanyIdStatic, ()=>new OptInRelations().CompanyEntityUsingParentCompanyId, null, new int[] { (int)OptInFieldIndex.ParentCompanyId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.CompanyEntity);
				AddNavigatorMetaData<OptInEntity, OutletEntity>("Outlet", "OptInCollection", (a, b) => a._outlet = b, a => a._outlet, (a, b) => a.Outlet = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticOptInRelations.OutletEntityUsingOutletIdStatic, ()=>new OptInRelations().OutletEntityUsingOutletId, null, new int[] { (int)OptInFieldIndex.OutletId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.OutletEntity);
			}
		}

		/// <summary>Static ctor</summary>
		static OptInEntity()
		{
		}

		/// <summary> CTor</summary>
		public OptInEntity()
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public OptInEntity(IEntityFields2 fields)
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this OptInEntity</param>
		public OptInEntity(IValidator validator)
		{
			InitClassEmpty(validator, null);
		}

		/// <summary> CTor</summary>
		/// <param name="optInId">PK value for OptIn which data should be fetched into this OptIn object</param>
		public OptInEntity(System.Int32 optInId) : this(optInId, null)
		{
		}

		/// <summary> CTor</summary>
		/// <param name="optInId">PK value for OptIn which data should be fetched into this OptIn object</param>
		/// <param name="validator">The custom validator object for this OptInEntity</param>
		public OptInEntity(System.Int32 optInId, IValidator validator)
		{
			InitClassEmpty(validator, null);
			this.OptInId = optInId;
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected OptInEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Company' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoCompany() { return CreateRelationInfoForNavigator("Company"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Outlet' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoOutlet() { return CreateRelationInfoForNavigator("Outlet"); }
		
		/// <inheritdoc/>
		protected override EntityStaticMetaDataBase GetEntityStaticMetaData() {	return _staticMetaData; }

		/// <summary>Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		/// <summary>Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this OptInEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary>The relations object holding all relations of this entity with other entity classes.</summary>
		public static OptInRelations Relations { get { return _relationsFactory; } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Company' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathCompanyEntity { get { return _staticMetaData.GetPrefetchPathElement("Company", CommonEntityBase.CreateEntityCollection<CompanyEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Outlet' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathOutletEntity { get { return _staticMetaData.GetPrefetchPathElement("Outlet", CommonEntityBase.CreateEntityCollection<OutletEntity>()); } }

		/// <summary>The OptInId property of the Entity OptIn<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OptIn"."OptInId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 OptInId
		{
			get { return (System.Int32)GetValue((int)OptInFieldIndex.OptInId, true); }
			set { SetValue((int)OptInFieldIndex.OptInId, value); }		}

		/// <summary>The ParentCompanyId property of the Entity OptIn<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OptIn"."ParentCompanyId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ParentCompanyId
		{
			get { return (System.Int32)GetValue((int)OptInFieldIndex.ParentCompanyId, true); }
			set	{ SetValue((int)OptInFieldIndex.ParentCompanyId, value); }
		}

		/// <summary>The OutletId property of the Entity OptIn<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OptIn"."OutletId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 OutletId
		{
			get { return (System.Int32)GetValue((int)OptInFieldIndex.OutletId, true); }
			set	{ SetValue((int)OptInFieldIndex.OutletId, value); }
		}

		/// <summary>The Email property of the Entity OptIn<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OptIn"."Email".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 250.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Email
		{
			get { return (System.String)GetValue((int)OptInFieldIndex.Email, true); }
			set	{ SetValue((int)OptInFieldIndex.Email, value); }
		}

		/// <summary>The PhoneNumber property of the Entity OptIn<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OptIn"."PhoneNumber".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 250.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PhoneNumber
		{
			get { return (System.String)GetValue((int)OptInFieldIndex.PhoneNumber, true); }
			set	{ SetValue((int)OptInFieldIndex.PhoneNumber, value); }
		}

		/// <summary>The CreatedUTC property of the Entity OptIn<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OptIn"."CreatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime CreatedUTC
		{
			get { return (System.DateTime)GetValue((int)OptInFieldIndex.CreatedUTC, true); }
			set	{ SetValue((int)OptInFieldIndex.CreatedUTC, value); }
		}

		/// <summary>The CreatedBy property of the Entity OptIn<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OptIn"."CreatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)OptInFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)OptInFieldIndex.CreatedBy, value); }
		}

		/// <summary>The UpdatedUTC property of the Entity OptIn<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OptIn"."UpdatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)OptInFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)OptInFieldIndex.UpdatedUTC, value); }
		}

		/// <summary>The UpdatedBy property of the Entity OptIn<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OptIn"."UpdatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)OptInFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)OptInFieldIndex.UpdatedBy, value); }
		}

		/// <summary>The CustomerName property of the Entity OptIn<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OptIn"."CustomerName".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 250.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CustomerName
		{
			get { return (System.String)GetValue((int)OptInFieldIndex.CustomerName, true); }
			set	{ SetValue((int)OptInFieldIndex.CustomerName, value); }
		}

		/// <summary>Gets / sets related entity of type 'CompanyEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual CompanyEntity Company
		{
			get { return _company; }
			set { SetSingleRelatedEntityNavigator(value, "Company"); }
		}

		/// <summary>Gets / sets related entity of type 'OutletEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual OutletEntity Outlet
		{
			get { return _outlet; }
			set { SetSingleRelatedEntityNavigator(value, "Outlet"); }
		}


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen
{
	public enum OptInFieldIndex
	{
		///<summary>OptInId. </summary>
		OptInId,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>OutletId. </summary>
		OutletId,
		///<summary>Email. </summary>
		Email,
		///<summary>PhoneNumber. </summary>
		PhoneNumber,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CustomerName. </summary>
		CustomerName,
		/// <summary></summary>
		AmountOfFields
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: OptIn. </summary>
	public partial class OptInRelations: RelationFactory
	{
		#region Custom Relation code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomRelationCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		


		/// <summary>Gets all relations and delete rules of the OptInEntity as a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects.</summary>
		/// <returns>a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects</returns>
		public virtual Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> GetRelationsAndDeleteRules()
		{
			Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> toReturn = new Dictionary<IEntityRelation, ReferentialConstraintDeleteRule>();
			return toReturn;
		}		
	
	

		/// <summary>Returns a new IEntityRelation object, between OptInEntity and CompanyEntity over the m:1 relation they have, using the relation between the fields: OptIn.ParentCompanyId - Company.CompanyId</summary>
		public virtual IEntityRelation CompanyEntityUsingParentCompanyId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Company", false, new[] { CompanyFields.CompanyId, OptInFields.ParentCompanyId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between OptInEntity and OutletEntity over the m:1 relation they have, using the relation between the fields: OptIn.OutletId - Outlet.OutletId</summary>
		public virtual IEntityRelation OutletEntityUsingOutletId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Outlet", false, new[] { OutletFields.OutletId, OptInFields.OutletId }); }
		}

	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticOptInRelations
	{
		internal static readonly IEntityRelation CompanyEntityUsingParentCompanyIdStatic = new OptInRelations().CompanyEntityUsingParentCompanyId;
		internal static readonly IEntityRelation OutletEntityUsingOutletIdStatic = new OptInRelations().OutletEntityUsingOutletId;

		/// <summary>CTor</summary>
		static StaticOptInRelations() { }
	}
}

