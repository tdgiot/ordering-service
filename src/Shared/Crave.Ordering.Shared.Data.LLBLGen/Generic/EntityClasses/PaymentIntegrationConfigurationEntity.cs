﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 5.8
// Code is generated on: 
// Code is generated using templates: Crave.TemplateBindings
// Templates vendor: Crave Interactive
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.FactoryClasses;
using Crave.Ordering.Shared.Data.LLBLGen.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

// ReSharper disable All
namespace Crave.Ordering.Shared.Data.LLBLGen.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'PaymentIntegrationConfiguration'.<br/><br/></summary>
	[Serializable]
	public partial class PaymentIntegrationConfigurationEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END
	
	{
		private EntityCollection<CheckoutMethodEntity> _checkoutMethodCollection;
		private EntityCollection<PaymentTransactionEntity> _paymentTransactionCollection;
		private CompanyEntity _company;
		private PaymentProviderEntity _paymentProvider;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		private static PaymentIntegrationConfigurationEntityStaticMetaData _staticMetaData = new PaymentIntegrationConfigurationEntityStaticMetaData();
		private static PaymentIntegrationConfigurationRelations _relationsFactory = new PaymentIntegrationConfigurationRelations();		

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Company</summary>
			public static readonly string Company = "Company";
			/// <summary>Member name PaymentProvider</summary>
			public static readonly string PaymentProvider = "PaymentProvider";
			/// <summary>Member name CheckoutMethodCollection</summary>
			public static readonly string CheckoutMethodCollection = "CheckoutMethodCollection";
			/// <summary>Member name PaymentTransactionCollection</summary>
			public static readonly string PaymentTransactionCollection = "PaymentTransactionCollection";
		}

		/// <summary>Static meta-data storage for navigator related information</summary>
		protected class PaymentIntegrationConfigurationEntityStaticMetaData : EntityStaticMetaDataBase
		{
			public PaymentIntegrationConfigurationEntityStaticMetaData()
			{
				SetEntityCoreInfo("PaymentIntegrationConfigurationEntity", InheritanceHierarchyType.None, false, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.PaymentIntegrationConfigurationEntity, typeof(PaymentIntegrationConfigurationEntity), typeof(PaymentIntegrationConfigurationEntityFactory), false);
				AddNavigatorMetaData<PaymentIntegrationConfigurationEntity, EntityCollection<CheckoutMethodEntity>>("CheckoutMethodCollection", a => a._checkoutMethodCollection, (a, b) => a._checkoutMethodCollection = b, a => a.CheckoutMethodCollection, () => new PaymentIntegrationConfigurationRelations().CheckoutMethodEntityUsingPaymentIntegrationConfigurationId, typeof(CheckoutMethodEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.CheckoutMethodEntity);
				AddNavigatorMetaData<PaymentIntegrationConfigurationEntity, EntityCollection<PaymentTransactionEntity>>("PaymentTransactionCollection", a => a._paymentTransactionCollection, (a, b) => a._paymentTransactionCollection = b, a => a.PaymentTransactionCollection, () => new PaymentIntegrationConfigurationRelations().PaymentTransactionEntityUsingPaymentIntegrationConfigurationId, typeof(PaymentTransactionEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.PaymentTransactionEntity);
				AddNavigatorMetaData<PaymentIntegrationConfigurationEntity, CompanyEntity>("Company", "PaymentIntegrationConfigurationCollection", (a, b) => a._company = b, a => a._company, (a, b) => a.Company = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticPaymentIntegrationConfigurationRelations.CompanyEntityUsingCompanyIdStatic, ()=>new PaymentIntegrationConfigurationRelations().CompanyEntityUsingCompanyId, null, new int[] { (int)PaymentIntegrationConfigurationFieldIndex.CompanyId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.CompanyEntity);
				AddNavigatorMetaData<PaymentIntegrationConfigurationEntity, PaymentProviderEntity>("PaymentProvider", "PaymentIntegrationConfigurationCollection", (a, b) => a._paymentProvider = b, a => a._paymentProvider, (a, b) => a.PaymentProvider = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticPaymentIntegrationConfigurationRelations.PaymentProviderEntityUsingPaymentProviderIdStatic, ()=>new PaymentIntegrationConfigurationRelations().PaymentProviderEntityUsingPaymentProviderId, null, new int[] { (int)PaymentIntegrationConfigurationFieldIndex.PaymentProviderId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.PaymentProviderEntity);
			}
		}

		/// <summary>Static ctor</summary>
		static PaymentIntegrationConfigurationEntity()
		{
		}

		/// <summary> CTor</summary>
		public PaymentIntegrationConfigurationEntity()
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public PaymentIntegrationConfigurationEntity(IEntityFields2 fields)
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this PaymentIntegrationConfigurationEntity</param>
		public PaymentIntegrationConfigurationEntity(IValidator validator)
		{
			InitClassEmpty(validator, null);
		}

		/// <summary> CTor</summary>
		/// <param name="paymentIntegrationConfigurationId">PK value for PaymentIntegrationConfiguration which data should be fetched into this PaymentIntegrationConfiguration object</param>
		public PaymentIntegrationConfigurationEntity(System.Int32 paymentIntegrationConfigurationId) : this(paymentIntegrationConfigurationId, null)
		{
		}

		/// <summary> CTor</summary>
		/// <param name="paymentIntegrationConfigurationId">PK value for PaymentIntegrationConfiguration which data should be fetched into this PaymentIntegrationConfiguration object</param>
		/// <param name="validator">The custom validator object for this PaymentIntegrationConfigurationEntity</param>
		public PaymentIntegrationConfigurationEntity(System.Int32 paymentIntegrationConfigurationId, IValidator validator)
		{
			InitClassEmpty(validator, null);
			this.PaymentIntegrationConfigurationId = paymentIntegrationConfigurationId;
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected PaymentIntegrationConfigurationEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'CheckoutMethod' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoCheckoutMethodCollection() { return CreateRelationInfoForNavigator("CheckoutMethodCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'PaymentTransaction' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoPaymentTransactionCollection() { return CreateRelationInfoForNavigator("PaymentTransactionCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Company' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoCompany() { return CreateRelationInfoForNavigator("Company"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'PaymentProvider' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoPaymentProvider() { return CreateRelationInfoForNavigator("PaymentProvider"); }
		
		/// <inheritdoc/>
		protected override EntityStaticMetaDataBase GetEntityStaticMetaData() {	return _staticMetaData; }

		/// <summary>Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		/// <summary>Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this PaymentIntegrationConfigurationEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary>The relations object holding all relations of this entity with other entity classes.</summary>
		public static PaymentIntegrationConfigurationRelations Relations { get { return _relationsFactory; } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'CheckoutMethod' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathCheckoutMethodCollection { get { return _staticMetaData.GetPrefetchPathElement("CheckoutMethodCollection", CommonEntityBase.CreateEntityCollection<CheckoutMethodEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'PaymentTransaction' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathPaymentTransactionCollection { get { return _staticMetaData.GetPrefetchPathElement("PaymentTransactionCollection", CommonEntityBase.CreateEntityCollection<PaymentTransactionEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Company' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathCompanyEntity { get { return _staticMetaData.GetPrefetchPathElement("Company", CommonEntityBase.CreateEntityCollection<CompanyEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'PaymentProvider' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathPaymentProviderEntity { get { return _staticMetaData.GetPrefetchPathElement("PaymentProvider", CommonEntityBase.CreateEntityCollection<PaymentProviderEntity>()); } }

		/// <summary>The PaymentIntegrationConfigurationId property of the Entity PaymentIntegrationConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentIntegrationConfiguration"."PaymentIntegrationConfigurationId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 PaymentIntegrationConfigurationId
		{
			get { return (System.Int32)GetValue((int)PaymentIntegrationConfigurationFieldIndex.PaymentIntegrationConfigurationId, true); }
			set { SetValue((int)PaymentIntegrationConfigurationFieldIndex.PaymentIntegrationConfigurationId, value); }		}

		/// <summary>The PaymentProviderId property of the Entity PaymentIntegrationConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentIntegrationConfiguration"."PaymentProviderId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 PaymentProviderId
		{
			get { return (System.Int32)GetValue((int)PaymentIntegrationConfigurationFieldIndex.PaymentProviderId, true); }
			set	{ SetValue((int)PaymentIntegrationConfigurationFieldIndex.PaymentProviderId, value); }
		}

		/// <summary>The CompanyId property of the Entity PaymentIntegrationConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentIntegrationConfiguration"."CompanyId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CompanyId
		{
			get { return (System.Int32)GetValue((int)PaymentIntegrationConfigurationFieldIndex.CompanyId, true); }
			set	{ SetValue((int)PaymentIntegrationConfigurationFieldIndex.CompanyId, value); }
		}

		/// <summary>The DisplayName property of the Entity PaymentIntegrationConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentIntegrationConfiguration"."DisplayName".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 250.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String DisplayName
		{
			get { return (System.String)GetValue((int)PaymentIntegrationConfigurationFieldIndex.DisplayName, true); }
			set	{ SetValue((int)PaymentIntegrationConfigurationFieldIndex.DisplayName, value); }
		}

		/// <summary>The MerchantName property of the Entity PaymentIntegrationConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentIntegrationConfiguration"."MerchantName".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 250.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String MerchantName
		{
			get { return (System.String)GetValue((int)PaymentIntegrationConfigurationFieldIndex.MerchantName, true); }
			set	{ SetValue((int)PaymentIntegrationConfigurationFieldIndex.MerchantName, value); }
		}

		/// <summary>The AdyenMerchantCode property of the Entity PaymentIntegrationConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentIntegrationConfiguration"."AdyenMerchantCode".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 250.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String AdyenMerchantCode
		{
			get { return (System.String)GetValue((int)PaymentIntegrationConfigurationFieldIndex.AdyenMerchantCode, true); }
			set	{ SetValue((int)PaymentIntegrationConfigurationFieldIndex.AdyenMerchantCode, value); }
		}

		/// <summary>The GooglePayMerchantIdentifier property of the Entity PaymentIntegrationConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentIntegrationConfiguration"."GooglePayMerchantIdentifier".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 250.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String GooglePayMerchantIdentifier
		{
			get { return (System.String)GetValue((int)PaymentIntegrationConfigurationFieldIndex.GooglePayMerchantIdentifier, true); }
			set	{ SetValue((int)PaymentIntegrationConfigurationFieldIndex.GooglePayMerchantIdentifier, value); }
		}

		/// <summary>The ApplePayMerchantIdentifier property of the Entity PaymentIntegrationConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentIntegrationConfiguration"."ApplePayMerchantIdentifier".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 250.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ApplePayMerchantIdentifier
		{
			get { return (System.String)GetValue((int)PaymentIntegrationConfigurationFieldIndex.ApplePayMerchantIdentifier, true); }
			set	{ SetValue((int)PaymentIntegrationConfigurationFieldIndex.ApplePayMerchantIdentifier, value); }
		}

		/// <summary>The CreatedUTC property of the Entity PaymentIntegrationConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentIntegrationConfiguration"."CreatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)PaymentIntegrationConfigurationFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)PaymentIntegrationConfigurationFieldIndex.CreatedUTC, value); }
		}

		/// <summary>The UpdatedUTC property of the Entity PaymentIntegrationConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentIntegrationConfiguration"."UpdatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)PaymentIntegrationConfigurationFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)PaymentIntegrationConfigurationFieldIndex.UpdatedUTC, value); }
		}

		/// <summary>The PaymentProcessorInfo property of the Entity PaymentIntegrationConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentIntegrationConfiguration"."PaymentProcessorInfo".<br/>Table field type characteristics (type, precision, scale, length): Text, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PaymentProcessorInfo
		{
			get { return (System.String)GetValue((int)PaymentIntegrationConfigurationFieldIndex.PaymentProcessorInfo, true); }
			set	{ SetValue((int)PaymentIntegrationConfigurationFieldIndex.PaymentProcessorInfo, value); }
		}

		/// <summary>The PaymentProcessorInfoFooter property of the Entity PaymentIntegrationConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentIntegrationConfiguration"."PaymentProcessorInfoFooter".<br/>Table field type characteristics (type, precision, scale, length): Text, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PaymentProcessorInfoFooter
		{
			get { return (System.String)GetValue((int)PaymentIntegrationConfigurationFieldIndex.PaymentProcessorInfoFooter, true); }
			set	{ SetValue((int)PaymentIntegrationConfigurationFieldIndex.PaymentProcessorInfoFooter, value); }
		}

		/// <summary>The AdyenAccountHolderCode property of the Entity PaymentIntegrationConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentIntegrationConfiguration"."AdyenAccountHolderCode".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 250.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String AdyenAccountHolderCode
		{
			get { return (System.String)GetValue((int)PaymentIntegrationConfigurationFieldIndex.AdyenAccountHolderCode, true); }
			set	{ SetValue((int)PaymentIntegrationConfigurationFieldIndex.AdyenAccountHolderCode, value); }
		}

		/// <summary>The AdyenAccountHolderName property of the Entity PaymentIntegrationConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentIntegrationConfiguration"."AdyenAccountHolderName".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 250.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String AdyenAccountHolderName
		{
			get { return (System.String)GetValue((int)PaymentIntegrationConfigurationFieldIndex.AdyenAccountHolderName, true); }
			set	{ SetValue((int)PaymentIntegrationConfigurationFieldIndex.AdyenAccountHolderName, value); }
		}

		/// <summary>The AdyenAccountCode property of the Entity PaymentIntegrationConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentIntegrationConfiguration"."AdyenAccountCode".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 250.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String AdyenAccountCode
		{
			get { return (System.String)GetValue((int)PaymentIntegrationConfigurationFieldIndex.AdyenAccountCode, true); }
			set	{ SetValue((int)PaymentIntegrationConfigurationFieldIndex.AdyenAccountCode, value); }
		}

		/// <summary>The AdyenAccountName property of the Entity PaymentIntegrationConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentIntegrationConfiguration"."AdyenAccountName".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 250.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String AdyenAccountName
		{
			get { return (System.String)GetValue((int)PaymentIntegrationConfigurationFieldIndex.AdyenAccountName, true); }
			set	{ SetValue((int)PaymentIntegrationConfigurationFieldIndex.AdyenAccountName, value); }
		}

		/// <summary>Gets the EntityCollection with the related entities of type 'CheckoutMethodEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(CheckoutMethodEntity))]
		public virtual EntityCollection<CheckoutMethodEntity> CheckoutMethodCollection { get { return GetOrCreateEntityCollection<CheckoutMethodEntity, CheckoutMethodEntityFactory>("PaymentIntegrationConfiguration", true, false, ref _checkoutMethodCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'PaymentTransactionEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(PaymentTransactionEntity))]
		public virtual EntityCollection<PaymentTransactionEntity> PaymentTransactionCollection { get { return GetOrCreateEntityCollection<PaymentTransactionEntity, PaymentTransactionEntityFactory>("PaymentIntegrationConfiguration", true, false, ref _paymentTransactionCollection); } }

		/// <summary>Gets / sets related entity of type 'CompanyEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual CompanyEntity Company
		{
			get { return _company; }
			set { SetSingleRelatedEntityNavigator(value, "Company"); }
		}

		/// <summary>Gets / sets related entity of type 'PaymentProviderEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual PaymentProviderEntity PaymentProvider
		{
			get { return _paymentProvider; }
			set { SetSingleRelatedEntityNavigator(value, "PaymentProvider"); }
		}


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen
{
	public enum PaymentIntegrationConfigurationFieldIndex
	{
		///<summary>PaymentIntegrationConfigurationId. </summary>
		PaymentIntegrationConfigurationId,
		///<summary>PaymentProviderId. </summary>
		PaymentProviderId,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>DisplayName. </summary>
		DisplayName,
		///<summary>MerchantName. </summary>
		MerchantName,
		///<summary>AdyenMerchantCode. </summary>
		AdyenMerchantCode,
		///<summary>GooglePayMerchantIdentifier. </summary>
		GooglePayMerchantIdentifier,
		///<summary>ApplePayMerchantIdentifier. </summary>
		ApplePayMerchantIdentifier,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>PaymentProcessorInfo. </summary>
		PaymentProcessorInfo,
		///<summary>PaymentProcessorInfoFooter. </summary>
		PaymentProcessorInfoFooter,
		///<summary>AdyenAccountHolderCode. </summary>
		AdyenAccountHolderCode,
		///<summary>AdyenAccountHolderName. </summary>
		AdyenAccountHolderName,
		///<summary>AdyenAccountCode. </summary>
		AdyenAccountCode,
		///<summary>AdyenAccountName. </summary>
		AdyenAccountName,
		/// <summary></summary>
		AmountOfFields
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: PaymentIntegrationConfiguration. </summary>
	public partial class PaymentIntegrationConfigurationRelations: RelationFactory
	{
		#region Custom Relation code
		
        // __LLBLGENPRO_USER_CODE_REGION_START CustomRelationCode
        private const ReferentialConstraintDeleteRule DeleteRuleForCheckoutMethodEntityUsingPaymentIntegrationConfigurationId = ReferentialConstraintDeleteRule.SetNull;
        private const ReferentialConstraintDeleteRule DeleteRuleForPaymentTransactionEntityUsingPaymentIntegrationConfigurationId = ReferentialConstraintDeleteRule.SetNull;
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		
		// private const ReferentialConstraintDeleteRule DeleteRuleForCheckoutMethodEntityUsingPaymentIntegrationConfigurationId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForPaymentTransactionEntityUsingPaymentIntegrationConfigurationId = ReferentialConstraintDeleteRule


		/// <summary>Gets all relations and delete rules of the PaymentIntegrationConfigurationEntity as a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects.</summary>
		/// <returns>a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects</returns>
		public virtual Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> GetRelationsAndDeleteRules()
		{
			Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> toReturn = new Dictionary<IEntityRelation, ReferentialConstraintDeleteRule>();
			toReturn.Add(this.CheckoutMethodEntityUsingPaymentIntegrationConfigurationId, PaymentIntegrationConfigurationRelations.DeleteRuleForCheckoutMethodEntityUsingPaymentIntegrationConfigurationId);
			toReturn.Add(this.PaymentTransactionEntityUsingPaymentIntegrationConfigurationId, PaymentIntegrationConfigurationRelations.DeleteRuleForPaymentTransactionEntityUsingPaymentIntegrationConfigurationId);
			return toReturn;
		}		
	
	
		/// <summary>Returns a new IEntityRelation object, between PaymentIntegrationConfigurationEntity and CheckoutMethodEntity over the 1:n relation they have, using the relation between the fields: PaymentIntegrationConfiguration.PaymentIntegrationConfigurationId - CheckoutMethod.PaymentIntegrationConfigurationId</summary>
		public virtual IEntityRelation CheckoutMethodEntityUsingPaymentIntegrationConfigurationId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "CheckoutMethodCollection", true, new[] { PaymentIntegrationConfigurationFields.PaymentIntegrationConfigurationId, CheckoutMethodFields.PaymentIntegrationConfigurationId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between PaymentIntegrationConfigurationEntity and PaymentTransactionEntity over the 1:n relation they have, using the relation between the fields: PaymentIntegrationConfiguration.PaymentIntegrationConfigurationId - PaymentTransaction.PaymentIntegrationConfigurationId</summary>
		public virtual IEntityRelation PaymentTransactionEntityUsingPaymentIntegrationConfigurationId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "PaymentTransactionCollection", true, new[] { PaymentIntegrationConfigurationFields.PaymentIntegrationConfigurationId, PaymentTransactionFields.PaymentIntegrationConfigurationId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between PaymentIntegrationConfigurationEntity and CompanyEntity over the m:1 relation they have, using the relation between the fields: PaymentIntegrationConfiguration.CompanyId - Company.CompanyId</summary>
		public virtual IEntityRelation CompanyEntityUsingCompanyId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Company", false, new[] { CompanyFields.CompanyId, PaymentIntegrationConfigurationFields.CompanyId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between PaymentIntegrationConfigurationEntity and PaymentProviderEntity over the m:1 relation they have, using the relation between the fields: PaymentIntegrationConfiguration.PaymentProviderId - PaymentProvider.PaymentProviderId</summary>
		public virtual IEntityRelation PaymentProviderEntityUsingPaymentProviderId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "PaymentProvider", false, new[] { PaymentProviderFields.PaymentProviderId, PaymentIntegrationConfigurationFields.PaymentProviderId }); }
		}

	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticPaymentIntegrationConfigurationRelations
	{
		internal static readonly IEntityRelation CheckoutMethodEntityUsingPaymentIntegrationConfigurationIdStatic = new PaymentIntegrationConfigurationRelations().CheckoutMethodEntityUsingPaymentIntegrationConfigurationId;
		internal static readonly IEntityRelation PaymentTransactionEntityUsingPaymentIntegrationConfigurationIdStatic = new PaymentIntegrationConfigurationRelations().PaymentTransactionEntityUsingPaymentIntegrationConfigurationId;
		internal static readonly IEntityRelation CompanyEntityUsingCompanyIdStatic = new PaymentIntegrationConfigurationRelations().CompanyEntityUsingCompanyId;
		internal static readonly IEntityRelation PaymentProviderEntityUsingPaymentProviderIdStatic = new PaymentIntegrationConfigurationRelations().PaymentProviderEntityUsingPaymentProviderId;

		/// <summary>CTor</summary>
		static StaticPaymentIntegrationConfigurationRelations() { }
	}
}

