﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 5.8
// Code is generated on: 
// Code is generated using templates: Crave.TemplateBindings
// Templates vendor: Crave Interactive
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.FactoryClasses;
using Crave.Ordering.Shared.Data.LLBLGen.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

// ReSharper disable All
namespace Crave.Ordering.Shared.Data.LLBLGen.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'Businesshour'.<br/><br/></summary>
	[Serializable]
	public partial class BusinesshourEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END
	
	{
		private CompanyEntity _company;
		private OutletEntity _outlet;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		private static BusinesshourEntityStaticMetaData _staticMetaData = new BusinesshourEntityStaticMetaData();
		private static BusinesshourRelations _relationsFactory = new BusinesshourRelations();		

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Company</summary>
			public static readonly string Company = "Company";
			/// <summary>Member name Outlet</summary>
			public static readonly string Outlet = "Outlet";
		}

		/// <summary>Static meta-data storage for navigator related information</summary>
		protected class BusinesshourEntityStaticMetaData : EntityStaticMetaDataBase
		{
			public BusinesshourEntityStaticMetaData()
			{
				SetEntityCoreInfo("BusinesshourEntity", InheritanceHierarchyType.None, false, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.BusinesshourEntity, typeof(BusinesshourEntity), typeof(BusinesshourEntityFactory), false);
				AddNavigatorMetaData<BusinesshourEntity, CompanyEntity>("Company", "BusinesshourCollection", (a, b) => a._company = b, a => a._company, (a, b) => a.Company = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticBusinesshourRelations.CompanyEntityUsingCompanyIdStatic, ()=>new BusinesshourRelations().CompanyEntityUsingCompanyId, null, new int[] { (int)BusinesshourFieldIndex.CompanyId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.CompanyEntity);
				AddNavigatorMetaData<BusinesshourEntity, OutletEntity>("Outlet", "BusinesshourCollection", (a, b) => a._outlet = b, a => a._outlet, (a, b) => a.Outlet = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticBusinesshourRelations.OutletEntityUsingOutletIdStatic, ()=>new BusinesshourRelations().OutletEntityUsingOutletId, null, new int[] { (int)BusinesshourFieldIndex.OutletId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.OutletEntity);
			}
		}

		/// <summary>Static ctor</summary>
		static BusinesshourEntity()
		{
		}

		/// <summary> CTor</summary>
		public BusinesshourEntity()
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public BusinesshourEntity(IEntityFields2 fields)
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this BusinesshourEntity</param>
		public BusinesshourEntity(IValidator validator)
		{
			InitClassEmpty(validator, null);
		}

		/// <summary> CTor</summary>
		/// <param name="businesshoursId">PK value for Businesshour which data should be fetched into this Businesshour object</param>
		public BusinesshourEntity(System.Int32 businesshoursId) : this(businesshoursId, null)
		{
		}

		/// <summary> CTor</summary>
		/// <param name="businesshoursId">PK value for Businesshour which data should be fetched into this Businesshour object</param>
		/// <param name="validator">The custom validator object for this BusinesshourEntity</param>
		public BusinesshourEntity(System.Int32 businesshoursId, IValidator validator)
		{
			InitClassEmpty(validator, null);
			this.BusinesshoursId = businesshoursId;
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected BusinesshourEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Company' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoCompany() { return CreateRelationInfoForNavigator("Company"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Outlet' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoOutlet() { return CreateRelationInfoForNavigator("Outlet"); }
		
		/// <inheritdoc/>
		protected override EntityStaticMetaDataBase GetEntityStaticMetaData() {	return _staticMetaData; }

		/// <summary>Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		/// <summary>Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this BusinesshourEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary>The relations object holding all relations of this entity with other entity classes.</summary>
		public static BusinesshourRelations Relations { get { return _relationsFactory; } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Company' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathCompanyEntity { get { return _staticMetaData.GetPrefetchPathElement("Company", CommonEntityBase.CreateEntityCollection<CompanyEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Outlet' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathOutletEntity { get { return _staticMetaData.GetPrefetchPathElement("Outlet", CommonEntityBase.CreateEntityCollection<OutletEntity>()); } }

		/// <summary>The BusinesshoursId property of the Entity Businesshour<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Businesshours"."BusinesshoursId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 BusinesshoursId
		{
			get { return (System.Int32)GetValue((int)BusinesshourFieldIndex.BusinesshoursId, true); }
			set { SetValue((int)BusinesshourFieldIndex.BusinesshoursId, value); }		}

		/// <summary>The CompanyId property of the Entity Businesshour<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Businesshours"."CompanyId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CompanyId
		{
			get { return (Nullable<System.Int32>)GetValue((int)BusinesshourFieldIndex.CompanyId, false); }
			set	{ SetValue((int)BusinesshourFieldIndex.CompanyId, value); }
		}

		/// <summary>The PointOfInterestId property of the Entity Businesshour<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Businesshours"."PointOfInterestId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> PointOfInterestId
		{
			get { return (Nullable<System.Int32>)GetValue((int)BusinesshourFieldIndex.PointOfInterestId, false); }
			set	{ SetValue((int)BusinesshourFieldIndex.PointOfInterestId, value); }
		}

		/// <summary>The DayOfWeekAndTime property of the Entity Businesshour<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Businesshours"."DayOfWeekAndTime".<br/>Table field type characteristics (type, precision, scale, length): Char, 0, 0, 5.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String DayOfWeekAndTime
		{
			get { return (System.String)GetValue((int)BusinesshourFieldIndex.DayOfWeekAndTime, true); }
			set	{ SetValue((int)BusinesshourFieldIndex.DayOfWeekAndTime, value); }
		}

		/// <summary>The Opening property of the Entity Businesshour<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Businesshours"."Opening".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean Opening
		{
			get { return (System.Boolean)GetValue((int)BusinesshourFieldIndex.Opening, true); }
			set	{ SetValue((int)BusinesshourFieldIndex.Opening, value); }
		}

		/// <summary>The CreatedBy property of the Entity Businesshour<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Businesshours"."CreatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)BusinesshourFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)BusinesshourFieldIndex.CreatedBy, value); }
		}

		/// <summary>The UpdatedBy property of the Entity Businesshour<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Businesshours"."UpdatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)BusinesshourFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)BusinesshourFieldIndex.UpdatedBy, value); }
		}

		/// <summary>The CreatedUTC property of the Entity Businesshour<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Businesshours"."CreatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)BusinesshourFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)BusinesshourFieldIndex.CreatedUTC, value); }
		}

		/// <summary>The UpdatedUTC property of the Entity Businesshour<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Businesshours"."UpdatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)BusinesshourFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)BusinesshourFieldIndex.UpdatedUTC, value); }
		}

		/// <summary>The OutletId property of the Entity Businesshour<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Businesshours"."OutletId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> OutletId
		{
			get { return (Nullable<System.Int32>)GetValue((int)BusinesshourFieldIndex.OutletId, false); }
			set	{ SetValue((int)BusinesshourFieldIndex.OutletId, value); }
		}

		/// <summary>Gets / sets related entity of type 'CompanyEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual CompanyEntity Company
		{
			get { return _company; }
			set { SetSingleRelatedEntityNavigator(value, "Company"); }
		}

		/// <summary>Gets / sets related entity of type 'OutletEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual OutletEntity Outlet
		{
			get { return _outlet; }
			set { SetSingleRelatedEntityNavigator(value, "Outlet"); }
		}


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen
{
	public enum BusinesshourFieldIndex
	{
		///<summary>BusinesshoursId. </summary>
		BusinesshoursId,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>PointOfInterestId. </summary>
		PointOfInterestId,
		///<summary>DayOfWeekAndTime. </summary>
		DayOfWeekAndTime,
		///<summary>Opening. </summary>
		Opening,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>OutletId. </summary>
		OutletId,
		/// <summary></summary>
		AmountOfFields
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Businesshour. </summary>
	public partial class BusinesshourRelations: RelationFactory
	{
		#region Custom Relation code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomRelationCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		


		/// <summary>Gets all relations and delete rules of the BusinesshourEntity as a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects.</summary>
		/// <returns>a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects</returns>
		public virtual Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> GetRelationsAndDeleteRules()
		{
			Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> toReturn = new Dictionary<IEntityRelation, ReferentialConstraintDeleteRule>();
			return toReturn;
		}		
	
	

		/// <summary>Returns a new IEntityRelation object, between BusinesshourEntity and CompanyEntity over the m:1 relation they have, using the relation between the fields: Businesshour.CompanyId - Company.CompanyId</summary>
		public virtual IEntityRelation CompanyEntityUsingCompanyId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Company", false, new[] { CompanyFields.CompanyId, BusinesshourFields.CompanyId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between BusinesshourEntity and OutletEntity over the m:1 relation they have, using the relation between the fields: Businesshour.OutletId - Outlet.OutletId</summary>
		public virtual IEntityRelation OutletEntityUsingOutletId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Outlet", false, new[] { OutletFields.OutletId, BusinesshourFields.OutletId }); }
		}

	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticBusinesshourRelations
	{
		internal static readonly IEntityRelation CompanyEntityUsingCompanyIdStatic = new BusinesshourRelations().CompanyEntityUsingCompanyId;
		internal static readonly IEntityRelation OutletEntityUsingOutletIdStatic = new BusinesshourRelations().OutletEntityUsingOutletId;

		/// <summary>CTor</summary>
		static StaticBusinesshourRelations() { }
	}
}

