﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 5.8
// Code is generated on: 
// Code is generated using templates: Crave.TemplateBindings
// Templates vendor: Crave Interactive
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.FactoryClasses;
using Crave.Ordering.Shared.Data.LLBLGen.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

// ReSharper disable All
namespace Crave.Ordering.Shared.Data.LLBLGen.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'ProductAlteration'.<br/><br/></summary>
	[Serializable]
	public partial class ProductAlterationEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END
	
	{
		private AlterationEntity _alteration;
		private ProductEntity _product;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		private static ProductAlterationEntityStaticMetaData _staticMetaData = new ProductAlterationEntityStaticMetaData();
		private static ProductAlterationRelations _relationsFactory = new ProductAlterationRelations();		

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Alteration</summary>
			public static readonly string Alteration = "Alteration";
			/// <summary>Member name Product</summary>
			public static readonly string Product = "Product";
		}

		/// <summary>Static meta-data storage for navigator related information</summary>
		protected class ProductAlterationEntityStaticMetaData : EntityStaticMetaDataBase
		{
			public ProductAlterationEntityStaticMetaData()
			{
				SetEntityCoreInfo("ProductAlterationEntity", InheritanceHierarchyType.None, false, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ProductAlterationEntity, typeof(ProductAlterationEntity), typeof(ProductAlterationEntityFactory), false);
				AddNavigatorMetaData<ProductAlterationEntity, AlterationEntity>("Alteration", "ProductAlterationCollection", (a, b) => a._alteration = b, a => a._alteration, (a, b) => a.Alteration = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticProductAlterationRelations.AlterationEntityUsingAlterationIdStatic, ()=>new ProductAlterationRelations().AlterationEntityUsingAlterationId, null, new int[] { (int)ProductAlterationFieldIndex.AlterationId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.AlterationEntity);
				AddNavigatorMetaData<ProductAlterationEntity, ProductEntity>("Product", "ProductAlterationCollection", (a, b) => a._product = b, a => a._product, (a, b) => a.Product = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticProductAlterationRelations.ProductEntityUsingProductIdStatic, ()=>new ProductAlterationRelations().ProductEntityUsingProductId, null, new int[] { (int)ProductAlterationFieldIndex.ProductId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ProductEntity);
			}
		}

		/// <summary>Static ctor</summary>
		static ProductAlterationEntity()
		{
		}

		/// <summary> CTor</summary>
		public ProductAlterationEntity()
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public ProductAlterationEntity(IEntityFields2 fields)
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this ProductAlterationEntity</param>
		public ProductAlterationEntity(IValidator validator)
		{
			InitClassEmpty(validator, null);
		}

		/// <summary> CTor</summary>
		/// <param name="productAlterationId">PK value for ProductAlteration which data should be fetched into this ProductAlteration object</param>
		public ProductAlterationEntity(System.Int32 productAlterationId) : this(productAlterationId, null)
		{
		}

		/// <summary> CTor</summary>
		/// <param name="productAlterationId">PK value for ProductAlteration which data should be fetched into this ProductAlteration object</param>
		/// <param name="validator">The custom validator object for this ProductAlterationEntity</param>
		public ProductAlterationEntity(System.Int32 productAlterationId, IValidator validator)
		{
			InitClassEmpty(validator, null);
			this.ProductAlterationId = productAlterationId;
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ProductAlterationEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Alteration' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoAlteration() { return CreateRelationInfoForNavigator("Alteration"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Product' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoProduct() { return CreateRelationInfoForNavigator("Product"); }
		
		/// <inheritdoc/>
		protected override EntityStaticMetaDataBase GetEntityStaticMetaData() {	return _staticMetaData; }

		/// <summary>Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		/// <summary>Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this ProductAlterationEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary>The relations object holding all relations of this entity with other entity classes.</summary>
		public static ProductAlterationRelations Relations { get { return _relationsFactory; } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Alteration' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathAlterationEntity { get { return _staticMetaData.GetPrefetchPathElement("Alteration", CommonEntityBase.CreateEntityCollection<AlterationEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Product' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathProductEntity { get { return _staticMetaData.GetPrefetchPathElement("Product", CommonEntityBase.CreateEntityCollection<ProductEntity>()); } }

		/// <summary>The ProductAlterationId property of the Entity ProductAlteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ProductAlteration"."ProductAlterationId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 ProductAlterationId
		{
			get { return (System.Int32)GetValue((int)ProductAlterationFieldIndex.ProductAlterationId, true); }
			set { SetValue((int)ProductAlterationFieldIndex.ProductAlterationId, value); }		}

		/// <summary>The ParentCompanyId property of the Entity ProductAlteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ProductAlteration"."ParentCompanyId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ParentCompanyId
		{
			get { return (Nullable<System.Int32>)GetValue((int)ProductAlterationFieldIndex.ParentCompanyId, false); }
			set	{ SetValue((int)ProductAlterationFieldIndex.ParentCompanyId, value); }
		}

		/// <summary>The Version property of the Entity ProductAlteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ProductAlteration"."Version".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 Version
		{
			get { return (System.Int32)GetValue((int)ProductAlterationFieldIndex.Version, true); }
			set	{ SetValue((int)ProductAlterationFieldIndex.Version, value); }
		}

		/// <summary>The ProductId property of the Entity ProductAlteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ProductAlteration"."ProductId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ProductId
		{
			get { return (System.Int32)GetValue((int)ProductAlterationFieldIndex.ProductId, true); }
			set	{ SetValue((int)ProductAlterationFieldIndex.ProductId, value); }
		}

		/// <summary>The AlterationId property of the Entity ProductAlteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ProductAlteration"."AlterationId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 AlterationId
		{
			get { return (System.Int32)GetValue((int)ProductAlterationFieldIndex.AlterationId, true); }
			set	{ SetValue((int)ProductAlterationFieldIndex.AlterationId, value); }
		}

		/// <summary>The SortOrder property of the Entity ProductAlteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ProductAlteration"."SortOrder".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 SortOrder
		{
			get { return (System.Int32)GetValue((int)ProductAlterationFieldIndex.SortOrder, true); }
			set	{ SetValue((int)ProductAlterationFieldIndex.SortOrder, value); }
		}

		/// <summary>The PosproductPosalterationId property of the Entity ProductAlteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ProductAlteration"."PosproductPosalterationId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> PosproductPosalterationId
		{
			get { return (Nullable<System.Int32>)GetValue((int)ProductAlterationFieldIndex.PosproductPosalterationId, false); }
			set	{ SetValue((int)ProductAlterationFieldIndex.PosproductPosalterationId, value); }
		}

		/// <summary>The GenericproductGenericalterationId property of the Entity ProductAlteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ProductAlteration"."GenericproductGenericalterationId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> GenericproductGenericalterationId
		{
			get { return (Nullable<System.Int32>)GetValue((int)ProductAlterationFieldIndex.GenericproductGenericalterationId, false); }
			set	{ SetValue((int)ProductAlterationFieldIndex.GenericproductGenericalterationId, value); }
		}

		/// <summary>The CreatedBy property of the Entity ProductAlteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ProductAlteration"."CreatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)ProductAlterationFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)ProductAlterationFieldIndex.CreatedBy, value); }
		}

		/// <summary>The UpdatedBy property of the Entity ProductAlteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ProductAlteration"."UpdatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)ProductAlterationFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)ProductAlterationFieldIndex.UpdatedBy, value); }
		}

		/// <summary>The CreatedUTC property of the Entity ProductAlteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ProductAlteration"."CreatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ProductAlterationFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)ProductAlterationFieldIndex.CreatedUTC, value); }
		}

		/// <summary>The UpdatedUTC property of the Entity ProductAlteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ProductAlteration"."UpdatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ProductAlterationFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)ProductAlterationFieldIndex.UpdatedUTC, value); }
		}

		/// <summary>Gets / sets related entity of type 'AlterationEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual AlterationEntity Alteration
		{
			get { return _alteration; }
			set { SetSingleRelatedEntityNavigator(value, "Alteration"); }
		}

		/// <summary>Gets / sets related entity of type 'ProductEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual ProductEntity Product
		{
			get { return _product; }
			set { SetSingleRelatedEntityNavigator(value, "Product"); }
		}


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen
{
	public enum ProductAlterationFieldIndex
	{
		///<summary>ProductAlterationId. </summary>
		ProductAlterationId,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>Version. </summary>
		Version,
		///<summary>ProductId. </summary>
		ProductId,
		///<summary>AlterationId. </summary>
		AlterationId,
		///<summary>SortOrder. </summary>
		SortOrder,
		///<summary>PosproductPosalterationId. </summary>
		PosproductPosalterationId,
		///<summary>GenericproductGenericalterationId. </summary>
		GenericproductGenericalterationId,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: ProductAlteration. </summary>
	public partial class ProductAlterationRelations: RelationFactory
	{
		#region Custom Relation code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomRelationCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		


		/// <summary>Gets all relations and delete rules of the ProductAlterationEntity as a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects.</summary>
		/// <returns>a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects</returns>
		public virtual Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> GetRelationsAndDeleteRules()
		{
			Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> toReturn = new Dictionary<IEntityRelation, ReferentialConstraintDeleteRule>();
			return toReturn;
		}		
	
	

		/// <summary>Returns a new IEntityRelation object, between ProductAlterationEntity and AlterationEntity over the m:1 relation they have, using the relation between the fields: ProductAlteration.AlterationId - Alteration.AlterationId</summary>
		public virtual IEntityRelation AlterationEntityUsingAlterationId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Alteration", false, new[] { AlterationFields.AlterationId, ProductAlterationFields.AlterationId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between ProductAlterationEntity and ProductEntity over the m:1 relation they have, using the relation between the fields: ProductAlteration.ProductId - Product.ProductId</summary>
		public virtual IEntityRelation ProductEntityUsingProductId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Product", false, new[] { ProductFields.ProductId, ProductAlterationFields.ProductId }); }
		}

	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticProductAlterationRelations
	{
		internal static readonly IEntityRelation AlterationEntityUsingAlterationIdStatic = new ProductAlterationRelations().AlterationEntityUsingAlterationId;
		internal static readonly IEntityRelation ProductEntityUsingProductIdStatic = new ProductAlterationRelations().ProductEntityUsingProductId;

		/// <summary>CTor</summary>
		static StaticProductAlterationRelations() { }
	}
}

