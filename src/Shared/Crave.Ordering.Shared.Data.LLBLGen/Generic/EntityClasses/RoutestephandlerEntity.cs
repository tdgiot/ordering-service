﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 5.8
// Code is generated on: 
// Code is generated using templates: Crave.TemplateBindings
// Templates vendor: Crave Interactive
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.FactoryClasses;
using Crave.Ordering.Shared.Data.LLBLGen.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

// ReSharper disable All
namespace Crave.Ordering.Shared.Data.LLBLGen.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'Routestephandler'.<br/><br/></summary>
	[Serializable]
	public partial class RoutestephandlerEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END
	
	{
		private EntityCollection<MediaEntity> _mediaCollection;
		private ExternalSystemEntity _externalSystem;
		private RoutestepEntity _routestep;
		private SupportpoolEntity _supportpool;
		private TerminalEntity _terminal;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		private static RoutestephandlerEntityStaticMetaData _staticMetaData = new RoutestephandlerEntityStaticMetaData();
		private static RoutestephandlerRelations _relationsFactory = new RoutestephandlerRelations();		

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name ExternalSystem</summary>
			public static readonly string ExternalSystem = "ExternalSystem";
			/// <summary>Member name Routestep</summary>
			public static readonly string Routestep = "Routestep";
			/// <summary>Member name Supportpool</summary>
			public static readonly string Supportpool = "Supportpool";
			/// <summary>Member name Terminal</summary>
			public static readonly string Terminal = "Terminal";
			/// <summary>Member name MediaCollection</summary>
			public static readonly string MediaCollection = "MediaCollection";
		}

		/// <summary>Static meta-data storage for navigator related information</summary>
		protected class RoutestephandlerEntityStaticMetaData : EntityStaticMetaDataBase
		{
			public RoutestephandlerEntityStaticMetaData()
			{
				SetEntityCoreInfo("RoutestephandlerEntity", InheritanceHierarchyType.None, false, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.RoutestephandlerEntity, typeof(RoutestephandlerEntity), typeof(RoutestephandlerEntityFactory), false);
				AddNavigatorMetaData<RoutestephandlerEntity, EntityCollection<MediaEntity>>("MediaCollection", a => a._mediaCollection, (a, b) => a._mediaCollection = b, a => a.MediaCollection, () => new RoutestephandlerRelations().MediaEntityUsingRoutestephandlerId, typeof(MediaEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.MediaEntity);
				AddNavigatorMetaData<RoutestephandlerEntity, ExternalSystemEntity>("ExternalSystem", "RoutestephandlerCollection", (a, b) => a._externalSystem = b, a => a._externalSystem, (a, b) => a.ExternalSystem = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticRoutestephandlerRelations.ExternalSystemEntityUsingExternalSystemIdStatic, ()=>new RoutestephandlerRelations().ExternalSystemEntityUsingExternalSystemId, null, new int[] { (int)RoutestephandlerFieldIndex.ExternalSystemId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ExternalSystemEntity);
				AddNavigatorMetaData<RoutestephandlerEntity, RoutestepEntity>("Routestep", "RoutestephandlerCollection", (a, b) => a._routestep = b, a => a._routestep, (a, b) => a.Routestep = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticRoutestephandlerRelations.RoutestepEntityUsingRoutestepIdStatic, ()=>new RoutestephandlerRelations().RoutestepEntityUsingRoutestepId, null, new int[] { (int)RoutestephandlerFieldIndex.RoutestepId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.RoutestepEntity);
				AddNavigatorMetaData<RoutestephandlerEntity, SupportpoolEntity>("Supportpool", "RoutestephandlerCollection", (a, b) => a._supportpool = b, a => a._supportpool, (a, b) => a.Supportpool = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticRoutestephandlerRelations.SupportpoolEntityUsingSupportpoolIdStatic, ()=>new RoutestephandlerRelations().SupportpoolEntityUsingSupportpoolId, null, new int[] { (int)RoutestephandlerFieldIndex.SupportpoolId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.SupportpoolEntity);
				AddNavigatorMetaData<RoutestephandlerEntity, TerminalEntity>("Terminal", "RoutestephandlerCollection", (a, b) => a._terminal = b, a => a._terminal, (a, b) => a.Terminal = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticRoutestephandlerRelations.TerminalEntityUsingTerminalIdStatic, ()=>new RoutestephandlerRelations().TerminalEntityUsingTerminalId, null, new int[] { (int)RoutestephandlerFieldIndex.TerminalId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.TerminalEntity);
			}
		}

		/// <summary>Static ctor</summary>
		static RoutestephandlerEntity()
		{
		}

		/// <summary> CTor</summary>
		public RoutestephandlerEntity()
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public RoutestephandlerEntity(IEntityFields2 fields)
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this RoutestephandlerEntity</param>
		public RoutestephandlerEntity(IValidator validator)
		{
			InitClassEmpty(validator, null);
		}

		/// <summary> CTor</summary>
		/// <param name="routestephandlerId">PK value for Routestephandler which data should be fetched into this Routestephandler object</param>
		public RoutestephandlerEntity(System.Int32 routestephandlerId) : this(routestephandlerId, null)
		{
		}

		/// <summary> CTor</summary>
		/// <param name="routestephandlerId">PK value for Routestephandler which data should be fetched into this Routestephandler object</param>
		/// <param name="validator">The custom validator object for this RoutestephandlerEntity</param>
		public RoutestephandlerEntity(System.Int32 routestephandlerId, IValidator validator)
		{
			InitClassEmpty(validator, null);
			this.RoutestephandlerId = routestephandlerId;
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected RoutestephandlerEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Media' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoMediaCollection() { return CreateRelationInfoForNavigator("MediaCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'ExternalSystem' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoExternalSystem() { return CreateRelationInfoForNavigator("ExternalSystem"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Routestep' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoRoutestep() { return CreateRelationInfoForNavigator("Routestep"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Supportpool' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoSupportpool() { return CreateRelationInfoForNavigator("Supportpool"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Terminal' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoTerminal() { return CreateRelationInfoForNavigator("Terminal"); }
		
		/// <inheritdoc/>
		protected override EntityStaticMetaDataBase GetEntityStaticMetaData() {	return _staticMetaData; }

		/// <summary>Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		/// <summary>Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this RoutestephandlerEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary>The relations object holding all relations of this entity with other entity classes.</summary>
		public static RoutestephandlerRelations Relations { get { return _relationsFactory; } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Media' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathMediaCollection { get { return _staticMetaData.GetPrefetchPathElement("MediaCollection", CommonEntityBase.CreateEntityCollection<MediaEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'ExternalSystem' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathExternalSystemEntity { get { return _staticMetaData.GetPrefetchPathElement("ExternalSystem", CommonEntityBase.CreateEntityCollection<ExternalSystemEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Routestep' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathRoutestepEntity { get { return _staticMetaData.GetPrefetchPathElement("Routestep", CommonEntityBase.CreateEntityCollection<RoutestepEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Supportpool' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathSupportpoolEntity { get { return _staticMetaData.GetPrefetchPathElement("Supportpool", CommonEntityBase.CreateEntityCollection<SupportpoolEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Terminal' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathTerminalEntity { get { return _staticMetaData.GetPrefetchPathElement("Terminal", CommonEntityBase.CreateEntityCollection<TerminalEntity>()); } }

		/// <summary>The RoutestephandlerId property of the Entity Routestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Routestephandler"."RoutestephandlerId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 RoutestephandlerId
		{
			get { return (System.Int32)GetValue((int)RoutestephandlerFieldIndex.RoutestephandlerId, true); }
			set { SetValue((int)RoutestephandlerFieldIndex.RoutestephandlerId, value); }		}

		/// <summary>The ParentCompanyId property of the Entity Routestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Routestephandler"."ParentCompanyId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ParentCompanyId
		{
			get { return (System.Int32)GetValue((int)RoutestephandlerFieldIndex.ParentCompanyId, true); }
			set	{ SetValue((int)RoutestephandlerFieldIndex.ParentCompanyId, value); }
		}

		/// <summary>The RoutestepId property of the Entity Routestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Routestephandler"."RoutestepId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 RoutestepId
		{
			get { return (System.Int32)GetValue((int)RoutestephandlerFieldIndex.RoutestepId, true); }
			set	{ SetValue((int)RoutestephandlerFieldIndex.RoutestepId, value); }
		}

		/// <summary>The TerminalId property of the Entity Routestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Routestephandler"."TerminalId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> TerminalId
		{
			get { return (Nullable<System.Int32>)GetValue((int)RoutestephandlerFieldIndex.TerminalId, false); }
			set	{ SetValue((int)RoutestephandlerFieldIndex.TerminalId, value); }
		}

		/// <summary>The HandlerType property of the Entity Routestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Routestephandler"."HandlerType".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Crave.Enums.RoutestephandlerType HandlerType
		{
			get { return (Crave.Enums.RoutestephandlerType)GetValue((int)RoutestephandlerFieldIndex.HandlerType, true); }
			set	{ SetValue((int)RoutestephandlerFieldIndex.HandlerType, value); }
		}

		/// <summary>The PrintReportType property of the Entity Routestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Routestephandler"."PrintReportType".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<Crave.Enums.PrintReportType> PrintReportType
		{
			get { return (Nullable<Crave.Enums.PrintReportType>)GetValue((int)RoutestephandlerFieldIndex.PrintReportType, false); }
			set	{ SetValue((int)RoutestephandlerFieldIndex.PrintReportType, value); }
		}

		/// <summary>The FieldValue1 property of the Entity Routestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Routestephandler"."FieldValue1".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue1
		{
			get { return (System.String)GetValue((int)RoutestephandlerFieldIndex.FieldValue1, true); }
			set	{ SetValue((int)RoutestephandlerFieldIndex.FieldValue1, value); }
		}

		/// <summary>The FieldValue2 property of the Entity Routestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Routestephandler"."FieldValue2".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue2
		{
			get { return (System.String)GetValue((int)RoutestephandlerFieldIndex.FieldValue2, true); }
			set	{ SetValue((int)RoutestephandlerFieldIndex.FieldValue2, value); }
		}

		/// <summary>The FieldValue3 property of the Entity Routestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Routestephandler"."FieldValue3".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue3
		{
			get { return (System.String)GetValue((int)RoutestephandlerFieldIndex.FieldValue3, true); }
			set	{ SetValue((int)RoutestephandlerFieldIndex.FieldValue3, value); }
		}

		/// <summary>The FieldValue4 property of the Entity Routestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Routestephandler"."FieldValue4".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue4
		{
			get { return (System.String)GetValue((int)RoutestephandlerFieldIndex.FieldValue4, true); }
			set	{ SetValue((int)RoutestephandlerFieldIndex.FieldValue4, value); }
		}

		/// <summary>The FieldValue5 property of the Entity Routestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Routestephandler"."FieldValue5".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue5
		{
			get { return (System.String)GetValue((int)RoutestephandlerFieldIndex.FieldValue5, true); }
			set	{ SetValue((int)RoutestephandlerFieldIndex.FieldValue5, value); }
		}

		/// <summary>The FieldValue6 property of the Entity Routestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Routestephandler"."FieldValue6".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue6
		{
			get { return (System.String)GetValue((int)RoutestephandlerFieldIndex.FieldValue6, true); }
			set	{ SetValue((int)RoutestephandlerFieldIndex.FieldValue6, value); }
		}

		/// <summary>The FieldValue7 property of the Entity Routestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Routestephandler"."FieldValue7".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue7
		{
			get { return (System.String)GetValue((int)RoutestephandlerFieldIndex.FieldValue7, true); }
			set	{ SetValue((int)RoutestephandlerFieldIndex.FieldValue7, value); }
		}

		/// <summary>The FieldValue8 property of the Entity Routestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Routestephandler"."FieldValue8".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue8
		{
			get { return (System.String)GetValue((int)RoutestephandlerFieldIndex.FieldValue8, true); }
			set	{ SetValue((int)RoutestephandlerFieldIndex.FieldValue8, value); }
		}

		/// <summary>The FieldValue9 property of the Entity Routestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Routestephandler"."FieldValue9".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue9
		{
			get { return (System.String)GetValue((int)RoutestephandlerFieldIndex.FieldValue9, true); }
			set	{ SetValue((int)RoutestephandlerFieldIndex.FieldValue9, value); }
		}

		/// <summary>The FieldValue10 property of the Entity Routestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Routestephandler"."FieldValue10".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue10
		{
			get { return (System.String)GetValue((int)RoutestephandlerFieldIndex.FieldValue10, true); }
			set	{ SetValue((int)RoutestephandlerFieldIndex.FieldValue10, value); }
		}

		/// <summary>The SupportpoolId property of the Entity Routestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Routestephandler"."SupportpoolId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> SupportpoolId
		{
			get { return (Nullable<System.Int32>)GetValue((int)RoutestephandlerFieldIndex.SupportpoolId, false); }
			set	{ SetValue((int)RoutestephandlerFieldIndex.SupportpoolId, value); }
		}

		/// <summary>The CreatedBy property of the Entity Routestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Routestephandler"."CreatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)RoutestephandlerFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)RoutestephandlerFieldIndex.CreatedBy, value); }
		}

		/// <summary>The UpdatedBy property of the Entity Routestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Routestephandler"."UpdatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)RoutestephandlerFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)RoutestephandlerFieldIndex.UpdatedBy, value); }
		}

		/// <summary>The CreatedUTC property of the Entity Routestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Routestephandler"."CreatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)RoutestephandlerFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)RoutestephandlerFieldIndex.CreatedUTC, value); }
		}

		/// <summary>The UpdatedUTC property of the Entity Routestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Routestephandler"."UpdatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)RoutestephandlerFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)RoutestephandlerFieldIndex.UpdatedUTC, value); }
		}

		/// <summary>The ExternalSystemId property of the Entity Routestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Routestephandler"."ExternalSystemId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ExternalSystemId
		{
			get { return (Nullable<System.Int32>)GetValue((int)RoutestephandlerFieldIndex.ExternalSystemId, false); }
			set	{ SetValue((int)RoutestephandlerFieldIndex.ExternalSystemId, value); }
		}

		/// <summary>Gets the EntityCollection with the related entities of type 'MediaEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(MediaEntity))]
		public virtual EntityCollection<MediaEntity> MediaCollection { get { return GetOrCreateEntityCollection<MediaEntity, MediaEntityFactory>("Routestephandler", true, false, ref _mediaCollection); } }

		/// <summary>Gets / sets related entity of type 'ExternalSystemEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual ExternalSystemEntity ExternalSystem
		{
			get { return _externalSystem; }
			set { SetSingleRelatedEntityNavigator(value, "ExternalSystem"); }
		}

		/// <summary>Gets / sets related entity of type 'RoutestepEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual RoutestepEntity Routestep
		{
			get { return _routestep; }
			set { SetSingleRelatedEntityNavigator(value, "Routestep"); }
		}

		/// <summary>Gets / sets related entity of type 'SupportpoolEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual SupportpoolEntity Supportpool
		{
			get { return _supportpool; }
			set { SetSingleRelatedEntityNavigator(value, "Supportpool"); }
		}

		/// <summary>Gets / sets related entity of type 'TerminalEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual TerminalEntity Terminal
		{
			get { return _terminal; }
			set { SetSingleRelatedEntityNavigator(value, "Terminal"); }
		}


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen
{
	public enum RoutestephandlerFieldIndex
	{
		///<summary>RoutestephandlerId. </summary>
		RoutestephandlerId,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>RoutestepId. </summary>
		RoutestepId,
		///<summary>TerminalId. </summary>
		TerminalId,
		///<summary>HandlerType. </summary>
		HandlerType,
		///<summary>PrintReportType. </summary>
		PrintReportType,
		///<summary>FieldValue1. </summary>
		FieldValue1,
		///<summary>FieldValue2. </summary>
		FieldValue2,
		///<summary>FieldValue3. </summary>
		FieldValue3,
		///<summary>FieldValue4. </summary>
		FieldValue4,
		///<summary>FieldValue5. </summary>
		FieldValue5,
		///<summary>FieldValue6. </summary>
		FieldValue6,
		///<summary>FieldValue7. </summary>
		FieldValue7,
		///<summary>FieldValue8. </summary>
		FieldValue8,
		///<summary>FieldValue9. </summary>
		FieldValue9,
		///<summary>FieldValue10. </summary>
		FieldValue10,
		///<summary>SupportpoolId. </summary>
		SupportpoolId,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>ExternalSystemId. </summary>
		ExternalSystemId,
		/// <summary></summary>
		AmountOfFields
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Routestephandler. </summary>
	public partial class RoutestephandlerRelations: RelationFactory
	{
		#region Custom Relation code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomRelationCode
        private const ReferentialConstraintDeleteRule DeleteRuleForMediaEntityUsingRoutestephandlerId = ReferentialConstraintDeleteRule.Cascade;
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		
		// private const ReferentialConstraintDeleteRule DeleteRuleForMediaEntityUsingRoutestephandlerId = ReferentialConstraintDeleteRule


		/// <summary>Gets all relations and delete rules of the RoutestephandlerEntity as a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects.</summary>
		/// <returns>a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects</returns>
		public virtual Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> GetRelationsAndDeleteRules()
		{
			Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> toReturn = new Dictionary<IEntityRelation, ReferentialConstraintDeleteRule>();
			toReturn.Add(this.MediaEntityUsingRoutestephandlerId, RoutestephandlerRelations.DeleteRuleForMediaEntityUsingRoutestephandlerId);
			return toReturn;
		}		
	
	
		/// <summary>Returns a new IEntityRelation object, between RoutestephandlerEntity and MediaEntity over the 1:n relation they have, using the relation between the fields: Routestephandler.RoutestephandlerId - Media.RoutestephandlerId</summary>
		public virtual IEntityRelation MediaEntityUsingRoutestephandlerId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "MediaCollection", true, new[] { RoutestephandlerFields.RoutestephandlerId, MediaFields.RoutestephandlerId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between RoutestephandlerEntity and ExternalSystemEntity over the m:1 relation they have, using the relation between the fields: Routestephandler.ExternalSystemId - ExternalSystem.ExternalSystemId</summary>
		public virtual IEntityRelation ExternalSystemEntityUsingExternalSystemId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "ExternalSystem", false, new[] { ExternalSystemFields.ExternalSystemId, RoutestephandlerFields.ExternalSystemId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between RoutestephandlerEntity and RoutestepEntity over the m:1 relation they have, using the relation between the fields: Routestephandler.RoutestepId - Routestep.RoutestepId</summary>
		public virtual IEntityRelation RoutestepEntityUsingRoutestepId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Routestep", false, new[] { RoutestepFields.RoutestepId, RoutestephandlerFields.RoutestepId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between RoutestephandlerEntity and SupportpoolEntity over the m:1 relation they have, using the relation between the fields: Routestephandler.SupportpoolId - Supportpool.SupportpoolId</summary>
		public virtual IEntityRelation SupportpoolEntityUsingSupportpoolId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Supportpool", false, new[] { SupportpoolFields.SupportpoolId, RoutestephandlerFields.SupportpoolId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between RoutestephandlerEntity and TerminalEntity over the m:1 relation they have, using the relation between the fields: Routestephandler.TerminalId - Terminal.TerminalId</summary>
		public virtual IEntityRelation TerminalEntityUsingTerminalId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Terminal", false, new[] { TerminalFields.TerminalId, RoutestephandlerFields.TerminalId }); }
		}

	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticRoutestephandlerRelations
	{
		internal static readonly IEntityRelation MediaEntityUsingRoutestephandlerIdStatic = new RoutestephandlerRelations().MediaEntityUsingRoutestephandlerId;
		internal static readonly IEntityRelation ExternalSystemEntityUsingExternalSystemIdStatic = new RoutestephandlerRelations().ExternalSystemEntityUsingExternalSystemId;
		internal static readonly IEntityRelation RoutestepEntityUsingRoutestepIdStatic = new RoutestephandlerRelations().RoutestepEntityUsingRoutestepId;
		internal static readonly IEntityRelation SupportpoolEntityUsingSupportpoolIdStatic = new RoutestephandlerRelations().SupportpoolEntityUsingSupportpoolId;
		internal static readonly IEntityRelation TerminalEntityUsingTerminalIdStatic = new RoutestephandlerRelations().TerminalEntityUsingTerminalId;

		/// <summary>CTor</summary>
		static StaticRoutestephandlerRelations() { }
	}
}

