﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 5.8
// Code is generated on: 
// Code is generated using templates: Crave.TemplateBindings
// Templates vendor: Crave Interactive
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.FactoryClasses;
using Crave.Ordering.Shared.Data.LLBLGen.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

// ReSharper disable All
namespace Crave.Ordering.Shared.Data.LLBLGen.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'OrderitemAlterationitemTag'.<br/><br/></summary>
	[Serializable]
	public partial class OrderitemAlterationitemTagEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END
	
	{
		private AlterationoptionEntity _alterationoption;
		private OrderitemAlterationitemEntity _orderitemAlterationitem;
		private TagEntity _tag;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		private static OrderitemAlterationitemTagEntityStaticMetaData _staticMetaData = new OrderitemAlterationitemTagEntityStaticMetaData();
		private static OrderitemAlterationitemTagRelations _relationsFactory = new OrderitemAlterationitemTagRelations();		

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Alterationoption</summary>
			public static readonly string Alterationoption = "Alterationoption";
			/// <summary>Member name OrderitemAlterationitem</summary>
			public static readonly string OrderitemAlterationitem = "OrderitemAlterationitem";
			/// <summary>Member name Tag</summary>
			public static readonly string Tag = "Tag";
		}

		/// <summary>Static meta-data storage for navigator related information</summary>
		protected class OrderitemAlterationitemTagEntityStaticMetaData : EntityStaticMetaDataBase
		{
			public OrderitemAlterationitemTagEntityStaticMetaData()
			{
				SetEntityCoreInfo("OrderitemAlterationitemTagEntity", InheritanceHierarchyType.None, false, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.OrderitemAlterationitemTagEntity, typeof(OrderitemAlterationitemTagEntity), typeof(OrderitemAlterationitemTagEntityFactory), false);
				AddNavigatorMetaData<OrderitemAlterationitemTagEntity, AlterationoptionEntity>("Alterationoption", "OrderitemAlterationitemTagCollection", (a, b) => a._alterationoption = b, a => a._alterationoption, (a, b) => a.Alterationoption = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticOrderitemAlterationitemTagRelations.AlterationoptionEntityUsingAlterationoptionIdStatic, ()=>new OrderitemAlterationitemTagRelations().AlterationoptionEntityUsingAlterationoptionId, null, new int[] { (int)OrderitemAlterationitemTagFieldIndex.AlterationoptionId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.AlterationoptionEntity);
				AddNavigatorMetaData<OrderitemAlterationitemTagEntity, OrderitemAlterationitemEntity>("OrderitemAlterationitem", "OrderitemAlterationitemTagCollection", (a, b) => a._orderitemAlterationitem = b, a => a._orderitemAlterationitem, (a, b) => a.OrderitemAlterationitem = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticOrderitemAlterationitemTagRelations.OrderitemAlterationitemEntityUsingOrderitemAlterationitemIdStatic, ()=>new OrderitemAlterationitemTagRelations().OrderitemAlterationitemEntityUsingOrderitemAlterationitemId, null, new int[] { (int)OrderitemAlterationitemTagFieldIndex.OrderitemAlterationitemId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.OrderitemAlterationitemEntity);
				AddNavigatorMetaData<OrderitemAlterationitemTagEntity, TagEntity>("Tag", "OrderitemAlterationitemTagCollection", (a, b) => a._tag = b, a => a._tag, (a, b) => a.Tag = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticOrderitemAlterationitemTagRelations.TagEntityUsingTagIdStatic, ()=>new OrderitemAlterationitemTagRelations().TagEntityUsingTagId, null, new int[] { (int)OrderitemAlterationitemTagFieldIndex.TagId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.TagEntity);
			}
		}

		/// <summary>Static ctor</summary>
		static OrderitemAlterationitemTagEntity()
		{
		}

		/// <summary> CTor</summary>
		public OrderitemAlterationitemTagEntity()
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public OrderitemAlterationitemTagEntity(IEntityFields2 fields)
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this OrderitemAlterationitemTagEntity</param>
		public OrderitemAlterationitemTagEntity(IValidator validator)
		{
			InitClassEmpty(validator, null);
		}

		/// <summary> CTor</summary>
		/// <param name="orderitemAlterationitemTagId">PK value for OrderitemAlterationitemTag which data should be fetched into this OrderitemAlterationitemTag object</param>
		public OrderitemAlterationitemTagEntity(System.Int32 orderitemAlterationitemTagId) : this(orderitemAlterationitemTagId, null)
		{
		}

		/// <summary> CTor</summary>
		/// <param name="orderitemAlterationitemTagId">PK value for OrderitemAlterationitemTag which data should be fetched into this OrderitemAlterationitemTag object</param>
		/// <param name="validator">The custom validator object for this OrderitemAlterationitemTagEntity</param>
		public OrderitemAlterationitemTagEntity(System.Int32 orderitemAlterationitemTagId, IValidator validator)
		{
			InitClassEmpty(validator, null);
			this.OrderitemAlterationitemTagId = orderitemAlterationitemTagId;
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected OrderitemAlterationitemTagEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Alterationoption' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoAlterationoption() { return CreateRelationInfoForNavigator("Alterationoption"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'OrderitemAlterationitem' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoOrderitemAlterationitem() { return CreateRelationInfoForNavigator("OrderitemAlterationitem"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Tag' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoTag() { return CreateRelationInfoForNavigator("Tag"); }
		
		/// <inheritdoc/>
		protected override EntityStaticMetaDataBase GetEntityStaticMetaData() {	return _staticMetaData; }

		/// <summary>Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		/// <summary>Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this OrderitemAlterationitemTagEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary>The relations object holding all relations of this entity with other entity classes.</summary>
		public static OrderitemAlterationitemTagRelations Relations { get { return _relationsFactory; } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Alterationoption' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathAlterationoptionEntity { get { return _staticMetaData.GetPrefetchPathElement("Alterationoption", CommonEntityBase.CreateEntityCollection<AlterationoptionEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'OrderitemAlterationitem' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathOrderitemAlterationitemEntity { get { return _staticMetaData.GetPrefetchPathElement("OrderitemAlterationitem", CommonEntityBase.CreateEntityCollection<OrderitemAlterationitemEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Tag' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathTagEntity { get { return _staticMetaData.GetPrefetchPathElement("Tag", CommonEntityBase.CreateEntityCollection<TagEntity>()); } }

		/// <summary>The OrderitemAlterationitemTagId property of the Entity OrderitemAlterationitemTag<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderitemAlterationitemTag"."OrderitemAlterationitemTagId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 OrderitemAlterationitemTagId
		{
			get { return (System.Int32)GetValue((int)OrderitemAlterationitemTagFieldIndex.OrderitemAlterationitemTagId, true); }
			set { SetValue((int)OrderitemAlterationitemTagFieldIndex.OrderitemAlterationitemTagId, value); }		}

		/// <summary>The OrderitemAlterationitemId property of the Entity OrderitemAlterationitemTag<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderitemAlterationitemTag"."OrderitemAlterationitemId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 OrderitemAlterationitemId
		{
			get { return (System.Int32)GetValue((int)OrderitemAlterationitemTagFieldIndex.OrderitemAlterationitemId, true); }
			set	{ SetValue((int)OrderitemAlterationitemTagFieldIndex.OrderitemAlterationitemId, value); }
		}

		/// <summary>The AlterationoptionId property of the Entity OrderitemAlterationitemTag<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderitemAlterationitemTag"."AlterationoptionId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> AlterationoptionId
		{
			get { return (Nullable<System.Int32>)GetValue((int)OrderitemAlterationitemTagFieldIndex.AlterationoptionId, false); }
			set	{ SetValue((int)OrderitemAlterationitemTagFieldIndex.AlterationoptionId, value); }
		}

		/// <summary>The TagId property of the Entity OrderitemAlterationitemTag<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderitemAlterationitemTag"."TagId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 TagId
		{
			get { return (System.Int32)GetValue((int)OrderitemAlterationitemTagFieldIndex.TagId, true); }
			set	{ SetValue((int)OrderitemAlterationitemTagFieldIndex.TagId, value); }
		}

		/// <summary>The ParentCompanyId property of the Entity OrderitemAlterationitemTag<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderitemAlterationitemTag"."ParentCompanyId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ParentCompanyId
		{
			get { return (Nullable<System.Int32>)GetValue((int)OrderitemAlterationitemTagFieldIndex.ParentCompanyId, false); }
			set	{ SetValue((int)OrderitemAlterationitemTagFieldIndex.ParentCompanyId, value); }
		}

		/// <summary>The CreatedUTC property of the Entity OrderitemAlterationitemTag<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderitemAlterationitemTag"."CreatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime CreatedUTC
		{
			get { return (System.DateTime)GetValue((int)OrderitemAlterationitemTagFieldIndex.CreatedUTC, true); }
			set	{ SetValue((int)OrderitemAlterationitemTagFieldIndex.CreatedUTC, value); }
		}

		/// <summary>The UpdatedUTC property of the Entity OrderitemAlterationitemTag<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderitemAlterationitemTag"."UpdatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)OrderitemAlterationitemTagFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)OrderitemAlterationitemTagFieldIndex.UpdatedUTC, value); }
		}

		/// <summary>Gets / sets related entity of type 'AlterationoptionEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual AlterationoptionEntity Alterationoption
		{
			get { return _alterationoption; }
			set { SetSingleRelatedEntityNavigator(value, "Alterationoption"); }
		}

		/// <summary>Gets / sets related entity of type 'OrderitemAlterationitemEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual OrderitemAlterationitemEntity OrderitemAlterationitem
		{
			get { return _orderitemAlterationitem; }
			set { SetSingleRelatedEntityNavigator(value, "OrderitemAlterationitem"); }
		}

		/// <summary>Gets / sets related entity of type 'TagEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual TagEntity Tag
		{
			get { return _tag; }
			set { SetSingleRelatedEntityNavigator(value, "Tag"); }
		}


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen
{
	public enum OrderitemAlterationitemTagFieldIndex
	{
		///<summary>OrderitemAlterationitemTagId. </summary>
		OrderitemAlterationitemTagId,
		///<summary>OrderitemAlterationitemId. </summary>
		OrderitemAlterationitemId,
		///<summary>AlterationoptionId. </summary>
		AlterationoptionId,
		///<summary>TagId. </summary>
		TagId,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: OrderitemAlterationitemTag. </summary>
	public partial class OrderitemAlterationitemTagRelations: RelationFactory
	{
		#region Custom Relation code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomRelationCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		


		/// <summary>Gets all relations and delete rules of the OrderitemAlterationitemTagEntity as a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects.</summary>
		/// <returns>a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects</returns>
		public virtual Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> GetRelationsAndDeleteRules()
		{
			Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> toReturn = new Dictionary<IEntityRelation, ReferentialConstraintDeleteRule>();
			return toReturn;
		}		
	
	

		/// <summary>Returns a new IEntityRelation object, between OrderitemAlterationitemTagEntity and AlterationoptionEntity over the m:1 relation they have, using the relation between the fields: OrderitemAlterationitemTag.AlterationoptionId - Alterationoption.AlterationoptionId</summary>
		public virtual IEntityRelation AlterationoptionEntityUsingAlterationoptionId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Alterationoption", false, new[] { AlterationoptionFields.AlterationoptionId, OrderitemAlterationitemTagFields.AlterationoptionId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between OrderitemAlterationitemTagEntity and OrderitemAlterationitemEntity over the m:1 relation they have, using the relation between the fields: OrderitemAlterationitemTag.OrderitemAlterationitemId - OrderitemAlterationitem.OrderitemAlterationitemId</summary>
		public virtual IEntityRelation OrderitemAlterationitemEntityUsingOrderitemAlterationitemId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "OrderitemAlterationitem", false, new[] { OrderitemAlterationitemFields.OrderitemAlterationitemId, OrderitemAlterationitemTagFields.OrderitemAlterationitemId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between OrderitemAlterationitemTagEntity and TagEntity over the m:1 relation they have, using the relation between the fields: OrderitemAlterationitemTag.TagId - Tag.TagId</summary>
		public virtual IEntityRelation TagEntityUsingTagId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Tag", false, new[] { TagFields.TagId, OrderitemAlterationitemTagFields.TagId }); }
		}

	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticOrderitemAlterationitemTagRelations
	{
		internal static readonly IEntityRelation AlterationoptionEntityUsingAlterationoptionIdStatic = new OrderitemAlterationitemTagRelations().AlterationoptionEntityUsingAlterationoptionId;
		internal static readonly IEntityRelation OrderitemAlterationitemEntityUsingOrderitemAlterationitemIdStatic = new OrderitemAlterationitemTagRelations().OrderitemAlterationitemEntityUsingOrderitemAlterationitemId;
		internal static readonly IEntityRelation TagEntityUsingTagIdStatic = new OrderitemAlterationitemTagRelations().TagEntityUsingTagId;

		/// <summary>CTor</summary>
		static StaticOrderitemAlterationitemTagRelations() { }
	}
}

