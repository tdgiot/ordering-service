﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 5.8
// Code is generated on: 
// Code is generated using templates: Crave.TemplateBindings
// Templates vendor: Crave Interactive
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.FactoryClasses;
using Crave.Ordering.Shared.Data.LLBLGen.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

// ReSharper disable All
namespace Crave.Ordering.Shared.Data.LLBLGen.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'Client'.<br/><br/></summary>
	[Serializable]
	public partial class ClientEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END
	
	{
		private EntityCollection<ClientLogEntity> _clientLogCollection;
		private EntityCollection<CustomerEntity> _customerCollection;
		private EntityCollection<NetmessageEntity> _netmessageCollection;
		private EntityCollection<NetmessageEntity> _netmessageCollection1;
		private EntityCollection<OrderEntity> _orderCollection;
		private CompanyEntity _company;
		private DeliverypointEntity _deliverypoint;
		private DeliverypointEntity _deliverypointEntity1;
		private DeliverypointgroupEntity _deliverypointgroup;
		private DeviceEntity _device;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		private static ClientEntityStaticMetaData _staticMetaData = new ClientEntityStaticMetaData();
		private static ClientRelations _relationsFactory = new ClientRelations();		

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Company</summary>
			public static readonly string Company = "Company";
			/// <summary>Member name Deliverypoint</summary>
			public static readonly string Deliverypoint = "Deliverypoint";
			/// <summary>Member name DeliverypointEntity1</summary>
			public static readonly string DeliverypointEntity1 = "DeliverypointEntity1";
			/// <summary>Member name Deliverypointgroup</summary>
			public static readonly string Deliverypointgroup = "Deliverypointgroup";
			/// <summary>Member name Device</summary>
			public static readonly string Device = "Device";
			/// <summary>Member name ClientLogCollection</summary>
			public static readonly string ClientLogCollection = "ClientLogCollection";
			/// <summary>Member name CustomerCollection</summary>
			public static readonly string CustomerCollection = "CustomerCollection";
			/// <summary>Member name NetmessageCollection</summary>
			public static readonly string NetmessageCollection = "NetmessageCollection";
			/// <summary>Member name NetmessageCollection1</summary>
			public static readonly string NetmessageCollection1 = "NetmessageCollection1";
			/// <summary>Member name OrderCollection</summary>
			public static readonly string OrderCollection = "OrderCollection";
		}

		/// <summary>Static meta-data storage for navigator related information</summary>
		protected class ClientEntityStaticMetaData : EntityStaticMetaDataBase
		{
			public ClientEntityStaticMetaData()
			{
				SetEntityCoreInfo("ClientEntity", InheritanceHierarchyType.None, false, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ClientEntity, typeof(ClientEntity), typeof(ClientEntityFactory), false);
				AddNavigatorMetaData<ClientEntity, EntityCollection<ClientLogEntity>>("ClientLogCollection", a => a._clientLogCollection, (a, b) => a._clientLogCollection = b, a => a.ClientLogCollection, () => new ClientRelations().ClientLogEntityUsingClientId, typeof(ClientLogEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ClientLogEntity);
				AddNavigatorMetaData<ClientEntity, EntityCollection<CustomerEntity>>("CustomerCollection", a => a._customerCollection, (a, b) => a._customerCollection = b, a => a.CustomerCollection, () => new ClientRelations().CustomerEntityUsingClientId, typeof(CustomerEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.CustomerEntity);
				AddNavigatorMetaData<ClientEntity, EntityCollection<NetmessageEntity>>("NetmessageCollection", a => a._netmessageCollection, (a, b) => a._netmessageCollection = b, a => a.NetmessageCollection, () => new ClientRelations().NetmessageEntityUsingSenderClientId, typeof(NetmessageEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.NetmessageEntity);
				AddNavigatorMetaData<ClientEntity, EntityCollection<NetmessageEntity>>("NetmessageCollection1", a => a._netmessageCollection1, (a, b) => a._netmessageCollection1 = b, a => a.NetmessageCollection1, () => new ClientRelations().NetmessageEntityUsingReceiverClientId, typeof(NetmessageEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.NetmessageEntity);
				AddNavigatorMetaData<ClientEntity, EntityCollection<OrderEntity>>("OrderCollection", a => a._orderCollection, (a, b) => a._orderCollection = b, a => a.OrderCollection, () => new ClientRelations().OrderEntityUsingClientId, typeof(OrderEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.OrderEntity);
				AddNavigatorMetaData<ClientEntity, CompanyEntity>("Company", "ClientCollection", (a, b) => a._company = b, a => a._company, (a, b) => a.Company = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticClientRelations.CompanyEntityUsingCompanyIdStatic, ()=>new ClientRelations().CompanyEntityUsingCompanyId, null, new int[] { (int)ClientFieldIndex.CompanyId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.CompanyEntity);
				AddNavigatorMetaData<ClientEntity, DeliverypointEntity>("Deliverypoint", "ClientCollection", (a, b) => a._deliverypoint = b, a => a._deliverypoint, (a, b) => a.Deliverypoint = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticClientRelations.DeliverypointEntityUsingDeliverypointIdStatic, ()=>new ClientRelations().DeliverypointEntityUsingDeliverypointId, null, new int[] { (int)ClientFieldIndex.DeliverypointId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.DeliverypointEntity);
				AddNavigatorMetaData<ClientEntity, DeliverypointEntity>("DeliverypointEntity1", "ClientCollection1", (a, b) => a._deliverypointEntity1 = b, a => a._deliverypointEntity1, (a, b) => a.DeliverypointEntity1 = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticClientRelations.DeliverypointEntityUsingLastDeliverypointIdStatic, ()=>new ClientRelations().DeliverypointEntityUsingLastDeliverypointId, null, new int[] { (int)ClientFieldIndex.LastDeliverypointId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.DeliverypointEntity);
				AddNavigatorMetaData<ClientEntity, DeliverypointgroupEntity>("Deliverypointgroup", "ClientCollection", (a, b) => a._deliverypointgroup = b, a => a._deliverypointgroup, (a, b) => a.Deliverypointgroup = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticClientRelations.DeliverypointgroupEntityUsingDeliverypointgroupIdStatic, ()=>new ClientRelations().DeliverypointgroupEntityUsingDeliverypointgroupId, null, new int[] { (int)ClientFieldIndex.DeliverypointgroupId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.DeliverypointgroupEntity);
				AddNavigatorMetaData<ClientEntity, DeviceEntity>("Device", "ClientCollection", (a, b) => a._device = b, a => a._device, (a, b) => a.Device = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticClientRelations.DeviceEntityUsingDeviceIdStatic, ()=>new ClientRelations().DeviceEntityUsingDeviceId, new string[] { "DeviceIdentifier" }, new int[] { (int)ClientFieldIndex.DeviceId }, a => a.OnDevicePropertyChanged, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.DeviceEntity);
			}
		}

		/// <summary>Static ctor</summary>
		static ClientEntity()
		{
		}

		/// <summary> CTor</summary>
		public ClientEntity()
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public ClientEntity(IEntityFields2 fields)
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this ClientEntity</param>
		public ClientEntity(IValidator validator)
		{
			InitClassEmpty(validator, null);
		}

		/// <summary> CTor</summary>
		/// <param name="clientId">PK value for Client which data should be fetched into this Client object</param>
		public ClientEntity(System.Int32 clientId) : this(clientId, null)
		{
		}

		/// <summary> CTor</summary>
		/// <param name="clientId">PK value for Client which data should be fetched into this Client object</param>
		/// <param name="validator">The custom validator object for this ClientEntity</param>
		public ClientEntity(System.Int32 clientId, IValidator validator)
		{
			InitClassEmpty(validator, null);
			this.ClientId = clientId;
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ClientEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'ClientLog' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoClientLogCollection() { return CreateRelationInfoForNavigator("ClientLogCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Customer' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoCustomerCollection() { return CreateRelationInfoForNavigator("CustomerCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Netmessage' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoNetmessageCollection() { return CreateRelationInfoForNavigator("NetmessageCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Netmessage' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoNetmessageCollection1() { return CreateRelationInfoForNavigator("NetmessageCollection1"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Order' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoOrderCollection() { return CreateRelationInfoForNavigator("OrderCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Company' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoCompany() { return CreateRelationInfoForNavigator("Company"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Deliverypoint' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoDeliverypoint() { return CreateRelationInfoForNavigator("Deliverypoint"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Deliverypoint' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoDeliverypointEntity1() { return CreateRelationInfoForNavigator("DeliverypointEntity1"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Deliverypointgroup' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoDeliverypointgroup() { return CreateRelationInfoForNavigator("Deliverypointgroup"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Device' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoDevice() { return CreateRelationInfoForNavigator("Device"); }
		
		/// <inheritdoc/>
		protected override EntityStaticMetaDataBase GetEntityStaticMetaData() {	return _staticMetaData; }

		/// <summary>Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnDevicePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				case "Identifier":
					this.OnPropertyChanged("DeviceIdentifier");
					break;
			}
		}

		/// <summary>Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this ClientEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary>The relations object holding all relations of this entity with other entity classes.</summary>
		public static ClientRelations Relations { get { return _relationsFactory; } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'ClientLog' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathClientLogCollection { get { return _staticMetaData.GetPrefetchPathElement("ClientLogCollection", CommonEntityBase.CreateEntityCollection<ClientLogEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Customer' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathCustomerCollection { get { return _staticMetaData.GetPrefetchPathElement("CustomerCollection", CommonEntityBase.CreateEntityCollection<CustomerEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Netmessage' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathNetmessageCollection { get { return _staticMetaData.GetPrefetchPathElement("NetmessageCollection", CommonEntityBase.CreateEntityCollection<NetmessageEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Netmessage' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathNetmessageCollection1 { get { return _staticMetaData.GetPrefetchPathElement("NetmessageCollection1", CommonEntityBase.CreateEntityCollection<NetmessageEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Order' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathOrderCollection { get { return _staticMetaData.GetPrefetchPathElement("OrderCollection", CommonEntityBase.CreateEntityCollection<OrderEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Company' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathCompanyEntity { get { return _staticMetaData.GetPrefetchPathElement("Company", CommonEntityBase.CreateEntityCollection<CompanyEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Deliverypoint' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathDeliverypointEntity { get { return _staticMetaData.GetPrefetchPathElement("Deliverypoint", CommonEntityBase.CreateEntityCollection<DeliverypointEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Deliverypoint' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathDeliverypointEntity1Entity { get { return _staticMetaData.GetPrefetchPathElement("DeliverypointEntity1", CommonEntityBase.CreateEntityCollection<DeliverypointEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Deliverypointgroup' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathDeliverypointgroupEntity { get { return _staticMetaData.GetPrefetchPathElement("Deliverypointgroup", CommonEntityBase.CreateEntityCollection<DeliverypointgroupEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Device' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathDeviceEntity { get { return _staticMetaData.GetPrefetchPathElement("Device", CommonEntityBase.CreateEntityCollection<DeviceEntity>()); } }

		/// <summary>The ClientId property of the Entity Client<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Client"."ClientId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 ClientId
		{
			get { return (System.Int32)GetValue((int)ClientFieldIndex.ClientId, true); }
			set { SetValue((int)ClientFieldIndex.ClientId, value); }		}

		/// <summary>The CompanyId property of the Entity Client<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Client"."CompanyId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CompanyId
		{
			get { return (System.Int32)GetValue((int)ClientFieldIndex.CompanyId, true); }
			set	{ SetValue((int)ClientFieldIndex.CompanyId, value); }
		}

		/// <summary>The Notes property of the Entity Client<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Client"."Notes".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Notes
		{
			get { return (System.String)GetValue((int)ClientFieldIndex.Notes, true); }
			set	{ SetValue((int)ClientFieldIndex.Notes, value); }
		}

		/// <summary>The DeliverypointgroupId property of the Entity Client<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Client"."DeliverypointGroupId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> DeliverypointgroupId
		{
			get { return (Nullable<System.Int32>)GetValue((int)ClientFieldIndex.DeliverypointgroupId, false); }
			set	{ SetValue((int)ClientFieldIndex.DeliverypointgroupId, value); }
		}

		/// <summary>The OperationMode property of the Entity Client<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Client"."OperationMode".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Crave.Enums.ClientOperationMode OperationMode
		{
			get { return (Crave.Enums.ClientOperationMode)GetValue((int)ClientFieldIndex.OperationMode, true); }
			set	{ SetValue((int)ClientFieldIndex.OperationMode, value); }
		}

		/// <summary>The LastOperationMode property of the Entity Client<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Client"."LastOperationMode".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<Crave.Enums.ClientOperationMode> LastOperationMode
		{
			get { return (Nullable<Crave.Enums.ClientOperationMode>)GetValue((int)ClientFieldIndex.LastOperationMode, false); }
			set	{ SetValue((int)ClientFieldIndex.LastOperationMode, value); }
		}

		/// <summary>The LastStatus property of the Entity Client<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Client"."LastStatus".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<Crave.Enums.ClientStatus> LastStatus
		{
			get { return (Nullable<Crave.Enums.ClientStatus>)GetValue((int)ClientFieldIndex.LastStatus, false); }
			set	{ SetValue((int)ClientFieldIndex.LastStatus, value); }
		}

		/// <summary>The LastStatusText property of the Entity Client<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Client"."LastStatusText".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 250.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String LastStatusText
		{
			get { return (System.String)GetValue((int)ClientFieldIndex.LastStatusText, true); }
			set	{ SetValue((int)ClientFieldIndex.LastStatusText, value); }
		}

		/// <summary>The LastDeliverypointId property of the Entity Client<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Client"."LastDeliverypointId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> LastDeliverypointId
		{
			get { return (Nullable<System.Int32>)GetValue((int)ClientFieldIndex.LastDeliverypointId, false); }
			set	{ SetValue((int)ClientFieldIndex.LastDeliverypointId, value); }
		}

		/// <summary>The LastDeliverypointNumber property of the Entity Client<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Client"."LastDeliverypointNumber".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String LastDeliverypointNumber
		{
			get { return (System.String)GetValue((int)ClientFieldIndex.LastDeliverypointNumber, true); }
			set	{ SetValue((int)ClientFieldIndex.LastDeliverypointNumber, value); }
		}

		/// <summary>The LastCommunicationMethod property of the Entity Client<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Client"."LastCommunicationMethod".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Crave.Enums.ClientCommunicationMethod LastCommunicationMethod
		{
			get { return (Crave.Enums.ClientCommunicationMethod)GetValue((int)ClientFieldIndex.LastCommunicationMethod, true); }
			set	{ SetValue((int)ClientFieldIndex.LastCommunicationMethod, value); }
		}

		/// <summary>The LastCloudEnvironment property of the Entity Client<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Client"."LastCloudEnvironment".<br/>Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 50.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastCloudEnvironment
		{
			get { return (System.String)GetValue((int)ClientFieldIndex.LastCloudEnvironment, true); }
			set	{ SetValue((int)ClientFieldIndex.LastCloudEnvironment, value); }
		}

		/// <summary>The LastStateOnline property of the Entity Client<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Client"."LastStateOnline".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> LastStateOnline
		{
			get { return (Nullable<System.Boolean>)GetValue((int)ClientFieldIndex.LastStateOnline, false); }
			set	{ SetValue((int)ClientFieldIndex.LastStateOnline, value); }
		}

		/// <summary>The LastStateOperationMode property of the Entity Client<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Client"."LastStateOperationMode".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<Crave.Enums.ClientOperationMode> LastStateOperationMode
		{
			get { return (Nullable<Crave.Enums.ClientOperationMode>)GetValue((int)ClientFieldIndex.LastStateOperationMode, false); }
			set	{ SetValue((int)ClientFieldIndex.LastStateOperationMode, value); }
		}

		/// <summary>The OutOfChargeNotificationsSent property of the Entity Client<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Client"."OutOfChargeNotificationsSent".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 OutOfChargeNotificationsSent
		{
			get { return (System.Int32)GetValue((int)ClientFieldIndex.OutOfChargeNotificationsSent, true); }
			set	{ SetValue((int)ClientFieldIndex.OutOfChargeNotificationsSent, value); }
		}

		/// <summary>The OutOfChargeNotificationLastSentUTC property of the Entity Client<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Client"."OutOfChargeNotificationLastSentUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> OutOfChargeNotificationLastSentUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ClientFieldIndex.OutOfChargeNotificationLastSentUTC, false); }
			set	{ SetValue((int)ClientFieldIndex.OutOfChargeNotificationLastSentUTC, value); }
		}

		/// <summary>The HeartbeatPoll property of the Entity Client<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Client"."HeartbeatPoll".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 HeartbeatPoll
		{
			get { return (System.Int32)GetValue((int)ClientFieldIndex.HeartbeatPoll, true); }
			set	{ SetValue((int)ClientFieldIndex.HeartbeatPoll, value); }
		}

		/// <summary>The LogToFile property of the Entity Client<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Client"."LogToFile".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean LogToFile
		{
			get { return (System.Boolean)GetValue((int)ClientFieldIndex.LogToFile, true); }
			set	{ SetValue((int)ClientFieldIndex.LogToFile, value); }
		}

		/// <summary>The DeviceId property of the Entity Client<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Client"."DeviceId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> DeviceId
		{
			get { return (Nullable<System.Int32>)GetValue((int)ClientFieldIndex.DeviceId, false); }
			set	{ SetValue((int)ClientFieldIndex.DeviceId, value); }
		}

		/// <summary>The DeliverypointId property of the Entity Client<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Client"."DeliverypointId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> DeliverypointId
		{
			get { return (Nullable<System.Int32>)GetValue((int)ClientFieldIndex.DeliverypointId, false); }
			set	{ SetValue((int)ClientFieldIndex.DeliverypointId, value); }
		}

		/// <summary>The BluetoothKeyboardConnected property of the Entity Client<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Client"."BluetoothKeyboardConnected".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean BluetoothKeyboardConnected
		{
			get { return (System.Boolean)GetValue((int)ClientFieldIndex.BluetoothKeyboardConnected, true); }
			set	{ SetValue((int)ClientFieldIndex.BluetoothKeyboardConnected, value); }
		}

		/// <summary>The RoomControlAreaId property of the Entity Client<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Client"."RoomControlAreaId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> RoomControlAreaId
		{
			get { return (Nullable<System.Int32>)GetValue((int)ClientFieldIndex.RoomControlAreaId, false); }
			set	{ SetValue((int)ClientFieldIndex.RoomControlAreaId, value); }
		}

		/// <summary>The WakeUpTimeUtc property of the Entity Client<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Client"."WakeUpTimeUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> WakeUpTimeUtc
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ClientFieldIndex.WakeUpTimeUtc, false); }
			set	{ SetValue((int)ClientFieldIndex.WakeUpTimeUtc, value); }
		}

		/// <summary>The RoomControlConnected property of the Entity Client<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Client"."RoomControlConnected".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean RoomControlConnected
		{
			get { return (System.Boolean)GetValue((int)ClientFieldIndex.RoomControlConnected, true); }
			set	{ SetValue((int)ClientFieldIndex.RoomControlConnected, value); }
		}

		/// <summary>The LoadedSuccessfully property of the Entity Client<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Client"."LoadedSuccessfully".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean LoadedSuccessfully
		{
			get { return (System.Boolean)GetValue((int)ClientFieldIndex.LoadedSuccessfully, true); }
			set	{ SetValue((int)ClientFieldIndex.LoadedSuccessfully, value); }
		}

		/// <summary>The DoNotDisturbActive property of the Entity Client<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Client"."DoNotDisturbActive".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean DoNotDisturbActive
		{
			get { return (System.Boolean)GetValue((int)ClientFieldIndex.DoNotDisturbActive, true); }
			set	{ SetValue((int)ClientFieldIndex.DoNotDisturbActive, value); }
		}

		/// <summary>The ServiceRoomActive property of the Entity Client<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Client"."ServiceRoomActive".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean ServiceRoomActive
		{
			get { return (System.Boolean)GetValue((int)ClientFieldIndex.ServiceRoomActive, true); }
			set	{ SetValue((int)ClientFieldIndex.ServiceRoomActive, value); }
		}

		/// <summary>The LastUserInteraction property of the Entity Client<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Client"."LastUserInteraction".<br/>Table field type characteristics (type, precision, scale, length): BigInt, 19, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> LastUserInteraction
		{
			get { return (Nullable<System.Int64>)GetValue((int)ClientFieldIndex.LastUserInteraction, false); }
			set	{ SetValue((int)ClientFieldIndex.LastUserInteraction, value); }
		}

		/// <summary>The IsTestClient property of the Entity Client<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Client"."IsTestClient".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsTestClient
		{
			get { return (System.Boolean)GetValue((int)ClientFieldIndex.IsTestClient, true); }
			set	{ SetValue((int)ClientFieldIndex.IsTestClient, value); }
		}

		/// <summary>The StatusUpdatedUTC property of the Entity Client<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Client"."StatusUpdatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> StatusUpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ClientFieldIndex.StatusUpdatedUTC, false); }
			set	{ SetValue((int)ClientFieldIndex.StatusUpdatedUTC, value); }
		}

		/// <summary>The CreatedBy property of the Entity Client<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Client"."CreatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)ClientFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)ClientFieldIndex.CreatedBy, value); }
		}

		/// <summary>The UpdatedBy property of the Entity Client<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Client"."UpdatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)ClientFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)ClientFieldIndex.UpdatedBy, value); }
		}

		/// <summary>The CreatedUTC property of the Entity Client<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Client"."CreatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ClientFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)ClientFieldIndex.CreatedUTC, value); }
		}

		/// <summary>The UpdatedUTC property of the Entity Client<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Client"."UpdatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ClientFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)ClientFieldIndex.UpdatedUTC, value); }
		}

		/// <summary>The LastWifiSsid property of the Entity Client<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Client"."LastWifiSsid".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String LastWifiSsid
		{
			get { return (System.String)GetValue((int)ClientFieldIndex.LastWifiSsid, true); }
			set	{ SetValue((int)ClientFieldIndex.LastWifiSsid, value); }
		}

		/// <summary>The LastApiVersion property of the Entity Client<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Client"."LastApiVersion".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 LastApiVersion
		{
			get { return (System.Int32)GetValue((int)ClientFieldIndex.LastApiVersion, true); }
			set	{ SetValue((int)ClientFieldIndex.LastApiVersion, value); }
		}

		/// <summary>Gets the EntityCollection with the related entities of type 'ClientLogEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(ClientLogEntity))]
		public virtual EntityCollection<ClientLogEntity> ClientLogCollection { get { return GetOrCreateEntityCollection<ClientLogEntity, ClientLogEntityFactory>("Client", true, false, ref _clientLogCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'CustomerEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(CustomerEntity))]
		public virtual EntityCollection<CustomerEntity> CustomerCollection { get { return GetOrCreateEntityCollection<CustomerEntity, CustomerEntityFactory>("Client", true, false, ref _customerCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'NetmessageEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(NetmessageEntity))]
		public virtual EntityCollection<NetmessageEntity> NetmessageCollection { get { return GetOrCreateEntityCollection<NetmessageEntity, NetmessageEntityFactory>("Client", true, false, ref _netmessageCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'NetmessageEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(NetmessageEntity))]
		public virtual EntityCollection<NetmessageEntity> NetmessageCollection1 { get { return GetOrCreateEntityCollection<NetmessageEntity, NetmessageEntityFactory>("ClientEntity1", true, false, ref _netmessageCollection1); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'OrderEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(OrderEntity))]
		public virtual EntityCollection<OrderEntity> OrderCollection { get { return GetOrCreateEntityCollection<OrderEntity, OrderEntityFactory>("Client", true, false, ref _orderCollection); } }

		/// <summary>Gets / sets related entity of type 'CompanyEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual CompanyEntity Company
		{
			get { return _company; }
			set { SetSingleRelatedEntityNavigator(value, "Company"); }
		}

		/// <summary>Gets / sets related entity of type 'DeliverypointEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual DeliverypointEntity Deliverypoint
		{
			get { return _deliverypoint; }
			set { SetSingleRelatedEntityNavigator(value, "Deliverypoint"); }
		}

		/// <summary>Gets / sets related entity of type 'DeliverypointEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual DeliverypointEntity DeliverypointEntity1
		{
			get { return _deliverypointEntity1; }
			set { SetSingleRelatedEntityNavigator(value, "DeliverypointEntity1"); }
		}

		/// <summary>Gets / sets related entity of type 'DeliverypointgroupEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual DeliverypointgroupEntity Deliverypointgroup
		{
			get { return _deliverypointgroup; }
			set { SetSingleRelatedEntityNavigator(value, "Deliverypointgroup"); }
		}

		/// <summary>Gets / sets related entity of type 'DeviceEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual DeviceEntity Device
		{
			get { return _device; }
			set { SetSingleRelatedEntityNavigator(value, "Device"); }
		}
 
		/// <summary>Gets the value of the related field this.Device.Identifier.<br/><br/></summary>
		public virtual System.String DeviceIdentifier
		{
			get { return this.Device==null ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : this.Device.Identifier; }
		}

		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen
{
	public enum ClientFieldIndex
	{
		///<summary>ClientId. </summary>
		ClientId,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>Notes. </summary>
		Notes,
		///<summary>DeliverypointgroupId. </summary>
		DeliverypointgroupId,
		///<summary>OperationMode. </summary>
		OperationMode,
		///<summary>LastOperationMode. </summary>
		LastOperationMode,
		///<summary>LastStatus. </summary>
		LastStatus,
		///<summary>LastStatusText. </summary>
		LastStatusText,
		///<summary>LastDeliverypointId. </summary>
		LastDeliverypointId,
		///<summary>LastDeliverypointNumber. </summary>
		LastDeliverypointNumber,
		///<summary>LastCommunicationMethod. </summary>
		LastCommunicationMethod,
		///<summary>LastCloudEnvironment. </summary>
		LastCloudEnvironment,
		///<summary>LastStateOnline. </summary>
		LastStateOnline,
		///<summary>LastStateOperationMode. </summary>
		LastStateOperationMode,
		///<summary>OutOfChargeNotificationsSent. </summary>
		OutOfChargeNotificationsSent,
		///<summary>OutOfChargeNotificationLastSentUTC. </summary>
		OutOfChargeNotificationLastSentUTC,
		///<summary>HeartbeatPoll. </summary>
		HeartbeatPoll,
		///<summary>LogToFile. </summary>
		LogToFile,
		///<summary>DeviceId. </summary>
		DeviceId,
		///<summary>DeliverypointId. </summary>
		DeliverypointId,
		///<summary>BluetoothKeyboardConnected. </summary>
		BluetoothKeyboardConnected,
		///<summary>RoomControlAreaId. </summary>
		RoomControlAreaId,
		///<summary>WakeUpTimeUtc. </summary>
		WakeUpTimeUtc,
		///<summary>RoomControlConnected. </summary>
		RoomControlConnected,
		///<summary>LoadedSuccessfully. </summary>
		LoadedSuccessfully,
		///<summary>DoNotDisturbActive. </summary>
		DoNotDisturbActive,
		///<summary>ServiceRoomActive. </summary>
		ServiceRoomActive,
		///<summary>LastUserInteraction. </summary>
		LastUserInteraction,
		///<summary>IsTestClient. </summary>
		IsTestClient,
		///<summary>StatusUpdatedUTC. </summary>
		StatusUpdatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>LastWifiSsid. </summary>
		LastWifiSsid,
		///<summary>LastApiVersion. </summary>
		LastApiVersion,
		/// <summary></summary>
		AmountOfFields
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Client. </summary>
	public partial class ClientRelations: RelationFactory
	{
		#region Custom Relation code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomRelationCode
        private const ReferentialConstraintDeleteRule DeleteRuleForClientLogEntityUsingClientId = ReferentialConstraintDeleteRule.Cascade;
        private const ReferentialConstraintDeleteRule DeleteRuleForCustomerEntityUsingClientId = ReferentialConstraintDeleteRule.SetNull;
        private const ReferentialConstraintDeleteRule DeleteRuleForNetmessageEntityUsingSenderClientId = ReferentialConstraintDeleteRule.Cascade;
        private const ReferentialConstraintDeleteRule DeleteRuleForNetmessageEntityUsingReceiverClientId = ReferentialConstraintDeleteRule.Cascade;
        private const ReferentialConstraintDeleteRule DeleteRuleForOrderEntityUsingClientId = ReferentialConstraintDeleteRule.SetNull;
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		
		// private const ReferentialConstraintDeleteRule DeleteRuleForClientLogEntityUsingClientId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForCustomerEntityUsingClientId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForNetmessageEntityUsingSenderClientId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForNetmessageEntityUsingReceiverClientId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForOrderEntityUsingClientId = ReferentialConstraintDeleteRule


		/// <summary>Gets all relations and delete rules of the ClientEntity as a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects.</summary>
		/// <returns>a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects</returns>
		public virtual Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> GetRelationsAndDeleteRules()
		{
			Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> toReturn = new Dictionary<IEntityRelation, ReferentialConstraintDeleteRule>();
			toReturn.Add(this.ClientLogEntityUsingClientId, ClientRelations.DeleteRuleForClientLogEntityUsingClientId);
			toReturn.Add(this.CustomerEntityUsingClientId, ClientRelations.DeleteRuleForCustomerEntityUsingClientId);
			toReturn.Add(this.NetmessageEntityUsingSenderClientId, ClientRelations.DeleteRuleForNetmessageEntityUsingSenderClientId);
			toReturn.Add(this.NetmessageEntityUsingReceiverClientId, ClientRelations.DeleteRuleForNetmessageEntityUsingReceiverClientId);
			toReturn.Add(this.OrderEntityUsingClientId, ClientRelations.DeleteRuleForOrderEntityUsingClientId);
			return toReturn;
		}		
	
	
		/// <summary>Returns a new IEntityRelation object, between ClientEntity and ClientLogEntity over the 1:n relation they have, using the relation between the fields: Client.ClientId - ClientLog.ClientId</summary>
		public virtual IEntityRelation ClientLogEntityUsingClientId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "ClientLogCollection", true, new[] { ClientFields.ClientId, ClientLogFields.ClientId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between ClientEntity and CustomerEntity over the 1:n relation they have, using the relation between the fields: Client.ClientId - Customer.ClientId</summary>
		public virtual IEntityRelation CustomerEntityUsingClientId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "CustomerCollection", true, new[] { ClientFields.ClientId, CustomerFields.ClientId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between ClientEntity and NetmessageEntity over the 1:n relation they have, using the relation between the fields: Client.ClientId - Netmessage.SenderClientId</summary>
		public virtual IEntityRelation NetmessageEntityUsingSenderClientId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "NetmessageCollection", true, new[] { ClientFields.ClientId, NetmessageFields.SenderClientId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between ClientEntity and NetmessageEntity over the 1:n relation they have, using the relation between the fields: Client.ClientId - Netmessage.ReceiverClientId</summary>
		public virtual IEntityRelation NetmessageEntityUsingReceiverClientId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "NetmessageCollection1", true, new[] { ClientFields.ClientId, NetmessageFields.ReceiverClientId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between ClientEntity and OrderEntity over the 1:n relation they have, using the relation between the fields: Client.ClientId - Order.ClientId</summary>
		public virtual IEntityRelation OrderEntityUsingClientId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "OrderCollection", true, new[] { ClientFields.ClientId, OrderFields.ClientId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between ClientEntity and CompanyEntity over the m:1 relation they have, using the relation between the fields: Client.CompanyId - Company.CompanyId</summary>
		public virtual IEntityRelation CompanyEntityUsingCompanyId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Company", false, new[] { CompanyFields.CompanyId, ClientFields.CompanyId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between ClientEntity and DeliverypointEntity over the m:1 relation they have, using the relation between the fields: Client.DeliverypointId - Deliverypoint.DeliverypointId</summary>
		public virtual IEntityRelation DeliverypointEntityUsingDeliverypointId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Deliverypoint", false, new[] { DeliverypointFields.DeliverypointId, ClientFields.DeliverypointId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between ClientEntity and DeliverypointEntity over the m:1 relation they have, using the relation between the fields: Client.LastDeliverypointId - Deliverypoint.DeliverypointId</summary>
		public virtual IEntityRelation DeliverypointEntityUsingLastDeliverypointId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "DeliverypointEntity1", false, new[] { DeliverypointFields.DeliverypointId, ClientFields.LastDeliverypointId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between ClientEntity and DeliverypointgroupEntity over the m:1 relation they have, using the relation between the fields: Client.DeliverypointgroupId - Deliverypointgroup.DeliverypointgroupId</summary>
		public virtual IEntityRelation DeliverypointgroupEntityUsingDeliverypointgroupId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Deliverypointgroup", false, new[] { DeliverypointgroupFields.DeliverypointgroupId, ClientFields.DeliverypointgroupId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between ClientEntity and DeviceEntity over the m:1 relation they have, using the relation between the fields: Client.DeviceId - Device.DeviceId</summary>
		public virtual IEntityRelation DeviceEntityUsingDeviceId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Device", false, new[] { DeviceFields.DeviceId, ClientFields.DeviceId }); }
		}

	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticClientRelations
	{
		internal static readonly IEntityRelation ClientLogEntityUsingClientIdStatic = new ClientRelations().ClientLogEntityUsingClientId;
		internal static readonly IEntityRelation CustomerEntityUsingClientIdStatic = new ClientRelations().CustomerEntityUsingClientId;
		internal static readonly IEntityRelation NetmessageEntityUsingSenderClientIdStatic = new ClientRelations().NetmessageEntityUsingSenderClientId;
		internal static readonly IEntityRelation NetmessageEntityUsingReceiverClientIdStatic = new ClientRelations().NetmessageEntityUsingReceiverClientId;
		internal static readonly IEntityRelation OrderEntityUsingClientIdStatic = new ClientRelations().OrderEntityUsingClientId;
		internal static readonly IEntityRelation CompanyEntityUsingCompanyIdStatic = new ClientRelations().CompanyEntityUsingCompanyId;
		internal static readonly IEntityRelation DeliverypointEntityUsingDeliverypointIdStatic = new ClientRelations().DeliverypointEntityUsingDeliverypointId;
		internal static readonly IEntityRelation DeliverypointEntityUsingLastDeliverypointIdStatic = new ClientRelations().DeliverypointEntityUsingLastDeliverypointId;
		internal static readonly IEntityRelation DeliverypointgroupEntityUsingDeliverypointgroupIdStatic = new ClientRelations().DeliverypointgroupEntityUsingDeliverypointgroupId;
		internal static readonly IEntityRelation DeviceEntityUsingDeviceIdStatic = new ClientRelations().DeviceEntityUsingDeviceId;

		/// <summary>CTor</summary>
		static StaticClientRelations() { }
	}
}

