﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 5.8
// Code is generated on: 
// Code is generated using templates: Crave.TemplateBindings
// Templates vendor: Crave Interactive
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.FactoryClasses;
using Crave.Ordering.Shared.Data.LLBLGen.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

// ReSharper disable All
namespace Crave.Ordering.Shared.Data.LLBLGen.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'CheckoutMethodDeliverypointgroup'.<br/><br/></summary>
	[Serializable]
	public partial class CheckoutMethodDeliverypointgroupEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END
	
	{
		private CheckoutMethodEntity _checkoutMethod;
		private CompanyEntity _company;
		private DeliverypointgroupEntity _deliverypointgroup;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		private static CheckoutMethodDeliverypointgroupEntityStaticMetaData _staticMetaData = new CheckoutMethodDeliverypointgroupEntityStaticMetaData();
		private static CheckoutMethodDeliverypointgroupRelations _relationsFactory = new CheckoutMethodDeliverypointgroupRelations();		

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name CheckoutMethod</summary>
			public static readonly string CheckoutMethod = "CheckoutMethod";
			/// <summary>Member name Company</summary>
			public static readonly string Company = "Company";
			/// <summary>Member name Deliverypointgroup</summary>
			public static readonly string Deliverypointgroup = "Deliverypointgroup";
		}

		/// <summary>Static meta-data storage for navigator related information</summary>
		protected class CheckoutMethodDeliverypointgroupEntityStaticMetaData : EntityStaticMetaDataBase
		{
			public CheckoutMethodDeliverypointgroupEntityStaticMetaData()
			{
				SetEntityCoreInfo("CheckoutMethodDeliverypointgroupEntity", InheritanceHierarchyType.None, false, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.CheckoutMethodDeliverypointgroupEntity, typeof(CheckoutMethodDeliverypointgroupEntity), typeof(CheckoutMethodDeliverypointgroupEntityFactory), false);
				AddNavigatorMetaData<CheckoutMethodDeliverypointgroupEntity, CheckoutMethodEntity>("CheckoutMethod", "CheckoutMethodDeliverypointgroupCollection", (a, b) => a._checkoutMethod = b, a => a._checkoutMethod, (a, b) => a.CheckoutMethod = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticCheckoutMethodDeliverypointgroupRelations.CheckoutMethodEntityUsingCheckoutMethodIdStatic, ()=>new CheckoutMethodDeliverypointgroupRelations().CheckoutMethodEntityUsingCheckoutMethodId, null, new int[] { (int)CheckoutMethodDeliverypointgroupFieldIndex.CheckoutMethodId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.CheckoutMethodEntity);
				AddNavigatorMetaData<CheckoutMethodDeliverypointgroupEntity, CompanyEntity>("Company", "CheckoutMethodDeliverypointgroupCollection", (a, b) => a._company = b, a => a._company, (a, b) => a.Company = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticCheckoutMethodDeliverypointgroupRelations.CompanyEntityUsingParentCompanyIdStatic, ()=>new CheckoutMethodDeliverypointgroupRelations().CompanyEntityUsingParentCompanyId, null, new int[] { (int)CheckoutMethodDeliverypointgroupFieldIndex.ParentCompanyId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.CompanyEntity);
				AddNavigatorMetaData<CheckoutMethodDeliverypointgroupEntity, DeliverypointgroupEntity>("Deliverypointgroup", "CheckoutMethodDeliverypointgroupCollection", (a, b) => a._deliverypointgroup = b, a => a._deliverypointgroup, (a, b) => a.Deliverypointgroup = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticCheckoutMethodDeliverypointgroupRelations.DeliverypointgroupEntityUsingDeliverypointgroupIdStatic, ()=>new CheckoutMethodDeliverypointgroupRelations().DeliverypointgroupEntityUsingDeliverypointgroupId, null, new int[] { (int)CheckoutMethodDeliverypointgroupFieldIndex.DeliverypointgroupId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.DeliverypointgroupEntity);
			}
		}

		/// <summary>Static ctor</summary>
		static CheckoutMethodDeliverypointgroupEntity()
		{
		}

		/// <summary> CTor</summary>
		public CheckoutMethodDeliverypointgroupEntity()
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public CheckoutMethodDeliverypointgroupEntity(IEntityFields2 fields)
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this CheckoutMethodDeliverypointgroupEntity</param>
		public CheckoutMethodDeliverypointgroupEntity(IValidator validator)
		{
			InitClassEmpty(validator, null);
		}

		/// <summary> CTor</summary>
		/// <param name="deliverypointgroupId">PK value for CheckoutMethodDeliverypointgroup which data should be fetched into this CheckoutMethodDeliverypointgroup object</param>
		/// <param name="checkoutMethodId">PK value for CheckoutMethodDeliverypointgroup which data should be fetched into this CheckoutMethodDeliverypointgroup object</param>
		public CheckoutMethodDeliverypointgroupEntity(System.Int32 deliverypointgroupId, System.Int32 checkoutMethodId) : this(deliverypointgroupId, checkoutMethodId, null)
		{
		}

		/// <summary> CTor</summary>
		/// <param name="deliverypointgroupId">PK value for CheckoutMethodDeliverypointgroup which data should be fetched into this CheckoutMethodDeliverypointgroup object</param>
		/// <param name="checkoutMethodId">PK value for CheckoutMethodDeliverypointgroup which data should be fetched into this CheckoutMethodDeliverypointgroup object</param>
		/// <param name="validator">The custom validator object for this CheckoutMethodDeliverypointgroupEntity</param>
		public CheckoutMethodDeliverypointgroupEntity(System.Int32 deliverypointgroupId, System.Int32 checkoutMethodId, IValidator validator)
		{
			InitClassEmpty(validator, null);
			this.DeliverypointgroupId = deliverypointgroupId;
			this.CheckoutMethodId = checkoutMethodId;
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected CheckoutMethodDeliverypointgroupEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'CheckoutMethod' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoCheckoutMethod() { return CreateRelationInfoForNavigator("CheckoutMethod"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Company' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoCompany() { return CreateRelationInfoForNavigator("Company"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Deliverypointgroup' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoDeliverypointgroup() { return CreateRelationInfoForNavigator("Deliverypointgroup"); }
		
		/// <inheritdoc/>
		protected override EntityStaticMetaDataBase GetEntityStaticMetaData() {	return _staticMetaData; }

		/// <summary>Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		/// <summary>Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this CheckoutMethodDeliverypointgroupEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary>The relations object holding all relations of this entity with other entity classes.</summary>
		public static CheckoutMethodDeliverypointgroupRelations Relations { get { return _relationsFactory; } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'CheckoutMethod' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathCheckoutMethodEntity { get { return _staticMetaData.GetPrefetchPathElement("CheckoutMethod", CommonEntityBase.CreateEntityCollection<CheckoutMethodEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Company' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathCompanyEntity { get { return _staticMetaData.GetPrefetchPathElement("Company", CommonEntityBase.CreateEntityCollection<CompanyEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Deliverypointgroup' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathDeliverypointgroupEntity { get { return _staticMetaData.GetPrefetchPathElement("Deliverypointgroup", CommonEntityBase.CreateEntityCollection<DeliverypointgroupEntity>()); } }

		/// <summary>The DeliverypointgroupId property of the Entity CheckoutMethodDeliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CheckoutMethodDeliverypointgroup"."DeliverypointgroupId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int32 DeliverypointgroupId
		{
			get { return (System.Int32)GetValue((int)CheckoutMethodDeliverypointgroupFieldIndex.DeliverypointgroupId, true); }
			set	{ SetValue((int)CheckoutMethodDeliverypointgroupFieldIndex.DeliverypointgroupId, value); }
		}

		/// <summary>The CheckoutMethodId property of the Entity CheckoutMethodDeliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CheckoutMethodDeliverypointgroup"."CheckoutMethodId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int32 CheckoutMethodId
		{
			get { return (System.Int32)GetValue((int)CheckoutMethodDeliverypointgroupFieldIndex.CheckoutMethodId, true); }
			set	{ SetValue((int)CheckoutMethodDeliverypointgroupFieldIndex.CheckoutMethodId, value); }
		}

		/// <summary>The ParentCompanyId property of the Entity CheckoutMethodDeliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CheckoutMethodDeliverypointgroup"."ParentCompanyId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ParentCompanyId
		{
			get { return (System.Int32)GetValue((int)CheckoutMethodDeliverypointgroupFieldIndex.ParentCompanyId, true); }
			set	{ SetValue((int)CheckoutMethodDeliverypointgroupFieldIndex.ParentCompanyId, value); }
		}

		/// <summary>The CreatedUTC property of the Entity CheckoutMethodDeliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CheckoutMethodDeliverypointgroup"."CreatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)CheckoutMethodDeliverypointgroupFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)CheckoutMethodDeliverypointgroupFieldIndex.CreatedUTC, value); }
		}

		/// <summary>The CreatedBy property of the Entity CheckoutMethodDeliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CheckoutMethodDeliverypointgroup"."CreatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)CheckoutMethodDeliverypointgroupFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)CheckoutMethodDeliverypointgroupFieldIndex.CreatedBy, value); }
		}

		/// <summary>The UpdatedUTC property of the Entity CheckoutMethodDeliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CheckoutMethodDeliverypointgroup"."UpdatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)CheckoutMethodDeliverypointgroupFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)CheckoutMethodDeliverypointgroupFieldIndex.UpdatedUTC, value); }
		}

		/// <summary>The UpdatedBy property of the Entity CheckoutMethodDeliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CheckoutMethodDeliverypointgroup"."UpdatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)CheckoutMethodDeliverypointgroupFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)CheckoutMethodDeliverypointgroupFieldIndex.UpdatedBy, value); }
		}

		/// <summary>Gets / sets related entity of type 'CheckoutMethodEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual CheckoutMethodEntity CheckoutMethod
		{
			get { return _checkoutMethod; }
			set { SetSingleRelatedEntityNavigator(value, "CheckoutMethod"); }
		}

		/// <summary>Gets / sets related entity of type 'CompanyEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual CompanyEntity Company
		{
			get { return _company; }
			set { SetSingleRelatedEntityNavigator(value, "Company"); }
		}

		/// <summary>Gets / sets related entity of type 'DeliverypointgroupEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual DeliverypointgroupEntity Deliverypointgroup
		{
			get { return _deliverypointgroup; }
			set { SetSingleRelatedEntityNavigator(value, "Deliverypointgroup"); }
		}


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen
{
	public enum CheckoutMethodDeliverypointgroupFieldIndex
	{
		///<summary>DeliverypointgroupId. </summary>
		DeliverypointgroupId,
		///<summary>CheckoutMethodId. </summary>
		CheckoutMethodId,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		/// <summary></summary>
		AmountOfFields
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: CheckoutMethodDeliverypointgroup. </summary>
	public partial class CheckoutMethodDeliverypointgroupRelations: RelationFactory
	{
		#region Custom Relation code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomRelationCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		


		/// <summary>Gets all relations and delete rules of the CheckoutMethodDeliverypointgroupEntity as a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects.</summary>
		/// <returns>a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects</returns>
		public virtual Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> GetRelationsAndDeleteRules()
		{
			Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> toReturn = new Dictionary<IEntityRelation, ReferentialConstraintDeleteRule>();
			return toReturn;
		}		
	
	

		/// <summary>Returns a new IEntityRelation object, between CheckoutMethodDeliverypointgroupEntity and CheckoutMethodEntity over the m:1 relation they have, using the relation between the fields: CheckoutMethodDeliverypointgroup.CheckoutMethodId - CheckoutMethod.CheckoutMethodId</summary>
		public virtual IEntityRelation CheckoutMethodEntityUsingCheckoutMethodId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "CheckoutMethod", false, new[] { CheckoutMethodFields.CheckoutMethodId, CheckoutMethodDeliverypointgroupFields.CheckoutMethodId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between CheckoutMethodDeliverypointgroupEntity and CompanyEntity over the m:1 relation they have, using the relation between the fields: CheckoutMethodDeliverypointgroup.ParentCompanyId - Company.CompanyId</summary>
		public virtual IEntityRelation CompanyEntityUsingParentCompanyId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Company", false, new[] { CompanyFields.CompanyId, CheckoutMethodDeliverypointgroupFields.ParentCompanyId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between CheckoutMethodDeliverypointgroupEntity and DeliverypointgroupEntity over the m:1 relation they have, using the relation between the fields: CheckoutMethodDeliverypointgroup.DeliverypointgroupId - Deliverypointgroup.DeliverypointgroupId</summary>
		public virtual IEntityRelation DeliverypointgroupEntityUsingDeliverypointgroupId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Deliverypointgroup", false, new[] { DeliverypointgroupFields.DeliverypointgroupId, CheckoutMethodDeliverypointgroupFields.DeliverypointgroupId }); }
		}

	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticCheckoutMethodDeliverypointgroupRelations
	{
		internal static readonly IEntityRelation CheckoutMethodEntityUsingCheckoutMethodIdStatic = new CheckoutMethodDeliverypointgroupRelations().CheckoutMethodEntityUsingCheckoutMethodId;
		internal static readonly IEntityRelation CompanyEntityUsingParentCompanyIdStatic = new CheckoutMethodDeliverypointgroupRelations().CompanyEntityUsingParentCompanyId;
		internal static readonly IEntityRelation DeliverypointgroupEntityUsingDeliverypointgroupIdStatic = new CheckoutMethodDeliverypointgroupRelations().DeliverypointgroupEntityUsingDeliverypointgroupId;

		/// <summary>CTor</summary>
		static StaticCheckoutMethodDeliverypointgroupRelations() { }
	}
}

