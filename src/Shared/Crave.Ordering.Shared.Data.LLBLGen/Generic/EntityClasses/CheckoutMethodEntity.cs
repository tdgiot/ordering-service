﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 5.8
// Code is generated on: 
// Code is generated using templates: Crave.TemplateBindings
// Templates vendor: Crave Interactive
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.FactoryClasses;
using Crave.Ordering.Shared.Data.LLBLGen.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

// ReSharper disable All
namespace Crave.Ordering.Shared.Data.LLBLGen.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'CheckoutMethod'.<br/><br/></summary>
	[Serializable]
	public partial class CheckoutMethodEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END
	
	{
		private EntityCollection<CheckoutMethodDeliverypointgroupEntity> _checkoutMethodDeliverypointgroupCollection;
		private EntityCollection<OrderEntity> _orderCollection;
		private CompanyEntity _company;
		private OutletEntity _outlet;
		private PaymentIntegrationConfigurationEntity _paymentIntegrationConfiguration;
		private ReceiptTemplateEntity _receiptTemplate;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		private static CheckoutMethodEntityStaticMetaData _staticMetaData = new CheckoutMethodEntityStaticMetaData();
		private static CheckoutMethodRelations _relationsFactory = new CheckoutMethodRelations();		

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Company</summary>
			public static readonly string Company = "Company";
			/// <summary>Member name Outlet</summary>
			public static readonly string Outlet = "Outlet";
			/// <summary>Member name PaymentIntegrationConfiguration</summary>
			public static readonly string PaymentIntegrationConfiguration = "PaymentIntegrationConfiguration";
			/// <summary>Member name ReceiptTemplate</summary>
			public static readonly string ReceiptTemplate = "ReceiptTemplate";
			/// <summary>Member name CheckoutMethodDeliverypointgroupCollection</summary>
			public static readonly string CheckoutMethodDeliverypointgroupCollection = "CheckoutMethodDeliverypointgroupCollection";
			/// <summary>Member name OrderCollection</summary>
			public static readonly string OrderCollection = "OrderCollection";
		}

		/// <summary>Static meta-data storage for navigator related information</summary>
		protected class CheckoutMethodEntityStaticMetaData : EntityStaticMetaDataBase
		{
			public CheckoutMethodEntityStaticMetaData()
			{
				SetEntityCoreInfo("CheckoutMethodEntity", InheritanceHierarchyType.None, false, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.CheckoutMethodEntity, typeof(CheckoutMethodEntity), typeof(CheckoutMethodEntityFactory), false);
				AddNavigatorMetaData<CheckoutMethodEntity, EntityCollection<CheckoutMethodDeliverypointgroupEntity>>("CheckoutMethodDeliverypointgroupCollection", a => a._checkoutMethodDeliverypointgroupCollection, (a, b) => a._checkoutMethodDeliverypointgroupCollection = b, a => a.CheckoutMethodDeliverypointgroupCollection, () => new CheckoutMethodRelations().CheckoutMethodDeliverypointgroupEntityUsingCheckoutMethodId, typeof(CheckoutMethodDeliverypointgroupEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.CheckoutMethodDeliverypointgroupEntity);
				AddNavigatorMetaData<CheckoutMethodEntity, EntityCollection<OrderEntity>>("OrderCollection", a => a._orderCollection, (a, b) => a._orderCollection = b, a => a.OrderCollection, () => new CheckoutMethodRelations().OrderEntityUsingCheckoutMethodId, typeof(OrderEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.OrderEntity);
				AddNavigatorMetaData<CheckoutMethodEntity, CompanyEntity>("Company", "CheckoutMethodCollection", (a, b) => a._company = b, a => a._company, (a, b) => a.Company = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticCheckoutMethodRelations.CompanyEntityUsingCompanyIdStatic, ()=>new CheckoutMethodRelations().CompanyEntityUsingCompanyId, null, new int[] { (int)CheckoutMethodFieldIndex.CompanyId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.CompanyEntity);
				AddNavigatorMetaData<CheckoutMethodEntity, OutletEntity>("Outlet", "CheckoutMethodCollection", (a, b) => a._outlet = b, a => a._outlet, (a, b) => a.Outlet = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticCheckoutMethodRelations.OutletEntityUsingOutletIdStatic, ()=>new CheckoutMethodRelations().OutletEntityUsingOutletId, null, new int[] { (int)CheckoutMethodFieldIndex.OutletId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.OutletEntity);
				AddNavigatorMetaData<CheckoutMethodEntity, PaymentIntegrationConfigurationEntity>("PaymentIntegrationConfiguration", "CheckoutMethodCollection", (a, b) => a._paymentIntegrationConfiguration = b, a => a._paymentIntegrationConfiguration, (a, b) => a.PaymentIntegrationConfiguration = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticCheckoutMethodRelations.PaymentIntegrationConfigurationEntityUsingPaymentIntegrationConfigurationIdStatic, ()=>new CheckoutMethodRelations().PaymentIntegrationConfigurationEntityUsingPaymentIntegrationConfigurationId, null, new int[] { (int)CheckoutMethodFieldIndex.PaymentIntegrationConfigurationId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.PaymentIntegrationConfigurationEntity);
				AddNavigatorMetaData<CheckoutMethodEntity, ReceiptTemplateEntity>("ReceiptTemplate", "CheckoutMethodCollection", (a, b) => a._receiptTemplate = b, a => a._receiptTemplate, (a, b) => a.ReceiptTemplate = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticCheckoutMethodRelations.ReceiptTemplateEntityUsingReceiptTemplateIdStatic, ()=>new CheckoutMethodRelations().ReceiptTemplateEntityUsingReceiptTemplateId, null, new int[] { (int)CheckoutMethodFieldIndex.ReceiptTemplateId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ReceiptTemplateEntity);
			}
		}

		/// <summary>Static ctor</summary>
		static CheckoutMethodEntity()
		{
		}

		/// <summary> CTor</summary>
		public CheckoutMethodEntity()
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public CheckoutMethodEntity(IEntityFields2 fields)
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this CheckoutMethodEntity</param>
		public CheckoutMethodEntity(IValidator validator)
		{
			InitClassEmpty(validator, null);
		}

		/// <summary> CTor</summary>
		/// <param name="checkoutMethodId">PK value for CheckoutMethod which data should be fetched into this CheckoutMethod object</param>
		public CheckoutMethodEntity(System.Int32 checkoutMethodId) : this(checkoutMethodId, null)
		{
		}

		/// <summary> CTor</summary>
		/// <param name="checkoutMethodId">PK value for CheckoutMethod which data should be fetched into this CheckoutMethod object</param>
		/// <param name="validator">The custom validator object for this CheckoutMethodEntity</param>
		public CheckoutMethodEntity(System.Int32 checkoutMethodId, IValidator validator)
		{
			InitClassEmpty(validator, null);
			this.CheckoutMethodId = checkoutMethodId;
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected CheckoutMethodEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'CheckoutMethodDeliverypointgroup' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoCheckoutMethodDeliverypointgroupCollection() { return CreateRelationInfoForNavigator("CheckoutMethodDeliverypointgroupCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Order' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoOrderCollection() { return CreateRelationInfoForNavigator("OrderCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Company' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoCompany() { return CreateRelationInfoForNavigator("Company"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Outlet' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoOutlet() { return CreateRelationInfoForNavigator("Outlet"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'PaymentIntegrationConfiguration' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoPaymentIntegrationConfiguration() { return CreateRelationInfoForNavigator("PaymentIntegrationConfiguration"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'ReceiptTemplate' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoReceiptTemplate() { return CreateRelationInfoForNavigator("ReceiptTemplate"); }
		
		/// <inheritdoc/>
		protected override EntityStaticMetaDataBase GetEntityStaticMetaData() {	return _staticMetaData; }

		/// <summary>Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		/// <summary>Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this CheckoutMethodEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary>The relations object holding all relations of this entity with other entity classes.</summary>
		public static CheckoutMethodRelations Relations { get { return _relationsFactory; } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'CheckoutMethodDeliverypointgroup' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathCheckoutMethodDeliverypointgroupCollection { get { return _staticMetaData.GetPrefetchPathElement("CheckoutMethodDeliverypointgroupCollection", CommonEntityBase.CreateEntityCollection<CheckoutMethodDeliverypointgroupEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Order' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathOrderCollection { get { return _staticMetaData.GetPrefetchPathElement("OrderCollection", CommonEntityBase.CreateEntityCollection<OrderEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Company' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathCompanyEntity { get { return _staticMetaData.GetPrefetchPathElement("Company", CommonEntityBase.CreateEntityCollection<CompanyEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Outlet' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathOutletEntity { get { return _staticMetaData.GetPrefetchPathElement("Outlet", CommonEntityBase.CreateEntityCollection<OutletEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'PaymentIntegrationConfiguration' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathPaymentIntegrationConfigurationEntity { get { return _staticMetaData.GetPrefetchPathElement("PaymentIntegrationConfiguration", CommonEntityBase.CreateEntityCollection<PaymentIntegrationConfigurationEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'ReceiptTemplate' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathReceiptTemplateEntity { get { return _staticMetaData.GetPrefetchPathElement("ReceiptTemplate", CommonEntityBase.CreateEntityCollection<ReceiptTemplateEntity>()); } }

		/// <summary>The CheckoutMethodId property of the Entity CheckoutMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CheckoutMethod"."CheckoutMethodId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 CheckoutMethodId
		{
			get { return (System.Int32)GetValue((int)CheckoutMethodFieldIndex.CheckoutMethodId, true); }
			set { SetValue((int)CheckoutMethodFieldIndex.CheckoutMethodId, value); }		}

		/// <summary>The CompanyId property of the Entity CheckoutMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CheckoutMethod"."CompanyId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CompanyId
		{
			get { return (System.Int32)GetValue((int)CheckoutMethodFieldIndex.CompanyId, true); }
			set	{ SetValue((int)CheckoutMethodFieldIndex.CompanyId, value); }
		}

		/// <summary>The OutletId property of the Entity CheckoutMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CheckoutMethod"."OutletId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> OutletId
		{
			get { return (Nullable<System.Int32>)GetValue((int)CheckoutMethodFieldIndex.OutletId, false); }
			set	{ SetValue((int)CheckoutMethodFieldIndex.OutletId, value); }
		}

		/// <summary>The ReceiptTemplateId property of the Entity CheckoutMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CheckoutMethod"."ReceiptTemplateId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ReceiptTemplateId
		{
			get { return (Nullable<System.Int32>)GetValue((int)CheckoutMethodFieldIndex.ReceiptTemplateId, false); }
			set	{ SetValue((int)CheckoutMethodFieldIndex.ReceiptTemplateId, value); }
		}

		/// <summary>The Active property of the Entity CheckoutMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CheckoutMethod"."Active".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean Active
		{
			get { return (System.Boolean)GetValue((int)CheckoutMethodFieldIndex.Active, true); }
			set	{ SetValue((int)CheckoutMethodFieldIndex.Active, value); }
		}

		/// <summary>The Name property of the Entity CheckoutMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CheckoutMethod"."Name".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 256.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)CheckoutMethodFieldIndex.Name, true); }
			set	{ SetValue((int)CheckoutMethodFieldIndex.Name, value); }
		}

		/// <summary>The Label property of the Entity CheckoutMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CheckoutMethod"."Label".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 256.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Label
		{
			get { return (System.String)GetValue((int)CheckoutMethodFieldIndex.Label, true); }
			set	{ SetValue((int)CheckoutMethodFieldIndex.Label, value); }
		}

		/// <summary>The Description property of the Entity CheckoutMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CheckoutMethod"."Description".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 256.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)CheckoutMethodFieldIndex.Description, true); }
			set	{ SetValue((int)CheckoutMethodFieldIndex.Description, value); }
		}

		/// <summary>The CheckoutType property of the Entity CheckoutMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CheckoutMethod"."CheckoutType".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Crave.Enums.CheckoutType CheckoutType
		{
			get { return (Crave.Enums.CheckoutType)GetValue((int)CheckoutMethodFieldIndex.CheckoutType, true); }
			set	{ SetValue((int)CheckoutMethodFieldIndex.CheckoutType, value); }
		}

		/// <summary>The ConfirmationActive property of the Entity CheckoutMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CheckoutMethod"."ConfirmationActive".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> ConfirmationActive
		{
			get { return (Nullable<System.Boolean>)GetValue((int)CheckoutMethodFieldIndex.ConfirmationActive, false); }
			set	{ SetValue((int)CheckoutMethodFieldIndex.ConfirmationActive, value); }
		}

		/// <summary>The ConfirmationDescription property of the Entity CheckoutMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CheckoutMethod"."ConfirmationDescription".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 512.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ConfirmationDescription
		{
			get { return (System.String)GetValue((int)CheckoutMethodFieldIndex.ConfirmationDescription, true); }
			set	{ SetValue((int)CheckoutMethodFieldIndex.ConfirmationDescription, value); }
		}

		/// <summary>The CreatedUTC property of the Entity CheckoutMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CheckoutMethod"."CreatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)CheckoutMethodFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)CheckoutMethodFieldIndex.CreatedUTC, value); }
		}

		/// <summary>The CreatedBy property of the Entity CheckoutMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CheckoutMethod"."CreatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)CheckoutMethodFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)CheckoutMethodFieldIndex.CreatedBy, value); }
		}

		/// <summary>The UpdatedUTC property of the Entity CheckoutMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CheckoutMethod"."UpdatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)CheckoutMethodFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)CheckoutMethodFieldIndex.UpdatedUTC, value); }
		}

		/// <summary>The UpdatedBy property of the Entity CheckoutMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CheckoutMethod"."UpdatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)CheckoutMethodFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)CheckoutMethodFieldIndex.UpdatedBy, value); }
		}

		/// <summary>The PaymentIntegrationConfigurationId property of the Entity CheckoutMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CheckoutMethod"."PaymentIntegrationConfigurationId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> PaymentIntegrationConfigurationId
		{
			get { return (Nullable<System.Int32>)GetValue((int)CheckoutMethodFieldIndex.PaymentIntegrationConfigurationId, false); }
			set	{ SetValue((int)CheckoutMethodFieldIndex.PaymentIntegrationConfigurationId, value); }
		}

		/// <summary>The OrderConfirmationNotificationMethod property of the Entity CheckoutMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CheckoutMethod"."OrderConfirmationNotificationMethod".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Crave.Enums.OrderNotificationMethod OrderConfirmationNotificationMethod
		{
			get { return (Crave.Enums.OrderNotificationMethod)GetValue((int)CheckoutMethodFieldIndex.OrderConfirmationNotificationMethod, true); }
			set	{ SetValue((int)CheckoutMethodFieldIndex.OrderConfirmationNotificationMethod, value); }
		}

		/// <summary>The OrderPaymentPendingNotificationMethod property of the Entity CheckoutMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CheckoutMethod"."OrderPaymentPendingNotificationMethod".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Crave.Enums.OrderNotificationMethod OrderPaymentPendingNotificationMethod
		{
			get { return (Crave.Enums.OrderNotificationMethod)GetValue((int)CheckoutMethodFieldIndex.OrderPaymentPendingNotificationMethod, true); }
			set	{ SetValue((int)CheckoutMethodFieldIndex.OrderPaymentPendingNotificationMethod, value); }
		}

		/// <summary>The OrderPaymentFailedNotificationMethod property of the Entity CheckoutMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CheckoutMethod"."OrderPaymentFailedNotificationMethod".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Crave.Enums.OrderNotificationMethod OrderPaymentFailedNotificationMethod
		{
			get { return (Crave.Enums.OrderNotificationMethod)GetValue((int)CheckoutMethodFieldIndex.OrderPaymentFailedNotificationMethod, true); }
			set	{ SetValue((int)CheckoutMethodFieldIndex.OrderPaymentFailedNotificationMethod, value); }
		}

		/// <summary>The OrderReceiptNotificationMethod property of the Entity CheckoutMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CheckoutMethod"."OrderReceiptNotificationMethod".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Crave.Enums.OrderNotificationMethod OrderReceiptNotificationMethod
		{
			get { return (Crave.Enums.OrderNotificationMethod)GetValue((int)CheckoutMethodFieldIndex.OrderReceiptNotificationMethod, true); }
			set	{ SetValue((int)CheckoutMethodFieldIndex.OrderReceiptNotificationMethod, value); }
		}

		/// <summary>The TermsAndConditionsRequired property of the Entity CheckoutMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CheckoutMethod"."TermsAndConditionsRequired".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean TermsAndConditionsRequired
		{
			get { return (System.Boolean)GetValue((int)CheckoutMethodFieldIndex.TermsAndConditionsRequired, true); }
			set	{ SetValue((int)CheckoutMethodFieldIndex.TermsAndConditionsRequired, value); }
		}

		/// <summary>The TermsAndConditionsLabel property of the Entity CheckoutMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CheckoutMethod"."TermsAndConditionsLabel".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String TermsAndConditionsLabel
		{
			get { return (System.String)GetValue((int)CheckoutMethodFieldIndex.TermsAndConditionsLabel, true); }
			set	{ SetValue((int)CheckoutMethodFieldIndex.TermsAndConditionsLabel, value); }
		}

		/// <summary>The CurrencyCode property of the Entity CheckoutMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CheckoutMethod"."CurrencyCode".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 3.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CurrencyCode
		{
			get { return (System.String)GetValue((int)CheckoutMethodFieldIndex.CurrencyCode, true); }
			set	{ SetValue((int)CheckoutMethodFieldIndex.CurrencyCode, value); }
		}

		/// <summary>The CountryCode property of the Entity CheckoutMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CheckoutMethod"."CountryCode".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 3.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CountryCode
		{
			get { return (System.String)GetValue((int)CheckoutMethodFieldIndex.CountryCode, true); }
			set	{ SetValue((int)CheckoutMethodFieldIndex.CountryCode, value); }
		}

		/// <summary>Gets the EntityCollection with the related entities of type 'CheckoutMethodDeliverypointgroupEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(CheckoutMethodDeliverypointgroupEntity))]
		public virtual EntityCollection<CheckoutMethodDeliverypointgroupEntity> CheckoutMethodDeliverypointgroupCollection { get { return GetOrCreateEntityCollection<CheckoutMethodDeliverypointgroupEntity, CheckoutMethodDeliverypointgroupEntityFactory>("CheckoutMethod", true, false, ref _checkoutMethodDeliverypointgroupCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'OrderEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(OrderEntity))]
		public virtual EntityCollection<OrderEntity> OrderCollection { get { return GetOrCreateEntityCollection<OrderEntity, OrderEntityFactory>("CheckoutMethod", true, false, ref _orderCollection); } }

		/// <summary>Gets / sets related entity of type 'CompanyEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual CompanyEntity Company
		{
			get { return _company; }
			set { SetSingleRelatedEntityNavigator(value, "Company"); }
		}

		/// <summary>Gets / sets related entity of type 'OutletEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual OutletEntity Outlet
		{
			get { return _outlet; }
			set { SetSingleRelatedEntityNavigator(value, "Outlet"); }
		}

		/// <summary>Gets / sets related entity of type 'PaymentIntegrationConfigurationEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual PaymentIntegrationConfigurationEntity PaymentIntegrationConfiguration
		{
			get { return _paymentIntegrationConfiguration; }
			set { SetSingleRelatedEntityNavigator(value, "PaymentIntegrationConfiguration"); }
		}

		/// <summary>Gets / sets related entity of type 'ReceiptTemplateEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual ReceiptTemplateEntity ReceiptTemplate
		{
			get { return _receiptTemplate; }
			set { SetSingleRelatedEntityNavigator(value, "ReceiptTemplate"); }
		}


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen
{
	public enum CheckoutMethodFieldIndex
	{
		///<summary>CheckoutMethodId. </summary>
		CheckoutMethodId,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>OutletId. </summary>
		OutletId,
		///<summary>ReceiptTemplateId. </summary>
		ReceiptTemplateId,
		///<summary>Active. </summary>
		Active,
		///<summary>Name. </summary>
		Name,
		///<summary>Label. </summary>
		Label,
		///<summary>Description. </summary>
		Description,
		///<summary>CheckoutType. </summary>
		CheckoutType,
		///<summary>ConfirmationActive. </summary>
		ConfirmationActive,
		///<summary>ConfirmationDescription. </summary>
		ConfirmationDescription,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>PaymentIntegrationConfigurationId. </summary>
		PaymentIntegrationConfigurationId,
		///<summary>OrderConfirmationNotificationMethod. </summary>
		OrderConfirmationNotificationMethod,
		///<summary>OrderPaymentPendingNotificationMethod. </summary>
		OrderPaymentPendingNotificationMethod,
		///<summary>OrderPaymentFailedNotificationMethod. </summary>
		OrderPaymentFailedNotificationMethod,
		///<summary>OrderReceiptNotificationMethod. </summary>
		OrderReceiptNotificationMethod,
		///<summary>TermsAndConditionsRequired. </summary>
		TermsAndConditionsRequired,
		///<summary>TermsAndConditionsLabel. </summary>
		TermsAndConditionsLabel,
		///<summary>CurrencyCode. </summary>
		CurrencyCode,
		///<summary>CountryCode. </summary>
		CountryCode,
		/// <summary></summary>
		AmountOfFields
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: CheckoutMethod. </summary>
	public partial class CheckoutMethodRelations: RelationFactory
	{
		#region Custom Relation code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomRelationCode
        private const ReferentialConstraintDeleteRule DeleteRuleForOrderEntityUsingCheckoutMethodId = ReferentialConstraintDeleteRule.Cascade;
        private const ReferentialConstraintDeleteRule DeleteRuleForCheckoutMethodDeliverypointgroupEntityUsingCheckoutMethodId = ReferentialConstraintDeleteRule.Cascade;
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		
		// private const ReferentialConstraintDeleteRule DeleteRuleForCheckoutMethodDeliverypointgroupEntityUsingCheckoutMethodId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForOrderEntityUsingCheckoutMethodId = ReferentialConstraintDeleteRule


		/// <summary>Gets all relations and delete rules of the CheckoutMethodEntity as a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects.</summary>
		/// <returns>a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects</returns>
		public virtual Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> GetRelationsAndDeleteRules()
		{
			Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> toReturn = new Dictionary<IEntityRelation, ReferentialConstraintDeleteRule>();
			toReturn.Add(this.CheckoutMethodDeliverypointgroupEntityUsingCheckoutMethodId, CheckoutMethodRelations.DeleteRuleForCheckoutMethodDeliverypointgroupEntityUsingCheckoutMethodId);
			toReturn.Add(this.OrderEntityUsingCheckoutMethodId, CheckoutMethodRelations.DeleteRuleForOrderEntityUsingCheckoutMethodId);
			return toReturn;
		}		
	
	
		/// <summary>Returns a new IEntityRelation object, between CheckoutMethodEntity and CheckoutMethodDeliverypointgroupEntity over the 1:n relation they have, using the relation between the fields: CheckoutMethod.CheckoutMethodId - CheckoutMethodDeliverypointgroup.CheckoutMethodId</summary>
		public virtual IEntityRelation CheckoutMethodDeliverypointgroupEntityUsingCheckoutMethodId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "CheckoutMethodDeliverypointgroupCollection", true, new[] { CheckoutMethodFields.CheckoutMethodId, CheckoutMethodDeliverypointgroupFields.CheckoutMethodId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between CheckoutMethodEntity and OrderEntity over the 1:n relation they have, using the relation between the fields: CheckoutMethod.CheckoutMethodId - Order.CheckoutMethodId</summary>
		public virtual IEntityRelation OrderEntityUsingCheckoutMethodId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "OrderCollection", true, new[] { CheckoutMethodFields.CheckoutMethodId, OrderFields.CheckoutMethodId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between CheckoutMethodEntity and CompanyEntity over the m:1 relation they have, using the relation between the fields: CheckoutMethod.CompanyId - Company.CompanyId</summary>
		public virtual IEntityRelation CompanyEntityUsingCompanyId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Company", false, new[] { CompanyFields.CompanyId, CheckoutMethodFields.CompanyId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between CheckoutMethodEntity and OutletEntity over the m:1 relation they have, using the relation between the fields: CheckoutMethod.OutletId - Outlet.OutletId</summary>
		public virtual IEntityRelation OutletEntityUsingOutletId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Outlet", false, new[] { OutletFields.OutletId, CheckoutMethodFields.OutletId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between CheckoutMethodEntity and PaymentIntegrationConfigurationEntity over the m:1 relation they have, using the relation between the fields: CheckoutMethod.PaymentIntegrationConfigurationId - PaymentIntegrationConfiguration.PaymentIntegrationConfigurationId</summary>
		public virtual IEntityRelation PaymentIntegrationConfigurationEntityUsingPaymentIntegrationConfigurationId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "PaymentIntegrationConfiguration", false, new[] { PaymentIntegrationConfigurationFields.PaymentIntegrationConfigurationId, CheckoutMethodFields.PaymentIntegrationConfigurationId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between CheckoutMethodEntity and ReceiptTemplateEntity over the m:1 relation they have, using the relation between the fields: CheckoutMethod.ReceiptTemplateId - ReceiptTemplate.ReceiptTemplateId</summary>
		public virtual IEntityRelation ReceiptTemplateEntityUsingReceiptTemplateId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "ReceiptTemplate", false, new[] { ReceiptTemplateFields.ReceiptTemplateId, CheckoutMethodFields.ReceiptTemplateId }); }
		}

	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticCheckoutMethodRelations
	{
		internal static readonly IEntityRelation CheckoutMethodDeliverypointgroupEntityUsingCheckoutMethodIdStatic = new CheckoutMethodRelations().CheckoutMethodDeliverypointgroupEntityUsingCheckoutMethodId;
		internal static readonly IEntityRelation OrderEntityUsingCheckoutMethodIdStatic = new CheckoutMethodRelations().OrderEntityUsingCheckoutMethodId;
		internal static readonly IEntityRelation CompanyEntityUsingCompanyIdStatic = new CheckoutMethodRelations().CompanyEntityUsingCompanyId;
		internal static readonly IEntityRelation OutletEntityUsingOutletIdStatic = new CheckoutMethodRelations().OutletEntityUsingOutletId;
		internal static readonly IEntityRelation PaymentIntegrationConfigurationEntityUsingPaymentIntegrationConfigurationIdStatic = new CheckoutMethodRelations().PaymentIntegrationConfigurationEntityUsingPaymentIntegrationConfigurationId;
		internal static readonly IEntityRelation ReceiptTemplateEntityUsingReceiptTemplateIdStatic = new CheckoutMethodRelations().ReceiptTemplateEntityUsingReceiptTemplateId;

		/// <summary>CTor</summary>
		static StaticCheckoutMethodRelations() { }
	}
}

