﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 5.8
// Code is generated on: 
// Code is generated using templates: Crave.TemplateBindings
// Templates vendor: Crave Interactive
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.FactoryClasses;
using Crave.Ordering.Shared.Data.LLBLGen.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

// ReSharper disable All
namespace Crave.Ordering.Shared.Data.LLBLGen.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'AnalyticsProcessingTask'.<br/><br/></summary>
	[Serializable]
	public partial class AnalyticsProcessingTaskEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END
	
	{
		private OrderEntity _order;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		private static AnalyticsProcessingTaskEntityStaticMetaData _staticMetaData = new AnalyticsProcessingTaskEntityStaticMetaData();
		private static AnalyticsProcessingTaskRelations _relationsFactory = new AnalyticsProcessingTaskRelations();		

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Order</summary>
			public static readonly string Order = "Order";
		}

		/// <summary>Static meta-data storage for navigator related information</summary>
		protected class AnalyticsProcessingTaskEntityStaticMetaData : EntityStaticMetaDataBase
		{
			public AnalyticsProcessingTaskEntityStaticMetaData()
			{
				SetEntityCoreInfo("AnalyticsProcessingTaskEntity", InheritanceHierarchyType.None, false, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.AnalyticsProcessingTaskEntity, typeof(AnalyticsProcessingTaskEntity), typeof(AnalyticsProcessingTaskEntityFactory), false);
				AddNavigatorMetaData<AnalyticsProcessingTaskEntity, OrderEntity>("Order", "AnalyticsProcessingTaskCollection", (a, b) => a._order = b, a => a._order, (a, b) => a.Order = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticAnalyticsProcessingTaskRelations.OrderEntityUsingOrderIdStatic, ()=>new AnalyticsProcessingTaskRelations().OrderEntityUsingOrderId, null, new int[] { (int)AnalyticsProcessingTaskFieldIndex.OrderId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.OrderEntity);
			}
		}

		/// <summary>Static ctor</summary>
		static AnalyticsProcessingTaskEntity()
		{
		}

		/// <summary> CTor</summary>
		public AnalyticsProcessingTaskEntity()
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public AnalyticsProcessingTaskEntity(IEntityFields2 fields)
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this AnalyticsProcessingTaskEntity</param>
		public AnalyticsProcessingTaskEntity(IValidator validator)
		{
			InitClassEmpty(validator, null);
		}

		/// <summary> CTor</summary>
		/// <param name="analyticsProcessingTaskId">PK value for AnalyticsProcessingTask which data should be fetched into this AnalyticsProcessingTask object</param>
		public AnalyticsProcessingTaskEntity(System.Int32 analyticsProcessingTaskId) : this(analyticsProcessingTaskId, null)
		{
		}

		/// <summary> CTor</summary>
		/// <param name="analyticsProcessingTaskId">PK value for AnalyticsProcessingTask which data should be fetched into this AnalyticsProcessingTask object</param>
		/// <param name="validator">The custom validator object for this AnalyticsProcessingTaskEntity</param>
		public AnalyticsProcessingTaskEntity(System.Int32 analyticsProcessingTaskId, IValidator validator)
		{
			InitClassEmpty(validator, null);
			this.AnalyticsProcessingTaskId = analyticsProcessingTaskId;
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected AnalyticsProcessingTaskEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Order' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoOrder() { return CreateRelationInfoForNavigator("Order"); }
		
		/// <inheritdoc/>
		protected override EntityStaticMetaDataBase GetEntityStaticMetaData() {	return _staticMetaData; }

		/// <summary>Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		/// <summary>Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this AnalyticsProcessingTaskEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary>The relations object holding all relations of this entity with other entity classes.</summary>
		public static AnalyticsProcessingTaskRelations Relations { get { return _relationsFactory; } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Order' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathOrderEntity { get { return _staticMetaData.GetPrefetchPathElement("Order", CommonEntityBase.CreateEntityCollection<OrderEntity>()); } }

		/// <summary>The AnalyticsProcessingTaskId property of the Entity AnalyticsProcessingTask<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AnalyticsProcessingTask"."AnalyticsProcessingTaskId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 AnalyticsProcessingTaskId
		{
			get { return (System.Int32)GetValue((int)AnalyticsProcessingTaskFieldIndex.AnalyticsProcessingTaskId, true); }
			set { SetValue((int)AnalyticsProcessingTaskFieldIndex.AnalyticsProcessingTaskId, value); }		}

		/// <summary>The OrderId property of the Entity AnalyticsProcessingTask<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AnalyticsProcessingTask"."OrderId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 OrderId
		{
			get { return (System.Int32)GetValue((int)AnalyticsProcessingTaskFieldIndex.OrderId, true); }
			set	{ SetValue((int)AnalyticsProcessingTaskFieldIndex.OrderId, value); }
		}

		/// <summary>The EventAction property of the Entity AnalyticsProcessingTask<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AnalyticsProcessingTask"."EventAction".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String EventAction
		{
			get { return (System.String)GetValue((int)AnalyticsProcessingTaskFieldIndex.EventAction, true); }
			set	{ SetValue((int)AnalyticsProcessingTaskFieldIndex.EventAction, value); }
		}

		/// <summary>The EventLabel property of the Entity AnalyticsProcessingTask<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AnalyticsProcessingTask"."EventLabel".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String EventLabel
		{
			get { return (System.String)GetValue((int)AnalyticsProcessingTaskFieldIndex.EventLabel, true); }
			set	{ SetValue((int)AnalyticsProcessingTaskFieldIndex.EventLabel, value); }
		}

		/// <summary>The CreatedUTC property of the Entity AnalyticsProcessingTask<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AnalyticsProcessingTask"."CreatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)AnalyticsProcessingTaskFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)AnalyticsProcessingTaskFieldIndex.CreatedUTC, value); }
		}

		/// <summary>The UpdatedUTC property of the Entity AnalyticsProcessingTask<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AnalyticsProcessingTask"."UpdatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)AnalyticsProcessingTaskFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)AnalyticsProcessingTaskFieldIndex.UpdatedUTC, value); }
		}

		/// <summary>Gets / sets related entity of type 'OrderEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual OrderEntity Order
		{
			get { return _order; }
			set { SetSingleRelatedEntityNavigator(value, "Order"); }
		}


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen
{
	public enum AnalyticsProcessingTaskFieldIndex
	{
		///<summary>AnalyticsProcessingTaskId. </summary>
		AnalyticsProcessingTaskId,
		///<summary>OrderId. </summary>
		OrderId,
		///<summary>EventAction. </summary>
		EventAction,
		///<summary>EventLabel. </summary>
		EventLabel,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: AnalyticsProcessingTask. </summary>
	public partial class AnalyticsProcessingTaskRelations: RelationFactory
	{
		#region Custom Relation code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomRelationCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		


		/// <summary>Gets all relations and delete rules of the AnalyticsProcessingTaskEntity as a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects.</summary>
		/// <returns>a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects</returns>
		public virtual Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> GetRelationsAndDeleteRules()
		{
			Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> toReturn = new Dictionary<IEntityRelation, ReferentialConstraintDeleteRule>();
			return toReturn;
		}		
	
	

		/// <summary>Returns a new IEntityRelation object, between AnalyticsProcessingTaskEntity and OrderEntity over the m:1 relation they have, using the relation between the fields: AnalyticsProcessingTask.OrderId - Order.OrderId</summary>
		public virtual IEntityRelation OrderEntityUsingOrderId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Order", false, new[] { OrderFields.OrderId, AnalyticsProcessingTaskFields.OrderId }); }
		}

	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticAnalyticsProcessingTaskRelations
	{
		internal static readonly IEntityRelation OrderEntityUsingOrderIdStatic = new AnalyticsProcessingTaskRelations().OrderEntityUsingOrderId;

		/// <summary>CTor</summary>
		static StaticAnalyticsProcessingTaskRelations() { }
	}
}

