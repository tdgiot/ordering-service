﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 5.8
// Code is generated on: 
// Code is generated using templates: Crave.TemplateBindings
// Templates vendor: Crave Interactive
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.FactoryClasses;
using Crave.Ordering.Shared.Data.LLBLGen.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

// ReSharper disable All
namespace Crave.Ordering.Shared.Data.LLBLGen.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'ServiceMethod'.<br/><br/></summary>
	[Serializable]
	public partial class ServiceMethodEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END
	
	{
		private EntityCollection<DeliveryDistanceEntity> _deliveryDistanceCollection;
		private EntityCollection<OrderEntity> _orderCollection;
		private EntityCollection<ServiceMethodDeliverypointgroupEntity> _serviceMethodDeliverypointgroupCollection;
		private CompanyEntity _company;
		private OutletEntity _outlet;
		private ProductEntity _product;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		private static ServiceMethodEntityStaticMetaData _staticMetaData = new ServiceMethodEntityStaticMetaData();
		private static ServiceMethodRelations _relationsFactory = new ServiceMethodRelations();		

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Company</summary>
			public static readonly string Company = "Company";
			/// <summary>Member name Outlet</summary>
			public static readonly string Outlet = "Outlet";
			/// <summary>Member name Product</summary>
			public static readonly string Product = "Product";
			/// <summary>Member name DeliveryDistanceCollection</summary>
			public static readonly string DeliveryDistanceCollection = "DeliveryDistanceCollection";
			/// <summary>Member name OrderCollection</summary>
			public static readonly string OrderCollection = "OrderCollection";
			/// <summary>Member name ServiceMethodDeliverypointgroupCollection</summary>
			public static readonly string ServiceMethodDeliverypointgroupCollection = "ServiceMethodDeliverypointgroupCollection";
		}

		/// <summary>Static meta-data storage for navigator related information</summary>
		protected class ServiceMethodEntityStaticMetaData : EntityStaticMetaDataBase
		{
			public ServiceMethodEntityStaticMetaData()
			{
				SetEntityCoreInfo("ServiceMethodEntity", InheritanceHierarchyType.None, false, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ServiceMethodEntity, typeof(ServiceMethodEntity), typeof(ServiceMethodEntityFactory), false);
				AddNavigatorMetaData<ServiceMethodEntity, EntityCollection<DeliveryDistanceEntity>>("DeliveryDistanceCollection", a => a._deliveryDistanceCollection, (a, b) => a._deliveryDistanceCollection = b, a => a.DeliveryDistanceCollection, () => new ServiceMethodRelations().DeliveryDistanceEntityUsingServiceMethodId, typeof(DeliveryDistanceEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.DeliveryDistanceEntity);
				AddNavigatorMetaData<ServiceMethodEntity, EntityCollection<OrderEntity>>("OrderCollection", a => a._orderCollection, (a, b) => a._orderCollection = b, a => a.OrderCollection, () => new ServiceMethodRelations().OrderEntityUsingServiceMethodId, typeof(OrderEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.OrderEntity);
				AddNavigatorMetaData<ServiceMethodEntity, EntityCollection<ServiceMethodDeliverypointgroupEntity>>("ServiceMethodDeliverypointgroupCollection", a => a._serviceMethodDeliverypointgroupCollection, (a, b) => a._serviceMethodDeliverypointgroupCollection = b, a => a.ServiceMethodDeliverypointgroupCollection, () => new ServiceMethodRelations().ServiceMethodDeliverypointgroupEntityUsingServiceMethodId, typeof(ServiceMethodDeliverypointgroupEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ServiceMethodDeliverypointgroupEntity);
				AddNavigatorMetaData<ServiceMethodEntity, CompanyEntity>("Company", "ServiceMethodCollection", (a, b) => a._company = b, a => a._company, (a, b) => a.Company = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticServiceMethodRelations.CompanyEntityUsingCompanyIdStatic, ()=>new ServiceMethodRelations().CompanyEntityUsingCompanyId, null, new int[] { (int)ServiceMethodFieldIndex.CompanyId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.CompanyEntity);
				AddNavigatorMetaData<ServiceMethodEntity, OutletEntity>("Outlet", "ServiceMethodCollection", (a, b) => a._outlet = b, a => a._outlet, (a, b) => a.Outlet = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticServiceMethodRelations.OutletEntityUsingOutletIdStatic, ()=>new ServiceMethodRelations().OutletEntityUsingOutletId, null, new int[] { (int)ServiceMethodFieldIndex.OutletId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.OutletEntity);
				AddNavigatorMetaData<ServiceMethodEntity, ProductEntity>("Product", "ServiceMethodCollection", (a, b) => a._product = b, a => a._product, (a, b) => a.Product = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticServiceMethodRelations.ProductEntityUsingServiceChargeProductIdStatic, ()=>new ServiceMethodRelations().ProductEntityUsingServiceChargeProductId, null, new int[] { (int)ServiceMethodFieldIndex.ServiceChargeProductId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ProductEntity);
			}
		}

		/// <summary>Static ctor</summary>
		static ServiceMethodEntity()
		{
		}

		/// <summary> CTor</summary>
		public ServiceMethodEntity()
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public ServiceMethodEntity(IEntityFields2 fields)
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this ServiceMethodEntity</param>
		public ServiceMethodEntity(IValidator validator)
		{
			InitClassEmpty(validator, null);
		}

		/// <summary> CTor</summary>
		/// <param name="serviceMethodId">PK value for ServiceMethod which data should be fetched into this ServiceMethod object</param>
		public ServiceMethodEntity(System.Int32 serviceMethodId) : this(serviceMethodId, null)
		{
		}

		/// <summary> CTor</summary>
		/// <param name="serviceMethodId">PK value for ServiceMethod which data should be fetched into this ServiceMethod object</param>
		/// <param name="validator">The custom validator object for this ServiceMethodEntity</param>
		public ServiceMethodEntity(System.Int32 serviceMethodId, IValidator validator)
		{
			InitClassEmpty(validator, null);
			this.ServiceMethodId = serviceMethodId;
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ServiceMethodEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'DeliveryDistance' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoDeliveryDistanceCollection() { return CreateRelationInfoForNavigator("DeliveryDistanceCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Order' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoOrderCollection() { return CreateRelationInfoForNavigator("OrderCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'ServiceMethodDeliverypointgroup' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoServiceMethodDeliverypointgroupCollection() { return CreateRelationInfoForNavigator("ServiceMethodDeliverypointgroupCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Company' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoCompany() { return CreateRelationInfoForNavigator("Company"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Outlet' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoOutlet() { return CreateRelationInfoForNavigator("Outlet"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Product' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoProduct() { return CreateRelationInfoForNavigator("Product"); }
		
		/// <inheritdoc/>
		protected override EntityStaticMetaDataBase GetEntityStaticMetaData() {	return _staticMetaData; }

		/// <summary>Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		/// <summary>Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this ServiceMethodEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary>The relations object holding all relations of this entity with other entity classes.</summary>
		public static ServiceMethodRelations Relations { get { return _relationsFactory; } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'DeliveryDistance' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathDeliveryDistanceCollection { get { return _staticMetaData.GetPrefetchPathElement("DeliveryDistanceCollection", CommonEntityBase.CreateEntityCollection<DeliveryDistanceEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Order' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathOrderCollection { get { return _staticMetaData.GetPrefetchPathElement("OrderCollection", CommonEntityBase.CreateEntityCollection<OrderEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'ServiceMethodDeliverypointgroup' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathServiceMethodDeliverypointgroupCollection { get { return _staticMetaData.GetPrefetchPathElement("ServiceMethodDeliverypointgroupCollection", CommonEntityBase.CreateEntityCollection<ServiceMethodDeliverypointgroupEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Company' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathCompanyEntity { get { return _staticMetaData.GetPrefetchPathElement("Company", CommonEntityBase.CreateEntityCollection<CompanyEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Outlet' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathOutletEntity { get { return _staticMetaData.GetPrefetchPathElement("Outlet", CommonEntityBase.CreateEntityCollection<OutletEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Product' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathProductEntity { get { return _staticMetaData.GetPrefetchPathElement("Product", CommonEntityBase.CreateEntityCollection<ProductEntity>()); } }

		/// <summary>The ServiceMethodId property of the Entity ServiceMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ServiceMethod"."ServiceMethodId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 ServiceMethodId
		{
			get { return (System.Int32)GetValue((int)ServiceMethodFieldIndex.ServiceMethodId, true); }
			set { SetValue((int)ServiceMethodFieldIndex.ServiceMethodId, value); }		}

		/// <summary>The CompanyId property of the Entity ServiceMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ServiceMethod"."CompanyId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CompanyId
		{
			get { return (System.Int32)GetValue((int)ServiceMethodFieldIndex.CompanyId, true); }
			set	{ SetValue((int)ServiceMethodFieldIndex.CompanyId, value); }
		}

		/// <summary>The OutletId property of the Entity ServiceMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ServiceMethod"."OutletId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 OutletId
		{
			get { return (System.Int32)GetValue((int)ServiceMethodFieldIndex.OutletId, true); }
			set	{ SetValue((int)ServiceMethodFieldIndex.OutletId, value); }
		}

		/// <summary>The Active property of the Entity ServiceMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ServiceMethod"."Active".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean Active
		{
			get { return (System.Boolean)GetValue((int)ServiceMethodFieldIndex.Active, true); }
			set	{ SetValue((int)ServiceMethodFieldIndex.Active, value); }
		}

		/// <summary>The Type property of the Entity ServiceMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ServiceMethod"."Type".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Crave.Enums.ServiceMethodType Type
		{
			get { return (Crave.Enums.ServiceMethodType)GetValue((int)ServiceMethodFieldIndex.Type, true); }
			set	{ SetValue((int)ServiceMethodFieldIndex.Type, value); }
		}

		/// <summary>The Name property of the Entity ServiceMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ServiceMethod"."Name".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 256.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)ServiceMethodFieldIndex.Name, true); }
			set	{ SetValue((int)ServiceMethodFieldIndex.Name, value); }
		}

		/// <summary>The Label property of the Entity ServiceMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ServiceMethod"."Label".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 256.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Label
		{
			get { return (System.String)GetValue((int)ServiceMethodFieldIndex.Label, true); }
			set	{ SetValue((int)ServiceMethodFieldIndex.Label, value); }
		}

		/// <summary>The Description property of the Entity ServiceMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ServiceMethod"."Description".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 256.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)ServiceMethodFieldIndex.Description, true); }
			set	{ SetValue((int)ServiceMethodFieldIndex.Description, value); }
		}

		/// <summary>The AskEntryPermission property of the Entity ServiceMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ServiceMethod"."AskEntryPermission".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> AskEntryPermission
		{
			get { return (Nullable<System.Boolean>)GetValue((int)ServiceMethodFieldIndex.AskEntryPermission, false); }
			set	{ SetValue((int)ServiceMethodFieldIndex.AskEntryPermission, value); }
		}

		/// <summary>The ServiceChargePercentage property of the Entity ServiceMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ServiceMethod"."ServiceChargePercentage".<br/>Table field type characteristics (type, precision, scale, length): Float, 38, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Double> ServiceChargePercentage
		{
			get { return (Nullable<System.Double>)GetValue((int)ServiceMethodFieldIndex.ServiceChargePercentage, false); }
			set	{ SetValue((int)ServiceMethodFieldIndex.ServiceChargePercentage, value); }
		}

		/// <summary>The ServiceChargeAmount property of the Entity ServiceMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ServiceMethod"."ServiceChargeAmount".<br/>Table field type characteristics (type, precision, scale, length): Money, 19, 4, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Decimal> ServiceChargeAmount
		{
			get { return (Nullable<System.Decimal>)GetValue((int)ServiceMethodFieldIndex.ServiceChargeAmount, false); }
			set	{ SetValue((int)ServiceMethodFieldIndex.ServiceChargeAmount, value); }
		}

		/// <summary>The ServiceChargeProductId property of the Entity ServiceMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ServiceMethod"."ServiceChargeProductId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ServiceChargeProductId
		{
			get { return (Nullable<System.Int32>)GetValue((int)ServiceMethodFieldIndex.ServiceChargeProductId, false); }
			set	{ SetValue((int)ServiceMethodFieldIndex.ServiceChargeProductId, value); }
		}

		/// <summary>The ConfirmationActive property of the Entity ServiceMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ServiceMethod"."ConfirmationActive".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> ConfirmationActive
		{
			get { return (Nullable<System.Boolean>)GetValue((int)ServiceMethodFieldIndex.ConfirmationActive, false); }
			set	{ SetValue((int)ServiceMethodFieldIndex.ConfirmationActive, value); }
		}

		/// <summary>The CreatedUTC property of the Entity ServiceMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ServiceMethod"."CreatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ServiceMethodFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)ServiceMethodFieldIndex.CreatedUTC, value); }
		}

		/// <summary>The CreatedBy property of the Entity ServiceMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ServiceMethod"."CreatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)ServiceMethodFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)ServiceMethodFieldIndex.CreatedBy, value); }
		}

		/// <summary>The UpdatedUTC property of the Entity ServiceMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ServiceMethod"."UpdatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ServiceMethodFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)ServiceMethodFieldIndex.UpdatedUTC, value); }
		}

		/// <summary>The UpdatedBy property of the Entity ServiceMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ServiceMethod"."UpdatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)ServiceMethodFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)ServiceMethodFieldIndex.UpdatedBy, value); }
		}

		/// <summary>The MinimumOrderAmount property of the Entity ServiceMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ServiceMethod"."MinimumOrderAmount".<br/>Table field type characteristics (type, precision, scale, length): Money, 19, 4, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal MinimumOrderAmount
		{
			get { return (System.Decimal)GetValue((int)ServiceMethodFieldIndex.MinimumOrderAmount, true); }
			set	{ SetValue((int)ServiceMethodFieldIndex.MinimumOrderAmount, value); }
		}

		/// <summary>The FreeDeliveryAmount property of the Entity ServiceMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ServiceMethod"."FreeDeliveryAmount".<br/>Table field type characteristics (type, precision, scale, length): Money, 19, 4, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal FreeDeliveryAmount
		{
			get { return (System.Decimal)GetValue((int)ServiceMethodFieldIndex.FreeDeliveryAmount, true); }
			set	{ SetValue((int)ServiceMethodFieldIndex.FreeDeliveryAmount, value); }
		}

		/// <summary>The ServiceChargeType property of the Entity ServiceMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ServiceMethod"."ServiceChargeType".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ServiceChargeType
		{
			get { return (System.Int32)GetValue((int)ServiceMethodFieldIndex.ServiceChargeType, true); }
			set	{ SetValue((int)ServiceMethodFieldIndex.ServiceChargeType, value); }
		}

		/// <summary>The MinimumAmountForFreeDelivery property of the Entity ServiceMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ServiceMethod"."MinimumAmountForFreeDelivery".<br/>Table field type characteristics (type, precision, scale, length): Money, 19, 4, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal MinimumAmountForFreeDelivery
		{
			get { return (System.Decimal)GetValue((int)ServiceMethodFieldIndex.MinimumAmountForFreeDelivery, true); }
			set	{ SetValue((int)ServiceMethodFieldIndex.MinimumAmountForFreeDelivery, value); }
		}

		/// <summary>The MinimumAmountForFreeServiceCharge property of the Entity ServiceMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ServiceMethod"."MinimumAmountForFreeServiceCharge".<br/>Table field type characteristics (type, precision, scale, length): Money, 19, 4, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal MinimumAmountForFreeServiceCharge
		{
			get { return (System.Decimal)GetValue((int)ServiceMethodFieldIndex.MinimumAmountForFreeServiceCharge, true); }
			set	{ SetValue((int)ServiceMethodFieldIndex.MinimumAmountForFreeServiceCharge, value); }
		}

		/// <summary>The OrderCompleteNotificationMethod property of the Entity ServiceMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ServiceMethod"."OrderCompleteNotificationMethod".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Crave.Enums.OrderNotificationMethod OrderCompleteNotificationMethod
		{
			get { return (Crave.Enums.OrderNotificationMethod)GetValue((int)ServiceMethodFieldIndex.OrderCompleteNotificationMethod, true); }
			set	{ SetValue((int)ServiceMethodFieldIndex.OrderCompleteNotificationMethod, value); }
		}

		/// <summary>The OrderCompleteTextMessage property of the Entity ServiceMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ServiceMethod"."OrderCompleteTextMessage".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 320.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String OrderCompleteTextMessage
		{
			get { return (System.String)GetValue((int)ServiceMethodFieldIndex.OrderCompleteTextMessage, true); }
			set	{ SetValue((int)ServiceMethodFieldIndex.OrderCompleteTextMessage, value); }
		}

		/// <summary>The OrderCompleteMailSubject property of the Entity ServiceMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ServiceMethod"."OrderCompleteMailSubject".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 250.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String OrderCompleteMailSubject
		{
			get { return (System.String)GetValue((int)ServiceMethodFieldIndex.OrderCompleteMailSubject, true); }
			set	{ SetValue((int)ServiceMethodFieldIndex.OrderCompleteMailSubject, value); }
		}

		/// <summary>The OrderCompleteMailMessage property of the Entity ServiceMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ServiceMethod"."OrderCompleteMailMessage".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2048.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String OrderCompleteMailMessage
		{
			get { return (System.String)GetValue((int)ServiceMethodFieldIndex.OrderCompleteMailMessage, true); }
			set	{ SetValue((int)ServiceMethodFieldIndex.OrderCompleteMailMessage, value); }
		}

		/// <summary>The MaxCovers property of the Entity ServiceMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ServiceMethod"."MaxCovers".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 MaxCovers
		{
			get { return (System.Int32)GetValue((int)ServiceMethodFieldIndex.MaxCovers, true); }
			set	{ SetValue((int)ServiceMethodFieldIndex.MaxCovers, value); }
		}

		/// <summary>The CoversType property of the Entity ServiceMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ServiceMethod"."CoversType".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Crave.Enums.CoversType CoversType
		{
			get { return (Crave.Enums.CoversType)GetValue((int)ServiceMethodFieldIndex.CoversType, true); }
			set	{ SetValue((int)ServiceMethodFieldIndex.CoversType, value); }
		}

		/// <summary>Gets the EntityCollection with the related entities of type 'DeliveryDistanceEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(DeliveryDistanceEntity))]
		public virtual EntityCollection<DeliveryDistanceEntity> DeliveryDistanceCollection { get { return GetOrCreateEntityCollection<DeliveryDistanceEntity, DeliveryDistanceEntityFactory>("ServiceMethod", true, false, ref _deliveryDistanceCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'OrderEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(OrderEntity))]
		public virtual EntityCollection<OrderEntity> OrderCollection { get { return GetOrCreateEntityCollection<OrderEntity, OrderEntityFactory>("ServiceMethod", true, false, ref _orderCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'ServiceMethodDeliverypointgroupEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(ServiceMethodDeliverypointgroupEntity))]
		public virtual EntityCollection<ServiceMethodDeliverypointgroupEntity> ServiceMethodDeliverypointgroupCollection { get { return GetOrCreateEntityCollection<ServiceMethodDeliverypointgroupEntity, ServiceMethodDeliverypointgroupEntityFactory>("ServiceMethod", true, false, ref _serviceMethodDeliverypointgroupCollection); } }

		/// <summary>Gets / sets related entity of type 'CompanyEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual CompanyEntity Company
		{
			get { return _company; }
			set { SetSingleRelatedEntityNavigator(value, "Company"); }
		}

		/// <summary>Gets / sets related entity of type 'OutletEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual OutletEntity Outlet
		{
			get { return _outlet; }
			set { SetSingleRelatedEntityNavigator(value, "Outlet"); }
		}

		/// <summary>Gets / sets related entity of type 'ProductEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual ProductEntity Product
		{
			get { return _product; }
			set { SetSingleRelatedEntityNavigator(value, "Product"); }
		}


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen
{
	public enum ServiceMethodFieldIndex
	{
		///<summary>ServiceMethodId. </summary>
		ServiceMethodId,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>OutletId. </summary>
		OutletId,
		///<summary>Active. </summary>
		Active,
		///<summary>Type. </summary>
		Type,
		///<summary>Name. </summary>
		Name,
		///<summary>Label. </summary>
		Label,
		///<summary>Description. </summary>
		Description,
		///<summary>AskEntryPermission. </summary>
		AskEntryPermission,
		///<summary>ServiceChargePercentage. </summary>
		ServiceChargePercentage,
		///<summary>ServiceChargeAmount. </summary>
		ServiceChargeAmount,
		///<summary>ServiceChargeProductId. </summary>
		ServiceChargeProductId,
		///<summary>ConfirmationActive. </summary>
		ConfirmationActive,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>MinimumOrderAmount. </summary>
		MinimumOrderAmount,
		///<summary>FreeDeliveryAmount. </summary>
		FreeDeliveryAmount,
		///<summary>ServiceChargeType. </summary>
		ServiceChargeType,
		///<summary>MinimumAmountForFreeDelivery. </summary>
		MinimumAmountForFreeDelivery,
		///<summary>MinimumAmountForFreeServiceCharge. </summary>
		MinimumAmountForFreeServiceCharge,
		///<summary>OrderCompleteNotificationMethod. </summary>
		OrderCompleteNotificationMethod,
		///<summary>OrderCompleteTextMessage. </summary>
		OrderCompleteTextMessage,
		///<summary>OrderCompleteMailSubject. </summary>
		OrderCompleteMailSubject,
		///<summary>OrderCompleteMailMessage. </summary>
		OrderCompleteMailMessage,
		///<summary>MaxCovers. </summary>
		MaxCovers,
		///<summary>CoversType. </summary>
		CoversType,
		/// <summary></summary>
		AmountOfFields
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: ServiceMethod. </summary>
	public partial class ServiceMethodRelations: RelationFactory
	{
		#region Custom Relation code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomRelationCode
        private const ReferentialConstraintDeleteRule DeleteRuleForDeliveryDistanceEntityUsingServiceMethodId = ReferentialConstraintDeleteRule.NoAction;
        private const ReferentialConstraintDeleteRule DeleteRuleForOrderEntityUsingServiceMethodId = ReferentialConstraintDeleteRule.NoAction;
        private const ReferentialConstraintDeleteRule DeleteRuleForServiceMethodDeliverypointgroupEntityUsingServiceMethodId = ReferentialConstraintDeleteRule.Cascade;
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		
		// private const ReferentialConstraintDeleteRule DeleteRuleForDeliveryDistanceEntityUsingServiceMethodId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForOrderEntityUsingServiceMethodId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForServiceMethodDeliverypointgroupEntityUsingServiceMethodId = ReferentialConstraintDeleteRule


		/// <summary>Gets all relations and delete rules of the ServiceMethodEntity as a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects.</summary>
		/// <returns>a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects</returns>
		public virtual Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> GetRelationsAndDeleteRules()
		{
			Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> toReturn = new Dictionary<IEntityRelation, ReferentialConstraintDeleteRule>();
			toReturn.Add(this.DeliveryDistanceEntityUsingServiceMethodId, ServiceMethodRelations.DeleteRuleForDeliveryDistanceEntityUsingServiceMethodId);
			toReturn.Add(this.OrderEntityUsingServiceMethodId, ServiceMethodRelations.DeleteRuleForOrderEntityUsingServiceMethodId);
			toReturn.Add(this.ServiceMethodDeliverypointgroupEntityUsingServiceMethodId, ServiceMethodRelations.DeleteRuleForServiceMethodDeliverypointgroupEntityUsingServiceMethodId);
			return toReturn;
		}		
	
	
		/// <summary>Returns a new IEntityRelation object, between ServiceMethodEntity and DeliveryDistanceEntity over the 1:n relation they have, using the relation between the fields: ServiceMethod.ServiceMethodId - DeliveryDistance.ServiceMethodId</summary>
		public virtual IEntityRelation DeliveryDistanceEntityUsingServiceMethodId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "DeliveryDistanceCollection", true, new[] { ServiceMethodFields.ServiceMethodId, DeliveryDistanceFields.ServiceMethodId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between ServiceMethodEntity and OrderEntity over the 1:n relation they have, using the relation between the fields: ServiceMethod.ServiceMethodId - Order.ServiceMethodId</summary>
		public virtual IEntityRelation OrderEntityUsingServiceMethodId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "OrderCollection", true, new[] { ServiceMethodFields.ServiceMethodId, OrderFields.ServiceMethodId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between ServiceMethodEntity and ServiceMethodDeliverypointgroupEntity over the 1:n relation they have, using the relation between the fields: ServiceMethod.ServiceMethodId - ServiceMethodDeliverypointgroup.ServiceMethodId</summary>
		public virtual IEntityRelation ServiceMethodDeliverypointgroupEntityUsingServiceMethodId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "ServiceMethodDeliverypointgroupCollection", true, new[] { ServiceMethodFields.ServiceMethodId, ServiceMethodDeliverypointgroupFields.ServiceMethodId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between ServiceMethodEntity and CompanyEntity over the m:1 relation they have, using the relation between the fields: ServiceMethod.CompanyId - Company.CompanyId</summary>
		public virtual IEntityRelation CompanyEntityUsingCompanyId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Company", false, new[] { CompanyFields.CompanyId, ServiceMethodFields.CompanyId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between ServiceMethodEntity and OutletEntity over the m:1 relation they have, using the relation between the fields: ServiceMethod.OutletId - Outlet.OutletId</summary>
		public virtual IEntityRelation OutletEntityUsingOutletId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Outlet", false, new[] { OutletFields.OutletId, ServiceMethodFields.OutletId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between ServiceMethodEntity and ProductEntity over the m:1 relation they have, using the relation between the fields: ServiceMethod.ServiceChargeProductId - Product.ProductId</summary>
		public virtual IEntityRelation ProductEntityUsingServiceChargeProductId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Product", false, new[] { ProductFields.ProductId, ServiceMethodFields.ServiceChargeProductId }); }
		}

	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticServiceMethodRelations
	{
		internal static readonly IEntityRelation DeliveryDistanceEntityUsingServiceMethodIdStatic = new ServiceMethodRelations().DeliveryDistanceEntityUsingServiceMethodId;
		internal static readonly IEntityRelation OrderEntityUsingServiceMethodIdStatic = new ServiceMethodRelations().OrderEntityUsingServiceMethodId;
		internal static readonly IEntityRelation ServiceMethodDeliverypointgroupEntityUsingServiceMethodIdStatic = new ServiceMethodRelations().ServiceMethodDeliverypointgroupEntityUsingServiceMethodId;
		internal static readonly IEntityRelation CompanyEntityUsingCompanyIdStatic = new ServiceMethodRelations().CompanyEntityUsingCompanyId;
		internal static readonly IEntityRelation OutletEntityUsingOutletIdStatic = new ServiceMethodRelations().OutletEntityUsingOutletId;
		internal static readonly IEntityRelation ProductEntityUsingServiceChargeProductIdStatic = new ServiceMethodRelations().ProductEntityUsingServiceChargeProductId;

		/// <summary>CTor</summary>
		static StaticServiceMethodRelations() { }
	}
}

