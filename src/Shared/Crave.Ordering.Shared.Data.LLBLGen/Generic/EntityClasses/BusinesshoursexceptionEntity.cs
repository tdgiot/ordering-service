﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 5.8
// Code is generated on: 
// Code is generated using templates: Crave.TemplateBindings
// Templates vendor: Crave Interactive
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.FactoryClasses;
using Crave.Ordering.Shared.Data.LLBLGen.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

// ReSharper disable All
namespace Crave.Ordering.Shared.Data.LLBLGen.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'Businesshoursexception'.<br/><br/></summary>
	[Serializable]
	public partial class BusinesshoursexceptionEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END
	
	{
		private CompanyEntity _company;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		private static BusinesshoursexceptionEntityStaticMetaData _staticMetaData = new BusinesshoursexceptionEntityStaticMetaData();
		private static BusinesshoursexceptionRelations _relationsFactory = new BusinesshoursexceptionRelations();		

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Company</summary>
			public static readonly string Company = "Company";
		}

		/// <summary>Static meta-data storage for navigator related information</summary>
		protected class BusinesshoursexceptionEntityStaticMetaData : EntityStaticMetaDataBase
		{
			public BusinesshoursexceptionEntityStaticMetaData()
			{
				SetEntityCoreInfo("BusinesshoursexceptionEntity", InheritanceHierarchyType.None, false, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.BusinesshoursexceptionEntity, typeof(BusinesshoursexceptionEntity), typeof(BusinesshoursexceptionEntityFactory), false);
				AddNavigatorMetaData<BusinesshoursexceptionEntity, CompanyEntity>("Company", "BusinesshoursexceptionCollection", (a, b) => a._company = b, a => a._company, (a, b) => a.Company = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticBusinesshoursexceptionRelations.CompanyEntityUsingCompanyIdStatic, ()=>new BusinesshoursexceptionRelations().CompanyEntityUsingCompanyId, null, new int[] { (int)BusinesshoursexceptionFieldIndex.CompanyId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.CompanyEntity);
			}
		}

		/// <summary>Static ctor</summary>
		static BusinesshoursexceptionEntity()
		{
		}

		/// <summary> CTor</summary>
		public BusinesshoursexceptionEntity()
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public BusinesshoursexceptionEntity(IEntityFields2 fields)
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this BusinesshoursexceptionEntity</param>
		public BusinesshoursexceptionEntity(IValidator validator)
		{
			InitClassEmpty(validator, null);
		}

		/// <summary> CTor</summary>
		/// <param name="businesshoursexceptionId">PK value for Businesshoursexception which data should be fetched into this Businesshoursexception object</param>
		public BusinesshoursexceptionEntity(System.Int32 businesshoursexceptionId) : this(businesshoursexceptionId, null)
		{
		}

		/// <summary> CTor</summary>
		/// <param name="businesshoursexceptionId">PK value for Businesshoursexception which data should be fetched into this Businesshoursexception object</param>
		/// <param name="validator">The custom validator object for this BusinesshoursexceptionEntity</param>
		public BusinesshoursexceptionEntity(System.Int32 businesshoursexceptionId, IValidator validator)
		{
			InitClassEmpty(validator, null);
			this.BusinesshoursexceptionId = businesshoursexceptionId;
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected BusinesshoursexceptionEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Company' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoCompany() { return CreateRelationInfoForNavigator("Company"); }
		
		/// <inheritdoc/>
		protected override EntityStaticMetaDataBase GetEntityStaticMetaData() {	return _staticMetaData; }

		/// <summary>Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		/// <summary>Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this BusinesshoursexceptionEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary>The relations object holding all relations of this entity with other entity classes.</summary>
		public static BusinesshoursexceptionRelations Relations { get { return _relationsFactory; } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Company' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathCompanyEntity { get { return _staticMetaData.GetPrefetchPathElement("Company", CommonEntityBase.CreateEntityCollection<CompanyEntity>()); } }

		/// <summary>The BusinesshoursexceptionId property of the Entity Businesshoursexception<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Businesshoursexception"."BusinesshoursexceptionId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 BusinesshoursexceptionId
		{
			get { return (System.Int32)GetValue((int)BusinesshoursexceptionFieldIndex.BusinesshoursexceptionId, true); }
			set { SetValue((int)BusinesshoursexceptionFieldIndex.BusinesshoursexceptionId, value); }		}

		/// <summary>The CompanyId property of the Entity Businesshoursexception<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Businesshoursexception"."CompanyId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CompanyId
		{
			get { return (System.Int32)GetValue((int)BusinesshoursexceptionFieldIndex.CompanyId, true); }
			set	{ SetValue((int)BusinesshoursexceptionFieldIndex.CompanyId, value); }
		}

		/// <summary>The Opened property of the Entity Businesshoursexception<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Businesshoursexception"."Opened".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean Opened
		{
			get { return (System.Boolean)GetValue((int)BusinesshoursexceptionFieldIndex.Opened, true); }
			set	{ SetValue((int)BusinesshoursexceptionFieldIndex.Opened, value); }
		}

		/// <summary>The FromDateTime property of the Entity Businesshoursexception<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Businesshoursexception"."FromDateTime".<br/>Table field type characteristics (type, precision, scale, length): DateTime, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime FromDateTime
		{
			get { return (System.DateTime)GetValue((int)BusinesshoursexceptionFieldIndex.FromDateTime, true); }
			set	{ SetValue((int)BusinesshoursexceptionFieldIndex.FromDateTime, value); }
		}

		/// <summary>The TillDateTime property of the Entity Businesshoursexception<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Businesshoursexception"."TillDateTime".<br/>Table field type characteristics (type, precision, scale, length): DateTime, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime TillDateTime
		{
			get { return (System.DateTime)GetValue((int)BusinesshoursexceptionFieldIndex.TillDateTime, true); }
			set	{ SetValue((int)BusinesshoursexceptionFieldIndex.TillDateTime, value); }
		}

		/// <summary>The CreatedBy property of the Entity Businesshoursexception<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Businesshoursexception"."CreatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)BusinesshoursexceptionFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)BusinesshoursexceptionFieldIndex.CreatedBy, value); }
		}

		/// <summary>The UpdatedBy property of the Entity Businesshoursexception<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Businesshoursexception"."UpdatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)BusinesshoursexceptionFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)BusinesshoursexceptionFieldIndex.UpdatedBy, value); }
		}

		/// <summary>The CreatedUTC property of the Entity Businesshoursexception<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Businesshoursexception"."CreatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)BusinesshoursexceptionFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)BusinesshoursexceptionFieldIndex.CreatedUTC, value); }
		}

		/// <summary>The UpdatedUTC property of the Entity Businesshoursexception<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Businesshoursexception"."UpdatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)BusinesshoursexceptionFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)BusinesshoursexceptionFieldIndex.UpdatedUTC, value); }
		}

		/// <summary>Gets / sets related entity of type 'CompanyEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual CompanyEntity Company
		{
			get { return _company; }
			set { SetSingleRelatedEntityNavigator(value, "Company"); }
		}


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen
{
	public enum BusinesshoursexceptionFieldIndex
	{
		///<summary>BusinesshoursexceptionId. </summary>
		BusinesshoursexceptionId,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>Opened. </summary>
		Opened,
		///<summary>FromDateTime. </summary>
		FromDateTime,
		///<summary>TillDateTime. </summary>
		TillDateTime,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Businesshoursexception. </summary>
	public partial class BusinesshoursexceptionRelations: RelationFactory
	{
		#region Custom Relation code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomRelationCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		


		/// <summary>Gets all relations and delete rules of the BusinesshoursexceptionEntity as a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects.</summary>
		/// <returns>a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects</returns>
		public virtual Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> GetRelationsAndDeleteRules()
		{
			Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> toReturn = new Dictionary<IEntityRelation, ReferentialConstraintDeleteRule>();
			return toReturn;
		}		
	
	

		/// <summary>Returns a new IEntityRelation object, between BusinesshoursexceptionEntity and CompanyEntity over the m:1 relation they have, using the relation between the fields: Businesshoursexception.CompanyId - Company.CompanyId</summary>
		public virtual IEntityRelation CompanyEntityUsingCompanyId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Company", false, new[] { CompanyFields.CompanyId, BusinesshoursexceptionFields.CompanyId }); }
		}

	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticBusinesshoursexceptionRelations
	{
		internal static readonly IEntityRelation CompanyEntityUsingCompanyIdStatic = new BusinesshoursexceptionRelations().CompanyEntityUsingCompanyId;

		/// <summary>CTor</summary>
		static StaticBusinesshoursexceptionRelations() { }
	}
}

