﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 5.8
// Code is generated on: 
// Code is generated using templates: Crave.TemplateBindings
// Templates vendor: Crave Interactive
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.FactoryClasses;
using Crave.Ordering.Shared.Data.LLBLGen.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

// ReSharper disable All
namespace Crave.Ordering.Shared.Data.LLBLGen.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'Orderitem'.<br/><br/></summary>
	[Serializable]
	public partial class OrderitemEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END
	
	{
		private EntityCollection<OrderitemAlterationitemEntity> _orderitemAlterationitemCollection;
		private EntityCollection<OrderitemTagEntity> _orderitemTagCollection;
		private CategoryEntity _category;
		private OrderEntity _order;
		private PriceLevelItemEntity _priceLevelItem;
		private ProductEntity _product;
		private TaxTariffEntity _taxTariff;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		private static OrderitemEntityStaticMetaData _staticMetaData = new OrderitemEntityStaticMetaData();
		private static OrderitemRelations _relationsFactory = new OrderitemRelations();		

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Category</summary>
			public static readonly string Category = "Category";
			/// <summary>Member name Order</summary>
			public static readonly string Order = "Order";
			/// <summary>Member name PriceLevelItem</summary>
			public static readonly string PriceLevelItem = "PriceLevelItem";
			/// <summary>Member name Product</summary>
			public static readonly string Product = "Product";
			/// <summary>Member name TaxTariff</summary>
			public static readonly string TaxTariff = "TaxTariff";
			/// <summary>Member name OrderitemAlterationitemCollection</summary>
			public static readonly string OrderitemAlterationitemCollection = "OrderitemAlterationitemCollection";
			/// <summary>Member name OrderitemTagCollection</summary>
			public static readonly string OrderitemTagCollection = "OrderitemTagCollection";
		}

		/// <summary>Static meta-data storage for navigator related information</summary>
		protected class OrderitemEntityStaticMetaData : EntityStaticMetaDataBase
		{
			public OrderitemEntityStaticMetaData()
			{
				SetEntityCoreInfo("OrderitemEntity", InheritanceHierarchyType.None, false, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.OrderitemEntity, typeof(OrderitemEntity), typeof(OrderitemEntityFactory), false);
				AddNavigatorMetaData<OrderitemEntity, EntityCollection<OrderitemAlterationitemEntity>>("OrderitemAlterationitemCollection", a => a._orderitemAlterationitemCollection, (a, b) => a._orderitemAlterationitemCollection = b, a => a.OrderitemAlterationitemCollection, () => new OrderitemRelations().OrderitemAlterationitemEntityUsingOrderitemId, typeof(OrderitemAlterationitemEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.OrderitemAlterationitemEntity);
				AddNavigatorMetaData<OrderitemEntity, EntityCollection<OrderitemTagEntity>>("OrderitemTagCollection", a => a._orderitemTagCollection, (a, b) => a._orderitemTagCollection = b, a => a.OrderitemTagCollection, () => new OrderitemRelations().OrderitemTagEntityUsingOrderitemId, typeof(OrderitemTagEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.OrderitemTagEntity);
				AddNavigatorMetaData<OrderitemEntity, CategoryEntity>("Category", "OrderitemCollection", (a, b) => a._category = b, a => a._category, (a, b) => a.Category = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticOrderitemRelations.CategoryEntityUsingCategoryIdStatic, ()=>new OrderitemRelations().CategoryEntityUsingCategoryId, null, new int[] { (int)OrderitemFieldIndex.CategoryId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.CategoryEntity);
				AddNavigatorMetaData<OrderitemEntity, OrderEntity>("Order", "OrderitemCollection", (a, b) => a._order = b, a => a._order, (a, b) => a.Order = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticOrderitemRelations.OrderEntityUsingOrderIdStatic, ()=>new OrderitemRelations().OrderEntityUsingOrderId, null, new int[] { (int)OrderitemFieldIndex.OrderId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.OrderEntity);
				AddNavigatorMetaData<OrderitemEntity, PriceLevelItemEntity>("PriceLevelItem", "OrderitemCollection", (a, b) => a._priceLevelItem = b, a => a._priceLevelItem, (a, b) => a.PriceLevelItem = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticOrderitemRelations.PriceLevelItemEntityUsingPriceLevelItemIdStatic, ()=>new OrderitemRelations().PriceLevelItemEntityUsingPriceLevelItemId, null, new int[] { (int)OrderitemFieldIndex.PriceLevelItemId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.PriceLevelItemEntity);
				AddNavigatorMetaData<OrderitemEntity, ProductEntity>("Product", "OrderitemCollection", (a, b) => a._product = b, a => a._product, (a, b) => a.Product = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticOrderitemRelations.ProductEntityUsingProductIdStatic, ()=>new OrderitemRelations().ProductEntityUsingProductId, null, new int[] { (int)OrderitemFieldIndex.ProductId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ProductEntity);
				AddNavigatorMetaData<OrderitemEntity, TaxTariffEntity>("TaxTariff", "OrderitemCollection", (a, b) => a._taxTariff = b, a => a._taxTariff, (a, b) => a.TaxTariff = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticOrderitemRelations.TaxTariffEntityUsingTaxTariffIdStatic, ()=>new OrderitemRelations().TaxTariffEntityUsingTaxTariffId, null, new int[] { (int)OrderitemFieldIndex.TaxTariffId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.TaxTariffEntity);
			}
		}

		/// <summary>Static ctor</summary>
		static OrderitemEntity()
		{
		}

		/// <summary> CTor</summary>
		public OrderitemEntity()
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public OrderitemEntity(IEntityFields2 fields)
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this OrderitemEntity</param>
		public OrderitemEntity(IValidator validator)
		{
			InitClassEmpty(validator, null);
		}

		/// <summary> CTor</summary>
		/// <param name="orderitemId">PK value for Orderitem which data should be fetched into this Orderitem object</param>
		public OrderitemEntity(System.Int32 orderitemId) : this(orderitemId, null)
		{
		}

		/// <summary> CTor</summary>
		/// <param name="orderitemId">PK value for Orderitem which data should be fetched into this Orderitem object</param>
		/// <param name="validator">The custom validator object for this OrderitemEntity</param>
		public OrderitemEntity(System.Int32 orderitemId, IValidator validator)
		{
			InitClassEmpty(validator, null);
			this.OrderitemId = orderitemId;
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected OrderitemEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'OrderitemAlterationitem' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoOrderitemAlterationitemCollection() { return CreateRelationInfoForNavigator("OrderitemAlterationitemCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'OrderitemTag' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoOrderitemTagCollection() { return CreateRelationInfoForNavigator("OrderitemTagCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Category' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoCategory() { return CreateRelationInfoForNavigator("Category"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Order' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoOrder() { return CreateRelationInfoForNavigator("Order"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'PriceLevelItem' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoPriceLevelItem() { return CreateRelationInfoForNavigator("PriceLevelItem"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Product' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoProduct() { return CreateRelationInfoForNavigator("Product"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'TaxTariff' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoTaxTariff() { return CreateRelationInfoForNavigator("TaxTariff"); }
		
		/// <inheritdoc/>
		protected override EntityStaticMetaDataBase GetEntityStaticMetaData() {	return _staticMetaData; }

		/// <summary>Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		/// <summary>Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this OrderitemEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary>The relations object holding all relations of this entity with other entity classes.</summary>
		public static OrderitemRelations Relations { get { return _relationsFactory; } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'OrderitemAlterationitem' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathOrderitemAlterationitemCollection { get { return _staticMetaData.GetPrefetchPathElement("OrderitemAlterationitemCollection", CommonEntityBase.CreateEntityCollection<OrderitemAlterationitemEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'OrderitemTag' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathOrderitemTagCollection { get { return _staticMetaData.GetPrefetchPathElement("OrderitemTagCollection", CommonEntityBase.CreateEntityCollection<OrderitemTagEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Category' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathCategoryEntity { get { return _staticMetaData.GetPrefetchPathElement("Category", CommonEntityBase.CreateEntityCollection<CategoryEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Order' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathOrderEntity { get { return _staticMetaData.GetPrefetchPathElement("Order", CommonEntityBase.CreateEntityCollection<OrderEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'PriceLevelItem' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathPriceLevelItemEntity { get { return _staticMetaData.GetPrefetchPathElement("PriceLevelItem", CommonEntityBase.CreateEntityCollection<PriceLevelItemEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Product' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathProductEntity { get { return _staticMetaData.GetPrefetchPathElement("Product", CommonEntityBase.CreateEntityCollection<ProductEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'TaxTariff' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathTaxTariffEntity { get { return _staticMetaData.GetPrefetchPathElement("TaxTariff", CommonEntityBase.CreateEntityCollection<TaxTariffEntity>()); } }

		/// <summary>The OrderitemId property of the Entity Orderitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Orderitem"."OrderitemId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 OrderitemId
		{
			get { return (System.Int32)GetValue((int)OrderitemFieldIndex.OrderitemId, true); }
			set { SetValue((int)OrderitemFieldIndex.OrderitemId, value); }		}

		/// <summary>The ParentCompanyId property of the Entity Orderitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Orderitem"."ParentCompanyId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ParentCompanyId
		{
			get { return (System.Int32)GetValue((int)OrderitemFieldIndex.ParentCompanyId, true); }
			set	{ SetValue((int)OrderitemFieldIndex.ParentCompanyId, value); }
		}

		/// <summary>The OrderId property of the Entity Orderitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Orderitem"."OrderId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 OrderId
		{
			get { return (System.Int32)GetValue((int)OrderitemFieldIndex.OrderId, true); }
			set	{ SetValue((int)OrderitemFieldIndex.OrderId, value); }
		}

		/// <summary>The CategoryId property of the Entity Orderitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Orderitem"."CategoryId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CategoryId
		{
			get { return (Nullable<System.Int32>)GetValue((int)OrderitemFieldIndex.CategoryId, false); }
			set	{ SetValue((int)OrderitemFieldIndex.CategoryId, value); }
		}

		/// <summary>The CategoryName property of the Entity Orderitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Orderitem"."CategoryName".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 500.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CategoryName
		{
			get { return (System.String)GetValue((int)OrderitemFieldIndex.CategoryName, true); }
			set	{ SetValue((int)OrderitemFieldIndex.CategoryName, value); }
		}

		/// <summary>The CategoryPath property of the Entity Orderitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Orderitem"."CategoryPath".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CategoryPath
		{
			get { return (System.String)GetValue((int)OrderitemFieldIndex.CategoryPath, true); }
			set	{ SetValue((int)OrderitemFieldIndex.CategoryPath, value); }
		}

		/// <summary>The ProductId property of the Entity Orderitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Orderitem"."ProductId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ProductId
		{
			get { return (Nullable<System.Int32>)GetValue((int)OrderitemFieldIndex.ProductId, false); }
			set	{ SetValue((int)OrderitemFieldIndex.ProductId, value); }
		}

		/// <summary>The ProductDescription property of the Entity Orderitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Orderitem"."ProductDescription".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ProductDescription
		{
			get { return (System.String)GetValue((int)OrderitemFieldIndex.ProductDescription, true); }
			set	{ SetValue((int)OrderitemFieldIndex.ProductDescription, value); }
		}

		/// <summary>The ProductName property of the Entity Orderitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Orderitem"."ProductName".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ProductName
		{
			get { return (System.String)GetValue((int)OrderitemFieldIndex.ProductName, true); }
			set	{ SetValue((int)OrderitemFieldIndex.ProductName, value); }
		}

		/// <summary>The ProductPriceIn property of the Entity Orderitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Orderitem"."ProductPriceIn".<br/>Table field type characteristics (type, precision, scale, length): Money, 19, 4, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal ProductPriceIn
		{
			get { return (System.Decimal)GetValue((int)OrderitemFieldIndex.ProductPriceIn, true); }
			set	{ SetValue((int)OrderitemFieldIndex.ProductPriceIn, value); }
		}

		/// <summary>The Quantity property of the Entity Orderitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Orderitem"."Quantity".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 Quantity
		{
			get { return (System.Int32)GetValue((int)OrderitemFieldIndex.Quantity, true); }
			set	{ SetValue((int)OrderitemFieldIndex.Quantity, value); }
		}

		/// <summary>The VatPercentage property of the Entity Orderitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Orderitem"."VatPercentage".<br/>Table field type characteristics (type, precision, scale, length): Float, 38, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Double VatPercentage
		{
			get { return (System.Double)GetValue((int)OrderitemFieldIndex.VatPercentage, true); }
			set	{ SetValue((int)OrderitemFieldIndex.VatPercentage, value); }
		}

		/// <summary>The Notes property of the Entity Orderitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Orderitem"."Notes".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Notes
		{
			get { return (System.String)GetValue((int)OrderitemFieldIndex.Notes, true); }
			set	{ SetValue((int)OrderitemFieldIndex.Notes, value); }
		}

		/// <summary>The CreatedBy property of the Entity Orderitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Orderitem"."CreatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)OrderitemFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)OrderitemFieldIndex.CreatedBy, value); }
		}

		/// <summary>The UpdatedBy property of the Entity Orderitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Orderitem"."UpdatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)OrderitemFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)OrderitemFieldIndex.UpdatedBy, value); }
		}

		/// <summary>The Guid property of the Entity Orderitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Orderitem"."Guid".<br/>Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 128.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Guid
		{
			get { return (System.String)GetValue((int)OrderitemFieldIndex.Guid, true); }
			set	{ SetValue((int)OrderitemFieldIndex.Guid, value); }
		}

		/// <summary>The Color property of the Entity Orderitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Orderitem"."Color".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 Color
		{
			get { return (System.Int32)GetValue((int)OrderitemFieldIndex.Color, true); }
			set	{ SetValue((int)OrderitemFieldIndex.Color, value); }
		}

		/// <summary>The OrderSource property of the Entity Orderitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Orderitem"."OrderSource".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 OrderSource
		{
			get { return (System.Int32)GetValue((int)OrderitemFieldIndex.OrderSource, true); }
			set	{ SetValue((int)OrderitemFieldIndex.OrderSource, value); }
		}

		/// <summary>The PriceLevelItemId property of the Entity Orderitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Orderitem"."PriceLevelItemId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> PriceLevelItemId
		{
			get { return (Nullable<System.Int32>)GetValue((int)OrderitemFieldIndex.PriceLevelItemId, false); }
			set	{ SetValue((int)OrderitemFieldIndex.PriceLevelItemId, value); }
		}

		/// <summary>The PriceLevelLog property of the Entity Orderitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Orderitem"."PriceLevelLog".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PriceLevelLog
		{
			get { return (System.String)GetValue((int)OrderitemFieldIndex.PriceLevelLog, true); }
			set	{ SetValue((int)OrderitemFieldIndex.PriceLevelLog, value); }
		}

		/// <summary>The CreatedUTC property of the Entity Orderitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Orderitem"."CreatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)OrderitemFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)OrderitemFieldIndex.CreatedUTC, value); }
		}

		/// <summary>The UpdatedUTC property of the Entity Orderitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Orderitem"."UpdatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)OrderitemFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)OrderitemFieldIndex.UpdatedUTC, value); }
		}

		/// <summary>The Type property of the Entity Orderitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Orderitem"."Type".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Crave.Enums.OrderitemType Type
		{
			get { return (Crave.Enums.OrderitemType)GetValue((int)OrderitemFieldIndex.Type, true); }
			set	{ SetValue((int)OrderitemFieldIndex.Type, value); }
		}

		/// <summary>The PriceInTax property of the Entity Orderitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Orderitem"."PriceInTax".<br/>Table field type characteristics (type, precision, scale, length): Money, 19, 4, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal PriceInTax
		{
			get { return (System.Decimal)GetValue((int)OrderitemFieldIndex.PriceInTax, true); }
			set	{ SetValue((int)OrderitemFieldIndex.PriceInTax, value); }
		}

		/// <summary>The PriceExTax property of the Entity Orderitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Orderitem"."PriceExTax".<br/>Table field type characteristics (type, precision, scale, length): Money, 19, 4, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal PriceExTax
		{
			get { return (System.Decimal)GetValue((int)OrderitemFieldIndex.PriceExTax, true); }
			set	{ SetValue((int)OrderitemFieldIndex.PriceExTax, value); }
		}

		/// <summary>The PriceTotalInTax property of the Entity Orderitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Orderitem"."PriceTotalInTax".<br/>Table field type characteristics (type, precision, scale, length): Money, 19, 4, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal PriceTotalInTax
		{
			get { return (System.Decimal)GetValue((int)OrderitemFieldIndex.PriceTotalInTax, true); }
			set	{ SetValue((int)OrderitemFieldIndex.PriceTotalInTax, value); }
		}

		/// <summary>The PriceTotalExTax property of the Entity Orderitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Orderitem"."PriceTotalExTax".<br/>Table field type characteristics (type, precision, scale, length): Money, 19, 4, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal PriceTotalExTax
		{
			get { return (System.Decimal)GetValue((int)OrderitemFieldIndex.PriceTotalExTax, true); }
			set	{ SetValue((int)OrderitemFieldIndex.PriceTotalExTax, value); }
		}

		/// <summary>The TaxPercentage property of the Entity Orderitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Orderitem"."TaxPercentage".<br/>Table field type characteristics (type, precision, scale, length): Float, 38, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Double TaxPercentage
		{
			get { return (System.Double)GetValue((int)OrderitemFieldIndex.TaxPercentage, true); }
			set	{ SetValue((int)OrderitemFieldIndex.TaxPercentage, value); }
		}

		/// <summary>The TaxTariffId property of the Entity Orderitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Orderitem"."TaxTariffId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> TaxTariffId
		{
			get { return (Nullable<System.Int32>)GetValue((int)OrderitemFieldIndex.TaxTariffId, false); }
			set	{ SetValue((int)OrderitemFieldIndex.TaxTariffId, value); }
		}

		/// <summary>The Tax property of the Entity Orderitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Orderitem"."Tax".<br/>Table field type characteristics (type, precision, scale, length): Money, 19, 4, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal Tax
		{
			get { return (System.Decimal)GetValue((int)OrderitemFieldIndex.Tax, true); }
			set	{ SetValue((int)OrderitemFieldIndex.Tax, value); }
		}

		/// <summary>The TaxTotal property of the Entity Orderitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Orderitem"."TaxTotal".<br/>Table field type characteristics (type, precision, scale, length): Money, 19, 4, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TaxTotal
		{
			get { return (System.Decimal)GetValue((int)OrderitemFieldIndex.TaxTotal, true); }
			set	{ SetValue((int)OrderitemFieldIndex.TaxTotal, value); }
		}

		/// <summary>The TaxName property of the Entity Orderitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Orderitem"."TaxName".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 200.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String TaxName
		{
			get { return (System.String)GetValue((int)OrderitemFieldIndex.TaxName, true); }
			set	{ SetValue((int)OrderitemFieldIndex.TaxName, value); }
		}

		/// <summary>The ExternalIdentifier property of the Entity Orderitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Orderitem"."ExternalIdentifier".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ExternalIdentifier
		{
			get { return (System.String)GetValue((int)OrderitemFieldIndex.ExternalIdentifier, true); }
			set	{ SetValue((int)OrderitemFieldIndex.ExternalIdentifier, value); }
		}

		/// <summary>Gets the EntityCollection with the related entities of type 'OrderitemAlterationitemEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(OrderitemAlterationitemEntity))]
		public virtual EntityCollection<OrderitemAlterationitemEntity> OrderitemAlterationitemCollection { get { return GetOrCreateEntityCollection<OrderitemAlterationitemEntity, OrderitemAlterationitemEntityFactory>("Orderitem", true, false, ref _orderitemAlterationitemCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'OrderitemTagEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(OrderitemTagEntity))]
		public virtual EntityCollection<OrderitemTagEntity> OrderitemTagCollection { get { return GetOrCreateEntityCollection<OrderitemTagEntity, OrderitemTagEntityFactory>("Orderitem", true, false, ref _orderitemTagCollection); } }

		/// <summary>Gets / sets related entity of type 'CategoryEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual CategoryEntity Category
		{
			get { return _category; }
			set { SetSingleRelatedEntityNavigator(value, "Category"); }
		}

		/// <summary>Gets / sets related entity of type 'OrderEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual OrderEntity Order
		{
			get { return _order; }
			set { SetSingleRelatedEntityNavigator(value, "Order"); }
		}

		/// <summary>Gets / sets related entity of type 'PriceLevelItemEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual PriceLevelItemEntity PriceLevelItem
		{
			get { return _priceLevelItem; }
			set { SetSingleRelatedEntityNavigator(value, "PriceLevelItem"); }
		}

		/// <summary>Gets / sets related entity of type 'ProductEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual ProductEntity Product
		{
			get { return _product; }
			set { SetSingleRelatedEntityNavigator(value, "Product"); }
		}

		/// <summary>Gets / sets related entity of type 'TaxTariffEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual TaxTariffEntity TaxTariff
		{
			get { return _taxTariff; }
			set { SetSingleRelatedEntityNavigator(value, "TaxTariff"); }
		}


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen
{
	public enum OrderitemFieldIndex
	{
		///<summary>OrderitemId. </summary>
		OrderitemId,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>OrderId. </summary>
		OrderId,
		///<summary>CategoryId. </summary>
		CategoryId,
		///<summary>CategoryName. </summary>
		CategoryName,
		///<summary>CategoryPath. </summary>
		CategoryPath,
		///<summary>ProductId. </summary>
		ProductId,
		///<summary>ProductDescription. </summary>
		ProductDescription,
		///<summary>ProductName. </summary>
		ProductName,
		///<summary>ProductPriceIn. </summary>
		ProductPriceIn,
		///<summary>Quantity. </summary>
		Quantity,
		///<summary>VatPercentage. </summary>
		VatPercentage,
		///<summary>Notes. </summary>
		Notes,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>Guid. </summary>
		Guid,
		///<summary>Color. </summary>
		Color,
		///<summary>OrderSource. </summary>
		OrderSource,
		///<summary>PriceLevelItemId. </summary>
		PriceLevelItemId,
		///<summary>PriceLevelLog. </summary>
		PriceLevelLog,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>Type. </summary>
		Type,
		///<summary>PriceInTax. </summary>
		PriceInTax,
		///<summary>PriceExTax. </summary>
		PriceExTax,
		///<summary>PriceTotalInTax. </summary>
		PriceTotalInTax,
		///<summary>PriceTotalExTax. </summary>
		PriceTotalExTax,
		///<summary>TaxPercentage. </summary>
		TaxPercentage,
		///<summary>TaxTariffId. </summary>
		TaxTariffId,
		///<summary>Tax. </summary>
		Tax,
		///<summary>TaxTotal. </summary>
		TaxTotal,
		///<summary>TaxName. </summary>
		TaxName,
		///<summary>ExternalIdentifier. </summary>
		ExternalIdentifier,
		/// <summary></summary>
		AmountOfFields
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Orderitem. </summary>
	public partial class OrderitemRelations: RelationFactory
	{
		#region Custom Relation code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomRelationCode
        private const ReferentialConstraintDeleteRule DeleteRuleForOrderitemAlterationitemEntityUsingOrderitemId = ReferentialConstraintDeleteRule.Cascade;
        private const ReferentialConstraintDeleteRule DeleteRuleForOrderitemTagEntityUsingOrderitemId = ReferentialConstraintDeleteRule.Cascade;
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		
		// private const ReferentialConstraintDeleteRule DeleteRuleForOrderitemAlterationitemEntityUsingOrderitemId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForOrderitemTagEntityUsingOrderitemId = ReferentialConstraintDeleteRule


		/// <summary>Gets all relations and delete rules of the OrderitemEntity as a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects.</summary>
		/// <returns>a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects</returns>
		public virtual Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> GetRelationsAndDeleteRules()
		{
			Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> toReturn = new Dictionary<IEntityRelation, ReferentialConstraintDeleteRule>();
			toReturn.Add(this.OrderitemAlterationitemEntityUsingOrderitemId, OrderitemRelations.DeleteRuleForOrderitemAlterationitemEntityUsingOrderitemId);
			toReturn.Add(this.OrderitemTagEntityUsingOrderitemId, OrderitemRelations.DeleteRuleForOrderitemTagEntityUsingOrderitemId);
			return toReturn;
		}		
	
	
		/// <summary>Returns a new IEntityRelation object, between OrderitemEntity and OrderitemAlterationitemEntity over the 1:n relation they have, using the relation between the fields: Orderitem.OrderitemId - OrderitemAlterationitem.OrderitemId</summary>
		public virtual IEntityRelation OrderitemAlterationitemEntityUsingOrderitemId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "OrderitemAlterationitemCollection", true, new[] { OrderitemFields.OrderitemId, OrderitemAlterationitemFields.OrderitemId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between OrderitemEntity and OrderitemTagEntity over the 1:n relation they have, using the relation between the fields: Orderitem.OrderitemId - OrderitemTag.OrderitemId</summary>
		public virtual IEntityRelation OrderitemTagEntityUsingOrderitemId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "OrderitemTagCollection", true, new[] { OrderitemFields.OrderitemId, OrderitemTagFields.OrderitemId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between OrderitemEntity and CategoryEntity over the m:1 relation they have, using the relation between the fields: Orderitem.CategoryId - Category.CategoryId</summary>
		public virtual IEntityRelation CategoryEntityUsingCategoryId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Category", false, new[] { CategoryFields.CategoryId, OrderitemFields.CategoryId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between OrderitemEntity and OrderEntity over the m:1 relation they have, using the relation between the fields: Orderitem.OrderId - Order.OrderId</summary>
		public virtual IEntityRelation OrderEntityUsingOrderId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Order", false, new[] { OrderFields.OrderId, OrderitemFields.OrderId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between OrderitemEntity and PriceLevelItemEntity over the m:1 relation they have, using the relation between the fields: Orderitem.PriceLevelItemId - PriceLevelItem.PriceLevelItemId</summary>
		public virtual IEntityRelation PriceLevelItemEntityUsingPriceLevelItemId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "PriceLevelItem", false, new[] { PriceLevelItemFields.PriceLevelItemId, OrderitemFields.PriceLevelItemId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between OrderitemEntity and ProductEntity over the m:1 relation they have, using the relation between the fields: Orderitem.ProductId - Product.ProductId</summary>
		public virtual IEntityRelation ProductEntityUsingProductId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Product", false, new[] { ProductFields.ProductId, OrderitemFields.ProductId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between OrderitemEntity and TaxTariffEntity over the m:1 relation they have, using the relation between the fields: Orderitem.TaxTariffId - TaxTariff.TaxTariffId</summary>
		public virtual IEntityRelation TaxTariffEntityUsingTaxTariffId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "TaxTariff", false, new[] { TaxTariffFields.TaxTariffId, OrderitemFields.TaxTariffId }); }
		}

	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticOrderitemRelations
	{
		internal static readonly IEntityRelation OrderitemAlterationitemEntityUsingOrderitemIdStatic = new OrderitemRelations().OrderitemAlterationitemEntityUsingOrderitemId;
		internal static readonly IEntityRelation OrderitemTagEntityUsingOrderitemIdStatic = new OrderitemRelations().OrderitemTagEntityUsingOrderitemId;
		internal static readonly IEntityRelation CategoryEntityUsingCategoryIdStatic = new OrderitemRelations().CategoryEntityUsingCategoryId;
		internal static readonly IEntityRelation OrderEntityUsingOrderIdStatic = new OrderitemRelations().OrderEntityUsingOrderId;
		internal static readonly IEntityRelation PriceLevelItemEntityUsingPriceLevelItemIdStatic = new OrderitemRelations().PriceLevelItemEntityUsingPriceLevelItemId;
		internal static readonly IEntityRelation ProductEntityUsingProductIdStatic = new OrderitemRelations().ProductEntityUsingProductId;
		internal static readonly IEntityRelation TaxTariffEntityUsingTaxTariffIdStatic = new OrderitemRelations().TaxTariffEntityUsingTaxTariffId;

		/// <summary>CTor</summary>
		static StaticOrderitemRelations() { }
	}
}

