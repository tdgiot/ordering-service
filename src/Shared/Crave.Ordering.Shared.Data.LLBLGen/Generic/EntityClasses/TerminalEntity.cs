﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 5.8
// Code is generated on: 
// Code is generated using templates: Crave.TemplateBindings
// Templates vendor: Crave Interactive
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.FactoryClasses;
using Crave.Ordering.Shared.Data.LLBLGen.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

// ReSharper disable All
namespace Crave.Ordering.Shared.Data.LLBLGen.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'Terminal'.<br/><br/></summary>
	[Serializable]
	public partial class TerminalEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END
	
	{
		private EntityCollection<DeliverypointgroupEntity> _deliverypointgroupCollection;
		private EntityCollection<NetmessageEntity> _sentNetmessageCollection;
		private EntityCollection<NetmessageEntity> _receivedNetmessageCollection;
		private EntityCollection<OrderRoutestephandlerEntity> _orderRoutestephandlerCollection;
		private EntityCollection<OrderRoutestephandlerEntity> _orderRoutestephandlerCollection1;
		private EntityCollection<OrderRoutestephandlerHistoryEntity> _orderRoutestephandlerHistoryCollection;
		private EntityCollection<OrderRoutestephandlerHistoryEntity> _orderRoutestephandlerHistoryCollection1;
		private EntityCollection<RoutestephandlerEntity> _routestephandlerCollection;
		private EntityCollection<TerminalEntity> _terminalCollection;
		private CompanyEntity _company;
		private DeliverypointEntity _altSystemMessagesDeliverypoint;
		private DeliverypointEntity _systemMessagesDeliverypoint;
		private DeliverypointgroupEntity _deliverypointgroup;
		private DeviceEntity _device;
		private ProductEntity _batteryLowProduct;
		private ProductEntity _clientDisconnectedProduct;
		private ProductEntity _orderFailedProduct;
		private ProductEntity _unlockDeliverypointProduct;
		private TerminalEntity _forwardToTerminal;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		private static TerminalEntityStaticMetaData _staticMetaData = new TerminalEntityStaticMetaData();
		private static TerminalRelations _relationsFactory = new TerminalRelations();		

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Company</summary>
			public static readonly string Company = "Company";
			/// <summary>Member name AltSystemMessagesDeliverypoint</summary>
			public static readonly string AltSystemMessagesDeliverypoint = "AltSystemMessagesDeliverypoint";
			/// <summary>Member name SystemMessagesDeliverypoint</summary>
			public static readonly string SystemMessagesDeliverypoint = "SystemMessagesDeliverypoint";
			/// <summary>Member name Deliverypointgroup</summary>
			public static readonly string Deliverypointgroup = "Deliverypointgroup";
			/// <summary>Member name Device</summary>
			public static readonly string Device = "Device";
			/// <summary>Member name BatteryLowProduct</summary>
			public static readonly string BatteryLowProduct = "BatteryLowProduct";
			/// <summary>Member name ClientDisconnectedProduct</summary>
			public static readonly string ClientDisconnectedProduct = "ClientDisconnectedProduct";
			/// <summary>Member name OrderFailedProduct</summary>
			public static readonly string OrderFailedProduct = "OrderFailedProduct";
			/// <summary>Member name UnlockDeliverypointProduct</summary>
			public static readonly string UnlockDeliverypointProduct = "UnlockDeliverypointProduct";
			/// <summary>Member name ForwardToTerminal</summary>
			public static readonly string ForwardToTerminal = "ForwardToTerminal";
			/// <summary>Member name DeliverypointgroupCollection</summary>
			public static readonly string DeliverypointgroupCollection = "DeliverypointgroupCollection";
			/// <summary>Member name SentNetmessageCollection</summary>
			public static readonly string SentNetmessageCollection = "SentNetmessageCollection";
			/// <summary>Member name ReceivedNetmessageCollection</summary>
			public static readonly string ReceivedNetmessageCollection = "ReceivedNetmessageCollection";
			/// <summary>Member name OrderRoutestephandlerCollection</summary>
			public static readonly string OrderRoutestephandlerCollection = "OrderRoutestephandlerCollection";
			/// <summary>Member name OrderRoutestephandlerCollection1</summary>
			public static readonly string OrderRoutestephandlerCollection1 = "OrderRoutestephandlerCollection1";
			/// <summary>Member name OrderRoutestephandlerHistoryCollection</summary>
			public static readonly string OrderRoutestephandlerHistoryCollection = "OrderRoutestephandlerHistoryCollection";
			/// <summary>Member name OrderRoutestephandlerHistoryCollection1</summary>
			public static readonly string OrderRoutestephandlerHistoryCollection1 = "OrderRoutestephandlerHistoryCollection1";
			/// <summary>Member name RoutestephandlerCollection</summary>
			public static readonly string RoutestephandlerCollection = "RoutestephandlerCollection";
			/// <summary>Member name TerminalCollection</summary>
			public static readonly string TerminalCollection = "TerminalCollection";
		}

		/// <summary>Static meta-data storage for navigator related information</summary>
		protected class TerminalEntityStaticMetaData : EntityStaticMetaDataBase
		{
			public TerminalEntityStaticMetaData()
			{
				SetEntityCoreInfo("TerminalEntity", InheritanceHierarchyType.None, false, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.TerminalEntity, typeof(TerminalEntity), typeof(TerminalEntityFactory), false);
				AddNavigatorMetaData<TerminalEntity, EntityCollection<DeliverypointgroupEntity>>("DeliverypointgroupCollection", a => a._deliverypointgroupCollection, (a, b) => a._deliverypointgroupCollection = b, a => a.DeliverypointgroupCollection, () => new TerminalRelations().DeliverypointgroupEntityUsingTerminalId, typeof(DeliverypointgroupEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.DeliverypointgroupEntity);
				AddNavigatorMetaData<TerminalEntity, EntityCollection<NetmessageEntity>>("SentNetmessageCollection", a => a._sentNetmessageCollection, (a, b) => a._sentNetmessageCollection = b, a => a.SentNetmessageCollection, () => new TerminalRelations().NetmessageEntityUsingSenderTerminalId, typeof(NetmessageEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.NetmessageEntity);
				AddNavigatorMetaData<TerminalEntity, EntityCollection<NetmessageEntity>>("ReceivedNetmessageCollection", a => a._receivedNetmessageCollection, (a, b) => a._receivedNetmessageCollection = b, a => a.ReceivedNetmessageCollection, () => new TerminalRelations().NetmessageEntityUsingReceiverTerminalId, typeof(NetmessageEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.NetmessageEntity);
				AddNavigatorMetaData<TerminalEntity, EntityCollection<OrderRoutestephandlerEntity>>("OrderRoutestephandlerCollection", a => a._orderRoutestephandlerCollection, (a, b) => a._orderRoutestephandlerCollection = b, a => a.OrderRoutestephandlerCollection, () => new TerminalRelations().OrderRoutestephandlerEntityUsingTerminalId, typeof(OrderRoutestephandlerEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.OrderRoutestephandlerEntity);
				AddNavigatorMetaData<TerminalEntity, EntityCollection<OrderRoutestephandlerEntity>>("OrderRoutestephandlerCollection1", a => a._orderRoutestephandlerCollection1, (a, b) => a._orderRoutestephandlerCollection1 = b, a => a.OrderRoutestephandlerCollection1, () => new TerminalRelations().OrderRoutestephandlerEntityUsingForwardedFromTerminalId, typeof(OrderRoutestephandlerEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.OrderRoutestephandlerEntity);
				AddNavigatorMetaData<TerminalEntity, EntityCollection<OrderRoutestephandlerHistoryEntity>>("OrderRoutestephandlerHistoryCollection", a => a._orderRoutestephandlerHistoryCollection, (a, b) => a._orderRoutestephandlerHistoryCollection = b, a => a.OrderRoutestephandlerHistoryCollection, () => new TerminalRelations().OrderRoutestephandlerHistoryEntityUsingTerminalId, typeof(OrderRoutestephandlerHistoryEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.OrderRoutestephandlerHistoryEntity);
				AddNavigatorMetaData<TerminalEntity, EntityCollection<OrderRoutestephandlerHistoryEntity>>("OrderRoutestephandlerHistoryCollection1", a => a._orderRoutestephandlerHistoryCollection1, (a, b) => a._orderRoutestephandlerHistoryCollection1 = b, a => a.OrderRoutestephandlerHistoryCollection1, () => new TerminalRelations().OrderRoutestephandlerHistoryEntityUsingForwardedFromTerminalId, typeof(OrderRoutestephandlerHistoryEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.OrderRoutestephandlerHistoryEntity);
				AddNavigatorMetaData<TerminalEntity, EntityCollection<RoutestephandlerEntity>>("RoutestephandlerCollection", a => a._routestephandlerCollection, (a, b) => a._routestephandlerCollection = b, a => a.RoutestephandlerCollection, () => new TerminalRelations().RoutestephandlerEntityUsingTerminalId, typeof(RoutestephandlerEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.RoutestephandlerEntity);
				AddNavigatorMetaData<TerminalEntity, EntityCollection<TerminalEntity>>("TerminalCollection", a => a._terminalCollection, (a, b) => a._terminalCollection = b, a => a.TerminalCollection, () => new TerminalRelations().TerminalEntityUsingForwardToTerminalId, typeof(TerminalEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.TerminalEntity);
				AddNavigatorMetaData<TerminalEntity, CompanyEntity>("Company", "TerminalCollection", (a, b) => a._company = b, a => a._company, (a, b) => a.Company = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticTerminalRelations.CompanyEntityUsingCompanyIdStatic, ()=>new TerminalRelations().CompanyEntityUsingCompanyId, null, new int[] { (int)TerminalFieldIndex.CompanyId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.CompanyEntity);
				AddNavigatorMetaData<TerminalEntity, DeliverypointEntity>("AltSystemMessagesDeliverypoint", "TerminalCollection1", (a, b) => a._altSystemMessagesDeliverypoint = b, a => a._altSystemMessagesDeliverypoint, (a, b) => a.AltSystemMessagesDeliverypoint = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticTerminalRelations.DeliverypointEntityUsingAltSystemMessagesDeliverypointIdStatic, ()=>new TerminalRelations().DeliverypointEntityUsingAltSystemMessagesDeliverypointId, null, new int[] { (int)TerminalFieldIndex.AltSystemMessagesDeliverypointId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.DeliverypointEntity);
				AddNavigatorMetaData<TerminalEntity, DeliverypointEntity>("SystemMessagesDeliverypoint", "TerminalCollection", (a, b) => a._systemMessagesDeliverypoint = b, a => a._systemMessagesDeliverypoint, (a, b) => a.SystemMessagesDeliverypoint = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticTerminalRelations.DeliverypointEntityUsingSystemMessagesDeliverypointIdStatic, ()=>new TerminalRelations().DeliverypointEntityUsingSystemMessagesDeliverypointId, null, new int[] { (int)TerminalFieldIndex.SystemMessagesDeliverypointId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.DeliverypointEntity);
				AddNavigatorMetaData<TerminalEntity, DeliverypointgroupEntity>("Deliverypointgroup", "TerminalCollection", (a, b) => a._deliverypointgroup = b, a => a._deliverypointgroup, (a, b) => a.Deliverypointgroup = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticTerminalRelations.DeliverypointgroupEntityUsingDeliverypointgroupIdStatic, ()=>new TerminalRelations().DeliverypointgroupEntityUsingDeliverypointgroupId, null, new int[] { (int)TerminalFieldIndex.DeliverypointgroupId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.DeliverypointgroupEntity);
				AddNavigatorMetaData<TerminalEntity, DeviceEntity>("Device", "TerminalCollection", (a, b) => a._device = b, a => a._device, (a, b) => a.Device = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticTerminalRelations.DeviceEntityUsingDeviceIdStatic, ()=>new TerminalRelations().DeviceEntityUsingDeviceId, null, new int[] { (int)TerminalFieldIndex.DeviceId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.DeviceEntity);
				AddNavigatorMetaData<TerminalEntity, ProductEntity>("BatteryLowProduct", "TerminalCollection1", (a, b) => a._batteryLowProduct = b, a => a._batteryLowProduct, (a, b) => a.BatteryLowProduct = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticTerminalRelations.ProductEntityUsingBatteryLowProductIdStatic, ()=>new TerminalRelations().ProductEntityUsingBatteryLowProductId, null, new int[] { (int)TerminalFieldIndex.BatteryLowProductId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ProductEntity);
				AddNavigatorMetaData<TerminalEntity, ProductEntity>("ClientDisconnectedProduct", "TerminalCollection2", (a, b) => a._clientDisconnectedProduct = b, a => a._clientDisconnectedProduct, (a, b) => a.ClientDisconnectedProduct = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticTerminalRelations.ProductEntityUsingClientDisconnectedProductIdStatic, ()=>new TerminalRelations().ProductEntityUsingClientDisconnectedProductId, null, new int[] { (int)TerminalFieldIndex.ClientDisconnectedProductId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ProductEntity);
				AddNavigatorMetaData<TerminalEntity, ProductEntity>("OrderFailedProduct", "TerminalCollection3", (a, b) => a._orderFailedProduct = b, a => a._orderFailedProduct, (a, b) => a.OrderFailedProduct = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticTerminalRelations.ProductEntityUsingOrderFailedProductIdStatic, ()=>new TerminalRelations().ProductEntityUsingOrderFailedProductId, null, new int[] { (int)TerminalFieldIndex.OrderFailedProductId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ProductEntity);
				AddNavigatorMetaData<TerminalEntity, ProductEntity>("UnlockDeliverypointProduct", "TerminalCollection", (a, b) => a._unlockDeliverypointProduct = b, a => a._unlockDeliverypointProduct, (a, b) => a.UnlockDeliverypointProduct = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticTerminalRelations.ProductEntityUsingUnlockDeliverypointProductIdStatic, ()=>new TerminalRelations().ProductEntityUsingUnlockDeliverypointProductId, null, new int[] { (int)TerminalFieldIndex.UnlockDeliverypointProductId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ProductEntity);
				AddNavigatorMetaData<TerminalEntity, TerminalEntity>("ForwardToTerminal", "TerminalCollection", (a, b) => a._forwardToTerminal = b, a => a._forwardToTerminal, (a, b) => a.ForwardToTerminal = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticTerminalRelations.TerminalEntityUsingTerminalIdForwardToTerminalIdStatic, ()=>new TerminalRelations().TerminalEntityUsingTerminalIdForwardToTerminalId, null, new int[] { (int)TerminalFieldIndex.ForwardToTerminalId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.TerminalEntity);
			}
		}

		/// <summary>Static ctor</summary>
		static TerminalEntity()
		{
		}

		/// <summary> CTor</summary>
		public TerminalEntity()
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public TerminalEntity(IEntityFields2 fields)
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this TerminalEntity</param>
		public TerminalEntity(IValidator validator)
		{
			InitClassEmpty(validator, null);
		}

		/// <summary> CTor</summary>
		/// <param name="terminalId">PK value for Terminal which data should be fetched into this Terminal object</param>
		public TerminalEntity(System.Int32 terminalId) : this(terminalId, null)
		{
		}

		/// <summary> CTor</summary>
		/// <param name="terminalId">PK value for Terminal which data should be fetched into this Terminal object</param>
		/// <param name="validator">The custom validator object for this TerminalEntity</param>
		public TerminalEntity(System.Int32 terminalId, IValidator validator)
		{
			InitClassEmpty(validator, null);
			this.TerminalId = terminalId;
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected TerminalEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Deliverypointgroup' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoDeliverypointgroupCollection() { return CreateRelationInfoForNavigator("DeliverypointgroupCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Netmessage' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoSentNetmessageCollection() { return CreateRelationInfoForNavigator("SentNetmessageCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Netmessage' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoReceivedNetmessageCollection() { return CreateRelationInfoForNavigator("ReceivedNetmessageCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'OrderRoutestephandler' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoOrderRoutestephandlerCollection() { return CreateRelationInfoForNavigator("OrderRoutestephandlerCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'OrderRoutestephandler' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoOrderRoutestephandlerCollection1() { return CreateRelationInfoForNavigator("OrderRoutestephandlerCollection1"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'OrderRoutestephandlerHistory' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoOrderRoutestephandlerHistoryCollection() { return CreateRelationInfoForNavigator("OrderRoutestephandlerHistoryCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'OrderRoutestephandlerHistory' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoOrderRoutestephandlerHistoryCollection1() { return CreateRelationInfoForNavigator("OrderRoutestephandlerHistoryCollection1"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Routestephandler' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoRoutestephandlerCollection() { return CreateRelationInfoForNavigator("RoutestephandlerCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Terminal' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoTerminalCollection() { return CreateRelationInfoForNavigator("TerminalCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Company' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoCompany() { return CreateRelationInfoForNavigator("Company"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Deliverypoint' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoAltSystemMessagesDeliverypoint() { return CreateRelationInfoForNavigator("AltSystemMessagesDeliverypoint"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Deliverypoint' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoSystemMessagesDeliverypoint() { return CreateRelationInfoForNavigator("SystemMessagesDeliverypoint"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Deliverypointgroup' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoDeliverypointgroup() { return CreateRelationInfoForNavigator("Deliverypointgroup"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Device' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoDevice() { return CreateRelationInfoForNavigator("Device"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Product' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoBatteryLowProduct() { return CreateRelationInfoForNavigator("BatteryLowProduct"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Product' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoClientDisconnectedProduct() { return CreateRelationInfoForNavigator("ClientDisconnectedProduct"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Product' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoOrderFailedProduct() { return CreateRelationInfoForNavigator("OrderFailedProduct"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Product' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoUnlockDeliverypointProduct() { return CreateRelationInfoForNavigator("UnlockDeliverypointProduct"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Terminal' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoForwardToTerminal() { return CreateRelationInfoForNavigator("ForwardToTerminal"); }
		
		/// <inheritdoc/>
		protected override EntityStaticMetaDataBase GetEntityStaticMetaData() {	return _staticMetaData; }

		/// <summary>Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		/// <summary>Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this TerminalEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary>The relations object holding all relations of this entity with other entity classes.</summary>
		public static TerminalRelations Relations { get { return _relationsFactory; } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Deliverypointgroup' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathDeliverypointgroupCollection { get { return _staticMetaData.GetPrefetchPathElement("DeliverypointgroupCollection", CommonEntityBase.CreateEntityCollection<DeliverypointgroupEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Netmessage' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathSentNetmessageCollection { get { return _staticMetaData.GetPrefetchPathElement("SentNetmessageCollection", CommonEntityBase.CreateEntityCollection<NetmessageEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Netmessage' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathReceivedNetmessageCollection { get { return _staticMetaData.GetPrefetchPathElement("ReceivedNetmessageCollection", CommonEntityBase.CreateEntityCollection<NetmessageEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'OrderRoutestephandler' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathOrderRoutestephandlerCollection { get { return _staticMetaData.GetPrefetchPathElement("OrderRoutestephandlerCollection", CommonEntityBase.CreateEntityCollection<OrderRoutestephandlerEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'OrderRoutestephandler' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathOrderRoutestephandlerCollection1 { get { return _staticMetaData.GetPrefetchPathElement("OrderRoutestephandlerCollection1", CommonEntityBase.CreateEntityCollection<OrderRoutestephandlerEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'OrderRoutestephandlerHistory' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathOrderRoutestephandlerHistoryCollection { get { return _staticMetaData.GetPrefetchPathElement("OrderRoutestephandlerHistoryCollection", CommonEntityBase.CreateEntityCollection<OrderRoutestephandlerHistoryEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'OrderRoutestephandlerHistory' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathOrderRoutestephandlerHistoryCollection1 { get { return _staticMetaData.GetPrefetchPathElement("OrderRoutestephandlerHistoryCollection1", CommonEntityBase.CreateEntityCollection<OrderRoutestephandlerHistoryEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Routestephandler' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathRoutestephandlerCollection { get { return _staticMetaData.GetPrefetchPathElement("RoutestephandlerCollection", CommonEntityBase.CreateEntityCollection<RoutestephandlerEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Terminal' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathTerminalCollection { get { return _staticMetaData.GetPrefetchPathElement("TerminalCollection", CommonEntityBase.CreateEntityCollection<TerminalEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Company' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathCompanyEntity { get { return _staticMetaData.GetPrefetchPathElement("Company", CommonEntityBase.CreateEntityCollection<CompanyEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Deliverypoint' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathAltSystemMessagesDeliverypointEntity { get { return _staticMetaData.GetPrefetchPathElement("AltSystemMessagesDeliverypoint", CommonEntityBase.CreateEntityCollection<DeliverypointEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Deliverypoint' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathSystemMessagesDeliverypointEntity { get { return _staticMetaData.GetPrefetchPathElement("SystemMessagesDeliverypoint", CommonEntityBase.CreateEntityCollection<DeliverypointEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Deliverypointgroup' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathDeliverypointgroupEntity { get { return _staticMetaData.GetPrefetchPathElement("Deliverypointgroup", CommonEntityBase.CreateEntityCollection<DeliverypointgroupEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Device' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathDeviceEntity { get { return _staticMetaData.GetPrefetchPathElement("Device", CommonEntityBase.CreateEntityCollection<DeviceEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Product' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathBatteryLowProductEntity { get { return _staticMetaData.GetPrefetchPathElement("BatteryLowProduct", CommonEntityBase.CreateEntityCollection<ProductEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Product' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathClientDisconnectedProductEntity { get { return _staticMetaData.GetPrefetchPathElement("ClientDisconnectedProduct", CommonEntityBase.CreateEntityCollection<ProductEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Product' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathOrderFailedProductEntity { get { return _staticMetaData.GetPrefetchPathElement("OrderFailedProduct", CommonEntityBase.CreateEntityCollection<ProductEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Product' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathUnlockDeliverypointProductEntity { get { return _staticMetaData.GetPrefetchPathElement("UnlockDeliverypointProduct", CommonEntityBase.CreateEntityCollection<ProductEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Terminal' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathForwardToTerminalEntity { get { return _staticMetaData.GetPrefetchPathElement("ForwardToTerminal", CommonEntityBase.CreateEntityCollection<TerminalEntity>()); } }

		/// <summary>The TerminalId property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."TerminalId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 TerminalId
		{
			get { return (System.Int32)GetValue((int)TerminalFieldIndex.TerminalId, true); }
			set { SetValue((int)TerminalFieldIndex.TerminalId, value); }		}

		/// <summary>The CompanyId property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."CompanyId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CompanyId
		{
			get { return (System.Int32)GetValue((int)TerminalFieldIndex.CompanyId, true); }
			set	{ SetValue((int)TerminalFieldIndex.CompanyId, value); }
		}

		/// <summary>The UIModeId property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."UIModeId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UIModeId
		{
			get { return (Nullable<System.Int32>)GetValue((int)TerminalFieldIndex.UIModeId, false); }
			set	{ SetValue((int)TerminalFieldIndex.UIModeId, value); }
		}

		/// <summary>The Type property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."Type".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<Crave.Enums.TerminalType> Type
		{
			get { return (Nullable<Crave.Enums.TerminalType>)GetValue((int)TerminalFieldIndex.Type, false); }
			set	{ SetValue((int)TerminalFieldIndex.Type, value); }
		}

		/// <summary>The OnsiteServerDeliverypointgroupId property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."OnsiteServerDeliverypointgroupId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> OnsiteServerDeliverypointgroupId
		{
			get { return (Nullable<System.Int32>)GetValue((int)TerminalFieldIndex.OnsiteServerDeliverypointgroupId, false); }
			set	{ SetValue((int)TerminalFieldIndex.OnsiteServerDeliverypointgroupId, value); }
		}

		/// <summary>The Name property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."Name".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)TerminalFieldIndex.Name, true); }
			set	{ SetValue((int)TerminalFieldIndex.Name, value); }
		}

		/// <summary>The LastStatus property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."LastStatus".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<Crave.Enums.TerminalStatus> LastStatus
		{
			get { return (Nullable<Crave.Enums.TerminalStatus>)GetValue((int)TerminalFieldIndex.LastStatus, false); }
			set	{ SetValue((int)TerminalFieldIndex.LastStatus, value); }
		}

		/// <summary>The LastStatusText property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."LastStatusText".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 250.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String LastStatusText
		{
			get { return (System.String)GetValue((int)TerminalFieldIndex.LastStatusText, true); }
			set	{ SetValue((int)TerminalFieldIndex.LastStatusText, value); }
		}

		/// <summary>The LastStatusMessage property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."LastStatusMessage".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 250.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String LastStatusMessage
		{
			get { return (System.String)GetValue((int)TerminalFieldIndex.LastStatusMessage, true); }
			set	{ SetValue((int)TerminalFieldIndex.LastStatusMessage, value); }
		}

		/// <summary>The OutOfChargeNotificationsSent property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."OutOfChargeNotificationsSent".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 OutOfChargeNotificationsSent
		{
			get { return (System.Int32)GetValue((int)TerminalFieldIndex.OutOfChargeNotificationsSent, true); }
			set	{ SetValue((int)TerminalFieldIndex.OutOfChargeNotificationsSent, value); }
		}

		/// <summary>The RequestInterval property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."RequestInterval".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 RequestInterval
		{
			get { return (System.Int32)GetValue((int)TerminalFieldIndex.RequestInterval, true); }
			set	{ SetValue((int)TerminalFieldIndex.RequestInterval, value); }
		}

		/// <summary>The DiagnoseInterval property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."DiagnoseInterval".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 DiagnoseInterval
		{
			get { return (System.Int32)GetValue((int)TerminalFieldIndex.DiagnoseInterval, true); }
			set	{ SetValue((int)TerminalFieldIndex.DiagnoseInterval, value); }
		}

		/// <summary>The PrintingEnabled property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."PrintingEnabled".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean PrintingEnabled
		{
			get { return (System.Boolean)GetValue((int)TerminalFieldIndex.PrintingEnabled, true); }
			set	{ SetValue((int)TerminalFieldIndex.PrintingEnabled, value); }
		}

		/// <summary>The PrinterName property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."PrinterName".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PrinterName
		{
			get { return (System.String)GetValue((int)TerminalFieldIndex.PrinterName, true); }
			set	{ SetValue((int)TerminalFieldIndex.PrinterName, value); }
		}

		/// <summary>The RecordAllRequestsToRequestLog property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."RecordAllRequestsToRequestLog".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean RecordAllRequestsToRequestLog
		{
			get { return (System.Boolean)GetValue((int)TerminalFieldIndex.RecordAllRequestsToRequestLog, true); }
			set	{ SetValue((int)TerminalFieldIndex.RecordAllRequestsToRequestLog, value); }
		}

		/// <summary>The TeamviewerId property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."TeamviewerId".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 250.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String TeamviewerId
		{
			get { return (System.String)GetValue((int)TerminalFieldIndex.TeamviewerId, true); }
			set	{ SetValue((int)TerminalFieldIndex.TeamviewerId, value); }
		}

		/// <summary>The MaxDaysLogHistory property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."MaxDaysLogHistory".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 MaxDaysLogHistory
		{
			get { return (System.Int32)GetValue((int)TerminalFieldIndex.MaxDaysLogHistory, true); }
			set	{ SetValue((int)TerminalFieldIndex.MaxDaysLogHistory, value); }
		}

		/// <summary>The POSConnectorType property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."POSConnectorType".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Crave.Enums.POSConnectorType POSConnectorType
		{
			get { return (Crave.Enums.POSConnectorType)GetValue((int)TerminalFieldIndex.POSConnectorType, true); }
			set	{ SetValue((int)TerminalFieldIndex.POSConnectorType, value); }
		}

		/// <summary>The PMSConnectorType property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."PMSConnectorType".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Crave.Enums.PMSConnectorType PMSConnectorType
		{
			get { return (Crave.Enums.PMSConnectorType)GetValue((int)TerminalFieldIndex.PMSConnectorType, true); }
			set	{ SetValue((int)TerminalFieldIndex.PMSConnectorType, value); }
		}

		/// <summary>The SynchronizeWithPOSOnStart property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."SynchronizeWithPOSOnStart".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean SynchronizeWithPOSOnStart
		{
			get { return (System.Boolean)GetValue((int)TerminalFieldIndex.SynchronizeWithPOSOnStart, true); }
			set	{ SetValue((int)TerminalFieldIndex.SynchronizeWithPOSOnStart, value); }
		}

		/// <summary>The MaxStatusReports property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."MaxStatusReports".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 MaxStatusReports
		{
			get { return (System.Int32)GetValue((int)TerminalFieldIndex.MaxStatusReports, true); }
			set	{ SetValue((int)TerminalFieldIndex.MaxStatusReports, value); }
		}

		/// <summary>The Active property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."Active".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean Active
		{
			get { return (System.Boolean)GetValue((int)TerminalFieldIndex.Active, true); }
			set	{ SetValue((int)TerminalFieldIndex.Active, value); }
		}

		/// <summary>The LanguageCode property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."LanguageCode".<br/>Table field type characteristics (type, precision, scale, length): Char, 0, 0, 2.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LanguageCode
		{
			get { return (System.String)GetValue((int)TerminalFieldIndex.LanguageCode, true); }
			set	{ SetValue((int)TerminalFieldIndex.LanguageCode, value); }
		}

		/// <summary>The UnlockDeliverypointProductId property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."UnlockDeliverypointProductId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UnlockDeliverypointProductId
		{
			get { return (Nullable<System.Int32>)GetValue((int)TerminalFieldIndex.UnlockDeliverypointProductId, false); }
			set	{ SetValue((int)TerminalFieldIndex.UnlockDeliverypointProductId, value); }
		}

		/// <summary>The BatteryLowProductId property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."BatteryLowProductId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> BatteryLowProductId
		{
			get { return (Nullable<System.Int32>)GetValue((int)TerminalFieldIndex.BatteryLowProductId, false); }
			set	{ SetValue((int)TerminalFieldIndex.BatteryLowProductId, value); }
		}

		/// <summary>The ClientDisconnectedProductId property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."ClientDisconnectedProductId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ClientDisconnectedProductId
		{
			get { return (Nullable<System.Int32>)GetValue((int)TerminalFieldIndex.ClientDisconnectedProductId, false); }
			set	{ SetValue((int)TerminalFieldIndex.ClientDisconnectedProductId, value); }
		}

		/// <summary>The OrderFailedProductId property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."OrderFailedProductId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> OrderFailedProductId
		{
			get { return (Nullable<System.Int32>)GetValue((int)TerminalFieldIndex.OrderFailedProductId, false); }
			set	{ SetValue((int)TerminalFieldIndex.OrderFailedProductId, value); }
		}

		/// <summary>The SystemMessagesDeliverypointId property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."SystemMessagesDeliverypointId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> SystemMessagesDeliverypointId
		{
			get { return (Nullable<System.Int32>)GetValue((int)TerminalFieldIndex.SystemMessagesDeliverypointId, false); }
			set	{ SetValue((int)TerminalFieldIndex.SystemMessagesDeliverypointId, value); }
		}

		/// <summary>The AltSystemMessagesDeliverypointId property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."AltSystemMessagesDeliverypointId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> AltSystemMessagesDeliverypointId
		{
			get { return (Nullable<System.Int32>)GetValue((int)TerminalFieldIndex.AltSystemMessagesDeliverypointId, false); }
			set	{ SetValue((int)TerminalFieldIndex.AltSystemMessagesDeliverypointId, value); }
		}

		/// <summary>The IcrtouchprintermappingId property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."IcrtouchprintermappingId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> IcrtouchprintermappingId
		{
			get { return (Nullable<System.Int32>)GetValue((int)TerminalFieldIndex.IcrtouchprintermappingId, false); }
			set	{ SetValue((int)TerminalFieldIndex.IcrtouchprintermappingId, value); }
		}

		/// <summary>The OperationMode property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."OperationMode".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Crave.Enums.TerminalOperationMode OperationMode
		{
			get { return (Crave.Enums.TerminalOperationMode)GetValue((int)TerminalFieldIndex.OperationMode, true); }
			set	{ SetValue((int)TerminalFieldIndex.OperationMode, value); }
		}

		/// <summary>The OperationState property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."OperationState".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 OperationState
		{
			get { return (System.Int32)GetValue((int)TerminalFieldIndex.OperationState, true); }
			set	{ SetValue((int)TerminalFieldIndex.OperationState, value); }
		}

		/// <summary>The HandlingMethod property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."HandlingMethod".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 HandlingMethod
		{
			get { return (System.Int32)GetValue((int)TerminalFieldIndex.HandlingMethod, true); }
			set	{ SetValue((int)TerminalFieldIndex.HandlingMethod, value); }
		}

		/// <summary>The NotificationForNewOrder property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."NotificationForNewOrder".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 NotificationForNewOrder
		{
			get { return (System.Int32)GetValue((int)TerminalFieldIndex.NotificationForNewOrder, true); }
			set	{ SetValue((int)TerminalFieldIndex.NotificationForNewOrder, value); }
		}

		/// <summary>The NotificationForOverdueOrder property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."NotificationForOverdueOrder".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 NotificationForOverdueOrder
		{
			get { return (System.Int32)GetValue((int)TerminalFieldIndex.NotificationForOverdueOrder, true); }
			set	{ SetValue((int)TerminalFieldIndex.NotificationForOverdueOrder, value); }
		}

		/// <summary>The DeliverypointCaption property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."DeliverypointCaption".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String DeliverypointCaption
		{
			get { return (System.String)GetValue((int)TerminalFieldIndex.DeliverypointCaption, true); }
			set	{ SetValue((int)TerminalFieldIndex.DeliverypointCaption, value); }
		}

		/// <summary>The DeliverypointgroupId property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."DeliverypointgroupId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> DeliverypointgroupId
		{
			get { return (Nullable<System.Int32>)GetValue((int)TerminalFieldIndex.DeliverypointgroupId, false); }
			set	{ SetValue((int)TerminalFieldIndex.DeliverypointgroupId, value); }
		}

		/// <summary>The ForwardToTerminalId property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."ForwardToTerminalId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ForwardToTerminalId
		{
			get { return (Nullable<System.Int32>)GetValue((int)TerminalFieldIndex.ForwardToTerminalId, false); }
			set	{ SetValue((int)TerminalFieldIndex.ForwardToTerminalId, value); }
		}

		/// <summary>The PosValue1 property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."PosValue1".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PosValue1
		{
			get { return (System.String)GetValue((int)TerminalFieldIndex.PosValue1, true); }
			set	{ SetValue((int)TerminalFieldIndex.PosValue1, value); }
		}

		/// <summary>The PosValue2 property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."PosValue2".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PosValue2
		{
			get { return (System.String)GetValue((int)TerminalFieldIndex.PosValue2, true); }
			set	{ SetValue((int)TerminalFieldIndex.PosValue2, value); }
		}

		/// <summary>The PosValue3 property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."PosValue3".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PosValue3
		{
			get { return (System.String)GetValue((int)TerminalFieldIndex.PosValue3, true); }
			set	{ SetValue((int)TerminalFieldIndex.PosValue3, value); }
		}

		/// <summary>The PosValue4 property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."PosValue4".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PosValue4
		{
			get { return (System.String)GetValue((int)TerminalFieldIndex.PosValue4, true); }
			set	{ SetValue((int)TerminalFieldIndex.PosValue4, value); }
		}

		/// <summary>The PosValue5 property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."PosValue5".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PosValue5
		{
			get { return (System.String)GetValue((int)TerminalFieldIndex.PosValue5, true); }
			set	{ SetValue((int)TerminalFieldIndex.PosValue5, value); }
		}

		/// <summary>The PosValue6 property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."PosValue6".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PosValue6
		{
			get { return (System.String)GetValue((int)TerminalFieldIndex.PosValue6, true); }
			set	{ SetValue((int)TerminalFieldIndex.PosValue6, value); }
		}

		/// <summary>The PosValue7 property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."PosValue7".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PosValue7
		{
			get { return (System.String)GetValue((int)TerminalFieldIndex.PosValue7, true); }
			set	{ SetValue((int)TerminalFieldIndex.PosValue7, value); }
		}

		/// <summary>The PosValue8 property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."PosValue8".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PosValue8
		{
			get { return (System.String)GetValue((int)TerminalFieldIndex.PosValue8, true); }
			set	{ SetValue((int)TerminalFieldIndex.PosValue8, value); }
		}

		/// <summary>The PosValue9 property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."PosValue9".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PosValue9
		{
			get { return (System.String)GetValue((int)TerminalFieldIndex.PosValue9, true); }
			set	{ SetValue((int)TerminalFieldIndex.PosValue9, value); }
		}

		/// <summary>The PosValue10 property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."PosValue10".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PosValue10
		{
			get { return (System.String)GetValue((int)TerminalFieldIndex.PosValue10, true); }
			set	{ SetValue((int)TerminalFieldIndex.PosValue10, value); }
		}

		/// <summary>The PosValue11 property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."PosValue11".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PosValue11
		{
			get { return (System.String)GetValue((int)TerminalFieldIndex.PosValue11, true); }
			set	{ SetValue((int)TerminalFieldIndex.PosValue11, value); }
		}

		/// <summary>The PosValue12 property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."PosValue12".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PosValue12
		{
			get { return (System.String)GetValue((int)TerminalFieldIndex.PosValue12, true); }
			set	{ SetValue((int)TerminalFieldIndex.PosValue12, value); }
		}

		/// <summary>The PosValue13 property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."PosValue13".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PosValue13
		{
			get { return (System.String)GetValue((int)TerminalFieldIndex.PosValue13, true); }
			set	{ SetValue((int)TerminalFieldIndex.PosValue13, value); }
		}

		/// <summary>The PosValue14 property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."PosValue14".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PosValue14
		{
			get { return (System.String)GetValue((int)TerminalFieldIndex.PosValue14, true); }
			set	{ SetValue((int)TerminalFieldIndex.PosValue14, value); }
		}

		/// <summary>The PosValue15 property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."PosValue15".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PosValue15
		{
			get { return (System.String)GetValue((int)TerminalFieldIndex.PosValue15, true); }
			set	{ SetValue((int)TerminalFieldIndex.PosValue15, value); }
		}

		/// <summary>The PosValue16 property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."PosValue16".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PosValue16
		{
			get { return (System.String)GetValue((int)TerminalFieldIndex.PosValue16, true); }
			set	{ SetValue((int)TerminalFieldIndex.PosValue16, value); }
		}

		/// <summary>The PosValue17 property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."PosValue17".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PosValue17
		{
			get { return (System.String)GetValue((int)TerminalFieldIndex.PosValue17, true); }
			set	{ SetValue((int)TerminalFieldIndex.PosValue17, value); }
		}

		/// <summary>The PosValue18 property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."PosValue18".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PosValue18
		{
			get { return (System.String)GetValue((int)TerminalFieldIndex.PosValue18, true); }
			set	{ SetValue((int)TerminalFieldIndex.PosValue18, value); }
		}

		/// <summary>The PosValue19 property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."PosValue19".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PosValue19
		{
			get { return (System.String)GetValue((int)TerminalFieldIndex.PosValue19, true); }
			set	{ SetValue((int)TerminalFieldIndex.PosValue19, value); }
		}

		/// <summary>The PosValue20 property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."PosValue20".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PosValue20
		{
			get { return (System.String)GetValue((int)TerminalFieldIndex.PosValue20, true); }
			set	{ SetValue((int)TerminalFieldIndex.PosValue20, value); }
		}

		/// <summary>The DeviceId property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."DeviceId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> DeviceId
		{
			get { return (Nullable<System.Int32>)GetValue((int)TerminalFieldIndex.DeviceId, false); }
			set	{ SetValue((int)TerminalFieldIndex.DeviceId, value); }
		}

		/// <summary>The AutomaticSignOnUserId property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."AutomaticSignOnUserId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> AutomaticSignOnUserId
		{
			get { return (Nullable<System.Int32>)GetValue((int)TerminalFieldIndex.AutomaticSignOnUserId, false); }
			set	{ SetValue((int)TerminalFieldIndex.AutomaticSignOnUserId, value); }
		}

		/// <summary>The SurveyResultNotifications property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."SurveyResultNotifications".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean SurveyResultNotifications
		{
			get { return (System.Boolean)GetValue((int)TerminalFieldIndex.SurveyResultNotifications, true); }
			set	{ SetValue((int)TerminalFieldIndex.SurveyResultNotifications, value); }
		}

		/// <summary>The LastCommunicationMethod property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."LastCommunicationMethod".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 LastCommunicationMethod
		{
			get { return (System.Int32)GetValue((int)TerminalFieldIndex.LastCommunicationMethod, true); }
			set	{ SetValue((int)TerminalFieldIndex.LastCommunicationMethod, value); }
		}

		/// <summary>The LastCloudEnvironment property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."LastCloudEnvironment".<br/>Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 50.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastCloudEnvironment
		{
			get { return (System.String)GetValue((int)TerminalFieldIndex.LastCloudEnvironment, true); }
			set	{ SetValue((int)TerminalFieldIndex.LastCloudEnvironment, value); }
		}

		/// <summary>The LastStateOnline property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."LastStateOnline".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> LastStateOnline
		{
			get { return (Nullable<System.Boolean>)GetValue((int)TerminalFieldIndex.LastStateOnline, false); }
			set	{ SetValue((int)TerminalFieldIndex.LastStateOnline, value); }
		}

		/// <summary>The LastStateOperationMode property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."LastStateOperationMode".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<Crave.Enums.TerminalOperationMode> LastStateOperationMode
		{
			get { return (Nullable<Crave.Enums.TerminalOperationMode>)GetValue((int)TerminalFieldIndex.LastStateOperationMode, false); }
			set	{ SetValue((int)TerminalFieldIndex.LastStateOperationMode, value); }
		}

		/// <summary>The UseMonitoring property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."UseMonitoring".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean UseMonitoring
		{
			get { return (System.Boolean)GetValue((int)TerminalFieldIndex.UseMonitoring, true); }
			set	{ SetValue((int)TerminalFieldIndex.UseMonitoring, value); }
		}

		/// <summary>The MasterTab property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."MasterTab".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean MasterTab
		{
			get { return (System.Boolean)GetValue((int)TerminalFieldIndex.MasterTab, true); }
			set	{ SetValue((int)TerminalFieldIndex.MasterTab, value); }
		}

		/// <summary>The ResetTimeout property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."ResetTimeout".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ResetTimeout
		{
			get { return (System.Int32)GetValue((int)TerminalFieldIndex.ResetTimeout, true); }
			set	{ SetValue((int)TerminalFieldIndex.ResetTimeout, value); }
		}

		/// <summary>The PrinterConnected property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."PrinterConnected".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean PrinterConnected
		{
			get { return (System.Boolean)GetValue((int)TerminalFieldIndex.PrinterConnected, true); }
			set	{ SetValue((int)TerminalFieldIndex.PrinterConnected, value); }
		}

		/// <summary>The OfflineNotificationEnabled property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."OfflineNotificationEnabled".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean OfflineNotificationEnabled
		{
			get { return (System.Boolean)GetValue((int)TerminalFieldIndex.OfflineNotificationEnabled, true); }
			set	{ SetValue((int)TerminalFieldIndex.OfflineNotificationEnabled, value); }
		}

		/// <summary>The LoadedSuccessfully property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."LoadedSuccessfully".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean LoadedSuccessfully
		{
			get { return (System.Boolean)GetValue((int)TerminalFieldIndex.LoadedSuccessfully, true); }
			set	{ SetValue((int)TerminalFieldIndex.LoadedSuccessfully, value); }
		}

		/// <summary>The SynchronizeWithPMSOnStart property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."SynchronizeWithPMSOnStart".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean SynchronizeWithPMSOnStart
		{
			get { return (System.Boolean)GetValue((int)TerminalFieldIndex.SynchronizeWithPMSOnStart, true); }
			set	{ SetValue((int)TerminalFieldIndex.SynchronizeWithPMSOnStart, value); }
		}

		/// <summary>The StatusUpdatedUTC property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."StatusUpdatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> StatusUpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TerminalFieldIndex.StatusUpdatedUTC, false); }
			set	{ SetValue((int)TerminalFieldIndex.StatusUpdatedUTC, value); }
		}

		/// <summary>The LastSyncUTC property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."LastSyncUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> LastSyncUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TerminalFieldIndex.LastSyncUTC, false); }
			set	{ SetValue((int)TerminalFieldIndex.LastSyncUTC, value); }
		}

		/// <summary>The PmsTerminalOfflineNotificationEnabled property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."PmsTerminalOfflineNotificationEnabled".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean PmsTerminalOfflineNotificationEnabled
		{
			get { return (System.Boolean)GetValue((int)TerminalFieldIndex.PmsTerminalOfflineNotificationEnabled, true); }
			set	{ SetValue((int)TerminalFieldIndex.PmsTerminalOfflineNotificationEnabled, value); }
		}

		/// <summary>The CreatedBy property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."CreatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)TerminalFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)TerminalFieldIndex.CreatedBy, value); }
		}

		/// <summary>The UpdatedBy property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."UpdatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)TerminalFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)TerminalFieldIndex.UpdatedBy, value); }
		}

		/// <summary>The CreatedUTC property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."CreatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TerminalFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)TerminalFieldIndex.CreatedUTC, value); }
		}

		/// <summary>The UpdatedUTC property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."UpdatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TerminalFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)TerminalFieldIndex.UpdatedUTC, value); }
		}

		/// <summary>The OutOfChargeNotificationLastSentUTC property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."OutOfChargeNotificationLastSentUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> OutOfChargeNotificationLastSentUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TerminalFieldIndex.OutOfChargeNotificationLastSentUTC, false); }
			set	{ SetValue((int)TerminalFieldIndex.OutOfChargeNotificationLastSentUTC, value); }
		}

		/// <summary>The UseHardKeyboard property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."UseHardKeyboard".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UseHardKeyboard
		{
			get { return (System.Int32)GetValue((int)TerminalFieldIndex.UseHardKeyboard, true); }
			set	{ SetValue((int)TerminalFieldIndex.UseHardKeyboard, value); }
		}

		/// <summary>The MaxVolume property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."MaxVolume".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 MaxVolume
		{
			get { return (System.Int32)GetValue((int)TerminalFieldIndex.MaxVolume, true); }
			set	{ SetValue((int)TerminalFieldIndex.MaxVolume, value); }
		}

		/// <summary>The MaxProcessTime property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."MaxProcessTime".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 MaxProcessTime
		{
			get { return (System.Int32)GetValue((int)TerminalFieldIndex.MaxProcessTime, true); }
			set	{ SetValue((int)TerminalFieldIndex.MaxProcessTime, value); }
		}

		/// <summary>Gets the EntityCollection with the related entities of type 'DeliverypointgroupEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(DeliverypointgroupEntity))]
		public virtual EntityCollection<DeliverypointgroupEntity> DeliverypointgroupCollection { get { return GetOrCreateEntityCollection<DeliverypointgroupEntity, DeliverypointgroupEntityFactory>("Terminal", true, false, ref _deliverypointgroupCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'NetmessageEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(NetmessageEntity))]
		public virtual EntityCollection<NetmessageEntity> SentNetmessageCollection { get { return GetOrCreateEntityCollection<NetmessageEntity, NetmessageEntityFactory>("Terminal", true, false, ref _sentNetmessageCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'NetmessageEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(NetmessageEntity))]
		public virtual EntityCollection<NetmessageEntity> ReceivedNetmessageCollection { get { return GetOrCreateEntityCollection<NetmessageEntity, NetmessageEntityFactory>("TerminalEntity1", true, false, ref _receivedNetmessageCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'OrderRoutestephandlerEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(OrderRoutestephandlerEntity))]
		public virtual EntityCollection<OrderRoutestephandlerEntity> OrderRoutestephandlerCollection { get { return GetOrCreateEntityCollection<OrderRoutestephandlerEntity, OrderRoutestephandlerEntityFactory>("Terminal", true, false, ref _orderRoutestephandlerCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'OrderRoutestephandlerEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(OrderRoutestephandlerEntity))]
		public virtual EntityCollection<OrderRoutestephandlerEntity> OrderRoutestephandlerCollection1 { get { return GetOrCreateEntityCollection<OrderRoutestephandlerEntity, OrderRoutestephandlerEntityFactory>("TerminalEntity1", true, false, ref _orderRoutestephandlerCollection1); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'OrderRoutestephandlerHistoryEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(OrderRoutestephandlerHistoryEntity))]
		public virtual EntityCollection<OrderRoutestephandlerHistoryEntity> OrderRoutestephandlerHistoryCollection { get { return GetOrCreateEntityCollection<OrderRoutestephandlerHistoryEntity, OrderRoutestephandlerHistoryEntityFactory>("Terminal", true, false, ref _orderRoutestephandlerHistoryCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'OrderRoutestephandlerHistoryEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(OrderRoutestephandlerHistoryEntity))]
		public virtual EntityCollection<OrderRoutestephandlerHistoryEntity> OrderRoutestephandlerHistoryCollection1 { get { return GetOrCreateEntityCollection<OrderRoutestephandlerHistoryEntity, OrderRoutestephandlerHistoryEntityFactory>("TerminalEntity1", true, false, ref _orderRoutestephandlerHistoryCollection1); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'RoutestephandlerEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(RoutestephandlerEntity))]
		public virtual EntityCollection<RoutestephandlerEntity> RoutestephandlerCollection { get { return GetOrCreateEntityCollection<RoutestephandlerEntity, RoutestephandlerEntityFactory>("Terminal", true, false, ref _routestephandlerCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'TerminalEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(TerminalEntity))]
		public virtual EntityCollection<TerminalEntity> TerminalCollection { get { return GetOrCreateEntityCollection<TerminalEntity, TerminalEntityFactory>("ForwardToTerminal", true, false, ref _terminalCollection); } }

		/// <summary>Gets / sets related entity of type 'CompanyEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual CompanyEntity Company
		{
			get { return _company; }
			set { SetSingleRelatedEntityNavigator(value, "Company"); }
		}

		/// <summary>Gets / sets related entity of type 'DeliverypointEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual DeliverypointEntity AltSystemMessagesDeliverypoint
		{
			get { return _altSystemMessagesDeliverypoint; }
			set { SetSingleRelatedEntityNavigator(value, "AltSystemMessagesDeliverypoint"); }
		}

		/// <summary>Gets / sets related entity of type 'DeliverypointEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual DeliverypointEntity SystemMessagesDeliverypoint
		{
			get { return _systemMessagesDeliverypoint; }
			set { SetSingleRelatedEntityNavigator(value, "SystemMessagesDeliverypoint"); }
		}

		/// <summary>Gets / sets related entity of type 'DeliverypointgroupEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual DeliverypointgroupEntity Deliverypointgroup
		{
			get { return _deliverypointgroup; }
			set { SetSingleRelatedEntityNavigator(value, "Deliverypointgroup"); }
		}

		/// <summary>Gets / sets related entity of type 'DeviceEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual DeviceEntity Device
		{
			get { return _device; }
			set { SetSingleRelatedEntityNavigator(value, "Device"); }
		}

		/// <summary>Gets / sets related entity of type 'ProductEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual ProductEntity BatteryLowProduct
		{
			get { return _batteryLowProduct; }
			set { SetSingleRelatedEntityNavigator(value, "BatteryLowProduct"); }
		}

		/// <summary>Gets / sets related entity of type 'ProductEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual ProductEntity ClientDisconnectedProduct
		{
			get { return _clientDisconnectedProduct; }
			set { SetSingleRelatedEntityNavigator(value, "ClientDisconnectedProduct"); }
		}

		/// <summary>Gets / sets related entity of type 'ProductEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual ProductEntity OrderFailedProduct
		{
			get { return _orderFailedProduct; }
			set { SetSingleRelatedEntityNavigator(value, "OrderFailedProduct"); }
		}

		/// <summary>Gets / sets related entity of type 'ProductEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual ProductEntity UnlockDeliverypointProduct
		{
			get { return _unlockDeliverypointProduct; }
			set { SetSingleRelatedEntityNavigator(value, "UnlockDeliverypointProduct"); }
		}

		/// <summary>Gets / sets related entity of type 'TerminalEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual TerminalEntity ForwardToTerminal
		{
			get { return _forwardToTerminal; }
			set { SetSingleRelatedEntityNavigator(value, "ForwardToTerminal"); }
		}


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen
{
	public enum TerminalFieldIndex
	{
		///<summary>TerminalId. </summary>
		TerminalId,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>UIModeId. </summary>
		UIModeId,
		///<summary>Type. </summary>
		Type,
		///<summary>OnsiteServerDeliverypointgroupId. </summary>
		OnsiteServerDeliverypointgroupId,
		///<summary>Name. </summary>
		Name,
		///<summary>LastStatus. </summary>
		LastStatus,
		///<summary>LastStatusText. </summary>
		LastStatusText,
		///<summary>LastStatusMessage. </summary>
		LastStatusMessage,
		///<summary>OutOfChargeNotificationsSent. </summary>
		OutOfChargeNotificationsSent,
		///<summary>RequestInterval. </summary>
		RequestInterval,
		///<summary>DiagnoseInterval. </summary>
		DiagnoseInterval,
		///<summary>PrintingEnabled. </summary>
		PrintingEnabled,
		///<summary>PrinterName. </summary>
		PrinterName,
		///<summary>RecordAllRequestsToRequestLog. </summary>
		RecordAllRequestsToRequestLog,
		///<summary>TeamviewerId. </summary>
		TeamviewerId,
		///<summary>MaxDaysLogHistory. </summary>
		MaxDaysLogHistory,
		///<summary>POSConnectorType. </summary>
		POSConnectorType,
		///<summary>PMSConnectorType. </summary>
		PMSConnectorType,
		///<summary>SynchronizeWithPOSOnStart. </summary>
		SynchronizeWithPOSOnStart,
		///<summary>MaxStatusReports. </summary>
		MaxStatusReports,
		///<summary>Active. </summary>
		Active,
		///<summary>LanguageCode. </summary>
		LanguageCode,
		///<summary>UnlockDeliverypointProductId. </summary>
		UnlockDeliverypointProductId,
		///<summary>BatteryLowProductId. </summary>
		BatteryLowProductId,
		///<summary>ClientDisconnectedProductId. </summary>
		ClientDisconnectedProductId,
		///<summary>OrderFailedProductId. </summary>
		OrderFailedProductId,
		///<summary>SystemMessagesDeliverypointId. </summary>
		SystemMessagesDeliverypointId,
		///<summary>AltSystemMessagesDeliverypointId. </summary>
		AltSystemMessagesDeliverypointId,
		///<summary>IcrtouchprintermappingId. </summary>
		IcrtouchprintermappingId,
		///<summary>OperationMode. </summary>
		OperationMode,
		///<summary>OperationState. </summary>
		OperationState,
		///<summary>HandlingMethod. </summary>
		HandlingMethod,
		///<summary>NotificationForNewOrder. </summary>
		NotificationForNewOrder,
		///<summary>NotificationForOverdueOrder. </summary>
		NotificationForOverdueOrder,
		///<summary>DeliverypointCaption. </summary>
		DeliverypointCaption,
		///<summary>DeliverypointgroupId. </summary>
		DeliverypointgroupId,
		///<summary>ForwardToTerminalId. </summary>
		ForwardToTerminalId,
		///<summary>PosValue1. </summary>
		PosValue1,
		///<summary>PosValue2. </summary>
		PosValue2,
		///<summary>PosValue3. </summary>
		PosValue3,
		///<summary>PosValue4. </summary>
		PosValue4,
		///<summary>PosValue5. </summary>
		PosValue5,
		///<summary>PosValue6. </summary>
		PosValue6,
		///<summary>PosValue7. </summary>
		PosValue7,
		///<summary>PosValue8. </summary>
		PosValue8,
		///<summary>PosValue9. </summary>
		PosValue9,
		///<summary>PosValue10. </summary>
		PosValue10,
		///<summary>PosValue11. </summary>
		PosValue11,
		///<summary>PosValue12. </summary>
		PosValue12,
		///<summary>PosValue13. </summary>
		PosValue13,
		///<summary>PosValue14. </summary>
		PosValue14,
		///<summary>PosValue15. </summary>
		PosValue15,
		///<summary>PosValue16. </summary>
		PosValue16,
		///<summary>PosValue17. </summary>
		PosValue17,
		///<summary>PosValue18. </summary>
		PosValue18,
		///<summary>PosValue19. </summary>
		PosValue19,
		///<summary>PosValue20. </summary>
		PosValue20,
		///<summary>DeviceId. </summary>
		DeviceId,
		///<summary>AutomaticSignOnUserId. </summary>
		AutomaticSignOnUserId,
		///<summary>SurveyResultNotifications. </summary>
		SurveyResultNotifications,
		///<summary>LastCommunicationMethod. </summary>
		LastCommunicationMethod,
		///<summary>LastCloudEnvironment. </summary>
		LastCloudEnvironment,
		///<summary>LastStateOnline. </summary>
		LastStateOnline,
		///<summary>LastStateOperationMode. </summary>
		LastStateOperationMode,
		///<summary>UseMonitoring. </summary>
		UseMonitoring,
		///<summary>MasterTab. </summary>
		MasterTab,
		///<summary>ResetTimeout. </summary>
		ResetTimeout,
		///<summary>PrinterConnected. </summary>
		PrinterConnected,
		///<summary>OfflineNotificationEnabled. </summary>
		OfflineNotificationEnabled,
		///<summary>LoadedSuccessfully. </summary>
		LoadedSuccessfully,
		///<summary>SynchronizeWithPMSOnStart. </summary>
		SynchronizeWithPMSOnStart,
		///<summary>StatusUpdatedUTC. </summary>
		StatusUpdatedUTC,
		///<summary>LastSyncUTC. </summary>
		LastSyncUTC,
		///<summary>PmsTerminalOfflineNotificationEnabled. </summary>
		PmsTerminalOfflineNotificationEnabled,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>OutOfChargeNotificationLastSentUTC. </summary>
		OutOfChargeNotificationLastSentUTC,
		///<summary>UseHardKeyboard. </summary>
		UseHardKeyboard,
		///<summary>MaxVolume. </summary>
		MaxVolume,
		///<summary>MaxProcessTime. </summary>
		MaxProcessTime,
		/// <summary></summary>
		AmountOfFields
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Terminal. </summary>
	public partial class TerminalRelations: RelationFactory
	{
		#region Custom Relation code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomRelationCode
		private const ReferentialConstraintDeleteRule DeleteRuleForDeliverypointgroupEntityUsingTerminalId = ReferentialConstraintDeleteRule.NoAction;
		private const ReferentialConstraintDeleteRule DeleteRuleForNetmessageEntityUsingSenderTerminalId = ReferentialConstraintDeleteRule.Cascade;
		private const ReferentialConstraintDeleteRule DeleteRuleForNetmessageEntityUsingReceiverTerminalId = ReferentialConstraintDeleteRule.Cascade;
		private const ReferentialConstraintDeleteRule DeleteRuleForOrderRoutestephandlerEntityUsingTerminalId = ReferentialConstraintDeleteRule.NoAction;
		private const ReferentialConstraintDeleteRule DeleteRuleForOrderRoutestephandlerEntityUsingForwardedFromTerminalId = ReferentialConstraintDeleteRule.NoAction;
		private const ReferentialConstraintDeleteRule DeleteRuleForOrderRoutestephandlerHistoryEntityUsingTerminalId = ReferentialConstraintDeleteRule.SetNull;
		private const ReferentialConstraintDeleteRule DeleteRuleForOrderRoutestephandlerHistoryEntityUsingForwardedFromTerminalId = ReferentialConstraintDeleteRule.SetNull;
		private const ReferentialConstraintDeleteRule DeleteRuleForRoutestephandlerEntityUsingTerminalId = ReferentialConstraintDeleteRule.NoAction;
		private const ReferentialConstraintDeleteRule DeleteRuleForTerminalEntityUsingForwardToTerminalId = ReferentialConstraintDeleteRule.SetNull;
        // __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		
		// private const ReferentialConstraintDeleteRule DeleteRuleForDeliverypointgroupEntityUsingTerminalId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForNetmessageEntityUsingSenderTerminalId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForNetmessageEntityUsingReceiverTerminalId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForOrderRoutestephandlerEntityUsingTerminalId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForOrderRoutestephandlerEntityUsingForwardedFromTerminalId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForOrderRoutestephandlerHistoryEntityUsingTerminalId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForOrderRoutestephandlerHistoryEntityUsingForwardedFromTerminalId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForRoutestephandlerEntityUsingTerminalId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForTerminalEntityUsingForwardToTerminalId = ReferentialConstraintDeleteRule


		/// <summary>Gets all relations and delete rules of the TerminalEntity as a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects.</summary>
		/// <returns>a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects</returns>
		public virtual Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> GetRelationsAndDeleteRules()
		{
			Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> toReturn = new Dictionary<IEntityRelation, ReferentialConstraintDeleteRule>();
			toReturn.Add(this.DeliverypointgroupEntityUsingTerminalId, TerminalRelations.DeleteRuleForDeliverypointgroupEntityUsingTerminalId);
			toReturn.Add(this.NetmessageEntityUsingSenderTerminalId, TerminalRelations.DeleteRuleForNetmessageEntityUsingSenderTerminalId);
			toReturn.Add(this.NetmessageEntityUsingReceiverTerminalId, TerminalRelations.DeleteRuleForNetmessageEntityUsingReceiverTerminalId);
			toReturn.Add(this.OrderRoutestephandlerEntityUsingTerminalId, TerminalRelations.DeleteRuleForOrderRoutestephandlerEntityUsingTerminalId);
			toReturn.Add(this.OrderRoutestephandlerEntityUsingForwardedFromTerminalId, TerminalRelations.DeleteRuleForOrderRoutestephandlerEntityUsingForwardedFromTerminalId);
			toReturn.Add(this.OrderRoutestephandlerHistoryEntityUsingTerminalId, TerminalRelations.DeleteRuleForOrderRoutestephandlerHistoryEntityUsingTerminalId);
			toReturn.Add(this.OrderRoutestephandlerHistoryEntityUsingForwardedFromTerminalId, TerminalRelations.DeleteRuleForOrderRoutestephandlerHistoryEntityUsingForwardedFromTerminalId);
			toReturn.Add(this.RoutestephandlerEntityUsingTerminalId, TerminalRelations.DeleteRuleForRoutestephandlerEntityUsingTerminalId);
			toReturn.Add(this.TerminalEntityUsingForwardToTerminalId, TerminalRelations.DeleteRuleForTerminalEntityUsingForwardToTerminalId);
			return toReturn;
		}		
	
	
		/// <summary>Returns a new IEntityRelation object, between TerminalEntity and DeliverypointgroupEntity over the 1:n relation they have, using the relation between the fields: Terminal.TerminalId - Deliverypointgroup.TerminalId</summary>
		public virtual IEntityRelation DeliverypointgroupEntityUsingTerminalId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "DeliverypointgroupCollection", true, new[] { TerminalFields.TerminalId, DeliverypointgroupFields.TerminalId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between TerminalEntity and NetmessageEntity over the 1:n relation they have, using the relation between the fields: Terminal.TerminalId - Netmessage.SenderTerminalId</summary>
		public virtual IEntityRelation NetmessageEntityUsingSenderTerminalId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "SentNetmessageCollection", true, new[] { TerminalFields.TerminalId, NetmessageFields.SenderTerminalId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between TerminalEntity and NetmessageEntity over the 1:n relation they have, using the relation between the fields: Terminal.TerminalId - Netmessage.ReceiverTerminalId</summary>
		public virtual IEntityRelation NetmessageEntityUsingReceiverTerminalId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "ReceivedNetmessageCollection", true, new[] { TerminalFields.TerminalId, NetmessageFields.ReceiverTerminalId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between TerminalEntity and OrderRoutestephandlerEntity over the 1:n relation they have, using the relation between the fields: Terminal.TerminalId - OrderRoutestephandler.TerminalId</summary>
		public virtual IEntityRelation OrderRoutestephandlerEntityUsingTerminalId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "OrderRoutestephandlerCollection", true, new[] { TerminalFields.TerminalId, OrderRoutestephandlerFields.TerminalId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between TerminalEntity and OrderRoutestephandlerEntity over the 1:n relation they have, using the relation between the fields: Terminal.TerminalId - OrderRoutestephandler.ForwardedFromTerminalId</summary>
		public virtual IEntityRelation OrderRoutestephandlerEntityUsingForwardedFromTerminalId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "OrderRoutestephandlerCollection1", true, new[] { TerminalFields.TerminalId, OrderRoutestephandlerFields.ForwardedFromTerminalId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between TerminalEntity and OrderRoutestephandlerHistoryEntity over the 1:n relation they have, using the relation between the fields: Terminal.TerminalId - OrderRoutestephandlerHistory.TerminalId</summary>
		public virtual IEntityRelation OrderRoutestephandlerHistoryEntityUsingTerminalId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "OrderRoutestephandlerHistoryCollection", true, new[] { TerminalFields.TerminalId, OrderRoutestephandlerHistoryFields.TerminalId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between TerminalEntity and OrderRoutestephandlerHistoryEntity over the 1:n relation they have, using the relation between the fields: Terminal.TerminalId - OrderRoutestephandlerHistory.ForwardedFromTerminalId</summary>
		public virtual IEntityRelation OrderRoutestephandlerHistoryEntityUsingForwardedFromTerminalId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "OrderRoutestephandlerHistoryCollection1", true, new[] { TerminalFields.TerminalId, OrderRoutestephandlerHistoryFields.ForwardedFromTerminalId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between TerminalEntity and RoutestephandlerEntity over the 1:n relation they have, using the relation between the fields: Terminal.TerminalId - Routestephandler.TerminalId</summary>
		public virtual IEntityRelation RoutestephandlerEntityUsingTerminalId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "RoutestephandlerCollection", true, new[] { TerminalFields.TerminalId, RoutestephandlerFields.TerminalId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between TerminalEntity and TerminalEntity over the 1:n relation they have, using the relation between the fields: Terminal.TerminalId - Terminal.ForwardToTerminalId</summary>
		public virtual IEntityRelation TerminalEntityUsingForwardToTerminalId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "TerminalCollection", true, new[] { TerminalFields.TerminalId, TerminalFields.ForwardToTerminalId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between TerminalEntity and CompanyEntity over the m:1 relation they have, using the relation between the fields: Terminal.CompanyId - Company.CompanyId</summary>
		public virtual IEntityRelation CompanyEntityUsingCompanyId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Company", false, new[] { CompanyFields.CompanyId, TerminalFields.CompanyId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between TerminalEntity and DeliverypointEntity over the m:1 relation they have, using the relation between the fields: Terminal.AltSystemMessagesDeliverypointId - Deliverypoint.DeliverypointId</summary>
		public virtual IEntityRelation DeliverypointEntityUsingAltSystemMessagesDeliverypointId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "AltSystemMessagesDeliverypoint", false, new[] { DeliverypointFields.DeliverypointId, TerminalFields.AltSystemMessagesDeliverypointId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between TerminalEntity and DeliverypointEntity over the m:1 relation they have, using the relation between the fields: Terminal.SystemMessagesDeliverypointId - Deliverypoint.DeliverypointId</summary>
		public virtual IEntityRelation DeliverypointEntityUsingSystemMessagesDeliverypointId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "SystemMessagesDeliverypoint", false, new[] { DeliverypointFields.DeliverypointId, TerminalFields.SystemMessagesDeliverypointId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between TerminalEntity and DeliverypointgroupEntity over the m:1 relation they have, using the relation between the fields: Terminal.DeliverypointgroupId - Deliverypointgroup.DeliverypointgroupId</summary>
		public virtual IEntityRelation DeliverypointgroupEntityUsingDeliverypointgroupId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Deliverypointgroup", false, new[] { DeliverypointgroupFields.DeliverypointgroupId, TerminalFields.DeliverypointgroupId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between TerminalEntity and DeviceEntity over the m:1 relation they have, using the relation between the fields: Terminal.DeviceId - Device.DeviceId</summary>
		public virtual IEntityRelation DeviceEntityUsingDeviceId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Device", false, new[] { DeviceFields.DeviceId, TerminalFields.DeviceId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between TerminalEntity and ProductEntity over the m:1 relation they have, using the relation between the fields: Terminal.BatteryLowProductId - Product.ProductId</summary>
		public virtual IEntityRelation ProductEntityUsingBatteryLowProductId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "BatteryLowProduct", false, new[] { ProductFields.ProductId, TerminalFields.BatteryLowProductId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between TerminalEntity and ProductEntity over the m:1 relation they have, using the relation between the fields: Terminal.ClientDisconnectedProductId - Product.ProductId</summary>
		public virtual IEntityRelation ProductEntityUsingClientDisconnectedProductId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "ClientDisconnectedProduct", false, new[] { ProductFields.ProductId, TerminalFields.ClientDisconnectedProductId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between TerminalEntity and ProductEntity over the m:1 relation they have, using the relation between the fields: Terminal.OrderFailedProductId - Product.ProductId</summary>
		public virtual IEntityRelation ProductEntityUsingOrderFailedProductId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "OrderFailedProduct", false, new[] { ProductFields.ProductId, TerminalFields.OrderFailedProductId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between TerminalEntity and ProductEntity over the m:1 relation they have, using the relation between the fields: Terminal.UnlockDeliverypointProductId - Product.ProductId</summary>
		public virtual IEntityRelation ProductEntityUsingUnlockDeliverypointProductId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "UnlockDeliverypointProduct", false, new[] { ProductFields.ProductId, TerminalFields.UnlockDeliverypointProductId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between TerminalEntity and TerminalEntity over the m:1 relation they have, using the relation between the fields: Terminal.ForwardToTerminalId - Terminal.TerminalId</summary>
		public virtual IEntityRelation TerminalEntityUsingTerminalIdForwardToTerminalId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "ForwardToTerminal", false, new[] { TerminalFields.TerminalId, TerminalFields.ForwardToTerminalId }); }
		}

	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticTerminalRelations
	{
		internal static readonly IEntityRelation DeliverypointgroupEntityUsingTerminalIdStatic = new TerminalRelations().DeliverypointgroupEntityUsingTerminalId;
		internal static readonly IEntityRelation NetmessageEntityUsingSenderTerminalIdStatic = new TerminalRelations().NetmessageEntityUsingSenderTerminalId;
		internal static readonly IEntityRelation NetmessageEntityUsingReceiverTerminalIdStatic = new TerminalRelations().NetmessageEntityUsingReceiverTerminalId;
		internal static readonly IEntityRelation OrderRoutestephandlerEntityUsingTerminalIdStatic = new TerminalRelations().OrderRoutestephandlerEntityUsingTerminalId;
		internal static readonly IEntityRelation OrderRoutestephandlerEntityUsingForwardedFromTerminalIdStatic = new TerminalRelations().OrderRoutestephandlerEntityUsingForwardedFromTerminalId;
		internal static readonly IEntityRelation OrderRoutestephandlerHistoryEntityUsingTerminalIdStatic = new TerminalRelations().OrderRoutestephandlerHistoryEntityUsingTerminalId;
		internal static readonly IEntityRelation OrderRoutestephandlerHistoryEntityUsingForwardedFromTerminalIdStatic = new TerminalRelations().OrderRoutestephandlerHistoryEntityUsingForwardedFromTerminalId;
		internal static readonly IEntityRelation RoutestephandlerEntityUsingTerminalIdStatic = new TerminalRelations().RoutestephandlerEntityUsingTerminalId;
		internal static readonly IEntityRelation TerminalEntityUsingForwardToTerminalIdStatic = new TerminalRelations().TerminalEntityUsingForwardToTerminalId;
		internal static readonly IEntityRelation CompanyEntityUsingCompanyIdStatic = new TerminalRelations().CompanyEntityUsingCompanyId;
		internal static readonly IEntityRelation DeliverypointEntityUsingAltSystemMessagesDeliverypointIdStatic = new TerminalRelations().DeliverypointEntityUsingAltSystemMessagesDeliverypointId;
		internal static readonly IEntityRelation DeliverypointEntityUsingSystemMessagesDeliverypointIdStatic = new TerminalRelations().DeliverypointEntityUsingSystemMessagesDeliverypointId;
		internal static readonly IEntityRelation DeliverypointgroupEntityUsingDeliverypointgroupIdStatic = new TerminalRelations().DeliverypointgroupEntityUsingDeliverypointgroupId;
		internal static readonly IEntityRelation DeviceEntityUsingDeviceIdStatic = new TerminalRelations().DeviceEntityUsingDeviceId;
		internal static readonly IEntityRelation ProductEntityUsingBatteryLowProductIdStatic = new TerminalRelations().ProductEntityUsingBatteryLowProductId;
		internal static readonly IEntityRelation ProductEntityUsingClientDisconnectedProductIdStatic = new TerminalRelations().ProductEntityUsingClientDisconnectedProductId;
		internal static readonly IEntityRelation ProductEntityUsingOrderFailedProductIdStatic = new TerminalRelations().ProductEntityUsingOrderFailedProductId;
		internal static readonly IEntityRelation ProductEntityUsingUnlockDeliverypointProductIdStatic = new TerminalRelations().ProductEntityUsingUnlockDeliverypointProductId;
		internal static readonly IEntityRelation TerminalEntityUsingTerminalIdForwardToTerminalIdStatic = new TerminalRelations().TerminalEntityUsingTerminalIdForwardToTerminalId;

		/// <summary>CTor</summary>
		static StaticTerminalRelations() { }
	}
}

