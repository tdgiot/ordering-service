﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 5.8
// Code is generated on: 
// Code is generated using templates: Crave.TemplateBindings
// Templates vendor: Crave Interactive
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.FactoryClasses;
using Crave.Ordering.Shared.Data.LLBLGen.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

// ReSharper disable All
namespace Crave.Ordering.Shared.Data.LLBLGen.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'Schedule'.<br/><br/></summary>
	[Serializable]
	public partial class ScheduleEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END
	
	{
		private EntityCollection<CategoryEntity> _categoryCollection;
		private EntityCollection<ProductEntity> _productCollection;
		private EntityCollection<ScheduleitemEntity> _scheduleitemCollection;
		private CompanyEntity _company;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		private static ScheduleEntityStaticMetaData _staticMetaData = new ScheduleEntityStaticMetaData();
		private static ScheduleRelations _relationsFactory = new ScheduleRelations();		

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Company</summary>
			public static readonly string Company = "Company";
			/// <summary>Member name CategoryCollection</summary>
			public static readonly string CategoryCollection = "CategoryCollection";
			/// <summary>Member name ProductCollection</summary>
			public static readonly string ProductCollection = "ProductCollection";
			/// <summary>Member name ScheduleitemCollection</summary>
			public static readonly string ScheduleitemCollection = "ScheduleitemCollection";
		}

		/// <summary>Static meta-data storage for navigator related information</summary>
		protected class ScheduleEntityStaticMetaData : EntityStaticMetaDataBase
		{
			public ScheduleEntityStaticMetaData()
			{
				SetEntityCoreInfo("ScheduleEntity", InheritanceHierarchyType.None, false, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ScheduleEntity, typeof(ScheduleEntity), typeof(ScheduleEntityFactory), false);
				AddNavigatorMetaData<ScheduleEntity, EntityCollection<CategoryEntity>>("CategoryCollection", a => a._categoryCollection, (a, b) => a._categoryCollection = b, a => a.CategoryCollection, () => new ScheduleRelations().CategoryEntityUsingScheduleId, typeof(CategoryEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.CategoryEntity);
				AddNavigatorMetaData<ScheduleEntity, EntityCollection<ProductEntity>>("ProductCollection", a => a._productCollection, (a, b) => a._productCollection = b, a => a.ProductCollection, () => new ScheduleRelations().ProductEntityUsingScheduleId, typeof(ProductEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ProductEntity);
				AddNavigatorMetaData<ScheduleEntity, EntityCollection<ScheduleitemEntity>>("ScheduleitemCollection", a => a._scheduleitemCollection, (a, b) => a._scheduleitemCollection = b, a => a.ScheduleitemCollection, () => new ScheduleRelations().ScheduleitemEntityUsingScheduleId, typeof(ScheduleitemEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ScheduleitemEntity);
				AddNavigatorMetaData<ScheduleEntity, CompanyEntity>("Company", "ScheduleCollection", (a, b) => a._company = b, a => a._company, (a, b) => a.Company = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticScheduleRelations.CompanyEntityUsingCompanyIdStatic, ()=>new ScheduleRelations().CompanyEntityUsingCompanyId, null, new int[] { (int)ScheduleFieldIndex.CompanyId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.CompanyEntity);
			}
		}

		/// <summary>Static ctor</summary>
		static ScheduleEntity()
		{
		}

		/// <summary> CTor</summary>
		public ScheduleEntity()
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public ScheduleEntity(IEntityFields2 fields)
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this ScheduleEntity</param>
		public ScheduleEntity(IValidator validator)
		{
			InitClassEmpty(validator, null);
		}

		/// <summary> CTor</summary>
		/// <param name="scheduleId">PK value for Schedule which data should be fetched into this Schedule object</param>
		public ScheduleEntity(System.Int32 scheduleId) : this(scheduleId, null)
		{
		}

		/// <summary> CTor</summary>
		/// <param name="scheduleId">PK value for Schedule which data should be fetched into this Schedule object</param>
		/// <param name="validator">The custom validator object for this ScheduleEntity</param>
		public ScheduleEntity(System.Int32 scheduleId, IValidator validator)
		{
			InitClassEmpty(validator, null);
			this.ScheduleId = scheduleId;
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ScheduleEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Category' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoCategoryCollection() { return CreateRelationInfoForNavigator("CategoryCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Product' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoProductCollection() { return CreateRelationInfoForNavigator("ProductCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Scheduleitem' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoScheduleitemCollection() { return CreateRelationInfoForNavigator("ScheduleitemCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Company' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoCompany() { return CreateRelationInfoForNavigator("Company"); }
		
		/// <inheritdoc/>
		protected override EntityStaticMetaDataBase GetEntityStaticMetaData() {	return _staticMetaData; }

		/// <summary>Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		/// <summary>Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this ScheduleEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary>The relations object holding all relations of this entity with other entity classes.</summary>
		public static ScheduleRelations Relations { get { return _relationsFactory; } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Category' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathCategoryCollection { get { return _staticMetaData.GetPrefetchPathElement("CategoryCollection", CommonEntityBase.CreateEntityCollection<CategoryEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Product' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathProductCollection { get { return _staticMetaData.GetPrefetchPathElement("ProductCollection", CommonEntityBase.CreateEntityCollection<ProductEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Scheduleitem' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathScheduleitemCollection { get { return _staticMetaData.GetPrefetchPathElement("ScheduleitemCollection", CommonEntityBase.CreateEntityCollection<ScheduleitemEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Company' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathCompanyEntity { get { return _staticMetaData.GetPrefetchPathElement("Company", CommonEntityBase.CreateEntityCollection<CompanyEntity>()); } }

		/// <summary>The ScheduleId property of the Entity Schedule<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Schedule"."ScheduleId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 ScheduleId
		{
			get { return (System.Int32)GetValue((int)ScheduleFieldIndex.ScheduleId, true); }
			set { SetValue((int)ScheduleFieldIndex.ScheduleId, value); }		}

		/// <summary>The CompanyId property of the Entity Schedule<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Schedule"."CompanyId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CompanyId
		{
			get { return (System.Int32)GetValue((int)ScheduleFieldIndex.CompanyId, true); }
			set	{ SetValue((int)ScheduleFieldIndex.CompanyId, value); }
		}

		/// <summary>The Name property of the Entity Schedule<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Schedule"."Name".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)ScheduleFieldIndex.Name, true); }
			set	{ SetValue((int)ScheduleFieldIndex.Name, value); }
		}

		/// <summary>The CreatedUTC property of the Entity Schedule<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Schedule"."CreatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ScheduleFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)ScheduleFieldIndex.CreatedUTC, value); }
		}

		/// <summary>The CreatedBy property of the Entity Schedule<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Schedule"."CreatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CreatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)ScheduleFieldIndex.CreatedBy, false); }
			set	{ SetValue((int)ScheduleFieldIndex.CreatedBy, value); }
		}

		/// <summary>The UpdatedUTC property of the Entity Schedule<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Schedule"."UpdatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ScheduleFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)ScheduleFieldIndex.UpdatedUTC, value); }
		}

		/// <summary>The UpdatedBy property of the Entity Schedule<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Schedule"."UpdatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UpdatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)ScheduleFieldIndex.UpdatedBy, false); }
			set	{ SetValue((int)ScheduleFieldIndex.UpdatedBy, value); }
		}

		/// <summary>Gets the EntityCollection with the related entities of type 'CategoryEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(CategoryEntity))]
		public virtual EntityCollection<CategoryEntity> CategoryCollection { get { return GetOrCreateEntityCollection<CategoryEntity, CategoryEntityFactory>("Schedule", true, false, ref _categoryCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'ProductEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(ProductEntity))]
		public virtual EntityCollection<ProductEntity> ProductCollection { get { return GetOrCreateEntityCollection<ProductEntity, ProductEntityFactory>("Schedule", true, false, ref _productCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'ScheduleitemEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(ScheduleitemEntity))]
		public virtual EntityCollection<ScheduleitemEntity> ScheduleitemCollection { get { return GetOrCreateEntityCollection<ScheduleitemEntity, ScheduleitemEntityFactory>("Schedule", true, false, ref _scheduleitemCollection); } }

		/// <summary>Gets / sets related entity of type 'CompanyEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual CompanyEntity Company
		{
			get { return _company; }
			set { SetSingleRelatedEntityNavigator(value, "Company"); }
		}


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen
{
	public enum ScheduleFieldIndex
	{
		///<summary>ScheduleId. </summary>
		ScheduleId,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>Name. </summary>
		Name,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		/// <summary></summary>
		AmountOfFields
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Schedule. </summary>
	public partial class ScheduleRelations: RelationFactory
	{
		#region Custom Relation code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomRelationCode
        private const ReferentialConstraintDeleteRule DeleteRuleForCategoryEntityUsingScheduleId = ReferentialConstraintDeleteRule.NoAction;
        private const ReferentialConstraintDeleteRule DeleteRuleForProductEntityUsingScheduleId = ReferentialConstraintDeleteRule.NoAction;
        private const ReferentialConstraintDeleteRule DeleteRuleForScheduleitemEntityUsingScheduleId = ReferentialConstraintDeleteRule.Cascade;
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		
		// private const ReferentialConstraintDeleteRule DeleteRuleForCategoryEntityUsingScheduleId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForProductEntityUsingScheduleId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForScheduleitemEntityUsingScheduleId = ReferentialConstraintDeleteRule


		/// <summary>Gets all relations and delete rules of the ScheduleEntity as a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects.</summary>
		/// <returns>a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects</returns>
		public virtual Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> GetRelationsAndDeleteRules()
		{
			Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> toReturn = new Dictionary<IEntityRelation, ReferentialConstraintDeleteRule>();
			toReturn.Add(this.CategoryEntityUsingScheduleId, ScheduleRelations.DeleteRuleForCategoryEntityUsingScheduleId);
			toReturn.Add(this.ProductEntityUsingScheduleId, ScheduleRelations.DeleteRuleForProductEntityUsingScheduleId);
			toReturn.Add(this.ScheduleitemEntityUsingScheduleId, ScheduleRelations.DeleteRuleForScheduleitemEntityUsingScheduleId);
			return toReturn;
		}		
	
	
		/// <summary>Returns a new IEntityRelation object, between ScheduleEntity and CategoryEntity over the 1:n relation they have, using the relation between the fields: Schedule.ScheduleId - Category.ScheduleId</summary>
		public virtual IEntityRelation CategoryEntityUsingScheduleId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "CategoryCollection", true, new[] { ScheduleFields.ScheduleId, CategoryFields.ScheduleId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between ScheduleEntity and ProductEntity over the 1:n relation they have, using the relation between the fields: Schedule.ScheduleId - Product.ScheduleId</summary>
		public virtual IEntityRelation ProductEntityUsingScheduleId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "ProductCollection", true, new[] { ScheduleFields.ScheduleId, ProductFields.ScheduleId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between ScheduleEntity and ScheduleitemEntity over the 1:n relation they have, using the relation between the fields: Schedule.ScheduleId - Scheduleitem.ScheduleId</summary>
		public virtual IEntityRelation ScheduleitemEntityUsingScheduleId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "ScheduleitemCollection", true, new[] { ScheduleFields.ScheduleId, ScheduleitemFields.ScheduleId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between ScheduleEntity and CompanyEntity over the m:1 relation they have, using the relation between the fields: Schedule.CompanyId - Company.CompanyId</summary>
		public virtual IEntityRelation CompanyEntityUsingCompanyId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Company", false, new[] { CompanyFields.CompanyId, ScheduleFields.CompanyId }); }
		}

	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticScheduleRelations
	{
		internal static readonly IEntityRelation CategoryEntityUsingScheduleIdStatic = new ScheduleRelations().CategoryEntityUsingScheduleId;
		internal static readonly IEntityRelation ProductEntityUsingScheduleIdStatic = new ScheduleRelations().ProductEntityUsingScheduleId;
		internal static readonly IEntityRelation ScheduleitemEntityUsingScheduleIdStatic = new ScheduleRelations().ScheduleitemEntityUsingScheduleId;
		internal static readonly IEntityRelation CompanyEntityUsingCompanyIdStatic = new ScheduleRelations().CompanyEntityUsingCompanyId;

		/// <summary>CTor</summary>
		static StaticScheduleRelations() { }
	}
}

