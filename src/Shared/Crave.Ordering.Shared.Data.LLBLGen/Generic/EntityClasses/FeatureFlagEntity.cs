﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 5.8
// Code is generated on: 
// Code is generated using templates: Crave.TemplateBindings
// Templates vendor: Crave Interactive
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.FactoryClasses;
using Crave.Ordering.Shared.Data.LLBLGen.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

// ReSharper disable All
namespace Crave.Ordering.Shared.Data.LLBLGen.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'FeatureFlag'.<br/><br/></summary>
	[Serializable]
	public partial class FeatureFlagEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END
	
	{
		private ApplicationConfigurationEntity _applicationConfiguration;
		private CompanyEntity _company;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		private static FeatureFlagEntityStaticMetaData _staticMetaData = new FeatureFlagEntityStaticMetaData();
		private static FeatureFlagRelations _relationsFactory = new FeatureFlagRelations();		

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name ApplicationConfiguration</summary>
			public static readonly string ApplicationConfiguration = "ApplicationConfiguration";
			/// <summary>Member name Company</summary>
			public static readonly string Company = "Company";
		}

		/// <summary>Static meta-data storage for navigator related information</summary>
		protected class FeatureFlagEntityStaticMetaData : EntityStaticMetaDataBase
		{
			public FeatureFlagEntityStaticMetaData()
			{
				SetEntityCoreInfo("FeatureFlagEntity", InheritanceHierarchyType.None, false, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.FeatureFlagEntity, typeof(FeatureFlagEntity), typeof(FeatureFlagEntityFactory), false);
				AddNavigatorMetaData<FeatureFlagEntity, ApplicationConfigurationEntity>("ApplicationConfiguration", "FeatureFlagCollection", (a, b) => a._applicationConfiguration = b, a => a._applicationConfiguration, (a, b) => a.ApplicationConfiguration = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticFeatureFlagRelations.ApplicationConfigurationEntityUsingApplicationConfigurationIdStatic, ()=>new FeatureFlagRelations().ApplicationConfigurationEntityUsingApplicationConfigurationId, null, new int[] { (int)FeatureFlagFieldIndex.ApplicationConfigurationId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ApplicationConfigurationEntity);
				AddNavigatorMetaData<FeatureFlagEntity, CompanyEntity>("Company", "FeatureFlagCollection", (a, b) => a._company = b, a => a._company, (a, b) => a.Company = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticFeatureFlagRelations.CompanyEntityUsingCompanyIdStatic, ()=>new FeatureFlagRelations().CompanyEntityUsingCompanyId, null, new int[] { (int)FeatureFlagFieldIndex.CompanyId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.CompanyEntity);
			}
		}

		/// <summary>Static ctor</summary>
		static FeatureFlagEntity()
		{
		}

		/// <summary> CTor</summary>
		public FeatureFlagEntity()
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public FeatureFlagEntity(IEntityFields2 fields)
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this FeatureFlagEntity</param>
		public FeatureFlagEntity(IValidator validator)
		{
			InitClassEmpty(validator, null);
		}

		/// <summary> CTor</summary>
		/// <param name="featureFlagId">PK value for FeatureFlag which data should be fetched into this FeatureFlag object</param>
		public FeatureFlagEntity(System.Int32 featureFlagId) : this(featureFlagId, null)
		{
		}

		/// <summary> CTor</summary>
		/// <param name="featureFlagId">PK value for FeatureFlag which data should be fetched into this FeatureFlag object</param>
		/// <param name="validator">The custom validator object for this FeatureFlagEntity</param>
		public FeatureFlagEntity(System.Int32 featureFlagId, IValidator validator)
		{
			InitClassEmpty(validator, null);
			this.FeatureFlagId = featureFlagId;
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected FeatureFlagEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'ApplicationConfiguration' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoApplicationConfiguration() { return CreateRelationInfoForNavigator("ApplicationConfiguration"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Company' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoCompany() { return CreateRelationInfoForNavigator("Company"); }
		
		/// <inheritdoc/>
		protected override EntityStaticMetaDataBase GetEntityStaticMetaData() {	return _staticMetaData; }

		/// <summary>Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		/// <summary>Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this FeatureFlagEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary>The relations object holding all relations of this entity with other entity classes.</summary>
		public static FeatureFlagRelations Relations { get { return _relationsFactory; } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'ApplicationConfiguration' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathApplicationConfigurationEntity { get { return _staticMetaData.GetPrefetchPathElement("ApplicationConfiguration", CommonEntityBase.CreateEntityCollection<ApplicationConfigurationEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Company' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathCompanyEntity { get { return _staticMetaData.GetPrefetchPathElement("Company", CommonEntityBase.CreateEntityCollection<CompanyEntity>()); } }

		/// <summary>The FeatureFlagId property of the Entity FeatureFlag<br/><br/></summary>
		/// <remarks>Mapped on  table field: "FeatureFlag"."FeatureFlagId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 FeatureFlagId
		{
			get { return (System.Int32)GetValue((int)FeatureFlagFieldIndex.FeatureFlagId, true); }
			set { SetValue((int)FeatureFlagFieldIndex.FeatureFlagId, value); }		}

		/// <summary>The ApplicationConfigurationId property of the Entity FeatureFlag<br/><br/></summary>
		/// <remarks>Mapped on  table field: "FeatureFlag"."ApplicationConfigurationId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ApplicationConfigurationId
		{
			get { return (System.Int32)GetValue((int)FeatureFlagFieldIndex.ApplicationConfigurationId, true); }
			set	{ SetValue((int)FeatureFlagFieldIndex.ApplicationConfigurationId, value); }
		}

		/// <summary>The ApplessFeatureFlags property of the Entity FeatureFlag<br/><br/></summary>
		/// <remarks>Mapped on  table field: "FeatureFlag"."ApplessFeatureFlags".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Crave.Enums.ApplessFeatureFlags ApplessFeatureFlags
		{
			get { return (Crave.Enums.ApplessFeatureFlags)GetValue((int)FeatureFlagFieldIndex.ApplessFeatureFlags, true); }
			set	{ SetValue((int)FeatureFlagFieldIndex.ApplessFeatureFlags, value); }
		}

		/// <summary>The CreatedUTC property of the Entity FeatureFlag<br/><br/></summary>
		/// <remarks>Mapped on  table field: "FeatureFlag"."CreatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime CreatedUTC
		{
			get { return (System.DateTime)GetValue((int)FeatureFlagFieldIndex.CreatedUTC, true); }
			set	{ SetValue((int)FeatureFlagFieldIndex.CreatedUTC, value); }
		}

		/// <summary>The CreatedBy property of the Entity FeatureFlag<br/><br/></summary>
		/// <remarks>Mapped on  table field: "FeatureFlag"."CreatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CreatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)FeatureFlagFieldIndex.CreatedBy, false); }
			set	{ SetValue((int)FeatureFlagFieldIndex.CreatedBy, value); }
		}

		/// <summary>The UpdatedUTC property of the Entity FeatureFlag<br/><br/></summary>
		/// <remarks>Mapped on  table field: "FeatureFlag"."UpdatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)FeatureFlagFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)FeatureFlagFieldIndex.UpdatedUTC, value); }
		}

		/// <summary>The UpdatedBy property of the Entity FeatureFlag<br/><br/></summary>
		/// <remarks>Mapped on  table field: "FeatureFlag"."UpdatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UpdatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)FeatureFlagFieldIndex.UpdatedBy, false); }
			set	{ SetValue((int)FeatureFlagFieldIndex.UpdatedBy, value); }
		}

		/// <summary>The CompanyId property of the Entity FeatureFlag<br/><br/></summary>
		/// <remarks>Mapped on  table field: "FeatureFlag"."CompanyId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CompanyId
		{
			get { return (System.Int32)GetValue((int)FeatureFlagFieldIndex.CompanyId, true); }
			set	{ SetValue((int)FeatureFlagFieldIndex.CompanyId, value); }
		}

		/// <summary>Gets / sets related entity of type 'ApplicationConfigurationEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual ApplicationConfigurationEntity ApplicationConfiguration
		{
			get { return _applicationConfiguration; }
			set { SetSingleRelatedEntityNavigator(value, "ApplicationConfiguration"); }
		}

		/// <summary>Gets / sets related entity of type 'CompanyEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual CompanyEntity Company
		{
			get { return _company; }
			set { SetSingleRelatedEntityNavigator(value, "Company"); }
		}


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen
{
	public enum FeatureFlagFieldIndex
	{
		///<summary>FeatureFlagId. </summary>
		FeatureFlagId,
		///<summary>ApplicationConfigurationId. </summary>
		ApplicationConfigurationId,
		///<summary>ApplessFeatureFlags. </summary>
		ApplessFeatureFlags,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CompanyId. </summary>
		CompanyId,
		/// <summary></summary>
		AmountOfFields
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: FeatureFlag. </summary>
	public partial class FeatureFlagRelations: RelationFactory
	{
		#region Custom Relation code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomRelationCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		


		/// <summary>Gets all relations and delete rules of the FeatureFlagEntity as a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects.</summary>
		/// <returns>a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects</returns>
		public virtual Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> GetRelationsAndDeleteRules()
		{
			Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> toReturn = new Dictionary<IEntityRelation, ReferentialConstraintDeleteRule>();
			return toReturn;
		}		
	
	

		/// <summary>Returns a new IEntityRelation object, between FeatureFlagEntity and ApplicationConfigurationEntity over the m:1 relation they have, using the relation between the fields: FeatureFlag.ApplicationConfigurationId - ApplicationConfiguration.ApplicationConfigurationId</summary>
		public virtual IEntityRelation ApplicationConfigurationEntityUsingApplicationConfigurationId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "ApplicationConfiguration", false, new[] { ApplicationConfigurationFields.ApplicationConfigurationId, FeatureFlagFields.ApplicationConfigurationId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between FeatureFlagEntity and CompanyEntity over the m:1 relation they have, using the relation between the fields: FeatureFlag.CompanyId - Company.CompanyId</summary>
		public virtual IEntityRelation CompanyEntityUsingCompanyId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Company", false, new[] { CompanyFields.CompanyId, FeatureFlagFields.CompanyId }); }
		}

	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticFeatureFlagRelations
	{
		internal static readonly IEntityRelation ApplicationConfigurationEntityUsingApplicationConfigurationIdStatic = new FeatureFlagRelations().ApplicationConfigurationEntityUsingApplicationConfigurationId;
		internal static readonly IEntityRelation CompanyEntityUsingCompanyIdStatic = new FeatureFlagRelations().CompanyEntityUsingCompanyId;

		/// <summary>CTor</summary>
		static StaticFeatureFlagRelations() { }
	}
}

