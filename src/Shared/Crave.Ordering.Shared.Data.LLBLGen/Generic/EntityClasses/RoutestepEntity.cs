﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 5.8
// Code is generated on: 
// Code is generated using templates: Crave.TemplateBindings
// Templates vendor: Crave Interactive
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.FactoryClasses;
using Crave.Ordering.Shared.Data.LLBLGen.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

// ReSharper disable All
namespace Crave.Ordering.Shared.Data.LLBLGen.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'Routestep'.<br/><br/></summary>
	[Serializable]
	public partial class RoutestepEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END
	
	{
		private EntityCollection<RoutestephandlerEntity> _routestephandlerCollection;
		private RouteEntity _route;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		private static RoutestepEntityStaticMetaData _staticMetaData = new RoutestepEntityStaticMetaData();
		private static RoutestepRelations _relationsFactory = new RoutestepRelations();		

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Route</summary>
			public static readonly string Route = "Route";
			/// <summary>Member name RoutestephandlerCollection</summary>
			public static readonly string RoutestephandlerCollection = "RoutestephandlerCollection";
		}

		/// <summary>Static meta-data storage for navigator related information</summary>
		protected class RoutestepEntityStaticMetaData : EntityStaticMetaDataBase
		{
			public RoutestepEntityStaticMetaData()
			{
				SetEntityCoreInfo("RoutestepEntity", InheritanceHierarchyType.None, false, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.RoutestepEntity, typeof(RoutestepEntity), typeof(RoutestepEntityFactory), false);
				AddNavigatorMetaData<RoutestepEntity, EntityCollection<RoutestephandlerEntity>>("RoutestephandlerCollection", a => a._routestephandlerCollection, (a, b) => a._routestephandlerCollection = b, a => a.RoutestephandlerCollection, () => new RoutestepRelations().RoutestephandlerEntityUsingRoutestepId, typeof(RoutestephandlerEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.RoutestephandlerEntity);
				AddNavigatorMetaData<RoutestepEntity, RouteEntity>("Route", "RoutestepCollection", (a, b) => a._route = b, a => a._route, (a, b) => a.Route = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticRoutestepRelations.RouteEntityUsingRouteIdStatic, ()=>new RoutestepRelations().RouteEntityUsingRouteId, null, new int[] { (int)RoutestepFieldIndex.RouteId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.RouteEntity);
			}
		}

		/// <summary>Static ctor</summary>
		static RoutestepEntity()
		{
		}

		/// <summary> CTor</summary>
		public RoutestepEntity()
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public RoutestepEntity(IEntityFields2 fields)
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this RoutestepEntity</param>
		public RoutestepEntity(IValidator validator)
		{
			InitClassEmpty(validator, null);
		}

		/// <summary> CTor</summary>
		/// <param name="routestepId">PK value for Routestep which data should be fetched into this Routestep object</param>
		public RoutestepEntity(System.Int32 routestepId) : this(routestepId, null)
		{
		}

		/// <summary> CTor</summary>
		/// <param name="routestepId">PK value for Routestep which data should be fetched into this Routestep object</param>
		/// <param name="validator">The custom validator object for this RoutestepEntity</param>
		public RoutestepEntity(System.Int32 routestepId, IValidator validator)
		{
			InitClassEmpty(validator, null);
			this.RoutestepId = routestepId;
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected RoutestepEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Routestephandler' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoRoutestephandlerCollection() { return CreateRelationInfoForNavigator("RoutestephandlerCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Route' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoRoute() { return CreateRelationInfoForNavigator("Route"); }
		
		/// <inheritdoc/>
		protected override EntityStaticMetaDataBase GetEntityStaticMetaData() {	return _staticMetaData; }

		/// <summary>Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		/// <summary>Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this RoutestepEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary>The relations object holding all relations of this entity with other entity classes.</summary>
		public static RoutestepRelations Relations { get { return _relationsFactory; } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Routestephandler' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathRoutestephandlerCollection { get { return _staticMetaData.GetPrefetchPathElement("RoutestephandlerCollection", CommonEntityBase.CreateEntityCollection<RoutestephandlerEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Route' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathRouteEntity { get { return _staticMetaData.GetPrefetchPathElement("Route", CommonEntityBase.CreateEntityCollection<RouteEntity>()); } }

		/// <summary>The RoutestepId property of the Entity Routestep<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Routestep"."RoutestepId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 RoutestepId
		{
			get { return (System.Int32)GetValue((int)RoutestepFieldIndex.RoutestepId, true); }
			set { SetValue((int)RoutestepFieldIndex.RoutestepId, value); }		}

		/// <summary>The ParentCompanyId property of the Entity Routestep<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Routestep"."ParentCompanyId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ParentCompanyId
		{
			get { return (System.Int32)GetValue((int)RoutestepFieldIndex.ParentCompanyId, true); }
			set	{ SetValue((int)RoutestepFieldIndex.ParentCompanyId, value); }
		}

		/// <summary>The RouteId property of the Entity Routestep<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Routestep"."RouteId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 RouteId
		{
			get { return (System.Int32)GetValue((int)RoutestepFieldIndex.RouteId, true); }
			set	{ SetValue((int)RoutestepFieldIndex.RouteId, value); }
		}

		/// <summary>The Number property of the Entity Routestep<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Routestep"."Number".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 Number
		{
			get { return (System.Int32)GetValue((int)RoutestepFieldIndex.Number, true); }
			set	{ SetValue((int)RoutestepFieldIndex.Number, value); }
		}

		/// <summary>The TimeoutMinutes property of the Entity Routestep<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Routestep"."TimeoutMinutes".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> TimeoutMinutes
		{
			get { return (Nullable<System.Int32>)GetValue((int)RoutestepFieldIndex.TimeoutMinutes, false); }
			set	{ SetValue((int)RoutestepFieldIndex.TimeoutMinutes, value); }
		}

		/// <summary>The ContinueOnFailure property of the Entity Routestep<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Routestep"."ContinueOnFailure".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean ContinueOnFailure
		{
			get { return (System.Boolean)GetValue((int)RoutestepFieldIndex.ContinueOnFailure, true); }
			set	{ SetValue((int)RoutestepFieldIndex.ContinueOnFailure, value); }
		}

		/// <summary>The CompleteRouteOnComplete property of the Entity Routestep<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Routestep"."CompleteRouteOnComplete".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean CompleteRouteOnComplete
		{
			get { return (System.Boolean)GetValue((int)RoutestepFieldIndex.CompleteRouteOnComplete, true); }
			set	{ SetValue((int)RoutestepFieldIndex.CompleteRouteOnComplete, value); }
		}

		/// <summary>The CreatedBy property of the Entity Routestep<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Routestep"."CreatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)RoutestepFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)RoutestepFieldIndex.CreatedBy, value); }
		}

		/// <summary>The UpdatedBy property of the Entity Routestep<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Routestep"."UpdatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)RoutestepFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)RoutestepFieldIndex.UpdatedBy, value); }
		}

		/// <summary>The CreatedUTC property of the Entity Routestep<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Routestep"."CreatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)RoutestepFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)RoutestepFieldIndex.CreatedUTC, value); }
		}

		/// <summary>The UpdatedUTC property of the Entity Routestep<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Routestep"."UpdatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)RoutestepFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)RoutestepFieldIndex.UpdatedUTC, value); }
		}

		/// <summary>Gets the EntityCollection with the related entities of type 'RoutestephandlerEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(RoutestephandlerEntity))]
		public virtual EntityCollection<RoutestephandlerEntity> RoutestephandlerCollection { get { return GetOrCreateEntityCollection<RoutestephandlerEntity, RoutestephandlerEntityFactory>("Routestep", true, false, ref _routestephandlerCollection); } }

		/// <summary>Gets / sets related entity of type 'RouteEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual RouteEntity Route
		{
			get { return _route; }
			set { SetSingleRelatedEntityNavigator(value, "Route"); }
		}


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen
{
	public enum RoutestepFieldIndex
	{
		///<summary>RoutestepId. </summary>
		RoutestepId,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>RouteId. </summary>
		RouteId,
		///<summary>Number. </summary>
		Number,
		///<summary>TimeoutMinutes. </summary>
		TimeoutMinutes,
		///<summary>ContinueOnFailure. </summary>
		ContinueOnFailure,
		///<summary>CompleteRouteOnComplete. </summary>
		CompleteRouteOnComplete,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Routestep. </summary>
	public partial class RoutestepRelations: RelationFactory
	{
		#region Custom Relation code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomRelationCode
        private const ReferentialConstraintDeleteRule DeleteRuleForRoutestephandlerEntityUsingRoutestepId = ReferentialConstraintDeleteRule.Cascade;
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		
		// private const ReferentialConstraintDeleteRule DeleteRuleForRoutestephandlerEntityUsingRoutestepId = ReferentialConstraintDeleteRule


		/// <summary>Gets all relations and delete rules of the RoutestepEntity as a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects.</summary>
		/// <returns>a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects</returns>
		public virtual Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> GetRelationsAndDeleteRules()
		{
			Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> toReturn = new Dictionary<IEntityRelation, ReferentialConstraintDeleteRule>();
			toReturn.Add(this.RoutestephandlerEntityUsingRoutestepId, RoutestepRelations.DeleteRuleForRoutestephandlerEntityUsingRoutestepId);
			return toReturn;
		}		
	
	
		/// <summary>Returns a new IEntityRelation object, between RoutestepEntity and RoutestephandlerEntity over the 1:n relation they have, using the relation between the fields: Routestep.RoutestepId - Routestephandler.RoutestepId</summary>
		public virtual IEntityRelation RoutestephandlerEntityUsingRoutestepId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "RoutestephandlerCollection", true, new[] { RoutestepFields.RoutestepId, RoutestephandlerFields.RoutestepId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between RoutestepEntity and RouteEntity over the m:1 relation they have, using the relation between the fields: Routestep.RouteId - Route.RouteId</summary>
		public virtual IEntityRelation RouteEntityUsingRouteId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Route", false, new[] { RouteFields.RouteId, RoutestepFields.RouteId }); }
		}

	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticRoutestepRelations
	{
		internal static readonly IEntityRelation RoutestephandlerEntityUsingRoutestepIdStatic = new RoutestepRelations().RoutestephandlerEntityUsingRoutestepId;
		internal static readonly IEntityRelation RouteEntityUsingRouteIdStatic = new RoutestepRelations().RouteEntityUsingRouteId;

		/// <summary>CTor</summary>
		static StaticRoutestepRelations() { }
	}
}

