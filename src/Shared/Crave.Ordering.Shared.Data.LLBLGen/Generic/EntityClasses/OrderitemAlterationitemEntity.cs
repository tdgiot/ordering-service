﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 5.8
// Code is generated on: 
// Code is generated using templates: Crave.TemplateBindings
// Templates vendor: Crave Interactive
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.FactoryClasses;
using Crave.Ordering.Shared.Data.LLBLGen.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

// ReSharper disable All
namespace Crave.Ordering.Shared.Data.LLBLGen.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'OrderitemAlterationitem'.<br/><br/></summary>
	[Serializable]
	public partial class OrderitemAlterationitemEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END
	
	{
		private EntityCollection<OrderitemAlterationitemTagEntity> _orderitemAlterationitemTagCollection;
		private AlterationEntity _alteration;
		private AlterationitemEntity _alterationitem;
		private OrderitemEntity _orderitem;
		private PriceLevelItemEntity _priceLevelItem;
		private ProductEntity _product;
		private TaxTariffEntity _taxTariff;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		private static OrderitemAlterationitemEntityStaticMetaData _staticMetaData = new OrderitemAlterationitemEntityStaticMetaData();
		private static OrderitemAlterationitemRelations _relationsFactory = new OrderitemAlterationitemRelations();		

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Alteration</summary>
			public static readonly string Alteration = "Alteration";
			/// <summary>Member name Alterationitem</summary>
			public static readonly string Alterationitem = "Alterationitem";
			/// <summary>Member name Orderitem</summary>
			public static readonly string Orderitem = "Orderitem";
			/// <summary>Member name PriceLevelItem</summary>
			public static readonly string PriceLevelItem = "PriceLevelItem";
			/// <summary>Member name Product</summary>
			public static readonly string Product = "Product";
			/// <summary>Member name TaxTariff</summary>
			public static readonly string TaxTariff = "TaxTariff";
			/// <summary>Member name OrderitemAlterationitemTagCollection</summary>
			public static readonly string OrderitemAlterationitemTagCollection = "OrderitemAlterationitemTagCollection";
		}

		/// <summary>Static meta-data storage for navigator related information</summary>
		protected class OrderitemAlterationitemEntityStaticMetaData : EntityStaticMetaDataBase
		{
			public OrderitemAlterationitemEntityStaticMetaData()
			{
				SetEntityCoreInfo("OrderitemAlterationitemEntity", InheritanceHierarchyType.None, false, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.OrderitemAlterationitemEntity, typeof(OrderitemAlterationitemEntity), typeof(OrderitemAlterationitemEntityFactory), false);
				AddNavigatorMetaData<OrderitemAlterationitemEntity, EntityCollection<OrderitemAlterationitemTagEntity>>("OrderitemAlterationitemTagCollection", a => a._orderitemAlterationitemTagCollection, (a, b) => a._orderitemAlterationitemTagCollection = b, a => a.OrderitemAlterationitemTagCollection, () => new OrderitemAlterationitemRelations().OrderitemAlterationitemTagEntityUsingOrderitemAlterationitemId, typeof(OrderitemAlterationitemTagEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.OrderitemAlterationitemTagEntity);
				AddNavigatorMetaData<OrderitemAlterationitemEntity, AlterationEntity>("Alteration", "OrderitemAlterationitemCollection", (a, b) => a._alteration = b, a => a._alteration, (a, b) => a.Alteration = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticOrderitemAlterationitemRelations.AlterationEntityUsingAlterationIdStatic, ()=>new OrderitemAlterationitemRelations().AlterationEntityUsingAlterationId, null, new int[] { (int)OrderitemAlterationitemFieldIndex.AlterationId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.AlterationEntity);
				AddNavigatorMetaData<OrderitemAlterationitemEntity, AlterationitemEntity>("Alterationitem", "OrderitemAlterationitemCollection", (a, b) => a._alterationitem = b, a => a._alterationitem, (a, b) => a.Alterationitem = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticOrderitemAlterationitemRelations.AlterationitemEntityUsingAlterationitemIdStatic, ()=>new OrderitemAlterationitemRelations().AlterationitemEntityUsingAlterationitemId, null, new int[] { (int)OrderitemAlterationitemFieldIndex.AlterationitemId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.AlterationitemEntity);
				AddNavigatorMetaData<OrderitemAlterationitemEntity, OrderitemEntity>("Orderitem", "OrderitemAlterationitemCollection", (a, b) => a._orderitem = b, a => a._orderitem, (a, b) => a.Orderitem = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticOrderitemAlterationitemRelations.OrderitemEntityUsingOrderitemIdStatic, ()=>new OrderitemAlterationitemRelations().OrderitemEntityUsingOrderitemId, null, new int[] { (int)OrderitemAlterationitemFieldIndex.OrderitemId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.OrderitemEntity);
				AddNavigatorMetaData<OrderitemAlterationitemEntity, PriceLevelItemEntity>("PriceLevelItem", "OrderitemAlterationitemCollection", (a, b) => a._priceLevelItem = b, a => a._priceLevelItem, (a, b) => a.PriceLevelItem = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticOrderitemAlterationitemRelations.PriceLevelItemEntityUsingPriceLevelItemIdStatic, ()=>new OrderitemAlterationitemRelations().PriceLevelItemEntityUsingPriceLevelItemId, null, new int[] { (int)OrderitemAlterationitemFieldIndex.PriceLevelItemId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.PriceLevelItemEntity);
				AddNavigatorMetaData<OrderitemAlterationitemEntity, ProductEntity>("Product", "OrderitemAlterationitemCollection", (a, b) => a._product = b, a => a._product, (a, b) => a.Product = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticOrderitemAlterationitemRelations.ProductEntityUsingAlterationoptionProductIdStatic, ()=>new OrderitemAlterationitemRelations().ProductEntityUsingAlterationoptionProductId, null, new int[] { (int)OrderitemAlterationitemFieldIndex.AlterationoptionProductId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ProductEntity);
				AddNavigatorMetaData<OrderitemAlterationitemEntity, TaxTariffEntity>("TaxTariff", "OrderitemAlterationitemCollection", (a, b) => a._taxTariff = b, a => a._taxTariff, (a, b) => a.TaxTariff = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticOrderitemAlterationitemRelations.TaxTariffEntityUsingTaxTariffIdStatic, ()=>new OrderitemAlterationitemRelations().TaxTariffEntityUsingTaxTariffId, null, new int[] { (int)OrderitemAlterationitemFieldIndex.TaxTariffId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.TaxTariffEntity);
			}
		}

		/// <summary>Static ctor</summary>
		static OrderitemAlterationitemEntity()
		{
		}

		/// <summary> CTor</summary>
		public OrderitemAlterationitemEntity()
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public OrderitemAlterationitemEntity(IEntityFields2 fields)
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this OrderitemAlterationitemEntity</param>
		public OrderitemAlterationitemEntity(IValidator validator)
		{
			InitClassEmpty(validator, null);
		}

		/// <summary> CTor</summary>
		/// <param name="orderitemAlterationitemId">PK value for OrderitemAlterationitem which data should be fetched into this OrderitemAlterationitem object</param>
		public OrderitemAlterationitemEntity(System.Int32 orderitemAlterationitemId) : this(orderitemAlterationitemId, null)
		{
		}

		/// <summary> CTor</summary>
		/// <param name="orderitemAlterationitemId">PK value for OrderitemAlterationitem which data should be fetched into this OrderitemAlterationitem object</param>
		/// <param name="validator">The custom validator object for this OrderitemAlterationitemEntity</param>
		public OrderitemAlterationitemEntity(System.Int32 orderitemAlterationitemId, IValidator validator)
		{
			InitClassEmpty(validator, null);
			this.OrderitemAlterationitemId = orderitemAlterationitemId;
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected OrderitemAlterationitemEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'OrderitemAlterationitemTag' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoOrderitemAlterationitemTagCollection() { return CreateRelationInfoForNavigator("OrderitemAlterationitemTagCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Alteration' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoAlteration() { return CreateRelationInfoForNavigator("Alteration"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Alterationitem' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoAlterationitem() { return CreateRelationInfoForNavigator("Alterationitem"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Orderitem' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoOrderitem() { return CreateRelationInfoForNavigator("Orderitem"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'PriceLevelItem' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoPriceLevelItem() { return CreateRelationInfoForNavigator("PriceLevelItem"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Product' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoProduct() { return CreateRelationInfoForNavigator("Product"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'TaxTariff' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoTaxTariff() { return CreateRelationInfoForNavigator("TaxTariff"); }
		
		/// <inheritdoc/>
		protected override EntityStaticMetaDataBase GetEntityStaticMetaData() {	return _staticMetaData; }

		/// <summary>Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		/// <summary>Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this OrderitemAlterationitemEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary>The relations object holding all relations of this entity with other entity classes.</summary>
		public static OrderitemAlterationitemRelations Relations { get { return _relationsFactory; } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'OrderitemAlterationitemTag' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathOrderitemAlterationitemTagCollection { get { return _staticMetaData.GetPrefetchPathElement("OrderitemAlterationitemTagCollection", CommonEntityBase.CreateEntityCollection<OrderitemAlterationitemTagEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Alteration' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathAlterationEntity { get { return _staticMetaData.GetPrefetchPathElement("Alteration", CommonEntityBase.CreateEntityCollection<AlterationEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Alterationitem' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathAlterationitemEntity { get { return _staticMetaData.GetPrefetchPathElement("Alterationitem", CommonEntityBase.CreateEntityCollection<AlterationitemEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Orderitem' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathOrderitemEntity { get { return _staticMetaData.GetPrefetchPathElement("Orderitem", CommonEntityBase.CreateEntityCollection<OrderitemEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'PriceLevelItem' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathPriceLevelItemEntity { get { return _staticMetaData.GetPrefetchPathElement("PriceLevelItem", CommonEntityBase.CreateEntityCollection<PriceLevelItemEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Product' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathProductEntity { get { return _staticMetaData.GetPrefetchPathElement("Product", CommonEntityBase.CreateEntityCollection<ProductEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'TaxTariff' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathTaxTariffEntity { get { return _staticMetaData.GetPrefetchPathElement("TaxTariff", CommonEntityBase.CreateEntityCollection<TaxTariffEntity>()); } }

		/// <summary>The OrderitemAlterationitemId property of the Entity OrderitemAlterationitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderitemAlterationitem"."OrderitemAlterationitemId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 OrderitemAlterationitemId
		{
			get { return (System.Int32)GetValue((int)OrderitemAlterationitemFieldIndex.OrderitemAlterationitemId, true); }
			set { SetValue((int)OrderitemAlterationitemFieldIndex.OrderitemAlterationitemId, value); }		}

		/// <summary>The ParentCompanyId property of the Entity OrderitemAlterationitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderitemAlterationitem"."ParentCompanyId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ParentCompanyId
		{
			get { return (System.Int32)GetValue((int)OrderitemAlterationitemFieldIndex.ParentCompanyId, true); }
			set	{ SetValue((int)OrderitemAlterationitemFieldIndex.ParentCompanyId, value); }
		}

		/// <summary>The OrderitemId property of the Entity OrderitemAlterationitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderitemAlterationitem"."OrderitemId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 OrderitemId
		{
			get { return (System.Int32)GetValue((int)OrderitemAlterationitemFieldIndex.OrderitemId, true); }
			set	{ SetValue((int)OrderitemAlterationitemFieldIndex.OrderitemId, value); }
		}

		/// <summary>The AlterationId property of the Entity OrderitemAlterationitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderitemAlterationitem"."AlterationId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> AlterationId
		{
			get { return (Nullable<System.Int32>)GetValue((int)OrderitemAlterationitemFieldIndex.AlterationId, false); }
			set	{ SetValue((int)OrderitemAlterationitemFieldIndex.AlterationId, value); }
		}

		/// <summary>The AlterationName property of the Entity OrderitemAlterationitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderitemAlterationitem"."AlterationName".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String AlterationName
		{
			get { return (System.String)GetValue((int)OrderitemAlterationitemFieldIndex.AlterationName, true); }
			set	{ SetValue((int)OrderitemAlterationitemFieldIndex.AlterationName, value); }
		}

		/// <summary>The AlterationType property of the Entity OrderitemAlterationitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderitemAlterationitem"."AlterationType".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Crave.Enums.AlterationType AlterationType
		{
			get { return (Crave.Enums.AlterationType)GetValue((int)OrderitemAlterationitemFieldIndex.AlterationType, true); }
			set	{ SetValue((int)OrderitemAlterationitemFieldIndex.AlterationType, value); }
		}

		/// <summary>The AlterationitemId property of the Entity OrderitemAlterationitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderitemAlterationitem"."AlterationitemId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> AlterationitemId
		{
			get { return (Nullable<System.Int32>)GetValue((int)OrderitemAlterationitemFieldIndex.AlterationitemId, false); }
			set	{ SetValue((int)OrderitemAlterationitemFieldIndex.AlterationitemId, value); }
		}

		/// <summary>The AlterationoptionName property of the Entity OrderitemAlterationitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderitemAlterationitem"."AlterationoptionName".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String AlterationoptionName
		{
			get { return (System.String)GetValue((int)OrderitemAlterationitemFieldIndex.AlterationoptionName, true); }
			set	{ SetValue((int)OrderitemAlterationitemFieldIndex.AlterationoptionName, value); }
		}

		/// <summary>The AlterationoptionType property of the Entity OrderitemAlterationitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderitemAlterationitem"."AlterationoptionType".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Crave.Enums.AlterationoptionType AlterationoptionType
		{
			get { return (Crave.Enums.AlterationoptionType)GetValue((int)OrderitemAlterationitemFieldIndex.AlterationoptionType, true); }
			set	{ SetValue((int)OrderitemAlterationitemFieldIndex.AlterationoptionType, value); }
		}

		/// <summary>The AlterationoptionPriceIn property of the Entity OrderitemAlterationitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderitemAlterationitem"."AlterationoptionPriceIn".<br/>Table field type characteristics (type, precision, scale, length): Money, 19, 4, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal AlterationoptionPriceIn
		{
			get { return (System.Decimal)GetValue((int)OrderitemAlterationitemFieldIndex.AlterationoptionPriceIn, true); }
			set	{ SetValue((int)OrderitemAlterationitemFieldIndex.AlterationoptionPriceIn, value); }
		}

		/// <summary>The Time property of the Entity OrderitemAlterationitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderitemAlterationitem"."Time".<br/>Table field type characteristics (type, precision, scale, length): DateTime, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> Time
		{
			get { return (Nullable<System.DateTime>)GetValue((int)OrderitemAlterationitemFieldIndex.Time, false); }
			set	{ SetValue((int)OrderitemAlterationitemFieldIndex.Time, value); }
		}

		/// <summary>The Value property of the Entity OrderitemAlterationitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderitemAlterationitem"."Value".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Value
		{
			get { return (System.String)GetValue((int)OrderitemAlterationitemFieldIndex.Value, true); }
			set	{ SetValue((int)OrderitemAlterationitemFieldIndex.Value, value); }
		}

		/// <summary>The FormerAlterationitemId property of the Entity OrderitemAlterationitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderitemAlterationitem"."FormerAlterationitemId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> FormerAlterationitemId
		{
			get { return (Nullable<System.Int32>)GetValue((int)OrderitemAlterationitemFieldIndex.FormerAlterationitemId, false); }
			set	{ SetValue((int)OrderitemAlterationitemFieldIndex.FormerAlterationitemId, value); }
		}

		/// <summary>The Guid property of the Entity OrderitemAlterationitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderitemAlterationitem"."Guid".<br/>Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 128.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Guid
		{
			get { return (System.String)GetValue((int)OrderitemAlterationitemFieldIndex.Guid, true); }
			set	{ SetValue((int)OrderitemAlterationitemFieldIndex.Guid, value); }
		}

		/// <summary>The PriceLevelItemId property of the Entity OrderitemAlterationitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderitemAlterationitem"."PriceLevelItemId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> PriceLevelItemId
		{
			get { return (Nullable<System.Int32>)GetValue((int)OrderitemAlterationitemFieldIndex.PriceLevelItemId, false); }
			set	{ SetValue((int)OrderitemAlterationitemFieldIndex.PriceLevelItemId, value); }
		}

		/// <summary>The PriceLevelLog property of the Entity OrderitemAlterationitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderitemAlterationitem"."PriceLevelLog".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PriceLevelLog
		{
			get { return (System.String)GetValue((int)OrderitemAlterationitemFieldIndex.PriceLevelLog, true); }
			set	{ SetValue((int)OrderitemAlterationitemFieldIndex.PriceLevelLog, value); }
		}

		/// <summary>The TimeUTC property of the Entity OrderitemAlterationitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderitemAlterationitem"."TimeUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> TimeUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)OrderitemAlterationitemFieldIndex.TimeUTC, false); }
			set	{ SetValue((int)OrderitemAlterationitemFieldIndex.TimeUTC, value); }
		}

		/// <summary>The CreatedBy property of the Entity OrderitemAlterationitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderitemAlterationitem"."CreatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)OrderitemAlterationitemFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)OrderitemAlterationitemFieldIndex.CreatedBy, value); }
		}

		/// <summary>The UpdatedBy property of the Entity OrderitemAlterationitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderitemAlterationitem"."UpdatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)OrderitemAlterationitemFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)OrderitemAlterationitemFieldIndex.UpdatedBy, value); }
		}

		/// <summary>The CreatedUTC property of the Entity OrderitemAlterationitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderitemAlterationitem"."CreatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)OrderitemAlterationitemFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)OrderitemAlterationitemFieldIndex.CreatedUTC, value); }
		}

		/// <summary>The UpdatedUTC property of the Entity OrderitemAlterationitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderitemAlterationitem"."UpdatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)OrderitemAlterationitemFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)OrderitemAlterationitemFieldIndex.UpdatedUTC, value); }
		}

		/// <summary>The OrderLevelEnabled property of the Entity OrderitemAlterationitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderitemAlterationitem"."OrderLevelEnabled".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean OrderLevelEnabled
		{
			get { return (System.Boolean)GetValue((int)OrderitemAlterationitemFieldIndex.OrderLevelEnabled, true); }
			set	{ SetValue((int)OrderitemAlterationitemFieldIndex.OrderLevelEnabled, value); }
		}

		/// <summary>The PriceInTax property of the Entity OrderitemAlterationitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderitemAlterationitem"."PriceInTax".<br/>Table field type characteristics (type, precision, scale, length): Money, 19, 4, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal PriceInTax
		{
			get { return (System.Decimal)GetValue((int)OrderitemAlterationitemFieldIndex.PriceInTax, true); }
			set	{ SetValue((int)OrderitemAlterationitemFieldIndex.PriceInTax, value); }
		}

		/// <summary>The PriceExTax property of the Entity OrderitemAlterationitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderitemAlterationitem"."PriceExTax".<br/>Table field type characteristics (type, precision, scale, length): Money, 19, 4, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal PriceExTax
		{
			get { return (System.Decimal)GetValue((int)OrderitemAlterationitemFieldIndex.PriceExTax, true); }
			set	{ SetValue((int)OrderitemAlterationitemFieldIndex.PriceExTax, value); }
		}

		/// <summary>The PriceTotalInTax property of the Entity OrderitemAlterationitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderitemAlterationitem"."PriceTotalInTax".<br/>Table field type characteristics (type, precision, scale, length): Money, 19, 4, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal PriceTotalInTax
		{
			get { return (System.Decimal)GetValue((int)OrderitemAlterationitemFieldIndex.PriceTotalInTax, true); }
			set	{ SetValue((int)OrderitemAlterationitemFieldIndex.PriceTotalInTax, value); }
		}

		/// <summary>The PriceTotalExTax property of the Entity OrderitemAlterationitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderitemAlterationitem"."PriceTotalExTax".<br/>Table field type characteristics (type, precision, scale, length): Money, 19, 4, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal PriceTotalExTax
		{
			get { return (System.Decimal)GetValue((int)OrderitemAlterationitemFieldIndex.PriceTotalExTax, true); }
			set	{ SetValue((int)OrderitemAlterationitemFieldIndex.PriceTotalExTax, value); }
		}

		/// <summary>The TaxPercentage property of the Entity OrderitemAlterationitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderitemAlterationitem"."TaxPercentage".<br/>Table field type characteristics (type, precision, scale, length): Float, 38, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Double TaxPercentage
		{
			get { return (System.Double)GetValue((int)OrderitemAlterationitemFieldIndex.TaxPercentage, true); }
			set	{ SetValue((int)OrderitemAlterationitemFieldIndex.TaxPercentage, value); }
		}

		/// <summary>The TaxTariffId property of the Entity OrderitemAlterationitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderitemAlterationitem"."TaxTariffId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> TaxTariffId
		{
			get { return (Nullable<System.Int32>)GetValue((int)OrderitemAlterationitemFieldIndex.TaxTariffId, false); }
			set	{ SetValue((int)OrderitemAlterationitemFieldIndex.TaxTariffId, value); }
		}

		/// <summary>The Tax property of the Entity OrderitemAlterationitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderitemAlterationitem"."Tax".<br/>Table field type characteristics (type, precision, scale, length): Money, 19, 4, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal Tax
		{
			get { return (System.Decimal)GetValue((int)OrderitemAlterationitemFieldIndex.Tax, true); }
			set	{ SetValue((int)OrderitemAlterationitemFieldIndex.Tax, value); }
		}

		/// <summary>The TaxTotal property of the Entity OrderitemAlterationitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderitemAlterationitem"."TaxTotal".<br/>Table field type characteristics (type, precision, scale, length): Money, 19, 4, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TaxTotal
		{
			get { return (System.Decimal)GetValue((int)OrderitemAlterationitemFieldIndex.TaxTotal, true); }
			set	{ SetValue((int)OrderitemAlterationitemFieldIndex.TaxTotal, value); }
		}

		/// <summary>The TaxName property of the Entity OrderitemAlterationitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderitemAlterationitem"."TaxName".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 200.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String TaxName
		{
			get { return (System.String)GetValue((int)OrderitemAlterationitemFieldIndex.TaxName, true); }
			set	{ SetValue((int)OrderitemAlterationitemFieldIndex.TaxName, value); }
		}

		/// <summary>The AlterationoptionProductId property of the Entity OrderitemAlterationitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderitemAlterationitem"."AlterationoptionProductId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> AlterationoptionProductId
		{
			get { return (Nullable<System.Int32>)GetValue((int)OrderitemAlterationitemFieldIndex.AlterationoptionProductId, false); }
			set	{ SetValue((int)OrderitemAlterationitemFieldIndex.AlterationoptionProductId, value); }
		}

		/// <summary>The ExternalIdentifier property of the Entity OrderitemAlterationitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderitemAlterationitem"."ExternalIdentifier".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ExternalIdentifier
		{
			get { return (System.String)GetValue((int)OrderitemAlterationitemFieldIndex.ExternalIdentifier, true); }
			set	{ SetValue((int)OrderitemAlterationitemFieldIndex.ExternalIdentifier, value); }
		}

		/// <summary>Gets the EntityCollection with the related entities of type 'OrderitemAlterationitemTagEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(OrderitemAlterationitemTagEntity))]
		public virtual EntityCollection<OrderitemAlterationitemTagEntity> OrderitemAlterationitemTagCollection { get { return GetOrCreateEntityCollection<OrderitemAlterationitemTagEntity, OrderitemAlterationitemTagEntityFactory>("OrderitemAlterationitem", true, false, ref _orderitemAlterationitemTagCollection); } }

		/// <summary>Gets / sets related entity of type 'AlterationEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual AlterationEntity Alteration
		{
			get { return _alteration; }
			set { SetSingleRelatedEntityNavigator(value, "Alteration"); }
		}

		/// <summary>Gets / sets related entity of type 'AlterationitemEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual AlterationitemEntity Alterationitem
		{
			get { return _alterationitem; }
			set { SetSingleRelatedEntityNavigator(value, "Alterationitem"); }
		}

		/// <summary>Gets / sets related entity of type 'OrderitemEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual OrderitemEntity Orderitem
		{
			get { return _orderitem; }
			set { SetSingleRelatedEntityNavigator(value, "Orderitem"); }
		}

		/// <summary>Gets / sets related entity of type 'PriceLevelItemEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual PriceLevelItemEntity PriceLevelItem
		{
			get { return _priceLevelItem; }
			set { SetSingleRelatedEntityNavigator(value, "PriceLevelItem"); }
		}

		/// <summary>Gets / sets related entity of type 'ProductEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual ProductEntity Product
		{
			get { return _product; }
			set { SetSingleRelatedEntityNavigator(value, "Product"); }
		}

		/// <summary>Gets / sets related entity of type 'TaxTariffEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual TaxTariffEntity TaxTariff
		{
			get { return _taxTariff; }
			set { SetSingleRelatedEntityNavigator(value, "TaxTariff"); }
		}


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

		public int? AlterationoptionId { get; set; }

		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen
{
	public enum OrderitemAlterationitemFieldIndex
	{
		///<summary>OrderitemAlterationitemId. </summary>
		OrderitemAlterationitemId,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>OrderitemId. </summary>
		OrderitemId,
		///<summary>AlterationId. </summary>
		AlterationId,
		///<summary>AlterationName. </summary>
		AlterationName,
		///<summary>AlterationType. </summary>
		AlterationType,
		///<summary>AlterationitemId. </summary>
		AlterationitemId,
		///<summary>AlterationoptionName. </summary>
		AlterationoptionName,
		///<summary>AlterationoptionType. </summary>
		AlterationoptionType,
		///<summary>AlterationoptionPriceIn. </summary>
		AlterationoptionPriceIn,
		///<summary>Time. </summary>
		Time,
		///<summary>Value. </summary>
		Value,
		///<summary>FormerAlterationitemId. </summary>
		FormerAlterationitemId,
		///<summary>Guid. </summary>
		Guid,
		///<summary>PriceLevelItemId. </summary>
		PriceLevelItemId,
		///<summary>PriceLevelLog. </summary>
		PriceLevelLog,
		///<summary>TimeUTC. </summary>
		TimeUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>OrderLevelEnabled. </summary>
		OrderLevelEnabled,
		///<summary>PriceInTax. </summary>
		PriceInTax,
		///<summary>PriceExTax. </summary>
		PriceExTax,
		///<summary>PriceTotalInTax. </summary>
		PriceTotalInTax,
		///<summary>PriceTotalExTax. </summary>
		PriceTotalExTax,
		///<summary>TaxPercentage. </summary>
		TaxPercentage,
		///<summary>TaxTariffId. </summary>
		TaxTariffId,
		///<summary>Tax. </summary>
		Tax,
		///<summary>TaxTotal. </summary>
		TaxTotal,
		///<summary>TaxName. </summary>
		TaxName,
		///<summary>AlterationoptionProductId. </summary>
		AlterationoptionProductId,
		///<summary>ExternalIdentifier. </summary>
		ExternalIdentifier,
		/// <summary></summary>
		AmountOfFields
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: OrderitemAlterationitem. </summary>
	public partial class OrderitemAlterationitemRelations: RelationFactory
	{
		#region Custom Relation code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomRelationCode
        private const ReferentialConstraintDeleteRule DeleteRuleForOrderitemAlterationitemTagEntityUsingOrderitemAlterationitemId = ReferentialConstraintDeleteRule.NoAction;
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		
		// private const ReferentialConstraintDeleteRule DeleteRuleForOrderitemAlterationitemTagEntityUsingOrderitemAlterationitemId = ReferentialConstraintDeleteRule


		/// <summary>Gets all relations and delete rules of the OrderitemAlterationitemEntity as a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects.</summary>
		/// <returns>a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects</returns>
		public virtual Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> GetRelationsAndDeleteRules()
		{
			Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> toReturn = new Dictionary<IEntityRelation, ReferentialConstraintDeleteRule>();
			toReturn.Add(this.OrderitemAlterationitemTagEntityUsingOrderitemAlterationitemId, OrderitemAlterationitemRelations.DeleteRuleForOrderitemAlterationitemTagEntityUsingOrderitemAlterationitemId);
			return toReturn;
		}		
	
	
		/// <summary>Returns a new IEntityRelation object, between OrderitemAlterationitemEntity and OrderitemAlterationitemTagEntity over the 1:n relation they have, using the relation between the fields: OrderitemAlterationitem.OrderitemAlterationitemId - OrderitemAlterationitemTag.OrderitemAlterationitemId</summary>
		public virtual IEntityRelation OrderitemAlterationitemTagEntityUsingOrderitemAlterationitemId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "OrderitemAlterationitemTagCollection", true, new[] { OrderitemAlterationitemFields.OrderitemAlterationitemId, OrderitemAlterationitemTagFields.OrderitemAlterationitemId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between OrderitemAlterationitemEntity and AlterationEntity over the m:1 relation they have, using the relation between the fields: OrderitemAlterationitem.AlterationId - Alteration.AlterationId</summary>
		public virtual IEntityRelation AlterationEntityUsingAlterationId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Alteration", false, new[] { AlterationFields.AlterationId, OrderitemAlterationitemFields.AlterationId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between OrderitemAlterationitemEntity and AlterationitemEntity over the m:1 relation they have, using the relation between the fields: OrderitemAlterationitem.AlterationitemId - Alterationitem.AlterationitemId</summary>
		public virtual IEntityRelation AlterationitemEntityUsingAlterationitemId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Alterationitem", false, new[] { AlterationitemFields.AlterationitemId, OrderitemAlterationitemFields.AlterationitemId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between OrderitemAlterationitemEntity and OrderitemEntity over the m:1 relation they have, using the relation between the fields: OrderitemAlterationitem.OrderitemId - Orderitem.OrderitemId</summary>
		public virtual IEntityRelation OrderitemEntityUsingOrderitemId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Orderitem", false, new[] { OrderitemFields.OrderitemId, OrderitemAlterationitemFields.OrderitemId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between OrderitemAlterationitemEntity and PriceLevelItemEntity over the m:1 relation they have, using the relation between the fields: OrderitemAlterationitem.PriceLevelItemId - PriceLevelItem.PriceLevelItemId</summary>
		public virtual IEntityRelation PriceLevelItemEntityUsingPriceLevelItemId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "PriceLevelItem", false, new[] { PriceLevelItemFields.PriceLevelItemId, OrderitemAlterationitemFields.PriceLevelItemId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between OrderitemAlterationitemEntity and ProductEntity over the m:1 relation they have, using the relation between the fields: OrderitemAlterationitem.AlterationoptionProductId - Product.ProductId</summary>
		public virtual IEntityRelation ProductEntityUsingAlterationoptionProductId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Product", false, new[] { ProductFields.ProductId, OrderitemAlterationitemFields.AlterationoptionProductId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between OrderitemAlterationitemEntity and TaxTariffEntity over the m:1 relation they have, using the relation between the fields: OrderitemAlterationitem.TaxTariffId - TaxTariff.TaxTariffId</summary>
		public virtual IEntityRelation TaxTariffEntityUsingTaxTariffId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "TaxTariff", false, new[] { TaxTariffFields.TaxTariffId, OrderitemAlterationitemFields.TaxTariffId }); }
		}

	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticOrderitemAlterationitemRelations
	{
		internal static readonly IEntityRelation OrderitemAlterationitemTagEntityUsingOrderitemAlterationitemIdStatic = new OrderitemAlterationitemRelations().OrderitemAlterationitemTagEntityUsingOrderitemAlterationitemId;
		internal static readonly IEntityRelation AlterationEntityUsingAlterationIdStatic = new OrderitemAlterationitemRelations().AlterationEntityUsingAlterationId;
		internal static readonly IEntityRelation AlterationitemEntityUsingAlterationitemIdStatic = new OrderitemAlterationitemRelations().AlterationitemEntityUsingAlterationitemId;
		internal static readonly IEntityRelation OrderitemEntityUsingOrderitemIdStatic = new OrderitemAlterationitemRelations().OrderitemEntityUsingOrderitemId;
		internal static readonly IEntityRelation PriceLevelItemEntityUsingPriceLevelItemIdStatic = new OrderitemAlterationitemRelations().PriceLevelItemEntityUsingPriceLevelItemId;
		internal static readonly IEntityRelation ProductEntityUsingAlterationoptionProductIdStatic = new OrderitemAlterationitemRelations().ProductEntityUsingAlterationoptionProductId;
		internal static readonly IEntityRelation TaxTariffEntityUsingTaxTariffIdStatic = new OrderitemAlterationitemRelations().TaxTariffEntityUsingTaxTariffId;

		/// <summary>CTor</summary>
		static StaticOrderitemAlterationitemRelations() { }
	}
}

