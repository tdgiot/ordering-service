﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 5.8
// Code is generated on: 
// Code is generated using templates: Crave.TemplateBindings
// Templates vendor: Crave Interactive
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.FactoryClasses;
using Crave.Ordering.Shared.Data.LLBLGen.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

// ReSharper disable All
namespace Crave.Ordering.Shared.Data.LLBLGen.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'Alterationitem'.<br/><br/></summary>
	[Serializable]
	public partial class AlterationitemEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END
	
	{
		private EntityCollection<AlterationitemAlterationEntity> _alterationitemAlterationCollection;
		private EntityCollection<OrderitemAlterationitemEntity> _orderitemAlterationitemCollection;
		private AlterationEntity _alteration;
		private AlterationoptionEntity _alterationoption;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		private static AlterationitemEntityStaticMetaData _staticMetaData = new AlterationitemEntityStaticMetaData();
		private static AlterationitemRelations _relationsFactory = new AlterationitemRelations();		

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Alteration</summary>
			public static readonly string Alteration = "Alteration";
			/// <summary>Member name Alterationoption</summary>
			public static readonly string Alterationoption = "Alterationoption";
			/// <summary>Member name AlterationitemAlterationCollection</summary>
			public static readonly string AlterationitemAlterationCollection = "AlterationitemAlterationCollection";
			/// <summary>Member name OrderitemAlterationitemCollection</summary>
			public static readonly string OrderitemAlterationitemCollection = "OrderitemAlterationitemCollection";
		}

		/// <summary>Static meta-data storage for navigator related information</summary>
		protected class AlterationitemEntityStaticMetaData : EntityStaticMetaDataBase
		{
			public AlterationitemEntityStaticMetaData()
			{
				SetEntityCoreInfo("AlterationitemEntity", InheritanceHierarchyType.None, false, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.AlterationitemEntity, typeof(AlterationitemEntity), typeof(AlterationitemEntityFactory), false);
				AddNavigatorMetaData<AlterationitemEntity, EntityCollection<AlterationitemAlterationEntity>>("AlterationitemAlterationCollection", a => a._alterationitemAlterationCollection, (a, b) => a._alterationitemAlterationCollection = b, a => a.AlterationitemAlterationCollection, () => new AlterationitemRelations().AlterationitemAlterationEntityUsingAlterationitemId, typeof(AlterationitemAlterationEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.AlterationitemAlterationEntity);
				AddNavigatorMetaData<AlterationitemEntity, EntityCollection<OrderitemAlterationitemEntity>>("OrderitemAlterationitemCollection", a => a._orderitemAlterationitemCollection, (a, b) => a._orderitemAlterationitemCollection = b, a => a.OrderitemAlterationitemCollection, () => new AlterationitemRelations().OrderitemAlterationitemEntityUsingAlterationitemId, typeof(OrderitemAlterationitemEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.OrderitemAlterationitemEntity);
				AddNavigatorMetaData<AlterationitemEntity, AlterationEntity>("Alteration", "AlterationitemCollection", (a, b) => a._alteration = b, a => a._alteration, (a, b) => a.Alteration = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticAlterationitemRelations.AlterationEntityUsingAlterationIdStatic, ()=>new AlterationitemRelations().AlterationEntityUsingAlterationId, null, new int[] { (int)AlterationitemFieldIndex.AlterationId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.AlterationEntity);
				AddNavigatorMetaData<AlterationitemEntity, AlterationoptionEntity>("Alterationoption", "AlterationitemCollection", (a, b) => a._alterationoption = b, a => a._alterationoption, (a, b) => a.Alterationoption = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticAlterationitemRelations.AlterationoptionEntityUsingAlterationoptionIdStatic, ()=>new AlterationitemRelations().AlterationoptionEntityUsingAlterationoptionId, null, new int[] { (int)AlterationitemFieldIndex.AlterationoptionId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.AlterationoptionEntity);
			}
		}

		/// <summary>Static ctor</summary>
		static AlterationitemEntity()
		{
		}

		/// <summary> CTor</summary>
		public AlterationitemEntity()
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public AlterationitemEntity(IEntityFields2 fields)
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this AlterationitemEntity</param>
		public AlterationitemEntity(IValidator validator)
		{
			InitClassEmpty(validator, null);
		}

		/// <summary> CTor</summary>
		/// <param name="alterationitemId">PK value for Alterationitem which data should be fetched into this Alterationitem object</param>
		public AlterationitemEntity(System.Int32 alterationitemId) : this(alterationitemId, null)
		{
		}

		/// <summary> CTor</summary>
		/// <param name="alterationitemId">PK value for Alterationitem which data should be fetched into this Alterationitem object</param>
		/// <param name="validator">The custom validator object for this AlterationitemEntity</param>
		public AlterationitemEntity(System.Int32 alterationitemId, IValidator validator)
		{
			InitClassEmpty(validator, null);
			this.AlterationitemId = alterationitemId;
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected AlterationitemEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'AlterationitemAlteration' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoAlterationitemAlterationCollection() { return CreateRelationInfoForNavigator("AlterationitemAlterationCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'OrderitemAlterationitem' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoOrderitemAlterationitemCollection() { return CreateRelationInfoForNavigator("OrderitemAlterationitemCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Alteration' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoAlteration() { return CreateRelationInfoForNavigator("Alteration"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Alterationoption' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoAlterationoption() { return CreateRelationInfoForNavigator("Alterationoption"); }
		
		/// <inheritdoc/>
		protected override EntityStaticMetaDataBase GetEntityStaticMetaData() {	return _staticMetaData; }

		/// <summary>Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		/// <summary>Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this AlterationitemEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary>The relations object holding all relations of this entity with other entity classes.</summary>
		public static AlterationitemRelations Relations { get { return _relationsFactory; } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'AlterationitemAlteration' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathAlterationitemAlterationCollection { get { return _staticMetaData.GetPrefetchPathElement("AlterationitemAlterationCollection", CommonEntityBase.CreateEntityCollection<AlterationitemAlterationEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'OrderitemAlterationitem' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathOrderitemAlterationitemCollection { get { return _staticMetaData.GetPrefetchPathElement("OrderitemAlterationitemCollection", CommonEntityBase.CreateEntityCollection<OrderitemAlterationitemEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Alteration' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathAlterationEntity { get { return _staticMetaData.GetPrefetchPathElement("Alteration", CommonEntityBase.CreateEntityCollection<AlterationEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Alterationoption' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathAlterationoptionEntity { get { return _staticMetaData.GetPrefetchPathElement("Alterationoption", CommonEntityBase.CreateEntityCollection<AlterationoptionEntity>()); } }

		/// <summary>The AlterationitemId property of the Entity Alterationitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alterationitem"."AlterationitemId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 AlterationitemId
		{
			get { return (System.Int32)GetValue((int)AlterationitemFieldIndex.AlterationitemId, true); }
			set { SetValue((int)AlterationitemFieldIndex.AlterationitemId, value); }		}

		/// <summary>The ParentCompanyId property of the Entity Alterationitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alterationitem"."ParentCompanyId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ParentCompanyId
		{
			get { return (Nullable<System.Int32>)GetValue((int)AlterationitemFieldIndex.ParentCompanyId, false); }
			set	{ SetValue((int)AlterationitemFieldIndex.ParentCompanyId, value); }
		}

		/// <summary>The Version property of the Entity Alterationitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alterationitem"."Version".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 Version
		{
			get { return (System.Int32)GetValue((int)AlterationitemFieldIndex.Version, true); }
			set	{ SetValue((int)AlterationitemFieldIndex.Version, value); }
		}

		/// <summary>The AlterationId property of the Entity Alterationitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alterationitem"."AlterationId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 AlterationId
		{
			get { return (System.Int32)GetValue((int)AlterationitemFieldIndex.AlterationId, true); }
			set	{ SetValue((int)AlterationitemFieldIndex.AlterationId, value); }
		}

		/// <summary>The AlterationoptionId property of the Entity Alterationitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alterationitem"."AlterationoptionId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 AlterationoptionId
		{
			get { return (System.Int32)GetValue((int)AlterationitemFieldIndex.AlterationoptionId, true); }
			set	{ SetValue((int)AlterationitemFieldIndex.AlterationoptionId, value); }
		}

		/// <summary>The SelectedOnDefault property of the Entity Alterationitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alterationitem"."SelectedOnDefault".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean SelectedOnDefault
		{
			get { return (System.Boolean)GetValue((int)AlterationitemFieldIndex.SelectedOnDefault, true); }
			set	{ SetValue((int)AlterationitemFieldIndex.SelectedOnDefault, value); }
		}

		/// <summary>The SortOrder property of the Entity Alterationitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alterationitem"."SortOrder".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 SortOrder
		{
			get { return (System.Int32)GetValue((int)AlterationitemFieldIndex.SortOrder, true); }
			set	{ SetValue((int)AlterationitemFieldIndex.SortOrder, value); }
		}

		/// <summary>The PosalterationitemId property of the Entity Alterationitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alterationitem"."PosalterationitemId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> PosalterationitemId
		{
			get { return (Nullable<System.Int32>)GetValue((int)AlterationitemFieldIndex.PosalterationitemId, false); }
			set	{ SetValue((int)AlterationitemFieldIndex.PosalterationitemId, value); }
		}

		/// <summary>The GenericalterationitemId property of the Entity Alterationitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alterationitem"."GenericalterationitemId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> GenericalterationitemId
		{
			get { return (Nullable<System.Int32>)GetValue((int)AlterationitemFieldIndex.GenericalterationitemId, false); }
			set	{ SetValue((int)AlterationitemFieldIndex.GenericalterationitemId, value); }
		}

		/// <summary>The CreatedUTC property of the Entity Alterationitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alterationitem"."CreatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)AlterationitemFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)AlterationitemFieldIndex.CreatedUTC, value); }
		}

		/// <summary>The CreatedBy property of the Entity Alterationitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alterationitem"."CreatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CreatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)AlterationitemFieldIndex.CreatedBy, false); }
			set	{ SetValue((int)AlterationitemFieldIndex.CreatedBy, value); }
		}

		/// <summary>The UpdatedUTC property of the Entity Alterationitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alterationitem"."UpdatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)AlterationitemFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)AlterationitemFieldIndex.UpdatedUTC, value); }
		}

		/// <summary>The UpdatedBy property of the Entity Alterationitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alterationitem"."UpdatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UpdatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)AlterationitemFieldIndex.UpdatedBy, false); }
			set	{ SetValue((int)AlterationitemFieldIndex.UpdatedBy, value); }
		}

		/// <summary>Gets the EntityCollection with the related entities of type 'AlterationitemAlterationEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(AlterationitemAlterationEntity))]
		public virtual EntityCollection<AlterationitemAlterationEntity> AlterationitemAlterationCollection { get { return GetOrCreateEntityCollection<AlterationitemAlterationEntity, AlterationitemAlterationEntityFactory>("Alterationitem", true, false, ref _alterationitemAlterationCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'OrderitemAlterationitemEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(OrderitemAlterationitemEntity))]
		public virtual EntityCollection<OrderitemAlterationitemEntity> OrderitemAlterationitemCollection { get { return GetOrCreateEntityCollection<OrderitemAlterationitemEntity, OrderitemAlterationitemEntityFactory>("Alterationitem", true, false, ref _orderitemAlterationitemCollection); } }

		/// <summary>Gets / sets related entity of type 'AlterationEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual AlterationEntity Alteration
		{
			get { return _alteration; }
			set { SetSingleRelatedEntityNavigator(value, "Alteration"); }
		}

		/// <summary>Gets / sets related entity of type 'AlterationoptionEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual AlterationoptionEntity Alterationoption
		{
			get { return _alterationoption; }
			set { SetSingleRelatedEntityNavigator(value, "Alterationoption"); }
		}


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen
{
	public enum AlterationitemFieldIndex
	{
		///<summary>AlterationitemId. </summary>
		AlterationitemId,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>Version. </summary>
		Version,
		///<summary>AlterationId. </summary>
		AlterationId,
		///<summary>AlterationoptionId. </summary>
		AlterationoptionId,
		///<summary>SelectedOnDefault. </summary>
		SelectedOnDefault,
		///<summary>SortOrder. </summary>
		SortOrder,
		///<summary>PosalterationitemId. </summary>
		PosalterationitemId,
		///<summary>GenericalterationitemId. </summary>
		GenericalterationitemId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		/// <summary></summary>
		AmountOfFields
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Alterationitem. </summary>
	public partial class AlterationitemRelations: RelationFactory
	{
		#region Custom Relation code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomRelationCode
        private const ReferentialConstraintDeleteRule DeleteRuleForAlterationitemAlterationEntityUsingAlterationitemId = ReferentialConstraintDeleteRule.Cascade;
        private const ReferentialConstraintDeleteRule DeleteRuleForOrderitemAlterationitemEntityUsingAlterationitemId = ReferentialConstraintDeleteRule.SetNull;
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		
		// private const ReferentialConstraintDeleteRule DeleteRuleForAlterationitemAlterationEntityUsingAlterationitemId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForOrderitemAlterationitemEntityUsingAlterationitemId = ReferentialConstraintDeleteRule


		/// <summary>Gets all relations and delete rules of the AlterationitemEntity as a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects.</summary>
		/// <returns>a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects</returns>
		public virtual Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> GetRelationsAndDeleteRules()
		{
			Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> toReturn = new Dictionary<IEntityRelation, ReferentialConstraintDeleteRule>();
			toReturn.Add(this.AlterationitemAlterationEntityUsingAlterationitemId, AlterationitemRelations.DeleteRuleForAlterationitemAlterationEntityUsingAlterationitemId);
			toReturn.Add(this.OrderitemAlterationitemEntityUsingAlterationitemId, AlterationitemRelations.DeleteRuleForOrderitemAlterationitemEntityUsingAlterationitemId);
			return toReturn;
		}		
	
	
		/// <summary>Returns a new IEntityRelation object, between AlterationitemEntity and AlterationitemAlterationEntity over the 1:n relation they have, using the relation between the fields: Alterationitem.AlterationitemId - AlterationitemAlteration.AlterationitemId</summary>
		public virtual IEntityRelation AlterationitemAlterationEntityUsingAlterationitemId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "AlterationitemAlterationCollection", true, new[] { AlterationitemFields.AlterationitemId, AlterationitemAlterationFields.AlterationitemId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between AlterationitemEntity and OrderitemAlterationitemEntity over the 1:n relation they have, using the relation between the fields: Alterationitem.AlterationitemId - OrderitemAlterationitem.AlterationitemId</summary>
		public virtual IEntityRelation OrderitemAlterationitemEntityUsingAlterationitemId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "OrderitemAlterationitemCollection", true, new[] { AlterationitemFields.AlterationitemId, OrderitemAlterationitemFields.AlterationitemId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between AlterationitemEntity and AlterationEntity over the m:1 relation they have, using the relation between the fields: Alterationitem.AlterationId - Alteration.AlterationId</summary>
		public virtual IEntityRelation AlterationEntityUsingAlterationId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Alteration", false, new[] { AlterationFields.AlterationId, AlterationitemFields.AlterationId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between AlterationitemEntity and AlterationoptionEntity over the m:1 relation they have, using the relation between the fields: Alterationitem.AlterationoptionId - Alterationoption.AlterationoptionId</summary>
		public virtual IEntityRelation AlterationoptionEntityUsingAlterationoptionId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Alterationoption", false, new[] { AlterationoptionFields.AlterationoptionId, AlterationitemFields.AlterationoptionId }); }
		}

	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticAlterationitemRelations
	{
		internal static readonly IEntityRelation AlterationitemAlterationEntityUsingAlterationitemIdStatic = new AlterationitemRelations().AlterationitemAlterationEntityUsingAlterationitemId;
		internal static readonly IEntityRelation OrderitemAlterationitemEntityUsingAlterationitemIdStatic = new AlterationitemRelations().OrderitemAlterationitemEntityUsingAlterationitemId;
		internal static readonly IEntityRelation AlterationEntityUsingAlterationIdStatic = new AlterationitemRelations().AlterationEntityUsingAlterationId;
		internal static readonly IEntityRelation AlterationoptionEntityUsingAlterationoptionIdStatic = new AlterationitemRelations().AlterationoptionEntityUsingAlterationoptionId;

		/// <summary>CTor</summary>
		static StaticAlterationitemRelations() { }
	}
}

