﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 5.8
// Code is generated on: 
// Code is generated using templates: Crave.TemplateBindings
// Templates vendor: Crave Interactive
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.FactoryClasses;
using Crave.Ordering.Shared.Data.LLBLGen.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

// ReSharper disable All
namespace Crave.Ordering.Shared.Data.LLBLGen.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'Supportagent'.<br/><br/></summary>
	[Serializable]
	public partial class SupportagentEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END
	
	{
		private EntityCollection<SupportpoolSupportagentEntity> _supportpoolSupportagentCollection;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		private static SupportagentEntityStaticMetaData _staticMetaData = new SupportagentEntityStaticMetaData();
		private static SupportagentRelations _relationsFactory = new SupportagentRelations();		

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name SupportpoolSupportagentCollection</summary>
			public static readonly string SupportpoolSupportagentCollection = "SupportpoolSupportagentCollection";
		}

		/// <summary>Static meta-data storage for navigator related information</summary>
		protected class SupportagentEntityStaticMetaData : EntityStaticMetaDataBase
		{
			public SupportagentEntityStaticMetaData()
			{
				SetEntityCoreInfo("SupportagentEntity", InheritanceHierarchyType.None, false, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.SupportagentEntity, typeof(SupportagentEntity), typeof(SupportagentEntityFactory), false);
				AddNavigatorMetaData<SupportagentEntity, EntityCollection<SupportpoolSupportagentEntity>>("SupportpoolSupportagentCollection", a => a._supportpoolSupportagentCollection, (a, b) => a._supportpoolSupportagentCollection = b, a => a.SupportpoolSupportagentCollection, () => new SupportagentRelations().SupportpoolSupportagentEntityUsingSupportagentId, typeof(SupportpoolSupportagentEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.SupportpoolSupportagentEntity);
			}
		}

		/// <summary>Static ctor</summary>
		static SupportagentEntity()
		{
		}

		/// <summary> CTor</summary>
		public SupportagentEntity()
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public SupportagentEntity(IEntityFields2 fields)
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this SupportagentEntity</param>
		public SupportagentEntity(IValidator validator)
		{
			InitClassEmpty(validator, null);
		}

		/// <summary> CTor</summary>
		/// <param name="supportagentId">PK value for Supportagent which data should be fetched into this Supportagent object</param>
		public SupportagentEntity(System.Int32 supportagentId) : this(supportagentId, null)
		{
		}

		/// <summary> CTor</summary>
		/// <param name="supportagentId">PK value for Supportagent which data should be fetched into this Supportagent object</param>
		/// <param name="validator">The custom validator object for this SupportagentEntity</param>
		public SupportagentEntity(System.Int32 supportagentId, IValidator validator)
		{
			InitClassEmpty(validator, null);
			this.SupportagentId = supportagentId;
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected SupportagentEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'SupportpoolSupportagent' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoSupportpoolSupportagentCollection() { return CreateRelationInfoForNavigator("SupportpoolSupportagentCollection"); }
		
		/// <inheritdoc/>
		protected override EntityStaticMetaDataBase GetEntityStaticMetaData() {	return _staticMetaData; }

		/// <summary>Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		/// <summary>Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this SupportagentEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary>The relations object holding all relations of this entity with other entity classes.</summary>
		public static SupportagentRelations Relations { get { return _relationsFactory; } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'SupportpoolSupportagent' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathSupportpoolSupportagentCollection { get { return _staticMetaData.GetPrefetchPathElement("SupportpoolSupportagentCollection", CommonEntityBase.CreateEntityCollection<SupportpoolSupportagentEntity>()); } }

		/// <summary>The SupportagentId property of the Entity Supportagent<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Supportagent"."SupportagentId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 SupportagentId
		{
			get { return (System.Int32)GetValue((int)SupportagentFieldIndex.SupportagentId, true); }
			set { SetValue((int)SupportagentFieldIndex.SupportagentId, value); }		}

		/// <summary>The Firstname property of the Entity Supportagent<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Supportagent"."Firstname".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Firstname
		{
			get { return (System.String)GetValue((int)SupportagentFieldIndex.Firstname, true); }
			set	{ SetValue((int)SupportagentFieldIndex.Firstname, value); }
		}

		/// <summary>The Lastname property of the Entity Supportagent<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Supportagent"."Lastname".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Lastname
		{
			get { return (System.String)GetValue((int)SupportagentFieldIndex.Lastname, true); }
			set	{ SetValue((int)SupportagentFieldIndex.Lastname, value); }
		}

		/// <summary>The LastnamePrefix property of the Entity Supportagent<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Supportagent"."LastnamePrefix".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String LastnamePrefix
		{
			get { return (System.String)GetValue((int)SupportagentFieldIndex.LastnamePrefix, true); }
			set	{ SetValue((int)SupportagentFieldIndex.LastnamePrefix, value); }
		}

		/// <summary>The Email property of the Entity Supportagent<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Supportagent"."Email".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Email
		{
			get { return (System.String)GetValue((int)SupportagentFieldIndex.Email, true); }
			set	{ SetValue((int)SupportagentFieldIndex.Email, value); }
		}

		/// <summary>The Phonenumber property of the Entity Supportagent<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Supportagent"."Phonenumber".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Phonenumber
		{
			get { return (System.String)GetValue((int)SupportagentFieldIndex.Phonenumber, true); }
			set	{ SetValue((int)SupportagentFieldIndex.Phonenumber, value); }
		}

		/// <summary>The SlackUsername property of the Entity Supportagent<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Supportagent"."SlackUsername".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String SlackUsername
		{
			get { return (System.String)GetValue((int)SupportagentFieldIndex.SlackUsername, true); }
			set	{ SetValue((int)SupportagentFieldIndex.SlackUsername, value); }
		}

		/// <summary>The Active property of the Entity Supportagent<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Supportagent"."Active".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean Active
		{
			get { return (System.Boolean)GetValue((int)SupportagentFieldIndex.Active, true); }
			set	{ SetValue((int)SupportagentFieldIndex.Active, value); }
		}

		/// <summary>The CreatedUTC property of the Entity Supportagent<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Supportagent"."CreatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)SupportagentFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)SupportagentFieldIndex.CreatedUTC, value); }
		}

		/// <summary>The CreatedBy property of the Entity Supportagent<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Supportagent"."CreatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CreatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)SupportagentFieldIndex.CreatedBy, false); }
			set	{ SetValue((int)SupportagentFieldIndex.CreatedBy, value); }
		}

		/// <summary>The UpdatedUTC property of the Entity Supportagent<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Supportagent"."UpdatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)SupportagentFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)SupportagentFieldIndex.UpdatedUTC, value); }
		}

		/// <summary>The UpdatedBy property of the Entity Supportagent<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Supportagent"."UpdatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UpdatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)SupportagentFieldIndex.UpdatedBy, false); }
			set	{ SetValue((int)SupportagentFieldIndex.UpdatedBy, value); }
		}

		/// <summary>Gets the EntityCollection with the related entities of type 'SupportpoolSupportagentEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(SupportpoolSupportagentEntity))]
		public virtual EntityCollection<SupportpoolSupportagentEntity> SupportpoolSupportagentCollection { get { return GetOrCreateEntityCollection<SupportpoolSupportagentEntity, SupportpoolSupportagentEntityFactory>("Supportagent", true, false, ref _supportpoolSupportagentCollection); } }


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen
{
	public enum SupportagentFieldIndex
	{
		///<summary>SupportagentId. </summary>
		SupportagentId,
		///<summary>Firstname. </summary>
		Firstname,
		///<summary>Lastname. </summary>
		Lastname,
		///<summary>LastnamePrefix. </summary>
		LastnamePrefix,
		///<summary>Email. </summary>
		Email,
		///<summary>Phonenumber. </summary>
		Phonenumber,
		///<summary>SlackUsername. </summary>
		SlackUsername,
		///<summary>Active. </summary>
		Active,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		/// <summary></summary>
		AmountOfFields
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Supportagent. </summary>
	public partial class SupportagentRelations: RelationFactory
	{
		#region Custom Relation code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomRelationCode
        private const ReferentialConstraintDeleteRule DeleteRuleForSupportpoolSupportagentEntityUsingSupportagentId = ReferentialConstraintDeleteRule.Cascade;
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		
		// private const ReferentialConstraintDeleteRule DeleteRuleForSupportpoolSupportagentEntityUsingSupportagentId = ReferentialConstraintDeleteRule


		/// <summary>Gets all relations and delete rules of the SupportagentEntity as a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects.</summary>
		/// <returns>a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects</returns>
		public virtual Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> GetRelationsAndDeleteRules()
		{
			Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> toReturn = new Dictionary<IEntityRelation, ReferentialConstraintDeleteRule>();
			toReturn.Add(this.SupportpoolSupportagentEntityUsingSupportagentId, SupportagentRelations.DeleteRuleForSupportpoolSupportagentEntityUsingSupportagentId);
			return toReturn;
		}		
	
	
		/// <summary>Returns a new IEntityRelation object, between SupportagentEntity and SupportpoolSupportagentEntity over the 1:n relation they have, using the relation between the fields: Supportagent.SupportagentId - SupportpoolSupportagent.SupportagentId</summary>
		public virtual IEntityRelation SupportpoolSupportagentEntityUsingSupportagentId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "SupportpoolSupportagentCollection", true, new[] { SupportagentFields.SupportagentId, SupportpoolSupportagentFields.SupportagentId }); }
		}

	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticSupportagentRelations
	{
		internal static readonly IEntityRelation SupportpoolSupportagentEntityUsingSupportagentIdStatic = new SupportagentRelations().SupportpoolSupportagentEntityUsingSupportagentId;

		/// <summary>CTor</summary>
		static StaticSupportagentRelations() { }
	}
}

