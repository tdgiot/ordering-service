﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 5.8
// Code is generated on: 
// Code is generated using templates: Crave.TemplateBindings
// Templates vendor: Crave Interactive
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.FactoryClasses;
using Crave.Ordering.Shared.Data.LLBLGen.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

// ReSharper disable All
namespace Crave.Ordering.Shared.Data.LLBLGen.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'Route'.<br/><br/></summary>
	[Serializable]
	public partial class RouteEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END
	
	{
		private EntityCollection<CategoryEntity> _categoryCollection;
		private EntityCollection<ClientConfigurationRouteEntity> _clientConfigurationRouteCollection;
		private EntityCollection<CompanyEntity> _companyCollection;
		private EntityCollection<DeliverypointgroupEntity> _deliverypointgroupCollection1;
		private EntityCollection<DeliverypointgroupEntity> _deliverypointgroupCollection2;
		private EntityCollection<DeliverypointgroupEntity> _deliverypointgroupCollection;
		private EntityCollection<DeliverypointgroupEntity> _deliverypointgroupCollection3;
		private EntityCollection<ProductEntity> _productCollection;
		private EntityCollection<RouteEntity> _routeCollection;
		private EntityCollection<RoutestepEntity> _routestepCollection;
		private CompanyEntity _company;
		private RouteEntity _escalationRoute;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		private static RouteEntityStaticMetaData _staticMetaData = new RouteEntityStaticMetaData();
		private static RouteRelations _relationsFactory = new RouteRelations();		

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Company</summary>
			public static readonly string Company = "Company";
			/// <summary>Member name EscalationRoute</summary>
			public static readonly string EscalationRoute = "EscalationRoute";
			/// <summary>Member name CategoryCollection</summary>
			public static readonly string CategoryCollection = "CategoryCollection";
			/// <summary>Member name ClientConfigurationRouteCollection</summary>
			public static readonly string ClientConfigurationRouteCollection = "ClientConfigurationRouteCollection";
			/// <summary>Member name CompanyCollection</summary>
			public static readonly string CompanyCollection = "CompanyCollection";
			/// <summary>Member name DeliverypointgroupCollection1</summary>
			public static readonly string DeliverypointgroupCollection1 = "DeliverypointgroupCollection1";
			/// <summary>Member name DeliverypointgroupCollection2</summary>
			public static readonly string DeliverypointgroupCollection2 = "DeliverypointgroupCollection2";
			/// <summary>Member name DeliverypointgroupCollection</summary>
			public static readonly string DeliverypointgroupCollection = "DeliverypointgroupCollection";
			/// <summary>Member name DeliverypointgroupCollection3</summary>
			public static readonly string DeliverypointgroupCollection3 = "DeliverypointgroupCollection3";
			/// <summary>Member name ProductCollection</summary>
			public static readonly string ProductCollection = "ProductCollection";
			/// <summary>Member name RouteCollection</summary>
			public static readonly string RouteCollection = "RouteCollection";
			/// <summary>Member name RoutestepCollection</summary>
			public static readonly string RoutestepCollection = "RoutestepCollection";
		}

		/// <summary>Static meta-data storage for navigator related information</summary>
		protected class RouteEntityStaticMetaData : EntityStaticMetaDataBase
		{
			public RouteEntityStaticMetaData()
			{
				SetEntityCoreInfo("RouteEntity", InheritanceHierarchyType.None, false, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.RouteEntity, typeof(RouteEntity), typeof(RouteEntityFactory), false);
				AddNavigatorMetaData<RouteEntity, EntityCollection<CategoryEntity>>("CategoryCollection", a => a._categoryCollection, (a, b) => a._categoryCollection = b, a => a.CategoryCollection, () => new RouteRelations().CategoryEntityUsingRouteId, typeof(CategoryEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.CategoryEntity);
				AddNavigatorMetaData<RouteEntity, EntityCollection<ClientConfigurationRouteEntity>>("ClientConfigurationRouteCollection", a => a._clientConfigurationRouteCollection, (a, b) => a._clientConfigurationRouteCollection = b, a => a.ClientConfigurationRouteCollection, () => new RouteRelations().ClientConfigurationRouteEntityUsingRouteId, typeof(ClientConfigurationRouteEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ClientConfigurationRouteEntity);
				AddNavigatorMetaData<RouteEntity, EntityCollection<CompanyEntity>>("CompanyCollection", a => a._companyCollection, (a, b) => a._companyCollection = b, a => a.CompanyCollection, () => new RouteRelations().CompanyEntityUsingRouteId, typeof(CompanyEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.CompanyEntity);
				AddNavigatorMetaData<RouteEntity, EntityCollection<DeliverypointgroupEntity>>("DeliverypointgroupCollection1", a => a._deliverypointgroupCollection1, (a, b) => a._deliverypointgroupCollection1 = b, a => a.DeliverypointgroupCollection1, () => new RouteRelations().DeliverypointgroupEntityUsingEmailDocumentRouteId, typeof(DeliverypointgroupEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.DeliverypointgroupEntity);
				AddNavigatorMetaData<RouteEntity, EntityCollection<DeliverypointgroupEntity>>("DeliverypointgroupCollection2", a => a._deliverypointgroupCollection2, (a, b) => a._deliverypointgroupCollection2 = b, a => a.DeliverypointgroupCollection2, () => new RouteRelations().DeliverypointgroupEntityUsingOrderNotesRouteId, typeof(DeliverypointgroupEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.DeliverypointgroupEntity);
				AddNavigatorMetaData<RouteEntity, EntityCollection<DeliverypointgroupEntity>>("DeliverypointgroupCollection", a => a._deliverypointgroupCollection, (a, b) => a._deliverypointgroupCollection = b, a => a.DeliverypointgroupCollection, () => new RouteRelations().DeliverypointgroupEntityUsingRouteId, typeof(DeliverypointgroupEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.DeliverypointgroupEntity);
				AddNavigatorMetaData<RouteEntity, EntityCollection<DeliverypointgroupEntity>>("DeliverypointgroupCollection3", a => a._deliverypointgroupCollection3, (a, b) => a._deliverypointgroupCollection3 = b, a => a.DeliverypointgroupCollection3, () => new RouteRelations().DeliverypointgroupEntityUsingSystemMessageRouteId, typeof(DeliverypointgroupEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.DeliverypointgroupEntity);
				AddNavigatorMetaData<RouteEntity, EntityCollection<ProductEntity>>("ProductCollection", a => a._productCollection, (a, b) => a._productCollection = b, a => a.ProductCollection, () => new RouteRelations().ProductEntityUsingRouteId, typeof(ProductEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ProductEntity);
				AddNavigatorMetaData<RouteEntity, EntityCollection<RouteEntity>>("RouteCollection", a => a._routeCollection, (a, b) => a._routeCollection = b, a => a.RouteCollection, () => new RouteRelations().RouteEntityUsingEscalationRouteId, typeof(RouteEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.RouteEntity);
				AddNavigatorMetaData<RouteEntity, EntityCollection<RoutestepEntity>>("RoutestepCollection", a => a._routestepCollection, (a, b) => a._routestepCollection = b, a => a.RoutestepCollection, () => new RouteRelations().RoutestepEntityUsingRouteId, typeof(RoutestepEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.RoutestepEntity);
				AddNavigatorMetaData<RouteEntity, CompanyEntity>("Company", "RouteCollection", (a, b) => a._company = b, a => a._company, (a, b) => a.Company = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticRouteRelations.CompanyEntityUsingCompanyIdStatic, ()=>new RouteRelations().CompanyEntityUsingCompanyId, null, new int[] { (int)RouteFieldIndex.CompanyId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.CompanyEntity);
				AddNavigatorMetaData<RouteEntity, RouteEntity>("EscalationRoute", "RouteCollection", (a, b) => a._escalationRoute = b, a => a._escalationRoute, (a, b) => a.EscalationRoute = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticRouteRelations.RouteEntityUsingRouteIdEscalationRouteIdStatic, ()=>new RouteRelations().RouteEntityUsingRouteIdEscalationRouteId, null, new int[] { (int)RouteFieldIndex.EscalationRouteId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.RouteEntity);
			}
		}

		/// <summary>Static ctor</summary>
		static RouteEntity()
		{
		}

		/// <summary> CTor</summary>
		public RouteEntity()
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public RouteEntity(IEntityFields2 fields)
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this RouteEntity</param>
		public RouteEntity(IValidator validator)
		{
			InitClassEmpty(validator, null);
		}

		/// <summary> CTor</summary>
		/// <param name="routeId">PK value for Route which data should be fetched into this Route object</param>
		public RouteEntity(System.Int32 routeId) : this(routeId, null)
		{
		}

		/// <summary> CTor</summary>
		/// <param name="routeId">PK value for Route which data should be fetched into this Route object</param>
		/// <param name="validator">The custom validator object for this RouteEntity</param>
		public RouteEntity(System.Int32 routeId, IValidator validator)
		{
			InitClassEmpty(validator, null);
			this.RouteId = routeId;
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected RouteEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Category' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoCategoryCollection() { return CreateRelationInfoForNavigator("CategoryCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'ClientConfigurationRoute' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoClientConfigurationRouteCollection() { return CreateRelationInfoForNavigator("ClientConfigurationRouteCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Company' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoCompanyCollection() { return CreateRelationInfoForNavigator("CompanyCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Deliverypointgroup' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoDeliverypointgroupCollection1() { return CreateRelationInfoForNavigator("DeliverypointgroupCollection1"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Deliverypointgroup' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoDeliverypointgroupCollection2() { return CreateRelationInfoForNavigator("DeliverypointgroupCollection2"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Deliverypointgroup' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoDeliverypointgroupCollection() { return CreateRelationInfoForNavigator("DeliverypointgroupCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Deliverypointgroup' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoDeliverypointgroupCollection3() { return CreateRelationInfoForNavigator("DeliverypointgroupCollection3"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Product' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoProductCollection() { return CreateRelationInfoForNavigator("ProductCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Route' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoRouteCollection() { return CreateRelationInfoForNavigator("RouteCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Routestep' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoRoutestepCollection() { return CreateRelationInfoForNavigator("RoutestepCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Company' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoCompany() { return CreateRelationInfoForNavigator("Company"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Route' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoEscalationRoute() { return CreateRelationInfoForNavigator("EscalationRoute"); }
		
		/// <inheritdoc/>
		protected override EntityStaticMetaDataBase GetEntityStaticMetaData() {	return _staticMetaData; }

		/// <summary>Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		/// <summary>Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this RouteEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary>The relations object holding all relations of this entity with other entity classes.</summary>
		public static RouteRelations Relations { get { return _relationsFactory; } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Category' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathCategoryCollection { get { return _staticMetaData.GetPrefetchPathElement("CategoryCollection", CommonEntityBase.CreateEntityCollection<CategoryEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'ClientConfigurationRoute' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathClientConfigurationRouteCollection { get { return _staticMetaData.GetPrefetchPathElement("ClientConfigurationRouteCollection", CommonEntityBase.CreateEntityCollection<ClientConfigurationRouteEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Company' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathCompanyCollection { get { return _staticMetaData.GetPrefetchPathElement("CompanyCollection", CommonEntityBase.CreateEntityCollection<CompanyEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Deliverypointgroup' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathDeliverypointgroupCollection1 { get { return _staticMetaData.GetPrefetchPathElement("DeliverypointgroupCollection1", CommonEntityBase.CreateEntityCollection<DeliverypointgroupEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Deliverypointgroup' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathDeliverypointgroupCollection2 { get { return _staticMetaData.GetPrefetchPathElement("DeliverypointgroupCollection2", CommonEntityBase.CreateEntityCollection<DeliverypointgroupEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Deliverypointgroup' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathDeliverypointgroupCollection { get { return _staticMetaData.GetPrefetchPathElement("DeliverypointgroupCollection", CommonEntityBase.CreateEntityCollection<DeliverypointgroupEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Deliverypointgroup' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathDeliverypointgroupCollection3 { get { return _staticMetaData.GetPrefetchPathElement("DeliverypointgroupCollection3", CommonEntityBase.CreateEntityCollection<DeliverypointgroupEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Product' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathProductCollection { get { return _staticMetaData.GetPrefetchPathElement("ProductCollection", CommonEntityBase.CreateEntityCollection<ProductEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Route' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathRouteCollection { get { return _staticMetaData.GetPrefetchPathElement("RouteCollection", CommonEntityBase.CreateEntityCollection<RouteEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Routestep' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathRoutestepCollection { get { return _staticMetaData.GetPrefetchPathElement("RoutestepCollection", CommonEntityBase.CreateEntityCollection<RoutestepEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Company' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathCompanyEntity { get { return _staticMetaData.GetPrefetchPathElement("Company", CommonEntityBase.CreateEntityCollection<CompanyEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Route' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathEscalationRouteEntity { get { return _staticMetaData.GetPrefetchPathElement("EscalationRoute", CommonEntityBase.CreateEntityCollection<RouteEntity>()); } }

		/// <summary>The RouteId property of the Entity Route<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Route"."RouteId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 RouteId
		{
			get { return (System.Int32)GetValue((int)RouteFieldIndex.RouteId, true); }
			set { SetValue((int)RouteFieldIndex.RouteId, value); }		}

		/// <summary>The CompanyId property of the Entity Route<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Route"."CompanyId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CompanyId
		{
			get { return (System.Int32)GetValue((int)RouteFieldIndex.CompanyId, true); }
			set	{ SetValue((int)RouteFieldIndex.CompanyId, value); }
		}

		/// <summary>The Name property of the Entity Route<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Route"."Name".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)RouteFieldIndex.Name, true); }
			set	{ SetValue((int)RouteFieldIndex.Name, value); }
		}

		/// <summary>The StepTimeoutMinutes property of the Entity Route<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Route"."StepTimeoutMinutes".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 StepTimeoutMinutes
		{
			get { return (System.Int32)GetValue((int)RouteFieldIndex.StepTimeoutMinutes, true); }
			set	{ SetValue((int)RouteFieldIndex.StepTimeoutMinutes, value); }
		}

		/// <summary>The RetrievalSupportNotificationTimeoutMinutes property of the Entity Route<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Route"."RetrievalSupportNotificationTimeoutMinutes".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> RetrievalSupportNotificationTimeoutMinutes
		{
			get { return (Nullable<System.Int32>)GetValue((int)RouteFieldIndex.RetrievalSupportNotificationTimeoutMinutes, false); }
			set	{ SetValue((int)RouteFieldIndex.RetrievalSupportNotificationTimeoutMinutes, value); }
		}

		/// <summary>The BeingHandledSupportNotificationTimeoutMinutes property of the Entity Route<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Route"."BeingHandledSupportNotificationTimeoutMinutes".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> BeingHandledSupportNotificationTimeoutMinutes
		{
			get { return (Nullable<System.Int32>)GetValue((int)RouteFieldIndex.BeingHandledSupportNotificationTimeoutMinutes, false); }
			set	{ SetValue((int)RouteFieldIndex.BeingHandledSupportNotificationTimeoutMinutes, value); }
		}

		/// <summary>The LogAlways property of the Entity Route<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Route"."LogAlways".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean LogAlways
		{
			get { return (System.Boolean)GetValue((int)RouteFieldIndex.LogAlways, true); }
			set	{ SetValue((int)RouteFieldIndex.LogAlways, value); }
		}

		/// <summary>The EscalationRouteId property of the Entity Route<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Route"."EscalationRouteId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> EscalationRouteId
		{
			get { return (Nullable<System.Int32>)GetValue((int)RouteFieldIndex.EscalationRouteId, false); }
			set	{ SetValue((int)RouteFieldIndex.EscalationRouteId, value); }
		}

		/// <summary>The CreatedBy property of the Entity Route<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Route"."CreatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)RouteFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)RouteFieldIndex.CreatedBy, value); }
		}

		/// <summary>The UpdatedBy property of the Entity Route<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Route"."UpdatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)RouteFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)RouteFieldIndex.UpdatedBy, value); }
		}

		/// <summary>The CreatedUTC property of the Entity Route<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Route"."CreatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)RouteFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)RouteFieldIndex.CreatedUTC, value); }
		}

		/// <summary>The UpdatedUTC property of the Entity Route<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Route"."UpdatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)RouteFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)RouteFieldIndex.UpdatedUTC, value); }
		}

		/// <summary>Gets the EntityCollection with the related entities of type 'CategoryEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(CategoryEntity))]
		public virtual EntityCollection<CategoryEntity> CategoryCollection { get { return GetOrCreateEntityCollection<CategoryEntity, CategoryEntityFactory>("Route", true, false, ref _categoryCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'ClientConfigurationRouteEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(ClientConfigurationRouteEntity))]
		public virtual EntityCollection<ClientConfigurationRouteEntity> ClientConfigurationRouteCollection { get { return GetOrCreateEntityCollection<ClientConfigurationRouteEntity, ClientConfigurationRouteEntityFactory>("Route", true, false, ref _clientConfigurationRouteCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'CompanyEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(CompanyEntity))]
		public virtual EntityCollection<CompanyEntity> CompanyCollection { get { return GetOrCreateEntityCollection<CompanyEntity, CompanyEntityFactory>("Route", true, false, ref _companyCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'DeliverypointgroupEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(DeliverypointgroupEntity))]
		public virtual EntityCollection<DeliverypointgroupEntity> DeliverypointgroupCollection1 { get { return GetOrCreateEntityCollection<DeliverypointgroupEntity, DeliverypointgroupEntityFactory>("EmailDocumentRoute", true, false, ref _deliverypointgroupCollection1); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'DeliverypointgroupEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(DeliverypointgroupEntity))]
		public virtual EntityCollection<DeliverypointgroupEntity> DeliverypointgroupCollection2 { get { return GetOrCreateEntityCollection<DeliverypointgroupEntity, DeliverypointgroupEntityFactory>("OrderNotesRoute", true, false, ref _deliverypointgroupCollection2); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'DeliverypointgroupEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(DeliverypointgroupEntity))]
		public virtual EntityCollection<DeliverypointgroupEntity> DeliverypointgroupCollection { get { return GetOrCreateEntityCollection<DeliverypointgroupEntity, DeliverypointgroupEntityFactory>("Route", true, false, ref _deliverypointgroupCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'DeliverypointgroupEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(DeliverypointgroupEntity))]
		public virtual EntityCollection<DeliverypointgroupEntity> DeliverypointgroupCollection3 { get { return GetOrCreateEntityCollection<DeliverypointgroupEntity, DeliverypointgroupEntityFactory>("SystemMessageRoute", true, false, ref _deliverypointgroupCollection3); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'ProductEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(ProductEntity))]
		public virtual EntityCollection<ProductEntity> ProductCollection { get { return GetOrCreateEntityCollection<ProductEntity, ProductEntityFactory>("Route", true, false, ref _productCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'RouteEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(RouteEntity))]
		public virtual EntityCollection<RouteEntity> RouteCollection { get { return GetOrCreateEntityCollection<RouteEntity, RouteEntityFactory>("EscalationRoute", true, false, ref _routeCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'RoutestepEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(RoutestepEntity))]
		public virtual EntityCollection<RoutestepEntity> RoutestepCollection { get { return GetOrCreateEntityCollection<RoutestepEntity, RoutestepEntityFactory>("Route", true, false, ref _routestepCollection); } }

		/// <summary>Gets / sets related entity of type 'CompanyEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual CompanyEntity Company
		{
			get { return _company; }
			set { SetSingleRelatedEntityNavigator(value, "Company"); }
		}

		/// <summary>Gets / sets related entity of type 'RouteEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual RouteEntity EscalationRoute
		{
			get { return _escalationRoute; }
			set { SetSingleRelatedEntityNavigator(value, "EscalationRoute"); }
		}


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen
{
	public enum RouteFieldIndex
	{
		///<summary>RouteId. </summary>
		RouteId,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>Name. </summary>
		Name,
		///<summary>StepTimeoutMinutes. </summary>
		StepTimeoutMinutes,
		///<summary>RetrievalSupportNotificationTimeoutMinutes. </summary>
		RetrievalSupportNotificationTimeoutMinutes,
		///<summary>BeingHandledSupportNotificationTimeoutMinutes. </summary>
		BeingHandledSupportNotificationTimeoutMinutes,
		///<summary>LogAlways. </summary>
		LogAlways,
		///<summary>EscalationRouteId. </summary>
		EscalationRouteId,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Route. </summary>
	public partial class RouteRelations: RelationFactory
	{
		#region Custom Relation code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomRelationCode
        private const ReferentialConstraintDeleteRule DeleteRuleForCategoryEntityUsingRouteId = ReferentialConstraintDeleteRule.NoAction;
        private const ReferentialConstraintDeleteRule DeleteRuleForClientConfigurationRouteEntityUsingRouteId = ReferentialConstraintDeleteRule.NoAction;
        private const ReferentialConstraintDeleteRule DeleteRuleForCompanyEntityUsingRouteId = ReferentialConstraintDeleteRule.NoAction;
        private const ReferentialConstraintDeleteRule DeleteRuleForDeliverypointgroupEntityUsingRouteId = ReferentialConstraintDeleteRule.NoAction;
        private const ReferentialConstraintDeleteRule DeleteRuleForDeliverypointgroupEntityUsingEmailDocumentRouteId = ReferentialConstraintDeleteRule.NoAction;
        private const ReferentialConstraintDeleteRule DeleteRuleForDeliverypointgroupEntityUsingOrderNotesRouteId = ReferentialConstraintDeleteRule.NoAction;
        private const ReferentialConstraintDeleteRule DeleteRuleForDeliverypointgroupEntityUsingSystemMessageRouteId = ReferentialConstraintDeleteRule.NoAction;
        private const ReferentialConstraintDeleteRule DeleteRuleForProductEntityUsingRouteId = ReferentialConstraintDeleteRule.NoAction;
        private const ReferentialConstraintDeleteRule DeleteRuleForRouteEntityUsingEscalationRouteId = ReferentialConstraintDeleteRule.NoAction;
        private const ReferentialConstraintDeleteRule DeleteRuleForRoutestepEntityUsingRouteId = ReferentialConstraintDeleteRule.Cascade;
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		
		// private const ReferentialConstraintDeleteRule DeleteRuleForCategoryEntityUsingRouteId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForClientConfigurationRouteEntityUsingRouteId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForCompanyEntityUsingRouteId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForDeliverypointgroupEntityUsingEmailDocumentRouteId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForDeliverypointgroupEntityUsingOrderNotesRouteId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForDeliverypointgroupEntityUsingRouteId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForDeliverypointgroupEntityUsingSystemMessageRouteId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForProductEntityUsingRouteId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForRouteEntityUsingEscalationRouteId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForRoutestepEntityUsingRouteId = ReferentialConstraintDeleteRule


		/// <summary>Gets all relations and delete rules of the RouteEntity as a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects.</summary>
		/// <returns>a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects</returns>
		public virtual Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> GetRelationsAndDeleteRules()
		{
			Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> toReturn = new Dictionary<IEntityRelation, ReferentialConstraintDeleteRule>();
			toReturn.Add(this.CategoryEntityUsingRouteId, RouteRelations.DeleteRuleForCategoryEntityUsingRouteId);
			toReturn.Add(this.ClientConfigurationRouteEntityUsingRouteId, RouteRelations.DeleteRuleForClientConfigurationRouteEntityUsingRouteId);
			toReturn.Add(this.CompanyEntityUsingRouteId, RouteRelations.DeleteRuleForCompanyEntityUsingRouteId);
			toReturn.Add(this.DeliverypointgroupEntityUsingEmailDocumentRouteId, RouteRelations.DeleteRuleForDeliverypointgroupEntityUsingEmailDocumentRouteId);
			toReturn.Add(this.DeliverypointgroupEntityUsingOrderNotesRouteId, RouteRelations.DeleteRuleForDeliverypointgroupEntityUsingOrderNotesRouteId);
			toReturn.Add(this.DeliverypointgroupEntityUsingRouteId, RouteRelations.DeleteRuleForDeliverypointgroupEntityUsingRouteId);
			toReturn.Add(this.DeliverypointgroupEntityUsingSystemMessageRouteId, RouteRelations.DeleteRuleForDeliverypointgroupEntityUsingSystemMessageRouteId);
			toReturn.Add(this.ProductEntityUsingRouteId, RouteRelations.DeleteRuleForProductEntityUsingRouteId);
			toReturn.Add(this.RouteEntityUsingEscalationRouteId, RouteRelations.DeleteRuleForRouteEntityUsingEscalationRouteId);
			toReturn.Add(this.RoutestepEntityUsingRouteId, RouteRelations.DeleteRuleForRoutestepEntityUsingRouteId);
			return toReturn;
		}		
	
	
		/// <summary>Returns a new IEntityRelation object, between RouteEntity and CategoryEntity over the 1:n relation they have, using the relation between the fields: Route.RouteId - Category.RouteId</summary>
		public virtual IEntityRelation CategoryEntityUsingRouteId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "CategoryCollection", true, new[] { RouteFields.RouteId, CategoryFields.RouteId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between RouteEntity and ClientConfigurationRouteEntity over the 1:n relation they have, using the relation between the fields: Route.RouteId - ClientConfigurationRoute.RouteId</summary>
		public virtual IEntityRelation ClientConfigurationRouteEntityUsingRouteId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "ClientConfigurationRouteCollection", true, new[] { RouteFields.RouteId, ClientConfigurationRouteFields.RouteId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between RouteEntity and CompanyEntity over the 1:n relation they have, using the relation between the fields: Route.RouteId - Company.RouteId</summary>
		public virtual IEntityRelation CompanyEntityUsingRouteId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "CompanyCollection", true, new[] { RouteFields.RouteId, CompanyFields.RouteId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between RouteEntity and DeliverypointgroupEntity over the 1:n relation they have, using the relation between the fields: Route.RouteId - Deliverypointgroup.EmailDocumentRouteId</summary>
		public virtual IEntityRelation DeliverypointgroupEntityUsingEmailDocumentRouteId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "DeliverypointgroupCollection1", true, new[] { RouteFields.RouteId, DeliverypointgroupFields.EmailDocumentRouteId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between RouteEntity and DeliverypointgroupEntity over the 1:n relation they have, using the relation between the fields: Route.RouteId - Deliverypointgroup.OrderNotesRouteId</summary>
		public virtual IEntityRelation DeliverypointgroupEntityUsingOrderNotesRouteId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "DeliverypointgroupCollection2", true, new[] { RouteFields.RouteId, DeliverypointgroupFields.OrderNotesRouteId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between RouteEntity and DeliverypointgroupEntity over the 1:n relation they have, using the relation between the fields: Route.RouteId - Deliverypointgroup.RouteId</summary>
		public virtual IEntityRelation DeliverypointgroupEntityUsingRouteId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "DeliverypointgroupCollection", true, new[] { RouteFields.RouteId, DeliverypointgroupFields.RouteId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between RouteEntity and DeliverypointgroupEntity over the 1:n relation they have, using the relation between the fields: Route.RouteId - Deliverypointgroup.SystemMessageRouteId</summary>
		public virtual IEntityRelation DeliverypointgroupEntityUsingSystemMessageRouteId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "DeliverypointgroupCollection3", true, new[] { RouteFields.RouteId, DeliverypointgroupFields.SystemMessageRouteId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between RouteEntity and ProductEntity over the 1:n relation they have, using the relation between the fields: Route.RouteId - Product.RouteId</summary>
		public virtual IEntityRelation ProductEntityUsingRouteId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "ProductCollection", true, new[] { RouteFields.RouteId, ProductFields.RouteId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between RouteEntity and RouteEntity over the 1:n relation they have, using the relation between the fields: Route.RouteId - Route.EscalationRouteId</summary>
		public virtual IEntityRelation RouteEntityUsingEscalationRouteId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "RouteCollection", true, new[] { RouteFields.RouteId, RouteFields.EscalationRouteId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between RouteEntity and RoutestepEntity over the 1:n relation they have, using the relation between the fields: Route.RouteId - Routestep.RouteId</summary>
		public virtual IEntityRelation RoutestepEntityUsingRouteId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "RoutestepCollection", true, new[] { RouteFields.RouteId, RoutestepFields.RouteId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between RouteEntity and CompanyEntity over the m:1 relation they have, using the relation between the fields: Route.CompanyId - Company.CompanyId</summary>
		public virtual IEntityRelation CompanyEntityUsingCompanyId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Company", false, new[] { CompanyFields.CompanyId, RouteFields.CompanyId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between RouteEntity and RouteEntity over the m:1 relation they have, using the relation between the fields: Route.EscalationRouteId - Route.RouteId</summary>
		public virtual IEntityRelation RouteEntityUsingRouteIdEscalationRouteId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "EscalationRoute", false, new[] { RouteFields.RouteId, RouteFields.EscalationRouteId }); }
		}

	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticRouteRelations
	{
		internal static readonly IEntityRelation CategoryEntityUsingRouteIdStatic = new RouteRelations().CategoryEntityUsingRouteId;
		internal static readonly IEntityRelation ClientConfigurationRouteEntityUsingRouteIdStatic = new RouteRelations().ClientConfigurationRouteEntityUsingRouteId;
		internal static readonly IEntityRelation CompanyEntityUsingRouteIdStatic = new RouteRelations().CompanyEntityUsingRouteId;
		internal static readonly IEntityRelation DeliverypointgroupEntityUsingEmailDocumentRouteIdStatic = new RouteRelations().DeliverypointgroupEntityUsingEmailDocumentRouteId;
		internal static readonly IEntityRelation DeliverypointgroupEntityUsingOrderNotesRouteIdStatic = new RouteRelations().DeliverypointgroupEntityUsingOrderNotesRouteId;
		internal static readonly IEntityRelation DeliverypointgroupEntityUsingRouteIdStatic = new RouteRelations().DeliverypointgroupEntityUsingRouteId;
		internal static readonly IEntityRelation DeliverypointgroupEntityUsingSystemMessageRouteIdStatic = new RouteRelations().DeliverypointgroupEntityUsingSystemMessageRouteId;
		internal static readonly IEntityRelation ProductEntityUsingRouteIdStatic = new RouteRelations().ProductEntityUsingRouteId;
		internal static readonly IEntityRelation RouteEntityUsingEscalationRouteIdStatic = new RouteRelations().RouteEntityUsingEscalationRouteId;
		internal static readonly IEntityRelation RoutestepEntityUsingRouteIdStatic = new RouteRelations().RoutestepEntityUsingRouteId;
		internal static readonly IEntityRelation CompanyEntityUsingCompanyIdStatic = new RouteRelations().CompanyEntityUsingCompanyId;
		internal static readonly IEntityRelation RouteEntityUsingRouteIdEscalationRouteIdStatic = new RouteRelations().RouteEntityUsingRouteIdEscalationRouteId;

		/// <summary>CTor</summary>
		static StaticRouteRelations() { }
	}
}

