﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 5.8
// Code is generated on: 
// Code is generated using templates: Crave.TemplateBindings
// Templates vendor: Crave Interactive
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.FactoryClasses;
using Crave.Ordering.Shared.Data.LLBLGen.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

// ReSharper disable All
namespace Crave.Ordering.Shared.Data.LLBLGen.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'OrderNotificationLog'.<br/><br/></summary>
	[Serializable]
	public partial class OrderNotificationLogEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END
	
	{
		private OrderEntity _order;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		private static OrderNotificationLogEntityStaticMetaData _staticMetaData = new OrderNotificationLogEntityStaticMetaData();
		private static OrderNotificationLogRelations _relationsFactory = new OrderNotificationLogRelations();		

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Order</summary>
			public static readonly string Order = "Order";
		}

		/// <summary>Static meta-data storage for navigator related information</summary>
		protected class OrderNotificationLogEntityStaticMetaData : EntityStaticMetaDataBase
		{
			public OrderNotificationLogEntityStaticMetaData()
			{
				SetEntityCoreInfo("OrderNotificationLogEntity", InheritanceHierarchyType.None, false, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.OrderNotificationLogEntity, typeof(OrderNotificationLogEntity), typeof(OrderNotificationLogEntityFactory), false);
				AddNavigatorMetaData<OrderNotificationLogEntity, OrderEntity>("Order", "OrderNotificationLogCollection", (a, b) => a._order = b, a => a._order, (a, b) => a.Order = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticOrderNotificationLogRelations.OrderEntityUsingOrderIdStatic, ()=>new OrderNotificationLogRelations().OrderEntityUsingOrderId, null, new int[] { (int)OrderNotificationLogFieldIndex.OrderId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.OrderEntity);
			}
		}

		/// <summary>Static ctor</summary>
		static OrderNotificationLogEntity()
		{
		}

		/// <summary> CTor</summary>
		public OrderNotificationLogEntity()
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public OrderNotificationLogEntity(IEntityFields2 fields)
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this OrderNotificationLogEntity</param>
		public OrderNotificationLogEntity(IValidator validator)
		{
			InitClassEmpty(validator, null);
		}

		/// <summary> CTor</summary>
		/// <param name="orderNotificationLogId">PK value for OrderNotificationLog which data should be fetched into this OrderNotificationLog object</param>
		public OrderNotificationLogEntity(System.Int32 orderNotificationLogId) : this(orderNotificationLogId, null)
		{
		}

		/// <summary> CTor</summary>
		/// <param name="orderNotificationLogId">PK value for OrderNotificationLog which data should be fetched into this OrderNotificationLog object</param>
		/// <param name="validator">The custom validator object for this OrderNotificationLogEntity</param>
		public OrderNotificationLogEntity(System.Int32 orderNotificationLogId, IValidator validator)
		{
			InitClassEmpty(validator, null);
			this.OrderNotificationLogId = orderNotificationLogId;
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected OrderNotificationLogEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Order' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoOrder() { return CreateRelationInfoForNavigator("Order"); }
		
		/// <inheritdoc/>
		protected override EntityStaticMetaDataBase GetEntityStaticMetaData() {	return _staticMetaData; }

		/// <summary>Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		/// <summary>Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this OrderNotificationLogEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary>The relations object holding all relations of this entity with other entity classes.</summary>
		public static OrderNotificationLogRelations Relations { get { return _relationsFactory; } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Order' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathOrderEntity { get { return _staticMetaData.GetPrefetchPathElement("Order", CommonEntityBase.CreateEntityCollection<OrderEntity>()); } }

		/// <summary>The OrderNotificationLogId property of the Entity OrderNotificationLog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderNotificationLog"."OrderNotificationLogId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 OrderNotificationLogId
		{
			get { return (System.Int32)GetValue((int)OrderNotificationLogFieldIndex.OrderNotificationLogId, true); }
			set { SetValue((int)OrderNotificationLogFieldIndex.OrderNotificationLogId, value); }		}

		/// <summary>The OrderId property of the Entity OrderNotificationLog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderNotificationLog"."OrderId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 OrderId
		{
			get { return (System.Int32)GetValue((int)OrderNotificationLogFieldIndex.OrderId, true); }
			set	{ SetValue((int)OrderNotificationLogFieldIndex.OrderId, value); }
		}

		/// <summary>The ParentCompanyId property of the Entity OrderNotificationLog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderNotificationLog"."ParentCompanyId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ParentCompanyId
		{
			get { return (System.Int32)GetValue((int)OrderNotificationLogFieldIndex.ParentCompanyId, true); }
			set	{ SetValue((int)OrderNotificationLogFieldIndex.ParentCompanyId, value); }
		}

		/// <summary>The Type property of the Entity OrderNotificationLog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderNotificationLog"."Type".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Crave.Enums.OrderNotificationType Type
		{
			get { return (Crave.Enums.OrderNotificationType)GetValue((int)OrderNotificationLogFieldIndex.Type, true); }
			set	{ SetValue((int)OrderNotificationLogFieldIndex.Type, value); }
		}

		/// <summary>The Method property of the Entity OrderNotificationLog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderNotificationLog"."Method".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Crave.Enums.OrderNotificationMethod Method
		{
			get { return (Crave.Enums.OrderNotificationMethod)GetValue((int)OrderNotificationLogFieldIndex.Method, true); }
			set	{ SetValue((int)OrderNotificationLogFieldIndex.Method, value); }
		}

		/// <summary>The Status property of the Entity OrderNotificationLog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderNotificationLog"."Status".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Crave.Enums.OrderNotificationLogStatus Status
		{
			get { return (Crave.Enums.OrderNotificationLogStatus)GetValue((int)OrderNotificationLogFieldIndex.Status, true); }
			set	{ SetValue((int)OrderNotificationLogFieldIndex.Status, value); }
		}

		/// <summary>The Receiver property of the Entity OrderNotificationLog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderNotificationLog"."Receiver".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 250.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Receiver
		{
			get { return (System.String)GetValue((int)OrderNotificationLogFieldIndex.Receiver, true); }
			set	{ SetValue((int)OrderNotificationLogFieldIndex.Receiver, value); }
		}

		/// <summary>The Subject property of the Entity OrderNotificationLog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderNotificationLog"."Subject".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 250.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Subject
		{
			get { return (System.String)GetValue((int)OrderNotificationLogFieldIndex.Subject, true); }
			set	{ SetValue((int)OrderNotificationLogFieldIndex.Subject, value); }
		}

		/// <summary>The Message property of the Entity OrderNotificationLog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderNotificationLog"."Message".<br/>Table field type characteristics (type, precision, scale, length): Text, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Message
		{
			get { return (System.String)GetValue((int)OrderNotificationLogFieldIndex.Message, true); }
			set	{ SetValue((int)OrderNotificationLogFieldIndex.Message, value); }
		}

		/// <summary>The Log property of the Entity OrderNotificationLog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderNotificationLog"."Log".<br/>Table field type characteristics (type, precision, scale, length): Text, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Log
		{
			get { return (System.String)GetValue((int)OrderNotificationLogFieldIndex.Log, true); }
			set	{ SetValue((int)OrderNotificationLogFieldIndex.Log, value); }
		}

		/// <summary>The CreatedUTC property of the Entity OrderNotificationLog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderNotificationLog"."CreatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime CreatedUTC
		{
			get { return (System.DateTime)GetValue((int)OrderNotificationLogFieldIndex.CreatedUTC, true); }
			set	{ SetValue((int)OrderNotificationLogFieldIndex.CreatedUTC, value); }
		}

		/// <summary>The SentUTC property of the Entity OrderNotificationLog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderNotificationLog"."SentUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> SentUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)OrderNotificationLogFieldIndex.SentUTC, false); }
			set	{ SetValue((int)OrderNotificationLogFieldIndex.SentUTC, value); }
		}

		/// <summary>Gets / sets related entity of type 'OrderEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual OrderEntity Order
		{
			get { return _order; }
			set { SetSingleRelatedEntityNavigator(value, "Order"); }
		}


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen
{
	public enum OrderNotificationLogFieldIndex
	{
		///<summary>OrderNotificationLogId. </summary>
		OrderNotificationLogId,
		///<summary>OrderId. </summary>
		OrderId,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>Type. </summary>
		Type,
		///<summary>Method. </summary>
		Method,
		///<summary>Status. </summary>
		Status,
		///<summary>Receiver. </summary>
		Receiver,
		///<summary>Subject. </summary>
		Subject,
		///<summary>Message. </summary>
		Message,
		///<summary>Log. </summary>
		Log,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>SentUTC. </summary>
		SentUTC,
		/// <summary></summary>
		AmountOfFields
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: OrderNotificationLog. </summary>
	public partial class OrderNotificationLogRelations: RelationFactory
	{
		#region Custom Relation code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomRelationCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		


		/// <summary>Gets all relations and delete rules of the OrderNotificationLogEntity as a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects.</summary>
		/// <returns>a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects</returns>
		public virtual Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> GetRelationsAndDeleteRules()
		{
			Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> toReturn = new Dictionary<IEntityRelation, ReferentialConstraintDeleteRule>();
			return toReturn;
		}		
	
	

		/// <summary>Returns a new IEntityRelation object, between OrderNotificationLogEntity and OrderEntity over the m:1 relation they have, using the relation between the fields: OrderNotificationLog.OrderId - Order.OrderId</summary>
		public virtual IEntityRelation OrderEntityUsingOrderId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Order", false, new[] { OrderFields.OrderId, OrderNotificationLogFields.OrderId }); }
		}

	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticOrderNotificationLogRelations
	{
		internal static readonly IEntityRelation OrderEntityUsingOrderIdStatic = new OrderNotificationLogRelations().OrderEntityUsingOrderId;

		/// <summary>CTor</summary>
		static StaticOrderNotificationLogRelations() { }
	}
}

