﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 5.8
// Code is generated on: 
// Code is generated using templates: Crave.TemplateBindings
// Templates vendor: Crave Interactive
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.FactoryClasses;
using Crave.Ordering.Shared.Data.LLBLGen.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

// ReSharper disable All
namespace Crave.Ordering.Shared.Data.LLBLGen.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'DeliverypointExternalDeliverypoint'.<br/><br/></summary>
	[Serializable]
	public partial class DeliverypointExternalDeliverypointEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END
	
	{
		private DeliverypointEntity _deliverypoint;
		private ExternalDeliverypointEntity _externalDeliverypoint;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		private static DeliverypointExternalDeliverypointEntityStaticMetaData _staticMetaData = new DeliverypointExternalDeliverypointEntityStaticMetaData();
		private static DeliverypointExternalDeliverypointRelations _relationsFactory = new DeliverypointExternalDeliverypointRelations();		

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Deliverypoint</summary>
			public static readonly string Deliverypoint = "Deliverypoint";
			/// <summary>Member name ExternalDeliverypoint</summary>
			public static readonly string ExternalDeliverypoint = "ExternalDeliverypoint";
		}

		/// <summary>Static meta-data storage for navigator related information</summary>
		protected class DeliverypointExternalDeliverypointEntityStaticMetaData : EntityStaticMetaDataBase
		{
			public DeliverypointExternalDeliverypointEntityStaticMetaData()
			{
				SetEntityCoreInfo("DeliverypointExternalDeliverypointEntity", InheritanceHierarchyType.None, false, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.DeliverypointExternalDeliverypointEntity, typeof(DeliverypointExternalDeliverypointEntity), typeof(DeliverypointExternalDeliverypointEntityFactory), false);
				AddNavigatorMetaData<DeliverypointExternalDeliverypointEntity, DeliverypointEntity>("Deliverypoint", "DeliverypointExternalDeliverypointCollection", (a, b) => a._deliverypoint = b, a => a._deliverypoint, (a, b) => a.Deliverypoint = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticDeliverypointExternalDeliverypointRelations.DeliverypointEntityUsingDeliverypointIdStatic, ()=>new DeliverypointExternalDeliverypointRelations().DeliverypointEntityUsingDeliverypointId, null, new int[] { (int)DeliverypointExternalDeliverypointFieldIndex.DeliverypointId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.DeliverypointEntity);
				AddNavigatorMetaData<DeliverypointExternalDeliverypointEntity, ExternalDeliverypointEntity>("ExternalDeliverypoint", "DeliverypointExternalDeliverypointCollection", (a, b) => a._externalDeliverypoint = b, a => a._externalDeliverypoint, (a, b) => a.ExternalDeliverypoint = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticDeliverypointExternalDeliverypointRelations.ExternalDeliverypointEntityUsingExternalDeliverypointIdStatic, ()=>new DeliverypointExternalDeliverypointRelations().ExternalDeliverypointEntityUsingExternalDeliverypointId, null, new int[] { (int)DeliverypointExternalDeliverypointFieldIndex.ExternalDeliverypointId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ExternalDeliverypointEntity);
			}
		}

		/// <summary>Static ctor</summary>
		static DeliverypointExternalDeliverypointEntity()
		{
		}

		/// <summary> CTor</summary>
		public DeliverypointExternalDeliverypointEntity()
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public DeliverypointExternalDeliverypointEntity(IEntityFields2 fields)
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this DeliverypointExternalDeliverypointEntity</param>
		public DeliverypointExternalDeliverypointEntity(IValidator validator)
		{
			InitClassEmpty(validator, null);
		}

		/// <summary> CTor</summary>
		/// <param name="deliverypointExternalDeliverypointId">PK value for DeliverypointExternalDeliverypoint which data should be fetched into this DeliverypointExternalDeliverypoint object</param>
		public DeliverypointExternalDeliverypointEntity(System.Int32 deliverypointExternalDeliverypointId) : this(deliverypointExternalDeliverypointId, null)
		{
		}

		/// <summary> CTor</summary>
		/// <param name="deliverypointExternalDeliverypointId">PK value for DeliverypointExternalDeliverypoint which data should be fetched into this DeliverypointExternalDeliverypoint object</param>
		/// <param name="validator">The custom validator object for this DeliverypointExternalDeliverypointEntity</param>
		public DeliverypointExternalDeliverypointEntity(System.Int32 deliverypointExternalDeliverypointId, IValidator validator)
		{
			InitClassEmpty(validator, null);
			this.DeliverypointExternalDeliverypointId = deliverypointExternalDeliverypointId;
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected DeliverypointExternalDeliverypointEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary>Method which will construct a filter (predicate expression) for the unique constraint defined on the fields: DeliverypointId , ExternalDeliverypointId .</summary>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public IPredicateExpression ConstructFilterForUCDeliverypointIdExternalDeliverypointId()
		{
			var filter = new PredicateExpression();
			filter.Add(Crave.Ordering.Shared.Data.LLBLGen.HelperClasses.DeliverypointExternalDeliverypointFields.DeliverypointId == this.Fields.GetCurrentValue((int)DeliverypointExternalDeliverypointFieldIndex.DeliverypointId));
			filter.Add(Crave.Ordering.Shared.Data.LLBLGen.HelperClasses.DeliverypointExternalDeliverypointFields.ExternalDeliverypointId == this.Fields.GetCurrentValue((int)DeliverypointExternalDeliverypointFieldIndex.ExternalDeliverypointId));
 			return filter;
		}

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Deliverypoint' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoDeliverypoint() { return CreateRelationInfoForNavigator("Deliverypoint"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'ExternalDeliverypoint' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoExternalDeliverypoint() { return CreateRelationInfoForNavigator("ExternalDeliverypoint"); }
		
		/// <inheritdoc/>
		protected override EntityStaticMetaDataBase GetEntityStaticMetaData() {	return _staticMetaData; }

		/// <summary>Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		/// <summary>Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this DeliverypointExternalDeliverypointEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary>The relations object holding all relations of this entity with other entity classes.</summary>
		public static DeliverypointExternalDeliverypointRelations Relations { get { return _relationsFactory; } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Deliverypoint' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathDeliverypointEntity { get { return _staticMetaData.GetPrefetchPathElement("Deliverypoint", CommonEntityBase.CreateEntityCollection<DeliverypointEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'ExternalDeliverypoint' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathExternalDeliverypointEntity { get { return _staticMetaData.GetPrefetchPathElement("ExternalDeliverypoint", CommonEntityBase.CreateEntityCollection<ExternalDeliverypointEntity>()); } }

		/// <summary>The DeliverypointExternalDeliverypointId property of the Entity DeliverypointExternalDeliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliverypointExternalDeliverypoint"."DeliverypointExternalDeliverypointId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 DeliverypointExternalDeliverypointId
		{
			get { return (System.Int32)GetValue((int)DeliverypointExternalDeliverypointFieldIndex.DeliverypointExternalDeliverypointId, true); }
			set { SetValue((int)DeliverypointExternalDeliverypointFieldIndex.DeliverypointExternalDeliverypointId, value); }		}

		/// <summary>The DeliverypointId property of the Entity DeliverypointExternalDeliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliverypointExternalDeliverypoint"."DeliverypointId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 DeliverypointId
		{
			get { return (System.Int32)GetValue((int)DeliverypointExternalDeliverypointFieldIndex.DeliverypointId, true); }
			set	{ SetValue((int)DeliverypointExternalDeliverypointFieldIndex.DeliverypointId, value); }
		}

		/// <summary>The ExternalDeliverypointId property of the Entity DeliverypointExternalDeliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliverypointExternalDeliverypoint"."ExternalDeliverypointId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ExternalDeliverypointId
		{
			get { return (System.Int32)GetValue((int)DeliverypointExternalDeliverypointFieldIndex.ExternalDeliverypointId, true); }
			set	{ SetValue((int)DeliverypointExternalDeliverypointFieldIndex.ExternalDeliverypointId, value); }
		}

		/// <summary>The ParentCompanyId property of the Entity DeliverypointExternalDeliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliverypointExternalDeliverypoint"."ParentCompanyId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ParentCompanyId
		{
			get { return (System.Int32)GetValue((int)DeliverypointExternalDeliverypointFieldIndex.ParentCompanyId, true); }
			set	{ SetValue((int)DeliverypointExternalDeliverypointFieldIndex.ParentCompanyId, value); }
		}

		/// <summary>The CreatedUTC property of the Entity DeliverypointExternalDeliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliverypointExternalDeliverypoint"."CreatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime CreatedUTC
		{
			get { return (System.DateTime)GetValue((int)DeliverypointExternalDeliverypointFieldIndex.CreatedUTC, true); }
			set	{ SetValue((int)DeliverypointExternalDeliverypointFieldIndex.CreatedUTC, value); }
		}

		/// <summary>The UpdatedUTC property of the Entity DeliverypointExternalDeliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliverypointExternalDeliverypoint"."UpdatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)DeliverypointExternalDeliverypointFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)DeliverypointExternalDeliverypointFieldIndex.UpdatedUTC, value); }
		}

		/// <summary>Gets / sets related entity of type 'DeliverypointEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual DeliverypointEntity Deliverypoint
		{
			get { return _deliverypoint; }
			set { SetSingleRelatedEntityNavigator(value, "Deliverypoint"); }
		}

		/// <summary>Gets / sets related entity of type 'ExternalDeliverypointEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual ExternalDeliverypointEntity ExternalDeliverypoint
		{
			get { return _externalDeliverypoint; }
			set { SetSingleRelatedEntityNavigator(value, "ExternalDeliverypoint"); }
		}


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen
{
	public enum DeliverypointExternalDeliverypointFieldIndex
	{
		///<summary>DeliverypointExternalDeliverypointId. </summary>
		DeliverypointExternalDeliverypointId,
		///<summary>DeliverypointId. </summary>
		DeliverypointId,
		///<summary>ExternalDeliverypointId. </summary>
		ExternalDeliverypointId,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: DeliverypointExternalDeliverypoint. </summary>
	public partial class DeliverypointExternalDeliverypointRelations: RelationFactory
	{
		#region Custom Relation code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomRelationCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		


		/// <summary>Gets all relations and delete rules of the DeliverypointExternalDeliverypointEntity as a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects.</summary>
		/// <returns>a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects</returns>
		public virtual Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> GetRelationsAndDeleteRules()
		{
			Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> toReturn = new Dictionary<IEntityRelation, ReferentialConstraintDeleteRule>();
			return toReturn;
		}		
	
	

		/// <summary>Returns a new IEntityRelation object, between DeliverypointExternalDeliverypointEntity and DeliverypointEntity over the m:1 relation they have, using the relation between the fields: DeliverypointExternalDeliverypoint.DeliverypointId - Deliverypoint.DeliverypointId</summary>
		public virtual IEntityRelation DeliverypointEntityUsingDeliverypointId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Deliverypoint", false, new[] { DeliverypointFields.DeliverypointId, DeliverypointExternalDeliverypointFields.DeliverypointId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between DeliverypointExternalDeliverypointEntity and ExternalDeliverypointEntity over the m:1 relation they have, using the relation between the fields: DeliverypointExternalDeliverypoint.ExternalDeliverypointId - ExternalDeliverypoint.ExternalDeliverypointId</summary>
		public virtual IEntityRelation ExternalDeliverypointEntityUsingExternalDeliverypointId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "ExternalDeliverypoint", false, new[] { ExternalDeliverypointFields.ExternalDeliverypointId, DeliverypointExternalDeliverypointFields.ExternalDeliverypointId }); }
		}

	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticDeliverypointExternalDeliverypointRelations
	{
		internal static readonly IEntityRelation DeliverypointEntityUsingDeliverypointIdStatic = new DeliverypointExternalDeliverypointRelations().DeliverypointEntityUsingDeliverypointId;
		internal static readonly IEntityRelation ExternalDeliverypointEntityUsingExternalDeliverypointIdStatic = new DeliverypointExternalDeliverypointRelations().ExternalDeliverypointEntityUsingExternalDeliverypointId;

		/// <summary>CTor</summary>
		static StaticDeliverypointExternalDeliverypointRelations() { }
	}
}

