﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 5.8
// Code is generated on: 
// Code is generated using templates: Crave.TemplateBindings
// Templates vendor: Crave Interactive
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.FactoryClasses;
using Crave.Ordering.Shared.Data.LLBLGen.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

// ReSharper disable All
namespace Crave.Ordering.Shared.Data.LLBLGen.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'PriceLevelItem'.<br/><br/></summary>
	[Serializable]
	public partial class PriceLevelItemEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END
	
	{
		private EntityCollection<OrderitemEntity> _orderitemCollection;
		private EntityCollection<OrderitemAlterationitemEntity> _orderitemAlterationitemCollection;
		private AlterationEntity _alteration;
		private AlterationoptionEntity _alterationoption;
		private PriceLevelEntity _priceLevel;
		private ProductEntity _product;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		private static PriceLevelItemEntityStaticMetaData _staticMetaData = new PriceLevelItemEntityStaticMetaData();
		private static PriceLevelItemRelations _relationsFactory = new PriceLevelItemRelations();		

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Alteration</summary>
			public static readonly string Alteration = "Alteration";
			/// <summary>Member name Alterationoption</summary>
			public static readonly string Alterationoption = "Alterationoption";
			/// <summary>Member name PriceLevel</summary>
			public static readonly string PriceLevel = "PriceLevel";
			/// <summary>Member name Product</summary>
			public static readonly string Product = "Product";
			/// <summary>Member name OrderitemCollection</summary>
			public static readonly string OrderitemCollection = "OrderitemCollection";
			/// <summary>Member name OrderitemAlterationitemCollection</summary>
			public static readonly string OrderitemAlterationitemCollection = "OrderitemAlterationitemCollection";
		}

		/// <summary>Static meta-data storage for navigator related information</summary>
		protected class PriceLevelItemEntityStaticMetaData : EntityStaticMetaDataBase
		{
			public PriceLevelItemEntityStaticMetaData()
			{
				SetEntityCoreInfo("PriceLevelItemEntity", InheritanceHierarchyType.None, false, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.PriceLevelItemEntity, typeof(PriceLevelItemEntity), typeof(PriceLevelItemEntityFactory), false);
				AddNavigatorMetaData<PriceLevelItemEntity, EntityCollection<OrderitemEntity>>("OrderitemCollection", a => a._orderitemCollection, (a, b) => a._orderitemCollection = b, a => a.OrderitemCollection, () => new PriceLevelItemRelations().OrderitemEntityUsingPriceLevelItemId, typeof(OrderitemEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.OrderitemEntity);
				AddNavigatorMetaData<PriceLevelItemEntity, EntityCollection<OrderitemAlterationitemEntity>>("OrderitemAlterationitemCollection", a => a._orderitemAlterationitemCollection, (a, b) => a._orderitemAlterationitemCollection = b, a => a.OrderitemAlterationitemCollection, () => new PriceLevelItemRelations().OrderitemAlterationitemEntityUsingPriceLevelItemId, typeof(OrderitemAlterationitemEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.OrderitemAlterationitemEntity);
				AddNavigatorMetaData<PriceLevelItemEntity, AlterationEntity>("Alteration", "PriceLevelItemCollection", (a, b) => a._alteration = b, a => a._alteration, (a, b) => a.Alteration = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticPriceLevelItemRelations.AlterationEntityUsingAlterationIdStatic, ()=>new PriceLevelItemRelations().AlterationEntityUsingAlterationId, null, new int[] { (int)PriceLevelItemFieldIndex.AlterationId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.AlterationEntity);
				AddNavigatorMetaData<PriceLevelItemEntity, AlterationoptionEntity>("Alterationoption", "PriceLevelItemCollection", (a, b) => a._alterationoption = b, a => a._alterationoption, (a, b) => a.Alterationoption = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticPriceLevelItemRelations.AlterationoptionEntityUsingAlterationoptionIdStatic, ()=>new PriceLevelItemRelations().AlterationoptionEntityUsingAlterationoptionId, null, new int[] { (int)PriceLevelItemFieldIndex.AlterationoptionId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.AlterationoptionEntity);
				AddNavigatorMetaData<PriceLevelItemEntity, PriceLevelEntity>("PriceLevel", "PriceLevelItemCollection", (a, b) => a._priceLevel = b, a => a._priceLevel, (a, b) => a.PriceLevel = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticPriceLevelItemRelations.PriceLevelEntityUsingPriceLevelIdStatic, ()=>new PriceLevelItemRelations().PriceLevelEntityUsingPriceLevelId, null, new int[] { (int)PriceLevelItemFieldIndex.PriceLevelId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.PriceLevelEntity);
				AddNavigatorMetaData<PriceLevelItemEntity, ProductEntity>("Product", "PriceLevelItemCollection", (a, b) => a._product = b, a => a._product, (a, b) => a.Product = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticPriceLevelItemRelations.ProductEntityUsingProductIdStatic, ()=>new PriceLevelItemRelations().ProductEntityUsingProductId, null, new int[] { (int)PriceLevelItemFieldIndex.ProductId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ProductEntity);
			}
		}

		/// <summary>Static ctor</summary>
		static PriceLevelItemEntity()
		{
		}

		/// <summary> CTor</summary>
		public PriceLevelItemEntity()
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public PriceLevelItemEntity(IEntityFields2 fields)
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this PriceLevelItemEntity</param>
		public PriceLevelItemEntity(IValidator validator)
		{
			InitClassEmpty(validator, null);
		}

		/// <summary> CTor</summary>
		/// <param name="priceLevelItemId">PK value for PriceLevelItem which data should be fetched into this PriceLevelItem object</param>
		public PriceLevelItemEntity(System.Int32 priceLevelItemId) : this(priceLevelItemId, null)
		{
		}

		/// <summary> CTor</summary>
		/// <param name="priceLevelItemId">PK value for PriceLevelItem which data should be fetched into this PriceLevelItem object</param>
		/// <param name="validator">The custom validator object for this PriceLevelItemEntity</param>
		public PriceLevelItemEntity(System.Int32 priceLevelItemId, IValidator validator)
		{
			InitClassEmpty(validator, null);
			this.PriceLevelItemId = priceLevelItemId;
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected PriceLevelItemEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Orderitem' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoOrderitemCollection() { return CreateRelationInfoForNavigator("OrderitemCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'OrderitemAlterationitem' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoOrderitemAlterationitemCollection() { return CreateRelationInfoForNavigator("OrderitemAlterationitemCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Alteration' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoAlteration() { return CreateRelationInfoForNavigator("Alteration"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Alterationoption' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoAlterationoption() { return CreateRelationInfoForNavigator("Alterationoption"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'PriceLevel' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoPriceLevel() { return CreateRelationInfoForNavigator("PriceLevel"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Product' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoProduct() { return CreateRelationInfoForNavigator("Product"); }
		
		/// <inheritdoc/>
		protected override EntityStaticMetaDataBase GetEntityStaticMetaData() {	return _staticMetaData; }

		/// <summary>Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		/// <summary>Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this PriceLevelItemEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary>The relations object holding all relations of this entity with other entity classes.</summary>
		public static PriceLevelItemRelations Relations { get { return _relationsFactory; } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Orderitem' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathOrderitemCollection { get { return _staticMetaData.GetPrefetchPathElement("OrderitemCollection", CommonEntityBase.CreateEntityCollection<OrderitemEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'OrderitemAlterationitem' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathOrderitemAlterationitemCollection { get { return _staticMetaData.GetPrefetchPathElement("OrderitemAlterationitemCollection", CommonEntityBase.CreateEntityCollection<OrderitemAlterationitemEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Alteration' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathAlterationEntity { get { return _staticMetaData.GetPrefetchPathElement("Alteration", CommonEntityBase.CreateEntityCollection<AlterationEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Alterationoption' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathAlterationoptionEntity { get { return _staticMetaData.GetPrefetchPathElement("Alterationoption", CommonEntityBase.CreateEntityCollection<AlterationoptionEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'PriceLevel' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathPriceLevelEntity { get { return _staticMetaData.GetPrefetchPathElement("PriceLevel", CommonEntityBase.CreateEntityCollection<PriceLevelEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Product' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathProductEntity { get { return _staticMetaData.GetPrefetchPathElement("Product", CommonEntityBase.CreateEntityCollection<ProductEntity>()); } }

		/// <summary>The PriceLevelItemId property of the Entity PriceLevelItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceLevelItem"."PriceLevelItemId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 PriceLevelItemId
		{
			get { return (System.Int32)GetValue((int)PriceLevelItemFieldIndex.PriceLevelItemId, true); }
			set { SetValue((int)PriceLevelItemFieldIndex.PriceLevelItemId, value); }		}

		/// <summary>The ParentCompanyId property of the Entity PriceLevelItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceLevelItem"."ParentCompanyId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ParentCompanyId
		{
			get { return (Nullable<System.Int32>)GetValue((int)PriceLevelItemFieldIndex.ParentCompanyId, false); }
			set	{ SetValue((int)PriceLevelItemFieldIndex.ParentCompanyId, value); }
		}

		/// <summary>The PriceLevelId property of the Entity PriceLevelItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceLevelItem"."PriceLevelId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 PriceLevelId
		{
			get { return (System.Int32)GetValue((int)PriceLevelItemFieldIndex.PriceLevelId, true); }
			set	{ SetValue((int)PriceLevelItemFieldIndex.PriceLevelId, value); }
		}

		/// <summary>The Price property of the Entity PriceLevelItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceLevelItem"."Price".<br/>Table field type characteristics (type, precision, scale, length): Money, 19, 4, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal Price
		{
			get { return (System.Decimal)GetValue((int)PriceLevelItemFieldIndex.Price, true); }
			set	{ SetValue((int)PriceLevelItemFieldIndex.Price, value); }
		}

		/// <summary>The ProductId property of the Entity PriceLevelItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceLevelItem"."ProductId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ProductId
		{
			get { return (Nullable<System.Int32>)GetValue((int)PriceLevelItemFieldIndex.ProductId, false); }
			set	{ SetValue((int)PriceLevelItemFieldIndex.ProductId, value); }
		}

		/// <summary>The AlterationId property of the Entity PriceLevelItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceLevelItem"."AlterationId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> AlterationId
		{
			get { return (Nullable<System.Int32>)GetValue((int)PriceLevelItemFieldIndex.AlterationId, false); }
			set	{ SetValue((int)PriceLevelItemFieldIndex.AlterationId, value); }
		}

		/// <summary>The AlterationoptionId property of the Entity PriceLevelItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceLevelItem"."AlterationoptionId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> AlterationoptionId
		{
			get { return (Nullable<System.Int32>)GetValue((int)PriceLevelItemFieldIndex.AlterationoptionId, false); }
			set	{ SetValue((int)PriceLevelItemFieldIndex.AlterationoptionId, value); }
		}

		/// <summary>The CreatedUTC property of the Entity PriceLevelItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceLevelItem"."CreatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)PriceLevelItemFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)PriceLevelItemFieldIndex.CreatedUTC, value); }
		}

		/// <summary>The CreatedBy property of the Entity PriceLevelItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceLevelItem"."CreatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CreatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)PriceLevelItemFieldIndex.CreatedBy, false); }
			set	{ SetValue((int)PriceLevelItemFieldIndex.CreatedBy, value); }
		}

		/// <summary>The UpdatedUTC property of the Entity PriceLevelItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceLevelItem"."UpdatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)PriceLevelItemFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)PriceLevelItemFieldIndex.UpdatedUTC, value); }
		}

		/// <summary>The UpdatedBy property of the Entity PriceLevelItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceLevelItem"."UpdatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UpdatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)PriceLevelItemFieldIndex.UpdatedBy, false); }
			set	{ SetValue((int)PriceLevelItemFieldIndex.UpdatedBy, value); }
		}

		/// <summary>Gets the EntityCollection with the related entities of type 'OrderitemEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(OrderitemEntity))]
		public virtual EntityCollection<OrderitemEntity> OrderitemCollection { get { return GetOrCreateEntityCollection<OrderitemEntity, OrderitemEntityFactory>("PriceLevelItem", true, false, ref _orderitemCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'OrderitemAlterationitemEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(OrderitemAlterationitemEntity))]
		public virtual EntityCollection<OrderitemAlterationitemEntity> OrderitemAlterationitemCollection { get { return GetOrCreateEntityCollection<OrderitemAlterationitemEntity, OrderitemAlterationitemEntityFactory>("PriceLevelItem", true, false, ref _orderitemAlterationitemCollection); } }

		/// <summary>Gets / sets related entity of type 'AlterationEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual AlterationEntity Alteration
		{
			get { return _alteration; }
			set { SetSingleRelatedEntityNavigator(value, "Alteration"); }
		}

		/// <summary>Gets / sets related entity of type 'AlterationoptionEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual AlterationoptionEntity Alterationoption
		{
			get { return _alterationoption; }
			set { SetSingleRelatedEntityNavigator(value, "Alterationoption"); }
		}

		/// <summary>Gets / sets related entity of type 'PriceLevelEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual PriceLevelEntity PriceLevel
		{
			get { return _priceLevel; }
			set { SetSingleRelatedEntityNavigator(value, "PriceLevel"); }
		}

		/// <summary>Gets / sets related entity of type 'ProductEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual ProductEntity Product
		{
			get { return _product; }
			set { SetSingleRelatedEntityNavigator(value, "Product"); }
		}


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen
{
	public enum PriceLevelItemFieldIndex
	{
		///<summary>PriceLevelItemId. </summary>
		PriceLevelItemId,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>PriceLevelId. </summary>
		PriceLevelId,
		///<summary>Price. </summary>
		Price,
		///<summary>ProductId. </summary>
		ProductId,
		///<summary>AlterationId. </summary>
		AlterationId,
		///<summary>AlterationoptionId. </summary>
		AlterationoptionId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		/// <summary></summary>
		AmountOfFields
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: PriceLevelItem. </summary>
	public partial class PriceLevelItemRelations: RelationFactory
	{
		#region Custom Relation code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomRelationCode
        private const ReferentialConstraintDeleteRule DeleteRuleForOrderitemEntityUsingPriceLevelItemId = ReferentialConstraintDeleteRule.SetNull;
        private const ReferentialConstraintDeleteRule DeleteRuleForOrderitemAlterationitemEntityUsingPriceLevelItemId = ReferentialConstraintDeleteRule.SetNull;
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		
		// private const ReferentialConstraintDeleteRule DeleteRuleForOrderitemEntityUsingPriceLevelItemId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForOrderitemAlterationitemEntityUsingPriceLevelItemId = ReferentialConstraintDeleteRule


		/// <summary>Gets all relations and delete rules of the PriceLevelItemEntity as a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects.</summary>
		/// <returns>a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects</returns>
		public virtual Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> GetRelationsAndDeleteRules()
		{
			Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> toReturn = new Dictionary<IEntityRelation, ReferentialConstraintDeleteRule>();
			toReturn.Add(this.OrderitemEntityUsingPriceLevelItemId, PriceLevelItemRelations.DeleteRuleForOrderitemEntityUsingPriceLevelItemId);
			toReturn.Add(this.OrderitemAlterationitemEntityUsingPriceLevelItemId, PriceLevelItemRelations.DeleteRuleForOrderitemAlterationitemEntityUsingPriceLevelItemId);
			return toReturn;
		}		
	
	
		/// <summary>Returns a new IEntityRelation object, between PriceLevelItemEntity and OrderitemEntity over the 1:n relation they have, using the relation between the fields: PriceLevelItem.PriceLevelItemId - Orderitem.PriceLevelItemId</summary>
		public virtual IEntityRelation OrderitemEntityUsingPriceLevelItemId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "OrderitemCollection", true, new[] { PriceLevelItemFields.PriceLevelItemId, OrderitemFields.PriceLevelItemId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between PriceLevelItemEntity and OrderitemAlterationitemEntity over the 1:n relation they have, using the relation between the fields: PriceLevelItem.PriceLevelItemId - OrderitemAlterationitem.PriceLevelItemId</summary>
		public virtual IEntityRelation OrderitemAlterationitemEntityUsingPriceLevelItemId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "OrderitemAlterationitemCollection", true, new[] { PriceLevelItemFields.PriceLevelItemId, OrderitemAlterationitemFields.PriceLevelItemId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between PriceLevelItemEntity and AlterationEntity over the m:1 relation they have, using the relation between the fields: PriceLevelItem.AlterationId - Alteration.AlterationId</summary>
		public virtual IEntityRelation AlterationEntityUsingAlterationId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Alteration", false, new[] { AlterationFields.AlterationId, PriceLevelItemFields.AlterationId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between PriceLevelItemEntity and AlterationoptionEntity over the m:1 relation they have, using the relation between the fields: PriceLevelItem.AlterationoptionId - Alterationoption.AlterationoptionId</summary>
		public virtual IEntityRelation AlterationoptionEntityUsingAlterationoptionId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Alterationoption", false, new[] { AlterationoptionFields.AlterationoptionId, PriceLevelItemFields.AlterationoptionId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between PriceLevelItemEntity and PriceLevelEntity over the m:1 relation they have, using the relation between the fields: PriceLevelItem.PriceLevelId - PriceLevel.PriceLevelId</summary>
		public virtual IEntityRelation PriceLevelEntityUsingPriceLevelId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "PriceLevel", false, new[] { PriceLevelFields.PriceLevelId, PriceLevelItemFields.PriceLevelId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between PriceLevelItemEntity and ProductEntity over the m:1 relation they have, using the relation between the fields: PriceLevelItem.ProductId - Product.ProductId</summary>
		public virtual IEntityRelation ProductEntityUsingProductId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Product", false, new[] { ProductFields.ProductId, PriceLevelItemFields.ProductId }); }
		}

	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticPriceLevelItemRelations
	{
		internal static readonly IEntityRelation OrderitemEntityUsingPriceLevelItemIdStatic = new PriceLevelItemRelations().OrderitemEntityUsingPriceLevelItemId;
		internal static readonly IEntityRelation OrderitemAlterationitemEntityUsingPriceLevelItemIdStatic = new PriceLevelItemRelations().OrderitemAlterationitemEntityUsingPriceLevelItemId;
		internal static readonly IEntityRelation AlterationEntityUsingAlterationIdStatic = new PriceLevelItemRelations().AlterationEntityUsingAlterationId;
		internal static readonly IEntityRelation AlterationoptionEntityUsingAlterationoptionIdStatic = new PriceLevelItemRelations().AlterationoptionEntityUsingAlterationoptionId;
		internal static readonly IEntityRelation PriceLevelEntityUsingPriceLevelIdStatic = new PriceLevelItemRelations().PriceLevelEntityUsingPriceLevelId;
		internal static readonly IEntityRelation ProductEntityUsingProductIdStatic = new PriceLevelItemRelations().ProductEntityUsingProductId;

		/// <summary>CTor</summary>
		static StaticPriceLevelItemRelations() { }
	}
}

