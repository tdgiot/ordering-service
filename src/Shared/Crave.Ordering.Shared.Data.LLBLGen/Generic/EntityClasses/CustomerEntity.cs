﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 5.8
// Code is generated on: 
// Code is generated using templates: Crave.TemplateBindings
// Templates vendor: Crave Interactive
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.FactoryClasses;
using Crave.Ordering.Shared.Data.LLBLGen.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

// ReSharper disable All
namespace Crave.Ordering.Shared.Data.LLBLGen.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'Customer'.<br/><br/></summary>
	[Serializable]
	public partial class CustomerEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END
	
	{
		private EntityCollection<DeviceEntity> _deviceCollection;
		private EntityCollection<NetmessageEntity> _netmessageCollection;
		private EntityCollection<NetmessageEntity> _netmessageCollection1;
		private EntityCollection<OrderEntity> _orderCollection;
		private ClientEntity _client;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		private static CustomerEntityStaticMetaData _staticMetaData = new CustomerEntityStaticMetaData();
		private static CustomerRelations _relationsFactory = new CustomerRelations();		

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Client</summary>
			public static readonly string Client = "Client";
			/// <summary>Member name DeviceCollection</summary>
			public static readonly string DeviceCollection = "DeviceCollection";
			/// <summary>Member name NetmessageCollection</summary>
			public static readonly string NetmessageCollection = "NetmessageCollection";
			/// <summary>Member name NetmessageCollection1</summary>
			public static readonly string NetmessageCollection1 = "NetmessageCollection1";
			/// <summary>Member name OrderCollection</summary>
			public static readonly string OrderCollection = "OrderCollection";
		}

		/// <summary>Static meta-data storage for navigator related information</summary>
		protected class CustomerEntityStaticMetaData : EntityStaticMetaDataBase
		{
			public CustomerEntityStaticMetaData()
			{
				SetEntityCoreInfo("CustomerEntity", InheritanceHierarchyType.None, false, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.CustomerEntity, typeof(CustomerEntity), typeof(CustomerEntityFactory), false);
				AddNavigatorMetaData<CustomerEntity, EntityCollection<DeviceEntity>>("DeviceCollection", a => a._deviceCollection, (a, b) => a._deviceCollection = b, a => a.DeviceCollection, () => new CustomerRelations().DeviceEntityUsingCustomerId, typeof(DeviceEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.DeviceEntity);
				AddNavigatorMetaData<CustomerEntity, EntityCollection<NetmessageEntity>>("NetmessageCollection", a => a._netmessageCollection, (a, b) => a._netmessageCollection = b, a => a.NetmessageCollection, () => new CustomerRelations().NetmessageEntityUsingSenderCustomerId, typeof(NetmessageEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.NetmessageEntity);
				AddNavigatorMetaData<CustomerEntity, EntityCollection<NetmessageEntity>>("NetmessageCollection1", a => a._netmessageCollection1, (a, b) => a._netmessageCollection1 = b, a => a.NetmessageCollection1, () => new CustomerRelations().NetmessageEntityUsingReceiverCustomerId, typeof(NetmessageEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.NetmessageEntity);
				AddNavigatorMetaData<CustomerEntity, EntityCollection<OrderEntity>>("OrderCollection", a => a._orderCollection, (a, b) => a._orderCollection = b, a => a.OrderCollection, () => new CustomerRelations().OrderEntityUsingCustomerId, typeof(OrderEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.OrderEntity);
				AddNavigatorMetaData<CustomerEntity, ClientEntity>("Client", "CustomerCollection", (a, b) => a._client = b, a => a._client, (a, b) => a.Client = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticCustomerRelations.ClientEntityUsingClientIdStatic, ()=>new CustomerRelations().ClientEntityUsingClientId, null, new int[] { (int)CustomerFieldIndex.ClientId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ClientEntity);
			}
		}

		/// <summary>Static ctor</summary>
		static CustomerEntity()
		{
		}

		/// <summary> CTor</summary>
		public CustomerEntity()
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public CustomerEntity(IEntityFields2 fields)
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this CustomerEntity</param>
		public CustomerEntity(IValidator validator)
		{
			InitClassEmpty(validator, null);
		}

		/// <summary> CTor</summary>
		/// <param name="customerId">PK value for Customer which data should be fetched into this Customer object</param>
		public CustomerEntity(System.Int32 customerId) : this(customerId, null)
		{
		}

		/// <summary> CTor</summary>
		/// <param name="customerId">PK value for Customer which data should be fetched into this Customer object</param>
		/// <param name="validator">The custom validator object for this CustomerEntity</param>
		public CustomerEntity(System.Int32 customerId, IValidator validator)
		{
			InitClassEmpty(validator, null);
			this.CustomerId = customerId;
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected CustomerEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Device' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoDeviceCollection() { return CreateRelationInfoForNavigator("DeviceCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Netmessage' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoNetmessageCollection() { return CreateRelationInfoForNavigator("NetmessageCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Netmessage' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoNetmessageCollection1() { return CreateRelationInfoForNavigator("NetmessageCollection1"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Order' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoOrderCollection() { return CreateRelationInfoForNavigator("OrderCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Client' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoClient() { return CreateRelationInfoForNavigator("Client"); }
		
		/// <inheritdoc/>
		protected override EntityStaticMetaDataBase GetEntityStaticMetaData() {	return _staticMetaData; }

		/// <summary>Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		/// <summary>Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this CustomerEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary>The relations object holding all relations of this entity with other entity classes.</summary>
		public static CustomerRelations Relations { get { return _relationsFactory; } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Device' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathDeviceCollection { get { return _staticMetaData.GetPrefetchPathElement("DeviceCollection", CommonEntityBase.CreateEntityCollection<DeviceEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Netmessage' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathNetmessageCollection { get { return _staticMetaData.GetPrefetchPathElement("NetmessageCollection", CommonEntityBase.CreateEntityCollection<NetmessageEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Netmessage' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathNetmessageCollection1 { get { return _staticMetaData.GetPrefetchPathElement("NetmessageCollection1", CommonEntityBase.CreateEntityCollection<NetmessageEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Order' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathOrderCollection { get { return _staticMetaData.GetPrefetchPathElement("OrderCollection", CommonEntityBase.CreateEntityCollection<OrderEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Client' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathClientEntity { get { return _staticMetaData.GetPrefetchPathElement("Client", CommonEntityBase.CreateEntityCollection<ClientEntity>()); } }

		/// <summary>The CustomerId property of the Entity Customer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Customer"."CustomerId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 CustomerId
		{
			get { return (System.Int32)GetValue((int)CustomerFieldIndex.CustomerId, true); }
			set { SetValue((int)CustomerFieldIndex.CustomerId, value); }		}

		/// <summary>The Email property of the Entity Customer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Customer"."Email".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Email
		{
			get { return (System.String)GetValue((int)CustomerFieldIndex.Email, true); }
			set	{ SetValue((int)CustomerFieldIndex.Email, value); }
		}

		/// <summary>The Phonenumber property of the Entity Customer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Customer"."Phonenumber".<br/>Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 50.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Phonenumber
		{
			get { return (System.String)GetValue((int)CustomerFieldIndex.Phonenumber, true); }
			set	{ SetValue((int)CustomerFieldIndex.Phonenumber, value); }
		}

		/// <summary>The Password property of the Entity Customer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Customer"."Password".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 160.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Password
		{
			get { return (System.String)GetValue((int)CustomerFieldIndex.Password, true); }
			set	{ SetValue((int)CustomerFieldIndex.Password, value); }
		}

		/// <summary>The PasswordSalt property of the Entity Customer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Customer"."PasswordSalt".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 256.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PasswordSalt
		{
			get { return (System.String)GetValue((int)CustomerFieldIndex.PasswordSalt, true); }
			set	{ SetValue((int)CustomerFieldIndex.PasswordSalt, value); }
		}

		/// <summary>The CommunicationSalt property of the Entity Customer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Customer"."CommunicationSalt".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 256.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CommunicationSalt
		{
			get { return (System.String)GetValue((int)CustomerFieldIndex.CommunicationSalt, true); }
			set	{ SetValue((int)CustomerFieldIndex.CommunicationSalt, value); }
		}

		/// <summary>The Verified property of the Entity Customer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Customer"."Verified".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean Verified
		{
			get { return (System.Boolean)GetValue((int)CustomerFieldIndex.Verified, true); }
			set	{ SetValue((int)CustomerFieldIndex.Verified, value); }
		}

		/// <summary>The Blacklisted property of the Entity Customer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Customer"."Blacklisted".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean Blacklisted
		{
			get { return (System.Boolean)GetValue((int)CustomerFieldIndex.Blacklisted, true); }
			set	{ SetValue((int)CustomerFieldIndex.Blacklisted, value); }
		}

		/// <summary>The BlacklistedNotes property of the Entity Customer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Customer"."BlacklistedNotes".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String BlacklistedNotes
		{
			get { return (System.String)GetValue((int)CustomerFieldIndex.BlacklistedNotes, true); }
			set	{ SetValue((int)CustomerFieldIndex.BlacklistedNotes, value); }
		}

		/// <summary>The Firstname property of the Entity Customer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Customer"."Firstname".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 100.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Firstname
		{
			get { return (System.String)GetValue((int)CustomerFieldIndex.Firstname, true); }
			set	{ SetValue((int)CustomerFieldIndex.Firstname, value); }
		}

		/// <summary>The Lastname property of the Entity Customer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Customer"."Lastname".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 100.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Lastname
		{
			get { return (System.String)GetValue((int)CustomerFieldIndex.Lastname, true); }
			set	{ SetValue((int)CustomerFieldIndex.Lastname, value); }
		}

		/// <summary>The LastnamePrefix property of the Entity Customer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Customer"."LastnamePrefix".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 100.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String LastnamePrefix
		{
			get { return (System.String)GetValue((int)CustomerFieldIndex.LastnamePrefix, true); }
			set	{ SetValue((int)CustomerFieldIndex.LastnamePrefix, value); }
		}

		/// <summary>The RandomValue property of the Entity Customer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Customer"."RandomValue".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String RandomValue
		{
			get { return (System.String)GetValue((int)CustomerFieldIndex.RandomValue, true); }
			set	{ SetValue((int)CustomerFieldIndex.RandomValue, value); }
		}

		/// <summary>The Gender property of the Entity Customer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Customer"."Gender".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> Gender
		{
			get { return (Nullable<System.Boolean>)GetValue((int)CustomerFieldIndex.Gender, false); }
			set	{ SetValue((int)CustomerFieldIndex.Gender, value); }
		}

		/// <summary>The Addressline1 property of the Entity Customer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Customer"."Addressline1".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Addressline1
		{
			get { return (System.String)GetValue((int)CustomerFieldIndex.Addressline1, true); }
			set	{ SetValue((int)CustomerFieldIndex.Addressline1, value); }
		}

		/// <summary>The Addressline2 property of the Entity Customer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Customer"."Addressline2".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Addressline2
		{
			get { return (System.String)GetValue((int)CustomerFieldIndex.Addressline2, true); }
			set	{ SetValue((int)CustomerFieldIndex.Addressline2, value); }
		}

		/// <summary>The Zipcode property of the Entity Customer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Customer"."Zipcode".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 25.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Zipcode
		{
			get { return (System.String)GetValue((int)CustomerFieldIndex.Zipcode, true); }
			set	{ SetValue((int)CustomerFieldIndex.Zipcode, value); }
		}

		/// <summary>The City property of the Entity Customer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Customer"."City".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String City
		{
			get { return (System.String)GetValue((int)CustomerFieldIndex.City, true); }
			set	{ SetValue((int)CustomerFieldIndex.City, value); }
		}

		/// <summary>The CountryCode property of the Entity Customer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Customer"."CountryCode".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 3.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CountryCode
		{
			get { return (System.String)GetValue((int)CustomerFieldIndex.CountryCode, true); }
			set	{ SetValue((int)CustomerFieldIndex.CountryCode, value); }
		}

		/// <summary>The Notes property of the Entity Customer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Customer"."Notes".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Notes
		{
			get { return (System.String)GetValue((int)CustomerFieldIndex.Notes, true); }
			set	{ SetValue((int)CustomerFieldIndex.Notes, value); }
		}

		/// <summary>The SmsRequestsSent property of the Entity Customer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Customer"."SmsRequestsSent".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 SmsRequestsSent
		{
			get { return (System.Int32)GetValue((int)CustomerFieldIndex.SmsRequestsSent, true); }
			set	{ SetValue((int)CustomerFieldIndex.SmsRequestsSent, value); }
		}

		/// <summary>The SmsCreateRequests property of the Entity Customer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Customer"."SmsCreateRequests".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 SmsCreateRequests
		{
			get { return (System.Int32)GetValue((int)CustomerFieldIndex.SmsCreateRequests, true); }
			set	{ SetValue((int)CustomerFieldIndex.SmsCreateRequests, value); }
		}

		/// <summary>The SmsResetPincodeRequests property of the Entity Customer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Customer"."SmsResetPincodeRequests".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 SmsResetPincodeRequests
		{
			get { return (System.Int32)GetValue((int)CustomerFieldIndex.SmsResetPincodeRequests, true); }
			set	{ SetValue((int)CustomerFieldIndex.SmsResetPincodeRequests, value); }
		}

		/// <summary>The SmsUnlockAccountRequests property of the Entity Customer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Customer"."SmsUnlockAccountRequests".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 SmsUnlockAccountRequests
		{
			get { return (System.Int32)GetValue((int)CustomerFieldIndex.SmsUnlockAccountRequests, true); }
			set	{ SetValue((int)CustomerFieldIndex.SmsUnlockAccountRequests, value); }
		}

		/// <summary>The SmsNonProcessableRequests property of the Entity Customer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Customer"."SmsNonProcessableRequests".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 SmsNonProcessableRequests
		{
			get { return (System.Int32)GetValue((int)CustomerFieldIndex.SmsNonProcessableRequests, true); }
			set	{ SetValue((int)CustomerFieldIndex.SmsNonProcessableRequests, value); }
		}

		/// <summary>The HasChangedInititialPincode property of the Entity Customer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Customer"."HasChangedInititialPincode".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean HasChangedInititialPincode
		{
			get { return (System.Boolean)GetValue((int)CustomerFieldIndex.HasChangedInititialPincode, true); }
			set	{ SetValue((int)CustomerFieldIndex.HasChangedInititialPincode, value); }
		}

		/// <summary>The InitialLinkIdentifier property of the Entity Customer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Customer"."InitialLinkIdentifier".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String InitialLinkIdentifier
		{
			get { return (System.String)GetValue((int)CustomerFieldIndex.InitialLinkIdentifier, true); }
			set	{ SetValue((int)CustomerFieldIndex.InitialLinkIdentifier, value); }
		}

		/// <summary>The PasswordResetLinkIdentifier property of the Entity Customer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Customer"."PasswordResetLinkIdentifier".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PasswordResetLinkIdentifier
		{
			get { return (System.String)GetValue((int)CustomerFieldIndex.PasswordResetLinkIdentifier, true); }
			set	{ SetValue((int)CustomerFieldIndex.PasswordResetLinkIdentifier, value); }
		}

		/// <summary>The PasswordResetLinkFailedAtttempts property of the Entity Customer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Customer"."PasswordResetLinkFailedAtttempts".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 PasswordResetLinkFailedAtttempts
		{
			get { return (System.Int32)GetValue((int)CustomerFieldIndex.PasswordResetLinkFailedAtttempts, true); }
			set	{ SetValue((int)CustomerFieldIndex.PasswordResetLinkFailedAtttempts, value); }
		}

		/// <summary>The FailedPasswordAttemptCount property of the Entity Customer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Customer"."FailedPasswordAttemptCount".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 FailedPasswordAttemptCount
		{
			get { return (System.Int32)GetValue((int)CustomerFieldIndex.FailedPasswordAttemptCount, true); }
			set	{ SetValue((int)CustomerFieldIndex.FailedPasswordAttemptCount, value); }
		}

		/// <summary>The RememberOnMobileWebsite property of the Entity Customer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Customer"."RememberOnMobileWebsite".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean RememberOnMobileWebsite
		{
			get { return (System.Boolean)GetValue((int)CustomerFieldIndex.RememberOnMobileWebsite, true); }
			set	{ SetValue((int)CustomerFieldIndex.RememberOnMobileWebsite, value); }
		}

		/// <summary>The AnonymousAccount property of the Entity Customer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Customer"."AnonymousAccount".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean AnonymousAccount
		{
			get { return (System.Boolean)GetValue((int)CustomerFieldIndex.AnonymousAccount, true); }
			set	{ SetValue((int)CustomerFieldIndex.AnonymousAccount, value); }
		}

		/// <summary>The ClientId property of the Entity Customer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Customer"."ClientId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ClientId
		{
			get { return (Nullable<System.Int32>)GetValue((int)CustomerFieldIndex.ClientId, false); }
			set	{ SetValue((int)CustomerFieldIndex.ClientId, value); }
		}

		/// <summary>The NewEmailAddress property of the Entity Customer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Customer"."NewEmailAddress".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String NewEmailAddress
		{
			get { return (System.String)GetValue((int)CustomerFieldIndex.NewEmailAddress, true); }
			set	{ SetValue((int)CustomerFieldIndex.NewEmailAddress, value); }
		}

		/// <summary>The NewEmailAddressConfirmationGuid property of the Entity Customer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Customer"."NewEmailAddressConfirmationGuid".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String NewEmailAddressConfirmationGuid
		{
			get { return (System.String)GetValue((int)CustomerFieldIndex.NewEmailAddressConfirmationGuid, true); }
			set	{ SetValue((int)CustomerFieldIndex.NewEmailAddressConfirmationGuid, value); }
		}

		/// <summary>The NewEmailAddressVerified property of the Entity Customer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Customer"."NewEmailAddressVerified".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean NewEmailAddressVerified
		{
			get { return (System.Boolean)GetValue((int)CustomerFieldIndex.NewEmailAddressVerified, true); }
			set	{ SetValue((int)CustomerFieldIndex.NewEmailAddressVerified, value); }
		}

		/// <summary>The CampaignName property of the Entity Customer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Customer"."CampaignName".<br/>Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CampaignName
		{
			get { return (System.String)GetValue((int)CustomerFieldIndex.CampaignName, true); }
			set	{ SetValue((int)CustomerFieldIndex.CampaignName, value); }
		}

		/// <summary>The CampaignSource property of the Entity Customer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Customer"."CampaignSource".<br/>Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CampaignSource
		{
			get { return (System.String)GetValue((int)CustomerFieldIndex.CampaignSource, true); }
			set	{ SetValue((int)CustomerFieldIndex.CampaignSource, value); }
		}

		/// <summary>The CampaignMedium property of the Entity Customer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Customer"."CampaignMedium".<br/>Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CampaignMedium
		{
			get { return (System.String)GetValue((int)CustomerFieldIndex.CampaignMedium, true); }
			set	{ SetValue((int)CustomerFieldIndex.CampaignMedium, value); }
		}

		/// <summary>The CampaignContent property of the Entity Customer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Customer"."CampaignContent".<br/>Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CampaignContent
		{
			get { return (System.String)GetValue((int)CustomerFieldIndex.CampaignContent, true); }
			set	{ SetValue((int)CustomerFieldIndex.CampaignContent, value); }
		}

		/// <summary>The CreatedBy property of the Entity Customer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Customer"."CreatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)CustomerFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)CustomerFieldIndex.CreatedBy, value); }
		}

		/// <summary>The UpdatedBy property of the Entity Customer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Customer"."UpdatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)CustomerFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)CustomerFieldIndex.UpdatedBy, value); }
		}

		/// <summary>The GUID property of the Entity Customer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Customer"."GUID".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String GUID
		{
			get { return (System.String)GetValue((int)CustomerFieldIndex.GUID, true); }
			set	{ SetValue((int)CustomerFieldIndex.GUID, value); }
		}

		/// <summary>The CanSingleSignOn property of the Entity Customer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Customer"."CanSingleSignOn".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean CanSingleSignOn
		{
			get { return (System.Boolean)GetValue((int)CustomerFieldIndex.CanSingleSignOn, true); }
			set	{ SetValue((int)CustomerFieldIndex.CanSingleSignOn, value); }
		}

		/// <summary>The CreatedUTC property of the Entity Customer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Customer"."CreatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)CustomerFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)CustomerFieldIndex.CreatedUTC, value); }
		}

		/// <summary>The UpdatedUTC property of the Entity Customer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Customer"."UpdatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)CustomerFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)CustomerFieldIndex.UpdatedUTC, value); }
		}

		/// <summary>The PasswordResetLinkGeneratedUTC property of the Entity Customer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Customer"."PasswordResetLinkGeneratedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> PasswordResetLinkGeneratedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)CustomerFieldIndex.PasswordResetLinkGeneratedUTC, false); }
			set	{ SetValue((int)CustomerFieldIndex.PasswordResetLinkGeneratedUTC, value); }
		}

		/// <summary>The LastFailedPasswordAttemptUTC property of the Entity Customer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Customer"."LastFailedPasswordAttemptUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> LastFailedPasswordAttemptUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)CustomerFieldIndex.LastFailedPasswordAttemptUTC, false); }
			set	{ SetValue((int)CustomerFieldIndex.LastFailedPasswordAttemptUTC, value); }
		}

		/// <summary>Gets the EntityCollection with the related entities of type 'DeviceEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(DeviceEntity))]
		public virtual EntityCollection<DeviceEntity> DeviceCollection { get { return GetOrCreateEntityCollection<DeviceEntity, DeviceEntityFactory>("Customer", true, false, ref _deviceCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'NetmessageEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(NetmessageEntity))]
		public virtual EntityCollection<NetmessageEntity> NetmessageCollection { get { return GetOrCreateEntityCollection<NetmessageEntity, NetmessageEntityFactory>("Customer", true, false, ref _netmessageCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'NetmessageEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(NetmessageEntity))]
		public virtual EntityCollection<NetmessageEntity> NetmessageCollection1 { get { return GetOrCreateEntityCollection<NetmessageEntity, NetmessageEntityFactory>("CustomerEntity1", true, false, ref _netmessageCollection1); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'OrderEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(OrderEntity))]
		public virtual EntityCollection<OrderEntity> OrderCollection { get { return GetOrCreateEntityCollection<OrderEntity, OrderEntityFactory>("Customer", true, false, ref _orderCollection); } }

		/// <summary>Gets / sets related entity of type 'ClientEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual ClientEntity Client
		{
			get { return _client; }
			set { SetSingleRelatedEntityNavigator(value, "Client"); }
		}


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen
{
	public enum CustomerFieldIndex
	{
		///<summary>CustomerId. </summary>
		CustomerId,
		///<summary>Email. </summary>
		Email,
		///<summary>Phonenumber. </summary>
		Phonenumber,
		///<summary>Password. </summary>
		Password,
		///<summary>PasswordSalt. </summary>
		PasswordSalt,
		///<summary>CommunicationSalt. </summary>
		CommunicationSalt,
		///<summary>Verified. </summary>
		Verified,
		///<summary>Blacklisted. </summary>
		Blacklisted,
		///<summary>BlacklistedNotes. </summary>
		BlacklistedNotes,
		///<summary>Firstname. </summary>
		Firstname,
		///<summary>Lastname. </summary>
		Lastname,
		///<summary>LastnamePrefix. </summary>
		LastnamePrefix,
		///<summary>RandomValue. </summary>
		RandomValue,
		///<summary>Gender. </summary>
		Gender,
		///<summary>Addressline1. </summary>
		Addressline1,
		///<summary>Addressline2. </summary>
		Addressline2,
		///<summary>Zipcode. </summary>
		Zipcode,
		///<summary>City. </summary>
		City,
		///<summary>CountryCode. </summary>
		CountryCode,
		///<summary>Notes. </summary>
		Notes,
		///<summary>SmsRequestsSent. </summary>
		SmsRequestsSent,
		///<summary>SmsCreateRequests. </summary>
		SmsCreateRequests,
		///<summary>SmsResetPincodeRequests. </summary>
		SmsResetPincodeRequests,
		///<summary>SmsUnlockAccountRequests. </summary>
		SmsUnlockAccountRequests,
		///<summary>SmsNonProcessableRequests. </summary>
		SmsNonProcessableRequests,
		///<summary>HasChangedInititialPincode. </summary>
		HasChangedInititialPincode,
		///<summary>InitialLinkIdentifier. </summary>
		InitialLinkIdentifier,
		///<summary>PasswordResetLinkIdentifier. </summary>
		PasswordResetLinkIdentifier,
		///<summary>PasswordResetLinkFailedAtttempts. </summary>
		PasswordResetLinkFailedAtttempts,
		///<summary>FailedPasswordAttemptCount. </summary>
		FailedPasswordAttemptCount,
		///<summary>RememberOnMobileWebsite. </summary>
		RememberOnMobileWebsite,
		///<summary>AnonymousAccount. </summary>
		AnonymousAccount,
		///<summary>ClientId. </summary>
		ClientId,
		///<summary>NewEmailAddress. </summary>
		NewEmailAddress,
		///<summary>NewEmailAddressConfirmationGuid. </summary>
		NewEmailAddressConfirmationGuid,
		///<summary>NewEmailAddressVerified. </summary>
		NewEmailAddressVerified,
		///<summary>CampaignName. </summary>
		CampaignName,
		///<summary>CampaignSource. </summary>
		CampaignSource,
		///<summary>CampaignMedium. </summary>
		CampaignMedium,
		///<summary>CampaignContent. </summary>
		CampaignContent,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>GUID. </summary>
		GUID,
		///<summary>CanSingleSignOn. </summary>
		CanSingleSignOn,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>PasswordResetLinkGeneratedUTC. </summary>
		PasswordResetLinkGeneratedUTC,
		///<summary>LastFailedPasswordAttemptUTC. </summary>
		LastFailedPasswordAttemptUTC,
		/// <summary></summary>
		AmountOfFields
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Customer. </summary>
	public partial class CustomerRelations: RelationFactory
	{
		#region Custom Relation code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomRelationCode
        private const ReferentialConstraintDeleteRule DeleteRuleForDeviceEntityUsingCustomerId = ReferentialConstraintDeleteRule.SetNull;
        private const ReferentialConstraintDeleteRule DeleteRuleForNetmessageEntityUsingSenderCustomerId = ReferentialConstraintDeleteRule.Cascade;
        private const ReferentialConstraintDeleteRule DeleteRuleForNetmessageEntityUsingReceiverCustomerId = ReferentialConstraintDeleteRule.Cascade;
        private const ReferentialConstraintDeleteRule DeleteRuleForOrderEntityUsingCustomerId = ReferentialConstraintDeleteRule.SetNull;
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		
		// private const ReferentialConstraintDeleteRule DeleteRuleForDeviceEntityUsingCustomerId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForNetmessageEntityUsingSenderCustomerId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForNetmessageEntityUsingReceiverCustomerId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForOrderEntityUsingCustomerId = ReferentialConstraintDeleteRule


		/// <summary>Gets all relations and delete rules of the CustomerEntity as a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects.</summary>
		/// <returns>a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects</returns>
		public virtual Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> GetRelationsAndDeleteRules()
		{
			Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> toReturn = new Dictionary<IEntityRelation, ReferentialConstraintDeleteRule>();
			toReturn.Add(this.DeviceEntityUsingCustomerId, CustomerRelations.DeleteRuleForDeviceEntityUsingCustomerId);
			toReturn.Add(this.NetmessageEntityUsingSenderCustomerId, CustomerRelations.DeleteRuleForNetmessageEntityUsingSenderCustomerId);
			toReturn.Add(this.NetmessageEntityUsingReceiverCustomerId, CustomerRelations.DeleteRuleForNetmessageEntityUsingReceiverCustomerId);
			toReturn.Add(this.OrderEntityUsingCustomerId, CustomerRelations.DeleteRuleForOrderEntityUsingCustomerId);
			return toReturn;
		}		
	
	
		/// <summary>Returns a new IEntityRelation object, between CustomerEntity and DeviceEntity over the 1:n relation they have, using the relation between the fields: Customer.CustomerId - Device.CustomerId</summary>
		public virtual IEntityRelation DeviceEntityUsingCustomerId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "DeviceCollection", true, new[] { CustomerFields.CustomerId, DeviceFields.CustomerId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between CustomerEntity and NetmessageEntity over the 1:n relation they have, using the relation between the fields: Customer.CustomerId - Netmessage.SenderCustomerId</summary>
		public virtual IEntityRelation NetmessageEntityUsingSenderCustomerId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "NetmessageCollection", true, new[] { CustomerFields.CustomerId, NetmessageFields.SenderCustomerId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between CustomerEntity and NetmessageEntity over the 1:n relation they have, using the relation between the fields: Customer.CustomerId - Netmessage.ReceiverCustomerId</summary>
		public virtual IEntityRelation NetmessageEntityUsingReceiverCustomerId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "NetmessageCollection1", true, new[] { CustomerFields.CustomerId, NetmessageFields.ReceiverCustomerId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between CustomerEntity and OrderEntity over the 1:n relation they have, using the relation between the fields: Customer.CustomerId - Order.CustomerId</summary>
		public virtual IEntityRelation OrderEntityUsingCustomerId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "OrderCollection", true, new[] { CustomerFields.CustomerId, OrderFields.CustomerId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between CustomerEntity and ClientEntity over the m:1 relation they have, using the relation between the fields: Customer.ClientId - Client.ClientId</summary>
		public virtual IEntityRelation ClientEntityUsingClientId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Client", false, new[] { ClientFields.ClientId, CustomerFields.ClientId }); }
		}

	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticCustomerRelations
	{
		internal static readonly IEntityRelation DeviceEntityUsingCustomerIdStatic = new CustomerRelations().DeviceEntityUsingCustomerId;
		internal static readonly IEntityRelation NetmessageEntityUsingSenderCustomerIdStatic = new CustomerRelations().NetmessageEntityUsingSenderCustomerId;
		internal static readonly IEntityRelation NetmessageEntityUsingReceiverCustomerIdStatic = new CustomerRelations().NetmessageEntityUsingReceiverCustomerId;
		internal static readonly IEntityRelation OrderEntityUsingCustomerIdStatic = new CustomerRelations().OrderEntityUsingCustomerId;
		internal static readonly IEntityRelation ClientEntityUsingClientIdStatic = new CustomerRelations().ClientEntityUsingClientId;

		/// <summary>CTor</summary>
		static StaticCustomerRelations() { }
	}
}

