﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 5.8
// Code is generated on: 
// Code is generated using templates: Crave.TemplateBindings
// Templates vendor: Crave Interactive
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.FactoryClasses;
using Crave.Ordering.Shared.Data.LLBLGen.RelationClasses;
using System.Diagnostics;
using SD.LLBLGen.Pro.ORMSupportClasses;

// ReSharper disable All
namespace Crave.Ordering.Shared.Data.LLBLGen.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'ExternalProduct'.<br/><br/></summary>
	[Serializable]
	[DebuggerDisplay("ExternalProductId={ExternalProductId}, Name={Name}")]
	public partial class ExternalProductEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END
	
	{
		private EntityCollection<AlterationEntity> _alterationCollection;
		private EntityCollection<AlterationoptionEntity> _alterationoptionCollection;
		private EntityCollection<ExternalSubProductEntity> _externalSubProductCollection;
		private EntityCollection<ExternalSubProductEntity> _externalSubProductCollection1;
		private EntityCollection<ProductEntity> _productCollection;
		private ExternalMenuEntity _externalMenu;
		private ExternalSystemEntity _externalSystem;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		private static ExternalProductEntityStaticMetaData _staticMetaData = new ExternalProductEntityStaticMetaData();
		private static ExternalProductRelations _relationsFactory = new ExternalProductRelations();		

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name ExternalMenu</summary>
			public static readonly string ExternalMenu = "ExternalMenu";
			/// <summary>Member name ExternalSystem</summary>
			public static readonly string ExternalSystem = "ExternalSystem";
			/// <summary>Member name AlterationCollection</summary>
			public static readonly string AlterationCollection = "AlterationCollection";
			/// <summary>Member name AlterationoptionCollection</summary>
			public static readonly string AlterationoptionCollection = "AlterationoptionCollection";
			/// <summary>Member name ExternalSubProductCollection</summary>
			public static readonly string ExternalSubProductCollection = "ExternalSubProductCollection";
			/// <summary>Member name ExternalSubProductCollection1</summary>
			public static readonly string ExternalSubProductCollection1 = "ExternalSubProductCollection1";
			/// <summary>Member name ProductCollection</summary>
			public static readonly string ProductCollection = "ProductCollection";
		}

		/// <summary>Static meta-data storage for navigator related information</summary>
		protected class ExternalProductEntityStaticMetaData : EntityStaticMetaDataBase
		{
			public ExternalProductEntityStaticMetaData()
			{
				SetEntityCoreInfo("ExternalProductEntity", InheritanceHierarchyType.None, false, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ExternalProductEntity, typeof(ExternalProductEntity), typeof(ExternalProductEntityFactory), false);
				AddNavigatorMetaData<ExternalProductEntity, EntityCollection<AlterationEntity>>("AlterationCollection", a => a._alterationCollection, (a, b) => a._alterationCollection = b, a => a.AlterationCollection, () => new ExternalProductRelations().AlterationEntityUsingExternalProductId, typeof(AlterationEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.AlterationEntity);
				AddNavigatorMetaData<ExternalProductEntity, EntityCollection<AlterationoptionEntity>>("AlterationoptionCollection", a => a._alterationoptionCollection, (a, b) => a._alterationoptionCollection = b, a => a.AlterationoptionCollection, () => new ExternalProductRelations().AlterationoptionEntityUsingExternalProductId, typeof(AlterationoptionEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.AlterationoptionEntity);
				AddNavigatorMetaData<ExternalProductEntity, EntityCollection<ExternalSubProductEntity>>("ExternalSubProductCollection", a => a._externalSubProductCollection, (a, b) => a._externalSubProductCollection = b, a => a.ExternalSubProductCollection, () => new ExternalProductRelations().ExternalSubProductEntityUsingExternalProductId, typeof(ExternalSubProductEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ExternalSubProductEntity);
				AddNavigatorMetaData<ExternalProductEntity, EntityCollection<ExternalSubProductEntity>>("ExternalSubProductCollection1", a => a._externalSubProductCollection1, (a, b) => a._externalSubProductCollection1 = b, a => a.ExternalSubProductCollection1, () => new ExternalProductRelations().ExternalSubProductEntityUsingExternalSubProductId, typeof(ExternalSubProductEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ExternalSubProductEntity);
				AddNavigatorMetaData<ExternalProductEntity, EntityCollection<ProductEntity>>("ProductCollection", a => a._productCollection, (a, b) => a._productCollection = b, a => a.ProductCollection, () => new ExternalProductRelations().ProductEntityUsingExternalProductId, typeof(ProductEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ProductEntity);
				AddNavigatorMetaData<ExternalProductEntity, ExternalMenuEntity>("ExternalMenu", "ExternalProductCollection", (a, b) => a._externalMenu = b, a => a._externalMenu, (a, b) => a.ExternalMenu = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticExternalProductRelations.ExternalMenuEntityUsingExternalMenuIdStatic, ()=>new ExternalProductRelations().ExternalMenuEntityUsingExternalMenuId, null, new int[] { (int)ExternalProductFieldIndex.ExternalMenuId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ExternalMenuEntity);
				AddNavigatorMetaData<ExternalProductEntity, ExternalSystemEntity>("ExternalSystem", "ExternalProductCollection", (a, b) => a._externalSystem = b, a => a._externalSystem, (a, b) => a.ExternalSystem = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticExternalProductRelations.ExternalSystemEntityUsingExternalSystemIdStatic, ()=>new ExternalProductRelations().ExternalSystemEntityUsingExternalSystemId, null, new int[] { (int)ExternalProductFieldIndex.ExternalSystemId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ExternalSystemEntity);
			}
		}

		/// <summary>Static ctor</summary>
		static ExternalProductEntity()
		{
		}

		/// <summary> CTor</summary>
		public ExternalProductEntity()
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public ExternalProductEntity(IEntityFields2 fields)
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this ExternalProductEntity</param>
		public ExternalProductEntity(IValidator validator)
		{
			InitClassEmpty(validator, null);
		}

		/// <summary> CTor</summary>
		/// <param name="externalProductId">PK value for ExternalProduct which data should be fetched into this ExternalProduct object</param>
		public ExternalProductEntity(System.Int32 externalProductId) : this(externalProductId, null)
		{
		}

		/// <summary> CTor</summary>
		/// <param name="externalProductId">PK value for ExternalProduct which data should be fetched into this ExternalProduct object</param>
		/// <param name="validator">The custom validator object for this ExternalProductEntity</param>
		public ExternalProductEntity(System.Int32 externalProductId, IValidator validator)
		{
			InitClassEmpty(validator, null);
			this.ExternalProductId = externalProductId;
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ExternalProductEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Alteration' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoAlterationCollection() { return CreateRelationInfoForNavigator("AlterationCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Alterationoption' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoAlterationoptionCollection() { return CreateRelationInfoForNavigator("AlterationoptionCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'ExternalSubProduct' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoExternalSubProductCollection() { return CreateRelationInfoForNavigator("ExternalSubProductCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'ExternalSubProduct' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoExternalSubProductCollection1() { return CreateRelationInfoForNavigator("ExternalSubProductCollection1"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Product' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoProductCollection() { return CreateRelationInfoForNavigator("ProductCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'ExternalMenu' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoExternalMenu() { return CreateRelationInfoForNavigator("ExternalMenu"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'ExternalSystem' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoExternalSystem() { return CreateRelationInfoForNavigator("ExternalSystem"); }
		
		/// <inheritdoc/>
		protected override EntityStaticMetaDataBase GetEntityStaticMetaData() {	return _staticMetaData; }

		/// <summary>Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		/// <summary>Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this ExternalProductEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary>The relations object holding all relations of this entity with other entity classes.</summary>
		public static ExternalProductRelations Relations { get { return _relationsFactory; } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Alteration' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathAlterationCollection { get { return _staticMetaData.GetPrefetchPathElement("AlterationCollection", CommonEntityBase.CreateEntityCollection<AlterationEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Alterationoption' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathAlterationoptionCollection { get { return _staticMetaData.GetPrefetchPathElement("AlterationoptionCollection", CommonEntityBase.CreateEntityCollection<AlterationoptionEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'ExternalSubProduct' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathExternalSubProductCollection { get { return _staticMetaData.GetPrefetchPathElement("ExternalSubProductCollection", CommonEntityBase.CreateEntityCollection<ExternalSubProductEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'ExternalSubProduct' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathExternalSubProductCollection1 { get { return _staticMetaData.GetPrefetchPathElement("ExternalSubProductCollection1", CommonEntityBase.CreateEntityCollection<ExternalSubProductEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Product' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathProductCollection { get { return _staticMetaData.GetPrefetchPathElement("ProductCollection", CommonEntityBase.CreateEntityCollection<ProductEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'ExternalMenu' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathExternalMenuEntity { get { return _staticMetaData.GetPrefetchPathElement("ExternalMenu", CommonEntityBase.CreateEntityCollection<ExternalMenuEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'ExternalSystem' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathExternalSystemEntity { get { return _staticMetaData.GetPrefetchPathElement("ExternalSystem", CommonEntityBase.CreateEntityCollection<ExternalSystemEntity>()); } }

		/// <summary>The ExternalProductId property of the Entity ExternalProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalProduct"."ExternalProductId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 ExternalProductId
		{
			get { return (System.Int32)GetValue((int)ExternalProductFieldIndex.ExternalProductId, true); }
			set { SetValue((int)ExternalProductFieldIndex.ExternalProductId, value); }		}

		/// <summary>The ExternalSystemId property of the Entity ExternalProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalProduct"."ExternalSystemId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ExternalSystemId
		{
			get { return (System.Int32)GetValue((int)ExternalProductFieldIndex.ExternalSystemId, true); }
			set	{ SetValue((int)ExternalProductFieldIndex.ExternalSystemId, value); }
		}

		/// <summary>The ParentCompanyId property of the Entity ExternalProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalProduct"."ParentCompanyId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ParentCompanyId
		{
			get { return (System.Int32)GetValue((int)ExternalProductFieldIndex.ParentCompanyId, true); }
			set	{ SetValue((int)ExternalProductFieldIndex.ParentCompanyId, value); }
		}

		/// <summary>The Id property of the Entity ExternalProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalProduct"."Id".<br/>Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 50.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Id
		{
			get { return (System.String)GetValue((int)ExternalProductFieldIndex.Id, true); }
			set	{ SetValue((int)ExternalProductFieldIndex.Id, value); }
		}

		/// <summary>The Name property of the Entity ExternalProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalProduct"."Name".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 100.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)ExternalProductFieldIndex.Name, true); }
			set	{ SetValue((int)ExternalProductFieldIndex.Name, value); }
		}

		/// <summary>The Price property of the Entity ExternalProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalProduct"."Price".<br/>Table field type characteristics (type, precision, scale, length): Money, 19, 4, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal Price
		{
			get { return (System.Decimal)GetValue((int)ExternalProductFieldIndex.Price, true); }
			set	{ SetValue((int)ExternalProductFieldIndex.Price, value); }
		}

		/// <summary>The Visible property of the Entity ExternalProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalProduct"."Visible".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean Visible
		{
			get { return (System.Boolean)GetValue((int)ExternalProductFieldIndex.Visible, true); }
			set	{ SetValue((int)ExternalProductFieldIndex.Visible, value); }
		}

		/// <summary>The StringValue1 property of the Entity ExternalProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalProduct"."StringValue1".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String StringValue1
		{
			get { return (System.String)GetValue((int)ExternalProductFieldIndex.StringValue1, true); }
			set	{ SetValue((int)ExternalProductFieldIndex.StringValue1, value); }
		}

		/// <summary>The StringValue2 property of the Entity ExternalProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalProduct"."StringValue2".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String StringValue2
		{
			get { return (System.String)GetValue((int)ExternalProductFieldIndex.StringValue2, true); }
			set	{ SetValue((int)ExternalProductFieldIndex.StringValue2, value); }
		}

		/// <summary>The StringValue3 property of the Entity ExternalProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalProduct"."StringValue3".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String StringValue3
		{
			get { return (System.String)GetValue((int)ExternalProductFieldIndex.StringValue3, true); }
			set	{ SetValue((int)ExternalProductFieldIndex.StringValue3, value); }
		}

		/// <summary>The StringValue4 property of the Entity ExternalProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalProduct"."StringValue4".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String StringValue4
		{
			get { return (System.String)GetValue((int)ExternalProductFieldIndex.StringValue4, true); }
			set	{ SetValue((int)ExternalProductFieldIndex.StringValue4, value); }
		}

		/// <summary>The StringValue5 property of the Entity ExternalProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalProduct"."StringValue5".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String StringValue5
		{
			get { return (System.String)GetValue((int)ExternalProductFieldIndex.StringValue5, true); }
			set	{ SetValue((int)ExternalProductFieldIndex.StringValue5, value); }
		}

		/// <summary>The IntValue1 property of the Entity ExternalProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalProduct"."IntValue1".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> IntValue1
		{
			get { return (Nullable<System.Int32>)GetValue((int)ExternalProductFieldIndex.IntValue1, false); }
			set	{ SetValue((int)ExternalProductFieldIndex.IntValue1, value); }
		}

		/// <summary>The IntValue2 property of the Entity ExternalProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalProduct"."IntValue2".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> IntValue2
		{
			get { return (Nullable<System.Int32>)GetValue((int)ExternalProductFieldIndex.IntValue2, false); }
			set	{ SetValue((int)ExternalProductFieldIndex.IntValue2, value); }
		}

		/// <summary>The IntValue3 property of the Entity ExternalProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalProduct"."IntValue3".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> IntValue3
		{
			get { return (Nullable<System.Int32>)GetValue((int)ExternalProductFieldIndex.IntValue3, false); }
			set	{ SetValue((int)ExternalProductFieldIndex.IntValue3, value); }
		}

		/// <summary>The IntValue4 property of the Entity ExternalProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalProduct"."IntValue4".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> IntValue4
		{
			get { return (Nullable<System.Int32>)GetValue((int)ExternalProductFieldIndex.IntValue4, false); }
			set	{ SetValue((int)ExternalProductFieldIndex.IntValue4, value); }
		}

		/// <summary>The IntValue5 property of the Entity ExternalProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalProduct"."IntValue5".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> IntValue5
		{
			get { return (Nullable<System.Int32>)GetValue((int)ExternalProductFieldIndex.IntValue5, false); }
			set	{ SetValue((int)ExternalProductFieldIndex.IntValue5, value); }
		}

		/// <summary>The BoolValue1 property of the Entity ExternalProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalProduct"."BoolValue1".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> BoolValue1
		{
			get { return (Nullable<System.Boolean>)GetValue((int)ExternalProductFieldIndex.BoolValue1, false); }
			set	{ SetValue((int)ExternalProductFieldIndex.BoolValue1, value); }
		}

		/// <summary>The BoolValue2 property of the Entity ExternalProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalProduct"."BoolValue2".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> BoolValue2
		{
			get { return (Nullable<System.Boolean>)GetValue((int)ExternalProductFieldIndex.BoolValue2, false); }
			set	{ SetValue((int)ExternalProductFieldIndex.BoolValue2, value); }
		}

		/// <summary>The BoolValue3 property of the Entity ExternalProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalProduct"."BoolValue3".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> BoolValue3
		{
			get { return (Nullable<System.Boolean>)GetValue((int)ExternalProductFieldIndex.BoolValue3, false); }
			set	{ SetValue((int)ExternalProductFieldIndex.BoolValue3, value); }
		}

		/// <summary>The BoolValue4 property of the Entity ExternalProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalProduct"."BoolValue4".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> BoolValue4
		{
			get { return (Nullable<System.Boolean>)GetValue((int)ExternalProductFieldIndex.BoolValue4, false); }
			set	{ SetValue((int)ExternalProductFieldIndex.BoolValue4, value); }
		}

		/// <summary>The BoolValue5 property of the Entity ExternalProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalProduct"."BoolValue5".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> BoolValue5
		{
			get { return (Nullable<System.Boolean>)GetValue((int)ExternalProductFieldIndex.BoolValue5, false); }
			set	{ SetValue((int)ExternalProductFieldIndex.BoolValue5, value); }
		}

		/// <summary>The CreatedInBatchId property of the Entity ExternalProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalProduct"."CreatedInBatchId".<br/>Table field type characteristics (type, precision, scale, length): BigInt, 19, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CreatedInBatchId
		{
			get { return (Nullable<System.Int64>)GetValue((int)ExternalProductFieldIndex.CreatedInBatchId, false); }
			set	{ SetValue((int)ExternalProductFieldIndex.CreatedInBatchId, value); }
		}

		/// <summary>The UpdatedInBatchId property of the Entity ExternalProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalProduct"."UpdatedInBatchId".<br/>Table field type characteristics (type, precision, scale, length): BigInt, 19, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> UpdatedInBatchId
		{
			get { return (Nullable<System.Int64>)GetValue((int)ExternalProductFieldIndex.UpdatedInBatchId, false); }
			set	{ SetValue((int)ExternalProductFieldIndex.UpdatedInBatchId, value); }
		}

		/// <summary>The SynchronisationBatchId property of the Entity ExternalProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalProduct"."SynchronisationBatchId".<br/>Table field type characteristics (type, precision, scale, length): BigInt, 19, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> SynchronisationBatchId
		{
			get { return (Nullable<System.Int64>)GetValue((int)ExternalProductFieldIndex.SynchronisationBatchId, false); }
			set	{ SetValue((int)ExternalProductFieldIndex.SynchronisationBatchId, value); }
		}

		/// <summary>The CreatedUTC property of the Entity ExternalProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalProduct"."CreatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ExternalProductFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)ExternalProductFieldIndex.CreatedUTC, value); }
		}

		/// <summary>The CreatedBy property of the Entity ExternalProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalProduct"."CreatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CreatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)ExternalProductFieldIndex.CreatedBy, false); }
			set	{ SetValue((int)ExternalProductFieldIndex.CreatedBy, value); }
		}

		/// <summary>The UpdatedUTC property of the Entity ExternalProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalProduct"."UpdatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ExternalProductFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)ExternalProductFieldIndex.UpdatedUTC, value); }
		}

		/// <summary>The UpdatedBy property of the Entity ExternalProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalProduct"."UpdatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UpdatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)ExternalProductFieldIndex.UpdatedBy, false); }
			set	{ SetValue((int)ExternalProductFieldIndex.UpdatedBy, value); }
		}

		/// <summary>The Type property of the Entity ExternalProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalProduct"."Type".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Crave.Enums.ExternalProductType Type
		{
			get { return (Crave.Enums.ExternalProductType)GetValue((int)ExternalProductFieldIndex.Type, true); }
			set	{ SetValue((int)ExternalProductFieldIndex.Type, value); }
		}

		/// <summary>The MinOptions property of the Entity ExternalProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalProduct"."MinOptions".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 MinOptions
		{
			get { return (System.Int32)GetValue((int)ExternalProductFieldIndex.MinOptions, true); }
			set	{ SetValue((int)ExternalProductFieldIndex.MinOptions, value); }
		}

		/// <summary>The MaxOptions property of the Entity ExternalProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalProduct"."MaxOptions".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 MaxOptions
		{
			get { return (System.Int32)GetValue((int)ExternalProductFieldIndex.MaxOptions, true); }
			set	{ SetValue((int)ExternalProductFieldIndex.MaxOptions, value); }
		}

		/// <summary>The AllowDuplicates property of the Entity ExternalProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalProduct"."AllowDuplicates".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean AllowDuplicates
		{
			get { return (System.Boolean)GetValue((int)ExternalProductFieldIndex.AllowDuplicates, true); }
			set	{ SetValue((int)ExternalProductFieldIndex.AllowDuplicates, value); }
		}

		/// <summary>The IsEnabled property of the Entity ExternalProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalProduct"."IsEnabled".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsEnabled
		{
			get { return (System.Boolean)GetValue((int)ExternalProductFieldIndex.IsEnabled, true); }
			set	{ SetValue((int)ExternalProductFieldIndex.IsEnabled, value); }
		}

		/// <summary>The IsSnoozed property of the Entity ExternalProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalProduct"."IsSnoozed".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsSnoozed
		{
			get { return (System.Boolean)GetValue((int)ExternalProductFieldIndex.IsSnoozed, true); }
			set	{ SetValue((int)ExternalProductFieldIndex.IsSnoozed, value); }
		}

		/// <summary>The ExternalMenuId property of the Entity ExternalProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalProduct"."ExternalMenuId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ExternalMenuId
		{
			get { return (Nullable<System.Int32>)GetValue((int)ExternalProductFieldIndex.ExternalMenuId, false); }
			set	{ SetValue((int)ExternalProductFieldIndex.ExternalMenuId, value); }
		}

		/// <summary>Gets the EntityCollection with the related entities of type 'AlterationEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(AlterationEntity))]
		public virtual EntityCollection<AlterationEntity> AlterationCollection { get { return GetOrCreateEntityCollection<AlterationEntity, AlterationEntityFactory>("ExternalProduct", true, false, ref _alterationCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'AlterationoptionEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(AlterationoptionEntity))]
		public virtual EntityCollection<AlterationoptionEntity> AlterationoptionCollection { get { return GetOrCreateEntityCollection<AlterationoptionEntity, AlterationoptionEntityFactory>("ExternalProduct", true, false, ref _alterationoptionCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'ExternalSubProductEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(ExternalSubProductEntity))]
		public virtual EntityCollection<ExternalSubProductEntity> ExternalSubProductCollection { get { return GetOrCreateEntityCollection<ExternalSubProductEntity, ExternalSubProductEntityFactory>("ExternalProduct", true, false, ref _externalSubProductCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'ExternalSubProductEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(ExternalSubProductEntity))]
		public virtual EntityCollection<ExternalSubProductEntity> ExternalSubProductCollection1 { get { return GetOrCreateEntityCollection<ExternalSubProductEntity, ExternalSubProductEntityFactory>("ExternalProduct1", true, false, ref _externalSubProductCollection1); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'ProductEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(ProductEntity))]
		public virtual EntityCollection<ProductEntity> ProductCollection { get { return GetOrCreateEntityCollection<ProductEntity, ProductEntityFactory>("ExternalProduct", true, false, ref _productCollection); } }

		/// <summary>Gets / sets related entity of type 'ExternalMenuEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual ExternalMenuEntity ExternalMenu
		{
			get { return _externalMenu; }
			set { SetSingleRelatedEntityNavigator(value, "ExternalMenu"); }
		}

		/// <summary>Gets / sets related entity of type 'ExternalSystemEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual ExternalSystemEntity ExternalSystem
		{
			get { return _externalSystem; }
			set { SetSingleRelatedEntityNavigator(value, "ExternalSystem"); }
		}


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen
{
	public enum ExternalProductFieldIndex
	{
		///<summary>ExternalProductId. </summary>
		ExternalProductId,
		///<summary>ExternalSystemId. </summary>
		ExternalSystemId,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>Id. </summary>
		Id,
		///<summary>Name. </summary>
		Name,
		///<summary>Price. </summary>
		Price,
		///<summary>Visible. </summary>
		Visible,
		///<summary>StringValue1. </summary>
		StringValue1,
		///<summary>StringValue2. </summary>
		StringValue2,
		///<summary>StringValue3. </summary>
		StringValue3,
		///<summary>StringValue4. </summary>
		StringValue4,
		///<summary>StringValue5. </summary>
		StringValue5,
		///<summary>IntValue1. </summary>
		IntValue1,
		///<summary>IntValue2. </summary>
		IntValue2,
		///<summary>IntValue3. </summary>
		IntValue3,
		///<summary>IntValue4. </summary>
		IntValue4,
		///<summary>IntValue5. </summary>
		IntValue5,
		///<summary>BoolValue1. </summary>
		BoolValue1,
		///<summary>BoolValue2. </summary>
		BoolValue2,
		///<summary>BoolValue3. </summary>
		BoolValue3,
		///<summary>BoolValue4. </summary>
		BoolValue4,
		///<summary>BoolValue5. </summary>
		BoolValue5,
		///<summary>CreatedInBatchId. </summary>
		CreatedInBatchId,
		///<summary>UpdatedInBatchId. </summary>
		UpdatedInBatchId,
		///<summary>SynchronisationBatchId. </summary>
		SynchronisationBatchId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>Type. </summary>
		Type,
		///<summary>MinOptions. </summary>
		MinOptions,
		///<summary>MaxOptions. </summary>
		MaxOptions,
		///<summary>AllowDuplicates. </summary>
		AllowDuplicates,
		///<summary>IsEnabled. </summary>
		IsEnabled,
		///<summary>IsSnoozed. </summary>
		IsSnoozed,
		///<summary>ExternalMenuId. </summary>
		ExternalMenuId,
		/// <summary></summary>
		AmountOfFields
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: ExternalProduct. </summary>
	public partial class ExternalProductRelations: RelationFactory
	{
		#region Custom Relation code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomRelationCode
        private const ReferentialConstraintDeleteRule DeleteRuleForAlterationoptionEntityUsingExternalProductId = ReferentialConstraintDeleteRule.SetNull;
        private const ReferentialConstraintDeleteRule DeleteRuleForExternalSubProductEntityUsingExternalProductId = ReferentialConstraintDeleteRule.Cascade;
        private const ReferentialConstraintDeleteRule DeleteRuleForExternalSubProductEntityUsingExternalSubProductId = ReferentialConstraintDeleteRule.Cascade;
		private const ReferentialConstraintDeleteRule DeleteRuleForProductEntityUsingExternalProductId = ReferentialConstraintDeleteRule.SetNull;
        private const ReferentialConstraintDeleteRule DeleteRuleForAlterationEntityUsingExternalProductId = ReferentialConstraintDeleteRule.SetNull;
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		
		// private const ReferentialConstraintDeleteRule DeleteRuleForAlterationEntityUsingExternalProductId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForAlterationoptionEntityUsingExternalProductId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForExternalSubProductEntityUsingExternalProductId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForExternalSubProductEntityUsingExternalSubProductId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForProductEntityUsingExternalProductId = ReferentialConstraintDeleteRule


		/// <summary>Gets all relations and delete rules of the ExternalProductEntity as a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects.</summary>
		/// <returns>a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects</returns>
		public virtual Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> GetRelationsAndDeleteRules()
		{
			Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> toReturn = new Dictionary<IEntityRelation, ReferentialConstraintDeleteRule>();
			toReturn.Add(this.AlterationEntityUsingExternalProductId, ExternalProductRelations.DeleteRuleForAlterationEntityUsingExternalProductId);
			toReturn.Add(this.AlterationoptionEntityUsingExternalProductId, ExternalProductRelations.DeleteRuleForAlterationoptionEntityUsingExternalProductId);
			toReturn.Add(this.ExternalSubProductEntityUsingExternalProductId, ExternalProductRelations.DeleteRuleForExternalSubProductEntityUsingExternalProductId);
			toReturn.Add(this.ExternalSubProductEntityUsingExternalSubProductId, ExternalProductRelations.DeleteRuleForExternalSubProductEntityUsingExternalSubProductId);
			toReturn.Add(this.ProductEntityUsingExternalProductId, ExternalProductRelations.DeleteRuleForProductEntityUsingExternalProductId);
			return toReturn;
		}		
	
	
		/// <summary>Returns a new IEntityRelation object, between ExternalProductEntity and AlterationEntity over the 1:n relation they have, using the relation between the fields: ExternalProduct.ExternalProductId - Alteration.ExternalProductId</summary>
		public virtual IEntityRelation AlterationEntityUsingExternalProductId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "AlterationCollection", true, new[] { ExternalProductFields.ExternalProductId, AlterationFields.ExternalProductId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between ExternalProductEntity and AlterationoptionEntity over the 1:n relation they have, using the relation between the fields: ExternalProduct.ExternalProductId - Alterationoption.ExternalProductId</summary>
		public virtual IEntityRelation AlterationoptionEntityUsingExternalProductId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "AlterationoptionCollection", true, new[] { ExternalProductFields.ExternalProductId, AlterationoptionFields.ExternalProductId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between ExternalProductEntity and ExternalSubProductEntity over the 1:n relation they have, using the relation between the fields: ExternalProduct.ExternalProductId - ExternalSubProduct.ExternalProductId</summary>
		public virtual IEntityRelation ExternalSubProductEntityUsingExternalProductId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "ExternalSubProductCollection", true, new[] { ExternalProductFields.ExternalProductId, ExternalSubProductFields.ExternalProductId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between ExternalProductEntity and ExternalSubProductEntity over the 1:n relation they have, using the relation between the fields: ExternalProduct.ExternalProductId - ExternalSubProduct.ExternalSubProductId</summary>
		public virtual IEntityRelation ExternalSubProductEntityUsingExternalSubProductId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "ExternalSubProductCollection1", true, new[] { ExternalProductFields.ExternalProductId, ExternalSubProductFields.ExternalSubProductId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between ExternalProductEntity and ProductEntity over the 1:n relation they have, using the relation between the fields: ExternalProduct.ExternalProductId - Product.ExternalProductId</summary>
		public virtual IEntityRelation ProductEntityUsingExternalProductId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "ProductCollection", true, new[] { ExternalProductFields.ExternalProductId, ProductFields.ExternalProductId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between ExternalProductEntity and ExternalMenuEntity over the m:1 relation they have, using the relation between the fields: ExternalProduct.ExternalMenuId - ExternalMenu.ExternalMenuId</summary>
		public virtual IEntityRelation ExternalMenuEntityUsingExternalMenuId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "ExternalMenu", false, new[] { ExternalMenuFields.ExternalMenuId, ExternalProductFields.ExternalMenuId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between ExternalProductEntity and ExternalSystemEntity over the m:1 relation they have, using the relation between the fields: ExternalProduct.ExternalSystemId - ExternalSystem.ExternalSystemId</summary>
		public virtual IEntityRelation ExternalSystemEntityUsingExternalSystemId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "ExternalSystem", false, new[] { ExternalSystemFields.ExternalSystemId, ExternalProductFields.ExternalSystemId }); }
		}

	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticExternalProductRelations
	{
		internal static readonly IEntityRelation AlterationEntityUsingExternalProductIdStatic = new ExternalProductRelations().AlterationEntityUsingExternalProductId;
		internal static readonly IEntityRelation AlterationoptionEntityUsingExternalProductIdStatic = new ExternalProductRelations().AlterationoptionEntityUsingExternalProductId;
		internal static readonly IEntityRelation ExternalSubProductEntityUsingExternalProductIdStatic = new ExternalProductRelations().ExternalSubProductEntityUsingExternalProductId;
		internal static readonly IEntityRelation ExternalSubProductEntityUsingExternalSubProductIdStatic = new ExternalProductRelations().ExternalSubProductEntityUsingExternalSubProductId;
		internal static readonly IEntityRelation ProductEntityUsingExternalProductIdStatic = new ExternalProductRelations().ProductEntityUsingExternalProductId;
		internal static readonly IEntityRelation ExternalMenuEntityUsingExternalMenuIdStatic = new ExternalProductRelations().ExternalMenuEntityUsingExternalMenuId;
		internal static readonly IEntityRelation ExternalSystemEntityUsingExternalSystemIdStatic = new ExternalProductRelations().ExternalSystemEntityUsingExternalSystemId;

		/// <summary>CTor</summary>
		static StaticExternalProductRelations() { }
	}
}

