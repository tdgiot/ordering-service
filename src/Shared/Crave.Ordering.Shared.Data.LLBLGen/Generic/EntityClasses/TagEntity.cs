﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 5.8
// Code is generated on: 
// Code is generated using templates: Crave.TemplateBindings
// Templates vendor: Crave Interactive
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.FactoryClasses;
using Crave.Ordering.Shared.Data.LLBLGen.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

// ReSharper disable All
namespace Crave.Ordering.Shared.Data.LLBLGen.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'Tag'.<br/><br/></summary>
	[Serializable]
	public partial class TagEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END
	
	{
		private EntityCollection<AlterationoptionTagEntity> _alterationoptionTagCollection;
		private EntityCollection<OrderitemAlterationitemTagEntity> _orderitemAlterationitemTagCollection;
		private EntityCollection<OrderitemTagEntity> _orderitemTagCollection;
		private EntityCollection<ProductCategoryTagEntity> _productCategoryTagCollection;
		private EntityCollection<ProductTagEntity> _productTagCollection;
		private CompanyEntity _company;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		private static TagEntityStaticMetaData _staticMetaData = new TagEntityStaticMetaData();
		private static TagRelations _relationsFactory = new TagRelations();		

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Company</summary>
			public static readonly string Company = "Company";
			/// <summary>Member name AlterationoptionTagCollection</summary>
			public static readonly string AlterationoptionTagCollection = "AlterationoptionTagCollection";
			/// <summary>Member name OrderitemAlterationitemTagCollection</summary>
			public static readonly string OrderitemAlterationitemTagCollection = "OrderitemAlterationitemTagCollection";
			/// <summary>Member name OrderitemTagCollection</summary>
			public static readonly string OrderitemTagCollection = "OrderitemTagCollection";
			/// <summary>Member name ProductCategoryTagCollection</summary>
			public static readonly string ProductCategoryTagCollection = "ProductCategoryTagCollection";
			/// <summary>Member name ProductTagCollection</summary>
			public static readonly string ProductTagCollection = "ProductTagCollection";
		}

		/// <summary>Static meta-data storage for navigator related information</summary>
		protected class TagEntityStaticMetaData : EntityStaticMetaDataBase
		{
			public TagEntityStaticMetaData()
			{
				SetEntityCoreInfo("TagEntity", InheritanceHierarchyType.None, false, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.TagEntity, typeof(TagEntity), typeof(TagEntityFactory), false);
				AddNavigatorMetaData<TagEntity, EntityCollection<AlterationoptionTagEntity>>("AlterationoptionTagCollection", a => a._alterationoptionTagCollection, (a, b) => a._alterationoptionTagCollection = b, a => a.AlterationoptionTagCollection, () => new TagRelations().AlterationoptionTagEntityUsingTagId, typeof(AlterationoptionTagEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.AlterationoptionTagEntity);
				AddNavigatorMetaData<TagEntity, EntityCollection<OrderitemAlterationitemTagEntity>>("OrderitemAlterationitemTagCollection", a => a._orderitemAlterationitemTagCollection, (a, b) => a._orderitemAlterationitemTagCollection = b, a => a.OrderitemAlterationitemTagCollection, () => new TagRelations().OrderitemAlterationitemTagEntityUsingTagId, typeof(OrderitemAlterationitemTagEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.OrderitemAlterationitemTagEntity);
				AddNavigatorMetaData<TagEntity, EntityCollection<OrderitemTagEntity>>("OrderitemTagCollection", a => a._orderitemTagCollection, (a, b) => a._orderitemTagCollection = b, a => a.OrderitemTagCollection, () => new TagRelations().OrderitemTagEntityUsingTagId, typeof(OrderitemTagEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.OrderitemTagEntity);
				AddNavigatorMetaData<TagEntity, EntityCollection<ProductCategoryTagEntity>>("ProductCategoryTagCollection", a => a._productCategoryTagCollection, (a, b) => a._productCategoryTagCollection = b, a => a.ProductCategoryTagCollection, () => new TagRelations().ProductCategoryTagEntityUsingTagId, typeof(ProductCategoryTagEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ProductCategoryTagEntity);
				AddNavigatorMetaData<TagEntity, EntityCollection<ProductTagEntity>>("ProductTagCollection", a => a._productTagCollection, (a, b) => a._productTagCollection = b, a => a.ProductTagCollection, () => new TagRelations().ProductTagEntityUsingTagId, typeof(ProductTagEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ProductTagEntity);
				AddNavigatorMetaData<TagEntity, CompanyEntity>("Company", "TagCollection", (a, b) => a._company = b, a => a._company, (a, b) => a.Company = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticTagRelations.CompanyEntityUsingCompanyIdStatic, ()=>new TagRelations().CompanyEntityUsingCompanyId, null, new int[] { (int)TagFieldIndex.CompanyId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.CompanyEntity);
			}
		}

		/// <summary>Static ctor</summary>
		static TagEntity()
		{
		}

		/// <summary> CTor</summary>
		public TagEntity()
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public TagEntity(IEntityFields2 fields)
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this TagEntity</param>
		public TagEntity(IValidator validator)
		{
			InitClassEmpty(validator, null);
		}

		/// <summary> CTor</summary>
		/// <param name="tagId">PK value for Tag which data should be fetched into this Tag object</param>
		public TagEntity(System.Int32 tagId) : this(tagId, null)
		{
		}

		/// <summary> CTor</summary>
		/// <param name="tagId">PK value for Tag which data should be fetched into this Tag object</param>
		/// <param name="validator">The custom validator object for this TagEntity</param>
		public TagEntity(System.Int32 tagId, IValidator validator)
		{
			InitClassEmpty(validator, null);
			this.TagId = tagId;
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected TagEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'AlterationoptionTag' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoAlterationoptionTagCollection() { return CreateRelationInfoForNavigator("AlterationoptionTagCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'OrderitemAlterationitemTag' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoOrderitemAlterationitemTagCollection() { return CreateRelationInfoForNavigator("OrderitemAlterationitemTagCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'OrderitemTag' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoOrderitemTagCollection() { return CreateRelationInfoForNavigator("OrderitemTagCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'ProductCategoryTag' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoProductCategoryTagCollection() { return CreateRelationInfoForNavigator("ProductCategoryTagCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'ProductTag' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoProductTagCollection() { return CreateRelationInfoForNavigator("ProductTagCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Company' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoCompany() { return CreateRelationInfoForNavigator("Company"); }
		
		/// <inheritdoc/>
		protected override EntityStaticMetaDataBase GetEntityStaticMetaData() {	return _staticMetaData; }

		/// <summary>Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		/// <summary>Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this TagEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary>The relations object holding all relations of this entity with other entity classes.</summary>
		public static TagRelations Relations { get { return _relationsFactory; } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'AlterationoptionTag' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathAlterationoptionTagCollection { get { return _staticMetaData.GetPrefetchPathElement("AlterationoptionTagCollection", CommonEntityBase.CreateEntityCollection<AlterationoptionTagEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'OrderitemAlterationitemTag' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathOrderitemAlterationitemTagCollection { get { return _staticMetaData.GetPrefetchPathElement("OrderitemAlterationitemTagCollection", CommonEntityBase.CreateEntityCollection<OrderitemAlterationitemTagEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'OrderitemTag' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathOrderitemTagCollection { get { return _staticMetaData.GetPrefetchPathElement("OrderitemTagCollection", CommonEntityBase.CreateEntityCollection<OrderitemTagEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'ProductCategoryTag' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathProductCategoryTagCollection { get { return _staticMetaData.GetPrefetchPathElement("ProductCategoryTagCollection", CommonEntityBase.CreateEntityCollection<ProductCategoryTagEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'ProductTag' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathProductTagCollection { get { return _staticMetaData.GetPrefetchPathElement("ProductTagCollection", CommonEntityBase.CreateEntityCollection<ProductTagEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Company' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathCompanyEntity { get { return _staticMetaData.GetPrefetchPathElement("Company", CommonEntityBase.CreateEntityCollection<CompanyEntity>()); } }

		/// <summary>The TagId property of the Entity Tag<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Tag"."TagId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 TagId
		{
			get { return (System.Int32)GetValue((int)TagFieldIndex.TagId, true); }
			set { SetValue((int)TagFieldIndex.TagId, value); }		}

		/// <summary>The CompanyId property of the Entity Tag<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Tag"."CompanyId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CompanyId
		{
			get { return (Nullable<System.Int32>)GetValue((int)TagFieldIndex.CompanyId, false); }
			set	{ SetValue((int)TagFieldIndex.CompanyId, value); }
		}

		/// <summary>The Name property of the Entity Tag<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Tag"."Name".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)TagFieldIndex.Name, true); }
			set	{ SetValue((int)TagFieldIndex.Name, value); }
		}

		/// <summary>The CreatedUTC property of the Entity Tag<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Tag"."CreatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime CreatedUTC
		{
			get { return (System.DateTime)GetValue((int)TagFieldIndex.CreatedUTC, true); }
			set	{ SetValue((int)TagFieldIndex.CreatedUTC, value); }
		}

		/// <summary>The UpdatedUTC property of the Entity Tag<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Tag"."UpdatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TagFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)TagFieldIndex.UpdatedUTC, value); }
		}

		/// <summary>Gets the EntityCollection with the related entities of type 'AlterationoptionTagEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(AlterationoptionTagEntity))]
		public virtual EntityCollection<AlterationoptionTagEntity> AlterationoptionTagCollection { get { return GetOrCreateEntityCollection<AlterationoptionTagEntity, AlterationoptionTagEntityFactory>("Tag", true, false, ref _alterationoptionTagCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'OrderitemAlterationitemTagEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(OrderitemAlterationitemTagEntity))]
		public virtual EntityCollection<OrderitemAlterationitemTagEntity> OrderitemAlterationitemTagCollection { get { return GetOrCreateEntityCollection<OrderitemAlterationitemTagEntity, OrderitemAlterationitemTagEntityFactory>("Tag", true, false, ref _orderitemAlterationitemTagCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'OrderitemTagEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(OrderitemTagEntity))]
		public virtual EntityCollection<OrderitemTagEntity> OrderitemTagCollection { get { return GetOrCreateEntityCollection<OrderitemTagEntity, OrderitemTagEntityFactory>("Tag", true, false, ref _orderitemTagCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'ProductCategoryTagEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(ProductCategoryTagEntity))]
		public virtual EntityCollection<ProductCategoryTagEntity> ProductCategoryTagCollection { get { return GetOrCreateEntityCollection<ProductCategoryTagEntity, ProductCategoryTagEntityFactory>("Tag", true, false, ref _productCategoryTagCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'ProductTagEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(ProductTagEntity))]
		public virtual EntityCollection<ProductTagEntity> ProductTagCollection { get { return GetOrCreateEntityCollection<ProductTagEntity, ProductTagEntityFactory>("Tag", true, false, ref _productTagCollection); } }

		/// <summary>Gets / sets related entity of type 'CompanyEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual CompanyEntity Company
		{
			get { return _company; }
			set { SetSingleRelatedEntityNavigator(value, "Company"); }
		}


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen
{
	public enum TagFieldIndex
	{
		///<summary>TagId. </summary>
		TagId,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>Name. </summary>
		Name,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Tag. </summary>
	public partial class TagRelations: RelationFactory
	{
		#region Custom Relation code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomRelationCode
        private const ReferentialConstraintDeleteRule DeleteRuleForAlterationoptionTagEntityUsingTagId = ReferentialConstraintDeleteRule.NoAction;
		private const ReferentialConstraintDeleteRule DeleteRuleForOrderitemAlterationitemTagEntityUsingTagId = ReferentialConstraintDeleteRule.NoAction;
		private const ReferentialConstraintDeleteRule DeleteRuleForOrderitemTagEntityUsingTagId = ReferentialConstraintDeleteRule.NoAction;
		private const ReferentialConstraintDeleteRule DeleteRuleForProductCategoryTagEntityUsingTagId = ReferentialConstraintDeleteRule.NoAction;
		private const ReferentialConstraintDeleteRule DeleteRuleForProductTagEntityUsingTagId = ReferentialConstraintDeleteRule.NoAction;
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		
		// private const ReferentialConstraintDeleteRule DeleteRuleForAlterationoptionTagEntityUsingTagId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForOrderitemAlterationitemTagEntityUsingTagId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForOrderitemTagEntityUsingTagId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForProductCategoryTagEntityUsingTagId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForProductTagEntityUsingTagId = ReferentialConstraintDeleteRule


		/// <summary>Gets all relations and delete rules of the TagEntity as a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects.</summary>
		/// <returns>a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects</returns>
		public virtual Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> GetRelationsAndDeleteRules()
		{
			Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> toReturn = new Dictionary<IEntityRelation, ReferentialConstraintDeleteRule>();
			toReturn.Add(this.AlterationoptionTagEntityUsingTagId, TagRelations.DeleteRuleForAlterationoptionTagEntityUsingTagId);
			toReturn.Add(this.OrderitemAlterationitemTagEntityUsingTagId, TagRelations.DeleteRuleForOrderitemAlterationitemTagEntityUsingTagId);
			toReturn.Add(this.OrderitemTagEntityUsingTagId, TagRelations.DeleteRuleForOrderitemTagEntityUsingTagId);
			toReturn.Add(this.ProductCategoryTagEntityUsingTagId, TagRelations.DeleteRuleForProductCategoryTagEntityUsingTagId);
			toReturn.Add(this.ProductTagEntityUsingTagId, TagRelations.DeleteRuleForProductTagEntityUsingTagId);
			return toReturn;
		}		
	
	
		/// <summary>Returns a new IEntityRelation object, between TagEntity and AlterationoptionTagEntity over the 1:n relation they have, using the relation between the fields: Tag.TagId - AlterationoptionTag.TagId</summary>
		public virtual IEntityRelation AlterationoptionTagEntityUsingTagId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "AlterationoptionTagCollection", true, new[] { TagFields.TagId, AlterationoptionTagFields.TagId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between TagEntity and OrderitemAlterationitemTagEntity over the 1:n relation they have, using the relation between the fields: Tag.TagId - OrderitemAlterationitemTag.TagId</summary>
		public virtual IEntityRelation OrderitemAlterationitemTagEntityUsingTagId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "OrderitemAlterationitemTagCollection", true, new[] { TagFields.TagId, OrderitemAlterationitemTagFields.TagId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between TagEntity and OrderitemTagEntity over the 1:n relation they have, using the relation between the fields: Tag.TagId - OrderitemTag.TagId</summary>
		public virtual IEntityRelation OrderitemTagEntityUsingTagId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "OrderitemTagCollection", true, new[] { TagFields.TagId, OrderitemTagFields.TagId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between TagEntity and ProductCategoryTagEntity over the 1:n relation they have, using the relation between the fields: Tag.TagId - ProductCategoryTag.TagId</summary>
		public virtual IEntityRelation ProductCategoryTagEntityUsingTagId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "ProductCategoryTagCollection", true, new[] { TagFields.TagId, ProductCategoryTagFields.TagId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between TagEntity and ProductTagEntity over the 1:n relation they have, using the relation between the fields: Tag.TagId - ProductTag.TagId</summary>
		public virtual IEntityRelation ProductTagEntityUsingTagId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "ProductTagCollection", true, new[] { TagFields.TagId, ProductTagFields.TagId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between TagEntity and CompanyEntity over the m:1 relation they have, using the relation between the fields: Tag.CompanyId - Company.CompanyId</summary>
		public virtual IEntityRelation CompanyEntityUsingCompanyId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Company", false, new[] { CompanyFields.CompanyId, TagFields.CompanyId }); }
		}

	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticTagRelations
	{
		internal static readonly IEntityRelation AlterationoptionTagEntityUsingTagIdStatic = new TagRelations().AlterationoptionTagEntityUsingTagId;
		internal static readonly IEntityRelation OrderitemAlterationitemTagEntityUsingTagIdStatic = new TagRelations().OrderitemAlterationitemTagEntityUsingTagId;
		internal static readonly IEntityRelation OrderitemTagEntityUsingTagIdStatic = new TagRelations().OrderitemTagEntityUsingTagId;
		internal static readonly IEntityRelation ProductCategoryTagEntityUsingTagIdStatic = new TagRelations().ProductCategoryTagEntityUsingTagId;
		internal static readonly IEntityRelation ProductTagEntityUsingTagIdStatic = new TagRelations().ProductTagEntityUsingTagId;
		internal static readonly IEntityRelation CompanyEntityUsingCompanyIdStatic = new TagRelations().CompanyEntityUsingCompanyId;

		/// <summary>CTor</summary>
		static StaticTagRelations() { }
	}
}

