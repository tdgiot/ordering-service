﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 5.8
// Code is generated on: 
// Code is generated using templates: Crave.TemplateBindings
// Templates vendor: Crave Interactive
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.FactoryClasses;
using Crave.Ordering.Shared.Data.LLBLGen.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

// ReSharper disable All
namespace Crave.Ordering.Shared.Data.LLBLGen.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'PriceScheduleItemOccurrence'.<br/><br/></summary>
	[Serializable]
	public partial class PriceScheduleItemOccurrenceEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END
	
	{
		private PriceScheduleItemEntity _priceScheduleItem;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		private static PriceScheduleItemOccurrenceEntityStaticMetaData _staticMetaData = new PriceScheduleItemOccurrenceEntityStaticMetaData();
		private static PriceScheduleItemOccurrenceRelations _relationsFactory = new PriceScheduleItemOccurrenceRelations();		

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name PriceScheduleItem</summary>
			public static readonly string PriceScheduleItem = "PriceScheduleItem";
		}

		/// <summary>Static meta-data storage for navigator related information</summary>
		protected class PriceScheduleItemOccurrenceEntityStaticMetaData : EntityStaticMetaDataBase
		{
			public PriceScheduleItemOccurrenceEntityStaticMetaData()
			{
				SetEntityCoreInfo("PriceScheduleItemOccurrenceEntity", InheritanceHierarchyType.None, false, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.PriceScheduleItemOccurrenceEntity, typeof(PriceScheduleItemOccurrenceEntity), typeof(PriceScheduleItemOccurrenceEntityFactory), false);
				AddNavigatorMetaData<PriceScheduleItemOccurrenceEntity, PriceScheduleItemEntity>("PriceScheduleItem", "PriceScheduleItemOccurrenceCollection", (a, b) => a._priceScheduleItem = b, a => a._priceScheduleItem, (a, b) => a.PriceScheduleItem = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticPriceScheduleItemOccurrenceRelations.PriceScheduleItemEntityUsingPriceScheduleItemIdStatic, ()=>new PriceScheduleItemOccurrenceRelations().PriceScheduleItemEntityUsingPriceScheduleItemId, null, new int[] { (int)PriceScheduleItemOccurrenceFieldIndex.PriceScheduleItemId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.PriceScheduleItemEntity);
			}
		}

		/// <summary>Static ctor</summary>
		static PriceScheduleItemOccurrenceEntity()
		{
		}

		/// <summary> CTor</summary>
		public PriceScheduleItemOccurrenceEntity()
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public PriceScheduleItemOccurrenceEntity(IEntityFields2 fields)
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this PriceScheduleItemOccurrenceEntity</param>
		public PriceScheduleItemOccurrenceEntity(IValidator validator)
		{
			InitClassEmpty(validator, null);
		}

		/// <summary> CTor</summary>
		/// <param name="priceScheduleItemOccurrenceId">PK value for PriceScheduleItemOccurrence which data should be fetched into this PriceScheduleItemOccurrence object</param>
		public PriceScheduleItemOccurrenceEntity(System.Int32 priceScheduleItemOccurrenceId) : this(priceScheduleItemOccurrenceId, null)
		{
		}

		/// <summary> CTor</summary>
		/// <param name="priceScheduleItemOccurrenceId">PK value for PriceScheduleItemOccurrence which data should be fetched into this PriceScheduleItemOccurrence object</param>
		/// <param name="validator">The custom validator object for this PriceScheduleItemOccurrenceEntity</param>
		public PriceScheduleItemOccurrenceEntity(System.Int32 priceScheduleItemOccurrenceId, IValidator validator)
		{
			InitClassEmpty(validator, null);
			this.PriceScheduleItemOccurrenceId = priceScheduleItemOccurrenceId;
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected PriceScheduleItemOccurrenceEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'PriceScheduleItem' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoPriceScheduleItem() { return CreateRelationInfoForNavigator("PriceScheduleItem"); }
		
		/// <inheritdoc/>
		protected override EntityStaticMetaDataBase GetEntityStaticMetaData() {	return _staticMetaData; }

		/// <summary>Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		/// <summary>Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this PriceScheduleItemOccurrenceEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary>The relations object holding all relations of this entity with other entity classes.</summary>
		public static PriceScheduleItemOccurrenceRelations Relations { get { return _relationsFactory; } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'PriceScheduleItem' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathPriceScheduleItemEntity { get { return _staticMetaData.GetPrefetchPathElement("PriceScheduleItem", CommonEntityBase.CreateEntityCollection<PriceScheduleItemEntity>()); } }

		/// <summary>The PriceScheduleItemOccurrenceId property of the Entity PriceScheduleItemOccurrence<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceScheduleItemOccurrence"."PriceScheduleItemOccurrenceId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 PriceScheduleItemOccurrenceId
		{
			get { return (System.Int32)GetValue((int)PriceScheduleItemOccurrenceFieldIndex.PriceScheduleItemOccurrenceId, true); }
			set { SetValue((int)PriceScheduleItemOccurrenceFieldIndex.PriceScheduleItemOccurrenceId, value); }		}

		/// <summary>The ParentCompanyId property of the Entity PriceScheduleItemOccurrence<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceScheduleItemOccurrence"."ParentCompanyId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ParentCompanyId
		{
			get { return (Nullable<System.Int32>)GetValue((int)PriceScheduleItemOccurrenceFieldIndex.ParentCompanyId, false); }
			set	{ SetValue((int)PriceScheduleItemOccurrenceFieldIndex.ParentCompanyId, value); }
		}

		/// <summary>The PriceScheduleItemId property of the Entity PriceScheduleItemOccurrence<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceScheduleItemOccurrence"."PriceScheduleItemId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 PriceScheduleItemId
		{
			get { return (System.Int32)GetValue((int)PriceScheduleItemOccurrenceFieldIndex.PriceScheduleItemId, true); }
			set	{ SetValue((int)PriceScheduleItemOccurrenceFieldIndex.PriceScheduleItemId, value); }
		}

		/// <summary>The StartTime property of the Entity PriceScheduleItemOccurrence<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceScheduleItemOccurrence"."StartTime".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> StartTime
		{
			get { return (Nullable<System.DateTime>)GetValue((int)PriceScheduleItemOccurrenceFieldIndex.StartTime, false); }
			set	{ SetValue((int)PriceScheduleItemOccurrenceFieldIndex.StartTime, value); }
		}

		/// <summary>The EndTime property of the Entity PriceScheduleItemOccurrence<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceScheduleItemOccurrence"."EndTime".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> EndTime
		{
			get { return (Nullable<System.DateTime>)GetValue((int)PriceScheduleItemOccurrenceFieldIndex.EndTime, false); }
			set	{ SetValue((int)PriceScheduleItemOccurrenceFieldIndex.EndTime, value); }
		}

		/// <summary>The Recurring property of the Entity PriceScheduleItemOccurrence<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceScheduleItemOccurrence"."Recurring".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean Recurring
		{
			get { return (System.Boolean)GetValue((int)PriceScheduleItemOccurrenceFieldIndex.Recurring, true); }
			set	{ SetValue((int)PriceScheduleItemOccurrenceFieldIndex.Recurring, value); }
		}

		/// <summary>The RecurrenceType property of the Entity PriceScheduleItemOccurrence<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceScheduleItemOccurrence"."RecurrenceType".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 RecurrenceType
		{
			get { return (System.Int32)GetValue((int)PriceScheduleItemOccurrenceFieldIndex.RecurrenceType, true); }
			set	{ SetValue((int)PriceScheduleItemOccurrenceFieldIndex.RecurrenceType, value); }
		}

		/// <summary>The RecurrenceRange property of the Entity PriceScheduleItemOccurrence<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceScheduleItemOccurrence"."RecurrenceRange".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 RecurrenceRange
		{
			get { return (System.Int32)GetValue((int)PriceScheduleItemOccurrenceFieldIndex.RecurrenceRange, true); }
			set	{ SetValue((int)PriceScheduleItemOccurrenceFieldIndex.RecurrenceRange, value); }
		}

		/// <summary>The RecurrenceStart property of the Entity PriceScheduleItemOccurrence<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceScheduleItemOccurrence"."RecurrenceStart".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> RecurrenceStart
		{
			get { return (Nullable<System.DateTime>)GetValue((int)PriceScheduleItemOccurrenceFieldIndex.RecurrenceStart, false); }
			set	{ SetValue((int)PriceScheduleItemOccurrenceFieldIndex.RecurrenceStart, value); }
		}

		/// <summary>The RecurrenceEnd property of the Entity PriceScheduleItemOccurrence<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceScheduleItemOccurrence"."RecurrenceEnd".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> RecurrenceEnd
		{
			get { return (Nullable<System.DateTime>)GetValue((int)PriceScheduleItemOccurrenceFieldIndex.RecurrenceEnd, false); }
			set	{ SetValue((int)PriceScheduleItemOccurrenceFieldIndex.RecurrenceEnd, value); }
		}

		/// <summary>The RecurrenceOccurrenceCount property of the Entity PriceScheduleItemOccurrence<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceScheduleItemOccurrence"."RecurrenceOccurrenceCount".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 RecurrenceOccurrenceCount
		{
			get { return (System.Int32)GetValue((int)PriceScheduleItemOccurrenceFieldIndex.RecurrenceOccurrenceCount, true); }
			set	{ SetValue((int)PriceScheduleItemOccurrenceFieldIndex.RecurrenceOccurrenceCount, value); }
		}

		/// <summary>The RecurrencePeriodicity property of the Entity PriceScheduleItemOccurrence<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceScheduleItemOccurrence"."RecurrencePeriodicity".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 RecurrencePeriodicity
		{
			get { return (System.Int32)GetValue((int)PriceScheduleItemOccurrenceFieldIndex.RecurrencePeriodicity, true); }
			set	{ SetValue((int)PriceScheduleItemOccurrenceFieldIndex.RecurrencePeriodicity, value); }
		}

		/// <summary>The RecurrenceDayNumber property of the Entity PriceScheduleItemOccurrence<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceScheduleItemOccurrence"."RecurrenceDayNumber".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 RecurrenceDayNumber
		{
			get { return (System.Int32)GetValue((int)PriceScheduleItemOccurrenceFieldIndex.RecurrenceDayNumber, true); }
			set	{ SetValue((int)PriceScheduleItemOccurrenceFieldIndex.RecurrenceDayNumber, value); }
		}

		/// <summary>The RecurrenceWeekDays property of the Entity PriceScheduleItemOccurrence<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceScheduleItemOccurrence"."RecurrenceWeekDays".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 RecurrenceWeekDays
		{
			get { return (System.Int32)GetValue((int)PriceScheduleItemOccurrenceFieldIndex.RecurrenceWeekDays, true); }
			set	{ SetValue((int)PriceScheduleItemOccurrenceFieldIndex.RecurrenceWeekDays, value); }
		}

		/// <summary>The RecurrenceWeekOfMonth property of the Entity PriceScheduleItemOccurrence<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceScheduleItemOccurrence"."RecurrenceWeekOfMonth".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 RecurrenceWeekOfMonth
		{
			get { return (System.Int32)GetValue((int)PriceScheduleItemOccurrenceFieldIndex.RecurrenceWeekOfMonth, true); }
			set	{ SetValue((int)PriceScheduleItemOccurrenceFieldIndex.RecurrenceWeekOfMonth, value); }
		}

		/// <summary>The RecurrenceMonth property of the Entity PriceScheduleItemOccurrence<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceScheduleItemOccurrence"."RecurrenceMonth".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 RecurrenceMonth
		{
			get { return (System.Int32)GetValue((int)PriceScheduleItemOccurrenceFieldIndex.RecurrenceMonth, true); }
			set	{ SetValue((int)PriceScheduleItemOccurrenceFieldIndex.RecurrenceMonth, value); }
		}

		/// <summary>The BackgroundColor property of the Entity PriceScheduleItemOccurrence<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceScheduleItemOccurrence"."BackgroundColor".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 BackgroundColor
		{
			get { return (System.Int32)GetValue((int)PriceScheduleItemOccurrenceFieldIndex.BackgroundColor, true); }
			set	{ SetValue((int)PriceScheduleItemOccurrenceFieldIndex.BackgroundColor, value); }
		}

		/// <summary>The TextColor property of the Entity PriceScheduleItemOccurrence<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceScheduleItemOccurrence"."TextColor".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 TextColor
		{
			get { return (System.Int32)GetValue((int)PriceScheduleItemOccurrenceFieldIndex.TextColor, true); }
			set	{ SetValue((int)PriceScheduleItemOccurrenceFieldIndex.TextColor, value); }
		}

		/// <summary>The StartTimeUTC property of the Entity PriceScheduleItemOccurrence<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceScheduleItemOccurrence"."StartTimeUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> StartTimeUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)PriceScheduleItemOccurrenceFieldIndex.StartTimeUTC, false); }
			set	{ SetValue((int)PriceScheduleItemOccurrenceFieldIndex.StartTimeUTC, value); }
		}

		/// <summary>The EndTimeUTC property of the Entity PriceScheduleItemOccurrence<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceScheduleItemOccurrence"."EndTimeUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> EndTimeUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)PriceScheduleItemOccurrenceFieldIndex.EndTimeUTC, false); }
			set	{ SetValue((int)PriceScheduleItemOccurrenceFieldIndex.EndTimeUTC, value); }
		}

		/// <summary>The RecurrenceStartUTC property of the Entity PriceScheduleItemOccurrence<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceScheduleItemOccurrence"."RecurrenceStartUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> RecurrenceStartUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)PriceScheduleItemOccurrenceFieldIndex.RecurrenceStartUTC, false); }
			set	{ SetValue((int)PriceScheduleItemOccurrenceFieldIndex.RecurrenceStartUTC, value); }
		}

		/// <summary>The RecurrenceEndUTC property of the Entity PriceScheduleItemOccurrence<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceScheduleItemOccurrence"."RecurrenceEndUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> RecurrenceEndUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)PriceScheduleItemOccurrenceFieldIndex.RecurrenceEndUTC, false); }
			set	{ SetValue((int)PriceScheduleItemOccurrenceFieldIndex.RecurrenceEndUTC, value); }
		}

		/// <summary>The CreatedUTC property of the Entity PriceScheduleItemOccurrence<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceScheduleItemOccurrence"."CreatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)PriceScheduleItemOccurrenceFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)PriceScheduleItemOccurrenceFieldIndex.CreatedUTC, value); }
		}

		/// <summary>The CreatedBy property of the Entity PriceScheduleItemOccurrence<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceScheduleItemOccurrence"."CreatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CreatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)PriceScheduleItemOccurrenceFieldIndex.CreatedBy, false); }
			set	{ SetValue((int)PriceScheduleItemOccurrenceFieldIndex.CreatedBy, value); }
		}

		/// <summary>The UpdatedUTC property of the Entity PriceScheduleItemOccurrence<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceScheduleItemOccurrence"."UpdatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)PriceScheduleItemOccurrenceFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)PriceScheduleItemOccurrenceFieldIndex.UpdatedUTC, value); }
		}

		/// <summary>The UpdatedBy property of the Entity PriceScheduleItemOccurrence<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceScheduleItemOccurrence"."UpdatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UpdatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)PriceScheduleItemOccurrenceFieldIndex.UpdatedBy, false); }
			set	{ SetValue((int)PriceScheduleItemOccurrenceFieldIndex.UpdatedBy, value); }
		}

		/// <summary>Gets / sets related entity of type 'PriceScheduleItemEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual PriceScheduleItemEntity PriceScheduleItem
		{
			get { return _priceScheduleItem; }
			set { SetSingleRelatedEntityNavigator(value, "PriceScheduleItem"); }
		}


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen
{
	public enum PriceScheduleItemOccurrenceFieldIndex
	{
		///<summary>PriceScheduleItemOccurrenceId. </summary>
		PriceScheduleItemOccurrenceId,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>PriceScheduleItemId. </summary>
		PriceScheduleItemId,
		///<summary>StartTime. </summary>
		StartTime,
		///<summary>EndTime. </summary>
		EndTime,
		///<summary>Recurring. </summary>
		Recurring,
		///<summary>RecurrenceType. </summary>
		RecurrenceType,
		///<summary>RecurrenceRange. </summary>
		RecurrenceRange,
		///<summary>RecurrenceStart. </summary>
		RecurrenceStart,
		///<summary>RecurrenceEnd. </summary>
		RecurrenceEnd,
		///<summary>RecurrenceOccurrenceCount. </summary>
		RecurrenceOccurrenceCount,
		///<summary>RecurrencePeriodicity. </summary>
		RecurrencePeriodicity,
		///<summary>RecurrenceDayNumber. </summary>
		RecurrenceDayNumber,
		///<summary>RecurrenceWeekDays. </summary>
		RecurrenceWeekDays,
		///<summary>RecurrenceWeekOfMonth. </summary>
		RecurrenceWeekOfMonth,
		///<summary>RecurrenceMonth. </summary>
		RecurrenceMonth,
		///<summary>BackgroundColor. </summary>
		BackgroundColor,
		///<summary>TextColor. </summary>
		TextColor,
		///<summary>StartTimeUTC. </summary>
		StartTimeUTC,
		///<summary>EndTimeUTC. </summary>
		EndTimeUTC,
		///<summary>RecurrenceStartUTC. </summary>
		RecurrenceStartUTC,
		///<summary>RecurrenceEndUTC. </summary>
		RecurrenceEndUTC,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		/// <summary></summary>
		AmountOfFields
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: PriceScheduleItemOccurrence. </summary>
	public partial class PriceScheduleItemOccurrenceRelations: RelationFactory
	{
		#region Custom Relation code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomRelationCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		


		/// <summary>Gets all relations and delete rules of the PriceScheduleItemOccurrenceEntity as a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects.</summary>
		/// <returns>a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects</returns>
		public virtual Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> GetRelationsAndDeleteRules()
		{
			Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> toReturn = new Dictionary<IEntityRelation, ReferentialConstraintDeleteRule>();
			return toReturn;
		}		
	
	

		/// <summary>Returns a new IEntityRelation object, between PriceScheduleItemOccurrenceEntity and PriceScheduleItemEntity over the m:1 relation they have, using the relation between the fields: PriceScheduleItemOccurrence.PriceScheduleItemId - PriceScheduleItem.PriceScheduleItemId</summary>
		public virtual IEntityRelation PriceScheduleItemEntityUsingPriceScheduleItemId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "PriceScheduleItem", false, new[] { PriceScheduleItemFields.PriceScheduleItemId, PriceScheduleItemOccurrenceFields.PriceScheduleItemId }); }
		}

	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticPriceScheduleItemOccurrenceRelations
	{
		internal static readonly IEntityRelation PriceScheduleItemEntityUsingPriceScheduleItemIdStatic = new PriceScheduleItemOccurrenceRelations().PriceScheduleItemEntityUsingPriceScheduleItemId;

		/// <summary>CTor</summary>
		static StaticPriceScheduleItemOccurrenceRelations() { }
	}
}

