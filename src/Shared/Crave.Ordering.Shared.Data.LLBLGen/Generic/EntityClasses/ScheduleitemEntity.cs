﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 5.8
// Code is generated on: 
// Code is generated using templates: Crave.TemplateBindings
// Templates vendor: Crave Interactive
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.FactoryClasses;
using Crave.Ordering.Shared.Data.LLBLGen.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

// ReSharper disable All
namespace Crave.Ordering.Shared.Data.LLBLGen.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'Scheduleitem'.<br/><br/></summary>
	[Serializable]
	public partial class ScheduleitemEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END
	
	{
		private ScheduleEntity _schedule;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		private static ScheduleitemEntityStaticMetaData _staticMetaData = new ScheduleitemEntityStaticMetaData();
		private static ScheduleitemRelations _relationsFactory = new ScheduleitemRelations();		

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Schedule</summary>
			public static readonly string Schedule = "Schedule";
		}

		/// <summary>Static meta-data storage for navigator related information</summary>
		protected class ScheduleitemEntityStaticMetaData : EntityStaticMetaDataBase
		{
			public ScheduleitemEntityStaticMetaData()
			{
				SetEntityCoreInfo("ScheduleitemEntity", InheritanceHierarchyType.None, false, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ScheduleitemEntity, typeof(ScheduleitemEntity), typeof(ScheduleitemEntityFactory), false);
				AddNavigatorMetaData<ScheduleitemEntity, ScheduleEntity>("Schedule", "ScheduleitemCollection", (a, b) => a._schedule = b, a => a._schedule, (a, b) => a.Schedule = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticScheduleitemRelations.ScheduleEntityUsingScheduleIdStatic, ()=>new ScheduleitemRelations().ScheduleEntityUsingScheduleId, null, new int[] { (int)ScheduleitemFieldIndex.ScheduleId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ScheduleEntity);
			}
		}

		/// <summary>Static ctor</summary>
		static ScheduleitemEntity()
		{
		}

		/// <summary> CTor</summary>
		public ScheduleitemEntity()
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public ScheduleitemEntity(IEntityFields2 fields)
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this ScheduleitemEntity</param>
		public ScheduleitemEntity(IValidator validator)
		{
			InitClassEmpty(validator, null);
		}

		/// <summary> CTor</summary>
		/// <param name="scheduleitemId">PK value for Scheduleitem which data should be fetched into this Scheduleitem object</param>
		public ScheduleitemEntity(System.Int32 scheduleitemId) : this(scheduleitemId, null)
		{
		}

		/// <summary> CTor</summary>
		/// <param name="scheduleitemId">PK value for Scheduleitem which data should be fetched into this Scheduleitem object</param>
		/// <param name="validator">The custom validator object for this ScheduleitemEntity</param>
		public ScheduleitemEntity(System.Int32 scheduleitemId, IValidator validator)
		{
			InitClassEmpty(validator, null);
			this.ScheduleitemId = scheduleitemId;
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ScheduleitemEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Schedule' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoSchedule() { return CreateRelationInfoForNavigator("Schedule"); }
		
		/// <inheritdoc/>
		protected override EntityStaticMetaDataBase GetEntityStaticMetaData() {	return _staticMetaData; }

		/// <summary>Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		/// <summary>Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this ScheduleitemEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary>The relations object holding all relations of this entity with other entity classes.</summary>
		public static ScheduleitemRelations Relations { get { return _relationsFactory; } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Schedule' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathScheduleEntity { get { return _staticMetaData.GetPrefetchPathElement("Schedule", CommonEntityBase.CreateEntityCollection<ScheduleEntity>()); } }

		/// <summary>The ScheduleitemId property of the Entity Scheduleitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Scheduleitem"."ScheduleitemId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 ScheduleitemId
		{
			get { return (System.Int32)GetValue((int)ScheduleitemFieldIndex.ScheduleitemId, true); }
			set { SetValue((int)ScheduleitemFieldIndex.ScheduleitemId, value); }		}

		/// <summary>The ParentCompanyId property of the Entity Scheduleitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Scheduleitem"."ParentCompanyId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ParentCompanyId
		{
			get { return (System.Int32)GetValue((int)ScheduleitemFieldIndex.ParentCompanyId, true); }
			set	{ SetValue((int)ScheduleitemFieldIndex.ParentCompanyId, value); }
		}

		/// <summary>The ScheduleId property of the Entity Scheduleitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Scheduleitem"."ScheduleId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ScheduleId
		{
			get { return (System.Int32)GetValue((int)ScheduleitemFieldIndex.ScheduleId, true); }
			set	{ SetValue((int)ScheduleitemFieldIndex.ScheduleId, value); }
		}

		/// <summary>The SortOrder property of the Entity Scheduleitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Scheduleitem"."SortOrder".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 SortOrder
		{
			get { return (System.Int32)GetValue((int)ScheduleitemFieldIndex.SortOrder, true); }
			set	{ SetValue((int)ScheduleitemFieldIndex.SortOrder, value); }
		}

		/// <summary>The DayOfWeek property of the Entity Scheduleitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Scheduleitem"."DayOfWeek".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DayOfWeek> DayOfWeek
		{
			get { return (Nullable<System.DayOfWeek>)GetValue((int)ScheduleitemFieldIndex.DayOfWeek, false); }
			set	{ SetValue((int)ScheduleitemFieldIndex.DayOfWeek, value); }
		}

		/// <summary>The TimeStart property of the Entity Scheduleitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Scheduleitem"."TimeStart".<br/>Table field type characteristics (type, precision, scale, length): Char, 0, 0, 4.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String TimeStart
		{
			get { return (System.String)GetValue((int)ScheduleitemFieldIndex.TimeStart, true); }
			set	{ SetValue((int)ScheduleitemFieldIndex.TimeStart, value); }
		}

		/// <summary>The TimeEnd property of the Entity Scheduleitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Scheduleitem"."TimeEnd".<br/>Table field type characteristics (type, precision, scale, length): Char, 0, 0, 4.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String TimeEnd
		{
			get { return (System.String)GetValue((int)ScheduleitemFieldIndex.TimeEnd, true); }
			set	{ SetValue((int)ScheduleitemFieldIndex.TimeEnd, value); }
		}

		/// <summary>The CreatedUTC property of the Entity Scheduleitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Scheduleitem"."CreatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ScheduleitemFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)ScheduleitemFieldIndex.CreatedUTC, value); }
		}

		/// <summary>The CreatedBy property of the Entity Scheduleitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Scheduleitem"."CreatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CreatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)ScheduleitemFieldIndex.CreatedBy, false); }
			set	{ SetValue((int)ScheduleitemFieldIndex.CreatedBy, value); }
		}

		/// <summary>The UpdatedUTC property of the Entity Scheduleitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Scheduleitem"."UpdatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ScheduleitemFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)ScheduleitemFieldIndex.UpdatedUTC, value); }
		}

		/// <summary>The UpdatedBy property of the Entity Scheduleitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Scheduleitem"."UpdatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UpdatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)ScheduleitemFieldIndex.UpdatedBy, false); }
			set	{ SetValue((int)ScheduleitemFieldIndex.UpdatedBy, value); }
		}

		/// <summary>Gets / sets related entity of type 'ScheduleEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual ScheduleEntity Schedule
		{
			get { return _schedule; }
			set { SetSingleRelatedEntityNavigator(value, "Schedule"); }
		}


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen
{
	public enum ScheduleitemFieldIndex
	{
		///<summary>ScheduleitemId. </summary>
		ScheduleitemId,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>ScheduleId. </summary>
		ScheduleId,
		///<summary>SortOrder. </summary>
		SortOrder,
		///<summary>DayOfWeek. </summary>
		DayOfWeek,
		///<summary>TimeStart. </summary>
		TimeStart,
		///<summary>TimeEnd. </summary>
		TimeEnd,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		/// <summary></summary>
		AmountOfFields
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Scheduleitem. </summary>
	public partial class ScheduleitemRelations: RelationFactory
	{
		#region Custom Relation code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomRelationCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		


		/// <summary>Gets all relations and delete rules of the ScheduleitemEntity as a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects.</summary>
		/// <returns>a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects</returns>
		public virtual Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> GetRelationsAndDeleteRules()
		{
			Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> toReturn = new Dictionary<IEntityRelation, ReferentialConstraintDeleteRule>();
			return toReturn;
		}		
	
	

		/// <summary>Returns a new IEntityRelation object, between ScheduleitemEntity and ScheduleEntity over the m:1 relation they have, using the relation between the fields: Scheduleitem.ScheduleId - Schedule.ScheduleId</summary>
		public virtual IEntityRelation ScheduleEntityUsingScheduleId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Schedule", false, new[] { ScheduleFields.ScheduleId, ScheduleitemFields.ScheduleId }); }
		}

	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticScheduleitemRelations
	{
		internal static readonly IEntityRelation ScheduleEntityUsingScheduleIdStatic = new ScheduleitemRelations().ScheduleEntityUsingScheduleId;

		/// <summary>CTor</summary>
		static StaticScheduleitemRelations() { }
	}
}

