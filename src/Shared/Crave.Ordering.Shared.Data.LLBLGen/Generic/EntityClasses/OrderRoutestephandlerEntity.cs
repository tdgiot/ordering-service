﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 5.8
// Code is generated on: 
// Code is generated using templates: Crave.TemplateBindings
// Templates vendor: Crave Interactive
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.FactoryClasses;
using Crave.Ordering.Shared.Data.LLBLGen.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

// ReSharper disable All
namespace Crave.Ordering.Shared.Data.LLBLGen.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'OrderRoutestephandler'.<br/><br/></summary>
	[Serializable]
	public partial class OrderRoutestephandlerEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END
	
	{
		private ExternalSystemEntity _externalSystem;
		private OrderEntity _order;
		private SupportpoolEntity _supportpool;
		private TerminalEntity _terminal;
		private TerminalEntity _terminalEntity1;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		private static OrderRoutestephandlerEntityStaticMetaData _staticMetaData = new OrderRoutestephandlerEntityStaticMetaData();
		private static OrderRoutestephandlerRelations _relationsFactory = new OrderRoutestephandlerRelations();		

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name ExternalSystem</summary>
			public static readonly string ExternalSystem = "ExternalSystem";
			/// <summary>Member name Order</summary>
			public static readonly string Order = "Order";
			/// <summary>Member name Supportpool</summary>
			public static readonly string Supportpool = "Supportpool";
			/// <summary>Member name Terminal</summary>
			public static readonly string Terminal = "Terminal";
			/// <summary>Member name TerminalEntity1</summary>
			public static readonly string TerminalEntity1 = "TerminalEntity1";
		}

		/// <summary>Static meta-data storage for navigator related information</summary>
		protected class OrderRoutestephandlerEntityStaticMetaData : EntityStaticMetaDataBase
		{
			public OrderRoutestephandlerEntityStaticMetaData()
			{
				SetEntityCoreInfo("OrderRoutestephandlerEntity", InheritanceHierarchyType.None, false, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.OrderRoutestephandlerEntity, typeof(OrderRoutestephandlerEntity), typeof(OrderRoutestephandlerEntityFactory), false);
				AddNavigatorMetaData<OrderRoutestephandlerEntity, ExternalSystemEntity>("ExternalSystem", "OrderRoutestephandlerCollection", (a, b) => a._externalSystem = b, a => a._externalSystem, (a, b) => a.ExternalSystem = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticOrderRoutestephandlerRelations.ExternalSystemEntityUsingExternalSystemIdStatic, ()=>new OrderRoutestephandlerRelations().ExternalSystemEntityUsingExternalSystemId, null, new int[] { (int)OrderRoutestephandlerFieldIndex.ExternalSystemId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ExternalSystemEntity);
				AddNavigatorMetaData<OrderRoutestephandlerEntity, OrderEntity>("Order", "OrderRoutestephandlerCollection", (a, b) => a._order = b, a => a._order, (a, b) => a.Order = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticOrderRoutestephandlerRelations.OrderEntityUsingOrderIdStatic, ()=>new OrderRoutestephandlerRelations().OrderEntityUsingOrderId, null, new int[] { (int)OrderRoutestephandlerFieldIndex.OrderId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.OrderEntity);
				AddNavigatorMetaData<OrderRoutestephandlerEntity, SupportpoolEntity>("Supportpool", "OrderRoutestephandlerCollection", (a, b) => a._supportpool = b, a => a._supportpool, (a, b) => a.Supportpool = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticOrderRoutestephandlerRelations.SupportpoolEntityUsingSupportpoolIdStatic, ()=>new OrderRoutestephandlerRelations().SupportpoolEntityUsingSupportpoolId, null, new int[] { (int)OrderRoutestephandlerFieldIndex.SupportpoolId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.SupportpoolEntity);
				AddNavigatorMetaData<OrderRoutestephandlerEntity, TerminalEntity>("Terminal", "OrderRoutestephandlerCollection", (a, b) => a._terminal = b, a => a._terminal, (a, b) => a.Terminal = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticOrderRoutestephandlerRelations.TerminalEntityUsingTerminalIdStatic, ()=>new OrderRoutestephandlerRelations().TerminalEntityUsingTerminalId, null, new int[] { (int)OrderRoutestephandlerFieldIndex.TerminalId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.TerminalEntity);
				AddNavigatorMetaData<OrderRoutestephandlerEntity, TerminalEntity>("TerminalEntity1", "OrderRoutestephandlerCollection1", (a, b) => a._terminalEntity1 = b, a => a._terminalEntity1, (a, b) => a.TerminalEntity1 = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticOrderRoutestephandlerRelations.TerminalEntityUsingForwardedFromTerminalIdStatic, ()=>new OrderRoutestephandlerRelations().TerminalEntityUsingForwardedFromTerminalId, null, new int[] { (int)OrderRoutestephandlerFieldIndex.ForwardedFromTerminalId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.TerminalEntity);
			}
		}

		/// <summary>Static ctor</summary>
		static OrderRoutestephandlerEntity()
		{
		}

		/// <summary> CTor</summary>
		public OrderRoutestephandlerEntity()
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public OrderRoutestephandlerEntity(IEntityFields2 fields)
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this OrderRoutestephandlerEntity</param>
		public OrderRoutestephandlerEntity(IValidator validator)
		{
			InitClassEmpty(validator, null);
		}

		/// <summary> CTor</summary>
		/// <param name="orderRoutestephandlerId">PK value for OrderRoutestephandler which data should be fetched into this OrderRoutestephandler object</param>
		public OrderRoutestephandlerEntity(System.Int32 orderRoutestephandlerId) : this(orderRoutestephandlerId, null)
		{
		}

		/// <summary> CTor</summary>
		/// <param name="orderRoutestephandlerId">PK value for OrderRoutestephandler which data should be fetched into this OrderRoutestephandler object</param>
		/// <param name="validator">The custom validator object for this OrderRoutestephandlerEntity</param>
		public OrderRoutestephandlerEntity(System.Int32 orderRoutestephandlerId, IValidator validator)
		{
			InitClassEmpty(validator, null);
			this.OrderRoutestephandlerId = orderRoutestephandlerId;
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected OrderRoutestephandlerEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'ExternalSystem' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoExternalSystem() { return CreateRelationInfoForNavigator("ExternalSystem"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Order' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoOrder() { return CreateRelationInfoForNavigator("Order"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Supportpool' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoSupportpool() { return CreateRelationInfoForNavigator("Supportpool"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Terminal' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoTerminal() { return CreateRelationInfoForNavigator("Terminal"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Terminal' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoTerminalEntity1() { return CreateRelationInfoForNavigator("TerminalEntity1"); }
		
		/// <inheritdoc/>
		protected override EntityStaticMetaDataBase GetEntityStaticMetaData() {	return _staticMetaData; }

		/// <summary>Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		/// <summary>Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this OrderRoutestephandlerEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary>The relations object holding all relations of this entity with other entity classes.</summary>
		public static OrderRoutestephandlerRelations Relations { get { return _relationsFactory; } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'ExternalSystem' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathExternalSystemEntity { get { return _staticMetaData.GetPrefetchPathElement("ExternalSystem", CommonEntityBase.CreateEntityCollection<ExternalSystemEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Order' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathOrderEntity { get { return _staticMetaData.GetPrefetchPathElement("Order", CommonEntityBase.CreateEntityCollection<OrderEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Supportpool' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathSupportpoolEntity { get { return _staticMetaData.GetPrefetchPathElement("Supportpool", CommonEntityBase.CreateEntityCollection<SupportpoolEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Terminal' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathTerminalEntity { get { return _staticMetaData.GetPrefetchPathElement("Terminal", CommonEntityBase.CreateEntityCollection<TerminalEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Terminal' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathTerminalEntity1Entity { get { return _staticMetaData.GetPrefetchPathElement("TerminalEntity1", CommonEntityBase.CreateEntityCollection<TerminalEntity>()); } }

		/// <summary>The OrderRoutestephandlerId property of the Entity OrderRoutestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandler"."OrderRoutestephandlerId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 OrderRoutestephandlerId
		{
			get { return (System.Int32)GetValue((int)OrderRoutestephandlerFieldIndex.OrderRoutestephandlerId, true); }
			set { SetValue((int)OrderRoutestephandlerFieldIndex.OrderRoutestephandlerId, value); }		}

		/// <summary>The ParentCompanyId property of the Entity OrderRoutestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandler"."ParentCompanyId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ParentCompanyId
		{
			get { return (System.Int32)GetValue((int)OrderRoutestephandlerFieldIndex.ParentCompanyId, true); }
			set	{ SetValue((int)OrderRoutestephandlerFieldIndex.ParentCompanyId, value); }
		}

		/// <summary>The Guid property of the Entity OrderRoutestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandler"."Guid".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 128.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Guid
		{
			get { return (System.String)GetValue((int)OrderRoutestephandlerFieldIndex.Guid, true); }
			set	{ SetValue((int)OrderRoutestephandlerFieldIndex.Guid, value); }
		}

		/// <summary>The OrderId property of the Entity OrderRoutestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandler"."OrderId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 OrderId
		{
			get { return (System.Int32)GetValue((int)OrderRoutestephandlerFieldIndex.OrderId, true); }
			set	{ SetValue((int)OrderRoutestephandlerFieldIndex.OrderId, value); }
		}

		/// <summary>The TerminalId property of the Entity OrderRoutestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandler"."TerminalId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> TerminalId
		{
			get { return (Nullable<System.Int32>)GetValue((int)OrderRoutestephandlerFieldIndex.TerminalId, false); }
			set	{ SetValue((int)OrderRoutestephandlerFieldIndex.TerminalId, value); }
		}

		/// <summary>The ForwardedFromTerminalId property of the Entity OrderRoutestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandler"."ForwardedFromTerminalId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ForwardedFromTerminalId
		{
			get { return (Nullable<System.Int32>)GetValue((int)OrderRoutestephandlerFieldIndex.ForwardedFromTerminalId, false); }
			set	{ SetValue((int)OrderRoutestephandlerFieldIndex.ForwardedFromTerminalId, value); }
		}

		/// <summary>The Number property of the Entity OrderRoutestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandler"."Number".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 Number
		{
			get { return (System.Int32)GetValue((int)OrderRoutestephandlerFieldIndex.Number, true); }
			set	{ SetValue((int)OrderRoutestephandlerFieldIndex.Number, value); }
		}

		/// <summary>The ContinueOnFailure property of the Entity OrderRoutestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandler"."ContinueOnFailure".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean ContinueOnFailure
		{
			get { return (System.Boolean)GetValue((int)OrderRoutestephandlerFieldIndex.ContinueOnFailure, true); }
			set	{ SetValue((int)OrderRoutestephandlerFieldIndex.ContinueOnFailure, value); }
		}

		/// <summary>The CompleteRouteOnComplete property of the Entity OrderRoutestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandler"."CompleteRouteOnComplete".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean CompleteRouteOnComplete
		{
			get { return (System.Boolean)GetValue((int)OrderRoutestephandlerFieldIndex.CompleteRouteOnComplete, true); }
			set	{ SetValue((int)OrderRoutestephandlerFieldIndex.CompleteRouteOnComplete, value); }
		}

		/// <summary>The HandlerType property of the Entity OrderRoutestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandler"."HandlerType".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Crave.Enums.RoutestephandlerType HandlerType
		{
			get { return (Crave.Enums.RoutestephandlerType)GetValue((int)OrderRoutestephandlerFieldIndex.HandlerType, true); }
			set	{ SetValue((int)OrderRoutestephandlerFieldIndex.HandlerType, value); }
		}

		/// <summary>The HandlerTypeText property of the Entity OrderRoutestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandler"."HandlerTypeText".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 100.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String HandlerTypeText
		{
			get { return (System.String)GetValue((int)OrderRoutestephandlerFieldIndex.HandlerTypeText, true); }
			set	{ SetValue((int)OrderRoutestephandlerFieldIndex.HandlerTypeText, value); }
		}

		/// <summary>The PrintReportType property of the Entity OrderRoutestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandler"."PrintReportType".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<Crave.Enums.PrintReportType> PrintReportType
		{
			get { return (Nullable<Crave.Enums.PrintReportType>)GetValue((int)OrderRoutestephandlerFieldIndex.PrintReportType, false); }
			set	{ SetValue((int)OrderRoutestephandlerFieldIndex.PrintReportType, value); }
		}

		/// <summary>The EscalationRouteId property of the Entity OrderRoutestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandler"."EscalationRouteId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> EscalationRouteId
		{
			get { return (Nullable<System.Int32>)GetValue((int)OrderRoutestephandlerFieldIndex.EscalationRouteId, false); }
			set	{ SetValue((int)OrderRoutestephandlerFieldIndex.EscalationRouteId, value); }
		}

		/// <summary>The Status property of the Entity OrderRoutestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandler"."Status".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Crave.Enums.OrderRoutestephandlerStatus Status
		{
			get { return (Crave.Enums.OrderRoutestephandlerStatus)GetValue((int)OrderRoutestephandlerFieldIndex.Status, true); }
			set	{ SetValue((int)OrderRoutestephandlerFieldIndex.Status, value); }
		}

		/// <summary>The StatusText property of the Entity OrderRoutestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandler"."StatusText".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 100.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String StatusText
		{
			get { return (System.String)GetValue((int)OrderRoutestephandlerFieldIndex.StatusText, true); }
			set	{ SetValue((int)OrderRoutestephandlerFieldIndex.StatusText, value); }
		}

		/// <summary>The Timeout property of the Entity OrderRoutestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandler"."Timeout".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 Timeout
		{
			get { return (System.Int32)GetValue((int)OrderRoutestephandlerFieldIndex.Timeout, true); }
			set	{ SetValue((int)OrderRoutestephandlerFieldIndex.Timeout, value); }
		}

		/// <summary>The RetrievalSupportNotificationTimeoutMinutes property of the Entity OrderRoutestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandler"."RetrievalSupportNotificationTimeoutMinutes".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 RetrievalSupportNotificationTimeoutMinutes
		{
			get { return (System.Int32)GetValue((int)OrderRoutestephandlerFieldIndex.RetrievalSupportNotificationTimeoutMinutes, true); }
			set	{ SetValue((int)OrderRoutestephandlerFieldIndex.RetrievalSupportNotificationTimeoutMinutes, value); }
		}

		/// <summary>The RetrievalSupportNotificationSent property of the Entity OrderRoutestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandler"."RetrievalSupportNotificationSent".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean RetrievalSupportNotificationSent
		{
			get { return (System.Boolean)GetValue((int)OrderRoutestephandlerFieldIndex.RetrievalSupportNotificationSent, true); }
			set	{ SetValue((int)OrderRoutestephandlerFieldIndex.RetrievalSupportNotificationSent, value); }
		}

		/// <summary>The BeingHandledSupportNotificationTimeoutMinutes property of the Entity OrderRoutestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandler"."BeingHandledSupportNotificationTimeoutMinutes".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 BeingHandledSupportNotificationTimeoutMinutes
		{
			get { return (System.Int32)GetValue((int)OrderRoutestephandlerFieldIndex.BeingHandledSupportNotificationTimeoutMinutes, true); }
			set	{ SetValue((int)OrderRoutestephandlerFieldIndex.BeingHandledSupportNotificationTimeoutMinutes, value); }
		}

		/// <summary>The BeingHandledSupportNotificationSent property of the Entity OrderRoutestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandler"."BeingHandledSupportNotificationSent".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean BeingHandledSupportNotificationSent
		{
			get { return (System.Boolean)GetValue((int)OrderRoutestephandlerFieldIndex.BeingHandledSupportNotificationSent, true); }
			set	{ SetValue((int)OrderRoutestephandlerFieldIndex.BeingHandledSupportNotificationSent, value); }
		}

		/// <summary>The ErrorCode property of the Entity OrderRoutestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandler"."ErrorCode".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Crave.Enums.OrderErrorCode ErrorCode
		{
			get { return (Crave.Enums.OrderErrorCode)GetValue((int)OrderRoutestephandlerFieldIndex.ErrorCode, true); }
			set	{ SetValue((int)OrderRoutestephandlerFieldIndex.ErrorCode, value); }
		}

		/// <summary>The ErrorText property of the Entity OrderRoutestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandler"."ErrorText".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ErrorText
		{
			get { return (System.String)GetValue((int)OrderRoutestephandlerFieldIndex.ErrorText, true); }
			set	{ SetValue((int)OrderRoutestephandlerFieldIndex.ErrorText, value); }
		}

		/// <summary>The EscalationStep property of the Entity OrderRoutestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandler"."EscalationStep".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean EscalationStep
		{
			get { return (System.Boolean)GetValue((int)OrderRoutestephandlerFieldIndex.EscalationStep, true); }
			set	{ SetValue((int)OrderRoutestephandlerFieldIndex.EscalationStep, value); }
		}

		/// <summary>The FieldValue1 property of the Entity OrderRoutestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandler"."FieldValue1".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue1
		{
			get { return (System.String)GetValue((int)OrderRoutestephandlerFieldIndex.FieldValue1, true); }
			set	{ SetValue((int)OrderRoutestephandlerFieldIndex.FieldValue1, value); }
		}

		/// <summary>The FieldValue2 property of the Entity OrderRoutestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandler"."FieldValue2".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue2
		{
			get { return (System.String)GetValue((int)OrderRoutestephandlerFieldIndex.FieldValue2, true); }
			set	{ SetValue((int)OrderRoutestephandlerFieldIndex.FieldValue2, value); }
		}

		/// <summary>The FieldValue3 property of the Entity OrderRoutestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandler"."FieldValue3".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue3
		{
			get { return (System.String)GetValue((int)OrderRoutestephandlerFieldIndex.FieldValue3, true); }
			set	{ SetValue((int)OrderRoutestephandlerFieldIndex.FieldValue3, value); }
		}

		/// <summary>The FieldValue4 property of the Entity OrderRoutestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandler"."FieldValue4".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue4
		{
			get { return (System.String)GetValue((int)OrderRoutestephandlerFieldIndex.FieldValue4, true); }
			set	{ SetValue((int)OrderRoutestephandlerFieldIndex.FieldValue4, value); }
		}

		/// <summary>The FieldValue5 property of the Entity OrderRoutestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandler"."FieldValue5".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue5
		{
			get { return (System.String)GetValue((int)OrderRoutestephandlerFieldIndex.FieldValue5, true); }
			set	{ SetValue((int)OrderRoutestephandlerFieldIndex.FieldValue5, value); }
		}

		/// <summary>The FieldValue6 property of the Entity OrderRoutestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandler"."FieldValue6".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue6
		{
			get { return (System.String)GetValue((int)OrderRoutestephandlerFieldIndex.FieldValue6, true); }
			set	{ SetValue((int)OrderRoutestephandlerFieldIndex.FieldValue6, value); }
		}

		/// <summary>The FieldValue7 property of the Entity OrderRoutestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandler"."FieldValue7".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue7
		{
			get { return (System.String)GetValue((int)OrderRoutestephandlerFieldIndex.FieldValue7, true); }
			set	{ SetValue((int)OrderRoutestephandlerFieldIndex.FieldValue7, value); }
		}

		/// <summary>The FieldValue8 property of the Entity OrderRoutestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandler"."FieldValue8".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue8
		{
			get { return (System.String)GetValue((int)OrderRoutestephandlerFieldIndex.FieldValue8, true); }
			set	{ SetValue((int)OrderRoutestephandlerFieldIndex.FieldValue8, value); }
		}

		/// <summary>The FieldValue9 property of the Entity OrderRoutestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandler"."FieldValue9".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue9
		{
			get { return (System.String)GetValue((int)OrderRoutestephandlerFieldIndex.FieldValue9, true); }
			set	{ SetValue((int)OrderRoutestephandlerFieldIndex.FieldValue9, value); }
		}

		/// <summary>The FieldValue10 property of the Entity OrderRoutestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandler"."FieldValue10".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue10
		{
			get { return (System.String)GetValue((int)OrderRoutestephandlerFieldIndex.FieldValue10, true); }
			set	{ SetValue((int)OrderRoutestephandlerFieldIndex.FieldValue10, value); }
		}

		/// <summary>The LogAlways property of the Entity OrderRoutestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandler"."LogAlways".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean LogAlways
		{
			get { return (System.Boolean)GetValue((int)OrderRoutestephandlerFieldIndex.LogAlways, true); }
			set	{ SetValue((int)OrderRoutestephandlerFieldIndex.LogAlways, value); }
		}

		/// <summary>The SupportpoolId property of the Entity OrderRoutestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandler"."SupportpoolId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> SupportpoolId
		{
			get { return (Nullable<System.Int32>)GetValue((int)OrderRoutestephandlerFieldIndex.SupportpoolId, false); }
			set	{ SetValue((int)OrderRoutestephandlerFieldIndex.SupportpoolId, value); }
		}

		/// <summary>The OriginatedFromRoutestepHandlerId property of the Entity OrderRoutestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandler"."OriginatedFromRoutestepHandlerId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> OriginatedFromRoutestepHandlerId
		{
			get { return (Nullable<System.Int32>)GetValue((int)OrderRoutestephandlerFieldIndex.OriginatedFromRoutestepHandlerId, false); }
			set	{ SetValue((int)OrderRoutestephandlerFieldIndex.OriginatedFromRoutestepHandlerId, value); }
		}

		/// <summary>The CreatedBy property of the Entity OrderRoutestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandler"."CreatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)OrderRoutestephandlerFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)OrderRoutestephandlerFieldIndex.CreatedBy, value); }
		}

		/// <summary>The UpdatedBy property of the Entity OrderRoutestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandler"."UpdatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)OrderRoutestephandlerFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)OrderRoutestephandlerFieldIndex.UpdatedBy, value); }
		}

		/// <summary>The CreatedUTC property of the Entity OrderRoutestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandler"."CreatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)OrderRoutestephandlerFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)OrderRoutestephandlerFieldIndex.CreatedUTC, value); }
		}

		/// <summary>The UpdatedUTC property of the Entity OrderRoutestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandler"."UpdatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)OrderRoutestephandlerFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)OrderRoutestephandlerFieldIndex.UpdatedUTC, value); }
		}

		/// <summary>The TimeoutExpiresUTC property of the Entity OrderRoutestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandler"."TimeoutExpiresUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> TimeoutExpiresUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)OrderRoutestephandlerFieldIndex.TimeoutExpiresUTC, false); }
			set	{ SetValue((int)OrderRoutestephandlerFieldIndex.TimeoutExpiresUTC, value); }
		}

		/// <summary>The RetrievalSupportNotificationTimeoutUTC property of the Entity OrderRoutestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandler"."RetrievalSupportNotificationTimeoutUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> RetrievalSupportNotificationTimeoutUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)OrderRoutestephandlerFieldIndex.RetrievalSupportNotificationTimeoutUTC, false); }
			set	{ SetValue((int)OrderRoutestephandlerFieldIndex.RetrievalSupportNotificationTimeoutUTC, value); }
		}

		/// <summary>The BeingHandledSupportNotificationTimeoutUTC property of the Entity OrderRoutestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandler"."BeingHandledSupportNotificationTimeoutUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> BeingHandledSupportNotificationTimeoutUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)OrderRoutestephandlerFieldIndex.BeingHandledSupportNotificationTimeoutUTC, false); }
			set	{ SetValue((int)OrderRoutestephandlerFieldIndex.BeingHandledSupportNotificationTimeoutUTC, value); }
		}

		/// <summary>The WaitingToBeRetrievedUTC property of the Entity OrderRoutestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandler"."WaitingToBeRetrievedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> WaitingToBeRetrievedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)OrderRoutestephandlerFieldIndex.WaitingToBeRetrievedUTC, false); }
			set	{ SetValue((int)OrderRoutestephandlerFieldIndex.WaitingToBeRetrievedUTC, value); }
		}

		/// <summary>The RetrievedByHandlerUTC property of the Entity OrderRoutestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandler"."RetrievedByHandlerUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> RetrievedByHandlerUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)OrderRoutestephandlerFieldIndex.RetrievedByHandlerUTC, false); }
			set	{ SetValue((int)OrderRoutestephandlerFieldIndex.RetrievedByHandlerUTC, value); }
		}

		/// <summary>The BeingHandledUTC property of the Entity OrderRoutestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandler"."BeingHandledUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> BeingHandledUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)OrderRoutestephandlerFieldIndex.BeingHandledUTC, false); }
			set	{ SetValue((int)OrderRoutestephandlerFieldIndex.BeingHandledUTC, value); }
		}

		/// <summary>The CompletedUTC property of the Entity OrderRoutestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandler"."CompletedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CompletedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)OrderRoutestephandlerFieldIndex.CompletedUTC, false); }
			set	{ SetValue((int)OrderRoutestephandlerFieldIndex.CompletedUTC, value); }
		}

		/// <summary>The ExternalSystemId property of the Entity OrderRoutestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandler"."ExternalSystemId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ExternalSystemId
		{
			get { return (Nullable<System.Int32>)GetValue((int)OrderRoutestephandlerFieldIndex.ExternalSystemId, false); }
			set	{ SetValue((int)OrderRoutestephandlerFieldIndex.ExternalSystemId, value); }
		}

		/// <summary>Gets / sets related entity of type 'ExternalSystemEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual ExternalSystemEntity ExternalSystem
		{
			get { return _externalSystem; }
			set { SetSingleRelatedEntityNavigator(value, "ExternalSystem"); }
		}

		/// <summary>Gets / sets related entity of type 'OrderEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual OrderEntity Order
		{
			get { return _order; }
			set { SetSingleRelatedEntityNavigator(value, "Order"); }
		}

		/// <summary>Gets / sets related entity of type 'SupportpoolEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual SupportpoolEntity Supportpool
		{
			get { return _supportpool; }
			set { SetSingleRelatedEntityNavigator(value, "Supportpool"); }
		}

		/// <summary>Gets / sets related entity of type 'TerminalEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual TerminalEntity Terminal
		{
			get { return _terminal; }
			set { SetSingleRelatedEntityNavigator(value, "Terminal"); }
		}

		/// <summary>Gets / sets related entity of type 'TerminalEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual TerminalEntity TerminalEntity1
		{
			get { return _terminalEntity1; }
			set { SetSingleRelatedEntityNavigator(value, "TerminalEntity1"); }
		}


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen
{
	public enum OrderRoutestephandlerFieldIndex
	{
		///<summary>OrderRoutestephandlerId. </summary>
		OrderRoutestephandlerId,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>Guid. </summary>
		Guid,
		///<summary>OrderId. </summary>
		OrderId,
		///<summary>TerminalId. </summary>
		TerminalId,
		///<summary>ForwardedFromTerminalId. </summary>
		ForwardedFromTerminalId,
		///<summary>Number. </summary>
		Number,
		///<summary>ContinueOnFailure. </summary>
		ContinueOnFailure,
		///<summary>CompleteRouteOnComplete. </summary>
		CompleteRouteOnComplete,
		///<summary>HandlerType. </summary>
		HandlerType,
		///<summary>HandlerTypeText. </summary>
		HandlerTypeText,
		///<summary>PrintReportType. </summary>
		PrintReportType,
		///<summary>EscalationRouteId. </summary>
		EscalationRouteId,
		///<summary>Status. </summary>
		Status,
		///<summary>StatusText. </summary>
		StatusText,
		///<summary>Timeout. </summary>
		Timeout,
		///<summary>RetrievalSupportNotificationTimeoutMinutes. </summary>
		RetrievalSupportNotificationTimeoutMinutes,
		///<summary>RetrievalSupportNotificationSent. </summary>
		RetrievalSupportNotificationSent,
		///<summary>BeingHandledSupportNotificationTimeoutMinutes. </summary>
		BeingHandledSupportNotificationTimeoutMinutes,
		///<summary>BeingHandledSupportNotificationSent. </summary>
		BeingHandledSupportNotificationSent,
		///<summary>ErrorCode. </summary>
		ErrorCode,
		///<summary>ErrorText. </summary>
		ErrorText,
		///<summary>EscalationStep. </summary>
		EscalationStep,
		///<summary>FieldValue1. </summary>
		FieldValue1,
		///<summary>FieldValue2. </summary>
		FieldValue2,
		///<summary>FieldValue3. </summary>
		FieldValue3,
		///<summary>FieldValue4. </summary>
		FieldValue4,
		///<summary>FieldValue5. </summary>
		FieldValue5,
		///<summary>FieldValue6. </summary>
		FieldValue6,
		///<summary>FieldValue7. </summary>
		FieldValue7,
		///<summary>FieldValue8. </summary>
		FieldValue8,
		///<summary>FieldValue9. </summary>
		FieldValue9,
		///<summary>FieldValue10. </summary>
		FieldValue10,
		///<summary>LogAlways. </summary>
		LogAlways,
		///<summary>SupportpoolId. </summary>
		SupportpoolId,
		///<summary>OriginatedFromRoutestepHandlerId. </summary>
		OriginatedFromRoutestepHandlerId,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>TimeoutExpiresUTC. </summary>
		TimeoutExpiresUTC,
		///<summary>RetrievalSupportNotificationTimeoutUTC. </summary>
		RetrievalSupportNotificationTimeoutUTC,
		///<summary>BeingHandledSupportNotificationTimeoutUTC. </summary>
		BeingHandledSupportNotificationTimeoutUTC,
		///<summary>WaitingToBeRetrievedUTC. </summary>
		WaitingToBeRetrievedUTC,
		///<summary>RetrievedByHandlerUTC. </summary>
		RetrievedByHandlerUTC,
		///<summary>BeingHandledUTC. </summary>
		BeingHandledUTC,
		///<summary>CompletedUTC. </summary>
		CompletedUTC,
		///<summary>ExternalSystemId. </summary>
		ExternalSystemId,
		/// <summary></summary>
		AmountOfFields
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: OrderRoutestephandler. </summary>
	public partial class OrderRoutestephandlerRelations: RelationFactory
	{
		#region Custom Relation code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomRelationCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		


		/// <summary>Gets all relations and delete rules of the OrderRoutestephandlerEntity as a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects.</summary>
		/// <returns>a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects</returns>
		public virtual Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> GetRelationsAndDeleteRules()
		{
			Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> toReturn = new Dictionary<IEntityRelation, ReferentialConstraintDeleteRule>();
			return toReturn;
		}		
	
	

		/// <summary>Returns a new IEntityRelation object, between OrderRoutestephandlerEntity and ExternalSystemEntity over the m:1 relation they have, using the relation between the fields: OrderRoutestephandler.ExternalSystemId - ExternalSystem.ExternalSystemId</summary>
		public virtual IEntityRelation ExternalSystemEntityUsingExternalSystemId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "ExternalSystem", false, new[] { ExternalSystemFields.ExternalSystemId, OrderRoutestephandlerFields.ExternalSystemId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between OrderRoutestephandlerEntity and OrderEntity over the m:1 relation they have, using the relation between the fields: OrderRoutestephandler.OrderId - Order.OrderId</summary>
		public virtual IEntityRelation OrderEntityUsingOrderId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Order", false, new[] { OrderFields.OrderId, OrderRoutestephandlerFields.OrderId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between OrderRoutestephandlerEntity and SupportpoolEntity over the m:1 relation they have, using the relation between the fields: OrderRoutestephandler.SupportpoolId - Supportpool.SupportpoolId</summary>
		public virtual IEntityRelation SupportpoolEntityUsingSupportpoolId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Supportpool", false, new[] { SupportpoolFields.SupportpoolId, OrderRoutestephandlerFields.SupportpoolId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between OrderRoutestephandlerEntity and TerminalEntity over the m:1 relation they have, using the relation between the fields: OrderRoutestephandler.TerminalId - Terminal.TerminalId</summary>
		public virtual IEntityRelation TerminalEntityUsingTerminalId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Terminal", false, new[] { TerminalFields.TerminalId, OrderRoutestephandlerFields.TerminalId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between OrderRoutestephandlerEntity and TerminalEntity over the m:1 relation they have, using the relation between the fields: OrderRoutestephandler.ForwardedFromTerminalId - Terminal.TerminalId</summary>
		public virtual IEntityRelation TerminalEntityUsingForwardedFromTerminalId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "TerminalEntity1", false, new[] { TerminalFields.TerminalId, OrderRoutestephandlerFields.ForwardedFromTerminalId }); }
		}

	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticOrderRoutestephandlerRelations
	{
		internal static readonly IEntityRelation ExternalSystemEntityUsingExternalSystemIdStatic = new OrderRoutestephandlerRelations().ExternalSystemEntityUsingExternalSystemId;
		internal static readonly IEntityRelation OrderEntityUsingOrderIdStatic = new OrderRoutestephandlerRelations().OrderEntityUsingOrderId;
		internal static readonly IEntityRelation SupportpoolEntityUsingSupportpoolIdStatic = new OrderRoutestephandlerRelations().SupportpoolEntityUsingSupportpoolId;
		internal static readonly IEntityRelation TerminalEntityUsingTerminalIdStatic = new OrderRoutestephandlerRelations().TerminalEntityUsingTerminalId;
		internal static readonly IEntityRelation TerminalEntityUsingForwardedFromTerminalIdStatic = new OrderRoutestephandlerRelations().TerminalEntityUsingForwardedFromTerminalId;

		/// <summary>CTor</summary>
		static StaticOrderRoutestephandlerRelations() { }
	}
}

