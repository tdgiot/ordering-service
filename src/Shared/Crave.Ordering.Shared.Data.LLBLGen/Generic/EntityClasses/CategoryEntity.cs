﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 5.8
// Code is generated on: 
// Code is generated using templates: Crave.TemplateBindings
// Templates vendor: Crave Interactive
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.FactoryClasses;
using Crave.Ordering.Shared.Data.LLBLGen.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

// ReSharper disable All
namespace Crave.Ordering.Shared.Data.LLBLGen.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'Category'.<br/><br/></summary>
	[Serializable]
	public partial class CategoryEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END
	
	{
		private EntityCollection<CategoryEntity> _categoryCollection;
		private EntityCollection<MediaEntity> _mediaCollection;
		private EntityCollection<MediaEntity> _mediaCollection1;
		private EntityCollection<OrderitemEntity> _orderitemCollection;
		private EntityCollection<ProductCategoryEntity> _productCategoryCollection;
		private EntityCollection<ProductCategoryTagEntity> _productCategoryTagCollection;
		private EntityCollection<ProductEntity> _productCollectionViaProductCategory;
		private CategoryEntity _parentCategory;
		private CompanyEntity _company;
		private MenuEntity _menu;
		private ProductEntity _product;
		private RouteEntity _route;
		private ScheduleEntity _schedule;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		private static CategoryEntityStaticMetaData _staticMetaData = new CategoryEntityStaticMetaData();
		private static CategoryRelations _relationsFactory = new CategoryRelations();		

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name ParentCategory</summary>
			public static readonly string ParentCategory = "ParentCategory";
			/// <summary>Member name Company</summary>
			public static readonly string Company = "Company";
			/// <summary>Member name Menu</summary>
			public static readonly string Menu = "Menu";
			/// <summary>Member name Product</summary>
			public static readonly string Product = "Product";
			/// <summary>Member name Route</summary>
			public static readonly string Route = "Route";
			/// <summary>Member name Schedule</summary>
			public static readonly string Schedule = "Schedule";
			/// <summary>Member name CategoryCollection</summary>
			public static readonly string CategoryCollection = "CategoryCollection";
			/// <summary>Member name MediaCollection</summary>
			public static readonly string MediaCollection = "MediaCollection";
			/// <summary>Member name MediaCollection1</summary>
			public static readonly string MediaCollection1 = "MediaCollection1";
			/// <summary>Member name OrderitemCollection</summary>
			public static readonly string OrderitemCollection = "OrderitemCollection";
			/// <summary>Member name ProductCategoryCollection</summary>
			public static readonly string ProductCategoryCollection = "ProductCategoryCollection";
			/// <summary>Member name ProductCategoryTagCollection</summary>
			public static readonly string ProductCategoryTagCollection = "ProductCategoryTagCollection";
			/// <summary>Member name ProductCollectionViaProductCategory</summary>
			public static readonly string ProductCollectionViaProductCategory = "ProductCollectionViaProductCategory";
		}

		/// <summary>Static meta-data storage for navigator related information</summary>
		protected class CategoryEntityStaticMetaData : EntityStaticMetaDataBase
		{
			public CategoryEntityStaticMetaData()
			{
				SetEntityCoreInfo("CategoryEntity", InheritanceHierarchyType.None, false, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.CategoryEntity, typeof(CategoryEntity), typeof(CategoryEntityFactory), false);
				AddNavigatorMetaData<CategoryEntity, EntityCollection<CategoryEntity>>("CategoryCollection", a => a._categoryCollection, (a, b) => a._categoryCollection = b, a => a.CategoryCollection, () => new CategoryRelations().CategoryEntityUsingParentCategoryId, typeof(CategoryEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.CategoryEntity);
				AddNavigatorMetaData<CategoryEntity, EntityCollection<MediaEntity>>("MediaCollection", a => a._mediaCollection, (a, b) => a._mediaCollection = b, a => a.MediaCollection, () => new CategoryRelations().MediaEntityUsingActionCategoryId, typeof(MediaEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.MediaEntity);
				AddNavigatorMetaData<CategoryEntity, EntityCollection<MediaEntity>>("MediaCollection1", a => a._mediaCollection1, (a, b) => a._mediaCollection1 = b, a => a.MediaCollection1, () => new CategoryRelations().MediaEntityUsingCategoryId, typeof(MediaEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.MediaEntity);
				AddNavigatorMetaData<CategoryEntity, EntityCollection<OrderitemEntity>>("OrderitemCollection", a => a._orderitemCollection, (a, b) => a._orderitemCollection = b, a => a.OrderitemCollection, () => new CategoryRelations().OrderitemEntityUsingCategoryId, typeof(OrderitemEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.OrderitemEntity);
				AddNavigatorMetaData<CategoryEntity, EntityCollection<ProductCategoryEntity>>("ProductCategoryCollection", a => a._productCategoryCollection, (a, b) => a._productCategoryCollection = b, a => a.ProductCategoryCollection, () => new CategoryRelations().ProductCategoryEntityUsingCategoryId, typeof(ProductCategoryEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ProductCategoryEntity);
				AddNavigatorMetaData<CategoryEntity, EntityCollection<ProductCategoryTagEntity>>("ProductCategoryTagCollection", a => a._productCategoryTagCollection, (a, b) => a._productCategoryTagCollection = b, a => a.ProductCategoryTagCollection, () => new CategoryRelations().ProductCategoryTagEntityUsingCategoryId, typeof(ProductCategoryTagEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ProductCategoryTagEntity);
				AddNavigatorMetaData<CategoryEntity, CategoryEntity>("ParentCategory", "CategoryCollection", (a, b) => a._parentCategory = b, a => a._parentCategory, (a, b) => a.ParentCategory = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticCategoryRelations.CategoryEntityUsingCategoryIdParentCategoryIdStatic, ()=>new CategoryRelations().CategoryEntityUsingCategoryIdParentCategoryId, null, new int[] { (int)CategoryFieldIndex.ParentCategoryId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.CategoryEntity);
				AddNavigatorMetaData<CategoryEntity, CompanyEntity>("Company", "CategoryCollection", (a, b) => a._company = b, a => a._company, (a, b) => a.Company = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticCategoryRelations.CompanyEntityUsingCompanyIdStatic, ()=>new CategoryRelations().CompanyEntityUsingCompanyId, null, new int[] { (int)CategoryFieldIndex.CompanyId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.CompanyEntity);
				AddNavigatorMetaData<CategoryEntity, MenuEntity>("Menu", "CategoryCollection", (a, b) => a._menu = b, a => a._menu, (a, b) => a.Menu = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticCategoryRelations.MenuEntityUsingMenuIdStatic, ()=>new CategoryRelations().MenuEntityUsingMenuId, null, new int[] { (int)CategoryFieldIndex.MenuId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.MenuEntity);
				AddNavigatorMetaData<CategoryEntity, ProductEntity>("Product", "CategoryCollection", (a, b) => a._product = b, a => a._product, (a, b) => a.Product = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticCategoryRelations.ProductEntityUsingProductIdStatic, ()=>new CategoryRelations().ProductEntityUsingProductId, null, new int[] { (int)CategoryFieldIndex.ProductId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ProductEntity);
				AddNavigatorMetaData<CategoryEntity, RouteEntity>("Route", "CategoryCollection", (a, b) => a._route = b, a => a._route, (a, b) => a.Route = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticCategoryRelations.RouteEntityUsingRouteIdStatic, ()=>new CategoryRelations().RouteEntityUsingRouteId, null, new int[] { (int)CategoryFieldIndex.RouteId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.RouteEntity);
				AddNavigatorMetaData<CategoryEntity, ScheduleEntity>("Schedule", "CategoryCollection", (a, b) => a._schedule = b, a => a._schedule, (a, b) => a.Schedule = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticCategoryRelations.ScheduleEntityUsingScheduleIdStatic, ()=>new CategoryRelations().ScheduleEntityUsingScheduleId, null, new int[] { (int)CategoryFieldIndex.ScheduleId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ScheduleEntity);
				AddNavigatorMetaData<CategoryEntity, EntityCollection<ProductEntity>>("ProductCollectionViaProductCategory", a => a._productCollectionViaProductCategory, (a, b) => a._productCollectionViaProductCategory = b, a => a.ProductCollectionViaProductCategory, () => new CategoryRelations().ProductCategoryEntityUsingCategoryId, () => new ProductCategoryRelations().ProductEntityUsingProductId, "CategoryEntity__", "ProductCategory_", typeof(ProductEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ProductEntity);
			}
		}

		/// <summary>Static ctor</summary>
		static CategoryEntity()
		{
		}

		/// <summary> CTor</summary>
		public CategoryEntity()
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public CategoryEntity(IEntityFields2 fields)
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this CategoryEntity</param>
		public CategoryEntity(IValidator validator)
		{
			InitClassEmpty(validator, null);
		}

		/// <summary> CTor</summary>
		/// <param name="categoryId">PK value for Category which data should be fetched into this Category object</param>
		public CategoryEntity(System.Int32 categoryId) : this(categoryId, null)
		{
		}

		/// <summary> CTor</summary>
		/// <param name="categoryId">PK value for Category which data should be fetched into this Category object</param>
		/// <param name="validator">The custom validator object for this CategoryEntity</param>
		public CategoryEntity(System.Int32 categoryId, IValidator validator)
		{
			InitClassEmpty(validator, null);
			this.CategoryId = categoryId;
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected CategoryEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Category' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoCategoryCollection() { return CreateRelationInfoForNavigator("CategoryCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Media' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoMediaCollection() { return CreateRelationInfoForNavigator("MediaCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Media' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoMediaCollection1() { return CreateRelationInfoForNavigator("MediaCollection1"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Orderitem' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoOrderitemCollection() { return CreateRelationInfoForNavigator("OrderitemCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'ProductCategory' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoProductCategoryCollection() { return CreateRelationInfoForNavigator("ProductCategoryCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'ProductCategoryTag' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoProductCategoryTagCollection() { return CreateRelationInfoForNavigator("ProductCategoryTagCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Product' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoProductCollectionViaProductCategory() { return CreateRelationInfoForNavigator("ProductCollectionViaProductCategory"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Category' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoParentCategory() { return CreateRelationInfoForNavigator("ParentCategory"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Company' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoCompany() { return CreateRelationInfoForNavigator("Company"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Menu' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoMenu() { return CreateRelationInfoForNavigator("Menu"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Product' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoProduct() { return CreateRelationInfoForNavigator("Product"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Route' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoRoute() { return CreateRelationInfoForNavigator("Route"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Schedule' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoSchedule() { return CreateRelationInfoForNavigator("Schedule"); }
		
		/// <inheritdoc/>
		protected override EntityStaticMetaDataBase GetEntityStaticMetaData() {	return _staticMetaData; }

		/// <summary>Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		/// <summary>Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this CategoryEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary>The relations object holding all relations of this entity with other entity classes.</summary>
		public static CategoryRelations Relations { get { return _relationsFactory; } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Category' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathCategoryCollection { get { return _staticMetaData.GetPrefetchPathElement("CategoryCollection", CommonEntityBase.CreateEntityCollection<CategoryEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Media' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathMediaCollection { get { return _staticMetaData.GetPrefetchPathElement("MediaCollection", CommonEntityBase.CreateEntityCollection<MediaEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Media' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathMediaCollection1 { get { return _staticMetaData.GetPrefetchPathElement("MediaCollection1", CommonEntityBase.CreateEntityCollection<MediaEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Orderitem' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathOrderitemCollection { get { return _staticMetaData.GetPrefetchPathElement("OrderitemCollection", CommonEntityBase.CreateEntityCollection<OrderitemEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'ProductCategory' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathProductCategoryCollection { get { return _staticMetaData.GetPrefetchPathElement("ProductCategoryCollection", CommonEntityBase.CreateEntityCollection<ProductCategoryEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'ProductCategoryTag' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathProductCategoryTagCollection { get { return _staticMetaData.GetPrefetchPathElement("ProductCategoryTagCollection", CommonEntityBase.CreateEntityCollection<ProductCategoryTagEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Product' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathProductCollectionViaProductCategory { get { return _staticMetaData.GetPrefetchPathElement("ProductCollectionViaProductCategory", CommonEntityBase.CreateEntityCollection<ProductEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Category' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathParentCategoryEntity { get { return _staticMetaData.GetPrefetchPathElement("ParentCategory", CommonEntityBase.CreateEntityCollection<CategoryEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Company' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathCompanyEntity { get { return _staticMetaData.GetPrefetchPathElement("Company", CommonEntityBase.CreateEntityCollection<CompanyEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Menu' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathMenuEntity { get { return _staticMetaData.GetPrefetchPathElement("Menu", CommonEntityBase.CreateEntityCollection<MenuEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Product' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathProductEntity { get { return _staticMetaData.GetPrefetchPathElement("Product", CommonEntityBase.CreateEntityCollection<ProductEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Route' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathRouteEntity { get { return _staticMetaData.GetPrefetchPathElement("Route", CommonEntityBase.CreateEntityCollection<RouteEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Schedule' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathScheduleEntity { get { return _staticMetaData.GetPrefetchPathElement("Schedule", CommonEntityBase.CreateEntityCollection<ScheduleEntity>()); } }

		/// <summary>The CategoryId property of the Entity Category<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Category"."CategoryId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 CategoryId
		{
			get { return (System.Int32)GetValue((int)CategoryFieldIndex.CategoryId, true); }
			set { SetValue((int)CategoryFieldIndex.CategoryId, value); }		}

		/// <summary>The CompanyId property of the Entity Category<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Category"."CompanyId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CompanyId
		{
			get { return (System.Int32)GetValue((int)CategoryFieldIndex.CompanyId, true); }
			set	{ SetValue((int)CategoryFieldIndex.CompanyId, value); }
		}

		/// <summary>The MenuId property of the Entity Category<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Category"."MenuId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> MenuId
		{
			get { return (Nullable<System.Int32>)GetValue((int)CategoryFieldIndex.MenuId, false); }
			set	{ SetValue((int)CategoryFieldIndex.MenuId, value); }
		}

		/// <summary>The ParentCategoryId property of the Entity Category<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Category"."ParentCategoryId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ParentCategoryId
		{
			get { return (Nullable<System.Int32>)GetValue((int)CategoryFieldIndex.ParentCategoryId, false); }
			set	{ SetValue((int)CategoryFieldIndex.ParentCategoryId, value); }
		}

		/// <summary>The GenericcategoryId property of the Entity Category<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Category"."GenericcategoryId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> GenericcategoryId
		{
			get { return (Nullable<System.Int32>)GetValue((int)CategoryFieldIndex.GenericcategoryId, false); }
			set	{ SetValue((int)CategoryFieldIndex.GenericcategoryId, value); }
		}

		/// <summary>The RouteId property of the Entity Category<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Category"."RouteId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> RouteId
		{
			get { return (Nullable<System.Int32>)GetValue((int)CategoryFieldIndex.RouteId, false); }
			set	{ SetValue((int)CategoryFieldIndex.RouteId, value); }
		}

		/// <summary>The ScheduleId property of the Entity Category<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Category"."ScheduleId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ScheduleId
		{
			get { return (Nullable<System.Int32>)GetValue((int)CategoryFieldIndex.ScheduleId, false); }
			set	{ SetValue((int)CategoryFieldIndex.ScheduleId, value); }
		}

		/// <summary>The Name property of the Entity Category<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Category"."Name".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 100.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)CategoryFieldIndex.Name, true); }
			set	{ SetValue((int)CategoryFieldIndex.Name, value); }
		}

		/// <summary>The SortOrder property of the Entity Category<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Category"."SortOrder".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 SortOrder
		{
			get { return (System.Int32)GetValue((int)CategoryFieldIndex.SortOrder, true); }
			set	{ SetValue((int)CategoryFieldIndex.SortOrder, value); }
		}

		/// <summary>The PoscategoryId property of the Entity Category<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Category"."PoscategoryId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> PoscategoryId
		{
			get { return (Nullable<System.Int32>)GetValue((int)CategoryFieldIndex.PoscategoryId, false); }
			set	{ SetValue((int)CategoryFieldIndex.PoscategoryId, value); }
		}

		/// <summary>The ProductId property of the Entity Category<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Category"."ProductId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ProductId
		{
			get { return (Nullable<System.Int32>)GetValue((int)CategoryFieldIndex.ProductId, false); }
			set	{ SetValue((int)CategoryFieldIndex.ProductId, value); }
		}

		/// <summary>The AvailableOnOtoucho property of the Entity Category<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Category"."AvailableOnOtoucho".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean AvailableOnOtoucho
		{
			get { return (System.Boolean)GetValue((int)CategoryFieldIndex.AvailableOnOtoucho, true); }
			set	{ SetValue((int)CategoryFieldIndex.AvailableOnOtoucho, value); }
		}

		/// <summary>The AvailableOnObymobi property of the Entity Category<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Category"."AvailableOnObymobi".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean AvailableOnObymobi
		{
			get { return (System.Boolean)GetValue((int)CategoryFieldIndex.AvailableOnObymobi, true); }
			set	{ SetValue((int)CategoryFieldIndex.AvailableOnObymobi, value); }
		}

		/// <summary>The Rateable property of the Entity Category<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Category"."Rateable".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> Rateable
		{
			get { return (Nullable<System.Boolean>)GetValue((int)CategoryFieldIndex.Rateable, false); }
			set	{ SetValue((int)CategoryFieldIndex.Rateable, value); }
		}

		/// <summary>The Visible property of the Entity Category<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Category"."Visible".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean Visible
		{
			get { return (System.Boolean)GetValue((int)CategoryFieldIndex.Visible, true); }
			set	{ SetValue((int)CategoryFieldIndex.Visible, value); }
		}

		/// <summary>The Type property of the Entity Category<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Category"."Type".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Crave.Enums.CategoryType Type
		{
			get { return (Crave.Enums.CategoryType)GetValue((int)CategoryFieldIndex.Type, true); }
			set	{ SetValue((int)CategoryFieldIndex.Type, value); }
		}

		/// <summary>The HidePrices property of the Entity Category<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Category"."HidePrices".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean HidePrices
		{
			get { return (System.Boolean)GetValue((int)CategoryFieldIndex.HidePrices, true); }
			set	{ SetValue((int)CategoryFieldIndex.HidePrices, value); }
		}

		/// <summary>The AnnouncementAction property of the Entity Category<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Category"."AnnouncementAction".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean AnnouncementAction
		{
			get { return (System.Boolean)GetValue((int)CategoryFieldIndex.AnnouncementAction, true); }
			set	{ SetValue((int)CategoryFieldIndex.AnnouncementAction, value); }
		}

		/// <summary>The Color property of the Entity Category<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Category"."Color".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 Color
		{
			get { return (System.Int32)GetValue((int)CategoryFieldIndex.Color, true); }
			set	{ SetValue((int)CategoryFieldIndex.Color, value); }
		}

		/// <summary>The Description property of the Entity Category<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Category"."Description".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)CategoryFieldIndex.Description, true); }
			set	{ SetValue((int)CategoryFieldIndex.Description, value); }
		}

		/// <summary>The Geofencing property of the Entity Category<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Category"."Geofencing".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> Geofencing
		{
			get { return (Nullable<System.Boolean>)GetValue((int)CategoryFieldIndex.Geofencing, false); }
			set	{ SetValue((int)CategoryFieldIndex.Geofencing, value); }
		}

		/// <summary>The AllowFreeText property of the Entity Category<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Category"."AllowFreeText".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> AllowFreeText
		{
			get { return (Nullable<System.Boolean>)GetValue((int)CategoryFieldIndex.AllowFreeText, false); }
			set	{ SetValue((int)CategoryFieldIndex.AllowFreeText, value); }
		}

		/// <summary>The ViewLayoutType property of the Entity Category<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Category"."ViewLayoutType".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Crave.Enums.ViewLayoutType ViewLayoutType
		{
			get { return (Crave.Enums.ViewLayoutType)GetValue((int)CategoryFieldIndex.ViewLayoutType, true); }
			set	{ SetValue((int)CategoryFieldIndex.ViewLayoutType, value); }
		}

		/// <summary>The DeliveryLocationType property of the Entity Category<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Category"."DeliveryLocationType".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Crave.Enums.DeliveryLocationType DeliveryLocationType
		{
			get { return (Crave.Enums.DeliveryLocationType)GetValue((int)CategoryFieldIndex.DeliveryLocationType, true); }
			set	{ SetValue((int)CategoryFieldIndex.DeliveryLocationType, value); }
		}

		/// <summary>The SupportNotificationType property of the Entity Category<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Category"."SupportNotificationType".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Crave.Enums.SupportNotificationType SupportNotificationType
		{
			get { return (Crave.Enums.SupportNotificationType)GetValue((int)CategoryFieldIndex.SupportNotificationType, true); }
			set	{ SetValue((int)CategoryFieldIndex.SupportNotificationType, value); }
		}

		/// <summary>The VisibilityType property of the Entity Category<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Category"."VisibilityType".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Crave.Enums.VisibilityType VisibilityType
		{
			get { return (Crave.Enums.VisibilityType)GetValue((int)CategoryFieldIndex.VisibilityType, true); }
			set	{ SetValue((int)CategoryFieldIndex.VisibilityType, value); }
		}

		/// <summary>The ButtonText property of the Entity Category<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Category"."ButtonText".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 100.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ButtonText
		{
			get { return (System.String)GetValue((int)CategoryFieldIndex.ButtonText, true); }
			set	{ SetValue((int)CategoryFieldIndex.ButtonText, value); }
		}

		/// <summary>The CustomizeButtonText property of the Entity Category<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Category"."CustomizeButtonText".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 100.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CustomizeButtonText
		{
			get { return (System.String)GetValue((int)CategoryFieldIndex.CustomizeButtonText, true); }
			set	{ SetValue((int)CategoryFieldIndex.CustomizeButtonText, value); }
		}

		/// <summary>The CreatedBy property of the Entity Category<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Category"."CreatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)CategoryFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)CategoryFieldIndex.CreatedBy, value); }
		}

		/// <summary>The UpdatedBy property of the Entity Category<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Category"."UpdatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)CategoryFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)CategoryFieldIndex.UpdatedBy, value); }
		}

		/// <summary>The CreatedUTC property of the Entity Category<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Category"."CreatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)CategoryFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)CategoryFieldIndex.CreatedUTC, value); }
		}

		/// <summary>The UpdatedUTC property of the Entity Category<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Category"."UpdatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)CategoryFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)CategoryFieldIndex.UpdatedUTC, value); }
		}

		/// <summary>The ViewType property of the Entity Category<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Category"."ViewType".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ViewType
		{
			get { return (System.Int32)GetValue((int)CategoryFieldIndex.ViewType, true); }
			set	{ SetValue((int)CategoryFieldIndex.ViewType, value); }
		}

		/// <summary>The MenuItemsMustBeLinkedToExternalProduct property of the Entity Category<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Category"."MenuItemsMustBeLinkedToExternalProduct".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean MenuItemsMustBeLinkedToExternalProduct
		{
			get { return (System.Boolean)GetValue((int)CategoryFieldIndex.MenuItemsMustBeLinkedToExternalProduct, true); }
			set	{ SetValue((int)CategoryFieldIndex.MenuItemsMustBeLinkedToExternalProduct, value); }
		}

		/// <summary>The ShowName property of the Entity Category<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Category"."ShowName".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean ShowName
		{
			get { return (System.Boolean)GetValue((int)CategoryFieldIndex.ShowName, true); }
			set	{ SetValue((int)CategoryFieldIndex.ShowName, value); }
		}

		/// <summary>The CoversType property of the Entity Category<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Category"."CoversType".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CoversType
		{
			get { return (System.Int32)GetValue((int)CategoryFieldIndex.CoversType, true); }
			set	{ SetValue((int)CategoryFieldIndex.CoversType, value); }
		}

		/// <summary>Gets the EntityCollection with the related entities of type 'CategoryEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(CategoryEntity))]
		public virtual EntityCollection<CategoryEntity> CategoryCollection { get { return GetOrCreateEntityCollection<CategoryEntity, CategoryEntityFactory>("ParentCategory", true, false, ref _categoryCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'MediaEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(MediaEntity))]
		public virtual EntityCollection<MediaEntity> MediaCollection { get { return GetOrCreateEntityCollection<MediaEntity, MediaEntityFactory>("Category", true, false, ref _mediaCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'MediaEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(MediaEntity))]
		public virtual EntityCollection<MediaEntity> MediaCollection1 { get { return GetOrCreateEntityCollection<MediaEntity, MediaEntityFactory>("Category1", true, false, ref _mediaCollection1); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'OrderitemEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(OrderitemEntity))]
		public virtual EntityCollection<OrderitemEntity> OrderitemCollection { get { return GetOrCreateEntityCollection<OrderitemEntity, OrderitemEntityFactory>("Category", true, false, ref _orderitemCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'ProductCategoryEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(ProductCategoryEntity))]
		public virtual EntityCollection<ProductCategoryEntity> ProductCategoryCollection { get { return GetOrCreateEntityCollection<ProductCategoryEntity, ProductCategoryEntityFactory>("Category", true, false, ref _productCategoryCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'ProductCategoryTagEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(ProductCategoryTagEntity))]
		public virtual EntityCollection<ProductCategoryTagEntity> ProductCategoryTagCollection { get { return GetOrCreateEntityCollection<ProductCategoryTagEntity, ProductCategoryTagEntityFactory>("Category", true, false, ref _productCategoryTagCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'ProductEntity' which are related to this entity via a relation of type 'm:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(ProductEntity))]
		public virtual EntityCollection<ProductEntity> ProductCollectionViaProductCategory { get { return GetOrCreateEntityCollection<ProductEntity, ProductEntityFactory>("CategoryCollectionViaProductCategory", false, true, ref _productCollectionViaProductCategory); } }

		/// <summary>Gets / sets related entity of type 'CategoryEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual CategoryEntity ParentCategory
		{
			get { return _parentCategory; }
			set { SetSingleRelatedEntityNavigator(value, "ParentCategory"); }
		}

		/// <summary>Gets / sets related entity of type 'CompanyEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual CompanyEntity Company
		{
			get { return _company; }
			set { SetSingleRelatedEntityNavigator(value, "Company"); }
		}

		/// <summary>Gets / sets related entity of type 'MenuEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual MenuEntity Menu
		{
			get { return _menu; }
			set { SetSingleRelatedEntityNavigator(value, "Menu"); }
		}

		/// <summary>Gets / sets related entity of type 'ProductEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual ProductEntity Product
		{
			get { return _product; }
			set { SetSingleRelatedEntityNavigator(value, "Product"); }
		}

		/// <summary>Gets / sets related entity of type 'RouteEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual RouteEntity Route
		{
			get { return _route; }
			set { SetSingleRelatedEntityNavigator(value, "Route"); }
		}

		/// <summary>Gets / sets related entity of type 'ScheduleEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual ScheduleEntity Schedule
		{
			get { return _schedule; }
			set { SetSingleRelatedEntityNavigator(value, "Schedule"); }
		}


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen
{
	public enum CategoryFieldIndex
	{
		///<summary>CategoryId. </summary>
		CategoryId,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>MenuId. </summary>
		MenuId,
		///<summary>ParentCategoryId. </summary>
		ParentCategoryId,
		///<summary>GenericcategoryId. </summary>
		GenericcategoryId,
		///<summary>RouteId. </summary>
		RouteId,
		///<summary>ScheduleId. </summary>
		ScheduleId,
		///<summary>Name. </summary>
		Name,
		///<summary>SortOrder. </summary>
		SortOrder,
		///<summary>PoscategoryId. </summary>
		PoscategoryId,
		///<summary>ProductId. </summary>
		ProductId,
		///<summary>AvailableOnOtoucho. </summary>
		AvailableOnOtoucho,
		///<summary>AvailableOnObymobi. </summary>
		AvailableOnObymobi,
		///<summary>Rateable. </summary>
		Rateable,
		///<summary>Visible. </summary>
		Visible,
		///<summary>Type. </summary>
		Type,
		///<summary>HidePrices. </summary>
		HidePrices,
		///<summary>AnnouncementAction. </summary>
		AnnouncementAction,
		///<summary>Color. </summary>
		Color,
		///<summary>Description. </summary>
		Description,
		///<summary>Geofencing. </summary>
		Geofencing,
		///<summary>AllowFreeText. </summary>
		AllowFreeText,
		///<summary>ViewLayoutType. </summary>
		ViewLayoutType,
		///<summary>DeliveryLocationType. </summary>
		DeliveryLocationType,
		///<summary>SupportNotificationType. </summary>
		SupportNotificationType,
		///<summary>VisibilityType. </summary>
		VisibilityType,
		///<summary>ButtonText. </summary>
		ButtonText,
		///<summary>CustomizeButtonText. </summary>
		CustomizeButtonText,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>ViewType. </summary>
		ViewType,
		///<summary>MenuItemsMustBeLinkedToExternalProduct. </summary>
		MenuItemsMustBeLinkedToExternalProduct,
		///<summary>ShowName. </summary>
		ShowName,
		///<summary>CoversType. </summary>
		CoversType,
		/// <summary></summary>
		AmountOfFields
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Category. </summary>
	public partial class CategoryRelations: RelationFactory
	{
		#region Custom Relation code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomRelationCode
		private const ReferentialConstraintDeleteRule DeleteRuleForCategoryEntityUsingParentCategoryId = ReferentialConstraintDeleteRule.SetNull;
        private const ReferentialConstraintDeleteRule DeleteRuleForMediaEntityUsingActionCategoryId = ReferentialConstraintDeleteRule.SetNull;
        private const ReferentialConstraintDeleteRule DeleteRuleForMediaEntityUsingCategoryId = ReferentialConstraintDeleteRule.Cascade;
		private const ReferentialConstraintDeleteRule DeleteRuleForOrderitemEntityUsingCategoryId = ReferentialConstraintDeleteRule.SetNull;
        private const ReferentialConstraintDeleteRule DeleteRuleForProductCategoryEntityUsingCategoryId = ReferentialConstraintDeleteRule.Cascade;
        private const ReferentialConstraintDeleteRule DeleteRuleForProductCategoryTagEntityUsingCategoryId = ReferentialConstraintDeleteRule.NoAction;
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		
		// private const ReferentialConstraintDeleteRule DeleteRuleForCategoryEntityUsingParentCategoryId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForMediaEntityUsingActionCategoryId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForMediaEntityUsingCategoryId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForOrderitemEntityUsingCategoryId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForProductCategoryEntityUsingCategoryId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForProductCategoryTagEntityUsingCategoryId = ReferentialConstraintDeleteRule


		/// <summary>Gets all relations and delete rules of the CategoryEntity as a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects.</summary>
		/// <returns>a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects</returns>
		public virtual Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> GetRelationsAndDeleteRules()
		{
			Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> toReturn = new Dictionary<IEntityRelation, ReferentialConstraintDeleteRule>();
			toReturn.Add(this.CategoryEntityUsingParentCategoryId, CategoryRelations.DeleteRuleForCategoryEntityUsingParentCategoryId);
			toReturn.Add(this.MediaEntityUsingActionCategoryId, CategoryRelations.DeleteRuleForMediaEntityUsingActionCategoryId);
			toReturn.Add(this.MediaEntityUsingCategoryId, CategoryRelations.DeleteRuleForMediaEntityUsingCategoryId);
			toReturn.Add(this.OrderitemEntityUsingCategoryId, CategoryRelations.DeleteRuleForOrderitemEntityUsingCategoryId);
			toReturn.Add(this.ProductCategoryEntityUsingCategoryId, CategoryRelations.DeleteRuleForProductCategoryEntityUsingCategoryId);
			toReturn.Add(this.ProductCategoryTagEntityUsingCategoryId, CategoryRelations.DeleteRuleForProductCategoryTagEntityUsingCategoryId);
			return toReturn;
		}		
	
	
		/// <summary>Returns a new IEntityRelation object, between CategoryEntity and CategoryEntity over the 1:n relation they have, using the relation between the fields: Category.CategoryId - Category.ParentCategoryId</summary>
		public virtual IEntityRelation CategoryEntityUsingParentCategoryId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "CategoryCollection", true, new[] { CategoryFields.CategoryId, CategoryFields.ParentCategoryId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between CategoryEntity and MediaEntity over the 1:n relation they have, using the relation between the fields: Category.CategoryId - Media.ActionCategoryId</summary>
		public virtual IEntityRelation MediaEntityUsingActionCategoryId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "MediaCollection", true, new[] { CategoryFields.CategoryId, MediaFields.ActionCategoryId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between CategoryEntity and MediaEntity over the 1:n relation they have, using the relation between the fields: Category.CategoryId - Media.CategoryId</summary>
		public virtual IEntityRelation MediaEntityUsingCategoryId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "MediaCollection1", true, new[] { CategoryFields.CategoryId, MediaFields.CategoryId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between CategoryEntity and OrderitemEntity over the 1:n relation they have, using the relation between the fields: Category.CategoryId - Orderitem.CategoryId</summary>
		public virtual IEntityRelation OrderitemEntityUsingCategoryId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "OrderitemCollection", true, new[] { CategoryFields.CategoryId, OrderitemFields.CategoryId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between CategoryEntity and ProductCategoryEntity over the 1:n relation they have, using the relation between the fields: Category.CategoryId - ProductCategory.CategoryId</summary>
		public virtual IEntityRelation ProductCategoryEntityUsingCategoryId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "ProductCategoryCollection", true, new[] { CategoryFields.CategoryId, ProductCategoryFields.CategoryId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between CategoryEntity and ProductCategoryTagEntity over the 1:n relation they have, using the relation between the fields: Category.CategoryId - ProductCategoryTag.CategoryId</summary>
		public virtual IEntityRelation ProductCategoryTagEntityUsingCategoryId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "ProductCategoryTagCollection", true, new[] { CategoryFields.CategoryId, ProductCategoryTagFields.CategoryId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between CategoryEntity and CategoryEntity over the m:1 relation they have, using the relation between the fields: Category.ParentCategoryId - Category.CategoryId</summary>
		public virtual IEntityRelation CategoryEntityUsingCategoryIdParentCategoryId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "ParentCategory", false, new[] { CategoryFields.CategoryId, CategoryFields.ParentCategoryId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between CategoryEntity and CompanyEntity over the m:1 relation they have, using the relation between the fields: Category.CompanyId - Company.CompanyId</summary>
		public virtual IEntityRelation CompanyEntityUsingCompanyId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Company", false, new[] { CompanyFields.CompanyId, CategoryFields.CompanyId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between CategoryEntity and MenuEntity over the m:1 relation they have, using the relation between the fields: Category.MenuId - Menu.MenuId</summary>
		public virtual IEntityRelation MenuEntityUsingMenuId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Menu", false, new[] { MenuFields.MenuId, CategoryFields.MenuId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between CategoryEntity and ProductEntity over the m:1 relation they have, using the relation between the fields: Category.ProductId - Product.ProductId</summary>
		public virtual IEntityRelation ProductEntityUsingProductId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Product", false, new[] { ProductFields.ProductId, CategoryFields.ProductId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between CategoryEntity and RouteEntity over the m:1 relation they have, using the relation between the fields: Category.RouteId - Route.RouteId</summary>
		public virtual IEntityRelation RouteEntityUsingRouteId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Route", false, new[] { RouteFields.RouteId, CategoryFields.RouteId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between CategoryEntity and ScheduleEntity over the m:1 relation they have, using the relation between the fields: Category.ScheduleId - Schedule.ScheduleId</summary>
		public virtual IEntityRelation ScheduleEntityUsingScheduleId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Schedule", false, new[] { ScheduleFields.ScheduleId, CategoryFields.ScheduleId }); }
		}

	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticCategoryRelations
	{
		internal static readonly IEntityRelation CategoryEntityUsingParentCategoryIdStatic = new CategoryRelations().CategoryEntityUsingParentCategoryId;
		internal static readonly IEntityRelation MediaEntityUsingActionCategoryIdStatic = new CategoryRelations().MediaEntityUsingActionCategoryId;
		internal static readonly IEntityRelation MediaEntityUsingCategoryIdStatic = new CategoryRelations().MediaEntityUsingCategoryId;
		internal static readonly IEntityRelation OrderitemEntityUsingCategoryIdStatic = new CategoryRelations().OrderitemEntityUsingCategoryId;
		internal static readonly IEntityRelation ProductCategoryEntityUsingCategoryIdStatic = new CategoryRelations().ProductCategoryEntityUsingCategoryId;
		internal static readonly IEntityRelation ProductCategoryTagEntityUsingCategoryIdStatic = new CategoryRelations().ProductCategoryTagEntityUsingCategoryId;
		internal static readonly IEntityRelation CategoryEntityUsingCategoryIdParentCategoryIdStatic = new CategoryRelations().CategoryEntityUsingCategoryIdParentCategoryId;
		internal static readonly IEntityRelation CompanyEntityUsingCompanyIdStatic = new CategoryRelations().CompanyEntityUsingCompanyId;
		internal static readonly IEntityRelation MenuEntityUsingMenuIdStatic = new CategoryRelations().MenuEntityUsingMenuId;
		internal static readonly IEntityRelation ProductEntityUsingProductIdStatic = new CategoryRelations().ProductEntityUsingProductId;
		internal static readonly IEntityRelation RouteEntityUsingRouteIdStatic = new CategoryRelations().RouteEntityUsingRouteId;
		internal static readonly IEntityRelation ScheduleEntityUsingScheduleIdStatic = new CategoryRelations().ScheduleEntityUsingScheduleId;

		/// <summary>CTor</summary>
		static StaticCategoryRelations() { }
	}
}

