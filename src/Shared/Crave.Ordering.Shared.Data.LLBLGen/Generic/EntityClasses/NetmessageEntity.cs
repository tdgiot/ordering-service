﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 5.8
// Code is generated on: 
// Code is generated using templates: Crave.TemplateBindings
// Templates vendor: Crave Interactive
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.FactoryClasses;
using Crave.Ordering.Shared.Data.LLBLGen.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

// ReSharper disable All
namespace Crave.Ordering.Shared.Data.LLBLGen.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'Netmessage'.<br/><br/></summary>
	[Serializable]
	public partial class NetmessageEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END
	
	{
		private ClientEntity _client;
		private ClientEntity _clientEntity1;
		private CompanyEntity _company;
		private CompanyEntity _companyEntity1;
		private CustomerEntity _customer;
		private CustomerEntity _customerEntity1;
		private DeliverypointEntity _deliverypoint;
		private DeliverypointEntity _deliverypointEntity1;
		private DeliverypointgroupEntity _deliverypointgroup;
		private TerminalEntity _terminal;
		private TerminalEntity _terminalEntity1;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		private static NetmessageEntityStaticMetaData _staticMetaData = new NetmessageEntityStaticMetaData();
		private static NetmessageRelations _relationsFactory = new NetmessageRelations();		

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Client</summary>
			public static readonly string Client = "Client";
			/// <summary>Member name ClientEntity1</summary>
			public static readonly string ClientEntity1 = "ClientEntity1";
			/// <summary>Member name Company</summary>
			public static readonly string Company = "Company";
			/// <summary>Member name CompanyEntity1</summary>
			public static readonly string CompanyEntity1 = "CompanyEntity1";
			/// <summary>Member name Customer</summary>
			public static readonly string Customer = "Customer";
			/// <summary>Member name CustomerEntity1</summary>
			public static readonly string CustomerEntity1 = "CustomerEntity1";
			/// <summary>Member name Deliverypoint</summary>
			public static readonly string Deliverypoint = "Deliverypoint";
			/// <summary>Member name DeliverypointEntity1</summary>
			public static readonly string DeliverypointEntity1 = "DeliverypointEntity1";
			/// <summary>Member name Deliverypointgroup</summary>
			public static readonly string Deliverypointgroup = "Deliverypointgroup";
			/// <summary>Member name Terminal</summary>
			public static readonly string Terminal = "Terminal";
			/// <summary>Member name TerminalEntity1</summary>
			public static readonly string TerminalEntity1 = "TerminalEntity1";
		}

		/// <summary>Static meta-data storage for navigator related information</summary>
		protected class NetmessageEntityStaticMetaData : EntityStaticMetaDataBase
		{
			public NetmessageEntityStaticMetaData()
			{
				SetEntityCoreInfo("NetmessageEntity", InheritanceHierarchyType.None, false, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.NetmessageEntity, typeof(NetmessageEntity), typeof(NetmessageEntityFactory), false);
				AddNavigatorMetaData<NetmessageEntity, ClientEntity>("Client", "NetmessageCollection", (a, b) => a._client = b, a => a._client, (a, b) => a.Client = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticNetmessageRelations.ClientEntityUsingSenderClientIdStatic, ()=>new NetmessageRelations().ClientEntityUsingSenderClientId, null, new int[] { (int)NetmessageFieldIndex.SenderClientId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ClientEntity);
				AddNavigatorMetaData<NetmessageEntity, ClientEntity>("ClientEntity1", "NetmessageCollection1", (a, b) => a._clientEntity1 = b, a => a._clientEntity1, (a, b) => a.ClientEntity1 = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticNetmessageRelations.ClientEntityUsingReceiverClientIdStatic, ()=>new NetmessageRelations().ClientEntityUsingReceiverClientId, null, new int[] { (int)NetmessageFieldIndex.ReceiverClientId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ClientEntity);
				AddNavigatorMetaData<NetmessageEntity, CompanyEntity>("Company", "NetmessageCollection", (a, b) => a._company = b, a => a._company, (a, b) => a.Company = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticNetmessageRelations.CompanyEntityUsingSenderCompanyIdStatic, ()=>new NetmessageRelations().CompanyEntityUsingSenderCompanyId, null, new int[] { (int)NetmessageFieldIndex.SenderCompanyId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.CompanyEntity);
				AddNavigatorMetaData<NetmessageEntity, CompanyEntity>("CompanyEntity1", "NetmessageCollection1", (a, b) => a._companyEntity1 = b, a => a._companyEntity1, (a, b) => a.CompanyEntity1 = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticNetmessageRelations.CompanyEntityUsingReceiverCompanyIdStatic, ()=>new NetmessageRelations().CompanyEntityUsingReceiverCompanyId, null, new int[] { (int)NetmessageFieldIndex.ReceiverCompanyId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.CompanyEntity);
				AddNavigatorMetaData<NetmessageEntity, CustomerEntity>("Customer", "NetmessageCollection", (a, b) => a._customer = b, a => a._customer, (a, b) => a.Customer = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticNetmessageRelations.CustomerEntityUsingSenderCustomerIdStatic, ()=>new NetmessageRelations().CustomerEntityUsingSenderCustomerId, null, new int[] { (int)NetmessageFieldIndex.SenderCustomerId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.CustomerEntity);
				AddNavigatorMetaData<NetmessageEntity, CustomerEntity>("CustomerEntity1", "NetmessageCollection1", (a, b) => a._customerEntity1 = b, a => a._customerEntity1, (a, b) => a.CustomerEntity1 = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticNetmessageRelations.CustomerEntityUsingReceiverCustomerIdStatic, ()=>new NetmessageRelations().CustomerEntityUsingReceiverCustomerId, null, new int[] { (int)NetmessageFieldIndex.ReceiverCustomerId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.CustomerEntity);
				AddNavigatorMetaData<NetmessageEntity, DeliverypointEntity>("Deliverypoint", "NetmessageCollection", (a, b) => a._deliverypoint = b, a => a._deliverypoint, (a, b) => a.Deliverypoint = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticNetmessageRelations.DeliverypointEntityUsingSenderDeliverypointIdStatic, ()=>new NetmessageRelations().DeliverypointEntityUsingSenderDeliverypointId, null, new int[] { (int)NetmessageFieldIndex.SenderDeliverypointId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.DeliverypointEntity);
				AddNavigatorMetaData<NetmessageEntity, DeliverypointEntity>("DeliverypointEntity1", "NetmessageCollection1", (a, b) => a._deliverypointEntity1 = b, a => a._deliverypointEntity1, (a, b) => a.DeliverypointEntity1 = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticNetmessageRelations.DeliverypointEntityUsingReceiverDeliverypointIdStatic, ()=>new NetmessageRelations().DeliverypointEntityUsingReceiverDeliverypointId, null, new int[] { (int)NetmessageFieldIndex.ReceiverDeliverypointId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.DeliverypointEntity);
				AddNavigatorMetaData<NetmessageEntity, DeliverypointgroupEntity>("Deliverypointgroup", "NetmessageCollection", (a, b) => a._deliverypointgroup = b, a => a._deliverypointgroup, (a, b) => a.Deliverypointgroup = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticNetmessageRelations.DeliverypointgroupEntityUsingReceiverDeliverypointgroupIdStatic, ()=>new NetmessageRelations().DeliverypointgroupEntityUsingReceiverDeliverypointgroupId, null, new int[] { (int)NetmessageFieldIndex.ReceiverDeliverypointgroupId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.DeliverypointgroupEntity);
				AddNavigatorMetaData<NetmessageEntity, TerminalEntity>("Terminal", "SentNetmessageCollection", (a, b) => a._terminal = b, a => a._terminal, (a, b) => a.Terminal = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticNetmessageRelations.TerminalEntityUsingSenderTerminalIdStatic, ()=>new NetmessageRelations().TerminalEntityUsingSenderTerminalId, null, new int[] { (int)NetmessageFieldIndex.SenderTerminalId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.TerminalEntity);
				AddNavigatorMetaData<NetmessageEntity, TerminalEntity>("TerminalEntity1", "ReceivedNetmessageCollection", (a, b) => a._terminalEntity1 = b, a => a._terminalEntity1, (a, b) => a.TerminalEntity1 = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticNetmessageRelations.TerminalEntityUsingReceiverTerminalIdStatic, ()=>new NetmessageRelations().TerminalEntityUsingReceiverTerminalId, null, new int[] { (int)NetmessageFieldIndex.ReceiverTerminalId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.TerminalEntity);
			}
		}

		/// <summary>Static ctor</summary>
		static NetmessageEntity()
		{
		}

		/// <summary> CTor</summary>
		public NetmessageEntity()
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public NetmessageEntity(IEntityFields2 fields)
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this NetmessageEntity</param>
		public NetmessageEntity(IValidator validator)
		{
			InitClassEmpty(validator, null);
		}

		/// <summary> CTor</summary>
		/// <param name="netmessageId">PK value for Netmessage which data should be fetched into this Netmessage object</param>
		public NetmessageEntity(System.Int32 netmessageId) : this(netmessageId, null)
		{
		}

		/// <summary> CTor</summary>
		/// <param name="netmessageId">PK value for Netmessage which data should be fetched into this Netmessage object</param>
		/// <param name="validator">The custom validator object for this NetmessageEntity</param>
		public NetmessageEntity(System.Int32 netmessageId, IValidator validator)
		{
			InitClassEmpty(validator, null);
			this.NetmessageId = netmessageId;
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected NetmessageEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Client' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoClient() { return CreateRelationInfoForNavigator("Client"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Client' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoClientEntity1() { return CreateRelationInfoForNavigator("ClientEntity1"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Company' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoCompany() { return CreateRelationInfoForNavigator("Company"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Company' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoCompanyEntity1() { return CreateRelationInfoForNavigator("CompanyEntity1"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Customer' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoCustomer() { return CreateRelationInfoForNavigator("Customer"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Customer' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoCustomerEntity1() { return CreateRelationInfoForNavigator("CustomerEntity1"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Deliverypoint' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoDeliverypoint() { return CreateRelationInfoForNavigator("Deliverypoint"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Deliverypoint' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoDeliverypointEntity1() { return CreateRelationInfoForNavigator("DeliverypointEntity1"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Deliverypointgroup' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoDeliverypointgroup() { return CreateRelationInfoForNavigator("Deliverypointgroup"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Terminal' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoTerminal() { return CreateRelationInfoForNavigator("Terminal"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Terminal' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoTerminalEntity1() { return CreateRelationInfoForNavigator("TerminalEntity1"); }
		
		/// <inheritdoc/>
		protected override EntityStaticMetaDataBase GetEntityStaticMetaData() {	return _staticMetaData; }

		/// <summary>Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		/// <summary>Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this NetmessageEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary>The relations object holding all relations of this entity with other entity classes.</summary>
		public static NetmessageRelations Relations { get { return _relationsFactory; } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Client' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathClientEntity { get { return _staticMetaData.GetPrefetchPathElement("Client", CommonEntityBase.CreateEntityCollection<ClientEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Client' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathClientEntity1Entity { get { return _staticMetaData.GetPrefetchPathElement("ClientEntity1", CommonEntityBase.CreateEntityCollection<ClientEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Company' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathCompanyEntity { get { return _staticMetaData.GetPrefetchPathElement("Company", CommonEntityBase.CreateEntityCollection<CompanyEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Company' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathCompanyEntity1Entity { get { return _staticMetaData.GetPrefetchPathElement("CompanyEntity1", CommonEntityBase.CreateEntityCollection<CompanyEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Customer' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathCustomerEntity { get { return _staticMetaData.GetPrefetchPathElement("Customer", CommonEntityBase.CreateEntityCollection<CustomerEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Customer' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathCustomerEntity1Entity { get { return _staticMetaData.GetPrefetchPathElement("CustomerEntity1", CommonEntityBase.CreateEntityCollection<CustomerEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Deliverypoint' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathDeliverypointEntity { get { return _staticMetaData.GetPrefetchPathElement("Deliverypoint", CommonEntityBase.CreateEntityCollection<DeliverypointEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Deliverypoint' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathDeliverypointEntity1Entity { get { return _staticMetaData.GetPrefetchPathElement("DeliverypointEntity1", CommonEntityBase.CreateEntityCollection<DeliverypointEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Deliverypointgroup' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathDeliverypointgroupEntity { get { return _staticMetaData.GetPrefetchPathElement("Deliverypointgroup", CommonEntityBase.CreateEntityCollection<DeliverypointgroupEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Terminal' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathTerminalEntity { get { return _staticMetaData.GetPrefetchPathElement("Terminal", CommonEntityBase.CreateEntityCollection<TerminalEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Terminal' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathTerminalEntity1Entity { get { return _staticMetaData.GetPrefetchPathElement("TerminalEntity1", CommonEntityBase.CreateEntityCollection<TerminalEntity>()); } }

		/// <summary>The NetmessageId property of the Entity Netmessage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Netmessage"."NetmessageId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 NetmessageId
		{
			get { return (System.Int32)GetValue((int)NetmessageFieldIndex.NetmessageId, true); }
			set { SetValue((int)NetmessageFieldIndex.NetmessageId, value); }		}

		/// <summary>The Guid property of the Entity Netmessage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Netmessage"."Guid".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Guid
		{
			get { return (System.String)GetValue((int)NetmessageFieldIndex.Guid, true); }
			set	{ SetValue((int)NetmessageFieldIndex.Guid, value); }
		}

		/// <summary>The SenderIdentifier property of the Entity Netmessage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Netmessage"."SenderIdentifier".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String SenderIdentifier
		{
			get { return (System.String)GetValue((int)NetmessageFieldIndex.SenderIdentifier, true); }
			set	{ SetValue((int)NetmessageFieldIndex.SenderIdentifier, value); }
		}

		/// <summary>The ReceiverIdentifier property of the Entity Netmessage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Netmessage"."ReceiverIdentifier".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ReceiverIdentifier
		{
			get { return (System.String)GetValue((int)NetmessageFieldIndex.ReceiverIdentifier, true); }
			set	{ SetValue((int)NetmessageFieldIndex.ReceiverIdentifier, value); }
		}

		/// <summary>The SenderCustomerId property of the Entity Netmessage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Netmessage"."SenderCustomerId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> SenderCustomerId
		{
			get { return (Nullable<System.Int32>)GetValue((int)NetmessageFieldIndex.SenderCustomerId, false); }
			set	{ SetValue((int)NetmessageFieldIndex.SenderCustomerId, value); }
		}

		/// <summary>The SenderClientId property of the Entity Netmessage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Netmessage"."SenderClientId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> SenderClientId
		{
			get { return (Nullable<System.Int32>)GetValue((int)NetmessageFieldIndex.SenderClientId, false); }
			set	{ SetValue((int)NetmessageFieldIndex.SenderClientId, value); }
		}

		/// <summary>The SenderCompanyId property of the Entity Netmessage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Netmessage"."SenderCompanyId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> SenderCompanyId
		{
			get { return (Nullable<System.Int32>)GetValue((int)NetmessageFieldIndex.SenderCompanyId, false); }
			set	{ SetValue((int)NetmessageFieldIndex.SenderCompanyId, value); }
		}

		/// <summary>The SenderDeliverypointId property of the Entity Netmessage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Netmessage"."SenderDeliverypointId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> SenderDeliverypointId
		{
			get { return (Nullable<System.Int32>)GetValue((int)NetmessageFieldIndex.SenderDeliverypointId, false); }
			set	{ SetValue((int)NetmessageFieldIndex.SenderDeliverypointId, value); }
		}

		/// <summary>The SenderTerminalId property of the Entity Netmessage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Netmessage"."SenderTerminalId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> SenderTerminalId
		{
			get { return (Nullable<System.Int32>)GetValue((int)NetmessageFieldIndex.SenderTerminalId, false); }
			set	{ SetValue((int)NetmessageFieldIndex.SenderTerminalId, value); }
		}

		/// <summary>The ReceiverCustomerId property of the Entity Netmessage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Netmessage"."ReceiverCustomerId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ReceiverCustomerId
		{
			get { return (Nullable<System.Int32>)GetValue((int)NetmessageFieldIndex.ReceiverCustomerId, false); }
			set	{ SetValue((int)NetmessageFieldIndex.ReceiverCustomerId, value); }
		}

		/// <summary>The ReceiverClientId property of the Entity Netmessage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Netmessage"."ReceiverClientId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ReceiverClientId
		{
			get { return (Nullable<System.Int32>)GetValue((int)NetmessageFieldIndex.ReceiverClientId, false); }
			set	{ SetValue((int)NetmessageFieldIndex.ReceiverClientId, value); }
		}

		/// <summary>The ReceiverCompanyId property of the Entity Netmessage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Netmessage"."ReceiverCompanyId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ReceiverCompanyId
		{
			get { return (Nullable<System.Int32>)GetValue((int)NetmessageFieldIndex.ReceiverCompanyId, false); }
			set	{ SetValue((int)NetmessageFieldIndex.ReceiverCompanyId, value); }
		}

		/// <summary>The ReceiverDeliverypointId property of the Entity Netmessage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Netmessage"."ReceiverDeliverypointId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ReceiverDeliverypointId
		{
			get { return (Nullable<System.Int32>)GetValue((int)NetmessageFieldIndex.ReceiverDeliverypointId, false); }
			set	{ SetValue((int)NetmessageFieldIndex.ReceiverDeliverypointId, value); }
		}

		/// <summary>The ReceiverTerminalId property of the Entity Netmessage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Netmessage"."ReceiverTerminalId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ReceiverTerminalId
		{
			get { return (Nullable<System.Int32>)GetValue((int)NetmessageFieldIndex.ReceiverTerminalId, false); }
			set	{ SetValue((int)NetmessageFieldIndex.ReceiverTerminalId, value); }
		}

		/// <summary>The MessageType property of the Entity Netmessage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Netmessage"."MessageType".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Crave.Enums.NetmessageType MessageType
		{
			get { return (Crave.Enums.NetmessageType)GetValue((int)NetmessageFieldIndex.MessageType, true); }
			set	{ SetValue((int)NetmessageFieldIndex.MessageType, value); }
		}

		/// <summary>The MessageTypeText property of the Entity Netmessage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Netmessage"."MessageTypeText".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 100.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String MessageTypeText
		{
			get { return (System.String)GetValue((int)NetmessageFieldIndex.MessageTypeText, true); }
			set	{ SetValue((int)NetmessageFieldIndex.MessageTypeText, value); }
		}

		/// <summary>The Status property of the Entity Netmessage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Netmessage"."Status".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Crave.Enums.NetmessageStatus Status
		{
			get { return (Crave.Enums.NetmessageStatus)GetValue((int)NetmessageFieldIndex.Status, true); }
			set	{ SetValue((int)NetmessageFieldIndex.Status, value); }
		}

		/// <summary>The StatusText property of the Entity Netmessage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Netmessage"."StatusText".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 100.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String StatusText
		{
			get { return (System.String)GetValue((int)NetmessageFieldIndex.StatusText, true); }
			set	{ SetValue((int)NetmessageFieldIndex.StatusText, value); }
		}

		/// <summary>The FieldValue1 property of the Entity Netmessage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Netmessage"."FieldValue1".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue1
		{
			get { return (System.String)GetValue((int)NetmessageFieldIndex.FieldValue1, true); }
			set	{ SetValue((int)NetmessageFieldIndex.FieldValue1, value); }
		}

		/// <summary>The FieldValue2 property of the Entity Netmessage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Netmessage"."FieldValue2".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue2
		{
			get { return (System.String)GetValue((int)NetmessageFieldIndex.FieldValue2, true); }
			set	{ SetValue((int)NetmessageFieldIndex.FieldValue2, value); }
		}

		/// <summary>The FieldValue3 property of the Entity Netmessage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Netmessage"."FieldValue3".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue3
		{
			get { return (System.String)GetValue((int)NetmessageFieldIndex.FieldValue3, true); }
			set	{ SetValue((int)NetmessageFieldIndex.FieldValue3, value); }
		}

		/// <summary>The FieldValue4 property of the Entity Netmessage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Netmessage"."FieldValue4".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue4
		{
			get { return (System.String)GetValue((int)NetmessageFieldIndex.FieldValue4, true); }
			set	{ SetValue((int)NetmessageFieldIndex.FieldValue4, value); }
		}

		/// <summary>The FieldValue5 property of the Entity Netmessage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Netmessage"."FieldValue5".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue5
		{
			get { return (System.String)GetValue((int)NetmessageFieldIndex.FieldValue5, true); }
			set	{ SetValue((int)NetmessageFieldIndex.FieldValue5, value); }
		}

		/// <summary>The FieldValue6 property of the Entity Netmessage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Netmessage"."FieldValue6".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue6
		{
			get { return (System.String)GetValue((int)NetmessageFieldIndex.FieldValue6, true); }
			set	{ SetValue((int)NetmessageFieldIndex.FieldValue6, value); }
		}

		/// <summary>The FieldValue7 property of the Entity Netmessage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Netmessage"."FieldValue7".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue7
		{
			get { return (System.String)GetValue((int)NetmessageFieldIndex.FieldValue7, true); }
			set	{ SetValue((int)NetmessageFieldIndex.FieldValue7, value); }
		}

		/// <summary>The FieldValue8 property of the Entity Netmessage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Netmessage"."FieldValue8".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue8
		{
			get { return (System.String)GetValue((int)NetmessageFieldIndex.FieldValue8, true); }
			set	{ SetValue((int)NetmessageFieldIndex.FieldValue8, value); }
		}

		/// <summary>The FieldValue9 property of the Entity Netmessage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Netmessage"."FieldValue9".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue9
		{
			get { return (System.String)GetValue((int)NetmessageFieldIndex.FieldValue9, true); }
			set	{ SetValue((int)NetmessageFieldIndex.FieldValue9, value); }
		}

		/// <summary>The FieldValue10 property of the Entity Netmessage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Netmessage"."FieldValue10".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue10
		{
			get { return (System.String)GetValue((int)NetmessageFieldIndex.FieldValue10, true); }
			set	{ SetValue((int)NetmessageFieldIndex.FieldValue10, value); }
		}

		/// <summary>The CreatedBy property of the Entity Netmessage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Netmessage"."CreatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)NetmessageFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)NetmessageFieldIndex.CreatedBy, value); }
		}

		/// <summary>The UpdatedBy property of the Entity Netmessage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Netmessage"."UpdatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)NetmessageFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)NetmessageFieldIndex.UpdatedBy, value); }
		}

		/// <summary>The ReceiverDeliverypointgroupId property of the Entity Netmessage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Netmessage"."ReceiverDeliverypointgroupId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ReceiverDeliverypointgroupId
		{
			get { return (Nullable<System.Int32>)GetValue((int)NetmessageFieldIndex.ReceiverDeliverypointgroupId, false); }
			set	{ SetValue((int)NetmessageFieldIndex.ReceiverDeliverypointgroupId, value); }
		}

		/// <summary>The ErrorMessage property of the Entity Netmessage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Netmessage"."ErrorMessage".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ErrorMessage
		{
			get { return (System.String)GetValue((int)NetmessageFieldIndex.ErrorMessage, true); }
			set	{ SetValue((int)NetmessageFieldIndex.ErrorMessage, value); }
		}

		/// <summary>The MessageLog property of the Entity Netmessage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Netmessage"."MessageLog".<br/>Table field type characteristics (type, precision, scale, length): Text, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String MessageLog
		{
			get { return (System.String)GetValue((int)NetmessageFieldIndex.MessageLog, true); }
			set	{ SetValue((int)NetmessageFieldIndex.MessageLog, value); }
		}

		/// <summary>The CreatedUTC property of the Entity Netmessage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Netmessage"."CreatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)NetmessageFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)NetmessageFieldIndex.CreatedUTC, value); }
		}

		/// <summary>The UpdatedUTC property of the Entity Netmessage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Netmessage"."UpdatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)NetmessageFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)NetmessageFieldIndex.UpdatedUTC, value); }
		}

		/// <summary>Gets / sets related entity of type 'ClientEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual ClientEntity Client
		{
			get { return _client; }
			set { SetSingleRelatedEntityNavigator(value, "Client"); }
		}

		/// <summary>Gets / sets related entity of type 'ClientEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual ClientEntity ClientEntity1
		{
			get { return _clientEntity1; }
			set { SetSingleRelatedEntityNavigator(value, "ClientEntity1"); }
		}

		/// <summary>Gets / sets related entity of type 'CompanyEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual CompanyEntity Company
		{
			get { return _company; }
			set { SetSingleRelatedEntityNavigator(value, "Company"); }
		}

		/// <summary>Gets / sets related entity of type 'CompanyEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual CompanyEntity CompanyEntity1
		{
			get { return _companyEntity1; }
			set { SetSingleRelatedEntityNavigator(value, "CompanyEntity1"); }
		}

		/// <summary>Gets / sets related entity of type 'CustomerEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual CustomerEntity Customer
		{
			get { return _customer; }
			set { SetSingleRelatedEntityNavigator(value, "Customer"); }
		}

		/// <summary>Gets / sets related entity of type 'CustomerEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual CustomerEntity CustomerEntity1
		{
			get { return _customerEntity1; }
			set { SetSingleRelatedEntityNavigator(value, "CustomerEntity1"); }
		}

		/// <summary>Gets / sets related entity of type 'DeliverypointEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual DeliverypointEntity Deliverypoint
		{
			get { return _deliverypoint; }
			set { SetSingleRelatedEntityNavigator(value, "Deliverypoint"); }
		}

		/// <summary>Gets / sets related entity of type 'DeliverypointEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual DeliverypointEntity DeliverypointEntity1
		{
			get { return _deliverypointEntity1; }
			set { SetSingleRelatedEntityNavigator(value, "DeliverypointEntity1"); }
		}

		/// <summary>Gets / sets related entity of type 'DeliverypointgroupEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual DeliverypointgroupEntity Deliverypointgroup
		{
			get { return _deliverypointgroup; }
			set { SetSingleRelatedEntityNavigator(value, "Deliverypointgroup"); }
		}

		/// <summary>Gets / sets related entity of type 'TerminalEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual TerminalEntity Terminal
		{
			get { return _terminal; }
			set { SetSingleRelatedEntityNavigator(value, "Terminal"); }
		}

		/// <summary>Gets / sets related entity of type 'TerminalEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual TerminalEntity TerminalEntity1
		{
			get { return _terminalEntity1; }
			set { SetSingleRelatedEntityNavigator(value, "TerminalEntity1"); }
		}


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen
{
	public enum NetmessageFieldIndex
	{
		///<summary>NetmessageId. </summary>
		NetmessageId,
		///<summary>Guid. </summary>
		Guid,
		///<summary>SenderIdentifier. </summary>
		SenderIdentifier,
		///<summary>ReceiverIdentifier. </summary>
		ReceiverIdentifier,
		///<summary>SenderCustomerId. </summary>
		SenderCustomerId,
		///<summary>SenderClientId. </summary>
		SenderClientId,
		///<summary>SenderCompanyId. </summary>
		SenderCompanyId,
		///<summary>SenderDeliverypointId. </summary>
		SenderDeliverypointId,
		///<summary>SenderTerminalId. </summary>
		SenderTerminalId,
		///<summary>ReceiverCustomerId. </summary>
		ReceiverCustomerId,
		///<summary>ReceiverClientId. </summary>
		ReceiverClientId,
		///<summary>ReceiverCompanyId. </summary>
		ReceiverCompanyId,
		///<summary>ReceiverDeliverypointId. </summary>
		ReceiverDeliverypointId,
		///<summary>ReceiverTerminalId. </summary>
		ReceiverTerminalId,
		///<summary>MessageType. </summary>
		MessageType,
		///<summary>MessageTypeText. </summary>
		MessageTypeText,
		///<summary>Status. </summary>
		Status,
		///<summary>StatusText. </summary>
		StatusText,
		///<summary>FieldValue1. </summary>
		FieldValue1,
		///<summary>FieldValue2. </summary>
		FieldValue2,
		///<summary>FieldValue3. </summary>
		FieldValue3,
		///<summary>FieldValue4. </summary>
		FieldValue4,
		///<summary>FieldValue5. </summary>
		FieldValue5,
		///<summary>FieldValue6. </summary>
		FieldValue6,
		///<summary>FieldValue7. </summary>
		FieldValue7,
		///<summary>FieldValue8. </summary>
		FieldValue8,
		///<summary>FieldValue9. </summary>
		FieldValue9,
		///<summary>FieldValue10. </summary>
		FieldValue10,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>ReceiverDeliverypointgroupId. </summary>
		ReceiverDeliverypointgroupId,
		///<summary>ErrorMessage. </summary>
		ErrorMessage,
		///<summary>MessageLog. </summary>
		MessageLog,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Netmessage. </summary>
	public partial class NetmessageRelations: RelationFactory
	{
		#region Custom Relation code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomRelationCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		


		/// <summary>Gets all relations and delete rules of the NetmessageEntity as a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects.</summary>
		/// <returns>a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects</returns>
		public virtual Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> GetRelationsAndDeleteRules()
		{
			Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> toReturn = new Dictionary<IEntityRelation, ReferentialConstraintDeleteRule>();
			return toReturn;
		}		
	
	

		/// <summary>Returns a new IEntityRelation object, between NetmessageEntity and ClientEntity over the m:1 relation they have, using the relation between the fields: Netmessage.SenderClientId - Client.ClientId</summary>
		public virtual IEntityRelation ClientEntityUsingSenderClientId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Client", false, new[] { ClientFields.ClientId, NetmessageFields.SenderClientId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between NetmessageEntity and ClientEntity over the m:1 relation they have, using the relation between the fields: Netmessage.ReceiverClientId - Client.ClientId</summary>
		public virtual IEntityRelation ClientEntityUsingReceiverClientId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "ClientEntity1", false, new[] { ClientFields.ClientId, NetmessageFields.ReceiverClientId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between NetmessageEntity and CompanyEntity over the m:1 relation they have, using the relation between the fields: Netmessage.SenderCompanyId - Company.CompanyId</summary>
		public virtual IEntityRelation CompanyEntityUsingSenderCompanyId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Company", false, new[] { CompanyFields.CompanyId, NetmessageFields.SenderCompanyId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between NetmessageEntity and CompanyEntity over the m:1 relation they have, using the relation between the fields: Netmessage.ReceiverCompanyId - Company.CompanyId</summary>
		public virtual IEntityRelation CompanyEntityUsingReceiverCompanyId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "CompanyEntity1", false, new[] { CompanyFields.CompanyId, NetmessageFields.ReceiverCompanyId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between NetmessageEntity and CustomerEntity over the m:1 relation they have, using the relation between the fields: Netmessage.SenderCustomerId - Customer.CustomerId</summary>
		public virtual IEntityRelation CustomerEntityUsingSenderCustomerId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Customer", false, new[] { CustomerFields.CustomerId, NetmessageFields.SenderCustomerId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between NetmessageEntity and CustomerEntity over the m:1 relation they have, using the relation between the fields: Netmessage.ReceiverCustomerId - Customer.CustomerId</summary>
		public virtual IEntityRelation CustomerEntityUsingReceiverCustomerId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "CustomerEntity1", false, new[] { CustomerFields.CustomerId, NetmessageFields.ReceiverCustomerId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between NetmessageEntity and DeliverypointEntity over the m:1 relation they have, using the relation between the fields: Netmessage.SenderDeliverypointId - Deliverypoint.DeliverypointId</summary>
		public virtual IEntityRelation DeliverypointEntityUsingSenderDeliverypointId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Deliverypoint", false, new[] { DeliverypointFields.DeliverypointId, NetmessageFields.SenderDeliverypointId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between NetmessageEntity and DeliverypointEntity over the m:1 relation they have, using the relation between the fields: Netmessage.ReceiverDeliverypointId - Deliverypoint.DeliverypointId</summary>
		public virtual IEntityRelation DeliverypointEntityUsingReceiverDeliverypointId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "DeliverypointEntity1", false, new[] { DeliverypointFields.DeliverypointId, NetmessageFields.ReceiverDeliverypointId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between NetmessageEntity and DeliverypointgroupEntity over the m:1 relation they have, using the relation between the fields: Netmessage.ReceiverDeliverypointgroupId - Deliverypointgroup.DeliverypointgroupId</summary>
		public virtual IEntityRelation DeliverypointgroupEntityUsingReceiverDeliverypointgroupId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Deliverypointgroup", false, new[] { DeliverypointgroupFields.DeliverypointgroupId, NetmessageFields.ReceiverDeliverypointgroupId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between NetmessageEntity and TerminalEntity over the m:1 relation they have, using the relation between the fields: Netmessage.SenderTerminalId - Terminal.TerminalId</summary>
		public virtual IEntityRelation TerminalEntityUsingSenderTerminalId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Terminal", false, new[] { TerminalFields.TerminalId, NetmessageFields.SenderTerminalId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between NetmessageEntity and TerminalEntity over the m:1 relation they have, using the relation between the fields: Netmessage.ReceiverTerminalId - Terminal.TerminalId</summary>
		public virtual IEntityRelation TerminalEntityUsingReceiverTerminalId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "TerminalEntity1", false, new[] { TerminalFields.TerminalId, NetmessageFields.ReceiverTerminalId }); }
		}

	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticNetmessageRelations
	{
		internal static readonly IEntityRelation ClientEntityUsingSenderClientIdStatic = new NetmessageRelations().ClientEntityUsingSenderClientId;
		internal static readonly IEntityRelation ClientEntityUsingReceiverClientIdStatic = new NetmessageRelations().ClientEntityUsingReceiverClientId;
		internal static readonly IEntityRelation CompanyEntityUsingSenderCompanyIdStatic = new NetmessageRelations().CompanyEntityUsingSenderCompanyId;
		internal static readonly IEntityRelation CompanyEntityUsingReceiverCompanyIdStatic = new NetmessageRelations().CompanyEntityUsingReceiverCompanyId;
		internal static readonly IEntityRelation CustomerEntityUsingSenderCustomerIdStatic = new NetmessageRelations().CustomerEntityUsingSenderCustomerId;
		internal static readonly IEntityRelation CustomerEntityUsingReceiverCustomerIdStatic = new NetmessageRelations().CustomerEntityUsingReceiverCustomerId;
		internal static readonly IEntityRelation DeliverypointEntityUsingSenderDeliverypointIdStatic = new NetmessageRelations().DeliverypointEntityUsingSenderDeliverypointId;
		internal static readonly IEntityRelation DeliverypointEntityUsingReceiverDeliverypointIdStatic = new NetmessageRelations().DeliverypointEntityUsingReceiverDeliverypointId;
		internal static readonly IEntityRelation DeliverypointgroupEntityUsingReceiverDeliverypointgroupIdStatic = new NetmessageRelations().DeliverypointgroupEntityUsingReceiverDeliverypointgroupId;
		internal static readonly IEntityRelation TerminalEntityUsingSenderTerminalIdStatic = new NetmessageRelations().TerminalEntityUsingSenderTerminalId;
		internal static readonly IEntityRelation TerminalEntityUsingReceiverTerminalIdStatic = new NetmessageRelations().TerminalEntityUsingReceiverTerminalId;

		/// <summary>CTor</summary>
		static StaticNetmessageRelations() { }
	}
}

