﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 5.8
// Code is generated on: 
// Code is generated using templates: Crave.TemplateBindings
// Templates vendor: Crave Interactive
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.FactoryClasses;
using Crave.Ordering.Shared.Data.LLBLGen.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

// ReSharper disable All
namespace Crave.Ordering.Shared.Data.LLBLGen.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'Device'.<br/><br/></summary>
	[Serializable]
	public partial class DeviceEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END
	
	{
		private EntityCollection<ClientEntity> _clientCollection;
		private EntityCollection<DeliverypointEntity> _deliverypointCollection;
		private EntityCollection<TerminalEntity> _terminalCollection;
		private EntityCollection<ClientConfigurationEntity> _clientConfigurationCollectionViaDeliverypoint;
		private CustomerEntity _customer;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		private static DeviceEntityStaticMetaData _staticMetaData = new DeviceEntityStaticMetaData();
		private static DeviceRelations _relationsFactory = new DeviceRelations();		

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Customer</summary>
			public static readonly string Customer = "Customer";
			/// <summary>Member name ClientCollection</summary>
			public static readonly string ClientCollection = "ClientCollection";
			/// <summary>Member name DeliverypointCollection</summary>
			public static readonly string DeliverypointCollection = "DeliverypointCollection";
			/// <summary>Member name TerminalCollection</summary>
			public static readonly string TerminalCollection = "TerminalCollection";
			/// <summary>Member name ClientConfigurationCollectionViaDeliverypoint</summary>
			public static readonly string ClientConfigurationCollectionViaDeliverypoint = "ClientConfigurationCollectionViaDeliverypoint";
		}

		/// <summary>Static meta-data storage for navigator related information</summary>
		protected class DeviceEntityStaticMetaData : EntityStaticMetaDataBase
		{
			public DeviceEntityStaticMetaData()
			{
				SetEntityCoreInfo("DeviceEntity", InheritanceHierarchyType.None, false, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.DeviceEntity, typeof(DeviceEntity), typeof(DeviceEntityFactory), false);
				AddNavigatorMetaData<DeviceEntity, EntityCollection<ClientEntity>>("ClientCollection", a => a._clientCollection, (a, b) => a._clientCollection = b, a => a.ClientCollection, () => new DeviceRelations().ClientEntityUsingDeviceId, typeof(ClientEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ClientEntity);
				AddNavigatorMetaData<DeviceEntity, EntityCollection<DeliverypointEntity>>("DeliverypointCollection", a => a._deliverypointCollection, (a, b) => a._deliverypointCollection = b, a => a.DeliverypointCollection, () => new DeviceRelations().DeliverypointEntityUsingDeviceId, typeof(DeliverypointEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.DeliverypointEntity);
				AddNavigatorMetaData<DeviceEntity, EntityCollection<TerminalEntity>>("TerminalCollection", a => a._terminalCollection, (a, b) => a._terminalCollection = b, a => a.TerminalCollection, () => new DeviceRelations().TerminalEntityUsingDeviceId, typeof(TerminalEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.TerminalEntity);
				AddNavigatorMetaData<DeviceEntity, CustomerEntity>("Customer", "DeviceCollection", (a, b) => a._customer = b, a => a._customer, (a, b) => a.Customer = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticDeviceRelations.CustomerEntityUsingCustomerIdStatic, ()=>new DeviceRelations().CustomerEntityUsingCustomerId, null, new int[] { (int)DeviceFieldIndex.CustomerId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.CustomerEntity);
				AddNavigatorMetaData<DeviceEntity, EntityCollection<ClientConfigurationEntity>>("ClientConfigurationCollectionViaDeliverypoint", a => a._clientConfigurationCollectionViaDeliverypoint, (a, b) => a._clientConfigurationCollectionViaDeliverypoint = b, a => a.ClientConfigurationCollectionViaDeliverypoint, () => new DeviceRelations().DeliverypointEntityUsingDeviceId, () => new DeliverypointRelations().ClientConfigurationEntityUsingClientConfigurationId, "DeviceEntity__", "Deliverypoint_", typeof(ClientConfigurationEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ClientConfigurationEntity);
			}
		}

		/// <summary>Static ctor</summary>
		static DeviceEntity()
		{
		}

		/// <summary> CTor</summary>
		public DeviceEntity()
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public DeviceEntity(IEntityFields2 fields)
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this DeviceEntity</param>
		public DeviceEntity(IValidator validator)
		{
			InitClassEmpty(validator, null);
		}

		/// <summary> CTor</summary>
		/// <param name="deviceId">PK value for Device which data should be fetched into this Device object</param>
		public DeviceEntity(System.Int32 deviceId) : this(deviceId, null)
		{
		}

		/// <summary> CTor</summary>
		/// <param name="deviceId">PK value for Device which data should be fetched into this Device object</param>
		/// <param name="validator">The custom validator object for this DeviceEntity</param>
		public DeviceEntity(System.Int32 deviceId, IValidator validator)
		{
			InitClassEmpty(validator, null);
			this.DeviceId = deviceId;
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected DeviceEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary>Method which will construct a filter (predicate expression) for the unique constraint defined on the fields: Identifier .</summary>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public IPredicateExpression ConstructFilterForUCIdentifier()
		{
			var filter = new PredicateExpression();
			filter.Add(Crave.Ordering.Shared.Data.LLBLGen.HelperClasses.DeviceFields.Identifier == this.Fields.GetCurrentValue((int)DeviceFieldIndex.Identifier));
 			return filter;
		}

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Client' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoClientCollection() { return CreateRelationInfoForNavigator("ClientCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Deliverypoint' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoDeliverypointCollection() { return CreateRelationInfoForNavigator("DeliverypointCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Terminal' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoTerminalCollection() { return CreateRelationInfoForNavigator("TerminalCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'ClientConfiguration' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoClientConfigurationCollectionViaDeliverypoint() { return CreateRelationInfoForNavigator("ClientConfigurationCollectionViaDeliverypoint"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Customer' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoCustomer() { return CreateRelationInfoForNavigator("Customer"); }
		
		/// <inheritdoc/>
		protected override EntityStaticMetaDataBase GetEntityStaticMetaData() {	return _staticMetaData; }

		/// <summary>Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		/// <summary>Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this DeviceEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary>The relations object holding all relations of this entity with other entity classes.</summary>
		public static DeviceRelations Relations { get { return _relationsFactory; } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Client' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathClientCollection { get { return _staticMetaData.GetPrefetchPathElement("ClientCollection", CommonEntityBase.CreateEntityCollection<ClientEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Deliverypoint' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathDeliverypointCollection { get { return _staticMetaData.GetPrefetchPathElement("DeliverypointCollection", CommonEntityBase.CreateEntityCollection<DeliverypointEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Terminal' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathTerminalCollection { get { return _staticMetaData.GetPrefetchPathElement("TerminalCollection", CommonEntityBase.CreateEntityCollection<TerminalEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'ClientConfiguration' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathClientConfigurationCollectionViaDeliverypoint { get { return _staticMetaData.GetPrefetchPathElement("ClientConfigurationCollectionViaDeliverypoint", CommonEntityBase.CreateEntityCollection<ClientConfigurationEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Customer' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathCustomerEntity { get { return _staticMetaData.GetPrefetchPathElement("Customer", CommonEntityBase.CreateEntityCollection<CustomerEntity>()); } }

		/// <summary>The DeviceId property of the Entity Device<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Device"."DeviceId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 DeviceId
		{
			get { return (System.Int32)GetValue((int)DeviceFieldIndex.DeviceId, true); }
			set { SetValue((int)DeviceFieldIndex.DeviceId, value); }		}

		/// <summary>The Identifier property of the Entity Device<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Device"."Identifier".<br/>Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 255.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Identifier
		{
			get { return (System.String)GetValue((int)DeviceFieldIndex.Identifier, true); }
			set	{ SetValue((int)DeviceFieldIndex.Identifier, value); }
		}

		/// <summary>The Type property of the Entity Device<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Device"."Type".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 Type
		{
			get { return (System.Int32)GetValue((int)DeviceFieldIndex.Type, true); }
			set	{ SetValue((int)DeviceFieldIndex.Type, value); }
		}

		/// <summary>The Notes property of the Entity Device<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Device"."Notes".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Notes
		{
			get { return (System.String)GetValue((int)DeviceFieldIndex.Notes, true); }
			set	{ SetValue((int)DeviceFieldIndex.Notes, value); }
		}

		/// <summary>The UpdateStatus property of the Entity Device<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Device"."UpdateStatus".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Crave.Enums.UpdateStatus UpdateStatus
		{
			get { return (Crave.Enums.UpdateStatus)GetValue((int)DeviceFieldIndex.UpdateStatus, true); }
			set	{ SetValue((int)DeviceFieldIndex.UpdateStatus, value); }
		}

		/// <summary>The CustomerId property of the Entity Device<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Device"."CustomerId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CustomerId
		{
			get { return (Nullable<System.Int32>)GetValue((int)DeviceFieldIndex.CustomerId, false); }
			set	{ SetValue((int)DeviceFieldIndex.CustomerId, value); }
		}

		/// <summary>The DevicePlatform property of the Entity Device<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Device"."DevicePlatform".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 DevicePlatform
		{
			get { return (System.Int32)GetValue((int)DeviceFieldIndex.DevicePlatform, true); }
			set	{ SetValue((int)DeviceFieldIndex.DevicePlatform, value); }
		}

		/// <summary>The Model property of the Entity Device<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Device"."DeviceModel".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> Model
		{
			get { return (Nullable<System.Int32>)GetValue((int)DeviceFieldIndex.Model, false); }
			set	{ SetValue((int)DeviceFieldIndex.Model, value); }
		}

		/// <summary>The IsDevelopment property of the Entity Device<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Device"."IsDevelopment".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsDevelopment
		{
			get { return (System.Boolean)GetValue((int)DeviceFieldIndex.IsDevelopment, true); }
			set	{ SetValue((int)DeviceFieldIndex.IsDevelopment, value); }
		}

		/// <summary>The LastRequestUTC property of the Entity Device<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Device"."LastRequestUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> LastRequestUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)DeviceFieldIndex.LastRequestUTC, false); }
			set	{ SetValue((int)DeviceFieldIndex.LastRequestUTC, value); }
		}

		/// <summary>The LastRequestNotifiedBySmsUTC property of the Entity Device<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Device"."LastRequestNotifiedBySmsUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> LastRequestNotifiedBySmsUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)DeviceFieldIndex.LastRequestNotifiedBySmsUTC, false); }
			set	{ SetValue((int)DeviceFieldIndex.LastRequestNotifiedBySmsUTC, value); }
		}

		/// <summary>The LastSupportToolsRequestUTC property of the Entity Device<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Device"."LastSupportToolsRequestUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> LastSupportToolsRequestUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)DeviceFieldIndex.LastSupportToolsRequestUTC, false); }
			set	{ SetValue((int)DeviceFieldIndex.LastSupportToolsRequestUTC, value); }
		}

		/// <summary>The IsCharging property of the Entity Device<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Device"."IsCharging".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsCharging
		{
			get { return (System.Boolean)GetValue((int)DeviceFieldIndex.IsCharging, true); }
			set	{ SetValue((int)DeviceFieldIndex.IsCharging, value); }
		}

		/// <summary>The BatteryLevel property of the Entity Device<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Device"."BatteryLevel".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> BatteryLevel
		{
			get { return (Nullable<System.Int32>)GetValue((int)DeviceFieldIndex.BatteryLevel, false); }
			set	{ SetValue((int)DeviceFieldIndex.BatteryLevel, value); }
		}

		/// <summary>The PublicIpAddress property of the Entity Device<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Device"."PublicIpAddress".<br/>Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 255.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PublicIpAddress
		{
			get { return (System.String)GetValue((int)DeviceFieldIndex.PublicIpAddress, true); }
			set	{ SetValue((int)DeviceFieldIndex.PublicIpAddress, value); }
		}

		/// <summary>The PrivateIpAddresses property of the Entity Device<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Device"."PrivateIpAddresses".<br/>Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 255.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PrivateIpAddresses
		{
			get { return (System.String)GetValue((int)DeviceFieldIndex.PrivateIpAddresses, true); }
			set	{ SetValue((int)DeviceFieldIndex.PrivateIpAddresses, value); }
		}

		/// <summary>The ApplicationVersion property of the Entity Device<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Device"."ApplicationVersion".<br/>Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 50.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ApplicationVersion
		{
			get { return (System.String)GetValue((int)DeviceFieldIndex.ApplicationVersion, true); }
			set	{ SetValue((int)DeviceFieldIndex.ApplicationVersion, value); }
		}

		/// <summary>The AgentRunning property of the Entity Device<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Device"."AgentRunning".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean AgentRunning
		{
			get { return (System.Boolean)GetValue((int)DeviceFieldIndex.AgentRunning, true); }
			set	{ SetValue((int)DeviceFieldIndex.AgentRunning, value); }
		}

		/// <summary>The AgentVersion property of the Entity Device<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Device"."AgentVersion".<br/>Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 50.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String AgentVersion
		{
			get { return (System.String)GetValue((int)DeviceFieldIndex.AgentVersion, true); }
			set	{ SetValue((int)DeviceFieldIndex.AgentVersion, value); }
		}

		/// <summary>The SupportToolsRunning property of the Entity Device<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Device"."SupportToolsRunning".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean SupportToolsRunning
		{
			get { return (System.Boolean)GetValue((int)DeviceFieldIndex.SupportToolsRunning, true); }
			set	{ SetValue((int)DeviceFieldIndex.SupportToolsRunning, value); }
		}

		/// <summary>The SupportToolsVersion property of the Entity Device<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Device"."SupportToolsVersion".<br/>Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 50.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String SupportToolsVersion
		{
			get { return (System.String)GetValue((int)DeviceFieldIndex.SupportToolsVersion, true); }
			set	{ SetValue((int)DeviceFieldIndex.SupportToolsVersion, value); }
		}

		/// <summary>The OsVersion property of the Entity Device<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Device"."OsVersion".<br/>Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 50.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String OsVersion
		{
			get { return (System.String)GetValue((int)DeviceFieldIndex.OsVersion, true); }
			set	{ SetValue((int)DeviceFieldIndex.OsVersion, value); }
		}

		/// <summary>The WifiStrength property of the Entity Device<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Device"."WifiStrength".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> WifiStrength
		{
			get { return (Nullable<System.Int32>)GetValue((int)DeviceFieldIndex.WifiStrength, false); }
			set	{ SetValue((int)DeviceFieldIndex.WifiStrength, value); }
		}

		/// <summary>The CloudEnvironment property of the Entity Device<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Device"."CloudEnvironment".<br/>Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 50.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CloudEnvironment
		{
			get { return (System.String)GetValue((int)DeviceFieldIndex.CloudEnvironment, true); }
			set	{ SetValue((int)DeviceFieldIndex.CloudEnvironment, value); }
		}

		/// <summary>The CommunicationMethod property of the Entity Device<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Device"."CommunicationMethod".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Crave.Enums.ClientCommunicationMethod CommunicationMethod
		{
			get { return (Crave.Enums.ClientCommunicationMethod)GetValue((int)DeviceFieldIndex.CommunicationMethod, true); }
			set	{ SetValue((int)DeviceFieldIndex.CommunicationMethod, value); }
		}

		/// <summary>The LastUserInteraction property of the Entity Device<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Device"."LastUserInteraction".<br/>Table field type characteristics (type, precision, scale, length): BigInt, 19, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> LastUserInteraction
		{
			get { return (Nullable<System.Int64>)GetValue((int)DeviceFieldIndex.LastUserInteraction, false); }
			set	{ SetValue((int)DeviceFieldIndex.LastUserInteraction, value); }
		}

		/// <summary>The DeviceInfoUpdatedUTC property of the Entity Device<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Device"."DeviceInfoUpdatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> DeviceInfoUpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)DeviceFieldIndex.DeviceInfoUpdatedUTC, false); }
			set	{ SetValue((int)DeviceFieldIndex.DeviceInfoUpdatedUTC, value); }
		}

		/// <summary>The UpdateEmenuDownloaded property of the Entity Device<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Device"."UpdateEmenuDownloaded".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean UpdateEmenuDownloaded
		{
			get { return (System.Boolean)GetValue((int)DeviceFieldIndex.UpdateEmenuDownloaded, true); }
			set	{ SetValue((int)DeviceFieldIndex.UpdateEmenuDownloaded, value); }
		}

		/// <summary>The UpdateConsoleDownloaded property of the Entity Device<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Device"."UpdateConsoleDownloaded".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean UpdateConsoleDownloaded
		{
			get { return (System.Boolean)GetValue((int)DeviceFieldIndex.UpdateConsoleDownloaded, true); }
			set	{ SetValue((int)DeviceFieldIndex.UpdateConsoleDownloaded, value); }
		}

		/// <summary>The UpdateAgentDownloaded property of the Entity Device<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Device"."UpdateAgentDownloaded".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean UpdateAgentDownloaded
		{
			get { return (System.Boolean)GetValue((int)DeviceFieldIndex.UpdateAgentDownloaded, true); }
			set	{ SetValue((int)DeviceFieldIndex.UpdateAgentDownloaded, value); }
		}

		/// <summary>The UpdateSupportToolsDownloaded property of the Entity Device<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Device"."UpdateSupportToolsDownloaded".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean UpdateSupportToolsDownloaded
		{
			get { return (System.Boolean)GetValue((int)DeviceFieldIndex.UpdateSupportToolsDownloaded, true); }
			set	{ SetValue((int)DeviceFieldIndex.UpdateSupportToolsDownloaded, value); }
		}

		/// <summary>The UpdateOSDownloaded property of the Entity Device<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Device"."UpdateOSDownloaded".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean UpdateOSDownloaded
		{
			get { return (System.Boolean)GetValue((int)DeviceFieldIndex.UpdateOSDownloaded, true); }
			set	{ SetValue((int)DeviceFieldIndex.UpdateOSDownloaded, value); }
		}

		/// <summary>The CreatedBy property of the Entity Device<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Device"."CreatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)DeviceFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)DeviceFieldIndex.CreatedBy, value); }
		}

		/// <summary>The UpdatedBy property of the Entity Device<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Device"."UpdatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)DeviceFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)DeviceFieldIndex.UpdatedBy, value); }
		}

		/// <summary>The CreatedUTC property of the Entity Device<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Device"."CreatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)DeviceFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)DeviceFieldIndex.CreatedUTC, value); }
		}

		/// <summary>The UpdatedUTC property of the Entity Device<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Device"."UpdatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)DeviceFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)DeviceFieldIndex.UpdatedUTC, value); }
		}

		/// <summary>The UpdateStatusChangedUTC property of the Entity Device<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Device"."UpdateStatusChangedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime UpdateStatusChangedUTC
		{
			get { return (System.DateTime)GetValue((int)DeviceFieldIndex.UpdateStatusChangedUTC, true); }
			set	{ SetValue((int)DeviceFieldIndex.UpdateStatusChangedUTC, value); }
		}

		/// <summary>The Token property of the Entity Device<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Device"."Token".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Token
		{
			get { return (System.String)GetValue((int)DeviceFieldIndex.Token, true); }
			set	{ SetValue((int)DeviceFieldIndex.Token, value); }
		}

		/// <summary>The SandboxMode property of the Entity Device<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Device"."SandboxMode".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean SandboxMode
		{
			get { return (System.Boolean)GetValue((int)DeviceFieldIndex.SandboxMode, true); }
			set	{ SetValue((int)DeviceFieldIndex.SandboxMode, value); }
		}

		/// <summary>The MessagingServiceRunning property of the Entity Device<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Device"."MessagingServiceRunning".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean MessagingServiceRunning
		{
			get { return (System.Boolean)GetValue((int)DeviceFieldIndex.MessagingServiceRunning, true); }
			set	{ SetValue((int)DeviceFieldIndex.MessagingServiceRunning, value); }
		}

		/// <summary>The MessagingServiceVersion property of the Entity Device<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Device"."MessagingServiceVersion".<br/>Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 50.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String MessagingServiceVersion
		{
			get { return (System.String)GetValue((int)DeviceFieldIndex.MessagingServiceVersion, true); }
			set	{ SetValue((int)DeviceFieldIndex.MessagingServiceVersion, value); }
		}

		/// <summary>The LastMessagingServiceRequest property of the Entity Device<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Device"."LastMessagingServiceRequest".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> LastMessagingServiceRequest
		{
			get { return (Nullable<System.DateTime>)GetValue((int)DeviceFieldIndex.LastMessagingServiceRequest, false); }
			set	{ SetValue((int)DeviceFieldIndex.LastMessagingServiceRequest, value); }
		}

		/// <summary>Gets the EntityCollection with the related entities of type 'ClientEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(ClientEntity))]
		public virtual EntityCollection<ClientEntity> ClientCollection { get { return GetOrCreateEntityCollection<ClientEntity, ClientEntityFactory>("Device", true, false, ref _clientCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'DeliverypointEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(DeliverypointEntity))]
		public virtual EntityCollection<DeliverypointEntity> DeliverypointCollection { get { return GetOrCreateEntityCollection<DeliverypointEntity, DeliverypointEntityFactory>("Device", true, false, ref _deliverypointCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'TerminalEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(TerminalEntity))]
		public virtual EntityCollection<TerminalEntity> TerminalCollection { get { return GetOrCreateEntityCollection<TerminalEntity, TerminalEntityFactory>("Device", true, false, ref _terminalCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'ClientConfigurationEntity' which are related to this entity via a relation of type 'm:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(ClientConfigurationEntity))]
		public virtual EntityCollection<ClientConfigurationEntity> ClientConfigurationCollectionViaDeliverypoint { get { return GetOrCreateEntityCollection<ClientConfigurationEntity, ClientConfigurationEntityFactory>("", false, true, ref _clientConfigurationCollectionViaDeliverypoint); } }

		/// <summary>Gets / sets related entity of type 'CustomerEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual CustomerEntity Customer
		{
			get { return _customer; }
			set { SetSingleRelatedEntityNavigator(value, "Customer"); }
		}


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen
{
	public enum DeviceFieldIndex
	{
		///<summary>DeviceId. </summary>
		DeviceId,
		///<summary>Identifier. </summary>
		Identifier,
		///<summary>Type. </summary>
		Type,
		///<summary>Notes. </summary>
		Notes,
		///<summary>UpdateStatus. </summary>
		UpdateStatus,
		///<summary>CustomerId. </summary>
		CustomerId,
		///<summary>DevicePlatform. </summary>
		DevicePlatform,
		///<summary>Model. </summary>
		Model,
		///<summary>IsDevelopment. </summary>
		IsDevelopment,
		///<summary>LastRequestUTC. </summary>
		LastRequestUTC,
		///<summary>LastRequestNotifiedBySmsUTC. </summary>
		LastRequestNotifiedBySmsUTC,
		///<summary>LastSupportToolsRequestUTC. </summary>
		LastSupportToolsRequestUTC,
		///<summary>IsCharging. </summary>
		IsCharging,
		///<summary>BatteryLevel. </summary>
		BatteryLevel,
		///<summary>PublicIpAddress. </summary>
		PublicIpAddress,
		///<summary>PrivateIpAddresses. </summary>
		PrivateIpAddresses,
		///<summary>ApplicationVersion. </summary>
		ApplicationVersion,
		///<summary>AgentRunning. </summary>
		AgentRunning,
		///<summary>AgentVersion. </summary>
		AgentVersion,
		///<summary>SupportToolsRunning. </summary>
		SupportToolsRunning,
		///<summary>SupportToolsVersion. </summary>
		SupportToolsVersion,
		///<summary>OsVersion. </summary>
		OsVersion,
		///<summary>WifiStrength. </summary>
		WifiStrength,
		///<summary>CloudEnvironment. </summary>
		CloudEnvironment,
		///<summary>CommunicationMethod. </summary>
		CommunicationMethod,
		///<summary>LastUserInteraction. </summary>
		LastUserInteraction,
		///<summary>DeviceInfoUpdatedUTC. </summary>
		DeviceInfoUpdatedUTC,
		///<summary>UpdateEmenuDownloaded. </summary>
		UpdateEmenuDownloaded,
		///<summary>UpdateConsoleDownloaded. </summary>
		UpdateConsoleDownloaded,
		///<summary>UpdateAgentDownloaded. </summary>
		UpdateAgentDownloaded,
		///<summary>UpdateSupportToolsDownloaded. </summary>
		UpdateSupportToolsDownloaded,
		///<summary>UpdateOSDownloaded. </summary>
		UpdateOSDownloaded,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdateStatusChangedUTC. </summary>
		UpdateStatusChangedUTC,
		///<summary>Token. </summary>
		Token,
		///<summary>SandboxMode. </summary>
		SandboxMode,
		///<summary>MessagingServiceRunning. </summary>
		MessagingServiceRunning,
		///<summary>MessagingServiceVersion. </summary>
		MessagingServiceVersion,
		///<summary>LastMessagingServiceRequest. </summary>
		LastMessagingServiceRequest,
		/// <summary></summary>
		AmountOfFields
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Device. </summary>
	public partial class DeviceRelations: RelationFactory
	{
		#region Custom Relation code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomRelationCode
        private const ReferentialConstraintDeleteRule DeleteRuleForClientEntityUsingDeviceId = ReferentialConstraintDeleteRule.SetNull;
        private const ReferentialConstraintDeleteRule DeleteRuleForDeliverypointEntityUsingDeviceId = ReferentialConstraintDeleteRule.SetNull;
        private const ReferentialConstraintDeleteRule DeleteRuleForTerminalEntityUsingDeviceId = ReferentialConstraintDeleteRule.SetNull;
        // __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		
		// private const ReferentialConstraintDeleteRule DeleteRuleForClientEntityUsingDeviceId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForDeliverypointEntityUsingDeviceId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForTerminalEntityUsingDeviceId = ReferentialConstraintDeleteRule


		/// <summary>Gets all relations and delete rules of the DeviceEntity as a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects.</summary>
		/// <returns>a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects</returns>
		public virtual Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> GetRelationsAndDeleteRules()
		{
			Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> toReturn = new Dictionary<IEntityRelation, ReferentialConstraintDeleteRule>();
			toReturn.Add(this.ClientEntityUsingDeviceId, DeviceRelations.DeleteRuleForClientEntityUsingDeviceId);
			toReturn.Add(this.DeliverypointEntityUsingDeviceId, DeviceRelations.DeleteRuleForDeliverypointEntityUsingDeviceId);
			toReturn.Add(this.TerminalEntityUsingDeviceId, DeviceRelations.DeleteRuleForTerminalEntityUsingDeviceId);
			return toReturn;
		}		
	
	
		/// <summary>Returns a new IEntityRelation object, between DeviceEntity and ClientEntity over the 1:n relation they have, using the relation between the fields: Device.DeviceId - Client.DeviceId</summary>
		public virtual IEntityRelation ClientEntityUsingDeviceId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "ClientCollection", true, new[] { DeviceFields.DeviceId, ClientFields.DeviceId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between DeviceEntity and DeliverypointEntity over the 1:n relation they have, using the relation between the fields: Device.DeviceId - Deliverypoint.DeviceId</summary>
		public virtual IEntityRelation DeliverypointEntityUsingDeviceId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "DeliverypointCollection", true, new[] { DeviceFields.DeviceId, DeliverypointFields.DeviceId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between DeviceEntity and TerminalEntity over the 1:n relation they have, using the relation between the fields: Device.DeviceId - Terminal.DeviceId</summary>
		public virtual IEntityRelation TerminalEntityUsingDeviceId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "TerminalCollection", true, new[] { DeviceFields.DeviceId, TerminalFields.DeviceId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between DeviceEntity and CustomerEntity over the m:1 relation they have, using the relation between the fields: Device.CustomerId - Customer.CustomerId</summary>
		public virtual IEntityRelation CustomerEntityUsingCustomerId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Customer", false, new[] { CustomerFields.CustomerId, DeviceFields.CustomerId }); }
		}

	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticDeviceRelations
	{
		internal static readonly IEntityRelation ClientEntityUsingDeviceIdStatic = new DeviceRelations().ClientEntityUsingDeviceId;
		internal static readonly IEntityRelation DeliverypointEntityUsingDeviceIdStatic = new DeviceRelations().DeliverypointEntityUsingDeviceId;
		internal static readonly IEntityRelation TerminalEntityUsingDeviceIdStatic = new DeviceRelations().TerminalEntityUsingDeviceId;
		internal static readonly IEntityRelation CustomerEntityUsingCustomerIdStatic = new DeviceRelations().CustomerEntityUsingCustomerId;

		/// <summary>CTor</summary>
		static StaticDeviceRelations() { }
	}
}

