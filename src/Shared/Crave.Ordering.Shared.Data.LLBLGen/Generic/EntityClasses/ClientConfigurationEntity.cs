﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 5.8
// Code is generated on: 
// Code is generated using templates: Crave.TemplateBindings
// Templates vendor: Crave Interactive
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.FactoryClasses;
using Crave.Ordering.Shared.Data.LLBLGen.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

// ReSharper disable All
namespace Crave.Ordering.Shared.Data.LLBLGen.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'ClientConfiguration'.<br/><br/></summary>
	[Serializable]
	public partial class ClientConfigurationEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END
	
	{
		private EntityCollection<ClientConfigurationRouteEntity> _clientConfigurationRouteCollection;
		private EntityCollection<DeliverypointEntity> _deliverypointCollection;
		private EntityCollection<DeliverypointgroupEntity> _deliverypointgroupCollection;
		private EntityCollection<MediaEntity> _mediaCollection;
		private CompanyEntity _company;
		private DeliverypointgroupEntity _deliverypointgroup;
		private MenuEntity _menu;
		private PriceScheduleEntity _priceSchedule;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		private static ClientConfigurationEntityStaticMetaData _staticMetaData = new ClientConfigurationEntityStaticMetaData();
		private static ClientConfigurationRelations _relationsFactory = new ClientConfigurationRelations();		

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Company</summary>
			public static readonly string Company = "Company";
			/// <summary>Member name Deliverypointgroup</summary>
			public static readonly string Deliverypointgroup = "Deliverypointgroup";
			/// <summary>Member name Menu</summary>
			public static readonly string Menu = "Menu";
			/// <summary>Member name PriceSchedule</summary>
			public static readonly string PriceSchedule = "PriceSchedule";
			/// <summary>Member name ClientConfigurationRouteCollection</summary>
			public static readonly string ClientConfigurationRouteCollection = "ClientConfigurationRouteCollection";
			/// <summary>Member name DeliverypointCollection</summary>
			public static readonly string DeliverypointCollection = "DeliverypointCollection";
			/// <summary>Member name DeliverypointgroupCollection</summary>
			public static readonly string DeliverypointgroupCollection = "DeliverypointgroupCollection";
			/// <summary>Member name MediaCollection</summary>
			public static readonly string MediaCollection = "MediaCollection";
		}

		/// <summary>Static meta-data storage for navigator related information</summary>
		protected class ClientConfigurationEntityStaticMetaData : EntityStaticMetaDataBase
		{
			public ClientConfigurationEntityStaticMetaData()
			{
				SetEntityCoreInfo("ClientConfigurationEntity", InheritanceHierarchyType.None, false, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ClientConfigurationEntity, typeof(ClientConfigurationEntity), typeof(ClientConfigurationEntityFactory), false);
				AddNavigatorMetaData<ClientConfigurationEntity, EntityCollection<ClientConfigurationRouteEntity>>("ClientConfigurationRouteCollection", a => a._clientConfigurationRouteCollection, (a, b) => a._clientConfigurationRouteCollection = b, a => a.ClientConfigurationRouteCollection, () => new ClientConfigurationRelations().ClientConfigurationRouteEntityUsingClientConfigurationId, typeof(ClientConfigurationRouteEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ClientConfigurationRouteEntity);
				AddNavigatorMetaData<ClientConfigurationEntity, EntityCollection<DeliverypointEntity>>("DeliverypointCollection", a => a._deliverypointCollection, (a, b) => a._deliverypointCollection = b, a => a.DeliverypointCollection, () => new ClientConfigurationRelations().DeliverypointEntityUsingClientConfigurationId, typeof(DeliverypointEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.DeliverypointEntity);
				AddNavigatorMetaData<ClientConfigurationEntity, EntityCollection<DeliverypointgroupEntity>>("DeliverypointgroupCollection", a => a._deliverypointgroupCollection, (a, b) => a._deliverypointgroupCollection = b, a => a.DeliverypointgroupCollection, () => new ClientConfigurationRelations().DeliverypointgroupEntityUsingClientConfigurationId, typeof(DeliverypointgroupEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.DeliverypointgroupEntity);
				AddNavigatorMetaData<ClientConfigurationEntity, EntityCollection<MediaEntity>>("MediaCollection", a => a._mediaCollection, (a, b) => a._mediaCollection = b, a => a.MediaCollection, () => new ClientConfigurationRelations().MediaEntityUsingClientConfigurationId, typeof(MediaEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.MediaEntity);
				AddNavigatorMetaData<ClientConfigurationEntity, CompanyEntity>("Company", "ClientConfigurationCollection", (a, b) => a._company = b, a => a._company, (a, b) => a.Company = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticClientConfigurationRelations.CompanyEntityUsingCompanyIdStatic, ()=>new ClientConfigurationRelations().CompanyEntityUsingCompanyId, null, new int[] { (int)ClientConfigurationFieldIndex.CompanyId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.CompanyEntity);
				AddNavigatorMetaData<ClientConfigurationEntity, DeliverypointgroupEntity>("Deliverypointgroup", "ClientConfigurationCollection", (a, b) => a._deliverypointgroup = b, a => a._deliverypointgroup, (a, b) => a.Deliverypointgroup = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticClientConfigurationRelations.DeliverypointgroupEntityUsingDeliverypointgroupIdStatic, ()=>new ClientConfigurationRelations().DeliverypointgroupEntityUsingDeliverypointgroupId, null, new int[] { (int)ClientConfigurationFieldIndex.DeliverypointgroupId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.DeliverypointgroupEntity);
				AddNavigatorMetaData<ClientConfigurationEntity, MenuEntity>("Menu", "ClientConfigurationCollection", (a, b) => a._menu = b, a => a._menu, (a, b) => a.Menu = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticClientConfigurationRelations.MenuEntityUsingMenuIdStatic, ()=>new ClientConfigurationRelations().MenuEntityUsingMenuId, null, new int[] { (int)ClientConfigurationFieldIndex.MenuId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.MenuEntity);
				AddNavigatorMetaData<ClientConfigurationEntity, PriceScheduleEntity>("PriceSchedule", "ClientConfigurationCollection", (a, b) => a._priceSchedule = b, a => a._priceSchedule, (a, b) => a.PriceSchedule = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticClientConfigurationRelations.PriceScheduleEntityUsingPriceScheduleIdStatic, ()=>new ClientConfigurationRelations().PriceScheduleEntityUsingPriceScheduleId, null, new int[] { (int)ClientConfigurationFieldIndex.PriceScheduleId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.PriceScheduleEntity);
			}
		}

		/// <summary>Static ctor</summary>
		static ClientConfigurationEntity()
		{
		}

		/// <summary> CTor</summary>
		public ClientConfigurationEntity()
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public ClientConfigurationEntity(IEntityFields2 fields)
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this ClientConfigurationEntity</param>
		public ClientConfigurationEntity(IValidator validator)
		{
			InitClassEmpty(validator, null);
		}

		/// <summary> CTor</summary>
		/// <param name="clientConfigurationId">PK value for ClientConfiguration which data should be fetched into this ClientConfiguration object</param>
		public ClientConfigurationEntity(System.Int32 clientConfigurationId) : this(clientConfigurationId, null)
		{
		}

		/// <summary> CTor</summary>
		/// <param name="clientConfigurationId">PK value for ClientConfiguration which data should be fetched into this ClientConfiguration object</param>
		/// <param name="validator">The custom validator object for this ClientConfigurationEntity</param>
		public ClientConfigurationEntity(System.Int32 clientConfigurationId, IValidator validator)
		{
			InitClassEmpty(validator, null);
			this.ClientConfigurationId = clientConfigurationId;
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ClientConfigurationEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'ClientConfigurationRoute' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoClientConfigurationRouteCollection() { return CreateRelationInfoForNavigator("ClientConfigurationRouteCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Deliverypoint' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoDeliverypointCollection() { return CreateRelationInfoForNavigator("DeliverypointCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Deliverypointgroup' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoDeliverypointgroupCollection() { return CreateRelationInfoForNavigator("DeliverypointgroupCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Media' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoMediaCollection() { return CreateRelationInfoForNavigator("MediaCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Company' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoCompany() { return CreateRelationInfoForNavigator("Company"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Deliverypointgroup' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoDeliverypointgroup() { return CreateRelationInfoForNavigator("Deliverypointgroup"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Menu' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoMenu() { return CreateRelationInfoForNavigator("Menu"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'PriceSchedule' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoPriceSchedule() { return CreateRelationInfoForNavigator("PriceSchedule"); }
		
		/// <inheritdoc/>
		protected override EntityStaticMetaDataBase GetEntityStaticMetaData() {	return _staticMetaData; }

		/// <summary>Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		/// <summary>Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this ClientConfigurationEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary>The relations object holding all relations of this entity with other entity classes.</summary>
		public static ClientConfigurationRelations Relations { get { return _relationsFactory; } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'ClientConfigurationRoute' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathClientConfigurationRouteCollection { get { return _staticMetaData.GetPrefetchPathElement("ClientConfigurationRouteCollection", CommonEntityBase.CreateEntityCollection<ClientConfigurationRouteEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Deliverypoint' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathDeliverypointCollection { get { return _staticMetaData.GetPrefetchPathElement("DeliverypointCollection", CommonEntityBase.CreateEntityCollection<DeliverypointEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Deliverypointgroup' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathDeliverypointgroupCollection { get { return _staticMetaData.GetPrefetchPathElement("DeliverypointgroupCollection", CommonEntityBase.CreateEntityCollection<DeliverypointgroupEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Media' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathMediaCollection { get { return _staticMetaData.GetPrefetchPathElement("MediaCollection", CommonEntityBase.CreateEntityCollection<MediaEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Company' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathCompanyEntity { get { return _staticMetaData.GetPrefetchPathElement("Company", CommonEntityBase.CreateEntityCollection<CompanyEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Deliverypointgroup' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathDeliverypointgroupEntity { get { return _staticMetaData.GetPrefetchPathElement("Deliverypointgroup", CommonEntityBase.CreateEntityCollection<DeliverypointgroupEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Menu' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathMenuEntity { get { return _staticMetaData.GetPrefetchPathElement("Menu", CommonEntityBase.CreateEntityCollection<MenuEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'PriceSchedule' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathPriceScheduleEntity { get { return _staticMetaData.GetPrefetchPathElement("PriceSchedule", CommonEntityBase.CreateEntityCollection<PriceScheduleEntity>()); } }

		/// <summary>The ClientConfigurationId property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."ClientConfigurationId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 ClientConfigurationId
		{
			get { return (System.Int32)GetValue((int)ClientConfigurationFieldIndex.ClientConfigurationId, true); }
			set { SetValue((int)ClientConfigurationFieldIndex.ClientConfigurationId, value); }		}

		/// <summary>The CompanyId property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."CompanyId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CompanyId
		{
			get { return (System.Int32)GetValue((int)ClientConfigurationFieldIndex.CompanyId, true); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.CompanyId, value); }
		}

		/// <summary>The Name property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."Name".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)ClientConfigurationFieldIndex.Name, true); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.Name, value); }
		}

		/// <summary>The DeliverypointgroupId property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."DeliverypointgroupId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> DeliverypointgroupId
		{
			get { return (Nullable<System.Int32>)GetValue((int)ClientConfigurationFieldIndex.DeliverypointgroupId, false); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.DeliverypointgroupId, value); }
		}

		/// <summary>The MenuId property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."MenuId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 MenuId
		{
			get { return (System.Int32)GetValue((int)ClientConfigurationFieldIndex.MenuId, true); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.MenuId, value); }
		}

		/// <summary>The UIThemeId property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."UIThemeId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UIThemeId
		{
			get { return (Nullable<System.Int32>)GetValue((int)ClientConfigurationFieldIndex.UIThemeId, false); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.UIThemeId, value); }
		}

		/// <summary>The UIScheduleId property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."UIScheduleId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UIScheduleId
		{
			get { return (Nullable<System.Int32>)GetValue((int)ClientConfigurationFieldIndex.UIScheduleId, false); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.UIScheduleId, value); }
		}

		/// <summary>The WifiConfigurationId property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."WifiConfigurationId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> WifiConfigurationId
		{
			get { return (Nullable<System.Int32>)GetValue((int)ClientConfigurationFieldIndex.WifiConfigurationId, false); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.WifiConfigurationId, value); }
		}

		/// <summary>The Pincode property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."Pincode".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 25.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Pincode
		{
			get { return (System.String)GetValue((int)ClientConfigurationFieldIndex.Pincode, true); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.Pincode, value); }
		}

		/// <summary>The PincodeSU property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."PincodeSU".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 25.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PincodeSU
		{
			get { return (System.String)GetValue((int)ClientConfigurationFieldIndex.PincodeSU, true); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.PincodeSU, value); }
		}

		/// <summary>The PincodeGM property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."PincodeGM".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 25.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PincodeGM
		{
			get { return (System.String)GetValue((int)ClientConfigurationFieldIndex.PincodeGM, true); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.PincodeGM, value); }
		}

		/// <summary>The PriceScheduleId property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."PriceScheduleId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> PriceScheduleId
		{
			get { return (Nullable<System.Int32>)GetValue((int)ClientConfigurationFieldIndex.PriceScheduleId, false); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.PriceScheduleId, value); }
		}

		/// <summary>The HidePrices property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."HidePrices".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean HidePrices
		{
			get { return (System.Boolean)GetValue((int)ClientConfigurationFieldIndex.HidePrices, true); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.HidePrices, value); }
		}

		/// <summary>The ShowCurrencySymbol property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."ShowCurrencySymbol".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean ShowCurrencySymbol
		{
			get { return (System.Boolean)GetValue((int)ClientConfigurationFieldIndex.ShowCurrencySymbol, true); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.ShowCurrencySymbol, value); }
		}

		/// <summary>The ClearSessionOnTimeout property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."ClearSessionOnTimeout".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean ClearSessionOnTimeout
		{
			get { return (System.Boolean)GetValue((int)ClientConfigurationFieldIndex.ClearSessionOnTimeout, true); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.ClearSessionOnTimeout, value); }
		}

		/// <summary>The ResetTimeout property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."ResetTimeout".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ResetTimeout
		{
			get { return (System.Int32)GetValue((int)ClientConfigurationFieldIndex.ResetTimeout, true); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.ResetTimeout, value); }
		}

		/// <summary>The ScreenTimeoutInterval property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."ScreenTimeoutInterval".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ScreenTimeoutInterval
		{
			get { return (System.Int32)GetValue((int)ClientConfigurationFieldIndex.ScreenTimeoutInterval, true); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.ScreenTimeoutInterval, value); }
		}

		/// <summary>The DimLevelDull property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."DimLevelDull".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 DimLevelDull
		{
			get { return (System.Int32)GetValue((int)ClientConfigurationFieldIndex.DimLevelDull, true); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.DimLevelDull, value); }
		}

		/// <summary>The DimLevelMedium property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."DimLevelMedium".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 DimLevelMedium
		{
			get { return (System.Int32)GetValue((int)ClientConfigurationFieldIndex.DimLevelMedium, true); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.DimLevelMedium, value); }
		}

		/// <summary>The DimLevelBright property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."DimLevelBright".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 DimLevelBright
		{
			get { return (System.Int32)GetValue((int)ClientConfigurationFieldIndex.DimLevelBright, true); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.DimLevelBright, value); }
		}

		/// <summary>The DockedDimLevelDull property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."DockedDimLevelDull".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 DockedDimLevelDull
		{
			get { return (System.Int32)GetValue((int)ClientConfigurationFieldIndex.DockedDimLevelDull, true); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.DockedDimLevelDull, value); }
		}

		/// <summary>The DockedDimLevelMedium property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."DockedDimLevelMedium".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 DockedDimLevelMedium
		{
			get { return (System.Int32)GetValue((int)ClientConfigurationFieldIndex.DockedDimLevelMedium, true); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.DockedDimLevelMedium, value); }
		}

		/// <summary>The DockedDimLevelBright property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."DockedDimLevelBright".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 DockedDimLevelBright
		{
			get { return (System.Int32)GetValue((int)ClientConfigurationFieldIndex.DockedDimLevelBright, true); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.DockedDimLevelBright, value); }
		}

		/// <summary>The PowerSaveTimeout property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."PowerSaveTimeout".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 PowerSaveTimeout
		{
			get { return (System.Int32)GetValue((int)ClientConfigurationFieldIndex.PowerSaveTimeout, true); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.PowerSaveTimeout, value); }
		}

		/// <summary>The PowerSaveLevel property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."PowerSaveLevel".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 PowerSaveLevel
		{
			get { return (System.Int32)GetValue((int)ClientConfigurationFieldIndex.PowerSaveLevel, true); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.PowerSaveLevel, value); }
		}

		/// <summary>The OutOfChargeLevel property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."OutOfChargeLevel".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 OutOfChargeLevel
		{
			get { return (System.Int32)GetValue((int)ClientConfigurationFieldIndex.OutOfChargeLevel, true); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.OutOfChargeLevel, value); }
		}

		/// <summary>The DailyOrderReset property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."DailyOrderReset".<br/>Table field type characteristics (type, precision, scale, length): Char, 0, 0, 4.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String DailyOrderReset
		{
			get { return (System.String)GetValue((int)ClientConfigurationFieldIndex.DailyOrderReset, true); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.DailyOrderReset, value); }
		}

		/// <summary>The SleepTime property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."SleepTime".<br/>Table field type characteristics (type, precision, scale, length): Char, 0, 0, 4.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String SleepTime
		{
			get { return (System.String)GetValue((int)ClientConfigurationFieldIndex.SleepTime, true); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.SleepTime, value); }
		}

		/// <summary>The WakeUpTime property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."WakeUpTime".<br/>Table field type characteristics (type, precision, scale, length): Char, 0, 0, 4.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String WakeUpTime
		{
			get { return (System.String)GetValue((int)ClientConfigurationFieldIndex.WakeUpTime, true); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.WakeUpTime, value); }
		}

		/// <summary>The HomepageSlideshowInterval property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."HomepageSlideshowInterval".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 HomepageSlideshowInterval
		{
			get { return (System.Int32)GetValue((int)ClientConfigurationFieldIndex.HomepageSlideshowInterval, true); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.HomepageSlideshowInterval, value); }
		}

		/// <summary>The IsChargerRemovedDialogEnabled property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."IsChargerRemovedDialogEnabled".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsChargerRemovedDialogEnabled
		{
			get { return (System.Boolean)GetValue((int)ClientConfigurationFieldIndex.IsChargerRemovedDialogEnabled, true); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.IsChargerRemovedDialogEnabled, value); }
		}

		/// <summary>The IsChargerRemovedReminderDialogEnabled property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."IsChargerRemovedReminderDialogEnabled".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsChargerRemovedReminderDialogEnabled
		{
			get { return (System.Boolean)GetValue((int)ClientConfigurationFieldIndex.IsChargerRemovedReminderDialogEnabled, true); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.IsChargerRemovedReminderDialogEnabled, value); }
		}

		/// <summary>The PowerButtonHardBehaviour property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."PowerButtonHardBehaviour".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Crave.Enums.PowerButtonMode PowerButtonHardBehaviour
		{
			get { return (Crave.Enums.PowerButtonMode)GetValue((int)ClientConfigurationFieldIndex.PowerButtonHardBehaviour, true); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.PowerButtonHardBehaviour, value); }
		}

		/// <summary>The PowerButtonSoftBehaviour property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."PowerButtonSoftBehaviour".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Crave.Enums.PowerButtonMode PowerButtonSoftBehaviour
		{
			get { return (Crave.Enums.PowerButtonMode)GetValue((int)ClientConfigurationFieldIndex.PowerButtonSoftBehaviour, true); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.PowerButtonSoftBehaviour, value); }
		}

		/// <summary>The ScreenOffMode property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."ScreenOffMode".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Crave.Enums.ScreenOffMode ScreenOffMode
		{
			get { return (Crave.Enums.ScreenOffMode)GetValue((int)ClientConfigurationFieldIndex.ScreenOffMode, true); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.ScreenOffMode, value); }
		}

		/// <summary>The ScreensaverMode property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."ScreensaverMode".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Crave.Enums.ScreensaverMode ScreensaverMode
		{
			get { return (Crave.Enums.ScreensaverMode)GetValue((int)ClientConfigurationFieldIndex.ScreensaverMode, true); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.ScreensaverMode, value); }
		}

		/// <summary>The DeviceRebootMethod property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."DeviceRebootMethod".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Crave.Enums.DeviceRebootMethod DeviceRebootMethod
		{
			get { return (Crave.Enums.DeviceRebootMethod)GetValue((int)ClientConfigurationFieldIndex.DeviceRebootMethod, true); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.DeviceRebootMethod, value); }
		}

		/// <summary>The RoomserviceCharge property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."RoomserviceCharge".<br/>Table field type characteristics (type, precision, scale, length): Money, 19, 4, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Decimal> RoomserviceCharge
		{
			get { return (Nullable<System.Decimal>)GetValue((int)ClientConfigurationFieldIndex.RoomserviceCharge, false); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.RoomserviceCharge, value); }
		}

		/// <summary>The AllowFreeText property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."AllowFreeText".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean AllowFreeText
		{
			get { return (System.Boolean)GetValue((int)ClientConfigurationFieldIndex.AllowFreeText, true); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.AllowFreeText, value); }
		}

		/// <summary>The BrowserAgeVerificationEnabled property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."BrowserAgeVerificationEnabled".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean BrowserAgeVerificationEnabled
		{
			get { return (System.Boolean)GetValue((int)ClientConfigurationFieldIndex.BrowserAgeVerificationEnabled, true); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.BrowserAgeVerificationEnabled, value); }
		}

		/// <summary>The BrowserAgeVerificationLayout property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."BrowserAgeVerificationLayout".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 BrowserAgeVerificationLayout
		{
			get { return (System.Int32)GetValue((int)ClientConfigurationFieldIndex.BrowserAgeVerificationLayout, true); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.BrowserAgeVerificationLayout, value); }
		}

		/// <summary>The RestartApplicationDialogEnabled property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."RestartApplicationDialogEnabled".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean RestartApplicationDialogEnabled
		{
			get { return (System.Boolean)GetValue((int)ClientConfigurationFieldIndex.RestartApplicationDialogEnabled, true); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.RestartApplicationDialogEnabled, value); }
		}

		/// <summary>The RebootDeviceDialogEnabled property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."RebootDeviceDialogEnabled".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean RebootDeviceDialogEnabled
		{
			get { return (System.Boolean)GetValue((int)ClientConfigurationFieldIndex.RebootDeviceDialogEnabled, true); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.RebootDeviceDialogEnabled, value); }
		}

		/// <summary>The RestartTimeoutSeconds property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."RestartTimeoutSeconds".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 RestartTimeoutSeconds
		{
			get { return (System.Int32)GetValue((int)ClientConfigurationFieldIndex.RestartTimeoutSeconds, true); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.RestartTimeoutSeconds, value); }
		}

		/// <summary>The GooglePrinterId property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."GooglePrinterId".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String GooglePrinterId
		{
			get { return (System.String)GetValue((int)ClientConfigurationFieldIndex.GooglePrinterId, true); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.GooglePrinterId, value); }
		}

		/// <summary>The PmsIntegration property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."PmsIntegration".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean PmsIntegration
		{
			get { return (System.Boolean)GetValue((int)ClientConfigurationFieldIndex.PmsIntegration, true); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.PmsIntegration, value); }
		}

		/// <summary>The PmsAllowShowBill property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."PmsAllowShowBill".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean PmsAllowShowBill
		{
			get { return (System.Boolean)GetValue((int)ClientConfigurationFieldIndex.PmsAllowShowBill, true); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.PmsAllowShowBill, value); }
		}

		/// <summary>The PmsAllowExpressCheckout property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."PmsAllowExpressCheckout".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean PmsAllowExpressCheckout
		{
			get { return (System.Boolean)GetValue((int)ClientConfigurationFieldIndex.PmsAllowExpressCheckout, true); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.PmsAllowExpressCheckout, value); }
		}

		/// <summary>The PmsAllowShowGuestName property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."PmsAllowShowGuestName".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean PmsAllowShowGuestName
		{
			get { return (System.Boolean)GetValue((int)ClientConfigurationFieldIndex.PmsAllowShowGuestName, true); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.PmsAllowShowGuestName, value); }
		}

		/// <summary>The PmsLockClientWhenNotCheckedIn property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."PmsLockClientWhenNotCheckedIn".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean PmsLockClientWhenNotCheckedIn
		{
			get { return (System.Boolean)GetValue((int)ClientConfigurationFieldIndex.PmsLockClientWhenNotCheckedIn, true); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.PmsLockClientWhenNotCheckedIn, value); }
		}

		/// <summary>The PmsWelcomeTimeoutMinutes property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."PmsWelcomeTimeoutMinutes".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 PmsWelcomeTimeoutMinutes
		{
			get { return (System.Int32)GetValue((int)ClientConfigurationFieldIndex.PmsWelcomeTimeoutMinutes, true); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.PmsWelcomeTimeoutMinutes, value); }
		}

		/// <summary>The PmsCheckoutCompleteTimeoutMinutes property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."PmsCheckoutCompleteTimeoutMinutes".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 PmsCheckoutCompleteTimeoutMinutes
		{
			get { return (System.Int32)GetValue((int)ClientConfigurationFieldIndex.PmsCheckoutCompleteTimeoutMinutes, true); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.PmsCheckoutCompleteTimeoutMinutes, value); }
		}

		/// <summary>The IsOrderitemAddedDialogEnabled property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."IsOrderitemAddedDialogEnabled".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsOrderitemAddedDialogEnabled
		{
			get { return (System.Boolean)GetValue((int)ClientConfigurationFieldIndex.IsOrderitemAddedDialogEnabled, true); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.IsOrderitemAddedDialogEnabled, value); }
		}

		/// <summary>The IsClearBasketDialogEnabled property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."IsClearBasketDialogEnabled".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsClearBasketDialogEnabled
		{
			get { return (System.Boolean)GetValue((int)ClientConfigurationFieldIndex.IsClearBasketDialogEnabled, true); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.IsClearBasketDialogEnabled, value); }
		}

		/// <summary>The OrderCompletedNotificationEnabled property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."OrderCompletedNotificationEnabled".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean OrderCompletedNotificationEnabled
		{
			get { return (System.Boolean)GetValue((int)ClientConfigurationFieldIndex.OrderCompletedNotificationEnabled, true); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.OrderCompletedNotificationEnabled, value); }
		}

		/// <summary>The CreatedUTC property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."CreatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ClientConfigurationFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.CreatedUTC, value); }
		}

		/// <summary>The CreatedBy property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."CreatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CreatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)ClientConfigurationFieldIndex.CreatedBy, false); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.CreatedBy, value); }
		}

		/// <summary>The UpdatedUTC property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."UpdatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ClientConfigurationFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.UpdatedUTC, value); }
		}

		/// <summary>The UpdatedBy property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."UpdatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UpdatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)ClientConfigurationFieldIndex.UpdatedBy, false); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.UpdatedBy, value); }
		}

		/// <summary>Gets the EntityCollection with the related entities of type 'ClientConfigurationRouteEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(ClientConfigurationRouteEntity))]
		public virtual EntityCollection<ClientConfigurationRouteEntity> ClientConfigurationRouteCollection { get { return GetOrCreateEntityCollection<ClientConfigurationRouteEntity, ClientConfigurationRouteEntityFactory>("ClientConfiguration", true, false, ref _clientConfigurationRouteCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'DeliverypointEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(DeliverypointEntity))]
		public virtual EntityCollection<DeliverypointEntity> DeliverypointCollection { get { return GetOrCreateEntityCollection<DeliverypointEntity, DeliverypointEntityFactory>("ClientConfiguration", true, false, ref _deliverypointCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'DeliverypointgroupEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(DeliverypointgroupEntity))]
		public virtual EntityCollection<DeliverypointgroupEntity> DeliverypointgroupCollection { get { return GetOrCreateEntityCollection<DeliverypointgroupEntity, DeliverypointgroupEntityFactory>("ClientConfiguration", true, false, ref _deliverypointgroupCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'MediaEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(MediaEntity))]
		public virtual EntityCollection<MediaEntity> MediaCollection { get { return GetOrCreateEntityCollection<MediaEntity, MediaEntityFactory>("ClientConfiguration", true, false, ref _mediaCollection); } }

		/// <summary>Gets / sets related entity of type 'CompanyEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual CompanyEntity Company
		{
			get { return _company; }
			set { SetSingleRelatedEntityNavigator(value, "Company"); }
		}

		/// <summary>Gets / sets related entity of type 'DeliverypointgroupEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual DeliverypointgroupEntity Deliverypointgroup
		{
			get { return _deliverypointgroup; }
			set { SetSingleRelatedEntityNavigator(value, "Deliverypointgroup"); }
		}

		/// <summary>Gets / sets related entity of type 'MenuEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual MenuEntity Menu
		{
			get { return _menu; }
			set { SetSingleRelatedEntityNavigator(value, "Menu"); }
		}

		/// <summary>Gets / sets related entity of type 'PriceScheduleEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual PriceScheduleEntity PriceSchedule
		{
			get { return _priceSchedule; }
			set { SetSingleRelatedEntityNavigator(value, "PriceSchedule"); }
		}


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen
{
	public enum ClientConfigurationFieldIndex
	{
		///<summary>ClientConfigurationId. </summary>
		ClientConfigurationId,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>Name. </summary>
		Name,
		///<summary>DeliverypointgroupId. </summary>
		DeliverypointgroupId,
		///<summary>MenuId. </summary>
		MenuId,
		///<summary>UIThemeId. </summary>
		UIThemeId,
		///<summary>UIScheduleId. </summary>
		UIScheduleId,
		///<summary>WifiConfigurationId. </summary>
		WifiConfigurationId,
		///<summary>Pincode. </summary>
		Pincode,
		///<summary>PincodeSU. </summary>
		PincodeSU,
		///<summary>PincodeGM. </summary>
		PincodeGM,
		///<summary>PriceScheduleId. </summary>
		PriceScheduleId,
		///<summary>HidePrices. </summary>
		HidePrices,
		///<summary>ShowCurrencySymbol. </summary>
		ShowCurrencySymbol,
		///<summary>ClearSessionOnTimeout. </summary>
		ClearSessionOnTimeout,
		///<summary>ResetTimeout. </summary>
		ResetTimeout,
		///<summary>ScreenTimeoutInterval. </summary>
		ScreenTimeoutInterval,
		///<summary>DimLevelDull. </summary>
		DimLevelDull,
		///<summary>DimLevelMedium. </summary>
		DimLevelMedium,
		///<summary>DimLevelBright. </summary>
		DimLevelBright,
		///<summary>DockedDimLevelDull. </summary>
		DockedDimLevelDull,
		///<summary>DockedDimLevelMedium. </summary>
		DockedDimLevelMedium,
		///<summary>DockedDimLevelBright. </summary>
		DockedDimLevelBright,
		///<summary>PowerSaveTimeout. </summary>
		PowerSaveTimeout,
		///<summary>PowerSaveLevel. </summary>
		PowerSaveLevel,
		///<summary>OutOfChargeLevel. </summary>
		OutOfChargeLevel,
		///<summary>DailyOrderReset. </summary>
		DailyOrderReset,
		///<summary>SleepTime. </summary>
		SleepTime,
		///<summary>WakeUpTime. </summary>
		WakeUpTime,
		///<summary>HomepageSlideshowInterval. </summary>
		HomepageSlideshowInterval,
		///<summary>IsChargerRemovedDialogEnabled. </summary>
		IsChargerRemovedDialogEnabled,
		///<summary>IsChargerRemovedReminderDialogEnabled. </summary>
		IsChargerRemovedReminderDialogEnabled,
		///<summary>PowerButtonHardBehaviour. </summary>
		PowerButtonHardBehaviour,
		///<summary>PowerButtonSoftBehaviour. </summary>
		PowerButtonSoftBehaviour,
		///<summary>ScreenOffMode. </summary>
		ScreenOffMode,
		///<summary>ScreensaverMode. </summary>
		ScreensaverMode,
		///<summary>DeviceRebootMethod. </summary>
		DeviceRebootMethod,
		///<summary>RoomserviceCharge. </summary>
		RoomserviceCharge,
		///<summary>AllowFreeText. </summary>
		AllowFreeText,
		///<summary>BrowserAgeVerificationEnabled. </summary>
		BrowserAgeVerificationEnabled,
		///<summary>BrowserAgeVerificationLayout. </summary>
		BrowserAgeVerificationLayout,
		///<summary>RestartApplicationDialogEnabled. </summary>
		RestartApplicationDialogEnabled,
		///<summary>RebootDeviceDialogEnabled. </summary>
		RebootDeviceDialogEnabled,
		///<summary>RestartTimeoutSeconds. </summary>
		RestartTimeoutSeconds,
		///<summary>GooglePrinterId. </summary>
		GooglePrinterId,
		///<summary>PmsIntegration. </summary>
		PmsIntegration,
		///<summary>PmsAllowShowBill. </summary>
		PmsAllowShowBill,
		///<summary>PmsAllowExpressCheckout. </summary>
		PmsAllowExpressCheckout,
		///<summary>PmsAllowShowGuestName. </summary>
		PmsAllowShowGuestName,
		///<summary>PmsLockClientWhenNotCheckedIn. </summary>
		PmsLockClientWhenNotCheckedIn,
		///<summary>PmsWelcomeTimeoutMinutes. </summary>
		PmsWelcomeTimeoutMinutes,
		///<summary>PmsCheckoutCompleteTimeoutMinutes. </summary>
		PmsCheckoutCompleteTimeoutMinutes,
		///<summary>IsOrderitemAddedDialogEnabled. </summary>
		IsOrderitemAddedDialogEnabled,
		///<summary>IsClearBasketDialogEnabled. </summary>
		IsClearBasketDialogEnabled,
		///<summary>OrderCompletedNotificationEnabled. </summary>
		OrderCompletedNotificationEnabled,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		/// <summary></summary>
		AmountOfFields
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: ClientConfiguration. </summary>
	public partial class ClientConfigurationRelations: RelationFactory
	{
		#region Custom Relation code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomRelationCode
        private const ReferentialConstraintDeleteRule DeleteRuleForClientConfigurationRouteEntityUsingClientConfigurationId = ReferentialConstraintDeleteRule.Cascade;
        private const ReferentialConstraintDeleteRule DeleteRuleForDeliverypointEntityUsingClientConfigurationId = ReferentialConstraintDeleteRule.NoAction;
        private const ReferentialConstraintDeleteRule DeleteRuleForDeliverypointgroupEntityUsingClientConfigurationId = ReferentialConstraintDeleteRule.NoAction;
        private const ReferentialConstraintDeleteRule DeleteRuleForMediaEntityUsingClientConfigurationId = ReferentialConstraintDeleteRule.Cascade;
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		
		// private const ReferentialConstraintDeleteRule DeleteRuleForClientConfigurationRouteEntityUsingClientConfigurationId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForDeliverypointEntityUsingClientConfigurationId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForDeliverypointgroupEntityUsingClientConfigurationId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForMediaEntityUsingClientConfigurationId = ReferentialConstraintDeleteRule


		/// <summary>Gets all relations and delete rules of the ClientConfigurationEntity as a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects.</summary>
		/// <returns>a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects</returns>
		public virtual Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> GetRelationsAndDeleteRules()
		{
			Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> toReturn = new Dictionary<IEntityRelation, ReferentialConstraintDeleteRule>();
			toReturn.Add(this.ClientConfigurationRouteEntityUsingClientConfigurationId, ClientConfigurationRelations.DeleteRuleForClientConfigurationRouteEntityUsingClientConfigurationId);
			toReturn.Add(this.DeliverypointEntityUsingClientConfigurationId, ClientConfigurationRelations.DeleteRuleForDeliverypointEntityUsingClientConfigurationId);
			toReturn.Add(this.DeliverypointgroupEntityUsingClientConfigurationId, ClientConfigurationRelations.DeleteRuleForDeliverypointgroupEntityUsingClientConfigurationId);
			toReturn.Add(this.MediaEntityUsingClientConfigurationId, ClientConfigurationRelations.DeleteRuleForMediaEntityUsingClientConfigurationId);
			return toReturn;
		}		
	
	
		/// <summary>Returns a new IEntityRelation object, between ClientConfigurationEntity and ClientConfigurationRouteEntity over the 1:n relation they have, using the relation between the fields: ClientConfiguration.ClientConfigurationId - ClientConfigurationRoute.ClientConfigurationId</summary>
		public virtual IEntityRelation ClientConfigurationRouteEntityUsingClientConfigurationId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "ClientConfigurationRouteCollection", true, new[] { ClientConfigurationFields.ClientConfigurationId, ClientConfigurationRouteFields.ClientConfigurationId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between ClientConfigurationEntity and DeliverypointEntity over the 1:n relation they have, using the relation between the fields: ClientConfiguration.ClientConfigurationId - Deliverypoint.ClientConfigurationId</summary>
		public virtual IEntityRelation DeliverypointEntityUsingClientConfigurationId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "DeliverypointCollection", true, new[] { ClientConfigurationFields.ClientConfigurationId, DeliverypointFields.ClientConfigurationId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between ClientConfigurationEntity and DeliverypointgroupEntity over the 1:n relation they have, using the relation between the fields: ClientConfiguration.ClientConfigurationId - Deliverypointgroup.ClientConfigurationId</summary>
		public virtual IEntityRelation DeliverypointgroupEntityUsingClientConfigurationId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "DeliverypointgroupCollection", true, new[] { ClientConfigurationFields.ClientConfigurationId, DeliverypointgroupFields.ClientConfigurationId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between ClientConfigurationEntity and MediaEntity over the 1:n relation they have, using the relation between the fields: ClientConfiguration.ClientConfigurationId - Media.ClientConfigurationId</summary>
		public virtual IEntityRelation MediaEntityUsingClientConfigurationId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "MediaCollection", true, new[] { ClientConfigurationFields.ClientConfigurationId, MediaFields.ClientConfigurationId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between ClientConfigurationEntity and CompanyEntity over the m:1 relation they have, using the relation between the fields: ClientConfiguration.CompanyId - Company.CompanyId</summary>
		public virtual IEntityRelation CompanyEntityUsingCompanyId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Company", false, new[] { CompanyFields.CompanyId, ClientConfigurationFields.CompanyId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between ClientConfigurationEntity and DeliverypointgroupEntity over the m:1 relation they have, using the relation between the fields: ClientConfiguration.DeliverypointgroupId - Deliverypointgroup.DeliverypointgroupId</summary>
		public virtual IEntityRelation DeliverypointgroupEntityUsingDeliverypointgroupId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Deliverypointgroup", false, new[] { DeliverypointgroupFields.DeliverypointgroupId, ClientConfigurationFields.DeliverypointgroupId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between ClientConfigurationEntity and MenuEntity over the m:1 relation they have, using the relation between the fields: ClientConfiguration.MenuId - Menu.MenuId</summary>
		public virtual IEntityRelation MenuEntityUsingMenuId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Menu", false, new[] { MenuFields.MenuId, ClientConfigurationFields.MenuId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between ClientConfigurationEntity and PriceScheduleEntity over the m:1 relation they have, using the relation between the fields: ClientConfiguration.PriceScheduleId - PriceSchedule.PriceScheduleId</summary>
		public virtual IEntityRelation PriceScheduleEntityUsingPriceScheduleId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "PriceSchedule", false, new[] { PriceScheduleFields.PriceScheduleId, ClientConfigurationFields.PriceScheduleId }); }
		}

	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticClientConfigurationRelations
	{
		internal static readonly IEntityRelation ClientConfigurationRouteEntityUsingClientConfigurationIdStatic = new ClientConfigurationRelations().ClientConfigurationRouteEntityUsingClientConfigurationId;
		internal static readonly IEntityRelation DeliverypointEntityUsingClientConfigurationIdStatic = new ClientConfigurationRelations().DeliverypointEntityUsingClientConfigurationId;
		internal static readonly IEntityRelation DeliverypointgroupEntityUsingClientConfigurationIdStatic = new ClientConfigurationRelations().DeliverypointgroupEntityUsingClientConfigurationId;
		internal static readonly IEntityRelation MediaEntityUsingClientConfigurationIdStatic = new ClientConfigurationRelations().MediaEntityUsingClientConfigurationId;
		internal static readonly IEntityRelation CompanyEntityUsingCompanyIdStatic = new ClientConfigurationRelations().CompanyEntityUsingCompanyId;
		internal static readonly IEntityRelation DeliverypointgroupEntityUsingDeliverypointgroupIdStatic = new ClientConfigurationRelations().DeliverypointgroupEntityUsingDeliverypointgroupId;
		internal static readonly IEntityRelation MenuEntityUsingMenuIdStatic = new ClientConfigurationRelations().MenuEntityUsingMenuId;
		internal static readonly IEntityRelation PriceScheduleEntityUsingPriceScheduleIdStatic = new ClientConfigurationRelations().PriceScheduleEntityUsingPriceScheduleId;

		/// <summary>CTor</summary>
		static StaticClientConfigurationRelations() { }
	}
}

