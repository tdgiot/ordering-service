﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 5.8
// Code is generated on: 
// Code is generated using templates: Crave.TemplateBindings
// Templates vendor: Crave Interactive
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.FactoryClasses;
using Crave.Ordering.Shared.Data.LLBLGen.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

// ReSharper disable All
namespace Crave.Ordering.Shared.Data.LLBLGen.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'OutletOperationalState'.<br/><br/></summary>
	[Serializable]
	public partial class OutletOperationalStateEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END
	
	{
		private EntityCollection<OutletEntity> _outletCollection;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		private static OutletOperationalStateEntityStaticMetaData _staticMetaData = new OutletOperationalStateEntityStaticMetaData();
		private static OutletOperationalStateRelations _relationsFactory = new OutletOperationalStateRelations();		

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name OutletCollection</summary>
			public static readonly string OutletCollection = "OutletCollection";
		}

		/// <summary>Static meta-data storage for navigator related information</summary>
		protected class OutletOperationalStateEntityStaticMetaData : EntityStaticMetaDataBase
		{
			public OutletOperationalStateEntityStaticMetaData()
			{
				SetEntityCoreInfo("OutletOperationalStateEntity", InheritanceHierarchyType.None, false, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.OutletOperationalStateEntity, typeof(OutletOperationalStateEntity), typeof(OutletOperationalStateEntityFactory), false);
				AddNavigatorMetaData<OutletOperationalStateEntity, EntityCollection<OutletEntity>>("OutletCollection", a => a._outletCollection, (a, b) => a._outletCollection = b, a => a.OutletCollection, () => new OutletOperationalStateRelations().OutletEntityUsingOutletOperationalStateId, typeof(OutletEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.OutletEntity);
			}
		}

		/// <summary>Static ctor</summary>
		static OutletOperationalStateEntity()
		{
		}

		/// <summary> CTor</summary>
		public OutletOperationalStateEntity()
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public OutletOperationalStateEntity(IEntityFields2 fields)
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this OutletOperationalStateEntity</param>
		public OutletOperationalStateEntity(IValidator validator)
		{
			InitClassEmpty(validator, null);
		}

		/// <summary> CTor</summary>
		/// <param name="outletOperationalStateId">PK value for OutletOperationalState which data should be fetched into this OutletOperationalState object</param>
		public OutletOperationalStateEntity(System.Int32 outletOperationalStateId) : this(outletOperationalStateId, null)
		{
		}

		/// <summary> CTor</summary>
		/// <param name="outletOperationalStateId">PK value for OutletOperationalState which data should be fetched into this OutletOperationalState object</param>
		/// <param name="validator">The custom validator object for this OutletOperationalStateEntity</param>
		public OutletOperationalStateEntity(System.Int32 outletOperationalStateId, IValidator validator)
		{
			InitClassEmpty(validator, null);
			this.OutletOperationalStateId = outletOperationalStateId;
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected OutletOperationalStateEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Outlet' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoOutletCollection() { return CreateRelationInfoForNavigator("OutletCollection"); }
		
		/// <inheritdoc/>
		protected override EntityStaticMetaDataBase GetEntityStaticMetaData() {	return _staticMetaData; }

		/// <summary>Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		/// <summary>Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this OutletOperationalStateEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary>The relations object holding all relations of this entity with other entity classes.</summary>
		public static OutletOperationalStateRelations Relations { get { return _relationsFactory; } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Outlet' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathOutletCollection { get { return _staticMetaData.GetPrefetchPathElement("OutletCollection", CommonEntityBase.CreateEntityCollection<OutletEntity>()); } }

		/// <summary>The OutletOperationalStateId property of the Entity OutletOperationalState<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OutletOperationalState"."OutletOperationalStateId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 OutletOperationalStateId
		{
			get { return (System.Int32)GetValue((int)OutletOperationalStateFieldIndex.OutletOperationalStateId, true); }
			set { SetValue((int)OutletOperationalStateFieldIndex.OutletOperationalStateId, value); }		}

		/// <summary>The OutletId property of the Entity OutletOperationalState<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OutletOperationalState"."OutletId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> OutletId
		{
			get { return (Nullable<System.Int32>)GetValue((int)OutletOperationalStateFieldIndex.OutletId, false); }
			set	{ SetValue((int)OutletOperationalStateFieldIndex.OutletId, value); }
		}

		/// <summary>The ParentCompanyId property of the Entity OutletOperationalState<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OutletOperationalState"."ParentCompanyId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ParentCompanyId
		{
			get { return (Nullable<System.Int32>)GetValue((int)OutletOperationalStateFieldIndex.ParentCompanyId, false); }
			set	{ SetValue((int)OutletOperationalStateFieldIndex.ParentCompanyId, value); }
		}

		/// <summary>The WaitTime property of the Entity OutletOperationalState<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OutletOperationalState"."WaitTime".<br/>Table field type characteristics (type, precision, scale, length): Time, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.TimeSpan> WaitTime
		{
			get { return (Nullable<System.TimeSpan>)GetValue((int)OutletOperationalStateFieldIndex.WaitTime, false); }
			set	{ SetValue((int)OutletOperationalStateFieldIndex.WaitTime, value); }
		}

		/// <summary>The OrderIntakeDisabled property of the Entity OutletOperationalState<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OutletOperationalState"."OrderIntakeDisabled".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean OrderIntakeDisabled
		{
			get { return (System.Boolean)GetValue((int)OutletOperationalStateFieldIndex.OrderIntakeDisabled, true); }
			set	{ SetValue((int)OutletOperationalStateFieldIndex.OrderIntakeDisabled, value); }
		}

		/// <summary>The CreatedUTC property of the Entity OutletOperationalState<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OutletOperationalState"."CreatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)OutletOperationalStateFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)OutletOperationalStateFieldIndex.CreatedUTC, value); }
		}

		/// <summary>The CreatedBy property of the Entity OutletOperationalState<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OutletOperationalState"."CreatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)OutletOperationalStateFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)OutletOperationalStateFieldIndex.CreatedBy, value); }
		}

		/// <summary>The UpdatedUTC property of the Entity OutletOperationalState<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OutletOperationalState"."UpdatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)OutletOperationalStateFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)OutletOperationalStateFieldIndex.UpdatedUTC, value); }
		}

		/// <summary>The UpdatedBy property of the Entity OutletOperationalState<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OutletOperationalState"."UpdatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)OutletOperationalStateFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)OutletOperationalStateFieldIndex.UpdatedBy, value); }
		}

		/// <summary>Gets the EntityCollection with the related entities of type 'OutletEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(OutletEntity))]
		public virtual EntityCollection<OutletEntity> OutletCollection { get { return GetOrCreateEntityCollection<OutletEntity, OutletEntityFactory>("OutletOperationalState", true, false, ref _outletCollection); } }


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen
{
	public enum OutletOperationalStateFieldIndex
	{
		///<summary>OutletOperationalStateId. </summary>
		OutletOperationalStateId,
		///<summary>OutletId. </summary>
		OutletId,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>WaitTime. </summary>
		WaitTime,
		///<summary>OrderIntakeDisabled. </summary>
		OrderIntakeDisabled,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		/// <summary></summary>
		AmountOfFields
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: OutletOperationalState. </summary>
	public partial class OutletOperationalStateRelations: RelationFactory
	{
		#region Custom Relation code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomRelationCode
        private const ReferentialConstraintDeleteRule DeleteRuleForOutletEntityUsingOutletOperationalStateId = ReferentialConstraintDeleteRule.NoAction;
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		
		// private const ReferentialConstraintDeleteRule DeleteRuleForOutletEntityUsingOutletOperationalStateId = ReferentialConstraintDeleteRule


		/// <summary>Gets all relations and delete rules of the OutletOperationalStateEntity as a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects.</summary>
		/// <returns>a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects</returns>
		public virtual Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> GetRelationsAndDeleteRules()
		{
			Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> toReturn = new Dictionary<IEntityRelation, ReferentialConstraintDeleteRule>();
			toReturn.Add(this.OutletEntityUsingOutletOperationalStateId, OutletOperationalStateRelations.DeleteRuleForOutletEntityUsingOutletOperationalStateId);
			return toReturn;
		}		
	
	
		/// <summary>Returns a new IEntityRelation object, between OutletOperationalStateEntity and OutletEntity over the 1:n relation they have, using the relation between the fields: OutletOperationalState.OutletOperationalStateId - Outlet.OutletOperationalStateId</summary>
		public virtual IEntityRelation OutletEntityUsingOutletOperationalStateId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "OutletCollection", true, new[] { OutletOperationalStateFields.OutletOperationalStateId, OutletFields.OutletOperationalStateId }); }
		}

	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticOutletOperationalStateRelations
	{
		internal static readonly IEntityRelation OutletEntityUsingOutletOperationalStateIdStatic = new OutletOperationalStateRelations().OutletEntityUsingOutletOperationalStateId;

		/// <summary>CTor</summary>
		static StaticOutletOperationalStateRelations() { }
	}
}

