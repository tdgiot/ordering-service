﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 5.8
// Code is generated on: 
// Code is generated using templates: Crave.TemplateBindings
// Templates vendor: Crave Interactive
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.FactoryClasses;
using Crave.Ordering.Shared.Data.LLBLGen.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

// ReSharper disable All
namespace Crave.Ordering.Shared.Data.LLBLGen.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'ClientConfigurationRoute'.<br/><br/></summary>
	[Serializable]
	public partial class ClientConfigurationRouteEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END
	
	{
		private ClientConfigurationEntity _clientConfiguration;
		private RouteEntity _route;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		private static ClientConfigurationRouteEntityStaticMetaData _staticMetaData = new ClientConfigurationRouteEntityStaticMetaData();
		private static ClientConfigurationRouteRelations _relationsFactory = new ClientConfigurationRouteRelations();		

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name ClientConfiguration</summary>
			public static readonly string ClientConfiguration = "ClientConfiguration";
			/// <summary>Member name Route</summary>
			public static readonly string Route = "Route";
		}

		/// <summary>Static meta-data storage for navigator related information</summary>
		protected class ClientConfigurationRouteEntityStaticMetaData : EntityStaticMetaDataBase
		{
			public ClientConfigurationRouteEntityStaticMetaData()
			{
				SetEntityCoreInfo("ClientConfigurationRouteEntity", InheritanceHierarchyType.None, false, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ClientConfigurationRouteEntity, typeof(ClientConfigurationRouteEntity), typeof(ClientConfigurationRouteEntityFactory), false);
				AddNavigatorMetaData<ClientConfigurationRouteEntity, ClientConfigurationEntity>("ClientConfiguration", "ClientConfigurationRouteCollection", (a, b) => a._clientConfiguration = b, a => a._clientConfiguration, (a, b) => a.ClientConfiguration = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticClientConfigurationRouteRelations.ClientConfigurationEntityUsingClientConfigurationIdStatic, ()=>new ClientConfigurationRouteRelations().ClientConfigurationEntityUsingClientConfigurationId, null, new int[] { (int)ClientConfigurationRouteFieldIndex.ClientConfigurationId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ClientConfigurationEntity);
				AddNavigatorMetaData<ClientConfigurationRouteEntity, RouteEntity>("Route", "ClientConfigurationRouteCollection", (a, b) => a._route = b, a => a._route, (a, b) => a.Route = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticClientConfigurationRouteRelations.RouteEntityUsingRouteIdStatic, ()=>new ClientConfigurationRouteRelations().RouteEntityUsingRouteId, null, new int[] { (int)ClientConfigurationRouteFieldIndex.RouteId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.RouteEntity);
			}
		}

		/// <summary>Static ctor</summary>
		static ClientConfigurationRouteEntity()
		{
		}

		/// <summary> CTor</summary>
		public ClientConfigurationRouteEntity()
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public ClientConfigurationRouteEntity(IEntityFields2 fields)
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this ClientConfigurationRouteEntity</param>
		public ClientConfigurationRouteEntity(IValidator validator)
		{
			InitClassEmpty(validator, null);
		}

		/// <summary> CTor</summary>
		/// <param name="clientConfigurationRouteId">PK value for ClientConfigurationRoute which data should be fetched into this ClientConfigurationRoute object</param>
		public ClientConfigurationRouteEntity(System.Int32 clientConfigurationRouteId) : this(clientConfigurationRouteId, null)
		{
		}

		/// <summary> CTor</summary>
		/// <param name="clientConfigurationRouteId">PK value for ClientConfigurationRoute which data should be fetched into this ClientConfigurationRoute object</param>
		/// <param name="validator">The custom validator object for this ClientConfigurationRouteEntity</param>
		public ClientConfigurationRouteEntity(System.Int32 clientConfigurationRouteId, IValidator validator)
		{
			InitClassEmpty(validator, null);
			this.ClientConfigurationRouteId = clientConfigurationRouteId;
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ClientConfigurationRouteEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'ClientConfiguration' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoClientConfiguration() { return CreateRelationInfoForNavigator("ClientConfiguration"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Route' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoRoute() { return CreateRelationInfoForNavigator("Route"); }
		
		/// <inheritdoc/>
		protected override EntityStaticMetaDataBase GetEntityStaticMetaData() {	return _staticMetaData; }

		/// <summary>Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		/// <summary>Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this ClientConfigurationRouteEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary>The relations object holding all relations of this entity with other entity classes.</summary>
		public static ClientConfigurationRouteRelations Relations { get { return _relationsFactory; } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'ClientConfiguration' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathClientConfigurationEntity { get { return _staticMetaData.GetPrefetchPathElement("ClientConfiguration", CommonEntityBase.CreateEntityCollection<ClientConfigurationEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Route' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathRouteEntity { get { return _staticMetaData.GetPrefetchPathElement("Route", CommonEntityBase.CreateEntityCollection<RouteEntity>()); } }

		/// <summary>The ClientConfigurationRouteId property of the Entity ClientConfigurationRoute<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfigurationRoute"."ClientConfigurationRouteId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 ClientConfigurationRouteId
		{
			get { return (System.Int32)GetValue((int)ClientConfigurationRouteFieldIndex.ClientConfigurationRouteId, true); }
			set { SetValue((int)ClientConfigurationRouteFieldIndex.ClientConfigurationRouteId, value); }		}

		/// <summary>The ParentCompanyId property of the Entity ClientConfigurationRoute<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfigurationRoute"."ParentCompanyId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ParentCompanyId
		{
			get { return (System.Int32)GetValue((int)ClientConfigurationRouteFieldIndex.ParentCompanyId, true); }
			set	{ SetValue((int)ClientConfigurationRouteFieldIndex.ParentCompanyId, value); }
		}

		/// <summary>The ClientConfigurationId property of the Entity ClientConfigurationRoute<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfigurationRoute"."ClientConfigurationId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ClientConfigurationId
		{
			get { return (System.Int32)GetValue((int)ClientConfigurationRouteFieldIndex.ClientConfigurationId, true); }
			set	{ SetValue((int)ClientConfigurationRouteFieldIndex.ClientConfigurationId, value); }
		}

		/// <summary>The RouteId property of the Entity ClientConfigurationRoute<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfigurationRoute"."RouteId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 RouteId
		{
			get { return (System.Int32)GetValue((int)ClientConfigurationRouteFieldIndex.RouteId, true); }
			set	{ SetValue((int)ClientConfigurationRouteFieldIndex.RouteId, value); }
		}

		/// <summary>The Type property of the Entity ClientConfigurationRoute<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfigurationRoute"."Type".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Crave.Enums.RouteType Type
		{
			get { return (Crave.Enums.RouteType)GetValue((int)ClientConfigurationRouteFieldIndex.Type, true); }
			set	{ SetValue((int)ClientConfigurationRouteFieldIndex.Type, value); }
		}

		/// <summary>The CreatedUTC property of the Entity ClientConfigurationRoute<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfigurationRoute"."CreatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ClientConfigurationRouteFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)ClientConfigurationRouteFieldIndex.CreatedUTC, value); }
		}

		/// <summary>The CreatedBy property of the Entity ClientConfigurationRoute<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfigurationRoute"."CreatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CreatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)ClientConfigurationRouteFieldIndex.CreatedBy, false); }
			set	{ SetValue((int)ClientConfigurationRouteFieldIndex.CreatedBy, value); }
		}

		/// <summary>The UpdatedUTC property of the Entity ClientConfigurationRoute<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfigurationRoute"."UpdatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ClientConfigurationRouteFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)ClientConfigurationRouteFieldIndex.UpdatedUTC, value); }
		}

		/// <summary>The UpdatedBy property of the Entity ClientConfigurationRoute<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfigurationRoute"."UpdatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UpdatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)ClientConfigurationRouteFieldIndex.UpdatedBy, false); }
			set	{ SetValue((int)ClientConfigurationRouteFieldIndex.UpdatedBy, value); }
		}

		/// <summary>Gets / sets related entity of type 'ClientConfigurationEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual ClientConfigurationEntity ClientConfiguration
		{
			get { return _clientConfiguration; }
			set { SetSingleRelatedEntityNavigator(value, "ClientConfiguration"); }
		}

		/// <summary>Gets / sets related entity of type 'RouteEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual RouteEntity Route
		{
			get { return _route; }
			set { SetSingleRelatedEntityNavigator(value, "Route"); }
		}


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen
{
	public enum ClientConfigurationRouteFieldIndex
	{
		///<summary>ClientConfigurationRouteId. </summary>
		ClientConfigurationRouteId,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>ClientConfigurationId. </summary>
		ClientConfigurationId,
		///<summary>RouteId. </summary>
		RouteId,
		///<summary>Type. </summary>
		Type,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		/// <summary></summary>
		AmountOfFields
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: ClientConfigurationRoute. </summary>
	public partial class ClientConfigurationRouteRelations: RelationFactory
	{
		#region Custom Relation code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomRelationCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		


		/// <summary>Gets all relations and delete rules of the ClientConfigurationRouteEntity as a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects.</summary>
		/// <returns>a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects</returns>
		public virtual Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> GetRelationsAndDeleteRules()
		{
			Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> toReturn = new Dictionary<IEntityRelation, ReferentialConstraintDeleteRule>();
			return toReturn;
		}		
	
	

		/// <summary>Returns a new IEntityRelation object, between ClientConfigurationRouteEntity and ClientConfigurationEntity over the m:1 relation they have, using the relation between the fields: ClientConfigurationRoute.ClientConfigurationId - ClientConfiguration.ClientConfigurationId</summary>
		public virtual IEntityRelation ClientConfigurationEntityUsingClientConfigurationId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "ClientConfiguration", false, new[] { ClientConfigurationFields.ClientConfigurationId, ClientConfigurationRouteFields.ClientConfigurationId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between ClientConfigurationRouteEntity and RouteEntity over the m:1 relation they have, using the relation between the fields: ClientConfigurationRoute.RouteId - Route.RouteId</summary>
		public virtual IEntityRelation RouteEntityUsingRouteId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Route", false, new[] { RouteFields.RouteId, ClientConfigurationRouteFields.RouteId }); }
		}

	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticClientConfigurationRouteRelations
	{
		internal static readonly IEntityRelation ClientConfigurationEntityUsingClientConfigurationIdStatic = new ClientConfigurationRouteRelations().ClientConfigurationEntityUsingClientConfigurationId;
		internal static readonly IEntityRelation RouteEntityUsingRouteIdStatic = new ClientConfigurationRouteRelations().RouteEntityUsingRouteId;

		/// <summary>CTor</summary>
		static StaticClientConfigurationRouteRelations() { }
	}
}

