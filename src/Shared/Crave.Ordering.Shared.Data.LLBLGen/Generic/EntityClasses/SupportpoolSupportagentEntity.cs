﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 5.8
// Code is generated on: 
// Code is generated using templates: Crave.TemplateBindings
// Templates vendor: Crave Interactive
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.FactoryClasses;
using Crave.Ordering.Shared.Data.LLBLGen.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

// ReSharper disable All
namespace Crave.Ordering.Shared.Data.LLBLGen.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'SupportpoolSupportagent'.<br/><br/></summary>
	[Serializable]
	public partial class SupportpoolSupportagentEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END
	
	{
		private SupportagentEntity _supportagent;
		private SupportpoolEntity _supportpool;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		private static SupportpoolSupportagentEntityStaticMetaData _staticMetaData = new SupportpoolSupportagentEntityStaticMetaData();
		private static SupportpoolSupportagentRelations _relationsFactory = new SupportpoolSupportagentRelations();		

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Supportagent</summary>
			public static readonly string Supportagent = "Supportagent";
			/// <summary>Member name Supportpool</summary>
			public static readonly string Supportpool = "Supportpool";
		}

		/// <summary>Static meta-data storage for navigator related information</summary>
		protected class SupportpoolSupportagentEntityStaticMetaData : EntityStaticMetaDataBase
		{
			public SupportpoolSupportagentEntityStaticMetaData()
			{
				SetEntityCoreInfo("SupportpoolSupportagentEntity", InheritanceHierarchyType.None, false, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.SupportpoolSupportagentEntity, typeof(SupportpoolSupportagentEntity), typeof(SupportpoolSupportagentEntityFactory), false);
				AddNavigatorMetaData<SupportpoolSupportagentEntity, SupportagentEntity>("Supportagent", "SupportpoolSupportagentCollection", (a, b) => a._supportagent = b, a => a._supportagent, (a, b) => a.Supportagent = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticSupportpoolSupportagentRelations.SupportagentEntityUsingSupportagentIdStatic, ()=>new SupportpoolSupportagentRelations().SupportagentEntityUsingSupportagentId, null, new int[] { (int)SupportpoolSupportagentFieldIndex.SupportagentId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.SupportagentEntity);
				AddNavigatorMetaData<SupportpoolSupportagentEntity, SupportpoolEntity>("Supportpool", "SupportpoolSupportagentCollection", (a, b) => a._supportpool = b, a => a._supportpool, (a, b) => a.Supportpool = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticSupportpoolSupportagentRelations.SupportpoolEntityUsingSupportpoolIdStatic, ()=>new SupportpoolSupportagentRelations().SupportpoolEntityUsingSupportpoolId, null, new int[] { (int)SupportpoolSupportagentFieldIndex.SupportpoolId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.SupportpoolEntity);
			}
		}

		/// <summary>Static ctor</summary>
		static SupportpoolSupportagentEntity()
		{
		}

		/// <summary> CTor</summary>
		public SupportpoolSupportagentEntity()
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public SupportpoolSupportagentEntity(IEntityFields2 fields)
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this SupportpoolSupportagentEntity</param>
		public SupportpoolSupportagentEntity(IValidator validator)
		{
			InitClassEmpty(validator, null);
		}

		/// <summary> CTor</summary>
		/// <param name="supportpoolSupportagentId">PK value for SupportpoolSupportagent which data should be fetched into this SupportpoolSupportagent object</param>
		public SupportpoolSupportagentEntity(System.Int32 supportpoolSupportagentId) : this(supportpoolSupportagentId, null)
		{
		}

		/// <summary> CTor</summary>
		/// <param name="supportpoolSupportagentId">PK value for SupportpoolSupportagent which data should be fetched into this SupportpoolSupportagent object</param>
		/// <param name="validator">The custom validator object for this SupportpoolSupportagentEntity</param>
		public SupportpoolSupportagentEntity(System.Int32 supportpoolSupportagentId, IValidator validator)
		{
			InitClassEmpty(validator, null);
			this.SupportpoolSupportagentId = supportpoolSupportagentId;
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected SupportpoolSupportagentEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary>Method which will construct a filter (predicate expression) for the unique constraint defined on the fields: SupportpoolId , SupportagentId .</summary>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public IPredicateExpression ConstructFilterForUCSupportpoolIdSupportagentId()
		{
			var filter = new PredicateExpression();
			filter.Add(Crave.Ordering.Shared.Data.LLBLGen.HelperClasses.SupportpoolSupportagentFields.SupportpoolId == this.Fields.GetCurrentValue((int)SupportpoolSupportagentFieldIndex.SupportpoolId));
			filter.Add(Crave.Ordering.Shared.Data.LLBLGen.HelperClasses.SupportpoolSupportagentFields.SupportagentId == this.Fields.GetCurrentValue((int)SupportpoolSupportagentFieldIndex.SupportagentId));
 			return filter;
		}

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Supportagent' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoSupportagent() { return CreateRelationInfoForNavigator("Supportagent"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Supportpool' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoSupportpool() { return CreateRelationInfoForNavigator("Supportpool"); }
		
		/// <inheritdoc/>
		protected override EntityStaticMetaDataBase GetEntityStaticMetaData() {	return _staticMetaData; }

		/// <summary>Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		/// <summary>Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this SupportpoolSupportagentEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary>The relations object holding all relations of this entity with other entity classes.</summary>
		public static SupportpoolSupportagentRelations Relations { get { return _relationsFactory; } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Supportagent' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathSupportagentEntity { get { return _staticMetaData.GetPrefetchPathElement("Supportagent", CommonEntityBase.CreateEntityCollection<SupportagentEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Supportpool' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathSupportpoolEntity { get { return _staticMetaData.GetPrefetchPathElement("Supportpool", CommonEntityBase.CreateEntityCollection<SupportpoolEntity>()); } }

		/// <summary>The SupportpoolSupportagentId property of the Entity SupportpoolSupportagent<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SupportpoolSupportagent"."SupportpoolSupportagentId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 SupportpoolSupportagentId
		{
			get { return (System.Int32)GetValue((int)SupportpoolSupportagentFieldIndex.SupportpoolSupportagentId, true); }
			set { SetValue((int)SupportpoolSupportagentFieldIndex.SupportpoolSupportagentId, value); }		}

		/// <summary>The SupportpoolId property of the Entity SupportpoolSupportagent<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SupportpoolSupportagent"."SupportpoolId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 SupportpoolId
		{
			get { return (System.Int32)GetValue((int)SupportpoolSupportagentFieldIndex.SupportpoolId, true); }
			set	{ SetValue((int)SupportpoolSupportagentFieldIndex.SupportpoolId, value); }
		}

		/// <summary>The SupportagentId property of the Entity SupportpoolSupportagent<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SupportpoolSupportagent"."SupportagentId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 SupportagentId
		{
			get { return (System.Int32)GetValue((int)SupportpoolSupportagentFieldIndex.SupportagentId, true); }
			set	{ SetValue((int)SupportpoolSupportagentFieldIndex.SupportagentId, value); }
		}

		/// <summary>The OnSupport property of the Entity SupportpoolSupportagent<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SupportpoolSupportagent"."OnSupport".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean OnSupport
		{
			get { return (System.Boolean)GetValue((int)SupportpoolSupportagentFieldIndex.OnSupport, true); }
			set	{ SetValue((int)SupportpoolSupportagentFieldIndex.OnSupport, value); }
		}

		/// <summary>The NotifyOfflineTerminals property of the Entity SupportpoolSupportagent<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SupportpoolSupportagent"."NotifyOfflineTerminals".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean NotifyOfflineTerminals
		{
			get { return (System.Boolean)GetValue((int)SupportpoolSupportagentFieldIndex.NotifyOfflineTerminals, true); }
			set	{ SetValue((int)SupportpoolSupportagentFieldIndex.NotifyOfflineTerminals, value); }
		}

		/// <summary>The NotifyOfflineClients property of the Entity SupportpoolSupportagent<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SupportpoolSupportagent"."NotifyOfflineClients".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean NotifyOfflineClients
		{
			get { return (System.Boolean)GetValue((int)SupportpoolSupportagentFieldIndex.NotifyOfflineClients, true); }
			set	{ SetValue((int)SupportpoolSupportagentFieldIndex.NotifyOfflineClients, value); }
		}

		/// <summary>The NotifyUnprocessableOrders property of the Entity SupportpoolSupportagent<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SupportpoolSupportagent"."NotifyUnprocessableOrders".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean NotifyUnprocessableOrders
		{
			get { return (System.Boolean)GetValue((int)SupportpoolSupportagentFieldIndex.NotifyUnprocessableOrders, true); }
			set	{ SetValue((int)SupportpoolSupportagentFieldIndex.NotifyUnprocessableOrders, value); }
		}

		/// <summary>The NotifyExpiredSteps property of the Entity SupportpoolSupportagent<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SupportpoolSupportagent"."NotifyExpiredSteps".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean NotifyExpiredSteps
		{
			get { return (System.Boolean)GetValue((int)SupportpoolSupportagentFieldIndex.NotifyExpiredSteps, true); }
			set	{ SetValue((int)SupportpoolSupportagentFieldIndex.NotifyExpiredSteps, value); }
		}

		/// <summary>The NotifyTooMuchOfflineClientsJump property of the Entity SupportpoolSupportagent<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SupportpoolSupportagent"."NotifyTooMuchOfflineClientsJump".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean NotifyTooMuchOfflineClientsJump
		{
			get { return (System.Boolean)GetValue((int)SupportpoolSupportagentFieldIndex.NotifyTooMuchOfflineClientsJump, true); }
			set	{ SetValue((int)SupportpoolSupportagentFieldIndex.NotifyTooMuchOfflineClientsJump, value); }
		}

		/// <summary>The NotifyBouncedEmails property of the Entity SupportpoolSupportagent<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SupportpoolSupportagent"."NotifyBouncedEmails".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean NotifyBouncedEmails
		{
			get { return (System.Boolean)GetValue((int)SupportpoolSupportagentFieldIndex.NotifyBouncedEmails, true); }
			set	{ SetValue((int)SupportpoolSupportagentFieldIndex.NotifyBouncedEmails, value); }
		}

		/// <summary>The CreatedUTC property of the Entity SupportpoolSupportagent<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SupportpoolSupportagent"."CreatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)SupportpoolSupportagentFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)SupportpoolSupportagentFieldIndex.CreatedUTC, value); }
		}

		/// <summary>The CreatedBy property of the Entity SupportpoolSupportagent<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SupportpoolSupportagent"."CreatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CreatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)SupportpoolSupportagentFieldIndex.CreatedBy, false); }
			set	{ SetValue((int)SupportpoolSupportagentFieldIndex.CreatedBy, value); }
		}

		/// <summary>The UpdatedUTC property of the Entity SupportpoolSupportagent<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SupportpoolSupportagent"."UpdatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)SupportpoolSupportagentFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)SupportpoolSupportagentFieldIndex.UpdatedUTC, value); }
		}

		/// <summary>The UpdatedBy property of the Entity SupportpoolSupportagent<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SupportpoolSupportagent"."UpdatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UpdatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)SupportpoolSupportagentFieldIndex.UpdatedBy, false); }
			set	{ SetValue((int)SupportpoolSupportagentFieldIndex.UpdatedBy, value); }
		}

		/// <summary>Gets / sets related entity of type 'SupportagentEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual SupportagentEntity Supportagent
		{
			get { return _supportagent; }
			set { SetSingleRelatedEntityNavigator(value, "Supportagent"); }
		}

		/// <summary>Gets / sets related entity of type 'SupportpoolEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual SupportpoolEntity Supportpool
		{
			get { return _supportpool; }
			set { SetSingleRelatedEntityNavigator(value, "Supportpool"); }
		}


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen
{
	public enum SupportpoolSupportagentFieldIndex
	{
		///<summary>SupportpoolSupportagentId. </summary>
		SupportpoolSupportagentId,
		///<summary>SupportpoolId. </summary>
		SupportpoolId,
		///<summary>SupportagentId. </summary>
		SupportagentId,
		///<summary>OnSupport. </summary>
		OnSupport,
		///<summary>NotifyOfflineTerminals. </summary>
		NotifyOfflineTerminals,
		///<summary>NotifyOfflineClients. </summary>
		NotifyOfflineClients,
		///<summary>NotifyUnprocessableOrders. </summary>
		NotifyUnprocessableOrders,
		///<summary>NotifyExpiredSteps. </summary>
		NotifyExpiredSteps,
		///<summary>NotifyTooMuchOfflineClientsJump. </summary>
		NotifyTooMuchOfflineClientsJump,
		///<summary>NotifyBouncedEmails. </summary>
		NotifyBouncedEmails,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		/// <summary></summary>
		AmountOfFields
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: SupportpoolSupportagent. </summary>
	public partial class SupportpoolSupportagentRelations: RelationFactory
	{
		#region Custom Relation code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomRelationCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		


		/// <summary>Gets all relations and delete rules of the SupportpoolSupportagentEntity as a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects.</summary>
		/// <returns>a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects</returns>
		public virtual Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> GetRelationsAndDeleteRules()
		{
			Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> toReturn = new Dictionary<IEntityRelation, ReferentialConstraintDeleteRule>();
			return toReturn;
		}		
	
	

		/// <summary>Returns a new IEntityRelation object, between SupportpoolSupportagentEntity and SupportagentEntity over the m:1 relation they have, using the relation between the fields: SupportpoolSupportagent.SupportagentId - Supportagent.SupportagentId</summary>
		public virtual IEntityRelation SupportagentEntityUsingSupportagentId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Supportagent", false, new[] { SupportagentFields.SupportagentId, SupportpoolSupportagentFields.SupportagentId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between SupportpoolSupportagentEntity and SupportpoolEntity over the m:1 relation they have, using the relation between the fields: SupportpoolSupportagent.SupportpoolId - Supportpool.SupportpoolId</summary>
		public virtual IEntityRelation SupportpoolEntityUsingSupportpoolId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Supportpool", false, new[] { SupportpoolFields.SupportpoolId, SupportpoolSupportagentFields.SupportpoolId }); }
		}

	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticSupportpoolSupportagentRelations
	{
		internal static readonly IEntityRelation SupportagentEntityUsingSupportagentIdStatic = new SupportpoolSupportagentRelations().SupportagentEntityUsingSupportagentId;
		internal static readonly IEntityRelation SupportpoolEntityUsingSupportpoolIdStatic = new SupportpoolSupportagentRelations().SupportpoolEntityUsingSupportpoolId;

		/// <summary>CTor</summary>
		static StaticSupportpoolSupportagentRelations() { }
	}
}

