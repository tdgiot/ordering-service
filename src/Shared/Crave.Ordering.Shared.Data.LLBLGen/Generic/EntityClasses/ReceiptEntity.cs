﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 5.8
// Code is generated on: 
// Code is generated using templates: Crave.TemplateBindings
// Templates vendor: Crave Interactive
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.FactoryClasses;
using Crave.Ordering.Shared.Data.LLBLGen.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

// ReSharper disable All
namespace Crave.Ordering.Shared.Data.LLBLGen.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'Receipt'.<br/><br/></summary>
	[Serializable]
	public partial class ReceiptEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END
	
	{
		private CompanyEntity _company;
		private OrderEntity _order;
		private OutletSellerInformationEntity _outletSellerInformation;
		private ReceiptTemplateEntity _receiptTemplate;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		private static ReceiptEntityStaticMetaData _staticMetaData = new ReceiptEntityStaticMetaData();
		private static ReceiptRelations _relationsFactory = new ReceiptRelations();		

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Company</summary>
			public static readonly string Company = "Company";
			/// <summary>Member name Order</summary>
			public static readonly string Order = "Order";
			/// <summary>Member name OutletSellerInformation</summary>
			public static readonly string OutletSellerInformation = "OutletSellerInformation";
			/// <summary>Member name ReceiptTemplate</summary>
			public static readonly string ReceiptTemplate = "ReceiptTemplate";
		}

		/// <summary>Static meta-data storage for navigator related information</summary>
		protected class ReceiptEntityStaticMetaData : EntityStaticMetaDataBase
		{
			public ReceiptEntityStaticMetaData()
			{
				SetEntityCoreInfo("ReceiptEntity", InheritanceHierarchyType.None, false, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ReceiptEntity, typeof(ReceiptEntity), typeof(ReceiptEntityFactory), false);
				AddNavigatorMetaData<ReceiptEntity, CompanyEntity>("Company", "ReceiptCollection", (a, b) => a._company = b, a => a._company, (a, b) => a.Company = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticReceiptRelations.CompanyEntityUsingCompanyIdStatic, ()=>new ReceiptRelations().CompanyEntityUsingCompanyId, null, new int[] { (int)ReceiptFieldIndex.CompanyId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.CompanyEntity);
				AddNavigatorMetaData<ReceiptEntity, OrderEntity>("Order", "ReceiptCollection", (a, b) => a._order = b, a => a._order, (a, b) => a.Order = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticReceiptRelations.OrderEntityUsingOrderIdStatic, ()=>new ReceiptRelations().OrderEntityUsingOrderId, null, new int[] { (int)ReceiptFieldIndex.OrderId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.OrderEntity);
				AddNavigatorMetaData<ReceiptEntity, OutletSellerInformationEntity>("OutletSellerInformation", "ReceiptCollection", (a, b) => a._outletSellerInformation = b, a => a._outletSellerInformation, (a, b) => a.OutletSellerInformation = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticReceiptRelations.OutletSellerInformationEntityUsingOutletSellerInformationIdStatic, ()=>new ReceiptRelations().OutletSellerInformationEntityUsingOutletSellerInformationId, null, new int[] { (int)ReceiptFieldIndex.OutletSellerInformationId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.OutletSellerInformationEntity);
				AddNavigatorMetaData<ReceiptEntity, ReceiptTemplateEntity>("ReceiptTemplate", "ReceiptCollection", (a, b) => a._receiptTemplate = b, a => a._receiptTemplate, (a, b) => a.ReceiptTemplate = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticReceiptRelations.ReceiptTemplateEntityUsingReceiptTemplateIdStatic, ()=>new ReceiptRelations().ReceiptTemplateEntityUsingReceiptTemplateId, null, new int[] { (int)ReceiptFieldIndex.ReceiptTemplateId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ReceiptTemplateEntity);
			}
		}

		/// <summary>Static ctor</summary>
		static ReceiptEntity()
		{
		}

		/// <summary> CTor</summary>
		public ReceiptEntity()
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public ReceiptEntity(IEntityFields2 fields)
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this ReceiptEntity</param>
		public ReceiptEntity(IValidator validator)
		{
			InitClassEmpty(validator, null);
		}

		/// <summary> CTor</summary>
		/// <param name="receiptId">PK value for Receipt which data should be fetched into this Receipt object</param>
		public ReceiptEntity(System.Int32 receiptId) : this(receiptId, null)
		{
		}

		/// <summary> CTor</summary>
		/// <param name="receiptId">PK value for Receipt which data should be fetched into this Receipt object</param>
		/// <param name="validator">The custom validator object for this ReceiptEntity</param>
		public ReceiptEntity(System.Int32 receiptId, IValidator validator)
		{
			InitClassEmpty(validator, null);
			this.ReceiptId = receiptId;
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ReceiptEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Company' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoCompany() { return CreateRelationInfoForNavigator("Company"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Order' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoOrder() { return CreateRelationInfoForNavigator("Order"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'OutletSellerInformation' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoOutletSellerInformation() { return CreateRelationInfoForNavigator("OutletSellerInformation"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'ReceiptTemplate' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoReceiptTemplate() { return CreateRelationInfoForNavigator("ReceiptTemplate"); }
		
		/// <inheritdoc/>
		protected override EntityStaticMetaDataBase GetEntityStaticMetaData() {	return _staticMetaData; }

		/// <summary>Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		/// <summary>Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this ReceiptEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary>The relations object holding all relations of this entity with other entity classes.</summary>
		public static ReceiptRelations Relations { get { return _relationsFactory; } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Company' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathCompanyEntity { get { return _staticMetaData.GetPrefetchPathElement("Company", CommonEntityBase.CreateEntityCollection<CompanyEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Order' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathOrderEntity { get { return _staticMetaData.GetPrefetchPathElement("Order", CommonEntityBase.CreateEntityCollection<OrderEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'OutletSellerInformation' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathOutletSellerInformationEntity { get { return _staticMetaData.GetPrefetchPathElement("OutletSellerInformation", CommonEntityBase.CreateEntityCollection<OutletSellerInformationEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'ReceiptTemplate' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathReceiptTemplateEntity { get { return _staticMetaData.GetPrefetchPathElement("ReceiptTemplate", CommonEntityBase.CreateEntityCollection<ReceiptTemplateEntity>()); } }

		/// <summary>The ReceiptId property of the Entity Receipt<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Receipt"."ReceiptId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 ReceiptId
		{
			get { return (System.Int32)GetValue((int)ReceiptFieldIndex.ReceiptId, true); }
			set { SetValue((int)ReceiptFieldIndex.ReceiptId, value); }		}

		/// <summary>The ReceiptTemplateId property of the Entity Receipt<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Receipt"."ReceiptTemplateId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ReceiptTemplateId
		{
			get { return (Nullable<System.Int32>)GetValue((int)ReceiptFieldIndex.ReceiptTemplateId, false); }
			set	{ SetValue((int)ReceiptFieldIndex.ReceiptTemplateId, value); }
		}

		/// <summary>The CompanyId property of the Entity Receipt<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Receipt"."CompanyId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CompanyId
		{
			get { return (System.Int32)GetValue((int)ReceiptFieldIndex.CompanyId, true); }
			set	{ SetValue((int)ReceiptFieldIndex.CompanyId, value); }
		}

		/// <summary>The OrderId property of the Entity Receipt<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Receipt"."OrderId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 OrderId
		{
			get { return (System.Int32)GetValue((int)ReceiptFieldIndex.OrderId, true); }
			set	{ SetValue((int)ReceiptFieldIndex.OrderId, value); }
		}

		/// <summary>The Number property of the Entity Receipt<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Receipt"."Number".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 Number
		{
			get { return (System.Int32)GetValue((int)ReceiptFieldIndex.Number, true); }
			set	{ SetValue((int)ReceiptFieldIndex.Number, value); }
		}

		/// <summary>The SellerName property of the Entity Receipt<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Receipt"."SellerName".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String SellerName
		{
			get { return (System.String)GetValue((int)ReceiptFieldIndex.SellerName, true); }
			set	{ SetValue((int)ReceiptFieldIndex.SellerName, value); }
		}

		/// <summary>The SellerAddress property of the Entity Receipt<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Receipt"."SellerAddress".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String SellerAddress
		{
			get { return (System.String)GetValue((int)ReceiptFieldIndex.SellerAddress, true); }
			set	{ SetValue((int)ReceiptFieldIndex.SellerAddress, value); }
		}

		/// <summary>The SellerContactInfo property of the Entity Receipt<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Receipt"."SellerContactInfo".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String SellerContactInfo
		{
			get { return (System.String)GetValue((int)ReceiptFieldIndex.SellerContactInfo, true); }
			set	{ SetValue((int)ReceiptFieldIndex.SellerContactInfo, value); }
		}

		/// <summary>The ServiceMethodName property of the Entity Receipt<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Receipt"."ServiceMethodName".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 100.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String ServiceMethodName
		{
			get { return (System.String)GetValue((int)ReceiptFieldIndex.ServiceMethodName, true); }
			set	{ SetValue((int)ReceiptFieldIndex.ServiceMethodName, value); }
		}

		/// <summary>The CheckoutMethodName property of the Entity Receipt<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Receipt"."CheckoutMethodName".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 100.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String CheckoutMethodName
		{
			get { return (System.String)GetValue((int)ReceiptFieldIndex.CheckoutMethodName, true); }
			set	{ SetValue((int)ReceiptFieldIndex.CheckoutMethodName, value); }
		}

		/// <summary>The CreatedBy property of the Entity Receipt<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Receipt"."CreatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)ReceiptFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)ReceiptFieldIndex.CreatedBy, value); }
		}

		/// <summary>The UpdatedBy property of the Entity Receipt<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Receipt"."UpdatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)ReceiptFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)ReceiptFieldIndex.UpdatedBy, value); }
		}

		/// <summary>The CreatedUTC property of the Entity Receipt<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Receipt"."CreatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ReceiptFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)ReceiptFieldIndex.CreatedUTC, value); }
		}

		/// <summary>The UpdatedUTC property of the Entity Receipt<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Receipt"."UpdatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ReceiptFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)ReceiptFieldIndex.UpdatedUTC, value); }
		}

		/// <summary>The TaxBreakdown property of the Entity Receipt<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Receipt"."TaxBreakdown".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Crave.Enums.TaxBreakdown TaxBreakdown
		{
			get { return (Crave.Enums.TaxBreakdown)GetValue((int)ReceiptFieldIndex.TaxBreakdown, true); }
			set	{ SetValue((int)ReceiptFieldIndex.TaxBreakdown, value); }
		}

		/// <summary>The Email property of the Entity Receipt<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Receipt"."Email".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2000.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Email
		{
			get { return (System.String)GetValue((int)ReceiptFieldIndex.Email, true); }
			set	{ SetValue((int)ReceiptFieldIndex.Email, value); }
		}

		/// <summary>The Phonenumber property of the Entity Receipt<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Receipt"."Phonenumber".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2000.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Phonenumber
		{
			get { return (System.String)GetValue((int)ReceiptFieldIndex.Phonenumber, true); }
			set	{ SetValue((int)ReceiptFieldIndex.Phonenumber, value); }
		}

		/// <summary>The VatNumber property of the Entity Receipt<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Receipt"."VatNumber".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String VatNumber
		{
			get { return (System.String)GetValue((int)ReceiptFieldIndex.VatNumber, true); }
			set	{ SetValue((int)ReceiptFieldIndex.VatNumber, value); }
		}

		/// <summary>The PaymentProcessorInfo property of the Entity Receipt<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Receipt"."PaymentProcessorInfo".<br/>Table field type characteristics (type, precision, scale, length): Text, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PaymentProcessorInfo
		{
			get { return (System.String)GetValue((int)ReceiptFieldIndex.PaymentProcessorInfo, true); }
			set	{ SetValue((int)ReceiptFieldIndex.PaymentProcessorInfo, value); }
		}

		/// <summary>The SellerContactEmail property of the Entity Receipt<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Receipt"."SellerContactEmail".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 250.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String SellerContactEmail
		{
			get { return (System.String)GetValue((int)ReceiptFieldIndex.SellerContactEmail, true); }
			set	{ SetValue((int)ReceiptFieldIndex.SellerContactEmail, value); }
		}

		/// <summary>The OutletSellerInformationId property of the Entity Receipt<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Receipt"."OutletSellerInformationId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> OutletSellerInformationId
		{
			get { return (Nullable<System.Int32>)GetValue((int)ReceiptFieldIndex.OutletSellerInformationId, false); }
			set	{ SetValue((int)ReceiptFieldIndex.OutletSellerInformationId, value); }
		}

		/// <summary>Gets / sets related entity of type 'CompanyEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual CompanyEntity Company
		{
			get { return _company; }
			set { SetSingleRelatedEntityNavigator(value, "Company"); }
		}

		/// <summary>Gets / sets related entity of type 'OrderEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual OrderEntity Order
		{
			get { return _order; }
			set { SetSingleRelatedEntityNavigator(value, "Order"); }
		}

		/// <summary>Gets / sets related entity of type 'OutletSellerInformationEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual OutletSellerInformationEntity OutletSellerInformation
		{
			get { return _outletSellerInformation; }
			set { SetSingleRelatedEntityNavigator(value, "OutletSellerInformation"); }
		}

		/// <summary>Gets / sets related entity of type 'ReceiptTemplateEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual ReceiptTemplateEntity ReceiptTemplate
		{
			get { return _receiptTemplate; }
			set { SetSingleRelatedEntityNavigator(value, "ReceiptTemplate"); }
		}


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen
{
	public enum ReceiptFieldIndex
	{
		///<summary>ReceiptId. </summary>
		ReceiptId,
		///<summary>ReceiptTemplateId. </summary>
		ReceiptTemplateId,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>OrderId. </summary>
		OrderId,
		///<summary>Number. </summary>
		Number,
		///<summary>SellerName. </summary>
		SellerName,
		///<summary>SellerAddress. </summary>
		SellerAddress,
		///<summary>SellerContactInfo. </summary>
		SellerContactInfo,
		///<summary>ServiceMethodName. </summary>
		ServiceMethodName,
		///<summary>CheckoutMethodName. </summary>
		CheckoutMethodName,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>TaxBreakdown. </summary>
		TaxBreakdown,
		///<summary>Email. </summary>
		Email,
		///<summary>Phonenumber. </summary>
		Phonenumber,
		///<summary>VatNumber. </summary>
		VatNumber,
		///<summary>PaymentProcessorInfo. </summary>
		PaymentProcessorInfo,
		///<summary>SellerContactEmail. </summary>
		SellerContactEmail,
		///<summary>OutletSellerInformationId. </summary>
		OutletSellerInformationId,
		/// <summary></summary>
		AmountOfFields
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Receipt. </summary>
	public partial class ReceiptRelations: RelationFactory
	{
		#region Custom Relation code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomRelationCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		


		/// <summary>Gets all relations and delete rules of the ReceiptEntity as a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects.</summary>
		/// <returns>a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects</returns>
		public virtual Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> GetRelationsAndDeleteRules()
		{
			Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> toReturn = new Dictionary<IEntityRelation, ReferentialConstraintDeleteRule>();
			return toReturn;
		}		
	
	

		/// <summary>Returns a new IEntityRelation object, between ReceiptEntity and CompanyEntity over the m:1 relation they have, using the relation between the fields: Receipt.CompanyId - Company.CompanyId</summary>
		public virtual IEntityRelation CompanyEntityUsingCompanyId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Company", false, new[] { CompanyFields.CompanyId, ReceiptFields.CompanyId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between ReceiptEntity and OrderEntity over the m:1 relation they have, using the relation between the fields: Receipt.OrderId - Order.OrderId</summary>
		public virtual IEntityRelation OrderEntityUsingOrderId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Order", false, new[] { OrderFields.OrderId, ReceiptFields.OrderId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between ReceiptEntity and OutletSellerInformationEntity over the m:1 relation they have, using the relation between the fields: Receipt.OutletSellerInformationId - OutletSellerInformation.OutletSellerInformationId</summary>
		public virtual IEntityRelation OutletSellerInformationEntityUsingOutletSellerInformationId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "OutletSellerInformation", false, new[] { OutletSellerInformationFields.OutletSellerInformationId, ReceiptFields.OutletSellerInformationId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between ReceiptEntity and ReceiptTemplateEntity over the m:1 relation they have, using the relation between the fields: Receipt.ReceiptTemplateId - ReceiptTemplate.ReceiptTemplateId</summary>
		public virtual IEntityRelation ReceiptTemplateEntityUsingReceiptTemplateId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "ReceiptTemplate", false, new[] { ReceiptTemplateFields.ReceiptTemplateId, ReceiptFields.ReceiptTemplateId }); }
		}

	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticReceiptRelations
	{
		internal static readonly IEntityRelation CompanyEntityUsingCompanyIdStatic = new ReceiptRelations().CompanyEntityUsingCompanyId;
		internal static readonly IEntityRelation OrderEntityUsingOrderIdStatic = new ReceiptRelations().OrderEntityUsingOrderId;
		internal static readonly IEntityRelation OutletSellerInformationEntityUsingOutletSellerInformationIdStatic = new ReceiptRelations().OutletSellerInformationEntityUsingOutletSellerInformationId;
		internal static readonly IEntityRelation ReceiptTemplateEntityUsingReceiptTemplateIdStatic = new ReceiptRelations().ReceiptTemplateEntityUsingReceiptTemplateId;

		/// <summary>CTor</summary>
		static StaticReceiptRelations() { }
	}
}

