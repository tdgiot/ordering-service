﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 5.8
// Code is generated on: 
// Code is generated using templates: Crave.TemplateBindings
// Templates vendor: Crave Interactive
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.FactoryClasses;
using Crave.Ordering.Shared.Data.LLBLGen.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

// ReSharper disable All
namespace Crave.Ordering.Shared.Data.LLBLGen.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'Product'.<br/><br/></summary>
	[Serializable]
	public partial class ProductEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END
	
	{
		private EntityCollection<AlterationoptionEntity> _alterationoptionCollection;
		private EntityCollection<AlterationProductEntity> _alterationProductCollection;
		private EntityCollection<CategoryEntity> _categoryCollection;
		private EntityCollection<DeliverypointgroupEntity> _deliverypointgroupCollection;
		private EntityCollection<MediaEntity> _mediaCollection;
		private EntityCollection<MediaEntity> _mediaCollection1;
		private EntityCollection<OrderitemEntity> _orderitemCollection;
		private EntityCollection<OrderitemAlterationitemEntity> _orderitemAlterationitemCollection;
		private EntityCollection<OrderitemTagEntity> _orderitemTagCollection;
		private EntityCollection<OutletEntity> _outletCollection__;
		private EntityCollection<OutletEntity> _outletCollection_;
		private EntityCollection<OutletEntity> _outletCollection;
		private EntityCollection<PriceLevelItemEntity> _priceLevelItemCollection;
		private EntityCollection<ProductEntity> _brandProductCollection;
		private EntityCollection<ProductAlterationEntity> _productAlterationCollection;
		private EntityCollection<ProductCategoryEntity> _productCategoryCollection;
		private EntityCollection<ProductTagEntity> _productTagCollection;
		private EntityCollection<ServiceMethodEntity> _serviceMethodCollection;
		private EntityCollection<TerminalEntity> _terminalCollection1;
		private EntityCollection<TerminalEntity> _terminalCollection2;
		private EntityCollection<TerminalEntity> _terminalCollection3;
		private EntityCollection<TerminalEntity> _terminalCollection;
		private EntityCollection<CategoryEntity> _categoryCollectionViaProductCategory;
		private CompanyEntity _company;
		private ExternalProductEntity _externalProduct;
		private ProductEntity _brandProduct;
		private RouteEntity _route;
		private ScheduleEntity _schedule;
		private TaxTariffEntity _taxTariff;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		private static ProductEntityStaticMetaData _staticMetaData = new ProductEntityStaticMetaData();
		private static ProductRelations _relationsFactory = new ProductRelations();		

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Company</summary>
			public static readonly string Company = "Company";
			/// <summary>Member name ExternalProduct</summary>
			public static readonly string ExternalProduct = "ExternalProduct";
			/// <summary>Member name BrandProduct</summary>
			public static readonly string BrandProduct = "BrandProduct";
			/// <summary>Member name Route</summary>
			public static readonly string Route = "Route";
			/// <summary>Member name Schedule</summary>
			public static readonly string Schedule = "Schedule";
			/// <summary>Member name TaxTariff</summary>
			public static readonly string TaxTariff = "TaxTariff";
			/// <summary>Member name AlterationoptionCollection</summary>
			public static readonly string AlterationoptionCollection = "AlterationoptionCollection";
			/// <summary>Member name AlterationProductCollection</summary>
			public static readonly string AlterationProductCollection = "AlterationProductCollection";
			/// <summary>Member name CategoryCollection</summary>
			public static readonly string CategoryCollection = "CategoryCollection";
			/// <summary>Member name DeliverypointgroupCollection</summary>
			public static readonly string DeliverypointgroupCollection = "DeliverypointgroupCollection";
			/// <summary>Member name MediaCollection</summary>
			public static readonly string MediaCollection = "MediaCollection";
			/// <summary>Member name MediaCollection1</summary>
			public static readonly string MediaCollection1 = "MediaCollection1";
			/// <summary>Member name OrderitemCollection</summary>
			public static readonly string OrderitemCollection = "OrderitemCollection";
			/// <summary>Member name OrderitemAlterationitemCollection</summary>
			public static readonly string OrderitemAlterationitemCollection = "OrderitemAlterationitemCollection";
			/// <summary>Member name OrderitemTagCollection</summary>
			public static readonly string OrderitemTagCollection = "OrderitemTagCollection";
			/// <summary>Member name OutletCollection__</summary>
			public static readonly string OutletCollection__ = "OutletCollection__";
			/// <summary>Member name OutletCollection_</summary>
			public static readonly string OutletCollection_ = "OutletCollection_";
			/// <summary>Member name OutletCollection</summary>
			public static readonly string OutletCollection = "OutletCollection";
			/// <summary>Member name PriceLevelItemCollection</summary>
			public static readonly string PriceLevelItemCollection = "PriceLevelItemCollection";
			/// <summary>Member name BrandProductCollection</summary>
			public static readonly string BrandProductCollection = "BrandProductCollection";
			/// <summary>Member name ProductAlterationCollection</summary>
			public static readonly string ProductAlterationCollection = "ProductAlterationCollection";
			/// <summary>Member name ProductCategoryCollection</summary>
			public static readonly string ProductCategoryCollection = "ProductCategoryCollection";
			/// <summary>Member name ProductTagCollection</summary>
			public static readonly string ProductTagCollection = "ProductTagCollection";
			/// <summary>Member name ServiceMethodCollection</summary>
			public static readonly string ServiceMethodCollection = "ServiceMethodCollection";
			/// <summary>Member name TerminalCollection1</summary>
			public static readonly string TerminalCollection1 = "TerminalCollection1";
			/// <summary>Member name TerminalCollection2</summary>
			public static readonly string TerminalCollection2 = "TerminalCollection2";
			/// <summary>Member name TerminalCollection3</summary>
			public static readonly string TerminalCollection3 = "TerminalCollection3";
			/// <summary>Member name TerminalCollection</summary>
			public static readonly string TerminalCollection = "TerminalCollection";
			/// <summary>Member name CategoryCollectionViaProductCategory</summary>
			public static readonly string CategoryCollectionViaProductCategory = "CategoryCollectionViaProductCategory";
		}

		/// <summary>Static meta-data storage for navigator related information</summary>
		protected class ProductEntityStaticMetaData : EntityStaticMetaDataBase
		{
			public ProductEntityStaticMetaData()
			{
				SetEntityCoreInfo("ProductEntity", InheritanceHierarchyType.None, false, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ProductEntity, typeof(ProductEntity), typeof(ProductEntityFactory), false);
				AddNavigatorMetaData<ProductEntity, EntityCollection<AlterationoptionEntity>>("AlterationoptionCollection", a => a._alterationoptionCollection, (a, b) => a._alterationoptionCollection = b, a => a.AlterationoptionCollection, () => new ProductRelations().AlterationoptionEntityUsingProductId, typeof(AlterationoptionEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.AlterationoptionEntity);
				AddNavigatorMetaData<ProductEntity, EntityCollection<AlterationProductEntity>>("AlterationProductCollection", a => a._alterationProductCollection, (a, b) => a._alterationProductCollection = b, a => a.AlterationProductCollection, () => new ProductRelations().AlterationProductEntityUsingProductId, typeof(AlterationProductEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.AlterationProductEntity);
				AddNavigatorMetaData<ProductEntity, EntityCollection<CategoryEntity>>("CategoryCollection", a => a._categoryCollection, (a, b) => a._categoryCollection = b, a => a.CategoryCollection, () => new ProductRelations().CategoryEntityUsingProductId, typeof(CategoryEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.CategoryEntity);
				AddNavigatorMetaData<ProductEntity, EntityCollection<DeliverypointgroupEntity>>("DeliverypointgroupCollection", a => a._deliverypointgroupCollection, (a, b) => a._deliverypointgroupCollection = b, a => a.DeliverypointgroupCollection, () => new ProductRelations().DeliverypointgroupEntityUsingHotSOSBatteryLowProductId, typeof(DeliverypointgroupEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.DeliverypointgroupEntity);
				AddNavigatorMetaData<ProductEntity, EntityCollection<MediaEntity>>("MediaCollection", a => a._mediaCollection, (a, b) => a._mediaCollection = b, a => a.MediaCollection, () => new ProductRelations().MediaEntityUsingActionProductId, typeof(MediaEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.MediaEntity);
				AddNavigatorMetaData<ProductEntity, EntityCollection<MediaEntity>>("MediaCollection1", a => a._mediaCollection1, (a, b) => a._mediaCollection1 = b, a => a.MediaCollection1, () => new ProductRelations().MediaEntityUsingProductId, typeof(MediaEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.MediaEntity);
				AddNavigatorMetaData<ProductEntity, EntityCollection<OrderitemEntity>>("OrderitemCollection", a => a._orderitemCollection, (a, b) => a._orderitemCollection = b, a => a.OrderitemCollection, () => new ProductRelations().OrderitemEntityUsingProductId, typeof(OrderitemEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.OrderitemEntity);
				AddNavigatorMetaData<ProductEntity, EntityCollection<OrderitemAlterationitemEntity>>("OrderitemAlterationitemCollection", a => a._orderitemAlterationitemCollection, (a, b) => a._orderitemAlterationitemCollection = b, a => a.OrderitemAlterationitemCollection, () => new ProductRelations().OrderitemAlterationitemEntityUsingAlterationoptionProductId, typeof(OrderitemAlterationitemEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.OrderitemAlterationitemEntity);
				AddNavigatorMetaData<ProductEntity, EntityCollection<OrderitemTagEntity>>("OrderitemTagCollection", a => a._orderitemTagCollection, (a, b) => a._orderitemTagCollection = b, a => a.OrderitemTagCollection, () => new ProductRelations().OrderitemTagEntityUsingProductId, typeof(OrderitemTagEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.OrderitemTagEntity);
				AddNavigatorMetaData<ProductEntity, EntityCollection<OutletEntity>>("OutletCollection__", a => a._outletCollection__, (a, b) => a._outletCollection__ = b, a => a.OutletCollection__, () => new ProductRelations().OutletEntityUsingDeliveryChargeProductId, typeof(OutletEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.OutletEntity);
				AddNavigatorMetaData<ProductEntity, EntityCollection<OutletEntity>>("OutletCollection_", a => a._outletCollection_, (a, b) => a._outletCollection_ = b, a => a.OutletCollection_, () => new ProductRelations().OutletEntityUsingServiceChargeProductId, typeof(OutletEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.OutletEntity);
				AddNavigatorMetaData<ProductEntity, EntityCollection<OutletEntity>>("OutletCollection", a => a._outletCollection, (a, b) => a._outletCollection = b, a => a.OutletCollection, () => new ProductRelations().OutletEntityUsingTippingProductId, typeof(OutletEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.OutletEntity);
				AddNavigatorMetaData<ProductEntity, EntityCollection<PriceLevelItemEntity>>("PriceLevelItemCollection", a => a._priceLevelItemCollection, (a, b) => a._priceLevelItemCollection = b, a => a.PriceLevelItemCollection, () => new ProductRelations().PriceLevelItemEntityUsingProductId, typeof(PriceLevelItemEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.PriceLevelItemEntity);
				AddNavigatorMetaData<ProductEntity, EntityCollection<ProductEntity>>("BrandProductCollection", a => a._brandProductCollection, (a, b) => a._brandProductCollection = b, a => a.BrandProductCollection, () => new ProductRelations().ProductEntityUsingBrandProductId, typeof(ProductEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ProductEntity);
				AddNavigatorMetaData<ProductEntity, EntityCollection<ProductAlterationEntity>>("ProductAlterationCollection", a => a._productAlterationCollection, (a, b) => a._productAlterationCollection = b, a => a.ProductAlterationCollection, () => new ProductRelations().ProductAlterationEntityUsingProductId, typeof(ProductAlterationEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ProductAlterationEntity);
				AddNavigatorMetaData<ProductEntity, EntityCollection<ProductCategoryEntity>>("ProductCategoryCollection", a => a._productCategoryCollection, (a, b) => a._productCategoryCollection = b, a => a.ProductCategoryCollection, () => new ProductRelations().ProductCategoryEntityUsingProductId, typeof(ProductCategoryEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ProductCategoryEntity);
				AddNavigatorMetaData<ProductEntity, EntityCollection<ProductTagEntity>>("ProductTagCollection", a => a._productTagCollection, (a, b) => a._productTagCollection = b, a => a.ProductTagCollection, () => new ProductRelations().ProductTagEntityUsingProductId, typeof(ProductTagEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ProductTagEntity);
				AddNavigatorMetaData<ProductEntity, EntityCollection<ServiceMethodEntity>>("ServiceMethodCollection", a => a._serviceMethodCollection, (a, b) => a._serviceMethodCollection = b, a => a.ServiceMethodCollection, () => new ProductRelations().ServiceMethodEntityUsingServiceChargeProductId, typeof(ServiceMethodEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ServiceMethodEntity);
				AddNavigatorMetaData<ProductEntity, EntityCollection<TerminalEntity>>("TerminalCollection1", a => a._terminalCollection1, (a, b) => a._terminalCollection1 = b, a => a.TerminalCollection1, () => new ProductRelations().TerminalEntityUsingBatteryLowProductId, typeof(TerminalEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.TerminalEntity);
				AddNavigatorMetaData<ProductEntity, EntityCollection<TerminalEntity>>("TerminalCollection2", a => a._terminalCollection2, (a, b) => a._terminalCollection2 = b, a => a.TerminalCollection2, () => new ProductRelations().TerminalEntityUsingClientDisconnectedProductId, typeof(TerminalEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.TerminalEntity);
				AddNavigatorMetaData<ProductEntity, EntityCollection<TerminalEntity>>("TerminalCollection3", a => a._terminalCollection3, (a, b) => a._terminalCollection3 = b, a => a.TerminalCollection3, () => new ProductRelations().TerminalEntityUsingOrderFailedProductId, typeof(TerminalEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.TerminalEntity);
				AddNavigatorMetaData<ProductEntity, EntityCollection<TerminalEntity>>("TerminalCollection", a => a._terminalCollection, (a, b) => a._terminalCollection = b, a => a.TerminalCollection, () => new ProductRelations().TerminalEntityUsingUnlockDeliverypointProductId, typeof(TerminalEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.TerminalEntity);
				AddNavigatorMetaData<ProductEntity, CompanyEntity>("Company", "ProductCollection", (a, b) => a._company = b, a => a._company, (a, b) => a.Company = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticProductRelations.CompanyEntityUsingCompanyIdStatic, ()=>new ProductRelations().CompanyEntityUsingCompanyId, null, new int[] { (int)ProductFieldIndex.CompanyId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.CompanyEntity);
				AddNavigatorMetaData<ProductEntity, ExternalProductEntity>("ExternalProduct", "ProductCollection", (a, b) => a._externalProduct = b, a => a._externalProduct, (a, b) => a.ExternalProduct = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticProductRelations.ExternalProductEntityUsingExternalProductIdStatic, ()=>new ProductRelations().ExternalProductEntityUsingExternalProductId, null, new int[] { (int)ProductFieldIndex.ExternalProductId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ExternalProductEntity);
				AddNavigatorMetaData<ProductEntity, ProductEntity>("BrandProduct", "BrandProductCollection", (a, b) => a._brandProduct = b, a => a._brandProduct, (a, b) => a.BrandProduct = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticProductRelations.ProductEntityUsingProductIdBrandProductIdStatic, ()=>new ProductRelations().ProductEntityUsingProductIdBrandProductId, null, new int[] { (int)ProductFieldIndex.BrandProductId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ProductEntity);
				AddNavigatorMetaData<ProductEntity, RouteEntity>("Route", "ProductCollection", (a, b) => a._route = b, a => a._route, (a, b) => a.Route = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticProductRelations.RouteEntityUsingRouteIdStatic, ()=>new ProductRelations().RouteEntityUsingRouteId, null, new int[] { (int)ProductFieldIndex.RouteId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.RouteEntity);
				AddNavigatorMetaData<ProductEntity, ScheduleEntity>("Schedule", "ProductCollection", (a, b) => a._schedule = b, a => a._schedule, (a, b) => a.Schedule = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticProductRelations.ScheduleEntityUsingScheduleIdStatic, ()=>new ProductRelations().ScheduleEntityUsingScheduleId, null, new int[] { (int)ProductFieldIndex.ScheduleId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ScheduleEntity);
				AddNavigatorMetaData<ProductEntity, TaxTariffEntity>("TaxTariff", "ProductCollection", (a, b) => a._taxTariff = b, a => a._taxTariff, (a, b) => a.TaxTariff = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticProductRelations.TaxTariffEntityUsingTaxTariffIdStatic, ()=>new ProductRelations().TaxTariffEntityUsingTaxTariffId, null, new int[] { (int)ProductFieldIndex.TaxTariffId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.TaxTariffEntity);
				AddNavigatorMetaData<ProductEntity, EntityCollection<CategoryEntity>>("CategoryCollectionViaProductCategory", a => a._categoryCollectionViaProductCategory, (a, b) => a._categoryCollectionViaProductCategory = b, a => a.CategoryCollectionViaProductCategory, () => new ProductRelations().ProductCategoryEntityUsingProductId, () => new ProductCategoryRelations().CategoryEntityUsingCategoryId, "ProductEntity__", "ProductCategory_", typeof(CategoryEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.CategoryEntity);
			}
		}

		/// <summary>Static ctor</summary>
		static ProductEntity()
		{
		}

		/// <summary> CTor</summary>
		public ProductEntity()
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public ProductEntity(IEntityFields2 fields)
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this ProductEntity</param>
		public ProductEntity(IValidator validator)
		{
			InitClassEmpty(validator, null);
		}

		/// <summary> CTor</summary>
		/// <param name="productId">PK value for Product which data should be fetched into this Product object</param>
		public ProductEntity(System.Int32 productId) : this(productId, null)
		{
		}

		/// <summary> CTor</summary>
		/// <param name="productId">PK value for Product which data should be fetched into this Product object</param>
		/// <param name="validator">The custom validator object for this ProductEntity</param>
		public ProductEntity(System.Int32 productId, IValidator validator)
		{
			InitClassEmpty(validator, null);
			this.ProductId = productId;
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ProductEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Alterationoption' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoAlterationoptionCollection() { return CreateRelationInfoForNavigator("AlterationoptionCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'AlterationProduct' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoAlterationProductCollection() { return CreateRelationInfoForNavigator("AlterationProductCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Category' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoCategoryCollection() { return CreateRelationInfoForNavigator("CategoryCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Deliverypointgroup' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoDeliverypointgroupCollection() { return CreateRelationInfoForNavigator("DeliverypointgroupCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Media' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoMediaCollection() { return CreateRelationInfoForNavigator("MediaCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Media' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoMediaCollection1() { return CreateRelationInfoForNavigator("MediaCollection1"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Orderitem' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoOrderitemCollection() { return CreateRelationInfoForNavigator("OrderitemCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'OrderitemAlterationitem' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoOrderitemAlterationitemCollection() { return CreateRelationInfoForNavigator("OrderitemAlterationitemCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'OrderitemTag' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoOrderitemTagCollection() { return CreateRelationInfoForNavigator("OrderitemTagCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Outlet' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoOutletCollection__() { return CreateRelationInfoForNavigator("OutletCollection__"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Outlet' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoOutletCollection_() { return CreateRelationInfoForNavigator("OutletCollection_"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Outlet' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoOutletCollection() { return CreateRelationInfoForNavigator("OutletCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'PriceLevelItem' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoPriceLevelItemCollection() { return CreateRelationInfoForNavigator("PriceLevelItemCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Product' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoBrandProductCollection() { return CreateRelationInfoForNavigator("BrandProductCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'ProductAlteration' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoProductAlterationCollection() { return CreateRelationInfoForNavigator("ProductAlterationCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'ProductCategory' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoProductCategoryCollection() { return CreateRelationInfoForNavigator("ProductCategoryCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'ProductTag' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoProductTagCollection() { return CreateRelationInfoForNavigator("ProductTagCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'ServiceMethod' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoServiceMethodCollection() { return CreateRelationInfoForNavigator("ServiceMethodCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Terminal' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoTerminalCollection1() { return CreateRelationInfoForNavigator("TerminalCollection1"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Terminal' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoTerminalCollection2() { return CreateRelationInfoForNavigator("TerminalCollection2"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Terminal' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoTerminalCollection3() { return CreateRelationInfoForNavigator("TerminalCollection3"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Terminal' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoTerminalCollection() { return CreateRelationInfoForNavigator("TerminalCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Category' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoCategoryCollectionViaProductCategory() { return CreateRelationInfoForNavigator("CategoryCollectionViaProductCategory"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Company' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoCompany() { return CreateRelationInfoForNavigator("Company"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'ExternalProduct' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoExternalProduct() { return CreateRelationInfoForNavigator("ExternalProduct"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Product' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoBrandProduct() { return CreateRelationInfoForNavigator("BrandProduct"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Route' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoRoute() { return CreateRelationInfoForNavigator("Route"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Schedule' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoSchedule() { return CreateRelationInfoForNavigator("Schedule"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'TaxTariff' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoTaxTariff() { return CreateRelationInfoForNavigator("TaxTariff"); }
		
		/// <inheritdoc/>
		protected override EntityStaticMetaDataBase GetEntityStaticMetaData() {	return _staticMetaData; }

		/// <summary>Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		/// <summary>Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this ProductEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary>The relations object holding all relations of this entity with other entity classes.</summary>
		public static ProductRelations Relations { get { return _relationsFactory; } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Alterationoption' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathAlterationoptionCollection { get { return _staticMetaData.GetPrefetchPathElement("AlterationoptionCollection", CommonEntityBase.CreateEntityCollection<AlterationoptionEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'AlterationProduct' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathAlterationProductCollection { get { return _staticMetaData.GetPrefetchPathElement("AlterationProductCollection", CommonEntityBase.CreateEntityCollection<AlterationProductEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Category' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathCategoryCollection { get { return _staticMetaData.GetPrefetchPathElement("CategoryCollection", CommonEntityBase.CreateEntityCollection<CategoryEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Deliverypointgroup' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathDeliverypointgroupCollection { get { return _staticMetaData.GetPrefetchPathElement("DeliverypointgroupCollection", CommonEntityBase.CreateEntityCollection<DeliverypointgroupEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Media' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathMediaCollection { get { return _staticMetaData.GetPrefetchPathElement("MediaCollection", CommonEntityBase.CreateEntityCollection<MediaEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Media' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathMediaCollection1 { get { return _staticMetaData.GetPrefetchPathElement("MediaCollection1", CommonEntityBase.CreateEntityCollection<MediaEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Orderitem' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathOrderitemCollection { get { return _staticMetaData.GetPrefetchPathElement("OrderitemCollection", CommonEntityBase.CreateEntityCollection<OrderitemEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'OrderitemAlterationitem' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathOrderitemAlterationitemCollection { get { return _staticMetaData.GetPrefetchPathElement("OrderitemAlterationitemCollection", CommonEntityBase.CreateEntityCollection<OrderitemAlterationitemEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'OrderitemTag' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathOrderitemTagCollection { get { return _staticMetaData.GetPrefetchPathElement("OrderitemTagCollection", CommonEntityBase.CreateEntityCollection<OrderitemTagEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Outlet' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathOutletCollection__ { get { return _staticMetaData.GetPrefetchPathElement("OutletCollection__", CommonEntityBase.CreateEntityCollection<OutletEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Outlet' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathOutletCollection_ { get { return _staticMetaData.GetPrefetchPathElement("OutletCollection_", CommonEntityBase.CreateEntityCollection<OutletEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Outlet' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathOutletCollection { get { return _staticMetaData.GetPrefetchPathElement("OutletCollection", CommonEntityBase.CreateEntityCollection<OutletEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'PriceLevelItem' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathPriceLevelItemCollection { get { return _staticMetaData.GetPrefetchPathElement("PriceLevelItemCollection", CommonEntityBase.CreateEntityCollection<PriceLevelItemEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Product' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathBrandProductCollection { get { return _staticMetaData.GetPrefetchPathElement("BrandProductCollection", CommonEntityBase.CreateEntityCollection<ProductEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'ProductAlteration' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathProductAlterationCollection { get { return _staticMetaData.GetPrefetchPathElement("ProductAlterationCollection", CommonEntityBase.CreateEntityCollection<ProductAlterationEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'ProductCategory' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathProductCategoryCollection { get { return _staticMetaData.GetPrefetchPathElement("ProductCategoryCollection", CommonEntityBase.CreateEntityCollection<ProductCategoryEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'ProductTag' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathProductTagCollection { get { return _staticMetaData.GetPrefetchPathElement("ProductTagCollection", CommonEntityBase.CreateEntityCollection<ProductTagEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'ServiceMethod' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathServiceMethodCollection { get { return _staticMetaData.GetPrefetchPathElement("ServiceMethodCollection", CommonEntityBase.CreateEntityCollection<ServiceMethodEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Terminal' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathTerminalCollection1 { get { return _staticMetaData.GetPrefetchPathElement("TerminalCollection1", CommonEntityBase.CreateEntityCollection<TerminalEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Terminal' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathTerminalCollection2 { get { return _staticMetaData.GetPrefetchPathElement("TerminalCollection2", CommonEntityBase.CreateEntityCollection<TerminalEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Terminal' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathTerminalCollection3 { get { return _staticMetaData.GetPrefetchPathElement("TerminalCollection3", CommonEntityBase.CreateEntityCollection<TerminalEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Terminal' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathTerminalCollection { get { return _staticMetaData.GetPrefetchPathElement("TerminalCollection", CommonEntityBase.CreateEntityCollection<TerminalEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Category' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathCategoryCollectionViaProductCategory { get { return _staticMetaData.GetPrefetchPathElement("CategoryCollectionViaProductCategory", CommonEntityBase.CreateEntityCollection<CategoryEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Company' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathCompanyEntity { get { return _staticMetaData.GetPrefetchPathElement("Company", CommonEntityBase.CreateEntityCollection<CompanyEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'ExternalProduct' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathExternalProductEntity { get { return _staticMetaData.GetPrefetchPathElement("ExternalProduct", CommonEntityBase.CreateEntityCollection<ExternalProductEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Product' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathBrandProductEntity { get { return _staticMetaData.GetPrefetchPathElement("BrandProduct", CommonEntityBase.CreateEntityCollection<ProductEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Route' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathRouteEntity { get { return _staticMetaData.GetPrefetchPathElement("Route", CommonEntityBase.CreateEntityCollection<RouteEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Schedule' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathScheduleEntity { get { return _staticMetaData.GetPrefetchPathElement("Schedule", CommonEntityBase.CreateEntityCollection<ScheduleEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'TaxTariff' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathTaxTariffEntity { get { return _staticMetaData.GetPrefetchPathElement("TaxTariff", CommonEntityBase.CreateEntityCollection<TaxTariffEntity>()); } }

		/// <summary>The ProductId property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Product"."ProductId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 ProductId
		{
			get { return (System.Int32)GetValue((int)ProductFieldIndex.ProductId, true); }
			set { SetValue((int)ProductFieldIndex.ProductId, value); }		}

		/// <summary>The CompanyId property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Product"."CompanyId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CompanyId
		{
			get { return (Nullable<System.Int32>)GetValue((int)ProductFieldIndex.CompanyId, false); }
			set	{ SetValue((int)ProductFieldIndex.CompanyId, value); }
		}

		/// <summary>The GenericproductId property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Product"."GenericproductId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> GenericproductId
		{
			get { return (Nullable<System.Int32>)GetValue((int)ProductFieldIndex.GenericproductId, false); }
			set	{ SetValue((int)ProductFieldIndex.GenericproductId, value); }
		}

		/// <summary>The BrandProductId property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Product"."BrandProductId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> BrandProductId
		{
			get { return (Nullable<System.Int32>)GetValue((int)ProductFieldIndex.BrandProductId, false); }
			set	{ SetValue((int)ProductFieldIndex.BrandProductId, value); }
		}

		/// <summary>The RouteId property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Product"."RouteId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> RouteId
		{
			get { return (Nullable<System.Int32>)GetValue((int)ProductFieldIndex.RouteId, false); }
			set	{ SetValue((int)ProductFieldIndex.RouteId, value); }
		}

		/// <summary>The ScheduleId property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Product"."ScheduleId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ScheduleId
		{
			get { return (Nullable<System.Int32>)GetValue((int)ProductFieldIndex.ScheduleId, false); }
			set	{ SetValue((int)ProductFieldIndex.ScheduleId, value); }
		}

		/// <summary>The Name property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Product"."Name".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)ProductFieldIndex.Name, true); }
			set	{ SetValue((int)ProductFieldIndex.Name, value); }
		}

		/// <summary>The Description property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Product"."Description".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)ProductFieldIndex.Description, true); }
			set	{ SetValue((int)ProductFieldIndex.Description, value); }
		}

		/// <summary>The Type property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Product"."Type".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Crave.Enums.ProductType Type
		{
			get { return (Crave.Enums.ProductType)GetValue((int)ProductFieldIndex.Type, true); }
			set	{ SetValue((int)ProductFieldIndex.Type, value); }
		}

		/// <summary>The SubType property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Product"."SubType".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Crave.Enums.ProductSubType SubType
		{
			get { return (Crave.Enums.ProductSubType)GetValue((int)ProductFieldIndex.SubType, true); }
			set	{ SetValue((int)ProductFieldIndex.SubType, value); }
		}

		/// <summary>The ButtonText property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Product"."ButtonText".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 100.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ButtonText
		{
			get { return (System.String)GetValue((int)ProductFieldIndex.ButtonText, true); }
			set	{ SetValue((int)ProductFieldIndex.ButtonText, value); }
		}

		/// <summary>The CustomizeButtonText property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Product"."CustomizeButtonText".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 100.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CustomizeButtonText
		{
			get { return (System.String)GetValue((int)ProductFieldIndex.CustomizeButtonText, true); }
			set	{ SetValue((int)ProductFieldIndex.CustomizeButtonText, value); }
		}

		/// <summary>The PriceIn property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Product"."PriceIn".<br/>Table field type characteristics (type, precision, scale, length): Money, 19, 4, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Decimal> PriceIn
		{
			get { return (Nullable<System.Decimal>)GetValue((int)ProductFieldIndex.PriceIn, false); }
			set	{ SetValue((int)ProductFieldIndex.PriceIn, value); }
		}

		/// <summary>The VattariffId property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Product"."VattariffId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 VattariffId
		{
			get { return (System.Int32)GetValue((int)ProductFieldIndex.VattariffId, true); }
			set	{ SetValue((int)ProductFieldIndex.VattariffId, value); }
		}

		/// <summary>The VattariffPercentage property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Product"."VattariffPercentage".<br/>Table field type characteristics (type, precision, scale, length): Float, 38, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Double VattariffPercentage
		{
			get { return (System.Double)GetValue((int)ProductFieldIndex.VattariffPercentage, true); }
			set	{ SetValue((int)ProductFieldIndex.VattariffPercentage, value); }
		}

		/// <summary>The SortOrder property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Product"."SortOrder".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 SortOrder
		{
			get { return (System.Int32)GetValue((int)ProductFieldIndex.SortOrder, true); }
			set	{ SetValue((int)ProductFieldIndex.SortOrder, value); }
		}

		/// <summary>The PosproductId property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Product"."PosproductId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> PosproductId
		{
			get { return (Nullable<System.Int32>)GetValue((int)ProductFieldIndex.PosproductId, false); }
			set	{ SetValue((int)ProductFieldIndex.PosproductId, value); }
		}

		/// <summary>The ExternalProductId property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Product"."ExternalProductId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ExternalProductId
		{
			get { return (Nullable<System.Int32>)GetValue((int)ProductFieldIndex.ExternalProductId, false); }
			set	{ SetValue((int)ProductFieldIndex.ExternalProductId, value); }
		}

		/// <summary>The TextColor property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Product"."TextColor".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 15.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String TextColor
		{
			get { return (System.String)GetValue((int)ProductFieldIndex.TextColor, true); }
			set	{ SetValue((int)ProductFieldIndex.TextColor, value); }
		}

		/// <summary>The BackgroundColor property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Product"."BackgroundColor".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 15.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String BackgroundColor
		{
			get { return (System.String)GetValue((int)ProductFieldIndex.BackgroundColor, true); }
			set	{ SetValue((int)ProductFieldIndex.BackgroundColor, value); }
		}

		/// <summary>The DisplayOnHomepage property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Product"."DisplayOnHomepage".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> DisplayOnHomepage
		{
			get { return (Nullable<System.Boolean>)GetValue((int)ProductFieldIndex.DisplayOnHomepage, false); }
			set	{ SetValue((int)ProductFieldIndex.DisplayOnHomepage, value); }
		}

		/// <summary>The AvailableOnOtoucho property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Product"."AvailableOnOtoucho".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean AvailableOnOtoucho
		{
			get { return (System.Boolean)GetValue((int)ProductFieldIndex.AvailableOnOtoucho, true); }
			set	{ SetValue((int)ProductFieldIndex.AvailableOnOtoucho, value); }
		}

		/// <summary>The AvailableOnObymobi property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Product"."AvailableOnObymobi".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean AvailableOnObymobi
		{
			get { return (System.Boolean)GetValue((int)ProductFieldIndex.AvailableOnObymobi, true); }
			set	{ SetValue((int)ProductFieldIndex.AvailableOnObymobi, value); }
		}

		/// <summary>The SoldOut property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Product"."SoldOut".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean SoldOut
		{
			get { return (System.Boolean)GetValue((int)ProductFieldIndex.SoldOut, true); }
			set	{ SetValue((int)ProductFieldIndex.SoldOut, value); }
		}

		/// <summary>The Rateable property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Product"."Rateable".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> Rateable
		{
			get { return (Nullable<System.Boolean>)GetValue((int)ProductFieldIndex.Rateable, false); }
			set	{ SetValue((int)ProductFieldIndex.Rateable, value); }
		}

		/// <summary>The AllowFreeText property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Product"."AllowFreeText".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> AllowFreeText
		{
			get { return (Nullable<System.Boolean>)GetValue((int)ProductFieldIndex.AllowFreeText, false); }
			set	{ SetValue((int)ProductFieldIndex.AllowFreeText, value); }
		}

		/// <summary>The AnnouncementAction property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Product"."AnnouncementAction".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean AnnouncementAction
		{
			get { return (System.Boolean)GetValue((int)ProductFieldIndex.AnnouncementAction, true); }
			set	{ SetValue((int)ProductFieldIndex.AnnouncementAction, value); }
		}

		/// <summary>The Color property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Product"."Color".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 Color
		{
			get { return (System.Int32)GetValue((int)ProductFieldIndex.Color, true); }
			set	{ SetValue((int)ProductFieldIndex.Color, value); }
		}

		/// <summary>The ManualDescriptionEnabled property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Product"."ManualDescriptionEnabled".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean ManualDescriptionEnabled
		{
			get { return (System.Boolean)GetValue((int)ProductFieldIndex.ManualDescriptionEnabled, true); }
			set	{ SetValue((int)ProductFieldIndex.ManualDescriptionEnabled, value); }
		}

		/// <summary>The WebTypeSmartphoneUrl property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Product"."WebTypeSmartphoneUrl".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String WebTypeSmartphoneUrl
		{
			get { return (System.String)GetValue((int)ProductFieldIndex.WebTypeSmartphoneUrl, true); }
			set	{ SetValue((int)ProductFieldIndex.WebTypeSmartphoneUrl, value); }
		}

		/// <summary>The WebTypeTabletUrl property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Product"."WebTypeTabletUrl".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String WebTypeTabletUrl
		{
			get { return (System.String)GetValue((int)ProductFieldIndex.WebTypeTabletUrl, true); }
			set	{ SetValue((int)ProductFieldIndex.WebTypeTabletUrl, value); }
		}

		/// <summary>The Geofencing property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Product"."Geofencing".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> Geofencing
		{
			get { return (Nullable<System.Boolean>)GetValue((int)ProductFieldIndex.Geofencing, false); }
			set	{ SetValue((int)ProductFieldIndex.Geofencing, value); }
		}

		/// <summary>The HotSOSIssueId property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Product"."HotSOSIssueId".<br/>Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 50.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String HotSOSIssueId
		{
			get { return (System.String)GetValue((int)ProductFieldIndex.HotSOSIssueId, true); }
			set	{ SetValue((int)ProductFieldIndex.HotSOSIssueId, value); }
		}

		/// <summary>The ViewLayoutType property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Product"."ViewLayoutType".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Crave.Enums.ViewLayoutType ViewLayoutType
		{
			get { return (Crave.Enums.ViewLayoutType)GetValue((int)ProductFieldIndex.ViewLayoutType, true); }
			set	{ SetValue((int)ProductFieldIndex.ViewLayoutType, value); }
		}

		/// <summary>The DeliveryLocationType property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Product"."DeliveryLocationType".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Crave.Enums.DeliveryLocationType DeliveryLocationType
		{
			get { return (Crave.Enums.DeliveryLocationType)GetValue((int)ProductFieldIndex.DeliveryLocationType, true); }
			set	{ SetValue((int)ProductFieldIndex.DeliveryLocationType, value); }
		}

		/// <summary>The SupportNotificationType property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Product"."SupportNotificationType".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Crave.Enums.SupportNotificationType SupportNotificationType
		{
			get { return (Crave.Enums.SupportNotificationType)GetValue((int)ProductFieldIndex.SupportNotificationType, true); }
			set	{ SetValue((int)ProductFieldIndex.SupportNotificationType, value); }
		}

		/// <summary>The HidePrice property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Product"."HidePrice".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean HidePrice
		{
			get { return (System.Boolean)GetValue((int)ProductFieldIndex.HidePrice, true); }
			set	{ SetValue((int)ProductFieldIndex.HidePrice, value); }
		}

		/// <summary>The VisibilityType property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Product"."VisibilityType".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Crave.Enums.VisibilityType VisibilityType
		{
			get { return (Crave.Enums.VisibilityType)GetValue((int)ProductFieldIndex.VisibilityType, true); }
			set	{ SetValue((int)ProductFieldIndex.VisibilityType, value); }
		}

		/// <summary>The InheritAlterations property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Product"."InheritAlterations".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean InheritAlterations
		{
			get { return (System.Boolean)GetValue((int)ProductFieldIndex.InheritAlterations, true); }
			set	{ SetValue((int)ProductFieldIndex.InheritAlterations, value); }
		}

		/// <summary>The InheritAlterationsFromBrand property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Product"."InheritAlterationsFromBrand".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean InheritAlterationsFromBrand
		{
			get { return (System.Boolean)GetValue((int)ProductFieldIndex.InheritAlterationsFromBrand, true); }
			set	{ SetValue((int)ProductFieldIndex.InheritAlterationsFromBrand, value); }
		}

		/// <summary>The InheritAttachmentsFromBrand property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Product"."InheritAttachmentsFromBrand".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean InheritAttachmentsFromBrand
		{
			get { return (System.Boolean)GetValue((int)ProductFieldIndex.InheritAttachmentsFromBrand, true); }
			set	{ SetValue((int)ProductFieldIndex.InheritAttachmentsFromBrand, value); }
		}

		/// <summary>The CreatedBy property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Product"."CreatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)ProductFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)ProductFieldIndex.CreatedBy, value); }
		}

		/// <summary>The UpdatedBy property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Product"."UpdatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)ProductFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)ProductFieldIndex.UpdatedBy, value); }
		}

		/// <summary>The CreatedUTC property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Product"."CreatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ProductFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)ProductFieldIndex.CreatedUTC, value); }
		}

		/// <summary>The UpdatedUTC property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Product"."UpdatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ProductFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)ProductFieldIndex.UpdatedUTC, value); }
		}

		/// <summary>The GenericProductChangedUTC property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Product"."GenericProductChangedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> GenericProductChangedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ProductFieldIndex.GenericProductChangedUTC, false); }
			set	{ SetValue((int)ProductFieldIndex.GenericProductChangedUTC, value); }
		}

		/// <summary>The IsLinkedToGeneric property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Product"."IsLinkedToGeneric".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsLinkedToGeneric
		{
			get { return (System.Boolean)GetValue((int)ProductFieldIndex.IsLinkedToGeneric, true); }
			set	{ SetValue((int)ProductFieldIndex.IsLinkedToGeneric, value); }
		}

		/// <summary>The OverrideSubType property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Product"."OverrideSubType".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean OverrideSubType
		{
			get { return (System.Boolean)GetValue((int)ProductFieldIndex.OverrideSubType, true); }
			set	{ SetValue((int)ProductFieldIndex.OverrideSubType, value); }
		}

		/// <summary>The ShortDescription property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Product"."ShortDescription".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ShortDescription
		{
			get { return (System.String)GetValue((int)ProductFieldIndex.ShortDescription, true); }
			set	{ SetValue((int)ProductFieldIndex.ShortDescription, value); }
		}

		/// <summary>The InheritName property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Product"."InheritName".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean InheritName
		{
			get { return (System.Boolean)GetValue((int)ProductFieldIndex.InheritName, true); }
			set	{ SetValue((int)ProductFieldIndex.InheritName, value); }
		}

		/// <summary>The InheritPrice property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Product"."InheritPrice".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean InheritPrice
		{
			get { return (System.Boolean)GetValue((int)ProductFieldIndex.InheritPrice, true); }
			set	{ SetValue((int)ProductFieldIndex.InheritPrice, value); }
		}

		/// <summary>The InheritButtonText property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Product"."InheritButtonText".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean InheritButtonText
		{
			get { return (System.Boolean)GetValue((int)ProductFieldIndex.InheritButtonText, true); }
			set	{ SetValue((int)ProductFieldIndex.InheritButtonText, value); }
		}

		/// <summary>The InheritCustomizeButtonText property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Product"."InheritCustomizeButtonText".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean InheritCustomizeButtonText
		{
			get { return (System.Boolean)GetValue((int)ProductFieldIndex.InheritCustomizeButtonText, true); }
			set	{ SetValue((int)ProductFieldIndex.InheritCustomizeButtonText, value); }
		}

		/// <summary>The InheritWebTypeTabletUrl property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Product"."InheritWebTypeTabletUrl".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean InheritWebTypeTabletUrl
		{
			get { return (System.Boolean)GetValue((int)ProductFieldIndex.InheritWebTypeTabletUrl, true); }
			set	{ SetValue((int)ProductFieldIndex.InheritWebTypeTabletUrl, value); }
		}

		/// <summary>The InheritWebTypeSmartphoneUrl property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Product"."InheritWebTypeSmartphoneUrl".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean InheritWebTypeSmartphoneUrl
		{
			get { return (System.Boolean)GetValue((int)ProductFieldIndex.InheritWebTypeSmartphoneUrl, true); }
			set	{ SetValue((int)ProductFieldIndex.InheritWebTypeSmartphoneUrl, value); }
		}

		/// <summary>The InheritDescription property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Product"."InheritDescription".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean InheritDescription
		{
			get { return (System.Boolean)GetValue((int)ProductFieldIndex.InheritDescription, true); }
			set	{ SetValue((int)ProductFieldIndex.InheritDescription, value); }
		}

		/// <summary>The InheritColor property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Product"."InheritColor".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean InheritColor
		{
			get { return (System.Boolean)GetValue((int)ProductFieldIndex.InheritColor, true); }
			set	{ SetValue((int)ProductFieldIndex.InheritColor, value); }
		}

		/// <summary>The InheritMedia property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Product"."InheritMedia".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean InheritMedia
		{
			get { return (System.Boolean)GetValue((int)ProductFieldIndex.InheritMedia, true); }
			set	{ SetValue((int)ProductFieldIndex.InheritMedia, value); }
		}

		/// <summary>The TaxTariffId property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Product"."TaxTariffId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> TaxTariffId
		{
			get { return (Nullable<System.Int32>)GetValue((int)ProductFieldIndex.TaxTariffId, false); }
			set	{ SetValue((int)ProductFieldIndex.TaxTariffId, value); }
		}

		/// <summary>The IsAlcoholic property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Product"."IsAlcoholic".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsAlcoholic
		{
			get { return (System.Boolean)GetValue((int)ProductFieldIndex.IsAlcoholic, true); }
			set	{ SetValue((int)ProductFieldIndex.IsAlcoholic, value); }
		}

		/// <summary>The IsAvailable property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Product"."IsAvailable".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsAvailable
		{
			get { return (System.Boolean)GetValue((int)ProductFieldIndex.IsAvailable, true); }
			set	{ SetValue((int)ProductFieldIndex.IsAvailable, value); }
		}

		/// <summary>The SystemName property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Product"."SystemName".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String SystemName
		{
			get { return (System.String)GetValue((int)ProductFieldIndex.SystemName, true); }
			set	{ SetValue((int)ProductFieldIndex.SystemName, value); }
		}

		/// <summary>The InheritSystemName property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Product"."InheritSystemName".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean InheritSystemName
		{
			get { return (System.Boolean)GetValue((int)ProductFieldIndex.InheritSystemName, true); }
			set	{ SetValue((int)ProductFieldIndex.InheritSystemName, value); }
		}

		/// <summary>The ExternalIdentifier property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Product"."ExternalIdentifier".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ExternalIdentifier
		{
			get { return (System.String)GetValue((int)ProductFieldIndex.ExternalIdentifier, true); }
			set	{ SetValue((int)ProductFieldIndex.ExternalIdentifier, value); }
		}

		/// <summary>The InheritExternalIdentifier property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Product"."InheritExternalIdentifier".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean InheritExternalIdentifier
		{
			get { return (System.Boolean)GetValue((int)ProductFieldIndex.InheritExternalIdentifier, true); }
			set	{ SetValue((int)ProductFieldIndex.InheritExternalIdentifier, value); }
		}

		/// <summary>The PointOfInterestId property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Product"."PointOfInterestId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> PointOfInterestId
		{
			get { return (Nullable<System.Int32>)GetValue((int)ProductFieldIndex.PointOfInterestId, false); }
			set	{ SetValue((int)ProductFieldIndex.PointOfInterestId, value); }
		}

		/// <summary>The CoversType property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Product"."CoversType".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CoversType
		{
			get { return (System.Int32)GetValue((int)ProductFieldIndex.CoversType, true); }
			set	{ SetValue((int)ProductFieldIndex.CoversType, value); }
		}

		/// <summary>Gets the EntityCollection with the related entities of type 'AlterationoptionEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(AlterationoptionEntity))]
		public virtual EntityCollection<AlterationoptionEntity> AlterationoptionCollection { get { return GetOrCreateEntityCollection<AlterationoptionEntity, AlterationoptionEntityFactory>("Product", true, false, ref _alterationoptionCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'AlterationProductEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(AlterationProductEntity))]
		public virtual EntityCollection<AlterationProductEntity> AlterationProductCollection { get { return GetOrCreateEntityCollection<AlterationProductEntity, AlterationProductEntityFactory>("Product", true, false, ref _alterationProductCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'CategoryEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(CategoryEntity))]
		public virtual EntityCollection<CategoryEntity> CategoryCollection { get { return GetOrCreateEntityCollection<CategoryEntity, CategoryEntityFactory>("Product", true, false, ref _categoryCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'DeliverypointgroupEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(DeliverypointgroupEntity))]
		public virtual EntityCollection<DeliverypointgroupEntity> DeliverypointgroupCollection { get { return GetOrCreateEntityCollection<DeliverypointgroupEntity, DeliverypointgroupEntityFactory>("Product", true, false, ref _deliverypointgroupCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'MediaEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(MediaEntity))]
		public virtual EntityCollection<MediaEntity> MediaCollection { get { return GetOrCreateEntityCollection<MediaEntity, MediaEntityFactory>("Product", true, false, ref _mediaCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'MediaEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(MediaEntity))]
		public virtual EntityCollection<MediaEntity> MediaCollection1 { get { return GetOrCreateEntityCollection<MediaEntity, MediaEntityFactory>("Product1", true, false, ref _mediaCollection1); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'OrderitemEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(OrderitemEntity))]
		public virtual EntityCollection<OrderitemEntity> OrderitemCollection { get { return GetOrCreateEntityCollection<OrderitemEntity, OrderitemEntityFactory>("Product", true, false, ref _orderitemCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'OrderitemAlterationitemEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(OrderitemAlterationitemEntity))]
		public virtual EntityCollection<OrderitemAlterationitemEntity> OrderitemAlterationitemCollection { get { return GetOrCreateEntityCollection<OrderitemAlterationitemEntity, OrderitemAlterationitemEntityFactory>("Product", true, false, ref _orderitemAlterationitemCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'OrderitemTagEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(OrderitemTagEntity))]
		public virtual EntityCollection<OrderitemTagEntity> OrderitemTagCollection { get { return GetOrCreateEntityCollection<OrderitemTagEntity, OrderitemTagEntityFactory>("Product", true, false, ref _orderitemTagCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'OutletEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(OutletEntity))]
		public virtual EntityCollection<OutletEntity> OutletCollection__ { get { return GetOrCreateEntityCollection<OutletEntity, OutletEntityFactory>("DeliveryChargeProduct", true, false, ref _outletCollection__); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'OutletEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(OutletEntity))]
		public virtual EntityCollection<OutletEntity> OutletCollection_ { get { return GetOrCreateEntityCollection<OutletEntity, OutletEntityFactory>("ServiceChargeProduct", true, false, ref _outletCollection_); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'OutletEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(OutletEntity))]
		public virtual EntityCollection<OutletEntity> OutletCollection { get { return GetOrCreateEntityCollection<OutletEntity, OutletEntityFactory>("TippingProduct", true, false, ref _outletCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'PriceLevelItemEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(PriceLevelItemEntity))]
		public virtual EntityCollection<PriceLevelItemEntity> PriceLevelItemCollection { get { return GetOrCreateEntityCollection<PriceLevelItemEntity, PriceLevelItemEntityFactory>("Product", true, false, ref _priceLevelItemCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'ProductEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(ProductEntity))]
		public virtual EntityCollection<ProductEntity> BrandProductCollection { get { return GetOrCreateEntityCollection<ProductEntity, ProductEntityFactory>("BrandProduct", true, false, ref _brandProductCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'ProductAlterationEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(ProductAlterationEntity))]
		public virtual EntityCollection<ProductAlterationEntity> ProductAlterationCollection { get { return GetOrCreateEntityCollection<ProductAlterationEntity, ProductAlterationEntityFactory>("Product", true, false, ref _productAlterationCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'ProductCategoryEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(ProductCategoryEntity))]
		public virtual EntityCollection<ProductCategoryEntity> ProductCategoryCollection { get { return GetOrCreateEntityCollection<ProductCategoryEntity, ProductCategoryEntityFactory>("Product", true, false, ref _productCategoryCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'ProductTagEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(ProductTagEntity))]
		public virtual EntityCollection<ProductTagEntity> ProductTagCollection { get { return GetOrCreateEntityCollection<ProductTagEntity, ProductTagEntityFactory>("Product", true, false, ref _productTagCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'ServiceMethodEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(ServiceMethodEntity))]
		public virtual EntityCollection<ServiceMethodEntity> ServiceMethodCollection { get { return GetOrCreateEntityCollection<ServiceMethodEntity, ServiceMethodEntityFactory>("Product", true, false, ref _serviceMethodCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'TerminalEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(TerminalEntity))]
		public virtual EntityCollection<TerminalEntity> TerminalCollection1 { get { return GetOrCreateEntityCollection<TerminalEntity, TerminalEntityFactory>("BatteryLowProduct", true, false, ref _terminalCollection1); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'TerminalEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(TerminalEntity))]
		public virtual EntityCollection<TerminalEntity> TerminalCollection2 { get { return GetOrCreateEntityCollection<TerminalEntity, TerminalEntityFactory>("ClientDisconnectedProduct", true, false, ref _terminalCollection2); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'TerminalEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(TerminalEntity))]
		public virtual EntityCollection<TerminalEntity> TerminalCollection3 { get { return GetOrCreateEntityCollection<TerminalEntity, TerminalEntityFactory>("OrderFailedProduct", true, false, ref _terminalCollection3); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'TerminalEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(TerminalEntity))]
		public virtual EntityCollection<TerminalEntity> TerminalCollection { get { return GetOrCreateEntityCollection<TerminalEntity, TerminalEntityFactory>("UnlockDeliverypointProduct", true, false, ref _terminalCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'CategoryEntity' which are related to this entity via a relation of type 'm:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(CategoryEntity))]
		public virtual EntityCollection<CategoryEntity> CategoryCollectionViaProductCategory { get { return GetOrCreateEntityCollection<CategoryEntity, CategoryEntityFactory>("ProductCollectionViaProductCategory", false, true, ref _categoryCollectionViaProductCategory); } }

		/// <summary>Gets / sets related entity of type 'CompanyEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual CompanyEntity Company
		{
			get { return _company; }
			set { SetSingleRelatedEntityNavigator(value, "Company"); }
		}

		/// <summary>Gets / sets related entity of type 'ExternalProductEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual ExternalProductEntity ExternalProduct
		{
			get { return _externalProduct; }
			set { SetSingleRelatedEntityNavigator(value, "ExternalProduct"); }
		}

		/// <summary>Gets / sets related entity of type 'ProductEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual ProductEntity BrandProduct
		{
			get { return _brandProduct; }
			set { SetSingleRelatedEntityNavigator(value, "BrandProduct"); }
		}

		/// <summary>Gets / sets related entity of type 'RouteEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual RouteEntity Route
		{
			get { return _route; }
			set { SetSingleRelatedEntityNavigator(value, "Route"); }
		}

		/// <summary>Gets / sets related entity of type 'ScheduleEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual ScheduleEntity Schedule
		{
			get { return _schedule; }
			set { SetSingleRelatedEntityNavigator(value, "Schedule"); }
		}

		/// <summary>Gets / sets related entity of type 'TaxTariffEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual TaxTariffEntity TaxTariff
		{
			get { return _taxTariff; }
			set { SetSingleRelatedEntityNavigator(value, "TaxTariff"); }
		}


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen
{
	public enum ProductFieldIndex
	{
		///<summary>ProductId. </summary>
		ProductId,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>GenericproductId. </summary>
		GenericproductId,
		///<summary>BrandProductId. </summary>
		BrandProductId,
		///<summary>RouteId. </summary>
		RouteId,
		///<summary>ScheduleId. </summary>
		ScheduleId,
		///<summary>Name. </summary>
		Name,
		///<summary>Description. </summary>
		Description,
		///<summary>Type. </summary>
		Type,
		///<summary>SubType. </summary>
		SubType,
		///<summary>ButtonText. </summary>
		ButtonText,
		///<summary>CustomizeButtonText. </summary>
		CustomizeButtonText,
		///<summary>PriceIn. </summary>
		PriceIn,
		///<summary>VattariffId. </summary>
		VattariffId,
		///<summary>VattariffPercentage. </summary>
		VattariffPercentage,
		///<summary>SortOrder. </summary>
		SortOrder,
		///<summary>PosproductId. </summary>
		PosproductId,
		///<summary>ExternalProductId. </summary>
		ExternalProductId,
		///<summary>TextColor. </summary>
		TextColor,
		///<summary>BackgroundColor. </summary>
		BackgroundColor,
		///<summary>DisplayOnHomepage. </summary>
		DisplayOnHomepage,
		///<summary>AvailableOnOtoucho. </summary>
		AvailableOnOtoucho,
		///<summary>AvailableOnObymobi. </summary>
		AvailableOnObymobi,
		///<summary>SoldOut. </summary>
		SoldOut,
		///<summary>Rateable. </summary>
		Rateable,
		///<summary>AllowFreeText. </summary>
		AllowFreeText,
		///<summary>AnnouncementAction. </summary>
		AnnouncementAction,
		///<summary>Color. </summary>
		Color,
		///<summary>ManualDescriptionEnabled. </summary>
		ManualDescriptionEnabled,
		///<summary>WebTypeSmartphoneUrl. </summary>
		WebTypeSmartphoneUrl,
		///<summary>WebTypeTabletUrl. </summary>
		WebTypeTabletUrl,
		///<summary>Geofencing. </summary>
		Geofencing,
		///<summary>HotSOSIssueId. </summary>
		HotSOSIssueId,
		///<summary>ViewLayoutType. </summary>
		ViewLayoutType,
		///<summary>DeliveryLocationType. </summary>
		DeliveryLocationType,
		///<summary>SupportNotificationType. </summary>
		SupportNotificationType,
		///<summary>HidePrice. </summary>
		HidePrice,
		///<summary>VisibilityType. </summary>
		VisibilityType,
		///<summary>InheritAlterations. </summary>
		InheritAlterations,
		///<summary>InheritAlterationsFromBrand. </summary>
		InheritAlterationsFromBrand,
		///<summary>InheritAttachmentsFromBrand. </summary>
		InheritAttachmentsFromBrand,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>GenericProductChangedUTC. </summary>
		GenericProductChangedUTC,
		///<summary>IsLinkedToGeneric. </summary>
		IsLinkedToGeneric,
		///<summary>OverrideSubType. </summary>
		OverrideSubType,
		///<summary>ShortDescription. </summary>
		ShortDescription,
		///<summary>InheritName. </summary>
		InheritName,
		///<summary>InheritPrice. </summary>
		InheritPrice,
		///<summary>InheritButtonText. </summary>
		InheritButtonText,
		///<summary>InheritCustomizeButtonText. </summary>
		InheritCustomizeButtonText,
		///<summary>InheritWebTypeTabletUrl. </summary>
		InheritWebTypeTabletUrl,
		///<summary>InheritWebTypeSmartphoneUrl. </summary>
		InheritWebTypeSmartphoneUrl,
		///<summary>InheritDescription. </summary>
		InheritDescription,
		///<summary>InheritColor. </summary>
		InheritColor,
		///<summary>InheritMedia. </summary>
		InheritMedia,
		///<summary>TaxTariffId. </summary>
		TaxTariffId,
		///<summary>IsAlcoholic. </summary>
		IsAlcoholic,
		///<summary>IsAvailable. </summary>
		IsAvailable,
		///<summary>SystemName. </summary>
		SystemName,
		///<summary>InheritSystemName. </summary>
		InheritSystemName,
		///<summary>ExternalIdentifier. </summary>
		ExternalIdentifier,
		///<summary>InheritExternalIdentifier. </summary>
		InheritExternalIdentifier,
		///<summary>PointOfInterestId. </summary>
		PointOfInterestId,
		///<summary>CoversType. </summary>
		CoversType,
		/// <summary></summary>
		AmountOfFields
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Product. </summary>
	public partial class ProductRelations: RelationFactory
	{
		#region Custom Relation code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomRelationCode
        private const ReferentialConstraintDeleteRule DeleteRuleForAlterationoptionEntityUsingProductId = ReferentialConstraintDeleteRule.SetNull;
		private const ReferentialConstraintDeleteRule DeleteRuleForAlterationProductEntityUsingProductId = ReferentialConstraintDeleteRule.Cascade;
		private const ReferentialConstraintDeleteRule DeleteRuleForCategoryEntityUsingProductId = ReferentialConstraintDeleteRule.SetNull;
		private const ReferentialConstraintDeleteRule DeleteRuleForDeliverypointgroupEntityUsingHotSOSBatteryLowProductId = ReferentialConstraintDeleteRule.SetNull;
        private const ReferentialConstraintDeleteRule DeleteRuleForMediaEntityUsingActionProductId = ReferentialConstraintDeleteRule.SetNull;
        private const ReferentialConstraintDeleteRule DeleteRuleForMediaEntityUsingProductId = ReferentialConstraintDeleteRule.Cascade;
		private const ReferentialConstraintDeleteRule DeleteRuleForOrderitemEntityUsingProductId = ReferentialConstraintDeleteRule.SetNull;
        private const ReferentialConstraintDeleteRule DeleteRuleForOrderitemAlterationitemEntityUsingAlterationoptionProductId = ReferentialConstraintDeleteRule.NoAction;
        private const ReferentialConstraintDeleteRule DeleteRuleForOrderitemTagEntityUsingProductId = ReferentialConstraintDeleteRule.NoAction;
		private const ReferentialConstraintDeleteRule DeleteRuleForOutletEntityUsingTippingProductId = ReferentialConstraintDeleteRule.NoAction;
        private const ReferentialConstraintDeleteRule DeleteRuleForOutletEntityUsingServiceChargeProductId = ReferentialConstraintDeleteRule.NoAction;
        private const ReferentialConstraintDeleteRule DeleteRuleForOutletEntityUsingDeliveryChargeProductId = ReferentialConstraintDeleteRule.NoAction;
		private const ReferentialConstraintDeleteRule DeleteRuleForPriceLevelItemEntityUsingProductId = ReferentialConstraintDeleteRule.Cascade;
		private const ReferentialConstraintDeleteRule DeleteRuleForProductEntityUsingBrandProductId = ReferentialConstraintDeleteRule.SetNull;
		private const ReferentialConstraintDeleteRule DeleteRuleForProductAlterationEntityUsingProductId = ReferentialConstraintDeleteRule.Cascade;
		private const ReferentialConstraintDeleteRule DeleteRuleForProductCategoryEntityUsingProductId = ReferentialConstraintDeleteRule.NoAction;
        private const ReferentialConstraintDeleteRule DeleteRuleForProductTagEntityUsingProductId = ReferentialConstraintDeleteRule.NoAction;
		private const ReferentialConstraintDeleteRule DeleteRuleForServiceMethodEntityUsingServiceChargeProductId = ReferentialConstraintDeleteRule.NoAction;
		private const ReferentialConstraintDeleteRule DeleteRuleForTerminalEntityUsingUnlockDeliverypointProductId = ReferentialConstraintDeleteRule.SetNull;
		private const ReferentialConstraintDeleteRule DeleteRuleForTerminalEntityUsingBatteryLowProductId = ReferentialConstraintDeleteRule.SetNull;
		private const ReferentialConstraintDeleteRule DeleteRuleForTerminalEntityUsingClientDisconnectedProductId = ReferentialConstraintDeleteRule.SetNull;
		private const ReferentialConstraintDeleteRule DeleteRuleForTerminalEntityUsingOrderFailedProductId = ReferentialConstraintDeleteRule.SetNull;
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		
		// private const ReferentialConstraintDeleteRule DeleteRuleForAlterationoptionEntityUsingProductId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForAlterationProductEntityUsingProductId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForCategoryEntityUsingProductId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForDeliverypointgroupEntityUsingHotSOSBatteryLowProductId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForMediaEntityUsingActionProductId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForMediaEntityUsingProductId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForOrderitemEntityUsingProductId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForOrderitemAlterationitemEntityUsingAlterationoptionProductId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForOrderitemTagEntityUsingProductId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForOutletEntityUsingDeliveryChargeProductId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForOutletEntityUsingServiceChargeProductId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForOutletEntityUsingTippingProductId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForPriceLevelItemEntityUsingProductId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForProductEntityUsingBrandProductId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForProductAlterationEntityUsingProductId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForProductCategoryEntityUsingProductId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForProductTagEntityUsingProductId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForServiceMethodEntityUsingServiceChargeProductId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForTerminalEntityUsingBatteryLowProductId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForTerminalEntityUsingClientDisconnectedProductId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForTerminalEntityUsingOrderFailedProductId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForTerminalEntityUsingUnlockDeliverypointProductId = ReferentialConstraintDeleteRule


		/// <summary>Gets all relations and delete rules of the ProductEntity as a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects.</summary>
		/// <returns>a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects</returns>
		public virtual Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> GetRelationsAndDeleteRules()
		{
			Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> toReturn = new Dictionary<IEntityRelation, ReferentialConstraintDeleteRule>();
			toReturn.Add(this.AlterationoptionEntityUsingProductId, ProductRelations.DeleteRuleForAlterationoptionEntityUsingProductId);
			toReturn.Add(this.AlterationProductEntityUsingProductId, ProductRelations.DeleteRuleForAlterationProductEntityUsingProductId);
			toReturn.Add(this.CategoryEntityUsingProductId, ProductRelations.DeleteRuleForCategoryEntityUsingProductId);
			toReturn.Add(this.DeliverypointgroupEntityUsingHotSOSBatteryLowProductId, ProductRelations.DeleteRuleForDeliverypointgroupEntityUsingHotSOSBatteryLowProductId);
			toReturn.Add(this.MediaEntityUsingActionProductId, ProductRelations.DeleteRuleForMediaEntityUsingActionProductId);
			toReturn.Add(this.MediaEntityUsingProductId, ProductRelations.DeleteRuleForMediaEntityUsingProductId);
			toReturn.Add(this.OrderitemEntityUsingProductId, ProductRelations.DeleteRuleForOrderitemEntityUsingProductId);
			toReturn.Add(this.OrderitemAlterationitemEntityUsingAlterationoptionProductId, ProductRelations.DeleteRuleForOrderitemAlterationitemEntityUsingAlterationoptionProductId);
			toReturn.Add(this.OrderitemTagEntityUsingProductId, ProductRelations.DeleteRuleForOrderitemTagEntityUsingProductId);
			toReturn.Add(this.OutletEntityUsingDeliveryChargeProductId, ProductRelations.DeleteRuleForOutletEntityUsingDeliveryChargeProductId);
			toReturn.Add(this.OutletEntityUsingServiceChargeProductId, ProductRelations.DeleteRuleForOutletEntityUsingServiceChargeProductId);
			toReturn.Add(this.OutletEntityUsingTippingProductId, ProductRelations.DeleteRuleForOutletEntityUsingTippingProductId);
			toReturn.Add(this.PriceLevelItemEntityUsingProductId, ProductRelations.DeleteRuleForPriceLevelItemEntityUsingProductId);
			toReturn.Add(this.ProductEntityUsingBrandProductId, ProductRelations.DeleteRuleForProductEntityUsingBrandProductId);
			toReturn.Add(this.ProductAlterationEntityUsingProductId, ProductRelations.DeleteRuleForProductAlterationEntityUsingProductId);
			toReturn.Add(this.ProductCategoryEntityUsingProductId, ProductRelations.DeleteRuleForProductCategoryEntityUsingProductId);
			toReturn.Add(this.ProductTagEntityUsingProductId, ProductRelations.DeleteRuleForProductTagEntityUsingProductId);
			toReturn.Add(this.ServiceMethodEntityUsingServiceChargeProductId, ProductRelations.DeleteRuleForServiceMethodEntityUsingServiceChargeProductId);
			toReturn.Add(this.TerminalEntityUsingBatteryLowProductId, ProductRelations.DeleteRuleForTerminalEntityUsingBatteryLowProductId);
			toReturn.Add(this.TerminalEntityUsingClientDisconnectedProductId, ProductRelations.DeleteRuleForTerminalEntityUsingClientDisconnectedProductId);
			toReturn.Add(this.TerminalEntityUsingOrderFailedProductId, ProductRelations.DeleteRuleForTerminalEntityUsingOrderFailedProductId);
			toReturn.Add(this.TerminalEntityUsingUnlockDeliverypointProductId, ProductRelations.DeleteRuleForTerminalEntityUsingUnlockDeliverypointProductId);
			return toReturn;
		}		
	
	
		/// <summary>Returns a new IEntityRelation object, between ProductEntity and AlterationoptionEntity over the 1:n relation they have, using the relation between the fields: Product.ProductId - Alterationoption.ProductId</summary>
		public virtual IEntityRelation AlterationoptionEntityUsingProductId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "AlterationoptionCollection", true, new[] { ProductFields.ProductId, AlterationoptionFields.ProductId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between ProductEntity and AlterationProductEntity over the 1:n relation they have, using the relation between the fields: Product.ProductId - AlterationProduct.ProductId</summary>
		public virtual IEntityRelation AlterationProductEntityUsingProductId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "AlterationProductCollection", true, new[] { ProductFields.ProductId, AlterationProductFields.ProductId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between ProductEntity and CategoryEntity over the 1:n relation they have, using the relation between the fields: Product.ProductId - Category.ProductId</summary>
		public virtual IEntityRelation CategoryEntityUsingProductId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "CategoryCollection", true, new[] { ProductFields.ProductId, CategoryFields.ProductId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between ProductEntity and DeliverypointgroupEntity over the 1:n relation they have, using the relation between the fields: Product.ProductId - Deliverypointgroup.HotSOSBatteryLowProductId</summary>
		public virtual IEntityRelation DeliverypointgroupEntityUsingHotSOSBatteryLowProductId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "DeliverypointgroupCollection", true, new[] { ProductFields.ProductId, DeliverypointgroupFields.HotSOSBatteryLowProductId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between ProductEntity and MediaEntity over the 1:n relation they have, using the relation between the fields: Product.ProductId - Media.ActionProductId</summary>
		public virtual IEntityRelation MediaEntityUsingActionProductId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "MediaCollection", true, new[] { ProductFields.ProductId, MediaFields.ActionProductId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between ProductEntity and MediaEntity over the 1:n relation they have, using the relation between the fields: Product.ProductId - Media.ProductId</summary>
		public virtual IEntityRelation MediaEntityUsingProductId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "MediaCollection1", true, new[] { ProductFields.ProductId, MediaFields.ProductId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between ProductEntity and OrderitemEntity over the 1:n relation they have, using the relation between the fields: Product.ProductId - Orderitem.ProductId</summary>
		public virtual IEntityRelation OrderitemEntityUsingProductId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "OrderitemCollection", true, new[] { ProductFields.ProductId, OrderitemFields.ProductId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between ProductEntity and OrderitemAlterationitemEntity over the 1:n relation they have, using the relation between the fields: Product.ProductId - OrderitemAlterationitem.AlterationoptionProductId</summary>
		public virtual IEntityRelation OrderitemAlterationitemEntityUsingAlterationoptionProductId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "OrderitemAlterationitemCollection", true, new[] { ProductFields.ProductId, OrderitemAlterationitemFields.AlterationoptionProductId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between ProductEntity and OrderitemTagEntity over the 1:n relation they have, using the relation between the fields: Product.ProductId - OrderitemTag.ProductId</summary>
		public virtual IEntityRelation OrderitemTagEntityUsingProductId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "OrderitemTagCollection", true, new[] { ProductFields.ProductId, OrderitemTagFields.ProductId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between ProductEntity and OutletEntity over the 1:n relation they have, using the relation between the fields: Product.ProductId - Outlet.DeliveryChargeProductId</summary>
		public virtual IEntityRelation OutletEntityUsingDeliveryChargeProductId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "OutletCollection__", true, new[] { ProductFields.ProductId, OutletFields.DeliveryChargeProductId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between ProductEntity and OutletEntity over the 1:n relation they have, using the relation between the fields: Product.ProductId - Outlet.ServiceChargeProductId</summary>
		public virtual IEntityRelation OutletEntityUsingServiceChargeProductId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "OutletCollection_", true, new[] { ProductFields.ProductId, OutletFields.ServiceChargeProductId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between ProductEntity and OutletEntity over the 1:n relation they have, using the relation between the fields: Product.ProductId - Outlet.TippingProductId</summary>
		public virtual IEntityRelation OutletEntityUsingTippingProductId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "OutletCollection", true, new[] { ProductFields.ProductId, OutletFields.TippingProductId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between ProductEntity and PriceLevelItemEntity over the 1:n relation they have, using the relation between the fields: Product.ProductId - PriceLevelItem.ProductId</summary>
		public virtual IEntityRelation PriceLevelItemEntityUsingProductId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "PriceLevelItemCollection", true, new[] { ProductFields.ProductId, PriceLevelItemFields.ProductId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between ProductEntity and ProductEntity over the 1:n relation they have, using the relation between the fields: Product.ProductId - Product.BrandProductId</summary>
		public virtual IEntityRelation ProductEntityUsingBrandProductId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "BrandProductCollection", true, new[] { ProductFields.ProductId, ProductFields.BrandProductId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between ProductEntity and ProductAlterationEntity over the 1:n relation they have, using the relation between the fields: Product.ProductId - ProductAlteration.ProductId</summary>
		public virtual IEntityRelation ProductAlterationEntityUsingProductId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "ProductAlterationCollection", true, new[] { ProductFields.ProductId, ProductAlterationFields.ProductId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between ProductEntity and ProductCategoryEntity over the 1:n relation they have, using the relation between the fields: Product.ProductId - ProductCategory.ProductId</summary>
		public virtual IEntityRelation ProductCategoryEntityUsingProductId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "ProductCategoryCollection", true, new[] { ProductFields.ProductId, ProductCategoryFields.ProductId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between ProductEntity and ProductTagEntity over the 1:n relation they have, using the relation between the fields: Product.ProductId - ProductTag.ProductId</summary>
		public virtual IEntityRelation ProductTagEntityUsingProductId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "ProductTagCollection", true, new[] { ProductFields.ProductId, ProductTagFields.ProductId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between ProductEntity and ServiceMethodEntity over the 1:n relation they have, using the relation between the fields: Product.ProductId - ServiceMethod.ServiceChargeProductId</summary>
		public virtual IEntityRelation ServiceMethodEntityUsingServiceChargeProductId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "ServiceMethodCollection", true, new[] { ProductFields.ProductId, ServiceMethodFields.ServiceChargeProductId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between ProductEntity and TerminalEntity over the 1:n relation they have, using the relation between the fields: Product.ProductId - Terminal.BatteryLowProductId</summary>
		public virtual IEntityRelation TerminalEntityUsingBatteryLowProductId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "TerminalCollection1", true, new[] { ProductFields.ProductId, TerminalFields.BatteryLowProductId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between ProductEntity and TerminalEntity over the 1:n relation they have, using the relation between the fields: Product.ProductId - Terminal.ClientDisconnectedProductId</summary>
		public virtual IEntityRelation TerminalEntityUsingClientDisconnectedProductId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "TerminalCollection2", true, new[] { ProductFields.ProductId, TerminalFields.ClientDisconnectedProductId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between ProductEntity and TerminalEntity over the 1:n relation they have, using the relation between the fields: Product.ProductId - Terminal.OrderFailedProductId</summary>
		public virtual IEntityRelation TerminalEntityUsingOrderFailedProductId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "TerminalCollection3", true, new[] { ProductFields.ProductId, TerminalFields.OrderFailedProductId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between ProductEntity and TerminalEntity over the 1:n relation they have, using the relation between the fields: Product.ProductId - Terminal.UnlockDeliverypointProductId</summary>
		public virtual IEntityRelation TerminalEntityUsingUnlockDeliverypointProductId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "TerminalCollection", true, new[] { ProductFields.ProductId, TerminalFields.UnlockDeliverypointProductId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between ProductEntity and CompanyEntity over the m:1 relation they have, using the relation between the fields: Product.CompanyId - Company.CompanyId</summary>
		public virtual IEntityRelation CompanyEntityUsingCompanyId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Company", false, new[] { CompanyFields.CompanyId, ProductFields.CompanyId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between ProductEntity and ExternalProductEntity over the m:1 relation they have, using the relation between the fields: Product.ExternalProductId - ExternalProduct.ExternalProductId</summary>
		public virtual IEntityRelation ExternalProductEntityUsingExternalProductId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "ExternalProduct", false, new[] { ExternalProductFields.ExternalProductId, ProductFields.ExternalProductId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between ProductEntity and ProductEntity over the m:1 relation they have, using the relation between the fields: Product.BrandProductId - Product.ProductId</summary>
		public virtual IEntityRelation ProductEntityUsingProductIdBrandProductId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "BrandProduct", false, new[] { ProductFields.ProductId, ProductFields.BrandProductId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between ProductEntity and RouteEntity over the m:1 relation they have, using the relation between the fields: Product.RouteId - Route.RouteId</summary>
		public virtual IEntityRelation RouteEntityUsingRouteId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Route", false, new[] { RouteFields.RouteId, ProductFields.RouteId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between ProductEntity and ScheduleEntity over the m:1 relation they have, using the relation between the fields: Product.ScheduleId - Schedule.ScheduleId</summary>
		public virtual IEntityRelation ScheduleEntityUsingScheduleId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Schedule", false, new[] { ScheduleFields.ScheduleId, ProductFields.ScheduleId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between ProductEntity and TaxTariffEntity over the m:1 relation they have, using the relation between the fields: Product.TaxTariffId - TaxTariff.TaxTariffId</summary>
		public virtual IEntityRelation TaxTariffEntityUsingTaxTariffId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "TaxTariff", false, new[] { TaxTariffFields.TaxTariffId, ProductFields.TaxTariffId }); }
		}

	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticProductRelations
	{
		internal static readonly IEntityRelation AlterationoptionEntityUsingProductIdStatic = new ProductRelations().AlterationoptionEntityUsingProductId;
		internal static readonly IEntityRelation AlterationProductEntityUsingProductIdStatic = new ProductRelations().AlterationProductEntityUsingProductId;
		internal static readonly IEntityRelation CategoryEntityUsingProductIdStatic = new ProductRelations().CategoryEntityUsingProductId;
		internal static readonly IEntityRelation DeliverypointgroupEntityUsingHotSOSBatteryLowProductIdStatic = new ProductRelations().DeliverypointgroupEntityUsingHotSOSBatteryLowProductId;
		internal static readonly IEntityRelation MediaEntityUsingActionProductIdStatic = new ProductRelations().MediaEntityUsingActionProductId;
		internal static readonly IEntityRelation MediaEntityUsingProductIdStatic = new ProductRelations().MediaEntityUsingProductId;
		internal static readonly IEntityRelation OrderitemEntityUsingProductIdStatic = new ProductRelations().OrderitemEntityUsingProductId;
		internal static readonly IEntityRelation OrderitemAlterationitemEntityUsingAlterationoptionProductIdStatic = new ProductRelations().OrderitemAlterationitemEntityUsingAlterationoptionProductId;
		internal static readonly IEntityRelation OrderitemTagEntityUsingProductIdStatic = new ProductRelations().OrderitemTagEntityUsingProductId;
		internal static readonly IEntityRelation OutletEntityUsingDeliveryChargeProductIdStatic = new ProductRelations().OutletEntityUsingDeliveryChargeProductId;
		internal static readonly IEntityRelation OutletEntityUsingServiceChargeProductIdStatic = new ProductRelations().OutletEntityUsingServiceChargeProductId;
		internal static readonly IEntityRelation OutletEntityUsingTippingProductIdStatic = new ProductRelations().OutletEntityUsingTippingProductId;
		internal static readonly IEntityRelation PriceLevelItemEntityUsingProductIdStatic = new ProductRelations().PriceLevelItemEntityUsingProductId;
		internal static readonly IEntityRelation ProductEntityUsingBrandProductIdStatic = new ProductRelations().ProductEntityUsingBrandProductId;
		internal static readonly IEntityRelation ProductAlterationEntityUsingProductIdStatic = new ProductRelations().ProductAlterationEntityUsingProductId;
		internal static readonly IEntityRelation ProductCategoryEntityUsingProductIdStatic = new ProductRelations().ProductCategoryEntityUsingProductId;
		internal static readonly IEntityRelation ProductTagEntityUsingProductIdStatic = new ProductRelations().ProductTagEntityUsingProductId;
		internal static readonly IEntityRelation ServiceMethodEntityUsingServiceChargeProductIdStatic = new ProductRelations().ServiceMethodEntityUsingServiceChargeProductId;
		internal static readonly IEntityRelation TerminalEntityUsingBatteryLowProductIdStatic = new ProductRelations().TerminalEntityUsingBatteryLowProductId;
		internal static readonly IEntityRelation TerminalEntityUsingClientDisconnectedProductIdStatic = new ProductRelations().TerminalEntityUsingClientDisconnectedProductId;
		internal static readonly IEntityRelation TerminalEntityUsingOrderFailedProductIdStatic = new ProductRelations().TerminalEntityUsingOrderFailedProductId;
		internal static readonly IEntityRelation TerminalEntityUsingUnlockDeliverypointProductIdStatic = new ProductRelations().TerminalEntityUsingUnlockDeliverypointProductId;
		internal static readonly IEntityRelation CompanyEntityUsingCompanyIdStatic = new ProductRelations().CompanyEntityUsingCompanyId;
		internal static readonly IEntityRelation ExternalProductEntityUsingExternalProductIdStatic = new ProductRelations().ExternalProductEntityUsingExternalProductId;
		internal static readonly IEntityRelation ProductEntityUsingProductIdBrandProductIdStatic = new ProductRelations().ProductEntityUsingProductIdBrandProductId;
		internal static readonly IEntityRelation RouteEntityUsingRouteIdStatic = new ProductRelations().RouteEntityUsingRouteId;
		internal static readonly IEntityRelation ScheduleEntityUsingScheduleIdStatic = new ProductRelations().ScheduleEntityUsingScheduleId;
		internal static readonly IEntityRelation TaxTariffEntityUsingTaxTariffIdStatic = new ProductRelations().TaxTariffEntityUsingTaxTariffId;

		/// <summary>CTor</summary>
		static StaticProductRelations() { }
	}
}

