﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 5.8
// Code is generated on: 
// Code is generated using templates: Crave.TemplateBindings
// Templates vendor: Crave Interactive
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.FactoryClasses;
using Crave.Ordering.Shared.Data.LLBLGen.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

// ReSharper disable All
namespace Crave.Ordering.Shared.Data.LLBLGen.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'ApplicationConfiguration'.<br/><br/></summary>
	[Serializable]
	public partial class ApplicationConfigurationEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END
	
	{
		private EntityCollection<FeatureFlagEntity> _featureFlagCollection;
		private EntityCollection<MediaEntity> _mediaCollection;
		private CompanyEntity _company;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		private static ApplicationConfigurationEntityStaticMetaData _staticMetaData = new ApplicationConfigurationEntityStaticMetaData();
		private static ApplicationConfigurationRelations _relationsFactory = new ApplicationConfigurationRelations();		

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Company</summary>
			public static readonly string Company = "Company";
			/// <summary>Member name FeatureFlagCollection</summary>
			public static readonly string FeatureFlagCollection = "FeatureFlagCollection";
			/// <summary>Member name MediaCollection</summary>
			public static readonly string MediaCollection = "MediaCollection";
		}

		/// <summary>Static meta-data storage for navigator related information</summary>
		protected class ApplicationConfigurationEntityStaticMetaData : EntityStaticMetaDataBase
		{
			public ApplicationConfigurationEntityStaticMetaData()
			{
				SetEntityCoreInfo("ApplicationConfigurationEntity", InheritanceHierarchyType.None, false, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ApplicationConfigurationEntity, typeof(ApplicationConfigurationEntity), typeof(ApplicationConfigurationEntityFactory), false);
				AddNavigatorMetaData<ApplicationConfigurationEntity, EntityCollection<FeatureFlagEntity>>("FeatureFlagCollection", a => a._featureFlagCollection, (a, b) => a._featureFlagCollection = b, a => a.FeatureFlagCollection, () => new ApplicationConfigurationRelations().FeatureFlagEntityUsingApplicationConfigurationId, typeof(FeatureFlagEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.FeatureFlagEntity);
				AddNavigatorMetaData<ApplicationConfigurationEntity, EntityCollection<MediaEntity>>("MediaCollection", a => a._mediaCollection, (a, b) => a._mediaCollection = b, a => a.MediaCollection, () => new ApplicationConfigurationRelations().MediaEntityUsingApplicationConfigurationId, typeof(MediaEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.MediaEntity);
				AddNavigatorMetaData<ApplicationConfigurationEntity, CompanyEntity>("Company", "ApplicationConfigurationCollection", (a, b) => a._company = b, a => a._company, (a, b) => a.Company = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticApplicationConfigurationRelations.CompanyEntityUsingCompanyIdStatic, ()=>new ApplicationConfigurationRelations().CompanyEntityUsingCompanyId, null, new int[] { (int)ApplicationConfigurationFieldIndex.CompanyId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.CompanyEntity);
			}
		}

		/// <summary>Static ctor</summary>
		static ApplicationConfigurationEntity()
		{
		}

		/// <summary> CTor</summary>
		public ApplicationConfigurationEntity()
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public ApplicationConfigurationEntity(IEntityFields2 fields)
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this ApplicationConfigurationEntity</param>
		public ApplicationConfigurationEntity(IValidator validator)
		{
			InitClassEmpty(validator, null);
		}

		/// <summary> CTor</summary>
		/// <param name="applicationConfigurationId">PK value for ApplicationConfiguration which data should be fetched into this ApplicationConfiguration object</param>
		public ApplicationConfigurationEntity(System.Int32 applicationConfigurationId) : this(applicationConfigurationId, null)
		{
		}

		/// <summary> CTor</summary>
		/// <param name="applicationConfigurationId">PK value for ApplicationConfiguration which data should be fetched into this ApplicationConfiguration object</param>
		/// <param name="validator">The custom validator object for this ApplicationConfigurationEntity</param>
		public ApplicationConfigurationEntity(System.Int32 applicationConfigurationId, IValidator validator)
		{
			InitClassEmpty(validator, null);
			this.ApplicationConfigurationId = applicationConfigurationId;
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ApplicationConfigurationEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'FeatureFlag' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoFeatureFlagCollection() { return CreateRelationInfoForNavigator("FeatureFlagCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Media' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoMediaCollection() { return CreateRelationInfoForNavigator("MediaCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Company' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoCompany() { return CreateRelationInfoForNavigator("Company"); }
		
		/// <inheritdoc/>
		protected override EntityStaticMetaDataBase GetEntityStaticMetaData() {	return _staticMetaData; }

		/// <summary>Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		/// <summary>Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this ApplicationConfigurationEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary>The relations object holding all relations of this entity with other entity classes.</summary>
		public static ApplicationConfigurationRelations Relations { get { return _relationsFactory; } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'FeatureFlag' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathFeatureFlagCollection { get { return _staticMetaData.GetPrefetchPathElement("FeatureFlagCollection", CommonEntityBase.CreateEntityCollection<FeatureFlagEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Media' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathMediaCollection { get { return _staticMetaData.GetPrefetchPathElement("MediaCollection", CommonEntityBase.CreateEntityCollection<MediaEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Company' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathCompanyEntity { get { return _staticMetaData.GetPrefetchPathElement("Company", CommonEntityBase.CreateEntityCollection<CompanyEntity>()); } }

		/// <summary>The ApplicationConfigurationId property of the Entity ApplicationConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ApplicationConfiguration"."ApplicationConfigurationId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 ApplicationConfigurationId
		{
			get { return (System.Int32)GetValue((int)ApplicationConfigurationFieldIndex.ApplicationConfigurationId, true); }
			set { SetValue((int)ApplicationConfigurationFieldIndex.ApplicationConfigurationId, value); }		}

		/// <summary>The ExternalId property of the Entity ApplicationConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ApplicationConfiguration"."ExternalId".<br/>Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 6.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String ExternalId
		{
			get { return (System.String)GetValue((int)ApplicationConfigurationFieldIndex.ExternalId, true); }
			set	{ SetValue((int)ApplicationConfigurationFieldIndex.ExternalId, value); }
		}

		/// <summary>The CompanyId property of the Entity ApplicationConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ApplicationConfiguration"."CompanyId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CompanyId
		{
			get { return (System.Int32)GetValue((int)ApplicationConfigurationFieldIndex.CompanyId, true); }
			set	{ SetValue((int)ApplicationConfigurationFieldIndex.CompanyId, value); }
		}

		/// <summary>The LandingPageId property of the Entity ApplicationConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ApplicationConfiguration"."LandingPageId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> LandingPageId
		{
			get { return (Nullable<System.Int32>)GetValue((int)ApplicationConfigurationFieldIndex.LandingPageId, false); }
			set	{ SetValue((int)ApplicationConfigurationFieldIndex.LandingPageId, value); }
		}

		/// <summary>The Name property of the Entity ApplicationConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ApplicationConfiguration"."Name".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)ApplicationConfigurationFieldIndex.Name, true); }
			set	{ SetValue((int)ApplicationConfigurationFieldIndex.Name, value); }
		}

		/// <summary>The ShortName property of the Entity ApplicationConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ApplicationConfiguration"."ShortName".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ShortName
		{
			get { return (System.String)GetValue((int)ApplicationConfigurationFieldIndex.ShortName, true); }
			set	{ SetValue((int)ApplicationConfigurationFieldIndex.ShortName, value); }
		}

		/// <summary>The Description property of the Entity ApplicationConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ApplicationConfiguration"."Description".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 1024.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)ApplicationConfigurationFieldIndex.Description, true); }
			set	{ SetValue((int)ApplicationConfigurationFieldIndex.Description, value); }
		}

		/// <summary>The CreatedUTC property of the Entity ApplicationConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ApplicationConfiguration"."CreatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime CreatedUTC
		{
			get { return (System.DateTime)GetValue((int)ApplicationConfigurationFieldIndex.CreatedUTC, true); }
			set	{ SetValue((int)ApplicationConfigurationFieldIndex.CreatedUTC, value); }
		}

		/// <summary>The CreatedBy property of the Entity ApplicationConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ApplicationConfiguration"."CreatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CreatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)ApplicationConfigurationFieldIndex.CreatedBy, false); }
			set	{ SetValue((int)ApplicationConfigurationFieldIndex.CreatedBy, value); }
		}

		/// <summary>The Updated property of the Entity ApplicationConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ApplicationConfiguration"."Updated".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> Updated
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ApplicationConfigurationFieldIndex.Updated, false); }
			set	{ SetValue((int)ApplicationConfigurationFieldIndex.Updated, value); }
		}

		/// <summary>The UpdatedBy property of the Entity ApplicationConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ApplicationConfiguration"."UpdatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UpdatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)ApplicationConfigurationFieldIndex.UpdatedBy, false); }
			set	{ SetValue((int)ApplicationConfigurationFieldIndex.UpdatedBy, value); }
		}

		/// <summary>The UpdatedUTC property of the Entity ApplicationConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ApplicationConfiguration"."UpdatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ApplicationConfigurationFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)ApplicationConfigurationFieldIndex.UpdatedUTC, value); }
		}

		/// <summary>The CultureCode property of the Entity ApplicationConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ApplicationConfiguration"."CultureCode".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 10.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CultureCode
		{
			get { return (System.String)GetValue((int)ApplicationConfigurationFieldIndex.CultureCode, true); }
			set	{ SetValue((int)ApplicationConfigurationFieldIndex.CultureCode, value); }
		}

		/// <summary>The AnalyticsEnvironment property of the Entity ApplicationConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ApplicationConfiguration"."AnalyticsEnvironment".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 AnalyticsEnvironment
		{
			get { return (System.Int32)GetValue((int)ApplicationConfigurationFieldIndex.AnalyticsEnvironment, true); }
			set	{ SetValue((int)ApplicationConfigurationFieldIndex.AnalyticsEnvironment, value); }
		}

		/// <summary>The GoogleAnalyticsId property of the Entity ApplicationConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ApplicationConfiguration"."GoogleAnalyticsId".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String GoogleAnalyticsId
		{
			get { return (System.String)GetValue((int)ApplicationConfigurationFieldIndex.GoogleAnalyticsId, true); }
			set	{ SetValue((int)ApplicationConfigurationFieldIndex.GoogleAnalyticsId, value); }
		}

		/// <summary>The GoogleTagManagerId property of the Entity ApplicationConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ApplicationConfiguration"."GoogleTagManagerId".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String GoogleTagManagerId
		{
			get { return (System.String)GetValue((int)ApplicationConfigurationFieldIndex.GoogleTagManagerId, true); }
			set	{ SetValue((int)ApplicationConfigurationFieldIndex.GoogleTagManagerId, value); }
		}

		/// <summary>The EnableCookieWall property of the Entity ApplicationConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ApplicationConfiguration"."EnableCookieWall".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean EnableCookieWall
		{
			get { return (System.Boolean)GetValue((int)ApplicationConfigurationFieldIndex.EnableCookieWall, true); }
			set	{ SetValue((int)ApplicationConfigurationFieldIndex.EnableCookieWall, value); }
		}

		/// <summary>The ProductSwipingEnabled property of the Entity ApplicationConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ApplicationConfiguration"."ProductSwipingEnabled".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean ProductSwipingEnabled
		{
			get { return (System.Boolean)GetValue((int)ApplicationConfigurationFieldIndex.ProductSwipingEnabled, true); }
			set	{ SetValue((int)ApplicationConfigurationFieldIndex.ProductSwipingEnabled, value); }
		}

		/// <summary>Gets the EntityCollection with the related entities of type 'FeatureFlagEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(FeatureFlagEntity))]
		public virtual EntityCollection<FeatureFlagEntity> FeatureFlagCollection { get { return GetOrCreateEntityCollection<FeatureFlagEntity, FeatureFlagEntityFactory>("ApplicationConfiguration", true, false, ref _featureFlagCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'MediaEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(MediaEntity))]
		public virtual EntityCollection<MediaEntity> MediaCollection { get { return GetOrCreateEntityCollection<MediaEntity, MediaEntityFactory>("ApplicationConfiguration", true, false, ref _mediaCollection); } }

		/// <summary>Gets / sets related entity of type 'CompanyEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual CompanyEntity Company
		{
			get { return _company; }
			set { SetSingleRelatedEntityNavigator(value, "Company"); }
		}


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen
{
	public enum ApplicationConfigurationFieldIndex
	{
		///<summary>ApplicationConfigurationId. </summary>
		ApplicationConfigurationId,
		///<summary>ExternalId. </summary>
		ExternalId,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>LandingPageId. </summary>
		LandingPageId,
		///<summary>Name. </summary>
		Name,
		///<summary>ShortName. </summary>
		ShortName,
		///<summary>Description. </summary>
		Description,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>Updated. </summary>
		Updated,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>CultureCode. </summary>
		CultureCode,
		///<summary>AnalyticsEnvironment. </summary>
		AnalyticsEnvironment,
		///<summary>GoogleAnalyticsId. </summary>
		GoogleAnalyticsId,
		///<summary>GoogleTagManagerId. </summary>
		GoogleTagManagerId,
		///<summary>EnableCookieWall. </summary>
		EnableCookieWall,
		///<summary>ProductSwipingEnabled. </summary>
		ProductSwipingEnabled,
		/// <summary></summary>
		AmountOfFields
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: ApplicationConfiguration. </summary>
	public partial class ApplicationConfigurationRelations: RelationFactory
	{
		#region Custom Relation code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomRelationCode
        private const ReferentialConstraintDeleteRule DeleteRuleForFeatureFlagEntityUsingApplicationConfigurationId = ReferentialConstraintDeleteRule.Cascade;
        private const ReferentialConstraintDeleteRule DeleteRuleForMediaEntityUsingApplicationConfigurationId = ReferentialConstraintDeleteRule.Cascade;
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		
		// private const ReferentialConstraintDeleteRule DeleteRuleForFeatureFlagEntityUsingApplicationConfigurationId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForMediaEntityUsingApplicationConfigurationId = ReferentialConstraintDeleteRule


		/// <summary>Gets all relations and delete rules of the ApplicationConfigurationEntity as a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects.</summary>
		/// <returns>a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects</returns>
		public virtual Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> GetRelationsAndDeleteRules()
		{
			Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> toReturn = new Dictionary<IEntityRelation, ReferentialConstraintDeleteRule>();
			toReturn.Add(this.FeatureFlagEntityUsingApplicationConfigurationId, ApplicationConfigurationRelations.DeleteRuleForFeatureFlagEntityUsingApplicationConfigurationId);
			toReturn.Add(this.MediaEntityUsingApplicationConfigurationId, ApplicationConfigurationRelations.DeleteRuleForMediaEntityUsingApplicationConfigurationId);
			return toReturn;
		}		
	
	
		/// <summary>Returns a new IEntityRelation object, between ApplicationConfigurationEntity and FeatureFlagEntity over the 1:n relation they have, using the relation between the fields: ApplicationConfiguration.ApplicationConfigurationId - FeatureFlag.ApplicationConfigurationId</summary>
		public virtual IEntityRelation FeatureFlagEntityUsingApplicationConfigurationId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "FeatureFlagCollection", true, new[] { ApplicationConfigurationFields.ApplicationConfigurationId, FeatureFlagFields.ApplicationConfigurationId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between ApplicationConfigurationEntity and MediaEntity over the 1:n relation they have, using the relation between the fields: ApplicationConfiguration.ApplicationConfigurationId - Media.ApplicationConfigurationId</summary>
		public virtual IEntityRelation MediaEntityUsingApplicationConfigurationId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "MediaCollection", true, new[] { ApplicationConfigurationFields.ApplicationConfigurationId, MediaFields.ApplicationConfigurationId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between ApplicationConfigurationEntity and CompanyEntity over the m:1 relation they have, using the relation between the fields: ApplicationConfiguration.CompanyId - Company.CompanyId</summary>
		public virtual IEntityRelation CompanyEntityUsingCompanyId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Company", false, new[] { CompanyFields.CompanyId, ApplicationConfigurationFields.CompanyId }); }
		}

	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticApplicationConfigurationRelations
	{
		internal static readonly IEntityRelation FeatureFlagEntityUsingApplicationConfigurationIdStatic = new ApplicationConfigurationRelations().FeatureFlagEntityUsingApplicationConfigurationId;
		internal static readonly IEntityRelation MediaEntityUsingApplicationConfigurationIdStatic = new ApplicationConfigurationRelations().MediaEntityUsingApplicationConfigurationId;
		internal static readonly IEntityRelation CompanyEntityUsingCompanyIdStatic = new ApplicationConfigurationRelations().CompanyEntityUsingCompanyId;

		/// <summary>CTor</summary>
		static StaticApplicationConfigurationRelations() { }
	}
}

