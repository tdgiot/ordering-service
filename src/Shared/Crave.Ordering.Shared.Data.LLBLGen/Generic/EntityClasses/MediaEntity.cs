﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 5.8
// Code is generated on: 
// Code is generated using templates: Crave.TemplateBindings
// Templates vendor: Crave Interactive
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.FactoryClasses;
using Crave.Ordering.Shared.Data.LLBLGen.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

// ReSharper disable All
namespace Crave.Ordering.Shared.Data.LLBLGen.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'Media'.<br/><br/></summary>
	[Serializable]
	public partial class MediaEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END
	
	{
		private EntityCollection<MediaEntity> _mediaCollection;
		private EntityCollection<MediaRatioTypeMediaEntity> _mediaRatioTypeMediaCollection;
		private AlterationEntity _alteration;
		private AlterationoptionEntity _alterationoption;
		private ApplicationConfigurationEntity _applicationConfiguration;
		private CategoryEntity _category;
		private CategoryEntity _category1;
		private ClientConfigurationEntity _clientConfiguration;
		private CompanyEntity _company;
		private DeliverypointgroupEntity _deliverypointgroup;
		private MediaEntity _media;
		private ProductEntity _product;
		private ProductEntity _product1;
		private RoutestephandlerEntity _routestephandler;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		private static MediaEntityStaticMetaData _staticMetaData = new MediaEntityStaticMetaData();
		private static MediaRelations _relationsFactory = new MediaRelations();		

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Alteration</summary>
			public static readonly string Alteration = "Alteration";
			/// <summary>Member name Alterationoption</summary>
			public static readonly string Alterationoption = "Alterationoption";
			/// <summary>Member name ApplicationConfiguration</summary>
			public static readonly string ApplicationConfiguration = "ApplicationConfiguration";
			/// <summary>Member name Category</summary>
			public static readonly string Category = "Category";
			/// <summary>Member name Category1</summary>
			public static readonly string Category1 = "Category1";
			/// <summary>Member name ClientConfiguration</summary>
			public static readonly string ClientConfiguration = "ClientConfiguration";
			/// <summary>Member name Company</summary>
			public static readonly string Company = "Company";
			/// <summary>Member name Deliverypointgroup</summary>
			public static readonly string Deliverypointgroup = "Deliverypointgroup";
			/// <summary>Member name Media</summary>
			public static readonly string Media = "Media";
			/// <summary>Member name Product</summary>
			public static readonly string Product = "Product";
			/// <summary>Member name Product1</summary>
			public static readonly string Product1 = "Product1";
			/// <summary>Member name Routestephandler</summary>
			public static readonly string Routestephandler = "Routestephandler";
			/// <summary>Member name MediaCollection</summary>
			public static readonly string MediaCollection = "MediaCollection";
			/// <summary>Member name MediaRatioTypeMediaCollection</summary>
			public static readonly string MediaRatioTypeMediaCollection = "MediaRatioTypeMediaCollection";
		}

		/// <summary>Static meta-data storage for navigator related information</summary>
		protected class MediaEntityStaticMetaData : EntityStaticMetaDataBase
		{
			public MediaEntityStaticMetaData()
			{
				SetEntityCoreInfo("MediaEntity", InheritanceHierarchyType.None, false, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.MediaEntity, typeof(MediaEntity), typeof(MediaEntityFactory), false);
				AddNavigatorMetaData<MediaEntity, EntityCollection<MediaEntity>>("MediaCollection", a => a._mediaCollection, (a, b) => a._mediaCollection = b, a => a.MediaCollection, () => new MediaRelations().MediaEntityUsingAgnosticMediaId, typeof(MediaEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.MediaEntity);
				AddNavigatorMetaData<MediaEntity, EntityCollection<MediaRatioTypeMediaEntity>>("MediaRatioTypeMediaCollection", a => a._mediaRatioTypeMediaCollection, (a, b) => a._mediaRatioTypeMediaCollection = b, a => a.MediaRatioTypeMediaCollection, () => new MediaRelations().MediaRatioTypeMediaEntityUsingMediaId, typeof(MediaRatioTypeMediaEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.MediaRatioTypeMediaEntity);
				AddNavigatorMetaData<MediaEntity, AlterationEntity>("Alteration", "MediaCollection", (a, b) => a._alteration = b, a => a._alteration, (a, b) => a.Alteration = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticMediaRelations.AlterationEntityUsingAlterationIdStatic, ()=>new MediaRelations().AlterationEntityUsingAlterationId, null, new int[] { (int)MediaFieldIndex.AlterationId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.AlterationEntity);
				AddNavigatorMetaData<MediaEntity, AlterationoptionEntity>("Alterationoption", "MediaCollection", (a, b) => a._alterationoption = b, a => a._alterationoption, (a, b) => a.Alterationoption = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticMediaRelations.AlterationoptionEntityUsingAlterationoptionIdStatic, ()=>new MediaRelations().AlterationoptionEntityUsingAlterationoptionId, null, new int[] { (int)MediaFieldIndex.AlterationoptionId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.AlterationoptionEntity);
				AddNavigatorMetaData<MediaEntity, ApplicationConfigurationEntity>("ApplicationConfiguration", "MediaCollection", (a, b) => a._applicationConfiguration = b, a => a._applicationConfiguration, (a, b) => a.ApplicationConfiguration = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticMediaRelations.ApplicationConfigurationEntityUsingApplicationConfigurationIdStatic, ()=>new MediaRelations().ApplicationConfigurationEntityUsingApplicationConfigurationId, null, new int[] { (int)MediaFieldIndex.ApplicationConfigurationId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ApplicationConfigurationEntity);
				AddNavigatorMetaData<MediaEntity, CategoryEntity>("Category", "MediaCollection", (a, b) => a._category = b, a => a._category, (a, b) => a.Category = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticMediaRelations.CategoryEntityUsingActionCategoryIdStatic, ()=>new MediaRelations().CategoryEntityUsingActionCategoryId, null, new int[] { (int)MediaFieldIndex.ActionCategoryId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.CategoryEntity);
				AddNavigatorMetaData<MediaEntity, CategoryEntity>("Category1", "MediaCollection1", (a, b) => a._category1 = b, a => a._category1, (a, b) => a.Category1 = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticMediaRelations.CategoryEntityUsingCategoryIdStatic, ()=>new MediaRelations().CategoryEntityUsingCategoryId, null, new int[] { (int)MediaFieldIndex.CategoryId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.CategoryEntity);
				AddNavigatorMetaData<MediaEntity, ClientConfigurationEntity>("ClientConfiguration", "MediaCollection", (a, b) => a._clientConfiguration = b, a => a._clientConfiguration, (a, b) => a.ClientConfiguration = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticMediaRelations.ClientConfigurationEntityUsingClientConfigurationIdStatic, ()=>new MediaRelations().ClientConfigurationEntityUsingClientConfigurationId, null, new int[] { (int)MediaFieldIndex.ClientConfigurationId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ClientConfigurationEntity);
				AddNavigatorMetaData<MediaEntity, CompanyEntity>("Company", "MediaCollection", (a, b) => a._company = b, a => a._company, (a, b) => a.Company = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticMediaRelations.CompanyEntityUsingCompanyIdStatic, ()=>new MediaRelations().CompanyEntityUsingCompanyId, null, new int[] { (int)MediaFieldIndex.CompanyId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.CompanyEntity);
				AddNavigatorMetaData<MediaEntity, DeliverypointgroupEntity>("Deliverypointgroup", "MediaCollection", (a, b) => a._deliverypointgroup = b, a => a._deliverypointgroup, (a, b) => a.Deliverypointgroup = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticMediaRelations.DeliverypointgroupEntityUsingDeliverypointgroupIdStatic, ()=>new MediaRelations().DeliverypointgroupEntityUsingDeliverypointgroupId, null, new int[] { (int)MediaFieldIndex.DeliverypointgroupId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.DeliverypointgroupEntity);
				AddNavigatorMetaData<MediaEntity, MediaEntity>("Media", "MediaCollection", (a, b) => a._media = b, a => a._media, (a, b) => a.Media = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticMediaRelations.MediaEntityUsingMediaIdAgnosticMediaIdStatic, ()=>new MediaRelations().MediaEntityUsingMediaIdAgnosticMediaId, null, new int[] { (int)MediaFieldIndex.AgnosticMediaId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.MediaEntity);
				AddNavigatorMetaData<MediaEntity, ProductEntity>("Product", "MediaCollection", (a, b) => a._product = b, a => a._product, (a, b) => a.Product = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticMediaRelations.ProductEntityUsingActionProductIdStatic, ()=>new MediaRelations().ProductEntityUsingActionProductId, null, new int[] { (int)MediaFieldIndex.ActionProductId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ProductEntity);
				AddNavigatorMetaData<MediaEntity, ProductEntity>("Product1", "MediaCollection1", (a, b) => a._product1 = b, a => a._product1, (a, b) => a.Product1 = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticMediaRelations.ProductEntityUsingProductIdStatic, ()=>new MediaRelations().ProductEntityUsingProductId, null, new int[] { (int)MediaFieldIndex.ProductId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ProductEntity);
				AddNavigatorMetaData<MediaEntity, RoutestephandlerEntity>("Routestephandler", "MediaCollection", (a, b) => a._routestephandler = b, a => a._routestephandler, (a, b) => a.Routestephandler = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticMediaRelations.RoutestephandlerEntityUsingRoutestephandlerIdStatic, ()=>new MediaRelations().RoutestephandlerEntityUsingRoutestephandlerId, null, new int[] { (int)MediaFieldIndex.RoutestephandlerId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.RoutestephandlerEntity);
			}
		}

		/// <summary>Static ctor</summary>
		static MediaEntity()
		{
		}

		/// <summary> CTor</summary>
		public MediaEntity()
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public MediaEntity(IEntityFields2 fields)
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this MediaEntity</param>
		public MediaEntity(IValidator validator)
		{
			InitClassEmpty(validator, null);
		}

		/// <summary> CTor</summary>
		/// <param name="mediaId">PK value for Media which data should be fetched into this Media object</param>
		public MediaEntity(System.Int32 mediaId) : this(mediaId, null)
		{
		}

		/// <summary> CTor</summary>
		/// <param name="mediaId">PK value for Media which data should be fetched into this Media object</param>
		/// <param name="validator">The custom validator object for this MediaEntity</param>
		public MediaEntity(System.Int32 mediaId, IValidator validator)
		{
			InitClassEmpty(validator, null);
			this.MediaId = mediaId;
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected MediaEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Media' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoMediaCollection() { return CreateRelationInfoForNavigator("MediaCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'MediaRatioTypeMedia' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoMediaRatioTypeMediaCollection() { return CreateRelationInfoForNavigator("MediaRatioTypeMediaCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Alteration' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoAlteration() { return CreateRelationInfoForNavigator("Alteration"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Alterationoption' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoAlterationoption() { return CreateRelationInfoForNavigator("Alterationoption"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'ApplicationConfiguration' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoApplicationConfiguration() { return CreateRelationInfoForNavigator("ApplicationConfiguration"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Category' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoCategory() { return CreateRelationInfoForNavigator("Category"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Category' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoCategory1() { return CreateRelationInfoForNavigator("Category1"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'ClientConfiguration' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoClientConfiguration() { return CreateRelationInfoForNavigator("ClientConfiguration"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Company' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoCompany() { return CreateRelationInfoForNavigator("Company"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Deliverypointgroup' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoDeliverypointgroup() { return CreateRelationInfoForNavigator("Deliverypointgroup"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Media' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoMedia() { return CreateRelationInfoForNavigator("Media"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Product' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoProduct() { return CreateRelationInfoForNavigator("Product"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Product' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoProduct1() { return CreateRelationInfoForNavigator("Product1"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Routestephandler' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoRoutestephandler() { return CreateRelationInfoForNavigator("Routestephandler"); }
		
		/// <inheritdoc/>
		protected override EntityStaticMetaDataBase GetEntityStaticMetaData() {	return _staticMetaData; }

		/// <summary>Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		/// <summary>Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this MediaEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary>The relations object holding all relations of this entity with other entity classes.</summary>
		public static MediaRelations Relations { get { return _relationsFactory; } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Media' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathMediaCollection { get { return _staticMetaData.GetPrefetchPathElement("MediaCollection", CommonEntityBase.CreateEntityCollection<MediaEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'MediaRatioTypeMedia' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathMediaRatioTypeMediaCollection { get { return _staticMetaData.GetPrefetchPathElement("MediaRatioTypeMediaCollection", CommonEntityBase.CreateEntityCollection<MediaRatioTypeMediaEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Alteration' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathAlterationEntity { get { return _staticMetaData.GetPrefetchPathElement("Alteration", CommonEntityBase.CreateEntityCollection<AlterationEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Alterationoption' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathAlterationoptionEntity { get { return _staticMetaData.GetPrefetchPathElement("Alterationoption", CommonEntityBase.CreateEntityCollection<AlterationoptionEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'ApplicationConfiguration' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathApplicationConfigurationEntity { get { return _staticMetaData.GetPrefetchPathElement("ApplicationConfiguration", CommonEntityBase.CreateEntityCollection<ApplicationConfigurationEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Category' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathCategoryEntity { get { return _staticMetaData.GetPrefetchPathElement("Category", CommonEntityBase.CreateEntityCollection<CategoryEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Category' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathCategory1Entity { get { return _staticMetaData.GetPrefetchPathElement("Category1", CommonEntityBase.CreateEntityCollection<CategoryEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'ClientConfiguration' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathClientConfigurationEntity { get { return _staticMetaData.GetPrefetchPathElement("ClientConfiguration", CommonEntityBase.CreateEntityCollection<ClientConfigurationEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Company' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathCompanyEntity { get { return _staticMetaData.GetPrefetchPathElement("Company", CommonEntityBase.CreateEntityCollection<CompanyEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Deliverypointgroup' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathDeliverypointgroupEntity { get { return _staticMetaData.GetPrefetchPathElement("Deliverypointgroup", CommonEntityBase.CreateEntityCollection<DeliverypointgroupEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Media' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathMediaEntity { get { return _staticMetaData.GetPrefetchPathElement("Media", CommonEntityBase.CreateEntityCollection<MediaEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Product' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathProductEntity { get { return _staticMetaData.GetPrefetchPathElement("Product", CommonEntityBase.CreateEntityCollection<ProductEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Product' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathProduct1Entity { get { return _staticMetaData.GetPrefetchPathElement("Product1", CommonEntityBase.CreateEntityCollection<ProductEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Routestephandler' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathRoutestephandlerEntity { get { return _staticMetaData.GetPrefetchPathElement("Routestephandler", CommonEntityBase.CreateEntityCollection<RoutestephandlerEntity>()); } }

		/// <summary>The MediaId property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."MediaId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 MediaId
		{
			get { return (System.Int32)GetValue((int)MediaFieldIndex.MediaId, true); }
			set { SetValue((int)MediaFieldIndex.MediaId, value); }		}

		/// <summary>The MediaType property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."MediaType".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Crave.Enums.MediaType MediaType
		{
			get { return (Crave.Enums.MediaType)GetValue((int)MediaFieldIndex.MediaType, true); }
			set	{ SetValue((int)MediaFieldIndex.MediaType, value); }
		}

		/// <summary>The SortOrder property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."SortOrder".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> SortOrder
		{
			get { return (Nullable<System.Int32>)GetValue((int)MediaFieldIndex.SortOrder, false); }
			set	{ SetValue((int)MediaFieldIndex.SortOrder, value); }
		}

		/// <summary>The DefaultItem property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."DefaultItem".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean DefaultItem
		{
			get { return (System.Boolean)GetValue((int)MediaFieldIndex.DefaultItem, true); }
			set	{ SetValue((int)MediaFieldIndex.DefaultItem, value); }
		}

		/// <summary>The Name property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."Name".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)MediaFieldIndex.Name, true); }
			set	{ SetValue((int)MediaFieldIndex.Name, value); }
		}

		/// <summary>The FilePathRelativeToMediaPath property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."FilePathRelativeToMediaPath".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FilePathRelativeToMediaPath
		{
			get { return (System.String)GetValue((int)MediaFieldIndex.FilePathRelativeToMediaPath, true); }
			set	{ SetValue((int)MediaFieldIndex.FilePathRelativeToMediaPath, value); }
		}

		/// <summary>The Description property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."Description".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)MediaFieldIndex.Description, true); }
			set	{ SetValue((int)MediaFieldIndex.Description, value); }
		}

		/// <summary>The MimeType property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."MimeType".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 150.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String MimeType
		{
			get { return (System.String)GetValue((int)MediaFieldIndex.MimeType, true); }
			set	{ SetValue((int)MediaFieldIndex.MimeType, value); }
		}

		/// <summary>The Extension property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."Extension".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 25.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Extension
		{
			get { return (System.String)GetValue((int)MediaFieldIndex.Extension, true); }
			set	{ SetValue((int)MediaFieldIndex.Extension, value); }
		}

		/// <summary>The SizeKb property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."SizeKb".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> SizeKb
		{
			get { return (Nullable<System.Int32>)GetValue((int)MediaFieldIndex.SizeKb, false); }
			set	{ SetValue((int)MediaFieldIndex.SizeKb, value); }
		}

		/// <summary>The RelatedCompanyId property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."RelatedCompanyId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> RelatedCompanyId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MediaFieldIndex.RelatedCompanyId, false); }
			set	{ SetValue((int)MediaFieldIndex.RelatedCompanyId, value); }
		}

		/// <summary>The CompanyId property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."CompanyId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CompanyId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MediaFieldIndex.CompanyId, false); }
			set	{ SetValue((int)MediaFieldIndex.CompanyId, value); }
		}

		/// <summary>The ProductId property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."ProductId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ProductId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MediaFieldIndex.ProductId, false); }
			set	{ SetValue((int)MediaFieldIndex.ProductId, value); }
		}

		/// <summary>The CategoryId property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."CategoryId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CategoryId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MediaFieldIndex.CategoryId, false); }
			set	{ SetValue((int)MediaFieldIndex.CategoryId, value); }
		}

		/// <summary>The AdvertisementId property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."AdvertisementId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> AdvertisementId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MediaFieldIndex.AdvertisementId, false); }
			set	{ SetValue((int)MediaFieldIndex.AdvertisementId, value); }
		}

		/// <summary>The EntertainmentId property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."EntertainmentId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> EntertainmentId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MediaFieldIndex.EntertainmentId, false); }
			set	{ SetValue((int)MediaFieldIndex.EntertainmentId, value); }
		}

		/// <summary>The AlterationoptionId property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."AlterationoptionId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> AlterationoptionId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MediaFieldIndex.AlterationoptionId, false); }
			set	{ SetValue((int)MediaFieldIndex.AlterationoptionId, value); }
		}

		/// <summary>The GenericproductId property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."GenericproductId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> GenericproductId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MediaFieldIndex.GenericproductId, false); }
			set	{ SetValue((int)MediaFieldIndex.GenericproductId, value); }
		}

		/// <summary>The GenericcategoryId property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."GenericcategoryId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> GenericcategoryId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MediaFieldIndex.GenericcategoryId, false); }
			set	{ SetValue((int)MediaFieldIndex.GenericcategoryId, value); }
		}

		/// <summary>The DeliverypointgroupId property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."DeliverypointgroupId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> DeliverypointgroupId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MediaFieldIndex.DeliverypointgroupId, false); }
			set	{ SetValue((int)MediaFieldIndex.DeliverypointgroupId, value); }
		}

		/// <summary>The PageElementId property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."PageElementId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> PageElementId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MediaFieldIndex.PageElementId, false); }
			set	{ SetValue((int)MediaFieldIndex.PageElementId, value); }
		}

		/// <summary>The PageTemplateElementId property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."PageTemplateElementId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> PageTemplateElementId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MediaFieldIndex.PageTemplateElementId, false); }
			set	{ SetValue((int)MediaFieldIndex.PageTemplateElementId, value); }
		}

		/// <summary>The SiteId property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."SiteId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> SiteId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MediaFieldIndex.SiteId, false); }
			set	{ SetValue((int)MediaFieldIndex.SiteId, value); }
		}

		/// <summary>The RoutestephandlerId property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."RoutestephandlerId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> RoutestephandlerId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MediaFieldIndex.RoutestephandlerId, false); }
			set	{ SetValue((int)MediaFieldIndex.RoutestephandlerId, value); }
		}

		/// <summary>The SurveyId property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."SurveyId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> SurveyId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MediaFieldIndex.SurveyId, false); }
			set	{ SetValue((int)MediaFieldIndex.SurveyId, value); }
		}

		/// <summary>The SurveyPageId property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."SurveyPageId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> SurveyPageId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MediaFieldIndex.SurveyPageId, false); }
			set	{ SetValue((int)MediaFieldIndex.SurveyPageId, value); }
		}

		/// <summary>The ActionProductId property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."ActionProductId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ActionProductId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MediaFieldIndex.ActionProductId, false); }
			set	{ SetValue((int)MediaFieldIndex.ActionProductId, value); }
		}

		/// <summary>The ActionCategoryId property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."ActionCategoryId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ActionCategoryId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MediaFieldIndex.ActionCategoryId, false); }
			set	{ SetValue((int)MediaFieldIndex.ActionCategoryId, value); }
		}

		/// <summary>The ActionEntertainmentId property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."ActionEntertainmentId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ActionEntertainmentId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MediaFieldIndex.ActionEntertainmentId, false); }
			set	{ SetValue((int)MediaFieldIndex.ActionEntertainmentId, value); }
		}

		/// <summary>The ActionEntertainmentcategoryId property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."ActionEntertainmentcategoryId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ActionEntertainmentcategoryId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MediaFieldIndex.ActionEntertainmentcategoryId, false); }
			set	{ SetValue((int)MediaFieldIndex.ActionEntertainmentcategoryId, value); }
		}

		/// <summary>The ActionSiteId property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."ActionSiteId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ActionSiteId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MediaFieldIndex.ActionSiteId, false); }
			set	{ SetValue((int)MediaFieldIndex.ActionSiteId, value); }
		}

		/// <summary>The ActionPageId property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."ActionPageId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ActionPageId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MediaFieldIndex.ActionPageId, false); }
			set	{ SetValue((int)MediaFieldIndex.ActionPageId, value); }
		}

		/// <summary>The ActionUrl property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."ActionUrl".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ActionUrl
		{
			get { return (System.String)GetValue((int)MediaFieldIndex.ActionUrl, true); }
			set	{ SetValue((int)MediaFieldIndex.ActionUrl, value); }
		}

		/// <summary>The FromSupplier property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."FromSupplier".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean FromSupplier
		{
			get { return (System.Boolean)GetValue((int)MediaFieldIndex.FromSupplier, true); }
			set	{ SetValue((int)MediaFieldIndex.FromSupplier, value); }
		}

		/// <summary>The AlterationId property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."AlterationId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> AlterationId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MediaFieldIndex.AlterationId, false); }
			set	{ SetValue((int)MediaFieldIndex.AlterationId, value); }
		}

		/// <summary>The PointOfInterestId property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."PointOfInterestId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> PointOfInterestId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MediaFieldIndex.PointOfInterestId, false); }
			set	{ SetValue((int)MediaFieldIndex.PointOfInterestId, value); }
		}

		/// <summary>The PageId property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."PageId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> PageId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MediaFieldIndex.PageId, false); }
			set	{ SetValue((int)MediaFieldIndex.PageId, value); }
		}

		/// <summary>The PageTemplateId property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."PageTemplateId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> PageTemplateId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MediaFieldIndex.PageTemplateId, false); }
			set	{ SetValue((int)MediaFieldIndex.PageTemplateId, value); }
		}

		/// <summary>The AttachmentId property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."AttachmentId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> AttachmentId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MediaFieldIndex.AttachmentId, false); }
			set	{ SetValue((int)MediaFieldIndex.AttachmentId, value); }
		}

		/// <summary>The UIWidgetId property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."UIWidgetId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UIWidgetId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MediaFieldIndex.UIWidgetId, false); }
			set	{ SetValue((int)MediaFieldIndex.UIWidgetId, value); }
		}

		/// <summary>The UIThemeId property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."UIThemeId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UIThemeId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MediaFieldIndex.UIThemeId, false); }
			set	{ SetValue((int)MediaFieldIndex.UIThemeId, value); }
		}

		/// <summary>The UIFooterItemId property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."UIFooterItemId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UIFooterItemId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MediaFieldIndex.UIFooterItemId, false); }
			set	{ SetValue((int)MediaFieldIndex.UIFooterItemId, value); }
		}

		/// <summary>The RoomControlSectionId property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."RoomControlSectionId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> RoomControlSectionId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MediaFieldIndex.RoomControlSectionId, false); }
			set	{ SetValue((int)MediaFieldIndex.RoomControlSectionId, value); }
		}

		/// <summary>The RoomControlSectionItemId property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."RoomControlSectionItemId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> RoomControlSectionItemId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MediaFieldIndex.RoomControlSectionItemId, false); }
			set	{ SetValue((int)MediaFieldIndex.RoomControlSectionItemId, value); }
		}

		/// <summary>The StationId property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."StationId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> StationId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MediaFieldIndex.StationId, false); }
			set	{ SetValue((int)MediaFieldIndex.StationId, value); }
		}

		/// <summary>The ClientConfigurationId property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."ClientConfigurationId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ClientConfigurationId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MediaFieldIndex.ClientConfigurationId, false); }
			set	{ SetValue((int)MediaFieldIndex.ClientConfigurationId, value); }
		}

		/// <summary>The ProductgroupId property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."ProductgroupId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ProductgroupId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MediaFieldIndex.ProductgroupId, false); }
			set	{ SetValue((int)MediaFieldIndex.ProductgroupId, value); }
		}

		/// <summary>The LandingPageId property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."LandingPageId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> LandingPageId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MediaFieldIndex.LandingPageId, false); }
			set	{ SetValue((int)MediaFieldIndex.LandingPageId, value); }
		}

		/// <summary>The CarouselItemId property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."CarouselItemId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CarouselItemId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MediaFieldIndex.CarouselItemId, false); }
			set	{ SetValue((int)MediaFieldIndex.CarouselItemId, value); }
		}

		/// <summary>The WidgetId property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."WidgetId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> WidgetId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MediaFieldIndex.WidgetId, false); }
			set	{ SetValue((int)MediaFieldIndex.WidgetId, value); }
		}

		/// <summary>The ApplicationConfigurationId property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."ApplicationConfigurationId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ApplicationConfigurationId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MediaFieldIndex.ApplicationConfigurationId, false); }
			set	{ SetValue((int)MediaFieldIndex.ApplicationConfigurationId, value); }
		}

		/// <summary>The JpgQuality property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."JpgQuality".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> JpgQuality
		{
			get { return (Nullable<System.Int32>)GetValue((int)MediaFieldIndex.JpgQuality, false); }
			set	{ SetValue((int)MediaFieldIndex.JpgQuality, value); }
		}

		/// <summary>The SizeMode property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."SizeMode".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 SizeMode
		{
			get { return (System.Int32)GetValue((int)MediaFieldIndex.SizeMode, true); }
			set	{ SetValue((int)MediaFieldIndex.SizeMode, value); }
		}

		/// <summary>The ZoomLevel property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."ZoomLevel".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ZoomLevel
		{
			get { return (System.Int32)GetValue((int)MediaFieldIndex.ZoomLevel, true); }
			set	{ SetValue((int)MediaFieldIndex.ZoomLevel, value); }
		}

		/// <summary>The MediaFileMd5 property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."MediaFileMd5".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 32.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String MediaFileMd5
		{
			get { return (System.String)GetValue((int)MediaFieldIndex.MediaFileMd5, true); }
			set	{ SetValue((int)MediaFieldIndex.MediaFileMd5, value); }
		}

		/// <summary>The CreatedBy property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."CreatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)MediaFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)MediaFieldIndex.CreatedBy, value); }
		}

		/// <summary>The UpdatedBy property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."UpdatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)MediaFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)MediaFieldIndex.UpdatedBy, value); }
		}

		/// <summary>The AgnosticMediaId property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."AgnosticMediaId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> AgnosticMediaId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MediaFieldIndex.AgnosticMediaId, false); }
			set	{ SetValue((int)MediaFieldIndex.AgnosticMediaId, value); }
		}

		/// <summary>The CreatedUTC property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."CreatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)MediaFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)MediaFieldIndex.CreatedUTC, value); }
		}

		/// <summary>The UpdatedUTC property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."UpdatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)MediaFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)MediaFieldIndex.UpdatedUTC, value); }
		}

		/// <summary>The LastDistributedVersionTicksAzure property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."LastDistributedVersionTicksAzure".<br/>Table field type characteristics (type, precision, scale, length): BigInt, 19, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> LastDistributedVersionTicksAzure
		{
			get { return (Nullable<System.Int64>)GetValue((int)MediaFieldIndex.LastDistributedVersionTicksAzure, false); }
			set	{ SetValue((int)MediaFieldIndex.LastDistributedVersionTicksAzure, value); }
		}

		/// <summary>The LastDistributedVersionTicksAmazon property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."LastDistributedVersionTicksAmazon".<br/>Table field type characteristics (type, precision, scale, length): BigInt, 19, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> LastDistributedVersionTicksAmazon
		{
			get { return (Nullable<System.Int64>)GetValue((int)MediaFieldIndex.LastDistributedVersionTicksAmazon, false); }
			set	{ SetValue((int)MediaFieldIndex.LastDistributedVersionTicksAmazon, value); }
		}

		/// <summary>The LastDistributedVersionTicks property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."LastDistributedVersionTicks".<br/>Table field type characteristics (type, precision, scale, length): BigInt, 19, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> LastDistributedVersionTicks
		{
			get { return (Nullable<System.Int64>)GetValue((int)MediaFieldIndex.LastDistributedVersionTicks, false); }
			set	{ SetValue((int)MediaFieldIndex.LastDistributedVersionTicks, value); }
		}

		/// <summary>The SizeWidth property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."SizeWidth".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> SizeWidth
		{
			get { return (Nullable<System.Int32>)GetValue((int)MediaFieldIndex.SizeWidth, false); }
			set	{ SetValue((int)MediaFieldIndex.SizeWidth, value); }
		}

		/// <summary>The SizeHeight property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."SizeHeight".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> SizeHeight
		{
			get { return (Nullable<System.Int32>)GetValue((int)MediaFieldIndex.SizeHeight, false); }
			set	{ SetValue((int)MediaFieldIndex.SizeHeight, value); }
		}

		/// <summary>Gets the EntityCollection with the related entities of type 'MediaEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(MediaEntity))]
		public virtual EntityCollection<MediaEntity> MediaCollection { get { return GetOrCreateEntityCollection<MediaEntity, MediaEntityFactory>("Media", true, false, ref _mediaCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'MediaRatioTypeMediaEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(MediaRatioTypeMediaEntity))]
		public virtual EntityCollection<MediaRatioTypeMediaEntity> MediaRatioTypeMediaCollection { get { return GetOrCreateEntityCollection<MediaRatioTypeMediaEntity, MediaRatioTypeMediaEntityFactory>("Media", true, false, ref _mediaRatioTypeMediaCollection); } }

		/// <summary>Gets / sets related entity of type 'AlterationEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual AlterationEntity Alteration
		{
			get { return _alteration; }
			set { SetSingleRelatedEntityNavigator(value, "Alteration"); }
		}

		/// <summary>Gets / sets related entity of type 'AlterationoptionEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual AlterationoptionEntity Alterationoption
		{
			get { return _alterationoption; }
			set { SetSingleRelatedEntityNavigator(value, "Alterationoption"); }
		}

		/// <summary>Gets / sets related entity of type 'ApplicationConfigurationEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual ApplicationConfigurationEntity ApplicationConfiguration
		{
			get { return _applicationConfiguration; }
			set { SetSingleRelatedEntityNavigator(value, "ApplicationConfiguration"); }
		}

		/// <summary>Gets / sets related entity of type 'CategoryEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual CategoryEntity Category
		{
			get { return _category; }
			set { SetSingleRelatedEntityNavigator(value, "Category"); }
		}

		/// <summary>Gets / sets related entity of type 'CategoryEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual CategoryEntity Category1
		{
			get { return _category1; }
			set { SetSingleRelatedEntityNavigator(value, "Category1"); }
		}

		/// <summary>Gets / sets related entity of type 'ClientConfigurationEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual ClientConfigurationEntity ClientConfiguration
		{
			get { return _clientConfiguration; }
			set { SetSingleRelatedEntityNavigator(value, "ClientConfiguration"); }
		}

		/// <summary>Gets / sets related entity of type 'CompanyEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual CompanyEntity Company
		{
			get { return _company; }
			set { SetSingleRelatedEntityNavigator(value, "Company"); }
		}

		/// <summary>Gets / sets related entity of type 'DeliverypointgroupEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual DeliverypointgroupEntity Deliverypointgroup
		{
			get { return _deliverypointgroup; }
			set { SetSingleRelatedEntityNavigator(value, "Deliverypointgroup"); }
		}

		/// <summary>Gets / sets related entity of type 'MediaEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual MediaEntity Media
		{
			get { return _media; }
			set { SetSingleRelatedEntityNavigator(value, "Media"); }
		}

		/// <summary>Gets / sets related entity of type 'ProductEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual ProductEntity Product
		{
			get { return _product; }
			set { SetSingleRelatedEntityNavigator(value, "Product"); }
		}

		/// <summary>Gets / sets related entity of type 'ProductEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual ProductEntity Product1
		{
			get { return _product1; }
			set { SetSingleRelatedEntityNavigator(value, "Product1"); }
		}

		/// <summary>Gets / sets related entity of type 'RoutestephandlerEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual RoutestephandlerEntity Routestephandler
		{
			get { return _routestephandler; }
			set { SetSingleRelatedEntityNavigator(value, "Routestephandler"); }
		}


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen
{
	public enum MediaFieldIndex
	{
		///<summary>MediaId. </summary>
		MediaId,
		///<summary>MediaType. </summary>
		MediaType,
		///<summary>SortOrder. </summary>
		SortOrder,
		///<summary>DefaultItem. </summary>
		DefaultItem,
		///<summary>Name. </summary>
		Name,
		///<summary>FilePathRelativeToMediaPath. </summary>
		FilePathRelativeToMediaPath,
		///<summary>Description. </summary>
		Description,
		///<summary>MimeType. </summary>
		MimeType,
		///<summary>Extension. </summary>
		Extension,
		///<summary>SizeKb. </summary>
		SizeKb,
		///<summary>RelatedCompanyId. </summary>
		RelatedCompanyId,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>ProductId. </summary>
		ProductId,
		///<summary>CategoryId. </summary>
		CategoryId,
		///<summary>AdvertisementId. </summary>
		AdvertisementId,
		///<summary>EntertainmentId. </summary>
		EntertainmentId,
		///<summary>AlterationoptionId. </summary>
		AlterationoptionId,
		///<summary>GenericproductId. </summary>
		GenericproductId,
		///<summary>GenericcategoryId. </summary>
		GenericcategoryId,
		///<summary>DeliverypointgroupId. </summary>
		DeliverypointgroupId,
		///<summary>PageElementId. </summary>
		PageElementId,
		///<summary>PageTemplateElementId. </summary>
		PageTemplateElementId,
		///<summary>SiteId. </summary>
		SiteId,
		///<summary>RoutestephandlerId. </summary>
		RoutestephandlerId,
		///<summary>SurveyId. </summary>
		SurveyId,
		///<summary>SurveyPageId. </summary>
		SurveyPageId,
		///<summary>ActionProductId. </summary>
		ActionProductId,
		///<summary>ActionCategoryId. </summary>
		ActionCategoryId,
		///<summary>ActionEntertainmentId. </summary>
		ActionEntertainmentId,
		///<summary>ActionEntertainmentcategoryId. </summary>
		ActionEntertainmentcategoryId,
		///<summary>ActionSiteId. </summary>
		ActionSiteId,
		///<summary>ActionPageId. </summary>
		ActionPageId,
		///<summary>ActionUrl. </summary>
		ActionUrl,
		///<summary>FromSupplier. </summary>
		FromSupplier,
		///<summary>AlterationId. </summary>
		AlterationId,
		///<summary>PointOfInterestId. </summary>
		PointOfInterestId,
		///<summary>PageId. </summary>
		PageId,
		///<summary>PageTemplateId. </summary>
		PageTemplateId,
		///<summary>AttachmentId. </summary>
		AttachmentId,
		///<summary>UIWidgetId. </summary>
		UIWidgetId,
		///<summary>UIThemeId. </summary>
		UIThemeId,
		///<summary>UIFooterItemId. </summary>
		UIFooterItemId,
		///<summary>RoomControlSectionId. </summary>
		RoomControlSectionId,
		///<summary>RoomControlSectionItemId. </summary>
		RoomControlSectionItemId,
		///<summary>StationId. </summary>
		StationId,
		///<summary>ClientConfigurationId. </summary>
		ClientConfigurationId,
		///<summary>ProductgroupId. </summary>
		ProductgroupId,
		///<summary>LandingPageId. </summary>
		LandingPageId,
		///<summary>CarouselItemId. </summary>
		CarouselItemId,
		///<summary>WidgetId. </summary>
		WidgetId,
		///<summary>ApplicationConfigurationId. </summary>
		ApplicationConfigurationId,
		///<summary>JpgQuality. </summary>
		JpgQuality,
		///<summary>SizeMode. </summary>
		SizeMode,
		///<summary>ZoomLevel. </summary>
		ZoomLevel,
		///<summary>MediaFileMd5. </summary>
		MediaFileMd5,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>AgnosticMediaId. </summary>
		AgnosticMediaId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>LastDistributedVersionTicksAzure. </summary>
		LastDistributedVersionTicksAzure,
		///<summary>LastDistributedVersionTicksAmazon. </summary>
		LastDistributedVersionTicksAmazon,
		///<summary>LastDistributedVersionTicks. </summary>
		LastDistributedVersionTicks,
		///<summary>SizeWidth. </summary>
		SizeWidth,
		///<summary>SizeHeight. </summary>
		SizeHeight,
		/// <summary></summary>
		AmountOfFields
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Media. </summary>
	public partial class MediaRelations: RelationFactory
	{
		#region Custom Relation code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomRelationCode
        private const ReferentialConstraintDeleteRule DeleteRuleForMediaEntityUsingAgnosticMediaId = ReferentialConstraintDeleteRule.SetNull;
        private const ReferentialConstraintDeleteRule DeleteRuleForMediaRatioTypeMediaEntityUsingMediaId = ReferentialConstraintDeleteRule.Cascade;
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		
		// private const ReferentialConstraintDeleteRule DeleteRuleForMediaEntityUsingAgnosticMediaId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForMediaRatioTypeMediaEntityUsingMediaId = ReferentialConstraintDeleteRule


		/// <summary>Gets all relations and delete rules of the MediaEntity as a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects.</summary>
		/// <returns>a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects</returns>
		public virtual Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> GetRelationsAndDeleteRules()
		{
			Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> toReturn = new Dictionary<IEntityRelation, ReferentialConstraintDeleteRule>();
			toReturn.Add(this.MediaEntityUsingAgnosticMediaId, MediaRelations.DeleteRuleForMediaEntityUsingAgnosticMediaId);
			toReturn.Add(this.MediaRatioTypeMediaEntityUsingMediaId, MediaRelations.DeleteRuleForMediaRatioTypeMediaEntityUsingMediaId);
			return toReturn;
		}		
	
	
		/// <summary>Returns a new IEntityRelation object, between MediaEntity and MediaEntity over the 1:n relation they have, using the relation between the fields: Media.MediaId - Media.AgnosticMediaId</summary>
		public virtual IEntityRelation MediaEntityUsingAgnosticMediaId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "MediaCollection", true, new[] { MediaFields.MediaId, MediaFields.AgnosticMediaId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between MediaEntity and MediaRatioTypeMediaEntity over the 1:n relation they have, using the relation between the fields: Media.MediaId - MediaRatioTypeMedia.MediaId</summary>
		public virtual IEntityRelation MediaRatioTypeMediaEntityUsingMediaId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "MediaRatioTypeMediaCollection", true, new[] { MediaFields.MediaId, MediaRatioTypeMediaFields.MediaId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between MediaEntity and AlterationEntity over the m:1 relation they have, using the relation between the fields: Media.AlterationId - Alteration.AlterationId</summary>
		public virtual IEntityRelation AlterationEntityUsingAlterationId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Alteration", false, new[] { AlterationFields.AlterationId, MediaFields.AlterationId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between MediaEntity and AlterationoptionEntity over the m:1 relation they have, using the relation between the fields: Media.AlterationoptionId - Alterationoption.AlterationoptionId</summary>
		public virtual IEntityRelation AlterationoptionEntityUsingAlterationoptionId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Alterationoption", false, new[] { AlterationoptionFields.AlterationoptionId, MediaFields.AlterationoptionId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between MediaEntity and ApplicationConfigurationEntity over the m:1 relation they have, using the relation between the fields: Media.ApplicationConfigurationId - ApplicationConfiguration.ApplicationConfigurationId</summary>
		public virtual IEntityRelation ApplicationConfigurationEntityUsingApplicationConfigurationId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "ApplicationConfiguration", false, new[] { ApplicationConfigurationFields.ApplicationConfigurationId, MediaFields.ApplicationConfigurationId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between MediaEntity and CategoryEntity over the m:1 relation they have, using the relation between the fields: Media.ActionCategoryId - Category.CategoryId</summary>
		public virtual IEntityRelation CategoryEntityUsingActionCategoryId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Category", false, new[] { CategoryFields.CategoryId, MediaFields.ActionCategoryId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between MediaEntity and CategoryEntity over the m:1 relation they have, using the relation between the fields: Media.CategoryId - Category.CategoryId</summary>
		public virtual IEntityRelation CategoryEntityUsingCategoryId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Category1", false, new[] { CategoryFields.CategoryId, MediaFields.CategoryId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between MediaEntity and ClientConfigurationEntity over the m:1 relation they have, using the relation between the fields: Media.ClientConfigurationId - ClientConfiguration.ClientConfigurationId</summary>
		public virtual IEntityRelation ClientConfigurationEntityUsingClientConfigurationId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "ClientConfiguration", false, new[] { ClientConfigurationFields.ClientConfigurationId, MediaFields.ClientConfigurationId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between MediaEntity and CompanyEntity over the m:1 relation they have, using the relation between the fields: Media.CompanyId - Company.CompanyId</summary>
		public virtual IEntityRelation CompanyEntityUsingCompanyId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Company", false, new[] { CompanyFields.CompanyId, MediaFields.CompanyId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between MediaEntity and DeliverypointgroupEntity over the m:1 relation they have, using the relation between the fields: Media.DeliverypointgroupId - Deliverypointgroup.DeliverypointgroupId</summary>
		public virtual IEntityRelation DeliverypointgroupEntityUsingDeliverypointgroupId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Deliverypointgroup", false, new[] { DeliverypointgroupFields.DeliverypointgroupId, MediaFields.DeliverypointgroupId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between MediaEntity and MediaEntity over the m:1 relation they have, using the relation between the fields: Media.AgnosticMediaId - Media.MediaId</summary>
		public virtual IEntityRelation MediaEntityUsingMediaIdAgnosticMediaId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Media", false, new[] { MediaFields.MediaId, MediaFields.AgnosticMediaId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between MediaEntity and ProductEntity over the m:1 relation they have, using the relation between the fields: Media.ActionProductId - Product.ProductId</summary>
		public virtual IEntityRelation ProductEntityUsingActionProductId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Product", false, new[] { ProductFields.ProductId, MediaFields.ActionProductId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between MediaEntity and ProductEntity over the m:1 relation they have, using the relation between the fields: Media.ProductId - Product.ProductId</summary>
		public virtual IEntityRelation ProductEntityUsingProductId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Product1", false, new[] { ProductFields.ProductId, MediaFields.ProductId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between MediaEntity and RoutestephandlerEntity over the m:1 relation they have, using the relation between the fields: Media.RoutestephandlerId - Routestephandler.RoutestephandlerId</summary>
		public virtual IEntityRelation RoutestephandlerEntityUsingRoutestephandlerId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Routestephandler", false, new[] { RoutestephandlerFields.RoutestephandlerId, MediaFields.RoutestephandlerId }); }
		}

	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticMediaRelations
	{
		internal static readonly IEntityRelation MediaEntityUsingAgnosticMediaIdStatic = new MediaRelations().MediaEntityUsingAgnosticMediaId;
		internal static readonly IEntityRelation MediaRatioTypeMediaEntityUsingMediaIdStatic = new MediaRelations().MediaRatioTypeMediaEntityUsingMediaId;
		internal static readonly IEntityRelation AlterationEntityUsingAlterationIdStatic = new MediaRelations().AlterationEntityUsingAlterationId;
		internal static readonly IEntityRelation AlterationoptionEntityUsingAlterationoptionIdStatic = new MediaRelations().AlterationoptionEntityUsingAlterationoptionId;
		internal static readonly IEntityRelation ApplicationConfigurationEntityUsingApplicationConfigurationIdStatic = new MediaRelations().ApplicationConfigurationEntityUsingApplicationConfigurationId;
		internal static readonly IEntityRelation CategoryEntityUsingActionCategoryIdStatic = new MediaRelations().CategoryEntityUsingActionCategoryId;
		internal static readonly IEntityRelation CategoryEntityUsingCategoryIdStatic = new MediaRelations().CategoryEntityUsingCategoryId;
		internal static readonly IEntityRelation ClientConfigurationEntityUsingClientConfigurationIdStatic = new MediaRelations().ClientConfigurationEntityUsingClientConfigurationId;
		internal static readonly IEntityRelation CompanyEntityUsingCompanyIdStatic = new MediaRelations().CompanyEntityUsingCompanyId;
		internal static readonly IEntityRelation DeliverypointgroupEntityUsingDeliverypointgroupIdStatic = new MediaRelations().DeliverypointgroupEntityUsingDeliverypointgroupId;
		internal static readonly IEntityRelation MediaEntityUsingMediaIdAgnosticMediaIdStatic = new MediaRelations().MediaEntityUsingMediaIdAgnosticMediaId;
		internal static readonly IEntityRelation ProductEntityUsingActionProductIdStatic = new MediaRelations().ProductEntityUsingActionProductId;
		internal static readonly IEntityRelation ProductEntityUsingProductIdStatic = new MediaRelations().ProductEntityUsingProductId;
		internal static readonly IEntityRelation RoutestephandlerEntityUsingRoutestephandlerIdStatic = new MediaRelations().RoutestephandlerEntityUsingRoutestephandlerId;

		/// <summary>CTor</summary>
		static StaticMediaRelations() { }
	}
}

