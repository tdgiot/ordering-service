﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 5.8
// Code is generated on: 
// Code is generated using templates: Crave.TemplateBindings
// Templates vendor: Crave Interactive
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.FactoryClasses;
using Crave.Ordering.Shared.Data.LLBLGen.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

// ReSharper disable All
namespace Crave.Ordering.Shared.Data.LLBLGen.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'PriceScheduleItem'.<br/><br/></summary>
	[Serializable]
	public partial class PriceScheduleItemEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END
	
	{
		private EntityCollection<PriceScheduleItemOccurrenceEntity> _priceScheduleItemOccurrenceCollection;
		private PriceLevelEntity _priceLevel;
		private PriceScheduleEntity _priceSchedule;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		private static PriceScheduleItemEntityStaticMetaData _staticMetaData = new PriceScheduleItemEntityStaticMetaData();
		private static PriceScheduleItemRelations _relationsFactory = new PriceScheduleItemRelations();		

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name PriceLevel</summary>
			public static readonly string PriceLevel = "PriceLevel";
			/// <summary>Member name PriceSchedule</summary>
			public static readonly string PriceSchedule = "PriceSchedule";
			/// <summary>Member name PriceScheduleItemOccurrenceCollection</summary>
			public static readonly string PriceScheduleItemOccurrenceCollection = "PriceScheduleItemOccurrenceCollection";
		}

		/// <summary>Static meta-data storage for navigator related information</summary>
		protected class PriceScheduleItemEntityStaticMetaData : EntityStaticMetaDataBase
		{
			public PriceScheduleItemEntityStaticMetaData()
			{
				SetEntityCoreInfo("PriceScheduleItemEntity", InheritanceHierarchyType.None, false, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.PriceScheduleItemEntity, typeof(PriceScheduleItemEntity), typeof(PriceScheduleItemEntityFactory), false);
				AddNavigatorMetaData<PriceScheduleItemEntity, EntityCollection<PriceScheduleItemOccurrenceEntity>>("PriceScheduleItemOccurrenceCollection", a => a._priceScheduleItemOccurrenceCollection, (a, b) => a._priceScheduleItemOccurrenceCollection = b, a => a.PriceScheduleItemOccurrenceCollection, () => new PriceScheduleItemRelations().PriceScheduleItemOccurrenceEntityUsingPriceScheduleItemId, typeof(PriceScheduleItemOccurrenceEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.PriceScheduleItemOccurrenceEntity);
				AddNavigatorMetaData<PriceScheduleItemEntity, PriceLevelEntity>("PriceLevel", "PriceScheduleItemCollection", (a, b) => a._priceLevel = b, a => a._priceLevel, (a, b) => a.PriceLevel = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticPriceScheduleItemRelations.PriceLevelEntityUsingPriceLevelIdStatic, ()=>new PriceScheduleItemRelations().PriceLevelEntityUsingPriceLevelId, null, new int[] { (int)PriceScheduleItemFieldIndex.PriceLevelId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.PriceLevelEntity);
				AddNavigatorMetaData<PriceScheduleItemEntity, PriceScheduleEntity>("PriceSchedule", "PriceScheduleItemCollection", (a, b) => a._priceSchedule = b, a => a._priceSchedule, (a, b) => a.PriceSchedule = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticPriceScheduleItemRelations.PriceScheduleEntityUsingPriceScheduleIdStatic, ()=>new PriceScheduleItemRelations().PriceScheduleEntityUsingPriceScheduleId, null, new int[] { (int)PriceScheduleItemFieldIndex.PriceScheduleId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.PriceScheduleEntity);
			}
		}

		/// <summary>Static ctor</summary>
		static PriceScheduleItemEntity()
		{
		}

		/// <summary> CTor</summary>
		public PriceScheduleItemEntity()
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public PriceScheduleItemEntity(IEntityFields2 fields)
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this PriceScheduleItemEntity</param>
		public PriceScheduleItemEntity(IValidator validator)
		{
			InitClassEmpty(validator, null);
		}

		/// <summary> CTor</summary>
		/// <param name="priceScheduleItemId">PK value for PriceScheduleItem which data should be fetched into this PriceScheduleItem object</param>
		public PriceScheduleItemEntity(System.Int32 priceScheduleItemId) : this(priceScheduleItemId, null)
		{
		}

		/// <summary> CTor</summary>
		/// <param name="priceScheduleItemId">PK value for PriceScheduleItem which data should be fetched into this PriceScheduleItem object</param>
		/// <param name="validator">The custom validator object for this PriceScheduleItemEntity</param>
		public PriceScheduleItemEntity(System.Int32 priceScheduleItemId, IValidator validator)
		{
			InitClassEmpty(validator, null);
			this.PriceScheduleItemId = priceScheduleItemId;
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected PriceScheduleItemEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'PriceScheduleItemOccurrence' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoPriceScheduleItemOccurrenceCollection() { return CreateRelationInfoForNavigator("PriceScheduleItemOccurrenceCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'PriceLevel' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoPriceLevel() { return CreateRelationInfoForNavigator("PriceLevel"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'PriceSchedule' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoPriceSchedule() { return CreateRelationInfoForNavigator("PriceSchedule"); }
		
		/// <inheritdoc/>
		protected override EntityStaticMetaDataBase GetEntityStaticMetaData() {	return _staticMetaData; }

		/// <summary>Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		/// <summary>Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this PriceScheduleItemEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary>The relations object holding all relations of this entity with other entity classes.</summary>
		public static PriceScheduleItemRelations Relations { get { return _relationsFactory; } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'PriceScheduleItemOccurrence' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathPriceScheduleItemOccurrenceCollection { get { return _staticMetaData.GetPrefetchPathElement("PriceScheduleItemOccurrenceCollection", CommonEntityBase.CreateEntityCollection<PriceScheduleItemOccurrenceEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'PriceLevel' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathPriceLevelEntity { get { return _staticMetaData.GetPrefetchPathElement("PriceLevel", CommonEntityBase.CreateEntityCollection<PriceLevelEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'PriceSchedule' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathPriceScheduleEntity { get { return _staticMetaData.GetPrefetchPathElement("PriceSchedule", CommonEntityBase.CreateEntityCollection<PriceScheduleEntity>()); } }

		/// <summary>The PriceScheduleItemId property of the Entity PriceScheduleItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceScheduleItem"."PriceScheduleItemId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 PriceScheduleItemId
		{
			get { return (System.Int32)GetValue((int)PriceScheduleItemFieldIndex.PriceScheduleItemId, true); }
			set { SetValue((int)PriceScheduleItemFieldIndex.PriceScheduleItemId, value); }		}

		/// <summary>The ParentCompanyId property of the Entity PriceScheduleItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceScheduleItem"."ParentCompanyId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ParentCompanyId
		{
			get { return (Nullable<System.Int32>)GetValue((int)PriceScheduleItemFieldIndex.ParentCompanyId, false); }
			set	{ SetValue((int)PriceScheduleItemFieldIndex.ParentCompanyId, value); }
		}

		/// <summary>The PriceScheduleId property of the Entity PriceScheduleItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceScheduleItem"."PriceScheduleId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 PriceScheduleId
		{
			get { return (System.Int32)GetValue((int)PriceScheduleItemFieldIndex.PriceScheduleId, true); }
			set	{ SetValue((int)PriceScheduleItemFieldIndex.PriceScheduleId, value); }
		}

		/// <summary>The PriceLevelId property of the Entity PriceScheduleItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceScheduleItem"."PriceLevelId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 PriceLevelId
		{
			get { return (System.Int32)GetValue((int)PriceScheduleItemFieldIndex.PriceLevelId, true); }
			set	{ SetValue((int)PriceScheduleItemFieldIndex.PriceLevelId, value); }
		}

		/// <summary>The CreatedUTC property of the Entity PriceScheduleItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceScheduleItem"."CreatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)PriceScheduleItemFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)PriceScheduleItemFieldIndex.CreatedUTC, value); }
		}

		/// <summary>The CreatedBy property of the Entity PriceScheduleItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceScheduleItem"."CreatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CreatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)PriceScheduleItemFieldIndex.CreatedBy, false); }
			set	{ SetValue((int)PriceScheduleItemFieldIndex.CreatedBy, value); }
		}

		/// <summary>The UpdatedUTC property of the Entity PriceScheduleItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceScheduleItem"."UpdatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)PriceScheduleItemFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)PriceScheduleItemFieldIndex.UpdatedUTC, value); }
		}

		/// <summary>The UpdatedBy property of the Entity PriceScheduleItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceScheduleItem"."UpdatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UpdatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)PriceScheduleItemFieldIndex.UpdatedBy, false); }
			set	{ SetValue((int)PriceScheduleItemFieldIndex.UpdatedBy, value); }
		}

		/// <summary>Gets the EntityCollection with the related entities of type 'PriceScheduleItemOccurrenceEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(PriceScheduleItemOccurrenceEntity))]
		public virtual EntityCollection<PriceScheduleItemOccurrenceEntity> PriceScheduleItemOccurrenceCollection { get { return GetOrCreateEntityCollection<PriceScheduleItemOccurrenceEntity, PriceScheduleItemOccurrenceEntityFactory>("PriceScheduleItem", true, false, ref _priceScheduleItemOccurrenceCollection); } }

		/// <summary>Gets / sets related entity of type 'PriceLevelEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual PriceLevelEntity PriceLevel
		{
			get { return _priceLevel; }
			set { SetSingleRelatedEntityNavigator(value, "PriceLevel"); }
		}

		/// <summary>Gets / sets related entity of type 'PriceScheduleEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual PriceScheduleEntity PriceSchedule
		{
			get { return _priceSchedule; }
			set { SetSingleRelatedEntityNavigator(value, "PriceSchedule"); }
		}


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen
{
	public enum PriceScheduleItemFieldIndex
	{
		///<summary>PriceScheduleItemId. </summary>
		PriceScheduleItemId,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>PriceScheduleId. </summary>
		PriceScheduleId,
		///<summary>PriceLevelId. </summary>
		PriceLevelId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		/// <summary></summary>
		AmountOfFields
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: PriceScheduleItem. </summary>
	public partial class PriceScheduleItemRelations: RelationFactory
	{
		#region Custom Relation code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomRelationCode
        private const ReferentialConstraintDeleteRule DeleteRuleForPriceScheduleItemOccurrenceEntityUsingPriceScheduleItemId = ReferentialConstraintDeleteRule.Cascade;
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		
		// private const ReferentialConstraintDeleteRule DeleteRuleForPriceScheduleItemOccurrenceEntityUsingPriceScheduleItemId = ReferentialConstraintDeleteRule


		/// <summary>Gets all relations and delete rules of the PriceScheduleItemEntity as a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects.</summary>
		/// <returns>a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects</returns>
		public virtual Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> GetRelationsAndDeleteRules()
		{
			Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> toReturn = new Dictionary<IEntityRelation, ReferentialConstraintDeleteRule>();
			toReturn.Add(this.PriceScheduleItemOccurrenceEntityUsingPriceScheduleItemId, PriceScheduleItemRelations.DeleteRuleForPriceScheduleItemOccurrenceEntityUsingPriceScheduleItemId);
			return toReturn;
		}		
	
	
		/// <summary>Returns a new IEntityRelation object, between PriceScheduleItemEntity and PriceScheduleItemOccurrenceEntity over the 1:n relation they have, using the relation between the fields: PriceScheduleItem.PriceScheduleItemId - PriceScheduleItemOccurrence.PriceScheduleItemId</summary>
		public virtual IEntityRelation PriceScheduleItemOccurrenceEntityUsingPriceScheduleItemId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "PriceScheduleItemOccurrenceCollection", true, new[] { PriceScheduleItemFields.PriceScheduleItemId, PriceScheduleItemOccurrenceFields.PriceScheduleItemId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between PriceScheduleItemEntity and PriceLevelEntity over the m:1 relation they have, using the relation between the fields: PriceScheduleItem.PriceLevelId - PriceLevel.PriceLevelId</summary>
		public virtual IEntityRelation PriceLevelEntityUsingPriceLevelId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "PriceLevel", false, new[] { PriceLevelFields.PriceLevelId, PriceScheduleItemFields.PriceLevelId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between PriceScheduleItemEntity and PriceScheduleEntity over the m:1 relation they have, using the relation between the fields: PriceScheduleItem.PriceScheduleId - PriceSchedule.PriceScheduleId</summary>
		public virtual IEntityRelation PriceScheduleEntityUsingPriceScheduleId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "PriceSchedule", false, new[] { PriceScheduleFields.PriceScheduleId, PriceScheduleItemFields.PriceScheduleId }); }
		}

	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticPriceScheduleItemRelations
	{
		internal static readonly IEntityRelation PriceScheduleItemOccurrenceEntityUsingPriceScheduleItemIdStatic = new PriceScheduleItemRelations().PriceScheduleItemOccurrenceEntityUsingPriceScheduleItemId;
		internal static readonly IEntityRelation PriceLevelEntityUsingPriceLevelIdStatic = new PriceScheduleItemRelations().PriceLevelEntityUsingPriceLevelId;
		internal static readonly IEntityRelation PriceScheduleEntityUsingPriceScheduleIdStatic = new PriceScheduleItemRelations().PriceScheduleEntityUsingPriceScheduleId;

		/// <summary>CTor</summary>
		static StaticPriceScheduleItemRelations() { }
	}
}

