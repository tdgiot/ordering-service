﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 5.8
// Code is generated on: 
// Code is generated using templates: Crave.TemplateBindings
// Templates vendor: Crave Interactive
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.FactoryClasses;
using Crave.Ordering.Shared.Data.LLBLGen.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

// ReSharper disable All
namespace Crave.Ordering.Shared.Data.LLBLGen.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'Alterationoption'.<br/><br/></summary>
	[Serializable]
	public partial class AlterationoptionEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END
	
	{
		private EntityCollection<AlterationitemEntity> _alterationitemCollection;
		private EntityCollection<AlterationoptionTagEntity> _alterationoptionTagCollection;
		private EntityCollection<MediaEntity> _mediaCollection;
		private EntityCollection<OrderitemAlterationitemTagEntity> _orderitemAlterationitemTagCollection;
		private EntityCollection<PriceLevelItemEntity> _priceLevelItemCollection;
		private CompanyEntity _company;
		private ExternalProductEntity _externalProduct;
		private ProductEntity _product;
		private TaxTariffEntity _taxTariff;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		private static AlterationoptionEntityStaticMetaData _staticMetaData = new AlterationoptionEntityStaticMetaData();
		private static AlterationoptionRelations _relationsFactory = new AlterationoptionRelations();		

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Company</summary>
			public static readonly string Company = "Company";
			/// <summary>Member name ExternalProduct</summary>
			public static readonly string ExternalProduct = "ExternalProduct";
			/// <summary>Member name Product</summary>
			public static readonly string Product = "Product";
			/// <summary>Member name TaxTariff</summary>
			public static readonly string TaxTariff = "TaxTariff";
			/// <summary>Member name AlterationitemCollection</summary>
			public static readonly string AlterationitemCollection = "AlterationitemCollection";
			/// <summary>Member name AlterationoptionTagCollection</summary>
			public static readonly string AlterationoptionTagCollection = "AlterationoptionTagCollection";
			/// <summary>Member name MediaCollection</summary>
			public static readonly string MediaCollection = "MediaCollection";
			/// <summary>Member name OrderitemAlterationitemTagCollection</summary>
			public static readonly string OrderitemAlterationitemTagCollection = "OrderitemAlterationitemTagCollection";
			/// <summary>Member name PriceLevelItemCollection</summary>
			public static readonly string PriceLevelItemCollection = "PriceLevelItemCollection";
		}

		/// <summary>Static meta-data storage for navigator related information</summary>
		protected class AlterationoptionEntityStaticMetaData : EntityStaticMetaDataBase
		{
			public AlterationoptionEntityStaticMetaData()
			{
				SetEntityCoreInfo("AlterationoptionEntity", InheritanceHierarchyType.None, false, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.AlterationoptionEntity, typeof(AlterationoptionEntity), typeof(AlterationoptionEntityFactory), false);
				AddNavigatorMetaData<AlterationoptionEntity, EntityCollection<AlterationitemEntity>>("AlterationitemCollection", a => a._alterationitemCollection, (a, b) => a._alterationitemCollection = b, a => a.AlterationitemCollection, () => new AlterationoptionRelations().AlterationitemEntityUsingAlterationoptionId, typeof(AlterationitemEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.AlterationitemEntity);
				AddNavigatorMetaData<AlterationoptionEntity, EntityCollection<AlterationoptionTagEntity>>("AlterationoptionTagCollection", a => a._alterationoptionTagCollection, (a, b) => a._alterationoptionTagCollection = b, a => a.AlterationoptionTagCollection, () => new AlterationoptionRelations().AlterationoptionTagEntityUsingAlterationoptionId, typeof(AlterationoptionTagEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.AlterationoptionTagEntity);
				AddNavigatorMetaData<AlterationoptionEntity, EntityCollection<MediaEntity>>("MediaCollection", a => a._mediaCollection, (a, b) => a._mediaCollection = b, a => a.MediaCollection, () => new AlterationoptionRelations().MediaEntityUsingAlterationoptionId, typeof(MediaEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.MediaEntity);
				AddNavigatorMetaData<AlterationoptionEntity, EntityCollection<OrderitemAlterationitemTagEntity>>("OrderitemAlterationitemTagCollection", a => a._orderitemAlterationitemTagCollection, (a, b) => a._orderitemAlterationitemTagCollection = b, a => a.OrderitemAlterationitemTagCollection, () => new AlterationoptionRelations().OrderitemAlterationitemTagEntityUsingAlterationoptionId, typeof(OrderitemAlterationitemTagEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.OrderitemAlterationitemTagEntity);
				AddNavigatorMetaData<AlterationoptionEntity, EntityCollection<PriceLevelItemEntity>>("PriceLevelItemCollection", a => a._priceLevelItemCollection, (a, b) => a._priceLevelItemCollection = b, a => a.PriceLevelItemCollection, () => new AlterationoptionRelations().PriceLevelItemEntityUsingAlterationoptionId, typeof(PriceLevelItemEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.PriceLevelItemEntity);
				AddNavigatorMetaData<AlterationoptionEntity, CompanyEntity>("Company", "AlterationoptionCollection", (a, b) => a._company = b, a => a._company, (a, b) => a.Company = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticAlterationoptionRelations.CompanyEntityUsingCompanyIdStatic, ()=>new AlterationoptionRelations().CompanyEntityUsingCompanyId, null, new int[] { (int)AlterationoptionFieldIndex.CompanyId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.CompanyEntity);
				AddNavigatorMetaData<AlterationoptionEntity, ExternalProductEntity>("ExternalProduct", "AlterationoptionCollection", (a, b) => a._externalProduct = b, a => a._externalProduct, (a, b) => a.ExternalProduct = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticAlterationoptionRelations.ExternalProductEntityUsingExternalProductIdStatic, ()=>new AlterationoptionRelations().ExternalProductEntityUsingExternalProductId, null, new int[] { (int)AlterationoptionFieldIndex.ExternalProductId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ExternalProductEntity);
				AddNavigatorMetaData<AlterationoptionEntity, ProductEntity>("Product", "AlterationoptionCollection", (a, b) => a._product = b, a => a._product, (a, b) => a.Product = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticAlterationoptionRelations.ProductEntityUsingProductIdStatic, ()=>new AlterationoptionRelations().ProductEntityUsingProductId, null, new int[] { (int)AlterationoptionFieldIndex.ProductId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ProductEntity);
				AddNavigatorMetaData<AlterationoptionEntity, TaxTariffEntity>("TaxTariff", "AlterationoptionCollection", (a, b) => a._taxTariff = b, a => a._taxTariff, (a, b) => a.TaxTariff = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticAlterationoptionRelations.TaxTariffEntityUsingTaxTariffIdStatic, ()=>new AlterationoptionRelations().TaxTariffEntityUsingTaxTariffId, null, new int[] { (int)AlterationoptionFieldIndex.TaxTariffId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.TaxTariffEntity);
			}
		}

		/// <summary>Static ctor</summary>
		static AlterationoptionEntity()
		{
		}

		/// <summary> CTor</summary>
		public AlterationoptionEntity()
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public AlterationoptionEntity(IEntityFields2 fields)
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this AlterationoptionEntity</param>
		public AlterationoptionEntity(IValidator validator)
		{
			InitClassEmpty(validator, null);
		}

		/// <summary> CTor</summary>
		/// <param name="alterationoptionId">PK value for Alterationoption which data should be fetched into this Alterationoption object</param>
		public AlterationoptionEntity(System.Int32 alterationoptionId) : this(alterationoptionId, null)
		{
		}

		/// <summary> CTor</summary>
		/// <param name="alterationoptionId">PK value for Alterationoption which data should be fetched into this Alterationoption object</param>
		/// <param name="validator">The custom validator object for this AlterationoptionEntity</param>
		public AlterationoptionEntity(System.Int32 alterationoptionId, IValidator validator)
		{
			InitClassEmpty(validator, null);
			this.AlterationoptionId = alterationoptionId;
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected AlterationoptionEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Alterationitem' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoAlterationitemCollection() { return CreateRelationInfoForNavigator("AlterationitemCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'AlterationoptionTag' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoAlterationoptionTagCollection() { return CreateRelationInfoForNavigator("AlterationoptionTagCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Media' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoMediaCollection() { return CreateRelationInfoForNavigator("MediaCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'OrderitemAlterationitemTag' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoOrderitemAlterationitemTagCollection() { return CreateRelationInfoForNavigator("OrderitemAlterationitemTagCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'PriceLevelItem' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoPriceLevelItemCollection() { return CreateRelationInfoForNavigator("PriceLevelItemCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Company' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoCompany() { return CreateRelationInfoForNavigator("Company"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'ExternalProduct' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoExternalProduct() { return CreateRelationInfoForNavigator("ExternalProduct"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Product' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoProduct() { return CreateRelationInfoForNavigator("Product"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'TaxTariff' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoTaxTariff() { return CreateRelationInfoForNavigator("TaxTariff"); }
		
		/// <inheritdoc/>
		protected override EntityStaticMetaDataBase GetEntityStaticMetaData() {	return _staticMetaData; }

		/// <summary>Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		/// <summary>Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this AlterationoptionEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary>The relations object holding all relations of this entity with other entity classes.</summary>
		public static AlterationoptionRelations Relations { get { return _relationsFactory; } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Alterationitem' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathAlterationitemCollection { get { return _staticMetaData.GetPrefetchPathElement("AlterationitemCollection", CommonEntityBase.CreateEntityCollection<AlterationitemEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'AlterationoptionTag' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathAlterationoptionTagCollection { get { return _staticMetaData.GetPrefetchPathElement("AlterationoptionTagCollection", CommonEntityBase.CreateEntityCollection<AlterationoptionTagEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Media' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathMediaCollection { get { return _staticMetaData.GetPrefetchPathElement("MediaCollection", CommonEntityBase.CreateEntityCollection<MediaEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'OrderitemAlterationitemTag' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathOrderitemAlterationitemTagCollection { get { return _staticMetaData.GetPrefetchPathElement("OrderitemAlterationitemTagCollection", CommonEntityBase.CreateEntityCollection<OrderitemAlterationitemTagEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'PriceLevelItem' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathPriceLevelItemCollection { get { return _staticMetaData.GetPrefetchPathElement("PriceLevelItemCollection", CommonEntityBase.CreateEntityCollection<PriceLevelItemEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Company' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathCompanyEntity { get { return _staticMetaData.GetPrefetchPathElement("Company", CommonEntityBase.CreateEntityCollection<CompanyEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'ExternalProduct' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathExternalProductEntity { get { return _staticMetaData.GetPrefetchPathElement("ExternalProduct", CommonEntityBase.CreateEntityCollection<ExternalProductEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Product' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathProductEntity { get { return _staticMetaData.GetPrefetchPathElement("Product", CommonEntityBase.CreateEntityCollection<ProductEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'TaxTariff' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathTaxTariffEntity { get { return _staticMetaData.GetPrefetchPathElement("TaxTariff", CommonEntityBase.CreateEntityCollection<TaxTariffEntity>()); } }

		/// <summary>The AlterationoptionId property of the Entity Alterationoption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alterationoption"."AlterationoptionId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 AlterationoptionId
		{
			get { return (System.Int32)GetValue((int)AlterationoptionFieldIndex.AlterationoptionId, true); }
			set { SetValue((int)AlterationoptionFieldIndex.AlterationoptionId, value); }		}

		/// <summary>The PosalterationoptionId property of the Entity Alterationoption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alterationoption"."PosalterationoptionId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> PosalterationoptionId
		{
			get { return (Nullable<System.Int32>)GetValue((int)AlterationoptionFieldIndex.PosalterationoptionId, false); }
			set	{ SetValue((int)AlterationoptionFieldIndex.PosalterationoptionId, value); }
		}

		/// <summary>The GenericalterationoptionId property of the Entity Alterationoption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alterationoption"."GenericalterationoptionId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> GenericalterationoptionId
		{
			get { return (Nullable<System.Int32>)GetValue((int)AlterationoptionFieldIndex.GenericalterationoptionId, false); }
			set	{ SetValue((int)AlterationoptionFieldIndex.GenericalterationoptionId, value); }
		}

		/// <summary>The Version property of the Entity Alterationoption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alterationoption"."Version".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 Version
		{
			get { return (System.Int32)GetValue((int)AlterationoptionFieldIndex.Version, true); }
			set	{ SetValue((int)AlterationoptionFieldIndex.Version, value); }
		}

		/// <summary>The Name property of the Entity Alterationoption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alterationoption"."Name".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)AlterationoptionFieldIndex.Name, true); }
			set	{ SetValue((int)AlterationoptionFieldIndex.Name, value); }
		}

		/// <summary>The FriendlyName property of the Entity Alterationoption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alterationoption"."FriendlyName".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FriendlyName
		{
			get { return (System.String)GetValue((int)AlterationoptionFieldIndex.FriendlyName, true); }
			set	{ SetValue((int)AlterationoptionFieldIndex.FriendlyName, value); }
		}

		/// <summary>The Type property of the Entity Alterationoption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alterationoption"."Type".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Crave.Enums.AlterationoptionType Type
		{
			get { return (Crave.Enums.AlterationoptionType)GetValue((int)AlterationoptionFieldIndex.Type, true); }
			set	{ SetValue((int)AlterationoptionFieldIndex.Type, value); }
		}

		/// <summary>The PriceIn property of the Entity Alterationoption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alterationoption"."PriceIn".<br/>Table field type characteristics (type, precision, scale, length): Money, 19, 4, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Decimal> PriceIn
		{
			get { return (Nullable<System.Decimal>)GetValue((int)AlterationoptionFieldIndex.PriceIn, false); }
			set	{ SetValue((int)AlterationoptionFieldIndex.PriceIn, value); }
		}

		/// <summary>The PriceAddition property of the Entity Alterationoption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alterationoption"."xPriceAddition".<br/>Table field type characteristics (type, precision, scale, length): Money, 19, 4, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Decimal> PriceAddition
		{
			get { return (Nullable<System.Decimal>)GetValue((int)AlterationoptionFieldIndex.PriceAddition, false); }
			set	{ SetValue((int)AlterationoptionFieldIndex.PriceAddition, value); }
		}

		/// <summary>The Description property of the Entity Alterationoption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alterationoption"."Description".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)AlterationoptionFieldIndex.Description, true); }
			set	{ SetValue((int)AlterationoptionFieldIndex.Description, value); }
		}

		/// <summary>The PosproductId property of the Entity Alterationoption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alterationoption"."PosproductId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> PosproductId
		{
			get { return (Nullable<System.Int32>)GetValue((int)AlterationoptionFieldIndex.PosproductId, false); }
			set	{ SetValue((int)AlterationoptionFieldIndex.PosproductId, value); }
		}

		/// <summary>The IsProductRelated property of the Entity Alterationoption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alterationoption"."IsProductRelated".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsProductRelated
		{
			get { return (System.Boolean)GetValue((int)AlterationoptionFieldIndex.IsProductRelated, true); }
			set	{ SetValue((int)AlterationoptionFieldIndex.IsProductRelated, value); }
		}

		/// <summary>The CompanyId property of the Entity Alterationoption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alterationoption"."CompanyId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CompanyId
		{
			get { return (Nullable<System.Int32>)GetValue((int)AlterationoptionFieldIndex.CompanyId, false); }
			set	{ SetValue((int)AlterationoptionFieldIndex.CompanyId, value); }
		}

		/// <summary>The StartTime property of the Entity Alterationoption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alterationoption"."StartTime".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> StartTime
		{
			get { return (Nullable<System.DateTime>)GetValue((int)AlterationoptionFieldIndex.StartTime, false); }
			set	{ SetValue((int)AlterationoptionFieldIndex.StartTime, value); }
		}

		/// <summary>The EndTime property of the Entity Alterationoption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alterationoption"."EndTime".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> EndTime
		{
			get { return (Nullable<System.DateTime>)GetValue((int)AlterationoptionFieldIndex.EndTime, false); }
			set	{ SetValue((int)AlterationoptionFieldIndex.EndTime, value); }
		}

		/// <summary>The MinLeadMinutes property of the Entity Alterationoption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alterationoption"."MinLeadMinutes".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 MinLeadMinutes
		{
			get { return (System.Int32)GetValue((int)AlterationoptionFieldIndex.MinLeadMinutes, true); }
			set	{ SetValue((int)AlterationoptionFieldIndex.MinLeadMinutes, value); }
		}

		/// <summary>The MaxLeadHours property of the Entity Alterationoption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alterationoption"."MaxLeadHours".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 MaxLeadHours
		{
			get { return (System.Int32)GetValue((int)AlterationoptionFieldIndex.MaxLeadHours, true); }
			set	{ SetValue((int)AlterationoptionFieldIndex.MaxLeadHours, value); }
		}

		/// <summary>The IntervalMinutes property of the Entity Alterationoption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alterationoption"."IntervalMinutes".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 IntervalMinutes
		{
			get { return (System.Int32)GetValue((int)AlterationoptionFieldIndex.IntervalMinutes, true); }
			set	{ SetValue((int)AlterationoptionFieldIndex.IntervalMinutes, value); }
		}

		/// <summary>The ShowDatePicker property of the Entity Alterationoption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alterationoption"."ShowDatePicker".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean ShowDatePicker
		{
			get { return (System.Boolean)GetValue((int)AlterationoptionFieldIndex.ShowDatePicker, true); }
			set	{ SetValue((int)AlterationoptionFieldIndex.ShowDatePicker, value); }
		}

		/// <summary>The CreatedBy property of the Entity Alterationoption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alterationoption"."CreatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)AlterationoptionFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)AlterationoptionFieldIndex.CreatedBy, value); }
		}

		/// <summary>The UpdatedBy property of the Entity Alterationoption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alterationoption"."UpdatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)AlterationoptionFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)AlterationoptionFieldIndex.UpdatedBy, value); }
		}

		/// <summary>The StartTimeUTC property of the Entity Alterationoption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alterationoption"."StartTimeUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> StartTimeUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)AlterationoptionFieldIndex.StartTimeUTC, false); }
			set	{ SetValue((int)AlterationoptionFieldIndex.StartTimeUTC, value); }
		}

		/// <summary>The EndTimeUTC property of the Entity Alterationoption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alterationoption"."EndTimeUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> EndTimeUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)AlterationoptionFieldIndex.EndTimeUTC, false); }
			set	{ SetValue((int)AlterationoptionFieldIndex.EndTimeUTC, value); }
		}

		/// <summary>The CreatedUTC property of the Entity Alterationoption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alterationoption"."CreatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)AlterationoptionFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)AlterationoptionFieldIndex.CreatedUTC, value); }
		}

		/// <summary>The UpdatedUTC property of the Entity Alterationoption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alterationoption"."UpdatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)AlterationoptionFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)AlterationoptionFieldIndex.UpdatedUTC, value); }
		}

		/// <summary>The TaxTariffId property of the Entity Alterationoption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alterationoption"."TaxTariffId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> TaxTariffId
		{
			get { return (Nullable<System.Int32>)GetValue((int)AlterationoptionFieldIndex.TaxTariffId, false); }
			set	{ SetValue((int)AlterationoptionFieldIndex.TaxTariffId, value); }
		}

		/// <summary>The IsAlcoholic property of the Entity Alterationoption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alterationoption"."IsAlcoholic".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsAlcoholic
		{
			get { return (System.Boolean)GetValue((int)AlterationoptionFieldIndex.IsAlcoholic, true); }
			set	{ SetValue((int)AlterationoptionFieldIndex.IsAlcoholic, value); }
		}

		/// <summary>The InheritName property of the Entity Alterationoption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alterationoption"."InheritName".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean InheritName
		{
			get { return (System.Boolean)GetValue((int)AlterationoptionFieldIndex.InheritName, true); }
			set	{ SetValue((int)AlterationoptionFieldIndex.InheritName, value); }
		}

		/// <summary>The InheritPrice property of the Entity Alterationoption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alterationoption"."InheritPrice".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean InheritPrice
		{
			get { return (System.Boolean)GetValue((int)AlterationoptionFieldIndex.InheritPrice, true); }
			set	{ SetValue((int)AlterationoptionFieldIndex.InheritPrice, value); }
		}

		/// <summary>The InheritTaxTariff property of the Entity Alterationoption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alterationoption"."InheritTaxTariff".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean InheritTaxTariff
		{
			get { return (System.Boolean)GetValue((int)AlterationoptionFieldIndex.InheritTaxTariff, true); }
			set	{ SetValue((int)AlterationoptionFieldIndex.InheritTaxTariff, value); }
		}

		/// <summary>The InheritIsAlcoholic property of the Entity Alterationoption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alterationoption"."InheritIsAlcoholic".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean InheritIsAlcoholic
		{
			get { return (System.Boolean)GetValue((int)AlterationoptionFieldIndex.InheritIsAlcoholic, true); }
			set	{ SetValue((int)AlterationoptionFieldIndex.InheritIsAlcoholic, value); }
		}

		/// <summary>The InheritTags property of the Entity Alterationoption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alterationoption"."InheritTags".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean InheritTags
		{
			get { return (System.Boolean)GetValue((int)AlterationoptionFieldIndex.InheritTags, true); }
			set	{ SetValue((int)AlterationoptionFieldIndex.InheritTags, value); }
		}

		/// <summary>The InheritCustomText property of the Entity Alterationoption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alterationoption"."InheritCustomText".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean InheritCustomText
		{
			get { return (System.Boolean)GetValue((int)AlterationoptionFieldIndex.InheritCustomText, true); }
			set	{ SetValue((int)AlterationoptionFieldIndex.InheritCustomText, value); }
		}

		/// <summary>The ProductId property of the Entity Alterationoption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alterationoption"."ProductId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ProductId
		{
			get { return (Nullable<System.Int32>)GetValue((int)AlterationoptionFieldIndex.ProductId, false); }
			set	{ SetValue((int)AlterationoptionFieldIndex.ProductId, value); }
		}

		/// <summary>The ExternalProductId property of the Entity Alterationoption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alterationoption"."ExternalProductId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ExternalProductId
		{
			get { return (Nullable<System.Int32>)GetValue((int)AlterationoptionFieldIndex.ExternalProductId, false); }
			set	{ SetValue((int)AlterationoptionFieldIndex.ExternalProductId, value); }
		}

		/// <summary>The IsAvailable property of the Entity Alterationoption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alterationoption"."IsAvailable".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsAvailable
		{
			get { return (System.Boolean)GetValue((int)AlterationoptionFieldIndex.IsAvailable, true); }
			set	{ SetValue((int)AlterationoptionFieldIndex.IsAvailable, value); }
		}

		/// <summary>The ExternalIdentifier property of the Entity Alterationoption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alterationoption"."ExternalIdentifier".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ExternalIdentifier
		{
			get { return (System.String)GetValue((int)AlterationoptionFieldIndex.ExternalIdentifier, true); }
			set	{ SetValue((int)AlterationoptionFieldIndex.ExternalIdentifier, value); }
		}

		/// <summary>Gets the EntityCollection with the related entities of type 'AlterationitemEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(AlterationitemEntity))]
		public virtual EntityCollection<AlterationitemEntity> AlterationitemCollection { get { return GetOrCreateEntityCollection<AlterationitemEntity, AlterationitemEntityFactory>("Alterationoption", true, false, ref _alterationitemCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'AlterationoptionTagEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(AlterationoptionTagEntity))]
		public virtual EntityCollection<AlterationoptionTagEntity> AlterationoptionTagCollection { get { return GetOrCreateEntityCollection<AlterationoptionTagEntity, AlterationoptionTagEntityFactory>("Alterationoption", true, false, ref _alterationoptionTagCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'MediaEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(MediaEntity))]
		public virtual EntityCollection<MediaEntity> MediaCollection { get { return GetOrCreateEntityCollection<MediaEntity, MediaEntityFactory>("Alterationoption", true, false, ref _mediaCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'OrderitemAlterationitemTagEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(OrderitemAlterationitemTagEntity))]
		public virtual EntityCollection<OrderitemAlterationitemTagEntity> OrderitemAlterationitemTagCollection { get { return GetOrCreateEntityCollection<OrderitemAlterationitemTagEntity, OrderitemAlterationitemTagEntityFactory>("Alterationoption", true, false, ref _orderitemAlterationitemTagCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'PriceLevelItemEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(PriceLevelItemEntity))]
		public virtual EntityCollection<PriceLevelItemEntity> PriceLevelItemCollection { get { return GetOrCreateEntityCollection<PriceLevelItemEntity, PriceLevelItemEntityFactory>("Alterationoption", true, false, ref _priceLevelItemCollection); } }

		/// <summary>Gets / sets related entity of type 'CompanyEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual CompanyEntity Company
		{
			get { return _company; }
			set { SetSingleRelatedEntityNavigator(value, "Company"); }
		}

		/// <summary>Gets / sets related entity of type 'ExternalProductEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual ExternalProductEntity ExternalProduct
		{
			get { return _externalProduct; }
			set { SetSingleRelatedEntityNavigator(value, "ExternalProduct"); }
		}

		/// <summary>Gets / sets related entity of type 'ProductEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual ProductEntity Product
		{
			get { return _product; }
			set { SetSingleRelatedEntityNavigator(value, "Product"); }
		}

		/// <summary>Gets / sets related entity of type 'TaxTariffEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual TaxTariffEntity TaxTariff
		{
			get { return _taxTariff; }
			set { SetSingleRelatedEntityNavigator(value, "TaxTariff"); }
		}


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen
{
	public enum AlterationoptionFieldIndex
	{
		///<summary>AlterationoptionId. </summary>
		AlterationoptionId,
		///<summary>PosalterationoptionId. </summary>
		PosalterationoptionId,
		///<summary>GenericalterationoptionId. </summary>
		GenericalterationoptionId,
		///<summary>Version. </summary>
		Version,
		///<summary>Name. </summary>
		Name,
		///<summary>FriendlyName. </summary>
		FriendlyName,
		///<summary>Type. </summary>
		Type,
		///<summary>PriceIn. </summary>
		PriceIn,
		///<summary>PriceAddition. </summary>
		PriceAddition,
		///<summary>Description. </summary>
		Description,
		///<summary>PosproductId. </summary>
		PosproductId,
		///<summary>IsProductRelated. </summary>
		IsProductRelated,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>StartTime. </summary>
		StartTime,
		///<summary>EndTime. </summary>
		EndTime,
		///<summary>MinLeadMinutes. </summary>
		MinLeadMinutes,
		///<summary>MaxLeadHours. </summary>
		MaxLeadHours,
		///<summary>IntervalMinutes. </summary>
		IntervalMinutes,
		///<summary>ShowDatePicker. </summary>
		ShowDatePicker,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>StartTimeUTC. </summary>
		StartTimeUTC,
		///<summary>EndTimeUTC. </summary>
		EndTimeUTC,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>TaxTariffId. </summary>
		TaxTariffId,
		///<summary>IsAlcoholic. </summary>
		IsAlcoholic,
		///<summary>InheritName. </summary>
		InheritName,
		///<summary>InheritPrice. </summary>
		InheritPrice,
		///<summary>InheritTaxTariff. </summary>
		InheritTaxTariff,
		///<summary>InheritIsAlcoholic. </summary>
		InheritIsAlcoholic,
		///<summary>InheritTags. </summary>
		InheritTags,
		///<summary>InheritCustomText. </summary>
		InheritCustomText,
		///<summary>ProductId. </summary>
		ProductId,
		///<summary>ExternalProductId. </summary>
		ExternalProductId,
		///<summary>IsAvailable. </summary>
		IsAvailable,
		///<summary>ExternalIdentifier. </summary>
		ExternalIdentifier,
		/// <summary></summary>
		AmountOfFields
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Alterationoption. </summary>
	public partial class AlterationoptionRelations: RelationFactory
	{
		#region Custom Relation code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomRelationCode
        private const ReferentialConstraintDeleteRule DeleteRuleForAlterationitemEntityUsingAlterationoptionId = ReferentialConstraintDeleteRule.Cascade;
        private const ReferentialConstraintDeleteRule DeleteRuleForAlterationoptionTagEntityUsingAlterationoptionId = ReferentialConstraintDeleteRule.SetNull;
		private const ReferentialConstraintDeleteRule DeleteRuleForMediaEntityUsingAlterationoptionId = ReferentialConstraintDeleteRule.Cascade;
        private const ReferentialConstraintDeleteRule DeleteRuleForOrderitemAlterationitemTagEntityUsingAlterationoptionId = ReferentialConstraintDeleteRule.SetNull;
		private const ReferentialConstraintDeleteRule DeleteRuleForPriceLevelItemEntityUsingAlterationoptionId = ReferentialConstraintDeleteRule.Cascade;
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		
		// private const ReferentialConstraintDeleteRule DeleteRuleForAlterationitemEntityUsingAlterationoptionId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForAlterationoptionTagEntityUsingAlterationoptionId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForMediaEntityUsingAlterationoptionId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForOrderitemAlterationitemTagEntityUsingAlterationoptionId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForPriceLevelItemEntityUsingAlterationoptionId = ReferentialConstraintDeleteRule


		/// <summary>Gets all relations and delete rules of the AlterationoptionEntity as a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects.</summary>
		/// <returns>a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects</returns>
		public virtual Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> GetRelationsAndDeleteRules()
		{
			Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> toReturn = new Dictionary<IEntityRelation, ReferentialConstraintDeleteRule>();
			toReturn.Add(this.AlterationitemEntityUsingAlterationoptionId, AlterationoptionRelations.DeleteRuleForAlterationitemEntityUsingAlterationoptionId);
			toReturn.Add(this.AlterationoptionTagEntityUsingAlterationoptionId, AlterationoptionRelations.DeleteRuleForAlterationoptionTagEntityUsingAlterationoptionId);
			toReturn.Add(this.MediaEntityUsingAlterationoptionId, AlterationoptionRelations.DeleteRuleForMediaEntityUsingAlterationoptionId);
			toReturn.Add(this.OrderitemAlterationitemTagEntityUsingAlterationoptionId, AlterationoptionRelations.DeleteRuleForOrderitemAlterationitemTagEntityUsingAlterationoptionId);
			toReturn.Add(this.PriceLevelItemEntityUsingAlterationoptionId, AlterationoptionRelations.DeleteRuleForPriceLevelItemEntityUsingAlterationoptionId);
			return toReturn;
		}		
	
	
		/// <summary>Returns a new IEntityRelation object, between AlterationoptionEntity and AlterationitemEntity over the 1:n relation they have, using the relation between the fields: Alterationoption.AlterationoptionId - Alterationitem.AlterationoptionId</summary>
		public virtual IEntityRelation AlterationitemEntityUsingAlterationoptionId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "AlterationitemCollection", true, new[] { AlterationoptionFields.AlterationoptionId, AlterationitemFields.AlterationoptionId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between AlterationoptionEntity and AlterationoptionTagEntity over the 1:n relation they have, using the relation between the fields: Alterationoption.AlterationoptionId - AlterationoptionTag.AlterationoptionId</summary>
		public virtual IEntityRelation AlterationoptionTagEntityUsingAlterationoptionId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "AlterationoptionTagCollection", true, new[] { AlterationoptionFields.AlterationoptionId, AlterationoptionTagFields.AlterationoptionId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between AlterationoptionEntity and MediaEntity over the 1:n relation they have, using the relation between the fields: Alterationoption.AlterationoptionId - Media.AlterationoptionId</summary>
		public virtual IEntityRelation MediaEntityUsingAlterationoptionId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "MediaCollection", true, new[] { AlterationoptionFields.AlterationoptionId, MediaFields.AlterationoptionId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between AlterationoptionEntity and OrderitemAlterationitemTagEntity over the 1:n relation they have, using the relation between the fields: Alterationoption.AlterationoptionId - OrderitemAlterationitemTag.AlterationoptionId</summary>
		public virtual IEntityRelation OrderitemAlterationitemTagEntityUsingAlterationoptionId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "OrderitemAlterationitemTagCollection", true, new[] { AlterationoptionFields.AlterationoptionId, OrderitemAlterationitemTagFields.AlterationoptionId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between AlterationoptionEntity and PriceLevelItemEntity over the 1:n relation they have, using the relation between the fields: Alterationoption.AlterationoptionId - PriceLevelItem.AlterationoptionId</summary>
		public virtual IEntityRelation PriceLevelItemEntityUsingAlterationoptionId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "PriceLevelItemCollection", true, new[] { AlterationoptionFields.AlterationoptionId, PriceLevelItemFields.AlterationoptionId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between AlterationoptionEntity and CompanyEntity over the m:1 relation they have, using the relation between the fields: Alterationoption.CompanyId - Company.CompanyId</summary>
		public virtual IEntityRelation CompanyEntityUsingCompanyId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Company", false, new[] { CompanyFields.CompanyId, AlterationoptionFields.CompanyId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between AlterationoptionEntity and ExternalProductEntity over the m:1 relation they have, using the relation between the fields: Alterationoption.ExternalProductId - ExternalProduct.ExternalProductId</summary>
		public virtual IEntityRelation ExternalProductEntityUsingExternalProductId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "ExternalProduct", false, new[] { ExternalProductFields.ExternalProductId, AlterationoptionFields.ExternalProductId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between AlterationoptionEntity and ProductEntity over the m:1 relation they have, using the relation between the fields: Alterationoption.ProductId - Product.ProductId</summary>
		public virtual IEntityRelation ProductEntityUsingProductId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Product", false, new[] { ProductFields.ProductId, AlterationoptionFields.ProductId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between AlterationoptionEntity and TaxTariffEntity over the m:1 relation they have, using the relation between the fields: Alterationoption.TaxTariffId - TaxTariff.TaxTariffId</summary>
		public virtual IEntityRelation TaxTariffEntityUsingTaxTariffId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "TaxTariff", false, new[] { TaxTariffFields.TaxTariffId, AlterationoptionFields.TaxTariffId }); }
		}

	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticAlterationoptionRelations
	{
		internal static readonly IEntityRelation AlterationitemEntityUsingAlterationoptionIdStatic = new AlterationoptionRelations().AlterationitemEntityUsingAlterationoptionId;
		internal static readonly IEntityRelation AlterationoptionTagEntityUsingAlterationoptionIdStatic = new AlterationoptionRelations().AlterationoptionTagEntityUsingAlterationoptionId;
		internal static readonly IEntityRelation MediaEntityUsingAlterationoptionIdStatic = new AlterationoptionRelations().MediaEntityUsingAlterationoptionId;
		internal static readonly IEntityRelation OrderitemAlterationitemTagEntityUsingAlterationoptionIdStatic = new AlterationoptionRelations().OrderitemAlterationitemTagEntityUsingAlterationoptionId;
		internal static readonly IEntityRelation PriceLevelItemEntityUsingAlterationoptionIdStatic = new AlterationoptionRelations().PriceLevelItemEntityUsingAlterationoptionId;
		internal static readonly IEntityRelation CompanyEntityUsingCompanyIdStatic = new AlterationoptionRelations().CompanyEntityUsingCompanyId;
		internal static readonly IEntityRelation ExternalProductEntityUsingExternalProductIdStatic = new AlterationoptionRelations().ExternalProductEntityUsingExternalProductId;
		internal static readonly IEntityRelation ProductEntityUsingProductIdStatic = new AlterationoptionRelations().ProductEntityUsingProductId;
		internal static readonly IEntityRelation TaxTariffEntityUsingTaxTariffIdStatic = new AlterationoptionRelations().TaxTariffEntityUsingTaxTariffId;

		/// <summary>CTor</summary>
		static StaticAlterationoptionRelations() { }
	}
}

