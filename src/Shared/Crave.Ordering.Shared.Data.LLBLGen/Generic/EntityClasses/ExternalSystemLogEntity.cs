﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 5.8
// Code is generated on: 
// Code is generated using templates: Crave.TemplateBindings
// Templates vendor: Crave Interactive
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.FactoryClasses;
using Crave.Ordering.Shared.Data.LLBLGen.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

// ReSharper disable All
namespace Crave.Ordering.Shared.Data.LLBLGen.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'ExternalSystemLog'.<br/><br/></summary>
	[Serializable]
	public partial class ExternalSystemLogEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END
	
	{
		private ExternalSystemEntity _externalSystem;
		private OrderEntity _order;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		private static ExternalSystemLogEntityStaticMetaData _staticMetaData = new ExternalSystemLogEntityStaticMetaData();
		private static ExternalSystemLogRelations _relationsFactory = new ExternalSystemLogRelations();		

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name ExternalSystem</summary>
			public static readonly string ExternalSystem = "ExternalSystem";
			/// <summary>Member name Order</summary>
			public static readonly string Order = "Order";
		}

		/// <summary>Static meta-data storage for navigator related information</summary>
		protected class ExternalSystemLogEntityStaticMetaData : EntityStaticMetaDataBase
		{
			public ExternalSystemLogEntityStaticMetaData()
			{
				SetEntityCoreInfo("ExternalSystemLogEntity", InheritanceHierarchyType.None, false, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ExternalSystemLogEntity, typeof(ExternalSystemLogEntity), typeof(ExternalSystemLogEntityFactory), false);
				AddNavigatorMetaData<ExternalSystemLogEntity, ExternalSystemEntity>("ExternalSystem", "ExternalSystemLogCollection", (a, b) => a._externalSystem = b, a => a._externalSystem, (a, b) => a.ExternalSystem = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticExternalSystemLogRelations.ExternalSystemEntityUsingExternalSystemIdStatic, ()=>new ExternalSystemLogRelations().ExternalSystemEntityUsingExternalSystemId, null, new int[] { (int)ExternalSystemLogFieldIndex.ExternalSystemId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ExternalSystemEntity);
				AddNavigatorMetaData<ExternalSystemLogEntity, OrderEntity>("Order", "ExternalSystemLogCollection", (a, b) => a._order = b, a => a._order, (a, b) => a.Order = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticExternalSystemLogRelations.OrderEntityUsingOrderIdStatic, ()=>new ExternalSystemLogRelations().OrderEntityUsingOrderId, null, new int[] { (int)ExternalSystemLogFieldIndex.OrderId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.OrderEntity);
			}
		}

		/// <summary>Static ctor</summary>
		static ExternalSystemLogEntity()
		{
		}

		/// <summary> CTor</summary>
		public ExternalSystemLogEntity()
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public ExternalSystemLogEntity(IEntityFields2 fields)
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this ExternalSystemLogEntity</param>
		public ExternalSystemLogEntity(IValidator validator)
		{
			InitClassEmpty(validator, null);
		}

		/// <summary> CTor</summary>
		/// <param name="externalSystemLogId">PK value for ExternalSystemLog which data should be fetched into this ExternalSystemLog object</param>
		public ExternalSystemLogEntity(System.Int32 externalSystemLogId) : this(externalSystemLogId, null)
		{
		}

		/// <summary> CTor</summary>
		/// <param name="externalSystemLogId">PK value for ExternalSystemLog which data should be fetched into this ExternalSystemLog object</param>
		/// <param name="validator">The custom validator object for this ExternalSystemLogEntity</param>
		public ExternalSystemLogEntity(System.Int32 externalSystemLogId, IValidator validator)
		{
			InitClassEmpty(validator, null);
			this.ExternalSystemLogId = externalSystemLogId;
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ExternalSystemLogEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'ExternalSystem' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoExternalSystem() { return CreateRelationInfoForNavigator("ExternalSystem"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Order' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoOrder() { return CreateRelationInfoForNavigator("Order"); }
		
		/// <inheritdoc/>
		protected override EntityStaticMetaDataBase GetEntityStaticMetaData() {	return _staticMetaData; }

		/// <summary>Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		/// <summary>Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this ExternalSystemLogEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary>The relations object holding all relations of this entity with other entity classes.</summary>
		public static ExternalSystemLogRelations Relations { get { return _relationsFactory; } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'ExternalSystem' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathExternalSystemEntity { get { return _staticMetaData.GetPrefetchPathElement("ExternalSystem", CommonEntityBase.CreateEntityCollection<ExternalSystemEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Order' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathOrderEntity { get { return _staticMetaData.GetPrefetchPathElement("Order", CommonEntityBase.CreateEntityCollection<OrderEntity>()); } }

		/// <summary>The ExternalSystemLogId property of the Entity ExternalSystemLog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalSystemLog"."ExternalSystemLogId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 ExternalSystemLogId
		{
			get { return (System.Int32)GetValue((int)ExternalSystemLogFieldIndex.ExternalSystemLogId, true); }
			set { SetValue((int)ExternalSystemLogFieldIndex.ExternalSystemLogId, value); }		}

		/// <summary>The ExternalSystemId property of the Entity ExternalSystemLog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalSystemLog"."ExternalSystemId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ExternalSystemId
		{
			get { return (System.Int32)GetValue((int)ExternalSystemLogFieldIndex.ExternalSystemId, true); }
			set	{ SetValue((int)ExternalSystemLogFieldIndex.ExternalSystemId, value); }
		}

		/// <summary>The Message property of the Entity ExternalSystemLog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalSystemLog"."Message".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 1024.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Message
		{
			get { return (System.String)GetValue((int)ExternalSystemLogFieldIndex.Message, true); }
			set	{ SetValue((int)ExternalSystemLogFieldIndex.Message, value); }
		}

		/// <summary>The LogType property of the Entity ExternalSystemLog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalSystemLog"."LogType".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Crave.Enums.ExternalSystemLogType LogType
		{
			get { return (Crave.Enums.ExternalSystemLogType)GetValue((int)ExternalSystemLogFieldIndex.LogType, true); }
			set	{ SetValue((int)ExternalSystemLogFieldIndex.LogType, value); }
		}

		/// <summary>The Success property of the Entity ExternalSystemLog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalSystemLog"."Success".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean Success
		{
			get { return (System.Boolean)GetValue((int)ExternalSystemLogFieldIndex.Success, true); }
			set	{ SetValue((int)ExternalSystemLogFieldIndex.Success, value); }
		}

		/// <summary>The Raw property of the Entity ExternalSystemLog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalSystemLog"."Raw".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Raw
		{
			get { return (System.String)GetValue((int)ExternalSystemLogFieldIndex.Raw, true); }
			set	{ SetValue((int)ExternalSystemLogFieldIndex.Raw, value); }
		}

		/// <summary>The CreatedUTC property of the Entity ExternalSystemLog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalSystemLog"."CreatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime CreatedUTC
		{
			get { return (System.DateTime)GetValue((int)ExternalSystemLogFieldIndex.CreatedUTC, true); }
			set	{ SetValue((int)ExternalSystemLogFieldIndex.CreatedUTC, value); }
		}

		/// <summary>The UpdatedUTC property of the Entity ExternalSystemLog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalSystemLog"."UpdatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ExternalSystemLogFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)ExternalSystemLogFieldIndex.UpdatedUTC, value); }
		}

		/// <summary>The OrderId property of the Entity ExternalSystemLog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalSystemLog"."OrderId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> OrderId
		{
			get { return (Nullable<System.Int32>)GetValue((int)ExternalSystemLogFieldIndex.OrderId, false); }
			set	{ SetValue((int)ExternalSystemLogFieldIndex.OrderId, value); }
		}

		/// <summary>The Log property of the Entity ExternalSystemLog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalSystemLog"."Log".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Log
		{
			get { return (System.String)GetValue((int)ExternalSystemLogFieldIndex.Log, true); }
			set	{ SetValue((int)ExternalSystemLogFieldIndex.Log, value); }
		}

		/// <summary>Gets / sets related entity of type 'ExternalSystemEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual ExternalSystemEntity ExternalSystem
		{
			get { return _externalSystem; }
			set { SetSingleRelatedEntityNavigator(value, "ExternalSystem"); }
		}

		/// <summary>Gets / sets related entity of type 'OrderEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual OrderEntity Order
		{
			get { return _order; }
			set { SetSingleRelatedEntityNavigator(value, "Order"); }
		}


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen
{
	public enum ExternalSystemLogFieldIndex
	{
		///<summary>ExternalSystemLogId. </summary>
		ExternalSystemLogId,
		///<summary>ExternalSystemId. </summary>
		ExternalSystemId,
		///<summary>Message. </summary>
		Message,
		///<summary>LogType. </summary>
		LogType,
		///<summary>Success. </summary>
		Success,
		///<summary>Raw. </summary>
		Raw,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>OrderId. </summary>
		OrderId,
		///<summary>Log. </summary>
		Log,
		/// <summary></summary>
		AmountOfFields
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: ExternalSystemLog. </summary>
	public partial class ExternalSystemLogRelations: RelationFactory
	{
		#region Custom Relation code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomRelationCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		


		/// <summary>Gets all relations and delete rules of the ExternalSystemLogEntity as a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects.</summary>
		/// <returns>a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects</returns>
		public virtual Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> GetRelationsAndDeleteRules()
		{
			Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> toReturn = new Dictionary<IEntityRelation, ReferentialConstraintDeleteRule>();
			return toReturn;
		}		
	
	

		/// <summary>Returns a new IEntityRelation object, between ExternalSystemLogEntity and ExternalSystemEntity over the m:1 relation they have, using the relation between the fields: ExternalSystemLog.ExternalSystemId - ExternalSystem.ExternalSystemId</summary>
		public virtual IEntityRelation ExternalSystemEntityUsingExternalSystemId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "ExternalSystem", false, new[] { ExternalSystemFields.ExternalSystemId, ExternalSystemLogFields.ExternalSystemId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between ExternalSystemLogEntity and OrderEntity over the m:1 relation they have, using the relation between the fields: ExternalSystemLog.OrderId - Order.OrderId</summary>
		public virtual IEntityRelation OrderEntityUsingOrderId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Order", false, new[] { OrderFields.OrderId, ExternalSystemLogFields.OrderId }); }
		}

	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticExternalSystemLogRelations
	{
		internal static readonly IEntityRelation ExternalSystemEntityUsingExternalSystemIdStatic = new ExternalSystemLogRelations().ExternalSystemEntityUsingExternalSystemId;
		internal static readonly IEntityRelation OrderEntityUsingOrderIdStatic = new ExternalSystemLogRelations().OrderEntityUsingOrderId;

		/// <summary>CTor</summary>
		static StaticExternalSystemLogRelations() { }
	}
}

