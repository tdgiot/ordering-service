﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 5.8
// Code is generated on: 
// Code is generated using templates: Crave.TemplateBindings
// Templates vendor: Crave Interactive
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.FactoryClasses;
using Crave.Ordering.Shared.Data.LLBLGen.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

// ReSharper disable All
namespace Crave.Ordering.Shared.Data.LLBLGen.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'MediaRatioTypeMedia'.<br/><br/></summary>
	[Serializable]
	public partial class MediaRatioTypeMediaEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END
	
	{
		private MediaEntity _media;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		private static MediaRatioTypeMediaEntityStaticMetaData _staticMetaData = new MediaRatioTypeMediaEntityStaticMetaData();
		private static MediaRatioTypeMediaRelations _relationsFactory = new MediaRatioTypeMediaRelations();		

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Media</summary>
			public static readonly string Media = "Media";
		}

		/// <summary>Static meta-data storage for navigator related information</summary>
		protected class MediaRatioTypeMediaEntityStaticMetaData : EntityStaticMetaDataBase
		{
			public MediaRatioTypeMediaEntityStaticMetaData()
			{
				SetEntityCoreInfo("MediaRatioTypeMediaEntity", InheritanceHierarchyType.None, false, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.MediaRatioTypeMediaEntity, typeof(MediaRatioTypeMediaEntity), typeof(MediaRatioTypeMediaEntityFactory), false);
				AddNavigatorMetaData<MediaRatioTypeMediaEntity, MediaEntity>("Media", "MediaRatioTypeMediaCollection", (a, b) => a._media = b, a => a._media, (a, b) => a.Media = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticMediaRatioTypeMediaRelations.MediaEntityUsingMediaIdStatic, ()=>new MediaRatioTypeMediaRelations().MediaEntityUsingMediaId, null, new int[] { (int)MediaRatioTypeMediaFieldIndex.MediaId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.MediaEntity);
			}
		}

		/// <summary>Static ctor</summary>
		static MediaRatioTypeMediaEntity()
		{
		}

		/// <summary> CTor</summary>
		public MediaRatioTypeMediaEntity()
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public MediaRatioTypeMediaEntity(IEntityFields2 fields)
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this MediaRatioTypeMediaEntity</param>
		public MediaRatioTypeMediaEntity(IValidator validator)
		{
			InitClassEmpty(validator, null);
		}

		/// <summary> CTor</summary>
		/// <param name="mediaRatioTypeMediaId">PK value for MediaRatioTypeMedia which data should be fetched into this MediaRatioTypeMedia object</param>
		public MediaRatioTypeMediaEntity(System.Int32 mediaRatioTypeMediaId) : this(mediaRatioTypeMediaId, null)
		{
		}

		/// <summary> CTor</summary>
		/// <param name="mediaRatioTypeMediaId">PK value for MediaRatioTypeMedia which data should be fetched into this MediaRatioTypeMedia object</param>
		/// <param name="validator">The custom validator object for this MediaRatioTypeMediaEntity</param>
		public MediaRatioTypeMediaEntity(System.Int32 mediaRatioTypeMediaId, IValidator validator)
		{
			InitClassEmpty(validator, null);
			this.MediaRatioTypeMediaId = mediaRatioTypeMediaId;
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected MediaRatioTypeMediaEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Media' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoMedia() { return CreateRelationInfoForNavigator("Media"); }
		
		/// <inheritdoc/>
		protected override EntityStaticMetaDataBase GetEntityStaticMetaData() {	return _staticMetaData; }

		/// <summary>Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		/// <summary>Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this MediaRatioTypeMediaEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary>The relations object holding all relations of this entity with other entity classes.</summary>
		public static MediaRatioTypeMediaRelations Relations { get { return _relationsFactory; } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Media' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathMediaEntity { get { return _staticMetaData.GetPrefetchPathElement("Media", CommonEntityBase.CreateEntityCollection<MediaEntity>()); } }

		/// <summary>The MediaRatioTypeMediaId property of the Entity MediaRatioTypeMedia<br/><br/></summary>
		/// <remarks>Mapped on  table field: "MediaRatioTypeMedia"."MediaRatioTypeMediaId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 MediaRatioTypeMediaId
		{
			get { return (System.Int32)GetValue((int)MediaRatioTypeMediaFieldIndex.MediaRatioTypeMediaId, true); }
			set { SetValue((int)MediaRatioTypeMediaFieldIndex.MediaRatioTypeMediaId, value); }		}

		/// <summary>The MediaId property of the Entity MediaRatioTypeMedia<br/><br/></summary>
		/// <remarks>Mapped on  table field: "MediaRatioTypeMedia"."MediaId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 MediaId
		{
			get { return (System.Int32)GetValue((int)MediaRatioTypeMediaFieldIndex.MediaId, true); }
			set	{ SetValue((int)MediaRatioTypeMediaFieldIndex.MediaId, value); }
		}

		/// <summary>The MediaType property of the Entity MediaRatioTypeMedia<br/><br/></summary>
		/// <remarks>Mapped on  table field: "MediaRatioTypeMedia"."MediaType".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<Crave.Enums.MediaType> MediaType
		{
			get { return (Nullable<Crave.Enums.MediaType>)GetValue((int)MediaRatioTypeMediaFieldIndex.MediaType, false); }
			set	{ SetValue((int)MediaRatioTypeMediaFieldIndex.MediaType, value); }
		}

		/// <summary>The Top property of the Entity MediaRatioTypeMedia<br/><br/></summary>
		/// <remarks>Mapped on  table field: "MediaRatioTypeMedia"."Top".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 Top
		{
			get { return (System.Int32)GetValue((int)MediaRatioTypeMediaFieldIndex.Top, true); }
			set	{ SetValue((int)MediaRatioTypeMediaFieldIndex.Top, value); }
		}

		/// <summary>The Left property of the Entity MediaRatioTypeMedia<br/><br/></summary>
		/// <remarks>Mapped on  table field: "MediaRatioTypeMedia"."Left".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 Left
		{
			get { return (System.Int32)GetValue((int)MediaRatioTypeMediaFieldIndex.Left, true); }
			set	{ SetValue((int)MediaRatioTypeMediaFieldIndex.Left, value); }
		}

		/// <summary>The Width property of the Entity MediaRatioTypeMedia<br/><br/></summary>
		/// <remarks>Mapped on  table field: "MediaRatioTypeMedia"."Width".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 Width
		{
			get { return (System.Int32)GetValue((int)MediaRatioTypeMediaFieldIndex.Width, true); }
			set	{ SetValue((int)MediaRatioTypeMediaFieldIndex.Width, value); }
		}

		/// <summary>The Height property of the Entity MediaRatioTypeMedia<br/><br/></summary>
		/// <remarks>Mapped on  table field: "MediaRatioTypeMedia"."Height".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 Height
		{
			get { return (System.Int32)GetValue((int)MediaRatioTypeMediaFieldIndex.Height, true); }
			set	{ SetValue((int)MediaRatioTypeMediaFieldIndex.Height, value); }
		}

		/// <summary>The ManuallyVerified property of the Entity MediaRatioTypeMedia<br/><br/></summary>
		/// <remarks>Mapped on  table field: "MediaRatioTypeMedia"."ManuallyVerified".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean ManuallyVerified
		{
			get { return (System.Boolean)GetValue((int)MediaRatioTypeMediaFieldIndex.ManuallyVerified, true); }
			set	{ SetValue((int)MediaRatioTypeMediaFieldIndex.ManuallyVerified, value); }
		}

		/// <summary>The LastDistributedVersionTicks property of the Entity MediaRatioTypeMedia<br/><br/></summary>
		/// <remarks>Mapped on  table field: "MediaRatioTypeMedia"."LastDistributedVersionTicks".<br/>Table field type characteristics (type, precision, scale, length): BigInt, 19, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> LastDistributedVersionTicks
		{
			get { return (Nullable<System.Int64>)GetValue((int)MediaRatioTypeMediaFieldIndex.LastDistributedVersionTicks, false); }
			set	{ SetValue((int)MediaRatioTypeMediaFieldIndex.LastDistributedVersionTicks, value); }
		}

		/// <summary>The LastDistributedVersionTicksAzure property of the Entity MediaRatioTypeMedia<br/><br/></summary>
		/// <remarks>Mapped on  table field: "MediaRatioTypeMedia"."LastDistributedVersionTicksAzure".<br/>Table field type characteristics (type, precision, scale, length): BigInt, 19, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> LastDistributedVersionTicksAzure
		{
			get { return (Nullable<System.Int64>)GetValue((int)MediaRatioTypeMediaFieldIndex.LastDistributedVersionTicksAzure, false); }
			set	{ SetValue((int)MediaRatioTypeMediaFieldIndex.LastDistributedVersionTicksAzure, value); }
		}

		/// <summary>The LastDistributedVersionTicksAmazon property of the Entity MediaRatioTypeMedia<br/><br/></summary>
		/// <remarks>Mapped on  table field: "MediaRatioTypeMedia"."LastDistributedVersionTicksAmazon".<br/>Table field type characteristics (type, precision, scale, length): BigInt, 19, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> LastDistributedVersionTicksAmazon
		{
			get { return (Nullable<System.Int64>)GetValue((int)MediaRatioTypeMediaFieldIndex.LastDistributedVersionTicksAmazon, false); }
			set	{ SetValue((int)MediaRatioTypeMediaFieldIndex.LastDistributedVersionTicksAmazon, value); }
		}

		/// <summary>The CreatedBy property of the Entity MediaRatioTypeMedia<br/><br/></summary>
		/// <remarks>Mapped on  table field: "MediaRatioTypeMedia"."CreatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)MediaRatioTypeMediaFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)MediaRatioTypeMediaFieldIndex.CreatedBy, value); }
		}

		/// <summary>The UpdatedBy property of the Entity MediaRatioTypeMedia<br/><br/></summary>
		/// <remarks>Mapped on  table field: "MediaRatioTypeMedia"."UpdatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)MediaRatioTypeMediaFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)MediaRatioTypeMediaFieldIndex.UpdatedBy, value); }
		}

		/// <summary>The CreatedUTC property of the Entity MediaRatioTypeMedia<br/><br/></summary>
		/// <remarks>Mapped on  table field: "MediaRatioTypeMedia"."CreatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)MediaRatioTypeMediaFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)MediaRatioTypeMediaFieldIndex.CreatedUTC, value); }
		}

		/// <summary>The UpdatedUTC property of the Entity MediaRatioTypeMedia<br/><br/></summary>
		/// <remarks>Mapped on  table field: "MediaRatioTypeMedia"."UpdatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)MediaRatioTypeMediaFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)MediaRatioTypeMediaFieldIndex.UpdatedUTC, value); }
		}

		/// <summary>Gets / sets related entity of type 'MediaEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual MediaEntity Media
		{
			get { return _media; }
			set { SetSingleRelatedEntityNavigator(value, "Media"); }
		}


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen
{
	public enum MediaRatioTypeMediaFieldIndex
	{
		///<summary>MediaRatioTypeMediaId. </summary>
		MediaRatioTypeMediaId,
		///<summary>MediaId. </summary>
		MediaId,
		///<summary>MediaType. </summary>
		MediaType,
		///<summary>Top. </summary>
		Top,
		///<summary>Left. </summary>
		Left,
		///<summary>Width. </summary>
		Width,
		///<summary>Height. </summary>
		Height,
		///<summary>ManuallyVerified. </summary>
		ManuallyVerified,
		///<summary>LastDistributedVersionTicks. </summary>
		LastDistributedVersionTicks,
		///<summary>LastDistributedVersionTicksAzure. </summary>
		LastDistributedVersionTicksAzure,
		///<summary>LastDistributedVersionTicksAmazon. </summary>
		LastDistributedVersionTicksAmazon,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: MediaRatioTypeMedia. </summary>
	public partial class MediaRatioTypeMediaRelations: RelationFactory
	{
		#region Custom Relation code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomRelationCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		


		/// <summary>Gets all relations and delete rules of the MediaRatioTypeMediaEntity as a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects.</summary>
		/// <returns>a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects</returns>
		public virtual Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> GetRelationsAndDeleteRules()
		{
			Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> toReturn = new Dictionary<IEntityRelation, ReferentialConstraintDeleteRule>();
			return toReturn;
		}		
	
	

		/// <summary>Returns a new IEntityRelation object, between MediaRatioTypeMediaEntity and MediaEntity over the m:1 relation they have, using the relation between the fields: MediaRatioTypeMedia.MediaId - Media.MediaId</summary>
		public virtual IEntityRelation MediaEntityUsingMediaId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Media", false, new[] { MediaFields.MediaId, MediaRatioTypeMediaFields.MediaId }); }
		}

	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticMediaRatioTypeMediaRelations
	{
		internal static readonly IEntityRelation MediaEntityUsingMediaIdStatic = new MediaRatioTypeMediaRelations().MediaEntityUsingMediaId;

		/// <summary>CTor</summary>
		static StaticMediaRatioTypeMediaRelations() { }
	}
}

