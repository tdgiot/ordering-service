﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 5.8
// Code is generated on: 
// Code is generated using templates: Crave.TemplateBindings
// Templates vendor: Crave Interactive
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.FactoryClasses;
using Crave.Ordering.Shared.Data.LLBLGen.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

// ReSharper disable All
namespace Crave.Ordering.Shared.Data.LLBLGen.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'Requestlog'.<br/><br/></summary>
	[Serializable]
	public partial class RequestlogEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END
	
	{

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		private static RequestlogEntityStaticMetaData _staticMetaData = new RequestlogEntityStaticMetaData();
		private static RequestlogRelations _relationsFactory = new RequestlogRelations();		

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
		}

		/// <summary>Static meta-data storage for navigator related information</summary>
		protected class RequestlogEntityStaticMetaData : EntityStaticMetaDataBase
		{
			public RequestlogEntityStaticMetaData()
			{
				SetEntityCoreInfo("RequestlogEntity", InheritanceHierarchyType.None, false, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.RequestlogEntity, typeof(RequestlogEntity), typeof(RequestlogEntityFactory), false);
			}
		}

		/// <summary>Static ctor</summary>
		static RequestlogEntity()
		{
		}

		/// <summary> CTor</summary>
		public RequestlogEntity()
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public RequestlogEntity(IEntityFields2 fields)
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this RequestlogEntity</param>
		public RequestlogEntity(IValidator validator)
		{
			InitClassEmpty(validator, null);
		}

		/// <summary> CTor</summary>
		/// <param name="requestlogId">PK value for Requestlog which data should be fetched into this Requestlog object</param>
		public RequestlogEntity(System.Int64 requestlogId) : this(requestlogId, null)
		{
		}

		/// <summary> CTor</summary>
		/// <param name="requestlogId">PK value for Requestlog which data should be fetched into this Requestlog object</param>
		/// <param name="validator">The custom validator object for this RequestlogEntity</param>
		public RequestlogEntity(System.Int64 requestlogId, IValidator validator)
		{
			InitClassEmpty(validator, null);
			this.RequestlogId = requestlogId;
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected RequestlogEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <inheritdoc/>
		protected override EntityStaticMetaDataBase GetEntityStaticMetaData() {	return _staticMetaData; }

		/// <summary>Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		/// <summary>Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this RequestlogEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary>The relations object holding all relations of this entity with other entity classes.</summary>
		public static RequestlogRelations Relations { get { return _relationsFactory; } }

		/// <summary>The RequestlogId property of the Entity Requestlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Requestlog"."RequestlogId".<br/>Table field type characteristics (type, precision, scale, length): BigInt, 19, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 RequestlogId
		{
			get { return (System.Int64)GetValue((int)RequestlogFieldIndex.RequestlogId, true); }
			set { SetValue((int)RequestlogFieldIndex.RequestlogId, value); }		}

		/// <summary>The ServerName property of the Entity Requestlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Requestlog"."ServerName".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ServerName
		{
			get { return (System.String)GetValue((int)RequestlogFieldIndex.ServerName, true); }
			set	{ SetValue((int)RequestlogFieldIndex.ServerName, value); }
		}

		/// <summary>The UserAgent property of the Entity Requestlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Requestlog"."UserAgent".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String UserAgent
		{
			get { return (System.String)GetValue((int)RequestlogFieldIndex.UserAgent, true); }
			set	{ SetValue((int)RequestlogFieldIndex.UserAgent, value); }
		}

		/// <summary>The SourceApplication property of the Entity Requestlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Requestlog"."SourceApplication".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String SourceApplication
		{
			get { return (System.String)GetValue((int)RequestlogFieldIndex.SourceApplication, true); }
			set	{ SetValue((int)RequestlogFieldIndex.SourceApplication, value); }
		}

		/// <summary>The Identifier property of the Entity Requestlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Requestlog"."Identifier".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Identifier
		{
			get { return (System.String)GetValue((int)RequestlogFieldIndex.Identifier, true); }
			set	{ SetValue((int)RequestlogFieldIndex.Identifier, value); }
		}

		/// <summary>The CustomerId property of the Entity Requestlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Requestlog"."CustomerId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CustomerId
		{
			get { return (Nullable<System.Int32>)GetValue((int)RequestlogFieldIndex.CustomerId, false); }
			set	{ SetValue((int)RequestlogFieldIndex.CustomerId, value); }
		}

		/// <summary>The TerminalId property of the Entity Requestlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Requestlog"."TerminalId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> TerminalId
		{
			get { return (Nullable<System.Int32>)GetValue((int)RequestlogFieldIndex.TerminalId, false); }
			set	{ SetValue((int)RequestlogFieldIndex.TerminalId, value); }
		}

		/// <summary>The Phonenumber property of the Entity Requestlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Requestlog"."Phonenumber".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Phonenumber
		{
			get { return (System.String)GetValue((int)RequestlogFieldIndex.Phonenumber, true); }
			set	{ SetValue((int)RequestlogFieldIndex.Phonenumber, value); }
		}

		/// <summary>The MethodName property of the Entity Requestlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Requestlog"."MethodName".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String MethodName
		{
			get { return (System.String)GetValue((int)RequestlogFieldIndex.MethodName, true); }
			set	{ SetValue((int)RequestlogFieldIndex.MethodName, value); }
		}

		/// <summary>The ResultEnumTypeName property of the Entity Requestlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Requestlog"."ResultEnumTypeName".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ResultEnumTypeName
		{
			get { return (System.String)GetValue((int)RequestlogFieldIndex.ResultEnumTypeName, true); }
			set	{ SetValue((int)RequestlogFieldIndex.ResultEnumTypeName, value); }
		}

		/// <summary>The ResultEnumValueName property of the Entity Requestlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Requestlog"."ResultEnumValueName".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ResultEnumValueName
		{
			get { return (System.String)GetValue((int)RequestlogFieldIndex.ResultEnumValueName, true); }
			set	{ SetValue((int)RequestlogFieldIndex.ResultEnumValueName, value); }
		}

		/// <summary>The ResultCode property of the Entity Requestlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Requestlog"."ResultCode".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ResultCode
		{
			get { return (System.String)GetValue((int)RequestlogFieldIndex.ResultCode, true); }
			set	{ SetValue((int)RequestlogFieldIndex.ResultCode, value); }
		}

		/// <summary>The ResultMessage property of the Entity Requestlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Requestlog"."ResultMessage".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ResultMessage
		{
			get { return (System.String)GetValue((int)RequestlogFieldIndex.ResultMessage, true); }
			set	{ SetValue((int)RequestlogFieldIndex.ResultMessage, value); }
		}

		/// <summary>The Parameter1 property of the Entity Requestlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Requestlog"."Parameter1".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Parameter1
		{
			get { return (System.String)GetValue((int)RequestlogFieldIndex.Parameter1, true); }
			set	{ SetValue((int)RequestlogFieldIndex.Parameter1, value); }
		}

		/// <summary>The Parameter2 property of the Entity Requestlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Requestlog"."Parameter2".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Parameter2
		{
			get { return (System.String)GetValue((int)RequestlogFieldIndex.Parameter2, true); }
			set	{ SetValue((int)RequestlogFieldIndex.Parameter2, value); }
		}

		/// <summary>The Parameter3 property of the Entity Requestlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Requestlog"."Parameter3".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Parameter3
		{
			get { return (System.String)GetValue((int)RequestlogFieldIndex.Parameter3, true); }
			set	{ SetValue((int)RequestlogFieldIndex.Parameter3, value); }
		}

		/// <summary>The Parameter4 property of the Entity Requestlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Requestlog"."Parameter4".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Parameter4
		{
			get { return (System.String)GetValue((int)RequestlogFieldIndex.Parameter4, true); }
			set	{ SetValue((int)RequestlogFieldIndex.Parameter4, value); }
		}

		/// <summary>The Parameter5 property of the Entity Requestlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Requestlog"."Parameter5".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Parameter5
		{
			get { return (System.String)GetValue((int)RequestlogFieldIndex.Parameter5, true); }
			set	{ SetValue((int)RequestlogFieldIndex.Parameter5, value); }
		}

		/// <summary>The Parameter6 property of the Entity Requestlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Requestlog"."Parameter6".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Parameter6
		{
			get { return (System.String)GetValue((int)RequestlogFieldIndex.Parameter6, true); }
			set	{ SetValue((int)RequestlogFieldIndex.Parameter6, value); }
		}

		/// <summary>The ResultBody property of the Entity Requestlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Requestlog"."ResultBody".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ResultBody
		{
			get { return (System.String)GetValue((int)RequestlogFieldIndex.ResultBody, true); }
			set	{ SetValue((int)RequestlogFieldIndex.ResultBody, value); }
		}

		/// <summary>The ErrorMessage property of the Entity Requestlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Requestlog"."ErrorMessage".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ErrorMessage
		{
			get { return (System.String)GetValue((int)RequestlogFieldIndex.ErrorMessage, true); }
			set	{ SetValue((int)RequestlogFieldIndex.ErrorMessage, value); }
		}

		/// <summary>The ErrorStackTrace property of the Entity Requestlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Requestlog"."ErrorStackTrace".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ErrorStackTrace
		{
			get { return (System.String)GetValue((int)RequestlogFieldIndex.ErrorStackTrace, true); }
			set	{ SetValue((int)RequestlogFieldIndex.ErrorStackTrace, value); }
		}

		/// <summary>The Xml property of the Entity Requestlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Requestlog"."Xml".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Xml
		{
			get { return (System.String)GetValue((int)RequestlogFieldIndex.Xml, true); }
			set	{ SetValue((int)RequestlogFieldIndex.Xml, value); }
		}

		/// <summary>The RawRequest property of the Entity Requestlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Requestlog"."RawRequest".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String RawRequest
		{
			get { return (System.String)GetValue((int)RequestlogFieldIndex.RawRequest, true); }
			set	{ SetValue((int)RequestlogFieldIndex.RawRequest, value); }
		}

		/// <summary>The TraceFromMobile property of the Entity Requestlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Requestlog"."TraceFromMobile".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String TraceFromMobile
		{
			get { return (System.String)GetValue((int)RequestlogFieldIndex.TraceFromMobile, true); }
			set	{ SetValue((int)RequestlogFieldIndex.TraceFromMobile, value); }
		}

		/// <summary>The MobileVendor property of the Entity Requestlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Requestlog"."MobileVendor".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String MobileVendor
		{
			get { return (System.String)GetValue((int)RequestlogFieldIndex.MobileVendor, true); }
			set	{ SetValue((int)RequestlogFieldIndex.MobileVendor, value); }
		}

		/// <summary>The MobileIdentifier property of the Entity Requestlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Requestlog"."MobileIdentifier".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String MobileIdentifier
		{
			get { return (System.String)GetValue((int)RequestlogFieldIndex.MobileIdentifier, true); }
			set	{ SetValue((int)RequestlogFieldIndex.MobileIdentifier, value); }
		}

		/// <summary>The MobilePlatform property of the Entity Requestlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Requestlog"."MobilePlatform".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String MobilePlatform
		{
			get { return (System.String)GetValue((int)RequestlogFieldIndex.MobilePlatform, true); }
			set	{ SetValue((int)RequestlogFieldIndex.MobilePlatform, value); }
		}

		/// <summary>The MobileOs property of the Entity Requestlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Requestlog"."MobileOs".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String MobileOs
		{
			get { return (System.String)GetValue((int)RequestlogFieldIndex.MobileOs, true); }
			set	{ SetValue((int)RequestlogFieldIndex.MobileOs, value); }
		}

		/// <summary>The MobileName property of the Entity Requestlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Requestlog"."MobileName".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String MobileName
		{
			get { return (System.String)GetValue((int)RequestlogFieldIndex.MobileName, true); }
			set	{ SetValue((int)RequestlogFieldIndex.MobileName, value); }
		}

		/// <summary>The DeviceInfo property of the Entity Requestlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Requestlog"."DeviceInfo".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String DeviceInfo
		{
			get { return (System.String)GetValue((int)RequestlogFieldIndex.DeviceInfo, true); }
			set	{ SetValue((int)RequestlogFieldIndex.DeviceInfo, value); }
		}

		/// <summary>The Log property of the Entity Requestlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Requestlog"."Log".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Log
		{
			get { return (System.String)GetValue((int)RequestlogFieldIndex.Log, true); }
			set	{ SetValue((int)RequestlogFieldIndex.Log, value); }
		}

		/// <summary>The CreatedBy property of the Entity Requestlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Requestlog"."CreatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CreatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)RequestlogFieldIndex.CreatedBy, false); }
			set	{ SetValue((int)RequestlogFieldIndex.CreatedBy, value); }
		}

		/// <summary>The UpdatedBy property of the Entity Requestlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Requestlog"."UpdatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UpdatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)RequestlogFieldIndex.UpdatedBy, false); }
			set	{ SetValue((int)RequestlogFieldIndex.UpdatedBy, value); }
		}

		/// <summary>The CreatedUTC property of the Entity Requestlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Requestlog"."CreatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)RequestlogFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)RequestlogFieldIndex.CreatedUTC, value); }
		}

		/// <summary>The UpdatedUTC property of the Entity Requestlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Requestlog"."UpdatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)RequestlogFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)RequestlogFieldIndex.UpdatedUTC, value); }
		}


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen
{
	public enum RequestlogFieldIndex
	{
		///<summary>RequestlogId. </summary>
		RequestlogId,
		///<summary>ServerName. </summary>
		ServerName,
		///<summary>UserAgent. </summary>
		UserAgent,
		///<summary>SourceApplication. </summary>
		SourceApplication,
		///<summary>Identifier. </summary>
		Identifier,
		///<summary>CustomerId. </summary>
		CustomerId,
		///<summary>TerminalId. </summary>
		TerminalId,
		///<summary>Phonenumber. </summary>
		Phonenumber,
		///<summary>MethodName. </summary>
		MethodName,
		///<summary>ResultEnumTypeName. </summary>
		ResultEnumTypeName,
		///<summary>ResultEnumValueName. </summary>
		ResultEnumValueName,
		///<summary>ResultCode. </summary>
		ResultCode,
		///<summary>ResultMessage. </summary>
		ResultMessage,
		///<summary>Parameter1. </summary>
		Parameter1,
		///<summary>Parameter2. </summary>
		Parameter2,
		///<summary>Parameter3. </summary>
		Parameter3,
		///<summary>Parameter4. </summary>
		Parameter4,
		///<summary>Parameter5. </summary>
		Parameter5,
		///<summary>Parameter6. </summary>
		Parameter6,
		///<summary>ResultBody. </summary>
		ResultBody,
		///<summary>ErrorMessage. </summary>
		ErrorMessage,
		///<summary>ErrorStackTrace. </summary>
		ErrorStackTrace,
		///<summary>Xml. </summary>
		Xml,
		///<summary>RawRequest. </summary>
		RawRequest,
		///<summary>TraceFromMobile. </summary>
		TraceFromMobile,
		///<summary>MobileVendor. </summary>
		MobileVendor,
		///<summary>MobileIdentifier. </summary>
		MobileIdentifier,
		///<summary>MobilePlatform. </summary>
		MobilePlatform,
		///<summary>MobileOs. </summary>
		MobileOs,
		///<summary>MobileName. </summary>
		MobileName,
		///<summary>DeviceInfo. </summary>
		DeviceInfo,
		///<summary>Log. </summary>
		Log,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Requestlog. </summary>
	public partial class RequestlogRelations: RelationFactory
	{
		#region Custom Relation code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomRelationCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		


		/// <summary>Gets all relations and delete rules of the RequestlogEntity as a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects.</summary>
		/// <returns>a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects</returns>
		public virtual Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> GetRelationsAndDeleteRules()
		{
			Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> toReturn = new Dictionary<IEntityRelation, ReferentialConstraintDeleteRule>();
			return toReturn;
		}		
	
	

	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticRequestlogRelations
	{

		/// <summary>CTor</summary>
		static StaticRequestlogRelations() { }
	}
}

