﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 5.8
// Code is generated on: 
// Code is generated using templates: Crave.TemplateBindings
// Templates vendor: Crave Interactive
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.FactoryClasses;
using Crave.Ordering.Shared.Data.LLBLGen.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

// ReSharper disable All
namespace Crave.Ordering.Shared.Data.LLBLGen.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'AlterationoptionTag'.<br/><br/></summary>
	[Serializable]
	public partial class AlterationoptionTagEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END
	
	{
		private AlterationoptionEntity _alterationoption;
		private TagEntity _tag;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		private static AlterationoptionTagEntityStaticMetaData _staticMetaData = new AlterationoptionTagEntityStaticMetaData();
		private static AlterationoptionTagRelations _relationsFactory = new AlterationoptionTagRelations();		

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Alterationoption</summary>
			public static readonly string Alterationoption = "Alterationoption";
			/// <summary>Member name Tag</summary>
			public static readonly string Tag = "Tag";
		}

		/// <summary>Static meta-data storage for navigator related information</summary>
		protected class AlterationoptionTagEntityStaticMetaData : EntityStaticMetaDataBase
		{
			public AlterationoptionTagEntityStaticMetaData()
			{
				SetEntityCoreInfo("AlterationoptionTagEntity", InheritanceHierarchyType.None, false, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.AlterationoptionTagEntity, typeof(AlterationoptionTagEntity), typeof(AlterationoptionTagEntityFactory), false);
				AddNavigatorMetaData<AlterationoptionTagEntity, AlterationoptionEntity>("Alterationoption", "AlterationoptionTagCollection", (a, b) => a._alterationoption = b, a => a._alterationoption, (a, b) => a.Alterationoption = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticAlterationoptionTagRelations.AlterationoptionEntityUsingAlterationoptionIdStatic, ()=>new AlterationoptionTagRelations().AlterationoptionEntityUsingAlterationoptionId, null, new int[] { (int)AlterationoptionTagFieldIndex.AlterationoptionId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.AlterationoptionEntity);
				AddNavigatorMetaData<AlterationoptionTagEntity, TagEntity>("Tag", "AlterationoptionTagCollection", (a, b) => a._tag = b, a => a._tag, (a, b) => a.Tag = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticAlterationoptionTagRelations.TagEntityUsingTagIdStatic, ()=>new AlterationoptionTagRelations().TagEntityUsingTagId, null, new int[] { (int)AlterationoptionTagFieldIndex.TagId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.TagEntity);
			}
		}

		/// <summary>Static ctor</summary>
		static AlterationoptionTagEntity()
		{
		}

		/// <summary> CTor</summary>
		public AlterationoptionTagEntity()
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public AlterationoptionTagEntity(IEntityFields2 fields)
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this AlterationoptionTagEntity</param>
		public AlterationoptionTagEntity(IValidator validator)
		{
			InitClassEmpty(validator, null);
		}

		/// <summary> CTor</summary>
		/// <param name="alterationoptionTagId">PK value for AlterationoptionTag which data should be fetched into this AlterationoptionTag object</param>
		public AlterationoptionTagEntity(System.Int32 alterationoptionTagId) : this(alterationoptionTagId, null)
		{
		}

		/// <summary> CTor</summary>
		/// <param name="alterationoptionTagId">PK value for AlterationoptionTag which data should be fetched into this AlterationoptionTag object</param>
		/// <param name="validator">The custom validator object for this AlterationoptionTagEntity</param>
		public AlterationoptionTagEntity(System.Int32 alterationoptionTagId, IValidator validator)
		{
			InitClassEmpty(validator, null);
			this.AlterationoptionTagId = alterationoptionTagId;
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected AlterationoptionTagEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Alterationoption' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoAlterationoption() { return CreateRelationInfoForNavigator("Alterationoption"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Tag' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoTag() { return CreateRelationInfoForNavigator("Tag"); }
		
		/// <inheritdoc/>
		protected override EntityStaticMetaDataBase GetEntityStaticMetaData() {	return _staticMetaData; }

		/// <summary>Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		/// <summary>Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this AlterationoptionTagEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary>The relations object holding all relations of this entity with other entity classes.</summary>
		public static AlterationoptionTagRelations Relations { get { return _relationsFactory; } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Alterationoption' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathAlterationoptionEntity { get { return _staticMetaData.GetPrefetchPathElement("Alterationoption", CommonEntityBase.CreateEntityCollection<AlterationoptionEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Tag' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathTagEntity { get { return _staticMetaData.GetPrefetchPathElement("Tag", CommonEntityBase.CreateEntityCollection<TagEntity>()); } }

		/// <summary>The AlterationoptionTagId property of the Entity AlterationoptionTag<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AlterationoptionTag"."AlterationoptionTagId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 AlterationoptionTagId
		{
			get { return (System.Int32)GetValue((int)AlterationoptionTagFieldIndex.AlterationoptionTagId, true); }
			set { SetValue((int)AlterationoptionTagFieldIndex.AlterationoptionTagId, value); }		}

		/// <summary>The AlterationoptionId property of the Entity AlterationoptionTag<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AlterationoptionTag"."AlterationoptionId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 AlterationoptionId
		{
			get { return (System.Int32)GetValue((int)AlterationoptionTagFieldIndex.AlterationoptionId, true); }
			set	{ SetValue((int)AlterationoptionTagFieldIndex.AlterationoptionId, value); }
		}

		/// <summary>The TagId property of the Entity AlterationoptionTag<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AlterationoptionTag"."TagId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 TagId
		{
			get { return (System.Int32)GetValue((int)AlterationoptionTagFieldIndex.TagId, true); }
			set	{ SetValue((int)AlterationoptionTagFieldIndex.TagId, value); }
		}

		/// <summary>The ParentCompanyId property of the Entity AlterationoptionTag<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AlterationoptionTag"."ParentCompanyId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ParentCompanyId
		{
			get { return (Nullable<System.Int32>)GetValue((int)AlterationoptionTagFieldIndex.ParentCompanyId, false); }
			set	{ SetValue((int)AlterationoptionTagFieldIndex.ParentCompanyId, value); }
		}

		/// <summary>The CreatedUTC property of the Entity AlterationoptionTag<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AlterationoptionTag"."CreatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime CreatedUTC
		{
			get { return (System.DateTime)GetValue((int)AlterationoptionTagFieldIndex.CreatedUTC, true); }
			set	{ SetValue((int)AlterationoptionTagFieldIndex.CreatedUTC, value); }
		}

		/// <summary>The UpdatedUTC property of the Entity AlterationoptionTag<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AlterationoptionTag"."UpdatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)AlterationoptionTagFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)AlterationoptionTagFieldIndex.UpdatedUTC, value); }
		}

		/// <summary>Gets / sets related entity of type 'AlterationoptionEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual AlterationoptionEntity Alterationoption
		{
			get { return _alterationoption; }
			set { SetSingleRelatedEntityNavigator(value, "Alterationoption"); }
		}

		/// <summary>Gets / sets related entity of type 'TagEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual TagEntity Tag
		{
			get { return _tag; }
			set { SetSingleRelatedEntityNavigator(value, "Tag"); }
		}


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen
{
	public enum AlterationoptionTagFieldIndex
	{
		///<summary>AlterationoptionTagId. </summary>
		AlterationoptionTagId,
		///<summary>AlterationoptionId. </summary>
		AlterationoptionId,
		///<summary>TagId. </summary>
		TagId,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: AlterationoptionTag. </summary>
	public partial class AlterationoptionTagRelations: RelationFactory
	{
		#region Custom Relation code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomRelationCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		


		/// <summary>Gets all relations and delete rules of the AlterationoptionTagEntity as a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects.</summary>
		/// <returns>a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects</returns>
		public virtual Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> GetRelationsAndDeleteRules()
		{
			Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> toReturn = new Dictionary<IEntityRelation, ReferentialConstraintDeleteRule>();
			return toReturn;
		}		
	
	

		/// <summary>Returns a new IEntityRelation object, between AlterationoptionTagEntity and AlterationoptionEntity over the m:1 relation they have, using the relation between the fields: AlterationoptionTag.AlterationoptionId - Alterationoption.AlterationoptionId</summary>
		public virtual IEntityRelation AlterationoptionEntityUsingAlterationoptionId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Alterationoption", false, new[] { AlterationoptionFields.AlterationoptionId, AlterationoptionTagFields.AlterationoptionId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between AlterationoptionTagEntity and TagEntity over the m:1 relation they have, using the relation between the fields: AlterationoptionTag.TagId - Tag.TagId</summary>
		public virtual IEntityRelation TagEntityUsingTagId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Tag", false, new[] { TagFields.TagId, AlterationoptionTagFields.TagId }); }
		}

	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticAlterationoptionTagRelations
	{
		internal static readonly IEntityRelation AlterationoptionEntityUsingAlterationoptionIdStatic = new AlterationoptionTagRelations().AlterationoptionEntityUsingAlterationoptionId;
		internal static readonly IEntityRelation TagEntityUsingTagIdStatic = new AlterationoptionTagRelations().TagEntityUsingTagId;

		/// <summary>CTor</summary>
		static StaticAlterationoptionTagRelations() { }
	}
}

