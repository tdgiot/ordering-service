﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 5.8
// Code is generated on: 
// Code is generated using templates: Crave.TemplateBindings
// Templates vendor: Crave Interactive
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.FactoryClasses;
using Crave.Ordering.Shared.Data.LLBLGen.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

// ReSharper disable All
namespace Crave.Ordering.Shared.Data.LLBLGen.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'OutletSellerInformation'.<br/><br/></summary>
	[Serializable]
	public partial class OutletSellerInformationEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END
	
	{
		private EntityCollection<OutletEntity> _outletCollection;
		private EntityCollection<ReceiptEntity> _receiptCollection;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		private static OutletSellerInformationEntityStaticMetaData _staticMetaData = new OutletSellerInformationEntityStaticMetaData();
		private static OutletSellerInformationRelations _relationsFactory = new OutletSellerInformationRelations();		

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name OutletCollection</summary>
			public static readonly string OutletCollection = "OutletCollection";
			/// <summary>Member name ReceiptCollection</summary>
			public static readonly string ReceiptCollection = "ReceiptCollection";
		}

		/// <summary>Static meta-data storage for navigator related information</summary>
		protected class OutletSellerInformationEntityStaticMetaData : EntityStaticMetaDataBase
		{
			public OutletSellerInformationEntityStaticMetaData()
			{
				SetEntityCoreInfo("OutletSellerInformationEntity", InheritanceHierarchyType.None, false, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.OutletSellerInformationEntity, typeof(OutletSellerInformationEntity), typeof(OutletSellerInformationEntityFactory), false);
				AddNavigatorMetaData<OutletSellerInformationEntity, EntityCollection<OutletEntity>>("OutletCollection", a => a._outletCollection, (a, b) => a._outletCollection = b, a => a.OutletCollection, () => new OutletSellerInformationRelations().OutletEntityUsingOutletSellerInformationId, typeof(OutletEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.OutletEntity);
				AddNavigatorMetaData<OutletSellerInformationEntity, EntityCollection<ReceiptEntity>>("ReceiptCollection", a => a._receiptCollection, (a, b) => a._receiptCollection = b, a => a.ReceiptCollection, () => new OutletSellerInformationRelations().ReceiptEntityUsingOutletSellerInformationId, typeof(ReceiptEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ReceiptEntity);
			}
		}

		/// <summary>Static ctor</summary>
		static OutletSellerInformationEntity()
		{
		}

		/// <summary> CTor</summary>
		public OutletSellerInformationEntity()
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public OutletSellerInformationEntity(IEntityFields2 fields)
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this OutletSellerInformationEntity</param>
		public OutletSellerInformationEntity(IValidator validator)
		{
			InitClassEmpty(validator, null);
		}

		/// <summary> CTor</summary>
		/// <param name="outletSellerInformationId">PK value for OutletSellerInformation which data should be fetched into this OutletSellerInformation object</param>
		public OutletSellerInformationEntity(System.Int32 outletSellerInformationId) : this(outletSellerInformationId, null)
		{
		}

		/// <summary> CTor</summary>
		/// <param name="outletSellerInformationId">PK value for OutletSellerInformation which data should be fetched into this OutletSellerInformation object</param>
		/// <param name="validator">The custom validator object for this OutletSellerInformationEntity</param>
		public OutletSellerInformationEntity(System.Int32 outletSellerInformationId, IValidator validator)
		{
			InitClassEmpty(validator, null);
			this.OutletSellerInformationId = outletSellerInformationId;
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected OutletSellerInformationEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Outlet' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoOutletCollection() { return CreateRelationInfoForNavigator("OutletCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Receipt' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoReceiptCollection() { return CreateRelationInfoForNavigator("ReceiptCollection"); }
		
		/// <inheritdoc/>
		protected override EntityStaticMetaDataBase GetEntityStaticMetaData() {	return _staticMetaData; }

		/// <summary>Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		/// <summary>Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this OutletSellerInformationEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary>The relations object holding all relations of this entity with other entity classes.</summary>
		public static OutletSellerInformationRelations Relations { get { return _relationsFactory; } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Outlet' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathOutletCollection { get { return _staticMetaData.GetPrefetchPathElement("OutletCollection", CommonEntityBase.CreateEntityCollection<OutletEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Receipt' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathReceiptCollection { get { return _staticMetaData.GetPrefetchPathElement("ReceiptCollection", CommonEntityBase.CreateEntityCollection<ReceiptEntity>()); } }

		/// <summary>The OutletSellerInformationId property of the Entity OutletSellerInformation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OutletSellerInformation"."OutletSellerInformationId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 OutletSellerInformationId
		{
			get { return (System.Int32)GetValue((int)OutletSellerInformationFieldIndex.OutletSellerInformationId, true); }
			set { SetValue((int)OutletSellerInformationFieldIndex.OutletSellerInformationId, value); }		}

		/// <summary>The OutletId property of the Entity OutletSellerInformation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OutletSellerInformation"."OutletId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> OutletId
		{
			get { return (Nullable<System.Int32>)GetValue((int)OutletSellerInformationFieldIndex.OutletId, false); }
			set	{ SetValue((int)OutletSellerInformationFieldIndex.OutletId, value); }
		}

		/// <summary>The ParentCompanyId property of the Entity OutletSellerInformation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OutletSellerInformation"."ParentCompanyId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ParentCompanyId
		{
			get { return (Nullable<System.Int32>)GetValue((int)OutletSellerInformationFieldIndex.ParentCompanyId, false); }
			set	{ SetValue((int)OutletSellerInformationFieldIndex.ParentCompanyId, value); }
		}

		/// <summary>The SellerName property of the Entity OutletSellerInformation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OutletSellerInformation"."SellerName".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String SellerName
		{
			get { return (System.String)GetValue((int)OutletSellerInformationFieldIndex.SellerName, true); }
			set	{ SetValue((int)OutletSellerInformationFieldIndex.SellerName, value); }
		}

		/// <summary>The Address property of the Entity OutletSellerInformation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OutletSellerInformation"."Address".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Address
		{
			get { return (System.String)GetValue((int)OutletSellerInformationFieldIndex.Address, true); }
			set	{ SetValue((int)OutletSellerInformationFieldIndex.Address, value); }
		}

		/// <summary>The Phonenumber property of the Entity OutletSellerInformation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OutletSellerInformation"."Phonenumber".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Phonenumber
		{
			get { return (System.String)GetValue((int)OutletSellerInformationFieldIndex.Phonenumber, true); }
			set	{ SetValue((int)OutletSellerInformationFieldIndex.Phonenumber, value); }
		}

		/// <summary>The Email property of the Entity OutletSellerInformation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OutletSellerInformation"."Email".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Email
		{
			get { return (System.String)GetValue((int)OutletSellerInformationFieldIndex.Email, true); }
			set	{ SetValue((int)OutletSellerInformationFieldIndex.Email, value); }
		}

		/// <summary>The VatNumber property of the Entity OutletSellerInformation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OutletSellerInformation"."VatNumber".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String VatNumber
		{
			get { return (System.String)GetValue((int)OutletSellerInformationFieldIndex.VatNumber, true); }
			set	{ SetValue((int)OutletSellerInformationFieldIndex.VatNumber, value); }
		}

		/// <summary>The ReceiptReceiversEmail property of the Entity OutletSellerInformation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OutletSellerInformation"."ReceiptReceiversEmail".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2000.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ReceiptReceiversEmail
		{
			get { return (System.String)GetValue((int)OutletSellerInformationFieldIndex.ReceiptReceiversEmail, true); }
			set	{ SetValue((int)OutletSellerInformationFieldIndex.ReceiptReceiversEmail, value); }
		}

		/// <summary>The ReceiptReceiversSms property of the Entity OutletSellerInformation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OutletSellerInformation"."ReceiptReceiversSms".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2000.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ReceiptReceiversSms
		{
			get { return (System.String)GetValue((int)OutletSellerInformationFieldIndex.ReceiptReceiversSms, true); }
			set	{ SetValue((int)OutletSellerInformationFieldIndex.ReceiptReceiversSms, value); }
		}

		/// <summary>The SmsOriginator property of the Entity OutletSellerInformation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OutletSellerInformation"."SmsOriginator".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 20.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String SmsOriginator
		{
			get { return (System.String)GetValue((int)OutletSellerInformationFieldIndex.SmsOriginator, true); }
			set	{ SetValue((int)OutletSellerInformationFieldIndex.SmsOriginator, value); }
		}

		/// <summary>The TaxBreakdown property of the Entity OutletSellerInformation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OutletSellerInformation"."TaxBreakdown".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Crave.Enums.TaxBreakdown TaxBreakdown
		{
			get { return (Crave.Enums.TaxBreakdown)GetValue((int)OutletSellerInformationFieldIndex.TaxBreakdown, true); }
			set	{ SetValue((int)OutletSellerInformationFieldIndex.TaxBreakdown, value); }
		}

		/// <summary>The CreatedUTC property of the Entity OutletSellerInformation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OutletSellerInformation"."CreatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime CreatedUTC
		{
			get { return (System.DateTime)GetValue((int)OutletSellerInformationFieldIndex.CreatedUTC, true); }
			set	{ SetValue((int)OutletSellerInformationFieldIndex.CreatedUTC, value); }
		}

		/// <summary>The CreatedBy property of the Entity OutletSellerInformation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OutletSellerInformation"."CreatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CreatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)OutletSellerInformationFieldIndex.CreatedBy, false); }
			set	{ SetValue((int)OutletSellerInformationFieldIndex.CreatedBy, value); }
		}

		/// <summary>The UpdatedUTC property of the Entity OutletSellerInformation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OutletSellerInformation"."UpdatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)OutletSellerInformationFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)OutletSellerInformationFieldIndex.UpdatedUTC, value); }
		}

		/// <summary>The UpdatedBy property of the Entity OutletSellerInformation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OutletSellerInformation"."UpdatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UpdatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)OutletSellerInformationFieldIndex.UpdatedBy, false); }
			set	{ SetValue((int)OutletSellerInformationFieldIndex.UpdatedBy, value); }
		}

		/// <summary>The ReceiveOrderReceiptNotifications property of the Entity OutletSellerInformation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OutletSellerInformation"."ReceiveOrderReceiptNotifications".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean ReceiveOrderReceiptNotifications
		{
			get { return (System.Boolean)GetValue((int)OutletSellerInformationFieldIndex.ReceiveOrderReceiptNotifications, true); }
			set	{ SetValue((int)OutletSellerInformationFieldIndex.ReceiveOrderReceiptNotifications, value); }
		}

		/// <summary>The ReceiveOrderConfirmationNotifications property of the Entity OutletSellerInformation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OutletSellerInformation"."ReceiveOrderConfirmationNotifications".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean ReceiveOrderConfirmationNotifications
		{
			get { return (System.Boolean)GetValue((int)OutletSellerInformationFieldIndex.ReceiveOrderConfirmationNotifications, true); }
			set	{ SetValue((int)OutletSellerInformationFieldIndex.ReceiveOrderConfirmationNotifications, value); }
		}

		/// <summary>Gets the EntityCollection with the related entities of type 'OutletEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(OutletEntity))]
		public virtual EntityCollection<OutletEntity> OutletCollection { get { return GetOrCreateEntityCollection<OutletEntity, OutletEntityFactory>("OutletSellerInformation", true, false, ref _outletCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'ReceiptEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(ReceiptEntity))]
		public virtual EntityCollection<ReceiptEntity> ReceiptCollection { get { return GetOrCreateEntityCollection<ReceiptEntity, ReceiptEntityFactory>("OutletSellerInformation", true, false, ref _receiptCollection); } }


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen
{
	public enum OutletSellerInformationFieldIndex
	{
		///<summary>OutletSellerInformationId. </summary>
		OutletSellerInformationId,
		///<summary>OutletId. </summary>
		OutletId,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>SellerName. </summary>
		SellerName,
		///<summary>Address. </summary>
		Address,
		///<summary>Phonenumber. </summary>
		Phonenumber,
		///<summary>Email. </summary>
		Email,
		///<summary>VatNumber. </summary>
		VatNumber,
		///<summary>ReceiptReceiversEmail. </summary>
		ReceiptReceiversEmail,
		///<summary>ReceiptReceiversSms. </summary>
		ReceiptReceiversSms,
		///<summary>SmsOriginator. </summary>
		SmsOriginator,
		///<summary>TaxBreakdown. </summary>
		TaxBreakdown,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>ReceiveOrderReceiptNotifications. </summary>
		ReceiveOrderReceiptNotifications,
		///<summary>ReceiveOrderConfirmationNotifications. </summary>
		ReceiveOrderConfirmationNotifications,
		/// <summary></summary>
		AmountOfFields
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: OutletSellerInformation. </summary>
	public partial class OutletSellerInformationRelations: RelationFactory
	{
		#region Custom Relation code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomRelationCode
        private const ReferentialConstraintDeleteRule DeleteRuleForOutletEntityUsingOutletSellerInformationId = ReferentialConstraintDeleteRule.Cascade;

        private const ReferentialConstraintDeleteRule DeleteRuleForReceiptEntityUsingOutletSellerInformationId = ReferentialConstraintDeleteRule.Cascade;
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		
		// private const ReferentialConstraintDeleteRule DeleteRuleForOutletEntityUsingOutletSellerInformationId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForReceiptEntityUsingOutletSellerInformationId = ReferentialConstraintDeleteRule


		/// <summary>Gets all relations and delete rules of the OutletSellerInformationEntity as a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects.</summary>
		/// <returns>a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects</returns>
		public virtual Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> GetRelationsAndDeleteRules()
		{
			Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> toReturn = new Dictionary<IEntityRelation, ReferentialConstraintDeleteRule>();
			toReturn.Add(this.OutletEntityUsingOutletSellerInformationId, OutletSellerInformationRelations.DeleteRuleForOutletEntityUsingOutletSellerInformationId);
			toReturn.Add(this.ReceiptEntityUsingOutletSellerInformationId, OutletSellerInformationRelations.DeleteRuleForReceiptEntityUsingOutletSellerInformationId);
			return toReturn;
		}		
	
	
		/// <summary>Returns a new IEntityRelation object, between OutletSellerInformationEntity and OutletEntity over the 1:n relation they have, using the relation between the fields: OutletSellerInformation.OutletSellerInformationId - Outlet.OutletSellerInformationId</summary>
		public virtual IEntityRelation OutletEntityUsingOutletSellerInformationId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "OutletCollection", true, new[] { OutletSellerInformationFields.OutletSellerInformationId, OutletFields.OutletSellerInformationId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between OutletSellerInformationEntity and ReceiptEntity over the 1:n relation they have, using the relation between the fields: OutletSellerInformation.OutletSellerInformationId - Receipt.OutletSellerInformationId</summary>
		public virtual IEntityRelation ReceiptEntityUsingOutletSellerInformationId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "ReceiptCollection", true, new[] { OutletSellerInformationFields.OutletSellerInformationId, ReceiptFields.OutletSellerInformationId }); }
		}

	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticOutletSellerInformationRelations
	{
		internal static readonly IEntityRelation OutletEntityUsingOutletSellerInformationIdStatic = new OutletSellerInformationRelations().OutletEntityUsingOutletSellerInformationId;
		internal static readonly IEntityRelation ReceiptEntityUsingOutletSellerInformationIdStatic = new OutletSellerInformationRelations().ReceiptEntityUsingOutletSellerInformationId;

		/// <summary>CTor</summary>
		static StaticOutletSellerInformationRelations() { }
	}
}

