﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 5.8
// Code is generated on: 
// Code is generated using templates: Crave.TemplateBindings
// Templates vendor: Crave Interactive
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.FactoryClasses;
using Crave.Ordering.Shared.Data.LLBLGen.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

// ReSharper disable All
namespace Crave.Ordering.Shared.Data.LLBLGen.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'AlterationProduct'.<br/><br/></summary>
	[Serializable]
	public partial class AlterationProductEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END
	
	{
		private AlterationEntity _alteration;
		private ProductEntity _product;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		private static AlterationProductEntityStaticMetaData _staticMetaData = new AlterationProductEntityStaticMetaData();
		private static AlterationProductRelations _relationsFactory = new AlterationProductRelations();		

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Alteration</summary>
			public static readonly string Alteration = "Alteration";
			/// <summary>Member name Product</summary>
			public static readonly string Product = "Product";
		}

		/// <summary>Static meta-data storage for navigator related information</summary>
		protected class AlterationProductEntityStaticMetaData : EntityStaticMetaDataBase
		{
			public AlterationProductEntityStaticMetaData()
			{
				SetEntityCoreInfo("AlterationProductEntity", InheritanceHierarchyType.None, false, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.AlterationProductEntity, typeof(AlterationProductEntity), typeof(AlterationProductEntityFactory), false);
				AddNavigatorMetaData<AlterationProductEntity, AlterationEntity>("Alteration", "AlterationProductCollection", (a, b) => a._alteration = b, a => a._alteration, (a, b) => a.Alteration = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticAlterationProductRelations.AlterationEntityUsingAlterationIdStatic, ()=>new AlterationProductRelations().AlterationEntityUsingAlterationId, null, new int[] { (int)AlterationProductFieldIndex.AlterationId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.AlterationEntity);
				AddNavigatorMetaData<AlterationProductEntity, ProductEntity>("Product", "AlterationProductCollection", (a, b) => a._product = b, a => a._product, (a, b) => a.Product = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticAlterationProductRelations.ProductEntityUsingProductIdStatic, ()=>new AlterationProductRelations().ProductEntityUsingProductId, null, new int[] { (int)AlterationProductFieldIndex.ProductId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ProductEntity);
			}
		}

		/// <summary>Static ctor</summary>
		static AlterationProductEntity()
		{
		}

		/// <summary> CTor</summary>
		public AlterationProductEntity()
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public AlterationProductEntity(IEntityFields2 fields)
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this AlterationProductEntity</param>
		public AlterationProductEntity(IValidator validator)
		{
			InitClassEmpty(validator, null);
		}

		/// <summary> CTor</summary>
		/// <param name="alterationProductId">PK value for AlterationProduct which data should be fetched into this AlterationProduct object</param>
		public AlterationProductEntity(System.Int32 alterationProductId) : this(alterationProductId, null)
		{
		}

		/// <summary> CTor</summary>
		/// <param name="alterationProductId">PK value for AlterationProduct which data should be fetched into this AlterationProduct object</param>
		/// <param name="validator">The custom validator object for this AlterationProductEntity</param>
		public AlterationProductEntity(System.Int32 alterationProductId, IValidator validator)
		{
			InitClassEmpty(validator, null);
			this.AlterationProductId = alterationProductId;
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected AlterationProductEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary>Method which will construct a filter (predicate expression) for the unique constraint defined on the fields: AlterationId , ProductId .</summary>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public IPredicateExpression ConstructFilterForUCAlterationIdProductId()
		{
			var filter = new PredicateExpression();
			filter.Add(Crave.Ordering.Shared.Data.LLBLGen.HelperClasses.AlterationProductFields.AlterationId == this.Fields.GetCurrentValue((int)AlterationProductFieldIndex.AlterationId));
			filter.Add(Crave.Ordering.Shared.Data.LLBLGen.HelperClasses.AlterationProductFields.ProductId == this.Fields.GetCurrentValue((int)AlterationProductFieldIndex.ProductId));
 			return filter;
		}

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Alteration' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoAlteration() { return CreateRelationInfoForNavigator("Alteration"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Product' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoProduct() { return CreateRelationInfoForNavigator("Product"); }
		
		/// <inheritdoc/>
		protected override EntityStaticMetaDataBase GetEntityStaticMetaData() {	return _staticMetaData; }

		/// <summary>Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		/// <summary>Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this AlterationProductEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary>The relations object holding all relations of this entity with other entity classes.</summary>
		public static AlterationProductRelations Relations { get { return _relationsFactory; } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Alteration' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathAlterationEntity { get { return _staticMetaData.GetPrefetchPathElement("Alteration", CommonEntityBase.CreateEntityCollection<AlterationEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Product' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathProductEntity { get { return _staticMetaData.GetPrefetchPathElement("Product", CommonEntityBase.CreateEntityCollection<ProductEntity>()); } }

		/// <summary>The AlterationProductId property of the Entity AlterationProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AlterationProduct"."AlterationProductId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 AlterationProductId
		{
			get { return (System.Int32)GetValue((int)AlterationProductFieldIndex.AlterationProductId, true); }
			set { SetValue((int)AlterationProductFieldIndex.AlterationProductId, value); }		}

		/// <summary>The ParentCompanyId property of the Entity AlterationProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AlterationProduct"."ParentCompanyId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ParentCompanyId
		{
			get { return (Nullable<System.Int32>)GetValue((int)AlterationProductFieldIndex.ParentCompanyId, false); }
			set	{ SetValue((int)AlterationProductFieldIndex.ParentCompanyId, value); }
		}

		/// <summary>The AlterationId property of the Entity AlterationProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AlterationProduct"."AlterationId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 AlterationId
		{
			get { return (System.Int32)GetValue((int)AlterationProductFieldIndex.AlterationId, true); }
			set	{ SetValue((int)AlterationProductFieldIndex.AlterationId, value); }
		}

		/// <summary>The ProductId property of the Entity AlterationProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AlterationProduct"."ProductId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ProductId
		{
			get { return (System.Int32)GetValue((int)AlterationProductFieldIndex.ProductId, true); }
			set	{ SetValue((int)AlterationProductFieldIndex.ProductId, value); }
		}

		/// <summary>The SortOrder property of the Entity AlterationProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AlterationProduct"."SortOrder".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 SortOrder
		{
			get { return (System.Int32)GetValue((int)AlterationProductFieldIndex.SortOrder, true); }
			set	{ SetValue((int)AlterationProductFieldIndex.SortOrder, value); }
		}

		/// <summary>The Visible property of the Entity AlterationProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AlterationProduct"."Visible".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean Visible
		{
			get { return (System.Boolean)GetValue((int)AlterationProductFieldIndex.Visible, true); }
			set	{ SetValue((int)AlterationProductFieldIndex.Visible, value); }
		}

		/// <summary>The CreatedUTC property of the Entity AlterationProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AlterationProduct"."CreatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)AlterationProductFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)AlterationProductFieldIndex.CreatedUTC, value); }
		}

		/// <summary>The CreatedBy property of the Entity AlterationProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AlterationProduct"."CreatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CreatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)AlterationProductFieldIndex.CreatedBy, false); }
			set	{ SetValue((int)AlterationProductFieldIndex.CreatedBy, value); }
		}

		/// <summary>The UpdatedUTC property of the Entity AlterationProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AlterationProduct"."UpdatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)AlterationProductFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)AlterationProductFieldIndex.UpdatedUTC, value); }
		}

		/// <summary>The UpdatedBy property of the Entity AlterationProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AlterationProduct"."UpdatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UpdatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)AlterationProductFieldIndex.UpdatedBy, false); }
			set	{ SetValue((int)AlterationProductFieldIndex.UpdatedBy, value); }
		}

		/// <summary>Gets / sets related entity of type 'AlterationEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual AlterationEntity Alteration
		{
			get { return _alteration; }
			set { SetSingleRelatedEntityNavigator(value, "Alteration"); }
		}

		/// <summary>Gets / sets related entity of type 'ProductEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual ProductEntity Product
		{
			get { return _product; }
			set { SetSingleRelatedEntityNavigator(value, "Product"); }
		}


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen
{
	public enum AlterationProductFieldIndex
	{
		///<summary>AlterationProductId. </summary>
		AlterationProductId,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>AlterationId. </summary>
		AlterationId,
		///<summary>ProductId. </summary>
		ProductId,
		///<summary>SortOrder. </summary>
		SortOrder,
		///<summary>Visible. </summary>
		Visible,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		/// <summary></summary>
		AmountOfFields
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: AlterationProduct. </summary>
	public partial class AlterationProductRelations: RelationFactory
	{
		#region Custom Relation code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomRelationCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		


		/// <summary>Gets all relations and delete rules of the AlterationProductEntity as a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects.</summary>
		/// <returns>a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects</returns>
		public virtual Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> GetRelationsAndDeleteRules()
		{
			Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> toReturn = new Dictionary<IEntityRelation, ReferentialConstraintDeleteRule>();
			return toReturn;
		}		
	
	

		/// <summary>Returns a new IEntityRelation object, between AlterationProductEntity and AlterationEntity over the m:1 relation they have, using the relation between the fields: AlterationProduct.AlterationId - Alteration.AlterationId</summary>
		public virtual IEntityRelation AlterationEntityUsingAlterationId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Alteration", false, new[] { AlterationFields.AlterationId, AlterationProductFields.AlterationId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between AlterationProductEntity and ProductEntity over the m:1 relation they have, using the relation between the fields: AlterationProduct.ProductId - Product.ProductId</summary>
		public virtual IEntityRelation ProductEntityUsingProductId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Product", false, new[] { ProductFields.ProductId, AlterationProductFields.ProductId }); }
		}

	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticAlterationProductRelations
	{
		internal static readonly IEntityRelation AlterationEntityUsingAlterationIdStatic = new AlterationProductRelations().AlterationEntityUsingAlterationId;
		internal static readonly IEntityRelation ProductEntityUsingProductIdStatic = new AlterationProductRelations().ProductEntityUsingProductId;

		/// <summary>CTor</summary>
		static StaticAlterationProductRelations() { }
	}
}

