﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 5.8
// Code is generated on: 
// Code is generated using templates: Crave.TemplateBindings
// Templates vendor: Crave Interactive
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.FactoryClasses;
using Crave.Ordering.Shared.Data.LLBLGen.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

// ReSharper disable All
namespace Crave.Ordering.Shared.Data.LLBLGen.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'ProductCategoryTag'.<br/><br/></summary>
	[Serializable]
	public partial class ProductCategoryTagEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END
	
	{
		private EntityCollection<OrderitemTagEntity> _orderitemTagCollection;
		private CategoryEntity _category;
		private ProductCategoryEntity _productCategory;
		private TagEntity _tag;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		private static ProductCategoryTagEntityStaticMetaData _staticMetaData = new ProductCategoryTagEntityStaticMetaData();
		private static ProductCategoryTagRelations _relationsFactory = new ProductCategoryTagRelations();		

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Category</summary>
			public static readonly string Category = "Category";
			/// <summary>Member name ProductCategory</summary>
			public static readonly string ProductCategory = "ProductCategory";
			/// <summary>Member name Tag</summary>
			public static readonly string Tag = "Tag";
			/// <summary>Member name OrderitemTagCollection</summary>
			public static readonly string OrderitemTagCollection = "OrderitemTagCollection";
		}

		/// <summary>Static meta-data storage for navigator related information</summary>
		protected class ProductCategoryTagEntityStaticMetaData : EntityStaticMetaDataBase
		{
			public ProductCategoryTagEntityStaticMetaData()
			{
				SetEntityCoreInfo("ProductCategoryTagEntity", InheritanceHierarchyType.None, false, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ProductCategoryTagEntity, typeof(ProductCategoryTagEntity), typeof(ProductCategoryTagEntityFactory), false);
				AddNavigatorMetaData<ProductCategoryTagEntity, EntityCollection<OrderitemTagEntity>>("OrderitemTagCollection", a => a._orderitemTagCollection, (a, b) => a._orderitemTagCollection = b, a => a.OrderitemTagCollection, () => new ProductCategoryTagRelations().OrderitemTagEntityUsingProductCategoryTagId, typeof(OrderitemTagEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.OrderitemTagEntity);
				AddNavigatorMetaData<ProductCategoryTagEntity, CategoryEntity>("Category", "ProductCategoryTagCollection", (a, b) => a._category = b, a => a._category, (a, b) => a.Category = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticProductCategoryTagRelations.CategoryEntityUsingCategoryIdStatic, ()=>new ProductCategoryTagRelations().CategoryEntityUsingCategoryId, null, new int[] { (int)ProductCategoryTagFieldIndex.CategoryId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.CategoryEntity);
				AddNavigatorMetaData<ProductCategoryTagEntity, ProductCategoryEntity>("ProductCategory", "ProductCategoryTagCollection", (a, b) => a._productCategory = b, a => a._productCategory, (a, b) => a.ProductCategory = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticProductCategoryTagRelations.ProductCategoryEntityUsingProductCategoryIdStatic, ()=>new ProductCategoryTagRelations().ProductCategoryEntityUsingProductCategoryId, null, new int[] { (int)ProductCategoryTagFieldIndex.ProductCategoryId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ProductCategoryEntity);
				AddNavigatorMetaData<ProductCategoryTagEntity, TagEntity>("Tag", "ProductCategoryTagCollection", (a, b) => a._tag = b, a => a._tag, (a, b) => a.Tag = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticProductCategoryTagRelations.TagEntityUsingTagIdStatic, ()=>new ProductCategoryTagRelations().TagEntityUsingTagId, null, new int[] { (int)ProductCategoryTagFieldIndex.TagId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.TagEntity);
			}
		}

		/// <summary>Static ctor</summary>
		static ProductCategoryTagEntity()
		{
		}

		/// <summary> CTor</summary>
		public ProductCategoryTagEntity()
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public ProductCategoryTagEntity(IEntityFields2 fields)
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this ProductCategoryTagEntity</param>
		public ProductCategoryTagEntity(IValidator validator)
		{
			InitClassEmpty(validator, null);
		}

		/// <summary> CTor</summary>
		/// <param name="productCategoryTagId">PK value for ProductCategoryTag which data should be fetched into this ProductCategoryTag object</param>
		public ProductCategoryTagEntity(System.Int32 productCategoryTagId) : this(productCategoryTagId, null)
		{
		}

		/// <summary> CTor</summary>
		/// <param name="productCategoryTagId">PK value for ProductCategoryTag which data should be fetched into this ProductCategoryTag object</param>
		/// <param name="validator">The custom validator object for this ProductCategoryTagEntity</param>
		public ProductCategoryTagEntity(System.Int32 productCategoryTagId, IValidator validator)
		{
			InitClassEmpty(validator, null);
			this.ProductCategoryTagId = productCategoryTagId;
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ProductCategoryTagEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'OrderitemTag' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoOrderitemTagCollection() { return CreateRelationInfoForNavigator("OrderitemTagCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Category' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoCategory() { return CreateRelationInfoForNavigator("Category"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'ProductCategory' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoProductCategory() { return CreateRelationInfoForNavigator("ProductCategory"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Tag' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoTag() { return CreateRelationInfoForNavigator("Tag"); }
		
		/// <inheritdoc/>
		protected override EntityStaticMetaDataBase GetEntityStaticMetaData() {	return _staticMetaData; }

		/// <summary>Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		/// <summary>Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this ProductCategoryTagEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary>The relations object holding all relations of this entity with other entity classes.</summary>
		public static ProductCategoryTagRelations Relations { get { return _relationsFactory; } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'OrderitemTag' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathOrderitemTagCollection { get { return _staticMetaData.GetPrefetchPathElement("OrderitemTagCollection", CommonEntityBase.CreateEntityCollection<OrderitemTagEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Category' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathCategoryEntity { get { return _staticMetaData.GetPrefetchPathElement("Category", CommonEntityBase.CreateEntityCollection<CategoryEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'ProductCategory' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathProductCategoryEntity { get { return _staticMetaData.GetPrefetchPathElement("ProductCategory", CommonEntityBase.CreateEntityCollection<ProductCategoryEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Tag' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathTagEntity { get { return _staticMetaData.GetPrefetchPathElement("Tag", CommonEntityBase.CreateEntityCollection<TagEntity>()); } }

		/// <summary>The ProductCategoryTagId property of the Entity ProductCategoryTag<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ProductCategoryTag"."ProductCategoryTagId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 ProductCategoryTagId
		{
			get { return (System.Int32)GetValue((int)ProductCategoryTagFieldIndex.ProductCategoryTagId, true); }
			set { SetValue((int)ProductCategoryTagFieldIndex.ProductCategoryTagId, value); }		}

		/// <summary>The ProductCategoryId property of the Entity ProductCategoryTag<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ProductCategoryTag"."ProductCategoryId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ProductCategoryId
		{
			get { return (System.Int32)GetValue((int)ProductCategoryTagFieldIndex.ProductCategoryId, true); }
			set	{ SetValue((int)ProductCategoryTagFieldIndex.ProductCategoryId, value); }
		}

		/// <summary>The CategoryId property of the Entity ProductCategoryTag<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ProductCategoryTag"."CategoryId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CategoryId
		{
			get { return (System.Int32)GetValue((int)ProductCategoryTagFieldIndex.CategoryId, true); }
			set	{ SetValue((int)ProductCategoryTagFieldIndex.CategoryId, value); }
		}

		/// <summary>The TagId property of the Entity ProductCategoryTag<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ProductCategoryTag"."TagId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 TagId
		{
			get { return (System.Int32)GetValue((int)ProductCategoryTagFieldIndex.TagId, true); }
			set	{ SetValue((int)ProductCategoryTagFieldIndex.TagId, value); }
		}

		/// <summary>The ParentCompanyId property of the Entity ProductCategoryTag<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ProductCategoryTag"."ParentCompanyId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ParentCompanyId
		{
			get { return (Nullable<System.Int32>)GetValue((int)ProductCategoryTagFieldIndex.ParentCompanyId, false); }
			set	{ SetValue((int)ProductCategoryTagFieldIndex.ParentCompanyId, value); }
		}

		/// <summary>The CreatedUTC property of the Entity ProductCategoryTag<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ProductCategoryTag"."CreatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime CreatedUTC
		{
			get { return (System.DateTime)GetValue((int)ProductCategoryTagFieldIndex.CreatedUTC, true); }
			set	{ SetValue((int)ProductCategoryTagFieldIndex.CreatedUTC, value); }
		}

		/// <summary>The UpdatedUTC property of the Entity ProductCategoryTag<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ProductCategoryTag"."UpdatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ProductCategoryTagFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)ProductCategoryTagFieldIndex.UpdatedUTC, value); }
		}

		/// <summary>Gets the EntityCollection with the related entities of type 'OrderitemTagEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(OrderitemTagEntity))]
		public virtual EntityCollection<OrderitemTagEntity> OrderitemTagCollection { get { return GetOrCreateEntityCollection<OrderitemTagEntity, OrderitemTagEntityFactory>("ProductCategoryTag", true, false, ref _orderitemTagCollection); } }

		/// <summary>Gets / sets related entity of type 'CategoryEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual CategoryEntity Category
		{
			get { return _category; }
			set { SetSingleRelatedEntityNavigator(value, "Category"); }
		}

		/// <summary>Gets / sets related entity of type 'ProductCategoryEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual ProductCategoryEntity ProductCategory
		{
			get { return _productCategory; }
			set { SetSingleRelatedEntityNavigator(value, "ProductCategory"); }
		}

		/// <summary>Gets / sets related entity of type 'TagEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual TagEntity Tag
		{
			get { return _tag; }
			set { SetSingleRelatedEntityNavigator(value, "Tag"); }
		}


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen
{
	public enum ProductCategoryTagFieldIndex
	{
		///<summary>ProductCategoryTagId. </summary>
		ProductCategoryTagId,
		///<summary>ProductCategoryId. </summary>
		ProductCategoryId,
		///<summary>CategoryId. </summary>
		CategoryId,
		///<summary>TagId. </summary>
		TagId,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: ProductCategoryTag. </summary>
	public partial class ProductCategoryTagRelations: RelationFactory
	{
		#region Custom Relation code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomRelationCode
        private const ReferentialConstraintDeleteRule DeleteRuleForOrderitemTagEntityUsingProductCategoryTagId = ReferentialConstraintDeleteRule.NoAction;
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		
		// private const ReferentialConstraintDeleteRule DeleteRuleForOrderitemTagEntityUsingProductCategoryTagId = ReferentialConstraintDeleteRule


		/// <summary>Gets all relations and delete rules of the ProductCategoryTagEntity as a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects.</summary>
		/// <returns>a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects</returns>
		public virtual Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> GetRelationsAndDeleteRules()
		{
			Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> toReturn = new Dictionary<IEntityRelation, ReferentialConstraintDeleteRule>();
			toReturn.Add(this.OrderitemTagEntityUsingProductCategoryTagId, ProductCategoryTagRelations.DeleteRuleForOrderitemTagEntityUsingProductCategoryTagId);
			return toReturn;
		}		
	
	
		/// <summary>Returns a new IEntityRelation object, between ProductCategoryTagEntity and OrderitemTagEntity over the 1:n relation they have, using the relation between the fields: ProductCategoryTag.ProductCategoryTagId - OrderitemTag.ProductCategoryTagId</summary>
		public virtual IEntityRelation OrderitemTagEntityUsingProductCategoryTagId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "OrderitemTagCollection", true, new[] { ProductCategoryTagFields.ProductCategoryTagId, OrderitemTagFields.ProductCategoryTagId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between ProductCategoryTagEntity and CategoryEntity over the m:1 relation they have, using the relation between the fields: ProductCategoryTag.CategoryId - Category.CategoryId</summary>
		public virtual IEntityRelation CategoryEntityUsingCategoryId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Category", false, new[] { CategoryFields.CategoryId, ProductCategoryTagFields.CategoryId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between ProductCategoryTagEntity and ProductCategoryEntity over the m:1 relation they have, using the relation between the fields: ProductCategoryTag.ProductCategoryId - ProductCategory.ProductCategoryId</summary>
		public virtual IEntityRelation ProductCategoryEntityUsingProductCategoryId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "ProductCategory", false, new[] { ProductCategoryFields.ProductCategoryId, ProductCategoryTagFields.ProductCategoryId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between ProductCategoryTagEntity and TagEntity over the m:1 relation they have, using the relation between the fields: ProductCategoryTag.TagId - Tag.TagId</summary>
		public virtual IEntityRelation TagEntityUsingTagId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Tag", false, new[] { TagFields.TagId, ProductCategoryTagFields.TagId }); }
		}

	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticProductCategoryTagRelations
	{
		internal static readonly IEntityRelation OrderitemTagEntityUsingProductCategoryTagIdStatic = new ProductCategoryTagRelations().OrderitemTagEntityUsingProductCategoryTagId;
		internal static readonly IEntityRelation CategoryEntityUsingCategoryIdStatic = new ProductCategoryTagRelations().CategoryEntityUsingCategoryId;
		internal static readonly IEntityRelation ProductCategoryEntityUsingProductCategoryIdStatic = new ProductCategoryTagRelations().ProductCategoryEntityUsingProductCategoryId;
		internal static readonly IEntityRelation TagEntityUsingTagIdStatic = new ProductCategoryTagRelations().TagEntityUsingTagId;

		/// <summary>CTor</summary>
		static StaticProductCategoryTagRelations() { }
	}
}

