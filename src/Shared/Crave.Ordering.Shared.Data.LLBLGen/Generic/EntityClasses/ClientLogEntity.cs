﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 5.8
// Code is generated on: 
// Code is generated using templates: Crave.TemplateBindings
// Templates vendor: Crave Interactive
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.FactoryClasses;
using Crave.Ordering.Shared.Data.LLBLGen.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

// ReSharper disable All
namespace Crave.Ordering.Shared.Data.LLBLGen.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'ClientLog'.<br/><br/></summary>
	[Serializable]
	public partial class ClientLogEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END
	
	{
		private ClientEntity _client;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		private static ClientLogEntityStaticMetaData _staticMetaData = new ClientLogEntityStaticMetaData();
		private static ClientLogRelations _relationsFactory = new ClientLogRelations();		

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Client</summary>
			public static readonly string Client = "Client";
		}

		/// <summary>Static meta-data storage for navigator related information</summary>
		protected class ClientLogEntityStaticMetaData : EntityStaticMetaDataBase
		{
			public ClientLogEntityStaticMetaData()
			{
				SetEntityCoreInfo("ClientLogEntity", InheritanceHierarchyType.None, false, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ClientLogEntity, typeof(ClientLogEntity), typeof(ClientLogEntityFactory), false);
				AddNavigatorMetaData<ClientLogEntity, ClientEntity>("Client", "ClientLogCollection", (a, b) => a._client = b, a => a._client, (a, b) => a.Client = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticClientLogRelations.ClientEntityUsingClientIdStatic, ()=>new ClientLogRelations().ClientEntityUsingClientId, null, new int[] { (int)ClientLogFieldIndex.ClientId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ClientEntity);
			}
		}

		/// <summary>Static ctor</summary>
		static ClientLogEntity()
		{
		}

		/// <summary> CTor</summary>
		public ClientLogEntity()
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public ClientLogEntity(IEntityFields2 fields)
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this ClientLogEntity</param>
		public ClientLogEntity(IValidator validator)
		{
			InitClassEmpty(validator, null);
		}

		/// <summary> CTor</summary>
		/// <param name="clientLogId">PK value for ClientLog which data should be fetched into this ClientLog object</param>
		public ClientLogEntity(System.Int32 clientLogId) : this(clientLogId, null)
		{
		}

		/// <summary> CTor</summary>
		/// <param name="clientLogId">PK value for ClientLog which data should be fetched into this ClientLog object</param>
		/// <param name="validator">The custom validator object for this ClientLogEntity</param>
		public ClientLogEntity(System.Int32 clientLogId, IValidator validator)
		{
			InitClassEmpty(validator, null);
			this.ClientLogId = clientLogId;
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ClientLogEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Client' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoClient() { return CreateRelationInfoForNavigator("Client"); }
		
		/// <inheritdoc/>
		protected override EntityStaticMetaDataBase GetEntityStaticMetaData() {	return _staticMetaData; }

		/// <summary>Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		/// <summary>Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this ClientLogEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary>The relations object holding all relations of this entity with other entity classes.</summary>
		public static ClientLogRelations Relations { get { return _relationsFactory; } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Client' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathClientEntity { get { return _staticMetaData.GetPrefetchPathElement("Client", CommonEntityBase.CreateEntityCollection<ClientEntity>()); } }

		/// <summary>The ClientLogId property of the Entity ClientLog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientLog"."ClientLogId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 ClientLogId
		{
			get { return (System.Int32)GetValue((int)ClientLogFieldIndex.ClientLogId, true); }
			set { SetValue((int)ClientLogFieldIndex.ClientLogId, value); }		}

		/// <summary>The ParentCompanyId property of the Entity ClientLog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientLog"."ParentCompanyId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ParentCompanyId
		{
			get { return (Nullable<System.Int32>)GetValue((int)ClientLogFieldIndex.ParentCompanyId, false); }
			set	{ SetValue((int)ClientLogFieldIndex.ParentCompanyId, value); }
		}

		/// <summary>The ClientId property of the Entity ClientLog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientLog"."ClientId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ClientId
		{
			get { return (Nullable<System.Int32>)GetValue((int)ClientLogFieldIndex.ClientId, false); }
			set	{ SetValue((int)ClientLogFieldIndex.ClientId, value); }
		}

		/// <summary>The Type property of the Entity ClientLog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientLog"."Type".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Crave.Enums.ClientLogType Type
		{
			get { return (Crave.Enums.ClientLogType)GetValue((int)ClientLogFieldIndex.Type, true); }
			set	{ SetValue((int)ClientLogFieldIndex.Type, value); }
		}

		/// <summary>The TypeText property of the Entity ClientLog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientLog"."TypeText".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 250.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String TypeText
		{
			get { return (System.String)GetValue((int)ClientLogFieldIndex.TypeText, true); }
			set	{ SetValue((int)ClientLogFieldIndex.TypeText, value); }
		}

		/// <summary>The Message property of the Entity ClientLog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientLog"."Message".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Message
		{
			get { return (System.String)GetValue((int)ClientLogFieldIndex.Message, true); }
			set	{ SetValue((int)ClientLogFieldIndex.Message, value); }
		}

		/// <summary>The FromStatus property of the Entity ClientLog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientLog"."FromStatus".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<Crave.Enums.ClientStatus> FromStatus
		{
			get { return (Nullable<Crave.Enums.ClientStatus>)GetValue((int)ClientLogFieldIndex.FromStatus, false); }
			set	{ SetValue((int)ClientLogFieldIndex.FromStatus, value); }
		}

		/// <summary>The FromStatusText property of the Entity ClientLog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientLog"."FromStatusText".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 250.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FromStatusText
		{
			get { return (System.String)GetValue((int)ClientLogFieldIndex.FromStatusText, true); }
			set	{ SetValue((int)ClientLogFieldIndex.FromStatusText, value); }
		}

		/// <summary>The ToStatus property of the Entity ClientLog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientLog"."ToStatus".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<Crave.Enums.ClientStatus> ToStatus
		{
			get { return (Nullable<Crave.Enums.ClientStatus>)GetValue((int)ClientLogFieldIndex.ToStatus, false); }
			set	{ SetValue((int)ClientLogFieldIndex.ToStatus, value); }
		}

		/// <summary>The ToStatusText property of the Entity ClientLog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientLog"."ToStatusText".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 250.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ToStatusText
		{
			get { return (System.String)GetValue((int)ClientLogFieldIndex.ToStatusText, true); }
			set	{ SetValue((int)ClientLogFieldIndex.ToStatusText, value); }
		}

		/// <summary>The FromOperationMode property of the Entity ClientLog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientLog"."FromOperationMode".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<Crave.Enums.ClientOperationMode> FromOperationMode
		{
			get { return (Nullable<Crave.Enums.ClientOperationMode>)GetValue((int)ClientLogFieldIndex.FromOperationMode, false); }
			set	{ SetValue((int)ClientLogFieldIndex.FromOperationMode, value); }
		}

		/// <summary>The FromOperationModeText property of the Entity ClientLog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientLog"."FromOperationModeText".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 250.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FromOperationModeText
		{
			get { return (System.String)GetValue((int)ClientLogFieldIndex.FromOperationModeText, true); }
			set	{ SetValue((int)ClientLogFieldIndex.FromOperationModeText, value); }
		}

		/// <summary>The ToOperationMode property of the Entity ClientLog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientLog"."ToOperationMode".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<Crave.Enums.ClientOperationMode> ToOperationMode
		{
			get { return (Nullable<Crave.Enums.ClientOperationMode>)GetValue((int)ClientLogFieldIndex.ToOperationMode, false); }
			set	{ SetValue((int)ClientLogFieldIndex.ToOperationMode, value); }
		}

		/// <summary>The ToOperationModeText property of the Entity ClientLog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientLog"."ToOperationModeText".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 250.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ToOperationModeText
		{
			get { return (System.String)GetValue((int)ClientLogFieldIndex.ToOperationModeText, true); }
			set	{ SetValue((int)ClientLogFieldIndex.ToOperationModeText, value); }
		}

		/// <summary>The ClientLogFileId property of the Entity ClientLog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientLog"."ClientLogFileId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ClientLogFileId
		{
			get { return (Nullable<System.Int32>)GetValue((int)ClientLogFieldIndex.ClientLogFileId, false); }
			set	{ SetValue((int)ClientLogFieldIndex.ClientLogFileId, value); }
		}

		/// <summary>The CreatedBy property of the Entity ClientLog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientLog"."CreatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)ClientLogFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)ClientLogFieldIndex.CreatedBy, value); }
		}

		/// <summary>The UpdatedBy property of the Entity ClientLog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientLog"."UpdatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)ClientLogFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)ClientLogFieldIndex.UpdatedBy, value); }
		}

		/// <summary>The CreatedUTC property of the Entity ClientLog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientLog"."CreatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ClientLogFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)ClientLogFieldIndex.CreatedUTC, value); }
		}

		/// <summary>The UpdatedUTC property of the Entity ClientLog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientLog"."UpdatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ClientLogFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)ClientLogFieldIndex.UpdatedUTC, value); }
		}

		/// <summary>Gets / sets related entity of type 'ClientEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual ClientEntity Client
		{
			get { return _client; }
			set { SetSingleRelatedEntityNavigator(value, "Client"); }
		}


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen
{
	public enum ClientLogFieldIndex
	{
		///<summary>ClientLogId. </summary>
		ClientLogId,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>ClientId. </summary>
		ClientId,
		///<summary>Type. </summary>
		Type,
		///<summary>TypeText. </summary>
		TypeText,
		///<summary>Message. </summary>
		Message,
		///<summary>FromStatus. </summary>
		FromStatus,
		///<summary>FromStatusText. </summary>
		FromStatusText,
		///<summary>ToStatus. </summary>
		ToStatus,
		///<summary>ToStatusText. </summary>
		ToStatusText,
		///<summary>FromOperationMode. </summary>
		FromOperationMode,
		///<summary>FromOperationModeText. </summary>
		FromOperationModeText,
		///<summary>ToOperationMode. </summary>
		ToOperationMode,
		///<summary>ToOperationModeText. </summary>
		ToOperationModeText,
		///<summary>ClientLogFileId. </summary>
		ClientLogFileId,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: ClientLog. </summary>
	public partial class ClientLogRelations: RelationFactory
	{
		#region Custom Relation code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomRelationCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		


		/// <summary>Gets all relations and delete rules of the ClientLogEntity as a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects.</summary>
		/// <returns>a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects</returns>
		public virtual Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> GetRelationsAndDeleteRules()
		{
			Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> toReturn = new Dictionary<IEntityRelation, ReferentialConstraintDeleteRule>();
			return toReturn;
		}		
	
	

		/// <summary>Returns a new IEntityRelation object, between ClientLogEntity and ClientEntity over the m:1 relation they have, using the relation between the fields: ClientLog.ClientId - Client.ClientId</summary>
		public virtual IEntityRelation ClientEntityUsingClientId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Client", false, new[] { ClientFields.ClientId, ClientLogFields.ClientId }); }
		}

	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticClientLogRelations
	{
		internal static readonly IEntityRelation ClientEntityUsingClientIdStatic = new ClientLogRelations().ClientEntityUsingClientId;

		/// <summary>CTor</summary>
		static StaticClientLogRelations() { }
	}
}

