﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 5.8
// Code is generated on: 
// Code is generated using templates: Crave.TemplateBindings
// Templates vendor: Crave Interactive
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.FactoryClasses;
using Crave.Ordering.Shared.Data.LLBLGen.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

// ReSharper disable All
namespace Crave.Ordering.Shared.Data.LLBLGen.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'Deliverypointgroup'.<br/><br/></summary>
	[Serializable]
	public partial class DeliverypointgroupEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END
	
	{
		private EntityCollection<CheckoutMethodDeliverypointgroupEntity> _checkoutMethodDeliverypointgroupCollection;
		private EntityCollection<ClientEntity> _clientCollection;
		private EntityCollection<ClientConfigurationEntity> _clientConfigurationCollection;
		private EntityCollection<DeliverypointEntity> _deliverypointCollection;
		private EntityCollection<MediaEntity> _mediaCollection;
		private EntityCollection<NetmessageEntity> _netmessageCollection;
		private EntityCollection<ServiceMethodDeliverypointgroupEntity> _serviceMethodDeliverypointgroupCollection;
		private EntityCollection<TerminalEntity> _terminalCollection;
		private EntityCollection<ClientConfigurationEntity> _clientConfigurationCollectionViaDeliverypoint;
		private ClientConfigurationEntity _clientConfiguration;
		private CompanyEntity _company;
		private MenuEntity _menu;
		private PriceScheduleEntity _priceSchedule;
		private ProductEntity _product;
		private RouteEntity _emailDocumentRoute;
		private RouteEntity _orderNotesRoute;
		private RouteEntity _route;
		private RouteEntity _systemMessageRoute;
		private TerminalEntity _terminal;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		private static DeliverypointgroupEntityStaticMetaData _staticMetaData = new DeliverypointgroupEntityStaticMetaData();
		private static DeliverypointgroupRelations _relationsFactory = new DeliverypointgroupRelations();		

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name ClientConfiguration</summary>
			public static readonly string ClientConfiguration = "ClientConfiguration";
			/// <summary>Member name Company</summary>
			public static readonly string Company = "Company";
			/// <summary>Member name Menu</summary>
			public static readonly string Menu = "Menu";
			/// <summary>Member name PriceSchedule</summary>
			public static readonly string PriceSchedule = "PriceSchedule";
			/// <summary>Member name Product</summary>
			public static readonly string Product = "Product";
			/// <summary>Member name EmailDocumentRoute</summary>
			public static readonly string EmailDocumentRoute = "EmailDocumentRoute";
			/// <summary>Member name OrderNotesRoute</summary>
			public static readonly string OrderNotesRoute = "OrderNotesRoute";
			/// <summary>Member name Route</summary>
			public static readonly string Route = "Route";
			/// <summary>Member name SystemMessageRoute</summary>
			public static readonly string SystemMessageRoute = "SystemMessageRoute";
			/// <summary>Member name Terminal</summary>
			public static readonly string Terminal = "Terminal";
			/// <summary>Member name CheckoutMethodDeliverypointgroupCollection</summary>
			public static readonly string CheckoutMethodDeliverypointgroupCollection = "CheckoutMethodDeliverypointgroupCollection";
			/// <summary>Member name ClientCollection</summary>
			public static readonly string ClientCollection = "ClientCollection";
			/// <summary>Member name ClientConfigurationCollection</summary>
			public static readonly string ClientConfigurationCollection = "ClientConfigurationCollection";
			/// <summary>Member name DeliverypointCollection</summary>
			public static readonly string DeliverypointCollection = "DeliverypointCollection";
			/// <summary>Member name MediaCollection</summary>
			public static readonly string MediaCollection = "MediaCollection";
			/// <summary>Member name NetmessageCollection</summary>
			public static readonly string NetmessageCollection = "NetmessageCollection";
			/// <summary>Member name ServiceMethodDeliverypointgroupCollection</summary>
			public static readonly string ServiceMethodDeliverypointgroupCollection = "ServiceMethodDeliverypointgroupCollection";
			/// <summary>Member name TerminalCollection</summary>
			public static readonly string TerminalCollection = "TerminalCollection";
			/// <summary>Member name ClientConfigurationCollectionViaDeliverypoint</summary>
			public static readonly string ClientConfigurationCollectionViaDeliverypoint = "ClientConfigurationCollectionViaDeliverypoint";
		}

		/// <summary>Static meta-data storage for navigator related information</summary>
		protected class DeliverypointgroupEntityStaticMetaData : EntityStaticMetaDataBase
		{
			public DeliverypointgroupEntityStaticMetaData()
			{
				SetEntityCoreInfo("DeliverypointgroupEntity", InheritanceHierarchyType.None, false, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.DeliverypointgroupEntity, typeof(DeliverypointgroupEntity), typeof(DeliverypointgroupEntityFactory), false);
				AddNavigatorMetaData<DeliverypointgroupEntity, EntityCollection<CheckoutMethodDeliverypointgroupEntity>>("CheckoutMethodDeliverypointgroupCollection", a => a._checkoutMethodDeliverypointgroupCollection, (a, b) => a._checkoutMethodDeliverypointgroupCollection = b, a => a.CheckoutMethodDeliverypointgroupCollection, () => new DeliverypointgroupRelations().CheckoutMethodDeliverypointgroupEntityUsingDeliverypointgroupId, typeof(CheckoutMethodDeliverypointgroupEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.CheckoutMethodDeliverypointgroupEntity);
				AddNavigatorMetaData<DeliverypointgroupEntity, EntityCollection<ClientEntity>>("ClientCollection", a => a._clientCollection, (a, b) => a._clientCollection = b, a => a.ClientCollection, () => new DeliverypointgroupRelations().ClientEntityUsingDeliverypointgroupId, typeof(ClientEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ClientEntity);
				AddNavigatorMetaData<DeliverypointgroupEntity, EntityCollection<ClientConfigurationEntity>>("ClientConfigurationCollection", a => a._clientConfigurationCollection, (a, b) => a._clientConfigurationCollection = b, a => a.ClientConfigurationCollection, () => new DeliverypointgroupRelations().ClientConfigurationEntityUsingDeliverypointgroupId, typeof(ClientConfigurationEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ClientConfigurationEntity);
				AddNavigatorMetaData<DeliverypointgroupEntity, EntityCollection<DeliverypointEntity>>("DeliverypointCollection", a => a._deliverypointCollection, (a, b) => a._deliverypointCollection = b, a => a.DeliverypointCollection, () => new DeliverypointgroupRelations().DeliverypointEntityUsingDeliverypointgroupId, typeof(DeliverypointEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.DeliverypointEntity);
				AddNavigatorMetaData<DeliverypointgroupEntity, EntityCollection<MediaEntity>>("MediaCollection", a => a._mediaCollection, (a, b) => a._mediaCollection = b, a => a.MediaCollection, () => new DeliverypointgroupRelations().MediaEntityUsingDeliverypointgroupId, typeof(MediaEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.MediaEntity);
				AddNavigatorMetaData<DeliverypointgroupEntity, EntityCollection<NetmessageEntity>>("NetmessageCollection", a => a._netmessageCollection, (a, b) => a._netmessageCollection = b, a => a.NetmessageCollection, () => new DeliverypointgroupRelations().NetmessageEntityUsingReceiverDeliverypointgroupId, typeof(NetmessageEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.NetmessageEntity);
				AddNavigatorMetaData<DeliverypointgroupEntity, EntityCollection<ServiceMethodDeliverypointgroupEntity>>("ServiceMethodDeliverypointgroupCollection", a => a._serviceMethodDeliverypointgroupCollection, (a, b) => a._serviceMethodDeliverypointgroupCollection = b, a => a.ServiceMethodDeliverypointgroupCollection, () => new DeliverypointgroupRelations().ServiceMethodDeliverypointgroupEntityUsingDeliverypointgroupId, typeof(ServiceMethodDeliverypointgroupEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ServiceMethodDeliverypointgroupEntity);
				AddNavigatorMetaData<DeliverypointgroupEntity, EntityCollection<TerminalEntity>>("TerminalCollection", a => a._terminalCollection, (a, b) => a._terminalCollection = b, a => a.TerminalCollection, () => new DeliverypointgroupRelations().TerminalEntityUsingDeliverypointgroupId, typeof(TerminalEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.TerminalEntity);
				AddNavigatorMetaData<DeliverypointgroupEntity, ClientConfigurationEntity>("ClientConfiguration", "DeliverypointgroupCollection", (a, b) => a._clientConfiguration = b, a => a._clientConfiguration, (a, b) => a.ClientConfiguration = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticDeliverypointgroupRelations.ClientConfigurationEntityUsingClientConfigurationIdStatic, ()=>new DeliverypointgroupRelations().ClientConfigurationEntityUsingClientConfigurationId, null, new int[] { (int)DeliverypointgroupFieldIndex.ClientConfigurationId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ClientConfigurationEntity);
				AddNavigatorMetaData<DeliverypointgroupEntity, CompanyEntity>("Company", "DeliverypointgroupCollection", (a, b) => a._company = b, a => a._company, (a, b) => a.Company = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticDeliverypointgroupRelations.CompanyEntityUsingCompanyIdStatic, ()=>new DeliverypointgroupRelations().CompanyEntityUsingCompanyId, null, new int[] { (int)DeliverypointgroupFieldIndex.CompanyId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.CompanyEntity);
				AddNavigatorMetaData<DeliverypointgroupEntity, MenuEntity>("Menu", "DeliverypointgroupCollection", (a, b) => a._menu = b, a => a._menu, (a, b) => a.Menu = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticDeliverypointgroupRelations.MenuEntityUsingMenuIdStatic, ()=>new DeliverypointgroupRelations().MenuEntityUsingMenuId, null, new int[] { (int)DeliverypointgroupFieldIndex.MenuId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.MenuEntity);
				AddNavigatorMetaData<DeliverypointgroupEntity, PriceScheduleEntity>("PriceSchedule", "DeliverypointgroupCollection", (a, b) => a._priceSchedule = b, a => a._priceSchedule, (a, b) => a.PriceSchedule = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticDeliverypointgroupRelations.PriceScheduleEntityUsingPriceScheduleIdStatic, ()=>new DeliverypointgroupRelations().PriceScheduleEntityUsingPriceScheduleId, null, new int[] { (int)DeliverypointgroupFieldIndex.PriceScheduleId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.PriceScheduleEntity);
				AddNavigatorMetaData<DeliverypointgroupEntity, ProductEntity>("Product", "DeliverypointgroupCollection", (a, b) => a._product = b, a => a._product, (a, b) => a.Product = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticDeliverypointgroupRelations.ProductEntityUsingHotSOSBatteryLowProductIdStatic, ()=>new DeliverypointgroupRelations().ProductEntityUsingHotSOSBatteryLowProductId, null, new int[] { (int)DeliverypointgroupFieldIndex.HotSOSBatteryLowProductId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ProductEntity);
				AddNavigatorMetaData<DeliverypointgroupEntity, RouteEntity>("EmailDocumentRoute", "DeliverypointgroupCollection1", (a, b) => a._emailDocumentRoute = b, a => a._emailDocumentRoute, (a, b) => a.EmailDocumentRoute = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticDeliverypointgroupRelations.RouteEntityUsingEmailDocumentRouteIdStatic, ()=>new DeliverypointgroupRelations().RouteEntityUsingEmailDocumentRouteId, null, new int[] { (int)DeliverypointgroupFieldIndex.EmailDocumentRouteId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.RouteEntity);
				AddNavigatorMetaData<DeliverypointgroupEntity, RouteEntity>("OrderNotesRoute", "DeliverypointgroupCollection2", (a, b) => a._orderNotesRoute = b, a => a._orderNotesRoute, (a, b) => a.OrderNotesRoute = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticDeliverypointgroupRelations.RouteEntityUsingOrderNotesRouteIdStatic, ()=>new DeliverypointgroupRelations().RouteEntityUsingOrderNotesRouteId, null, new int[] { (int)DeliverypointgroupFieldIndex.OrderNotesRouteId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.RouteEntity);
				AddNavigatorMetaData<DeliverypointgroupEntity, RouteEntity>("Route", "DeliverypointgroupCollection", (a, b) => a._route = b, a => a._route, (a, b) => a.Route = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticDeliverypointgroupRelations.RouteEntityUsingRouteIdStatic, ()=>new DeliverypointgroupRelations().RouteEntityUsingRouteId, null, new int[] { (int)DeliverypointgroupFieldIndex.RouteId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.RouteEntity);
				AddNavigatorMetaData<DeliverypointgroupEntity, RouteEntity>("SystemMessageRoute", "DeliverypointgroupCollection3", (a, b) => a._systemMessageRoute = b, a => a._systemMessageRoute, (a, b) => a.SystemMessageRoute = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticDeliverypointgroupRelations.RouteEntityUsingSystemMessageRouteIdStatic, ()=>new DeliverypointgroupRelations().RouteEntityUsingSystemMessageRouteId, null, new int[] { (int)DeliverypointgroupFieldIndex.SystemMessageRouteId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.RouteEntity);
				AddNavigatorMetaData<DeliverypointgroupEntity, TerminalEntity>("Terminal", "DeliverypointgroupCollection", (a, b) => a._terminal = b, a => a._terminal, (a, b) => a.Terminal = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticDeliverypointgroupRelations.TerminalEntityUsingTerminalIdStatic, ()=>new DeliverypointgroupRelations().TerminalEntityUsingTerminalId, null, new int[] { (int)DeliverypointgroupFieldIndex.TerminalId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.TerminalEntity);
				AddNavigatorMetaData<DeliverypointgroupEntity, EntityCollection<ClientConfigurationEntity>>("ClientConfigurationCollectionViaDeliverypoint", a => a._clientConfigurationCollectionViaDeliverypoint, (a, b) => a._clientConfigurationCollectionViaDeliverypoint = b, a => a.ClientConfigurationCollectionViaDeliverypoint, () => new DeliverypointgroupRelations().DeliverypointEntityUsingDeliverypointgroupId, () => new DeliverypointRelations().ClientConfigurationEntityUsingClientConfigurationId, "DeliverypointgroupEntity__", "Deliverypoint_", typeof(ClientConfigurationEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ClientConfigurationEntity);
			}
		}

		/// <summary>Static ctor</summary>
		static DeliverypointgroupEntity()
		{
		}

		/// <summary> CTor</summary>
		public DeliverypointgroupEntity()
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public DeliverypointgroupEntity(IEntityFields2 fields)
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this DeliverypointgroupEntity</param>
		public DeliverypointgroupEntity(IValidator validator)
		{
			InitClassEmpty(validator, null);
		}

		/// <summary> CTor</summary>
		/// <param name="deliverypointgroupId">PK value for Deliverypointgroup which data should be fetched into this Deliverypointgroup object</param>
		public DeliverypointgroupEntity(System.Int32 deliverypointgroupId) : this(deliverypointgroupId, null)
		{
		}

		/// <summary> CTor</summary>
		/// <param name="deliverypointgroupId">PK value for Deliverypointgroup which data should be fetched into this Deliverypointgroup object</param>
		/// <param name="validator">The custom validator object for this DeliverypointgroupEntity</param>
		public DeliverypointgroupEntity(System.Int32 deliverypointgroupId, IValidator validator)
		{
			InitClassEmpty(validator, null);
			this.DeliverypointgroupId = deliverypointgroupId;
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected DeliverypointgroupEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'CheckoutMethodDeliverypointgroup' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoCheckoutMethodDeliverypointgroupCollection() { return CreateRelationInfoForNavigator("CheckoutMethodDeliverypointgroupCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Client' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoClientCollection() { return CreateRelationInfoForNavigator("ClientCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'ClientConfiguration' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoClientConfigurationCollection() { return CreateRelationInfoForNavigator("ClientConfigurationCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Deliverypoint' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoDeliverypointCollection() { return CreateRelationInfoForNavigator("DeliverypointCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Media' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoMediaCollection() { return CreateRelationInfoForNavigator("MediaCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Netmessage' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoNetmessageCollection() { return CreateRelationInfoForNavigator("NetmessageCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'ServiceMethodDeliverypointgroup' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoServiceMethodDeliverypointgroupCollection() { return CreateRelationInfoForNavigator("ServiceMethodDeliverypointgroupCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Terminal' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoTerminalCollection() { return CreateRelationInfoForNavigator("TerminalCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'ClientConfiguration' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoClientConfigurationCollectionViaDeliverypoint() { return CreateRelationInfoForNavigator("ClientConfigurationCollectionViaDeliverypoint"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'ClientConfiguration' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoClientConfiguration() { return CreateRelationInfoForNavigator("ClientConfiguration"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Company' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoCompany() { return CreateRelationInfoForNavigator("Company"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Menu' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoMenu() { return CreateRelationInfoForNavigator("Menu"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'PriceSchedule' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoPriceSchedule() { return CreateRelationInfoForNavigator("PriceSchedule"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Product' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoProduct() { return CreateRelationInfoForNavigator("Product"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Route' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoEmailDocumentRoute() { return CreateRelationInfoForNavigator("EmailDocumentRoute"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Route' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoOrderNotesRoute() { return CreateRelationInfoForNavigator("OrderNotesRoute"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Route' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoRoute() { return CreateRelationInfoForNavigator("Route"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Route' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoSystemMessageRoute() { return CreateRelationInfoForNavigator("SystemMessageRoute"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Terminal' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoTerminal() { return CreateRelationInfoForNavigator("Terminal"); }
		
		/// <inheritdoc/>
		protected override EntityStaticMetaDataBase GetEntityStaticMetaData() {	return _staticMetaData; }

		/// <summary>Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		/// <summary>Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this DeliverypointgroupEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary>The relations object holding all relations of this entity with other entity classes.</summary>
		public static DeliverypointgroupRelations Relations { get { return _relationsFactory; } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'CheckoutMethodDeliverypointgroup' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathCheckoutMethodDeliverypointgroupCollection { get { return _staticMetaData.GetPrefetchPathElement("CheckoutMethodDeliverypointgroupCollection", CommonEntityBase.CreateEntityCollection<CheckoutMethodDeliverypointgroupEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Client' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathClientCollection { get { return _staticMetaData.GetPrefetchPathElement("ClientCollection", CommonEntityBase.CreateEntityCollection<ClientEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'ClientConfiguration' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathClientConfigurationCollection { get { return _staticMetaData.GetPrefetchPathElement("ClientConfigurationCollection", CommonEntityBase.CreateEntityCollection<ClientConfigurationEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Deliverypoint' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathDeliverypointCollection { get { return _staticMetaData.GetPrefetchPathElement("DeliverypointCollection", CommonEntityBase.CreateEntityCollection<DeliverypointEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Media' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathMediaCollection { get { return _staticMetaData.GetPrefetchPathElement("MediaCollection", CommonEntityBase.CreateEntityCollection<MediaEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Netmessage' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathNetmessageCollection { get { return _staticMetaData.GetPrefetchPathElement("NetmessageCollection", CommonEntityBase.CreateEntityCollection<NetmessageEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'ServiceMethodDeliverypointgroup' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathServiceMethodDeliverypointgroupCollection { get { return _staticMetaData.GetPrefetchPathElement("ServiceMethodDeliverypointgroupCollection", CommonEntityBase.CreateEntityCollection<ServiceMethodDeliverypointgroupEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Terminal' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathTerminalCollection { get { return _staticMetaData.GetPrefetchPathElement("TerminalCollection", CommonEntityBase.CreateEntityCollection<TerminalEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'ClientConfiguration' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathClientConfigurationCollectionViaDeliverypoint { get { return _staticMetaData.GetPrefetchPathElement("ClientConfigurationCollectionViaDeliverypoint", CommonEntityBase.CreateEntityCollection<ClientConfigurationEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'ClientConfiguration' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathClientConfigurationEntity { get { return _staticMetaData.GetPrefetchPathElement("ClientConfiguration", CommonEntityBase.CreateEntityCollection<ClientConfigurationEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Company' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathCompanyEntity { get { return _staticMetaData.GetPrefetchPathElement("Company", CommonEntityBase.CreateEntityCollection<CompanyEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Menu' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathMenuEntity { get { return _staticMetaData.GetPrefetchPathElement("Menu", CommonEntityBase.CreateEntityCollection<MenuEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'PriceSchedule' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathPriceScheduleEntity { get { return _staticMetaData.GetPrefetchPathElement("PriceSchedule", CommonEntityBase.CreateEntityCollection<PriceScheduleEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Product' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathProductEntity { get { return _staticMetaData.GetPrefetchPathElement("Product", CommonEntityBase.CreateEntityCollection<ProductEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Route' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathEmailDocumentRouteEntity { get { return _staticMetaData.GetPrefetchPathElement("EmailDocumentRoute", CommonEntityBase.CreateEntityCollection<RouteEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Route' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathOrderNotesRouteEntity { get { return _staticMetaData.GetPrefetchPathElement("OrderNotesRoute", CommonEntityBase.CreateEntityCollection<RouteEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Route' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathRouteEntity { get { return _staticMetaData.GetPrefetchPathElement("Route", CommonEntityBase.CreateEntityCollection<RouteEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Route' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathSystemMessageRouteEntity { get { return _staticMetaData.GetPrefetchPathElement("SystemMessageRoute", CommonEntityBase.CreateEntityCollection<RouteEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Terminal' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathTerminalEntity { get { return _staticMetaData.GetPrefetchPathElement("Terminal", CommonEntityBase.CreateEntityCollection<TerminalEntity>()); } }

		/// <summary>The DeliverypointgroupId property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."DeliverypointgroupId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 DeliverypointgroupId
		{
			get { return (System.Int32)GetValue((int)DeliverypointgroupFieldIndex.DeliverypointgroupId, true); }
			set { SetValue((int)DeliverypointgroupFieldIndex.DeliverypointgroupId, value); }		}

		/// <summary>The TerminalId property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."TerminalId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> TerminalId
		{
			get { return (Nullable<System.Int32>)GetValue((int)DeliverypointgroupFieldIndex.TerminalId, false); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.TerminalId, value); }
		}

		/// <summary>The CompanyId property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."CompanyId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CompanyId
		{
			get { return (System.Int32)GetValue((int)DeliverypointgroupFieldIndex.CompanyId, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.CompanyId, value); }
		}

		/// <summary>The ClientConfigurationId property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."ClientConfigurationId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ClientConfigurationId
		{
			get { return (Nullable<System.Int32>)GetValue((int)DeliverypointgroupFieldIndex.ClientConfigurationId, false); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.ClientConfigurationId, value); }
		}

		/// <summary>The RouteId property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."RouteId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> RouteId
		{
			get { return (Nullable<System.Int32>)GetValue((int)DeliverypointgroupFieldIndex.RouteId, false); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.RouteId, value); }
		}

		/// <summary>The SystemMessageRouteId property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."SystemMessageRouteId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> SystemMessageRouteId
		{
			get { return (Nullable<System.Int32>)GetValue((int)DeliverypointgroupFieldIndex.SystemMessageRouteId, false); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.SystemMessageRouteId, value); }
		}

		/// <summary>The OrderNotesRouteId property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."OrderNotesRouteId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> OrderNotesRouteId
		{
			get { return (Nullable<System.Int32>)GetValue((int)DeliverypointgroupFieldIndex.OrderNotesRouteId, false); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.OrderNotesRouteId, value); }
		}

		/// <summary>The MenuId property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."MenuId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> MenuId
		{
			get { return (Nullable<System.Int32>)GetValue((int)DeliverypointgroupFieldIndex.MenuId, false); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.MenuId, value); }
		}

		/// <summary>The EmailDocumentRouteId property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."EmailDocumentRouteId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> EmailDocumentRouteId
		{
			get { return (Nullable<System.Int32>)GetValue((int)DeliverypointgroupFieldIndex.EmailDocumentRouteId, false); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.EmailDocumentRouteId, value); }
		}

		/// <summary>The Name property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."Name".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 100.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.Name, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.Name, value); }
		}

		/// <summary>The DeliverypointCaption property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."DeliverypointCaption".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String DeliverypointCaption
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.DeliverypointCaption, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.DeliverypointCaption, value); }
		}

		/// <summary>The ConfirmationCodeDeliveryType property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."ConfirmationCodeDeliveryType".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ConfirmationCodeDeliveryType
		{
			get { return (Nullable<System.Int32>)GetValue((int)DeliverypointgroupFieldIndex.ConfirmationCodeDeliveryType, false); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.ConfirmationCodeDeliveryType, value); }
		}

		/// <summary>The Active property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."Active".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean Active
		{
			get { return (System.Boolean)GetValue((int)DeliverypointgroupFieldIndex.Active, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.Active, value); }
		}

		/// <summary>The AvailableOnObymobi property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."AvailableOnObymobi".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean AvailableOnObymobi
		{
			get { return (System.Boolean)GetValue((int)DeliverypointgroupFieldIndex.AvailableOnObymobi, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.AvailableOnObymobi, value); }
		}

		/// <summary>The WifiSsid property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."WifiSsid".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String WifiSsid
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.WifiSsid, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.WifiSsid, value); }
		}

		/// <summary>The WifiPassword property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."WifiPassword".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String WifiPassword
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.WifiPassword, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.WifiPassword, value); }
		}

		/// <summary>The WifiSecurityMethod property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."WifiSecurityMethod".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 WifiSecurityMethod
		{
			get { return (System.Int32)GetValue((int)DeliverypointgroupFieldIndex.WifiSecurityMethod, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.WifiSecurityMethod, value); }
		}

		/// <summary>The WifiEapMode property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."WifiEapMode".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 WifiEapMode
		{
			get { return (System.Int32)GetValue((int)DeliverypointgroupFieldIndex.WifiEapMode, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.WifiEapMode, value); }
		}

		/// <summary>The WifiPhase2Authentication property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."WifiPhase2Authentication".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 WifiPhase2Authentication
		{
			get { return (System.Int32)GetValue((int)DeliverypointgroupFieldIndex.WifiPhase2Authentication, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.WifiPhase2Authentication, value); }
		}

		/// <summary>The WifiAnonymousIdentify property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."WifiAnonymousIdentify".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String WifiAnonymousIdentify
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.WifiAnonymousIdentify, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.WifiAnonymousIdentify, value); }
		}

		/// <summary>The WifiIdentity property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."WifiIdentity".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String WifiIdentity
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.WifiIdentity, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.WifiIdentity, value); }
		}

		/// <summary>The Locale property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."Locale".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Locale
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.Locale, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.Locale, value); }
		}

		/// <summary>The Pincode property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."Pincode".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 25.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Pincode
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.Pincode, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.Pincode, value); }
		}

		/// <summary>The UseManualDeliverypoint property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."UseManualDeliverypoint".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean UseManualDeliverypoint
		{
			get { return (System.Boolean)GetValue((int)DeliverypointgroupFieldIndex.UseManualDeliverypoint, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.UseManualDeliverypoint, value); }
		}

		/// <summary>The UseManualDeliverypointEncryption property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."UseManualDeliverypointEncryption".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean UseManualDeliverypointEncryption
		{
			get { return (System.Boolean)GetValue((int)DeliverypointgroupFieldIndex.UseManualDeliverypointEncryption, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.UseManualDeliverypointEncryption, value); }
		}

		/// <summary>The AddDeliverypointToOrder property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."AddDeliverypointToOrder".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean AddDeliverypointToOrder
		{
			get { return (System.Boolean)GetValue((int)DeliverypointgroupFieldIndex.AddDeliverypointToOrder, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.AddDeliverypointToOrder, value); }
		}

		/// <summary>The EncryptionSalt property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."EncryptionSalt".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 EncryptionSalt
		{
			get { return (System.Int32)GetValue((int)DeliverypointgroupFieldIndex.EncryptionSalt, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.EncryptionSalt, value); }
		}

		/// <summary>The HideDeliverypointNumber property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."HideDeliverypointNumber".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean HideDeliverypointNumber
		{
			get { return (System.Boolean)GetValue((int)DeliverypointgroupFieldIndex.HideDeliverypointNumber, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.HideDeliverypointNumber, value); }
		}

		/// <summary>The HidePrices property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."HidePrices".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean HidePrices
		{
			get { return (System.Boolean)GetValue((int)DeliverypointgroupFieldIndex.HidePrices, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.HidePrices, value); }
		}

		/// <summary>The ClearSessionOnTimeout property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."ClearSessionOnTimeout".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean ClearSessionOnTimeout
		{
			get { return (System.Boolean)GetValue((int)DeliverypointgroupFieldIndex.ClearSessionOnTimeout, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.ClearSessionOnTimeout, value); }
		}

		/// <summary>The ResetTimeout property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."ResetTimeout".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ResetTimeout
		{
			get { return (System.Int32)GetValue((int)DeliverypointgroupFieldIndex.ResetTimeout, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.ResetTimeout, value); }
		}

		/// <summary>The ReorderNotificationEnabled property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."ReorderNotificationEnabled".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean ReorderNotificationEnabled
		{
			get { return (System.Boolean)GetValue((int)DeliverypointgroupFieldIndex.ReorderNotificationEnabled, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.ReorderNotificationEnabled, value); }
		}

		/// <summary>The ReorderNotificationInterval property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."ReorderNotificationInterval".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ReorderNotificationInterval
		{
			get { return (System.Int32)GetValue((int)DeliverypointgroupFieldIndex.ReorderNotificationInterval, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.ReorderNotificationInterval, value); }
		}

		/// <summary>The ReorderNotificationAnnouncementId property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."ReorderNotificationAnnouncementId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ReorderNotificationAnnouncementId
		{
			get { return (Nullable<System.Int32>)GetValue((int)DeliverypointgroupFieldIndex.ReorderNotificationAnnouncementId, false); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.ReorderNotificationAnnouncementId, value); }
		}

		/// <summary>The ScreenTimeoutInterval property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."ScreenTimeoutInterval".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ScreenTimeoutInterval
		{
			get { return (System.Int32)GetValue((int)DeliverypointgroupFieldIndex.ScreenTimeoutInterval, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.ScreenTimeoutInterval, value); }
		}

		/// <summary>The DeviceActivationRequired property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."DeviceActivationRequired".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean DeviceActivationRequired
		{
			get { return (System.Boolean)GetValue((int)DeliverypointgroupFieldIndex.DeviceActivationRequired, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.DeviceActivationRequired, value); }
		}

		/// <summary>The PosdeliverypointgroupId property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."PosdeliverypointgroupId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> PosdeliverypointgroupId
		{
			get { return (Nullable<System.Int32>)GetValue((int)DeliverypointgroupFieldIndex.PosdeliverypointgroupId, false); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.PosdeliverypointgroupId, value); }
		}

		/// <summary>The DefaultProductFullView property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."DefaultProductFullView".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean DefaultProductFullView
		{
			get { return (System.Boolean)GetValue((int)DeliverypointgroupFieldIndex.DefaultProductFullView, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.DefaultProductFullView, value); }
		}

		/// <summary>The MarketingSurveyUrl property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."MarketingSurveyUrl".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String MarketingSurveyUrl
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.MarketingSurveyUrl, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.MarketingSurveyUrl, value); }
		}

		/// <summary>The HotelUrl1 property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."HotelUrl1".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String HotelUrl1
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.HotelUrl1, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.HotelUrl1, value); }
		}

		/// <summary>The HotelUrl1Caption property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."HotelUrl1Caption".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String HotelUrl1Caption
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.HotelUrl1Caption, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.HotelUrl1Caption, value); }
		}

		/// <summary>The HotelUrl1Zoom property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."HotelUrl1Zoom".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> HotelUrl1Zoom
		{
			get { return (Nullable<System.Int32>)GetValue((int)DeliverypointgroupFieldIndex.HotelUrl1Zoom, false); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.HotelUrl1Zoom, value); }
		}

		/// <summary>The HotelUrl2 property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."HotelUrl2".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String HotelUrl2
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.HotelUrl2, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.HotelUrl2, value); }
		}

		/// <summary>The HotelUrl2Caption property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."HotelUrl2Caption".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String HotelUrl2Caption
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.HotelUrl2Caption, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.HotelUrl2Caption, value); }
		}

		/// <summary>The HotelUrl2Zoom property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."HotelUrl2Zoom".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> HotelUrl2Zoom
		{
			get { return (Nullable<System.Int32>)GetValue((int)DeliverypointgroupFieldIndex.HotelUrl2Zoom, false); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.HotelUrl2Zoom, value); }
		}

		/// <summary>The HotelUrl3 property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."HotelUrl3".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String HotelUrl3
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.HotelUrl3, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.HotelUrl3, value); }
		}

		/// <summary>The HotelUrl3Caption property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."HotelUrl3Caption".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String HotelUrl3Caption
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.HotelUrl3Caption, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.HotelUrl3Caption, value); }
		}

		/// <summary>The HotelUrl3Zoom property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."HotelUrl3Zoom".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> HotelUrl3Zoom
		{
			get { return (Nullable<System.Int32>)GetValue((int)DeliverypointgroupFieldIndex.HotelUrl3Zoom, false); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.HotelUrl3Zoom, value); }
		}

		/// <summary>The DimLevelDull property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."DimLevelDull".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 DimLevelDull
		{
			get { return (System.Int32)GetValue((int)DeliverypointgroupFieldIndex.DimLevelDull, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.DimLevelDull, value); }
		}

		/// <summary>The DimLevelMedium property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."DimLevelMedium".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 DimLevelMedium
		{
			get { return (System.Int32)GetValue((int)DeliverypointgroupFieldIndex.DimLevelMedium, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.DimLevelMedium, value); }
		}

		/// <summary>The DimLevelBright property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."DimLevelBright".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 DimLevelBright
		{
			get { return (System.Int32)GetValue((int)DeliverypointgroupFieldIndex.DimLevelBright, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.DimLevelBright, value); }
		}

		/// <summary>The PowerSaveTimeout property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."PowerSaveTimeout".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 PowerSaveTimeout
		{
			get { return (System.Int32)GetValue((int)DeliverypointgroupFieldIndex.PowerSaveTimeout, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.PowerSaveTimeout, value); }
		}

		/// <summary>The PowerSaveLevel property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."PowerSaveLevel".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 PowerSaveLevel
		{
			get { return (System.Int32)GetValue((int)DeliverypointgroupFieldIndex.PowerSaveLevel, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.PowerSaveLevel, value); }
		}

		/// <summary>The OutOfChargeLevel property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."OutOfChargeLevel".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 OutOfChargeLevel
		{
			get { return (System.Int32)GetValue((int)DeliverypointgroupFieldIndex.OutOfChargeLevel, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.OutOfChargeLevel, value); }
		}

		/// <summary>The OutOfChargeNotificationAmount property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."OutOfChargeNotificationAmount".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 OutOfChargeNotificationAmount
		{
			get { return (System.Int32)GetValue((int)DeliverypointgroupFieldIndex.OutOfChargeNotificationAmount, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.OutOfChargeNotificationAmount, value); }
		}

		/// <summary>The OutOfChargeTitle property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."OutOfChargeTitle".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String OutOfChargeTitle
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.OutOfChargeTitle, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.OutOfChargeTitle, value); }
		}

		/// <summary>The OutOfChargeMessage property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."OutOfChargeMessage".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String OutOfChargeMessage
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.OutOfChargeMessage, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.OutOfChargeMessage, value); }
		}

		/// <summary>The OrderProcessedTitle property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."OrderProcessedTitle".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String OrderProcessedTitle
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.OrderProcessedTitle, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.OrderProcessedTitle, value); }
		}

		/// <summary>The OrderProcessedMessage property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."OrderProcessedMessage".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String OrderProcessedMessage
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.OrderProcessedMessage, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.OrderProcessedMessage, value); }
		}

		/// <summary>The OrderFailedTitle property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."OrderFailedTitle".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String OrderFailedTitle
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.OrderFailedTitle, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.OrderFailedTitle, value); }
		}

		/// <summary>The OrderFailedMessage property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."OrderFailedMessage".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String OrderFailedMessage
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.OrderFailedMessage, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.OrderFailedMessage, value); }
		}

		/// <summary>The OrderCompletedTitle property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."OrderCompletedTitle".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String OrderCompletedTitle
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.OrderCompletedTitle, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.OrderCompletedTitle, value); }
		}

		/// <summary>The OrderCompletedMessage property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."OrderCompletedMessage".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String OrderCompletedMessage
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.OrderCompletedMessage, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.OrderCompletedMessage, value); }
		}

		/// <summary>The OrderCompletedEnabled property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."OrderCompletedEnabled".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean OrderCompletedEnabled
		{
			get { return (System.Boolean)GetValue((int)DeliverypointgroupFieldIndex.OrderCompletedEnabled, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.OrderCompletedEnabled, value); }
		}

		/// <summary>The OrderSavingTitle property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."OrderSavingTitle".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String OrderSavingTitle
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.OrderSavingTitle, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.OrderSavingTitle, value); }
		}

		/// <summary>The OrderSavingMessage property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."OrderSavingMessage".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String OrderSavingMessage
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.OrderSavingMessage, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.OrderSavingMessage, value); }
		}

		/// <summary>The ServiceSavingTitle property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."ServiceSavingTitle".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ServiceSavingTitle
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.ServiceSavingTitle, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.ServiceSavingTitle, value); }
		}

		/// <summary>The ServiceSavingMessage property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."ServiceSavingMessage".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ServiceSavingMessage
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.ServiceSavingMessage, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.ServiceSavingMessage, value); }
		}

		/// <summary>The ServiceProcessedTitle property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."ServiceProcessedTitle".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ServiceProcessedTitle
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.ServiceProcessedTitle, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.ServiceProcessedTitle, value); }
		}

		/// <summary>The ServiceProcessedMessage property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."ServiceProcessedMessage".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ServiceProcessedMessage
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.ServiceProcessedMessage, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.ServiceProcessedMessage, value); }
		}

		/// <summary>The ServiceFailedTitle property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."ServiceFailedTitle".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ServiceFailedTitle
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.ServiceFailedTitle, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.ServiceFailedTitle, value); }
		}

		/// <summary>The ServiceFailedMessage property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."ServiceFailedMessage".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ServiceFailedMessage
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.ServiceFailedMessage, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.ServiceFailedMessage, value); }
		}

		/// <summary>The OrderingNotAvailableMessage property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."OrderingNotAvailableMessage".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String OrderingNotAvailableMessage
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.OrderingNotAvailableMessage, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.OrderingNotAvailableMessage, value); }
		}

		/// <summary>The DailyOrderReset property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."DailyOrderReset".<br/>Table field type characteristics (type, precision, scale, length): Char, 0, 0, 4.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String DailyOrderReset
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.DailyOrderReset, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.DailyOrderReset, value); }
		}

		/// <summary>The SleepTime property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."SleepTime".<br/>Table field type characteristics (type, precision, scale, length): Char, 0, 0, 4.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String SleepTime
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.SleepTime, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.SleepTime, value); }
		}

		/// <summary>The WakeUpTime property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."WakeUpTime".<br/>Table field type characteristics (type, precision, scale, length): Char, 0, 0, 4.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String WakeUpTime
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.WakeUpTime, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.WakeUpTime, value); }
		}

		/// <summary>The OrderProcessingNotificationEnabled property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."OrderProcessingNotificationEnabled".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean OrderProcessingNotificationEnabled
		{
			get { return (System.Boolean)GetValue((int)DeliverypointgroupFieldIndex.OrderProcessingNotificationEnabled, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.OrderProcessingNotificationEnabled, value); }
		}

		/// <summary>The OrderProcessingNotificationTitle property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."OrderProcessingNotificationTitle".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String OrderProcessingNotificationTitle
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.OrderProcessingNotificationTitle, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.OrderProcessingNotificationTitle, value); }
		}

		/// <summary>The OrderProcessingNotification property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."OrderProcessingNotification".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String OrderProcessingNotification
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.OrderProcessingNotification, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.OrderProcessingNotification, value); }
		}

		/// <summary>The OrderProcessedNotificationEnabled property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."OrderProcessedNotificationEnabled".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean OrderProcessedNotificationEnabled
		{
			get { return (System.Boolean)GetValue((int)DeliverypointgroupFieldIndex.OrderProcessedNotificationEnabled, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.OrderProcessedNotificationEnabled, value); }
		}

		/// <summary>The OrderProcessedNotificationTitle property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."OrderProcessedNotificationTitle".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String OrderProcessedNotificationTitle
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.OrderProcessedNotificationTitle, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.OrderProcessedNotificationTitle, value); }
		}

		/// <summary>The OrderProcessedNotification property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."OrderProcessedNotification".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String OrderProcessedNotification
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.OrderProcessedNotification, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.OrderProcessedNotification, value); }
		}

		/// <summary>The FreeformMessageTitle property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."FreeformMessageTitle".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FreeformMessageTitle
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.FreeformMessageTitle, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.FreeformMessageTitle, value); }
		}

		/// <summary>The ServiceProcessingNotificationTitle property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."ServiceProcessingNotificationTitle".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ServiceProcessingNotificationTitle
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.ServiceProcessingNotificationTitle, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.ServiceProcessingNotificationTitle, value); }
		}

		/// <summary>The ServiceProcessingNotification property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."ServiceProcessingNotification".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ServiceProcessingNotification
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.ServiceProcessingNotification, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.ServiceProcessingNotification, value); }
		}

		/// <summary>The ServiceProcessedNotificationTitle property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."ServiceProcessedNotificationTitle".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ServiceProcessedNotificationTitle
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.ServiceProcessedNotificationTitle, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.ServiceProcessedNotificationTitle, value); }
		}

		/// <summary>The ServiceProcessedNotification property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."ServiceProcessedNotification".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ServiceProcessedNotification
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.ServiceProcessedNotification, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.ServiceProcessedNotification, value); }
		}

		/// <summary>The PreorderingEnabled property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."PreorderingEnabled".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean PreorderingEnabled
		{
			get { return (System.Boolean)GetValue((int)DeliverypointgroupFieldIndex.PreorderingEnabled, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.PreorderingEnabled, value); }
		}

		/// <summary>The PreorderingActiveMinutes property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."PreorderingActiveMinutes".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 PreorderingActiveMinutes
		{
			get { return (System.Int32)GetValue((int)DeliverypointgroupFieldIndex.PreorderingActiveMinutes, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.PreorderingActiveMinutes, value); }
		}

		/// <summary>The AnalyticsOrderingVisible property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."AnalyticsOrderingVisible".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean AnalyticsOrderingVisible
		{
			get { return (System.Boolean)GetValue((int)DeliverypointgroupFieldIndex.AnalyticsOrderingVisible, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.AnalyticsOrderingVisible, value); }
		}

		/// <summary>The AnalyticsBestsellersVisible property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."AnalyticsBestsellersVisible".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean AnalyticsBestsellersVisible
		{
			get { return (System.Boolean)GetValue((int)DeliverypointgroupFieldIndex.AnalyticsBestsellersVisible, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.AnalyticsBestsellersVisible, value); }
		}

		/// <summary>The OrderingEnabled property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."OrderingEnabled".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean OrderingEnabled
		{
			get { return (System.Boolean)GetValue((int)DeliverypointgroupFieldIndex.OrderingEnabled, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.OrderingEnabled, value); }
		}

		/// <summary>The HomepageSlideshowInterval property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."HomepageSlideshowInterval".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 HomepageSlideshowInterval
		{
			get { return (System.Int32)GetValue((int)DeliverypointgroupFieldIndex.HomepageSlideshowInterval, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.HomepageSlideshowInterval, value); }
		}

		/// <summary>The GooglePrinterId property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."GooglePrinterId".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String GooglePrinterId
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.GooglePrinterId, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.GooglePrinterId, value); }
		}

		/// <summary>The GooglePrinterName property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."GooglePrinterName".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String GooglePrinterName
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.GooglePrinterName, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.GooglePrinterName, value); }
		}

		/// <summary>The UIModeId property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."UIModeId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UIModeId
		{
			get { return (Nullable<System.Int32>)GetValue((int)DeliverypointgroupFieldIndex.UIModeId, false); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.UIModeId, value); }
		}

		/// <summary>The MobileUIModeId property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."MobileUIModeId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> MobileUIModeId
		{
			get { return (Nullable<System.Int32>)GetValue((int)DeliverypointgroupFieldIndex.MobileUIModeId, false); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.MobileUIModeId, value); }
		}

		/// <summary>The TabletUIModeId property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."TabletUIModeId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> TabletUIModeId
		{
			get { return (Nullable<System.Int32>)GetValue((int)DeliverypointgroupFieldIndex.TabletUIModeId, false); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.TabletUIModeId, value); }
		}

		/// <summary>The PmsIntegration property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."PmsIntegration".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean PmsIntegration
		{
			get { return (System.Boolean)GetValue((int)DeliverypointgroupFieldIndex.PmsIntegration, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.PmsIntegration, value); }
		}

		/// <summary>The PmsAllowShowBill property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."PmsAllowShowBill".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean PmsAllowShowBill
		{
			get { return (System.Boolean)GetValue((int)DeliverypointgroupFieldIndex.PmsAllowShowBill, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.PmsAllowShowBill, value); }
		}

		/// <summary>The PmsAllowExpressCheckout property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."PmsAllowExpressCheckout".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean PmsAllowExpressCheckout
		{
			get { return (System.Boolean)GetValue((int)DeliverypointgroupFieldIndex.PmsAllowExpressCheckout, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.PmsAllowExpressCheckout, value); }
		}

		/// <summary>The PmsAllowShowGuestName property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."PmsAllowShowGuestName".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean PmsAllowShowGuestName
		{
			get { return (System.Boolean)GetValue((int)DeliverypointgroupFieldIndex.PmsAllowShowGuestName, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.PmsAllowShowGuestName, value); }
		}

		/// <summary>The PmsLockClientWhenNotCheckedIn property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."PmsLockClientWhenNotCheckedIn".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean PmsLockClientWhenNotCheckedIn
		{
			get { return (System.Boolean)GetValue((int)DeliverypointgroupFieldIndex.PmsLockClientWhenNotCheckedIn, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.PmsLockClientWhenNotCheckedIn, value); }
		}

		/// <summary>The PmsDeviceLockedTitle property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."PmsDeviceLockedTitle".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PmsDeviceLockedTitle
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.PmsDeviceLockedTitle, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.PmsDeviceLockedTitle, value); }
		}

		/// <summary>The PmsDeviceLockedText property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."PmsDeviceLockedText".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PmsDeviceLockedText
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.PmsDeviceLockedText, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.PmsDeviceLockedText, value); }
		}

		/// <summary>The PmsCheckinFailedTitle property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."PmsCheckinFailedTitle".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PmsCheckinFailedTitle
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.PmsCheckinFailedTitle, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.PmsCheckinFailedTitle, value); }
		}

		/// <summary>The PmsCheckinFailedText property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."PmsCheckinFailedText".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PmsCheckinFailedText
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.PmsCheckinFailedText, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.PmsCheckinFailedText, value); }
		}

		/// <summary>The PmsRestartTitle property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."PmsRestartTitle".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PmsRestartTitle
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.PmsRestartTitle, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.PmsRestartTitle, value); }
		}

		/// <summary>The PmsRestartText property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."PmsRestartText".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PmsRestartText
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.PmsRestartText, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.PmsRestartText, value); }
		}

		/// <summary>The PmsWelcomeTitle property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."PmsWelcomeTitle".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PmsWelcomeTitle
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.PmsWelcomeTitle, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.PmsWelcomeTitle, value); }
		}

		/// <summary>The PmsWelcomeText property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."PmsWelcomeText".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PmsWelcomeText
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.PmsWelcomeText, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.PmsWelcomeText, value); }
		}

		/// <summary>The PmsCheckoutApproveText property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."PmsCheckoutApproveText".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PmsCheckoutApproveText
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.PmsCheckoutApproveText, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.PmsCheckoutApproveText, value); }
		}

		/// <summary>The PmsWelcomeTimeoutMinutes property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."PmsWelcomeTimeoutMinutes".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 PmsWelcomeTimeoutMinutes
		{
			get { return (System.Int32)GetValue((int)DeliverypointgroupFieldIndex.PmsWelcomeTimeoutMinutes, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.PmsWelcomeTimeoutMinutes, value); }
		}

		/// <summary>The PmsCheckoutCompleteTitle property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."PmsCheckoutCompleteTitle".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PmsCheckoutCompleteTitle
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.PmsCheckoutCompleteTitle, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.PmsCheckoutCompleteTitle, value); }
		}

		/// <summary>The PmsCheckoutCompleteText property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."PmsCheckoutCompleteText".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PmsCheckoutCompleteText
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.PmsCheckoutCompleteText, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.PmsCheckoutCompleteText, value); }
		}

		/// <summary>The PmsCheckoutCompleteTimeoutMinutes property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."PmsCheckoutCompleteTimeoutMinutes".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 PmsCheckoutCompleteTimeoutMinutes
		{
			get { return (System.Int32)GetValue((int)DeliverypointgroupFieldIndex.PmsCheckoutCompleteTimeoutMinutes, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.PmsCheckoutCompleteTimeoutMinutes, value); }
		}

		/// <summary>The PincodeSU property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."PincodeSU".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 25.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PincodeSU
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.PincodeSU, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.PincodeSU, value); }
		}

		/// <summary>The PincodeGM property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."PincodeGM".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 25.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PincodeGM
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.PincodeGM, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.PincodeGM, value); }
		}

		/// <summary>The DockedDimLevelDull property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."DockedDimLevelDull".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 DockedDimLevelDull
		{
			get { return (System.Int32)GetValue((int)DeliverypointgroupFieldIndex.DockedDimLevelDull, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.DockedDimLevelDull, value); }
		}

		/// <summary>The DockedDimLevelMedium property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."DockedDimLevelMedium".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 DockedDimLevelMedium
		{
			get { return (System.Int32)GetValue((int)DeliverypointgroupFieldIndex.DockedDimLevelMedium, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.DockedDimLevelMedium, value); }
		}

		/// <summary>The DockedDimLevelBright property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."DockedDimLevelBright".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 DockedDimLevelBright
		{
			get { return (System.Int32)GetValue((int)DeliverypointgroupFieldIndex.DockedDimLevelBright, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.DockedDimLevelBright, value); }
		}

		/// <summary>The OrderHistoryDialogEnabled property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."OrderHistoryDialogEnabled".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean OrderHistoryDialogEnabled
		{
			get { return (System.Boolean)GetValue((int)DeliverypointgroupFieldIndex.OrderHistoryDialogEnabled, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.OrderHistoryDialogEnabled, value); }
		}

		/// <summary>The IsChargerRemovedDialogEnabled property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."IsChargerRemovedDialogEnabled".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsChargerRemovedDialogEnabled
		{
			get { return (System.Boolean)GetValue((int)DeliverypointgroupFieldIndex.IsChargerRemovedDialogEnabled, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.IsChargerRemovedDialogEnabled, value); }
		}

		/// <summary>The ChargerRemovedDialogTitle property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."ChargerRemovedDialogTitle".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ChargerRemovedDialogTitle
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.ChargerRemovedDialogTitle, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.ChargerRemovedDialogTitle, value); }
		}

		/// <summary>The ChargerRemovedDialogText property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."ChargerRemovedDialogText".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ChargerRemovedDialogText
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.ChargerRemovedDialogText, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.ChargerRemovedDialogText, value); }
		}

		/// <summary>The IsChargerRemovedReminderDialogEnabled property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."IsChargerRemovedReminderDialogEnabled".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsChargerRemovedReminderDialogEnabled
		{
			get { return (System.Boolean)GetValue((int)DeliverypointgroupFieldIndex.IsChargerRemovedReminderDialogEnabled, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.IsChargerRemovedReminderDialogEnabled, value); }
		}

		/// <summary>The ChargerRemovedReminderDialogTitle property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."ChargerRemovedReminderDialogTitle".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ChargerRemovedReminderDialogTitle
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.ChargerRemovedReminderDialogTitle, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.ChargerRemovedReminderDialogTitle, value); }
		}

		/// <summary>The ChargerRemovedReminderDialogText property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."ChargerRemovedReminderDialogText".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ChargerRemovedReminderDialogText
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.ChargerRemovedReminderDialogText, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.ChargerRemovedReminderDialogText, value); }
		}

		/// <summary>The PowerButtonHardBehaviour property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."PowerButtonHardBehaviour".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 PowerButtonHardBehaviour
		{
			get { return (System.Int32)GetValue((int)DeliverypointgroupFieldIndex.PowerButtonHardBehaviour, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.PowerButtonHardBehaviour, value); }
		}

		/// <summary>The PowerButtonSoftBehaviour property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."PowerButtonSoftBehaviour".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 PowerButtonSoftBehaviour
		{
			get { return (System.Int32)GetValue((int)DeliverypointgroupFieldIndex.PowerButtonSoftBehaviour, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.PowerButtonSoftBehaviour, value); }
		}

		/// <summary>The ScreenOffMode property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."ScreenOffMode".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ScreenOffMode
		{
			get { return (System.Int32)GetValue((int)DeliverypointgroupFieldIndex.ScreenOffMode, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.ScreenOffMode, value); }
		}

		/// <summary>The UseHardKeyboard property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."UseHardKeyboard".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UseHardKeyboard
		{
			get { return (System.Int32)GetValue((int)DeliverypointgroupFieldIndex.UseHardKeyboard, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.UseHardKeyboard, value); }
		}

		/// <summary>The RoomserviceCharge property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."RoomserviceCharge".<br/>Table field type characteristics (type, precision, scale, length): Money, 19, 4, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Decimal> RoomserviceCharge
		{
			get { return (Nullable<System.Decimal>)GetValue((int)DeliverypointgroupFieldIndex.RoomserviceCharge, false); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.RoomserviceCharge, value); }
		}

		/// <summary>The HotSOSBatteryLowProductId property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."HotSOSBatteryLowProductId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> HotSOSBatteryLowProductId
		{
			get { return (Nullable<System.Int32>)GetValue((int)DeliverypointgroupFieldIndex.HotSOSBatteryLowProductId, false); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.HotSOSBatteryLowProductId, value); }
		}

		/// <summary>The EnableBatteryLowConsoleNotifications property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."EnableBatteryLowConsoleNotifications".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean EnableBatteryLowConsoleNotifications
		{
			get { return (System.Boolean)GetValue((int)DeliverypointgroupFieldIndex.EnableBatteryLowConsoleNotifications, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.EnableBatteryLowConsoleNotifications, value); }
		}

		/// <summary>The UIThemeId property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."UIThemeId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UIThemeId
		{
			get { return (Nullable<System.Int32>)GetValue((int)DeliverypointgroupFieldIndex.UIThemeId, false); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.UIThemeId, value); }
		}

		/// <summary>The ScreensaverMode property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."ScreensaverMode".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ScreensaverMode
		{
			get { return (System.Int32)GetValue((int)DeliverypointgroupFieldIndex.ScreensaverMode, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.ScreensaverMode, value); }
		}

		/// <summary>The TurnOffPrivacyTitle property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."TurnOffPrivacyTitle".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String TurnOffPrivacyTitle
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.TurnOffPrivacyTitle, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.TurnOffPrivacyTitle, value); }
		}

		/// <summary>The TurnOffPrivacyText property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."TurnOffPrivacyText".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String TurnOffPrivacyText
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.TurnOffPrivacyText, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.TurnOffPrivacyText, value); }
		}

		/// <summary>The ItemCurrentlyUnavailableText property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."ItemCurrentlyUnavailableText".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ItemCurrentlyUnavailableText
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.ItemCurrentlyUnavailableText, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.ItemCurrentlyUnavailableText, value); }
		}

		/// <summary>The EstimatedDeliveryTime property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."EstimatedDeliveryTime".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 EstimatedDeliveryTime
		{
			get { return (System.Int32)GetValue((int)DeliverypointgroupFieldIndex.EstimatedDeliveryTime, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.EstimatedDeliveryTime, value); }
		}

		/// <summary>The UIScheduleId property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."UIScheduleId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UIScheduleId
		{
			get { return (Nullable<System.Int32>)GetValue((int)DeliverypointgroupFieldIndex.UIScheduleId, false); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.UIScheduleId, value); }
		}

		/// <summary>The AlarmSetWhileNotChargingTitle property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."AlarmSetWhileNotChargingTitle".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String AlarmSetWhileNotChargingTitle
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.AlarmSetWhileNotChargingTitle, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.AlarmSetWhileNotChargingTitle, value); }
		}

		/// <summary>The AlarmSetWhileNotChargingText property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."AlarmSetWhileNotChargingText".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String AlarmSetWhileNotChargingText
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.AlarmSetWhileNotChargingText, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.AlarmSetWhileNotChargingText, value); }
		}

		/// <summary>The ClearBasketTitle property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."ClearBasketTitle".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ClearBasketTitle
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.ClearBasketTitle, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.ClearBasketTitle, value); }
		}

		/// <summary>The ClearBasketText property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."ClearBasketText".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ClearBasketText
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.ClearBasketText, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.ClearBasketText, value); }
		}

		/// <summary>The PriceScheduleId property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."PriceScheduleId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> PriceScheduleId
		{
			get { return (Nullable<System.Int32>)GetValue((int)DeliverypointgroupFieldIndex.PriceScheduleId, false); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.PriceScheduleId, value); }
		}

		/// <summary>The IsOrderitemAddedDialogEnabled property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."IsOrderitemAddedDialogEnabled".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsOrderitemAddedDialogEnabled
		{
			get { return (System.Boolean)GetValue((int)DeliverypointgroupFieldIndex.IsOrderitemAddedDialogEnabled, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.IsOrderitemAddedDialogEnabled, value); }
		}

		/// <summary>The IsClearBasketDialogEnabled property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."IsClearBasketDialogEnabled".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsClearBasketDialogEnabled
		{
			get { return (System.Boolean)GetValue((int)DeliverypointgroupFieldIndex.IsClearBasketDialogEnabled, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.IsClearBasketDialogEnabled, value); }
		}

		/// <summary>The BrowserAgeVerificationEnabled property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."BrowserAgeVerificationEnabled".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean BrowserAgeVerificationEnabled
		{
			get { return (System.Boolean)GetValue((int)DeliverypointgroupFieldIndex.BrowserAgeVerificationEnabled, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.BrowserAgeVerificationEnabled, value); }
		}

		/// <summary>The BrowserAgeVerificationLayout property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."BrowserAgeVerificationLayout".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 BrowserAgeVerificationLayout
		{
			get { return (System.Int32)GetValue((int)DeliverypointgroupFieldIndex.BrowserAgeVerificationLayout, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.BrowserAgeVerificationLayout, value); }
		}

		/// <summary>The RestartApplicationDialogEnabled property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."RestartApplicationDialogEnabled".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean RestartApplicationDialogEnabled
		{
			get { return (System.Boolean)GetValue((int)DeliverypointgroupFieldIndex.RestartApplicationDialogEnabled, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.RestartApplicationDialogEnabled, value); }
		}

		/// <summary>The RebootDeviceDialogEnabled property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."RebootDeviceDialogEnabled".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean RebootDeviceDialogEnabled
		{
			get { return (System.Boolean)GetValue((int)DeliverypointgroupFieldIndex.RebootDeviceDialogEnabled, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.RebootDeviceDialogEnabled, value); }
		}

		/// <summary>The RestartTimeoutSeconds property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."RestartTimeoutSeconds".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 RestartTimeoutSeconds
		{
			get { return (System.Int32)GetValue((int)DeliverypointgroupFieldIndex.RestartTimeoutSeconds, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.RestartTimeoutSeconds, value); }
		}

		/// <summary>The CraveAnalytics property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."CraveAnalytics".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean CraveAnalytics
		{
			get { return (System.Boolean)GetValue((int)DeliverypointgroupFieldIndex.CraveAnalytics, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.CraveAnalytics, value); }
		}

		/// <summary>The GeoFencingEnabled property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."GeoFencingEnabled".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean GeoFencingEnabled
		{
			get { return (System.Boolean)GetValue((int)DeliverypointgroupFieldIndex.GeoFencingEnabled, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.GeoFencingEnabled, value); }
		}

		/// <summary>The GeoFencingRadius property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."GeoFencingRadius".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 GeoFencingRadius
		{
			get { return (System.Int32)GetValue((int)DeliverypointgroupFieldIndex.GeoFencingRadius, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.GeoFencingRadius, value); }
		}

		/// <summary>The HideCompanyDetails property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."HideCompanyDetails".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean HideCompanyDetails
		{
			get { return (System.Boolean)GetValue((int)DeliverypointgroupFieldIndex.HideCompanyDetails, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.HideCompanyDetails, value); }
		}

		/// <summary>The CreatedBy property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."CreatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)DeliverypointgroupFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.CreatedBy, value); }
		}

		/// <summary>The UpdatedBy property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."UpdatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)DeliverypointgroupFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.UpdatedBy, value); }
		}

		/// <summary>The CreatedUTC property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."CreatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)DeliverypointgroupFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.CreatedUTC, value); }
		}

		/// <summary>The UpdatedUTC property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."UpdatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)DeliverypointgroupFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.UpdatedUTC, value); }
		}

		/// <summary>The ApiVersion property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."ApiVersion".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ApiVersion
		{
			get { return (System.Int32)GetValue((int)DeliverypointgroupFieldIndex.ApiVersion, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.ApiVersion, value); }
		}

		/// <summary>The MessagingVersion property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."MessagingVersion".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 MessagingVersion
		{
			get { return (System.Int32)GetValue((int)DeliverypointgroupFieldIndex.MessagingVersion, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.MessagingVersion, value); }
		}

		/// <summary>The AffiliateCampaignId property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."AffiliateCampaignId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> AffiliateCampaignId
		{
			get { return (Nullable<System.Int32>)GetValue((int)DeliverypointgroupFieldIndex.AffiliateCampaignId, false); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.AffiliateCampaignId, value); }
		}

		/// <summary>The AddressId property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."AddressId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> AddressId
		{
			get { return (Nullable<System.Int32>)GetValue((int)DeliverypointgroupFieldIndex.AddressId, false); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.AddressId, value); }
		}

		/// <summary>Gets the EntityCollection with the related entities of type 'CheckoutMethodDeliverypointgroupEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(CheckoutMethodDeliverypointgroupEntity))]
		public virtual EntityCollection<CheckoutMethodDeliverypointgroupEntity> CheckoutMethodDeliverypointgroupCollection { get { return GetOrCreateEntityCollection<CheckoutMethodDeliverypointgroupEntity, CheckoutMethodDeliverypointgroupEntityFactory>("Deliverypointgroup", true, false, ref _checkoutMethodDeliverypointgroupCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'ClientEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(ClientEntity))]
		public virtual EntityCollection<ClientEntity> ClientCollection { get { return GetOrCreateEntityCollection<ClientEntity, ClientEntityFactory>("Deliverypointgroup", true, false, ref _clientCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'ClientConfigurationEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(ClientConfigurationEntity))]
		public virtual EntityCollection<ClientConfigurationEntity> ClientConfigurationCollection { get { return GetOrCreateEntityCollection<ClientConfigurationEntity, ClientConfigurationEntityFactory>("Deliverypointgroup", true, false, ref _clientConfigurationCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'DeliverypointEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(DeliverypointEntity))]
		public virtual EntityCollection<DeliverypointEntity> DeliverypointCollection { get { return GetOrCreateEntityCollection<DeliverypointEntity, DeliverypointEntityFactory>("Deliverypointgroup", true, false, ref _deliverypointCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'MediaEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(MediaEntity))]
		public virtual EntityCollection<MediaEntity> MediaCollection { get { return GetOrCreateEntityCollection<MediaEntity, MediaEntityFactory>("Deliverypointgroup", true, false, ref _mediaCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'NetmessageEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(NetmessageEntity))]
		public virtual EntityCollection<NetmessageEntity> NetmessageCollection { get { return GetOrCreateEntityCollection<NetmessageEntity, NetmessageEntityFactory>("Deliverypointgroup", true, false, ref _netmessageCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'ServiceMethodDeliverypointgroupEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(ServiceMethodDeliverypointgroupEntity))]
		public virtual EntityCollection<ServiceMethodDeliverypointgroupEntity> ServiceMethodDeliverypointgroupCollection { get { return GetOrCreateEntityCollection<ServiceMethodDeliverypointgroupEntity, ServiceMethodDeliverypointgroupEntityFactory>("Deliverypointgroup", true, false, ref _serviceMethodDeliverypointgroupCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'TerminalEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(TerminalEntity))]
		public virtual EntityCollection<TerminalEntity> TerminalCollection { get { return GetOrCreateEntityCollection<TerminalEntity, TerminalEntityFactory>("Deliverypointgroup", true, false, ref _terminalCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'ClientConfigurationEntity' which are related to this entity via a relation of type 'm:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(ClientConfigurationEntity))]
		public virtual EntityCollection<ClientConfigurationEntity> ClientConfigurationCollectionViaDeliverypoint { get { return GetOrCreateEntityCollection<ClientConfigurationEntity, ClientConfigurationEntityFactory>("", false, true, ref _clientConfigurationCollectionViaDeliverypoint); } }

		/// <summary>Gets / sets related entity of type 'ClientConfigurationEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual ClientConfigurationEntity ClientConfiguration
		{
			get { return _clientConfiguration; }
			set { SetSingleRelatedEntityNavigator(value, "ClientConfiguration"); }
		}

		/// <summary>Gets / sets related entity of type 'CompanyEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual CompanyEntity Company
		{
			get { return _company; }
			set { SetSingleRelatedEntityNavigator(value, "Company"); }
		}

		/// <summary>Gets / sets related entity of type 'MenuEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual MenuEntity Menu
		{
			get { return _menu; }
			set { SetSingleRelatedEntityNavigator(value, "Menu"); }
		}

		/// <summary>Gets / sets related entity of type 'PriceScheduleEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual PriceScheduleEntity PriceSchedule
		{
			get { return _priceSchedule; }
			set { SetSingleRelatedEntityNavigator(value, "PriceSchedule"); }
		}

		/// <summary>Gets / sets related entity of type 'ProductEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual ProductEntity Product
		{
			get { return _product; }
			set { SetSingleRelatedEntityNavigator(value, "Product"); }
		}

		/// <summary>Gets / sets related entity of type 'RouteEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual RouteEntity EmailDocumentRoute
		{
			get { return _emailDocumentRoute; }
			set { SetSingleRelatedEntityNavigator(value, "EmailDocumentRoute"); }
		}

		/// <summary>Gets / sets related entity of type 'RouteEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual RouteEntity OrderNotesRoute
		{
			get { return _orderNotesRoute; }
			set { SetSingleRelatedEntityNavigator(value, "OrderNotesRoute"); }
		}

		/// <summary>Gets / sets related entity of type 'RouteEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual RouteEntity Route
		{
			get { return _route; }
			set { SetSingleRelatedEntityNavigator(value, "Route"); }
		}

		/// <summary>Gets / sets related entity of type 'RouteEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual RouteEntity SystemMessageRoute
		{
			get { return _systemMessageRoute; }
			set { SetSingleRelatedEntityNavigator(value, "SystemMessageRoute"); }
		}

		/// <summary>Gets / sets related entity of type 'TerminalEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual TerminalEntity Terminal
		{
			get { return _terminal; }
			set { SetSingleRelatedEntityNavigator(value, "Terminal"); }
		}


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen
{
	public enum DeliverypointgroupFieldIndex
	{
		///<summary>DeliverypointgroupId. </summary>
		DeliverypointgroupId,
		///<summary>TerminalId. </summary>
		TerminalId,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>ClientConfigurationId. </summary>
		ClientConfigurationId,
		///<summary>RouteId. </summary>
		RouteId,
		///<summary>SystemMessageRouteId. </summary>
		SystemMessageRouteId,
		///<summary>OrderNotesRouteId. </summary>
		OrderNotesRouteId,
		///<summary>MenuId. </summary>
		MenuId,
		///<summary>EmailDocumentRouteId. </summary>
		EmailDocumentRouteId,
		///<summary>Name. </summary>
		Name,
		///<summary>DeliverypointCaption. </summary>
		DeliverypointCaption,
		///<summary>ConfirmationCodeDeliveryType. </summary>
		ConfirmationCodeDeliveryType,
		///<summary>Active. </summary>
		Active,
		///<summary>AvailableOnObymobi. </summary>
		AvailableOnObymobi,
		///<summary>WifiSsid. </summary>
		WifiSsid,
		///<summary>WifiPassword. </summary>
		WifiPassword,
		///<summary>WifiSecurityMethod. </summary>
		WifiSecurityMethod,
		///<summary>WifiEapMode. </summary>
		WifiEapMode,
		///<summary>WifiPhase2Authentication. </summary>
		WifiPhase2Authentication,
		///<summary>WifiAnonymousIdentify. </summary>
		WifiAnonymousIdentify,
		///<summary>WifiIdentity. </summary>
		WifiIdentity,
		///<summary>Locale. </summary>
		Locale,
		///<summary>Pincode. </summary>
		Pincode,
		///<summary>UseManualDeliverypoint. </summary>
		UseManualDeliverypoint,
		///<summary>UseManualDeliverypointEncryption. </summary>
		UseManualDeliverypointEncryption,
		///<summary>AddDeliverypointToOrder. </summary>
		AddDeliverypointToOrder,
		///<summary>EncryptionSalt. </summary>
		EncryptionSalt,
		///<summary>HideDeliverypointNumber. </summary>
		HideDeliverypointNumber,
		///<summary>HidePrices. </summary>
		HidePrices,
		///<summary>ClearSessionOnTimeout. </summary>
		ClearSessionOnTimeout,
		///<summary>ResetTimeout. </summary>
		ResetTimeout,
		///<summary>ReorderNotificationEnabled. </summary>
		ReorderNotificationEnabled,
		///<summary>ReorderNotificationInterval. </summary>
		ReorderNotificationInterval,
		///<summary>ReorderNotificationAnnouncementId. </summary>
		ReorderNotificationAnnouncementId,
		///<summary>ScreenTimeoutInterval. </summary>
		ScreenTimeoutInterval,
		///<summary>DeviceActivationRequired. </summary>
		DeviceActivationRequired,
		///<summary>PosdeliverypointgroupId. </summary>
		PosdeliverypointgroupId,
		///<summary>DefaultProductFullView. </summary>
		DefaultProductFullView,
		///<summary>MarketingSurveyUrl. </summary>
		MarketingSurveyUrl,
		///<summary>HotelUrl1. </summary>
		HotelUrl1,
		///<summary>HotelUrl1Caption. </summary>
		HotelUrl1Caption,
		///<summary>HotelUrl1Zoom. </summary>
		HotelUrl1Zoom,
		///<summary>HotelUrl2. </summary>
		HotelUrl2,
		///<summary>HotelUrl2Caption. </summary>
		HotelUrl2Caption,
		///<summary>HotelUrl2Zoom. </summary>
		HotelUrl2Zoom,
		///<summary>HotelUrl3. </summary>
		HotelUrl3,
		///<summary>HotelUrl3Caption. </summary>
		HotelUrl3Caption,
		///<summary>HotelUrl3Zoom. </summary>
		HotelUrl3Zoom,
		///<summary>DimLevelDull. </summary>
		DimLevelDull,
		///<summary>DimLevelMedium. </summary>
		DimLevelMedium,
		///<summary>DimLevelBright. </summary>
		DimLevelBright,
		///<summary>PowerSaveTimeout. </summary>
		PowerSaveTimeout,
		///<summary>PowerSaveLevel. </summary>
		PowerSaveLevel,
		///<summary>OutOfChargeLevel. </summary>
		OutOfChargeLevel,
		///<summary>OutOfChargeNotificationAmount. </summary>
		OutOfChargeNotificationAmount,
		///<summary>OutOfChargeTitle. </summary>
		OutOfChargeTitle,
		///<summary>OutOfChargeMessage. </summary>
		OutOfChargeMessage,
		///<summary>OrderProcessedTitle. </summary>
		OrderProcessedTitle,
		///<summary>OrderProcessedMessage. </summary>
		OrderProcessedMessage,
		///<summary>OrderFailedTitle. </summary>
		OrderFailedTitle,
		///<summary>OrderFailedMessage. </summary>
		OrderFailedMessage,
		///<summary>OrderCompletedTitle. </summary>
		OrderCompletedTitle,
		///<summary>OrderCompletedMessage. </summary>
		OrderCompletedMessage,
		///<summary>OrderCompletedEnabled. </summary>
		OrderCompletedEnabled,
		///<summary>OrderSavingTitle. </summary>
		OrderSavingTitle,
		///<summary>OrderSavingMessage. </summary>
		OrderSavingMessage,
		///<summary>ServiceSavingTitle. </summary>
		ServiceSavingTitle,
		///<summary>ServiceSavingMessage. </summary>
		ServiceSavingMessage,
		///<summary>ServiceProcessedTitle. </summary>
		ServiceProcessedTitle,
		///<summary>ServiceProcessedMessage. </summary>
		ServiceProcessedMessage,
		///<summary>ServiceFailedTitle. </summary>
		ServiceFailedTitle,
		///<summary>ServiceFailedMessage. </summary>
		ServiceFailedMessage,
		///<summary>OrderingNotAvailableMessage. </summary>
		OrderingNotAvailableMessage,
		///<summary>DailyOrderReset. </summary>
		DailyOrderReset,
		///<summary>SleepTime. </summary>
		SleepTime,
		///<summary>WakeUpTime. </summary>
		WakeUpTime,
		///<summary>OrderProcessingNotificationEnabled. </summary>
		OrderProcessingNotificationEnabled,
		///<summary>OrderProcessingNotificationTitle. </summary>
		OrderProcessingNotificationTitle,
		///<summary>OrderProcessingNotification. </summary>
		OrderProcessingNotification,
		///<summary>OrderProcessedNotificationEnabled. </summary>
		OrderProcessedNotificationEnabled,
		///<summary>OrderProcessedNotificationTitle. </summary>
		OrderProcessedNotificationTitle,
		///<summary>OrderProcessedNotification. </summary>
		OrderProcessedNotification,
		///<summary>FreeformMessageTitle. </summary>
		FreeformMessageTitle,
		///<summary>ServiceProcessingNotificationTitle. </summary>
		ServiceProcessingNotificationTitle,
		///<summary>ServiceProcessingNotification. </summary>
		ServiceProcessingNotification,
		///<summary>ServiceProcessedNotificationTitle. </summary>
		ServiceProcessedNotificationTitle,
		///<summary>ServiceProcessedNotification. </summary>
		ServiceProcessedNotification,
		///<summary>PreorderingEnabled. </summary>
		PreorderingEnabled,
		///<summary>PreorderingActiveMinutes. </summary>
		PreorderingActiveMinutes,
		///<summary>AnalyticsOrderingVisible. </summary>
		AnalyticsOrderingVisible,
		///<summary>AnalyticsBestsellersVisible. </summary>
		AnalyticsBestsellersVisible,
		///<summary>OrderingEnabled. </summary>
		OrderingEnabled,
		///<summary>HomepageSlideshowInterval. </summary>
		HomepageSlideshowInterval,
		///<summary>GooglePrinterId. </summary>
		GooglePrinterId,
		///<summary>GooglePrinterName. </summary>
		GooglePrinterName,
		///<summary>UIModeId. </summary>
		UIModeId,
		///<summary>MobileUIModeId. </summary>
		MobileUIModeId,
		///<summary>TabletUIModeId. </summary>
		TabletUIModeId,
		///<summary>PmsIntegration. </summary>
		PmsIntegration,
		///<summary>PmsAllowShowBill. </summary>
		PmsAllowShowBill,
		///<summary>PmsAllowExpressCheckout. </summary>
		PmsAllowExpressCheckout,
		///<summary>PmsAllowShowGuestName. </summary>
		PmsAllowShowGuestName,
		///<summary>PmsLockClientWhenNotCheckedIn. </summary>
		PmsLockClientWhenNotCheckedIn,
		///<summary>PmsDeviceLockedTitle. </summary>
		PmsDeviceLockedTitle,
		///<summary>PmsDeviceLockedText. </summary>
		PmsDeviceLockedText,
		///<summary>PmsCheckinFailedTitle. </summary>
		PmsCheckinFailedTitle,
		///<summary>PmsCheckinFailedText. </summary>
		PmsCheckinFailedText,
		///<summary>PmsRestartTitle. </summary>
		PmsRestartTitle,
		///<summary>PmsRestartText. </summary>
		PmsRestartText,
		///<summary>PmsWelcomeTitle. </summary>
		PmsWelcomeTitle,
		///<summary>PmsWelcomeText. </summary>
		PmsWelcomeText,
		///<summary>PmsCheckoutApproveText. </summary>
		PmsCheckoutApproveText,
		///<summary>PmsWelcomeTimeoutMinutes. </summary>
		PmsWelcomeTimeoutMinutes,
		///<summary>PmsCheckoutCompleteTitle. </summary>
		PmsCheckoutCompleteTitle,
		///<summary>PmsCheckoutCompleteText. </summary>
		PmsCheckoutCompleteText,
		///<summary>PmsCheckoutCompleteTimeoutMinutes. </summary>
		PmsCheckoutCompleteTimeoutMinutes,
		///<summary>PincodeSU. </summary>
		PincodeSU,
		///<summary>PincodeGM. </summary>
		PincodeGM,
		///<summary>DockedDimLevelDull. </summary>
		DockedDimLevelDull,
		///<summary>DockedDimLevelMedium. </summary>
		DockedDimLevelMedium,
		///<summary>DockedDimLevelBright. </summary>
		DockedDimLevelBright,
		///<summary>OrderHistoryDialogEnabled. </summary>
		OrderHistoryDialogEnabled,
		///<summary>IsChargerRemovedDialogEnabled. </summary>
		IsChargerRemovedDialogEnabled,
		///<summary>ChargerRemovedDialogTitle. </summary>
		ChargerRemovedDialogTitle,
		///<summary>ChargerRemovedDialogText. </summary>
		ChargerRemovedDialogText,
		///<summary>IsChargerRemovedReminderDialogEnabled. </summary>
		IsChargerRemovedReminderDialogEnabled,
		///<summary>ChargerRemovedReminderDialogTitle. </summary>
		ChargerRemovedReminderDialogTitle,
		///<summary>ChargerRemovedReminderDialogText. </summary>
		ChargerRemovedReminderDialogText,
		///<summary>PowerButtonHardBehaviour. </summary>
		PowerButtonHardBehaviour,
		///<summary>PowerButtonSoftBehaviour. </summary>
		PowerButtonSoftBehaviour,
		///<summary>ScreenOffMode. </summary>
		ScreenOffMode,
		///<summary>UseHardKeyboard. </summary>
		UseHardKeyboard,
		///<summary>RoomserviceCharge. </summary>
		RoomserviceCharge,
		///<summary>HotSOSBatteryLowProductId. </summary>
		HotSOSBatteryLowProductId,
		///<summary>EnableBatteryLowConsoleNotifications. </summary>
		EnableBatteryLowConsoleNotifications,
		///<summary>UIThemeId. </summary>
		UIThemeId,
		///<summary>ScreensaverMode. </summary>
		ScreensaverMode,
		///<summary>TurnOffPrivacyTitle. </summary>
		TurnOffPrivacyTitle,
		///<summary>TurnOffPrivacyText. </summary>
		TurnOffPrivacyText,
		///<summary>ItemCurrentlyUnavailableText. </summary>
		ItemCurrentlyUnavailableText,
		///<summary>EstimatedDeliveryTime. </summary>
		EstimatedDeliveryTime,
		///<summary>UIScheduleId. </summary>
		UIScheduleId,
		///<summary>AlarmSetWhileNotChargingTitle. </summary>
		AlarmSetWhileNotChargingTitle,
		///<summary>AlarmSetWhileNotChargingText. </summary>
		AlarmSetWhileNotChargingText,
		///<summary>ClearBasketTitle. </summary>
		ClearBasketTitle,
		///<summary>ClearBasketText. </summary>
		ClearBasketText,
		///<summary>PriceScheduleId. </summary>
		PriceScheduleId,
		///<summary>IsOrderitemAddedDialogEnabled. </summary>
		IsOrderitemAddedDialogEnabled,
		///<summary>IsClearBasketDialogEnabled. </summary>
		IsClearBasketDialogEnabled,
		///<summary>BrowserAgeVerificationEnabled. </summary>
		BrowserAgeVerificationEnabled,
		///<summary>BrowserAgeVerificationLayout. </summary>
		BrowserAgeVerificationLayout,
		///<summary>RestartApplicationDialogEnabled. </summary>
		RestartApplicationDialogEnabled,
		///<summary>RebootDeviceDialogEnabled. </summary>
		RebootDeviceDialogEnabled,
		///<summary>RestartTimeoutSeconds. </summary>
		RestartTimeoutSeconds,
		///<summary>CraveAnalytics. </summary>
		CraveAnalytics,
		///<summary>GeoFencingEnabled. </summary>
		GeoFencingEnabled,
		///<summary>GeoFencingRadius. </summary>
		GeoFencingRadius,
		///<summary>HideCompanyDetails. </summary>
		HideCompanyDetails,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>ApiVersion. </summary>
		ApiVersion,
		///<summary>MessagingVersion. </summary>
		MessagingVersion,
		///<summary>AffiliateCampaignId. </summary>
		AffiliateCampaignId,
		///<summary>AddressId. </summary>
		AddressId,
		/// <summary></summary>
		AmountOfFields
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Deliverypointgroup. </summary>
	public partial class DeliverypointgroupRelations: RelationFactory
	{
		#region Custom Relation code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomRelationCode
        private const ReferentialConstraintDeleteRule DeleteRuleForCheckoutMethodDeliverypointgroupEntityUsingDeliverypointgroupId = ReferentialConstraintDeleteRule.Cascade;
        private const ReferentialConstraintDeleteRule DeleteRuleForClientEntityUsingDeliverypointgroupId = ReferentialConstraintDeleteRule.NoAction;
		private const ReferentialConstraintDeleteRule DeleteRuleForClientConfigurationEntityUsingDeliverypointgroupId = ReferentialConstraintDeleteRule.NoAction;
		private const ReferentialConstraintDeleteRule DeleteRuleForDeliverypointEntityUsingDeliverypointgroupId = ReferentialConstraintDeleteRule.Cascade;
        private const ReferentialConstraintDeleteRule DeleteRuleForMediaEntityUsingDeliverypointgroupId = ReferentialConstraintDeleteRule.Cascade;
		private const ReferentialConstraintDeleteRule DeleteRuleForNetmessageEntityUsingReceiverDeliverypointgroupId = ReferentialConstraintDeleteRule.Cascade;
        private const ReferentialConstraintDeleteRule DeleteRuleForServiceMethodDeliverypointgroupEntityUsingDeliverypointgroupId = ReferentialConstraintDeleteRule.Cascade;
		private const ReferentialConstraintDeleteRule DeleteRuleForTerminalEntityUsingDeliverypointgroupId = ReferentialConstraintDeleteRule.NoAction;
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		
		// private const ReferentialConstraintDeleteRule DeleteRuleForCheckoutMethodDeliverypointgroupEntityUsingDeliverypointgroupId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForClientEntityUsingDeliverypointgroupId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForClientConfigurationEntityUsingDeliverypointgroupId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForDeliverypointEntityUsingDeliverypointgroupId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForMediaEntityUsingDeliverypointgroupId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForNetmessageEntityUsingReceiverDeliverypointgroupId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForServiceMethodDeliverypointgroupEntityUsingDeliverypointgroupId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForTerminalEntityUsingDeliverypointgroupId = ReferentialConstraintDeleteRule


		/// <summary>Gets all relations and delete rules of the DeliverypointgroupEntity as a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects.</summary>
		/// <returns>a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects</returns>
		public virtual Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> GetRelationsAndDeleteRules()
		{
			Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> toReturn = new Dictionary<IEntityRelation, ReferentialConstraintDeleteRule>();
			toReturn.Add(this.CheckoutMethodDeliverypointgroupEntityUsingDeliverypointgroupId, DeliverypointgroupRelations.DeleteRuleForCheckoutMethodDeliverypointgroupEntityUsingDeliverypointgroupId);
			toReturn.Add(this.ClientEntityUsingDeliverypointgroupId, DeliverypointgroupRelations.DeleteRuleForClientEntityUsingDeliverypointgroupId);
			toReturn.Add(this.ClientConfigurationEntityUsingDeliverypointgroupId, DeliverypointgroupRelations.DeleteRuleForClientConfigurationEntityUsingDeliverypointgroupId);
			toReturn.Add(this.DeliverypointEntityUsingDeliverypointgroupId, DeliverypointgroupRelations.DeleteRuleForDeliverypointEntityUsingDeliverypointgroupId);
			toReturn.Add(this.MediaEntityUsingDeliverypointgroupId, DeliverypointgroupRelations.DeleteRuleForMediaEntityUsingDeliverypointgroupId);
			toReturn.Add(this.NetmessageEntityUsingReceiverDeliverypointgroupId, DeliverypointgroupRelations.DeleteRuleForNetmessageEntityUsingReceiverDeliverypointgroupId);
			toReturn.Add(this.ServiceMethodDeliverypointgroupEntityUsingDeliverypointgroupId, DeliverypointgroupRelations.DeleteRuleForServiceMethodDeliverypointgroupEntityUsingDeliverypointgroupId);
			toReturn.Add(this.TerminalEntityUsingDeliverypointgroupId, DeliverypointgroupRelations.DeleteRuleForTerminalEntityUsingDeliverypointgroupId);
			return toReturn;
		}		
	
	
		/// <summary>Returns a new IEntityRelation object, between DeliverypointgroupEntity and CheckoutMethodDeliverypointgroupEntity over the 1:n relation they have, using the relation between the fields: Deliverypointgroup.DeliverypointgroupId - CheckoutMethodDeliverypointgroup.DeliverypointgroupId</summary>
		public virtual IEntityRelation CheckoutMethodDeliverypointgroupEntityUsingDeliverypointgroupId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "CheckoutMethodDeliverypointgroupCollection", true, new[] { DeliverypointgroupFields.DeliverypointgroupId, CheckoutMethodDeliverypointgroupFields.DeliverypointgroupId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between DeliverypointgroupEntity and ClientEntity over the 1:n relation they have, using the relation between the fields: Deliverypointgroup.DeliverypointgroupId - Client.DeliverypointgroupId</summary>
		public virtual IEntityRelation ClientEntityUsingDeliverypointgroupId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "ClientCollection", true, new[] { DeliverypointgroupFields.DeliverypointgroupId, ClientFields.DeliverypointgroupId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between DeliverypointgroupEntity and ClientConfigurationEntity over the 1:n relation they have, using the relation between the fields: Deliverypointgroup.DeliverypointgroupId - ClientConfiguration.DeliverypointgroupId</summary>
		public virtual IEntityRelation ClientConfigurationEntityUsingDeliverypointgroupId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "ClientConfigurationCollection", true, new[] { DeliverypointgroupFields.DeliverypointgroupId, ClientConfigurationFields.DeliverypointgroupId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between DeliverypointgroupEntity and DeliverypointEntity over the 1:n relation they have, using the relation between the fields: Deliverypointgroup.DeliverypointgroupId - Deliverypoint.DeliverypointgroupId</summary>
		public virtual IEntityRelation DeliverypointEntityUsingDeliverypointgroupId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "DeliverypointCollection", true, new[] { DeliverypointgroupFields.DeliverypointgroupId, DeliverypointFields.DeliverypointgroupId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between DeliverypointgroupEntity and MediaEntity over the 1:n relation they have, using the relation between the fields: Deliverypointgroup.DeliverypointgroupId - Media.DeliverypointgroupId</summary>
		public virtual IEntityRelation MediaEntityUsingDeliverypointgroupId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "MediaCollection", true, new[] { DeliverypointgroupFields.DeliverypointgroupId, MediaFields.DeliverypointgroupId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between DeliverypointgroupEntity and NetmessageEntity over the 1:n relation they have, using the relation between the fields: Deliverypointgroup.DeliverypointgroupId - Netmessage.ReceiverDeliverypointgroupId</summary>
		public virtual IEntityRelation NetmessageEntityUsingReceiverDeliverypointgroupId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "NetmessageCollection", true, new[] { DeliverypointgroupFields.DeliverypointgroupId, NetmessageFields.ReceiverDeliverypointgroupId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between DeliverypointgroupEntity and ServiceMethodDeliverypointgroupEntity over the 1:n relation they have, using the relation between the fields: Deliverypointgroup.DeliverypointgroupId - ServiceMethodDeliverypointgroup.DeliverypointgroupId</summary>
		public virtual IEntityRelation ServiceMethodDeliverypointgroupEntityUsingDeliverypointgroupId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "ServiceMethodDeliverypointgroupCollection", true, new[] { DeliverypointgroupFields.DeliverypointgroupId, ServiceMethodDeliverypointgroupFields.DeliverypointgroupId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between DeliverypointgroupEntity and TerminalEntity over the 1:n relation they have, using the relation between the fields: Deliverypointgroup.DeliverypointgroupId - Terminal.DeliverypointgroupId</summary>
		public virtual IEntityRelation TerminalEntityUsingDeliverypointgroupId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "TerminalCollection", true, new[] { DeliverypointgroupFields.DeliverypointgroupId, TerminalFields.DeliverypointgroupId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between DeliverypointgroupEntity and ClientConfigurationEntity over the m:1 relation they have, using the relation between the fields: Deliverypointgroup.ClientConfigurationId - ClientConfiguration.ClientConfigurationId</summary>
		public virtual IEntityRelation ClientConfigurationEntityUsingClientConfigurationId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "ClientConfiguration", false, new[] { ClientConfigurationFields.ClientConfigurationId, DeliverypointgroupFields.ClientConfigurationId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between DeliverypointgroupEntity and CompanyEntity over the m:1 relation they have, using the relation between the fields: Deliverypointgroup.CompanyId - Company.CompanyId</summary>
		public virtual IEntityRelation CompanyEntityUsingCompanyId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Company", false, new[] { CompanyFields.CompanyId, DeliverypointgroupFields.CompanyId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between DeliverypointgroupEntity and MenuEntity over the m:1 relation they have, using the relation between the fields: Deliverypointgroup.MenuId - Menu.MenuId</summary>
		public virtual IEntityRelation MenuEntityUsingMenuId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Menu", false, new[] { MenuFields.MenuId, DeliverypointgroupFields.MenuId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between DeliverypointgroupEntity and PriceScheduleEntity over the m:1 relation they have, using the relation between the fields: Deliverypointgroup.PriceScheduleId - PriceSchedule.PriceScheduleId</summary>
		public virtual IEntityRelation PriceScheduleEntityUsingPriceScheduleId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "PriceSchedule", false, new[] { PriceScheduleFields.PriceScheduleId, DeliverypointgroupFields.PriceScheduleId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between DeliverypointgroupEntity and ProductEntity over the m:1 relation they have, using the relation between the fields: Deliverypointgroup.HotSOSBatteryLowProductId - Product.ProductId</summary>
		public virtual IEntityRelation ProductEntityUsingHotSOSBatteryLowProductId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Product", false, new[] { ProductFields.ProductId, DeliverypointgroupFields.HotSOSBatteryLowProductId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between DeliverypointgroupEntity and RouteEntity over the m:1 relation they have, using the relation between the fields: Deliverypointgroup.EmailDocumentRouteId - Route.RouteId</summary>
		public virtual IEntityRelation RouteEntityUsingEmailDocumentRouteId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "EmailDocumentRoute", false, new[] { RouteFields.RouteId, DeliverypointgroupFields.EmailDocumentRouteId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between DeliverypointgroupEntity and RouteEntity over the m:1 relation they have, using the relation between the fields: Deliverypointgroup.OrderNotesRouteId - Route.RouteId</summary>
		public virtual IEntityRelation RouteEntityUsingOrderNotesRouteId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "OrderNotesRoute", false, new[] { RouteFields.RouteId, DeliverypointgroupFields.OrderNotesRouteId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between DeliverypointgroupEntity and RouteEntity over the m:1 relation they have, using the relation between the fields: Deliverypointgroup.RouteId - Route.RouteId</summary>
		public virtual IEntityRelation RouteEntityUsingRouteId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Route", false, new[] { RouteFields.RouteId, DeliverypointgroupFields.RouteId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between DeliverypointgroupEntity and RouteEntity over the m:1 relation they have, using the relation between the fields: Deliverypointgroup.SystemMessageRouteId - Route.RouteId</summary>
		public virtual IEntityRelation RouteEntityUsingSystemMessageRouteId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "SystemMessageRoute", false, new[] { RouteFields.RouteId, DeliverypointgroupFields.SystemMessageRouteId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between DeliverypointgroupEntity and TerminalEntity over the m:1 relation they have, using the relation between the fields: Deliverypointgroup.TerminalId - Terminal.TerminalId</summary>
		public virtual IEntityRelation TerminalEntityUsingTerminalId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Terminal", false, new[] { TerminalFields.TerminalId, DeliverypointgroupFields.TerminalId }); }
		}

	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticDeliverypointgroupRelations
	{
		internal static readonly IEntityRelation CheckoutMethodDeliverypointgroupEntityUsingDeliverypointgroupIdStatic = new DeliverypointgroupRelations().CheckoutMethodDeliverypointgroupEntityUsingDeliverypointgroupId;
		internal static readonly IEntityRelation ClientEntityUsingDeliverypointgroupIdStatic = new DeliverypointgroupRelations().ClientEntityUsingDeliverypointgroupId;
		internal static readonly IEntityRelation ClientConfigurationEntityUsingDeliverypointgroupIdStatic = new DeliverypointgroupRelations().ClientConfigurationEntityUsingDeliverypointgroupId;
		internal static readonly IEntityRelation DeliverypointEntityUsingDeliverypointgroupIdStatic = new DeliverypointgroupRelations().DeliverypointEntityUsingDeliverypointgroupId;
		internal static readonly IEntityRelation MediaEntityUsingDeliverypointgroupIdStatic = new DeliverypointgroupRelations().MediaEntityUsingDeliverypointgroupId;
		internal static readonly IEntityRelation NetmessageEntityUsingReceiverDeliverypointgroupIdStatic = new DeliverypointgroupRelations().NetmessageEntityUsingReceiverDeliverypointgroupId;
		internal static readonly IEntityRelation ServiceMethodDeliverypointgroupEntityUsingDeliverypointgroupIdStatic = new DeliverypointgroupRelations().ServiceMethodDeliverypointgroupEntityUsingDeliverypointgroupId;
		internal static readonly IEntityRelation TerminalEntityUsingDeliverypointgroupIdStatic = new DeliverypointgroupRelations().TerminalEntityUsingDeliverypointgroupId;
		internal static readonly IEntityRelation ClientConfigurationEntityUsingClientConfigurationIdStatic = new DeliverypointgroupRelations().ClientConfigurationEntityUsingClientConfigurationId;
		internal static readonly IEntityRelation CompanyEntityUsingCompanyIdStatic = new DeliverypointgroupRelations().CompanyEntityUsingCompanyId;
		internal static readonly IEntityRelation MenuEntityUsingMenuIdStatic = new DeliverypointgroupRelations().MenuEntityUsingMenuId;
		internal static readonly IEntityRelation PriceScheduleEntityUsingPriceScheduleIdStatic = new DeliverypointgroupRelations().PriceScheduleEntityUsingPriceScheduleId;
		internal static readonly IEntityRelation ProductEntityUsingHotSOSBatteryLowProductIdStatic = new DeliverypointgroupRelations().ProductEntityUsingHotSOSBatteryLowProductId;
		internal static readonly IEntityRelation RouteEntityUsingEmailDocumentRouteIdStatic = new DeliverypointgroupRelations().RouteEntityUsingEmailDocumentRouteId;
		internal static readonly IEntityRelation RouteEntityUsingOrderNotesRouteIdStatic = new DeliverypointgroupRelations().RouteEntityUsingOrderNotesRouteId;
		internal static readonly IEntityRelation RouteEntityUsingRouteIdStatic = new DeliverypointgroupRelations().RouteEntityUsingRouteId;
		internal static readonly IEntityRelation RouteEntityUsingSystemMessageRouteIdStatic = new DeliverypointgroupRelations().RouteEntityUsingSystemMessageRouteId;
		internal static readonly IEntityRelation TerminalEntityUsingTerminalIdStatic = new DeliverypointgroupRelations().TerminalEntityUsingTerminalId;

		/// <summary>CTor</summary>
		static StaticDeliverypointgroupRelations() { }
	}
}

