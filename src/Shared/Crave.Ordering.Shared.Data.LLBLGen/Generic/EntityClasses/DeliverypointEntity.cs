﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 5.8
// Code is generated on: 
// Code is generated using templates: Crave.TemplateBindings
// Templates vendor: Crave Interactive
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.FactoryClasses;
using Crave.Ordering.Shared.Data.LLBLGen.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

// ReSharper disable All
namespace Crave.Ordering.Shared.Data.LLBLGen.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'Deliverypoint'.<br/><br/></summary>
	[Serializable]
	public partial class DeliverypointEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END
	
	{
		private EntityCollection<ClientEntity> _clientCollection;
		private EntityCollection<ClientEntity> _clientCollection1;
		private EntityCollection<DeliverypointExternalDeliverypointEntity> _deliverypointExternalDeliverypointCollection;
		private EntityCollection<NetmessageEntity> _netmessageCollection;
		private EntityCollection<NetmessageEntity> _netmessageCollection1;
		private EntityCollection<OrderEntity> _orderCollectionViaCharge;
		private EntityCollection<OrderEntity> _orderCollection;
		private EntityCollection<TerminalEntity> _terminalCollection1;
		private EntityCollection<TerminalEntity> _terminalCollection;
		private ClientConfigurationEntity _clientConfiguration;
		private CompanyEntity _company;
		private DeliverypointgroupEntity _deliverypointgroup;
		private DeviceEntity _device;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		private static DeliverypointEntityStaticMetaData _staticMetaData = new DeliverypointEntityStaticMetaData();
		private static DeliverypointRelations _relationsFactory = new DeliverypointRelations();		

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name ClientConfiguration</summary>
			public static readonly string ClientConfiguration = "ClientConfiguration";
			/// <summary>Member name Company</summary>
			public static readonly string Company = "Company";
			/// <summary>Member name Deliverypointgroup</summary>
			public static readonly string Deliverypointgroup = "Deliverypointgroup";
			/// <summary>Member name Device</summary>
			public static readonly string Device = "Device";
			/// <summary>Member name ClientCollection</summary>
			public static readonly string ClientCollection = "ClientCollection";
			/// <summary>Member name ClientCollection1</summary>
			public static readonly string ClientCollection1 = "ClientCollection1";
			/// <summary>Member name DeliverypointExternalDeliverypointCollection</summary>
			public static readonly string DeliverypointExternalDeliverypointCollection = "DeliverypointExternalDeliverypointCollection";
			/// <summary>Member name NetmessageCollection</summary>
			public static readonly string NetmessageCollection = "NetmessageCollection";
			/// <summary>Member name NetmessageCollection1</summary>
			public static readonly string NetmessageCollection1 = "NetmessageCollection1";
			/// <summary>Member name OrderCollectionViaCharge</summary>
			public static readonly string OrderCollectionViaCharge = "OrderCollectionViaCharge";
			/// <summary>Member name OrderCollection</summary>
			public static readonly string OrderCollection = "OrderCollection";
			/// <summary>Member name TerminalCollection1</summary>
			public static readonly string TerminalCollection1 = "TerminalCollection1";
			/// <summary>Member name TerminalCollection</summary>
			public static readonly string TerminalCollection = "TerminalCollection";
		}

		/// <summary>Static meta-data storage for navigator related information</summary>
		protected class DeliverypointEntityStaticMetaData : EntityStaticMetaDataBase
		{
			public DeliverypointEntityStaticMetaData()
			{
				SetEntityCoreInfo("DeliverypointEntity", InheritanceHierarchyType.None, false, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.DeliverypointEntity, typeof(DeliverypointEntity), typeof(DeliverypointEntityFactory), false);
				AddNavigatorMetaData<DeliverypointEntity, EntityCollection<ClientEntity>>("ClientCollection", a => a._clientCollection, (a, b) => a._clientCollection = b, a => a.ClientCollection, () => new DeliverypointRelations().ClientEntityUsingDeliverypointId, typeof(ClientEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ClientEntity);
				AddNavigatorMetaData<DeliverypointEntity, EntityCollection<ClientEntity>>("ClientCollection1", a => a._clientCollection1, (a, b) => a._clientCollection1 = b, a => a.ClientCollection1, () => new DeliverypointRelations().ClientEntityUsingLastDeliverypointId, typeof(ClientEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ClientEntity);
				AddNavigatorMetaData<DeliverypointEntity, EntityCollection<DeliverypointExternalDeliverypointEntity>>("DeliverypointExternalDeliverypointCollection", a => a._deliverypointExternalDeliverypointCollection, (a, b) => a._deliverypointExternalDeliverypointCollection = b, a => a.DeliverypointExternalDeliverypointCollection, () => new DeliverypointRelations().DeliverypointExternalDeliverypointEntityUsingDeliverypointId, typeof(DeliverypointExternalDeliverypointEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.DeliverypointExternalDeliverypointEntity);
				AddNavigatorMetaData<DeliverypointEntity, EntityCollection<NetmessageEntity>>("NetmessageCollection", a => a._netmessageCollection, (a, b) => a._netmessageCollection = b, a => a.NetmessageCollection, () => new DeliverypointRelations().NetmessageEntityUsingSenderDeliverypointId, typeof(NetmessageEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.NetmessageEntity);
				AddNavigatorMetaData<DeliverypointEntity, EntityCollection<NetmessageEntity>>("NetmessageCollection1", a => a._netmessageCollection1, (a, b) => a._netmessageCollection1 = b, a => a.NetmessageCollection1, () => new DeliverypointRelations().NetmessageEntityUsingReceiverDeliverypointId, typeof(NetmessageEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.NetmessageEntity);
				AddNavigatorMetaData<DeliverypointEntity, EntityCollection<OrderEntity>>("OrderCollectionViaCharge", a => a._orderCollectionViaCharge, (a, b) => a._orderCollectionViaCharge = b, a => a.OrderCollectionViaCharge, () => new DeliverypointRelations().OrderEntityUsingChargeToDeliverypointId, typeof(OrderEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.OrderEntity);
				AddNavigatorMetaData<DeliverypointEntity, EntityCollection<OrderEntity>>("OrderCollection", a => a._orderCollection, (a, b) => a._orderCollection = b, a => a.OrderCollection, () => new DeliverypointRelations().OrderEntityUsingDeliverypointId, typeof(OrderEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.OrderEntity);
				AddNavigatorMetaData<DeliverypointEntity, EntityCollection<TerminalEntity>>("TerminalCollection1", a => a._terminalCollection1, (a, b) => a._terminalCollection1 = b, a => a.TerminalCollection1, () => new DeliverypointRelations().TerminalEntityUsingAltSystemMessagesDeliverypointId, typeof(TerminalEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.TerminalEntity);
				AddNavigatorMetaData<DeliverypointEntity, EntityCollection<TerminalEntity>>("TerminalCollection", a => a._terminalCollection, (a, b) => a._terminalCollection = b, a => a.TerminalCollection, () => new DeliverypointRelations().TerminalEntityUsingSystemMessagesDeliverypointId, typeof(TerminalEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.TerminalEntity);
				AddNavigatorMetaData<DeliverypointEntity, ClientConfigurationEntity>("ClientConfiguration", "DeliverypointCollection", (a, b) => a._clientConfiguration = b, a => a._clientConfiguration, (a, b) => a.ClientConfiguration = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticDeliverypointRelations.ClientConfigurationEntityUsingClientConfigurationIdStatic, ()=>new DeliverypointRelations().ClientConfigurationEntityUsingClientConfigurationId, null, new int[] { (int)DeliverypointFieldIndex.ClientConfigurationId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ClientConfigurationEntity);
				AddNavigatorMetaData<DeliverypointEntity, CompanyEntity>("Company", "DeliverypointCollection", (a, b) => a._company = b, a => a._company, (a, b) => a.Company = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticDeliverypointRelations.CompanyEntityUsingCompanyIdStatic, ()=>new DeliverypointRelations().CompanyEntityUsingCompanyId, null, new int[] { (int)DeliverypointFieldIndex.CompanyId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.CompanyEntity);
				AddNavigatorMetaData<DeliverypointEntity, DeliverypointgroupEntity>("Deliverypointgroup", "DeliverypointCollection", (a, b) => a._deliverypointgroup = b, a => a._deliverypointgroup, (a, b) => a.Deliverypointgroup = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticDeliverypointRelations.DeliverypointgroupEntityUsingDeliverypointgroupIdStatic, ()=>new DeliverypointRelations().DeliverypointgroupEntityUsingDeliverypointgroupId, null, new int[] { (int)DeliverypointFieldIndex.DeliverypointgroupId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.DeliverypointgroupEntity);
				AddNavigatorMetaData<DeliverypointEntity, DeviceEntity>("Device", "DeliverypointCollection", (a, b) => a._device = b, a => a._device, (a, b) => a.Device = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticDeliverypointRelations.DeviceEntityUsingDeviceIdStatic, ()=>new DeliverypointRelations().DeviceEntityUsingDeviceId, null, new int[] { (int)DeliverypointFieldIndex.DeviceId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.DeviceEntity);
			}
		}

		/// <summary>Static ctor</summary>
		static DeliverypointEntity()
		{
		}

		/// <summary> CTor</summary>
		public DeliverypointEntity()
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public DeliverypointEntity(IEntityFields2 fields)
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this DeliverypointEntity</param>
		public DeliverypointEntity(IValidator validator)
		{
			InitClassEmpty(validator, null);
		}

		/// <summary> CTor</summary>
		/// <param name="deliverypointId">PK value for Deliverypoint which data should be fetched into this Deliverypoint object</param>
		public DeliverypointEntity(System.Int32 deliverypointId) : this(deliverypointId, null)
		{
		}

		/// <summary> CTor</summary>
		/// <param name="deliverypointId">PK value for Deliverypoint which data should be fetched into this Deliverypoint object</param>
		/// <param name="validator">The custom validator object for this DeliverypointEntity</param>
		public DeliverypointEntity(System.Int32 deliverypointId, IValidator validator)
		{
			InitClassEmpty(validator, null);
			this.DeliverypointId = deliverypointId;
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected DeliverypointEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary>Method which will construct a filter (predicate expression) for the unique constraint defined on the fields: DeliverypointgroupId , Number .</summary>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public IPredicateExpression ConstructFilterForUCDeliverypointgroupIdNumber()
		{
			var filter = new PredicateExpression();
			filter.Add(Crave.Ordering.Shared.Data.LLBLGen.HelperClasses.DeliverypointFields.DeliverypointgroupId == this.Fields.GetCurrentValue((int)DeliverypointFieldIndex.DeliverypointgroupId));
			filter.Add(Crave.Ordering.Shared.Data.LLBLGen.HelperClasses.DeliverypointFields.Number == this.Fields.GetCurrentValue((int)DeliverypointFieldIndex.Number));
 			return filter;
		}

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Client' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoClientCollection() { return CreateRelationInfoForNavigator("ClientCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Client' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoClientCollection1() { return CreateRelationInfoForNavigator("ClientCollection1"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'DeliverypointExternalDeliverypoint' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoDeliverypointExternalDeliverypointCollection() { return CreateRelationInfoForNavigator("DeliverypointExternalDeliverypointCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Netmessage' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoNetmessageCollection() { return CreateRelationInfoForNavigator("NetmessageCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Netmessage' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoNetmessageCollection1() { return CreateRelationInfoForNavigator("NetmessageCollection1"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Order' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoOrderCollectionViaCharge() { return CreateRelationInfoForNavigator("OrderCollectionViaCharge"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Order' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoOrderCollection() { return CreateRelationInfoForNavigator("OrderCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Terminal' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoTerminalCollection1() { return CreateRelationInfoForNavigator("TerminalCollection1"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Terminal' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoTerminalCollection() { return CreateRelationInfoForNavigator("TerminalCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'ClientConfiguration' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoClientConfiguration() { return CreateRelationInfoForNavigator("ClientConfiguration"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Company' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoCompany() { return CreateRelationInfoForNavigator("Company"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Deliverypointgroup' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoDeliverypointgroup() { return CreateRelationInfoForNavigator("Deliverypointgroup"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Device' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoDevice() { return CreateRelationInfoForNavigator("Device"); }
		
		/// <inheritdoc/>
		protected override EntityStaticMetaDataBase GetEntityStaticMetaData() {	return _staticMetaData; }

		/// <summary>Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		/// <summary>Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this DeliverypointEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary>The relations object holding all relations of this entity with other entity classes.</summary>
		public static DeliverypointRelations Relations { get { return _relationsFactory; } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Client' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathClientCollection { get { return _staticMetaData.GetPrefetchPathElement("ClientCollection", CommonEntityBase.CreateEntityCollection<ClientEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Client' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathClientCollection1 { get { return _staticMetaData.GetPrefetchPathElement("ClientCollection1", CommonEntityBase.CreateEntityCollection<ClientEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'DeliverypointExternalDeliverypoint' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathDeliverypointExternalDeliverypointCollection { get { return _staticMetaData.GetPrefetchPathElement("DeliverypointExternalDeliverypointCollection", CommonEntityBase.CreateEntityCollection<DeliverypointExternalDeliverypointEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Netmessage' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathNetmessageCollection { get { return _staticMetaData.GetPrefetchPathElement("NetmessageCollection", CommonEntityBase.CreateEntityCollection<NetmessageEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Netmessage' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathNetmessageCollection1 { get { return _staticMetaData.GetPrefetchPathElement("NetmessageCollection1", CommonEntityBase.CreateEntityCollection<NetmessageEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Order' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathOrderCollectionViaCharge { get { return _staticMetaData.GetPrefetchPathElement("OrderCollectionViaCharge", CommonEntityBase.CreateEntityCollection<OrderEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Order' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathOrderCollection { get { return _staticMetaData.GetPrefetchPathElement("OrderCollection", CommonEntityBase.CreateEntityCollection<OrderEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Terminal' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathTerminalCollection1 { get { return _staticMetaData.GetPrefetchPathElement("TerminalCollection1", CommonEntityBase.CreateEntityCollection<TerminalEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Terminal' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathTerminalCollection { get { return _staticMetaData.GetPrefetchPathElement("TerminalCollection", CommonEntityBase.CreateEntityCollection<TerminalEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'ClientConfiguration' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathClientConfigurationEntity { get { return _staticMetaData.GetPrefetchPathElement("ClientConfiguration", CommonEntityBase.CreateEntityCollection<ClientConfigurationEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Company' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathCompanyEntity { get { return _staticMetaData.GetPrefetchPathElement("Company", CommonEntityBase.CreateEntityCollection<CompanyEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Deliverypointgroup' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathDeliverypointgroupEntity { get { return _staticMetaData.GetPrefetchPathElement("Deliverypointgroup", CommonEntityBase.CreateEntityCollection<DeliverypointgroupEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Device' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathDeviceEntity { get { return _staticMetaData.GetPrefetchPathElement("Device", CommonEntityBase.CreateEntityCollection<DeviceEntity>()); } }

		/// <summary>The DeliverypointId property of the Entity Deliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypoint"."DeliverypointId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 DeliverypointId
		{
			get { return (System.Int32)GetValue((int)DeliverypointFieldIndex.DeliverypointId, true); }
			set { SetValue((int)DeliverypointFieldIndex.DeliverypointId, value); }		}

		/// <summary>The DeliverypointgroupId property of the Entity Deliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypoint"."DeliverypointgroupId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 DeliverypointgroupId
		{
			get { return (System.Int32)GetValue((int)DeliverypointFieldIndex.DeliverypointgroupId, true); }
			set	{ SetValue((int)DeliverypointFieldIndex.DeliverypointgroupId, value); }
		}

		/// <summary>The DeviceId property of the Entity Deliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypoint"."DeviceId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> DeviceId
		{
			get { return (Nullable<System.Int32>)GetValue((int)DeliverypointFieldIndex.DeviceId, false); }
			set	{ SetValue((int)DeliverypointFieldIndex.DeviceId, value); }
		}

		/// <summary>The Name property of the Entity Deliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypoint"."Name".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)DeliverypointFieldIndex.Name, true); }
			set	{ SetValue((int)DeliverypointFieldIndex.Name, value); }
		}

		/// <summary>The Number property of the Entity Deliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypoint"."Number".<br/>Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 50.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Number
		{
			get { return (System.String)GetValue((int)DeliverypointFieldIndex.Number, true); }
			set	{ SetValue((int)DeliverypointFieldIndex.Number, value); }
		}

		/// <summary>The PosdeliverypointId property of the Entity Deliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypoint"."PosdeliverypointId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> PosdeliverypointId
		{
			get { return (Nullable<System.Int32>)GetValue((int)DeliverypointFieldIndex.PosdeliverypointId, false); }
			set	{ SetValue((int)DeliverypointFieldIndex.PosdeliverypointId, value); }
		}

		/// <summary>The CompanyId property of the Entity Deliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypoint"."CompanyId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CompanyId
		{
			get { return (System.Int32)GetValue((int)DeliverypointFieldIndex.CompanyId, true); }
			set	{ SetValue((int)DeliverypointFieldIndex.CompanyId, value); }
		}

		/// <summary>The RoomControllerType property of the Entity Deliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypoint"."RoomControllerType".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<Crave.Enums.RoomControllerType> RoomControllerType
		{
			get { return (Nullable<Crave.Enums.RoomControllerType>)GetValue((int)DeliverypointFieldIndex.RoomControllerType, false); }
			set	{ SetValue((int)DeliverypointFieldIndex.RoomControllerType, value); }
		}

		/// <summary>The RoomControllerIp property of the Entity Deliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypoint"."RoomControllerIp".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 250.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String RoomControllerIp
		{
			get { return (System.String)GetValue((int)DeliverypointFieldIndex.RoomControllerIp, true); }
			set	{ SetValue((int)DeliverypointFieldIndex.RoomControllerIp, value); }
		}

		/// <summary>The RoomControllerSlaveId property of the Entity Deliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypoint"."RoomControllerSlaveId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> RoomControllerSlaveId
		{
			get { return (Nullable<System.Int32>)GetValue((int)DeliverypointFieldIndex.RoomControllerSlaveId, false); }
			set	{ SetValue((int)DeliverypointFieldIndex.RoomControllerSlaveId, value); }
		}

		/// <summary>The GooglePrinterId property of the Entity Deliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypoint"."GooglePrinterId".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String GooglePrinterId
		{
			get { return (System.String)GetValue((int)DeliverypointFieldIndex.GooglePrinterId, true); }
			set	{ SetValue((int)DeliverypointFieldIndex.GooglePrinterId, value); }
		}

		/// <summary>The GooglePrinterName property of the Entity Deliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypoint"."GooglePrinterName".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String GooglePrinterName
		{
			get { return (System.String)GetValue((int)DeliverypointFieldIndex.GooglePrinterName, true); }
			set	{ SetValue((int)DeliverypointFieldIndex.GooglePrinterName, value); }
		}

		/// <summary>The HotSOSRoomId property of the Entity Deliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypoint"."HotSOSRoomId".<br/>Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 50.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String HotSOSRoomId
		{
			get { return (System.String)GetValue((int)DeliverypointFieldIndex.HotSOSRoomId, true); }
			set	{ SetValue((int)DeliverypointFieldIndex.HotSOSRoomId, value); }
		}

		/// <summary>The RoomControlConfigurationId property of the Entity Deliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypoint"."RoomControlConfigurationId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> RoomControlConfigurationId
		{
			get { return (Nullable<System.Int32>)GetValue((int)DeliverypointFieldIndex.RoomControlConfigurationId, false); }
			set	{ SetValue((int)DeliverypointFieldIndex.RoomControlConfigurationId, value); }
		}

		/// <summary>The ClientConfigurationId property of the Entity Deliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypoint"."ClientConfigurationId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ClientConfigurationId
		{
			get { return (Nullable<System.Int32>)GetValue((int)DeliverypointFieldIndex.ClientConfigurationId, false); }
			set	{ SetValue((int)DeliverypointFieldIndex.ClientConfigurationId, value); }
		}

		/// <summary>The CreatedBy property of the Entity Deliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypoint"."CreatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)DeliverypointFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)DeliverypointFieldIndex.CreatedBy, value); }
		}

		/// <summary>The UpdatedBy property of the Entity Deliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypoint"."UpdatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)DeliverypointFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)DeliverypointFieldIndex.UpdatedBy, value); }
		}

		/// <summary>The EnableAnalytics property of the Entity Deliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypoint"."EnableAnalytics".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean EnableAnalytics
		{
			get { return (System.Boolean)GetValue((int)DeliverypointFieldIndex.EnableAnalytics, true); }
			set	{ SetValue((int)DeliverypointFieldIndex.EnableAnalytics, value); }
		}

		/// <summary>The CreatedUTC property of the Entity Deliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypoint"."CreatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)DeliverypointFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)DeliverypointFieldIndex.CreatedUTC, value); }
		}

		/// <summary>The UpdatedUTC property of the Entity Deliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypoint"."UpdatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)DeliverypointFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)DeliverypointFieldIndex.UpdatedUTC, value); }
		}

		/// <summary>Gets the EntityCollection with the related entities of type 'ClientEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(ClientEntity))]
		public virtual EntityCollection<ClientEntity> ClientCollection { get { return GetOrCreateEntityCollection<ClientEntity, ClientEntityFactory>("Deliverypoint", true, false, ref _clientCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'ClientEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(ClientEntity))]
		public virtual EntityCollection<ClientEntity> ClientCollection1 { get { return GetOrCreateEntityCollection<ClientEntity, ClientEntityFactory>("DeliverypointEntity1", true, false, ref _clientCollection1); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'DeliverypointExternalDeliverypointEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(DeliverypointExternalDeliverypointEntity))]
		public virtual EntityCollection<DeliverypointExternalDeliverypointEntity> DeliverypointExternalDeliverypointCollection { get { return GetOrCreateEntityCollection<DeliverypointExternalDeliverypointEntity, DeliverypointExternalDeliverypointEntityFactory>("Deliverypoint", true, false, ref _deliverypointExternalDeliverypointCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'NetmessageEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(NetmessageEntity))]
		public virtual EntityCollection<NetmessageEntity> NetmessageCollection { get { return GetOrCreateEntityCollection<NetmessageEntity, NetmessageEntityFactory>("Deliverypoint", true, false, ref _netmessageCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'NetmessageEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(NetmessageEntity))]
		public virtual EntityCollection<NetmessageEntity> NetmessageCollection1 { get { return GetOrCreateEntityCollection<NetmessageEntity, NetmessageEntityFactory>("DeliverypointEntity1", true, false, ref _netmessageCollection1); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'OrderEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(OrderEntity))]
		public virtual EntityCollection<OrderEntity> OrderCollectionViaCharge { get { return GetOrCreateEntityCollection<OrderEntity, OrderEntityFactory>("ChargeToDeliverypoint", true, false, ref _orderCollectionViaCharge); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'OrderEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(OrderEntity))]
		public virtual EntityCollection<OrderEntity> OrderCollection { get { return GetOrCreateEntityCollection<OrderEntity, OrderEntityFactory>("Deliverypoint", true, false, ref _orderCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'TerminalEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(TerminalEntity))]
		public virtual EntityCollection<TerminalEntity> TerminalCollection1 { get { return GetOrCreateEntityCollection<TerminalEntity, TerminalEntityFactory>("AltSystemMessagesDeliverypoint", true, false, ref _terminalCollection1); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'TerminalEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(TerminalEntity))]
		public virtual EntityCollection<TerminalEntity> TerminalCollection { get { return GetOrCreateEntityCollection<TerminalEntity, TerminalEntityFactory>("SystemMessagesDeliverypoint", true, false, ref _terminalCollection); } }

		/// <summary>Gets / sets related entity of type 'ClientConfigurationEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual ClientConfigurationEntity ClientConfiguration
		{
			get { return _clientConfiguration; }
			set { SetSingleRelatedEntityNavigator(value, "ClientConfiguration"); }
		}

		/// <summary>Gets / sets related entity of type 'CompanyEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual CompanyEntity Company
		{
			get { return _company; }
			set { SetSingleRelatedEntityNavigator(value, "Company"); }
		}

		/// <summary>Gets / sets related entity of type 'DeliverypointgroupEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual DeliverypointgroupEntity Deliverypointgroup
		{
			get { return _deliverypointgroup; }
			set { SetSingleRelatedEntityNavigator(value, "Deliverypointgroup"); }
		}

		/// <summary>Gets / sets related entity of type 'DeviceEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual DeviceEntity Device
		{
			get { return _device; }
			set { SetSingleRelatedEntityNavigator(value, "Device"); }
		}


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen
{
	public enum DeliverypointFieldIndex
	{
		///<summary>DeliverypointId. </summary>
		DeliverypointId,
		///<summary>DeliverypointgroupId. </summary>
		DeliverypointgroupId,
		///<summary>DeviceId. </summary>
		DeviceId,
		///<summary>Name. </summary>
		Name,
		///<summary>Number. </summary>
		Number,
		///<summary>PosdeliverypointId. </summary>
		PosdeliverypointId,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>RoomControllerType. </summary>
		RoomControllerType,
		///<summary>RoomControllerIp. </summary>
		RoomControllerIp,
		///<summary>RoomControllerSlaveId. </summary>
		RoomControllerSlaveId,
		///<summary>GooglePrinterId. </summary>
		GooglePrinterId,
		///<summary>GooglePrinterName. </summary>
		GooglePrinterName,
		///<summary>HotSOSRoomId. </summary>
		HotSOSRoomId,
		///<summary>RoomControlConfigurationId. </summary>
		RoomControlConfigurationId,
		///<summary>ClientConfigurationId. </summary>
		ClientConfigurationId,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>EnableAnalytics. </summary>
		EnableAnalytics,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Deliverypoint. </summary>
	public partial class DeliverypointRelations: RelationFactory
	{
		#region Custom Relation code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomRelationCode
        private const ReferentialConstraintDeleteRule DeleteRuleForClientEntityUsingDeliverypointId = ReferentialConstraintDeleteRule.SetNull;
        private const ReferentialConstraintDeleteRule DeleteRuleForDeliverypointExternalDeliverypointEntityUsingDeliverypointId = ReferentialConstraintDeleteRule.Cascade;
        private const ReferentialConstraintDeleteRule DeleteRuleForClientEntityUsingLastDeliverypointId = ReferentialConstraintDeleteRule.SetNull;
        private const ReferentialConstraintDeleteRule DeleteRuleForNetmessageEntityUsingSenderDeliverypointId = ReferentialConstraintDeleteRule.Cascade;
        private const ReferentialConstraintDeleteRule DeleteRuleForNetmessageEntityUsingReceiverDeliverypointId = ReferentialConstraintDeleteRule.Cascade;
        private const ReferentialConstraintDeleteRule DeleteRuleForOrderEntityUsingChargeToDeliverypointId = ReferentialConstraintDeleteRule.SetNull;
        private const ReferentialConstraintDeleteRule DeleteRuleForOrderEntityUsingDeliverypointId = ReferentialConstraintDeleteRule.SetNull;
        private const ReferentialConstraintDeleteRule DeleteRuleForTerminalEntityUsingSystemMessagesDeliverypointId = ReferentialConstraintDeleteRule.SetNull;
        private const ReferentialConstraintDeleteRule DeleteRuleForTerminalEntityUsingAltSystemMessagesDeliverypointId = ReferentialConstraintDeleteRule.SetNull;
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		
		// private const ReferentialConstraintDeleteRule DeleteRuleForClientEntityUsingDeliverypointId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForClientEntityUsingLastDeliverypointId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForDeliverypointExternalDeliverypointEntityUsingDeliverypointId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForNetmessageEntityUsingSenderDeliverypointId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForNetmessageEntityUsingReceiverDeliverypointId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForOrderEntityUsingChargeToDeliverypointId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForOrderEntityUsingDeliverypointId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForTerminalEntityUsingAltSystemMessagesDeliverypointId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForTerminalEntityUsingSystemMessagesDeliverypointId = ReferentialConstraintDeleteRule


		/// <summary>Gets all relations and delete rules of the DeliverypointEntity as a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects.</summary>
		/// <returns>a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects</returns>
		public virtual Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> GetRelationsAndDeleteRules()
		{
			Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> toReturn = new Dictionary<IEntityRelation, ReferentialConstraintDeleteRule>();
			toReturn.Add(this.ClientEntityUsingDeliverypointId, DeliverypointRelations.DeleteRuleForClientEntityUsingDeliverypointId);
			toReturn.Add(this.ClientEntityUsingLastDeliverypointId, DeliverypointRelations.DeleteRuleForClientEntityUsingLastDeliverypointId);
			toReturn.Add(this.DeliverypointExternalDeliverypointEntityUsingDeliverypointId, DeliverypointRelations.DeleteRuleForDeliverypointExternalDeliverypointEntityUsingDeliverypointId);
			toReturn.Add(this.NetmessageEntityUsingSenderDeliverypointId, DeliverypointRelations.DeleteRuleForNetmessageEntityUsingSenderDeliverypointId);
			toReturn.Add(this.NetmessageEntityUsingReceiverDeliverypointId, DeliverypointRelations.DeleteRuleForNetmessageEntityUsingReceiverDeliverypointId);
			toReturn.Add(this.OrderEntityUsingChargeToDeliverypointId, DeliverypointRelations.DeleteRuleForOrderEntityUsingChargeToDeliverypointId);
			toReturn.Add(this.OrderEntityUsingDeliverypointId, DeliverypointRelations.DeleteRuleForOrderEntityUsingDeliverypointId);
			toReturn.Add(this.TerminalEntityUsingAltSystemMessagesDeliverypointId, DeliverypointRelations.DeleteRuleForTerminalEntityUsingAltSystemMessagesDeliverypointId);
			toReturn.Add(this.TerminalEntityUsingSystemMessagesDeliverypointId, DeliverypointRelations.DeleteRuleForTerminalEntityUsingSystemMessagesDeliverypointId);
			return toReturn;
		}		
	
	
		/// <summary>Returns a new IEntityRelation object, between DeliverypointEntity and ClientEntity over the 1:n relation they have, using the relation between the fields: Deliverypoint.DeliverypointId - Client.DeliverypointId</summary>
		public virtual IEntityRelation ClientEntityUsingDeliverypointId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "ClientCollection", true, new[] { DeliverypointFields.DeliverypointId, ClientFields.DeliverypointId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between DeliverypointEntity and ClientEntity over the 1:n relation they have, using the relation between the fields: Deliverypoint.DeliverypointId - Client.LastDeliverypointId</summary>
		public virtual IEntityRelation ClientEntityUsingLastDeliverypointId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "ClientCollection1", true, new[] { DeliverypointFields.DeliverypointId, ClientFields.LastDeliverypointId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between DeliverypointEntity and DeliverypointExternalDeliverypointEntity over the 1:n relation they have, using the relation between the fields: Deliverypoint.DeliverypointId - DeliverypointExternalDeliverypoint.DeliverypointId</summary>
		public virtual IEntityRelation DeliverypointExternalDeliverypointEntityUsingDeliverypointId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "DeliverypointExternalDeliverypointCollection", true, new[] { DeliverypointFields.DeliverypointId, DeliverypointExternalDeliverypointFields.DeliverypointId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between DeliverypointEntity and NetmessageEntity over the 1:n relation they have, using the relation between the fields: Deliverypoint.DeliverypointId - Netmessage.SenderDeliverypointId</summary>
		public virtual IEntityRelation NetmessageEntityUsingSenderDeliverypointId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "NetmessageCollection", true, new[] { DeliverypointFields.DeliverypointId, NetmessageFields.SenderDeliverypointId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between DeliverypointEntity and NetmessageEntity over the 1:n relation they have, using the relation between the fields: Deliverypoint.DeliverypointId - Netmessage.ReceiverDeliverypointId</summary>
		public virtual IEntityRelation NetmessageEntityUsingReceiverDeliverypointId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "NetmessageCollection1", true, new[] { DeliverypointFields.DeliverypointId, NetmessageFields.ReceiverDeliverypointId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between DeliverypointEntity and OrderEntity over the 1:n relation they have, using the relation between the fields: Deliverypoint.DeliverypointId - Order.ChargeToDeliverypointId</summary>
		public virtual IEntityRelation OrderEntityUsingChargeToDeliverypointId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "OrderCollectionViaCharge", true, new[] { DeliverypointFields.DeliverypointId, OrderFields.ChargeToDeliverypointId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between DeliverypointEntity and OrderEntity over the 1:n relation they have, using the relation between the fields: Deliverypoint.DeliverypointId - Order.DeliverypointId</summary>
		public virtual IEntityRelation OrderEntityUsingDeliverypointId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "OrderCollection", true, new[] { DeliverypointFields.DeliverypointId, OrderFields.DeliverypointId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between DeliverypointEntity and TerminalEntity over the 1:n relation they have, using the relation between the fields: Deliverypoint.DeliverypointId - Terminal.AltSystemMessagesDeliverypointId</summary>
		public virtual IEntityRelation TerminalEntityUsingAltSystemMessagesDeliverypointId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "TerminalCollection1", true, new[] { DeliverypointFields.DeliverypointId, TerminalFields.AltSystemMessagesDeliverypointId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between DeliverypointEntity and TerminalEntity over the 1:n relation they have, using the relation between the fields: Deliverypoint.DeliverypointId - Terminal.SystemMessagesDeliverypointId</summary>
		public virtual IEntityRelation TerminalEntityUsingSystemMessagesDeliverypointId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "TerminalCollection", true, new[] { DeliverypointFields.DeliverypointId, TerminalFields.SystemMessagesDeliverypointId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between DeliverypointEntity and ClientConfigurationEntity over the m:1 relation they have, using the relation between the fields: Deliverypoint.ClientConfigurationId - ClientConfiguration.ClientConfigurationId</summary>
		public virtual IEntityRelation ClientConfigurationEntityUsingClientConfigurationId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "ClientConfiguration", false, new[] { ClientConfigurationFields.ClientConfigurationId, DeliverypointFields.ClientConfigurationId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between DeliverypointEntity and CompanyEntity over the m:1 relation they have, using the relation between the fields: Deliverypoint.CompanyId - Company.CompanyId</summary>
		public virtual IEntityRelation CompanyEntityUsingCompanyId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Company", false, new[] { CompanyFields.CompanyId, DeliverypointFields.CompanyId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between DeliverypointEntity and DeliverypointgroupEntity over the m:1 relation they have, using the relation between the fields: Deliverypoint.DeliverypointgroupId - Deliverypointgroup.DeliverypointgroupId</summary>
		public virtual IEntityRelation DeliverypointgroupEntityUsingDeliverypointgroupId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Deliverypointgroup", false, new[] { DeliverypointgroupFields.DeliverypointgroupId, DeliverypointFields.DeliverypointgroupId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between DeliverypointEntity and DeviceEntity over the m:1 relation they have, using the relation between the fields: Deliverypoint.DeviceId - Device.DeviceId</summary>
		public virtual IEntityRelation DeviceEntityUsingDeviceId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Device", false, new[] { DeviceFields.DeviceId, DeliverypointFields.DeviceId }); }
		}

	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticDeliverypointRelations
	{
		internal static readonly IEntityRelation ClientEntityUsingDeliverypointIdStatic = new DeliverypointRelations().ClientEntityUsingDeliverypointId;
		internal static readonly IEntityRelation ClientEntityUsingLastDeliverypointIdStatic = new DeliverypointRelations().ClientEntityUsingLastDeliverypointId;
		internal static readonly IEntityRelation DeliverypointExternalDeliverypointEntityUsingDeliverypointIdStatic = new DeliverypointRelations().DeliverypointExternalDeliverypointEntityUsingDeliverypointId;
		internal static readonly IEntityRelation NetmessageEntityUsingSenderDeliverypointIdStatic = new DeliverypointRelations().NetmessageEntityUsingSenderDeliverypointId;
		internal static readonly IEntityRelation NetmessageEntityUsingReceiverDeliverypointIdStatic = new DeliverypointRelations().NetmessageEntityUsingReceiverDeliverypointId;
		internal static readonly IEntityRelation OrderEntityUsingChargeToDeliverypointIdStatic = new DeliverypointRelations().OrderEntityUsingChargeToDeliverypointId;
		internal static readonly IEntityRelation OrderEntityUsingDeliverypointIdStatic = new DeliverypointRelations().OrderEntityUsingDeliverypointId;
		internal static readonly IEntityRelation TerminalEntityUsingAltSystemMessagesDeliverypointIdStatic = new DeliverypointRelations().TerminalEntityUsingAltSystemMessagesDeliverypointId;
		internal static readonly IEntityRelation TerminalEntityUsingSystemMessagesDeliverypointIdStatic = new DeliverypointRelations().TerminalEntityUsingSystemMessagesDeliverypointId;
		internal static readonly IEntityRelation ClientConfigurationEntityUsingClientConfigurationIdStatic = new DeliverypointRelations().ClientConfigurationEntityUsingClientConfigurationId;
		internal static readonly IEntityRelation CompanyEntityUsingCompanyIdStatic = new DeliverypointRelations().CompanyEntityUsingCompanyId;
		internal static readonly IEntityRelation DeliverypointgroupEntityUsingDeliverypointgroupIdStatic = new DeliverypointRelations().DeliverypointgroupEntityUsingDeliverypointgroupId;
		internal static readonly IEntityRelation DeviceEntityUsingDeviceIdStatic = new DeliverypointRelations().DeviceEntityUsingDeviceId;

		/// <summary>CTor</summary>
		static StaticDeliverypointRelations() { }
	}
}

