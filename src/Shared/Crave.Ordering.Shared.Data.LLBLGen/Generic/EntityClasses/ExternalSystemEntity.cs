﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 5.8
// Code is generated on: 
// Code is generated using templates: Crave.TemplateBindings
// Templates vendor: Crave Interactive
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.FactoryClasses;
using Crave.Ordering.Shared.Data.LLBLGen.RelationClasses;
using System.Diagnostics;
using SD.LLBLGen.Pro.ORMSupportClasses;

// ReSharper disable All
namespace Crave.Ordering.Shared.Data.LLBLGen.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'ExternalSystem'.<br/><br/></summary>
	[Serializable]
	[DebuggerDisplay("ExternalSystemId={ExternalSystemId}, Name={Name}")]
	public partial class ExternalSystemEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END
	
	{
		private EntityCollection<ExternalDeliverypointEntity> _externalDeliverypointCollection;
		private EntityCollection<ExternalMenuEntity> _externalMenuCollection;
		private EntityCollection<ExternalProductEntity> _externalProductCollection;
		private EntityCollection<ExternalSystemLogEntity> _externalSystemLogCollection;
		private EntityCollection<OrderRoutestephandlerEntity> _orderRoutestephandlerCollection;
		private EntityCollection<OrderRoutestephandlerHistoryEntity> _orderRoutestephandlerHistoryCollection;
		private EntityCollection<RoutestephandlerEntity> _routestephandlerCollection;
		private CompanyEntity _company;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		private static ExternalSystemEntityStaticMetaData _staticMetaData = new ExternalSystemEntityStaticMetaData();
		private static ExternalSystemRelations _relationsFactory = new ExternalSystemRelations();		

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Company</summary>
			public static readonly string Company = "Company";
			/// <summary>Member name ExternalDeliverypointCollection</summary>
			public static readonly string ExternalDeliverypointCollection = "ExternalDeliverypointCollection";
			/// <summary>Member name ExternalMenuCollection</summary>
			public static readonly string ExternalMenuCollection = "ExternalMenuCollection";
			/// <summary>Member name ExternalProductCollection</summary>
			public static readonly string ExternalProductCollection = "ExternalProductCollection";
			/// <summary>Member name ExternalSystemLogCollection</summary>
			public static readonly string ExternalSystemLogCollection = "ExternalSystemLogCollection";
			/// <summary>Member name OrderRoutestephandlerCollection</summary>
			public static readonly string OrderRoutestephandlerCollection = "OrderRoutestephandlerCollection";
			/// <summary>Member name OrderRoutestephandlerHistoryCollection</summary>
			public static readonly string OrderRoutestephandlerHistoryCollection = "OrderRoutestephandlerHistoryCollection";
			/// <summary>Member name RoutestephandlerCollection</summary>
			public static readonly string RoutestephandlerCollection = "RoutestephandlerCollection";
		}

		/// <summary>Static meta-data storage for navigator related information</summary>
		protected class ExternalSystemEntityStaticMetaData : EntityStaticMetaDataBase
		{
			public ExternalSystemEntityStaticMetaData()
			{
				SetEntityCoreInfo("ExternalSystemEntity", InheritanceHierarchyType.None, false, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ExternalSystemEntity, typeof(ExternalSystemEntity), typeof(ExternalSystemEntityFactory), false);
				AddNavigatorMetaData<ExternalSystemEntity, EntityCollection<ExternalDeliverypointEntity>>("ExternalDeliverypointCollection", a => a._externalDeliverypointCollection, (a, b) => a._externalDeliverypointCollection = b, a => a.ExternalDeliverypointCollection, () => new ExternalSystemRelations().ExternalDeliverypointEntityUsingExternalSystemId, typeof(ExternalDeliverypointEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ExternalDeliverypointEntity);
				AddNavigatorMetaData<ExternalSystemEntity, EntityCollection<ExternalMenuEntity>>("ExternalMenuCollection", a => a._externalMenuCollection, (a, b) => a._externalMenuCollection = b, a => a.ExternalMenuCollection, () => new ExternalSystemRelations().ExternalMenuEntityUsingExternalSystemId, typeof(ExternalMenuEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ExternalMenuEntity);
				AddNavigatorMetaData<ExternalSystemEntity, EntityCollection<ExternalProductEntity>>("ExternalProductCollection", a => a._externalProductCollection, (a, b) => a._externalProductCollection = b, a => a.ExternalProductCollection, () => new ExternalSystemRelations().ExternalProductEntityUsingExternalSystemId, typeof(ExternalProductEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ExternalProductEntity);
				AddNavigatorMetaData<ExternalSystemEntity, EntityCollection<ExternalSystemLogEntity>>("ExternalSystemLogCollection", a => a._externalSystemLogCollection, (a, b) => a._externalSystemLogCollection = b, a => a.ExternalSystemLogCollection, () => new ExternalSystemRelations().ExternalSystemLogEntityUsingExternalSystemId, typeof(ExternalSystemLogEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.ExternalSystemLogEntity);
				AddNavigatorMetaData<ExternalSystemEntity, EntityCollection<OrderRoutestephandlerEntity>>("OrderRoutestephandlerCollection", a => a._orderRoutestephandlerCollection, (a, b) => a._orderRoutestephandlerCollection = b, a => a.OrderRoutestephandlerCollection, () => new ExternalSystemRelations().OrderRoutestephandlerEntityUsingExternalSystemId, typeof(OrderRoutestephandlerEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.OrderRoutestephandlerEntity);
				AddNavigatorMetaData<ExternalSystemEntity, EntityCollection<OrderRoutestephandlerHistoryEntity>>("OrderRoutestephandlerHistoryCollection", a => a._orderRoutestephandlerHistoryCollection, (a, b) => a._orderRoutestephandlerHistoryCollection = b, a => a.OrderRoutestephandlerHistoryCollection, () => new ExternalSystemRelations().OrderRoutestephandlerHistoryEntityUsingExternalSystemId, typeof(OrderRoutestephandlerHistoryEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.OrderRoutestephandlerHistoryEntity);
				AddNavigatorMetaData<ExternalSystemEntity, EntityCollection<RoutestephandlerEntity>>("RoutestephandlerCollection", a => a._routestephandlerCollection, (a, b) => a._routestephandlerCollection = b, a => a.RoutestephandlerCollection, () => new ExternalSystemRelations().RoutestephandlerEntityUsingExternalSystemId, typeof(RoutestephandlerEntity), (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.RoutestephandlerEntity);
				AddNavigatorMetaData<ExternalSystemEntity, CompanyEntity>("Company", "ExternalSystemCollection", (a, b) => a._company = b, a => a._company, (a, b) => a.Company = b, Crave.Ordering.Shared.Data.LLBLGen.RelationClasses.StaticExternalSystemRelations.CompanyEntityUsingCompanyIdStatic, ()=>new ExternalSystemRelations().CompanyEntityUsingCompanyId, null, new int[] { (int)ExternalSystemFieldIndex.CompanyId }, null, true, (int)Crave.Ordering.Shared.Data.LLBLGen.EntityType.CompanyEntity);
			}
		}

		/// <summary>Static ctor</summary>
		static ExternalSystemEntity()
		{
		}

		/// <summary> CTor</summary>
		public ExternalSystemEntity()
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public ExternalSystemEntity(IEntityFields2 fields)
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this ExternalSystemEntity</param>
		public ExternalSystemEntity(IValidator validator)
		{
			InitClassEmpty(validator, null);
		}

		/// <summary> CTor</summary>
		/// <param name="externalSystemId">PK value for ExternalSystem which data should be fetched into this ExternalSystem object</param>
		public ExternalSystemEntity(System.Int32 externalSystemId) : this(externalSystemId, null)
		{
		}

		/// <summary> CTor</summary>
		/// <param name="externalSystemId">PK value for ExternalSystem which data should be fetched into this ExternalSystem object</param>
		/// <param name="validator">The custom validator object for this ExternalSystemEntity</param>
		public ExternalSystemEntity(System.Int32 externalSystemId, IValidator validator)
		{
			InitClassEmpty(validator, null);
			this.ExternalSystemId = externalSystemId;
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ExternalSystemEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'ExternalDeliverypoint' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoExternalDeliverypointCollection() { return CreateRelationInfoForNavigator("ExternalDeliverypointCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'ExternalMenu' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoExternalMenuCollection() { return CreateRelationInfoForNavigator("ExternalMenuCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'ExternalProduct' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoExternalProductCollection() { return CreateRelationInfoForNavigator("ExternalProductCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'ExternalSystemLog' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoExternalSystemLogCollection() { return CreateRelationInfoForNavigator("ExternalSystemLogCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'OrderRoutestephandler' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoOrderRoutestephandlerCollection() { return CreateRelationInfoForNavigator("OrderRoutestephandlerCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'OrderRoutestephandlerHistory' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoOrderRoutestephandlerHistoryCollection() { return CreateRelationInfoForNavigator("OrderRoutestephandlerHistoryCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Routestephandler' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoRoutestephandlerCollection() { return CreateRelationInfoForNavigator("RoutestephandlerCollection"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Company' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoCompany() { return CreateRelationInfoForNavigator("Company"); }
		
		/// <inheritdoc/>
		protected override EntityStaticMetaDataBase GetEntityStaticMetaData() {	return _staticMetaData; }

		/// <summary>Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		/// <summary>Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this ExternalSystemEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary>The relations object holding all relations of this entity with other entity classes.</summary>
		public static ExternalSystemRelations Relations { get { return _relationsFactory; } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'ExternalDeliverypoint' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathExternalDeliverypointCollection { get { return _staticMetaData.GetPrefetchPathElement("ExternalDeliverypointCollection", CommonEntityBase.CreateEntityCollection<ExternalDeliverypointEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'ExternalMenu' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathExternalMenuCollection { get { return _staticMetaData.GetPrefetchPathElement("ExternalMenuCollection", CommonEntityBase.CreateEntityCollection<ExternalMenuEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'ExternalProduct' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathExternalProductCollection { get { return _staticMetaData.GetPrefetchPathElement("ExternalProductCollection", CommonEntityBase.CreateEntityCollection<ExternalProductEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'ExternalSystemLog' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathExternalSystemLogCollection { get { return _staticMetaData.GetPrefetchPathElement("ExternalSystemLogCollection", CommonEntityBase.CreateEntityCollection<ExternalSystemLogEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'OrderRoutestephandler' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathOrderRoutestephandlerCollection { get { return _staticMetaData.GetPrefetchPathElement("OrderRoutestephandlerCollection", CommonEntityBase.CreateEntityCollection<OrderRoutestephandlerEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'OrderRoutestephandlerHistory' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathOrderRoutestephandlerHistoryCollection { get { return _staticMetaData.GetPrefetchPathElement("OrderRoutestephandlerHistoryCollection", CommonEntityBase.CreateEntityCollection<OrderRoutestephandlerHistoryEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Routestephandler' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathRoutestephandlerCollection { get { return _staticMetaData.GetPrefetchPathElement("RoutestephandlerCollection", CommonEntityBase.CreateEntityCollection<RoutestephandlerEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Company' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathCompanyEntity { get { return _staticMetaData.GetPrefetchPathElement("Company", CommonEntityBase.CreateEntityCollection<CompanyEntity>()); } }

		/// <summary>The ExternalSystemId property of the Entity ExternalSystem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalSystem"."ExternalSystemId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 ExternalSystemId
		{
			get { return (System.Int32)GetValue((int)ExternalSystemFieldIndex.ExternalSystemId, true); }
			set { SetValue((int)ExternalSystemFieldIndex.ExternalSystemId, value); }		}

		/// <summary>The CompanyId property of the Entity ExternalSystem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalSystem"."CompanyId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CompanyId
		{
			get { return (System.Int32)GetValue((int)ExternalSystemFieldIndex.CompanyId, true); }
			set	{ SetValue((int)ExternalSystemFieldIndex.CompanyId, value); }
		}

		/// <summary>The Name property of the Entity ExternalSystem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalSystem"."Name".<br/>Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 255.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)ExternalSystemFieldIndex.Name, true); }
			set	{ SetValue((int)ExternalSystemFieldIndex.Name, value); }
		}

		/// <summary>The Type property of the Entity ExternalSystem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalSystem"."Type".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Crave.Enums.ExternalSystemType Type
		{
			get { return (Crave.Enums.ExternalSystemType)GetValue((int)ExternalSystemFieldIndex.Type, true); }
			set	{ SetValue((int)ExternalSystemFieldIndex.Type, value); }
		}

		/// <summary>The StringValue1 property of the Entity ExternalSystem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalSystem"."StringValue1".<br/>Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String StringValue1
		{
			get { return (System.String)GetValue((int)ExternalSystemFieldIndex.StringValue1, true); }
			set	{ SetValue((int)ExternalSystemFieldIndex.StringValue1, value); }
		}

		/// <summary>The StringValue2 property of the Entity ExternalSystem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalSystem"."StringValue2".<br/>Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String StringValue2
		{
			get { return (System.String)GetValue((int)ExternalSystemFieldIndex.StringValue2, true); }
			set	{ SetValue((int)ExternalSystemFieldIndex.StringValue2, value); }
		}

		/// <summary>The StringValue3 property of the Entity ExternalSystem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalSystem"."StringValue3".<br/>Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String StringValue3
		{
			get { return (System.String)GetValue((int)ExternalSystemFieldIndex.StringValue3, true); }
			set	{ SetValue((int)ExternalSystemFieldIndex.StringValue3, value); }
		}

		/// <summary>The StringValue4 property of the Entity ExternalSystem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalSystem"."StringValue4".<br/>Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String StringValue4
		{
			get { return (System.String)GetValue((int)ExternalSystemFieldIndex.StringValue4, true); }
			set	{ SetValue((int)ExternalSystemFieldIndex.StringValue4, value); }
		}

		/// <summary>The StringValue5 property of the Entity ExternalSystem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalSystem"."StringValue5".<br/>Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String StringValue5
		{
			get { return (System.String)GetValue((int)ExternalSystemFieldIndex.StringValue5, true); }
			set	{ SetValue((int)ExternalSystemFieldIndex.StringValue5, value); }
		}

		/// <summary>The ExpiryDateUtc property of the Entity ExternalSystem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalSystem"."ExpiryDateUtc".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ExpiryDateUtc
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ExternalSystemFieldIndex.ExpiryDateUtc, false); }
			set	{ SetValue((int)ExternalSystemFieldIndex.ExpiryDateUtc, value); }
		}

		/// <summary>The CreatedUTC property of the Entity ExternalSystem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalSystem"."CreatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime CreatedUTC
		{
			get { return (System.DateTime)GetValue((int)ExternalSystemFieldIndex.CreatedUTC, true); }
			set	{ SetValue((int)ExternalSystemFieldIndex.CreatedUTC, value); }
		}

		/// <summary>The CreatedBy property of the Entity ExternalSystem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalSystem"."CreatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CreatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)ExternalSystemFieldIndex.CreatedBy, false); }
			set	{ SetValue((int)ExternalSystemFieldIndex.CreatedBy, value); }
		}

		/// <summary>The UpdatedUTC property of the Entity ExternalSystem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalSystem"."UpdatedUTC".<br/>Table field type characteristics (type, precision, scale, length): DateTime2, 7, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ExternalSystemFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)ExternalSystemFieldIndex.UpdatedUTC, value); }
		}

		/// <summary>The UpdatedBy property of the Entity ExternalSystem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalSystem"."UpdatedBy".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UpdatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)ExternalSystemFieldIndex.UpdatedBy, false); }
			set	{ SetValue((int)ExternalSystemFieldIndex.UpdatedBy, value); }
		}

		/// <summary>The Environment property of the Entity ExternalSystem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalSystem"."Environment".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Crave.Enums.ExternalSystemEnvironment Environment
		{
			get { return (Crave.Enums.ExternalSystemEnvironment)GetValue((int)ExternalSystemFieldIndex.Environment, true); }
			set	{ SetValue((int)ExternalSystemFieldIndex.Environment, value); }
		}

		/// <summary>The BoolValue1 property of the Entity ExternalSystem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalSystem"."BoolValue1".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> BoolValue1
		{
			get { return (Nullable<System.Boolean>)GetValue((int)ExternalSystemFieldIndex.BoolValue1, false); }
			set	{ SetValue((int)ExternalSystemFieldIndex.BoolValue1, value); }
		}

		/// <summary>The BoolValue2 property of the Entity ExternalSystem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalSystem"."BoolValue2".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> BoolValue2
		{
			get { return (Nullable<System.Boolean>)GetValue((int)ExternalSystemFieldIndex.BoolValue2, false); }
			set	{ SetValue((int)ExternalSystemFieldIndex.BoolValue2, value); }
		}

		/// <summary>The BoolValue3 property of the Entity ExternalSystem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalSystem"."BoolValue3".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> BoolValue3
		{
			get { return (Nullable<System.Boolean>)GetValue((int)ExternalSystemFieldIndex.BoolValue3, false); }
			set	{ SetValue((int)ExternalSystemFieldIndex.BoolValue3, value); }
		}

		/// <summary>The BoolValue4 property of the Entity ExternalSystem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalSystem"."BoolValue4".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> BoolValue4
		{
			get { return (Nullable<System.Boolean>)GetValue((int)ExternalSystemFieldIndex.BoolValue4, false); }
			set	{ SetValue((int)ExternalSystemFieldIndex.BoolValue4, value); }
		}

		/// <summary>The BoolValue5 property of the Entity ExternalSystem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalSystem"."BoolValue5".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> BoolValue5
		{
			get { return (Nullable<System.Boolean>)GetValue((int)ExternalSystemFieldIndex.BoolValue5, false); }
			set	{ SetValue((int)ExternalSystemFieldIndex.BoolValue5, value); }
		}

		/// <summary>The IsBusy property of the Entity ExternalSystem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalSystem"."IsBusy".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsBusy
		{
			get { return (System.Boolean)GetValue((int)ExternalSystemFieldIndex.IsBusy, true); }
			set	{ SetValue((int)ExternalSystemFieldIndex.IsBusy, value); }
		}

		/// <summary>Gets the EntityCollection with the related entities of type 'ExternalDeliverypointEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(ExternalDeliverypointEntity))]
		public virtual EntityCollection<ExternalDeliverypointEntity> ExternalDeliverypointCollection { get { return GetOrCreateEntityCollection<ExternalDeliverypointEntity, ExternalDeliverypointEntityFactory>("ExternalSystem", true, false, ref _externalDeliverypointCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'ExternalMenuEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(ExternalMenuEntity))]
		public virtual EntityCollection<ExternalMenuEntity> ExternalMenuCollection { get { return GetOrCreateEntityCollection<ExternalMenuEntity, ExternalMenuEntityFactory>("ExternalSystem", true, false, ref _externalMenuCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'ExternalProductEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(ExternalProductEntity))]
		public virtual EntityCollection<ExternalProductEntity> ExternalProductCollection { get { return GetOrCreateEntityCollection<ExternalProductEntity, ExternalProductEntityFactory>("ExternalSystem", true, false, ref _externalProductCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'ExternalSystemLogEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(ExternalSystemLogEntity))]
		public virtual EntityCollection<ExternalSystemLogEntity> ExternalSystemLogCollection { get { return GetOrCreateEntityCollection<ExternalSystemLogEntity, ExternalSystemLogEntityFactory>("ExternalSystem", true, false, ref _externalSystemLogCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'OrderRoutestephandlerEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(OrderRoutestephandlerEntity))]
		public virtual EntityCollection<OrderRoutestephandlerEntity> OrderRoutestephandlerCollection { get { return GetOrCreateEntityCollection<OrderRoutestephandlerEntity, OrderRoutestephandlerEntityFactory>("ExternalSystem", true, false, ref _orderRoutestephandlerCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'OrderRoutestephandlerHistoryEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(OrderRoutestephandlerHistoryEntity))]
		public virtual EntityCollection<OrderRoutestephandlerHistoryEntity> OrderRoutestephandlerHistoryCollection { get { return GetOrCreateEntityCollection<OrderRoutestephandlerHistoryEntity, OrderRoutestephandlerHistoryEntityFactory>("ExternalSystem", true, false, ref _orderRoutestephandlerHistoryCollection); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'RoutestephandlerEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(RoutestephandlerEntity))]
		public virtual EntityCollection<RoutestephandlerEntity> RoutestephandlerCollection { get { return GetOrCreateEntityCollection<RoutestephandlerEntity, RoutestephandlerEntityFactory>("ExternalSystem", true, false, ref _routestephandlerCollection); } }

		/// <summary>Gets / sets related entity of type 'CompanyEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual CompanyEntity Company
		{
			get { return _company; }
			set { SetSingleRelatedEntityNavigator(value, "Company"); }
		}


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen
{
	public enum ExternalSystemFieldIndex
	{
		///<summary>ExternalSystemId. </summary>
		ExternalSystemId,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>Name. </summary>
		Name,
		///<summary>Type. </summary>
		Type,
		///<summary>StringValue1. </summary>
		StringValue1,
		///<summary>StringValue2. </summary>
		StringValue2,
		///<summary>StringValue3. </summary>
		StringValue3,
		///<summary>StringValue4. </summary>
		StringValue4,
		///<summary>StringValue5. </summary>
		StringValue5,
		///<summary>ExpiryDateUtc. </summary>
		ExpiryDateUtc,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>Environment. </summary>
		Environment,
		///<summary>BoolValue1. </summary>
		BoolValue1,
		///<summary>BoolValue2. </summary>
		BoolValue2,
		///<summary>BoolValue3. </summary>
		BoolValue3,
		///<summary>BoolValue4. </summary>
		BoolValue4,
		///<summary>BoolValue5. </summary>
		BoolValue5,
		///<summary>IsBusy. </summary>
		IsBusy,
		/// <summary></summary>
		AmountOfFields
	}
}

namespace Crave.Ordering.Shared.Data.LLBLGen.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: ExternalSystem. </summary>
	public partial class ExternalSystemRelations: RelationFactory
	{
		#region Custom Relation code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomRelationCode
		private const ReferentialConstraintDeleteRule DeleteRuleForExternalDeliverypointEntityUsingExternalSystemId = ReferentialConstraintDeleteRule.Cascade;
		private const ReferentialConstraintDeleteRule DeleteRuleForExternalProductEntityUsingExternalSystemId = ReferentialConstraintDeleteRule.Cascade;
		private const ReferentialConstraintDeleteRule DeleteRuleForExternalSystemLogEntityUsingExternalSystemId = ReferentialConstraintDeleteRule.Cascade;
		private const ReferentialConstraintDeleteRule DeleteRuleForOrderRoutestephandlerEntityUsingExternalSystemId = ReferentialConstraintDeleteRule.NoAction;
		private const ReferentialConstraintDeleteRule DeleteRuleForOrderRoutestephandlerHistoryEntityUsingExternalSystemId = ReferentialConstraintDeleteRule.NoAction;
		private const ReferentialConstraintDeleteRule DeleteRuleForRoutestephandlerEntityUsingExternalSystemId = ReferentialConstraintDeleteRule.NoAction;
		private const ReferentialConstraintDeleteRule DeleteRuleForExternalMenuEntityUsingExternalSystemId = ReferentialConstraintDeleteRule.Cascade;
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		
		// private const ReferentialConstraintDeleteRule DeleteRuleForExternalDeliverypointEntityUsingExternalSystemId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForExternalMenuEntityUsingExternalSystemId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForExternalProductEntityUsingExternalSystemId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForExternalSystemLogEntityUsingExternalSystemId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForOrderRoutestephandlerEntityUsingExternalSystemId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForOrderRoutestephandlerHistoryEntityUsingExternalSystemId = ReferentialConstraintDeleteRule
		// private const ReferentialConstraintDeleteRule DeleteRuleForRoutestephandlerEntityUsingExternalSystemId = ReferentialConstraintDeleteRule


		/// <summary>Gets all relations and delete rules of the ExternalSystemEntity as a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects.</summary>
		/// <returns>a dictionary of IEntityRelation and ReferentialConstraintDeleteRule objects</returns>
		public virtual Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> GetRelationsAndDeleteRules()
		{
			Dictionary<IEntityRelation, ReferentialConstraintDeleteRule> toReturn = new Dictionary<IEntityRelation, ReferentialConstraintDeleteRule>();
			toReturn.Add(this.ExternalDeliverypointEntityUsingExternalSystemId, ExternalSystemRelations.DeleteRuleForExternalDeliverypointEntityUsingExternalSystemId);
			toReturn.Add(this.ExternalMenuEntityUsingExternalSystemId, ExternalSystemRelations.DeleteRuleForExternalMenuEntityUsingExternalSystemId);
			toReturn.Add(this.ExternalProductEntityUsingExternalSystemId, ExternalSystemRelations.DeleteRuleForExternalProductEntityUsingExternalSystemId);
			toReturn.Add(this.ExternalSystemLogEntityUsingExternalSystemId, ExternalSystemRelations.DeleteRuleForExternalSystemLogEntityUsingExternalSystemId);
			toReturn.Add(this.OrderRoutestephandlerEntityUsingExternalSystemId, ExternalSystemRelations.DeleteRuleForOrderRoutestephandlerEntityUsingExternalSystemId);
			toReturn.Add(this.OrderRoutestephandlerHistoryEntityUsingExternalSystemId, ExternalSystemRelations.DeleteRuleForOrderRoutestephandlerHistoryEntityUsingExternalSystemId);
			toReturn.Add(this.RoutestephandlerEntityUsingExternalSystemId, ExternalSystemRelations.DeleteRuleForRoutestephandlerEntityUsingExternalSystemId);
			return toReturn;
		}		
	
	
		/// <summary>Returns a new IEntityRelation object, between ExternalSystemEntity and ExternalDeliverypointEntity over the 1:n relation they have, using the relation between the fields: ExternalSystem.ExternalSystemId - ExternalDeliverypoint.ExternalSystemId</summary>
		public virtual IEntityRelation ExternalDeliverypointEntityUsingExternalSystemId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "ExternalDeliverypointCollection", true, new[] { ExternalSystemFields.ExternalSystemId, ExternalDeliverypointFields.ExternalSystemId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between ExternalSystemEntity and ExternalMenuEntity over the 1:n relation they have, using the relation between the fields: ExternalSystem.ExternalSystemId - ExternalMenu.ExternalSystemId</summary>
		public virtual IEntityRelation ExternalMenuEntityUsingExternalSystemId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "ExternalMenuCollection", true, new[] { ExternalSystemFields.ExternalSystemId, ExternalMenuFields.ExternalSystemId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between ExternalSystemEntity and ExternalProductEntity over the 1:n relation they have, using the relation between the fields: ExternalSystem.ExternalSystemId - ExternalProduct.ExternalSystemId</summary>
		public virtual IEntityRelation ExternalProductEntityUsingExternalSystemId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "ExternalProductCollection", true, new[] { ExternalSystemFields.ExternalSystemId, ExternalProductFields.ExternalSystemId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between ExternalSystemEntity and ExternalSystemLogEntity over the 1:n relation they have, using the relation between the fields: ExternalSystem.ExternalSystemId - ExternalSystemLog.ExternalSystemId</summary>
		public virtual IEntityRelation ExternalSystemLogEntityUsingExternalSystemId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "ExternalSystemLogCollection", true, new[] { ExternalSystemFields.ExternalSystemId, ExternalSystemLogFields.ExternalSystemId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between ExternalSystemEntity and OrderRoutestephandlerEntity over the 1:n relation they have, using the relation between the fields: ExternalSystem.ExternalSystemId - OrderRoutestephandler.ExternalSystemId</summary>
		public virtual IEntityRelation OrderRoutestephandlerEntityUsingExternalSystemId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "OrderRoutestephandlerCollection", true, new[] { ExternalSystemFields.ExternalSystemId, OrderRoutestephandlerFields.ExternalSystemId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between ExternalSystemEntity and OrderRoutestephandlerHistoryEntity over the 1:n relation they have, using the relation between the fields: ExternalSystem.ExternalSystemId - OrderRoutestephandlerHistory.ExternalSystemId</summary>
		public virtual IEntityRelation OrderRoutestephandlerHistoryEntityUsingExternalSystemId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "OrderRoutestephandlerHistoryCollection", true, new[] { ExternalSystemFields.ExternalSystemId, OrderRoutestephandlerHistoryFields.ExternalSystemId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between ExternalSystemEntity and RoutestephandlerEntity over the 1:n relation they have, using the relation between the fields: ExternalSystem.ExternalSystemId - Routestephandler.ExternalSystemId</summary>
		public virtual IEntityRelation RoutestephandlerEntityUsingExternalSystemId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "RoutestephandlerCollection", true, new[] { ExternalSystemFields.ExternalSystemId, RoutestephandlerFields.ExternalSystemId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between ExternalSystemEntity and CompanyEntity over the m:1 relation they have, using the relation between the fields: ExternalSystem.CompanyId - Company.CompanyId</summary>
		public virtual IEntityRelation CompanyEntityUsingCompanyId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Company", false, new[] { CompanyFields.CompanyId, ExternalSystemFields.CompanyId }); }
		}

	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticExternalSystemRelations
	{
		internal static readonly IEntityRelation ExternalDeliverypointEntityUsingExternalSystemIdStatic = new ExternalSystemRelations().ExternalDeliverypointEntityUsingExternalSystemId;
		internal static readonly IEntityRelation ExternalMenuEntityUsingExternalSystemIdStatic = new ExternalSystemRelations().ExternalMenuEntityUsingExternalSystemId;
		internal static readonly IEntityRelation ExternalProductEntityUsingExternalSystemIdStatic = new ExternalSystemRelations().ExternalProductEntityUsingExternalSystemId;
		internal static readonly IEntityRelation ExternalSystemLogEntityUsingExternalSystemIdStatic = new ExternalSystemRelations().ExternalSystemLogEntityUsingExternalSystemId;
		internal static readonly IEntityRelation OrderRoutestephandlerEntityUsingExternalSystemIdStatic = new ExternalSystemRelations().OrderRoutestephandlerEntityUsingExternalSystemId;
		internal static readonly IEntityRelation OrderRoutestephandlerHistoryEntityUsingExternalSystemIdStatic = new ExternalSystemRelations().OrderRoutestephandlerHistoryEntityUsingExternalSystemId;
		internal static readonly IEntityRelation RoutestephandlerEntityUsingExternalSystemIdStatic = new ExternalSystemRelations().RoutestephandlerEntityUsingExternalSystemId;
		internal static readonly IEntityRelation CompanyEntityUsingCompanyIdStatic = new ExternalSystemRelations().CompanyEntityUsingCompanyId;

		/// <summary>CTor</summary>
		static StaticExternalSystemRelations() { }
	}
}

