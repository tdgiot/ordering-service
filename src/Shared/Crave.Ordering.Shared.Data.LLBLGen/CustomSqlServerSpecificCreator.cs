﻿using System.Data.Common;
using System.Data.SqlClient;
using Amazon.XRay.Recorder.Handlers.SqlServer;
using SD.LLBLGen.Pro.DQE.SqlServer;

namespace Crave.Ordering.Shared.Data.LLBLGen
{
    public class CustomSqlServerSpecificCreator : SqlServerSpecificCreator
    {
        public override DbCommand CreateCommand(DbConnection connectionToUse)
        {
            SqlCommand command = (SqlCommand)base.CreateCommand(connectionToUse);

            return new TraceableSqlCommand(command.CommandText, command.Connection, command.Transaction);
        }
    }
}
