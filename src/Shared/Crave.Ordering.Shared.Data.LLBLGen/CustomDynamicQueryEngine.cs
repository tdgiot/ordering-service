﻿using SD.LLBLGen.Pro.DQE.SqlServer;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Crave.Ordering.Shared.Data.LLBLGen
{
    public class CustomDynamicQueryEngine : DynamicQueryEngine
    {
        protected override IDbSpecificCreator CreateDbSpecificCreator() => new CustomSqlServerSpecificCreator();
    }
}
