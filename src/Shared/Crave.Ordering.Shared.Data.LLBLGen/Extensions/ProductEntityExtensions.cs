﻿using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Ordering.Shared.Data.LLBLGen.Extensions
{
    public static class ProductEntityExtensions
    {
        public static bool IsOrderable(this ProductEntity productEntity)
        {
            return productEntity != null && 
                   productEntity.VisibilityType != Crave.Enums.VisibilityType.Never && 
                   productEntity.IsAvailable;
        }
    }
}
