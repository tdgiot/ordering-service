﻿using System.Linq;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;

namespace Crave.Ordering.Shared.Data.LLBLGen.Extensions
{
    public static class DeliveryDistanceCollectionExtensions
    {
        public static bool DetermineWithinRange(this EntityCollection<DeliveryDistanceEntity> deliveryDistanceCollection, int metres)
        {
            return deliveryDistanceCollection.Any(x => metres <= x.MaximumInMetres);
        }

        public static DeliveryDistanceEntity FindInRange(this EntityCollection<DeliveryDistanceEntity> deliveryDistanceCollection, int metres)
        {
            return deliveryDistanceCollection.OrderBy(x => x.MaximumInMetres).FirstOrDefault(x => metres <= x.MaximumInMetres);
        }
    }
}
