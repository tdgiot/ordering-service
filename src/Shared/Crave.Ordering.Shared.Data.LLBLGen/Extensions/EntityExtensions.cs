﻿using System.Linq;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Crave.Ordering.Shared.Data.LLBLGen.Extensions
{
    public static class EntityExtensions
    {
        public static bool IsChanged(this IEntity2 entity, IEntityField2 field)
        {
            return entity.Fields[field.Name].IsChanged;
        }

        public static object DbValue(this IEntity2 entity, IEntityField2 field)
        {
            return entity.Fields[field.Name].DbValue;
        }

        public static bool IsNewEntityOrFieldIsChanged(this IEntity2 entity, IEntityField2 field)
        {
            return entity.IsNew || entity.IsChanged(field);
        }

        public static bool IsNewEntityOrFieldsAreChanged(this IEntity2 entity, params IEntityField2[] fields)
        {
            return entity.IsNew || fields.Any(x => entity.IsChanged(x));
        }

        public static bool IsNullOrNew(this IEntity2 entity)
        {
            return entity == null || entity.IsNew;
        }
    }
}
