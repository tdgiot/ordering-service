﻿using Crave.Enums;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Ordering.Shared.Data.LLBLGen.Extensions
{
    public static class CheckoutMethodEntityExtensions
    {
        public static bool IsCustomerLastNameRequired(this CheckoutMethodEntity checkoutMethodEntity)
        {
            return checkoutMethodEntity.CheckoutType == CheckoutType.ChargeToRoom;
        }

        public static bool IsDeliverypointRequired(this CheckoutMethodEntity checkoutMethodEntity)
        {
            return checkoutMethodEntity.CheckoutType == CheckoutType.ChargeToRoom;
        }

        public static bool IsPriceRequired(this CheckoutMethodEntity checkoutMethodEntity)
        {
            return checkoutMethodEntity.CheckoutType == CheckoutType.PaymentProvider;
        }

        public static bool IsFree(this CheckoutMethodEntity checkoutMethodEntity)
        {
            return checkoutMethodEntity.CheckoutType == CheckoutType.FreeOfCharge;
        }
    }
}
