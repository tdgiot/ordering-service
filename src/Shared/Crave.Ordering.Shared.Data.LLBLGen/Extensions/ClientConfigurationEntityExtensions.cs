﻿using System.Linq;
using Crave.Enums;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Ordering.Shared.Data.LLBLGen.Extensions
{
    public static class ClientConfigurationEntityExtensions
    {
        public static RouteEntity GetRoute(this ClientConfigurationEntity entity, RouteType routeType)
        {
            ClientConfigurationRouteEntity route = entity.ClientConfigurationRouteCollection.SingleOrDefault(x => x.Type == routeType);
            return route?.Route;
        }
    }
}
