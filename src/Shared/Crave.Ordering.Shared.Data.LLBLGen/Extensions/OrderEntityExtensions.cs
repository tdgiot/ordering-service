﻿using System;
using System.Collections.Generic;
using Crave.Enums;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Ordering.Shared.Data.LLBLGen.Extensions
{
    public static class OrderEntityExtensions
    {
        private static List<OrderType> orderTypesNotRequiringOrderitems;

        public static bool RequiresOrderitems(this OrderEntity orderEntity)
        {
            return !OrderEntityExtensions.OrderTypesNotRequiringOrderitems.Contains(orderEntity.Type);
        }

        public static Globalization.TimeZone GetTimeZone(this OrderEntity orderEntity)
        {
            return (!orderEntity.TimeZoneOlsonId.IsNullOrWhiteSpace() ? Globalization.TimeZone.Mappings[orderEntity.TimeZoneOlsonId] : null);
        }

        private static List<OrderType> OrderTypesNotRequiringOrderitems
        {
            get
            {
                if (OrderEntityExtensions.orderTypesNotRequiringOrderitems == null)
                {
                    OrderEntityExtensions.orderTypesNotRequiringOrderitems = OrderEntityExtensions.GetOrderTypesNotRequiringOrderitems();
                }

                return OrderEntityExtensions.orderTypesNotRequiringOrderitems;
            }
        }

        private static List<OrderType> GetOrderTypesNotRequiringOrderitems()
        {
            List<OrderType> orderTypes = new List<OrderType>();

            foreach (OrderType type in Enum.GetValues(typeof(OrderType)))
            {
                if (type.DoesNotRequireOrderitems())
                {
                    orderTypes.Add(type);
                }
            }

            return orderTypes;
        }

        /// <summary>
        /// Indicates whether tipping is active.
        /// </summary>
        public static bool TippingActive(this OrderEntity orderEntity)
        {
            return orderEntity.Outlet.TippingActive && 
                   orderEntity.Outlet.TippingProductId.HasValue;
        }

        /// <summary>
        /// Indicates whether the order requires a minimum amount of tip.
        /// </summary>
        public static bool TippingMinimumRequired(this OrderEntity orderEntity)
        {
            return orderEntity.Outlet.TippingMinimumPercentage.GetValueOrDefault(0) > 0 &&
                   orderEntity.OrderitemTotalDisplayPrice > 0;
        }

        public static bool ServiceChargeRequired(this OrderEntity orderEntity)
        {
            return orderEntity.Outlet.ServiceChargeProductId.HasValue && 
                   (orderEntity.ServiceMethod.ServiceChargePercentage.GetValueOrDefault(0) > 0 || 
                    orderEntity.ServiceMethod.ServiceChargeAmount.GetValueOrDefault(0) > 0);
        }

        public static bool IsFromAppLessApi(this OrderEntity orderEntity)
        {
            return orderEntity.OutletId.HasValue;
        }

        public static bool IsFromRestApi(this OrderEntity orderEntity)
        {
            return orderEntity.ClientId.HasValue;
        }
    }
}
