﻿using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Ordering.Shared.Data.LLBLGen.Extensions
{
    public static class CategoryEntityExtensions
    {
        public static CategoryEntity GetCategoryWithRouteId(this CategoryEntity entity)
        {
            if (entity.RouteId.HasValue)
            {
                return entity;
            }

            if (entity.ParentCategory != null && !entity.ParentCategory.IsNew)
            {
                return entity.ParentCategory.GetCategoryWithRouteId();
            }

            return null;
        }
    }
}
