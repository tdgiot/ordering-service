﻿using Crave.Enums;
using Crave.Ordering.Shared.Constants;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Ordering.Shared.Data.LLBLGen.Extensions
{
    public static class ServiceMethodEntityExtensions
    {
        public static bool IsCustomerLastNameRequired(this ServiceMethodEntity serviceMethodEntity)
        {
            return serviceMethodEntity.Type == ServiceMethodType.RoomService;
        }

        public static bool IsDeliverypointRequired(this ServiceMethodEntity serviceMethodEntity)
        {
            return serviceMethodEntity.Type == ServiceMethodType.RoomService || serviceMethodEntity.Type == ServiceMethodType.TableService;
        }

        public static bool IsDeliveryInformationRequired(this ServiceMethodEntity serviceMethodEntity)
        {
            return serviceMethodEntity.Type == ServiceMethodType.Delivery;
        }
    
        public static bool IsFreeDeliveryActive(this ServiceMethodEntity serviceMethodEntity)
        {
            return serviceMethodEntity.MinimumAmountForFreeDelivery > 0;
        }

        public static bool IsMinimumPriceReachedForFreeDelivery(this ServiceMethodEntity serviceMethodEntity, decimal price)
        {
            return price >= serviceMethodEntity.MinimumAmountForFreeDelivery;
        }

        public static bool IsFreeServiceChargeActive(this ServiceMethodEntity serviceMethodEntity)
        {
            return serviceMethodEntity.MinimumAmountForFreeServiceCharge > 0;
        }

        public static bool IsMinimumPriceReachedForFreeServiceCharge(this ServiceMethodEntity serviceMethodEntity, decimal price)
        {
            return price >= serviceMethodEntity.MinimumAmountForFreeServiceCharge;
        }

        public static decimal CalculateServiceCharge(this ServiceMethodEntity serviceMethodEntity, decimal orderitemTotal)
        {
            return orderitemTotal * (decimal)(serviceMethodEntity.ServiceChargePercentage.GetValueOrDefault(0) / Percentage.Hundred);
        }
    }
}
