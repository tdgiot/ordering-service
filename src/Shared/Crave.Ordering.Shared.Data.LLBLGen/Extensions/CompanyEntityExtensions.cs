﻿using System;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Ordering.Shared.Data.LLBLGen.Extensions
{
    public static class CompanyEntityExtensions
    {
        public static Globalization.TimeZone GetTimeZone(this CompanyEntity companyEntity)
        {
            return (!companyEntity.TimeZoneOlsonId.IsNullOrWhiteSpace() ? Globalization.TimeZone.Mappings[companyEntity.TimeZoneOlsonId] : null);
        }
    }
}
