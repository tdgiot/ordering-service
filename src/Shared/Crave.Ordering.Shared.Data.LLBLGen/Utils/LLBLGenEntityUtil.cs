﻿using System;
using System.Collections.Generic;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Crave.Ordering.Shared.Data.LLBLGen.Utils
{
    public class LLBLGenEntityUtil
    {
        /// <summary>
        /// Copies the fields from the source to the result entity.
        /// </summary>
        /// <typeparam name="T">The type of the result entity.</typeparam>
        /// <param name="source">The source entity.</param>
        /// <param name="result">The result entity.</param>
        /// <returns>The result entity.</returns>
        public static T CopyFields<T>(IEntity2 source, T result)
            where T : IEntity2
        {
            return LLBLGenEntityUtil.CopyFields<T>(source, result, null, false);
        }

        /// <summary>
        /// Copies the fields from the source to the result entity, using the specified field mappings.
        /// </summary>
        /// <typeparam name="T">The type of the result entity.</typeparam>
        /// <param name="source">The source entity.</param>
        /// <param name="result">The result entity.</param>
        /// <param name="fieldMappings">The mappings between the source and result entity fields.</param>
        /// <param name="onlyCopyMappedFields">If set to <c>true</c>, only copies the mapped fields.</param>
        /// <returns>The result entity.</returns>
        public static T CopyFields<T>(IEntity2 source, T result, IDictionary<string, string> fieldMappings, bool onlyCopyMappedFields)
            where T : IEntity2
        {
            if (source == null)
            {
                throw new ArgumentNullException("source");
            }

            if (result == null)
            {
                throw new ArgumentNullException("result");
            }

            if (fieldMappings == null)
            {
                fieldMappings = new Dictionary<string, string>();
            }

            // Add default field mappings (if not already mapped)
            string[] ignoreFields = new string[] { "Archived", "Created", "CreatedBy", "Updated", "UpdatedBy", "Deleted" };
            foreach (string ignoreField in ignoreFields)
            {
                if (!fieldMappings.ContainsKey(ignoreField))
                {
                    fieldMappings.Add(ignoreField, null);
                }
            }

            // Copy fields
            foreach (IEntityField2 sourceField in source.Fields)
            {
                // Get result field
                IEntityField2 resultField = null;
                string resultFieldName;
                if (fieldMappings.TryGetValue(sourceField.Name, out resultFieldName))
                {
                    if (resultFieldName != null)
                    {
                        resultField = result.Fields[resultFieldName];
                    }
                }
                else if (!onlyCopyMappedFields)
                {
                    resultField = result.Fields[sourceField.Name];
                }

                // Copy field
                if (resultField != null &&
                    !resultField.IsReadOnly &&
                    !resultField.IsPrimaryKey)
                {
                    result.SetNewFieldValue(resultField.Name, sourceField.CurrentValue);
                }
            }

            return result;
        }
    }
}
