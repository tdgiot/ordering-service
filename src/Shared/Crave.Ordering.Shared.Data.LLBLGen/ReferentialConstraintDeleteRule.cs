﻿namespace Crave.Ordering.Shared.Data.LLBLGen
{
    /// <summary>
    /// Enum for setting delete rules for referential constraints
    /// </summary>
    public enum ReferentialConstraintDeleteRule
    {
        /// <summary>
        /// No action is being performed on foreign records
        /// </summary>
        NoAction = 0,
        /// <summary>
        /// Foreign records are being deleted is a record is being deleted
        /// </summary>
        Cascade = 1,
        /// <summary>
        /// Foreign keys are being set to null if a record is being deleted
        /// </summary>
        SetNull = 2
    }
}
