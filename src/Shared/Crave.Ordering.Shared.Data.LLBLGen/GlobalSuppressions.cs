﻿using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("MyCategory", "S101", MessageId = "RenameToMatchPascalCase", Scope = "module", Justification = "We don't need your kind here")]
[assembly: SuppressMessage("MyCategory", "S125", MessageId = "RemoveCommentedCode", Scope = "module", Justification = "We don't need your kind here")]
[assembly: SuppressMessage("MyCategory", "S1117", MessageId = "HiddenFieldWithSameName", Scope = "module", Justification = "We don't need your kind here")]
[assembly: SuppressMessage("MyCategory", "S1118", MessageId = "ProtectedConstructorOrStaticKeyword", Scope = "module", Justification = "We don't need your kind here")]
[assembly: SuppressMessage("MyCategory", "S1128", MessageId = "RemoveUnnecessaryUsings", Scope = "module", Justification = "We don't need your kind here")]
[assembly: SuppressMessage("MyCategory", "S3217", MessageId = "ChangeCollectionType", Scope = "module", Justification = "We don't need your kind here")]
[assembly: SuppressMessage("MyCategory", "S3925", MessageId = "ImplementationISerializable", Scope = "module", Justification = "We don't need your kind here")]
[assembly: SuppressMessage("MyCategory", "S4136", MessageId = "MethodOverloadShouldAdjecent", Scope = "module", Justification = "We don't need your kind here")]
[assembly: SuppressMessage("MyCategory", "S4275", MessageId = "RefactorSetterToReferField", Scope = "module", Justification = "We don't need your kind here")]
