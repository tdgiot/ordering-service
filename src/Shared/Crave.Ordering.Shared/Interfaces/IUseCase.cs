﻿namespace Crave.Ordering.Shared.Interfaces
{
    public interface IUseCase { }

    public interface IUseCase<TRequest, TResponse> : IUseCase
    {
        TResponse Execute(TRequest request);
    }

    public interface IUseCase<TResponse> : IUseCase
    {
        TResponse Execute();
    }
}
