﻿using System.Threading.Tasks;

namespace Crave.Ordering.Shared.Interfaces
{
    public interface IUseCaseAsync { }

    public interface IUseCaseAsync<TRequest, TResponse> : IUseCaseAsync
    {
        Task<TResponse> ExecuteAsync(TRequest request);
    }

    public interface IUseCaseAsync<TRequest> : IUseCaseAsync
    {
        Task ExecuteAsync(TRequest request);
    }
}
