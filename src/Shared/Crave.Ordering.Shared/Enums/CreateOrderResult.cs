﻿namespace Crave.Ordering.Shared.Enums
{
    /// <summary>
    /// Enumeration which represents the result of creating an order.
    /// </summary>
    public enum CreateOrderResult
    {
        /// <summary>
        /// Order successfully created.
        /// </summary>
        Ok = 1,

        /// <summary>
        /// Order does not exist in database.
        /// </summary>
        OrderNotFound = 2,

        /// <summary>
        /// Order already exists in database.
        /// </summary>
        OrderNotNew = 3,

        /// <summary>
        /// Deliverypoint or customer not set on the order.
        /// </summary>
        DeliverypointOrEmailNotSet = 4,

        /// <summary>
        /// Order items not set on the order.
        /// </summary>
        OrderItemsNotSet = 5,

        /// <summary>
        /// Unable to save the order entity.
        /// </summary>
        UnableToSaveOrder = 6,

        /// <summary>
        /// Unknown checkout method.
        /// </summary>
        UnknownCheckoutMethod = 7,

        /// <summary>
        /// Checkout method PaymentProvider requires as receipt template
        /// </summary>
        CheckoutMethodPaymentProviderRequiresReceiptTemplate = 8,

        /// <summary>
        /// Exception occurred while trying to create the order.
        /// </summary>
        Exception = 999
    }
}
