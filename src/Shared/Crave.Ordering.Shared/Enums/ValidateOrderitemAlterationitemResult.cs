﻿namespace Crave.Ordering.Shared.Enums
{
    /// <summary>
    /// Enumeration which represents the result of validating an order item alteration item before save.
    /// </summary>
    public enum ValidateOrderitemAlterationitemResult
    {
        ChangeNotAllowedToExistingItem = 1,
        OrderitemAlterationitemIsNotSavedThroughUseCase = 2,
        AlterationNameNotSet = 3,
        TimeNotSet = 4,
        ValueNotSet = 5,
        AlterationoptionNameNotSet = 6
    }
}
