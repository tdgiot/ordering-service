﻿namespace Crave.Ordering.Shared.Enums
{
    /// <summary>
    /// Enumeration which represents the result of creating an order route.
    /// </summary>
    public enum CreateOrderRouteResult
    {
        /// <summary>
        /// Order route successfully created.
        /// </summary>
        Ok = 1,

        /// <summary>
        /// Order does not exist in database.
        /// </summary>
        OrderNotFound = 2,

        /// <summary>
        /// Order already routed in the past.
        /// </summary>
        OrderAlreadyRouted = 3,

        /// <summary>
        /// Order need to be paid first before routing is started
        /// </summary>
        WaitingForPayment = 4,

        /// <summary>
        /// Exception occurred while trying to create the route.
        /// </summary>
        Exception = 999
    }
}
