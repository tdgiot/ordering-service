﻿namespace Crave.Ordering.Shared.Enums
{
    /// <summary>
    /// Enumeration which represents the result of validating an order item before save.
    /// </summary>
    public enum ValidateOrderitemResult
    {
        QuantityLessThanOrZero = 1,
        PriceInLessThanZero = 2,
        VatPercentageLessThanZero = 3,
        ProductIdNotSet = 4,
        ChangeNotAllowedToExistingOrderitem = 5
    }
}
