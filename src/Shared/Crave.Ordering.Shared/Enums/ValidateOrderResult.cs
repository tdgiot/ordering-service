﻿namespace Crave.Ordering.Shared.Enums
{
    /// <summary>
    /// Enumeration which represents the result of validating an order before save.
    /// </summary>
    public enum ValidateOrderResult
    {
        
    }
}
