﻿using System;
using System.Globalization;
using Crave.Enums;

namespace Crave.Ordering.Shared.Extensions
{
    public static class DateTimeExtensions
    {
        public static string FormatDate(this DateTime dateTime, ClockMode clockMode)
        {
            return clockMode switch
            {
                ClockMode.Hour24 => string.Format("{0:dd-MM-yyyy}", dateTime),
                ClockMode.AmPm => dateTime.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture),
                _ => string.Empty
            };
        }

        public static string FormatDateTime(this DateTime dateTime, ClockMode clockMode)
        {
            return clockMode switch
            {
                ClockMode.Hour24 => string.Format("{0:dd-MM-yyyy HH:mm}", dateTime),
                ClockMode.AmPm => dateTime.ToString("MM/dd/yyyy hh:mm tt", CultureInfo.InvariantCulture),
                _ => string.Empty
            };
        }

        public static DateTime ToLocalTime(this DateTime dateTime, string timeZoneOlsonId)
        {
            if (timeZoneOlsonId.IsNullOrWhiteSpace())
            {
                return dateTime;
            }

            if (!Globalization.TimeZone.Mappings.ContainsKey(timeZoneOlsonId))
            {
                return dateTime;
            }

            Globalization.TimeZone timeZone = Globalization.TimeZone.Mappings[timeZoneOlsonId];
            return TimeZoneInfo.ConvertTimeFromUtc(dateTime, timeZone.TimeZoneInfo);
        }
    }
}
