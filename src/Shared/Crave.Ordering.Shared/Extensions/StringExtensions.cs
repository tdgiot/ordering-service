﻿using System.Text.RegularExpressions;

namespace Crave.Ordering.Shared.Extensions
{
    public static class StringExtensions
    {
        private const string EMAIL_PATTERN = @"^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,})$";
        private const string PHONENUMBER_PATTERN = @"^(\+\s?)?((?<!\+.*)\(\+?\d+([\s\-\.]?\d+)?\)|\d+)([\s\-\.]?(\(\d+([\s\-\.]?\d+)?\)|\d+))*(\s?(x|ext\.?)\s?\d+)?$";
        private const RegexOptions REGEX_OPTIONS = RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.ExplicitCapture;

        private static readonly Regex EmailRegularExpression = new Regex(StringExtensions.EMAIL_PATTERN);
        private static readonly Regex PhonenumberRegularExpression = new Regex(StringExtensions.PHONENUMBER_PATTERN, StringExtensions.REGEX_OPTIONS);

        /// <summary>
        /// Indicates whether the format of the email address is valid.
        /// </summary>
        public static bool IsValidEmail(this string email)
        {
            return StringExtensions.EmailRegularExpression.IsMatch(email);
        }

        /// <summary>
        /// Indicates whether the format of the phone number is valid.
        /// </summary>
        public static bool IsValidPhonenumber(this string phonenumber)
        {
            return StringExtensions.PhonenumberRegularExpression.IsMatch(phonenumber);
        }

        /// <summary>
        /// Parses a string value to an int.
        /// </summary>
        /// <param name="value">The value to parse.</param>
        /// <returns>Int if parse successful, null if not.</returns>
        public static int? ToIntOrNull(this string value)
        {
            if (int.TryParse(value, out int integer))
            {
                return integer;
            }

            return null;
        }
    }
}
