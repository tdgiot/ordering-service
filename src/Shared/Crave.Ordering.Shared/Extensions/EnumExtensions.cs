﻿using System;

namespace Crave.Ordering.Shared.Extensions
{
    public static class EnumExtensions
    {
        /// <summary>
        /// Validates if the enumerator value is matching with one of the provided values.
        /// </summary>
        /// <typeparam name="TEnum">The type of the enumerator that should be compared</typeparam>
        /// <param name="enumerator">The current enumerator value which should be compared</param>
        /// <param name="matchingEnums">The allowed values that should be validated</param>
        /// <returns></returns>
        public static bool IsIn<TEnum>(this TEnum enumerator, params TEnum[] matchingEnums)
           where TEnum : Enum, IComparable, IFormattable, IConvertible
        {
            var enumType = typeof(TEnum);

            if (!enumType.IsEnum)
            {
                throw new ArgumentException("Bad argument", nameof(enumerator));
            }

            foreach (var matchingEnum in matchingEnums)
            {
                if (enumerator.CompareTo(matchingEnum) == 0)
                {
                    return true;
                }
            }

            return false;
        }
    }
}
