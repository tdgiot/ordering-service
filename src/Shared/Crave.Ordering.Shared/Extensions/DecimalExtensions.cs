﻿using System;
using Crave.Globalization;
using Crave.Ordering.Shared.Constants;

namespace Crave.Ordering.Shared.Extensions
{
    public static class DecimalExtensions
    {
        private const decimal MAX_ROUNDING_MISMATCH_ALLOWED = 0.001M;
        private const int DECIMAL_PLACES = 2;

        /// <summary>
        /// Adds tax to the price.
        /// </summary>
        /// <param name="priceExcl">Price excluding tax.</param>
        /// <param name="tax">Tax to add.</param>
        /// <returns>Price including tax.</returns>
        public static decimal AddTax(this decimal priceExcl, decimal tax) => priceExcl + tax;

        /// <summary>
        /// Removes tax from the price.
        /// </summary>
        /// <param name="priceIncl">Price including tax.</param>
        /// <param name="tax">Tax to remove.</param>
        /// <returns>Price excluding tax.</returns>
        public static decimal RemoveTax(this decimal priceIncl, decimal tax) => priceIncl - tax;

        /// <summary>
        /// Calculates the tax based on the percentage.
        /// </summary>
        /// <param name="price">The price to calculate the tax for.</param>
        /// <param name="taxPercentage">The percentage to use in the calculation.</param>
        /// <param name="pricesIncludeTaxes">Indicates whether the input price is including taxes.</param>
        /// <returns>The tax.</returns>
        public static decimal CalculateTax(this decimal price, double taxPercentage, bool pricesIncludeTaxes) => 
            pricesIncludeTaxes
                ? (price / (Percentage.Hundred + (decimal)taxPercentage)) * (decimal)taxPercentage
                : (price / Percentage.Hundred) * (decimal)taxPercentage;

        /// <summary>
        /// Calculates the tax based on the percentage and the quantity.
        /// </summary>
        /// <param name="price">The price to calculate the tax for.</param>
        /// <param name="taxPercentage">The percentage to use in the calculation.</param>
        /// <param name="quantity">The quantity of the item.</param>
        /// <param name="pricesIncludeTaxes">Indicates whether the input price is including taxes.</param>
        /// <returns>The tax.</returns>
        public static decimal CalculateTaxTotal(this decimal price, double taxPercentage, int quantity, bool pricesIncludeTaxes) => 
            quantity * price.CalculateTax(taxPercentage, pricesIncludeTaxes);

        /// <summary>
        /// Converts the price into a price including tax based on whether the price is shown to the customer including tax or not.
        /// </summary>
        /// <param name="price">The price with or without tax.</param>
        /// <param name="tax">The tax to apply.</param>
        /// <param name="priceIncludesTax">Price includes tax or not.</param>
        /// <returns>The price including tax.</returns>
        public static decimal ToPriceInTax(this decimal price, decimal tax, bool priceIncludesTax)
        {
            decimal priceInTax = priceIncludesTax ? price : price.AddTax(tax);

            return Math.Round(priceInTax, DECIMAL_PLACES, MidpointRounding.AwayFromZero);
        }

        /// <summary>
        /// Converts the price into a total price including tax based on whether the price is shown to the customer including tax or not.
        /// </summary>
        /// <param name="price">The price with or without tax.</param>
        /// <param name="tax">The tax to apply.</param>
        /// <param name="quantity">The quantity of the item.</param>
        /// <param name="priceIncludesTax">Price includes tax or not.</param>
        /// <returns>The price total including tax.</returns>
        public static decimal ToPriceTotalInTax(this decimal price, decimal tax, int quantity, bool priceIncludesTax)
        {
            decimal priceInTax = priceIncludesTax ? price : price.AddTax(tax);
            decimal priceTotalInTax = quantity * priceInTax;

            return Math.Round(priceTotalInTax, DECIMAL_PLACES, MidpointRounding.AwayFromZero);
        }

        /// <summary>
        /// Converts the price into a price excluding tax based on whether the price is shown to the customer including tax or not.
        /// </summary>
        /// <param name="price">The price with or without tax.</param>
        /// <param name="tax">The tax to apply.</param>
        /// <param name="priceIncludesTax">Price includes tax or not.</param>
        /// <returns>The price including tax.</returns>
        public static decimal ToPriceExTax(this decimal price, decimal tax, bool priceIncludesTax)
        {
            decimal priceExTax = priceIncludesTax ? price.RemoveTax(tax) : price;
            return Math.Round(priceExTax, DECIMAL_PLACES, MidpointRounding.AwayFromZero);
        }

        /// <summary>
        /// Converts the price into a total price excluding tax based on whether the price is shown to the customer including tax or not.
        /// </summary>
        /// <param name="price">The price with or without tax.</param>
        /// <param name="tax">The tax to apply.</param>
        /// <param name="quantity">The quantity of the item.</param>
        /// <param name="priceIncludesTax">Price includes tax or not.</param>
        /// <returns>The price total excluding tax.</returns>
        public static decimal ToPriceTotalExTax(this decimal price, decimal tax, int quantity, bool priceIncludesTax)
        {
            decimal priceExTax = priceIncludesTax ? price.RemoveTax(tax) : price;
            decimal priceTotalExTax = quantity * priceExTax;
            return Math.Round(priceTotalExTax, DECIMAL_PLACES, MidpointRounding.AwayFromZero);
        }

        /// <summary>
        /// Formats the price based on the culture code.
        /// </summary>
        /// <param name="price">The price to format.</param>
        /// <param name="cultureCode">The code of the culture to use.</param>
        /// <returns>Formatted price.</returns>
        public static string Format(this decimal price, string cultureCode)
        {
            Culture culture = (Culture)cultureCode;
            return price.ToString("C", culture.CultureInfo);
        }

        /// <summary>
        /// Compares two nullable decimals.
        /// </summary>
        /// <param name="firstAmount">The first decimal to compare.</param>
        /// <param name="secondAmount">The second decimal to compare.</param>
        /// <returns></returns>
        public static bool EqualsIgnoreNull(this decimal firstAmount, decimal? secondAmount)
        {
            if (!secondAmount.HasValue)
            {
                secondAmount = 0M;
            }

            return DecimalExtensions.EqualsIgnoreRounding(firstAmount, secondAmount.Value);
        }

        /// <summary>
        /// Compares two decimals and ignores a different of 0.001M.
        /// </summary>
        /// <param name="firstAmount">The first decimal to compare.</param>
        /// <param name="secondAmount">The second decimal to compare.</param>
        /// <returns></returns>
        public static bool EqualsIgnoreRounding(this decimal firstAmount, decimal secondAmount) =>
            Math.Abs(Math.Round(firstAmount, DECIMAL_PLACES, MidpointRounding.AwayFromZero) - Math.Round(secondAmount, DECIMAL_PLACES, MidpointRounding.AwayFromZero)) < MAX_ROUNDING_MISMATCH_ALLOWED;
    }
}
