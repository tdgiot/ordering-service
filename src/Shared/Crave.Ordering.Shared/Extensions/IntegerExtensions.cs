﻿namespace Crave.Ordering.Shared.Extensions
{
    public static class IntegerExtensions
    {
        public static int? ToValueOrNullIfZero(this int value)
        {
            if (value <= 0)
            {
                return null;
            }

            return value;
        }
    }
}
