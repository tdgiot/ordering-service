﻿using System.Collections.Generic;
using Crave.Enums;

namespace Crave.Ordering.Shared.Constants
{
    /// <summary>
    /// Class which contains constants used in the routing engine.
    /// </summary>
    public static class RoutingConstants
    {
        /// <summary>
        /// Constant which represents the first step number.
        /// </summary>
        public static readonly int FirstStepNumber = 1;

        /// <summary>
        /// Class which contains routestephandler constants
        /// </summary>
        public static class Routestephandlers
        {
            /// <summary>
            /// List of routestephandlers which are automatically handled.
            /// </summary>
            public static readonly IReadOnlyList<RoutestephandlerType> AutomaticlyHandledRoutestephandlers = new[]
                                                                                                             {
                                                                                                                 RoutestephandlerType.POS,
                                                                                                                 RoutestephandlerType.Printer,
                                                                                                                 RoutestephandlerType.EmailOrder,
                                                                                                                 RoutestephandlerType.SMS,
                                                                                                                 RoutestephandlerType.HotSOS,
                                                                                                                 RoutestephandlerType.EmailDocument
                                                                                                             };
        }

        /// <summary>
        /// Class which contains order routestephandler constants
        /// </summary>
        public static class OrderRoutestephandler
        {
            /// <summary>
            /// List of pending order statuses, meaning an order is about to be handled or being handled.
            /// </summary>
            public static readonly IReadOnlyList<OrderRoutestephandlerStatus> PendingStatuses = new[]
                                                                                                {
                                                                                                    OrderRoutestephandlerStatus.BeingHandled,
                                                                                                    OrderRoutestephandlerStatus.WaitingToBeRetrieved
                                                                                                };
        }
    }
}
