﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Crave.Ordering.Shared.Domain
{
	public class Response
	{
		protected const int StatusOk = 0;
		protected const int StatusError = 1;
		protected const int StatusNotFound = 2;

		private readonly int status;

		public static Response AsSuccess() => new Response(Response.StatusOk);

		public static Response AsSuccess(string message) => new Response(Response.StatusOk)
		{
			Message = message
		};

		public static Response AsError(string message) => new Response(Response.StatusError)
		{
			Message = message
		};

		public static Response AsNotFound(string message = "") => new Response(Response.StatusNotFound)
		{
			Message = message
		};

		public static Response Combine(IList<Response> responses)
			=> responses.FirstOrDefault(x => x.Success) ?? responses.First();

		protected Response(int status)
		{
			this.status = status;
		}

		public bool Success => status == Response.StatusOk;
		public bool NotFound => status == Response.StatusNotFound;
		public string Message { get; protected set; } = string.Empty;



	}

	public class Response<T> : Response where T : class
	{
		private Response(int status) : base(status)
		{

		}

		public static Response<T> AsSuccess(T obj) => new Response<T>(Response.StatusOk)
		{
			Model = obj
		};

		public new static Response<T> AsError(string message) => new Response<T>(Response.StatusError)
		{
			Message = message
		};

		public new static Response<T> AsNotFound(string message = "") => new Response<T>(Response.StatusNotFound)
		{
			Message = message
		};

		public T? Model { get; private set; }
	}
}