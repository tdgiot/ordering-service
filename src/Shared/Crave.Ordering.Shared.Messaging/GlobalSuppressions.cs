﻿// This file is used by Code Analysis to maintain SuppressMessage
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given
// a specific target and scoped to a namespace, type, member, etc.

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Major Code Smell", "S3956:\"Generic.List\" instances should not be part of public APIs", Justification = "Type must be serialized and so concrete type should be used", 
    Scope = "namespaceanddescendants", Target = "~N:Crave.Ordering.Shared.Messaging.Domain.UseCases")]
[assembly: SuppressMessage("Major Code Smell", "S3956:\"Generic.List\" instances should not be part of public APIs", Justification = "Type must be serialized and so concrete type should be used",
    Scope = "namespaceanddescendants", Target = "~N:Crave.Ordering.Shared.Messaging.Netmessages")]
