﻿using System;
using Crave.Ordering.Shared.Configuration.Extensions;
using Crave.Ordering.Shared.Configuration.Sections;
using Crave.Ordering.Shared.Messaging.Data.Repositories;
using Crave.Ordering.Shared.Messaging.Domain.UseCases;
using Crave.Ordering.Shared.Messaging.Interfaces.Repositories;
using Crave.Ordering.Shared.Messaging.Interfaces.UseCases;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ServiceStack.Messaging;
using ServiceStack.Messaging.Redis;
using ServiceStack.Redis;

namespace Crave.Ordering.Shared.Messaging.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static void AddMessaging(this IServiceCollection services, IConfiguration configuration) => services.Startup(configuration);

        // To be moved to startup when rewritten to lambda
        internal static void Startup(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddRedis(configuration.ReadAndValidate<RedisConfiguration>());
            services.AddRepositories();
            services.AddUseCases();
        }

        internal static void AddRedis(this IServiceCollection services, RedisConfiguration redisConfiguration)
        {
            if (redisConfiguration.Hosts.IsNullOrEmpty())
            {
                services.AddSingleton<IMessageFactory, InMemoryTransientMessageFactory>();
                services.AddSingleton<IMessageService, InMemoryTransientMessageService>();
            }
            else
            {
                services.AddSingleton<IRedisClientsManager>(new RedisManagerPool(redisConfiguration.Hosts));
                services.AddSingleton<IMessageFactory, RedisMessageFactory>();
                services.AddSingleton<IMessageService, RedisMqServer>();
            }
        }

        internal static void AddRepositories(this IServiceCollection services)
        {
            services.AddSingleton<IDeviceRepository, DeviceRepository>();
            services.AddSingleton<INetmessageRepository, NetmessageRepository>();
        }

        internal static void AddUseCases(this IServiceCollection services)
        {
            services.AddSingleton<ISaveNetmessagesUseCase, SaveNetmessagesUseCase>();
            services.AddSingleton<ISendNetmessageUseCase, SendNetmessageUseCase>();
        }
    }
}
