﻿using System;
using Crave.Enums;

namespace Crave.Ordering.Shared.Messaging.Netmessages
{
    public class NetmessageRoutestephandlerStatusUpdated : Netmessage
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="NetmessageRoutestephandlerStatusUpdated"/> type.
        /// </summary>
        public NetmessageRoutestephandlerStatusUpdated()
        {
            this.MessageType = NetmessageType.RoutestephandlerStatusUpdated;
            this.SaveToDatabase = true;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the id of the order.
        /// </summary>
        public int OrderId
        {
            get
            {
                int outValue;
                int.TryParse(this.FieldValue1, out outValue);
                return outValue;
            }
            set { this.FieldValue1 = value.ToString(); }
        }

        /// <summary>
        /// Gets or sets the id of the route step handler.
        /// </summary>
        public int OrderRoutestephandlerId
        {
            get
            {
                int outValue;
                int.TryParse(this.FieldValue2, out outValue);
                return outValue;
            }
            set { this.FieldValue2 = value.ToString(); }
        }

        /// <summary>
        /// Gets or sets the status of the order route step handler.
        /// </summary>
        public OrderRoutestephandlerStatus OrderRoutestephandlerStatus
        {
            get
            {
                OrderRoutestephandlerStatus outValue;
                Enum.TryParse(this.FieldValue3, out outValue);
                return outValue;
            }
            set { this.FieldValue3 = value.ToIntString(); }
        }

        #endregion
    }
}