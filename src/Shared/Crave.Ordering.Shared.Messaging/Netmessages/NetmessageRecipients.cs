﻿using System;
using System.Collections.Generic;
using Crave.Ordering.Shared.Messaging.Constants;

namespace Crave.Ordering.Shared.Messaging.Netmessages
{
    public class NetmessageRecipients
    {
        #region Properties

        /// <summary>
        /// Gets or sets the identifier of the recipients of the net message.
        /// </summary>
        public string Identifier { get; set; }

        /// <summary>
        /// Gets or sets the list of groups that contain the recipients of the net message.
        /// </summary>
        public List<string> Groups { get; } = new List<string>();

        /// <summary>
        /// Gets a value indicating whether recipients have been added.
        /// </summary>
        public bool HasRecipients => !Identifier.IsNullOrWhiteSpace() || Groups.Count > 0;

        #endregion

        #region Methods

        /// <summary>
        /// Adds the specified group to the list of recipient groups.
        /// </summary>
        /// <param name="group">The string representing the group to add.</param>
        public void AddGroup(string group)
        {
            if (!Groups.Contains(group))
            {
                Groups.Add(group);
            }
        }

        /// <summary>
        /// Adds a company to the list of recipient groups.
        /// </summary>
        /// <param name="companyId">The id of the company to add.</param>
        public void AddCompany(int companyId)
        {
            AddGroup(string.Format(MessagingConstants.GroupCompanyFormat, companyId));
        }

        /// <summary>
        /// Adds a delivery point to the list of recipients groups.
        /// </summary>
        /// <param name="deliverypointId">The id of the delivery point to add.</param>
        public void AddDeliverypoint(int deliverypointId)
        {
            AddGroup(string.Format(MessagingConstants.GroupDeliverypointFormat, deliverypointId));
        }

        /// <summary>
        /// Adds a delivery point group to the list of recipient groups.
        /// </summary>
        /// <param name="deliverypointgroupId">The id of the delivery point group to add.</param>
        public void AddDeliverypointgroup(int deliverypointgroupId)
        {
            AddGroup(string.Format(MessagingConstants.GroupDeliverypointgroupFormat, deliverypointgroupId));
        }

        /// <summary>
        /// Add the terminal group for a specific company
        /// </summary>
        /// <param name="companyId">The id of the company of which the terminals are part of</param>
        public void AddCompanyTerminals(int companyId)
        {
            AddGroup(string.Format(MessagingConstants.GroupCompanyTerminalsFormat, companyId));
        }

        #endregion
    }
}
