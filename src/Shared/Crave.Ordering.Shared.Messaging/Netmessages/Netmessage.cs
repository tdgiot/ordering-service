﻿using System;
using System.Runtime.Serialization;
using Crave.Enums;
using Jil;

namespace Crave.Ordering.Shared.Messaging.Netmessages
{
    /// <summary>
    /// Netmessage
    /// </summary>
    /// <remarks>
    /// Obsolete in new messaging service
    /// </remarks>
    [DataContract]
    public class Netmessage
    {
        #region Properties

        /// <summary>
        /// Get or set Netmessage id
        /// </summary>
        [DataMember]
        public int NetmessageId { get; set; }

        /// <summary>
        /// GUID set by the client
        /// </summary>
        [DataMember]
        public string Guid { get; set; }

        /// <summary>
        /// Separate version for each message used for backwards compatability
        /// </summary>
        [DataMember]
        public int MessageVersion { get; set; }

        /// <summary>
        /// Gets or sets the Netmessage command type
        /// </summary>
        [JilDirective(TreatEnumerationAs = typeof(int))]
        public NetmessageType MessageType { get; set; }
        
        /// <summary>
        /// Gets or sets the status
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// Gets or sets the created date
        /// </summary>
        public DateTime CreatedUTC { get; set; }

        /// <summary>
        /// Message log
        /// </summary>
        public string MessageLog { get; set; }

        /// <summary>
        /// Gets or sets whether this messages should be saved to the database
        /// </summary>
        public bool SaveToDatabase { get; set; }

        /// <summary>
        /// True or false whether the message has been received and processed by the receiver
        /// </summary>
        public bool Submitted { get; set; }

        /// <summary>
        /// Gets or sets the identifier of the receiver
        /// </summary>
        public string ReceiverIdentifier { get; set; }

        /// <summary>
        /// Gets or sets the identifier of the sender
        /// </summary>
        public string SenderIdentifier { get; set; }

        /// <summary>
        /// Gets or sets the sender company id
        /// </summary>
        [DataMember]
        [Obsolete("Use SenderIdentifier")]
        public int SenderCompanyId { get; set; }

        /// <summary>
        /// Gets or sets the receiver company id
        /// </summary>
        [DataMember]
        //[Obsolete("Use ReceiverIdentifier")]
        public int ReceiverCompanyId { get; set; }

        /// <summary>
        /// Gets or sets the sender client id
        /// </summary>
        [DataMember]
        //[Obsolete("Use SenderIdentifier")]
        public int SenderClientId { get; set; }

        /// <summary>
        /// Gets or sets the receiver client id
        /// </summary>
        [DataMember]
        //[Obsolete("Use ReceiverIdentifier")]
        public int ReceiverClientId { get; set; }

        /// <summary>
        /// Gets or sets the sender terminal id
        /// </summary>
        [DataMember]
        //[Obsolete("Use SenderIdentifier")]
        public int SenderTerminalId { get; set; }

        /// <summary>
        /// Gets or sets the receiver terminal id
        /// </summary>
        [DataMember]
        //[Obsolete("Use ReceiverIdentifier")]
        public int ReceiverTerminalId { get; set; }

        /// <summary>
        /// Gets or sets the sender deliverypoint id
        /// </summary>
        [DataMember]
        [Obsolete("Use SenderIdentifier")]
        public int SenderDeliverypointId { get; set; }

        /// <summary>
        /// Gets or sets the receiver deliverypoint id
        /// </summary>
        [DataMember]
        [Obsolete("Use ReceiverIdentifier")]
        public int ReceiverDeliverypointId { get; set; }

        /// <summary>
        /// Gets or sets field value 1
        /// </summary>
        [DataMember]
        public string FieldValue1 { get; set; }

        /// <summary>
        /// Gets or sets field value 2
        /// </summary>
        [DataMember]
        public string FieldValue2 { get; set; }

        /// <summary>
        /// Gets or sets field value 3
        /// </summary>
        [DataMember]
        public string FieldValue3 { get; set; }

        /// <summary>
        /// Gets or sets field value 4
        /// </summary>
        [DataMember]
        public string FieldValue4 { get; set; }

        /// <summary>
        /// Gets or sets field value 5
        /// </summary>
        [DataMember]
        public string FieldValue5 { get; set; }

        /// <summary>
        /// Gets or sets field value 6
        /// </summary>
        [DataMember]
        public string FieldValue6 { get; set; }

        /// <summary>
        /// Gets or sets field value 7
        /// </summary>
        [DataMember]
        public string FieldValue7 { get; set; }

        /// <summary>
        /// Gets or sets field value 8
        /// </summary>
        [DataMember]
        public string FieldValue8 { get; set; }

        /// <summary>
        /// Gets or sets field value 9
        /// </summary>
        [DataMember]
        public string FieldValue9 { get; set; }

        /// <summary>
        /// Gets or sets field value 10
        /// </summary>
        [DataMember]
        public string FieldValue10 { get; set; }

        /// <summary>
        /// Gets or sets field value 11. THIS FIELD IS NOT SAVED IN THE DATABASE!
        /// </summary>
        [DataMember]
        public string FieldValue11 { get; set; }

        /// <summary>
        /// Gets or sets field value 12. THIS FIELD IS NOT SAVED IN THE DATABASE!
        /// </summary>
        [DataMember]
        public string FieldValue12 { get; set; }

        /// <summary>
        /// Gets or sets field value 13. THIS FIELD IS NOT SAVED IN THE DATABASE!
        /// </summary>
        [DataMember]
        public string FieldValue13 { get; set; }

        /// <summary>
        /// Gets or sets field value 14. THIS FIELD IS NOT SAVED IN THE DATABASE!
        /// </summary>
        [DataMember]
        public string FieldValue14 { get; set; }

        /// <summary>
        /// Gets or sets field value 15. THIS FIELD IS NOT SAVED IN THE DATABASE!
        /// </summary>
        [DataMember]
        public string FieldValue15 { get; set; }

        /// <summary>
        /// Gets or sets field value 16. THIS FIELD IS NOT SAVED IN THE DATABASE!
        /// </summary>
        [DataMember]
        public string FieldValue16 { get; set; }

        #endregion 

        /// <summary>
        /// This method is called to verify if the Netmessage found in the Netmessage table is a duplicate (i.e. 2 requests for restart) of this instance as is called upon saving the current instance of the Netmessage.
        /// With this method you can decide if the current message should be ignored since it's a duplicate and/or the existing message should be overwritten by this new message.        
        /// </summary>
        /// <param name="existingNetmessage">The message found in the Netmessage table</param>
        /// <param name="overwriteExistingNetmessageWithThisMessage">Decide wheter the existing message should be overwritten by the current instance.</param>
        /// <returns>False means it's not a duplicate and should be added to the Netmessage table, True means it's a duplicate and this instance should be disregarded, 
        /// but if overwriteExistingMessageWithThisMessage is true the existing message must be updated with the values of this instance and be saved.</returns>
        public virtual bool IsDuplicateMessage(Netmessage existingNetmessage) => false;

        /// <summary>
        /// Create json from this model
        /// </summary>
        /// <returns>Json string</returns>
        public string ToJson()
        {
            return JSON.Serialize(this, new Options(excludeNulls: true));
        }

        /// <summary>
        /// Convert message from generic netmessage to typed
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="validate"></param>
        /// <returns></returns>
        public T ConvertTo<T>(bool validate = true) where T : Netmessage, new()
        {
            T model = new T();
            model.NetmessageId = this.NetmessageId;
            model.Guid = this.Guid;
            model.MessageVersion = this.MessageVersion;
            model.MessageType = this.MessageType;
            model.SenderIdentifier = this.SenderIdentifier;
            model.ReceiverIdentifier = this.ReceiverIdentifier;
            model.FieldValue1 = this.FieldValue1;
            model.FieldValue2 = this.FieldValue2;
            model.FieldValue3 = this.FieldValue3;
            model.FieldValue4 = this.FieldValue4;
            model.FieldValue5 = this.FieldValue5;
            model.FieldValue6 = this.FieldValue6;
            model.FieldValue7 = this.FieldValue7;
            model.FieldValue8 = this.FieldValue8;
            model.FieldValue9 = this.FieldValue9;
            model.FieldValue11 = this.FieldValue11;
            model.FieldValue12 = this.FieldValue12;
            model.FieldValue13 = this.FieldValue13;
            model.FieldValue14 = this.FieldValue14;
            model.FieldValue15 = this.FieldValue15;
            model.FieldValue16 = this.FieldValue16;
            model.Status = this.Status;
            model.Submitted = this.Submitted;

            return model;
        }
    }
}