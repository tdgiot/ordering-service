﻿using System;
using Crave.Enums;

namespace Crave.Ordering.Shared.Messaging.Netmessages
{
    /// <summary>
    /// Order status updated
    /// </summary>
    public class NetmessageOrderStatusUpdated : Netmessage
    {
        /// <summary>
        /// Ctor
        /// </summary>
        public NetmessageOrderStatusUpdated()
        {
            this.MessageType = NetmessageType.OrderStatusUpdated;
        }

        /// <summary>
        /// Order id
        /// </summary>
        public int OrderId
        {
            get
            {
                int outValue;
                int.TryParse(this.FieldValue1, out outValue);
                return outValue;
            }
            set { this.FieldValue1 = value.ToString(); }
        }

        /// <summary>
        /// Order item ids
        /// </summary>
        public string OrderItemIds
        {
            get { return this.FieldValue2; }
            set { this.FieldValue2 = value; }
        }

        /// <summary>
        /// New order status
        /// </summary>
        public OrderStatus OrderStatus
        {
            get
            {
                OrderStatus outValue;
                Enum.TryParse(this.FieldValue3, out outValue);
                return outValue;
            }
            set { this.FieldValue3 = value.ToIntString(); }
        }
    }
}