﻿using System;
using Crave.Enums;
using Jil;

namespace Crave.Ordering.Shared.Messaging.Netmessages
{
    /// <summary>
    /// Netmessage wrapper containing receiver information and the actual data to send
    /// </summary>
    public class NetmessageContainer : EventArgs
    {
        /// <summary>
        /// Unique identifier to verify message on sender
        /// </summary>
        public string Guid { get; set; }

        /// <summary>
        /// Gets or sets the sender of this netmessage
        /// </summary>
        public string Sender { get; set; } = string.Empty;

        /// <summary>
        /// Gets or sets the receiver
        /// </summary>
        public string Receiver { get; set; } = string.Empty;

        /// <summary>
        /// Gets or sets the group names to which to send this message
        /// </summary>
        public string[] Groups { get; set; } = {};

        /// <summary>
        /// Netmessage type <see cref="NetmessageType"/>
        /// </summary>
        [JilDirective(TreatEnumerationAs = typeof(int))]
        public NetmessageType Type { get; set; } = NetmessageType.Unknown;

        /// <summary>
        /// Data to send to the receiver
        /// </summary>
        public string Data { get; set; } = string.Empty;

        /// <summary>
        /// Server instance name from which message is send (only used for scale out messages)
        /// </summary>
        public string Instance { get; set; }

        /// <inheritdoc />
        public override string ToString() => $"Sender={Sender},Receiver={Receiver},Groups={string.Join(",", Groups)},Type={Type},Data={Data}";
    }
}