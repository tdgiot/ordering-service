﻿using System.Collections.Generic;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Ordering.Shared.Messaging.Interfaces.Repositories
{
    /// <summary>
    /// Interface which is used to implement net message repository classes.
    /// </summary>
    internal interface INetmessageRepository
    {
        /// <summary>
        /// Saves the specified net messages.
        /// </summary>
        /// <param name="netmessages">The net message entities to save.</param>
        /// <returns>True if saving the netmessages were successful, False if not.</returns>.
        bool SaveNetmessages(IEnumerable<NetmessageEntity> netmessages);

        /// <summary>
        /// Cancel netmessages for a client which were send based on the linked deliverypointgroup
        /// </summary>
        /// <param name="clientEntity">The client entity for which to remove the netmessages</param>
        void CancelNetmessagesOnDeliverypointgroupChange(ClientEntity clientEntity);

        /// <summary>
        /// Cancel netmessages for a client which were send based on the linked deliverypoint
        /// </summary>
        /// <param name="clientEntity">The client entity for which to remove the netmessages</param>
        void CancelNetmessagesOnDeliverypointChange(ClientEntity clientEntity);

        /// <summary>
        /// Fetch a netmessage collection based on an entity
        /// </summary>
        /// <param name="involvedEntity">The netmessage entity to search with</param>
        /// <returns>Collection of netmessages</returns>
        ICollection<NetmessageEntity> GetNetmessagesBasedOnEntity(NetmessageEntity involvedEntity);
    }
}
