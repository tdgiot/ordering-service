﻿using Crave.Ordering.Shared.Interfaces;
using Crave.Ordering.Shared.Messaging.Domain.UseCases.Requests;

namespace Crave.Ordering.Shared.Messaging.Interfaces.UseCases
{
    public interface ISendNetmessageUseCase : IUseCase<SendNetmessage, bool>
    {
    }
}
