﻿using Crave.Ordering.Shared.Interfaces;
using Crave.Ordering.Shared.Messaging.Domain.UseCases.Requests;

namespace Crave.Ordering.Shared.Messaging.Interfaces.UseCases
{
    internal interface ISaveNetmessagesUseCase : IUseCase<SaveNetmessages, bool>
    {
    }
}
