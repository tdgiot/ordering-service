﻿namespace Crave.Ordering.Shared.Messaging.Constants
{
    public static class MessagingConstants
    {
        public static readonly string GroupCompanyFormat = "Company-{0}";
        public static readonly string GroupCompanyTerminalsFormat = "Company-{0}-Terminals";
        public static readonly string GroupDeliverypointFormat = "Deliverypoint-{0}";
        public static readonly string GroupDeliverypointgroupFormat = "Deliverypointgroup-{0}";
    }
}
