﻿using System.Collections.Generic;
using Crave.Enums;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.Extensions;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.SQLServer;
using Crave.Ordering.Shared.Messaging.Interfaces.Repositories;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Crave.Ordering.Shared.Messaging.Data.Repositories
{
    /// <summary>
    /// Repository class for net messages.
    /// </summary>
    public class NetmessageRepository : INetmessageRepository
    {
        /// <inheritdoc/>
        public bool SaveNetmessages(IEnumerable<NetmessageEntity> netmessages)
        {
            bool success = true;
            using (DataAccessAdapter adapter = new DataAccessAdapter())
            {
                foreach (NetmessageEntity netmessage in netmessages)
                {
                    if (!adapter.SaveEntity(netmessage, false))
                    {
                        success = false;
                    }
                }
            }
            return success;
        }

        /// <inheritdoc/>
        public void CancelNetmessagesOnDeliverypointgroupChange(ClientEntity clientEntity)
        {
            RelationPredicateBucket netmessageFilterBucket = new RelationPredicateBucket();
            netmessageFilterBucket.PredicateExpression.Add(NetmessageFields.ReceiverClientId == clientEntity.ClientId);
            netmessageFilterBucket.PredicateExpression.Add(NetmessageFields.Status == NetmessageStatus.WaitingForVerification);

            PredicateExpression subFilter = new PredicateExpression();
            subFilter.Add(NetmessageFields.ReceiverDeliverypointgroupId == (int)clientEntity.DbValue(ClientFields.DeliverypointgroupId));
            subFilter.AddWithOr(NetmessageFields.ReceiverDeliverypointId > 0);

            netmessageFilterBucket.PredicateExpression.Add(subFilter);

            // Set status of all messages matching the filter
            NetmessageEntity netmessageUpdate = new NetmessageEntity();
            netmessageUpdate.Status = NetmessageStatus.CancelledNewConfiguration;

            DataAccessAdapter.UpdateEntitiesDirectly(netmessageUpdate, netmessageFilterBucket, clientEntity.Adapter);
        }

        /// <inheritdoc/>
        public void CancelNetmessagesOnDeliverypointChange(ClientEntity clientEntity)
        {
            IncludeFieldsList includes = new IncludeFieldsList(ClientFields.DeliverypointId);
            ClientEntity refreshedClientEntity = DataAccessAdapter.FetchEntityById<ClientEntity>(clientEntity.ClientId, null, includes, clientEntity.Adapter);

            if (refreshedClientEntity.DeliverypointId.HasValue)
            {
                RelationPredicateBucket netmessageFilterBucket = new RelationPredicateBucket();
                netmessageFilterBucket.PredicateExpression.Add(NetmessageFields.ReceiverClientId == clientEntity.ClientId);
                netmessageFilterBucket.PredicateExpression.Add(NetmessageFields.Status == NetmessageStatus.WaitingForVerification);
                netmessageFilterBucket.PredicateExpression.Add(NetmessageFields.ReceiverDeliverypointId == (int)refreshedClientEntity.DbValue(ClientFields.DeliverypointId));

                // Set status of all messages matching the filter
                NetmessageEntity netmessageUpdate = new NetmessageEntity();
                netmessageUpdate.Status = NetmessageStatus.CancelledNewConfiguration;

                DataAccessAdapter.UpdateEntitiesDirectly(netmessageUpdate, netmessageFilterBucket, clientEntity.Adapter);
            }
        }

        public ICollection<NetmessageEntity> GetNetmessagesBasedOnEntity(NetmessageEntity involvedEntity)
        {
            RelationPredicateBucket filterBucket = new RelationPredicateBucket();
            filterBucket.PredicateExpression.Add(NetmessageFields.MessageType == involvedEntity.MessageType);
            filterBucket.PredicateExpression.Add(NetmessageFields.Status == NetmessageStatus.WaitingForVerification);
            filterBucket.PredicateExpression.Add(NetmessageFields.SenderClientId == involvedEntity.SenderClientId);
            filterBucket.PredicateExpression.Add(NetmessageFields.SenderCompanyId == involvedEntity.SenderCompanyId);
            filterBucket.PredicateExpression.Add(NetmessageFields.SenderDeliverypointId == involvedEntity.SenderDeliverypointId);
            filterBucket.PredicateExpression.Add(NetmessageFields.SenderTerminalId == involvedEntity.SenderTerminalId);
            filterBucket.PredicateExpression.Add(NetmessageFields.ReceiverClientId == involvedEntity.ReceiverClientId);
            filterBucket.PredicateExpression.Add(NetmessageFields.ReceiverCompanyId == involvedEntity.ReceiverCompanyId);
            filterBucket.PredicateExpression.Add(NetmessageFields.ReceiverDeliverypointId == involvedEntity.ReceiverDeliverypointId);
            filterBucket.PredicateExpression.Add(NetmessageFields.ReceiverDeliverypointgroupId == involvedEntity.ReceiverDeliverypointgroupId);
            filterBucket.PredicateExpression.Add(NetmessageFields.ReceiverTerminalId == involvedEntity.ReceiverTerminalId);

            return DataAccessAdapter.FetchEntityCollection<NetmessageEntity>(filterBucket, 0, null, null, null, involvedEntity.Adapter);
        }
    }
}
