﻿using System;
using System.Collections.Generic;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.SQLServer;
using Crave.Ordering.Shared.Messaging.Interfaces.Repositories;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Crave.Ordering.Shared.Messaging.Data.Repositories
{
    /// <summary>
    /// Repository class for devices.
    /// </summary>
    public class DeviceRepository : IDeviceRepository
    {
        /// <inheritdoc/>
        public DeviceEntity GetDevice(string identifier)
        {
            // Create the query parameters for fetching the device
            QueryParameters queryParameters = new QueryParameters
            {
                FilterToUse = new PredicateExpression(DeviceFields.Identifier == identifier),
                ExcludedIncludedFields = new IncludeFieldsList(DeviceFields.DeviceId, DeviceFields.Identifier, DeviceFields.Token),
                CacheResultset = true,
                CacheTag = $"GetDevice-{identifier}",
                CacheDuration = new TimeSpan(0, 5, 0) // Cache the device for 5 minutes
            };

            return DataAccessAdapter.FetchEntity<DeviceEntity>(queryParameters);
        }

        /// <inheritdoc/>
        public DeviceEntity GetDevice(int deviceId)
        {
            return DataAccessAdapter.FetchEntityById<DeviceEntity>(deviceId, null, null);
        }

        /// <inheritdoc/>
        public DeviceEntity GetDeviceForClient(int clientId)
        {
            RelationPredicateBucket filterBucket = new RelationPredicateBucket(ClientFields.ClientId == clientId);
            filterBucket.Relations.Add(DeviceEntity.Relations.ClientEntityUsingDeviceId);

            return DataAccessAdapter.FetchEntity<DeviceEntity>(filterBucket, null, new IncludeFieldsList(DeviceFields.Identifier));
        }

        /// <inheritdoc/>
        public DeviceEntity GetDeviceForTerminal(int terminalId)
        {
            RelationPredicateBucket filterBucket = new RelationPredicateBucket(TerminalFields.TerminalId == terminalId);
            filterBucket.Relations.Add(DeviceEntity.Relations.TerminalEntityUsingDeviceId);

            return DataAccessAdapter.FetchEntity<DeviceEntity>(filterBucket, null, new IncludeFieldsList(DeviceFields.Identifier));
        }

        /// <inheritdoc/>
        public IEnumerable<DeviceEntity> GetDevicesForCompanyClients(int companyId)
        {
            RelationPredicateBucket filterBucket = new RelationPredicateBucket();
            filterBucket.PredicateExpression.Add(ClientFields.CompanyId == companyId);
            filterBucket.Relations.Add(DeviceEntity.Relations.ClientEntityUsingDeviceId);

            PrefetchPath2 prefetch = new PrefetchPath2(EntityType.DeviceEntity);
            prefetch.Add(DeviceEntity.PrefetchPathClientCollection, new IncludeFieldsList(ClientFields.ClientId, ClientFields.CompanyId));

            return DataAccessAdapter.FetchEntityCollection<DeviceEntity>(filterBucket, 0, null, prefetch, new IncludeFieldsList(DeviceFields.DeviceId, DeviceFields.Identifier));
        }

        /// <inheritdoc/>
        public IEnumerable<DeviceEntity> GetDevicesForCompanyTerminals(int companyId)
        {
            RelationPredicateBucket filterBucket = new RelationPredicateBucket();
            filterBucket.PredicateExpression.Add(TerminalFields.CompanyId == companyId);
            filterBucket.Relations.Add(DeviceEntity.Relations.TerminalEntityUsingDeviceId);

            PrefetchPath2 prefetch = new PrefetchPath2(EntityType.DeviceEntity);
            prefetch.Add(DeviceEntity.PrefetchPathTerminalCollection, new IncludeFieldsList(TerminalFields.TerminalId, TerminalFields.CompanyId));

            return DataAccessAdapter.FetchEntityCollection<DeviceEntity>(filterBucket, 0, null, prefetch, new IncludeFieldsList(DeviceFields.DeviceId, DeviceFields.Identifier));
        }

        /// <inheritdoc/>
        public IEnumerable<DeviceEntity> GetDevicesForDeliverypoint(int deliverypointId)
        {
            RelationPredicateBucket filterBucket = new RelationPredicateBucket();
            filterBucket.PredicateExpression.Add(ClientFields.DeliverypointId == deliverypointId);
            filterBucket.Relations.Add(DeviceEntity.Relations.ClientEntityUsingDeviceId);

            PrefetchPath2 prefetch = new PrefetchPath2(EntityType.DeviceEntity);
            prefetch.Add(DeviceEntity.PrefetchPathClientCollection, new IncludeFieldsList(ClientFields.ClientId, ClientFields.CompanyId, ClientFields.DeliverypointId));

            return DataAccessAdapter.FetchEntityCollection<DeviceEntity>(filterBucket, 0, null, prefetch, new IncludeFieldsList(DeviceFields.DeviceId, DeviceFields.Identifier));
        }

        /// <inheritdoc/>
        public IEnumerable<DeviceEntity> GetDevicesForDeliverypointgroup(int deliverypointgroupId)
        {
            RelationPredicateBucket filterBucket = new RelationPredicateBucket();
            filterBucket.PredicateExpression.Add(ClientFields.DeliverypointgroupId == deliverypointgroupId);
            filterBucket.Relations.Add(DeviceEntity.Relations.ClientEntityUsingDeviceId);

            PrefetchPath2 prefetch = new PrefetchPath2(EntityType.DeviceEntity);
            prefetch.Add(DeviceEntity.PrefetchPathClientCollection, new IncludeFieldsList(ClientFields.ClientId, ClientFields.CompanyId, ClientFields.DeliverypointgroupId));

            return DataAccessAdapter.FetchEntityCollection<DeviceEntity>(filterBucket, 0, null, prefetch, new IncludeFieldsList(DeviceFields.DeviceId, DeviceFields.Identifier));
        }

        /// <inheritdoc/>
        public IEnumerable<DeviceEntity> GetDevicesForCustomer(int customerId)
        {
            RelationPredicateBucket filter = new RelationPredicateBucket(DeviceFields.CustomerId == customerId);
            IncludeFieldsList includeFields = new IncludeFieldsList(DeviceFields.Identifier);

            return DataAccessAdapter.FetchEntityCollection<DeviceEntity>(filter, 0, null, null, includeFields);
        }

        /// <inheritdoc/>
        public bool Save(DeviceEntity deviceEntity)
        {
            return DataAccessAdapter.Save(deviceEntity, false);
        }
    }
}
