﻿using Crave.Ordering.Shared.Messaging.Netmessages;

// Do not change this namespace or else the Messaging will not receive your messages
// MessageFactory registers the models using typeof() so it's namespace dependent
namespace Crave.Backend.MessageQueue.Requests
{
    /// <summary>
    /// Model for sending a netmessage request on the message bus
    /// </summary>
    public class SendNetmessageRequest
    {
        /// <summary>
        /// Gets or sets the netmessage container
        /// </summary>
        public NetmessageContainer Container { get; set; }
    }
}