﻿using Crave.Ordering.Shared.Messaging.Netmessages;

namespace Crave.Ordering.Shared.Messaging.Domain.UseCases.Requests
{
    public class SendNetmessage
    {
        /// <summary>
        /// Gets or sets the netmessage to send
        /// </summary>
        public Netmessage Netmessage { get; set; }

        /// <summary>
        /// Gets or sets the recipient identifier or groups
        /// </summary>
        public NetmessageRecipients Recipients { get; set; } = new NetmessageRecipients();
    }
}
