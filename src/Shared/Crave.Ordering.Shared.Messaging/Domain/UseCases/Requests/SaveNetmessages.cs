﻿using System.Collections.Generic;
using Crave.Ordering.Shared.Messaging.Netmessages;

namespace Crave.Ordering.Shared.Messaging.Domain.UseCases.Requests
{
    internal class SaveNetmessages
    {
        public IEnumerable<Netmessage> Netmessages { get; set; }
    }
}
