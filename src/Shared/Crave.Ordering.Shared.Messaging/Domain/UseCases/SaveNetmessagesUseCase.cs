﻿using System.Collections.Generic;
using System.Linq;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Messaging.Converters;
using Crave.Ordering.Shared.Messaging.Domain.UseCases.Requests;
using Crave.Ordering.Shared.Messaging.Interfaces.Repositories;
using Crave.Ordering.Shared.Messaging.Interfaces.UseCases;
using Crave.Ordering.Shared.Messaging.Netmessages;

namespace Crave.Ordering.Shared.Messaging.Domain.UseCases
{
    internal class SaveNetmessagesUseCase : ISaveNetmessagesUseCase
    {
        #region Fields

        private readonly INetmessageRepository netmessageRepository;

        #endregion

        #region Constructors

        public SaveNetmessagesUseCase(INetmessageRepository netmessageRepository)
        {
            this.netmessageRepository = netmessageRepository;
        }

        #endregion

        public bool Execute(SaveNetmessages request) =>
            request.Netmessages != null && request.Netmessages.Any() && netmessageRepository.SaveNetmessages(GetNetmessages(request.Netmessages));

        private List<NetmessageEntity> GetNetmessages(IEnumerable<Netmessage> requestMessages)
        {
            List<NetmessageEntity> list = new List<NetmessageEntity>();

            foreach (Netmessage netmessage in requestMessages)
            {
                NetmessageEntity entity = NetmessageConverter.ConvertModelToEntity(netmessage);

                if (!CheckForDuplicate(ref entity, netmessage))
                {
                    continue;
                }

                entity.MessageTypeText = entity.MessageType.ToString();
                entity.StatusText = entity.Status.ToString();

                list.Add(entity);
            }

            return list;
        }

        private bool CheckForDuplicate(ref NetmessageEntity entity, Netmessage model)
        {
            // Select all non-submitted netmessage rows from database where messagetype is the same as useableEntity
            ICollection<NetmessageEntity> netmessageCollection = this.netmessageRepository.GetNetmessagesBasedOnEntity(entity);

            if (netmessageCollection.Count <= 0)
            { return true; }

            foreach (NetmessageEntity netmessageEntity in netmessageCollection)
            {
                Netmessage savedModel = NetmessageConverter.ConvertEntityToModel(netmessageEntity);

                bool isDuplicate = model.IsDuplicateMessage(savedModel);

                if (!isDuplicate)
                { continue; }

                netmessageEntity.FieldValue1 = entity.FieldValue1;
                netmessageEntity.FieldValue2 = entity.FieldValue2;
                netmessageEntity.FieldValue3 = entity.FieldValue3;
                netmessageEntity.FieldValue4 = entity.FieldValue4;
                netmessageEntity.FieldValue5 = entity.FieldValue5;
                netmessageEntity.FieldValue6 = entity.FieldValue6;
                netmessageEntity.FieldValue7 = entity.FieldValue7;
                netmessageEntity.FieldValue8 = entity.FieldValue8;
                netmessageEntity.FieldValue9 = entity.FieldValue9;
                netmessageEntity.MessageLog = model.MessageLog;

                netmessageEntity.IsNew = false;
                netmessageEntity.Validator = null;

                // Switch new entity for old one
                entity = netmessageEntity;

                return true;
            }

            return false;
        }
    }
}