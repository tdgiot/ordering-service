﻿using System;
using System.Collections.Generic;
using System.Reflection;
using Crave.Backend.MessageQueue.Requests;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Messaging.Domain.UseCases.Requests;
using Crave.Ordering.Shared.Messaging.Interfaces.Repositories;
using Crave.Ordering.Shared.Messaging.Interfaces.UseCases;
using Crave.Ordering.Shared.Messaging.Netmessages;
using ServiceStack.Messaging;

namespace Crave.Ordering.Shared.Messaging.Domain.UseCases
{
    internal class SendNetmessageUseCase : ISendNetmessageUseCase
    {
        public enum SendNetmessageUseCaseResult
        {
            NetmessageIsNull = 1,
            NetmessageHasNoRecipients = 2
        }

        #region Fields

        private readonly IMessageFactory mqFactory;
        private readonly IDeviceRepository deviceRepository;

        private readonly ISaveNetmessagesUseCase saveNetmessagesUseCase;

        #endregion

        #region Constructors

        public SendNetmessageUseCase(IMessageFactory mqFactory, 
                                     IDeviceRepository deviceRepository,
                                     ISaveNetmessagesUseCase saveNetmessagesUseCase)
        {
            this.mqFactory = mqFactory;
            this.deviceRepository = deviceRepository;
            this.saveNetmessagesUseCase = saveNetmessagesUseCase;
        }

        #endregion

        public bool Execute(SendNetmessage request)
        {
            if (request.Netmessage == null)
            {
                throw new CraveException(SendNetmessageUseCaseResult.NetmessageIsNull);
            }
            if (!request.Recipients.HasRecipients)
            {
                throw new CraveException(SendNetmessageUseCaseResult.NetmessageHasNoRecipients);
            }

            Netmessage netmessage = request.Netmessage;

            if (netmessage.SaveToDatabase)
            {
                IEnumerable<Netmessage> netmessages = this.CreateNetmessagePerRecipients(netmessage, request.Recipients);
                this.saveNetmessagesUseCase.Execute(new SaveNetmessages
                                                    {
                                                        Netmessages = netmessages
                                                    });
            }

            // Create the message container
            NetmessageContainer netmessageContainer = new NetmessageContainer();
            netmessageContainer.Instance = "SendNetmessageUseCase";
            netmessageContainer.Data = netmessage.ToJson();
            netmessageContainer.Sender = netmessage.SenderIdentifier;

            // Set the recipients
            if (!request.Recipients.Identifier.IsNullOrWhiteSpace())
            { 
                netmessageContainer.Receiver = request.Recipients.Identifier;
            }
            else if (request.Recipients.Groups.Count > 0)
            {
                netmessageContainer.Groups = request.Recipients.Groups.ToArray();
            }

            // Publish the message on the message bus
            using (IMessageProducer mqClient = this.mqFactory.CreateMessageProducer())
            {
                mqClient.Publish(new SendNetmessageRequest {Container = netmessageContainer});
            }

            return true;
        }        

        private IEnumerable<Netmessage> CreateNetmessagePerRecipients(Netmessage netmessage, NetmessageRecipients recipients)
        {
            List<Netmessage> netmessages = new List<Netmessage>();

            if (!recipients.Identifier.IsNullOrWhiteSpace())
            {
                netmessages.Add(SendNetmessageUseCase.CreateNetmessageForIdentifier(netmessage, recipients.Identifier));
            }
            else if (recipients.Groups.Count > 0)
            {
                netmessages.AddRange(this.CreateNetmessagesForGroups(netmessage, recipients.Groups));
            }

            return netmessages;
        }

        private static Netmessage CreateNetmessageForIdentifier(Netmessage netmessage, string receiverIdentifier)
        {
            Netmessage clone = netmessage.ConvertTo<Netmessage>();
            clone.Guid = Guid.NewGuid().WithoutDashes();
            clone.ReceiverIdentifier = receiverIdentifier;

            return clone;
        }

        private IEnumerable<Netmessage> CreateNetmessagesForGroups(Netmessage netmessage, List<string> groups)
        {
            List<Netmessage> netmessages = new List<Netmessage>();

            foreach (string group in groups)
            {
                IEnumerable<DeviceEntity> devices = GetDevicesForGroup(group);

                foreach (DeviceEntity device in devices)
                {
                    netmessages.Add(CreateNetmessageForIdentifier(netmessage, device.Identifier));
                }
            }

            return netmessages;
        }

        private IEnumerable<DeviceEntity> GetDevicesForGroup(string group)
        {
            List<DeviceEntity> devices = new List<DeviceEntity>();

            if (!group.Contains("-"))
            {
                return devices;
            }

            string[] groupValues = group.Split('-');
            if (groupValues.Length <= 1)
            {
                return devices;
            }

            // See MessagingConstants for values
            string groupName = groupValues[0];
            string groupValue = groupValues[1];
            
            if (!int.TryParse(groupValue, out int value))
            {
                return devices;
            }

            if (groupName.Equals("Company", StringComparison.InvariantCultureIgnoreCase))
            {
                string subGroupName = groupValues.Length > 2 ? groupValues[groupValues.Length - 1] : string.Empty;

                if (subGroupName.Equals("Terminals", StringComparison.InvariantCultureIgnoreCase))
                {
                    devices.AddRange(this.deviceRepository.GetDevicesForCompanyTerminals(value));
                }
                else if (subGroupName.IsNullOrWhiteSpace())
                {
                    devices.AddRange(this.deviceRepository.GetDevicesForCompanyClients(value));
                }
            }
            else if (groupName.Equals("Deliverypointgroup", StringComparison.InvariantCultureIgnoreCase))
            {
                devices.AddRange(this.deviceRepository.GetDevicesForDeliverypointgroup(value));
            }
            else if (groupName.Equals("Deliverypoint", StringComparison.InvariantCultureIgnoreCase))
            {
                devices.AddRange(this.deviceRepository.GetDevicesForDeliverypoint(value));
            }

            return devices;
        }
    }
}