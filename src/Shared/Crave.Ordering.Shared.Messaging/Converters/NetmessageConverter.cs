﻿using Crave.Enums;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Extensions;
using Crave.Ordering.Shared.Messaging.Netmessages;

namespace Crave.Ordering.Shared.Messaging.Converters
{
    public static class NetmessageConverter
    {
        public static Netmessage ConvertEntityToModel(NetmessageEntity entity)
        {
            Netmessage netmessage = new Netmessage();
            netmessage.NetmessageId = entity.NetmessageId;
            netmessage.MessageType = entity.MessageType;
            netmessage.Guid = entity.Guid;
            netmessage.ReceiverIdentifier = entity.ReceiverIdentifier;
            netmessage.SenderIdentifier = entity.SenderIdentifier;
            netmessage.ReceiverClientId = entity.ReceiverClientId.GetValueOrDefault(0);
            netmessage.ReceiverCompanyId = entity.ReceiverCompanyId.GetValueOrDefault(0);
            netmessage.ReceiverTerminalId = entity.ReceiverTerminalId.GetValueOrDefault(0);
            netmessage.SenderClientId = entity.SenderClientId.GetValueOrDefault(0);
            netmessage.SenderTerminalId = entity.SenderTerminalId.GetValueOrDefault(0);
            netmessage.FieldValue1 = entity.FieldValue1;
            netmessage.FieldValue2 = entity.FieldValue2;
            netmessage.FieldValue3 = entity.FieldValue3;
            netmessage.FieldValue4 = entity.FieldValue4;
            netmessage.FieldValue5 = entity.FieldValue5;
            netmessage.FieldValue6 = entity.FieldValue6;
            netmessage.FieldValue7 = entity.FieldValue7;
            netmessage.FieldValue8 = entity.FieldValue8;
            netmessage.FieldValue9 = entity.FieldValue9;
            netmessage.Submitted = (entity.Status == NetmessageStatus.Completed);
            netmessage.Status = entity.Status.ToString();
            netmessage.CreatedUTC = entity.CreatedUTC.GetValueOrDefault();
            netmessage.MessageLog = entity.MessageLog;

            return netmessage;
        }

        public static NetmessageEntity ConvertModelToEntity(Netmessage response)
        {
            NetmessageEntity entity = new NetmessageEntity();
            entity.NetmessageId = response.NetmessageId;
            entity.Guid = response.Guid;
            entity.MessageType = response.MessageType;
            entity.ReceiverIdentifier = response.ReceiverIdentifier;
            entity.SenderIdentifier = response.SenderIdentifier;
            entity.ReceiverClientId = response.ReceiverClientId.ToValueOrNullIfZero();
            entity.ReceiverCompanyId = response.ReceiverCompanyId.ToValueOrNullIfZero();
            entity.ReceiverTerminalId = response.ReceiverTerminalId.ToValueOrNullIfZero();
            entity.SenderClientId = response.SenderClientId.ToValueOrNullIfZero();
            entity.SenderTerminalId = response.SenderTerminalId.ToValueOrNullIfZero();
            entity.FieldValue1 = response.FieldValue1;
            entity.FieldValue2 = response.FieldValue2;
            entity.FieldValue3 = response.FieldValue3;
            entity.FieldValue4 = response.FieldValue4;
            entity.FieldValue5 = response.FieldValue5;
            entity.FieldValue6 = response.FieldValue6;
            entity.FieldValue7 = response.FieldValue7;
            entity.FieldValue8 = response.FieldValue8;
            entity.FieldValue9 = response.FieldValue9;
            entity.CreatedUTC = response.CreatedUTC;
            entity.MessageLog = response.MessageLog;

            return entity;
        }
    }
}
