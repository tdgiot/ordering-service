﻿namespace Crave.Ordering.Shared.Configuration.Interfaces
{
    public interface IConfigurationSection
    {
        string Name { get; }
    }
}
