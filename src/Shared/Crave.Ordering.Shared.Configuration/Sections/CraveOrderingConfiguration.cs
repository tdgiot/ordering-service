﻿using System.ComponentModel.DataAnnotations;

namespace Crave.Ordering.Shared.Configuration.Sections
{
    public class CraveOrderingConfiguration
    {
        [Required]
        public string ApiUrl { get; set; }
    }
}
