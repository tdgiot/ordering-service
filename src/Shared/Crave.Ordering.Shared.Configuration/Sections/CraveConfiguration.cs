﻿using System.ComponentModel.DataAnnotations;
using Crave.Ordering.Shared.Configuration.Interfaces;

namespace Crave.Ordering.Shared.Configuration.Sections
{
    public class CraveConfiguration : IConfigurationSection
    {
        public string Name => "Crave";

        [Required]
        public CraveOrderingConfiguration Ordering { get; set; }
    }
}
