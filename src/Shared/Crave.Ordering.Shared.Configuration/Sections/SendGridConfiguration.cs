﻿using Crave.Ordering.Shared.Configuration.Interfaces;
using System.ComponentModel.DataAnnotations;

namespace Crave.Ordering.Shared.Configuration.Sections
{
    public class SendGridConfiguration : IConfigurationSection
    {
        public string Name => "SendGrid";

        [Required]
        public string ApiKey { get; set; }
    }
}
