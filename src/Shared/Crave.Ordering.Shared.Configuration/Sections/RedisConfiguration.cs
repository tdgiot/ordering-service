﻿using Crave.Ordering.Shared.Configuration.Interfaces;

namespace Crave.Ordering.Shared.Configuration.Sections
{
    public class RedisConfiguration : IConfigurationSection
    {
        public string Name => "Redis";

        public string Hosts { get; set; }
    }
}
