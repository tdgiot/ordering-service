﻿using Crave.Ordering.Shared.Configuration.Interfaces;
using System.ComponentModel.DataAnnotations;

namespace Crave.Ordering.Shared.Configuration.Sections
{
    public class MessageBirdConfiguration : IConfigurationSection
    {
        public string Name => "MessageBird";

        [Required]
        public string AccessKey { get; set; }
    }
}
