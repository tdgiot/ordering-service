﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using IConfigurationSection = Crave.Ordering.Shared.Configuration.Interfaces.IConfigurationSection;

namespace Crave.Ordering.Shared.Configuration.Extensions
{
    public static class ConfigurationExtensions
    {
        public static TSection ReadAndValidate<TSection>(this IConfiguration configuration) where TSection : class, IConfigurationSection, new()
        {
            if (configuration == null)
            {
                throw new ArgumentNullException(nameof(configuration));
            }

            TSection section = new TSection();

            configuration.GetSection(section.Name).Bind(section);

            List<ValidationResult> validationResults = new List<ValidationResult>();

            if (!Validator.TryValidateObject(section,
                                             new ValidationContext(section),
                                             validationResults))
            {
                throw new InvalidOperationException($"Invalid section {typeof(TSection).Name}, Message={string.Join(", ", validationResults)}");
            }

            return section;
        }
    }
}
