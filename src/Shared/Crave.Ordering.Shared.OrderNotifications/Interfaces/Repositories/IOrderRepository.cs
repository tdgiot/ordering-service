﻿using System.Threading.Tasks;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Ordering.Shared.OrderNotifications.Interfaces.Repositories
{
    /// <summary>
    /// Interface which is used to implement order repository classes.
    /// </summary>
    internal interface IOrderRepository
    {
        /// <summary>
        /// Gets the order for a receipt with specified id.
        /// </summary>
        /// <param name="orderId">The id of the order to get.</param>
        /// <returns>A <see cref="OrderEntity"/> instance.</returns>
        Task<OrderEntity> GetOrderForNotificationAsync(int orderId);
    }
}
