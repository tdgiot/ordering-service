﻿using System.Threading.Tasks;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Ordering.Shared.OrderNotifications.Interfaces.Repositories
{
    internal interface IOrderNotificationLogRepository
    {
        /// <summary>
        /// Saves an order notification log entity.
        /// </summary>
        /// <param name="entity">The order notification log to save.</param>
        /// <returns>A <see cref="OrderNotificationLogEntity"/> instance.</returns>
        Task<OrderNotificationLogEntity> SaveEntityAsync(OrderNotificationLogEntity entity);
    }
}