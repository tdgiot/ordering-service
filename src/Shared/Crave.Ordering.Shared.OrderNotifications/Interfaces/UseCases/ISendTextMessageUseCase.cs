﻿using Crave.Ordering.Shared.Interfaces;
using Crave.Ordering.Shared.OrderNotifications.Domain.UseCases.Requests;
using Crave.Ordering.Shared.OrderNotifications.Domain.UseCases.Responses;

namespace Crave.Ordering.Shared.OrderNotifications.Interfaces.UseCases
{
    public interface ISendTextMessageUseCase : IUseCase<SendSMSRequest, MessageQueueReponse>
    {
    }
}
