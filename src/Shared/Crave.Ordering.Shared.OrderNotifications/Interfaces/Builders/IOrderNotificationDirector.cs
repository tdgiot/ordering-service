﻿using Crave.Ordering.Shared.OrderNotifications.Models;

namespace Crave.Ordering.Shared.OrderNotifications.Interfaces.Builders
{
    public interface IOrderNotificationDirector
    {
        public OrderNotification BuildNotification();
    }
}
