﻿using Crave.Ordering.Shared.OrderNotifications.Models;

namespace Crave.Ordering.Shared.OrderNotifications.Interfaces.Builders
{
    public interface IOrderNotificationBuilder
    {
        IOrderNotificationBuilder AddCompanyInfo();
        IOrderNotificationBuilder AddSellerInfo();
        IOrderNotificationBuilder AddReceiptInfo();
        IOrderNotificationBuilder AddServiceMethodInfo();
        IOrderNotificationBuilder AddCheckoutMethodInfo();
        IOrderNotificationBuilder AddPaymentInfo();
        IOrderNotificationBuilder AddOrderInfo();
        IOrderNotificationBuilder AddOrderTotalInfo();
        IOrderNotificationBuilder AddCoversCountInfo();
        IOrderNotificationBuilder AddTaxBreakdownInfo();
        IOrderNotificationBuilder AddCustomerInfo();
        IOrderNotificationBuilder AddDeliveryInformation();
        IOrderNotificationBuilder AddPaymentProcessingInfo();
        IOrderNotificationBuilder AddDeliverypointInfo();
        IOrderNotificationBuilder AddTaxInfo();
        OrderNotification Build();
    }
}
