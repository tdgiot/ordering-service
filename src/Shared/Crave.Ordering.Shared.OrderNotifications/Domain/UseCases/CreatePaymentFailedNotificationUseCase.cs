﻿using Crave.Ordering.Shared.Interfaces;
using OrderNotification = Crave.Ordering.Shared.OrderNotifications.Models.OrderNotification;

namespace Crave.Ordering.Shared.OrderNotifications.Domain.UseCases
{
    internal class CreatePaymentFailedNotificationUseCase : IUseCase<object, OrderNotification>
    {
        public OrderNotification Execute(object request)
        {
            return new OrderNotification(Properties.Resources.PaymentFailed_MailSubject,
                Properties.Resources.PaymentFailedNotification,
                Properties.Resources.PaymentFailed_ShortMessage,
                false);
        }
    }
}