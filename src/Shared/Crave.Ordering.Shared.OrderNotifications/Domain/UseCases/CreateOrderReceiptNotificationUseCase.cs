﻿using Crave.Ordering.Shared.Interfaces;
using OrderNotification = Crave.Ordering.Shared.OrderNotifications.Models.OrderNotification;

namespace Crave.Ordering.Shared.OrderNotifications.Domain.UseCases
{
    internal class CreateOrderReceiptNotificationUseCase : IUseCase<object, OrderNotification>
    {
        public OrderNotification Execute(object request)
        {
            return new OrderNotification(Properties.Resources.OrderReceipt_MailSubject,
                Properties.Resources.OrderReceiptNotification,
                Properties.Resources.OrderReceipt_ShortMessage);
        }
    }
}