﻿using Crave.Enums;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Interfaces;
using OrderNotification = Crave.Ordering.Shared.OrderNotifications.Models.OrderNotification;

namespace Crave.Ordering.Shared.OrderNotifications.Domain.UseCases
{
    internal class CreateOrderConfirmationNotificationUseCase : IUseCase<OrderEntity, OrderNotification>
    {
        public OrderNotification Execute(OrderEntity request)
        {
            if (request.CheckoutMethodType == CheckoutType.PayLater)
            {
                return new OrderNotification(Properties.Resources.OrderConfirmation_MailSubject,
                    Properties.Resources.OrderConfirmationNotification_PaymentStatus,
                    Properties.Resources.OrderConfirmation_ShortMessage);
            }

            return new OrderNotification(Properties.Resources.OrderConfirmation_MailSubject,
                Properties.Resources.OrderConfirmationNotification,
                Properties.Resources.OrderConfirmation_ShortMessage);
        }
    }
}