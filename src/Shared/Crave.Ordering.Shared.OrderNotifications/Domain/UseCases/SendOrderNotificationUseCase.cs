﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Crave.Enums;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.Extensions;
using Crave.Ordering.Shared.OrderNotifications.Builders;
using Crave.Ordering.Shared.OrderNotifications.Domain.UseCases.Requests;
using Crave.Ordering.Shared.OrderNotifications.Domain.UseCases.Responses;
using Crave.Ordering.Shared.OrderNotifications.Interfaces.Repositories;
using Crave.Ordering.Shared.OrderNotifications.Interfaces.UseCases;
using Microsoft.Extensions.Logging;
using IOrderNotificationBuilder = Crave.Ordering.Shared.OrderNotifications.Interfaces.Builders.IOrderNotificationBuilder;
using OrderNotification = Crave.Ordering.Shared.OrderNotifications.Models.OrderNotification;
using OrderNotificationDirector = Crave.Ordering.Shared.OrderNotifications.Builders.OrderNotificationDirector;

namespace Crave.Ordering.Shared.OrderNotifications.Domain.UseCases
{
    internal class SendOrderNotificationUseCase : ISendOrderNotificationUseCase
    {
        private readonly ILogger<SendOrderNotificationUseCase> logger;
        private readonly IOrderRepository orderRepository;
        private readonly GetOrderNotificationRecipientsUseCase getOrderNotificationRecipientsUseCase;        
        private readonly GetOrderNotificationSenderUseCase getOrderNotificationSenderUseCase;
        private readonly CreateOrderNotificationTemplateUseCase createOrderNotificationTemplateUseCase;
        private readonly SendNotificationUseCase sendNotificationUseCase;
        private readonly CreateOrderNotificationLogUseCase createOrderNotificationLogUseCase;

        /// <summary>
        /// Initializes a new instance of the <see cref="SendOrderNotificationUseCase"/> class.
        /// </summary>
        public SendOrderNotificationUseCase(ILogger<SendOrderNotificationUseCase> logger,
                                            IOrderRepository orderRepository,
                                            GetOrderNotificationRecipientsUseCase getOrderNotificationRecipientsUseCase,
                                            GetOrderNotificationSenderUseCase getOrderNotificationSenderUseCase,
                                            CreateOrderNotificationTemplateUseCase createOrderNotificationTemplateUseCase,
                                            SendNotificationUseCase sendNotificationUseCase,
                                            CreateOrderNotificationLogUseCase createOrderNotificationLogUseCase)
        {
            this.logger = logger;
            this.orderRepository = orderRepository;
            this.getOrderNotificationRecipientsUseCase = getOrderNotificationRecipientsUseCase;
            this.getOrderNotificationSenderUseCase = getOrderNotificationSenderUseCase;
            this.createOrderNotificationTemplateUseCase = createOrderNotificationTemplateUseCase;
            this.sendNotificationUseCase = sendNotificationUseCase;
            this.createOrderNotificationLogUseCase = createOrderNotificationLogUseCase;
        }

        public async Task<SendOrderNotificationResponse> ExecuteAsync(SendOrderNotificationRequest request)
        {
            SendOrderNotificationResponse response = await this.ExecuteInternalAsync(request);

            if (!response.Message.IsNullOrWhiteSpace())
            {
                if (response.Success)
                {
                    this.logger.LogInformation($"[CreateAndSendOrderNotificationUseCase] {response.Message}", response.Args);
                }
                else if (!response.Success)
                {
                    this.logger.LogWarning($"[CreateAndSendOrderNotificationUseCase] {response.Message}", response.Args);
                }
            }

            return response;
        }

        private async Task<SendOrderNotificationResponse> ExecuteInternalAsync(SendOrderNotificationRequest requestDto)
        {
            OrderEntity orderEntity = await orderRepository.GetOrderForNotificationAsync(requestDto.OrderId);
            if (orderEntity.IsNullOrNew())
            {
                return new SendOrderNotificationResponse(false, "No order found (OrderId={OrderId})", requestDto.OrderId);
            }

            if (orderEntity.Outlet.IsNullOrNew())
            {
                // When no outlet is linked to the order just eat the warning and continue - this use-case is also triggered for non-AppLess orders
                return new SendOrderNotificationResponse(false);
            }

            GetOrderNotificationSenderResponse getOrderNotificationSenderResponse = this.getOrderNotificationSenderUseCase.Execute(new GetOrderNotificationSenderRequest(orderEntity));
            if (!getOrderNotificationSenderResponse.Success)
            {
                return new SendOrderNotificationResponse(false, "Unable to retrieve sender for order (OrderId={OrderId}, ErrorMessage={ErrorMessage})", requestDto.OrderId, getOrderNotificationSenderResponse.ErrorMessage);
            }

            GetOrderNotificationRecipientsResponse getOrderNotificationRecipientsResponse = this.getOrderNotificationRecipientsUseCase.Execute(new GetOrderNotificationRecipientsRequest(orderEntity,
                                                                                                                                                                                         requestDto.OrderNotificationType,
                                                                                                                                                                                         getOrderNotificationSenderResponse.OrderNotificationSender,
                                                                                                                                                                                         requestDto.SendToOriginalReceiver,
                                                                                                                                                                                         requestDto.AdditionalEmailReceivers));

            if (!getOrderNotificationRecipientsResponse.Success)
            {
                return new SendOrderNotificationResponse(false, "Unable to retrieve recipients for order (OrderId={OrderId}, ErrorMessage={ErrorMessage})", requestDto.OrderId, getOrderNotificationRecipientsResponse.ErrorMessage);
            }
            if (!getOrderNotificationRecipientsResponse.Recipients.Any())
            {
                return new SendOrderNotificationResponse(true, "No recipients for order (OrderId={OrderId})", requestDto.OrderId);
            }

            OrderNotification orderNotificationTemplate = this.createOrderNotificationTemplateUseCase.Execute(new CreateOrderNotificationTemplateRequest(orderEntity, requestDto.OrderNotificationType));
            
            IOrderNotificationBuilder orderNotificationBuilder = new OrderNotificationBuilder()
                .WithTemplate(orderNotificationTemplate)
                .WithOrder(orderEntity);

            OrderNotification orderNotification = new OrderNotificationDirector(orderNotificationBuilder)
                .BuildNotification();

            SendNotificationResponse sendOrderNotificationResponse = await sendNotificationUseCase.ExecuteAsync(new SendNotificationRequest(getOrderNotificationSenderResponse.OrderNotificationSender!,
                                                                                                                                      getOrderNotificationRecipientsResponse.Recipients,
                                                                                                                                      orderNotification));

            return await this.LogNotificationsAsync(sendOrderNotificationResponse, orderEntity, requestDto.OrderNotificationType);
        }

        private async Task<SendOrderNotificationResponse> LogNotificationsAsync(SendNotificationResponse sendOrderNotificationResponse, OrderEntity orderEntity, OrderNotificationType orderNotificationType)
        {
            StringBuilder sb = new StringBuilder();
            bool alwaysLogResult = false;
            foreach (SendNotificationResponse.OrderNotificationResult notificationResult in sendOrderNotificationResponse.OrderNotificationResults)
            {
                sb.AppendFormatLine(" * {0}", notificationResult.ToString());

                OrderNotificationLogStatus status = notificationResult.Success ? OrderNotificationLogStatus.Success : OrderNotificationLogStatus.Failed;
                CreateOrderNotificationLog logRequest = new CreateOrderNotificationLog(orderEntity.OrderId,
                    orderEntity.CompanyId,
                    orderNotificationType,
                    notificationResult.Method,
                    status,
                    notificationResult.Receivers,
                    notificationResult.NotificationTitle,
                    notificationResult.NotificationMessage,
                    notificationResult.ErrorMessage);
                CreateOrderNotificationLogResponse createLogResponse = await this.createOrderNotificationLogUseCase.ExecuteAsync(logRequest);

                if (!createLogResponse.Success)
                {
                    alwaysLogResult = true;
                    sb.AppendFormatLine(" *** FAILED to save OrderNotificationLog: {0}", createLogResponse.ErrorMessage);
                }
            }

            int totalCount = sendOrderNotificationResponse.OrderNotificationResults.Count;
            int failedCount = sendOrderNotificationResponse.OrderNotificationResults.Count(x => !x.Success);

            if (failedCount == totalCount)
            {
                return new SendOrderNotificationResponse(false, "Failed to send notifications for order (OrderId={OrderId}, Notification={NotificationType}).\n\rLog={Log}", orderEntity.OrderId, orderNotificationType.ToString(), sb);
            }

            if (failedCount > 0 && failedCount < totalCount)
            {
                return new SendOrderNotificationResponse(false, "Some notifications failed to send for order (OrderId={OrderId}, Notification={NotificationType}).\n\rLog={Log}", orderEntity.OrderId, orderNotificationType.ToString(), sb);
            }

            return new SendOrderNotificationResponse(true, alwaysLogResult ? "Error occurred while saving notification log (OrderId={OrderId}, Notification={NotificationType}).\n\rLog={Log}" : string.Empty, orderEntity.OrderId, orderNotificationType.ToString(), sb);
        }
    }
}
