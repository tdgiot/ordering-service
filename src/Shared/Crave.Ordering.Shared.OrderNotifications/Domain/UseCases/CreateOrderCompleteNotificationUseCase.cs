﻿using System;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Interfaces;
using OrderNotification = Crave.Ordering.Shared.OrderNotifications.Models.OrderNotification;

namespace Crave.Ordering.Shared.OrderNotifications.Domain.UseCases
{
    internal class CreateOrderCompleteNotificationUseCase : IUseCase<OrderEntity, OrderNotification>
    {
        private const string ORDER_COMPLETE_MESSAGE = "[[ORDER_COMPLETE_MESSAGE]]";

        public OrderNotification Execute(OrderEntity request)
        {
            ServiceMethodEntity serviceMethod = request.ServiceMethod;

            string title = serviceMethod.OrderCompleteMailSubject.IsNullOrWhiteSpace() ? Properties.Resources.OrderComplete_DefaultMailSubject : serviceMethod.OrderCompleteMailSubject;
            string messageTemplate = serviceMethod.OrderCompleteMailMessage.IsNullOrWhiteSpace() ? Properties.Resources.OrderComplete_DefaultMessage : serviceMethod.OrderCompleteMailMessage;
            string shortMessage = serviceMethod.OrderCompleteTextMessage.IsNullOrWhiteSpace() ? Properties.Resources.OrderComplete_DefaultShortMessage : serviceMethod.OrderCompleteTextMessage;

            string message = Properties.Resources.OrderCompleteNotification.Replace(CreateOrderCompleteNotificationUseCase.ORDER_COMPLETE_MESSAGE, messageTemplate);

            return new OrderNotification(title, message, shortMessage);
        }
    }
}