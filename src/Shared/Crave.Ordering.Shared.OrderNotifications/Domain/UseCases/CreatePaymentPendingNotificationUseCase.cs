﻿using Crave.Ordering.Shared.Interfaces;
using OrderNotification = Crave.Ordering.Shared.OrderNotifications.Models.OrderNotification;

namespace Crave.Ordering.Shared.OrderNotifications.Domain.UseCases
{
    internal class CreatePaymentPendingNotificationUseCase : IUseCase<object, OrderNotification>
    {
        public OrderNotification Execute(object request)
        {
            return new OrderNotification(Properties.Resources.PaymentPending_MailSubject,
                Properties.Resources.PaymentPendingNotification,
                Properties.Resources.PaymentPending_ShortMessage);
        }
    }
}