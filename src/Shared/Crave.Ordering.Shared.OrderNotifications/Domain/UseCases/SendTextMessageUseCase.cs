﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Crave.Libraries.SMS;
using Crave.Ordering.Shared.OrderNotifications.Domain.UseCases.Requests;
using Crave.Ordering.Shared.OrderNotifications.Domain.UseCases.Responses;
using Crave.Ordering.Shared.OrderNotifications.Interfaces.UseCases;
using Microsoft.Extensions.Logging;
using MessageResult = Crave.Libraries.SMS.MessageResult;

namespace Crave.Ordering.Shared.OrderNotifications.Domain.UseCases
{
    /// <summary>
    /// Use case for sending text messages
    /// </summary>
    public class SendTextMessageUseCase : ISendTextMessageUseCase
    {
        private readonly ILogger<SendTextMessageUseCase> logger;
        private readonly ISmsProvider smsProvider;

        /// <summary>
        /// Initializes a new instance of the <see cref="SendTextMessageUseCase"/> class.
        /// </summary>
        /// <param name="logger">Logger</param>
        /// <param name="smsProvider">SMS provider</param>
        public SendTextMessageUseCase(ILogger<SendTextMessageUseCase> logger,
                                      ISmsProvider smsProvider)
        {
            this.logger = logger;
            this.smsProvider = smsProvider;
        }

        /// <inheritdoc />
        public MessageQueueReponse Execute(SendSMSRequest request)
        {
            MessageQueueReponse response = new MessageQueueReponse { Success = true };

            if (request.Message.IsNullOrWhiteSpace())
            {
                this.logger.LogWarning("SendTextMessageUseCase | No message specified, skipping. Request: {Request}", request);

                response.ErrorMessage = "No message specified";
                response.Success = false;

                return response;
            }

            if (request.Phonenumbers.IsNullOrEmpty())
            {
                this.logger.LogWarning("SendTextMessageUseCase | No phonenumbers specified, skipping. Request: {Request}", request);

                response.ErrorMessage = "No phonenumbers specified";
                response.Success = false;

                return response;
            }

            if (request.Sender.IsNullOrWhiteSpace())
            {
                request.Sender = "Crave";
            }

            this.logger.LogInformation("SendTextMessageUseCase | Recipients: {Recipients}, Message: {Message}, Sender: {Sender}", request.Phonenumbers, request.Message, request.Sender);

            StringBuilder sb = new StringBuilder();
            List<long> recipients = new List<long>();
            foreach (string phonenumber in request.Phonenumbers)
            {
                string numberString = phonenumber.Replace("+", "00");
                if (!long.TryParse(numberString, out long number))
                {
                    this.logger.LogWarning("SendTextMessageUseCase | Unable to parse phonenumber: {Number}", phonenumber);

                    sb.AppendLine("Unable to parse phonenumber {0}", phonenumber);
                    response.Success = false;

                    continue;
                }

                recipients.Add(number);
            }

            if (recipients.Count > 0)
            {
                // Send text message
                MessageResult result = this.smsProvider.SendTextMessage(request.Sender, request.Message, recipients);

                this.logger.LogInformation("SendTextMessageUseCase | Reference: {Reference}, Recipients: {Recipients}", result.Reference, result.Recipients);

                if (!result.Success)
                {
                    this.logger.LogWarning("SendTextMessageUseCase | Failed to send text message. ErrorMessage: {ErrorMessage}", result.ErrorMessage);
                    sb.AppendLine("Failed to send text message. Recipients: {0}, ErrorMessage: {1}", string.Join(",", recipients), result.ErrorMessage);
                    response.Success = false;
                }
                else
                {
                    response.Success = result.Recipients.All(x => x.Sent);

                    if (!response.Success)
                    { result.Recipients.Where(x => !x.Sent).ToList().ForEach(r => sb.AppendLine($"Failed to send message {result.Reference} to number {r.Number}")); }
                }
            }
            else
            {
                this.logger.LogWarning("SendTextMessageUseCase | No recipients to send message to, skipping. Request: {Request}", request);
                sb.AppendLine("No recipients, not sending message");
            }

            response.ErrorMessage = sb.ToString();

            return response;
        }
    }
}
