﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Crave.Enums;
using Crave.Ordering.Shared.Interfaces;
using Crave.Ordering.Shared.OrderNotifications.Domain.UseCases.Requests;
using Crave.Ordering.Shared.OrderNotifications.Domain.UseCases.Responses;
using Crave.Ordering.Shared.OrderNotifications.Interfaces.UseCases;
using Crave.Ordering.Shared.OrderNotifications.Models;
using OrderNotification = Crave.Ordering.Shared.OrderNotifications.Models.OrderNotification;

namespace Crave.Ordering.Shared.OrderNotifications.Domain.UseCases
{
    /// <summary>
    /// Send an order notification for an order.
    /// </summary>
    internal class SendNotificationUseCase : IUseCaseAsync<SendNotificationRequest, SendNotificationResponse>
    {
        private readonly ISendTextMessageUseCase sendTextMessageUseCase;
        private readonly ISendMailUseCase sendMailUseCase;

        /// <summary>
        /// Initializes a new instance of the <see cref="SendOrderNotificationUseCase"/> class.
        /// </summary>
        /// <param name="sendTextMessageUseCase">The use-case for sending text message.</param>
        /// <param name="sendMailUseCase">The use-case for sending email.</param>
        public SendNotificationUseCase(ISendTextMessageUseCase sendTextMessageUseCase,
                                       ISendMailUseCase sendMailUseCase)
        {
            this.sendTextMessageUseCase = sendTextMessageUseCase;
            this.sendMailUseCase = sendMailUseCase;
        }

        /// <inheritdoc />
        public async Task<SendNotificationResponse> ExecuteAsync(SendNotificationRequest request)
        {
            OrderNotificationSender sender = request.Sender;
            IReadOnlyCollection<OrderNotificationRecipient> recipients = request.Recipients;
            OrderNotification notification = request.Notification;

            SendNotificationResponse sendOrderNotificationResponse = new SendNotificationResponse();

            foreach (OrderNotificationRecipient recipient in recipients)
            {
                SendNotificationResponse.OrderNotificationResult response = await SendNotification(sender, recipient, notification);

                sendOrderNotificationResponse.AddResult(response);
            }

            return sendOrderNotificationResponse;
        }

        private async Task<SendNotificationResponse.OrderNotificationResult> SendNotification(OrderNotificationSender sender, OrderNotificationRecipient recipient, OrderNotification notification)
        {
            switch (recipient.Method)
            {
                case OrderNotificationMethod.SMS:
                    return SendTextNotification(sender, recipient, notification);

                case OrderNotificationMethod.Email:
                    return await SendEmailNotification(sender, recipient, notification);

                default:
                    return new SendNotificationResponse.OrderNotificationResult(recipient.Method, new string[0], notification.Title, notification.Message, false, $"Implementation for order notification method '{recipient.Method}' does not exists.");
            }
        }

        private SendNotificationResponse.OrderNotificationResult SendTextNotification(OrderNotificationSender sender, OrderNotificationRecipient recipient, OrderNotification notification)
        {
            SendSMSRequest request = new SendSMSRequest
            {
                Sender = sender.SmsOriginator,
                Message = notification.ShortMessage,
                Phonenumbers = recipient.Receivers
            };

            MessageQueueReponse response = this.sendTextMessageUseCase.Execute(request);

            return new SendNotificationResponse.OrderNotificationResult(recipient.Method,
                recipient.Receivers,
                string.Empty,
                notification.ShortMessage,
                response.Success,
                response.ErrorMessage);
        }

        private async Task<SendNotificationResponse.OrderNotificationResult> SendEmailNotification(OrderNotificationSender sender, OrderNotificationRecipient recipient, OrderNotification notification)
        {
            SendMailRequest request = new SendMailRequest
            {
                SenderAddress = sender.EmailOriginator,
                Title = notification.Title,
                Message = notification.Message,
                IsMessageHtml = true,
                Recipients = recipient.Receivers
            };

            MessageQueueReponse response = await sendMailUseCase.ExecuteAsync(request);

            return new SendNotificationResponse.OrderNotificationResult(recipient.Method,
                recipient.Receivers,
                notification.Title,
                notification.Message,
                response.Success,
                response.ErrorMessage);
        }
    }
}