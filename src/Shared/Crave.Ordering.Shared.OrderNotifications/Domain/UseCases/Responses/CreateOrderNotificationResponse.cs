﻿using OrderNotification = Crave.Ordering.Shared.OrderNotifications.Models.OrderNotification;

namespace Crave.Ordering.Shared.OrderNotifications.Domain.UseCases.Responses
{
    internal class CreateOrderNotificationResponse
    {
        public CreateOrderNotificationResponse(OrderNotification notification)
        {
            this.Notification = notification;
        }

        public OrderNotification Notification { get; }
    }
}
