﻿namespace Crave.Ordering.Shared.OrderNotifications.Domain.UseCases.Responses
{
    internal class CreateOrderNotificationLogResponse
    {
        public CreateOrderNotificationLogResponse(int logId)
        {
            this.Success = true;
            this.OrderNotificationLogId = logId;
        }

        public CreateOrderNotificationLogResponse(string errorMessage)
        {
            this.Success = false;
            this.ErrorMessage = errorMessage;
        }

        public bool Success { get; }
        public int OrderNotificationLogId { get; }
        public string ErrorMessage { get; }
    }
}