﻿namespace Crave.Ordering.Shared.OrderNotifications.Domain.UseCases.Responses
{
    public class SendOrderNotificationResponse
    {
        public SendOrderNotificationResponse(bool success)
        {
            this.Success = success;
        }

        public SendOrderNotificationResponse(bool success, string message, params object[] args)
        {
            this.Success = success;
            this.Message = message;
            this.Args = args;
        }        

        public bool Success { get; }
        public string Message { get; } = string.Empty;
        public object[] Args { get; } = new object[0];
    }
}
