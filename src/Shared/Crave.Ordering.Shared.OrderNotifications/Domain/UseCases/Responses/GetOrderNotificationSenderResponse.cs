﻿using Crave.Ordering.Shared.OrderNotifications.Models;

namespace Crave.Ordering.Shared.OrderNotifications.Domain.UseCases.Responses
{
    internal class GetOrderNotificationSenderResponse
    {
        public GetOrderNotificationSenderResponse(bool success, OrderNotificationSender orderNotificationSender)
        {
            this.Success = success;
            this.OrderNotificationSender = orderNotificationSender;
        }

        public GetOrderNotificationSenderResponse(bool success, string errorMessage)
        {
            this.Success = success;
            this.ErrorMessage = errorMessage;
        }

        public bool Success { get; }
        public OrderNotificationSender OrderNotificationSender { get; }
        public string ErrorMessage { get; } = string.Empty;
    }
}
