﻿using System.Collections.Generic;
using Crave.Enums;

namespace Crave.Ordering.Shared.OrderNotifications.Domain.UseCases.Responses
{
    internal class SendNotificationResponse
    {
        private readonly List<OrderNotificationResult> results = new List<OrderNotificationResult>();

        public IReadOnlyList<OrderNotificationResult> OrderNotificationResults => this.results;

        public void AddResult(OrderNotificationResult result)
        {
            this.results.Add(result);
        }

        public class OrderNotificationResult
        {
            public OrderNotificationResult(OrderNotificationMethod method,
                string[] receivers,
                string notificationTitle,
                string notificationMessage,
                bool success,
                string errorMessage)
            {
                this.Method = method;
                this.Receivers = receivers;
                this.NotificationTitle = notificationTitle;
                this.NotificationMessage = notificationMessage;

                this.Success = success;
                this.ErrorMessage = errorMessage;
            }

            public OrderNotificationMethod Method { get; }
            public string[] Receivers { get; }
            public string NotificationTitle { get; }
            public string NotificationMessage { get; }

            public bool Success { get; }
            public string ErrorMessage { get; }

            public override string ToString()
            {
                // ReSharper disable once UseStringInterpolation
                return string.Format("{0}: Method={1}, Receivers={2}, Error={3}",
                    this.Success ? "Success" : "Failed",
                    this.Method.ToString(),
                    string.Join(",", this.Receivers),
                    this.ErrorMessage);
            }
        }
    }
}
