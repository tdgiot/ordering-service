﻿using System.Collections.Generic;
using Crave.Ordering.Shared.OrderNotifications.Models;

namespace Crave.Ordering.Shared.OrderNotifications.Domain.UseCases.Responses
{
    internal class GetOrderNotificationRecipientsResponse
    {
        public GetOrderNotificationRecipientsResponse(bool success, IReadOnlyCollection<OrderNotificationRecipient> recipients)
        {
            this.Success = success;
            this.Recipients = recipients;
        }

        public GetOrderNotificationRecipientsResponse(bool success, string errorMessage)
        {
            this.Success = success;
            this.ErrorMessage = errorMessage;
        }

        public bool Success { get; }
        public IReadOnlyCollection<OrderNotificationRecipient> Recipients { get; }
        public string ErrorMessage { get; } = string.Empty;
    }
}
