﻿using System.ComponentModel;
using Crave.Enums;
using Crave.Ordering.Shared.Interfaces;
using Crave.Ordering.Shared.OrderNotifications.Domain.UseCases.Requests;
using OrderNotification = Crave.Ordering.Shared.OrderNotifications.Models.OrderNotification;

namespace Crave.Ordering.Shared.OrderNotifications.Domain.UseCases
{
    /// <summary>
    /// Use-case for creating an order notification template.
    /// </summary>
    internal class CreateOrderNotificationTemplateUseCase : IUseCase<CreateOrderNotificationTemplateRequest, OrderNotification>
    {
        private readonly CreateOrderCompleteNotificationUseCase createOrderCompleteNotificationUseCase;
        private readonly CreateOrderConfirmationNotificationUseCase createOrderConfirmationNotificationUseCase;
        private readonly CreatePaymentPendingNotificationUseCase createPaymentPendingNotificationUseCase;
        private readonly CreatePaymentFailedNotificationUseCase createPaymentFailedNotificationUseCase;
        private readonly CreateOrderReceiptNotificationUseCase createOrderReceiptNotificationUseCase;
        

        /// <summary>
        /// Initializes a new instance of the <see cref="CreateOrderNotificationTemplateUseCase"/> class.
        /// </summary>
        public CreateOrderNotificationTemplateUseCase(CreateOrderCompleteNotificationUseCase createOrderCompleteNotificationUseCase,
            CreateOrderConfirmationNotificationUseCase createOrderConfirmationNotificationUseCase,
            CreatePaymentPendingNotificationUseCase createPaymentPendingNotificationUseCase,
            CreatePaymentFailedNotificationUseCase createPaymentFailedNotificationUseCase,
            CreateOrderReceiptNotificationUseCase createOrderReceiptNotificationUseCase)
        {
            this.createOrderCompleteNotificationUseCase = createOrderCompleteNotificationUseCase;
            this.createPaymentPendingNotificationUseCase = createPaymentPendingNotificationUseCase;
            this.createPaymentFailedNotificationUseCase = createPaymentFailedNotificationUseCase;
            this.createOrderReceiptNotificationUseCase = createOrderReceiptNotificationUseCase;
            this.createOrderConfirmationNotificationUseCase = createOrderConfirmationNotificationUseCase;
        }

        /// <inheritdoc />
        public OrderNotification Execute(CreateOrderNotificationTemplateRequest request) => request.OrderNotificationType switch
        {
            OrderNotificationType.OrderComplete => this.createOrderCompleteNotificationUseCase.Execute(request.Order),
            OrderNotificationType.OrderConfirmation => this.createOrderConfirmationNotificationUseCase.Execute(request.Order),
            OrderNotificationType.PaymentPending => this.createPaymentPendingNotificationUseCase.Execute(null),
            OrderNotificationType.PaymentFailed => this.createPaymentFailedNotificationUseCase.Execute(null),
            OrderNotificationType.Receipt => this.createOrderReceiptNotificationUseCase.Execute(null),
            _ => throw new InvalidEnumArgumentException(nameof(request.OrderNotificationType), (int)request.OrderNotificationType, typeof(OrderNotificationType))
        };
    }
}