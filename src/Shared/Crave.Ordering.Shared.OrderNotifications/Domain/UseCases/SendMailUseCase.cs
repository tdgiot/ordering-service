﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using Crave.Libraries.Mail;
using Crave.Ordering.Shared.OrderNotifications.Domain.UseCases.Requests;
using Crave.Ordering.Shared.OrderNotifications.Domain.UseCases.Responses;
using Crave.Ordering.Shared.OrderNotifications.Interfaces.UseCases;
using Microsoft.Extensions.Logging;

namespace Crave.Ordering.Shared.OrderNotifications.Domain.UseCases
{
    /// <summary>
    /// UseCase for sending emails
    /// </summary>
    public class SendMailUseCase : ISendMailUseCase
    {
        /// <summary>
        /// Contant email address for when no sender is defined
        /// </summary>
        internal const string EMAIL_SENDER_ADDRESS = "no-reply@trueguest.tech";

        /// <summary>
        /// Constand email sender for when no sender is defined
        /// </summary>
        internal const string EMAIL_SENDER_NAME = "Crave Interactive";

        private const string DISCLAIMER = "This email and any files transmitted with it are confidential and intended solely for the use of the individual or entity to whom they are addressed. " +
                                          "If you have received this email in error please notify the system manager. " +
                                          "This message contains confidential information and is intended only for the individual named. If you are not the named addressee you should not disseminate, distribute or copy this e-mail. " +
                                          "Please notify the sender immediately by e-mail if you have received this e-mail by mistake and delete this e-mail from your system. " +
                                          "If you are not the intended recipient you are notified that disclosing, copying, distributing or taking any action in reliance on the contents of this information is strictly prohibited.";

        private readonly ILogger<SendMailUseCase> logger;
        private readonly IMailProvider mailer;

        /// <summary>
        /// Initializes a new instance of the <see cref="SendMailUseCase" /> class.
        /// </summary>
        /// <param name="logger">Logger</param>
        /// <param name="mailer">Mail client</param>
        public SendMailUseCase(ILogger<SendMailUseCase> logger,
                               IMailProvider mailer)
        {
            this.logger = logger;
            this.mailer = mailer;
        }

        /// <inheritdoc />
        public async Task<MessageQueueReponse> ExecuteAsync(SendMailRequest request)
        {
            MessageQueueReponse response = new MessageQueueReponse();

            if (!request.Recipients.Any())
            {
                this.logger.LogWarning("SendMailUseCase | Unable to send e-mail, recipients count is 0 (zero). Request: {request}", request);

                response.Success = false;
                response.ErrorMessage = "Unable to send e-mail, recipients count is 0 (zero)";
            }
            else
            {
                string subject = request.Title;
                string message = request.Message;
                bool isMessageHtml = request.IsMessageHtml;

                // Replace template variables
                if (request.TemplateVariables?.Count > 0)
                {
                    subject = SendMailUseCase.ReplaceTemplateVariables(subject, request.TemplateVariables);
                    message = SendMailUseCase.ReplaceTemplateVariables(message, request.TemplateVariables);
                }

                // Add disclaimer to end of message
                message = $"{message}\n\r\n\r{SendMailUseCase.DISCLAIMER}";

                if (request.SenderAddress.IsNullOrWhiteSpace())
                {
                    // Set default sender e-mail when empty
                    request.SenderAddress = SendMailUseCase.EMAIL_SENDER_ADDRESS;
                    request.SenderName = SendMailUseCase.EMAIL_SENDER_NAME;
                }
                else if (request.SenderAddress.Equals(SendMailUseCase.EMAIL_SENDER_ADDRESS, StringComparison.OrdinalIgnoreCase) && request.SenderName.IsNullOrWhiteSpace())
                {
                    request.SenderName = SendMailUseCase.EMAIL_SENDER_NAME;
                }

                // Create basic message
                MailMessage mailMessage = new MailMessage().SetSender(request.SenderAddress, request.SenderName)
                                                           .AddRecipients(request.Recipients)
                                                           .AddRecipientsBcc(request.RecipientsBcc)
                                                           .SetSubject(subject)
                                                           .SetMessage(message, isMessageHtml);

                // Attachments
                foreach (KeyValuePair<string, byte[]> attachment in request.Attachments)
                {
                    if (!attachment.Value.IsNullOrEmpty())
                    {
                        mailMessage.AddAttachment(new MemoryStream(attachment.Value), attachment.Key);
                    }
                }

                this.logger.LogInformation("SendMailUseCase | Subject: {Subject}, Recipients: {Recipients}", request.Title, request.Recipients);

                MailResponse mailResponse = await this.mailer.SendAsync(mailMessage);

                response.Success = mailResponse.Success;
                response.ErrorMessage = mailResponse.ErrorMessage;

                if (!mailResponse.Success)
                {
                    this.logger.LogWarning("SendMailUseCase | Error sending email: {Message}", mailResponse.ErrorMessage);
                }
            }

            return response;
        }

        private static string ReplaceTemplateVariables(string template, Dictionary<string, string> templateVariables)
        {
            foreach (KeyValuePair<string, string> de in templateVariables)
            {
                if (de.Key.StartsWith("MEDIA:", StringComparison.InvariantCulture))
                {
                    string key = de.Key.Replace("MEDIA:", string.Empty);
                    template = template.Replace(key, de.Value, StringComparison.OrdinalIgnoreCase);
                }
                else
                {
                    template = template.Replace($"[[{de.Key}]]", de.Value, StringComparison.OrdinalIgnoreCase);
                }
            }

            return template;
        }
    }
}