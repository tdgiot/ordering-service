﻿using System;
using System.Collections.Generic;
using System.Linq;
using Crave.Enums;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.Extensions;
using Crave.Ordering.Shared.Interfaces;
using Crave.Ordering.Shared.OrderNotifications.Domain.UseCases.Requests;
using Crave.Ordering.Shared.OrderNotifications.Domain.UseCases.Responses;
using Crave.Ordering.Shared.OrderNotifications.Models;

namespace Crave.Ordering.Shared.OrderNotifications.Domain.UseCases
{
    /// <summary>
    /// Use-case for retrieving order notification recipients for an order.
    /// </summary>
    internal class GetOrderNotificationRecipientsUseCase : IUseCase<GetOrderNotificationRecipientsRequest, GetOrderNotificationRecipientsResponse>
    {
        /// <inheritdoc />
        public GetOrderNotificationRecipientsResponse Execute(GetOrderNotificationRecipientsRequest request)
        {
            OrderEntity orderEntity = request.Order;

            if (orderEntity.CheckoutMethod.IsNullOrNew())
            {
                return new GetOrderNotificationRecipientsResponse(false, "Missing checkout method on order");
            }

            if (orderEntity.ServiceMethod.IsNullOrNew())
            {
                return new GetOrderNotificationRecipientsResponse(false, "Missing service method on order");
            }

            OrderNotificationMethod orderNotificationMethod = GetOrderNotificationRecipientsUseCase.OrderNotificationMethodToOrderNotificationType(request.OrderNotificationType, orderEntity);

            List<OrderNotificationRecipient> recipientList = new List<OrderNotificationRecipient>();

            if (request.SendToOriginalReceiver)
            {
                recipientList.AddRange(GetOrderNotificationRecipientsUseCase.CreateRecipientsForCustomerIfSet(orderNotificationMethod, orderEntity));
                recipientList.AddRange(GetOrderNotificationRecipientsUseCase.CreateRecipientsForSenderIfSet(orderNotificationMethod, request.OrderNotificationType, request.Sender));
            }

            recipientList.AddRange(GetOrderNotificationRecipientsUseCase.CreateRecipientsForAdditionalEmailReceiversIfSet(orderNotificationMethod, request.AdditionalEmailReceivers));

            return new GetOrderNotificationRecipientsResponse(true, recipientList.AsReadOnly());
        }

        private static IEnumerable<OrderNotificationRecipient> CreateRecipientsForCustomerIfSet(OrderNotificationMethod orderNotificationMethod, OrderEntity orderEntity)
        {
            string[] emailAddresses = { orderEntity.Email };
            string[] phoneNumbers = { orderEntity.CustomerPhonenumber };

            return GetOrderNotificationRecipientsUseCase.CreateRecipientsForNotificationMethod(orderNotificationMethod, emailAddresses, phoneNumbers);
        }

        private static IEnumerable<OrderNotificationRecipient> CreateRecipientsForSenderIfSet(OrderNotificationMethod orderNotificationMethod, OrderNotificationType orderNotificationType, OrderNotificationSender sender)
        {
            List<OrderNotificationRecipient> recipientList = new List<OrderNotificationRecipient>();

            string[] emailAddresses = sender.EmailAddresses;
            string[] phoneNumbers = sender.PhoneNumbers;

            switch (orderNotificationType)
            {
                case OrderNotificationType.OrderConfirmation when sender.IncludeAsConfirmationRecipient:
                case OrderNotificationType.Receipt when sender.IncludeAsReceiptRecipient:
                    recipientList.AddRange(GetOrderNotificationRecipientsUseCase.CreateRecipientsForNotificationMethod(orderNotificationMethod, emailAddresses, phoneNumbers));
                    break;
            }

            return recipientList.AsReadOnly();
        }

        private static IEnumerable<OrderNotificationRecipient> CreateRecipientsForAdditionalEmailReceiversIfSet(OrderNotificationMethod orderNotificationMethod, string additionalEmailReceivers)
        {
            string[] emailAddresses = additionalEmailReceivers.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                                                              .Select(x => x.Trim())
                                                              .Where(y => !y.IsNullOrWhiteSpace())
                                                              .ToArray();
            string[] phoneNumbers = { };

            return GetOrderNotificationRecipientsUseCase.CreateRecipientsForNotificationMethod(orderNotificationMethod, emailAddresses, phoneNumbers);
        }

        private static OrderNotificationMethod OrderNotificationMethodToOrderNotificationType(OrderNotificationType orderNotificationType, OrderEntity orderEntity)
        {
            OrderNotificationMethod orderNotificationMethod;

            switch (orderNotificationType)
            {
                case OrderNotificationType.OrderConfirmation:
                    orderNotificationMethod = orderEntity.CheckoutMethod.OrderConfirmationNotificationMethod;
                    break;
                case OrderNotificationType.PaymentPending:
                    orderNotificationMethod = orderEntity.CheckoutMethod.OrderPaymentPendingNotificationMethod;
                    break;
                case OrderNotificationType.PaymentFailed:
                    orderNotificationMethod = orderEntity.CheckoutMethod.OrderPaymentFailedNotificationMethod;
                    break;
                case OrderNotificationType.Receipt:
                    orderNotificationMethod = orderEntity.CheckoutMethod.OrderReceiptNotificationMethod;
                    break;
                case OrderNotificationType.OrderComplete:
                    orderNotificationMethod = orderEntity.ServiceMethod.OrderCompleteNotificationMethod;
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(orderNotificationType), orderNotificationType, null);
            }

            return orderNotificationMethod;
        }

        private static IEnumerable<OrderNotificationRecipient> CreateRecipientsForNotificationMethod(OrderNotificationMethod orderNotificationMethod, string[] emailAddresses, string[] phoneNumbers)
        {
            List<OrderNotificationRecipient> recipientList = new List<OrderNotificationRecipient>();

            if (orderNotificationMethod.HasFlag(OrderNotificationMethod.Email))
            {
                GetOrderNotificationRecipientsUseCase.AddEmailRecipientIfSet(emailAddresses, ref recipientList);
            }

            if (orderNotificationMethod.HasFlag(OrderNotificationMethod.SMS))
            {
                GetOrderNotificationRecipientsUseCase.AddTextMessageRecipientIfSet(phoneNumbers, ref recipientList);
            }

            return recipientList.AsReadOnly();
        }

        private static void AddTextMessageRecipientIfSet(string[] phoneNumbers, ref List<OrderNotificationRecipient> recipientList)
        {
            GetOrderNotificationRecipientsUseCase.AddRecipientIfSet(OrderNotificationMethod.SMS, phoneNumbers, ref recipientList);
        }

        private static void AddEmailRecipientIfSet(string[] emailAddresses, ref List<OrderNotificationRecipient> recipientList)
        {
            GetOrderNotificationRecipientsUseCase.AddRecipientIfSet(OrderNotificationMethod.Email, emailAddresses, ref recipientList);
        }

        private static void AddRecipientIfSet(OrderNotificationMethod orderNotificationMethod, string[] recipientsAsString, ref List<OrderNotificationRecipient> recipientList)
        {
            recipientsAsString = recipientsAsString.Where(x => !x.IsNullOrWhiteSpace()).ToArray();

            if (!recipientsAsString.Any())
            {
                return;
            }

            recipientList.Add(new OrderNotificationRecipient(orderNotificationMethod, recipientsAsString));
        }
    }
}