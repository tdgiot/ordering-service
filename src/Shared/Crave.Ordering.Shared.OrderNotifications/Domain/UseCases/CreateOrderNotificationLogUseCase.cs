﻿using System;
using System.Threading.Tasks;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Interfaces;
using Crave.Ordering.Shared.OrderNotifications.Domain.UseCases.Requests;
using Crave.Ordering.Shared.OrderNotifications.Domain.UseCases.Responses;
using Crave.Ordering.Shared.OrderNotifications.Interfaces.Repositories;

namespace Crave.Ordering.Shared.OrderNotifications.Domain.UseCases
{
    internal class CreateOrderNotificationLogUseCase : IUseCaseAsync<CreateOrderNotificationLog, CreateOrderNotificationLogResponse>
    {
        private readonly IOrderNotificationLogRepository orderNotificationLogRepository;

        public CreateOrderNotificationLogUseCase(IOrderNotificationLogRepository orderNotificationLogRepository)
        {
            this.orderNotificationLogRepository = orderNotificationLogRepository;
        }

        public async Task<CreateOrderNotificationLogResponse> ExecuteAsync(CreateOrderNotificationLog request)
        {
            // Convert request to entity
            OrderNotificationLogEntity entity = new OrderNotificationLogEntity
            {
                OrderId = request.OrderId,
                ParentCompanyId = request.CompanyId,
                Type = request.Type,
                Method = request.Method,
                Status = request.Status,
                Receiver = string.Join(", ", request.Receivers),
                Subject = request.Subject,
                Message = request.Message,
                Log = request.Log,
                SentUTC = request.SentUtc,
                CreatedUTC = DateTime.UtcNow
            };

            try
            {
                OrderNotificationLogEntity savedEntity = await this.orderNotificationLogRepository.SaveEntityAsync(entity);

                return new CreateOrderNotificationLogResponse(savedEntity.OrderNotificationLogId);
            }
            catch (Exception ex)
            {
                return new CreateOrderNotificationLogResponse("Failed to create OrderNotificationLogEntity: " + ex.Message);
            }
        }
    }
}