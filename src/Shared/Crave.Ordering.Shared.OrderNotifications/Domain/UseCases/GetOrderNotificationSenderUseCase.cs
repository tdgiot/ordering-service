﻿using System;
using System.Collections.Generic;
using System.Linq;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.Extensions;
using Crave.Ordering.Shared.Interfaces;
using Crave.Ordering.Shared.OrderNotifications.Domain.UseCases.Requests;
using Crave.Ordering.Shared.OrderNotifications.Domain.UseCases.Responses;
using Crave.Ordering.Shared.OrderNotifications.Models;

namespace Crave.Ordering.Shared.OrderNotifications.Domain.UseCases
{
    /// <summary>
    /// Use-case for retrieving order notification sender for an order.
    /// </summary>
    internal class GetOrderNotificationSenderUseCase : IUseCase<GetOrderNotificationSenderRequest, GetOrderNotificationSenderResponse>
    {
        private const string DEFAULT_EMAIL_ORIGINATOR = "no-reply@trueguest.tech";
        private const string DEFAULT_SMS_ORIGINATOR = "Crave";

        /// <inheritdoc />
        public GetOrderNotificationSenderResponse Execute(GetOrderNotificationSenderRequest request)
        {
            OrderEntity orderEntity = request.Order;

            if (orderEntity.Outlet.IsNullOrNew())
            {
                return new GetOrderNotificationSenderResponse(false, "Missing outlet on order");
            }

            if (orderEntity.Outlet.OutletSellerInformation.IsNullOrNew())
            {
                return new GetOrderNotificationSenderResponse(false, $"Missing outlet seller information on outlet. OutletId=({orderEntity.Outlet.OutletId})");
            }

            OutletSellerInformationEntity outletSellerInformationEntity = orderEntity.Outlet.OutletSellerInformation;

            string emailOriginator = DEFAULT_EMAIL_ORIGINATOR;
            string smsOriginator = outletSellerInformationEntity.SmsOriginator ?? DEFAULT_SMS_ORIGINATOR;
            string[] phoneNumbers = SplitOnCommaAndRemoveWhiteSpaces(outletSellerInformationEntity.ReceiptReceiversSms);
            bool includeAsReceiptRecipient = outletSellerInformationEntity.ReceiveOrderReceiptNotifications;
            bool includeAsConfirmationRecipient = outletSellerInformationEntity.ReceiveOrderConfirmationNotifications;

            // DK 2020-08-13 - Temp hack for PPHE [APPLESS-1666]
            if (outletSellerInformationEntity.Email.EndsWith("@pphe.com"))
            {
                emailOriginator = "noreply@pphe.com";
            }

            OrderNotificationSender sender = new OrderNotificationSender(emailOriginator,
                                                                         smsOriginator,
                                                                         GetEmailAddresses(outletSellerInformationEntity),
                                                                         phoneNumbers,
                                                                         includeAsReceiptRecipient,
                                                                         includeAsConfirmationRecipient);

            return new GetOrderNotificationSenderResponse(true, sender);
        }

        private static string[] GetEmailAddresses(OutletSellerInformationEntity outletSellerInformationEntity)
        {
            List<string> emailAddresses = new List<string>();
            emailAddresses.AddRange(SplitOnCommaAndRemoveWhiteSpaces(outletSellerInformationEntity.ReceiptReceiversEmail).ToList());

            if (outletSellerInformationEntity.ReceiveOrderConfirmationNotifications && !emailAddresses.Contains(outletSellerInformationEntity.Email))
            { emailAddresses.Add(outletSellerInformationEntity.Email); }

            return emailAddresses.ToArray();
        }

        private static string[] SplitOnCommaAndRemoveWhiteSpaces(string receivers)
        {
            return receivers.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                              .Select(x => x.Trim())
                              .Where(y => !y.IsNullOrWhiteSpace())
                              .ToArray();
        }
    }
}