﻿using Crave.Enums;

namespace Crave.Ordering.Shared.OrderNotifications.Domain.UseCases.Requests
{
    public class SendOrderNotificationRequest
    {
        public SendOrderNotificationRequest(int orderId, OrderNotificationType orderNotificationType)
        {
            this.OrderId = orderId;
            this.OrderNotificationType = orderNotificationType;
        }

        public int OrderId { get; }
        public OrderNotificationType OrderNotificationType { get; }

        /// <summary>
        /// Gets if the order notification should be sent to the original receiver.
        /// </summary>
        public bool SendToOriginalReceiver { get; set; } = true;

        /// <summary>
        /// Gets the additional email notification receivers.
        /// </summary>
        public string AdditionalEmailReceivers { get; set; } = string.Empty;
    }
}
