﻿using Crave.Enums;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.OrderNotifications.Models;

namespace Crave.Ordering.Shared.OrderNotifications.Domain.UseCases.Requests
{
    /// <summary>
    /// Request class which is used to retrieve the order notification recipients.
    /// </summary>
    internal class GetOrderNotificationRecipientsRequest
    {
        public GetOrderNotificationRecipientsRequest(OrderEntity order,
                                                     OrderNotificationType orderNotificationType,
                                                     OrderNotificationSender sender,
                                                     bool sendToOriginalReceiver,
                                                     string additionalEmailReceivers)
        {
            this.Order = order;
            this.OrderNotificationType = orderNotificationType;
            this.Sender = sender;
            this.SendToOriginalReceiver = sendToOriginalReceiver;
            this.AdditionalEmailReceivers = additionalEmailReceivers;
        }

        /// <summary>
        /// Gets the order.
        /// </summary>
        public OrderEntity Order { get; }

        /// <summary>
        /// Gets the order notification type.
        /// </summary>
        public OrderNotificationType OrderNotificationType { get; }

        /// <summary>
        /// Gets the order notification sender.
        /// </summary>
        public OrderNotificationSender Sender { get; }

        /// <summary>
        /// Gets if the order notification should be sent to the original receiver.
        /// </summary>
        public bool SendToOriginalReceiver { get; set; }

        /// <summary>
        /// Gets the additional email notification receivers.
        /// </summary>
        public string AdditionalEmailReceivers { get; set; }
    }
}