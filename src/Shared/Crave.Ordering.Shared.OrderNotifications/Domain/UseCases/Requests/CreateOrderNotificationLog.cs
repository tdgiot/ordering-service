﻿using System;
using Crave.Enums;

namespace Crave.Ordering.Shared.OrderNotifications.Domain.UseCases.Requests
{
    internal class CreateOrderNotificationLog
    {
        public CreateOrderNotificationLog(int orderId,
                                          int companyId,
                                          OrderNotificationType type,
                                          OrderNotificationMethod method,
                                          OrderNotificationLogStatus status,
                                          string[] receivers,
                                          string subject,
                                          string message,
                                          string log)
        {
            this.OrderId = orderId;
            this.CompanyId = companyId;
            this.Type = type;
            this.Method = method;
            this.Status = status;
            this.Receivers = receivers;
            this.Subject = subject;
            this.Message = message;
            this.Log = log;

            if (status == OrderNotificationLogStatus.Success || status == OrderNotificationLogStatus.SuccessWithErrors)
            {
                this.SentUtc = DateTime.UtcNow;
            }
        }

        public int OrderId { get; }
        public int CompanyId { get; }
        public OrderNotificationType Type { get; }
        public OrderNotificationMethod Method { get; }
        public OrderNotificationLogStatus Status { get; }
        public string[] Receivers { get; }
        public string Subject { get; }
        public string Message { get; }
        public string Log { get; }
        public DateTime? SentUtc { get; } = null;
    }
}