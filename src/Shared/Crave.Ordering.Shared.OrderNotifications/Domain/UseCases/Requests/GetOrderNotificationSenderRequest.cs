﻿using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Ordering.Shared.OrderNotifications.Domain.UseCases.Requests
{
    /// <summary>
    /// Request class which is used to retrieve the order notification sender.
    /// </summary>
    internal class GetOrderNotificationSenderRequest
    {
        public GetOrderNotificationSenderRequest(OrderEntity order)
        {
            this.Order = order;
        }

        /// <summary>
        /// Gets the order.
        /// </summary>
        public OrderEntity Order { get; }
    }
}