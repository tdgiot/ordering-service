﻿using System.Collections.Generic;
using Crave.Ordering.Shared.OrderNotifications.Models;
using OrderNotification = Crave.Ordering.Shared.OrderNotifications.Models.OrderNotification;

namespace Crave.Ordering.Shared.OrderNotifications.Domain.UseCases.Requests
{
    /// <summary>
    /// Request class which is used to send an order notification.
    /// </summary>
    internal class SendNotificationRequest
    {
        public SendNotificationRequest(OrderNotificationSender sender,
                                       IReadOnlyCollection<OrderNotificationRecipient> recipients,
                                       OrderNotification notification)
        {
            this.Sender = sender;
            this.Recipients = recipients;
            this.Notification = notification;
        }

        /// <summary>
        /// Gets the order notification sender.
        /// </summary>
        public OrderNotificationSender Sender { get; }

        /// <summary>
        /// Gets the order notification recipients.
        /// </summary>
        public IReadOnlyCollection<OrderNotificationRecipient> Recipients { get; }

        /// <summary>
        /// Gets the order notification.
        /// </summary>
        public OrderNotification Notification { get; }
    }
}