﻿using System.Collections.Generic;

namespace Crave.Ordering.Shared.OrderNotifications.Domain.UseCases.Requests
{
    /// <summary>
    /// Model for sending a send mail request on the message bus
    /// </summary>
    public class SendMailRequest
    {
        /// <summary>
        /// Gets or sets the email address of the sender
        /// </summary>
        public string SenderAddress { get; set; }

        /// <summary>
        /// Gets or sets the name of the sender. Email will be used when left empty
        /// </summary>
        public string SenderName { get; set; }

        /// <summary>
        /// Gets or sets the list of recipients
        /// </summary>
        public IEnumerable<string> Recipients { get; set; } = new List<string>();

        /// <summary>
        /// Gets or sets the list of recipients bcc
        /// </summary>
        public IEnumerable<string> RecipientsBcc { get; set; } = new List<string>();

        /// <summary>
        /// Gets or sets the email title
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets the email body
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Gets or sets wether the message is in html format
        /// </summary>
        public bool IsMessageHtml { get; set; }

        /// <summary>
        /// Gets or sets template variables which will be replaced in the message body
        /// </summary>
        public Dictionary<string, string> TemplateVariables { get; set; } = new Dictionary<string, string>();

        /// <summary>
        /// Gets or sets mail attachments
        /// </summary>
        public Dictionary<string, byte[]> Attachments { get; set; } = new Dictionary<string, byte[]>();
    }
}