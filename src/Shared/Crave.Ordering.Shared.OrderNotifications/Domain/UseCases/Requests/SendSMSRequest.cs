﻿using System.Collections.Generic;

namespace Crave.Ordering.Shared.OrderNotifications.Domain.UseCases.Requests
{
    /// <summary>
    /// Model for sending a sms request on the message bus
    /// </summary>
    public class SendSMSRequest
    {
        /// <summary>
        /// Gets or sets the name of the sender. This can be a telephone number (including country code) or an alphanumeric string. In case of an alphanumeric string, the maximum length is 11 characters
        /// </summary>
        public string Sender { get; set; }

        /// <summary>
        /// Gets or set the list of phonenumbers to send the message to
        /// </summary>
        public IEnumerable<string> Phonenumbers { get; set; } = new List<string>();

        /// <summary>
        /// Gets or sets the message. The maximum length of text message that you can send is 918 characters. 
        /// However, if you send more than 160 characters then your message will be broken down in to chunks of 153 characters before being sent to the recipient's handset.
        /// </summary>
        public string Message { get; set; }
    }
}