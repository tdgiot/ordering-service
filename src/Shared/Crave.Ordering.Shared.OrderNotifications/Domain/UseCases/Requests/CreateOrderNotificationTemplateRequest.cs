﻿using Crave.Enums;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Ordering.Shared.OrderNotifications.Domain.UseCases.Requests
{
    /// <summary>
    /// Request class which is used to create an order notification template.
    /// </summary>
    internal class CreateOrderNotificationTemplateRequest
    {
        public CreateOrderNotificationTemplateRequest(OrderEntity order,
                                                      OrderNotificationType orderNotificationType)
        {
            this.Order = order;
            this.OrderNotificationType = orderNotificationType;
        }

        /// <summary>
        /// Gets the order.
        /// </summary>
        public OrderEntity Order { get; }

        /// <summary>
        /// Gets the order notification type.
        /// </summary>
        public OrderNotificationType OrderNotificationType { get; }
    }
}