﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using Crave.Enums;
using Crave.Globalization;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Extensions;
using Crave.Ordering.Shared.OrderNotifications.Extensions;
using Crave.Ordering.Shared.OrderNotifications.Interfaces.Builders;
using Crave.Ordering.Shared.OrderNotifications.Models;

namespace Crave.Ordering.Shared.OrderNotifications.Builders
{
    public class OrderNotificationBuilder : IOrderNotificationBuilder
    {
        private const string COMPANYNAME = "COMPANYNAME";
        private const string SELLER_NAME = "SELLERNAME";
        private const string SELLER_ADDRESS = "SELLERADDRESS";
        private const string SELLER_CONTACT = "SELLERCONTACT";
        private const string SELLER_CONTACT_EMAIL = "SELLERCONTACTEMAIL";
        private const string RECEIPT_CREATED = "RECEIPTCREATED";
        private const string RECEIPT_NUMBER = "RECEIPTNUMBER";
        private const string SERVICE_METHOD = "SERVICEMETHOD";
        private const string CHECKOUT_METHOD = "CHECKOUTMETHOD";
        private const string ORDER = "ORDER";
        private const string ORDERTOTAL = "ORDERTOTAL";
        private const string ORDER_NUMBER = "ORDERNUMBER";
        private const string ORDER_CREATED = "CREATED";
        private const string ORDER_STATUSTEXT = "STATUSTEXT";
        private const string ORDER_PRICE = "PRICE";
        private const string ORDER_NOTES = "NOTES";
        private const string TAXBREAKDOWN = "TAXBREAKDOWN";
        private const string PAYMENT_METHOD = "PAYMENTMETHOD";
        private const string PAYMENT_DETAILS = "PAYMENTDETAILS";
        private const string PAYMENT_STATUS = "PAYMENTSTATUS";
        private const string CUSTOMER_EMAIL = "CUSTOMEREMAIL";
        private const string CUSTOMER_PHONE = "CUSTOMERPHONE";
        private const string CUSTOMER_LASTNAME = "CUSTOMERLASTNAME";
        private const string DELIVERY_INFORMATION = "DELIVERYINFORMATION";
        private const string APPROVAL_CODE = "APPROVALCODE";
        private const string PAYMENT_PROCESSING_INFO = "PAYMENTPROCESSINGINFO";
        private const string DELIVERYPOINT = "DELIVERYPOINT";
        private const string DELIVERYPOINTNUMBER = "DELIVERYPOINTNUMBER";
        private const string DELIVERYPOINTNAME = "DELIVERYPOINTNAME";
        private const string DELIVERYPOINTCAPTION = "DELIVERYPOINTCAPTION";
        private const string TAX_NUMBER_TITLE = "TAXNUMBERTITLE";
        private const string TAX_NUMBER = "TAXNUMBER";
        private const string TAX_DISCLAIMER = "TAXDISCLAIMER";
        private const string NUMBER_OF_COVERS_TITLE = "NUMBEROFCOVERSTITLE";
        private const string NUMBER_OF_COVERS = "NUMBEROFCOVERS";

        private const string DEFAULT_PAYMENT_PROCESSING_INFO = "Crave Interactive B.V. trading as TakeawayToday<br>Naaldwijk, Netherlands<br>ttsupport@crave-emenu.com<br>";

        private const string DefaultTaxDisclaimer = "This is not a VAT invoice. To request a VAT invoice, please contact the venue.";
        private const string DefaultTaxNumberTitle = "VAT Number";

        private const string END_TABLE_ROW = "</tr>";

        public OrderNotificationBuilder() : this(new OrderEntity(),
                                                 new OrderNotification(string.Empty, string.Empty, string.Empty),
                                                 true,
                                                 new Dictionary<string, string>())
        {
        }

        private OrderNotificationBuilder(OrderEntity order,
                                         OrderNotification template,
                                         bool appendAdditionalTemplateVarsIfUnique,
                                         IDictionary<string, string> additionalTemplateVars)
        {
            Order = order;
            Template = template;
            AppendAdditionalTemplateVarsIfUnique = appendAdditionalTemplateVarsIfUnique;
            AdditionalTemplateVars = additionalTemplateVars;
            Variables = new Dictionary<string, string>();
        }

        private OrderEntity Order { get; }
        private OrderNotification Template { get; }
        private bool AppendAdditionalTemplateVarsIfUnique { get; }
        private IDictionary<string, string> AdditionalTemplateVars { get; }
        private IDictionary<string, string> Variables { get; }

        public OrderNotificationBuilder WithOrder(OrderEntity order) => new OrderNotificationBuilder(order, Template, AppendAdditionalTemplateVarsIfUnique, AdditionalTemplateVars);
        public OrderNotificationBuilder WithTemplate(OrderNotification template) => new OrderNotificationBuilder(Order, template, AppendAdditionalTemplateVarsIfUnique, AdditionalTemplateVars);
        public OrderNotificationBuilder WithAppendAdditionalTemplateVarsIfUnique(bool appendAdditionalTemplateVarsIfUnique) => new OrderNotificationBuilder(Order, Template, appendAdditionalTemplateVarsIfUnique, AdditionalTemplateVars);
        public OrderNotificationBuilder WithAdditionalTemplateVars(IDictionary<string, string> additionalTemplateVars) => new OrderNotificationBuilder(Order, Template, AppendAdditionalTemplateVarsIfUnique, additionalTemplateVars);

        public OrderNotification Build()
        {
            AppendAdditionalTemplateVars();

            string title = ReplaceTemplateVariables(Template.Title, Variables);
            string message = ReplaceTemplateVariables(Template.Message, Variables);
            string shortMessage = ReplaceTemplateVariables(Template.ShortMessage, Variables);

            return new OrderNotification(title, message, shortMessage);
        }

        private void AppendAdditionalTemplateVars()
        {
            // Append any additional template variables
            foreach ((string key, string value) in AdditionalTemplateVars)
            {
                if (AppendAdditionalTemplateVarsIfUnique && !Variables.ContainsKey(key)) {
                    // Append variable if not already exists
                    Variables.Add(key, value);
                }
                else if (!AppendAdditionalTemplateVarsIfUnique)
                {
                    // Add variable to list no matter what
                    Variables[key] = value;
                }
            }
        }

        private static string ReplaceTemplateVariables(string template, IDictionary<string, string> templateVariables)
        {
            if (template.IsNullOrWhiteSpace() || !template.Contains("[[", StringComparison.InvariantCulture) || !template.Contains("]]", StringComparison.InvariantCulture))
            {
                return template;
            }

            foreach (KeyValuePair<string, string> variable in templateVariables)
            {
                if (variable.Key.StartsWith("MEDIA:", StringComparison.InvariantCulture))
                {
                    string key = variable.Key.Replace("MEDIA:", string.Empty, StringComparison.InvariantCulture);
                    template = template.Replace(key, variable.Value, StringComparison.OrdinalIgnoreCase);
                }
                else
                {
                    template = template.Replace($"[[{variable.Key}]]", variable.Value, StringComparison.OrdinalIgnoreCase);
                }
            }

            return template;
        }

        public IOrderNotificationBuilder AddCompanyInfo()
        {
            Variables.Add(COMPANYNAME, Order.CompanyName);
            return this;
        }

        public IOrderNotificationBuilder AddSellerInfo()
        {
            OutletSellerInformationEntity outletSeller = Order.OutletId.HasValue ? Order.Outlet.OutletSellerInformation : null;

            Variables.Add(SELLER_NAME, outletSeller?.SellerName ?? string.Empty);
            Variables.Add(SELLER_ADDRESS, outletSeller?.Address ?? string.Empty);
            Variables.Add(SELLER_CONTACT, outletSeller?.Phonenumber ?? string.Empty);
            Variables.Add(SELLER_CONTACT_EMAIL, outletSeller?.Email ?? string.Empty);
            return this;
        }

        public IOrderNotificationBuilder AddTaxInfo()
        {
            OutletSellerInformationEntity outletSeller = Order.OutletId.HasValue ? Order.Outlet.OutletSellerInformation : null;
            AddTaxNumberInfo(outletSeller);
            AddTaxDisclaimerInfo(outletSeller);
            return this;
        }

        private void AddTaxNumberInfo(OutletSellerInformationEntity outletSeller)
        {
            if (!string.IsNullOrWhiteSpace(outletSeller?.VatNumber))
            {
                Variables.Add(TAX_NUMBER_TITLE, string.IsNullOrWhiteSpace(Order.Outlet.TaxNumberTitle) 
                    ? DefaultTaxNumberTitle 
                    : Order.Outlet.TaxNumberTitle);
                Variables.Add(TAX_NUMBER, ": " + outletSeller?.VatNumber);
                return;
            }

            Variables.Add(TAX_NUMBER_TITLE, string.Empty);        
            Variables.Add(TAX_NUMBER, string.Empty);
        }

        private void AddTaxDisclaimerInfo(OutletSellerInformationEntity outletSeller)
        {
            if (outletSeller == null || Order.Outlet.ShowTaxBreakdown == TaxBreakdown.None || string.IsNullOrWhiteSpace(outletSeller.VatNumber))
            {
                Variables.Add(TAX_DISCLAIMER, string.IsNullOrWhiteSpace(Order.Outlet?.TaxDisclaimer) 
                    ? DefaultTaxDisclaimer 
                    : Order.Outlet.TaxDisclaimer);
                return;
            }
            
            Variables.Add(TAX_DISCLAIMER, string.Empty);
        }

        public IOrderNotificationBuilder AddReceiptInfo()
        {
            ReceiptEntity receipt = Order.ReceiptCollection.LastOrDefault();
            if (receipt == null)
            {
                Variables.Add(RECEIPT_CREATED, string.Empty);
                Variables.Add(RECEIPT_NUMBER, string.Empty);
                return this;
            }

            Variables.Add(RECEIPT_CREATED, receipt.CreatedUTC.GetValueOrDefault(DateTime.UtcNow).ToLocalTime(Order.TimeZoneOlsonId).FormatDateTime(Order.Company.ClockMode));
            Variables.Add(RECEIPT_NUMBER, receipt.Number.ToString(CultureInfo.InvariantCulture));
            return this;
        }

        public IOrderNotificationBuilder AddServiceMethodInfo()
        {
            Variables.Add(SERVICE_METHOD, Order.ServiceMethodName);
            return this;
        }

        public IOrderNotificationBuilder AddCheckoutMethodInfo()
        {
            Variables.Add(CHECKOUT_METHOD, Order.CheckoutMethodName);
            return this;
        }

        public IOrderNotificationBuilder AddPaymentInfo()
        {
            PaymentTransactionEntity paymentTransactionEntity = Order.PaymentTransactionCollection.FirstOrDefault(x => x.Status == PaymentTransactionStatus.Paid);
            if (paymentTransactionEntity == null)
            {
                Variables.Add(PAYMENT_DETAILS, string.Empty);
                Variables.Add(APPROVAL_CODE, string.Empty);
                Variables.Add(PAYMENT_STATUS, "Not Paid");
                return this;
            }

            Variables.Add(PAYMENT_METHOD, paymentTransactionEntity.PaymentMethod);
            Variables.Add(PAYMENT_DETAILS, paymentTransactionEntity.CardSummary.IsNullOrWhiteSpace() ? string.Empty : $"************{paymentTransactionEntity.CardSummary}");
            Variables.Add(APPROVAL_CODE, paymentTransactionEntity.AuthCode);
            Variables.Add(PAYMENT_STATUS, paymentTransactionEntity.Status.ToString());
            return this;
        }

        public IOrderNotificationBuilder AddOrderInfo()
        {
            DateTime orderCreatedLocalTime = Order.CreatedUTC.GetValueOrDefault(DateTime.UtcNow).ToLocalTime(Order.TimeZoneOlsonId);

            Variables.Add(ORDER_NUMBER, Order.OrderId.ToString(CultureInfo.InvariantCulture));
            Variables.Add(ORDER, GetOrderHtml(Order, Template.ShowOrderPrices));
            Variables.Add(ORDER_CREATED, Order.Company != null ? orderCreatedLocalTime.FormatDateTime(Order.Company.ClockMode) : orderCreatedLocalTime.FormatDateTime(ClockMode.AmPm));
            Variables.Add(ORDER_STATUSTEXT, Order.StatusText);
            Variables.Add(ORDER_PRICE, !string.IsNullOrWhiteSpace(Order.CultureCode) ? Order.Total.ToString("0,0.00", ((Culture)Order.CultureCode).CultureInfo) : Order.Total.ToString("0.00", CultureInfo.InvariantCulture));
            Variables.Add(ORDER_NOTES, Order.Notes.ReplaceLineBreaks("<br/>"));

            return this;
        }

        public IOrderNotificationBuilder AddCoversCountInfo()
        {
            if (Order.CoversCount != null)
            {
                Variables.Add(NUMBER_OF_COVERS_TITLE, "Number of Covers:");
                Variables.Add(NUMBER_OF_COVERS, Order.CoversCount.ToString());
            }
            else
            {
                Variables.Add(NUMBER_OF_COVERS_TITLE, string.Empty);
                Variables.Add(NUMBER_OF_COVERS, string.Empty);
            }

            return this;
        }

        public IOrderNotificationBuilder AddOrderTotalInfo()
        {
            Variables.Add(ORDERTOTAL, GetOrderTotalHtml(Order, Template.ShowOrderPrices));
            return this;
        }

        public IOrderNotificationBuilder AddTaxBreakdownInfo()
        {
            Variables.Add(TAXBREAKDOWN, !Template.ShowOrderPrices || !Order.TaxBreakdown.IsIn(TaxBreakdown.Receipt, TaxBreakdown.ReceiptAndCheckout) 
                ? string.Empty 
                : GetTaxBreakdownHtml(Order));
            return this;
        }

        public IOrderNotificationBuilder AddCustomerInfo()
        {
            Variables.Add(CUSTOMER_EMAIL, Order.Email);
            Variables.Add(CUSTOMER_PHONE, Order.CustomerPhonenumber);
            Variables.Add(CUSTOMER_LASTNAME, Order.CustomerLastname);
            return this;
        }

        public IOrderNotificationBuilder AddDeliveryInformation()
        {
            Variables.Add(DELIVERY_INFORMATION, !Order.DeliveryInformationId.HasValue ? string.Empty : GetDeliveryInformationHtml(Order.DeliveryInformation));
            return this;
        }

        public IOrderNotificationBuilder AddPaymentProcessingInfo()
        {
            ReceiptEntity receipt = Order.ReceiptCollection.LastOrDefault();
            if (receipt == null)
            {
                Variables.Add(PAYMENT_PROCESSING_INFO, string.Empty);
                return this;
            }

            string processingInfo = receipt.PaymentProcessorInfo;
            if (processingInfo.IsNullOrWhiteSpace())
            {
                processingInfo = OrderNotificationBuilder.DEFAULT_PAYMENT_PROCESSING_INFO;
            }

            processingInfo = processingInfo.ReplaceLineBreaks("<br>");

            Variables.Add(PAYMENT_PROCESSING_INFO, processingInfo);
            return this;
        }

        public IOrderNotificationBuilder AddDeliverypointInfo()
        {
            Variables.Add(DELIVERYPOINT, Order.DeliverypointNumber);
            Variables.Add(DELIVERYPOINTNUMBER, Order.DeliverypointNumber);
            Variables.Add(DELIVERYPOINTNAME, Order.DeliverypointName);
            Variables.Add(DELIVERYPOINTCAPTION, 
                Order.DeliverypointId.HasValue && Order.Deliverypoint?.Deliverypointgroup != null 
                ? Order.Deliverypoint.Deliverypointgroup.DeliverypointCaption 
                : "Deliverypoint");

            return this;
        }

        private static string GetOrderHtml(OrderEntity order, bool showPrices)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<table class='order' border='0' cellpadding='0'>");

            foreach (OrderitemEntity orderitem in order.OrderitemCollection.OrderBy(x => x.OrderitemId))
            {
                sb.Append("<tr class='orderitem'>");
                sb.Append($"<td width='380' class='name'>{orderitem.ProductName}</td>");
                if (showPrices)
                {
                    sb.Append($"<td width='80' class='price'>{orderitem.Quantity} x {orderitem.DisplayPriceFormatted}</td>");
                    sb.Append($"<td width='80' class='price'>{orderitem.TotalDisplayPriceExcludingAlterationitemsFormatted}</td>");
                }
                else
                {
                    sb.Append($"<td width='80' class='price'>{orderitem.Quantity}</td>");
                }


                sb.Append(END_TABLE_ROW);

                foreach (OrderitemAlterationitemEntity orderitemalterationitem in orderitem.OrderitemAlterationitemCollection
                    .Where(x => x.Alteration == null || !x.Alteration.OrderLevelEnabled || (x.Alteration.OrderLevelEnabled && x.TotalDisplayPrice > 0))
                    .OrderBy(x => x.OrderitemAlterationitemId))
                {
                    sb.Append("<tr class='alterationitem'>");
                    sb.Append($"<td width='380' class='name'>{GetOrderItemAlterationItemDesc(orderitemalterationitem, order)}</td>");

                    if (orderitemalterationitem.AlterationoptionPriceIn > 0)
                    { sb.Append(GetAlterationOptionPriceInDesc(orderitemalterationitem, showPrices)); }

                    sb.Append(END_TABLE_ROW);
                }
            }

            sb.Append("</table>");
            return sb.ToString();
        }

        private static string GetAlterationOptionPriceInDesc(OrderitemAlterationitemEntity orderitemalterationitem, bool showPrices)
        {
            StringBuilder sb = new StringBuilder();

            if (showPrices)
            {
                sb.Append($"<td width='80' class='price'>{orderitemalterationitem.Quantity} x {orderitemalterationitem.DisplayPriceFormatted}</td>");
                sb.Append($"<td width='80' class='price'>{orderitemalterationitem.TotalDisplayPriceFormatted}</td>");
            }
            else
            { sb.Append($"<td width='80' class='price'>{orderitemalterationitem.Quantity}</td>"); }

            return sb.ToString();
        }

        private static string GetOrderItemAlterationItemDesc(OrderitemAlterationitemEntity orderitemalterationitem, OrderEntity order)
        {
            StringBuilder description = new StringBuilder();
            if (!orderitemalterationitem.AlterationName.IsNullOrWhiteSpace())
            {
                description.Append(orderitemalterationitem.AlterationName);
            }

            if (!orderitemalterationitem.AlterationoptionName.IsNullOrWhiteSpace())
            {
                description.Append($": {orderitemalterationitem.AlterationoptionName}");
            }

            if (!orderitemalterationitem.Value.IsNullOrWhiteSpace())
            {
                description.Append($": {orderitemalterationitem.Value}");
            }

            if (orderitemalterationitem.TimeLocal.HasValue)
            {
                if (orderitemalterationitem.AlterationType == AlterationType.Date)
                {
                    description.Append($": {orderitemalterationitem.TimeLocal.Value.FormatDate(order.Company.ClockMode)}");
                }
                else if (orderitemalterationitem.AlterationType == AlterationType.DateTime)
                {
                    description.Append($": {orderitemalterationitem.TimeLocal.Value.FormatDateTime(order.Company.ClockMode)}");
                }
            }

            return description.ToString();
        }

        public static string GetOrderTotalHtml(OrderEntity order, bool showOrderPrices)
        {
            if (!showOrderPrices)
            {
                return string.Empty;
            }

            StringBuilder sb = new StringBuilder();
            sb.Append("<table style='margin-top: 16px;'>");
            sb.Append("<tbody>");

            if (!order.PricesIncludeTaxes && order.OutletId.HasValue)
            {
                sb.Append("<tr>")
                    .Append("<td width='380'>Subtotal</td>")
                    .Append("<td width='80'></td>")
                    .Append($"<td width='80' style='text-align: right;'>{order.SubTotalFormatted}</td>")
                    .Append(END_TABLE_ROW);

                foreach (TaxTariffRecord taxTariffRecord in order.GetTaxTariffRecords(true))
                {
                    sb.Append("<tr>");

                    sb.Append(!taxTariffRecord.Name.IsNullOrWhiteSpace() 
                        ? $"<td width='380'>{taxTariffRecord.Name} ({taxTariffRecord.Percentage}%)</td>" 
                        : $"<td width='380'>{taxTariffRecord.Percentage}%</td>");

                    sb.Append("<td width='80'></td>")
                        .Append($"<td width='80' style='text-align: right;'>{taxTariffRecord.VatPrice.Format(order.CultureCode)}</td>")
                        .Append(END_TABLE_ROW);
                }
            }

            sb.Append("</tbody>");

            sb.Append("<tfoot>")
                .Append("<tr>")
                .Append("<th width='380' style='text-align: left;'>Total</th>")
                .Append("<th width='80'></th>")
                .Append($"<th width='80' style='text-align: right;'>{order.TotalFormatted}</th>")
                .Append(END_TABLE_ROW)
                .Append("</tfoot>");

            sb.Append("</table>");

            return sb.ToString();
        }

        private static string GetTaxBreakdownHtml(OrderEntity order)
        {
            StringBuilder sb = new StringBuilder();

            sb.Append("<table style='margin-top: 10px;'>");
            sb.Append("<thead>");
            sb.Append("<tr class='header'>");
            sb.Append("<th width='380' style='text-align: left;' class='label'>Rate</th>");
            sb.Append("<th width='80' style='text-align: right;' class='value'>Net</th>");
            sb.Append("<th width='80' style='text-align: right;' class='value'>VAT</th>");
            sb.Append(END_TABLE_ROW);
            sb.Append("</thead>");

            sb.Append("<tbody>");

            foreach (TaxTariffRecord taxTariffRecord in order.GetTaxTariffRecords())
            {
                sb.Append("<tr class='vat'>");

                sb.Append(!taxTariffRecord.Name.IsNullOrWhiteSpace() 
                    ? $"<td width='380' style='text-align: left;' class='label'>{taxTariffRecord.Name} ({taxTariffRecord.Percentage}%)</td>" 
                    : $"<td width='380' style='text-align: left;' class='label'>{taxTariffRecord.Percentage}%</td>");

                sb.Append($"<td width='80' style='text-align: right;' class='value'>{taxTariffRecord.NetPrice.Format(order.CultureCode)}</td>");
                sb.Append($"<td width='80' style='text-align: right;' class='value'>{taxTariffRecord.VatPrice.Format(order.CultureCode)}</td>");
                sb.Append(END_TABLE_ROW);
            }

            return sb.ToString();
        }

        private static string GetDeliveryInformationHtml(DeliveryInformationEntity deliveryInformation)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<tr>");
            sb.Append("<td class='label'>Flat number/building name:</td>");
            sb.Append($"<td class='value'>{deliveryInformation.BuildingName}</td>");
            sb.Append(END_TABLE_ROW);
            sb.Append("<tr>");
            sb.Append("<td class='label'>Address:</td>");
            sb.Append($"<td class='value'>{deliveryInformation.Address}</td>");
            sb.Append(END_TABLE_ROW);
            sb.Append("<tr>");
            sb.Append("<td class='label'>Zipcode:</td>");
            sb.Append($"<td class='value'>{deliveryInformation.Zipcode}</td>");
            sb.Append(END_TABLE_ROW);
            sb.Append("<tr>");
            sb.Append("<td class='label'>City:</td>");
            sb.Append($"<td class='value'>{deliveryInformation.City}</td>");
            sb.Append(END_TABLE_ROW);
            sb.Append("<tr>");
            sb.Append("<td class='label'>Instructions:</td>");
            sb.Append($"<td class='value'>{deliveryInformation.Instructions}</td>");
            sb.Append(END_TABLE_ROW);
            return sb.ToString();
        }
    }
}
