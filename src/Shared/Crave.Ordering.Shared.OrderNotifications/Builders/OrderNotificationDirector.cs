﻿using Crave.Ordering.Shared.OrderNotifications.Interfaces.Builders;
using Crave.Ordering.Shared.OrderNotifications.Models;

namespace Crave.Ordering.Shared.OrderNotifications.Builders
{
    public class OrderNotificationDirector : IOrderNotificationDirector
    {
        private readonly IOrderNotificationBuilder orderNotificationBuilder;

        public OrderNotificationDirector(IOrderNotificationBuilder orderNotificationBuilder)
        {
            this.orderNotificationBuilder = orderNotificationBuilder;
        }

        public OrderNotification BuildNotification() => orderNotificationBuilder
                                                        .AddCompanyInfo()
                                                        .AddSellerInfo()
                                                        .AddReceiptInfo()
                                                        .AddServiceMethodInfo()
                                                        .AddCheckoutMethodInfo()
                                                        .AddPaymentInfo()
                                                        .AddOrderInfo()
                                                        .AddOrderTotalInfo()
                                                        .AddCoversCountInfo()
                                                        .AddTaxBreakdownInfo()
                                                        .AddCustomerInfo()
                                                        .AddDeliveryInformation()
                                                        .AddPaymentProcessingInfo()
                                                        .AddDeliverypointInfo()
                                                        .AddTaxInfo()
                                                        .Build();
    }
}
