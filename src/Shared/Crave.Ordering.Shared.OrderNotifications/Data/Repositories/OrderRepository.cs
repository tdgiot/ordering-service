﻿using System.Threading.Tasks;
using Crave.Ordering.Shared.Data.LLBLGen;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.SQLServer;
using Crave.Ordering.Shared.OrderNotifications.Interfaces.Repositories;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Crave.Ordering.Shared.OrderNotifications.Data.Repositories
{
    /// <summary>
    /// Repository class for orders.
    /// </summary>
    internal class OrderRepository : IOrderRepository
    {
        /// <inheritdoc/>
        public Task<OrderEntity> GetOrderForNotificationAsync(int orderId)
        {
            RelationPredicateBucket filterBucket = new RelationPredicateBucket();
            filterBucket.PredicateExpression.Add(OrderFields.OrderId == orderId);

            PrefetchPath2 prefetch = new PrefetchPath2(EntityType.OrderEntity);
            prefetch.Add(OrderEntity.PrefetchPathCompanyEntity, new IncludeFieldsList(CompanyFields.PricesIncludeTaxes, CompanyFields.CultureCode, CompanyFields.ClockMode));
            prefetch.Add(OrderEntity.PrefetchPathDeliveryInformationEntity);
            prefetch.Add(OrderEntity.PrefetchPathPaymentTransactionCollection);
            prefetch.Add(OrderEntity.PrefetchPathCheckoutMethodEntity);
            prefetch.Add(OrderEntity.PrefetchPathServiceMethodEntity);
            prefetch.Add(OrderEntity.PrefetchPathOutletEntity).SubPath.Add(OutletEntity.PrefetchPathOutletSellerInformationEntity);
            prefetch.Add(OrderEntity.PrefetchPathReceiptCollection);

            IPrefetchPathElement2 prefetchOrderitemCollection = prefetch.Add(OrderEntity.PrefetchPathOrderitemCollection);
            prefetchOrderitemCollection.SubPath.Add(OrderitemEntity.PrefetchPathOrderitemAlterationitemCollection).SubPath.Add(OrderitemAlterationitemEntity.PrefetchPathAlterationEntity);
            prefetchOrderitemCollection.SubPath.Add(OrderitemEntity.PrefetchPathTaxTariffEntity);

            return DataAccessAdapter.FetchEntityAsync<OrderEntity>(filterBucket, prefetch, null);
        }
    }
}
