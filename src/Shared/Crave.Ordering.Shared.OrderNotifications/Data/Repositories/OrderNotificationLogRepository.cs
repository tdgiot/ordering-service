﻿using System.Threading.Tasks;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.SQLServer;
using Crave.Ordering.Shared.OrderNotifications.Interfaces.Repositories;

namespace Crave.Ordering.Shared.OrderNotifications.Data.Repositories
{
    internal class OrderNotificationLogRepository : IOrderNotificationLogRepository
    {
        public async Task<OrderNotificationLogEntity> SaveEntityAsync(OrderNotificationLogEntity entity)
        {
            await new DataAccessAdapter().SaveEntityAsync(entity, true);
            return entity;
        }
    }
}