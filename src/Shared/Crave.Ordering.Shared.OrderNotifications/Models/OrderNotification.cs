﻿namespace Crave.Ordering.Shared.OrderNotifications.Models
{
    public class OrderNotification
    {
        public OrderNotification(string title, string message, string shortMessage)
            : this(title, message, shortMessage, true)
        {

        }

        public OrderNotification(string title, string message, string shortMessage, bool showOrderPrices)
        {
            this.Title = title;
            this.Message = message;
            this.ShortMessage = shortMessage;
            this.ShowOrderPrices = showOrderPrices;
        }

        public string Title { get; }
        public string Message { get; }
        public string ShortMessage { get; }
        public bool ShowOrderPrices { get; }
    }
}
