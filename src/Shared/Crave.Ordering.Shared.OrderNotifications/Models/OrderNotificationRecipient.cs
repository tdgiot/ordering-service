﻿using Crave.Enums;

namespace Crave.Ordering.Shared.OrderNotifications.Models
{
    internal class OrderNotificationRecipient
    {
        public OrderNotificationRecipient(OrderNotificationMethod method, string[] receivers)
        {
            this.Method = method;
            this.Receivers = receivers;
        }

        public OrderNotificationMethod Method { get; }
        public string[] Receivers { get; }
    }
}
