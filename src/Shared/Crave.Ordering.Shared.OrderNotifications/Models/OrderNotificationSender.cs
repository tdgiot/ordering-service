﻿namespace Crave.Ordering.Shared.OrderNotifications.Models
{
    internal class OrderNotificationSender
    {
        public OrderNotificationSender(string emailOriginator,
                                       string smsOriginator,
                                       string[] emailAddresses,
                                       string[] phoneNumbers,
                                       bool includeAsReceiptRecipient,
                                       bool includeAsConfirmationRecipient)
        {
            this.EmailOriginator = emailOriginator;
            this.SmsOriginator = smsOriginator;
            this.EmailAddresses = emailAddresses;
            this.PhoneNumbers = phoneNumbers;
            this.IncludeAsReceiptRecipient = includeAsReceiptRecipient;
            this.IncludeAsConfirmationRecipient = includeAsConfirmationRecipient;
        }

        public string EmailOriginator { get; }
        public string SmsOriginator { get; }
        public string[] EmailAddresses { get; }
        public string[] PhoneNumbers { get; }
        public bool IncludeAsReceiptRecipient { get; }
        public bool IncludeAsConfirmationRecipient { get; }
    }
}
