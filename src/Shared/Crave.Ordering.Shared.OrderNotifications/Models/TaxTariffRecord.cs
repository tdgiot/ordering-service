﻿namespace Crave.Ordering.Shared.OrderNotifications.Models
{
    internal class TaxTariffRecord
    {
        public TaxTariffRecord(int taxTariffId, double percentage, decimal netPrice, decimal vatPrce, string name = null)
        {
            this.TaxTariffId = taxTariffId;
            this.Name = name;
            this.Percentage = percentage;
            this.NetPrice = netPrice;
            this.VatPrice = vatPrce;
        }

        public int TaxTariffId { get; }
        public string Name { get; set; }
        public double Percentage { get; }
        public decimal NetPrice { get; set; }
        public decimal VatPrice { get; set; }
    }
}
