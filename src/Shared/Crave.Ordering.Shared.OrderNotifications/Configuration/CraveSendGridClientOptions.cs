﻿using System;
using Crave.Ordering.Shared.Configuration.Sections;
using SendGrid;
using SendGrid.Helpers.Reliability;

namespace Crave.Ordering.Shared.OrderNotifications.Configuration
{
    public class CraveSendGridClientOptions : SendGridClientOptions
    {
        public CraveSendGridClientOptions(SendGridConfiguration sendGridConfiguration)
        {
            this.ApiKey = sendGridConfiguration.ApiKey;
            this.ReliabilitySettings = new ReliabilitySettings(2, TimeSpan.FromSeconds(1), TimeSpan.FromSeconds(10), TimeSpan.FromSeconds(3));
        }
    }
}
