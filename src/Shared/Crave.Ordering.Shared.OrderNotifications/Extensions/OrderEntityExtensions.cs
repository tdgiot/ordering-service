﻿using System.Collections.Generic;
using System.Linq;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.OrderNotifications.Models;

namespace Crave.Ordering.Shared.OrderNotifications.Extensions
{
    internal static class OrderEntityExtensions
    {
        public static IOrderedEnumerable<TaxTariffRecord> GetTaxTariffRecords(this OrderEntity order, bool excludeZeroPercentage = false)
        {
            Dictionary<int, TaxTariffRecord> taxTariffRecords = new Dictionary<int, TaxTariffRecord>();

            foreach (OrderitemEntity orderItem in order.OrderitemCollection)
            {
                foreach (OrderitemAlterationitemEntity alterationItem in orderItem.OrderitemAlterationitemCollection)
                {
                    if (excludeZeroPercentage && alterationItem.TaxPercentage <= 0 || !alterationItem.TaxTariffId.HasValue)
                    {
                        continue;
                    }

                    AppendTaxTariffRecord(taxTariffRecords, alterationItem.TaxTariffId.Value, alterationItem.TaxPercentage,
                                                                alterationItem.PriceTotalExTax, alterationItem.TaxTotal, alterationItem.TaxName);
                }

                if (excludeZeroPercentage && orderItem.TaxPercentage <= 0 || !orderItem.TaxTariffId.HasValue)
                {
                    continue;
                }

                AppendTaxTariffRecord(taxTariffRecords, orderItem.TaxTariffId.Value, orderItem.TaxPercentage,
                                                            orderItem.PriceTotalExTax, orderItem.TaxTotal, orderItem.TaxName);
            }

            return taxTariffRecords.Values.OrderByDescending(taxTariffRecord => taxTariffRecord.Percentage);
        }

        private static void AppendTaxTariffRecord(IDictionary<int, TaxTariffRecord> taxTariffRecords, int taxTariffId, double percentage,
            decimal priceTotalExTax, decimal taxTotalPrice, string name)
        {
            if (!taxTariffRecords.TryGetValue(taxTariffId, out TaxTariffRecord taxTariffRecord))
            {
                taxTariffRecord = new TaxTariffRecord(taxTariffId, percentage, decimal.Zero, decimal.Zero, name);
                taxTariffRecords.Add(taxTariffId, taxTariffRecord);
            }

            taxTariffRecord.NetPrice += priceTotalExTax;
            taxTariffRecord.VatPrice += taxTotalPrice;
        }
    }
}
