﻿using Crave.Libraries.Mail;
using Crave.Libraries.Mail.SendGrid;
using Crave.Libraries.SMS;
using Crave.Libraries.SMS.Messagebird;
using Crave.Ordering.Shared.Configuration.Extensions;
using Crave.Ordering.Shared.Configuration.Sections;
using Crave.Ordering.Shared.OrderNotifications.Configuration;
using Crave.Ordering.Shared.OrderNotifications.Data.Repositories;
using Crave.Ordering.Shared.OrderNotifications.Domain.UseCases;
using Crave.Ordering.Shared.OrderNotifications.Interfaces.Repositories;
using Crave.Ordering.Shared.OrderNotifications.Interfaces.UseCases;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SendGrid;

namespace Crave.Ordering.Shared.OrderNotifications.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static void AddOrderNotifications(this IServiceCollection services, IConfiguration configuration) => services.Startup(configuration);

        // To be moved to startup when rewritten to lambda
        internal static void Startup(this IServiceCollection services, IConfiguration configuration) 
        {
            services.AddMailProvider(configuration.ReadAndValidate<SendGridConfiguration>());
            services.AddSmsProvider(configuration.ReadAndValidate<MessageBirdConfiguration>());
            services.AddRepositories();
            services.AddUseCases();
        }

        internal static void AddMailProvider(this IServiceCollection services, SendGridConfiguration sendGridConfiguration)
        {
            services.AddSingleton<ISendGridClient>(new SendGridClient(new CraveSendGridClientOptions(sendGridConfiguration)));
            services.AddSingleton<IMailProvider, SendGridMailProvider>();
        }

        internal static void AddSmsProvider(this IServiceCollection services, MessageBirdConfiguration messageBirdConfiguration)
        {
            services.AddSingleton<ISmsProvider>(new MessagebirdProvider(messageBirdConfiguration.AccessKey));
        }

        internal static void AddRepositories(this IServiceCollection services)
        {
            services.AddSingleton<IOrderNotificationLogRepository, OrderNotificationLogRepository>();
            services.AddSingleton<IOrderRepository, OrderRepository>();
        }

        internal static void AddUseCases(this IServiceCollection services)
        {
            services.AddSingleton<CreateOrderCompleteNotificationUseCase>();
            services.AddSingleton<CreateOrderConfirmationNotificationUseCase>();
            services.AddSingleton<CreateOrderNotificationLogUseCase>();
            services.AddSingleton<CreateOrderNotificationTemplateUseCase>();
            services.AddSingleton<CreateOrderReceiptNotificationUseCase>();
            services.AddSingleton<CreatePaymentFailedNotificationUseCase>();
            services.AddSingleton<CreatePaymentPendingNotificationUseCase>();
            services.AddSingleton<GetOrderNotificationRecipientsUseCase>();
            services.AddSingleton<GetOrderNotificationSenderUseCase>();
            services.AddSingleton<SendNotificationUseCase>();
            services.AddSingleton<ISendMailUseCase, SendMailUseCase>();
            services.AddSingleton<ISendTextMessageUseCase, SendTextMessageUseCase>();
            services.AddSingleton<ISendOrderNotificationUseCase, SendOrderNotificationUseCase>();
        }
    }
}
