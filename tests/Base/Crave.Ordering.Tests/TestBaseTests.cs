﻿using NUnit.Framework;

namespace Crave.Ordering.Tests
{
    [TestFixture]
    public class TestBaseTests : TestBase
    {
        [Test]
        public void CreateSubsituteForClassWithParameters()
        {
            Assert.DoesNotThrow(() => this.CreateSubstitute<CtorWithParams>());
        }

        [Test]
        public void CreateSubsituteForClassWithoutParameters()
        {
            Assert.DoesNotThrow(() => this.CreateSubstitute<CtorWithoutParams>());
        }

        // ReSharper disable once MemberCanBePrivate.Global
        // ReSharper disable once ClassNeverInstantiated.Global
        public class CtorWithParams
        {
            public CtorWithParams(string one, int two)
            {
                
            }
        }

        // ReSharper disable once MemberCanBePrivate.Global
        // ReSharper disable once ClassNeverInstantiated.Global
        public class CtorWithoutParams
        {
            public CtorWithoutParams()
            {

            }
        }
    }
}
