﻿using Crave.Ordering.Shared.Extensions;
using NUnit.Framework;

namespace Crave.Ordering.Shared.Tests.Extensions
{
    public class DecimalExtensionsTests
    {
        [TestCase(12, 20, 2)]
        public void CalculateTax_WithPricesIncludeTaxes_True(decimal price, double taxPercentage, decimal expected)
        {
            const bool pricesIncludeTaxes = true;

            decimal result =  price.CalculateTax(taxPercentage, pricesIncludeTaxes);

            Assert.AreEqual(result, expected);
        }

        [TestCase(10, 20, 2)]
        public void CalculateTax_WithPricesIncludeTaxes_False(decimal price, double taxPercentage, decimal expected)
        {
            const bool pricesIncludeTaxes = false;

            decimal result = price.CalculateTax(taxPercentage, pricesIncludeTaxes);

            Assert.AreEqual(result, expected);
        }
    }
}
