using Crave.Ordering.Shared.Extensions;
using NUnit.Framework;

namespace Crave.Ordering.Core.Tests.Extensions
{
    public class StringExtensionsTests
    {
        [TestCase("aaa@bbb.cc")]
        [TestCase("123@456.aa")]
        [TestCase("a_b@c12.de")]
        public void IsValidEmail_WithValidEmail_ReturnsTrue(string emailAddress)
        {
            bool valid = emailAddress.IsValidEmail();

            Assert.IsTrue(valid);
        }

        [TestCase("")]
        [TestCase(" ")]
        [TestCase("test123@app.a")]
        public void IsValidEmail_WithInValidEmail_ReturnsFalse(string emailAddress)
        {
            bool valid = emailAddress.IsValidEmail();

            Assert.IsFalse(valid);
        }

        [TestCase("00311234567890")]
        [TestCase("+31 1234567890")]
        [TestCase("+316 1234567890")]
        public void IsValidPhonenumber_WithValidPhoneNumber_ReturnsTrue(string phonenumber)
        {
            bool valid = phonenumber.IsValidPhonenumber();

            Assert.IsTrue(valid);
        }

        [TestCase("a0031640891976")]
        [TestCase("+316=40891976")]
        [TestCase("-316  40891976")]
        [TestCase("")]
        [TestCase(" ")]
        public void IsValidPhonenumber_WithInValidPhoneNumber_ReturnsFalse(string phonenumber)
        {
            bool valid = phonenumber.IsValidPhonenumber();

            Assert.IsFalse(valid);
        }
    }
}