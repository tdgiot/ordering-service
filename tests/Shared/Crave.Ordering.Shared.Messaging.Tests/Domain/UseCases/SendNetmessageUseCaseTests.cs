﻿using System.Collections.Generic;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Messaging.Domain.UseCases;
using Crave.Ordering.Shared.Messaging.Domain.UseCases.Requests;
using Crave.Ordering.Shared.Messaging.Interfaces.Repositories;
using Crave.Ordering.Shared.Messaging.Interfaces.UseCases;
using Crave.Ordering.Shared.Messaging.Netmessages;
using Crave.Ordering.Tests;
using NSubstitute;
using NUnit.Framework;
using ServiceStack.Messaging;

namespace Crave.Ordering.Shared.Messaging.Tests.Domain.UseCases
{
    [TestFixture]
    public class SendNetmessageUseCaseTests : TestBase
    {
        private IMessageProducer messageProducer;
        private IMessageFactory messageFactory;
        private IDeviceRepository deviceRepository;
        private ISaveNetmessagesUseCase saveNetmessagesUseCase;

        private SendNetmessageUseCase useCase;

        [OneTimeSetUp]
        public void _OneTimeSetUp()
        {
            this.messageProducer = this.CreateSubstitute<IMessageProducer>();
            this.messageFactory = this.CreateSubstitute<IMessageFactory>();
            this.messageFactory.CreateMessageProducer().Returns(this.messageProducer);
            this.deviceRepository = this.CreateSubstitute<IDeviceRepository>();

            this.saveNetmessagesUseCase = this.CreateSubstitute<ISaveNetmessagesUseCase>();

            this.useCase = new SendNetmessageUseCase(this.messageFactory, this.deviceRepository, this.saveNetmessagesUseCase);

            // Setup
            DeviceEntity deviceEntity = new DeviceEntity();
            deviceEntity.ClientCollection.AddNew().ClientId = 1;
            this.deviceRepository.GetDevice(Arg.Any<string>()).Returns(info =>
                                                                       {
                                                                           deviceEntity.Identifier = info.Arg<string>();
                                                                           return deviceEntity;
                                                                       });
            List<DeviceEntity> deviceList = new List<DeviceEntity> {deviceEntity};
            this.deviceRepository.GetDevicesForCompanyClients(Arg.Any<int>()).Returns(deviceList);
            this.deviceRepository.GetDevicesForCompanyTerminals(Arg.Any<int>()).Returns(deviceList);
            this.deviceRepository.GetDevicesForDeliverypointgroup(Arg.Any<int>()).Returns(deviceList);
            this.deviceRepository.GetDevicesForDeliverypoint(Arg.Any<int>()).Returns(deviceList);
        }

        [Test]
        public void SendNetmessageWithoutRecipients_Throws()
        {
            Assert.Throws<CraveException>(() => this.useCase.Execute(new SendNetmessage()));
        }

        [Test]
        public void SaveNetmessageForRecipient_Identifier()
        {
            SendNetmessage request = new SendNetmessage
                                     {
                                         Netmessage = new Netmessage
                                                      {
                                                          SaveToDatabase = true
                                                      }
                                     };
            request.Recipients.Identifier = "1234";

            this.useCase.Execute(request);

            this.saveNetmessagesUseCase.Received().Execute(Arg.Any<SaveNetmessages>());
        }

        [TestCase("Company-1", 1, true)]
        [TestCase("Company-1-Terminals", 1, false)]
        [TestCase("Company-1-Derp", 1, false)]
        [TestCase("Company-NaN", 0, false)]
        public void SaveNetmessageForRecipient_CompanyClientsGroup(string groupName, int companyId, bool shouldReceive)
        {
            SendNetmessage request = new SendNetmessage
            {
                Netmessage = new Netmessage
                {
                    SaveToDatabase = true
                }
            };
            request.Recipients.Groups.Add(groupName);

            this.useCase.Execute(request);

            if (shouldReceive)
            {
                this.deviceRepository.Received().GetDevicesForCompanyClients(companyId);
                this.saveNetmessagesUseCase.Received().Execute(Arg.Any<SaveNetmessages>());
            }
            else
            {
                this.deviceRepository.DidNotReceive().GetDevicesForCompanyClients(companyId);
            }
        }

        [TestCase("Company-1", 1, false)]
        [TestCase("Company-2-Terminals", 2, true)]
        [TestCase("Company-3-Derp", 3, false)]
        [TestCase("Company-NaN-Terminals", 0, false)]
        public void SaveNetmessageForRecipient_CompanyTerminalsGroup(string groupName, int companyId, bool shouldReceive)
        {
            SendNetmessage request = new SendNetmessage
            {
                Netmessage = new Netmessage
                {
                    SaveToDatabase = true
                }
            };
            request.Recipients.Groups.Add(groupName);

            this.useCase.Execute(request);

            if (shouldReceive)
            {
                this.deviceRepository.Received().GetDevicesForCompanyTerminals(companyId);
                this.saveNetmessagesUseCase.Received().Execute(Arg.Any<SaveNetmessages>());
            }
            else
            {
                this.deviceRepository.DidNotReceive().GetDevicesForCompanyTerminals(companyId);
            }
        }

        [TestCase("Deliverypointgroup-1", 1, true)]
        [TestCase("Deliverypointgroup-2-Derp", 2, true)]
        [TestCase("Deliverypointgroup-NaN", 0, false)]
        public void SaveNetmessageForRecipient_DeliverypointgroupGroup(string groupName, int deliverypointgroupId, bool shouldReceive)
        {
            SendNetmessage request = new SendNetmessage
                                     {
                                         Netmessage = new Netmessage
                                                      {
                                                          SaveToDatabase = true
                                                      }
                                     };
            request.Recipients.Groups.Add(groupName);

            this.useCase.Execute(request);

            if (shouldReceive)
            {
                this.deviceRepository.Received().GetDevicesForDeliverypointgroup(deliverypointgroupId);
                this.saveNetmessagesUseCase.Received().Execute(Arg.Any<SaveNetmessages>());
            }
            else
            {
                this.deviceRepository.DidNotReceive().GetDevicesForDeliverypointgroup(deliverypointgroupId);
            }
        }

        [TestCase("Deliverypoint-1", 1, true)]
        [TestCase("Deliverypoint-2-Derp", 2, true)]
        [TestCase("Deliverypoint-NaN", 0, false)]
        public void SaveNetmessageForRecipient_DeliverypointGroup(string groupName, int deliverypointId, bool shouldReceive)
        {
            SendNetmessage request = new SendNetmessage
            {
                Netmessage = new Netmessage
                {
                    SaveToDatabase = true
                }
            };
            request.Recipients.Groups.Add(groupName);

            this.useCase.Execute(request);

            if (shouldReceive)
            {
                this.deviceRepository.Received().GetDevicesForDeliverypoint(deliverypointId);
                this.saveNetmessagesUseCase.Received().Execute(Arg.Any<SaveNetmessages>());
            }
            else
            {
                this.deviceRepository.DidNotReceive().GetDevicesForDeliverypoint(deliverypointId);
            }
        }
    }
}