﻿using System.Collections.Generic;
using Crave.Libraries.SMS;

namespace Crave.Ordering.Shared.OrderNotifications.Tests.Builders
{
    internal class MessageResultBuilder
    {
        private string Reference { get; }
        private List<Recipient> Recipients { get; }

        public MessageResultBuilder() : this("dummy", new List<Recipient>())
        {

        }

        private MessageResultBuilder(
            string reference,
            List<Recipient> recipients)
        {
            this.Reference = reference;
            this.Recipients = recipients;
        }

        public MessageResultBuilder W(string reference) => new MessageResultBuilder(reference, this.Recipients);

        public MessageResultBuilder W(List<Recipient> recipients) => new MessageResultBuilder(this.Reference, recipients);

        public MessageResult Build() => new MessageResult(true, this.Reference, this.Recipients);
    }
}
