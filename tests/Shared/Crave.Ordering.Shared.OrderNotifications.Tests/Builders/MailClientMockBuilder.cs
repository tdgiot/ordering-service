﻿using System.Net.Mail;
using Crave.Libraries.Mail;
using Moq;

namespace Crave.Ordering.Shared.OrderNotifications.Tests.Builders
{
    internal class MailClientMockBuilder
    {
        private bool Success { get; }

        public MailClientMockBuilder() : this(true)
        {

        }

        private MailClientMockBuilder(bool success) => this.Success = success;

        public MailClientMockBuilder W(bool success) => new MailClientMockBuilder(success);

        public IMailProvider Build()
        {
            Mock<IMailProvider> mailClientMock = new Mock<IMailProvider>();
            mailClientMock.Setup(client => client.SendAsync(It.IsAny<MailMessage>())).ReturnsAsync(new MailResponse(this.Success, string.Empty));
            return mailClientMock.Object;
        }
    }
}
