﻿using Crave.Ordering.Shared.OrderNotifications.Domain.UseCases;

namespace Crave.Ordering.Shared.OrderNotifications.Tests.Builders.UseCaseBuilders
{
    internal class GetOrderNotificationSenderUseCaseBuilder
    {
        public GetOrderNotificationSenderUseCase Build() => new GetOrderNotificationSenderUseCase();
    }
}