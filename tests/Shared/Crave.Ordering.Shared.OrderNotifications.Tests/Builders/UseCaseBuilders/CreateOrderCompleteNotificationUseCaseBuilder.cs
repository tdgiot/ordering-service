﻿using Crave.Ordering.Shared.OrderNotifications.Domain.UseCases;

namespace Crave.Ordering.Shared.OrderNotifications.Tests.Builders.UseCaseBuilders
{
    internal class CreateOrderCompleteNotificationUseCaseBuilder
    {
        public CreateOrderCompleteNotificationUseCase Build() => new CreateOrderCompleteNotificationUseCase();
    }
}