﻿using Crave.Ordering.Shared.OrderNotifications.Domain.UseCases;

namespace Crave.Ordering.Shared.OrderNotifications.Tests.Builders.UseCaseBuilders
{
    internal class GetOrderNotificationRecipientsUseCaseBuilder
    {
        public GetOrderNotificationRecipientsUseCase Build() => new GetOrderNotificationRecipientsUseCase();
    }
}