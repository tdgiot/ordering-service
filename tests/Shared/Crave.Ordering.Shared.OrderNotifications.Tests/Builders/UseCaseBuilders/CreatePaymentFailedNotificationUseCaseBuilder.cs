﻿using Crave.Ordering.Shared.OrderNotifications.Domain.UseCases;

namespace Crave.Ordering.Shared.OrderNotifications.Tests.Builders.UseCaseBuilders
{
    internal class CreatePaymentFailedNotificationUseCaseBuilder
    {
        public CreatePaymentFailedNotificationUseCase Build() => new CreatePaymentFailedNotificationUseCase();
    }
}