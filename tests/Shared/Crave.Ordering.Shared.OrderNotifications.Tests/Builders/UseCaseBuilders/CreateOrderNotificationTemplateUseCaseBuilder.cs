﻿using Crave.Ordering.Shared.OrderNotifications.Domain.UseCases;

namespace Crave.Ordering.Shared.OrderNotifications.Tests.Builders.UseCaseBuilders
{
    internal class CreateOrderNotificationTemplateUseCaseBuilder
    {
        private CreateOrderCompleteNotificationUseCase CreateOrderCompleteNotificationUseCase { get; }
        private CreateOrderConfirmationNotificationUseCase CreateOrderConfirmationNotificationUseCase { get; }
        private CreatePaymentPendingNotificationUseCase CreatePaymentPendingNotificationUseCase { get; }
        private CreatePaymentFailedNotificationUseCase CreatePaymentFailedNotificationUseCase { get; }
        private CreateOrderReceiptNotificationUseCase CreateOrderReceiptNotificationUseCase { get; }

        public CreateOrderNotificationTemplateUseCaseBuilder() : this(
            new CreateOrderCompleteNotificationUseCaseBuilder().Build(),
            new CreateOrderConfirmationNotificationUseCaseBuilder().Build(),
            new CreatePaymentPendingNotificationUseCaseBuilder().Build(),
            new CreatePaymentFailedNotificationUseCaseBuilder().Build(),
            new CreateOrderReceiptNotificationUseCaseBuilder().Build())
        {

        }

        private CreateOrderNotificationTemplateUseCaseBuilder(
            CreateOrderCompleteNotificationUseCase createOrderCompleteNotificationUseCase,
            CreateOrderConfirmationNotificationUseCase createOrderConfirmationNotificationUseCase,
            CreatePaymentPendingNotificationUseCase createPaymentPendingNotificationUseCase,
            CreatePaymentFailedNotificationUseCase createPaymentFailedNotificationUseCase,
            CreateOrderReceiptNotificationUseCase createOrderReceiptNotificationUseCase)
        {
            this.CreateOrderCompleteNotificationUseCase = createOrderCompleteNotificationUseCase;
            this.CreateOrderConfirmationNotificationUseCase = createOrderConfirmationNotificationUseCase;
            this.CreatePaymentPendingNotificationUseCase = createPaymentPendingNotificationUseCase;
            this.CreatePaymentFailedNotificationUseCase = createPaymentFailedNotificationUseCase;
            this.CreateOrderReceiptNotificationUseCase = createOrderReceiptNotificationUseCase;
        }

        public CreateOrderNotificationTemplateUseCase Build() => new CreateOrderNotificationTemplateUseCase(
            this.CreateOrderCompleteNotificationUseCase,
            this.CreateOrderConfirmationNotificationUseCase,
            this.CreatePaymentPendingNotificationUseCase,
            this.CreatePaymentFailedNotificationUseCase,
            this.CreateOrderReceiptNotificationUseCase);
    }
}