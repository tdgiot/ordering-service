﻿using Crave.Ordering.Shared.OrderNotifications.Domain.UseCases;
using Crave.Ordering.Shared.OrderNotifications.Interfaces.UseCases;

namespace Crave.Ordering.Shared.OrderNotifications.Tests.Builders.UseCaseBuilders
{
    internal class SendNotificationUseCaseBuilder
    {
        private ISendTextMessageUseCase SendTextMessageUseCase { get; }
        private SendMailUseCase SendMailUseCase { get; }

        public SendNotificationUseCaseBuilder() : this(
            new SendTextMessageUseCaseBuilder().Build(),
            new SendMailUseCaseBuilder().Build())
        {

        }

        private SendNotificationUseCaseBuilder(
            ISendTextMessageUseCase sendTextMessageUseCase,
            SendMailUseCase sendMailUseCase)
        {
            this.SendTextMessageUseCase = sendTextMessageUseCase;
            this.SendMailUseCase = sendMailUseCase;
        }

        public SendNotificationUseCaseBuilder W(SendTextMessageUseCaseBuilder sendTextMessageUseCaseBuilder) => new SendNotificationUseCaseBuilder(
            sendTextMessageUseCaseBuilder.Build(),
            this.SendMailUseCase
            );

        public SendNotificationUseCaseBuilder W(SendMailUseCaseBuilder sendMailUseCaseBuilder) => new SendNotificationUseCaseBuilder(
            this.SendTextMessageUseCase,
            sendMailUseCaseBuilder.Build()
        );

        public SendNotificationUseCase Build() => new SendNotificationUseCase(
            this.SendTextMessageUseCase,
            this.SendMailUseCase);
    }
}