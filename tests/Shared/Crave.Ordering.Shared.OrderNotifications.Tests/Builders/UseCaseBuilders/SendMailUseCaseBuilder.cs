﻿using Crave.Libraries.Mail;
using Crave.Ordering.Shared.OrderNotifications.Domain.UseCases;
using Microsoft.Extensions.Logging;

namespace Crave.Ordering.Shared.OrderNotifications.Tests.Builders.UseCaseBuilders
{
    internal class SendMailUseCaseBuilder
    {
        private ILogger<SendMailUseCase> Logger { get; }
        private IMailProvider MailClient { get; }

        public SendMailUseCaseBuilder() : this(
            new LoggerMockBuilder<SendMailUseCase>().Build(),
            new MailClientMockBuilder().Build())
        {

        }

        private SendMailUseCaseBuilder(
            ILogger<SendMailUseCase> logger,
            IMailProvider mailClient)
        {
            this.Logger = logger;
            this.MailClient = mailClient;
        }

        public SendMailUseCaseBuilder W(LoggerMockBuilder<SendMailUseCase> loggerMockBuilder) => new SendMailUseCaseBuilder(
            loggerMockBuilder.Build(),
            this.MailClient
            );

        public SendMailUseCaseBuilder W(MailClientMockBuilder mailClientBuilder) => new SendMailUseCaseBuilder(
            this.Logger,
            mailClientBuilder.Build()
        );

        public SendMailUseCase Build() => new SendMailUseCase(
            this.Logger,
            this.MailClient);
    }
}