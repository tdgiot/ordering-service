﻿using Crave.Ordering.Shared.OrderNotifications.Domain.UseCases;
using Crave.Ordering.Shared.OrderNotifications.Interfaces.Repositories;
using Crave.Ordering.Shared.OrderNotifications.Tests.Builders.RepositoryBuilders;
using Microsoft.Extensions.Logging;

namespace Crave.Ordering.Shared.OrderNotifications.Tests.Builders.UseCaseBuilders
{
    internal class SendOrderNotificationUseCaseBuilder
    {
        private ILogger<SendOrderNotificationUseCase> Logger { get; }
        private IOrderRepository OrderRepository { get; }
        private GetOrderNotificationRecipientsUseCase GetOrderNotificationRecipientsUseCase { get; }
        private GetOrderNotificationSenderUseCase GetOrderNotificationSenderUseCase { get; }
        private CreateOrderNotificationTemplateUseCase CreateOrderNotificationTemplateUseCase { get; }
        private SendNotificationUseCase SendOrderNotificationUseCase { get; }
        private CreateOrderNotificationLogUseCase CreateOrderNotificationLogUseCase { get; }

        public SendOrderNotificationUseCaseBuilder() : this(
            new LoggerMockBuilder<SendOrderNotificationUseCase>().Build(),
            new OrderRepositoryMockBuilder().Build().Object,
            new GetOrderNotificationRecipientsUseCaseBuilder().Build(),
            new GetOrderNotificationSenderUseCaseBuilder().Build(),
            new CreateOrderNotificationTemplateUseCaseBuilder().Build(),
            new SendNotificationUseCaseBuilder().Build(),
            new CreateOrderNotificationLogUseCaseBuilder().Build())
        {

        }

        private SendOrderNotificationUseCaseBuilder(ILogger<SendOrderNotificationUseCase> logger,
                                                    IOrderRepository orderRepository,
                                                    GetOrderNotificationRecipientsUseCase getOrderNotificationRecipientsUseCase,
                                                    GetOrderNotificationSenderUseCase getOrderNotificationSenderUseCase,
                                                    CreateOrderNotificationTemplateUseCase createOrderNotificationTemplateUseCase,
                                                    SendNotificationUseCase sendOrderNotificationUseCase,
                                                    CreateOrderNotificationLogUseCase createOrderNotificationLogUseCase)
        {
            this.Logger = logger;
            this.OrderRepository = orderRepository;
            this.GetOrderNotificationRecipientsUseCase = getOrderNotificationRecipientsUseCase;
            this.GetOrderNotificationSenderUseCase = getOrderNotificationSenderUseCase;
            this.CreateOrderNotificationTemplateUseCase = createOrderNotificationTemplateUseCase;
            this.SendOrderNotificationUseCase = sendOrderNotificationUseCase;
            this.CreateOrderNotificationLogUseCase = createOrderNotificationLogUseCase;
        }

        public SendOrderNotificationUseCaseBuilder W(ILogger<SendOrderNotificationUseCase> logger) => new SendOrderNotificationUseCaseBuilder(
            logger,
            this.OrderRepository,
            this.GetOrderNotificationRecipientsUseCase,
            this.GetOrderNotificationSenderUseCase,
            this.CreateOrderNotificationTemplateUseCase,
            this.SendOrderNotificationUseCase,
            this.CreateOrderNotificationLogUseCase
            );

        public SendOrderNotificationUseCaseBuilder W(OrderRepositoryMockBuilder orderRepositoryMockBuilder) => new SendOrderNotificationUseCaseBuilder(
            this.Logger,
            orderRepositoryMockBuilder.Build().Object,
            this.GetOrderNotificationRecipientsUseCase,
            this.GetOrderNotificationSenderUseCase,
            this.CreateOrderNotificationTemplateUseCase,
            this.SendOrderNotificationUseCase,
            this.CreateOrderNotificationLogUseCase
        );

        public SendOrderNotificationUseCaseBuilder W(GetOrderNotificationRecipientsUseCaseBuilder getOrderNotificationRecipientsUseCaseBuilder) => new SendOrderNotificationUseCaseBuilder(
            this.Logger,
            this.OrderRepository,
            getOrderNotificationRecipientsUseCaseBuilder.Build(),
            this.GetOrderNotificationSenderUseCase,
            this.CreateOrderNotificationTemplateUseCase,
            this.SendOrderNotificationUseCase,
            this.CreateOrderNotificationLogUseCase
        );

        public SendOrderNotificationUseCaseBuilder W(GetOrderNotificationSenderUseCaseBuilder getOrderNotificationSenderUseCaseBuilder) => new SendOrderNotificationUseCaseBuilder(
            this.Logger,
            this.OrderRepository,
            this.GetOrderNotificationRecipientsUseCase,
            getOrderNotificationSenderUseCaseBuilder.Build(),
            this.CreateOrderNotificationTemplateUseCase,
            this.SendOrderNotificationUseCase,
            this.CreateOrderNotificationLogUseCase
        );

        public SendOrderNotificationUseCaseBuilder W(CreateOrderNotificationTemplateUseCaseBuilder createOrderNotificationTemplateUseCaseBuilder) => new SendOrderNotificationUseCaseBuilder(
            this.Logger,
            this.OrderRepository,
            this.GetOrderNotificationRecipientsUseCase,
            this.GetOrderNotificationSenderUseCase,
            createOrderNotificationTemplateUseCaseBuilder.Build(),
            this.SendOrderNotificationUseCase,
            this.CreateOrderNotificationLogUseCase
        );

        public SendOrderNotificationUseCaseBuilder W(SendNotificationUseCaseBuilder sendNotificationUseCaseBuilder) => new SendOrderNotificationUseCaseBuilder(
            this.Logger,
            this.OrderRepository,
            this.GetOrderNotificationRecipientsUseCase,
            this.GetOrderNotificationSenderUseCase,
            this.CreateOrderNotificationTemplateUseCase,
            sendNotificationUseCaseBuilder.Build(),
            this.CreateOrderNotificationLogUseCase
        );

        public SendOrderNotificationUseCaseBuilder W(CreateOrderNotificationLogUseCaseBuilder createOrderNotificationLogUseCaseBuilder) => new SendOrderNotificationUseCaseBuilder(
            this.Logger,
            this.OrderRepository,
            this.GetOrderNotificationRecipientsUseCase,
            this.GetOrderNotificationSenderUseCase,
            this.CreateOrderNotificationTemplateUseCase,
            this.SendOrderNotificationUseCase,
            createOrderNotificationLogUseCaseBuilder.Build()
        );

        public SendOrderNotificationUseCase Build() =>
            new SendOrderNotificationUseCase(
                this.Logger,
                this.OrderRepository,
                this.GetOrderNotificationRecipientsUseCase,
                this.GetOrderNotificationSenderUseCase,
                this.CreateOrderNotificationTemplateUseCase,
                this.SendOrderNotificationUseCase,
                this.CreateOrderNotificationLogUseCase);
    }
}