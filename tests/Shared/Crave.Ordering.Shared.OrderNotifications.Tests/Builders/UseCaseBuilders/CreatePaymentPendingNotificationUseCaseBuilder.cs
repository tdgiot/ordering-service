﻿using Crave.Ordering.Shared.OrderNotifications.Domain.UseCases;

namespace Crave.Ordering.Shared.OrderNotifications.Tests.Builders.UseCaseBuilders
{
    internal class CreatePaymentPendingNotificationUseCaseBuilder
    {
        public CreatePaymentPendingNotificationUseCase Build() => new CreatePaymentPendingNotificationUseCase();
    }
}