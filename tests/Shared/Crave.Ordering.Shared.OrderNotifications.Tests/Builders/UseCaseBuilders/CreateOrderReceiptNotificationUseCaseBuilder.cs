﻿using Crave.Ordering.Shared.OrderNotifications.Domain.UseCases;

namespace Crave.Ordering.Shared.OrderNotifications.Tests.Builders.UseCaseBuilders
{
    internal class CreateOrderReceiptNotificationUseCaseBuilder
    {
        public CreateOrderReceiptNotificationUseCase Build() => new CreateOrderReceiptNotificationUseCase();
    }
}