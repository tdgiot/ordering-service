﻿using Crave.Libraries.SMS;
using Crave.Ordering.Shared.OrderNotifications.Domain.UseCases;
using Microsoft.Extensions.Logging;

namespace Crave.Ordering.Shared.OrderNotifications.Tests.Builders.UseCaseBuilders
{
    internal class SendTextMessageUseCaseBuilder
    {
        private ILogger<SendTextMessageUseCase> Logger { get; }
        private ISmsProvider SmsProvider { get; }

        public SendTextMessageUseCaseBuilder() : this(
            new LoggerMockBuilder<SendTextMessageUseCase>().Build(),
            new SmsProviderMockBuilder().Build())
        {

        }

        private SendTextMessageUseCaseBuilder(
            ILogger<SendTextMessageUseCase> logger,
            ISmsProvider smsProvider)
        {
            this.Logger = logger;
            this.SmsProvider = smsProvider;
        }

        public SendTextMessageUseCaseBuilder W(LoggerMockBuilder<SendTextMessageUseCase> loggerMockBuilder) => new SendTextMessageUseCaseBuilder(
            loggerMockBuilder.Build(),
            this.SmsProvider
            );

        public SendTextMessageUseCaseBuilder W(SmsProviderMockBuilder smsProviderMockBuilder) => new SendTextMessageUseCaseBuilder(
            this.Logger,
            smsProviderMockBuilder.Build()
        );

        public SendTextMessageUseCase Build() => new SendTextMessageUseCase(
            this.Logger,
            this.SmsProvider);
    }
}