﻿using Crave.Ordering.Shared.OrderNotifications.Domain.UseCases;
using Crave.Ordering.Shared.OrderNotifications.Interfaces.Repositories;
using Crave.Ordering.Shared.OrderNotifications.Tests.Builders.RepositoryBuilders;

namespace Crave.Ordering.Shared.OrderNotifications.Tests.Builders.UseCaseBuilders
{
    internal class CreateOrderNotificationLogUseCaseBuilder
    {
        private IOrderNotificationLogRepository OrderNotificationLogRepository { get; }

        public CreateOrderNotificationLogUseCaseBuilder() : this(new OrderNotificationLogRepositoryMockBuilder().Build())
        {

        }

        private CreateOrderNotificationLogUseCaseBuilder(IOrderNotificationLogRepository orderNotificationLogRepository)
        {
            this.OrderNotificationLogRepository = orderNotificationLogRepository;
        }

        public CreateOrderNotificationLogUseCaseBuilder W(IOrderNotificationLogRepository orderNotificationLogRepository) => new CreateOrderNotificationLogUseCaseBuilder(orderNotificationLogRepository);

        public CreateOrderNotificationLogUseCase Build() => new CreateOrderNotificationLogUseCase(this.OrderNotificationLogRepository);
    }
}