﻿using System.Collections.Generic;
using Crave.Ordering.Api.Domain.Tests.Builders.EntityBuilders;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.Tests.Builders;
using Crave.Ordering.Shared.OrderNotifications.Builders;
using Crave.Ordering.Shared.OrderNotifications.Interfaces.Builders;
using Crave.Ordering.Shared.OrderNotifications.Models;
using NUnit.Framework;

namespace Crave.Ordering.Shared.OrderNotifications.Tests.Builders
{
    public class OrderNotificationBuilderTests
    {
        private static class A 
        {
            public static OrderEntityBuilder OrderEntity => new OrderEntityBuilder();
            public static OrderitemEntityBuilder OrderitemEntity => new OrderitemEntityBuilder();
            public static OutletEntityBuilder OutletEntity => new OutletEntityBuilder();
            public static ProductEntityBuilder ProductEntity => new ProductEntityBuilder();
        }

        [Test]
        public void BuildNotification_WithAppendTemplates_OnlyAppendsNewVariables()
        {
            // Arrange
            OrderEntity orderEntity = A.OrderEntity
                                       .W((OrderId)42)
                                       .W(A.OrderitemEntity
                                           .W(A.ProductEntity))
                                       .W(A.OutletEntity)
                                       .Build();

            string expectedResult = "Order=42";

            IDictionary<string, string> additionalTemplateVars = new Dictionary<string, string>()
                                                                 {
                                                                     {"ORDERNUMBER", "1337"}
                                                                 };

            IOrderNotificationBuilder orderNotificationBuilder = new OrderNotificationBuilder()
                                                                 .WithTemplate(new OrderNotification(string.Empty, string.Empty, "Order=[[ORDERNUMBER]]"))
                                                                 .WithOrder(orderEntity)
                                                                 .WithAppendAdditionalTemplateVarsIfUnique(true)
                                                                 .WithAdditionalTemplateVars(additionalTemplateVars);

            // Act
            OrderNotification response = new OrderNotificationDirector(orderNotificationBuilder)
                .BuildNotification();

            // Assert
            StringAssert.Contains(expectedResult, response.ShortMessage);
        }

        [Test]
        public void BuildNotification_WithOverwriteTemplates_OverwriteExistingVariables()
        {
            // Arrange
            OrderEntity orderEntity = A.OrderEntity
                                       .W((OrderId)42)
                                       .W(A.OrderitemEntity
                                           .W(A.ProductEntity))
                                       .W(A.OutletEntity)
                                       .Build();

            string expectedResult = "Order=1337";

            IDictionary<string, string> additionalTemplateVars = new Dictionary<string, string>()
                                                                 {
                                                                     {"ORDERNUMBER", "1337"}
                                                                 };

            IOrderNotificationBuilder orderNotificationBuilder = new OrderNotificationBuilder()
                                                                 .WithTemplate(new OrderNotification(string.Empty, string.Empty, "Order=[[ORDERNUMBER]]"))
                                                                 .WithOrder(orderEntity)
                                                                 .WithAppendAdditionalTemplateVarsIfUnique(false)
                                                                 .WithAdditionalTemplateVars(additionalTemplateVars);

            // Act
            OrderNotification response = new OrderNotificationDirector(orderNotificationBuilder)
                .BuildNotification();

            // Assert
            StringAssert.Contains(expectedResult, response.ShortMessage);
        }
    }
}