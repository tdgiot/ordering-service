﻿using System.Collections.Generic;
using Crave.Libraries.SMS;
using Moq;

namespace Crave.Ordering.Shared.OrderNotifications.Tests.Builders
{
    internal class SmsProviderMockBuilder
    {
        private MessageResult MessageResult { get; }

        public SmsProviderMockBuilder() : this(new MessageResultBuilder().Build())
        {

        }

        private SmsProviderMockBuilder(MessageResult messageResult) => this.MessageResult = messageResult;

        public SmsProviderMockBuilder W(MessageResultBuilder messageResultMockBuilder) => new SmsProviderMockBuilder(messageResultMockBuilder.Build());

        public ISmsProvider Build()
        {
            Mock<ISmsProvider> smsProviderMock = new Mock<ISmsProvider>();
            smsProviderMock.Setup(provider => provider.SendTextMessage(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<IEnumerable<long>>())).Returns(this.MessageResult);
            return smsProviderMock.Object;
        }
    }
}
