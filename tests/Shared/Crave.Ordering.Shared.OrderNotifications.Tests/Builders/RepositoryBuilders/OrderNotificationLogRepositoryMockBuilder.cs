﻿using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.Tests.Builders;
using Crave.Ordering.Shared.OrderNotifications.Interfaces.Repositories;
using Moq;

namespace Crave.Ordering.Shared.OrderNotifications.Tests.Builders.RepositoryBuilders
{
    internal class OrderNotificationLogRepositoryMockBuilder
    {
        private OrderNotificationLogEntity OrderNotificationLogEntity { get; }

        public OrderNotificationLogRepositoryMockBuilder() : this(new OrderNotificationLogEntityBuilder().Build()) { }

        private OrderNotificationLogRepositoryMockBuilder(OrderNotificationLogEntity orderNotificationLogEntity) => this.OrderNotificationLogEntity = orderNotificationLogEntity;

        public OrderNotificationLogRepositoryMockBuilder W(OrderNotificationLogEntity orderNotificationLogEntity) => new OrderNotificationLogRepositoryMockBuilder(orderNotificationLogEntity);

        public IOrderNotificationLogRepository Build()
        {
            Mock<IOrderNotificationLogRepository> repositoryMock = new Mock<IOrderNotificationLogRepository>();
            _ = repositoryMock.Setup(repository => repository.SaveEntityAsync(It.IsAny<OrderNotificationLogEntity>())).ReturnsAsync(this.OrderNotificationLogEntity);
            return repositoryMock.Object;
        }
    }
}