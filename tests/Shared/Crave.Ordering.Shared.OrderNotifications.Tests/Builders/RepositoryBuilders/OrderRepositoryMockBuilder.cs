﻿using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.Tests.Builders;
using Crave.Ordering.Shared.OrderNotifications.Interfaces.Repositories;
using Moq;

namespace Crave.Ordering.Shared.OrderNotifications.Tests.Builders.RepositoryBuilders
{
    internal class OrderRepositoryMockBuilder
    {
        private OrderEntity OrderEntity { get; }

        public OrderRepositoryMockBuilder() : this(new OrderEntityBuilder().Build())
        {
        }

        private OrderRepositoryMockBuilder(OrderEntity orderEntity) => this.OrderEntity = orderEntity;

        public OrderRepositoryMockBuilder W(OrderEntityBuilder orderEntityBuilder) => new OrderRepositoryMockBuilder(orderEntityBuilder.Build());

        public Mock<IOrderRepository> Build()
        {
            Mock<IOrderRepository> repositoryMock = new Mock<IOrderRepository>();
            _ = repositoryMock.Setup(repository => repository.GetOrderForNotificationAsync(It.IsAny<int>())).ReturnsAsync(this.OrderEntity);
            return repositoryMock;
        }
    }
}
