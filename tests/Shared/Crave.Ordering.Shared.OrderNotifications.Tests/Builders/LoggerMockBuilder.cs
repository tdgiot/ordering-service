﻿using Microsoft.Extensions.Logging;
using Moq;

namespace Crave.Ordering.Shared.OrderNotifications.Tests.Builders
{
    internal class LoggerMockBuilder<T>
    {
        public LoggerMockBuilder()
        {

        }

        public ILogger<T> Build()
        {
            Mock<ILogger<T>> mock = new Mock<ILogger<T>>();
            return mock.Object;
        }
    }
}
