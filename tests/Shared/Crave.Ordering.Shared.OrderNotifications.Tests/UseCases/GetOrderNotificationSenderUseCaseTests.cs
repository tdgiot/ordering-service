﻿using Crave.Ordering.Api.Domain.Tests.Builders.EntityBuilders;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.Tests.Builders;
using Crave.Ordering.Shared.OrderNotifications.Domain.UseCases;
using Crave.Ordering.Shared.OrderNotifications.Domain.UseCases.Requests;
using Crave.Ordering.Shared.OrderNotifications.Domain.UseCases.Responses;
using Crave.Ordering.Shared.OrderNotifications.Tests.Builders.UseCaseBuilders;
using NUnit.Framework;

namespace Crave.Ordering.Shared.OrderNotifications.Tests.UseCases
{
    public class GetOrderNotificationSenderUseCaseTests
    {
        private static class A
        {
            public static OrderEntityBuilder OrderEntity => new OrderEntityBuilder(false);
            public static OutletEntityBuilder OutletEntity => new OutletEntityBuilder(false);
            public static OutletSellerInformationEntityBuilder OutletSellerInformationEntity => new OutletSellerInformationEntityBuilder(false);
            public static GetOrderNotificationSenderUseCaseBuilder UseCase => new GetOrderNotificationSenderUseCaseBuilder();
        }

        [Test]
        public void Execute_ForAnyOutletSeller_UseCraveNoReplyEmail()
        {
            // Arrange
            OrderEntity orderEntity = A.OrderEntity
                                       .W(A.OutletEntity
                                           .W(A.OutletSellerInformationEntity
                                               .WithSellerEmail("myhotel@venue.com")))
                                       .Build();

            GetOrderNotificationSenderUseCase sut = A.UseCase.Build();

            // Act
            GetOrderNotificationSenderResponse result = sut.Execute(new GetOrderNotificationSenderRequest(orderEntity));

            // Assert
            StringAssert.AreEqualIgnoringCase(result.OrderNotificationSender.EmailOriginator, "no-reply@trueguest.tech");
        }

        [Test]
        public void Execute_OutletSellerIsPPHE_ConvertEmail()
        {
            // Arrange
            OrderEntity orderEntity = A.OrderEntity
                                           .W(A.OutletEntity
                                               .W(A.OutletSellerInformationEntity
                                                   .WithSellerEmail("myhotel@pphe.com")))
                                           .Build();

            GetOrderNotificationSenderUseCase sut = A.UseCase.Build();

            // Act
            GetOrderNotificationSenderResponse result = sut.Execute(new GetOrderNotificationSenderRequest(orderEntity));

            // Assert
            StringAssert.AreEqualIgnoringCase(result.OrderNotificationSender.EmailOriginator, "noreply@pphe.com");
        }
    }
}