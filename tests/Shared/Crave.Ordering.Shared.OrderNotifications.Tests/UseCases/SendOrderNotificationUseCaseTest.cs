﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Crave.Enums;
using Crave.Libraries.SMS;
using Crave.Ordering.Api.Domain.Tests.Builders.EntityBuilders;
using Crave.Ordering.Shared.Data.LLBLGen.Tests.Builders;
using Crave.Ordering.Shared.OrderNotifications.Domain.UseCases.Requests;
using Crave.Ordering.Shared.OrderNotifications.Tests.Builders;
using Crave.Ordering.Shared.OrderNotifications.Tests.Builders.RepositoryBuilders;
using Crave.Ordering.Shared.OrderNotifications.Tests.Builders.UseCaseBuilders;
using NUnit.Framework;

namespace Crave.Ordering.Shared.OrderNotifications.Tests.UseCases
{
    [TestFixture]
    public class SendOrderNotificationUseCaseTest
    {
        static class A
        {
            public static SendOrderNotificationUseCaseBuilder SendOrderNotificationUseCaseBuilder => new SendOrderNotificationUseCaseBuilder();
            public static SendNotificationUseCaseBuilder SendNotificationUseCaseBuilder => new SendNotificationUseCaseBuilder();
            public static CreateOrderNotificationLogUseCaseBuilder CreateOrderNotificationLogUseCaseBuilder => new CreateOrderNotificationLogUseCaseBuilder();
            public static SendMailUseCaseBuilder SendMailUseCaseBuilder => new SendMailUseCaseBuilder();
            public static MailClientMockBuilder MailClientMockBuilder => new MailClientMockBuilder();
            public static SendTextMessageUseCaseBuilder SendTextMessageUseCaseBuilder => new SendTextMessageUseCaseBuilder();
            public static SmsProviderMockBuilder SmsProviderMockBuilder => new SmsProviderMockBuilder();
            public static MessageResultBuilder MessageResultMockBuilder => new MessageResultBuilder();
            public static OrderEntityBuilder OrderEntity => new OrderEntityBuilder();
            public static OrderitemEntityBuilder OrderitemEntity => new OrderitemEntityBuilder();
            public static OutletEntityBuilder OutletEntity => new OutletEntityBuilder();
            public static OutletSellerInformationEntityBuilder OutletSellerInformationEntity => new OutletSellerInformationEntityBuilder();
            public static CheckoutMethodEntityBuilder CheckoutMethodEntity => new CheckoutMethodEntityBuilder();
            public static ServiceMethodEntityBuilder ServiceMethodEntity => new ServiceMethodEntityBuilder();
            public static OrderRepositoryMockBuilder OrderRepository => new OrderRepositoryMockBuilder();
        }

        [Test]
        public async Task ExecuteAsync_WithNewOrder_ReturnsNoOrderFound()
        {
            // Arrange
            var sut = A.SendOrderNotificationUseCaseBuilder
                       .W(A.OrderRepository
                           .W(A.OrderEntity
                               .AsNew()))
                       .Build();

            var request = new SendOrderNotificationRequest(1, OrderNotificationType.OrderConfirmation);

            // Act
            var result = await sut.ExecuteAsync(request);

            // Assert
            Assert.False(result.Success);

            StringAssert.Contains("No order found", result.Message);
            CollectionAssert.Contains(result.Args, 1);
        }

        [Test]
        public async Task ExecuteAsync_WithMissingOutletOnOrder_ReturnsFalseWithoutMessage()
        {
            // Arrange
            var sut = A.SendOrderNotificationUseCaseBuilder
                       .W(A.OrderRepository
                           .W(A.OrderEntity
                               .W(A.OutletEntity
                                   .AsNew())))
                       .Build();

            var request = new SendOrderNotificationRequest(1, OrderNotificationType.OrderConfirmation);

            // Act
            var result = await sut.ExecuteAsync(request);

            // Assert
            Assert.False(result.Success);
            Assert.That(result.Message, Is.Empty);
        }

        [Test]
        public async Task ExecuteAsync_WithMissingOutletSellerInformationOnOrder_ReturnsUnableToRetrieveSenderForOrder_MissingOutletSellerInformation()
        {
            // Arrange
            var sut = A.SendOrderNotificationUseCaseBuilder
                       .W(A.OrderRepository
                           .W(A.OrderEntity
                               .W(A.OutletEntity
                                   .W(2)
                                   .W(A.OutletSellerInformationEntity
                                       .AsNew()))))
                       .Build();

            var request = new SendOrderNotificationRequest(1, OrderNotificationType.OrderConfirmation);

            // Act
            var result = await sut.ExecuteAsync(request);

            // Assert
            Assert.False(result.Success);

            StringAssert.Contains("Unable to retrieve sender for order", result.Message);
            CollectionAssert.Contains(result.Args, 1);
            CollectionAssert.Contains(result.Args, "Missing outlet seller information on outlet. OutletId=(2)");
        }

        [Test]
        public async Task ExecuteAsync_WithMissingCheckoutMethod_ReturnsUnableToRetrieveRecipients_MissingCheckoutMethod()
        {
            // Arrange
            var sut = A.SendOrderNotificationUseCaseBuilder
                       .W(A.OrderRepository
                           .W(A.OrderEntity
                               .W(A.OutletEntity
                                   .W(A.OutletSellerInformationEntity))
                               .W(A.CheckoutMethodEntity
                                   .AsNew())))
                       .Build();

            var request = new SendOrderNotificationRequest(1, OrderNotificationType.OrderConfirmation);

            // Act
            var result = await sut.ExecuteAsync(request);

            // Assert
            Assert.False(result.Success);

            StringAssert.Contains("Unable to retrieve recipients for order", result.Message);
            CollectionAssert.Contains(result.Args, 1);
            CollectionAssert.Contains(result.Args, "Missing checkout method on order");
        }

        [Test]
        public async Task ExecuteAsync_WithMissingCheckoutMethod_ReturnsUnableToRetrieveRecipients_MissingServiceMethod()
        {
            // Arrange
            var sut = A.SendOrderNotificationUseCaseBuilder
                       .W(A.OrderRepository
                           .W(A.OrderEntity
                               .W(A.OutletEntity
                                   .W(A.OutletSellerInformationEntity))
                               .W(A.CheckoutMethodEntity)
                               .W(A.ServiceMethodEntity
                                   .AsNew())))
                       .Build();

            var request = new SendOrderNotificationRequest(1, OrderNotificationType.OrderConfirmation);

            // Act
            var result = await sut.ExecuteAsync(request);

            // Assert
            Assert.False(result.Success);
            StringAssert.Contains("Unable to retrieve recipients for order", result.Message);
            CollectionAssert.Contains(result.Args, 1);
            CollectionAssert.Contains(result.Args, "Missing service method on order");
        }

        [Test]
        public async Task ExecuteAsync_WithMissingRecipients_ReturnsNoRecipientsForOrder()
        {
            // Arrange
            var sut = A.SendOrderNotificationUseCaseBuilder
                       .W(A.OrderRepository
                           .W(A.OrderEntity
                               .W((CustomerEmail)string.Empty)
                               .W((CustomerPhoneNumber)string.Empty)
                               .W(A.OutletEntity
                                   .W(A.OutletSellerInformationEntity))
                               .W(A.CheckoutMethodEntity
                                   .W(OrderNotificationMethod.EmailSMS))
                               .W(A.ServiceMethodEntity)))
                       .Build();

            var request = new SendOrderNotificationRequest(1, OrderNotificationType.OrderConfirmation);

            // Act
            var result = await sut.ExecuteAsync(request);

            // Assert
            Assert.True(result.Success);
            StringAssert.Contains("No recipients for order", result.Message);
            CollectionAssert.Contains(result.Args, 1);
        }

        [Test]
        public async Task ExecuteAsync_WithOrderNotificationMethodSetToNone_ReturnsNoRecipientsForOrder()
        {
            // Arrange
            var sut = A.SendOrderNotificationUseCaseBuilder
                       .W(A.OrderRepository
                           .W(A.OrderEntity
                               .W((CustomerEmail)"floris@crave-emenu.com")
                               .W((CustomerPhoneNumber)"0031612345678")
                               .W(A.OutletEntity
                                   .W(A.OutletSellerInformationEntity))
                               .W(A.CheckoutMethodEntity
                                   .W(OrderNotificationMethod.None))
                               .W(A.ServiceMethodEntity)))
                       .Build();

            var request = new SendOrderNotificationRequest(1, OrderNotificationType.OrderConfirmation);

            // Act
            var result = await sut.ExecuteAsync(request);

            // Assert
            Assert.True(result.Success);
            StringAssert.Contains("No recipients for order", result.Message);
            CollectionAssert.Contains(result.Args, 1);
        }

        [Test]
        public async Task ExecuteAsync_WithOrderNotificationMethodSetToEmail_EmailFails_ReturnsFailedToSendNotification()
        {
            // Arrange
            var sut = A.SendOrderNotificationUseCaseBuilder
                       .W(A.OrderRepository
                           .W(A.OrderEntity
                               .W((CustomerEmail)"floris@crave-emenu.com")
                               .W(A.OutletEntity
                                   .W(A.OutletSellerInformationEntity))
                               .W(A.CheckoutMethodEntity
                                   .W(OrderNotificationMethod.Email))
                               .W(A.ServiceMethodEntity)
                               .W(A.OrderitemEntity)))
                       .W(A.SendNotificationUseCaseBuilder
                           .W(A.SendMailUseCaseBuilder
                               .W(A.MailClientMockBuilder
                                   .W(false))))
                       .Build();

            var request = new SendOrderNotificationRequest(1, OrderNotificationType.OrderConfirmation);

            // Act
            var result = await sut.ExecuteAsync(request);

            // Assert
            Assert.False(result.Success);

            StringAssert.Contains("Failed to send notifications for order", result.Message);
            CollectionAssert.Contains(result.Args, 1);
            CollectionAssert.Contains(result.Args, "OrderConfirmation");
            StringAssert.Contains("Failed: Method=Email, Receivers=floris@crave-emenu.com, Error=", result.Args[2].ToString());
        }

        [Test]
        public async Task ExecuteAsync_WithOrderNotificationMethodSetToEmail_EmailSuccess_ReturnsSuccessEmptyMessage()
        {
            // Arrange
            var sut = A.SendOrderNotificationUseCaseBuilder
                       .W(A.OrderRepository
                           .W(A.OrderEntity
                               .W((CustomerEmail)"floris@crave-emenu.com")
                               .W(A.OutletEntity
                                   .W(A.OutletSellerInformationEntity))
                               .W(A.CheckoutMethodEntity
                                   .W(OrderNotificationMethod.Email))
                               .W(A.ServiceMethodEntity)
                               .W(A.OrderitemEntity)))
                       .W(A.SendNotificationUseCaseBuilder
                           .W(A.SendMailUseCaseBuilder
                               .W(A.MailClientMockBuilder
                                   .W(true))))
                       .Build();

            var request = new SendOrderNotificationRequest(1, OrderNotificationType.OrderConfirmation);

            // Act
            var result = await sut.ExecuteAsync(request);

            // Assert
            Assert.True(result.Success);
            Assert.IsEmpty(result.Message);
        }

        [Test]
        public async Task ExecuteAsync_WithOrderNotificationMethodSetToSms_SmsFails_ReturnsFailedToSendNotification()
        {
            // Arrange
            var sut = A.SendOrderNotificationUseCaseBuilder
                       .W(A.OrderRepository
                           .W(A.OrderEntity
                               .W((CustomerPhoneNumber)"0031612345678")
                               .W(A.OutletEntity
                                   .W(A.OutletSellerInformationEntity))
                               .W(A.CheckoutMethodEntity
                                   .W(OrderNotificationMethod.SMS))
                               .W(A.ServiceMethodEntity)
                               .W(A.OrderitemEntity)))
                       .W(A.SendNotificationUseCaseBuilder
                           .W(A.SendTextMessageUseCaseBuilder
                               .W(A.SmsProviderMockBuilder
                                   .W(A.MessageResultMockBuilder
                                       .W("dummy-reference")
                                       .W(new List<Recipient>()
                                       {
                                           new Recipient
                                           {
                                               Number = 0031612345678,
                                               Sent = false
                                           }
                                       })))))
                       .Build();

            var request = new SendOrderNotificationRequest(1, OrderNotificationType.OrderConfirmation);

            // Act
            var result = await sut.ExecuteAsync(request);

            // Assert
            Assert.False(result.Success);

            StringAssert.Contains("Failed to send notifications for order", result.Message);
            CollectionAssert.Contains(result.Args, 1);
            CollectionAssert.Contains(result.Args, "OrderConfirmation");
            StringAssert.Contains("Failed: Method=SMS, Receivers=0031612345678, Error=Failed to send message dummy-reference to number 31612345678", result.Args[2].ToString());
        }

        [Test]
        public async Task ExecuteAsync_WithOrderNotificationMethodSetToSms_SmsSuccess_ReturnsSuccessEmptyMessage()
        {
            // Arrange
            var sut = A.SendOrderNotificationUseCaseBuilder
                       .W(A.OrderRepository
                           .W(A.OrderEntity
                               .W((CustomerPhoneNumber)"0031612345678")
                               .W(A.OutletEntity
                                   .W(A.OutletSellerInformationEntity))
                               .W(A.CheckoutMethodEntity
                                   .W(OrderNotificationMethod.SMS))
                               .W(A.ServiceMethodEntity)
                               .W(A.OrderitemEntity)))
                       .W(A.SendNotificationUseCaseBuilder
                           .W(A.SendTextMessageUseCaseBuilder
                               .W(A.SmsProviderMockBuilder
                                   .W(A.MessageResultMockBuilder
                                       .W("dummy-reference")
                                       .W(new List<Recipient>()
                                       {
                                           new Recipient
                                           {
                                               Number = 0031612345678,
                                               Sent = true
                                           }
                                       })))))
                       .Build();

            var request = new SendOrderNotificationRequest(1, OrderNotificationType.OrderConfirmation);

            // Act
            var result = await sut.ExecuteAsync(request);

            // Assert
            Assert.True(result.Success);
            Assert.IsEmpty(result.Message);
        }

        [Test]
        public async Task ExecuteAsync_WithOrderNotificationMethodSetToEmailSms_ReturnsSuccessEmptyMessage()
        {
            // Arrange
            var createOrderNotificationLogUseCaseBuilder = A.CreateOrderNotificationLogUseCaseBuilder;

            var sut = A.SendOrderNotificationUseCaseBuilder
                       .W(createOrderNotificationLogUseCaseBuilder)
                       .W(A.OrderRepository
                           .W(A.OrderEntity
                               .W((CustomerEmail)"floris@crave-emenu.com")
                               .W((CustomerPhoneNumber)"0031612345678")
                               .W(A.OutletEntity
                                   .W(A.OutletSellerInformationEntity))
                               .W(A.CheckoutMethodEntity
                                   .W(OrderNotificationMethod.EmailSMS))
                               .W(A.ServiceMethodEntity)
                               .W(A.OrderitemEntity)))
                       .W(A.SendNotificationUseCaseBuilder
                           .W(A.SendMailUseCaseBuilder
                               .W(A.MailClientMockBuilder
                                   .W(true)))
                           .W(A.SendTextMessageUseCaseBuilder
                               .W(A.SmsProviderMockBuilder
                                   .W(A.MessageResultMockBuilder
                                       .W("dummy-reference")
                                       .W(new List<Recipient>()
                                       {
                                           new Recipient
                                           {
                                               Number = 0031612345678,
                                               Sent = true
                                           }
                                       })))))
                       .Build();

            var request = new SendOrderNotificationRequest(1, OrderNotificationType.OrderConfirmation);

            // Act
            var result = await sut.ExecuteAsync(request);

            // Assert
            Assert.True(result.Success);
            Assert.IsEmpty(result.Message);
        }

        [Test]
        public async Task Execute_WithAdditionalEmailReceiversOnly_ReturnsSuccessEmptyMessage()
        {
            // Arrange
            var createOrderNotificationLogUseCaseBuilder = A.CreateOrderNotificationLogUseCaseBuilder;

            var sut = A.SendOrderNotificationUseCaseBuilder
                       .W(createOrderNotificationLogUseCaseBuilder)
                       .W(A.OrderRepository
                           .W(A.OrderEntity
                               .W((CustomerEmail)"floris@crave-emenu.com")
                               .W((CustomerPhoneNumber)"0031612345678")
                               .W(A.OutletEntity
                                   .W(A.OutletSellerInformationEntity))
                               .W(A.CheckoutMethodEntity
                                   .W(OrderNotificationMethod.EmailSMS))
                               .W(A.ServiceMethodEntity)
                               .W(A.OrderitemEntity)))
                       .Build();

            var request = new SendOrderNotificationRequest(1, OrderNotificationType.OrderConfirmation)
            {
                SendToOriginalReceiver = false,
                AdditionalEmailReceivers = "floris-additional@crave-emenu.com, otto-additional@crave-emenu.com"
            };

            // Act
            var result = await sut.ExecuteAsync(request);

            // Assert
            Assert.True(result.Success);
            Assert.IsEmpty(result.Message);
        }
    }
}
