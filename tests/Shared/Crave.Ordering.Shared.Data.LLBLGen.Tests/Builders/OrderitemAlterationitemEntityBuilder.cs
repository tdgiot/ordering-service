﻿using AutoFixture;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Ordering.Shared.Data.LLBLGen.Tests.Builders
{
    public class OrderitemAlterationitemEntityBuilder : EntityBuilder<OrderitemAlterationitemEntity, OrderitemAlterationitemEntityBuilder.OrderitemAlterationitemEntityData, OrderitemAlterationitemEntityBuilder>
    {
        public OrderitemAlterationitemEntityBuilder() : base(Initialise())
        {
        }

        private OrderitemAlterationitemEntityBuilder(bool isNew) : base(Initialise(), isNew)
        {
        }

        private OrderitemAlterationitemEntityBuilder(OrderitemAlterationitemEntityData orderitemAlterationitemEntityData, bool isNew) : base(orderitemAlterationitemEntityData, isNew)
        {
        }

        public override OrderitemAlterationitemEntityBuilder AsNew() => new OrderitemAlterationitemEntityBuilder(true);

        public OrderitemAlterationitemEntityBuilder W(int alterationoptionId) => new OrderitemAlterationitemEntityBuilder(new OrderitemAlterationitemEntityData(EntityData)
        {
            AlterationoptionId = alterationoptionId
        }, IsNew);

        public OrderitemAlterationitemEntityBuilder W(AlterationoptionPriceIn alterationoptionPriceIn) => new OrderitemAlterationitemEntityBuilder(new OrderitemAlterationitemEntityData(EntityData)
        {
            AlterationoptionPriceIn = alterationoptionPriceIn
        }, IsNew);

        public OrderitemAlterationitemEntityBuilder W(bool orderLevelEnabled) => new OrderitemAlterationitemEntityBuilder(new OrderitemAlterationitemEntityData(EntityData)
        {
            OrderLevelEnabled = orderLevelEnabled
        }, IsNew);

        public OrderitemAlterationitemEntityBuilder W(string alterationName, string alterationoptionName) => new OrderitemAlterationitemEntityBuilder(new OrderitemAlterationitemEntityData(EntityData)
        {
            AlterationName = alterationName,
            AlterationoptionName = alterationoptionName,
        }, IsNew);

        public OrderitemAlterationitemEntityBuilder W(int? alterationId) => new OrderitemAlterationitemEntityBuilder(new OrderitemAlterationitemEntityData(EntityData)
        {
            AlterationId = alterationId
        }, IsNew);

        public OrderitemAlterationitemEntityBuilder W(string value) => new OrderitemAlterationitemEntityBuilder(new OrderitemAlterationitemEntityData(EntityData)
        {
            Value = value
        }, IsNew);

        public OrderitemAlterationitemEntityBuilder WithId(int orderitemAlterationitemId) => new OrderitemAlterationitemEntityBuilder(new OrderitemAlterationitemEntityData(EntityData)
        {
            OrderitemAlterationitemId = orderitemAlterationitemId
        }, IsNew);

        private new static OrderitemAlterationitemEntityData Initialise()
        {
            var fixture = new Fixture();
            fixture.Register(() => (AlterationoptionPriceIn)0M);
            return fixture.Create<OrderitemAlterationitemEntityData>();
        }

        protected override void EnrichEntity(OrderitemAlterationitemEntity entity, OrderitemAlterationitemEntityData entityData)
        {
            entity.OrderitemAlterationitemId = entityData.OrderitemAlterationitemId;
            entity.AlterationoptionId = entityData.AlterationoptionId;
            entity.AlterationoptionPriceIn = entityData.AlterationoptionPriceIn;
            entity.OrderLevelEnabled = entityData.OrderLevelEnabled;
            entity.AlterationName = entityData.AlterationName;
            entity.AlterationoptionName = entityData.AlterationoptionName;
            entity.AlterationId = entityData.AlterationId;
            entity.Value = entityData.Value;
        }

        public class OrderitemAlterationitemEntityData
        {
            public OrderitemAlterationitemEntityData() { }

            public OrderitemAlterationitemEntityData(OrderitemAlterationitemEntityData entityData)
            {
                OrderitemAlterationitemId = entityData.OrderitemAlterationitemId;
                AlterationoptionId = entityData.AlterationoptionId;
                AlterationoptionPriceIn = entityData.AlterationoptionPriceIn;
                OrderLevelEnabled = entityData.OrderLevelEnabled;
                AlterationName = entityData.AlterationName;
                AlterationoptionName = entityData.AlterationoptionName;
                AlterationId = entityData.AlterationId;
                Value = entityData.Value;
            }

            public int AlterationoptionId { get; set; }
            public AlterationoptionPriceIn AlterationoptionPriceIn { get; set; }
            public bool OrderLevelEnabled { get; set; }
            public string AlterationName { get; set; }
            public string AlterationoptionName { get; set; }
            public int? AlterationId { get; set; }
            public int OrderitemAlterationitemId { get; set; }
            public string Value { get; set; }
        }

        public class AlterationoptionPriceIn
        {
            private decimal Value { get; }

            private AlterationoptionPriceIn(decimal value) => Value = value;

            public static implicit operator decimal(AlterationoptionPriceIn alterationoptionPriceIn) => alterationoptionPriceIn.Value;

            public static implicit operator AlterationoptionPriceIn(decimal value) => new AlterationoptionPriceIn(value);
        }
    }
}