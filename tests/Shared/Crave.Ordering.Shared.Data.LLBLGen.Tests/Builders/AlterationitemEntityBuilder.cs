﻿using AutoFixture;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.Tests.Builders;

namespace Crave.Ordering.Api.Domain.Tests.Builders.EntityBuilders
{
	public class AlterationitemEntityBuilder : EntityBuilder<AlterationitemEntity, AlterationitemEntityBuilder.AlterationitemEntityData, AlterationitemEntityBuilder>
	{
		public AlterationitemEntityBuilder() : base(AlterationitemEntityBuilder.Initialise())
		{
		}

		private AlterationitemEntityBuilder(bool isNew) : base(AlterationitemEntityBuilder.Initialise(), isNew)
		{
		}

        private AlterationitemEntityBuilder(AlterationitemEntityData alterationitemEntityData, bool isNew) : base(alterationitemEntityData, isNew)
        {
        }

		private new static AlterationitemEntityData Initialise()
		{
			var fixture = new Fixture();
			fixture.Register(new AlterationoptionEntityBuilder().Build);
			return fixture.Create<AlterationitemEntityData>();
		}

        public AlterationitemEntityBuilder WithAlterationId(int alterationId) => new AlterationitemEntityBuilder(new AlterationitemEntityData(this.EntityData)
        {
            AlterationId = alterationId
        }, this.IsNew);

        public AlterationitemEntityBuilder WithAlterationoptionId(int alterationoptionId) => new AlterationitemEntityBuilder(new AlterationitemEntityData(this.EntityData)
        {
            AlterationoptionId = alterationoptionId
        }, this.IsNew);

		public AlterationitemEntityBuilder WithAlterationoption(AlterationoptionEntity alterationoptionEntity) => new AlterationitemEntityBuilder(new AlterationitemEntityData(this.EntityData)
        {
			Alterationoption = alterationoptionEntity
        }, this.IsNew);

		public override AlterationitemEntityBuilder AsNew() => new AlterationitemEntityBuilder(true);

		protected override void EnrichEntity(AlterationitemEntity entity, AlterationitemEntityData entityData)
		{
			entity.AlterationitemId = entityData.AlterationitemId;
            entity.AlterationId = entityData.AlterationId;
            entity.AlterationoptionId = entityData.AlterationoptionId;
			entity.Alterationoption = entityData.Alterationoption;
        }

		public class AlterationitemEntityData
		{
            public AlterationitemEntityData()
            {
            }

            public AlterationitemEntityData(AlterationitemEntityData data)
            {
                AlterationitemId = data.AlterationitemId;
                AlterationId = data.AlterationId;
                AlterationoptionId = data.AlterationoptionId;
                Alterationoption = data.Alterationoption;
            }

			public int AlterationitemId { get; set; }
            public int AlterationId { get; set; }
			public int AlterationoptionId { get; set; }
            public AlterationoptionEntity Alterationoption { get; set; }
		}
	}
}