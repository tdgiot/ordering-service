﻿using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.Tests.Builders;

namespace Crave.Ordering.Api.Domain.Tests.Builders.EntityBuilders
{
    public class DeliverypointgroupEntityBuilder : EntityBuilder<DeliverypointgroupEntity, DeliverypointgroupEntityBuilder.DpgData, DeliverypointgroupEntityBuilder>
    {
        public DeliverypointgroupEntityBuilder() : base(DeliverypointgroupEntityBuilder.Initialise())
        {

        }

        public DeliverypointgroupEntityBuilder(bool isNew) : base(DeliverypointgroupEntityBuilder.Initialise(), isNew)
        {

        }

        private DeliverypointgroupEntityBuilder(DpgData data) : base(data)
        {

        }

        public override DeliverypointgroupEntityBuilder AsNew() => new DeliverypointgroupEntityBuilder(true);

        public DeliverypointgroupEntityBuilder WithId(int id) => new DeliverypointgroupEntityBuilder(new DpgData(EntityData)
        {
            DeliverypointgroupId = id
        });

        public DeliverypointgroupEntityBuilder WithName(string name) => new DeliverypointgroupEntityBuilder(new DpgData(EntityData)
        {
            Name = name
        });

        protected override void EnrichEntity(DeliverypointgroupEntity entity, DpgData entityData)
        {
            entity.DeliverypointgroupId = entityData.DeliverypointgroupId;
            entity.Name = entityData.Name;
        }

        public class DpgData
        {
            public DpgData()
            {

            }

            public DpgData(DpgData old)
            {
                this.DeliverypointgroupId = old.DeliverypointgroupId;
                this.Name = old.Name;
            }

            public int DeliverypointgroupId { get; set; }
            public string Name { get; set; }
        }
        }
}