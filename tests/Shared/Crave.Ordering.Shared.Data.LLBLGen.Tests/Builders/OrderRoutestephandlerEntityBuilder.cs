﻿using AutoFixture;
using AutoFixture.Dsl;
using Crave.Enums;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Ordering.Shared.Data.LLBLGen.Tests.Builders
{
    public class OrderRoutestephandlerEntityBuilder : EntityBuilder<OrderRoutestephandlerEntity, OrderRoutestephandlerEntityBuilder.RoutestepEntityData, OrderRoutestephandlerEntityBuilder>
    {
        public OrderRoutestephandlerEntityBuilder() : base(Initialise())
        {
        }

        public OrderRoutestephandlerEntityBuilder(bool isNew) : base(Initialise(), isNew)
        {
        }

        private OrderRoutestephandlerEntityBuilder(RoutestepEntityData entityData) : base(entityData)
        {
        }

        private new static RoutestepEntityData Initialise()
        {
            ICustomizationComposer<RoutestepEntityData> entityData = new Fixture().Build<RoutestepEntityData>();
            entityData.With(x => x.OrderId, 1);
            entityData.With(x => x.OrderId, 1);
            entityData.With(x => x.Number, 1);
            entityData.With(x => x.Status, OrderRoutestephandlerStatus.WaitingForPreviousStep);
            entityData.With(x => x.HandlerType, RoutestephandlerType.Console);

            return entityData.Create();
        }

        public OrderRoutestephandlerEntityBuilder WithId(int id) => new OrderRoutestephandlerEntityBuilder(new RoutestepEntityData(EntityData)
        {
            Id = id
        });

        public OrderRoutestephandlerEntityBuilder WithOrderId(int orderId) => new OrderRoutestephandlerEntityBuilder(new RoutestepEntityData(EntityData)
        {
            OrderId = orderId
        });

        public OrderRoutestephandlerEntityBuilder WithNumber(int number) => new OrderRoutestephandlerEntityBuilder(new RoutestepEntityData(EntityData)
        {
            Number = number
        });

        public OrderRoutestephandlerEntityBuilder WithStatus(OrderRoutestephandlerStatus status) => new OrderRoutestephandlerEntityBuilder(new RoutestepEntityData(EntityData)
        {
            Status = status
        });

        public OrderRoutestephandlerEntityBuilder WithHandlerType(RoutestephandlerType type) => new OrderRoutestephandlerEntityBuilder(new RoutestepEntityData(EntityData)
        {
            HandlerType = type
        });

        public class RoutestepEntityData
        {
            public RoutestepEntityData()
            {
            }

            public RoutestepEntityData(RoutestepEntityData entityData)
            {
                Id = entityData.Id;
                OrderId = entityData.OrderId;
                Number = entityData.Number;
                Status = entityData.Status;
                HandlerType = entityData.HandlerType;
            }

            public int Id { get; set; }
            public int OrderId { get; set; }
            public int Number { get; set; }
            public OrderRoutestephandlerStatus Status { get; set; }
            public RoutestephandlerType HandlerType { get; set; }
        }

        public override OrderRoutestephandlerEntityBuilder AsNew() => new OrderRoutestephandlerEntityBuilder();

        protected override void EnrichEntity(OrderRoutestephandlerEntity entity, RoutestepEntityData entityData)
        {
            entity.OrderRoutestephandlerId = entityData.Id;
            entity.OrderId = entityData.OrderId;
            entity.Number = entityData.Number;
            entity.Status = entityData.Status;
            entity.HandlerType = entityData.HandlerType;
        }
    }
}