﻿using AutoFixture;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using System.Collections.Generic;

namespace Crave.Ordering.Shared.Data.LLBLGen.Tests.Builders
{
    public class RoutestephandlerEntityBuilder : EntityBuilder<RoutestephandlerEntity, RoutestephandlerEntityBuilder.RoutestephandlerEntityData, RoutestephandlerEntityBuilder>
    {
        public RoutestephandlerEntityBuilder() : base(Initialise(), true)
        {
        }

        private RoutestephandlerEntityBuilder(bool isNew) : base(Initialise(), isNew)
        {
        }

        private RoutestephandlerEntityBuilder(RoutestephandlerEntityData routestephandlerEntityData, bool isNew) : base(routestephandlerEntityData, isNew)
        {
        }

        public RoutestephandlerEntityBuilder W(int routestephandlerId) => new RoutestephandlerEntityBuilder(new RoutestephandlerEntityData(EntityData)
        {
            RoutestephandlerId = routestephandlerId
        }, IsNew);

        public RoutestephandlerEntityBuilder W(MediaEntityBuilder mediaEntityBuilder) => new RoutestephandlerEntityBuilder(new RoutestephandlerEntityData(EntityData)
        {
            MediaCollection = new[] { mediaEntityBuilder.Build() }
        }, IsNew);

        private new static RoutestephandlerEntityData Initialise()
        {
            Fixture fixture = new Fixture();
            fixture.Register(() => new MediaEntityBuilder().Build());
            return fixture.Create<RoutestephandlerEntityData>();
        }

        public override RoutestephandlerEntityBuilder AsNew() => new RoutestephandlerEntityBuilder(true);

        protected override void EnrichEntity(RoutestephandlerEntity entity, RoutestephandlerEntityData entityData)
        {
            entity.RoutestephandlerId = entityData.RoutestephandlerId;
            entity.MediaCollection.AddRange(entityData.MediaCollection);
        }

        public class RoutestephandlerEntityData
        {
            public RoutestephandlerEntityData()
            {

            }

            public RoutestephandlerEntityData(RoutestephandlerEntityData old)
            {
                RoutestephandlerId = old.RoutestephandlerId;
                MediaCollection = old.MediaCollection;
            }

            public int RoutestephandlerId { get; set; }
            public IEnumerable<MediaEntity> MediaCollection { get; set; }
        }
    }
}
