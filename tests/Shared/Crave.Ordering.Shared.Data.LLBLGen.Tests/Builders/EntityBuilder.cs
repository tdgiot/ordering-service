﻿using AutoFixture;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Crave.Ordering.Shared.Data.LLBLGen.Tests.Builders
{
    public abstract class EntityBuilder<TEntity, TEntityData, TBuilder>
        where TEntity : EntityCore<IEntityFields2>, new()
        where TBuilder : EntityBuilder<TEntity, TEntityData, TBuilder>

    {
        protected EntityBuilder() : this(false)
        {
        }

        protected EntityBuilder(TEntityData entityData) : this(entityData, false)
        {
        }

        protected EntityBuilder(bool isNew) : this(Initialise(), isNew)
        {
        }

        protected static TEntityData Initialise() => new Fixture().Create<TEntityData>();

        protected EntityBuilder(TEntityData entityData, bool isNew)
        {
            EntityData = entityData;
            IsNew = isNew;
        }

        protected bool IsNew { get; }
        protected TEntityData EntityData { get; }

        public abstract TBuilder AsNew();

        public TEntity Build()
        {
            var entity = new TEntity
            {
                IsNew = IsNew,
            };

            EnrichEntity(entity, EntityData);

            return entity;
        }

        protected abstract void EnrichEntity(TEntity entity, TEntityData entityData);
    }
}