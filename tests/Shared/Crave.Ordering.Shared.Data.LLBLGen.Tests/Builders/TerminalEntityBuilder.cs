﻿using Crave.Enums;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Ordering.Shared.Data.LLBLGen.Tests.Builders
{
    public class TerminalEntityBuilder : EntityBuilder<TerminalEntity, TerminalEntityBuilder.Data, TerminalEntityBuilder>
    {
        public TerminalEntityBuilder() : this(false)
        {
        }

        public TerminalEntityBuilder(bool isNew) : this(Initialise(), isNew)
        {
        }

        public TerminalEntityBuilder(Data entityData, bool isNew) : base(entityData, isNew)
        {
        }

        public override TerminalEntityBuilder AsNew()
        {
            return new TerminalEntityBuilder(EntityData, true);
        }

        public TerminalEntityBuilder WithTerminalId(int id) => new TerminalEntityBuilder(new Data(EntityData)
        {
            TerminalId = id
        }, IsNew);

        public TerminalEntityBuilder WithType(TerminalType type) => new TerminalEntityBuilder(new Data(EntityData)
        {
            Type = type
        }, IsNew);

        protected override void EnrichEntity(TerminalEntity entity, Data entityData)
        {
            entity.TerminalId = entityData.TerminalId;
            entity.Type = entityData.Type;
        }

        public class Data
        {
            public Data() { }

            public Data(Data old)
            {
                TerminalId = old.TerminalId;
                Type = old.Type;
            }

            public int TerminalId { get; set; }
            public TerminalType? Type { get; set; }
        }
    }
}