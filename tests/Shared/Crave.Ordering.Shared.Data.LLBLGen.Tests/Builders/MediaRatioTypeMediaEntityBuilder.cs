﻿using AutoFixture;
using Crave.Enums;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Ordering.Shared.Data.LLBLGen.Tests.Builders
{
    public class MediaRatioTypeMediaEntityBuilder : EntityBuilder<MediaRatioTypeMediaEntity, MediaRatioTypeMediaEntityBuilder.MediaRatioTypeMediaEntityData, MediaRatioTypeMediaEntityBuilder>
    {
        public MediaRatioTypeMediaEntityBuilder() : base(Initialise(), true)
        {
        }

        private MediaRatioTypeMediaEntityBuilder(bool isNew) : base(Initialise(), isNew)
        {
        }

        private MediaRatioTypeMediaEntityBuilder(MediaRatioTypeMediaEntityData mediaEntityData, bool isNew) : base(mediaEntityData, isNew)
        {
        }

        public MediaRatioTypeMediaEntityBuilder W(int mediaRatioTypeMediaId) => new MediaRatioTypeMediaEntityBuilder(new MediaRatioTypeMediaEntityData(EntityData)
        {
            MediaRatioTypeMediaId = mediaRatioTypeMediaId
        }, IsNew);

        public MediaRatioTypeMediaEntityBuilder W(MediaType mediaType) => new MediaRatioTypeMediaEntityBuilder(new MediaRatioTypeMediaEntityData(EntityData)
        {
            MediaType = mediaType
        }, IsNew);

        public MediaRatioTypeMediaEntityBuilder WithTop(int top) => new MediaRatioTypeMediaEntityBuilder(new MediaRatioTypeMediaEntityData(EntityData)
        {
            Top = top
        }, IsNew);

        public MediaRatioTypeMediaEntityBuilder WithLeft(int left) => new MediaRatioTypeMediaEntityBuilder(new MediaRatioTypeMediaEntityData(EntityData)
        {
            Left = left
        }, IsNew);

        public MediaRatioTypeMediaEntityBuilder WithWidth(int width) => new MediaRatioTypeMediaEntityBuilder(new MediaRatioTypeMediaEntityData(EntityData)
        {
            Width = width
        }, IsNew);

        public MediaRatioTypeMediaEntityBuilder WithHeight(int height) => new MediaRatioTypeMediaEntityBuilder(new MediaRatioTypeMediaEntityData(EntityData)
        {
            Height = height
        }, IsNew);

        public MediaRatioTypeMediaEntityBuilder WithTicks(long? ticks) => new MediaRatioTypeMediaEntityBuilder(new MediaRatioTypeMediaEntityData(EntityData)
        {
            LastDistributedVersionTicks = ticks
        }, IsNew);

        private new static MediaRatioTypeMediaEntityData Initialise()
        {
            Fixture fixture = new Fixture();
            return fixture.Create<MediaRatioTypeMediaEntityData>();
        }

        public override MediaRatioTypeMediaEntityBuilder AsNew() => new MediaRatioTypeMediaEntityBuilder(true);

        protected override void EnrichEntity(MediaRatioTypeMediaEntity entity, MediaRatioTypeMediaEntityData entityData)
        {
            entity.MediaRatioTypeMediaId = entityData.MediaRatioTypeMediaId;
            entity.MediaType = entityData.MediaType;
            entity.Top = entityData.Top;
            entity.Left = entityData.Left;
            entity.Height = entityData.Height;
            entity.Width = entityData.Width;
            entity.LastDistributedVersionTicks = entityData.LastDistributedVersionTicks;
        }

        public class MediaRatioTypeMediaEntityData
        {
            public MediaRatioTypeMediaEntityData()
            {

            }

            public MediaRatioTypeMediaEntityData(MediaRatioTypeMediaEntityData old)
            {
                MediaRatioTypeMediaId = old.MediaRatioTypeMediaId;
                MediaType = old.MediaType;
                Top = old.Top;
                Left = old.Left;
                Width = old.Width;
                Height = old.Height;
                LastDistributedVersionTicks = old.LastDistributedVersionTicks;
            }

            public int MediaRatioTypeMediaId { get; set; }
            public MediaType MediaType { get; set; }
            public int Top { get; set; }
            public int Left { get; set; }
            public int Width { get; set; }
            public int Height { get; set; }
            public long? LastDistributedVersionTicks { get; set; }
        }
    }
}
