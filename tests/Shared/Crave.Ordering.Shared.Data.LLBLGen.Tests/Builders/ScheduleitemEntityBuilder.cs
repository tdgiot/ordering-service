﻿using System;
using System.Globalization;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using NodaTime;
using NodaTime.Utility;

namespace Crave.Ordering.Api.Domain.Tests.Builders.EntityBuilders
{
	public class ScheduleitemEntityBuilder
	{
		private DayOfWeek? DayOfWeek { get; }

		private LocalTime TimeStart { get; }

		private LocalTime TimeEnd { get; }

		public ScheduleitemEntityBuilder() : this(DateTime.UtcNow.DayOfWeek, LocalTime.MinValue, LocalTime.MaxValue)
		{

		}

		private ScheduleitemEntityBuilder(DayOfWeek? dayOfWeek, LocalTime timeStart, LocalTime timeEnd)
		{
			DayOfWeek = dayOfWeek;
			TimeStart = timeStart;
			TimeEnd = timeEnd;
		}

		public ScheduleitemEntity Build() => new ScheduleitemEntity
		{
			DayOfWeek = DayOfWeek,
			TimeStart = ScheduleitemEntityBuilder.ConvertLocalTimeToString(TimeStart),
			TimeEnd = ScheduleitemEntityBuilder.ConvertLocalTimeToString(TimeEnd)
		};

		public ScheduleitemEntityBuilder W(IsoDayOfWeek isoDayOfWeek) => new ScheduleitemEntityBuilder(BclConversions.ToDayOfWeek(isoDayOfWeek), TimeStart, TimeEnd);

		public ScheduleitemEntityBuilder W(DayOfWeek? dayOfWeek) => new ScheduleitemEntityBuilder(dayOfWeek, TimeStart, TimeEnd);

		public ScheduleitemEntityBuilder W(LocalTime timeStart, LocalTime timeEnd) => new ScheduleitemEntityBuilder(DayOfWeek, timeStart, timeEnd);

		private static string ConvertLocalTimeToString(LocalTime localTime) => localTime.ToString("HHmm", CultureInfo.CurrentCulture);
	}
}
