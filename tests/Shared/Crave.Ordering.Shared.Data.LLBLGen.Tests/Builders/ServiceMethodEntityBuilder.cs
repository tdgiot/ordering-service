﻿using AutoFixture;
using Crave.Enums;
using Crave.Ordering.Api.Domain.Tests.Builders.EntityBuilders;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.Tests.Structs;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Crave.Ordering.Shared.Data.LLBLGen.Tests.Builders
{
    public class ServiceMethodEntityBuilder : EntityBuilder<ServiceMethodEntity, ServiceMethodEntityBuilder.ServiceMethodEntityData, ServiceMethodEntityBuilder>
    {
        public ServiceMethodEntityBuilder() : base(ServiceMethodEntityBuilder.Initialise())
        {
        }

        private ServiceMethodEntityBuilder(bool isNew) : base(ServiceMethodEntityBuilder.Initialise(), isNew)
        {
        }

        private ServiceMethodEntityBuilder(ServiceMethodEntityData entityData, bool isNew) : base(entityData, isNew)
        {
        }

        public override ServiceMethodEntityBuilder AsNew() => new ServiceMethodEntityBuilder(true);

        private new static ServiceMethodEntityData Initialise()
        {
            Fixture fixture = new Fixture();
            fixture.Register(new OutletEntityBuilder().Build);
            fixture.Register(new DeliveryDistanceEntityBuilder().Build);
            fixture.Register(() => (Active)true);
            fixture.Register(() => (ServiceChargeAmount)null);
            fixture.Register(() => (ServiceChargePercentage)null);
            fixture.Register(() => (MinimumAmountForFreeServiceCharge)0M);
            fixture.Register(() => (MinimumAmountForFreeServiceCharge)0M);
            fixture.Register(() => (OutletId)1);
            return fixture.Create<ServiceMethodEntityData>();
        }

        protected override void EnrichEntity(ServiceMethodEntity entity, ServiceMethodEntityData entityData)
        {
            entity.ServiceMethodId = entityData.ServiceMethodId;
            entity.Name = entityData.ServiceMethodName;
            entity.Type = entityData.ServiceMethodType;
            entity.DeliveryDistanceCollection.AddRange(entityData.DeliveryDistanceEntities);
            entity.Active = entityData.Active;
            entity.ServiceChargeAmount = entityData.ServiceChargeAmount;
            entity.ServiceChargePercentage = entityData.ServiceChargePercentage;
            entity.MinimumAmountForFreeServiceCharge = entityData.MinimumAmountForFreeServiceCharge;
            entity.CoversType = EntityData.CoversType;
            entity.Outlet = entityData.OutletEntity;
        }

        public ServiceMethodEntityBuilder W(ServiceMethodType serviceMethodType) => new ServiceMethodEntityBuilder(new ServiceMethodEntityData
        {
            ServiceMethodId = EntityData.ServiceMethodId,
            ServiceMethodName = EntityData.ServiceMethodName,
            ServiceMethodType = serviceMethodType,
            DeliveryDistanceEntities = EntityData.DeliveryDistanceEntities,
            Active = EntityData.Active,
            ServiceChargeAmount = EntityData.ServiceChargeAmount,
            ServiceChargePercentage = EntityData.ServiceChargePercentage,
            MinimumAmountForFreeServiceCharge = EntityData.MinimumAmountForFreeServiceCharge,
            CoversType = EntityData.CoversType,
            OutletEntity = EntityData.OutletEntity
        }, IsNew);

        public ServiceMethodEntityBuilder W(params DeliveryDistanceEntity[] deliveryDistanceEntities) => new ServiceMethodEntityBuilder(new ServiceMethodEntityData
        {
            ServiceMethodId = EntityData.ServiceMethodId,
            ServiceMethodName = EntityData.ServiceMethodName,
            ServiceMethodType = EntityData.ServiceMethodType,
            DeliveryDistanceEntities = deliveryDistanceEntities,
            Active = EntityData.Active,
            ServiceChargeAmount = EntityData.ServiceChargeAmount,
            ServiceChargePercentage = EntityData.ServiceChargePercentage,
            MinimumAmountForFreeServiceCharge = EntityData.MinimumAmountForFreeServiceCharge,
            CoversType = EntityData.CoversType,
            OutletEntity = EntityData.OutletEntity
        }, IsNew);

        public ServiceMethodEntityBuilder W(bool active) => new ServiceMethodEntityBuilder(new ServiceMethodEntityData
        {
            ServiceMethodId = EntityData.ServiceMethodId,
            ServiceMethodName = EntityData.ServiceMethodName,
            ServiceMethodType = EntityData.ServiceMethodType,
            DeliveryDistanceEntities = EntityData.DeliveryDistanceEntities,
            Active = active,
            ServiceChargeAmount = EntityData.ServiceChargeAmount,
            ServiceChargePercentage = EntityData.ServiceChargePercentage,
            MinimumAmountForFreeServiceCharge = EntityData.MinimumAmountForFreeServiceCharge,
            CoversType = EntityData.CoversType,
            OutletEntity = EntityData.OutletEntity
        }, IsNew);

        public ServiceMethodEntityBuilder W(ServiceChargeAmount serviceChargeAmount) => new ServiceMethodEntityBuilder(new ServiceMethodEntityData
        {
            ServiceMethodId = EntityData.ServiceMethodId,
            ServiceMethodName = EntityData.ServiceMethodName,
            ServiceMethodType = EntityData.ServiceMethodType,
            DeliveryDistanceEntities = EntityData.DeliveryDistanceEntities,
            Active = EntityData.Active,
            ServiceChargeAmount = serviceChargeAmount,
            ServiceChargePercentage = EntityData.ServiceChargePercentage,
            MinimumAmountForFreeServiceCharge = EntityData.MinimumAmountForFreeServiceCharge,
            OutletEntity = EntityData.OutletEntity,
            CoversType = EntityData.CoversType

        }, IsNew);

        public ServiceMethodEntityBuilder W(CoversType coverType) => new ServiceMethodEntityBuilder(new ServiceMethodEntityData
        {
            ServiceMethodId = EntityData.ServiceMethodId,
            ServiceMethodName = EntityData.ServiceMethodName,
            ServiceMethodType = EntityData.ServiceMethodType,
            DeliveryDistanceEntities = EntityData.DeliveryDistanceEntities,
            Active = EntityData.Active,
            ServiceChargeAmount = EntityData.ServiceChargeAmount,
            ServiceChargePercentage = EntityData.ServiceChargePercentage,
            MinimumAmountForFreeServiceCharge = EntityData.MinimumAmountForFreeServiceCharge,
            OutletEntity = EntityData.OutletEntity,
            CoversType = coverType
        }, IsNew);

        public ServiceMethodEntityBuilder W(ServiceChargePercentage serviceChargePercentage) => new ServiceMethodEntityBuilder(new ServiceMethodEntityData
        {
            ServiceMethodId = EntityData.ServiceMethodId,
            ServiceMethodName = EntityData.ServiceMethodName,
            ServiceMethodType = EntityData.ServiceMethodType,
            DeliveryDistanceEntities = EntityData.DeliveryDistanceEntities,
            Active = EntityData.Active,
            ServiceChargeAmount = EntityData.ServiceChargeAmount,
            ServiceChargePercentage = serviceChargePercentage,
            MinimumAmountForFreeServiceCharge = EntityData.MinimumAmountForFreeServiceCharge,
            OutletEntity = EntityData.OutletEntity,
            CoversType = EntityData.CoversType
        }, IsNew);

        public ServiceMethodEntityBuilder W(MinimumAmountForFreeServiceCharge minimumAmountForFreeServiceCharge) => new ServiceMethodEntityBuilder(new ServiceMethodEntityData
        {
            ServiceMethodId = EntityData.ServiceMethodId,
            ServiceMethodName = EntityData.ServiceMethodName,
            ServiceMethodType = EntityData.ServiceMethodType,
            DeliveryDistanceEntities = EntityData.DeliveryDistanceEntities,
            Active = EntityData.Active,
            ServiceChargeAmount = EntityData.ServiceChargeAmount,
            ServiceChargePercentage = EntityData.ServiceChargePercentage,
            MinimumAmountForFreeServiceCharge = minimumAmountForFreeServiceCharge,
            OutletEntity = EntityData.OutletEntity,
            CoversType = EntityData.CoversType
        }, IsNew);

        public class ServiceMethodEntityData
        {
            public int ServiceMethodId { get; set; }

            [StringLength(40)] public string ServiceMethodName { get; set; }

            public ServiceMethodType ServiceMethodType { get; set; }

            public IEnumerable<DeliveryDistanceEntity> DeliveryDistanceEntities { get; set; }

            public Active Active { get; set; }

            public ServiceChargeAmount ServiceChargeAmount { get; set; }

            public ServiceChargePercentage ServiceChargePercentage { get; set; }

            public MinimumAmountForFreeServiceCharge MinimumAmountForFreeServiceCharge { get; set; }

            public OutletEntity OutletEntity { get; set; }

            public OutletId OutletId { get; set; }
            public CoversType CoversType { get; internal set; }
        }
    }
}