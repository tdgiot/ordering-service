using AutoFixture;
using Crave.Enums;
using Crave.Ordering.Api.Domain.Tests.Builders.EntityBuilders;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Crave.Ordering.Shared.Data.LLBLGen.Tests.Builders
{
    public class OrderEntityBuilder : EntityBuilder<OrderEntity, OrderEntityBuilder.OrderEntityData, OrderEntityBuilder>
    {
        public OrderEntityBuilder() : base(Initialise())
        {
        }

        public OrderEntityBuilder(bool isNew) : base(Initialise(), isNew)
        {
        }

        private OrderEntityBuilder(OrderEntityData orderEntityData) : base(orderEntityData)
        {
        }

        private new static OrderEntityData Initialise()
        {
            Fixture fixture = new Fixture();
            fixture.Register(() => (OrderId)1);
            fixture.Register(() => (MasterOrderId)null);
            fixture.Register(new OutletEntityBuilder().Build);
            fixture.Register(new OrderitemEntityBuilder().Build);
            fixture.Register(new ServiceMethodEntityBuilder().Build);
            fixture.Register(new DeliveryInformationEntityBuilder().Build);
            fixture.Register(new CheckoutMethodEntityBuilder().Build);
            fixture.Register(new CompanyEntityBuilder().Build);
            fixture.Register(() => (TimeZoneOlsonId)"Europe/Amsterdam");
            fixture.Register(() => (CustomerLastname)fixture.Create<string>());
            fixture.Register(() => (CustomerEmail)fixture.Create<string>());
            fixture.Register(() => (CustomerPhoneNumber)fixture.Create<string>());
            fixture.Register(() => (DeliverypointId)1);
            fixture.Register(() => (ChargeToDeliverypointId)1);
            fixture.Register(() => (PricesIncludeTaxes)true);
            fixture.Register(() => CheckoutType.ChargeToRoom);
            fixture.Register(() => (CultureCode)"nl-NL");
            fixture.Register(() => ServiceMethodType.PickUp);
            fixture.Register(() => (OrderEntity[])null);
            fixture.Register(() => new OrderEntity());
            fixture.Register(() => OrderType.Standard);
            fixture.Register(() => (DeliverypointEntity)null);
            fixture.Register(() => (int?)null);
            fixture.Register(() => (OrderEntity?)null);
            fixture.Register(() => (CurrencyCode)fixture.Create<string>().Substring(0, 3).ToUpper());
            return fixture.Create<OrderEntityData>();
        }

        public OrderEntityBuilder W(OutletEntityBuilder outletEntityBuilder) => new OrderEntityBuilder(new OrderEntityData(EntityData)
        {
            OutletEntity = outletEntityBuilder.Build()
        });

        public OrderEntityBuilder W(OrderitemEntityBuilder orderitemEntityBuilder)
        {
            OrderitemEntity orderitemEntity = orderitemEntityBuilder.Build();
            return new OrderEntityBuilder(new OrderEntityData(EntityData)
            {
                OrderitemEntities = new[] { orderitemEntity }
            });
        }

        public OrderEntityBuilder W(DeliveryInformationEntity deliveryInformationEntity) => new OrderEntityBuilder(new OrderEntityData(EntityData)
        {
            DeliveryInformationEntity = deliveryInformationEntity
        });

        public OrderEntityBuilder W(DeliveryInformationEntityBuilder deliveryInformationEntityBuilder) => W(deliveryInformationEntityBuilder.Build());

        public OrderEntityBuilder W(ServiceMethodEntityBuilder serviceMethodEntityBuilder) => W(serviceMethodEntityBuilder.Build());

        public OrderEntityBuilder W(ServiceMethodEntity serviceMethodEntity) => new OrderEntityBuilder(new OrderEntityData(EntityData)
        {
            ServiceMethodEntity = serviceMethodEntity
        });

        public OrderEntityBuilder W(TimeZoneOlsonId timezoneOlsonId) => new OrderEntityBuilder(new OrderEntityData(EntityData)
        {
            TimeZoneOlsonId = timezoneOlsonId
        });

        public OrderEntityBuilder W(CustomerLastname customerLastname) => new OrderEntityBuilder(new OrderEntityData(EntityData)
        {
            CustomerLastname = customerLastname
        });

        public OrderEntityBuilder W(CustomerEmail customerEmail) => new OrderEntityBuilder(new OrderEntityData(EntityData)
        {
            CustomerEmail = customerEmail
        });

        public OrderEntityBuilder W(CustomerPhoneNumber customerPhoneNumber) => new OrderEntityBuilder(new OrderEntityData(EntityData)
        {
            CustomerPhoneNumber = customerPhoneNumber
        });

        public OrderEntityBuilder W(OrderitemEntity[] orderitemEntities) => new OrderEntityBuilder(new OrderEntityData(EntityData)
        {
            OrderitemEntities = orderitemEntities
        });

        public OrderEntityBuilder W(int? deliveryPointId) => new OrderEntityBuilder(new OrderEntityData(EntityData)
        {
            DeliverypointId = deliveryPointId
        });

        public OrderEntityBuilder W(CheckoutMethodEntityBuilder checkoutMethodEntityBuilder) => new OrderEntityBuilder(new OrderEntityData(EntityData)
        {
            CheckoutMethod = checkoutMethodEntityBuilder.Build()
        });

        public OrderEntityBuilder W(ChargeToDeliverypointId chargeToDeliverypointId) => new OrderEntityBuilder(new OrderEntityData(EntityData)
        {
            ChargeToDeliverypointId = chargeToDeliverypointId
        });

        public OrderEntityBuilder W(PricesIncludeTaxes pricesIncludeTaxes) => new OrderEntityBuilder(new OrderEntityData(EntityData)
        {
            PricesIncludeTaxes = pricesIncludeTaxes
        });

        public OrderEntityBuilder W(CheckoutType checkoutType) => new OrderEntityBuilder(new OrderEntityData(EntityData)
        {
            CheckoutMethodType = checkoutType
        });

        public OrderEntityBuilder W(OrderStatus orderStatus) => new OrderEntityBuilder(new OrderEntityData(EntityData)
        {
            OrderStatus = orderStatus
        });

        public OrderEntityBuilder W(OrderStatus orderStatus, OrderErrorCode errorCode) => new OrderEntityBuilder(new OrderEntityData(EntityData)
        {
            OrderStatus = orderStatus,
            ErrorCode = errorCode
        });

        public OrderEntityBuilder W(OrderId id) => new OrderEntityBuilder(new OrderEntityData(EntityData)
        {
            OrderId = id
        });

        public OrderEntityBuilder W(string timeZoneOlsonId) => new OrderEntityBuilder(new OrderEntityData(EntityData)
        {
            TimeZoneOlsonId = timeZoneOlsonId
        });

        public OrderEntityBuilder W(ServiceMethodType serviceMethodType) => new OrderEntityBuilder(new OrderEntityData(EntityData)
        {
            ServiceMethodType = serviceMethodType
        });

        public OrderEntityBuilder W(MasterOrderId id, OrderEntity masterOrderEntity) => new OrderEntityBuilder(new OrderEntityData(EntityData)
        {
            MasterOrderId = id,
            MasterOrderEntity = masterOrderEntity
        });

        public OrderEntityBuilder W(OptInStatus optInStatus) => new OrderEntityBuilder(new OrderEntityData(EntityData)
        {
            OptInStatus = optInStatus
        });

        public OrderEntityBuilder W(decimal total) => new OrderEntityBuilder(new OrderEntityData(EntityData)
        {
            Total = total
        });

        public OrderEntityBuilder W(OrderType orderType) => new OrderEntityBuilder(new OrderEntityData(EntityData)
        {
            OrderType = orderType
        });

        public OrderEntityBuilder WithCompanyId(int id) => new OrderEntityBuilder(new OrderEntityData(EntityData)
        {
            CompanyId = id
        });


        public OrderEntityBuilder WithCompany(CompanyEntityBuilder entity) => new OrderEntityBuilder(new OrderEntityData(EntityData)
        {
            CompanyEntity = entity.Build()
        });

        public OrderEntityBuilder WithClientId(int? id) => new OrderEntityBuilder(new OrderEntityData(EntityData)
        {
            ClientId = id
        });

        public OrderEntityBuilder WithDeliverypoint(DeliverypointEntityBuilder entity) => new OrderEntityBuilder(new OrderEntityData(EntityData)
        {
            DeliverypointEntity = entity.Build()
        });

        public OrderEntityBuilder AsMasterOrder(OrderEntity[] subOrders) => new OrderEntityBuilder(new OrderEntityData(EntityData)
        {
            SubOrders = subOrders
        });

        public OrderEntityBuilder WithNotes(string notes) => new OrderEntityBuilder(new OrderEntityData(EntityData)
        {
            Notes = notes
        });

        public OrderEntityBuilder WithGuid(string guid) => new OrderEntityBuilder(new OrderEntityData(EntityData)
        {
            Guid = guid
        });

        public OrderEntityBuilder WithCheckoutMethodName(string checkoutMethodName) => new OrderEntityBuilder(new OrderEntityData(EntityData)
        {
            CheckoutMethodName = checkoutMethodName
        });

        public OrderEntityBuilder WithCurrencyCode(CurrencyCode currencyCode) => new OrderEntityBuilder(new OrderEntityData(EntityData)
        {
            CurrencyCode = currencyCode
        });

        public OrderEntityBuilder WithNumberOfCustomers(int? numberOfCustomers) => new OrderEntityBuilder(new OrderEntityData(EntityData)
        {
            NumberOfCustomers = numberOfCustomers
        });

        public OrderEntityBuilder ClearOrderitems()
        {
            OrderEntityData entityData = new OrderEntityData(EntityData);
            entityData.OrderitemEntities = new List<OrderitemEntity>();
            return new OrderEntityBuilder(entityData);
        }

        public OrderEntityBuilder W(OrderitemEntity orderitem)
        {
            OrderEntityData entityData = new OrderEntityData(EntityData);

            List<OrderitemEntity> items = new List<OrderitemEntity>(entityData.OrderitemEntities);
            items.Add(orderitem);

            entityData.OrderitemEntities = items;

            return new OrderEntityBuilder(entityData);
        }

        public override OrderEntityBuilder AsNew() => new OrderEntityBuilder(true);

        protected override void EnrichEntity(OrderEntity entity, OrderEntityData entityData)
        {
            entity.OrderId = entityData.OrderId;
            entity.Outlet = entityData.OutletEntity;
            entity.OrderitemCollection.AddRange(entityData.OrderitemEntities);
            entity.PricesIncludeTaxes = entityData.PricesIncludeTaxes;
            entity.DeliveryInformation = entityData.DeliveryInformationEntity;
            entity.ServiceMethod = entityData.ServiceMethodEntity;
            entity.TimeZoneOlsonId = entityData.TimeZoneOlsonId;
            entity.CustomerLastname = entityData.CustomerLastname;
            entity.Email = entityData.CustomerEmail;
            entity.CustomerPhonenumber = entityData.CustomerPhoneNumber;
            entity.DeliverypointId = entityData.DeliverypointId;
            entity.CheckoutMethod = entityData.CheckoutMethod;
            entity.ChargeToDeliverypointId = entityData.ChargeToDeliverypointId;
            entity.CheckoutMethodType = entityData.CheckoutMethodType;
            entity.CultureCode = entityData.CultureCode;
            entity.Company = entityData.CompanyEntity;
            entity.ServiceMethodType = entityData.ServiceMethodType;
            entity.Status = entityData.OrderStatus;
            entity.ErrorCode = entityData.ErrorCode;
            entity.MasterOrderId = entityData.MasterOrderId;
            entity.MasterOrder = entityData.MasterOrderEntity;
            entity.OptInStatus = entityData.OptInStatus;
            entity.Total = entityData.Total;
            entity.Type = entityData.OrderType;
            entity.ClientId = entityData.ClientId;
            entity.CompanyId = entityData.CompanyId;
            entity.Notes = entityData.Notes;
            entity.Deliverypoint = entityData.DeliverypointEntity;
            entity.Guid = entityData.Guid;
            entity.CurrencyCode = entityData.CurrencyCode;
            entity.CheckoutMethodName = entityData.CheckoutMethodName;
            entity.CoversCount = entityData.NumberOfCustomers;

            entity.MasterOrder?.OrderCollection.Add(entity);

            if (entityData.SubOrders != null)
            {
                entity.OrderCollection.AddRange(entityData.SubOrders);
            }
        }

        public class OrderEntityData
        {
            public OrderEntityData()
            {
            }

            public OrderEntityData(OrderEntityData entityData)
            {
                OrderId = entityData.OrderId;
                OutletEntity = entityData.OutletEntity;
                PricesIncludeTaxes = entityData.PricesIncludeTaxes;
                OrderitemEntities = entityData.OrderitemEntities;
                DeliveryInformationEntity = entityData.DeliveryInformationEntity;
                ServiceMethodEntity = entityData.ServiceMethodEntity;
                TimeZoneOlsonId = entityData.TimeZoneOlsonId;
                CustomerLastname = entityData.CustomerLastname;
                CustomerEmail = entityData.CustomerEmail;
                CustomerPhoneNumber = entityData.CustomerPhoneNumber;
                DeliverypointId = entityData.DeliverypointId;
                CheckoutMethod = entityData.CheckoutMethod;
                ChargeToDeliverypointId = entityData.ChargeToDeliverypointId;
                CheckoutMethodType = entityData.CheckoutMethodType;
                CultureCode = entityData.CultureCode;
                CompanyEntity = entityData.CompanyEntity;
                OrderStatus = entityData.OrderStatus;
                ErrorCode = entityData.ErrorCode;
                MasterOrderId = entityData.MasterOrderId;
                SubOrders = entityData.SubOrders;
                TimeZoneOlsonId = entityData.TimeZoneOlsonId;
                OptInStatus = entityData.OptInStatus;
                Total = entityData.Total;
                OrderType = entityData.OrderType;
                ClientId = entityData.ClientId;
                CompanyId = entityData.CompanyId;
                Notes = entityData.Notes;
                DeliverypointEntity = entityData.DeliverypointEntity;
                ServiceMethodType = entityData.ServiceMethodType;
                MasterOrderEntity = entityData.MasterOrderEntity;
                Guid = entityData.Guid;
                CurrencyCode = entityData.CurrencyCode;
                CheckoutMethodName = entityData.CheckoutMethodName;
                NumberOfCustomers = entityData.NumberOfCustomers;

            }

            public OrderId OrderId { get; set; }
            public PricesIncludeTaxes PricesIncludeTaxes { get; set; }
            public OutletEntity OutletEntity { get; set; }
            public IEnumerable<OrderitemEntity> OrderitemEntities { get; set; }
            public DeliveryInformationEntity DeliveryInformationEntity { get; set; }
            public ServiceMethodEntity ServiceMethodEntity { get; set; }
            public TimeZoneOlsonId TimeZoneOlsonId { get; set; }
            public CustomerLastname CustomerLastname { get; set; }
            public CustomerEmail CustomerEmail { get; set; }
            public CustomerPhoneNumber CustomerPhoneNumber { get; set; }
            public DeliverypointId DeliverypointId { get; set; }
            public DeliverypointEntity DeliverypointEntity { get; set; }
            public CheckoutMethodEntity CheckoutMethod { get; set; }
            public CheckoutType CheckoutMethodType { get; set; }
            public ChargeToDeliverypointId ChargeToDeliverypointId { get; set; }
            public CultureCode CultureCode { get; set; }
            public CompanyEntity CompanyEntity { get; set; }
            public ServiceMethodType ServiceMethodType { get; set; }
            public OrderStatus OrderStatus { get; set; }
            public OrderErrorCode ErrorCode { get; set; }
            public MasterOrderId MasterOrderId { get; set; }
            public OrderEntity MasterOrderEntity { get; set; }
            public OrderEntity[] SubOrders { get; set; }
            public OptInStatus OptInStatus { get; set; }
            public decimal Total { get; set; }
            public OrderType OrderType { get; set; }
            public int CompanyId { get; set; }
            public int? ClientId { get; set; }
            [StringLength(0)] public string Notes { get; set; }
            public string Guid { get; set; }
            public CurrencyCode CurrencyCode { get; set; }
            public string CheckoutMethodName { get; set; }
            public int? NumberOfCustomers { get; set; }
        }
    }

    public readonly struct MasterOrderId
    {
        private int? Value { get; }

        private MasterOrderId(int? value) => Value = value;

        public static implicit operator int?(MasterOrderId orderId) => orderId.Value;

        public static implicit operator MasterOrderId(int? value) => new MasterOrderId(value);
    }

    public readonly struct OrderId
    {
        private int Value { get; }

        private OrderId(int value) => Value = value;

        public static implicit operator int(OrderId orderId) => orderId.Value;

        public static implicit operator OrderId(int value) => new OrderId(value);
    }

    public readonly struct DeliverypointId
    {
        private int? Value { get; }

        private DeliverypointId(int? value) => Value = value;

        public static implicit operator int?(DeliverypointId chargeToDeliverypointId) => chargeToDeliverypointId.Value;

        public static implicit operator DeliverypointId(int? value) => new DeliverypointId(value);
    }

    public readonly struct ChargeToDeliverypointId
    {
        private int? Value { get; }

        private ChargeToDeliverypointId(int? value) => Value = value;

        public static implicit operator int?(ChargeToDeliverypointId chargeToDeliverypointId) => chargeToDeliverypointId.Value;

        public static implicit operator ChargeToDeliverypointId(int? value) => new ChargeToDeliverypointId(value);
    }

    public class TimeZoneOlsonId
    {
        private TimeZoneOlsonId(string value) => Value = value;
        private string Value { get; }

        public static implicit operator string(TimeZoneOlsonId timeZoneOlsonId) => timeZoneOlsonId.Value;

        public static implicit operator TimeZoneOlsonId(string value) => new TimeZoneOlsonId(value);
    }

    public class CustomerLastname
    {
        private CustomerLastname(string value) => Value = value;
        private string Value { get; }

        public static implicit operator string(CustomerLastname customerLastname) => customerLastname.Value;

        public static implicit operator CustomerLastname(string value) => new CustomerLastname(value);
    }

    public class CustomerEmail
    {
        private CustomerEmail(string value) => Value = value;
        private string Value { get; }

        public static implicit operator string(CustomerEmail customerEmail) => customerEmail.Value;

        public static implicit operator CustomerEmail(string value) => new CustomerEmail(value);
    }

    public class CustomerPhoneNumber
    {
        private CustomerPhoneNumber(string value) => Value = value;
        private string Value { get; }

        public static implicit operator string(CustomerPhoneNumber customerPhoneNumber) => customerPhoneNumber.Value;

        public static implicit operator CustomerPhoneNumber(string value) => new CustomerPhoneNumber(value);
    }

    public class PricesIncludeTaxes
    {
        private PricesIncludeTaxes(bool value) => Value = value;
        private bool Value { get; }

        public static implicit operator bool(PricesIncludeTaxes pricesIncludeTaxes) => pricesIncludeTaxes.Value;

        public static implicit operator PricesIncludeTaxes(bool value) => new PricesIncludeTaxes(value);
    }

    public class CultureCode
    {
        private CultureCode(string value) => Value = value;
        private string Value { get; }

        public static implicit operator string(CultureCode cultureCode) => cultureCode.Value;

        public static implicit operator CultureCode(string value) => new CultureCode(value);
    }

    public class CurrencyCode
    {
        private CurrencyCode(string value) => Value = value;
        private string Value { get; }

        public static implicit operator string(CurrencyCode cultureCode) => cultureCode.Value;

        public static implicit operator CurrencyCode(string value) => new CurrencyCode(value);
    }
}
