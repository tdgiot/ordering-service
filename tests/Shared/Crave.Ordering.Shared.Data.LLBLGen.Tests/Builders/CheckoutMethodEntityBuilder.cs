﻿using System.ComponentModel.DataAnnotations;
using AutoFixture;
using Crave.Enums;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.Tests.Structs;

namespace Crave.Ordering.Shared.Data.LLBLGen.Tests.Builders
{
    public class CheckoutMethodEntityBuilder : EntityBuilder<CheckoutMethodEntity, CheckoutMethodEntityBuilder.CheckoutMethodEntityData, CheckoutMethodEntityBuilder>
    {
        public CheckoutMethodEntityBuilder() : base(Initialise())
        {
        }

        public CheckoutMethodEntityBuilder(bool isNew) : base(Initialise(), isNew)
        {
        }

        private CheckoutMethodEntityBuilder(int checkoutMethodId,
                                            string checkoutMethodName,
                                            CheckoutType checkoutType,
                                            int? receiptTemplateId,
                                            Active active,
                                            string currencyCode,
                                            string countyCode,
                                            OrderNotificationMethod orderConfirmationNotificationMethod,
                                            bool isNew) : base(new CheckoutMethodEntityData
                                            {
                                                CheckoutMethodId = checkoutMethodId,
                                                CheckoutMethodName = checkoutMethodName,
                                                CheckoutType = checkoutType,
                                                ReceiptTemplateId = receiptTemplateId,
                                                Active = active,
                                                CurrencyCode = currencyCode,
                                                CountryCode = countyCode,
                                                OrderConfirmationNotificationMethod = orderConfirmationNotificationMethod
                                            }, isNew)
        {
        }

        private new static CheckoutMethodEntityData Initialise()
        {
            Fixture fixture = new Fixture();
            fixture.Register(() => (Active)true);
            fixture.Register(() => OrderNotificationMethod.EmailSMS);

            return fixture
                .Build<CheckoutMethodEntityData>()
                .Without(p => p.CountryCode)
                .Without(p => p.CurrencyCode)
                .Create();
        }

        public override CheckoutMethodEntityBuilder AsNew() => new CheckoutMethodEntityBuilder(true);

        protected override void EnrichEntity(CheckoutMethodEntity entity, CheckoutMethodEntityData entityData)
        {
            entity.CheckoutMethodId = entityData.CheckoutMethodId;
            entity.Name = entityData.CheckoutMethodName;
            entity.CheckoutType = entityData.CheckoutType;
            entity.ReceiptTemplateId = entityData.ReceiptTemplateId;
            entity.Active = entityData.Active;
            entity.CurrencyCode = entityData.CurrencyCode;
            entity.CountryCode = entityData.CountryCode;
            entity.OrderConfirmationNotificationMethod = EntityData.OrderConfirmationNotificationMethod;
        }

        public CheckoutMethodEntityBuilder W(CheckoutType checkoutType) =>
            new CheckoutMethodEntityBuilder(
                EntityData.CheckoutMethodId,
                EntityData.CheckoutMethodName,
                checkoutType,
                EntityData.ReceiptTemplateId,
                EntityData.Active,
                EntityData.CurrencyCode,
                EntityData.CountryCode,
                EntityData.OrderConfirmationNotificationMethod,
                IsNew);

        public CheckoutMethodEntityBuilder W(int? receiptTemplateId) =>
            new CheckoutMethodEntityBuilder(
                EntityData.CheckoutMethodId,
                EntityData.CheckoutMethodName,
                EntityData.CheckoutType,
                receiptTemplateId,
                EntityData.Active,
                EntityData.CurrencyCode,
                EntityData.CountryCode,
                EntityData.OrderConfirmationNotificationMethod,
                IsNew);

        public CheckoutMethodEntityBuilder W(Active active) =>
            new CheckoutMethodEntityBuilder(
                EntityData.CheckoutMethodId,
                EntityData.CheckoutMethodName,
                EntityData.CheckoutType,
                EntityData.ReceiptTemplateId,
                active,
                EntityData.CurrencyCode,
                EntityData.CountryCode,
                EntityData.OrderConfirmationNotificationMethod,
                IsNew);

        public CheckoutMethodEntityBuilder WithCurrencyCode(string currencyCode) =>
            new CheckoutMethodEntityBuilder(
                EntityData.CheckoutMethodId,
                EntityData.CheckoutMethodName,
                EntityData.CheckoutType,
                EntityData.ReceiptTemplateId,
                EntityData.Active,
                currencyCode,
                EntityData.CountryCode,
                EntityData.OrderConfirmationNotificationMethod,
                IsNew);

        public CheckoutMethodEntityBuilder W(string countryCode) =>
            new CheckoutMethodEntityBuilder(
                EntityData.CheckoutMethodId,
                EntityData.CheckoutMethodName,
                EntityData.CheckoutType,
                EntityData.ReceiptTemplateId,
                EntityData.Active,
                EntityData.CurrencyCode,
                countryCode,
                EntityData.OrderConfirmationNotificationMethod,
                IsNew);

        public CheckoutMethodEntityBuilder W(OrderNotificationMethod orderConfirmationNotificationMethod) =>
            new CheckoutMethodEntityBuilder(
                EntityData.CheckoutMethodId,
                EntityData.CheckoutMethodName,
                EntityData.CheckoutType,
                EntityData.ReceiptTemplateId,
                EntityData.Active,
                EntityData.CurrencyCode,
                EntityData.CountryCode,
                orderConfirmationNotificationMethod,
                IsNew);

        public class CheckoutMethodEntityData
        {
            public int CheckoutMethodId { get; set; }
            [StringLength(40)] public string CheckoutMethodName { get; set; }
            public CheckoutType CheckoutType { get; set; }
            public int? ReceiptTemplateId { get; set; }
            [StringLength(3)] public string CountryCode { get; set; }
            [StringLength(3)] public string CurrencyCode { get; set; }
            public Active Active { get; set; }
            public OrderNotificationMethod OrderConfirmationNotificationMethod { get; set; }
        }
    }
}