﻿using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.Tests.Builders;

namespace Crave.Ordering.Api.Domain.Tests.Builders.EntityBuilders
{
    public class DeviceEntityBuilder : EntityBuilder<DeviceEntity, DeviceEntityBuilder.DeviceEntityData, DeviceEntityBuilder>
	{
        public DeviceEntityBuilder() : base(DeviceEntityBuilder.Initialise())
        {
        }

        private DeviceEntityBuilder(bool isNew) : base(DeviceEntityBuilder.Initialise(), isNew)
        {
        }

        private DeviceEntityBuilder(DeviceEntityData deviceEntityData) : base(deviceEntityData)
        {
        }

        public override DeviceEntityBuilder AsNew() => new DeviceEntityBuilder(true);

        private new static DeviceEntityData Initialise() => new DeviceEntityData
        {
            Identifier = "00:00:00:00:00:00"
        };

        protected override void EnrichEntity(DeviceEntity entity, DeviceEntityData entityData)
        {
            entity.Identifier = entityData.Identifier;
        }

        public DeviceEntityBuilder WithIdentifier(string identifier) => new DeviceEntityBuilder(new DeviceEntityData(this.EntityData)
        {
            Identifier = identifier
        });

        public class DeviceEntityData
        {
            public DeviceEntityData()
            {
            }

            public DeviceEntityData(DeviceEntityData entityData)
            {
                this.Identifier = entityData.Identifier;
            }

            public string Identifier { get; set; }
        }
	}
}