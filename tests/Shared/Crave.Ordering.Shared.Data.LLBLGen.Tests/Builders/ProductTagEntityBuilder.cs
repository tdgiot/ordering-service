﻿using AutoFixture;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Ordering.Shared.Data.LLBLGen.Tests.Builders
{
    public class ProductTagEntityBuilder : EntityBuilder<ProductTagEntity, ProductTagEntityBuilder.ProductTagEntityData, ProductTagEntityBuilder>
    {
        public ProductTagEntityBuilder()
            : base(Initialise(), true)
        {
        }

        private ProductTagEntityBuilder(bool isNew)
            : base(Initialise(), isNew)
        {
        }

        private ProductTagEntityBuilder(ProductTagEntityData productEntityData, bool isNew)
            : base(productEntityData, isNew)
        {
        }

        private new static ProductTagEntityData Initialise()
        {
            Fixture fixture = new Fixture();

            return fixture.Create<ProductTagEntityData>();
        }


        public override ProductTagEntityBuilder AsNew() => new ProductTagEntityBuilder(true);

        public ProductTagEntityBuilder WithProductTagId(int productTagId) => new ProductTagEntityBuilder(
            new ProductTagEntityData
            {
                ProductTagId = productTagId,
                ProductId = EntityData.ProductId,
                TagId = EntityData.TagId
            }, IsNew);

        public ProductTagEntityBuilder WithProductId(int productId) => new ProductTagEntityBuilder(
            new ProductTagEntityData
            {
                ProductTagId = EntityData.ProductTagId,
                ProductId = productId,
                TagId = EntityData.TagId
            }, IsNew);

        public ProductTagEntityBuilder WithTagId(int tagId) => new ProductTagEntityBuilder(
            new ProductTagEntityData
            {
                ProductTagId = EntityData.ProductTagId,
                ProductId = EntityData.ProductId,
                TagId = tagId
            }, IsNew);

        protected override void EnrichEntity(ProductTagEntity entity, ProductTagEntityData entityData)
        {
            entity.ProductId = entityData.ProductId;
            entity.TagId = entityData.TagId;
        }

        public class ProductTagEntityData
        {
            public int ProductTagId { get; set; }
            public int ProductId { get; set; }
            public int TagId { get; set; }
        }
    }
}
