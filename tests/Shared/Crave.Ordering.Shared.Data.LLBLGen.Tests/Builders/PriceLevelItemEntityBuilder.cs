﻿using AutoFixture;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Ordering.Shared.Data.LLBLGen.Tests.Builders
{
    public class PriceLevelItemEntityBuilder : EntityBuilder<PriceLevelItemEntity, PriceLevelItemEntityBuilder.PriceLevelItemEntityEntityData, PriceLevelItemEntityBuilder>
    {
        public PriceLevelItemEntityBuilder() : base(Initialise(), true)
        {
        }

        private PriceLevelItemEntityBuilder(bool isNew) : base(Initialise(), isNew)
        {
        }

        private PriceLevelItemEntityBuilder(PriceLevelItemEntityEntityData priceLevelItemEntityEntityData, bool isNew) : base(priceLevelItemEntityEntityData, isNew)
        {
        }

        private new static PriceLevelItemEntityEntityData Initialise()
        {
            Fixture fixture = new Fixture();

            return fixture.Create<PriceLevelItemEntityEntityData>();
        }

        public override PriceLevelItemEntityBuilder AsNew() => new PriceLevelItemEntityBuilder(true);

        public PriceLevelItemEntityBuilder WithPrice(decimal price) => new PriceLevelItemEntityBuilder(new PriceLevelItemEntityEntityData(EntityData)
        {
            Price = price
        }, IsNew);

        protected override void EnrichEntity(PriceLevelItemEntity entity, PriceLevelItemEntityEntityData entityData)
        {
            entity.PriceLevelItemId = entityData.PriceLevelItemId;
            entity.Price = entityData.Price;
        }

        public class PriceLevelItemEntityEntityData
        {
            public PriceLevelItemEntityEntityData()
            {
            }

            public PriceLevelItemEntityEntityData(PriceLevelItemEntityEntityData data)
            {
                PriceLevelItemId = data.PriceLevelItemId;
                Price = data.Price;
            }

            public int PriceLevelItemId { get; set; }
            public decimal Price { get; set; }
        }
    }
}