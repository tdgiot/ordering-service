﻿using Crave.Enums;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Ordering.Shared.Data.LLBLGen.Tests.Builders
{
    public class AlterationEntityBuilder : EntityBuilder<AlterationEntity, AlterationEntityBuilder.AlterationEntityData, AlterationEntityBuilder>
    {
        public AlterationEntityBuilder()
        {
        }

        private AlterationEntityBuilder(bool isNew) : base(isNew)
        {
        }

        private AlterationEntityBuilder(AlterationEntityData entityData, bool isNew) : base(entityData, isNew) { }

        public override AlterationEntityBuilder AsNew() => new AlterationEntityBuilder(true);

        public AlterationEntityBuilder WithId(int id) => new AlterationEntityBuilder(new AlterationEntityData(EntityData)
        {
            AlterationId = id
        }, IsNew);

        public AlterationEntityBuilder WithName(string name) => new AlterationEntityBuilder(new AlterationEntityData(EntityData)
        {
            Name = name
        }, IsNew);

        public AlterationEntityBuilder WithMinMaxOptions(int min, int max) => new AlterationEntityBuilder(new AlterationEntityData(EntityData)
        {
            AlterationType = AlterationType.Options,
            MinOptions = min,
            MaxOptions = max
        }, IsNew);

        public AlterationEntityBuilder WithType(AlterationType type) => new AlterationEntityBuilder(new AlterationEntityData(EntityData)
        {
            AlterationType = type
        }, IsNew);

        public AlterationEntityBuilder AsAvailable(bool isAvailable) => new AlterationEntityBuilder(new AlterationEntityData(EntityData)
        {
            IsAvailable = isAvailable
        }, IsNew);

        public AlterationEntityBuilder AsVisible(bool isVisible) => new AlterationEntityBuilder(new AlterationEntityData(EntityData)
        {
            IsVisible = isVisible
        }, IsNew);

        public AlterationEntityBuilder AsOrderLevelAlteration(bool isOrderLevel) => new AlterationEntityBuilder(new AlterationEntityData(EntityData)
        {
            IsOrderLevelAlteration = isOrderLevel
        }, IsNew);

        protected override void EnrichEntity(AlterationEntity entity, AlterationEntityData entityData)
        {
            entity.AlterationId = entityData.AlterationId;
            entity.Type = entityData.AlterationType;
            entity.OrderLevelEnabled = entityData.OrderLevelEnabled;
            entity.MinOptions = entityData.MinOptions;
            entity.MaxOptions = entityData.MaxOptions;
            entity.Visible = entityData.IsVisible;
            entity.IsAvailable = entityData.IsAvailable;
            entity.OrderLevelEnabled = entityData.OrderLevelEnabled;
            entity.Name = entityData.Name;
        }

        public class AlterationEntityData
        {
            public AlterationEntityData() { }

            public AlterationEntityData(AlterationEntityData old)
            {
                AlterationId = old.AlterationId;
                AlterationType = old.AlterationType;
                OrderLevelEnabled = old.OrderLevelEnabled;
                MinOptions = old.MinOptions;
                MaxOptions = old.MaxOptions;
                IsAvailable = old.IsAvailable;
                IsVisible = old.IsVisible;
                IsOrderLevelAlteration = old.IsOrderLevelAlteration;
                Name = old.Name;
            }

            public int AlterationId { get; set; }
            public AlterationType AlterationType { get; set; }
            public bool OrderLevelEnabled { get; set; }
            public int MinOptions { get; set; } = 0;
            public int MaxOptions { get; set; } = 0;
            public bool IsAvailable { get; set; } = true;
            public bool IsVisible { get; set; } = true;
            public bool IsOrderLevelAlteration { get; set; } = false;
            public string Name { get; set; }
        }
    }
}
