using AutoFixture;
using Crave.Enums;
using Crave.Ordering.Api.Domain.Tests.Builders.EntityBuilders;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Crave.Ordering.Shared.Data.LLBLGen.Tests.Builders
{
    public class CompanyEntityBuilder : EntityBuilder<CompanyEntity, CompanyEntityBuilder.CompanyEntityData, CompanyEntityBuilder>
    {
        public CompanyEntityBuilder() : base(CompanyEntityBuilder.Initialise())
        {
        }

        public CompanyEntityBuilder(bool isNew) : base(CompanyEntityBuilder.Initialise(), isNew)
        {
        }

        private CompanyEntityBuilder(CompanyEntityData companyEntityData) : base(companyEntityData)
        {
        }

        private new static CompanyEntityData Initialise()
        {
            Fixture fixture = new Fixture();
            fixture.Register(() => (Longitude)fixture.Create<double>());
            fixture.Register(() => (Latitude)fixture.Create<double>());
            fixture.Register(() => ClockMode.AmPm);
            fixture.Register(() => new ApplicationConfigurationEntityBuilder().Build());
            fixture.Register(() => new RouteEntityBuilder().Build());
            return fixture.Create<CompanyEntityData>();
        }

        public override CompanyEntityBuilder AsNew() => new CompanyEntityBuilder(true);

        protected override void EnrichEntity(CompanyEntity entity, CompanyEntityData entityData)
        {
            entity.CompanyId = entityData.CompanyId;
            entity.Name = entityData.CompanyName;
            entity.CultureCode = entityData.CultureCode;
            entity.CurrencyCode = entityData.CurrencyCode;
            entity.CountryCode = entityData.CountryCode;
            entity.TimeZoneOlsonId = entityData.TimeZoneOlsonId;
            entity.PricesIncludeTaxes = entityData.PricesIncludeTaxes;
            entity.Longitude = entityData.Longitude;
            entity.Latitude = entityData.Latitude;
            entity.ClockMode = entityData.ClockMode;
            entity.ApplicationConfigurationCollection.AddRange(entityData.ApplicationConfigurationCollection);
            entity.RouteId = entityData.RouteId;
            entity.Route = entityData.Route;
        }

        public CompanyEntityBuilder W(ApplicationConfigurationEntityBuilder applicationConfigurationEntityBuilder) => new CompanyEntityBuilder(new CompanyEntityData(EntityData)
        {
            ApplicationConfigurationCollection = new[] { applicationConfigurationEntityBuilder.Build() }
        });

        public CompanyEntityBuilder W(Latitude latitude) => new CompanyEntityBuilder(new CompanyEntityData(EntityData) { Latitude = latitude });

        public CompanyEntityBuilder W(Longitude longitude) => new CompanyEntityBuilder(new CompanyEntityData(EntityData) { Longitude = longitude });

        public CompanyEntityBuilder WithId(int id) => new CompanyEntityBuilder(new CompanyEntityData(EntityData)
        {
            CompanyId = id
        });

        public CompanyEntityBuilder WithName(string name) => new CompanyEntityBuilder(new CompanyEntityData(EntityData)
        {
            CompanyName = name
        });

        public CompanyEntityBuilder WithCountryCode(string countryCode) => new CompanyEntityBuilder(new CompanyEntityData(EntityData)
        {
            CountryCode = countryCode
        });

        public CompanyEntityBuilder WithCurrencyCode(string currencyCode) => new CompanyEntityBuilder(new CompanyEntityData(EntityData)
        {
            CurrencyCode = currencyCode
        });

        public CompanyEntityBuilder WithRouteId(int id) => new CompanyEntityBuilder(new CompanyEntityData(EntityData)
        {
            RouteId = id
        });

        public CompanyEntityBuilder WithRoute(RouteEntityBuilder route) => new CompanyEntityBuilder(new CompanyEntityData(EntityData)
        {
            Route = route.Build()
        });

        public class CompanyEntityData
        {
            public CompanyEntityData()
            {

            }
            public CompanyEntityData(CompanyEntityData old)
            {
                this.CompanyId = old.CompanyId;
                this.CompanyName = old.CompanyName;
                this.CultureCode = old.CultureCode;
                this.CurrencyCode = old.CurrencyCode;
                this.CountryCode = old.CountryCode;
                this.TimeZoneOlsonId = old.TimeZoneOlsonId;
                this.PricesIncludeTaxes = old.PricesIncludeTaxes;
                this.Longitude = old.Longitude;
                this.Latitude = old.Latitude;
                this.ClockMode = old.ClockMode;
                this.ApplicationConfigurationCollection = old.ApplicationConfigurationCollection;
                this.RouteId = old.RouteId;
                this.Route = old.Route;
            }

            public int CompanyId { get; set; }
            [StringLength(40)] public string CompanyName { get; set; }
            [StringLength(3)] public string CultureCode { get; set; }
            [StringLength(3)] public string CurrencyCode { get; set; }
            [StringLength(3)] public string CountryCode { get; set; }
            [StringLength(40)] public string TimeZoneOlsonId { get; set; }
            public bool PricesIncludeTaxes { get; set; }
            public Longitude Longitude { get; set; }
            public Latitude Latitude { get; set; }
            public ClockMode ClockMode { get; set; }
            public IEnumerable<ApplicationConfigurationEntity> ApplicationConfigurationCollection { get; set; }
            public int RouteId { get; set; }
            public RouteEntity Route { get; set; }

        }
    }
}