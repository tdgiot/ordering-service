﻿using AutoFixture;
using Crave.Enums;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Ordering.Shared.Data.LLBLGen.Tests.Builders
{
    public class ClientConfigurationRouteEntityBuilder : EntityBuilder<ClientConfigurationRouteEntity, ClientConfigurationRouteEntityBuilder.ClientConfigurationRouteData, ClientConfigurationRouteEntityBuilder>
    {
        public ClientConfigurationRouteEntityBuilder() : base(Initialise())
        {
        }

        private ClientConfigurationRouteEntityBuilder(bool isNew) : base(Initialise(), isNew)
        {
        }

        private ClientConfigurationRouteEntityBuilder(ClientConfigurationRouteData configEntityData, bool isNew) : base(configEntityData, isNew)
        {
        }
        private new static ClientConfigurationRouteData Initialise()
        {
            Fixture fixture = new Fixture();
            fixture.Register(() => new RouteEntityBuilder().Build());
            return fixture.Create<ClientConfigurationRouteData>();
        }

        public override ClientConfigurationRouteEntityBuilder AsNew() => new ClientConfigurationRouteEntityBuilder(EntityData, true);

        public ClientConfigurationRouteEntityBuilder WithRoute(RouteEntityBuilder routeEntity) => new ClientConfigurationRouteEntityBuilder(new ClientConfigurationRouteData(EntityData)
        {
            Route = routeEntity.Build()
        }, IsNew);

        public ClientConfigurationRouteEntityBuilder WithType(RouteType type) => new ClientConfigurationRouteEntityBuilder(new ClientConfigurationRouteData(EntityData)
        {
            RouteType = type
        }, IsNew);

        protected override void EnrichEntity(ClientConfigurationRouteEntity entity, ClientConfigurationRouteData entityData)
        {
            entity.Type = entityData.RouteType;
            entity.Route = entityData.Route;
        }

        public class ClientConfigurationRouteData
        {
            public ClientConfigurationRouteData()
            {

            }

            public ClientConfigurationRouteData(ClientConfigurationRouteData old)
            {
                RouteType = old.RouteType;
                Route = old.Route;
            }

            public RouteType RouteType { get; set; }
            public RouteEntity Route { get; set; }
        }

    }
}