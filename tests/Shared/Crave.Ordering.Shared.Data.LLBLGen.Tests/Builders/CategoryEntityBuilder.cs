﻿using System.ComponentModel.DataAnnotations;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Ordering.Shared.Data.LLBLGen.Tests.Builders
{
    public class CategoryEntityBuilder : EntityBuilder<CategoryEntity, CategoryEntityBuilder.CategoryEntityData, CategoryEntityBuilder>
    {
        public CategoryEntityBuilder()
        {
        }

        private CategoryEntityBuilder(bool isNew) : base(isNew)
        {
        }

        private CategoryEntityBuilder(CategoryEntityData data, bool isNew) : base(data, isNew)
        {
        }

        public override CategoryEntityBuilder AsNew() => new CategoryEntityBuilder(true);

        public CategoryEntityBuilder WithRouteId(int routeId) => new CategoryEntityBuilder(new CategoryEntityData(EntityData)
        {
            RouteId = routeId
        }, IsNew);

        protected override void EnrichEntity(CategoryEntity entity, CategoryEntityData entityData)
        {
            entity.CategoryId = entityData.CategoryId;
            entity.Color = entityData.Colour;
            entity.Name = entityData.Name;
            entity.RouteId = entityData.RouteId;
        }

        public class CategoryEntityData
        {
            public CategoryEntityData() { }

            public CategoryEntityData(CategoryEntityData old)
            {
                CategoryId = old.CategoryId;
                Colour = old.Colour;
                Name = old.Name;
                Path = old.Path;
                RouteId = old.RouteId;
            }

            public int CategoryId { get; set; }
            public int Colour { get; set; }
            [StringLength(40)] public string Name { get; set; }
            [StringLength(40)] public string Path { get; set; }
            public int? RouteId { get; set; }
        }
    }
}