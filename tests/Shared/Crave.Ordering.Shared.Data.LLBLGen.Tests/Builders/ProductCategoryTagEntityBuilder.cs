﻿using AutoFixture;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Ordering.Shared.Data.LLBLGen.Tests.Builders
{
    public class ProductCategoryTagEntityBuilder : EntityBuilder<ProductCategoryTagEntity, ProductCategoryTagEntityBuilder.ProductCategoryTagEntityData, ProductCategoryTagEntityBuilder>
    {
        public ProductCategoryTagEntityBuilder()
            : base(Initialise(), true)
        {
        }

        private ProductCategoryTagEntityBuilder(bool isNew)
            : base(Initialise(), isNew)
        {
        }

        private ProductCategoryTagEntityBuilder(ProductCategoryTagEntityData productCategoryTagEntityData, bool isNew)
            : base(productCategoryTagEntityData, isNew)
        {
        }

        private new static ProductCategoryTagEntityData Initialise()
        {
            Fixture fixture = new Fixture();

            return fixture.Create<ProductCategoryTagEntityData>();
        }


        public override ProductCategoryTagEntityBuilder AsNew() => new ProductCategoryTagEntityBuilder(true);

        public ProductCategoryTagEntityBuilder WithProductCategoryTagId(int productCategoryTagId) => new ProductCategoryTagEntityBuilder(
            new ProductCategoryTagEntityData
            {
                ProductCategoryTagId = productCategoryTagId,
                ProductCategoryId = EntityData.ProductCategoryId,
                CategoryId = EntityData.CategoryId,
                TagId = EntityData.TagId
            }, IsNew);

        public ProductCategoryTagEntityBuilder WithCategoryId(int categoryId) => new ProductCategoryTagEntityBuilder(
            new ProductCategoryTagEntityData
            {
                ProductCategoryTagId = EntityData.ProductCategoryTagId,
                CategoryId = categoryId,
                ProductCategoryId = EntityData.ProductCategoryId,
                TagId = EntityData.TagId
            }, IsNew);

        public ProductCategoryTagEntityBuilder WithProductId(int productCategoryId) => new ProductCategoryTagEntityBuilder(
            new ProductCategoryTagEntityData
            {
                ProductCategoryTagId = EntityData.ProductCategoryTagId,
                CategoryId = EntityData.CategoryId,
                ProductCategoryId = productCategoryId,
                TagId = EntityData.TagId
            }, IsNew);

        public ProductCategoryTagEntityBuilder WithTagId(int tagId) => new ProductCategoryTagEntityBuilder(
            new ProductCategoryTagEntityData
            {
                ProductCategoryTagId = EntityData.ProductCategoryTagId,
                ProductCategoryId = EntityData.ProductCategoryId,
                CategoryId = EntityData.CategoryId,
                TagId = tagId
            }, IsNew);

        protected override void EnrichEntity(ProductCategoryTagEntity entity, ProductCategoryTagEntityData entityData)
        {
            entity.ProductCategoryTagId = entityData.ProductCategoryTagId;
            entity.ProductCategoryId = entityData.ProductCategoryId;
            entity.CategoryId = entityData.CategoryId;
            entity.TagId = entityData.TagId;
        }

        public class ProductCategoryTagEntityData
        {
            public int ProductCategoryTagId { get; set; }
            public int ProductCategoryId { get; set; }
            public int CategoryId { get; set; }
            public int TagId { get; set; }
        }
    }
}
