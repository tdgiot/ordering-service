﻿using System.Collections.Generic;
using AutoFixture;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Ordering.Shared.Data.LLBLGen.Tests.Builders
{
    public class ApplicationConfigurationEntityBuilder : EntityBuilder<ApplicationConfigurationEntity, ApplicationConfigurationEntityBuilder.ApplicationConfigurationEntityData, ApplicationConfigurationEntityBuilder>
    {
        public ApplicationConfigurationEntityBuilder() : base(Initialise())
        {
        }

        public ApplicationConfigurationEntityBuilder(bool isNew) : base(Initialise(), isNew)
        {
        }

        private ApplicationConfigurationEntityBuilder(ApplicationConfigurationEntityData applicationConfigurationEntityData) : base(applicationConfigurationEntityData)
        {
        }

        private new static ApplicationConfigurationEntityData Initialise()
        {
            Fixture fixture = new Fixture();
            fixture.Register(() => new FeatureFlagEntityBuilder().Build());
            return fixture.Create<ApplicationConfigurationEntityData>();
        }

        public override ApplicationConfigurationEntityBuilder AsNew() => new ApplicationConfigurationEntityBuilder(true);

        protected override void EnrichEntity(ApplicationConfigurationEntity entity, ApplicationConfigurationEntityData entityData)
        {
            entity.FeatureFlagCollection.AddRange(entityData.FeatureFlagCollection);
        }

        public ApplicationConfigurationEntityBuilder W(FeatureFlagEntityBuilder featureFlagEntityBuilder) => new ApplicationConfigurationEntityBuilder(new ApplicationConfigurationEntityData
        {
            FeatureFlagCollection = new[] { featureFlagEntityBuilder.Build() },
        });

        public class ApplicationConfigurationEntityData
        {
            public IEnumerable<FeatureFlagEntity> FeatureFlagCollection { get; set; }
        }
    }
}