using AutoFixture;
using Crave.Enums;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.Tests.DomainTypes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Crave.Ordering.Shared.Data.LLBLGen.Tests.Builders
{
    public class OrderitemEntityBuilder : EntityBuilder<OrderitemEntity, OrderitemEntityBuilder.OrderitemEntityData, OrderitemEntityBuilder>
    {
        public OrderitemEntityBuilder() : base(Initialise())
        {
        }

        private OrderitemEntityBuilder(bool isNew) : base(Initialise(), isNew)
        {
        }

        private OrderitemEntityBuilder(OrderitemEntityData orderitemEntityData, bool isNew) : base(orderitemEntityData, isNew)
        {
        }

        public override OrderitemEntityBuilder AsNew() => new OrderitemEntityBuilder(true);

        public OrderitemEntityBuilder W(ProductEntityBuilder productEntityBuilder) => new OrderitemEntityBuilder(new OrderitemEntityData(EntityData)
        {
            ProductEntity = productEntityBuilder.Build()
        }, IsNew);

        public OrderitemEntityBuilder W(ProductEntity productEntity) => new OrderitemEntityBuilder(new OrderitemEntityData(EntityData)
        {
            ProductEntity = productEntity
        }, IsNew);

        public OrderitemEntityBuilder W(OrderitemType orderitemType) => new OrderitemEntityBuilder(new OrderitemEntityData(EntityData)
        {
            OrderitemType = orderitemType
        }, IsNew);

        public OrderitemEntityBuilder W(Guid guid) => new OrderitemEntityBuilder(new OrderitemEntityData(EntityData)
        {
            TempInternalGuid = guid
        }, IsNew);

        public OrderitemEntityBuilder W(PriceTotalInTax priceTotalInTax) => new OrderitemEntityBuilder(new OrderitemEntityData(EntityData)
        {
            PriceTotalInTax = priceTotalInTax
        }, IsNew);

        public OrderitemEntityBuilder W(PriceTotalExTax priceTotalExTax) => new OrderitemEntityBuilder(new OrderitemEntityData(EntityData)
        {
            PriceTotalExTax = priceTotalExTax
        }, IsNew);

        public OrderitemEntityBuilder W(PriceInTax priceInTax) => new OrderitemEntityBuilder(new OrderitemEntityData(EntityData)
        {
            PriceInTax = priceInTax
        }, IsNew);

        public OrderitemEntityBuilder W(PriceExTax priceExTax) => new OrderitemEntityBuilder(new OrderitemEntityData(EntityData)
        {
            PriceExTax = priceExTax
        }, IsNew);

        public OrderitemEntityBuilder W(Tax tax) => new OrderitemEntityBuilder(new OrderitemEntityData(EntityData)
        {
            Tax = tax
        }, IsNew);

        public OrderitemEntityBuilder W(TaxTotal taxTotal) => new OrderitemEntityBuilder(new OrderitemEntityData(EntityData)
        {
            TaxTotal = taxTotal
        }, IsNew);

        public OrderitemEntityBuilder W(ProductPriceIn productPriceIn) => new OrderitemEntityBuilder(new OrderitemEntityData(EntityData)
        {
            ProductPriceIn = productPriceIn
        }, IsNew);

        public OrderitemEntityBuilder W(int? productId) => new OrderitemEntityBuilder(new OrderitemEntityData(EntityData)
        {
            ProductId = productId
        }, IsNew);

        public OrderitemEntityBuilder W(Quantity quantity) => new OrderitemEntityBuilder(new OrderitemEntityData(EntityData)
        {
            Quantity = quantity
        }, IsNew);

        public OrderitemEntityBuilder W(TaxTariffId taxTariffId) => new OrderitemEntityBuilder(new OrderitemEntityData(EntityData)
        {
            TaxTariffId = taxTariffId
        }, IsNew);

        public OrderitemEntityBuilder W(OrderitemAlterationitemEntity[] orderitemAlterationitemEntities) => new OrderitemEntityBuilder(new OrderitemEntityData(EntityData)
        {
            OrderitemAlterationitemEntities = orderitemAlterationitemEntities
        }, IsNew);

        public OrderitemEntityBuilder WithId(int id) => new OrderitemEntityBuilder(new OrderitemEntityData(EntityData)
        {
            OrderitemId = id
        }, IsNew);

        public OrderitemEntityBuilder WithParentOrder(OrderEntityBuilder orderEntity) => new OrderitemEntityBuilder(new OrderitemEntityData(EntityData)
        {
            ParentOrderEntity = orderEntity.Build()
        }, IsNew);

        public OrderitemEntityBuilder WithNotes(string notes) => new OrderitemEntityBuilder(new OrderitemEntityData(EntityData)
        {
            Notes = notes
        }, IsNew);

        public OrderitemEntityBuilder WithTaxName(string taxName) => new OrderitemEntityBuilder(new OrderitemEntityData(EntityData)
        {
            TaxName = taxName
        }, IsNew);

        public OrderitemEntityBuilder WithProductId(int productId) => new OrderitemEntityBuilder(new OrderitemEntityData(EntityData)
        {
            ProductId = productId
        }, IsNew);

        public OrderitemEntityBuilder WithCategoryId(int? id) => new OrderitemEntityBuilder(new OrderitemEntityData(EntityData)
        {
            CategoryId = id
        }, IsNew);

        public OrderitemEntityBuilder WithProduct(ProductEntity? product) => new OrderitemEntityBuilder(new OrderitemEntityData(EntityData)
        {
            ProductEntity = product
        }, IsNew);

        public OrderitemEntityBuilder WithProductName(string productName) => new OrderitemEntityBuilder(new OrderitemEntityData(EntityData)
        {
            ProductName = productName
        }, IsNew);

        private new static OrderitemEntityData Initialise()
        {
            var fixture = new Fixture();
            fixture.Register(new OrderitemAlterationitemEntityBuilder().Build);
            fixture.Register(() => OrderitemType.Product);
            fixture.Register(() => (PriceTotalInTax)0M);
            fixture.Register(() => (PriceTotalExTax)0M);
            fixture.Register(() => (PriceInTax)0M);
            fixture.Register(() => (PriceExTax)0M);
            fixture.Register(() => (Quantity)1);
            fixture.Register(() => (ProductPriceIn)0M);
            fixture.Register(() => (TaxTariffId)fixture.Create<int>());
            fixture.Register(() => new ProductEntityBuilder().Build());
            fixture.Register(() => (OrderEntity)null);
            fixture.Register(() => (ProductEntity)null);
            fixture.Register(() => (Tax)0M);
            fixture.Register(() => (TaxTotal)0M);

            OrderitemEntityData orderitemEntityData = fixture.Create<OrderitemEntityData>();

            orderitemEntityData.ProductId = null;

            return orderitemEntityData;
        }

        protected override void EnrichEntity(OrderitemEntity entity, OrderitemEntityData entityData)
        {
            entity.OrderitemId = entityData.OrderitemId;
            entity.Order = entityData.ParentOrderEntity;
            entity.TempInternalGuid = entityData.TempInternalGuid;
            entity.Type = entityData.OrderitemType;
            entity.PriceTotalInTax = entityData.PriceTotalInTax;
            entity.PriceTotalExTax = entityData.PriceTotalExTax;
            entity.PriceInTax = entityData.PriceInTax;
            entity.PriceExTax = entityData.PriceExTax;
            entity.ProductPriceIn = entityData.ProductPriceIn;
            entity.Quantity = entityData.Quantity;
            entity.TaxTariffId = entityData.TaxTariffId;
            entity.OrderitemAlterationitemCollection.AddRange(entityData.OrderitemAlterationitemEntities);
            entity.CategoryId = entityData.CategoryId;
            entity.Notes = entityData.Notes;
            entity.Tax = entityData.Tax;
            entity.TaxTotal = entityData.TaxTotal;
            entity.TaxName = entityData.TaxName;
            entity.ProductName = entityData.ProductName;

            if (entityData.ProductEntity == null)
            {
                entity.ProductId = entityData.ProductId;
            }
            else
            {
                entity.Product = entityData.ProductEntity;
            }
        }

        public class OrderitemEntityData
        {
            public OrderitemEntityData()
            {

            }
            public OrderitemEntityData(OrderitemEntityData old)
            {
                OrderitemId = old.OrderitemId;
                ParentOrderEntity = old.ParentOrderEntity;
                TempInternalGuid = old.TempInternalGuid;
                OrderitemType = old.OrderitemType;
                PriceTotalInTax = old.PriceTotalInTax;
                PriceTotalExTax = old.PriceTotalExTax;
                ProductPriceIn = old.ProductPriceIn;
                PriceInTax = old.PriceInTax;
                PriceExTax = old.PriceExTax;
                Tax = old.Tax;
                TaxTotal = old.TaxTotal;
                Quantity = old.Quantity;
                TaxTariffId = old.TaxTariffId;
                OrderitemAlterationitemEntities = old.OrderitemAlterationitemEntities;
                ProductEntity = old.ProductEntity;
                ProductId = old.ProductId;
                CategoryId = old.CategoryId;
                Notes = old.Notes;
                TaxName = old.TaxName;
                ProductName = old.ProductName;
            }

            public int OrderitemId { get; set; }
            public OrderEntity ParentOrderEntity { get; set; }
            public Guid TempInternalGuid { get; set; }
            public OrderitemType OrderitemType { get; set; }
            public PriceTotalInTax PriceTotalInTax { get; set; }
            public PriceTotalExTax PriceTotalExTax { get; set; }
            public ProductPriceIn ProductPriceIn { get; set; }
            public PriceInTax PriceInTax { get; set; }
            public PriceExTax PriceExTax { get; set; }
            public Tax Tax { get; set; }
            public TaxTotal TaxTotal { get; set; }
            public int? ProductId { get; set; }
            public Quantity Quantity { get; set; }
            public TaxTariffId TaxTariffId { get; set; }
            public IEnumerable<OrderitemAlterationitemEntity> OrderitemAlterationitemEntities { get; set; }
            public ProductEntity? ProductEntity { get; set; }
            [StringLength(0)] public string Notes { get; set; }
            public int? CategoryId { get; set; }
            public string TaxName { get; set; }
            public string ProductName { get; set; }
        }
    }
}