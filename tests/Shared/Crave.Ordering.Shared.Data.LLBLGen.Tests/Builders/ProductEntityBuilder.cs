using AutoFixture;
using Crave.Enums;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Crave.Ordering.Shared.Data.LLBLGen.Tests.Builders
{
    public class ProductEntityBuilder : EntityBuilder<ProductEntity, ProductEntityBuilder.ProductEntityData, ProductEntityBuilder>
    {
        public ProductEntityBuilder() : base(Initialise(), true)
        {
        }

        private ProductEntityBuilder(bool isNew) : base(Initialise(), isNew)
        {
        }

        private ProductEntityBuilder(ProductEntityData productEntityData, bool isNew) : base(productEntityData, isNew)
        {
        }

        private new static ProductEntityData Initialise()
        {
            Fixture fixture = new Fixture();
            fixture.Register(() => new TaxTariffEntityBuilder().Build());
            fixture.Register(() => new ScheduleEntityBuilder().Build());
            fixture.Register(() => new ExternalProductEntityBuilder().Build());
            fixture.Register(() => new List<AlterationEntity>());

            return fixture.Create<ProductEntityData>();
        }

        public override ProductEntityBuilder AsNew() => new ProductEntityBuilder(true);

        public ProductEntityBuilder W(VisibilityType visibilityType) => new ProductEntityBuilder(new ProductEntityData(EntityData)
        {
            VisibilityType = visibilityType
        }, IsNew);

        public ProductEntityBuilder W(int productId, int scheduleId) => new ProductEntityBuilder(new ProductEntityData(EntityData)
        {
            ProductId = productId,
            ScheduleId = scheduleId
        }, IsNew);

        public ProductEntityBuilder W(ScheduleEntityBuilder scheduleEntityBuilder) => new ProductEntityBuilder(new ProductEntityData(EntityData)
        {
            ScheduleEntity = scheduleEntityBuilder.Build()
        }, IsNew);

        public ProductEntityBuilder W(ScheduleEntity scheduleEntity) => new ProductEntityBuilder(new ProductEntityData(EntityData)
        {
            ScheduleEntity = scheduleEntity
        }, IsNew);

        public ProductEntityBuilder W(ExternalProductEntity externalProductEntity) => new ProductEntityBuilder(new ProductEntityData(EntityData)
        {
            ExternalProductEntity = externalProductEntity
        }, IsNew);

        public ProductEntityBuilder WithPriceIn(decimal priceIn) => new ProductEntityBuilder(new ProductEntityData(EntityData)
        {
            PriceIn = priceIn
        }, IsNew);

        public ProductEntityBuilder WithRouteId(int routeId) => new ProductEntityBuilder(new ProductEntityData(EntityData)
        {
            RouteId = routeId
        }, IsNew);

        public ProductEntityBuilder WithExternalProductId(int externalProductId) => new ProductEntityBuilder(new ProductEntityData(EntityData)
        {
            ExternalProductId = externalProductId
        }, IsNew);

        public ProductEntityBuilder WithId(int id) => new ProductEntityBuilder(new ProductEntityData(EntityData)
        {
            ProductId = id
        }, IsNew);

        public ProductEntityBuilder WithIsAvailable(bool isAvailable) => new ProductEntityBuilder(new ProductEntityData(EntityData)
        {
            IsAvailable = isAvailable
        }, IsNew);

        public ProductEntityBuilder WithAlterations(params AlterationEntityBuilder[] builders) => new ProductEntityBuilder(new ProductEntityData(EntityData)
        {
            Alterations = builders.Select(x => x.Build()).ToList()
        }, IsNew);

        protected override void EnrichEntity(ProductEntity entity, ProductEntityData entityData)
        {
            entity.ScheduleId = entityData.ScheduleId;
            entity.ProductId = entityData.ProductId;
            entity.Color = entityData.Colour;
            entity.Name = entityData.Name;
            entity.TaxTariffId = entityData.TaxTariffId;
            entity.VisibilityType = entityData.VisibilityType;
            entity.TaxTariff = entityData.TaxTariff;
            entity.Schedule = entityData.ScheduleEntity;
            entity.ExternalProduct = entityData.ExternalProductEntity;
            entity.ExternalProductId = entityData.ExternalProductId;
            entity.PriceIn = entityData.PriceIn;
            entity.RouteId = entityData.RouteId;
            entity.IsAvailable = entityData.IsAvailable;

            foreach (AlterationEntity alterationEntity in entityData.Alterations)
            {
                ProductAlterationEntity productAlterationEntity = entity.ProductAlterationCollection.AddNew();
                productAlterationEntity.Alteration = alterationEntity;
            }
        }

        public class ProductEntityData
        {
            public ProductEntityData()
            {

            }

            public ProductEntityData(ProductEntityData old)
            {
                Colour = old.Colour;
                ProductId = old.ProductId;
                Name = old.Name;
                TaxTariffId = old.TaxTariffId;
                VisibilityType = old.VisibilityType;
                TaxTariff = old.TaxTariff;
                ScheduleEntity = old.ScheduleEntity;
                ExternalProductEntity = old.ExternalProductEntity;
                ExternalProductId = old.ExternalProductId;
                ScheduleId = old.ScheduleId;
                PriceIn = old.PriceIn;
                RouteId = old.RouteId;
                IsAvailable = old.IsAvailable;
            }

            public int Colour { get; set; }
            public int ProductId { get; set; }
            [StringLength(40)] public string Name { get; set; }
            public int? TaxTariffId { get; set; }
            public VisibilityType VisibilityType { get; set; } = VisibilityType.Always;
            public TaxTariffEntity TaxTariff { get; set; }
            public ScheduleEntity ScheduleEntity { get; set; }
            public ExternalProductEntity ExternalProductEntity { get; set; }
            public int ScheduleId { get; set; }
            public decimal PriceIn { get; set; }
            public int RouteId { get; set; }
            public int ExternalProductId { get; set; }
            public bool IsAvailable { get; set; } = true;
            public List<AlterationEntity> Alterations { get; set; } = new List<AlterationEntity>();
        }
    }
}