using AutoFixture;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Ordering.Shared.Data.LLBLGen.Tests.Builders
{
    public class ExternalProductEntityBuilder : EntityBuilder<ExternalProductEntity, ExternalProductEntityBuilder.ExternalProductEntityData, ExternalProductEntityBuilder>
    {
        public ExternalProductEntityBuilder() : base(Initialise(), true)
        {
        }

        private ExternalProductEntityBuilder(bool isNew) : base(Initialise(), isNew)
        {
        }

        private ExternalProductEntityBuilder(ExternalProductEntityData externalProductEntityData, bool isNew) : base(externalProductEntityData, isNew)
        {
        }

        private new static ExternalProductEntityData Initialise()
        {
            Fixture fixture = new Fixture();

            return fixture.Create<ExternalProductEntityData>();
        }

        public override ExternalProductEntityBuilder AsNew() => new ExternalProductEntityBuilder(true);

        public ExternalProductEntityBuilder W(int externalProductId) => new ExternalProductEntityBuilder(new ExternalProductEntityData(EntityData)
        {
            ExternalProductId = externalProductId
        }, IsNew);

        public ExternalProductEntityBuilder W(string name) => new ExternalProductEntityBuilder(new ExternalProductEntityData(EntityData)
        {
            Name = name
        }, IsNew);

        protected override void EnrichEntity(ExternalProductEntity entity, ExternalProductEntityData entityData)
        {
            entity.ExternalProductId = entityData.ExternalProductId;
            entity.Name = entityData.Name;
        }

        public class ExternalProductEntityData
        {
            public ExternalProductEntityData()
            {

            }

            public ExternalProductEntityData(ExternalProductEntityData old)
            {
                this.ExternalProductId = old.ExternalProductId;
                this.Name = old.Name;
            }

            public int ExternalProductId { get; set; }
            public string Name { get; set; }
        }
    }
}