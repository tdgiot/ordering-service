﻿using AutoFixture;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Ordering.Shared.Data.LLBLGen.Tests.Builders
{
    public class OutletSellerInformationEntityBuilder : EntityBuilder<OutletSellerInformationEntity, OutletSellerInformationEntityBuilder.OutletSellerInformationEntityData,
            OutletSellerInformationEntityBuilder>
    {
        public OutletSellerInformationEntityBuilder() : base(Initialise())
        {
        }

        public OutletSellerInformationEntityBuilder(bool isNew) : base(Initialise(), isNew)
        {
        }

        private OutletSellerInformationEntityBuilder(OutletSellerInformationEntityData data, bool isNew) : base(data, isNew)
        {
        }

        private new static OutletSellerInformationEntityData Initialise()
        {
            return new Fixture().Build<OutletSellerInformationEntityData>()
                                .With(x => x.SellerEmail, "no-reply@trueguest.tech")
                                .With(x => x.SellerName, "Crave")
                                .Create();
        }

        public override OutletSellerInformationEntityBuilder AsNew() => new OutletSellerInformationEntityBuilder(true);

        public OutletSellerInformationEntityBuilder W(int outletSellerInformationId) => new OutletSellerInformationEntityBuilder(new OutletSellerInformationEntityData(EntityData)
        {
            OutletSellerInformationId = outletSellerInformationId
        },
                                                                                                                                 IsNew);

        public OutletSellerInformationEntityBuilder WithSellerName(string sellerName) => new OutletSellerInformationEntityBuilder(new OutletSellerInformationEntityData(EntityData)
        {
            SellerName = sellerName
        },
                                                                                                                                  IsNew);

        public OutletSellerInformationEntityBuilder WithSellerEmail(string sellerEmail) => new OutletSellerInformationEntityBuilder(new OutletSellerInformationEntityData(EntityData)
        {
            SellerEmail = sellerEmail
        },
                                                                                                                                    IsNew);

        protected override void EnrichEntity(OutletSellerInformationEntity entity, OutletSellerInformationEntityData entityData)
        {
            entity.OutletSellerInformationId = entityData.OutletSellerInformationId;
            entity.SellerName = entityData.SellerName;
            entity.Email = entityData.SellerEmail;
        }

        public class OutletSellerInformationEntityData
        {
            public OutletSellerInformationEntityData()
            {
            }

            public OutletSellerInformationEntityData(OutletSellerInformationEntityData old)
            {
                OutletSellerInformationId = old.OutletSellerInformationId;
                SellerName = old.SellerName;
                SellerEmail = old.SellerEmail;
            }

            public int OutletSellerInformationId { get; set; }
            public string SellerName { get; set; }
            public string SellerEmail { get; set; }
        }
    }
}