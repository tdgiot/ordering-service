﻿using AutoFixture;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Ordering.Shared.Data.LLBLGen.Tests.Builders
{
    public class OrderNotificationLogEntityBuilder : EntityBuilder<OrderNotificationLogEntity, OrderNotificationLogEntityBuilder.OrderNotificationLogEntityData, OrderNotificationLogEntityBuilder>
    {
        public OrderNotificationLogEntityBuilder() : base(Initialise())
        {
        }

        public OrderNotificationLogEntityBuilder(bool isNew) : base(Initialise(), isNew)
        {
        }

        private OrderNotificationLogEntityBuilder(OrderNotificationLogEntityData orderNotificationLogEntityData) : base(orderNotificationLogEntityData)
        {
        }

        private new static OrderNotificationLogEntityData Initialise()
        {
            Fixture fixture = new Fixture();
            fixture.Register(() => 1);
            return fixture.Create<OrderNotificationLogEntityData>();
        }

        public OrderNotificationLogEntityBuilder W(int orderNotificationLogId) => new OrderNotificationLogEntityBuilder(new OrderNotificationLogEntityData
        {
            OrderNotificationLogId = orderNotificationLogId
        });

        public override OrderNotificationLogEntityBuilder AsNew() => new OrderNotificationLogEntityBuilder(true);

        protected override void EnrichEntity(OrderNotificationLogEntity entity, OrderNotificationLogEntityData entityData)
        {
            entity.OrderNotificationLogId = entityData.OrderNotificationLogId;
        }

        public class OrderNotificationLogEntityData
        {
            public int OrderNotificationLogId { get; set; }
        }
    }
}