﻿using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Ordering.Shared.Data.LLBLGen.Tests.Builders
{
    public class RouteEntityBuilder : EntityBuilder<RouteEntity, RouteEntityBuilder.RouteEntityData, RouteEntityBuilder>
    {
        public RouteEntityBuilder() : base(Initialise())
        {
        }

        private RouteEntityBuilder(bool isNew) : base(Initialise(), isNew)
        {
        }

        private RouteEntityBuilder(RouteEntityData routeEntityData, bool isNew) : base(routeEntityData, isNew)
        {
        }

        public override RouteEntityBuilder AsNew() => new RouteEntityBuilder(EntityData, true);

        public RouteEntityBuilder WithRouteId(int id) => new RouteEntityBuilder(new RouteEntityData(EntityData)
        {
            RouteId = id
        }, IsNew);

        protected override void EnrichEntity(RouteEntity entity, RouteEntityData entityData)
        {
            entity.RouteId = entityData.RouteId;
        }

        public class RouteEntityData
        {
            public RouteEntityData() { }

            public RouteEntityData(RouteEntityData old)
            {
                RouteId = old.RouteId;
            }

            public int RouteId { get; set; }
        }
    }
}