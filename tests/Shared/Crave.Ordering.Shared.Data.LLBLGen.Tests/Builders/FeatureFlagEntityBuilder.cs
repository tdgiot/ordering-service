﻿using AutoFixture;
using Crave.Enums;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Ordering.Shared.Data.LLBLGen.Tests.Builders
{
    public class FeatureFlagEntityBuilder : EntityBuilder<FeatureFlagEntity, FeatureFlagEntityBuilder.FeatureFlagEntityData, FeatureFlagEntityBuilder>
    {
        public FeatureFlagEntityBuilder() : base(Initialise())
        {
        }

        public FeatureFlagEntityBuilder(bool isNew) : base(Initialise(), isNew)
        {
        }

        private FeatureFlagEntityBuilder(FeatureFlagEntityData featureFlagEntityData) : base(featureFlagEntityData)
        {
        }

        private new static FeatureFlagEntityData Initialise()
        {
            Fixture fixture = new Fixture();
            fixture.Register(() => ApplessFeatureFlags.EatOutToHelpOutDiscount);
            return fixture.Create<FeatureFlagEntityData>();
        }

        public override FeatureFlagEntityBuilder AsNew() => new FeatureFlagEntityBuilder(true);

        protected override void EnrichEntity(FeatureFlagEntity entity, FeatureFlagEntityData entityData)
        {
            entity.ApplessFeatureFlags = entityData.ApplessFeatureFlags;
        }

        public FeatureFlagEntityBuilder W(ApplessFeatureFlags applessFeatureFlags) => new FeatureFlagEntityBuilder(new FeatureFlagEntityData
        {
            ApplessFeatureFlags = applessFeatureFlags
        });

        public class FeatureFlagEntityData
        {
            public ApplessFeatureFlags ApplessFeatureFlags { get; set; }
        }
    }
}