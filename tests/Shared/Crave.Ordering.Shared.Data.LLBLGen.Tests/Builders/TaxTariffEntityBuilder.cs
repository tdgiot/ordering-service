﻿using AutoFixture;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Ordering.Shared.Data.LLBLGen.Tests.Builders
{
    public class TaxTariffEntityBuilder : EntityBuilder<TaxTariffEntity, TaxTariffEntityBuilder.TaxTariffEntityData, TaxTariffEntityBuilder>
    {
        public TaxTariffEntityBuilder() : base(Initialise())
        {
        }

        private TaxTariffEntityBuilder(bool isNew) : base(Initialise(), isNew)
        {
        }

        private TaxTariffEntityBuilder(TaxTariffEntityData taxTariffEntityData, bool isNew) : base(taxTariffEntityData, isNew)
        {
        }

        public override TaxTariffEntityBuilder AsNew() => new TaxTariffEntityBuilder(true);

        public TaxTariffEntityBuilder W(int taxTariffId) => new TaxTariffEntityBuilder(new TaxTariffEntityData
        {
            TaxTariffId = taxTariffId,
            Percentage = EntityData.Percentage
        }, IsNew);

        public TaxTariffEntityBuilder W(double percentage) => new TaxTariffEntityBuilder(new TaxTariffEntityData
        {
            TaxTariffId = EntityData.TaxTariffId,
            Percentage = percentage
        }, IsNew);



        private new static TaxTariffEntityData Initialise()
        {
            Fixture fixture = new Fixture();
            fixture.Register(() => new CompanyEntityBuilder().Build());
            return fixture.Create<TaxTariffEntityData>();
        }

        protected override void EnrichEntity(TaxTariffEntity entity, TaxTariffEntityData entityData)
        {
            entity.TaxTariffId = entityData.TaxTariffId;
            entity.Percentage = entityData.Percentage;
            entity.Company = entityData.Company;
        }

        public class TaxTariffEntityData
        {
            public int TaxTariffId { get; set; }
            public double Percentage { get; set; }
            public CompanyEntity Company { get; set; }
        }
    }
}