﻿using System.Collections.Generic;
using AutoFixture;
using Crave.Enums;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.Tests.Builders;

namespace Crave.Ordering.Api.Domain.Tests.Builders.EntityBuilders
{
    public class OutletEntityBuilder : EntityBuilder<OutletEntity, OutletEntityBuilder.OutletEntityData, OutletEntityBuilder>
	{
		public OutletEntityBuilder() : base(OutletEntityBuilder.Initialise())
		{
		}

		public OutletEntityBuilder(bool isNew) : base(OutletEntityBuilder.Initialise(), isNew)
		{
		}

		private OutletEntityBuilder(OutletEntityData data, bool isNew) : base(data, isNew)
		{
		}

		private new static OutletEntityData Initialise()
		{
			Fixture fixture = new Fixture();
			fixture.Register(() => new OutletOperationalStateEntityBuilder().Build());
			fixture.Register(() => new BusinesshourEntityBuilder().Build());
			fixture.Register(() => (TippingActive) fixture.Create<bool>());
			fixture.Register(() => (CustomerDetailsNameRequired) false);
			fixture.Register(() => (CustomerDetailsEmailRequired) false);
			fixture.Register(() => (CustomerDetailsPhoneRequired) false);
			fixture.Register(() => (TippingProductId)fixture.Create<int>());
			fixture.Register(() => (ServiceChargeProductId)fixture.Create<int>());
            fixture.Register(() => new OutletSellerInformationEntityBuilder().Build());
			fixture.Register(() => new ProductEntityBuilder().Build());
			fixture.Register(() => new CompanyEntityBuilder().Build());

			return fixture.Create<OutletEntityData>();
		}

		public override OutletEntityBuilder AsNew() => new OutletEntityBuilder(true);

		protected override void EnrichEntity(OutletEntity entity, OutletEntityData entityData)
		{
			entity.OutletId = entityData.OutletId;
            entity.TippingActive = entityData.TippingActive;
			entity.TippingMinimumPercentage = entityData.TippingMinimumPercentage;
			entity.TippingProductId = entityData.TippingProductId;
			entity.OutletOperationalState = entityData.OutletOperationalState;
			entity.BusinesshourCollection.AddRange(entityData.BusinesshourCollection);
			entity.CustomerDetailsNameRequired = entityData.CustomerDetailsNameRequired;
			entity.CustomerDetailsEmailRequired = entityData.CustomerDetailsEmailRequired;
			entity.CustomerDetailsPhoneRequired = entityData.CustomerDetailsPhoneRequired;
			entity.ServiceChargeProductId = entityData.ServiceChargeProductId;
            entity.OutletSellerInformation = entityData.OutletSellerInformation;
			entity.DeliveryChargeProduct = entityData.ProductEntity;
			entity.Company = entityData.Company;
            entity.OptInType = entityData.OptInType;
        }

		public OutletEntityBuilder W(int outletId) => new OutletEntityBuilder(new OutletEntityData(this.EntityData)
        {
            OutletId = outletId
        }, this.IsNew);

		public OutletEntityBuilder W(TippingActive tippingActive) => new OutletEntityBuilder(new OutletEntityData(this.EntityData)
        {
            TippingActive = tippingActive
        }, this.IsNew);

		public OutletEntityBuilder W(double tippingMinimumPercentage) => new OutletEntityBuilder(new OutletEntityData(this.EntityData)
        {
            TippingMinimumPercentage = tippingMinimumPercentage
        }, this.IsNew);

		public OutletEntityBuilder W(TippingProductId tippingProductId) => new OutletEntityBuilder(new OutletEntityData(this.EntityData)
        {
            TippingProductId = tippingProductId
        }, this.IsNew);

		public OutletEntityBuilder W(OutletOperationalStateEntity outletOperationalStateEntity) => new OutletEntityBuilder(new OutletEntityData(this.EntityData)
        {
            OutletOperationalState = outletOperationalStateEntity
        }, this.IsNew);

		public OutletEntityBuilder W(OutletOperationalStateEntityBuilder outletOperationalStateEntityBuilder) => this.W(outletOperationalStateEntityBuilder.Build());

		public OutletEntityBuilder W(IEnumerable<BusinesshourEntity> businesshourEntities) => new OutletEntityBuilder(new OutletEntityData(this.EntityData)
        {
            BusinesshourCollection = businesshourEntities
        }, this.IsNew);

		public OutletEntityBuilder W(CustomerDetailsEmailRequired customerDetailsEmailRequired) => new OutletEntityBuilder(new OutletEntityData(this.EntityData)
        {
            CustomerDetailsEmailRequired = customerDetailsEmailRequired
        }, this.IsNew);

		public OutletEntityBuilder W(CustomerDetailsNameRequired customerDetailsNameRequired) => new OutletEntityBuilder(new OutletEntityData(this.EntityData)
        {
            CustomerDetailsNameRequired = customerDetailsNameRequired
        }, this.IsNew);

		public OutletEntityBuilder W(CustomerDetailsPhoneRequired customerDetailsPhoneRequired) => new OutletEntityBuilder(new OutletEntityData(this.EntityData)
        {
            CustomerDetailsPhoneRequired = customerDetailsPhoneRequired
        }, this.IsNew);

		public OutletEntityBuilder W(ServiceChargeProductId serviceChargeProductId) => new OutletEntityBuilder(new OutletEntityData(this.EntityData)
        {
            ServiceChargeProductId = serviceChargeProductId
        }, this.IsNew);

        public OutletEntityBuilder W(OutletSellerInformationEntityBuilder outletSellerInformationEntityBuilder) => new OutletEntityBuilder(new OutletEntityData(this.EntityData)
        {
            OutletSellerInformation = outletSellerInformationEntityBuilder.Build()
        }, this.IsNew);

        public OutletEntityBuilder W(CompanyEntityBuilder companyEntityBuilder) => new OutletEntityBuilder(new OutletEntityData(this.EntityData)
        {
            Company = companyEntityBuilder.Build()
        }, this.IsNew);

        public OutletEntityBuilder W(OptInType optInType) => new OutletEntityBuilder(new OutletEntityData(this.EntityData)
        {
            OptInType = optInType
		}, this.IsNew);

		public class OutletEntityData
		{
            public OutletEntityData()
            {
            }

            public OutletEntityData(OutletEntityData old)
            {
                this.OutletId = old.OutletId;
                this.TippingActive = old.TippingActive;
                this.TippingMinimumPercentage = old.TippingMinimumPercentage;
                this.TippingProductId = old.TippingProductId;
                this.OutletOperationalState = old.OutletOperationalState;
                this.BusinesshourCollection = old.BusinesshourCollection;
                this.CustomerDetailsNameRequired = old.CustomerDetailsNameRequired;
                this.CustomerDetailsEmailRequired = old.CustomerDetailsEmailRequired;
                this.CustomerDetailsPhoneRequired = old.CustomerDetailsPhoneRequired;
                this.ServiceChargeProductId = old.ServiceChargeProductId;
                this.OutletSellerInformation = old.OutletSellerInformation;
                this.ProductEntity = old.ProductEntity;
                this.Company = old.Company;
                this.OptInType = old.OptInType;
            }

            public int OutletId { get; set; }
			public TippingActive TippingActive { get; set; }
			public double TippingMinimumPercentage { get; set; }
			public TippingProductId TippingProductId { get; set; }
			public OutletOperationalStateEntity OutletOperationalState { get; set; }
			public IEnumerable<BusinesshourEntity> BusinesshourCollection { get; set; }
			public CustomerDetailsNameRequired CustomerDetailsNameRequired { get; set; }
			public CustomerDetailsEmailRequired CustomerDetailsEmailRequired { get; set; }
			public CustomerDetailsPhoneRequired CustomerDetailsPhoneRequired { get; set; }
			public ServiceChargeProductId ServiceChargeProductId { get; set; }
			public OutletSellerInformationEntity OutletSellerInformation { get; set; }
			public ProductEntity ProductEntity { get; set; }
			public CompanyEntity Company { get; set; }
			public OptInType OptInType { get; set; }
		}
	}

	public readonly struct ServiceChargeProductId
	{
		private int Value { get; }

		private ServiceChargeProductId(int value) => this.Value = value;

		public static implicit operator int(ServiceChargeProductId tippingProductId) => tippingProductId.Value;

		public static implicit operator ServiceChargeProductId(int value) => new ServiceChargeProductId(value);
	}

	public readonly struct TippingProductId
	{
		private int Value { get; }

		private TippingProductId(int value) => this.Value = value;

		public static implicit operator int(TippingProductId tippingProductId) => tippingProductId.Value;

		public static implicit operator TippingProductId(int value) => new TippingProductId(value);
	}

	public readonly struct TippingActive
	{
		private bool Value { get; }

		private TippingActive(bool value) => this.Value = value;

		public static implicit operator bool(TippingActive tippingActive) => tippingActive.Value;

		public static implicit operator TippingActive(bool value) => new TippingActive(value);
	}

	public readonly struct CustomerDetailsNameRequired
	{
		private bool Value { get; }

		private CustomerDetailsNameRequired(bool value) => this.Value = value;

		public static implicit operator bool(CustomerDetailsNameRequired CustomerDetailsNameRequired) => CustomerDetailsNameRequired.Value;

		public static implicit operator CustomerDetailsNameRequired(bool value) => new CustomerDetailsNameRequired(value);
	}

	public readonly struct CustomerDetailsEmailRequired
	{
		private bool Value { get; }

		private CustomerDetailsEmailRequired(bool value) => this.Value = value;

		public static implicit operator bool(CustomerDetailsEmailRequired CustomerDetailsEmailRequired) => CustomerDetailsEmailRequired.Value;

		public static implicit operator CustomerDetailsEmailRequired(bool value) => new CustomerDetailsEmailRequired(value);
	}

	public readonly struct CustomerDetailsPhoneRequired
	{
		private bool Value { get; }

		private CustomerDetailsPhoneRequired(bool value) => this.Value = value;

		public static implicit operator bool(CustomerDetailsPhoneRequired CustomerDetailsPhoneRequired) => CustomerDetailsPhoneRequired.Value;

		public static implicit operator CustomerDetailsPhoneRequired(bool value) => new CustomerDetailsPhoneRequired(value);
	}
}