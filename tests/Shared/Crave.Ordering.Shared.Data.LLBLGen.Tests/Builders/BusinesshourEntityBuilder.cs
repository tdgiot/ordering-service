﻿using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Ordering.Shared.Data.LLBLGen.Tests.Builders
{
    public class BusinesshourEntityBuilder : EntityBuilder<BusinesshourEntity, BusinesshourEntityBuilder.BusinesshourEntityData, BusinesshourEntityBuilder>
    {
        public BusinesshourEntityBuilder() : base(Initialise())
        {

        }

        private BusinesshourEntityBuilder(bool opening, string dayOfWeekAndTime, bool isNew) : base(new BusinesshourEntityData
        {
            Opening = opening,
            DayOfWeekAndTime = dayOfWeekAndTime
        }, isNew)
        { }

        private BusinesshourEntityBuilder(BusinesshourEntityData entityData, bool isNew) : base(entityData, isNew)
        {

        }

        private new static BusinesshourEntityData Initialise() =>
            new BusinesshourEntityData
            {
                Opening = true,
                DayOfWeekAndTime = "01000"
            };

        public class BusinesshourEntityData
        {
            public bool Opening { get; set; }
            public string DayOfWeekAndTime { get; set; }
        }

        public override BusinesshourEntityBuilder AsNew() => new BusinesshourEntityBuilder(EntityData, true);

        protected override void EnrichEntity(BusinesshourEntity entity, BusinesshourEntityData entityData)
        {
            entity.Opening = entityData.Opening;
            entity.DayOfWeekAndTime = entityData.DayOfWeekAndTime;
        }

        public BusinesshourEntityBuilder W(bool opening) => new BusinesshourEntityBuilder(opening, EntityData.DayOfWeekAndTime, IsNew);

        public BusinesshourEntityBuilder W(string dayOfWeekAndTime) => new BusinesshourEntityBuilder(EntityData.Opening, dayOfWeekAndTime, IsNew);
    }
}