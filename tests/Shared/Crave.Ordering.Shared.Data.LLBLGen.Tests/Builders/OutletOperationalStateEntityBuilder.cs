﻿using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Ordering.Shared.Data.LLBLGen.Tests.Builders
{
    public class OutletOperationalStateEntityBuilder : EntityBuilder<OutletOperationalStateEntity, OutletOperationalStateEntityBuilder.OutletOperationalStateEntityData, OutletOperationalStateEntityBuilder>
    {
        public OutletOperationalStateEntityBuilder() : base(Initialise())
        {

        }

        public OutletOperationalStateEntityBuilder(bool orderIntakeDisabled, bool isNew) : base(new OutletOperationalStateEntityData
        {
            OrderIntakeDisabled = orderIntakeDisabled
        }, isNew)
        {

        }

        private new static OutletOperationalStateEntityData Initialise() => new OutletOperationalStateEntityData();

        public override OutletOperationalStateEntityBuilder AsNew() => new OutletOperationalStateEntityBuilder(default, true);

        protected override void EnrichEntity(OutletOperationalStateEntity entity, OutletOperationalStateEntityData entityData)
        {
            entity.OrderIntakeDisabled = entityData.OrderIntakeDisabled;
        }

        public class OutletOperationalStateEntityData
        {
            public bool OrderIntakeDisabled { get; set; }
        }

        public OutletOperationalStateEntityBuilder W(bool orderIntakeDisabled) => new OutletOperationalStateEntityBuilder(orderIntakeDisabled, IsNew);
    }
}