﻿using AutoFixture;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Ordering.Shared.Data.LLBLGen.Tests.Builders
{
    public class ClientConfigurationEntityBuilder : EntityBuilder<ClientConfigurationEntity, ClientConfigurationEntityBuilder.ClientConfigurationData, ClientConfigurationEntityBuilder>
    {
        public ClientConfigurationEntityBuilder() : base(Initialise())
        {
        }

        private ClientConfigurationEntityBuilder(bool isNew) : base(Initialise(), isNew)
        {
        }

        private ClientConfigurationEntityBuilder(ClientConfigurationData configEntityData, bool isNew) : base(configEntityData, isNew)
        {
        }

        private new static ClientConfigurationData Initialise()
        {
            Fixture fixture = new Fixture();
            fixture.Register(() => new ClientConfigurationRouteEntityBuilder().Build());
            return fixture.Create<ClientConfigurationData>();
        }

        public override ClientConfigurationEntityBuilder AsNew() => new ClientConfigurationEntityBuilder(EntityData, true);

        public ClientConfigurationEntityBuilder WithClientConfigurationRoute(ClientConfigurationRouteEntityBuilder route) => new ClientConfigurationEntityBuilder(new ClientConfigurationData(EntityData)
        {
            Route = route.Build()
        }, IsNew);

        protected override void EnrichEntity(ClientConfigurationEntity entity, ClientConfigurationData entityData)
        {
            entity.ClientConfigurationRouteCollection.Add(entityData.Route);
        }

        public class ClientConfigurationData
        {
            public ClientConfigurationData()
            {

            }

            public ClientConfigurationData(ClientConfigurationData old)
            {
                Route = old.Route;
            }
            public ClientConfigurationRouteEntity Route { get; set; }
        }

    }
}