﻿using AutoFixture;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Ordering.Shared.Data.LLBLGen.Tests.Builders
{
    public class AlterationoptionTagEntityBuilder : EntityBuilder<AlterationoptionTagEntity, AlterationoptionTagEntityBuilder.AlterationoptionTagEntityData, AlterationoptionTagEntityBuilder>
    {
        public AlterationoptionTagEntityBuilder()
            : base(Initialise(), true)
        {
        }

        private AlterationoptionTagEntityBuilder(bool isNew)
            : base(Initialise(), isNew)
        {
        }

        private AlterationoptionTagEntityBuilder(AlterationoptionTagEntityData alterationoptionTagEntityData, bool isNew)
            : base(alterationoptionTagEntityData, isNew)
        {
        }

        private new static AlterationoptionTagEntityData Initialise()
        {
            Fixture fixture = new Fixture();

            return fixture.Create<AlterationoptionTagEntityData>();
        }

        public override AlterationoptionTagEntityBuilder AsNew() => new AlterationoptionTagEntityBuilder(true);

        public AlterationoptionTagEntityBuilder WithAlterationoptionTagId(int alterationoptionTagId) => new AlterationoptionTagEntityBuilder(
            new AlterationoptionTagEntityData
            {
                AlterationoptionTagId = alterationoptionTagId,
                AlterationoptionId = EntityData.AlterationoptionId,
                TagId = EntityData.TagId
            }, IsNew);

        public AlterationoptionTagEntityBuilder WithAlterationoptionId(int alterationoptionId) => new AlterationoptionTagEntityBuilder(
            new AlterationoptionTagEntityData
            {
                AlterationoptionTagId = EntityData.AlterationoptionTagId,
                AlterationoptionId = alterationoptionId,
                TagId = EntityData.TagId
            }, IsNew);

        public AlterationoptionTagEntityBuilder WithTagId(int tagId) => new AlterationoptionTagEntityBuilder(
            new AlterationoptionTagEntityData
            {
                AlterationoptionTagId = EntityData.AlterationoptionTagId,
                AlterationoptionId = EntityData.AlterationoptionId,
                TagId = tagId
            }, IsNew);

        protected override void EnrichEntity(AlterationoptionTagEntity entity, AlterationoptionTagEntityData entityData)
        {
            entity.AlterationoptionTagId = entityData.AlterationoptionTagId;
            entity.AlterationoptionId = entityData.AlterationoptionId;
            entity.TagId = entityData.TagId;
        }

        public class AlterationoptionTagEntityData
        {
            public int AlterationoptionTagId { get; set; }
            public int AlterationoptionId { get; set; }
            public int TagId { get; set; }
        }
    }
}
