﻿using AutoFixture;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.Tests.Builders;

namespace Crave.Ordering.Api.Domain.Tests.Builders.EntityBuilders
{
	public class DeliveryInformationEntityBuilder : EntityBuilder<DeliveryInformationEntity, DeliveryInformationEntityBuilder.DeliveryInformationEntityData, DeliveryInformationEntityBuilder>
	{
		public override DeliveryInformationEntityBuilder AsNew() => new DeliveryInformationEntityBuilder(EntityData, true);

		public DeliveryInformationEntityBuilder() : base(DeliveryInformationEntityBuilder.Initialise())
		{
			
		}

		private DeliveryInformationEntityBuilder(DeliveryInformationEntityData deliveryInformationEntityData, bool isNew) : base(deliveryInformationEntityData, isNew)
		{
			
		}

		private new static DeliveryInformationEntityData Initialise()
		{
			var fixture = new Fixture();
			fixture.Register(() =>(Address)fixture.Create<string>().Substring(0, 32));
			fixture.Register(() =>(Zipcode)fixture.Create<string>().Substring(0, 10));
			fixture.Register(() =>(City)fixture.Create<string>().Substring(0, 32));
			fixture.Register(() =>(Longitude)fixture.Create<double>());
			fixture.Register(() =>(Latitude)fixture.Create<double>());
			return fixture.Create<DeliveryInformationEntityData>();
		}

		protected override void EnrichEntity(DeliveryInformationEntity entity, DeliveryInformationEntityData entityData)
		{
			entity.Address = entityData.Address;
			entity.Zipcode = entityData.Zipcode;
			entity.City = entityData.City;
			entity.Longitude = entityData.Longitude;
			entity.Latitude = entityData.Latitude;
		}

		public DeliveryInformationEntityBuilder W(Address address) => new DeliveryInformationEntityBuilder(new DeliveryInformationEntityData
		{
			Address = address,
			Zipcode = EntityData.Zipcode,
			City = EntityData.City,
			Longitude = EntityData.Longitude,
			Latitude = EntityData.Latitude
		}, IsNew);

		public DeliveryInformationEntityBuilder W(Zipcode zipcode) => new DeliveryInformationEntityBuilder(new DeliveryInformationEntityData
		{
			Address = EntityData.Address,
			Zipcode = zipcode,
			City = EntityData.City,
			Longitude = EntityData.Longitude,
			Latitude = EntityData.Latitude
		}, IsNew);

		public DeliveryInformationEntityBuilder W(City city) => new DeliveryInformationEntityBuilder(new DeliveryInformationEntityData
		{
			Address = EntityData.Address,
			Zipcode = EntityData.Zipcode,
			City = city,
			Longitude = EntityData.Longitude,
			Latitude = EntityData.Latitude
		}, IsNew);

		public DeliveryInformationEntityBuilder W(Longitude longitude) => new DeliveryInformationEntityBuilder(new DeliveryInformationEntityData
		{
			Address = EntityData.Address,
			Zipcode = EntityData.Zipcode,
			City = EntityData.City,
			Longitude = longitude,
			Latitude = EntityData.Latitude
		}, IsNew);

		public DeliveryInformationEntityBuilder W(Latitude latitude) => new DeliveryInformationEntityBuilder(new DeliveryInformationEntityData
		{
			Address = EntityData.Address,
			Zipcode = EntityData.Zipcode,
			City = EntityData.City,
			Longitude = EntityData.Longitude,
			Latitude = latitude
		}, IsNew);

		public class DeliveryInformationEntityData
		{
			public Address Address { get; set; }
			public Zipcode Zipcode { get; set; }
			public City City { get; set; }
			public Longitude Longitude { get; set; }
			public Latitude Latitude { get; set; }
		}

	}

	public class Address
	{
		private string Value { get; }

		private Address(string value) => Value = value;

		public static implicit operator string(Address address) => address.Value;
		public static implicit operator Address(string value) => new Address(value);
	}

	public class Zipcode
	{
		private string Value { get; }

		private Zipcode(string value) => Value = value;

		public static implicit operator string(Zipcode zipcode) => zipcode.Value;
		public static implicit operator Zipcode(string value) => new Zipcode(value);
	}

	public class City
	{
		private string Value { get; }

		private City(string value) => Value = value;

		public static implicit operator string(City city) => city.Value;
		public static implicit operator City(string value) => new City(value);
	}

	public class Longitude
	{
		private double Value { get; }

		private Longitude(double value) => Value = value;

		public static implicit operator double(Longitude longitude) => longitude.Value;
		public static implicit operator Longitude(double value) => new Longitude(value);
	}

	public class Latitude
	{
		private double Value { get; }

		private Latitude(double value) => Value = value;

		public static implicit operator double(Latitude latitude) => latitude.Value;
		public static implicit operator Latitude(double value) => new Latitude(value);
	}
}