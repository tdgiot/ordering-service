﻿using AutoFixture;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.Tests.Builders;

namespace Crave.Ordering.Api.Domain.Tests.Builders.EntityBuilders
{
    public class AlterationoptionEntityBuilder : EntityBuilder<AlterationoptionEntity, AlterationoptionEntityBuilder.AlterationoptionEntityData, AlterationoptionEntityBuilder>
	{
        public AlterationoptionEntityBuilder() : base(Initialise())
        {
        }

        public AlterationoptionEntityBuilder(bool isNew) : base(Initialise(), isNew)
        {
        }

        private AlterationoptionEntityBuilder(AlterationoptionEntityData alterationoptionEntityData) : base(alterationoptionEntityData)
        {
        }

        public new static AlterationoptionEntityData Initialise()
        {
            Fixture fixture = new Fixture();
            return fixture.Create<AlterationoptionEntityData>();
        }

        public AlterationoptionEntityBuilder WithPriceIn(decimal? priceIn) => new AlterationoptionEntityBuilder(new AlterationoptionEntityData(this.EntityData)
        {
            PriceIn = priceIn
        });

        public override AlterationoptionEntityBuilder AsNew() => new AlterationoptionEntityBuilder(true);

        protected override void EnrichEntity(AlterationoptionEntity entity, AlterationoptionEntityData entityData)
        {
            entity.PriceIn = entityData.PriceIn;
        }

        public class AlterationoptionEntityData
        {
            public AlterationoptionEntityData()
            {
            }

            public AlterationoptionEntityData(AlterationoptionEntityData data)
            {
                this.PriceIn = data.PriceIn;
            }

            public decimal? PriceIn { get; set; }
        }
	}
}