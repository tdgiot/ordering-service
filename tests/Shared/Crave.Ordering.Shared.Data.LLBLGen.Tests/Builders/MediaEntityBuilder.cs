﻿using AutoFixture;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using System.Collections.Generic;

namespace Crave.Ordering.Shared.Data.LLBLGen.Tests.Builders
{
    public class MediaEntityBuilder : EntityBuilder<MediaEntity, MediaEntityBuilder.MediaEntityData, MediaEntityBuilder>
    {
        public MediaEntityBuilder() : base(Initialise(), true)
        {
        }

        private MediaEntityBuilder(bool isNew) : base(Initialise(), isNew)
        {
        }

        private MediaEntityBuilder(MediaEntityData mediaEntityData, bool isNew) : base(mediaEntityData, isNew)
        {
        }

        public MediaEntityBuilder W(int mediaId) => new MediaEntityBuilder(new MediaEntityData(EntityData)
        {
            MediaId = mediaId
        }, IsNew);

        public MediaEntityBuilder W(MediaRatioTypeMediaEntityBuilder mediaRatioTypeMediaEntityBuilder) => new MediaEntityBuilder(new MediaEntityData(EntityData)
        {
            MediaRatioTypeMediaCollection = new[] { mediaRatioTypeMediaEntityBuilder.Build() }
        }, IsNew);

        public MediaEntityBuilder WithName(string name) => new MediaEntityBuilder(new MediaEntityData(EntityData)
        {
            Name = name
        }, IsNew);

        public MediaEntityBuilder WithMimeType(string mimeType) => new MediaEntityBuilder(new MediaEntityData(EntityData)
        {
            MimeType = mimeType
        }, IsNew);

        public MediaEntityBuilder WithFilePathRelativeToMediaPath(string filePathRelativeToMediaPath) => new MediaEntityBuilder(new MediaEntityData(EntityData)
        {
            FilePathRelativeToMediaPath = filePathRelativeToMediaPath
        }, IsNew);

        private new static MediaEntityData Initialise()
        {
            Fixture fixture = new Fixture();
            fixture.Register(() => new MediaRatioTypeMediaEntityBuilder().Build());
            return fixture.Create<MediaEntityData>();
        }

        public override MediaEntityBuilder AsNew() => new MediaEntityBuilder(true);

        protected override void EnrichEntity(MediaEntity entity, MediaEntityData entityData)
        {
            entity.MediaId = entityData.MediaId;
            entity.Name = entityData.Name;
            entity.MimeType = entityData.MimeType;
            entity.FilePathRelativeToMediaPath = entityData.FilePathRelativeToMediaPath;
            entity.MediaRatioTypeMediaCollection.AddRange(entityData.MediaRatioTypeMediaCollection);
        }

        public class MediaEntityData
        {
            public MediaEntityData()
            {

            }

            public MediaEntityData(MediaEntityData old)
            {
                MediaId = old.MediaId;
                Name = old.Name;
                MimeType = old.MimeType;
                FilePathRelativeToMediaPath = old.FilePathRelativeToMediaPath;
                MediaRatioTypeMediaCollection = old.MediaRatioTypeMediaCollection;
            }

            public int MediaId { get; set; }
            public string Name { get; set; }
            public string MimeType { get; set; }
            public string FilePathRelativeToMediaPath { get; set; }
            public IEnumerable<MediaRatioTypeMediaEntity> MediaRatioTypeMediaCollection { get; set; }
        }
    }
}
