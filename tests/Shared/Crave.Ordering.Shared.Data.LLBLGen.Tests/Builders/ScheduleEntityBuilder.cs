﻿using System.Collections.Generic;
using System.Linq;
using Crave.Ordering.Api.Domain.Tests.Builders.EntityBuilders;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Ordering.Shared.Data.LLBLGen.Tests.Builders
{
    public class ScheduleEntityBuilder
    {
        private int ScheduleId { get; }

        private IEnumerable<ScheduleitemEntity> ScheduleitemEntities { get; }

        private IEnumerable<ProductEntity> ProductEntities { get; }

        public ScheduleEntityBuilder() : this(0, Enumerable.Empty<ScheduleitemEntity>(), Enumerable.Empty<ProductEntity>())
        {

        }

        private ScheduleEntityBuilder(int scheduleId, IEnumerable<ScheduleitemEntity> scheduleitemEntities, IEnumerable<ProductEntity> productEntities)
        {
            ScheduleId = scheduleId;
            ScheduleitemEntities = scheduleitemEntities;
            ProductEntities = productEntities;
        }

        public ScheduleEntity Build()
        {
            var scheduleEntity = new ScheduleEntity();
            scheduleEntity.ScheduleId = ScheduleId;
            scheduleEntity.ScheduleitemCollection.AddRange(ScheduleitemEntities);
            scheduleEntity.ProductCollection.AddRange(ProductEntities);
            scheduleEntity.IsNew = !ScheduleitemEntities.Any();

            return scheduleEntity;
        }

        public ScheduleEntityBuilder W(ScheduleitemEntityBuilder scheduleitemEntityBuilder) => new ScheduleEntityBuilder(ScheduleId, new[] { scheduleitemEntityBuilder.Build() }, ProductEntities);

        public ScheduleEntityBuilder W(ProductEntityBuilder productEntityBuilder) => new ScheduleEntityBuilder(ScheduleId, ScheduleitemEntities, new[] { productEntityBuilder.Build() });

        public ScheduleEntityBuilder W(int scheduleId) => new ScheduleEntityBuilder(scheduleId, ScheduleitemEntities, ProductEntities);
    }
}
