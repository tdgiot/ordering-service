﻿using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.Tests.Builders;

namespace Crave.Ordering.Api.Domain.Tests.Builders.EntityBuilders
{
	public class DeliveryDistanceEntityBuilder : EntityBuilder<DeliveryDistanceEntity, DeliveryDistanceEntityBuilder.DeliveryDistanceEntityData, DeliveryDistanceEntityBuilder>
	{
		public DeliveryDistanceEntityBuilder() : base(DeliveryDistanceEntityBuilder.Initialise())
		{
			
		}

		private DeliveryDistanceEntityBuilder(DeliveryDistanceEntityData entityData, bool isNew) : base(entityData, isNew)
		{
			
		}

		private new static DeliveryDistanceEntityData Initialise() =>
			new DeliveryDistanceEntityData
			{
				MaximumInMetres = 0,
				Price = 0M
			};

		public override DeliveryDistanceEntityBuilder AsNew() => new DeliveryDistanceEntityBuilder(EntityData, true);

		protected override void EnrichEntity(DeliveryDistanceEntity entity, DeliveryDistanceEntityData entityData)
		{
			entity.MaximumInMetres = entityData.MaximumInMetres;
			entity.Price = entityData.Price;
		}

		public DeliveryDistanceEntityBuilder W(int maximumInMetres) => new DeliveryDistanceEntityBuilder(new DeliveryDistanceEntityData
		{
			MaximumInMetres = maximumInMetres,
			Price = EntityData.Price
		}, IsNew);

		public DeliveryDistanceEntityBuilder W(decimal price) => new DeliveryDistanceEntityBuilder(new DeliveryDistanceEntityData
		{
			MaximumInMetres = EntityData.MaximumInMetres,
			Price = price
		}, IsNew);

		public class DeliveryDistanceEntityData
		{
			public int MaximumInMetres { get; set; }
			public decimal Price { get; set; }
		}
	}
}