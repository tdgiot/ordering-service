﻿using System.ComponentModel.DataAnnotations;
using AutoFixture;
using Crave.Ordering.Api.Domain.Tests.Builders.EntityBuilders;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Ordering.Shared.Data.LLBLGen.Tests.Builders
{
    public class DeliverypointEntityBuilder : EntityBuilder<DeliverypointEntity, DeliverypointEntityBuilder.DeliverypointEntityData, DeliverypointEntityBuilder>
    {

        public DeliverypointEntityBuilder() : base(Initialise())
        {

        }

        public DeliverypointEntityBuilder(bool isNew) : base(Initialise(), isNew)
        {

        }

        private DeliverypointEntityBuilder(DeliverypointEntityData data) : base(data)
        {

        }

        public override DeliverypointEntityBuilder AsNew() => new DeliverypointEntityBuilder(true);

        public DeliverypointEntityBuilder WithId(int id) => new DeliverypointEntityBuilder(new DeliverypointEntityData(EntityData)
        {
            DeliverypointId = id
        });

        public DeliverypointEntityBuilder WithDeliverypointgroup(DeliverypointgroupEntityBuilder entity) => new DeliverypointEntityBuilder(new DeliverypointEntityData(EntityData)
        {
            Deliverypointgroup = entity.Build()
        });

        private new static DeliverypointEntityData Initialise()
        {
            var fixture = new Fixture();
            fixture.Register(() => new DeliverypointgroupEntity());
            fixture.Register(() => new CompanyEntityBuilder().Build());
            return fixture.Create<DeliverypointEntityData>();
        }

        protected override void EnrichEntity(DeliverypointEntity entity, DeliverypointEntityData entityData)
        {
            entity.DeliverypointId = entityData.DeliverypointId;
            entity.Name = entityData.DeliverypointName;
            entity.Number = entityData.DeliverypointNumber;
            entity.DeliverypointgroupId = entityData.Deliverypointgroup?.DeliverypointgroupId ?? 0;
            entity.Deliverypointgroup = entityData.Deliverypointgroup;
            entity.Company = entityData.Company;
        }

        public class DeliverypointEntityData
        {
            public DeliverypointEntityData()
            {

            }

            public DeliverypointEntityData(DeliverypointEntityData old)
            {
                DeliverypointId = old.DeliverypointId;
                DeliverypointName = old.DeliverypointName;
                DeliverypointNumber = old.DeliverypointNumber;
                Deliverypointgroup = old.Deliverypointgroup;
                Company = old.Company;
            }

            public int DeliverypointId { get; set; }

            [StringLength(40)]
            public string DeliverypointName { get; set; }

            [StringLength(20)]
            public string DeliverypointNumber { get; set; }

            public DeliverypointgroupEntity Deliverypointgroup { get; set; }

            public CompanyEntity Company { get; set; }
        }

    }
}