﻿namespace Crave.Ordering.Shared.Data.LLBLGen.Tests.Structs
{
    public readonly struct OutletId
    {
        private int Value { get; }

        private OutletId(int value) => Value = value;

        public static implicit operator int(OutletId outletId) => outletId.Value;

        public static implicit operator OutletId(int value) => new OutletId(value);
    }
}