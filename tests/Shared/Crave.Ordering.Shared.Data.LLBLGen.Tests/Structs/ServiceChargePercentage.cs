﻿namespace Crave.Ordering.Shared.Data.LLBLGen.Tests.Structs
{
    public readonly struct ServiceChargePercentage
    {
        private double? Value { get; }

        private ServiceChargePercentage(double? value) => Value = value;

        public static implicit operator double?(ServiceChargePercentage serviceChargeAmount) => serviceChargeAmount.Value;

        public static implicit operator ServiceChargePercentage(double? value) => new ServiceChargePercentage(value);
    }
}