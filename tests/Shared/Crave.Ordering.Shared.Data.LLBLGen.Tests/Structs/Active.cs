﻿namespace Crave.Ordering.Shared.Data.LLBLGen.Tests.Structs
{
    public readonly struct Active
    {
        private bool Value { get; }

        private Active(bool value) => Value = value;

        public static implicit operator bool(Active active) => active.Value;

        public static implicit operator Active(bool value) => new Active(value);
    }
}