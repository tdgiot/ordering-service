﻿namespace Crave.Ordering.Shared.Data.LLBLGen.Tests.Structs
{
    public readonly struct MinimumAmountForFreeServiceCharge
    {
        private decimal Value { get; }

        private MinimumAmountForFreeServiceCharge(decimal value) => Value = value;

        public static implicit operator decimal(MinimumAmountForFreeServiceCharge serviceChargeAmount) => serviceChargeAmount.Value;

        public static implicit operator MinimumAmountForFreeServiceCharge(decimal value) => new MinimumAmountForFreeServiceCharge(value);
    }
}