﻿namespace Crave.Ordering.Shared.Data.LLBLGen.Tests.Structs
{
    public readonly struct ServiceChargeAmount
    {
        private decimal? Value { get; }

        private ServiceChargeAmount(decimal? value) => Value = value;

        public static implicit operator decimal?(ServiceChargeAmount serviceChargeAmount) => serviceChargeAmount.Value;

        public static implicit operator ServiceChargeAmount(decimal? value) => new ServiceChargeAmount(value);
    }
}