namespace Crave.Ordering.Shared.Data.LLBLGen.Tests.DomainTypes
{
    public readonly struct TaxTariffId
    {
        private int Value { get; }

        private TaxTariffId(int value) => this.Value = value;

        public static implicit operator int(TaxTariffId taxTariffId) => taxTariffId.Value;

        public static implicit operator TaxTariffId(int value) => new TaxTariffId(value);
    }
}