namespace Crave.Ordering.Shared.Data.LLBLGen.Tests.DomainTypes
{
    public class PriceInTax
    {
        private decimal Value { get; }

        private PriceInTax(decimal value) => this.Value = value;

        public static implicit operator decimal(PriceInTax priceInTax) => priceInTax.Value;

        public static implicit operator PriceInTax(decimal value) => new PriceInTax(value);
    }
}