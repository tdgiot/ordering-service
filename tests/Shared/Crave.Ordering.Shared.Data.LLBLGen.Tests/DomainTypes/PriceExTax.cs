namespace Crave.Ordering.Shared.Data.LLBLGen.Tests.DomainTypes
{
    public class PriceExTax
    {
        private decimal Value { get; }

        private PriceExTax(decimal value) => this.Value = value;

        public static implicit operator decimal(PriceExTax priceExTax) => priceExTax.Value;

        public static implicit operator PriceExTax(decimal value) => new PriceExTax(value);
    }
}