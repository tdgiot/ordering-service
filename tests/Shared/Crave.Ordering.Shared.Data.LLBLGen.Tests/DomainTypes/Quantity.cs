namespace Crave.Ordering.Shared.Data.LLBLGen.Tests.DomainTypes
{
    public readonly struct Quantity
    {
        private int Value { get; }

        private Quantity(int value) => this.Value = value;

        public static implicit operator int(Quantity quantity) => quantity.Value;

        public static implicit operator Quantity(int value) => new Quantity(value);
    }
}