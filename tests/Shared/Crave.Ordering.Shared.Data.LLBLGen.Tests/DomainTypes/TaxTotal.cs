namespace Crave.Ordering.Shared.Data.LLBLGen.Tests.DomainTypes
{
    public class TaxTotal
    {
        private decimal Value { get; }

        private TaxTotal(decimal value) => this.Value = value;

        public static implicit operator decimal(TaxTotal taxTotal) => taxTotal.Value;

        public static implicit operator TaxTotal(decimal value) => new TaxTotal(value);
    }
}