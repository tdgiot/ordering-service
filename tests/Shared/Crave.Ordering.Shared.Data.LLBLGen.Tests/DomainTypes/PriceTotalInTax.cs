namespace Crave.Ordering.Shared.Data.LLBLGen.Tests.DomainTypes
{
    public class PriceTotalInTax
    {
        private decimal Value { get; }

        private PriceTotalInTax(decimal value) => this.Value = value;

        public static implicit operator decimal(PriceTotalInTax priceTotalInTax) => priceTotalInTax.Value;

        public static implicit operator PriceTotalInTax(decimal value) => new PriceTotalInTax(value);
    }
}