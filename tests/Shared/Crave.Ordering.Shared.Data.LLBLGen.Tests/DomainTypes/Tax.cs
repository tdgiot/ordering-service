namespace Crave.Ordering.Shared.Data.LLBLGen.Tests.DomainTypes
{
    public class Tax
    {
        private decimal Value { get; }

        private Tax(decimal value) => this.Value = value;

        public static implicit operator decimal(Tax tax) => tax.Value;

        public static implicit operator Tax(decimal value) => new Tax(value);
    }
}