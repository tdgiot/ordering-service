namespace Crave.Ordering.Shared.Data.LLBLGen.Tests.DomainTypes
{
    public class PriceTotalExTax
    {
        private decimal Value { get; }

        private PriceTotalExTax(decimal value) => this.Value = value;

        public static implicit operator decimal(PriceTotalExTax priceTotalExTax) => priceTotalExTax.Value;

        public static implicit operator PriceTotalExTax(decimal value) => new PriceTotalExTax(value);
    }
}