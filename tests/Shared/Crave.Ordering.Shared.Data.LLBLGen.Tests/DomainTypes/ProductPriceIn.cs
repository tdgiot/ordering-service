namespace Crave.Ordering.Shared.Data.LLBLGen.Tests.DomainTypes
{
    public class ProductPriceIn
    {
        private decimal Value { get; }

        private ProductPriceIn(decimal value) => this.Value = value;

        public static implicit operator decimal(ProductPriceIn productPriceIn) => productPriceIn.Value;

        public static implicit operator ProductPriceIn(decimal value) => new ProductPriceIn(value);
    }
}