#define SKIP_TEST_LOCALLY

using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Crave.Infrastructure;
using Crave.Infrastructure.Enums;
using Crave.Infrastructure.Models;
using Crave.Ordering.Api.Client;
using Crave.Ordering.IntegrationTests.Requests;
using Crave.Ordering.IntegrationTests.Validation;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.FactoryClasses;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.SQLServer;
using Microsoft.Extensions.Configuration;
using NUnit.Framework;
using SD.LLBLGen.Pro.DQE.SqlServer;
using SD.LLBLGen.Pro.ORMSupportClasses;
using SD.LLBLGen.Pro.QuerySpec;
using SD.LLBLGen.Pro.QuerySpec.Adapter;
using AlterationType = Crave.Enums.AlterationType;
using OrderitemType = Crave.Ordering.Api.Client.OrderitemType;
using OrderRoutestephandlerStatus = Crave.Ordering.Api.Client.OrderRoutestephandlerStatus;
using OrderStatus = Crave.Ordering.Api.Client.OrderStatus;
using RoutestephandlerType = Crave.Enums.RoutestephandlerType;

namespace Crave.Ordering.IntegrationTests
{
    [TestFixture]
    [Explicit]
    public class RestApiOrdersIntegrationTests
    {
        private CraveEnvironment deploymentEnvironment;
        private OrdersClient client;

        public static class A
        {
            public static IConfiguration Configuration => new ConfigurationBuilder().AddJsonFile("appsettings.json").AddUserSecrets<RestApiOrdersIntegrationTests>(true).Build();
            public static CreateOrderRequestDtoBuilder CreateOrderRequestDto => new CreateOrderRequestDtoBuilder();
            public static OrderitemRequestDtoBuilder OrderitemRequestDto => new OrderitemRequestDtoBuilder();
            public static AlterationitemRequestDtoBuilder AlterationitemRequestDto => new AlterationitemRequestDtoBuilder();
            public static Random Random = new Random();
        }

        [OneTimeSetUp]
        public void Setup()
        {
            deploymentEnvironment =
#if DEBUG
            CraveEnvironments.GetDeploymentEnvironment(EnvironmentType.Development);
#else
            CraveEnvironments.GetDeploymentEnvironment();
#endif

            client = new OrdersClient(new HttpClient
            {
                BaseAddress = new Uri($"https://ordering-api.{deploymentEnvironment.DomainNames.External}")
            });

            // Setup database
            IConfiguration configuration = A.Configuration;
            string connectionString = configuration["Database:ConnectionString"];

            RuntimeConfiguration.AddConnectionString(DataAccessAdapter.ConnectionStringKeyName, connectionString);
            RuntimeConfiguration.ConfigureDQE<SQLServerDQEConfiguration>(c =>
            {
                c.AddDbProviderFactory(typeof(SqlClientFactory));
                c.AddCatalogNameOverwrite("*", new SqlConnectionStringBuilder(connectionString).InitialCatalog);
                c.SetTraceLevel(TraceLevel.Warning);
            });
            DynamicQueryEngine.UseNoLockHintOnSelects = true;
        }

        [TestCase(246325, TestName = "Normal product")]
        [TestCase(247329, TestName = "Service request")]
        [TestCase(226624, TestName = "Options_Min_1_Max_1")]
        [TestCase(226625, TestName = "Options_Min_1_Max_3")]
        [TestCase(240536, TestName = "Duplicate_Options")]
        [TestCase(240545, TestName = "Soft Drinks With Alteration Prices")]
        [TestCase(226627, TestName = "DateTime_With_Date_Picker")]
        [TestCase(226629, TestName = "Date_3_Days")]
        [TestCase(240540, TestName = "Form_Combined")]
        [TestCase(226656, TestName = "Product_All")]
        [Category("IntegrationTests")]
        public async Task CreateAndProcessOrder_SingleOrderitem_RestApi(int productId)
        {
#if (DEBUG && SKIP_TEST_LOCALLY)
            Assert.Ignore("Ignored for local debug run");
#endif

            ProductEntity productEntity = await GetProductInformation(productId);
            Assert.That(productEntity, Is.Not.Null);
            Assert.That(productEntity.CompanyId, Is.GreaterThan(0));

            int companyId = productEntity.CompanyId.GetValueOrDefault(0);

            ClientEntity clientEntity = await GetClientWithDeliverypoint(companyId, productEntity.CategoryCollectionViaProductCategory.First().MenuId ?? 0);
            Assert.That(clientEntity, Is.Not.Null);

            // Convert product entity to order item
            OrderitemRequestDtoBuilder orderitem = CreateOrderitemFromProduct(productEntity);

            // Create order request
            CreateOrderRequestDto createOrderRequest = A.CreateOrderRequestDto
                .WithCompanyId(companyId)
                .WithClientId(clientEntity.ClientId)
                .WithDeliverypointId(clientEntity.DeliverypointId)
                .WithOrderitems(orderitem)
                .Build();

            await SubmitAndValidateOrder(createOrderRequest);
        }

        [TestCase(246325, 226625, 240540, TestName = "Order with multiple items")]
        [Category("IntegrationTests")]
        public async Task CreateAndProcessOrder_MultiOrderitem_RestApi(params int[] productIds)
        {
#if (DEBUG && SKIP_TEST_LOCALLY)
            Assert.Ignore("Ignored for local debug run");
#endif
            
            List<ProductEntity> productEntities = new List<ProductEntity>();

            foreach (int productId in productIds)
            {
                ProductEntity productEntity = await GetProductInformation(productId);
                Assert.That(productEntity, Is.Not.Null);
                Assert.That(productEntity.CompanyId, Is.GreaterThan(0));

                productEntities.Add(productEntity);
            }

            int companyId = productEntities.First().CompanyId.GetValueOrDefault(0);

            ClientEntity clientEntity = await GetClientWithDeliverypoint(companyId, productEntities.First().CategoryCollectionViaProductCategory.First().MenuId ?? 0);
            Assert.That(clientEntity, Is.Not.Null);

            // Convert product entity to order item
            IEnumerable<OrderitemRequestDtoBuilder> result = productEntities.Select(CreateOrderitemFromProduct);
            
            // Create order request
            CreateOrderRequestDto createOrderRequest = A.CreateOrderRequestDto
                .WithCompanyId(companyId)
                .WithClientId(clientEntity.ClientId)
                .WithDeliverypointId(clientEntity.DeliverypointId)
                .WithOrderitems(result.ToArray())
                .Build();

            await SubmitAndValidateOrder(createOrderRequest);
        }

        private async Task SubmitAndValidateOrder(CreateOrderRequestDto createOrderRequest)
        {
            try
            {
                // Create new order
                string orderGuid = await client.CreateOrderAsync(createOrderRequest);

                Assert.That(orderGuid, Is.Not.Empty);

                // Fetch order
                OrderDto resultOrder = await GetOrderByGuidAsync(orderGuid, OrderStatus.InRoute);

                // Validate order
                OrderValidation.ValidateOrderWithCreateRequestDto(createOrderRequest, resultOrder);
                
                // Update order routestephandler status
                OrderRoutestephandlerEntity orderRoutestephandlerEntity = await GetOrderRoutestephandlerForOrder(resultOrder.OrderId);
                await client.UpdateOrderRoutestephandlerStatusAsync(orderGuid, orderRoutestephandlerEntity.OrderRoutestephandlerId, new UpdateOrderRoutestephandlerStatusRequestDto
                {
                    Status = OrderRoutestephandlerStatus.Completed,
                    TerminalId = orderRoutestephandlerEntity.TerminalId ?? 0
                });

                // Fetch order again
                OrderDto updatedOrder = await GetOrderByGuidAsync(orderGuid, OrderStatus.Processed);
                Assert.That(updatedOrder.Status, Is.EqualTo(OrderStatus.Processed));

                Assert.Pass($"Order GUID: {orderGuid} (ID: {resultOrder.OrderId})");
            }
            catch (ApiException<ValidationResponseDto> ex)
            {
                Assert.Fail(string.Join(", ", ex.Result.ValidationErrors.Select(x => x.ValidationErrorSubType)));
            }
            catch (ApiException<ErrorResponseDto> ex)
            {
                Assert.Fail(ex.Result.Message);
            }
            catch (ApiException ex)
            {
                Assert.Fail(ex.Message);
            }
        }

        private async Task<OrderDto> GetOrderByGuidAsync(string orderGuid, OrderStatus expectedOrderStatus)
        {
            OrderDto resultOrder = null;
            int fetchCount = 0;
            do
            {
                if (fetchCount >= 20)
                {
                    Assert.Fail($"Takes too long for order '{orderGuid}' to get status '{expectedOrderStatus}' on the server (got status: '{resultOrder?.Status}')");
                }

                Thread.Sleep(1000);
                fetchCount++;
                resultOrder = await client.GetOrderByGuidAsync(orderGuid);
            } while (resultOrder.Status != expectedOrderStatus);

            return resultOrder;
        }

        private async Task<ProductEntity> GetProductInformation(int productId)
        {
            EntityQuery<ProductEntity> query = new QueryFactory().Product
                .Where(ProductFields.ProductId == productId)
                .WithPath(ProductEntity.PrefetchPathCategoryCollectionViaProductCategory,
                    ProductEntity.PrefetchPathProductAlterationCollection
                        .WithSubPath(ProductAlterationEntity.PrefetchPathAlterationEntity
                            .WithSubPath(AlterationEntity.PrefetchPathAlterationitemCollection
                                .WithSubPath(AlterationitemEntity.PrefetchPathAlterationoptionEntity))));

            using DataAccessAdapter adapter = new DataAccessAdapter();
            return await adapter.FetchSingleAsync(query);
        }

        private async Task<ClientEntity> GetClientWithDeliverypoint(int companyId, int menuId)
        {
            EntityQuery<ClientEntity> query = new QueryFactory().Client
                .From(QueryTarget.LeftJoin(ClientEntity.Relations.DeliverypointgroupEntityUsingDeliverypointgroupId))
                .Where(ClientFields.CompanyId == companyId)
                .AndWhere(ClientFields.DeviceId != DBNull.Value)
                .AndWhere(ClientFields.DeliverypointId != DBNull.Value)
                .AndWhere(DeliverypointgroupFields.MenuId == menuId)
                .Limit(1);

            using DataAccessAdapter adapter = new DataAccessAdapter();
            return await adapter.FetchSingleAsync(query);
        }

        private async Task<OrderRoutestephandlerEntity> GetOrderRoutestephandlerForOrder(int orderId)
        {
            EntityQuery<OrderRoutestephandlerEntity> query = new QueryFactory().OrderRoutestephandler
                .Where(OrderRoutestephandlerFields.OrderId == orderId)
                .AndWhere(OrderRoutestephandlerFields.HandlerType == (int)RoutestephandlerType.Console)
                .Limit(1);

            using DataAccessAdapter adapter = new DataAccessAdapter();
            return await adapter.FetchSingleAsync(query);
        }

        private OrderitemRequestDtoBuilder CreateOrderitemFromProduct(ProductEntity product) => A.OrderitemRequestDto
                .WithProductId(product.ProductId)
                .WithCategoryId(product.CategoryCollectionViaProductCategory.First().CategoryId)
                .WithPrice(product.PriceIn.GetValueOrDefault(0))
                .WithQuantity(A.Random.Next(1, 10))
                .WithType(OrderitemType.Product)
                .WithAlterations(CreateOrderitemAlterationsFromProduct(product).ToArray());

        private IList<AlterationitemRequestDtoBuilder> CreateOrderitemAlterationsFromProduct(ProductEntity product)
        {
            if (product.ProductAlterationCollection.Count == 0)
            {
                return new List<AlterationitemRequestDtoBuilder>();
            }

            return new List<AlterationitemRequestDtoBuilder>(product.ProductAlterationCollection.SelectMany(CreateOrderitemAlteration));
        }

        private IEnumerable<AlterationitemRequestDtoBuilder> CreateOrderitemAlteration(ProductAlterationEntity productAlterationEntity)
        {
            AlterationEntity alterationEntity = productAlterationEntity.Alteration;
            AlterationType alterationType = productAlterationEntity.Alteration.Type;

            if (alterationType == AlterationType.Options)
            {
                // Always pick maximum number of options
                int numberOfOptions = alterationEntity.MaxOptions;

                // Get random alteration items with some extra logic in case the number of available alteration items
                // is less then the maximum number of options one can select. Plus this might also give small sets doubles
                int numberOfAvailableIndexes = alterationEntity.AlterationitemCollection.Count - 1;
                bool allowDuplicateOptions = alterationEntity.AllowDuplicates;
                for (int i = 0; i < numberOfOptions; i++)
                {
                    int index = allowDuplicateOptions ? A.Random.Next(0, numberOfAvailableIndexes) : i;
                    AlterationitemEntity alterationitem = alterationEntity.AlterationitemCollection[index];

                    yield return A.AlterationitemRequestDto
                        .WithAlterationId(alterationEntity.AlterationId)
                        .WithAlterationoptionId(alterationitem.AlterationoptionId)
                        .WithPrice(alterationitem.Alterationoption.PriceIn);
                }
            }
            else if (alterationType == AlterationType.DateTime || alterationType == AlterationType.Date)
            {
                yield return A.AlterationitemRequestDto
                    .WithAlterationId(alterationEntity.AlterationId)
                    .WithTimeUtc(DateTime.UtcNow);
            }
            else if (alterationType == AlterationType.Form)
            {
                foreach (AlterationitemEntity alterationitemEntity in alterationEntity.AlterationitemCollection)
                {
                    yield return A.AlterationitemRequestDto
                        .WithAlterationId(alterationEntity.AlterationId)
                        .WithAlterationoptionId(alterationitemEntity.AlterationoptionId)
                        .WithValue($"Value for '{alterationitemEntity.Alterationoption.Name}' goes here!");
                }
            }
        }
    }
}