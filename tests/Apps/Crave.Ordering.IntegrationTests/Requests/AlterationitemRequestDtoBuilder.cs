﻿using System;
using AutoMapper;
using Crave.Ordering.Api.Client;

namespace Crave.Ordering.IntegrationTests.Requests
{
    public class AlterationitemRequestDtoBuilder
    {
        private readonly RequestDtoData requestData;

        public AlterationitemRequestDtoBuilder() : this(new RequestDtoData())
        {

        }

        private AlterationitemRequestDtoBuilder(RequestDtoData requestData)
        {
            this.requestData = requestData;
        }

        public AlterationitemRequestDtoBuilder WithAlterationId(int id) => new AlterationitemRequestDtoBuilder(new RequestDtoData(this.requestData)
        {
            AlterationId = id
        });

        public AlterationitemRequestDtoBuilder WithAlterationoptionId(int id) => new AlterationitemRequestDtoBuilder(new RequestDtoData(this.requestData)
        {
            AlterationoptionId = id
        });
        
        public AlterationitemRequestDtoBuilder WithPrice(decimal? price) => new AlterationitemRequestDtoBuilder(new RequestDtoData(this.requestData)
        {
            Price = price
        });

        public AlterationitemRequestDtoBuilder WithValue(string value) => new AlterationitemRequestDtoBuilder(new RequestDtoData(this.requestData)
        {
            Value = value
        });

        public AlterationitemRequestDtoBuilder WithTimeUtc(DateTime value) => new AlterationitemRequestDtoBuilder(new RequestDtoData(this.requestData)
        {
            TimeUTC = value
        });

        public AlterationitemRequestDtoBuilder WithPriceLevelItemId(int id) => new AlterationitemRequestDtoBuilder(new RequestDtoData(this.requestData)
        {
            PriceLevelItemId = id
        });

        public AlterationitemRequestDto Build()
        {
            var config = new MapperConfiguration(cfg => cfg.CreateMap<RequestDtoData, AlterationitemRequestDto>());
            return new Mapper(config).Map<AlterationitemRequestDto>(requestData);
        }

        public class RequestDtoData
        {
            public RequestDtoData() {}

            public RequestDtoData(RequestDtoData old)
            {
                this.AlterationId = old.AlterationId;
                this.AlterationoptionId = old.AlterationoptionId;
                this.Price = old.Price;
                this.Value = old.Value;
                this.TimeUTC = old.TimeUTC;
                this.PriceLevelItemId = old.PriceLevelItemId;
            }

            public int AlterationId { get; set; }
            public int? AlterationoptionId { get; set; }
            public decimal? Price { get; set; }
            public string Value { get; set; }
            public DateTime? TimeUTC { get; set; }
            public int? PriceLevelItemId { get; set; }
        }
    }
}