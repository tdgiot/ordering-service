﻿using System.Collections.Generic;
using System.Linq;
using AutoFixture;
using AutoMapper;
using Crave.Ordering.Api.Client;

namespace Crave.Ordering.IntegrationTests.Requests
{
    public class CreateOrderRequestDtoBuilder
    {
        private readonly RequestDtoData requestData;

        public CreateOrderRequestDtoBuilder() : this(Initialize())
        {
            
        }

        private CreateOrderRequestDtoBuilder(RequestDtoData requestData)
        {
            this.requestData = requestData;
        }

        public CreateOrderRequestDtoBuilder WithCompanyId(int id) => new CreateOrderRequestDtoBuilder(new RequestDtoData(requestData)
        {
            CompanyId = id
        });

        public CreateOrderRequestDtoBuilder WithClientId(int id) => new CreateOrderRequestDtoBuilder(new RequestDtoData(requestData)
        {
            ClientId = id
        });

        public CreateOrderRequestDtoBuilder WithDeliverypointId(int? id) => new CreateOrderRequestDtoBuilder(new RequestDtoData(requestData)
        {
            DeliverypointId = id
        });

        public CreateOrderRequestDtoBuilder WithOrderitems(params OrderitemRequestDtoBuilder[] orderitems) => new CreateOrderRequestDtoBuilder(new RequestDtoData(requestData)
        {
            Orderitems = new List<OrderitemRequestDto>(orderitems.Select(o => o.Build()))
        });

        public CreateOrderRequestDto Build()
        {
            var config = new MapperConfiguration(cfg => cfg.CreateMap<RequestDtoData, CreateOrderRequestDto>());
            return new Mapper(config).Map<CreateOrderRequestDto>(requestData);
        }

        private static RequestDtoData Initialize()
        {
            var fixture = new Fixture();
            fixture.Register(new OrderitemRequestDtoBuilder().Build);

            return fixture.Create<RequestDtoData>();
        }

        public class RequestDtoData
        {
            public RequestDtoData()
            {
            }

            public RequestDtoData(RequestDtoData old)
            {
                this.OrderType = old.OrderType;
                this.Guid = old.Guid;
                this.CompanyId = old.CompanyId;
                this.DeliverypointId = old.DeliverypointId;
                this.ClientId = old.ClientId;
                this.Notes = old.Notes;
            }

            public OrderType OrderType { get; set; } = OrderType.Standard;
            public string Guid { get; set; }
            public int CompanyId { get; set; }
            public int? DeliverypointId { get; set; }
            public int? ClientId { get; set; }
            public string Notes { get; set; } = string.Empty;
            public ICollection<OrderitemRequestDto> Orderitems { get; set; }
        }
    }
}