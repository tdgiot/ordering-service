﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using AutoFixture;
using AutoMapper;
using Crave.Ordering.Api.Client;

namespace Crave.Ordering.IntegrationTests.Requests
{
    public class OrderitemRequestDtoBuilder
    {
        private readonly RequestDtoData requestData;

        public OrderitemRequestDtoBuilder() : this(Initialize())
        {

        }

        private OrderitemRequestDtoBuilder(RequestDtoData requestData)
        {
            this.requestData = requestData;
        }

        public OrderitemRequestDtoBuilder WithType(OrderitemType type) => new OrderitemRequestDtoBuilder(new RequestDtoData(requestData)
        {
            Type = type
        });

        public OrderitemRequestDtoBuilder WithProductId(int id) => new OrderitemRequestDtoBuilder(new RequestDtoData(requestData)
        {
            ProductId = id
        });

        public OrderitemRequestDtoBuilder WithCategoryId(int id) => new OrderitemRequestDtoBuilder(new RequestDtoData(requestData)
        {
            CategoryId = id
        });

        public OrderitemRequestDtoBuilder WithPrice(decimal price) => new OrderitemRequestDtoBuilder(new RequestDtoData(requestData)
        {
            Price = price
        });

        public OrderitemRequestDtoBuilder WithQuantity(int quantity) => new OrderitemRequestDtoBuilder(new RequestDtoData(requestData)
        {
            Quantity = quantity
        });

        public OrderitemRequestDtoBuilder WithNotes(string notes) => new OrderitemRequestDtoBuilder(new RequestDtoData(requestData)
        {
            Notes = notes
        });

        public OrderitemRequestDtoBuilder WithAlterations(params AlterationitemRequestDtoBuilder[] alterations) => new OrderitemRequestDtoBuilder(new RequestDtoData(requestData)
        {
            Alterationitems = alterations.Select(x => x.Build()).ToList()
        });
        
        public OrderitemRequestDto Build()
        {
            var config = new MapperConfiguration(cfg => cfg.CreateMap<RequestDtoData, OrderitemRequestDto>());
            return new Mapper(config).Map<OrderitemRequestDto>(requestData);
        }

        public static RequestDtoData Initialize()
        {
            return new RequestDtoData();
        }

        public class RequestDtoData
        {
            public RequestDtoData()
            {
                this.Guid = System.Guid.NewGuid().ToString("D");
            }

            public RequestDtoData(RequestDtoData old)
            {
                this.Type = old.Type;
                this.Guid = old.Guid;
                this.ProductId = old.ProductId;
                this.CategoryId = old.CategoryId;
                this.Quantity = old.Quantity;
                this.Price = old.Price;
                this.Notes = old.Notes;
                this.Alterationitems = old.Alterationitems;
            }

            public OrderitemType Type { get; set; } = OrderitemType.Product;
            public string Guid { get; set; }
            public int ProductId { get; set; }
            public int? CategoryId { get; set; }
            public int Quantity { get; set; } = 1;
            public decimal? Price { get; set; } = (decimal)0.0;
            public string Notes { get; set; }
            public List<AlterationitemRequestDto> Alterationitems { get; set; } = new List<AlterationitemRequestDto>();
        }
    }
}