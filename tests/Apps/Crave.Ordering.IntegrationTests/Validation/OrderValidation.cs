﻿using System;
using System.Collections.Generic;
using System.Linq;
using Crave.Ordering.Api.Client;
using NUnit.Framework;

namespace Crave.Ordering.IntegrationTests.Validation
{
    public static class OrderValidation
    {
        public static void ValidateOrderWithCreateRequestDto(CreateOrderRequestDto requestDto, OrderDto orderDto)
        {
            Assert.That(orderDto.ClientId, Is.EqualTo(requestDto.ClientId));
            Assert.That(orderDto.Deliverypoint.DeliverypointId, Is.EqualTo(requestDto.DeliverypointId));
            
            ValidateOrderitems(requestDto.Orderitems, orderDto.Orderitems);
        }

        private static void ValidateOrderitems(ICollection<OrderitemRequestDto> requestDtos, ICollection<OrderitemDto> orderitemDtos)
        {
            Assert.That(orderitemDtos, Is.Not.Empty);

            foreach (OrderitemRequestDto orderitemRequestDto in requestDtos)
            {
                OrderitemDto orderitemDto = orderitemDtos.First(x => x.Guid == orderitemRequestDto.Guid);

                Assert.That(orderitemDto, Is.Not.Null, "Orderitem with GUID '{0}' not found in orderitem collection from server", orderitemRequestDto.Guid);

                ValidateOrderitemAlterationitems(orderitemRequestDto.Alterationitems, orderitemDto.OrderitemAlterationitems);
            }
        }

        private static void ValidateOrderitemAlterationitems(ICollection<AlterationitemRequestDto> requestDtos, ICollection<OrderitemAlterationitemDto> orderitemAlterationitemDtos)
        {
            foreach (AlterationitemRequestDto alterationitemRequestDto in requestDtos)
            {
                OrderitemAlterationitemDto orderitemAlterationitemDto;
                if (alterationitemRequestDto.AlterationoptionId.HasValue)
                {
                    orderitemAlterationitemDto = orderitemAlterationitemDtos.First(x => x.AlterationId == alterationitemRequestDto.AlterationId &&
                                                                                        x.Alterationitem.AlterationoptionId == alterationitemRequestDto.AlterationoptionId);
                }
                else
                {
                    orderitemAlterationitemDto = orderitemAlterationitemDtos.First(x => x.AlterationId == alterationitemRequestDto.AlterationId);
                }

                Assert.That(orderitemAlterationitemDto, Is.Not.Null, "OrderitemAlteration not found (AlterationId: {0}, AlterationoptionId: {1}", alterationitemRequestDto.AlterationId, alterationitemRequestDto.AlterationoptionId);

                if (alterationitemRequestDto.Price.HasValue)
                {
                    Assert.That(orderitemAlterationitemDto.AlterationoptionPriceIn, Is.EqualTo(alterationitemRequestDto.Price));
                }

                if (alterationitemRequestDto.TimeUTC.HasValue)
                {
                    Assert.That(orderitemAlterationitemDto.TimeUTC, Is.EqualTo(alterationitemRequestDto.TimeUTC));
                }

                if (!alterationitemRequestDto.Value.IsNullOrWhiteSpace())
                {
                    Assert.That(orderitemAlterationitemDto.Value, Is.EqualTo(alterationitemRequestDto.Value));
                }
            }
        }
    }
}