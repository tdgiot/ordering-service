﻿using System.Threading.Tasks;
using Crave.Enums;
using Crave.Ordering.CompleteRoute.Domain.UseCases;
using Crave.Ordering.CompleteRoute.Interfaces.Repositories;
using Crave.Ordering.CompleteRoute.Tests.Builders.RepositoryBuilders;
using Crave.Ordering.Shared.Data.LLBLGen.Tests.Builders;
using NUnit.Framework;

namespace Crave.Ordering.CompleteRoute.Tests.UseCases
{
    [TestFixture]
    public class CheckCompleteOrderProcessedUseCaseTests
    {
        static class A
        {
            public static OrderEntityBuilder OrderEntity => new OrderEntityBuilder(false);
            public static OrderRepositoryMockBuilder OrderRepository => new OrderRepositoryMockBuilder();
        }

        [Test]
        public async Task Execute_OrderIsMaster_AllProcessed()
        {
            // Arrange
            OrderEntityBuilder orderEntity = A.OrderEntity
                .W((OrderId) 1)
                .W(OrderStatus.Processed, OrderErrorCode.None)
                .AsMasterOrder(new[]
                {
                    A.OrderEntity
                        .W((OrderId) 2)
                        .W((MasterOrderId) 1)
                        .W(OrderStatus.Processed, OrderErrorCode.None).Build()
                });
            IOrderRepository orderRepository = A.OrderRepository.W(orderEntity).Build();

            // Act
            CheckOrderCompletedUseCase sut = new CheckOrderCompletedUseCase(orderRepository);
            bool result = await sut.ExecuteAsync(1);
            
            // Assert
            Assert.True(result);
        }
        
        [Test]
        public async Task Execute_OrderIsMaster_SubOrderNotProcessed()
        {
            // Arrange
            OrderEntityBuilder orderEntity = A.OrderEntity
                                       .W((OrderId)1)
                                       .W(OrderStatus.Processed, OrderErrorCode.None)
                                       .AsMasterOrder(new[]
                                       {
                                           A.OrderEntity
                                            .W((OrderId)2)
                                            .W((MasterOrderId)1, null)
                                            .W(OrderStatus.InRoute, OrderErrorCode.None).Build()
                                       });
            IOrderRepository orderRepository = A.OrderRepository.W(orderEntity).Build();

            // Act
            CheckOrderCompletedUseCase sut = new CheckOrderCompletedUseCase(orderRepository);
            bool result = await sut.ExecuteAsync(1);

            // Assert
            Assert.False(result);
        }
        
        [Test]
        public async Task Execute_SubOrderIsMaster_AllProcessed()
        {
            // Arrange
            OrderEntityBuilder masterOrderBuilder = new OrderEntityBuilder(false)
                                                     .W((OrderId)1)
                                                     .W(OrderStatus.Processed, OrderErrorCode.None);
            
            OrderEntityBuilder subOrderBuilder = new OrderEntityBuilder(false)
                                                 .W((OrderId)2)
                                                 .W(OrderStatus.Processed, OrderErrorCode.None)
                                                 .W((MasterOrderId)1, masterOrderBuilder.Build());

            IOrderRepository orderRepository = A.OrderRepository.W(subOrderBuilder).Build();

            // Act
            CheckOrderCompletedUseCase sut = new CheckOrderCompletedUseCase(orderRepository);
            bool result = await sut.ExecuteAsync(2);

            // Assert
            Assert.True(result);
        }
        
        [Test]
        public async Task Execute_SubOrderIsMaster_SubOrderNotProcessed()
        {
            // Arrange
            OrderEntityBuilder masterOrderBuilder = new OrderEntityBuilder(false)
                    .W((OrderId)1)
                    .W(OrderStatus.Processed, OrderErrorCode.None);
            
            OrderEntityBuilder subOrderBuilder = new OrderEntityBuilder(false)
                                                 .W((OrderId)2)
                                                 .W(OrderStatus.Processed, OrderErrorCode.ManuallyProcessed)
                                                 .W((MasterOrderId)1, masterOrderBuilder.Build());

            IOrderRepository orderRepository = A.OrderRepository.W(subOrderBuilder).Build();

            // Act
            CheckOrderCompletedUseCase sut = new CheckOrderCompletedUseCase(orderRepository);
            bool result = await sut.ExecuteAsync(2);

            // Assert
            Assert.False(result);
        }
    }
}