﻿using Crave.Ordering.CompleteRoute.Interfaces.Repositories;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.Tests.Builders;
using Moq;

namespace Crave.Ordering.CompleteRoute.Tests.Builders.RepositoryBuilders
{
    public class OrderRepositoryMockBuilder
    {
        private OrderEntity OrderEntity { get; }

        public OrderRepositoryMockBuilder() : this(new OrderEntityBuilder().Build())
        {
        }

        private OrderRepositoryMockBuilder(OrderEntity orderEntity) => this.OrderEntity = orderEntity;

        public OrderRepositoryMockBuilder W(OrderEntityBuilder orderEntityBuilder) => new OrderRepositoryMockBuilder(orderEntityBuilder.Build());

        public OrderRepositoryMockBuilder W(OrderEntity orderEntity) => new OrderRepositoryMockBuilder(orderEntity);

        public IOrderRepository Build()
        {
            Mock<IOrderRepository> repositoryMock = new Mock<IOrderRepository>();
            _ = repositoryMock.Setup(repository => repository.GetByIdAsync(It.IsAny<int>())).ReturnsAsync(this.OrderEntity);
            _ = repositoryMock.Setup(repository => repository.GetMasterOrderForOrderId(It.IsAny<int>())).ReturnsAsync(this.OrderEntity);
            return repositoryMock.Object;
        }
    }
}    
