﻿using System.ComponentModel.DataAnnotations;
using AutoFixture;
using AutoMapper;
using Crave.Ordering.Api.Client;

namespace Crave.Ordering.ManualTests.Builders.Requests
{
    public class OrderitemRequestDtoBuilder
    {
        private readonly RequestDtoData requestData;

        public OrderitemRequestDtoBuilder() : this(Initialize())
        {

        }

        private OrderitemRequestDtoBuilder(RequestDtoData requestData)
        {
            this.requestData = requestData;
        }

        public OrderitemRequestDtoBuilder WithType(OrderitemType type) => new OrderitemRequestDtoBuilder(new RequestDtoData(requestData)
        {
            Type = type
        });

        public OrderitemRequestDtoBuilder WithProductId(int id) => new OrderitemRequestDtoBuilder(new RequestDtoData(requestData)
        {
            ProductId = id
        });

        public OrderitemRequestDtoBuilder WithCategoryId(int id) => new OrderitemRequestDtoBuilder(new RequestDtoData(requestData)
        {
            CategoryId = id
        });

        public OrderitemRequestDtoBuilder WithPrice(decimal price) => new OrderitemRequestDtoBuilder(new RequestDtoData(requestData)
        {
            Price = price
        });

        public OrderitemRequestDtoBuilder WithNotes(string notes) => new OrderitemRequestDtoBuilder(new RequestDtoData(requestData)
        {
            Notes = notes
        });

        public OrderitemRequestDto Build()
        {
            var config = new MapperConfiguration(cfg => cfg.CreateMap<RequestDtoData, OrderitemRequestDto>());
            return new Mapper(config).Map<OrderitemRequestDto>(requestData);
        }

        public static RequestDtoData Initialize()
        {
            return new Fixture().Create<RequestDtoData>();
        }

        public class RequestDtoData
        {
            public RequestDtoData()
            {
                this.Guid = System.Guid.NewGuid().ToString("D");
            }

            public RequestDtoData(RequestDtoData old)
            {
                this.Type = old.Type;
                this.Guid = old.Guid;
                this.ProductId = old.ProductId;
                this.CategoryId = old.CategoryId;
                this.Quantity = old.Quantity;
                this.Price = old.Price;
                this.Notes = old.Notes;
            }

            public OrderitemType Type { get; set; } = OrderitemType.Product;
            public string Guid { get; set; }
            public int ProductId { get; set; }
            public int? CategoryId { get; set; }
            [Range(1, 10)] public int Quantity { get; set; }
            [Range(0.0, 20.0)] public decimal? Price { get; set; }
            public string Notes { get; set; }
        }
    }
}