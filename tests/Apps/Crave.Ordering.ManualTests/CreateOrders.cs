﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Threading.Tasks;
using Crave.Ordering.Api.Client;
using Crave.Ordering.ManualTests.Builders.Requests;
using NUnit.Framework;

namespace Crave.Ordering.ManualTests
{
    [TestFixture, Explicit]
    public class CreateOrders
    {
        private OrdersClient client;

        public static class A
        {
            public static CreateOrderRequestDtoBuilder CreateOrderRequestDto => new CreateOrderRequestDtoBuilder();
            public static OrderitemRequestDtoBuilder OrderitemRequestDto => new OrderitemRequestDtoBuilder();
        }

        [OneTimeSetUp]
        public void Setup()
        {
            client = new OrdersClient(new HttpClient
            {
                BaseAddress = new Uri("https://ordering-api.dev.aws.cravecloud.xyz")
            });
        }

        [Test, Repeat(20)]
        public async Task OneThousandOrders()
        {
            CreateOrderRequestDto createOrderRequest = CreateEmenuOrderRequestDto(1);

            try
            {
                var result = await client.CreateOrderAsync(createOrderRequest);

                Trace.WriteLine($"Order GUID: {result}");
                Assert.That(result, Is.Not.Empty);
            }
            catch (ApiException ex)
            {
                Trace.WriteLine(ex.Message);
                throw;
            }
        }

        [Test]
        public async Task GetOrdersForTerminal()
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            
            var result = await this.client.GetOrdersAsync(9036);

            sw.Stop();

            Assert.That(result, Is.Not.Empty);
            Assert.Pass($"Count: {result.Count}, Time={sw.Elapsed:g}");
        }

        private CreateOrderRequestDto CreateEmenuOrderRequestDto(int numberOfOrderItems)
        {
            List<OrderitemRequestDtoBuilder> orderitems = new List<OrderitemRequestDtoBuilder>();
            for (int i = 0; i < numberOfOrderItems; i++)
            {
                orderitems.Add(A.OrderitemRequestDto
                    .WithProductId(246325)
                    .WithCategoryId(49832)
                    .WithPrice((decimal)6.25));
            }

            return A.CreateOrderRequestDto
                .WithCompanyId(550) // Katalonia V2
                .WithClientId(196433)
                .WithDeliverypointId(248875)
                .WithOrderitems(orderitems.ToArray())
                .Build();
        }
    }
}