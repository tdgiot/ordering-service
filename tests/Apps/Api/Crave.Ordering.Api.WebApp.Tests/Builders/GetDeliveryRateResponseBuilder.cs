﻿using Crave.Libraries.Distance.Models;
using Crave.Ordering.Api.Domain.Enums;
using Crave.Ordering.Api.Domain.UseCases.Responses;

namespace Crave.Ordering.Api.WebApp.Tests.Builders
{
	public class GetDeliveryRateResponseBuilder
	{
		private readonly decimal charge;
		private readonly decimal chargeTaxExcluded;
		private readonly decimal chargeTaxIncluded;
		private readonly DeliveryRateResult deliveryRateResult;
		private readonly Distance distance;
		private readonly int? taxTariffId;

		public GetDeliveryRateResponseBuilder() : this(new Distance(2345), DeliveryRateResult.OK, 12.95M, 3, 1.19M, 1M)
		{
		}

		private GetDeliveryRateResponseBuilder(
			Distance distance,
			DeliveryRateResult deliveryRateResult,
			decimal charge,
			int? taxTariffId,
			decimal chargeTaxIncluded,
			decimal chargeTaxExcluded)
		{
			this.distance = distance;
			this.deliveryRateResult = deliveryRateResult;
			this.charge = charge;
			this.taxTariffId = taxTariffId;
			this.chargeTaxIncluded = chargeTaxIncluded;
			this.chargeTaxExcluded = chargeTaxExcluded;
		}

		public GetDeliveryRateResponse Build() =>
			new GetDeliveryRateResponse(
				deliveryRateResult, charge,
				distance, taxTariffId, chargeTaxIncluded, chargeTaxExcluded);

		public GetDeliveryRateResponseBuilder W(Distance value)
			=> new GetDeliveryRateResponseBuilder(value, deliveryRateResult, charge, taxTariffId, chargeTaxIncluded, chargeTaxExcluded);

		public GetDeliveryRateResponseBuilder W(DeliveryRateResult value)
			=> new GetDeliveryRateResponseBuilder(distance, value, charge, taxTariffId, chargeTaxIncluded, chargeTaxExcluded);
	}
}
