﻿using Crave.Ordering.Api.WebApplication.Parameters;

namespace Crave.Ordering.Api.WebApp.Tests.Builders
{
    public class OrdersQueryParameterBuilder
    {
        private readonly int terminalId;

        public OrdersQueryParameterBuilder() : this(1)
        {
        }

        public OrdersQueryParameterBuilder(int terminalId) => this.terminalId = terminalId;

        public static OrdersQueryParameter Build() => new() { TerminalId = 1 };

        public static OrdersQueryParameterBuilder W(int terminal) => new(terminal);
    }
}
