﻿using AutoMapper;
using Crave.Ordering.Api.WebApplication.Controllers;
using Crave.Ordering.Api.WebApplication.Mappers;

namespace Crave.Ordering.Api.WebApp.Tests.Builders
{
	internal static class OrdersControllerBuilder
	{
		public static OrdersController Build() => new OrdersController(new AutoMapper.Mapper(new MapperConfiguration(cfg => cfg.AddProfile<AutoMapping>())));
	}
}
