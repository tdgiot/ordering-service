﻿using System;
using System.Collections.Generic;
using Crave.Enums;
using Crave.Ordering.Api.Domain.UseCases.Responses;

namespace Crave.Ordering.Api.WebApp.Tests.Builders
{
    internal class OrderModelBuilder
    {
        private readonly string countryCode;
        private readonly string currencyCode;
        private readonly string guid;
        private readonly int orderId;

        public OrderModelBuilder() : this(0, Guid.Empty.ToString(), "EUR", "NL")
        {
        }

        private OrderModelBuilder(int orderId, string guid, string currencyCode, string countryCode)
        {
            this.orderId = orderId;
            this.guid = guid;
            this.currencyCode = currencyCode;
            this.countryCode = countryCode;
        }

        public Order Build() => new(
            OrderType.Standard, this.orderId, this.guid, 1,
            null, null, false, "notes", OrderStatus.InRoute, null, null, null,
            null, false, TaxBreakdown.None, (decimal)12.34, (decimal)12.34, this.currencyCode, this.countryCode,
            new Deliverypoint(1, 1, "1"),
            new Customer(null, null, null, null, null, null, false),
            null, null, new List<Orderitem>(), new List<OrderRoutestephandler>(), new PaymentTransaction("0000", "Mastercard"), 1);

        public OrderModelBuilder WithOrderId(int value) => new(value, this.guid, this.currencyCode, this.countryCode);

        public OrderModelBuilder WithGuid(string value) => new(this.orderId, value, this.currencyCode, this.countryCode);

        public OrderModelBuilder WithCurrencyCode(string value) => new(this.orderId, this.guid, value, this.countryCode);

        public OrderModelBuilder WithCountryCode(string value) => new(this.orderId, this.guid, this.currencyCode, value);
    }
}
