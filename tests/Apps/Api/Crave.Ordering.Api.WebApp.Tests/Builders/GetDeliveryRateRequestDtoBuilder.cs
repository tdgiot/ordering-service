﻿using Crave.Ordering.Api.Core.Models.Requests;

namespace Crave.Ordering.Api.WebApp.Tests.Builders
{
	internal class GetDeliveryRateRequestDtoBuilder
	{
		private readonly int serviceMethodId;
		private readonly DeliveryLocationDto deliveryLocationDto;
		private readonly decimal totalPrice;

		public GetDeliveryRateRequestDtoBuilder() : this(0, new DeliveryLocationDto(), 0M)
		{
		}

		private GetDeliveryRateRequestDtoBuilder(int serviceMethodId, DeliveryLocationDto deliveryLocationDto, decimal totalPrice)
		{
			this.serviceMethodId = serviceMethodId;
			this.deliveryLocationDto = deliveryLocationDto;
			this.totalPrice = totalPrice;
		}

		public GetDeliveryRateRequestDto Build() => new GetDeliveryRateRequestDto
		{
			DeliveryLocation = deliveryLocationDto, 
			ServiceMethodId = serviceMethodId, 
			TotalPrice = totalPrice
		};

		public GetDeliveryRateRequestDtoBuilder WithServiceMethodId(int value) => new GetDeliveryRateRequestDtoBuilder(value, deliveryLocationDto, totalPrice);

		public GetDeliveryRateRequestDtoBuilder WithDeliveryLocationDto(DeliveryLocationDto value) => new GetDeliveryRateRequestDtoBuilder(serviceMethodId, value, totalPrice);

		public GetDeliveryRateRequestDtoBuilder WithTotalPrice(decimal value) => new GetDeliveryRateRequestDtoBuilder(serviceMethodId, deliveryLocationDto, value);
	}
}