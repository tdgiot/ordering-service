﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Runtime.Loader;
using Crave.Ordering.Api.Domain.UseCases;
using Crave.Ordering.Api.WebApplication;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;

namespace Crave.Ordering.Api.WebApp.Tests
{
    [TestFixture]
    public class StartupTests
    {
        [Test]
        public void TestConfigureServices_EnsureAllUseCasesAreRegistered()
        {
            IConfiguration configuration = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json")
                .Build();

            ServiceCollection serviceCollection = new ServiceCollection();

            Startup sut = new Startup(configuration, false);
            sut.ConfigureServices(serviceCollection);

            ServiceProvider serviceProvider = serviceCollection.BuildServiceProvider();

            IEnumerable<Type> expectedUseCaseImplementationTypes = GetImplementationTypesFromCraveAssemblies<IUseCase>();
            Assert.That(expectedUseCaseImplementationTypes, Is.Not.Empty);
            
            foreach (Type useCaseImplementationType in expectedUseCaseImplementationTypes)
            {
                AssertRegisteredAsInterfaceOrImplementation(serviceProvider, useCaseImplementationType);
            }
        }

        private static void AssertRegisteredAsInterfaceOrImplementation(ServiceProvider serviceProvider, Type expectedType)
        {
            object resolvedImplementation = null;
            foreach (Type implementedInterfaces in expectedType.GetInterfaces())
            {
                try
                {
                    resolvedImplementation = serviceProvider.GetService(implementedInterfaces);
                    if (resolvedImplementation != null)
                    {
                        break;
                    }
                }
                catch (Exception ex)
                {
                    Trace.WriteLine(ex.Message);
                    throw;
                }
            }

            if (resolvedImplementation == null)
            {
                resolvedImplementation = serviceProvider.GetService(expectedType);
            }

            Assert.That(resolvedImplementation, Is.Not.Null, $"Unable to resolve: {expectedType}");
        }

        /// <summary>
        /// Retrieve all implementation types of the specified interface or class.
        /// </summary>
        /// <typeparam name="T">The base type or interface to search for</typeparam>
        /// <returns>A collection of types.</returns>
        private static IList<Type> GetImplementationTypesFromCraveAssemblies<T>()
        {
            Type typeToSearchFor = typeof(T);

            IEnumerable<Assembly> craveSpecificAssemblies = AssemblyLoadContext.Default.Assemblies.Where(a => (a.FullName ?? string.Empty).Contains("Crave.Ordering.Api.Domain"));
            return craveSpecificAssemblies.SelectMany(a => a.GetTypes().Where(t => !t.IsInterface && !t.IsAbstract && typeToSearchFor.IsAssignableFrom(t))).ToList();
        }
    }
}