﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Crave.Enums;
using Crave.Libraries.Distance.Enums;
using Crave.Libraries.Distance.Models;
using Crave.Ordering.Api.Core.Enums;
using Crave.Ordering.Api.Core.Models.Requests;
using Crave.Ordering.Api.Core.Models.Responses;
using Crave.Ordering.Api.Domain.Interfaces.UseCases;
using Crave.Ordering.Api.Domain.Tests.Builders;
using Crave.Ordering.Api.Domain.Tests.Builders.EntityBuilders;
using Crave.Ordering.Api.Domain.Tests.Builders.RepositoryBuilders;
using Crave.Ordering.Api.Domain.Tests.Builders.UseCaseBuilders;
using Crave.Ordering.Api.Domain.UseCases.Requests;
using Crave.Ordering.Api.Domain.UseCases.Responses;
using Crave.Ordering.Api.Domain.Validation.Errors;
using Crave.Ordering.Api.WebApp.Tests.Builders;
using Crave.Ordering.Api.WebApplication.Controllers;
using Crave.Ordering.Api.WebApplication.Parameters;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.Tests.Builders;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;

namespace Crave.Ordering.Api.WebApp.Tests.Controllers
{
    [TestFixture]
    public class OrderControllerTests
    {
        private static class A
        {
            public static GetOrdersForTerminalUseCaseBuilder GetOrdersForTerminalUseCase => new();
            public static GetOrderUseCaseMockBuilder GetOrderUseCaseMock => new();
            public static UpdateOrderStatusByGuidUseCaseMockBuilder UpdateOrderStatusByGuidUseCaseMock => new();
            public static CreateAndSaveOrderUseCaseMockBuilder CreateAndSaveOrderUseCaseMock => new();
            public static UpdateRoutestephandlerForTerminalUseCaseMockBuilder UpdateRoutestephandlerForTerminalUseCaseMock => new();
            public static ValidateOrderitemsUseCaseMockBuilder ValdiateOrderitemsUseCaseMock => new();
            public static OrderModelBuilder OrderModel => new();
            public static GetDeliveryRateUseCaseBuilder GetDeliveryRateUseCase => new();
            public static GetDeliveryRateRequestDtoBuilder GetDeliveryRateRequestDto => new();
            public static IServiceMethodRepositoryBuilder ServiceMethodRepositoryMock => new ServiceMethodRepositoryMockBuilder();
            public static ServiceMethodEntityBuilder ServiceMethodEntity => new();
            public static IGetDistanceToOutletUseCaseBuilder GetDistanceToOutletUseCase => new GetDistanceToOutletUseCaseBuilder();
            public static IGetDistanceBetweenLocationsUseCaseBuilder GetDistanceBetweenLocationsUseCase => new GetDistanceBetweenLocationsUseCaseBuilder();
            public static IDistanceClientBuilder DistanceClientMock => new DistanceClientMockBuilder();
            public static IGetDistanceResponseBuilder GetDistanceResponse => new GetDistanceResponseBuilder();
            public static DeliveryDistanceEntityBuilder DeliveryDistanceEntity => new();
            public static GetOrderHistoryForTerminalUseCaseBuilder GetOrderHistoryForTerminalUseCase => new();
            public static OrderRepositoryMockBuilder OrderRepositoryMock => new();
            public static OrderEntityBuilder OrderEntity => new();
        }

        [Test]
        public async Task GetOrders_WithTerminalId_ReturnsOk()
        {
            // Arrange
            IGetOrdersForTerminalUseCase getOrdersUseCase = A.GetOrdersForTerminalUseCase
                .WithResponse(ResponseBase<IEnumerable<Order>>.AsSuccess(new List<Order>
                {
                    A.OrderModel
                        .WithOrderId(1)
                        .WithGuid("abc")
                        .Build()
                }))
                .Build().Object;

            OrdersQueryParameter queryParameter = OrdersQueryParameterBuilder.Build();

            // Act
            ActionResult<IEnumerable<OrderDto>> result = await OrdersControllerBuilder.Build().GetOrdersAsync(getOrdersUseCase, queryParameter);

            // Assert
            Assert.That(result.Result, Is.TypeOf<OkObjectResult>());
            OkObjectResult okObject = (OkObjectResult)result.Result;
            Assert.That(okObject.Value, Is.TypeOf<List<OrderDto>>());
            List<OrderDto> resultValue = (List<OrderDto>)okObject.Value;
            Assert.That(resultValue.Count, Is.EqualTo(1));
            Assert.That(resultValue[0].Guid, Is.EqualTo("abc"));
            Assert.That(resultValue[0]?.PaymentTransaction?.PaymentMethod, Is.EqualTo("Mastercard"));
        }

        [Test]
        public async Task GetOrderByGuid_ExistingGuid_ReturnsOk()
        {
            // Arrange
            IGetOrderUseCase getOrderUseCase = A.GetOrderUseCaseMock
                .WithResult(A.OrderModel
                    .WithOrderId(1)
                    .WithGuid("abc")
                    .Build())
                .Build();

            OrderByGuidRouteParameter routeParameter = new() { Guid = "test" };

            // Act
            ActionResult<OrderDto> result = await OrdersControllerBuilder.Build().GetOrderByGuidAsync(getOrderUseCase, routeParameter);

            // Assert
            Assert.That(result.Result, Is.TypeOf<OkObjectResult>());
            Mock.Get(getOrderUseCase).Verify();
        }

        [Test]
        public async Task GetOrderByGuid_NonExistingGuid_ReturnsNotFound()
        {
            // Arrange
            IGetOrderUseCase getOrderUseCase = A.GetOrderUseCaseMock
                .WithResult(null)
                .Build();

            OrderByGuidRouteParameter routeParameter = new() { Guid = "test" };

            // Act
            ActionResult<OrderDto> result = await OrdersControllerBuilder.Build().GetOrderByGuidAsync(getOrderUseCase, routeParameter);

            // Assert
            Assert.That(result.Result, Is.TypeOf<NotFoundResult>());
            Mock.Get(getOrderUseCase).Verify();
        }

        [Test]
        public async Task GetOrderByGuidAsync_WithCurrencyOnOrder_ReturnsCorrectCurrencyInDto()
        {
            // Arrange
            const string currencyCode = "GBP";

            IGetOrderUseCase getOrderUseCase = A.GetOrderUseCaseMock
                .WithResult(A.OrderModel
                    .WithCurrencyCode(currencyCode)
                    .Build())
                .Build();

            OrderByGuidRouteParameter routeParameter = new();

            // Act
            ActionResult<OrderDto> result = await OrdersControllerBuilder.Build().GetOrderByGuidAsync(getOrderUseCase, routeParameter);

            // Assert
            Assert.That(result.Result, Is.TypeOf<OkObjectResult>());
            OrderDto orderDto = (OrderDto)((OkObjectResult)result.Result).Value;
            Assert.AreEqual(currencyCode, orderDto.CurrencyCode);
        }

        [Test]
        public async Task UpdateOrderByGuid_ReturnsOk()
        {
            // Arrange
            IUpdateOrderStatusByGuidUseCase useCaseMock = A.UpdateOrderStatusByGuidUseCaseMock
                .WithResult(ResponseBase.AsSuccess())
                .Build();

            OrderByGuidRouteParameter routeParameter = new() { Guid = "abc" };
            UpdateOrderStatusRequestDto requestBody = new() { OrderStatus = OrderStatus.Processed };

            // Act
            ActionResult result = await OrdersControllerBuilder.Build().UpdateOrderByGuidAsync(useCaseMock, routeParameter, requestBody);

            // Assert
            Assert.That(result, Is.TypeOf<OkResult>());
        }

        [Test]
        public async Task UpdateOrderByGuid_ReturnsNotFound()
        {
            // Arrange
            IUpdateOrderStatusByGuidUseCase useCaseMock = A.UpdateOrderStatusByGuidUseCaseMock
                .WithResult(ResponseBase.AsNotFound("Not found error message"))
                .Build();

            OrderByGuidRouteParameter routeParameter = new() { Guid = "abc" };
            UpdateOrderStatusRequestDto requestBody = new() { OrderStatus = OrderStatus.Processed };

            // Act
            ActionResult result = await OrdersControllerBuilder.Build().UpdateOrderByGuidAsync(useCaseMock, routeParameter, requestBody);

            // Assert
            Assert.That(result, Is.TypeOf<NotFoundObjectResult>());
            ProblemDetails problemDetails = (ProblemDetails)((NotFoundObjectResult)result).Value;
            Assert.That(problemDetails.Detail, Is.EqualTo("Not found error message"));
        }

        [Test]
        public async Task UpdateOrderByGuid_ReturnsBadRequest()
        {
            // Arrange
            IUpdateOrderStatusByGuidUseCase useCaseMock = A.UpdateOrderStatusByGuidUseCaseMock
                .WithResult(ResponseBase.AsError("Something went wrong"))
                .Build();

            OrderByGuidRouteParameter routeParameter = new() { Guid = "abc" };
            UpdateOrderStatusRequestDto requestBody = new() { OrderStatus = OrderStatus.Processed };

            // Act
            ActionResult result = await OrdersControllerBuilder.Build().UpdateOrderByGuidAsync(useCaseMock, routeParameter, requestBody);

            // Assert
            Assert.That(result, Is.TypeOf<BadRequestObjectResult>());
            BadRequestObjectResult resultObject = (BadRequestObjectResult)result;
            Assert.That(resultObject.Value, Is.TypeOf<ErrorResponseDto>());
            ErrorResponseDto errorResponseDto = (ErrorResponseDto)resultObject.Value;
            Assert.That(errorResponseDto.Message, Is.EqualTo("Something went wrong"));
        }

        [Test]
        public async Task CreateOrder_ReturnsOk()
        {
            // Arrange
            ICreateAndSaveOrderUseCase useCaseMock = A.CreateAndSaveOrderUseCaseMock
                .WithResponse(CreateAndSaveOrderResponse.AsSuccess("1234"))
                .Build();

            CreateOrderRequestDto requestDto = new();

            // Act
            ActionResult result = await OrdersControllerBuilder.Build().CreateOrderAsync(useCaseMock, requestDto);

            // Assert
            Assert.That(result, Is.TypeOf<OkObjectResult>());
            OkObjectResult okObject = (OkObjectResult)result;
            Assert.That(okObject.Value, Is.EqualTo("1234"));
        }

        [Test]
        public async Task CreateOrder_CreationError_ReturnsBadRequest()
        {
            // Arrange
            CreateAndSaveOrderResponse response = CreateAndSaveOrderResponse.AsFailure(new ValidationResponse(new[] { new ValidationError(ValidationErrorSubType.CompanyInvalid) }));

            ICreateAndSaveOrderUseCase useCaseMock = A.CreateAndSaveOrderUseCaseMock
                .WithResponse(response)
                .Build();

            CreateOrderRequestDto requestDto = new();

            // Act
            ActionResult result = await OrdersControllerBuilder.Build().CreateOrderAsync(useCaseMock, requestDto);

            // Assert
            Assert.That(result, Is.TypeOf<BadRequestObjectResult>());
            BadRequestObjectResult objectResult = (BadRequestObjectResult)result;
            Assert.That(objectResult.Value, Is.TypeOf<ValidationResponseDto>());
            ValidationResponseDto validationResponse = (ValidationResponseDto)objectResult.Value;
            Assert.That(validationResponse.ValidationErrors.First().ValidationErrorSubType, Is.EqualTo(ValidationErrorSubType.CompanyInvalid));
        }

        [Test]
        public async Task UpdateOrderRoutestephandlerStatus_UseCaseThrowsError_ReturnsBadRequest()
        {
            // Arrange
            IUpdateOrderRoutestephandlerForTerminalUseCase useCase = A.UpdateRoutestephandlerForTerminalUseCaseMock
                .WithResponse(ResponseBase.AsError("Something went wrong"))
                .Build();
            OrderRoutestephandlerRouteParameter routeParameter = new() { OrderRoutestephandlerId = 13 };
            UpdateOrderRoutestephandlerStatusRequestDto request = new();

            ActionResult result = await OrdersControllerBuilder.Build().UpdateOrderRoutestephandlerStatus(useCase, routeParameter, request);

            Assert.That(result, Is.TypeOf<BadRequestObjectResult>());
            BadRequestObjectResult resultObject = (BadRequestObjectResult)result;
            Assert.That(resultObject.Value, Is.TypeOf<ErrorResponseDto>());
            ErrorResponseDto errorResponseDto = (ErrorResponseDto)resultObject.Value;
            Assert.That(errorResponseDto.Message, Is.EqualTo("Something went wrong"));
            Mock.Get(useCase).Verify(uc => uc.ExecuteAsync(It.IsAny<UpdateOrderRoutestephandlersForTerminal>()), Times.Once);
        }

        [Test]
        public async Task UpdateOrderRoutestephandlerStatus_StatusUpdated_ReturnsOk()
        {
            // Arrange
            IUpdateOrderRoutestephandlerForTerminalUseCase useCase = A.UpdateRoutestephandlerForTerminalUseCaseMock
                .WithResponse(ResponseBase.AsSuccess())
                .Build();
            OrderRoutestephandlerRouteParameter routeParameter = new() { OrderRoutestephandlerId = 13 };
            UpdateOrderRoutestephandlerStatusRequestDto request = new();

            ActionResult result = await OrdersControllerBuilder.Build().UpdateOrderRoutestephandlerStatus(useCase, routeParameter, request);

            Assert.That(result, Is.TypeOf<OkResult>());
            Mock.Get(useCase).Verify(uc => uc.ExecuteAsync(It.IsAny<UpdateOrderRoutestephandlersForTerminal>()), Times.Once);
        }

        [Test]
        public async Task ValidateOrder_ReturnsOkWithValidationResponse()
        {
            // Arrange
            IValidateOrderitemsUseCase useCase = A.ValdiateOrderitemsUseCaseMock.Build();

            // Act
            ActionResult result = await OrdersControllerBuilder.Build().ValidateOrderAsync(useCase, new ValidateOrderitemsRequestDto());

            // Assert
            Assert.That(result, Is.TypeOf<OkObjectResult>());
            OkObjectResult objectResult = (OkObjectResult)result;
            Assert.That(objectResult.Value, Is.TypeOf<ValidationResponseDto>());
            ValidationResponseDto validationResponse = (ValidationResponseDto)objectResult.Value;
            Assert.That(validationResponse.ValidationErrors.Count(), Is.EqualTo(0));
        }

        [Test]
        public async Task ValidateOrder_ValidationErrors_ReturnsOkWithValidationResponse()
        {
            // Arrange
            IValidateOrderitemsUseCase useCase = A.ValdiateOrderitemsUseCaseMock
                .WithResponse(new ValidationResponse(new[] { new ValidationError(ValidationErrorSubType.CategoryInvalid), new ValidationError(ValidationErrorSubType.CheckoutMethodNotActive) }))
                .Build();

            // Act
            ActionResult result = await OrdersControllerBuilder.Build().ValidateOrderAsync(useCase, new ValidateOrderitemsRequestDto());

            // Assert
            Assert.That(result, Is.TypeOf<OkObjectResult>());
            OkObjectResult objectResult = (OkObjectResult)result;
            Assert.That(objectResult.Value, Is.TypeOf<ValidationResponseDto>());
            ValidationResponseDto validationResponse = (ValidationResponseDto)objectResult.Value;
            List<ValidationErrorDto> validationErrors = validationResponse.ValidationErrors.ToList();
            Assert.That(validationErrors.Count, Is.EqualTo(2));
            Assert.That(validationErrors[0].ValidationErrorSubType, Is.EqualTo(ValidationErrorSubType.CategoryInvalid));
            Assert.That(validationErrors[1].ValidationErrorSubType, Is.EqualTo(ValidationErrorSubType.CheckoutMethodNotActive));
        }

        [Test]
        public async Task GetDeliveryRateAsync_WithInvalidServiceMethod_ReturnsInvalidServiceMethod()
        {
            // Arrange
            OrdersController sut = OrdersControllerBuilder.Build();

            // Act
            ActionResult result = await sut.GetDeliveryRateAsync(A.GetDeliveryRateUseCase
                .W(A.ServiceMethodRepositoryMock.W((ServiceMethodEntity)null))
                .Build(), A.GetDeliveryRateRequestDto.Build());

            // Assert
            AssertDeliveryRateResultEqual(DeliveryRateResult.InvalidServiceMethod, result);
        }

        [Test]
        public async Task GetDeliveryRateAsync_WithoutDeliveryDistanceOnServiceMethod_ReturnsNoDeliveryDistanceSpecifiedForServiceMethod()
        {
            // Arrange
            OrdersController sut = OrdersControllerBuilder.Build();

            // Act
            ActionResult result = await sut.GetDeliveryRateAsync(A.GetDeliveryRateUseCase
                    .W(A.ServiceMethodRepositoryMock
                        .W(A.ServiceMethodEntity
                            .W()))
                    .Build(),
                A.GetDeliveryRateRequestDto.Build());

            // Assert
            AssertDeliveryRateResultEqual(DeliveryRateResult.NoDeliveryDistanceSpecifiedForServiceMethod, result);
        }

        [Test]
        public async Task GetDeliveryRateAsync_WithoutDeliveryDistanceOnServiceMethod_ReturnsUnableToRetrieveDistanceBetweenOutletAndDeliveryLocation()
        {
            // Arrange
            OrdersController sut = OrdersControllerBuilder.Build();

            // Act
            ActionResult result = await sut.GetDeliveryRateAsync(A.GetDeliveryRateUseCase
                    .W(A.GetDistanceToOutletUseCase
                        .W(A.GetDistanceBetweenLocationsUseCase
                            .W(A.DistanceClientMock
                                .W(A.GetDistanceResponse.W(DistanceResult.NOT_FOUND))))
                    )
                    .Build(),
                A.GetDeliveryRateRequestDto.Build());

            // Assert
            AssertDeliveryRateResultEqual(DeliveryRateResult.UnableToRetrieveDistanceBetweenOutletAndDeliveryLocation, result);
        }

        [Test]
        public async Task GetDeliveryRateAsync_WithDeliveryOutsideRange_ReturnsOutOfRange()
        {
            // Arrange
            OrdersController sut = OrdersControllerBuilder.Build();
            const int maximumRange = 10000;

            // Act
            ActionResult result = await sut.GetDeliveryRateAsync(A.GetDeliveryRateUseCase
                    .W(A.ServiceMethodRepositoryMock
                        .W(A.ServiceMethodEntity
                            .W(A.DeliveryDistanceEntity.W(maximumRange).Build())))
                    .W(A.GetDistanceToOutletUseCase
                        .W(A.GetDistanceBetweenLocationsUseCase
                            .W(A.DistanceClientMock
                                .W(A.GetDistanceResponse.W(new Distance(maximumRange + 1)))))
                    )
                    .Build(),
                A.GetDeliveryRateRequestDto.Build());

            // Assert
            AssertDeliveryRateResultEqual(DeliveryRateResult.OutOfRange, result);
        }

        [Test]
        public async Task GetDeliveryRateAsync_WithDeliveryInRange_ReturnsOk()
        {
            // Arrange
            OrdersController sut = OrdersControllerBuilder.Build();
            const int maximumRange = 10000;

            // Act
            ActionResult result = await sut.GetDeliveryRateAsync(A.GetDeliveryRateUseCase
                    .W(A.ServiceMethodRepositoryMock
                        .W(A.ServiceMethodEntity
                            .W(A.DeliveryDistanceEntity.W(maximumRange).Build())))
                    .W(A.GetDistanceToOutletUseCase
                        .W(A.GetDistanceBetweenLocationsUseCase
                            .W(A.DistanceClientMock
                                .W(A.GetDistanceResponse.W(new Distance(maximumRange - 1)))))
                    )
                    .Build(),
                A.GetDeliveryRateRequestDto.Build());

            // Assert
            AssertDeliveryRateResultEqual(DeliveryRateResult.OK, result);
        }

        [Test]
        public async Task GetDeliveryRateAsync_WithDeliveryOnMaximumRange_ReturnsOk()
        {
            // Arrange
            OrdersController sut = OrdersControllerBuilder.Build();
            const int maximumRange = 10000;

            // Act
            ActionResult result = await sut.GetDeliveryRateAsync(A.GetDeliveryRateUseCase
                    .W(A.ServiceMethodRepositoryMock
                        .W(A.ServiceMethodEntity
                            .W(A.DeliveryDistanceEntity.W(maximumRange).Build())))
                    .W(A.GetDistanceToOutletUseCase
                        .W(A.GetDistanceBetweenLocationsUseCase
                            .W(A.DistanceClientMock
                                .W(A.GetDistanceResponse.W(new Distance(maximumRange)))))
                    )
                    .Build(),
                A.GetDeliveryRateRequestDto.Build());

            // Assert
            AssertDeliveryRateResultEqual(DeliveryRateResult.OK, result);
        }

        [Test]
        public async Task GetDeliveryRateAsync_WithDeliveryInRange_ReturnsCorrectDeliveryPrice()
        {
            // Arrange
            OrdersController sut = OrdersControllerBuilder.Build();
            const int maximumRange = 10000;
            const decimal expectedPrice = 1.95M;

            // Act
            ActionResult result = await sut.GetDeliveryRateAsync(A.GetDeliveryRateUseCase
                    .W(A.ServiceMethodRepositoryMock
                        .W(A.ServiceMethodEntity
                            .W(A.DeliveryDistanceEntity
                                .W(maximumRange)
                                .W(expectedPrice)
                                .Build())))
                    .W(A.GetDistanceToOutletUseCase
                        .W(A.GetDistanceBetweenLocationsUseCase
                            .W(A.DistanceClientMock
                                .W(A.GetDistanceResponse.W(new Distance(maximumRange - 1)))))
                    )
                    .Build(),
                A.GetDeliveryRateRequestDto.Build());

            // Assert
            AssertDeliveryRatePriceEqual(expectedPrice, result);
        }

        [Test]
        public async Task GetDeliveryRateAsync_WithDeliveryInRange_ReturnsCorrectDeliveryDistance()
        {
            // Arrange
            OrdersController sut = OrdersControllerBuilder.Build();
            const int maximumRange = 10000;
            const int expectedDistance = maximumRange - 1;

            // Act
            ActionResult result = await sut.GetDeliveryRateAsync(A.GetDeliveryRateUseCase
                    .W(A.ServiceMethodRepositoryMock
                        .W(A.ServiceMethodEntity
                            .W(A.DeliveryDistanceEntity
                                .W(maximumRange)
                                .Build())))
                    .W(A.GetDistanceToOutletUseCase
                        .W(A.GetDistanceBetweenLocationsUseCase
                            .W(A.DistanceClientMock
                                .W(A.GetDistanceResponse.W(new Distance(expectedDistance)))))
                    )
                    .Build(),
                A.GetDeliveryRateRequestDto.Build());

            // Assert
            AssertDeliveryRateDistanceEqual(expectedDistance, result);
        }

        [Test]
        public async Task GetOrderHistoryAsync_WithoutAnyOrdersInHistory_ReturnsOkWithoutAnyOrders()
        {
            // Arrange
            OrdersController sut = OrdersControllerBuilder
                .Build();

            OrdersQueryParameter queryParameter = OrdersQueryParameterBuilder.Build();

            IGetOrderHistoryForTerminalUseCase getOrderHistoryUseCase = A.GetOrderHistoryForTerminalUseCase
                .W(A.OrderRepositoryMock)
                .Build();

            // Act
            ActionResult<IEnumerable<OrderDto>> result = await sut.GetOrderHistoryAsync(getOrderHistoryUseCase, queryParameter);

            // Assert
            AssertIsEmptyList(result);
        }

        [Test]
        public async Task GetOrderHistoryAsync_WithOneOrderInHistory_ReturnsTheOrder()
        {
            // Arrange
            OrdersController sut = OrdersControllerBuilder.Build();
            OrdersQueryParameter queryParameter = OrdersQueryParameterBuilder.Build();
            OrderId orderId = 13580462;
            OrderEntity orderEntity = A.OrderEntity.W(orderId).Build();
            Order expectedOrder = A.OrderModel.WithOrderId(orderId).Build();

            IGetOrderHistoryForTerminalUseCase getOrderHistoryUseCase = A.GetOrderHistoryForTerminalUseCase
                .W(A.OrderRepositoryMock
                    .W(orderEntity))
                .Build();

            // Act
            ActionResult<IEnumerable<OrderDto>> result = await sut.GetOrderHistoryAsync(getOrderHistoryUseCase, queryParameter);

            // Assert
            AssertContainsOrder(expectedOrder, result);
        }

        [Test]
        public async Task GetOrderHistoryAsync_WithMultipleOrdersInHistory_ReturnsTheOrders()
        {
            // Arrange
            OrdersController sut = OrdersControllerBuilder.Build();
            OrdersQueryParameter queryParameter = OrdersQueryParameterBuilder.Build();

            OrderId[] orderIds = { 13580462, 13580463, 13580464 };

            OrderEntity[] orderEntities = { A.OrderEntity.W(orderIds[0]).Build(), A.OrderEntity.W(orderIds[1]).Build(), A.OrderEntity.W(orderIds[2]).Build() };
            Order[] expectedOrders = { A.OrderModel.WithOrderId(orderIds[0]).Build(), A.OrderModel.WithOrderId(orderIds[1]).Build(), A.OrderModel.WithOrderId(orderIds[2]).Build() };

            IGetOrderHistoryForTerminalUseCase getOrderHistoryUseCase = A.GetOrderHistoryForTerminalUseCase
                .W(A.OrderRepositoryMock
                    .W(orderEntities))
                .Build();

            // Act
            ActionResult<IEnumerable<OrderDto>> result = await sut.GetOrderHistoryAsync(getOrderHistoryUseCase, queryParameter);

            // Assert
            AssertContainsOrders(expectedOrders, result);
        }

        private static T ActionResultToObject<T>(ActionResult<T> actionResult) => (T)((OkObjectResult)actionResult.Result).Value;

        private static void AssertContainsOrder(Order expectedOrder, ActionResult<IEnumerable<OrderDto>> actionResult)
        {
            OrderDto[] orders = ActionResultToObject(actionResult).ToArray();
            Assert.AreEqual(1, orders.Length);
            Assert.AreEqual(expectedOrder.OrderId, orders[0].OrderId);
        }

        private static void AssertContainsOrders(IReadOnlyList<Order> expectedOrders, ActionResult<IEnumerable<OrderDto>> actionResult)
        {
            OrderDto[] orders = ActionResultToObject(actionResult).ToArray();
            Assert.AreEqual(expectedOrders.Count, orders.Length);
            Assert.Multiple(() =>
            {
                for (int index = 0; index < expectedOrders.Count; index++)
                {
                    Assert.AreEqual(expectedOrders[index].OrderId, orders[index].OrderId);
                }
            });
        }

        private static void AssertIsEmptyList<TDto>(ActionResult<IEnumerable<TDto>> actionResult) => Assert.IsEmpty(ActionResultToObject(actionResult));

        private static void AssertDeliveryRateDistanceEqual(int expectedDistance, ActionResult actualActionResult)
        {
            DeliveryRateResponseDto deliveryRateResponseDto = AssertResultIsDeliveryRateResponseDto(actualActionResult);
            Assert.AreEqual(expectedDistance, deliveryRateResponseDto.Distance?.Metres);
        }

        private static void AssertDeliveryRatePriceEqual(decimal expectedPrice, ActionResult actualActionResult)
        {
            DeliveryRateResponseDto deliveryRateResponseDto = AssertResultIsDeliveryRateResponseDto(actualActionResult);
            Assert.AreEqual(expectedPrice, deliveryRateResponseDto.PriceInformation?.Price);
        }

        private static void AssertDeliveryRateResultEqual(DeliveryRateResult expectedResult, ActionResult actualActionResult)
        {
            DeliveryRateResponseDto deliveryRateResponseDto = AssertResultIsDeliveryRateResponseDto(actualActionResult);
            Assert.AreEqual(expectedResult, deliveryRateResponseDto.Result);
        }

        private static DeliveryRateResponseDto AssertResultIsDeliveryRateResponseDto(ActionResult actualActionResult)
        {
            Assert.IsInstanceOf<OkObjectResult>(actualActionResult);
            Assert.IsInstanceOf<DeliveryRateResponseDto>(((OkObjectResult)actualActionResult).Value);

            DeliveryRateResponseDto deliveryRateResponseDto = (DeliveryRateResponseDto)((OkObjectResult)actualActionResult).Value;
            AssertRequiredFieldsNotNull(deliveryRateResponseDto);

            return deliveryRateResponseDto;
        }

        private static void AssertRequiredFieldsNotNull(DeliveryRateResponseDto deliveryRateResponseDto)
        {
            Assert.NotNull(deliveryRateResponseDto.Result);
            Assert.NotNull(deliveryRateResponseDto.Distance);
            Assert.NotNull(deliveryRateResponseDto.PriceInformation);
        }
    }
}
