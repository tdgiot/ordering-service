﻿using AutoMapper;
using Crave.Libraries.Distance.Models;
using Crave.Ordering.Api.Core.Models.Requests;
using Crave.Ordering.Api.Core.Models.Responses;
using Crave.Ordering.Api.Domain.Enums;
using Crave.Ordering.Api.Domain.UseCases.Requests;
using Crave.Ordering.Api.Domain.UseCases.Responses;
using Crave.Ordering.Api.WebApp.Tests.Builders;
using Crave.Ordering.Api.WebApplication.Mappers;
using NUnit.Framework;

namespace Crave.Ordering.Api.WebApp.Tests.Mapper
{
	public class AutoMappingTests
	{
		private static class A
		{
			public static GetDeliveryRateResponseBuilder GetDeliveryRateResponse => new GetDeliveryRateResponseBuilder();
		}

		[Test]
		public void ValidateConfiguration()
		{
			MapperConfiguration configuration = new MapperConfiguration(cfg => cfg.AddProfile<AutoMapping>());
			configuration.AssertConfigurationIsValid();
		}

		[Test]
		public void GetDeliveryRateResponse_To_DeliveryRateResponseDto()
		{
			MapperConfiguration configuration = new MapperConfiguration(cfg => cfg.AddProfile<AutoMapping>());
			IMapper mapper = configuration.CreateMapper();

			GetDeliveryRateResponse source = A.GetDeliveryRateResponse
				.W(DeliveryRateResult.OutOfRange)
				.Build();

			DeliveryRateResponseDto destination = mapper.Map<DeliveryRateResponseDto>(source);

			Assert.AreEqual((int)source.Result, (int)destination.Result);
			Assert.AreEqual(source.Distance?.Metres, destination.Distance?.Metres);
			Assert.AreEqual(source.Charge, destination.PriceInformation?.Price);
			Assert.AreEqual(source.TaxTariffId, destination.PriceInformation?.TaxTariffId);
		}

		[Test]
		public void GetDeliveryRateResponse_WithoutDistance_DoesNotMakeDistanceDtoNull()
		{
			MapperConfiguration configuration = new MapperConfiguration(cfg => cfg.AddProfile<AutoMapping>());
			IMapper mapper = configuration.CreateMapper();

			GetDeliveryRateResponse source = A.GetDeliveryRateResponse
				.W(DeliveryRateResult.NoDeliveryDistanceSpecifiedForServiceMethod)
				.W((Distance)null)
				.Build();

			DeliveryRateResponseDto destination = mapper.Map<DeliveryRateResponseDto>(source);

			Assert.NotNull(destination.Distance);
			Assert.AreEqual(default(int), destination.Distance.Metres);
		}

		[Test]
		public void GetDeliveryRateRequestDto_To_GetDeliveryRateRequest()
		{
			MapperConfiguration configuration = new MapperConfiguration(cfg => cfg.AddProfile<AutoMapping>());
			IMapper mapper = configuration.CreateMapper();

			GetDeliveryRateRequestDto source = new GetDeliveryRateRequestDto
			{
				ServiceMethodId = 5, 
				DeliveryLocation = new DeliveryLocationDto
				{
					Latitude = 99d,
					Longitude = 44d
				}, TotalPrice = 77.11M
			};

			GetDeliveryRateRequest destination = mapper.Map<GetDeliveryRateRequest>(source);

			Assert.AreEqual(source.ServiceMethodId, destination.ServiceMethodId);
			Assert.AreEqual(source.DeliveryLocation.Latitude, destination.DeliveryLocation.Latitude);
			Assert.AreEqual(source.DeliveryLocation.Longitude, destination.DeliveryLocation.Longitude);
			Assert.AreEqual(source.TotalPrice, destination.TotalPrice);
		}
	}
}

