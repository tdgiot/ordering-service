﻿using System.Collections.Generic;
using Crave.Ordering.Api.Domain.UseCases.Responses;
using Crave.Ordering.Api.Domain.Validation.Errors;

namespace Crave.Ordering.Api.Domain.Tests.Builders
{
    public class ValidationResponseBuilder
    {
        private List<ValidationError> errorList = new List<ValidationError>();

        public ValidationResponseBuilder() {}

        private ValidationResponseBuilder(List<ValidationError> errorList)
        {
            this.errorList = errorList;
        }

        public ValidationResponseBuilder WithError(params ValidationError[] errors) => new ValidationResponseBuilder(new List<ValidationError>(errors));

        public ValidationResponse Build()
        {
            return new ValidationResponse(errorList);
        }
    }
}