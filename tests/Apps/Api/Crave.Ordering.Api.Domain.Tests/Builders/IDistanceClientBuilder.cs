﻿using Crave.Libraries.Distance;

namespace Crave.Ordering.Api.Domain.Tests.Builders
{
	public interface IDistanceClientBuilder
	{
		IDistanceClient Build();
		IDistanceClientBuilder W(IGetDistanceResponseBuilder getDistanceResponseBuilder);
	}
}
