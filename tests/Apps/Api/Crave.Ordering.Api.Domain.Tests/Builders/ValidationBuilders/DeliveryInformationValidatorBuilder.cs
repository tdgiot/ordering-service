﻿using Crave.Ordering.Api.Domain.Interfaces.UseCases;
using Crave.Ordering.Api.Domain.Tests.Builders.UseCaseBuilders;
using Crave.Ordering.Api.Domain.Validation;

namespace Crave.Ordering.Api.Domain.Tests.Builders.ValidationBuilders
{
	internal class DeliveryInformationValidatorBuilder
	{
		private IGetDeliveryRateUseCase GetDeliveryRateUseCase { get; }

		public DeliveryInformationValidatorBuilder() : this(new GetDeliveryRateUseCaseBuilder().Build())
		{
			
		}

		private DeliveryInformationValidatorBuilder(IGetDeliveryRateUseCase getDeliveryRateUseCase) => GetDeliveryRateUseCase = getDeliveryRateUseCase;

		public DeliveryInformationValidatorBuilder W(GetDeliveryRateUseCaseBuilder getDeliveryRateUseCaseBuilder) => new DeliveryInformationValidatorBuilder(getDeliveryRateUseCaseBuilder.Build());

		public DeliveryInformationValidator Build() => new DeliveryInformationValidator(GetDeliveryRateUseCase);
	}
}