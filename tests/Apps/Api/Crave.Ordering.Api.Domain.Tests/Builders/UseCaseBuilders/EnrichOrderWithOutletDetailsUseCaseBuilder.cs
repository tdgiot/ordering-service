﻿using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Api.Domain.Tests.Builders.RepositoryBuilders;
using Crave.Ordering.Api.Domain.UseCases;

namespace Crave.Ordering.Api.Domain.Tests.Builders.UseCaseBuilders
{
	internal class EnrichOrderWithOutletDetailsUseCaseBuilder
	{
		private IOutletRepository OutletRepository { get; }
		
		
		public EnrichOrderWithOutletDetailsUseCaseBuilder() : this(new OutletRepositoryMockBuilder().Build())
		{

		}

		private EnrichOrderWithOutletDetailsUseCaseBuilder(IOutletRepository outletRepository)
		{
			OutletRepository = outletRepository;
        }

		public EnrichOrderWithOutletDetailsUseCaseBuilder W(OutletRepositoryMockBuilder outletRepositoryMockBuilder) =>
			new EnrichOrderWithOutletDetailsUseCaseBuilder(
				outletRepositoryMockBuilder.Build());

        public EnrichOrderWithOutletDetailsUseCase Build() => new EnrichOrderWithOutletDetailsUseCase(
			OutletRepository);
	}
}