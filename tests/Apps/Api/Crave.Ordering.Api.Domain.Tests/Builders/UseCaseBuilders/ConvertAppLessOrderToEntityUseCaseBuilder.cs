﻿using Crave.Ordering.Api.Domain.UseCases;

namespace Crave.Ordering.Api.Domain.Tests.Builders.UseCaseBuilders
{
	internal class ConvertAppLessOrderToEntityUseCaseBuilder
	{
		private EnrichOrderWithOutletDetailsUseCase EnrichOrderWithOutletDetailsUseCase { get; }
		private EnrichOrderWithCompanyDetailsUseCase EnrichOrderWithCompanyDetailsUseCase { get; }
		private EnrichOrderWithServiceMethodDetailsUseCase EnrichOrderWithServiceMethodDetailsUseCase { get; }
		private EnrichOrderWithCheckoutMethodDetailsUseCase EnrichOrderWithCheckoutMethodDetailsUseCase { get; }
		private EnrichOrderWithDeliverypointDetailsUseCase EnrichOrderWithDeliverypointDetailsUseCase { get; }
		private EnrichOrderWithChargeToDeliverypointUseCase EnrichOrderWithChargeToDeliverypointUseCase { get; }
		private EnrichOrderWithDeliveryInformationUseCase EnrichOrderWithDeliveryInformationUseCase { get; }
		private EnrichOrderWithOrderitemsUseCase EnrichOrderWithOrderitemsUseCase { get; }
		private EnrichOrderWithTaxesUseCase EnrichOrderWithTaxesUseCase { get; }
		private EnrichOrderWithOrderStatusUseCase EnrichOrderWithOrderStatusUseCase { get; }
		private EnrichOrderWithClientUseCase EnrichOrderWithClientUseCase { get; }
        private EnrichOrderWithAnalyticsUseCase EnrichOrderWithAnalyticsUseCase { get; }
        private EnrichOrderitemsWithTagsUseCase EnrichOrderitemsWithTagsUseCase { get; }

		public ConvertAppLessOrderToEntityUseCaseBuilder() : this(
            new EnrichOrderWithOutletDetailsUseCaseBuilder().Build(),
            new EnrichOrderWithCompanyDetailsUseCaseBuilder().Build(),
			new EnrichOrderWithServiceMethodDetailsUseCaseBuilder().Build(),
			new EnrichOrderWithCheckoutMethodDetailsUseCaseBuilder().Build(),
			new EnrichOrderWithDeliverypointDetailsUseCaseBuilder().Build(),
			new EnrichOrderWithChargeToDeliverypointUseCaseBuilder().Build(),
			new EnrichOrderWithDeliveryInformationUseCaseBuilder().Build(),
			new EnrichOrderWithOrderitemsUseCaseBuilder().Build(),
			new EnrichOrderWithTaxesUseCaseBuilder().Build(),
			new EnrichOrderWithOrderStatusUseCaseBuilder().Build(),
            new EnrichOrderWithClientUseCaseBuilder().Build(),
            new EnrichOrderWithAnalyticsUseCaseBuilder().Build(),
            new EnrichOrderitemsWithTagsUseCaseBuilder().Build())
		{

		}

		private ConvertAppLessOrderToEntityUseCaseBuilder(
			EnrichOrderWithOutletDetailsUseCase enrichOrderWithOutletDetailsUseCase,
            EnrichOrderWithCompanyDetailsUseCase enrichOrderWithCompanyDetailsUseCase,
            EnrichOrderWithServiceMethodDetailsUseCase enrichOrderWithServiceMethodDetailsUseCase,
			EnrichOrderWithCheckoutMethodDetailsUseCase enrichOrderWithCheckoutMethodDetailsUseCase,
			EnrichOrderWithDeliverypointDetailsUseCase enrichOrderWithDeliverypointDetailsUseCase,
			EnrichOrderWithChargeToDeliverypointUseCase enrichOrderWithChargeToDeliverypointUseCase,
			EnrichOrderWithDeliveryInformationUseCase enrichOrderWithDeliveryInformationUseCase,
			EnrichOrderWithOrderitemsUseCase enrichOrderWithOrderitemsUseCase,
			EnrichOrderWithTaxesUseCase enrichOrderWithTaxesUseCase,
			EnrichOrderWithOrderStatusUseCase enrichOrderWithOrderStatusUseCase,
            EnrichOrderWithClientUseCase enrichOrderWithClientUseCase,
            EnrichOrderWithAnalyticsUseCase enrichOrderWithAnalyticsUseCase,
            EnrichOrderitemsWithTagsUseCase enrichOrderitemsWithTagsUseCase)
		{
			EnrichOrderWithOutletDetailsUseCase = enrichOrderWithOutletDetailsUseCase;
            EnrichOrderWithCompanyDetailsUseCase = enrichOrderWithCompanyDetailsUseCase;
            EnrichOrderWithServiceMethodDetailsUseCase = enrichOrderWithServiceMethodDetailsUseCase;
			EnrichOrderWithCheckoutMethodDetailsUseCase = enrichOrderWithCheckoutMethodDetailsUseCase;
			EnrichOrderWithDeliverypointDetailsUseCase = enrichOrderWithDeliverypointDetailsUseCase;
			EnrichOrderWithChargeToDeliverypointUseCase = enrichOrderWithChargeToDeliverypointUseCase;
			EnrichOrderWithDeliveryInformationUseCase = enrichOrderWithDeliveryInformationUseCase;
			EnrichOrderWithOrderitemsUseCase = enrichOrderWithOrderitemsUseCase;
			EnrichOrderWithTaxesUseCase = enrichOrderWithTaxesUseCase;
			EnrichOrderWithOrderStatusUseCase = enrichOrderWithOrderStatusUseCase;
            EnrichOrderWithClientUseCase = enrichOrderWithClientUseCase;
            EnrichOrderWithAnalyticsUseCase = enrichOrderWithAnalyticsUseCase;
            EnrichOrderitemsWithTagsUseCase = enrichOrderitemsWithTagsUseCase;


        }

		public ConvertAppLessOrderToEntityUseCaseBuilder W(EnrichOrderWithOutletDetailsUseCaseBuilder enrichOrderWithOutletDetailsUseCaseBuilder) => new ConvertAppLessOrderToEntityUseCaseBuilder(
			enrichOrderWithOutletDetailsUseCaseBuilder.Build(),
			EnrichOrderWithCompanyDetailsUseCase,
			EnrichOrderWithServiceMethodDetailsUseCase,
			EnrichOrderWithCheckoutMethodDetailsUseCase,
			EnrichOrderWithDeliverypointDetailsUseCase,
			EnrichOrderWithChargeToDeliverypointUseCase,
			EnrichOrderWithDeliveryInformationUseCase,
			EnrichOrderWithOrderitemsUseCase,
			EnrichOrderWithTaxesUseCase,
			EnrichOrderWithOrderStatusUseCase,
			EnrichOrderWithClientUseCase,
			EnrichOrderWithAnalyticsUseCase,
            EnrichOrderitemsWithTagsUseCase
		);

        public ConvertAppLessOrderToEntityUseCaseBuilder W(EnrichOrderWithCompanyDetailsUseCaseBuilder enrichOrderWithCompanyDetailsUseCaseBuilder) => new ConvertAppLessOrderToEntityUseCaseBuilder(
            EnrichOrderWithOutletDetailsUseCase,
            enrichOrderWithCompanyDetailsUseCaseBuilder.Build(),
            EnrichOrderWithServiceMethodDetailsUseCase,
            EnrichOrderWithCheckoutMethodDetailsUseCase,
            EnrichOrderWithDeliverypointDetailsUseCase,
            EnrichOrderWithChargeToDeliverypointUseCase,
            EnrichOrderWithDeliveryInformationUseCase,
            EnrichOrderWithOrderitemsUseCase,
            EnrichOrderWithTaxesUseCase,
            EnrichOrderWithOrderStatusUseCase,
            EnrichOrderWithClientUseCase,
            EnrichOrderWithAnalyticsUseCase,
            EnrichOrderitemsWithTagsUseCase
		);

		public ConvertAppLessOrderToEntityUseCaseBuilder W(EnrichOrderWithServiceMethodDetailsUseCaseBuilder enrichOrderWithServiceMethodDetailsUseCaseBuilder) => new ConvertAppLessOrderToEntityUseCaseBuilder(
			EnrichOrderWithOutletDetailsUseCase,
            EnrichOrderWithCompanyDetailsUseCase,
			enrichOrderWithServiceMethodDetailsUseCaseBuilder.Build(),
			EnrichOrderWithCheckoutMethodDetailsUseCase,
			EnrichOrderWithDeliverypointDetailsUseCase,
			EnrichOrderWithChargeToDeliverypointUseCase,
			EnrichOrderWithDeliveryInformationUseCase,
			EnrichOrderWithOrderitemsUseCase,
			EnrichOrderWithTaxesUseCase,
			EnrichOrderWithOrderStatusUseCase,
            EnrichOrderWithClientUseCase,
            EnrichOrderWithAnalyticsUseCase,
            EnrichOrderitemsWithTagsUseCase
			);

		public ConvertAppLessOrderToEntityUseCaseBuilder W(EnrichOrderWithCheckoutMethodDetailsUseCaseBuilder enrichOrderWithCheckoutMethodDetailsUseCaseBuilder) => new ConvertAppLessOrderToEntityUseCaseBuilder(
			EnrichOrderWithOutletDetailsUseCase,
            EnrichOrderWithCompanyDetailsUseCase,
			EnrichOrderWithServiceMethodDetailsUseCase,
			enrichOrderWithCheckoutMethodDetailsUseCaseBuilder.Build(),
			EnrichOrderWithDeliverypointDetailsUseCase,
			EnrichOrderWithChargeToDeliverypointUseCase,
			EnrichOrderWithDeliveryInformationUseCase,
			EnrichOrderWithOrderitemsUseCase,
			EnrichOrderWithTaxesUseCase,
			EnrichOrderWithOrderStatusUseCase,
            EnrichOrderWithClientUseCase,
            EnrichOrderWithAnalyticsUseCase,
            EnrichOrderitemsWithTagsUseCase
			);

		public ConvertAppLessOrderToEntityUseCaseBuilder W(EnrichOrderWithDeliverypointDetailsUseCaseBuilder enrichOrderWithDeliverypointDetailsUseCaseBuilder) => new ConvertAppLessOrderToEntityUseCaseBuilder(
			EnrichOrderWithOutletDetailsUseCase,
            EnrichOrderWithCompanyDetailsUseCase,
			EnrichOrderWithServiceMethodDetailsUseCase,
			EnrichOrderWithCheckoutMethodDetailsUseCase,
			enrichOrderWithDeliverypointDetailsUseCaseBuilder.Build(),
			EnrichOrderWithChargeToDeliverypointUseCase,
			EnrichOrderWithDeliveryInformationUseCase,
			EnrichOrderWithOrderitemsUseCase,
			EnrichOrderWithTaxesUseCase,
			EnrichOrderWithOrderStatusUseCase,
            EnrichOrderWithClientUseCase,
            EnrichOrderWithAnalyticsUseCase,
            EnrichOrderitemsWithTagsUseCase
			);

		public ConvertAppLessOrderToEntityUseCaseBuilder W(EnrichOrderWithChargeToDeliverypointUseCaseBuilder enrichOrderWithChargeToDeliverypointUseCaseBuilder) => new ConvertAppLessOrderToEntityUseCaseBuilder(
			EnrichOrderWithOutletDetailsUseCase,
            EnrichOrderWithCompanyDetailsUseCase,
			EnrichOrderWithServiceMethodDetailsUseCase,
			EnrichOrderWithCheckoutMethodDetailsUseCase,
			EnrichOrderWithDeliverypointDetailsUseCase,
			enrichOrderWithChargeToDeliverypointUseCaseBuilder.Build(),
			EnrichOrderWithDeliveryInformationUseCase,
			EnrichOrderWithOrderitemsUseCase,
			EnrichOrderWithTaxesUseCase,
			EnrichOrderWithOrderStatusUseCase,
            EnrichOrderWithClientUseCase,
            EnrichOrderWithAnalyticsUseCase,
            EnrichOrderitemsWithTagsUseCase
			);

		public ConvertAppLessOrderToEntityUseCaseBuilder W(EnrichOrderWithDeliveryInformationUseCaseBuilder enrichOrderWithDeliveryInformationUseCaseBuilder) => new ConvertAppLessOrderToEntityUseCaseBuilder(
			EnrichOrderWithOutletDetailsUseCase,
            EnrichOrderWithCompanyDetailsUseCase,
            EnrichOrderWithServiceMethodDetailsUseCase,
			EnrichOrderWithCheckoutMethodDetailsUseCase,
			EnrichOrderWithDeliverypointDetailsUseCase,
			EnrichOrderWithChargeToDeliverypointUseCase,
			enrichOrderWithDeliveryInformationUseCaseBuilder.Build(),
			EnrichOrderWithOrderitemsUseCase,
			EnrichOrderWithTaxesUseCase,
			EnrichOrderWithOrderStatusUseCase,
            EnrichOrderWithClientUseCase,
            EnrichOrderWithAnalyticsUseCase,
            EnrichOrderitemsWithTagsUseCase
		);

		public ConvertAppLessOrderToEntityUseCaseBuilder W(EnrichOrderWithOrderitemsUseCaseBuilder enrichOrderWithOrderitemsUseCaseBuilder) => new ConvertAppLessOrderToEntityUseCaseBuilder(
			EnrichOrderWithOutletDetailsUseCase,
            EnrichOrderWithCompanyDetailsUseCase,
			EnrichOrderWithServiceMethodDetailsUseCase,
			EnrichOrderWithCheckoutMethodDetailsUseCase,
			EnrichOrderWithDeliverypointDetailsUseCase,
			EnrichOrderWithChargeToDeliverypointUseCase,
			EnrichOrderWithDeliveryInformationUseCase,
			enrichOrderWithOrderitemsUseCaseBuilder.Build(),
			EnrichOrderWithTaxesUseCase,
			EnrichOrderWithOrderStatusUseCase,
            EnrichOrderWithClientUseCase,
            EnrichOrderWithAnalyticsUseCase,
            EnrichOrderitemsWithTagsUseCase
		);

		public ConvertAppLessOrderToEntityUseCaseBuilder W(EnrichOrderWithTaxesUseCaseBuilder enrichOrderWithTaxesUseCaseBuilder) => new ConvertAppLessOrderToEntityUseCaseBuilder(
			EnrichOrderWithOutletDetailsUseCase,
            EnrichOrderWithCompanyDetailsUseCase,
			EnrichOrderWithServiceMethodDetailsUseCase,
			EnrichOrderWithCheckoutMethodDetailsUseCase,
			EnrichOrderWithDeliverypointDetailsUseCase,
			EnrichOrderWithChargeToDeliverypointUseCase,
			EnrichOrderWithDeliveryInformationUseCase,
			EnrichOrderWithOrderitemsUseCase,
			enrichOrderWithTaxesUseCaseBuilder.Build(),
			EnrichOrderWithOrderStatusUseCase,
            EnrichOrderWithClientUseCase,
            EnrichOrderWithAnalyticsUseCase,
            EnrichOrderitemsWithTagsUseCase
		);

		public ConvertAppLessOrderToEntityUseCaseBuilder W(EnrichOrderWithOrderStatusUseCaseBuilder enrichOrderWithOrderStatusUseCaseBuilder) => new ConvertAppLessOrderToEntityUseCaseBuilder(
			EnrichOrderWithOutletDetailsUseCase,
            EnrichOrderWithCompanyDetailsUseCase,
			EnrichOrderWithServiceMethodDetailsUseCase,
			EnrichOrderWithCheckoutMethodDetailsUseCase,
			EnrichOrderWithDeliverypointDetailsUseCase,
			EnrichOrderWithChargeToDeliverypointUseCase,
			EnrichOrderWithDeliveryInformationUseCase,
			EnrichOrderWithOrderitemsUseCase,
			EnrichOrderWithTaxesUseCase,
			enrichOrderWithOrderStatusUseCaseBuilder.Build(),
            EnrichOrderWithClientUseCase,
            EnrichOrderWithAnalyticsUseCase,
            EnrichOrderitemsWithTagsUseCase
		);

        public ConvertAppLessOrderToEntityUseCaseBuilder W(EnrichOrderWithClientUseCaseBuilder enrichOrderWithClientUseCaseBuilder) => new ConvertAppLessOrderToEntityUseCaseBuilder(
            EnrichOrderWithOutletDetailsUseCase,
            EnrichOrderWithCompanyDetailsUseCase,
            EnrichOrderWithServiceMethodDetailsUseCase,
            EnrichOrderWithCheckoutMethodDetailsUseCase,
            EnrichOrderWithDeliverypointDetailsUseCase,
            EnrichOrderWithChargeToDeliverypointUseCase,
            EnrichOrderWithDeliveryInformationUseCase,
            EnrichOrderWithOrderitemsUseCase,
            EnrichOrderWithTaxesUseCase,
            EnrichOrderWithOrderStatusUseCase,
			enrichOrderWithClientUseCaseBuilder.Build(),
            EnrichOrderWithAnalyticsUseCase,
            EnrichOrderitemsWithTagsUseCase
		);

        public ConvertAppLessOrderToEntityUseCaseBuilder W(EnrichOrderitemsWithTagsUseCaseBuilder enrichOrderitemsWithTagsUseCaseBuilder) => new ConvertAppLessOrderToEntityUseCaseBuilder(
            EnrichOrderWithOutletDetailsUseCase,
            EnrichOrderWithCompanyDetailsUseCase,
            EnrichOrderWithServiceMethodDetailsUseCase,
            EnrichOrderWithCheckoutMethodDetailsUseCase,
            EnrichOrderWithDeliverypointDetailsUseCase,
            EnrichOrderWithChargeToDeliverypointUseCase,
            EnrichOrderWithDeliveryInformationUseCase,
            EnrichOrderWithOrderitemsUseCase,
            EnrichOrderWithTaxesUseCase,
            EnrichOrderWithOrderStatusUseCase,
            EnrichOrderWithClientUseCase,
            EnrichOrderWithAnalyticsUseCase,
            enrichOrderitemsWithTagsUseCaseBuilder.Build()
        );

        public ConvertOrderToEntityUseCase Build() =>
			new ConvertOrderToEntityUseCase(
				EnrichOrderWithOutletDetailsUseCase,
                EnrichOrderWithCompanyDetailsUseCase,
				EnrichOrderWithServiceMethodDetailsUseCase,
				EnrichOrderWithCheckoutMethodDetailsUseCase,
				EnrichOrderWithDeliverypointDetailsUseCase,
				EnrichOrderWithChargeToDeliverypointUseCase,
				EnrichOrderWithDeliveryInformationUseCase,
				EnrichOrderWithOrderitemsUseCase,
				EnrichOrderWithTaxesUseCase,
				EnrichOrderWithOrderStatusUseCase, 
                EnrichOrderWithClientUseCase,
                EnrichOrderWithAnalyticsUseCase,
                EnrichOrderitemsWithTagsUseCase);
	}
}