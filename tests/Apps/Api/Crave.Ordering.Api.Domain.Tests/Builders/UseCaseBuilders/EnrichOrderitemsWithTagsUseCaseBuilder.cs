﻿using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Api.Domain.Tests.Builders.RepositoryBuilders;
using Crave.Ordering.Api.Domain.UseCases;

namespace Crave.Ordering.Api.Domain.Tests.Builders.UseCaseBuilders
{
    internal class EnrichOrderitemsWithTagsUseCaseBuilder
    {
        private IProductTagRepository ProductTagRepository { get; }
        private IProductCategoryTagRepository ProductCategoryTagRepository { get; }
        private IAlterationoptionTagRepository AlterationoptionTagRepository { get; }

        public EnrichOrderitemsWithTagsUseCaseBuilder() : this(
            new ProductTagRepositoryMockBuilder().Build(),
            new ProductCategoryTagRepositoryMockBuilder().Build(),
            new AlterationoptionTagRepositoryMockBuilder().Build())
        {

        }

        private EnrichOrderitemsWithTagsUseCaseBuilder(IProductTagRepository productTagRepository, IProductCategoryTagRepository productCategoryTagRepository, IAlterationoptionTagRepository alterationoptionTagRepository)
        {
            ProductTagRepository = productTagRepository;
            ProductCategoryTagRepository = productCategoryTagRepository;
            AlterationoptionTagRepository = alterationoptionTagRepository;
        }

        public EnrichOrderitemsWithTagsUseCase Build() => new EnrichOrderitemsWithTagsUseCase(ProductTagRepository, ProductCategoryTagRepository, AlterationoptionTagRepository);
    }
}
