﻿using Crave.Ordering.Api.Domain.Interfaces.UseCases;
using Crave.Ordering.Api.Domain.UseCases.Requests;
using Moq;

namespace Crave.Ordering.Api.Domain.Tests.Builders.UseCaseBuilders
{
    public class UpdateOrderRoutestephandlerStatusesUseCaseBuilder
    {
        private readonly bool useCaseResponse;

        public UpdateOrderRoutestephandlerStatusesUseCaseBuilder() : this(true)
        {

        }

        private UpdateOrderRoutestephandlerStatusesUseCaseBuilder(bool response)
        {
            this.useCaseResponse = response;
        }
        
        public UpdateOrderRoutestephandlerStatusesUseCaseBuilder WithResponse(bool response) => new UpdateOrderRoutestephandlerStatusesUseCaseBuilder(response);

        public Mock<IUpdateOrderRoutestephandlerStatusesUseCase> Build()
        {
            Mock<IUpdateOrderRoutestephandlerStatusesUseCase> useCaseMock = new Mock<IUpdateOrderRoutestephandlerStatusesUseCase>();
            useCaseMock.Setup(uc => uc.ExecuteAsync(It.IsAny<UpdateOrderRoutestephandlerStatuses>())).ReturnsAsync(this.useCaseResponse);

            return useCaseMock;
        }
    }
}