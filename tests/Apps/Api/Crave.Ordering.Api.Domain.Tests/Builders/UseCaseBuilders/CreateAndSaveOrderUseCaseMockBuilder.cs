﻿using Crave.Ordering.Api.Domain.Interfaces.UseCases;
using Crave.Ordering.Api.Domain.UseCases.Requests;
using Crave.Ordering.Api.Domain.UseCases.Responses;
using Moq;

namespace Crave.Ordering.Api.Domain.Tests.Builders.UseCaseBuilders
{
	public class CreateAndSaveOrderUseCaseMockBuilder
	{
		private readonly CreateAndSaveOrderResponse useCaseResponse;

		public CreateAndSaveOrderUseCaseMockBuilder() : this(CreateAndSaveOrderResponse.AsSuccess("abc"))
		{
		}

		private CreateAndSaveOrderUseCaseMockBuilder(CreateAndSaveOrderResponse response) => useCaseResponse = response;

		public CreateAndSaveOrderUseCaseMockBuilder WithResponse(CreateAndSaveOrderResponse response) => new CreateAndSaveOrderUseCaseMockBuilder(response);

		public ICreateAndSaveOrderUseCase Build()
		{
			Mock<ICreateAndSaveOrderUseCase> useCaseMock = new Mock<ICreateAndSaveOrderUseCase>();
			useCaseMock.Setup(uc => uc.ExecuteAsync(It.IsAny<CreateOrderRequest>())).ReturnsAsync(useCaseResponse);
			return useCaseMock.Object;
		}
	}
}
