﻿using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Api.Domain.Tests.Builders.RepositoryBuilders;
using Crave.Ordering.Api.Domain.UseCases;

namespace Crave.Ordering.Api.Domain.Tests.Builders.UseCaseBuilders
{
	internal class EnrichOrderWithCheckoutMethodDetailsUseCaseBuilder
	{
		private ICheckoutMethodRepository CheckoutMethodRepository { get; }

		public EnrichOrderWithCheckoutMethodDetailsUseCaseBuilder() : this(new CheckoutMethodRepositoryMockBuilder().Build())
		{

		}

		private EnrichOrderWithCheckoutMethodDetailsUseCaseBuilder(ICheckoutMethodRepository checkoutMethodRepository) => CheckoutMethodRepository = checkoutMethodRepository;

		public EnrichOrderWithCheckoutMethodDetailsUseCaseBuilder W(CheckoutMethodRepositoryMockBuilder checkoutMethodRepositoryMockBuilder) =>
			new EnrichOrderWithCheckoutMethodDetailsUseCaseBuilder(checkoutMethodRepositoryMockBuilder.Build());

		public EnrichOrderWithCheckoutMethodDetailsUseCase Build() => new EnrichOrderWithCheckoutMethodDetailsUseCase(CheckoutMethodRepository);
	}
}