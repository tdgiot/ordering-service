﻿using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Api.Domain.Interfaces.UseCases;
using Crave.Ordering.Api.Domain.Tests.Builders.RepositoryBuilders;
using Crave.Ordering.Api.Domain.UseCases;
using Crave.Ordering.Shared.OrderNotifications.Interfaces.UseCases;
using Moq;

namespace Crave.Ordering.Api.Domain.Tests.Builders.UseCaseBuilders
{
	internal class CreateOrderRouteUseCaseBuilder
	{
		public CreateOrderRouteUseCaseBuilder() : this(
            new OrderRepositoryMockBuilder().Build(),
            new ValidateOrderBeforeRoutingUseCaseMockBuilder().Build(),
            new RouteOrderUseCaseMockBuilder().Build(),
            new CreateReceiptUseCaseMockBuilder().Build(),
            new Mock<ISendOrderNotificationUseCase>().Object)
		{

		}

		private CreateOrderRouteUseCaseBuilder(IOrderRepository orderRepository,
                                               ValidateOrderBeforeRoutingUseCase validateOrderBeforeRoutingUseCase,
                                               IRouteOrderUseCase routeOrderUseCase,
                                               ICreateReceiptUseCase createReceiptUseCase,
                                               ISendOrderNotificationUseCase sendOrderNotificationUseCase)
		{
            OrderRepository = orderRepository;
            ValidateOrderBeforeRoutingUseCase = validateOrderBeforeRoutingUseCase;
            RouteOrderUseCase = routeOrderUseCase;
            CreateReceiptUseCase = createReceiptUseCase;
            SendOrderNotificationUseCase = sendOrderNotificationUseCase;
		}

        private IOrderRepository OrderRepository { get; }
        private ValidateOrderBeforeRoutingUseCase ValidateOrderBeforeRoutingUseCase { get; }
        private IRouteOrderUseCase RouteOrderUseCase { get; }
        private ICreateReceiptUseCase CreateReceiptUseCase { get; }
        private ISendOrderNotificationUseCase SendOrderNotificationUseCase { get; }

        public CreateOrderRouteUseCaseBuilder W(IOrderRepository orderRepository) => 
            new CreateOrderRouteUseCaseBuilder(orderRepository, ValidateOrderBeforeRoutingUseCase, RouteOrderUseCase, CreateReceiptUseCase, SendOrderNotificationUseCase);

        public CreateOrderRouteUseCaseBuilder W(ValidateOrderBeforeRoutingUseCase validateOrderBeforeRoutingUseCase) => 
            new CreateOrderRouteUseCaseBuilder(OrderRepository, validateOrderBeforeRoutingUseCase, RouteOrderUseCase, CreateReceiptUseCase, SendOrderNotificationUseCase);

        public CreateOrderRouteUseCaseBuilder W(IRouteOrderUseCase routeOrderUseCase) => 
            new CreateOrderRouteUseCaseBuilder(OrderRepository, ValidateOrderBeforeRoutingUseCase, routeOrderUseCase, CreateReceiptUseCase, SendOrderNotificationUseCase);

        public CreateOrderRouteUseCaseBuilder W(ICreateReceiptUseCase createReceiptUseCase) => 
            new CreateOrderRouteUseCaseBuilder(OrderRepository, ValidateOrderBeforeRoutingUseCase, RouteOrderUseCase, createReceiptUseCase, SendOrderNotificationUseCase);

        public CreateOrderRouteUseCaseBuilder W(ISendOrderNotificationUseCase sendOrderNotificationUseCase) => 
            new CreateOrderRouteUseCaseBuilder(OrderRepository, ValidateOrderBeforeRoutingUseCase, RouteOrderUseCase, CreateReceiptUseCase, sendOrderNotificationUseCase);

        public CreateOrderRouteUseCase Build() => new CreateOrderRouteUseCase(OrderRepository, ValidateOrderBeforeRoutingUseCase, RouteOrderUseCase, CreateReceiptUseCase, SendOrderNotificationUseCase);
	}
}