﻿using Crave.Ordering.Api.Domain.UseCases;

namespace Crave.Ordering.Api.Domain.Tests.Builders.UseCaseBuilders
{
	internal class EnrichOrderWithOrderStatusUseCaseBuilder
	{
		public EnrichOrderWithOrderStatusUseCase Build() => new EnrichOrderWithOrderStatusUseCase();
	}
}