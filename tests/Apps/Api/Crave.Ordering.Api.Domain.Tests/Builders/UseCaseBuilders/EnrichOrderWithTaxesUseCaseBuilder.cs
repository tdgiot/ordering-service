﻿using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Api.Domain.Tests.Builders.RepositoryBuilders;
using Crave.Ordering.Api.Domain.UseCases;

namespace Crave.Ordering.Api.Domain.Tests.Builders.UseCaseBuilders
{
	internal class EnrichOrderWithTaxesUseCaseBuilder
	{
		private ITaxTariffRepository TaxTariffRepository { get; }

		public EnrichOrderWithTaxesUseCaseBuilder() : this(new TaxTariffRepositoryMockBuilder().Build())
		{
		}

		private EnrichOrderWithTaxesUseCaseBuilder(ITaxTariffRepository taxTariffRepository) => TaxTariffRepository = taxTariffRepository;

		public EnrichOrderWithTaxesUseCaseBuilder W(TaxTariffRepositoryMockBuilder taxTariffRepositoryMockBuilder) => new EnrichOrderWithTaxesUseCaseBuilder(taxTariffRepositoryMockBuilder.Build());

		public EnrichOrderWithTaxesUseCase Build() => new EnrichOrderWithTaxesUseCase(TaxTariffRepository);
    }
}