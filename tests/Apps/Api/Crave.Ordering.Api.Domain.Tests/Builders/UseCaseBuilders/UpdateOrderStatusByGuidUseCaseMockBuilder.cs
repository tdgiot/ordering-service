﻿using Crave.Ordering.Api.Domain.Interfaces.UseCases;
using Crave.Ordering.Api.Domain.UseCases.Requests;
using Crave.Ordering.Api.Domain.UseCases.Responses;
using Moq;

namespace Crave.Ordering.Api.Domain.Tests.Builders.UseCaseBuilders
{
	public class UpdateOrderStatusByGuidUseCaseMockBuilder
	{
		private readonly ResponseBase useCaseResult;

		public UpdateOrderStatusByGuidUseCaseMockBuilder() : this(ResponseBase.AsSuccess())
		{
		}

		private UpdateOrderStatusByGuidUseCaseMockBuilder(ResponseBase result) => useCaseResult = result;

		public UpdateOrderStatusByGuidUseCaseMockBuilder WithResult(ResponseBase result) => new UpdateOrderStatusByGuidUseCaseMockBuilder(result);

		public IUpdateOrderStatusByGuidUseCase Build()
		{
			Mock<IUpdateOrderStatusByGuidUseCase> useCaseMock = new Mock<IUpdateOrderStatusByGuidUseCase>();
			useCaseMock.Setup(uc => uc.ExecuteAsync(It.IsAny<UpdateOrderStatusByGuidRequest>())).ReturnsAsync(useCaseResult);

			return useCaseMock.Object;
		}
	}
}
