﻿using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Api.Domain.Converters;
using Crave.Ordering.Api.Domain.Interfaces.UseCases;
using Crave.Ordering.Api.Domain.Tests.Builders.Converters;
using Crave.Ordering.Api.Domain.Tests.Builders.RepositoryBuilders;
using Crave.Ordering.Api.Domain.UseCases;

namespace Crave.Ordering.Api.Domain.Tests.Builders.UseCaseBuilders
{
	internal class GetOrderUseCaseBuilder
	{
		private readonly IOrderRepository orderRepository;
		private readonly OrderConverter orderConverter;

		public GetOrderUseCaseBuilder() : this(new OrderRepositoryMockBuilder().Build(), new OrderConverterBuilder().Build())
		{
			
		}

		private GetOrderUseCaseBuilder(IOrderRepository orderRepository, OrderConverter orderConverter)
		{
			this.orderRepository = orderRepository;
			this.orderConverter = orderConverter;
		}

		public IGetOrderUseCase Build() => new GetOrderUseCase(orderRepository, orderConverter);

		public GetOrderUseCaseBuilder W(OrderRepositoryMockBuilder builder) => W(builder.Build());

		public GetOrderUseCaseBuilder W(IOrderRepository repository) => new GetOrderUseCaseBuilder(repository, orderConverter);
	}
}
