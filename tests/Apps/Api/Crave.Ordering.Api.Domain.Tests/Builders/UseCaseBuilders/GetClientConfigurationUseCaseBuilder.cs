﻿using Crave.Ordering.Api.Domain.Interfaces.UseCases;
using Crave.Ordering.Api.Domain.UseCases.Requests;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.Tests.Builders;
using Moq;

namespace Crave.Ordering.Api.Domain.Tests.Builders.UseCaseBuilders
{
    public class GetClientConfigurationUseCaseBuilder
    {
        private readonly ClientConfigurationEntity? useCaseResponse;

        public GetClientConfigurationUseCaseBuilder() : this (new ClientConfigurationEntity { IsNew = false})
        {

        }

        private GetClientConfigurationUseCaseBuilder(ClientConfigurationEntity? entity)
        {
            this.useCaseResponse = entity;
        }

        public GetClientConfigurationUseCaseBuilder WithResponse(ClientConfigurationEntityBuilder entity) => new GetClientConfigurationUseCaseBuilder(entity.Build());

        public Mock<IGetClientConfigurationUseCase> Build()
        {
            Mock<IGetClientConfigurationUseCase> useCase = new Mock<IGetClientConfigurationUseCase>();
            useCase.Setup(uc => uc.ExecuteAsync(It.IsAny<GetClientConfiguration>())).ReturnsAsync(this.useCaseResponse);

            return useCase;
        }
    }
}