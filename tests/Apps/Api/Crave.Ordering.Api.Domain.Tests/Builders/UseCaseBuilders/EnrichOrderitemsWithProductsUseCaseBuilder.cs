﻿using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Api.Domain.Tests.Builders.RepositoryBuilders;
using Crave.Ordering.Api.Domain.UseCases;

namespace Crave.Ordering.Api.Domain.Tests.Builders.UseCaseBuilders
{
	internal class EnrichOrderitemsWithProductsUseCaseBuilder
	{
		private IProductRepository ProductRepository { get; }
		private IScheduleRepository ScheduleRepository { get; }

		public EnrichOrderitemsWithProductsUseCaseBuilder() : this(
			new ProductRepositoryMockBuilder().Build().Object,
			new ScheduleRepositoryMockBuilder().Build())
		{

		}

		private EnrichOrderitemsWithProductsUseCaseBuilder(IProductRepository productRepository, IScheduleRepository scheduleRepository)
		{
			ProductRepository = productRepository;
            ScheduleRepository = scheduleRepository;
        }

		public EnrichOrderitemsWithProductsUseCaseBuilder W(ProductRepositoryMockBuilder productRepositoryMockBuilder) => W(productRepositoryMockBuilder.Build().Object);

		public EnrichOrderitemsWithProductsUseCaseBuilder W(IProductRepository productRepository) =>
			new EnrichOrderitemsWithProductsUseCaseBuilder(productRepository, ScheduleRepository);

		public EnrichOrderitemsWithProductsUseCaseBuilder W(ScheduleRepositoryMockBuilder scheduleRepositoryBuilder) =>
			new EnrichOrderitemsWithProductsUseCaseBuilder(ProductRepository, scheduleRepositoryBuilder.Build());

		public EnrichOrderitemsWithProductsUseCase Build() => new EnrichOrderitemsWithProductsUseCase(ProductRepository, ScheduleRepository);
	}
}