﻿using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Api.Domain.Tests.Builders.RepositoryBuilders;
using Crave.Ordering.Api.Domain.UseCases;

namespace Crave.Ordering.Api.Domain.Tests.Builders.UseCaseBuilders
{
	internal class EnrichOrderitemsWithAlterationitemsUseCaseBuilder
	{
		private IAlterationitemRepository AlterationitemRepository { get; }

		public EnrichOrderitemsWithAlterationitemsUseCaseBuilder() : this(
			new AlterationitemRepositoryMockBuilder().Build())
		{

		}

		private EnrichOrderitemsWithAlterationitemsUseCaseBuilder(IAlterationitemRepository alterationitemRepository)
		{
			AlterationitemRepository = alterationitemRepository;
        }

		public EnrichOrderitemsWithAlterationitemsUseCaseBuilder W(AlterationitemRepositoryMockBuilder alterationitemRepositoryMockBuilder) =>
			new EnrichOrderitemsWithAlterationitemsUseCaseBuilder(alterationitemRepositoryMockBuilder.Build());

        public EnrichOrderitemsWithAlterationitemsUseCase Build() => new EnrichOrderitemsWithAlterationitemsUseCase(AlterationitemRepository);
	}
}