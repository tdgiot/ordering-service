﻿using Crave.Ordering.Api.Core.Enums;
using Crave.Ordering.Api.Domain.Exceptions;
using Crave.Ordering.Api.Domain.Interfaces.UseCases;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.Tests.Builders;
using Moq;

namespace Crave.Ordering.Api.Domain.Tests.Builders.UseCaseBuilders
{
    public class SaveOrderUseCaseBuilder
    {
        private readonly OrderEntity useCaseResponse;
        private readonly bool throwCheckoutException;

        public SaveOrderUseCaseBuilder() : this(new OrderEntityBuilder().Build())
        {

        }

        private SaveOrderUseCaseBuilder(OrderEntity orderEntity, bool throwException = false)
        {
            this.useCaseResponse = orderEntity;
            this.throwCheckoutException = throwException;
        }

        public SaveOrderUseCaseBuilder WithResponse(OrderEntityBuilder orderEntity) => new SaveOrderUseCaseBuilder(orderEntity.Build());

        public SaveOrderUseCaseBuilder WithCheckoutException() => new SaveOrderUseCaseBuilder(this.useCaseResponse, true);

        public Mock<ISaveOrderUseCase> Build()
        {
            Mock<ISaveOrderUseCase> useCaseMock = new Mock<ISaveOrderUseCase>();
            useCaseMock.Setup(uc => uc.ExecuteAsync(It.IsAny<OrderEntity>())).ReturnsAsync(this.useCaseResponse);

            if (throwCheckoutException)
            {
                useCaseMock.Setup(uc => uc.ExecuteAsync(It.IsAny<OrderEntity>())).Throws(new CheckoutValidationException(ValidationErrorSubType.CheckoutMethodInvalid));
            }

            return useCaseMock;
        }
    }
}