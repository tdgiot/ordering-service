﻿using System.Collections.Generic;
using Crave.Ordering.Api.Domain.Interfaces.UseCases;
using Crave.Ordering.Api.Domain.UseCases.Requests;
using Crave.Ordering.Api.Domain.UseCases.Responses;
using Moq;

namespace Crave.Ordering.Api.Domain.Tests.Builders.UseCaseBuilders
{
    public class GetOrdersHistoryForTerminalUseCaseMockBuilder
    {
        private readonly ResponseBase<IEnumerable<Order>> useCaseResult;

        public GetOrdersHistoryForTerminalUseCaseMockBuilder() : this(ResponseBase<IEnumerable<Order>>.AsSuccess(new List<Order>()))
        {
        }

        private GetOrdersHistoryForTerminalUseCaseMockBuilder(ResponseBase<IEnumerable<Order>> result) => this.useCaseResult = result;

        public GetOrdersHistoryForTerminalUseCaseMockBuilder W(ResponseBase<IEnumerable<Order>> response) => new GetOrdersHistoryForTerminalUseCaseMockBuilder(response);

        public IGetOrderHistoryForTerminalUseCase Build()
        {
            Mock<IGetOrderHistoryForTerminalUseCase> useCaseMock = new Mock<IGetOrderHistoryForTerminalUseCase>();
            useCaseMock.Setup(uc => uc.ExecuteAsync(It.IsAny<GetOrderHistoryForTerminal>())).ReturnsAsync(this.useCaseResult);
            return useCaseMock.Object;
        }
    }
}
