﻿using Crave.Ordering.Api.Domain.UseCases;
using Crave.Ordering.Api.Domain.UseCases.Requests;
using Crave.Ordering.Shared.Enums;
using Moq;

namespace Crave.Ordering.Api.Domain.Tests.Builders.UseCaseBuilders
{
    public class ValidateOrderBeforeRoutingUseCaseMockBuilder
    {
        private CreateOrderRouteResult createOrderRouteResult = CreateOrderRouteResult.Ok;

        public ValidateOrderBeforeRoutingUseCaseMockBuilder WithResult(CreateOrderRouteResult result) => new ValidateOrderBeforeRoutingUseCaseMockBuilder
        {
            createOrderRouteResult = result
        };

        public ValidateOrderBeforeRoutingUseCase Build()
        {
            Mock<ValidateOrderBeforeRoutingUseCase> mock = new Mock<ValidateOrderBeforeRoutingUseCase>();
            mock.Setup(m => m.Execute(It.IsAny<ValidateOrderBeforeRouting>())).Returns(this.createOrderRouteResult);
            return mock.Object;
        }
    }
}