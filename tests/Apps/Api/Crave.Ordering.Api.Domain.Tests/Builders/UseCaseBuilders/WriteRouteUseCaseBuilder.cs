﻿using Crave.Ordering.Api.Domain.Interfaces.UseCases;
using Crave.Ordering.Api.Domain.UseCases.Requests;
using Moq;

namespace Crave.Ordering.Api.Domain.Tests.Builders.UseCaseBuilders
{
    public class WriteRouteUseCaseBuilder
    {
        public Mock<IWriteRouteUseCase> Build()
        {
            Mock<IWriteRouteUseCase> useCaseMock = new Mock<IWriteRouteUseCase>();
            useCaseMock.Setup(uc => uc.ExecuteAsync(It.IsAny<WriteRoute>())).ReturnsAsync(true);

            return useCaseMock;
        }
    }
}