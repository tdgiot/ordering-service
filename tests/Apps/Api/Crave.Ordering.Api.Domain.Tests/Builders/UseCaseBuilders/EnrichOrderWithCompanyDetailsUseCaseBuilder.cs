﻿using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Api.Domain.Tests.Builders.RepositoryBuilders;
using Crave.Ordering.Api.Domain.UseCases;

namespace Crave.Ordering.Api.Domain.Tests.Builders.UseCaseBuilders
{
	internal class EnrichOrderWithCompanyDetailsUseCaseBuilder
	{
		private ICompanyRepository CompanyRepository { get; }

		public EnrichOrderWithCompanyDetailsUseCaseBuilder() : this(new CompanyRepositoryMockBuilder().Build())
		{

		}

		private EnrichOrderWithCompanyDetailsUseCaseBuilder(ICompanyRepository companyRepository) => CompanyRepository = companyRepository;

		public EnrichOrderWithCompanyDetailsUseCaseBuilder W(ICompanyRepositoryBuilder companyRepositoryMockBuilder) =>
			new EnrichOrderWithCompanyDetailsUseCaseBuilder(companyRepositoryMockBuilder.Build());

		public EnrichOrderWithCompanyDetailsUseCase Build() => new EnrichOrderWithCompanyDetailsUseCase(CompanyRepository);
	}
}