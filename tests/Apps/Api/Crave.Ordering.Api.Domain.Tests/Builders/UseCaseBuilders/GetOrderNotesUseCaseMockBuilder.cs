﻿using Crave.Ordering.Api.Domain.Interfaces.UseCases;
using Crave.Ordering.Api.Domain.UseCases.Requests;
using Moq;

namespace Crave.Ordering.Api.Domain.Tests.Builders.UseCaseBuilders
{
    public class GetOrderNotesUseCaseMockBuilder
    {
        public IGetOrderNotesUseCase Build()
        {
            Mock<IGetOrderNotesUseCase> useCaseMock = new Mock<IGetOrderNotesUseCase>();
            useCaseMock.Setup(uc => uc.Execute(It.IsAny<GetOrderNotes>())).Returns("");
            return useCaseMock.Object;
        }
    }
}
