﻿using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Api.Domain.Converters;
using Crave.Ordering.Api.Domain.Interfaces.UseCases;
using Crave.Ordering.Api.Domain.Tests.Builders.Converters;
using Crave.Ordering.Api.Domain.Tests.Builders.RepositoryBuilders;
using Crave.Ordering.Api.Domain.UseCases;

namespace Crave.Ordering.Api.Domain.Tests.Builders.UseCaseBuilders
{
    public class GetOrderHistoryForTerminalUseCaseBuilder
    {
        private readonly IOrderRepository orderRepository;
        private readonly OrderConverter orderConverter;

        public GetOrderHistoryForTerminalUseCaseBuilder() : this(new OrderRepositoryMockBuilder().Build(), new OrderConverterBuilder().Build())
        {
        }

        private GetOrderHistoryForTerminalUseCaseBuilder(IOrderRepository orderRepository, OrderConverter orderConverter)
        {
            this.orderRepository = orderRepository;
            this.orderConverter = orderConverter;
        }

        public GetOrderHistoryForTerminalUseCaseBuilder W(OrderRepositoryMockBuilder orderRepositoryMockBuilder) => W(orderRepositoryMockBuilder.Build());

        public GetOrderHistoryForTerminalUseCaseBuilder W(IOrderRepository repository) => new GetOrderHistoryForTerminalUseCaseBuilder(repository, this.orderConverter);

        public IGetOrderHistoryForTerminalUseCase Build() => new GetOrderHistoryForTerminalUseCase(this.orderRepository, this.orderConverter);
    }
}
