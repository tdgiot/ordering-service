﻿using Crave.Ordering.Api.Domain.Interfaces.UseCases;
using Crave.Ordering.Api.Domain.UseCases.Requests;
using Crave.Ordering.Api.Domain.UseCases.Responses;
using Crave.Ordering.Shared.Enums;
using Moq;

namespace Crave.Ordering.Api.Domain.Tests.Builders.UseCaseBuilders
{
    public class CreateOrderRouteUseCaseMockBuilder
    {
        public Mock<ICreateOrderRouteUseCase> Build()
        {
            Mock<ICreateOrderRouteUseCase> useCaseMock = new Mock<ICreateOrderRouteUseCase>();
            useCaseMock.Setup(uc => uc.ExecuteAsync(It.IsAny<CreateOrderRoute>())).ReturnsAsync(new CreateOrderRouteResponse(CreateOrderRouteResult.Ok));

            return useCaseMock;
        }
    }
}