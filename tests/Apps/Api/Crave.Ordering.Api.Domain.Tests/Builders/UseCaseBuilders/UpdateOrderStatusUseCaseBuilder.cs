﻿using Crave.Ordering.Api.Domain.Interfaces.UseCases;
using Crave.Ordering.Api.Domain.UseCases.Requests;
using Moq;

namespace Crave.Ordering.Api.Domain.Tests.Builders.UseCaseBuilders
{
    public class UpdateOrderStatusUseCaseBuilder
    {
        private readonly bool throwsOrderNotFoundException;
        private readonly bool useCaseResult;

        public UpdateOrderStatusUseCaseBuilder() : this(true, false)
        {

        }

        private UpdateOrderStatusUseCaseBuilder(bool result, bool throwsOrderNotFoundException)
        {
            this.useCaseResult = result;
            this.throwsOrderNotFoundException = throwsOrderNotFoundException;
        }

        public Mock<IUpdateOrderStatusUseCase> Build()
        {
            Mock<IUpdateOrderStatusUseCase> useCaseMock = new Mock<IUpdateOrderStatusUseCase>();

            if (this.throwsOrderNotFoundException)
            {
                useCaseMock.Setup(uc => uc.ExecuteAsync(It.IsAny<UpdateOrderStatus>())).Throws<CraveException>();
            }
            else
            {
                useCaseMock.Setup(uc => uc.ExecuteAsync(It.IsAny<UpdateOrderStatus>())).ReturnsAsync(this.useCaseResult);
            }

            return useCaseMock;
        }
    }
}