﻿using Crave.Ordering.Api.Domain.UseCases;

namespace Crave.Ordering.Api.Domain.Tests.Builders.UseCaseBuilders
{
	public interface IGetDistanceToOutletUseCaseBuilder
	{
		GetDistanceToOutletUseCase Build();
		IGetDistanceToOutletUseCaseBuilder W(IGetOutletLocationUseCaseBuilder getOutletLocationUseCaseBuilder);
		IGetDistanceToOutletUseCaseBuilder W(IGetDistanceBetweenLocationsUseCaseBuilder getDistanceBetweenLocationsUseCaseBuilder);
	}
}
