﻿using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Api.Domain.Tests.Builders.RepositoryBuilders;
using Crave.Ordering.Api.Domain.UseCases;

namespace Crave.Ordering.Api.Domain.Tests.Builders.UseCaseBuilders
{
	internal class EnrichOrderWithClientUseCaseBuilder
	{
        public EnrichOrderWithClientUseCaseBuilder() : this(new DeviceRepositoryMockBuilder().Build())
        {
        }

        private EnrichOrderWithClientUseCaseBuilder(IDeviceRepository deviceRepository) => DeviceRepository = deviceRepository;

        private IDeviceRepository DeviceRepository { get; }

        public EnrichOrderWithClientUseCaseBuilder WithDeviceRepository(DeviceRepositoryMockBuilder deviceRepositoryMockBuilder) => new EnrichOrderWithClientUseCaseBuilder(deviceRepositoryMockBuilder.Build());

        public EnrichOrderWithClientUseCase Build() => new EnrichOrderWithClientUseCase(DeviceRepository);
    }
}