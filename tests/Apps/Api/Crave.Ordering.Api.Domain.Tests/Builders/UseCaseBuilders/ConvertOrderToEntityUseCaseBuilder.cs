﻿using Crave.Ordering.Api.Domain.UseCases;

namespace Crave.Ordering.Api.Domain.Tests.Builders.UseCaseBuilders
{
    internal class ConvertOrderToEntityUseCaseBuilder
    {
		private EnrichOrderWithOutletDetailsUseCase EnrichOrderWithOutletDetailsUseCase { get; }
		private EnrichOrderWithCompanyDetailsUseCase EnrichOrderWithCompanyDetailsUseCase { get; }
		private EnrichOrderWithServiceMethodDetailsUseCase EnrichOrderWithServiceMethodDetailsUseCase { get; }
		private EnrichOrderWithCheckoutMethodDetailsUseCase EnrichOrderWithCheckoutMethodDetailsUseCase { get; }
		private EnrichOrderWithDeliverypointDetailsUseCase EnrichOrderWithDeliverypointDetailsUseCase { get; }
		private EnrichOrderWithChargeToDeliverypointUseCase EnrichOrderWithChargeToDeliverypointUseCase { get; }
		private EnrichOrderWithDeliveryInformationUseCase EnrichOrderWithDeliveryInformationUseCase { get; }
		private EnrichOrderWithOrderitemsUseCase EnrichOrderWithOrderitemsUseCase { get; }
		private EnrichOrderitemsWithTagsUseCase EnrichOrderitemsWithTagsUseCase { get; }
		private EnrichOrderWithTaxesUseCase EnrichOrderWithTaxesUseCase { get; }
		private EnrichOrderWithOrderStatusUseCase EnrichOrderWithOrderStatusUseCase { get; }
		private EnrichOrderWithClientUseCase EnrichOrderWithClientUseCase { get; }
		private EnrichOrderWithAnalyticsUseCase EnrichOrderWithAnalyticsUseCase { get; }

		public ConvertOrderToEntityUseCaseBuilder() : this(
			new EnrichOrderWithOutletDetailsUseCaseBuilder().Build(),
			new EnrichOrderWithCompanyDetailsUseCaseBuilder().Build(),
			new EnrichOrderWithServiceMethodDetailsUseCaseBuilder().Build(),
			new EnrichOrderWithCheckoutMethodDetailsUseCaseBuilder().Build(),
			new EnrichOrderWithDeliverypointDetailsUseCaseBuilder().Build(),
			new EnrichOrderWithChargeToDeliverypointUseCaseBuilder().Build(),
			new EnrichOrderWithDeliveryInformationUseCaseBuilder().Build(),
			new EnrichOrderWithOrderitemsUseCaseBuilder().Build(),
			new EnrichOrderitemsWithTagsUseCaseBuilder().Build(),
			new EnrichOrderWithTaxesUseCaseBuilder().Build(),
			new EnrichOrderWithOrderStatusUseCaseBuilder().Build(),
			new EnrichOrderWithClientUseCaseBuilder().Build(),
			new EnrichOrderWithAnalyticsUseCaseBuilder().Build())
		{

		}

		private ConvertOrderToEntityUseCaseBuilder(
			EnrichOrderWithOutletDetailsUseCase enrichOrderWithOutletDetailsUseCase,
			EnrichOrderWithCompanyDetailsUseCase enrichOrderWithCompanyDetailsUseCase,
			EnrichOrderWithServiceMethodDetailsUseCase enrichOrderWithServiceMethodDetailsUseCase,
			EnrichOrderWithCheckoutMethodDetailsUseCase enrichOrderWithCheckoutMethodDetailsUseCase,
			EnrichOrderWithDeliverypointDetailsUseCase enrichOrderWithDeliverypointDetailsUseCase,
			EnrichOrderWithChargeToDeliverypointUseCase enrichOrderWithChargeToDeliverypointUseCase,
			EnrichOrderWithDeliveryInformationUseCase enrichOrderWithDeliveryInformationUseCase,
			EnrichOrderWithOrderitemsUseCase enrichOrderWithOrderitemsUseCase,
			EnrichOrderitemsWithTagsUseCase enrichOrderitemsWithTagsUseCase,
			EnrichOrderWithTaxesUseCase enrichOrderWithTaxesUseCase,
			EnrichOrderWithOrderStatusUseCase enrichOrderWithOrderStatusUseCase,
			EnrichOrderWithClientUseCase enrichOrderWithClientUseCase,
			EnrichOrderWithAnalyticsUseCase enrichOrderWithAnalyticsUseCase)
		{
			EnrichOrderWithOutletDetailsUseCase = enrichOrderWithOutletDetailsUseCase;
			EnrichOrderWithCompanyDetailsUseCase = enrichOrderWithCompanyDetailsUseCase;
			EnrichOrderWithServiceMethodDetailsUseCase = enrichOrderWithServiceMethodDetailsUseCase;
			EnrichOrderWithCheckoutMethodDetailsUseCase = enrichOrderWithCheckoutMethodDetailsUseCase;
			EnrichOrderWithDeliverypointDetailsUseCase = enrichOrderWithDeliverypointDetailsUseCase;
			EnrichOrderWithChargeToDeliverypointUseCase = enrichOrderWithChargeToDeliverypointUseCase;
			EnrichOrderWithDeliveryInformationUseCase = enrichOrderWithDeliveryInformationUseCase;
			EnrichOrderWithOrderitemsUseCase = enrichOrderWithOrderitemsUseCase;
			EnrichOrderWithTaxesUseCase = enrichOrderWithTaxesUseCase;
			EnrichOrderWithOrderStatusUseCase = enrichOrderWithOrderStatusUseCase;
			EnrichOrderWithClientUseCase = enrichOrderWithClientUseCase;
			EnrichOrderWithAnalyticsUseCase = enrichOrderWithAnalyticsUseCase;
			EnrichOrderitemsWithTagsUseCase = enrichOrderitemsWithTagsUseCase;


		}

		public ConvertOrderToEntityUseCaseBuilder W(EnrichOrderWithOutletDetailsUseCaseBuilder enrichOrderWithOutletDetailsUseCaseBuilder) => new ConvertOrderToEntityUseCaseBuilder(
			enrichOrderWithOutletDetailsUseCaseBuilder.Build(),
			EnrichOrderWithCompanyDetailsUseCase,
			EnrichOrderWithServiceMethodDetailsUseCase,
			EnrichOrderWithCheckoutMethodDetailsUseCase,
			EnrichOrderWithDeliverypointDetailsUseCase,
			EnrichOrderWithChargeToDeliverypointUseCase,
			EnrichOrderWithDeliveryInformationUseCase,
			EnrichOrderWithOrderitemsUseCase,
			EnrichOrderitemsWithTagsUseCase,
			EnrichOrderWithTaxesUseCase,
			EnrichOrderWithOrderStatusUseCase,
			EnrichOrderWithClientUseCase,
			EnrichOrderWithAnalyticsUseCase
		);

		public ConvertOrderToEntityUseCaseBuilder W(EnrichOrderWithCompanyDetailsUseCaseBuilder enrichOrderWithCompanyDetailsUseCaseBuilder) => new ConvertOrderToEntityUseCaseBuilder(
			EnrichOrderWithOutletDetailsUseCase,
			enrichOrderWithCompanyDetailsUseCaseBuilder.Build(),
			EnrichOrderWithServiceMethodDetailsUseCase,
			EnrichOrderWithCheckoutMethodDetailsUseCase,
			EnrichOrderWithDeliverypointDetailsUseCase,
			EnrichOrderWithChargeToDeliverypointUseCase,
			EnrichOrderWithDeliveryInformationUseCase,
			EnrichOrderWithOrderitemsUseCase,
			EnrichOrderitemsWithTagsUseCase,
			EnrichOrderWithTaxesUseCase,
			EnrichOrderWithOrderStatusUseCase,
			EnrichOrderWithClientUseCase,
			EnrichOrderWithAnalyticsUseCase
		);

		public ConvertOrderToEntityUseCaseBuilder W(EnrichOrderWithServiceMethodDetailsUseCaseBuilder enrichOrderWithServiceMethodDetailsUseCaseBuilder) => new ConvertOrderToEntityUseCaseBuilder(
			EnrichOrderWithOutletDetailsUseCase,
			EnrichOrderWithCompanyDetailsUseCase,
			enrichOrderWithServiceMethodDetailsUseCaseBuilder.Build(),
			EnrichOrderWithCheckoutMethodDetailsUseCase,
			EnrichOrderWithDeliverypointDetailsUseCase,
			EnrichOrderWithChargeToDeliverypointUseCase,
			EnrichOrderWithDeliveryInformationUseCase,
			EnrichOrderWithOrderitemsUseCase,
			EnrichOrderitemsWithTagsUseCase,
			EnrichOrderWithTaxesUseCase,
			EnrichOrderWithOrderStatusUseCase,
			EnrichOrderWithClientUseCase,
			EnrichOrderWithAnalyticsUseCase
			);

		public ConvertOrderToEntityUseCaseBuilder W(EnrichOrderWithCheckoutMethodDetailsUseCaseBuilder enrichOrderWithCheckoutMethodDetailsUseCaseBuilder) => new ConvertOrderToEntityUseCaseBuilder(
			EnrichOrderWithOutletDetailsUseCase,
			EnrichOrderWithCompanyDetailsUseCase,
			EnrichOrderWithServiceMethodDetailsUseCase,
			enrichOrderWithCheckoutMethodDetailsUseCaseBuilder.Build(),
			EnrichOrderWithDeliverypointDetailsUseCase,
			EnrichOrderWithChargeToDeliverypointUseCase,
			EnrichOrderWithDeliveryInformationUseCase,
			EnrichOrderWithOrderitemsUseCase,
			EnrichOrderitemsWithTagsUseCase,
			EnrichOrderWithTaxesUseCase,
			EnrichOrderWithOrderStatusUseCase,
			EnrichOrderWithClientUseCase,
			EnrichOrderWithAnalyticsUseCase
			);

		public ConvertOrderToEntityUseCaseBuilder W(EnrichOrderWithDeliverypointDetailsUseCaseBuilder enrichOrderWithDeliverypointDetailsUseCaseBuilder) => new ConvertOrderToEntityUseCaseBuilder(
			EnrichOrderWithOutletDetailsUseCase,
			EnrichOrderWithCompanyDetailsUseCase,
			EnrichOrderWithServiceMethodDetailsUseCase,
			EnrichOrderWithCheckoutMethodDetailsUseCase,
			enrichOrderWithDeliverypointDetailsUseCaseBuilder.Build(),
			EnrichOrderWithChargeToDeliverypointUseCase,
			EnrichOrderWithDeliveryInformationUseCase,
			EnrichOrderWithOrderitemsUseCase,
			EnrichOrderitemsWithTagsUseCase,
			EnrichOrderWithTaxesUseCase,
			EnrichOrderWithOrderStatusUseCase,
			EnrichOrderWithClientUseCase,
			EnrichOrderWithAnalyticsUseCase
			);

		public ConvertOrderToEntityUseCaseBuilder W(EnrichOrderWithChargeToDeliverypointUseCaseBuilder enrichOrderWithChargeToDeliverypointUseCaseBuilder) => new ConvertOrderToEntityUseCaseBuilder(
			EnrichOrderWithOutletDetailsUseCase,
			EnrichOrderWithCompanyDetailsUseCase,
			EnrichOrderWithServiceMethodDetailsUseCase,
			EnrichOrderWithCheckoutMethodDetailsUseCase,
			EnrichOrderWithDeliverypointDetailsUseCase,
			enrichOrderWithChargeToDeliverypointUseCaseBuilder.Build(),
			EnrichOrderWithDeliveryInformationUseCase,
			EnrichOrderWithOrderitemsUseCase,
			EnrichOrderitemsWithTagsUseCase,
			EnrichOrderWithTaxesUseCase,
			EnrichOrderWithOrderStatusUseCase,
			EnrichOrderWithClientUseCase,
			EnrichOrderWithAnalyticsUseCase
			);

		public ConvertOrderToEntityUseCaseBuilder W(EnrichOrderWithDeliveryInformationUseCaseBuilder enrichOrderWithDeliveryInformationUseCaseBuilder) => new ConvertOrderToEntityUseCaseBuilder(
			EnrichOrderWithOutletDetailsUseCase,
			EnrichOrderWithCompanyDetailsUseCase,
			EnrichOrderWithServiceMethodDetailsUseCase,
			EnrichOrderWithCheckoutMethodDetailsUseCase,
			EnrichOrderWithDeliverypointDetailsUseCase,
			EnrichOrderWithChargeToDeliverypointUseCase,
			enrichOrderWithDeliveryInformationUseCaseBuilder.Build(),
			EnrichOrderWithOrderitemsUseCase,
			EnrichOrderitemsWithTagsUseCase,
			EnrichOrderWithTaxesUseCase,
			EnrichOrderWithOrderStatusUseCase,
			EnrichOrderWithClientUseCase,
			EnrichOrderWithAnalyticsUseCase
		);

		public ConvertOrderToEntityUseCaseBuilder W(EnrichOrderWithOrderitemsUseCaseBuilder enrichOrderWithOrderitemsUseCaseBuilder) => new ConvertOrderToEntityUseCaseBuilder(
			EnrichOrderWithOutletDetailsUseCase,
			EnrichOrderWithCompanyDetailsUseCase,
			EnrichOrderWithServiceMethodDetailsUseCase,
			EnrichOrderWithCheckoutMethodDetailsUseCase,
			EnrichOrderWithDeliverypointDetailsUseCase,
			EnrichOrderWithChargeToDeliverypointUseCase,
			EnrichOrderWithDeliveryInformationUseCase,
			enrichOrderWithOrderitemsUseCaseBuilder.Build(),
			EnrichOrderitemsWithTagsUseCase,
			EnrichOrderWithTaxesUseCase,
			EnrichOrderWithOrderStatusUseCase,
			EnrichOrderWithClientUseCase,
			EnrichOrderWithAnalyticsUseCase
		);

		public ConvertOrderToEntityUseCaseBuilder W(EnrichOrderWithTaxesUseCaseBuilder enrichOrderWithTaxesUseCaseBuilder) => new ConvertOrderToEntityUseCaseBuilder(
			EnrichOrderWithOutletDetailsUseCase,
			EnrichOrderWithCompanyDetailsUseCase,
			EnrichOrderWithServiceMethodDetailsUseCase,
			EnrichOrderWithCheckoutMethodDetailsUseCase,
			EnrichOrderWithDeliverypointDetailsUseCase,
			EnrichOrderWithChargeToDeliverypointUseCase,
			EnrichOrderWithDeliveryInformationUseCase,
			EnrichOrderWithOrderitemsUseCase,
			EnrichOrderitemsWithTagsUseCase,
			enrichOrderWithTaxesUseCaseBuilder.Build(),
			EnrichOrderWithOrderStatusUseCase,
			EnrichOrderWithClientUseCase,
			EnrichOrderWithAnalyticsUseCase
		);

		public ConvertOrderToEntityUseCaseBuilder W(EnrichOrderWithOrderStatusUseCaseBuilder enrichOrderWithOrderStatusUseCaseBuilder) => new ConvertOrderToEntityUseCaseBuilder(
			EnrichOrderWithOutletDetailsUseCase,
			EnrichOrderWithCompanyDetailsUseCase,
			EnrichOrderWithServiceMethodDetailsUseCase,
			EnrichOrderWithCheckoutMethodDetailsUseCase,
			EnrichOrderWithDeliverypointDetailsUseCase,
			EnrichOrderWithChargeToDeliverypointUseCase,
			EnrichOrderWithDeliveryInformationUseCase,
			EnrichOrderWithOrderitemsUseCase,
			EnrichOrderitemsWithTagsUseCase,
			EnrichOrderWithTaxesUseCase,
			enrichOrderWithOrderStatusUseCaseBuilder.Build(),
			EnrichOrderWithClientUseCase,
			EnrichOrderWithAnalyticsUseCase
		);

		public ConvertOrderToEntityUseCaseBuilder W(EnrichOrderWithClientUseCaseBuilder enrichOrderWithClientUseCaseBuilder) => new ConvertOrderToEntityUseCaseBuilder(
			EnrichOrderWithOutletDetailsUseCase,
			EnrichOrderWithCompanyDetailsUseCase,
			EnrichOrderWithServiceMethodDetailsUseCase,
			EnrichOrderWithCheckoutMethodDetailsUseCase,
			EnrichOrderWithDeliverypointDetailsUseCase,
			EnrichOrderWithChargeToDeliverypointUseCase,
			EnrichOrderWithDeliveryInformationUseCase,
			EnrichOrderWithOrderitemsUseCase,
			EnrichOrderitemsWithTagsUseCase,
			EnrichOrderWithTaxesUseCase,
			EnrichOrderWithOrderStatusUseCase,
			enrichOrderWithClientUseCaseBuilder.Build(),
			EnrichOrderWithAnalyticsUseCase
		);

		public ConvertOrderToEntityUseCaseBuilder W(EnrichOrderitemsWithTagsUseCaseBuilder enrichOrderitemsWithTagsUseCaseBuilder) => new ConvertOrderToEntityUseCaseBuilder(
			EnrichOrderWithOutletDetailsUseCase,
			EnrichOrderWithCompanyDetailsUseCase,
			EnrichOrderWithServiceMethodDetailsUseCase,
			EnrichOrderWithCheckoutMethodDetailsUseCase,
			EnrichOrderWithDeliverypointDetailsUseCase,
			EnrichOrderWithChargeToDeliverypointUseCase,
			EnrichOrderWithDeliveryInformationUseCase,
			EnrichOrderWithOrderitemsUseCase,
			enrichOrderitemsWithTagsUseCaseBuilder.Build(),
			EnrichOrderWithTaxesUseCase,
			EnrichOrderWithOrderStatusUseCase,
			EnrichOrderWithClientUseCase,
			EnrichOrderWithAnalyticsUseCase
		);

		public ConvertOrderToEntityUseCase Build() =>
			new ConvertOrderToEntityUseCase(
				EnrichOrderWithOutletDetailsUseCase,
				EnrichOrderWithCompanyDetailsUseCase,
				EnrichOrderWithServiceMethodDetailsUseCase,
				EnrichOrderWithCheckoutMethodDetailsUseCase,
				EnrichOrderWithDeliverypointDetailsUseCase,
				EnrichOrderWithChargeToDeliverypointUseCase,
				EnrichOrderWithDeliveryInformationUseCase,
				EnrichOrderWithOrderitemsUseCase,
				EnrichOrderWithTaxesUseCase,
				EnrichOrderWithOrderStatusUseCase,
				EnrichOrderWithClientUseCase,
				EnrichOrderWithAnalyticsUseCase,
				EnrichOrderitemsWithTagsUseCase);
	}
}
