﻿using Crave.Ordering.Api.Domain.UseCases;

namespace Crave.Ordering.Api.Domain.Tests.Builders.UseCaseBuilders
{
	public class GetDistanceToOutletUseCaseBuilder : IGetDistanceToOutletUseCaseBuilder
	{
		public GetDistanceToOutletUseCaseBuilder() : this(new GetOutletLocationUseCaseBuilder().Build(), new GetDistanceBetweenLocationsUseCaseBuilder().Build())
		{
		}

		private GetDistanceToOutletUseCaseBuilder(GetOutletLocationUseCase getOutletLocationUseCase, GetDistanceBetweenLocationsUseCase getDistanceBetweenLocationsUseCase)
		{
			GetOutletLocationUseCase = getOutletLocationUseCase;
			GetDistanceBetweenLocationsUseCase = getDistanceBetweenLocationsUseCase;
		}

		private GetOutletLocationUseCase GetOutletLocationUseCase { get; }
		private GetDistanceBetweenLocationsUseCase GetDistanceBetweenLocationsUseCase { get; }

		public GetDistanceToOutletUseCase Build() => new GetDistanceToOutletUseCase(GetOutletLocationUseCase, GetDistanceBetweenLocationsUseCase);

		public IGetDistanceToOutletUseCaseBuilder W(IGetOutletLocationUseCaseBuilder getOutletLocationUseCaseBuilder) =>
			new GetDistanceToOutletUseCaseBuilder(getOutletLocationUseCaseBuilder.Build(), GetDistanceBetweenLocationsUseCase);

		public IGetDistanceToOutletUseCaseBuilder W(IGetDistanceBetweenLocationsUseCaseBuilder getDistanceBetweenLocationsUseCaseBuilder) =>
			new GetDistanceToOutletUseCaseBuilder(GetOutletLocationUseCase, getDistanceBetweenLocationsUseCaseBuilder.Build());
	}
}