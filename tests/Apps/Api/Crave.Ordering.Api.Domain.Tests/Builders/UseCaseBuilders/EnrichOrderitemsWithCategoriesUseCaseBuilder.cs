﻿using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Api.Domain.Tests.Builders.RepositoryBuilders;
using Crave.Ordering.Api.Domain.UseCases;

namespace Crave.Ordering.Api.Domain.Tests.Builders.UseCaseBuilders
{
	internal class EnrichOrderitemsWithCategoriesUseCaseBuilder
	{
		private ICategoryRepository CategoryRepository { get; }

		public EnrichOrderitemsWithCategoriesUseCaseBuilder() : this(
            new CategoryRepositoryMockBuilder().Build().Object)
		{

		}

		private EnrichOrderitemsWithCategoriesUseCaseBuilder(ICategoryRepository categoryRepository)
		{
			CategoryRepository = categoryRepository;
        }

		public EnrichOrderitemsWithCategoriesUseCaseBuilder W(CategoryRepositoryMockBuilder categoryRepositoryMockBuilder) =>
			new EnrichOrderitemsWithCategoriesUseCaseBuilder(categoryRepositoryMockBuilder.Build().Object);

		public EnrichOrderitemsWithCategoriesUseCase Build() => new EnrichOrderitemsWithCategoriesUseCase(CategoryRepository);
	}
}