﻿using System.Collections.Generic;
using Crave.Ordering.Api.Domain.Interfaces.UseCases;
using Crave.Ordering.Api.Domain.UseCases.Requests;
using Crave.Ordering.Api.Domain.UseCases.Responses;
using Moq;

namespace Crave.Ordering.Api.Domain.Tests.Builders.UseCaseBuilders
{
    public class GetOrdersForTerminalUseCaseBuilder
    {
        private readonly ResponseBase<IEnumerable<Order>> useCaseResult;

        public GetOrdersForTerminalUseCaseBuilder() : this(ResponseBase<IEnumerable<Order>>.AsSuccess(new List<Order>()))
        {
        }

        private GetOrdersForTerminalUseCaseBuilder(ResponseBase<IEnumerable<Order>> result)
        {
            this.useCaseResult = result;
        }

        public GetOrdersForTerminalUseCaseBuilder WithResponse(ResponseBase<IEnumerable<Order>> response) => new GetOrdersForTerminalUseCaseBuilder(response);

        public Mock<IGetOrdersForTerminalUseCase> Build()
        {
            Mock<IGetOrdersForTerminalUseCase> useCaseMock = new Mock<IGetOrdersForTerminalUseCase>();
            useCaseMock.Setup(uc => uc.ExecuteAsync(It.IsAny<GetOrdersForTerminal>())).ReturnsAsync(this.useCaseResult);

            return useCaseMock;
        }
    }
}
