﻿using Crave.Ordering.Api.Domain.Tests.Builders.RepositoryBuilders;
using Crave.Ordering.Api.Domain.UseCases;

namespace Crave.Ordering.Api.Domain.Tests.Builders.UseCaseBuilders
{
	public interface IGetOutletLocationUseCaseBuilder
	{
		GetOutletLocationUseCase Build();
		IGetOutletLocationUseCaseBuilder W(ICompanyRepositoryBuilder companyRepositoryMockBuilder);
	}
}
