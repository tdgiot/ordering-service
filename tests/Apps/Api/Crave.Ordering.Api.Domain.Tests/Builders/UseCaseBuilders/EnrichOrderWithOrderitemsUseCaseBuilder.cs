﻿using Crave.Ordering.Api.Domain.UseCases;

namespace Crave.Ordering.Api.Domain.Tests.Builders.UseCaseBuilders
{
	internal class EnrichOrderWithOrderitemsUseCaseBuilder
	{
		private EnrichOrderitemsWithProductsUseCase EnrichOrderitemsWithProductsUseCase { get; }
		private EnrichOrderitemsWithCategoriesUseCase EnrichOrderitemsWithCategoriesUseCase { get; }
        private EnrichOrderitemsWithAlterationsUseCase EnrichOrderitemsWithAlterationsUseCase { get; }
		private EnrichOrderitemsWithAlterationitemsUseCase EnrichOrderitemsWithAlterationitemsUseCase { get; }
        private EnrichOrderitemsWithPriceLevelItemsUseCase EnrichOrderitemsWithPriceLevelItemsUseCase { get; }

		public EnrichOrderWithOrderitemsUseCaseBuilder() : this(
            new EnrichOrderitemsWithProductsUseCaseBuilder().Build(),
			new EnrichOrderitemsWithCategoriesUseCaseBuilder().Build(),
			new EnrichOrderitemsWithAlterationsUseCaseBuilder().Build(),
			new EnrichOrderitemsWithAlterationitemsUseCaseBuilder().Build(),
			new EnrichOrderitemsWithPriceLevelItemsUseCaseBuilder().Build())
		{

		}

		private EnrichOrderWithOrderitemsUseCaseBuilder(EnrichOrderitemsWithProductsUseCase enrichOrderitemsWithProductsUseCase, 
														EnrichOrderitemsWithCategoriesUseCase enrichOrderitemsWithCategoriesUseCase,
														EnrichOrderitemsWithAlterationsUseCase enrichOrderitemsWithAlterationsUseCase,
														EnrichOrderitemsWithAlterationitemsUseCase enrichOrderitemsWithAlterationitemsUseCase,
														EnrichOrderitemsWithPriceLevelItemsUseCase enrichOrderitemsWithPriceLevelItemsUseCase)
        {
            EnrichOrderitemsWithProductsUseCase = enrichOrderitemsWithProductsUseCase;
            EnrichOrderitemsWithCategoriesUseCase = enrichOrderitemsWithCategoriesUseCase;
			EnrichOrderitemsWithAlterationsUseCase = enrichOrderitemsWithAlterationsUseCase;
            EnrichOrderitemsWithAlterationitemsUseCase = enrichOrderitemsWithAlterationitemsUseCase;
            EnrichOrderitemsWithPriceLevelItemsUseCase = enrichOrderitemsWithPriceLevelItemsUseCase;
        }

		public EnrichOrderWithOrderitemsUseCaseBuilder W(EnrichOrderitemsWithProductsUseCaseBuilder enrichOrderitemsWithProductsUseCaseBuilder) =>
			new EnrichOrderWithOrderitemsUseCaseBuilder(enrichOrderitemsWithProductsUseCaseBuilder.Build(), EnrichOrderitemsWithCategoriesUseCase, EnrichOrderitemsWithAlterationsUseCase, EnrichOrderitemsWithAlterationitemsUseCase, EnrichOrderitemsWithPriceLevelItemsUseCase);

        public EnrichOrderWithOrderitemsUseCaseBuilder W(EnrichOrderitemsWithCategoriesUseCaseBuilder enrichOrderitemsWithCategoriesUseCaseBuilder) =>
            new EnrichOrderWithOrderitemsUseCaseBuilder(EnrichOrderitemsWithProductsUseCase, enrichOrderitemsWithCategoriesUseCaseBuilder.Build(), EnrichOrderitemsWithAlterationsUseCase, EnrichOrderitemsWithAlterationitemsUseCase, EnrichOrderitemsWithPriceLevelItemsUseCase);

        public EnrichOrderWithOrderitemsUseCaseBuilder W(EnrichOrderitemsWithAlterationsUseCaseBuilder enrichOrderitemsWithAlterationsUseCaseBuilder) =>
            new EnrichOrderWithOrderitemsUseCaseBuilder(EnrichOrderitemsWithProductsUseCase, EnrichOrderitemsWithCategoriesUseCase, enrichOrderitemsWithAlterationsUseCaseBuilder.Build(), EnrichOrderitemsWithAlterationitemsUseCase, EnrichOrderitemsWithPriceLevelItemsUseCase);

        public EnrichOrderWithOrderitemsUseCaseBuilder W(EnrichOrderitemsWithAlterationitemsUseCaseBuilder enrichOrderitemsWithAlterationitemsUseCaseBuilder) =>
            new EnrichOrderWithOrderitemsUseCaseBuilder(EnrichOrderitemsWithProductsUseCase, EnrichOrderitemsWithCategoriesUseCase, EnrichOrderitemsWithAlterationsUseCase, enrichOrderitemsWithAlterationitemsUseCaseBuilder.Build(), EnrichOrderitemsWithPriceLevelItemsUseCase);

        public EnrichOrderWithOrderitemsUseCaseBuilder W(EnrichOrderitemsWithPriceLevelItemsUseCaseBuilder enrichOrderitemsWithPriceLevelItemsUseCaseBuilder) =>
            new EnrichOrderWithOrderitemsUseCaseBuilder(EnrichOrderitemsWithProductsUseCase, EnrichOrderitemsWithCategoriesUseCase, EnrichOrderitemsWithAlterationsUseCase, EnrichOrderitemsWithAlterationitemsUseCase, enrichOrderitemsWithPriceLevelItemsUseCaseBuilder.Build());

		public EnrichOrderWithOrderitemsUseCase Build() => new EnrichOrderWithOrderitemsUseCase(EnrichOrderitemsWithProductsUseCase, 
                                                                                                EnrichOrderitemsWithCategoriesUseCase,
                                                                                                EnrichOrderitemsWithAlterationsUseCase,
                                                                                                EnrichOrderitemsWithAlterationitemsUseCase,
                                                                                                EnrichOrderitemsWithPriceLevelItemsUseCase);
	}
}