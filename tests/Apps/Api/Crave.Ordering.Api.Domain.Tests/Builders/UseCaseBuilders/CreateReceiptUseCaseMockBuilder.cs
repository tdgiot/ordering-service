﻿using Crave.Ordering.Api.Domain.Interfaces.UseCases;
using Moq;

namespace Crave.Ordering.Api.Domain.Tests.Builders.UseCaseBuilders
{
    public class CreateReceiptUseCaseMockBuilder
    {
        public ICreateReceiptUseCase Build()
        {
            Mock<ICreateReceiptUseCase> useCaseMock = new Mock<ICreateReceiptUseCase>();
            return useCaseMock.Object;
        }
    }
}