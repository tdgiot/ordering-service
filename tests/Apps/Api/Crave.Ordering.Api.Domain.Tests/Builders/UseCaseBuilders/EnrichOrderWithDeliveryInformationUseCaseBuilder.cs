﻿using Crave.Ordering.Api.Domain.UseCases;

namespace Crave.Ordering.Api.Domain.Tests.Builders.UseCaseBuilders
{
	internal class EnrichOrderWithDeliveryInformationUseCaseBuilder
	{
		public EnrichOrderWithDeliveryInformationUseCase Build() => new EnrichOrderWithDeliveryInformationUseCase();
	}
}