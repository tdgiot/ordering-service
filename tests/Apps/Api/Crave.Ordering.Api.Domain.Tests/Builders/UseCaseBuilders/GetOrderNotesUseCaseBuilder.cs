﻿using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Api.Domain.Interfaces.UseCases;
using Crave.Ordering.Api.Domain.Tests.Builders.RepositoryBuilders;
using Crave.Ordering.Api.Domain.UseCases;

namespace Crave.Ordering.Api.Domain.Tests.Builders.UseCaseBuilders
{
	internal class GetOrderNotesUseCaseBuilder
	{
		public GetOrderNotesUseCaseBuilder() : this(
			new CompanyRepositoryMockBuilder().Build(),
			new DeliverypointgroupRepositoryMockBuilder().Build(),
			new AlterationRepositoryMockBuilder().Build())
		{
		}

		private GetOrderNotesUseCaseBuilder(
			ICompanyRepository companyRepository, 
			IDeliverypointgroupRepository deliverypointgroupRepository, 
			IAlterationRepository alterationRepository)
		{
			CompanyRepository = companyRepository;
			DeliverypointgroupRepository = deliverypointgroupRepository;
			AlterationRepository = alterationRepository;
		}

		private ICompanyRepository CompanyRepository { get; }

		private IDeliverypointgroupRepository DeliverypointgroupRepository { get; }

		private IAlterationRepository AlterationRepository { get; }

		public IGetOrderNotesUseCase Build() => new GetOrderNotesUseCase(CompanyRepository, DeliverypointgroupRepository, AlterationRepository);

		public GetOrderNotesUseCaseBuilder W(IAlterationRepository alterationRepository) 
			=> new GetOrderNotesUseCaseBuilder(CompanyRepository, DeliverypointgroupRepository, alterationRepository);
	}
}
