﻿using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Api.Domain.Tests.Builders.RepositoryBuilders;
using Crave.Ordering.Api.Domain.UseCases;

namespace Crave.Ordering.Api.Domain.Tests.Builders.UseCaseBuilders
{
	internal class EnrichOrderitemsWithAlterationsUseCaseBuilder
	{
		private IAlterationRepository AlterationRepository { get; }

		public EnrichOrderitemsWithAlterationsUseCaseBuilder() : this(
            new AlterationRepositoryMockBuilder().Build())
		{

		}

		private EnrichOrderitemsWithAlterationsUseCaseBuilder(IAlterationRepository alterationRepository)
		{
			AlterationRepository = alterationRepository;
        }

        public EnrichOrderitemsWithAlterationsUseCaseBuilder W(AlterationRepositoryMockBuilder alterationRepositoryMockBuilder) =>
			new EnrichOrderitemsWithAlterationsUseCaseBuilder(alterationRepositoryMockBuilder.Build());

        public EnrichOrderitemsWithAlterationsUseCase Build() => new EnrichOrderitemsWithAlterationsUseCase(AlterationRepository);
	}
}