﻿using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Api.Domain.UseCases;
using Crave.Ordering.Shared.Messaging.Interfaces.UseCases;
using Moq;

namespace Crave.Ordering.Api.Domain.Tests.Builders.UseCaseBuilders
{
    internal class ExecuteRoutestepUseCaseBuilder
    {
        private readonly UseCaseDependencies dependencies;

        public ExecuteRoutestepUseCaseBuilder() : this(new UseCaseDependencies())
        {

        }

        private ExecuteRoutestepUseCaseBuilder(UseCaseDependencies dependencies)
        {
            this.dependencies = dependencies;
        }

        public ExecuteRoutestepUseCaseBuilder WithOrderRoutestephandlerRepository(IOrderRoutestephandlerRepository repository) => new ExecuteRoutestepUseCaseBuilder(new UseCaseDependencies(this.dependencies)
        {
            OrderRoutestephandlerRepository = repository
        });

        public ExecuteRoutestepUseCaseBuilder WithHandleNonTerminalRoutstephanlerUseCase(HandleNonTerminalRoutingstephandlerUseCase useCase) => new ExecuteRoutestepUseCaseBuilder(new UseCaseDependencies(this.dependencies)
        {
            HandleNonTerminalRoutingstephandlerUseCase = useCase
        });

        public ExecuteRoutestepUseCase Build() => new ExecuteRoutestepUseCase(
            this.dependencies.OrderRoutestephandlerRepository,
            this.dependencies.DeviceRepository,
            this.dependencies.SetForwardedTerminalUseCase,
            this.dependencies.HandleNonTerminalRoutingstephandlerUseCase,
            this.dependencies.SendNetmessageUseCase,
            this.dependencies.UpdateOrderRoutestephandlerUseCase);

        public class UseCaseDependencies
        {
            public UseCaseDependencies()
            {
                this.OrderRoutestephandlerRepository = Mock.Of<IOrderRoutestephandlerRepository>();
                this.DeviceRepository = Mock.Of<IDeviceRepository>();
                this.SetForwardedTerminalUseCase = Mock.Of<SetForwardedTerminalUseCase>();
                this.HandleNonTerminalRoutingstephandlerUseCase = Mock.Of<HandleNonTerminalRoutingstephandlerUseCase>();
                this.SendNetmessageUseCase = Mock.Of<ISendNetmessageUseCase>();
                this.UpdateOrderRoutestephandlerUseCase = Mock.Of<UpdateOrderRoutestephandlerUseCase>();
            }

            public UseCaseDependencies(UseCaseDependencies old)
            {
                OrderRoutestephandlerRepository = old.OrderRoutestephandlerRepository;
                DeviceRepository = old.DeviceRepository;
                SetForwardedTerminalUseCase = old.SetForwardedTerminalUseCase;
                HandleNonTerminalRoutingstephandlerUseCase = old.HandleNonTerminalRoutingstephandlerUseCase;
                SendNetmessageUseCase = old.SendNetmessageUseCase;
                UpdateOrderRoutestephandlerUseCase = old.UpdateOrderRoutestephandlerUseCase;
            }

            public IOrderRoutestephandlerRepository OrderRoutestephandlerRepository { get; set; }
            public IDeviceRepository DeviceRepository { get; set; }
            public SetForwardedTerminalUseCase SetForwardedTerminalUseCase { get; set; }
            public HandleNonTerminalRoutingstephandlerUseCase HandleNonTerminalRoutingstephandlerUseCase { get; set; }
            public ISendNetmessageUseCase SendNetmessageUseCase { get; set; }
            public UpdateOrderRoutestephandlerUseCase UpdateOrderRoutestephandlerUseCase { get; set; }
        }
    }
}