﻿using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Api.Domain.Tests.Builders.RepositoryBuilders;
using Crave.Ordering.Api.Domain.UseCases;

namespace Crave.Ordering.Api.Domain.Tests.Builders.UseCaseBuilders
{
	internal class EnrichOrderWithDeliverypointDetailsUseCaseBuilder
	{
		private IDeliverypointRepository DeliverypointRepository { get; }

		public EnrichOrderWithDeliverypointDetailsUseCaseBuilder() : this(new DeliverypointRepositoryMockBuilder().Build())
		{

		}

		private EnrichOrderWithDeliverypointDetailsUseCaseBuilder(IDeliverypointRepository deliverypointRepository) => DeliverypointRepository = deliverypointRepository;

		public EnrichOrderWithDeliverypointDetailsUseCaseBuilder W(DeliverypointRepositoryMockBuilder deliverypointRepositoryMockBuilder) =>
			new EnrichOrderWithDeliverypointDetailsUseCaseBuilder(deliverypointRepositoryMockBuilder.Build());

		public EnrichOrderWithDeliverypointDetailsUseCase Build() => new EnrichOrderWithDeliverypointDetailsUseCase(DeliverypointRepository);
	}
}