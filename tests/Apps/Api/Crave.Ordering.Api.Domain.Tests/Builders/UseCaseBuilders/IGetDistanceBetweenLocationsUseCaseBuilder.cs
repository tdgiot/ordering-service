﻿using Crave.Ordering.Api.Domain.UseCases;

namespace Crave.Ordering.Api.Domain.Tests.Builders.UseCaseBuilders
{
	public interface IGetDistanceBetweenLocationsUseCaseBuilder
	{
		GetDistanceBetweenLocationsUseCase Build();
		IGetDistanceBetweenLocationsUseCaseBuilder W(IDistanceClientBuilder distanceClientBuilder);
	}
}
