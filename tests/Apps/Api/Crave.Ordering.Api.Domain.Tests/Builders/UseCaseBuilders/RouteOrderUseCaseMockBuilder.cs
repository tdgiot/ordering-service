﻿using Crave.Ordering.Api.Domain.Interfaces.UseCases;
using Crave.Ordering.Api.Domain.UseCases.Requests;
using Moq;

namespace Crave.Ordering.Api.Domain.Tests.Builders.UseCaseBuilders
{
    public class RouteOrderUseCaseMockBuilder
    {
        public RouteOrderUseCaseMockBuilder() : this(true)
        {
            
        }

        private RouteOrderUseCaseMockBuilder(bool result)
        {
            Result = result;

        }

        private bool Result { get; }

        public RouteOrderUseCaseMockBuilder WithResult(bool result) => new RouteOrderUseCaseMockBuilder(result);

        public IRouteOrderUseCase Build()
        {
            Mock<IRouteOrderUseCase> mock = new Mock<IRouteOrderUseCase>();
            mock.Setup(m => m.ExecuteAsync(It.IsAny<RouteOrder>())).ReturnsAsync(Result);
            return mock.Object;
        }
    }
}