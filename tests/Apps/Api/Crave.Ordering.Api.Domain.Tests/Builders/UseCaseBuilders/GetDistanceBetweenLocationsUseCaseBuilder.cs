﻿using Crave.Libraries.Distance;
using Crave.Ordering.Api.Domain.UseCases;

namespace Crave.Ordering.Api.Domain.Tests.Builders.UseCaseBuilders
{
	public class GetDistanceBetweenLocationsUseCaseBuilder : IGetDistanceBetweenLocationsUseCaseBuilder
	{
		public GetDistanceBetweenLocationsUseCaseBuilder() : this(new DistanceClientMockBuilder().Build())
		{
		}

		private GetDistanceBetweenLocationsUseCaseBuilder(IDistanceClient distanceClient) => DistanceClient = distanceClient;

		private IDistanceClient DistanceClient { get; }

		public GetDistanceBetweenLocationsUseCase Build() => new GetDistanceBetweenLocationsUseCase(DistanceClient);

		public IGetDistanceBetweenLocationsUseCaseBuilder W(IDistanceClientBuilder distanceClientBuilder) => new GetDistanceBetweenLocationsUseCaseBuilder(distanceClientBuilder.Build());
	}
}
