﻿using Crave.Ordering.Api.Domain.Interfaces.UseCases;
using Crave.Ordering.Api.Domain.UseCases.Requests;
using Crave.Ordering.Api.Domain.UseCases.Responses;
using Moq;

namespace Crave.Ordering.Api.Domain.Tests.Builders.UseCaseBuilders
{
    public class ValidateOrderUseCaseBuilder
    {
        private readonly ValidateOrderResponse useCaseResponse;

        public ValidateOrderUseCaseBuilder() : this(new ValidateOrderResponseBuilder().Build()) {}

        private ValidateOrderUseCaseBuilder(ValidateOrderResponse response)
        {
            this.useCaseResponse = response;
        }

        public ValidateOrderUseCaseBuilder WithResponse(ValidateOrderResponseBuilder response) => new ValidateOrderUseCaseBuilder(response.Build());

        public Mock<IValidateOrderUseCase> Build()
        {
            Mock<IValidateOrderUseCase> useCaseMock = new Mock<IValidateOrderUseCase>();
            useCaseMock.Setup(uc => uc.ExecuteAsync(It.IsAny<CreateOrderRequest>())).ReturnsAsync(useCaseResponse);

            return useCaseMock;
        }
    }
}