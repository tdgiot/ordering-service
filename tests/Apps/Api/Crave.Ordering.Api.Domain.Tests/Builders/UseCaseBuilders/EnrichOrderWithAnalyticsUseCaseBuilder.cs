﻿using Crave.Ordering.Api.Domain.UseCases;

namespace Crave.Ordering.Api.Domain.Tests.Builders.UseCaseBuilders
{
	internal class EnrichOrderWithAnalyticsUseCaseBuilder
	{
        public EnrichOrderWithAnalyticsUseCase Build() => new EnrichOrderWithAnalyticsUseCase();
    }
}