﻿using Crave.Ordering.Api.Domain.Interfaces.UseCases;
using Crave.Ordering.Api.Domain.UseCases.Responses;
using Moq;

namespace Crave.Ordering.Api.Domain.Tests.Builders.UseCaseBuilders
{
	public class GetOrderUseCaseMockBuilder
	{
		private readonly Order? useCaseResult;

		public GetOrderUseCaseMockBuilder()
		{
		}

		private GetOrderUseCaseMockBuilder(Order? result) => useCaseResult = result;

		public GetOrderUseCaseMockBuilder WithResult(Order? result) => new GetOrderUseCaseMockBuilder(result);

		public IGetOrderUseCase Build()
		{
			Mock<IGetOrderUseCase> useCaseMock = new Mock<IGetOrderUseCase>();
			useCaseMock.Setup(uc => uc.ExecuteAsync(It.IsAny<string>())).ReturnsAsync(useCaseResult);

			return useCaseMock.Object;
		}
	}
}
