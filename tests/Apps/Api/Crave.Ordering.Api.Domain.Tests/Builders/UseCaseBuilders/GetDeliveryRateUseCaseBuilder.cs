﻿using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Api.Domain.Interfaces.UseCases;
using Crave.Ordering.Api.Domain.Tests.Builders.RepositoryBuilders;
using Crave.Ordering.Api.Domain.UseCases;

namespace Crave.Ordering.Api.Domain.Tests.Builders.UseCaseBuilders
{
	public class GetDeliveryRateUseCaseBuilder
	{
		private readonly GetDistanceToOutletUseCase getDistanceToOutletUseCase;
		private readonly IServiceMethodRepository serviceMethodRepository;

		public GetDeliveryRateUseCaseBuilder() : this(new GetDistanceToOutletUseCaseBuilder().Build(), new ServiceMethodRepositoryMockBuilder().Build())
		{
			
		}

		private GetDeliveryRateUseCaseBuilder(GetDistanceToOutletUseCase getDistanceToOutletUseCase, IServiceMethodRepository serviceMethodRepository)
		{
			this.getDistanceToOutletUseCase = getDistanceToOutletUseCase;
			this.serviceMethodRepository = serviceMethodRepository;
		}

		public IGetDeliveryRateUseCase Build()
		{
			IGetDeliveryRateUseCase useCase = new GetDeliveryRateUseCase(getDistanceToOutletUseCase, serviceMethodRepository);
			return useCase;
		}

		public GetDeliveryRateUseCaseBuilder W(IGetDistanceToOutletUseCaseBuilder builder) => W(builder.Build());

		public GetDeliveryRateUseCaseBuilder W(GetDistanceToOutletUseCase value) => new GetDeliveryRateUseCaseBuilder(value, serviceMethodRepository);

		public GetDeliveryRateUseCaseBuilder W(IServiceMethodRepositoryBuilder builder) => W(builder.Build());

		public GetDeliveryRateUseCaseBuilder W(IServiceMethodRepository value) => new GetDeliveryRateUseCaseBuilder(getDistanceToOutletUseCase, value);
	}
}