﻿using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Api.Domain.Tests.Builders.RepositoryBuilders;
using Crave.Ordering.Api.Domain.UseCases;

namespace Crave.Ordering.Api.Domain.Tests.Builders.UseCaseBuilders
{
	internal class EnrichOrderitemsWithPriceLevelItemsUseCaseBuilder
	{
		private IPriceScheduleRepository PriceScheduleRepository { get; }

		public EnrichOrderitemsWithPriceLevelItemsUseCaseBuilder() : this(
            new PriceScheduleRepositoryMockBuilder().Build())
		{

		}

		private EnrichOrderitemsWithPriceLevelItemsUseCaseBuilder(IPriceScheduleRepository priceScheduleRepository)
        {
            PriceScheduleRepository = priceScheduleRepository;
        }

        public EnrichOrderitemsWithPriceLevelItemsUseCaseBuilder W(PriceScheduleRepositoryMockBuilder priceScheduleRepositoryMockBuilder) =>
			new EnrichOrderitemsWithPriceLevelItemsUseCaseBuilder(priceScheduleRepositoryMockBuilder.Build());

        public EnrichOrderitemsWithPriceLevelItemsUseCase Build() => new EnrichOrderitemsWithPriceLevelItemsUseCase(PriceScheduleRepository);
	}
}