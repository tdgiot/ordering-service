﻿using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Api.Domain.Tests.Builders.RepositoryBuilders;
using Crave.Ordering.Api.Domain.UseCases;

namespace Crave.Ordering.Api.Domain.Tests.Builders.UseCaseBuilders
{
	public class GetOutletLocationUseCaseBuilder : IGetOutletLocationUseCaseBuilder
	{
		public GetOutletLocationUseCaseBuilder() : this(new CompanyRepositoryMockBuilder().Build())
		{
		}

		private GetOutletLocationUseCaseBuilder(ICompanyRepository companyRepository) => CompanyRepository = companyRepository;
		private ICompanyRepository CompanyRepository { get; }

		public GetOutletLocationUseCase Build() => new GetOutletLocationUseCase(CompanyRepository);

		public IGetOutletLocationUseCaseBuilder W(ICompanyRepositoryBuilder companyRepositoryMockBuilder) => new GetOutletLocationUseCaseBuilder(companyRepositoryMockBuilder.Build());
	}
}
