﻿using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Api.Domain.Tests.Builders.RepositoryBuilders;
using Crave.Ordering.Api.Domain.UseCases;

namespace Crave.Ordering.Api.Domain.Tests.Builders.UseCaseBuilders
{
	internal class EnrichOrderWithChargeToDeliverypointUseCaseBuilder
	{
		private IDeliverypointRepository DeliverypointRepository { get; }

		public EnrichOrderWithChargeToDeliverypointUseCaseBuilder() : this(new DeliverypointRepositoryMockBuilder().Build())
		{

		}

		private EnrichOrderWithChargeToDeliverypointUseCaseBuilder(IDeliverypointRepository deliverypointRepository) => DeliverypointRepository = deliverypointRepository;

		public EnrichOrderWithChargeToDeliverypointUseCaseBuilder W(DeliverypointRepositoryMockBuilder deliverypointRepositoryMockBuilder) =>
			new EnrichOrderWithChargeToDeliverypointUseCaseBuilder(deliverypointRepositoryMockBuilder.Build());

		public EnrichOrderWithChargeToDeliverypointUseCase Build() => new EnrichOrderWithChargeToDeliverypointUseCase(DeliverypointRepository);
	}
}