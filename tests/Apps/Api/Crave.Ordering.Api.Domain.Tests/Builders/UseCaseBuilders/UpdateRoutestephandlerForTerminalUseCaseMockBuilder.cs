﻿using Crave.Ordering.Api.Domain.Interfaces.UseCases;
using Crave.Ordering.Api.Domain.UseCases.Requests;
using Crave.Ordering.Api.Domain.UseCases.Responses;
using Moq;

namespace Crave.Ordering.Api.Domain.Tests.Builders.UseCaseBuilders
{
    public class UpdateRoutestephandlerForTerminalUseCaseMockBuilder
    {
        private readonly ResponseBase useCaseResponse;

        public UpdateRoutestephandlerForTerminalUseCaseMockBuilder() : this(ResponseBase.AsSuccess())
        {

        }

        private UpdateRoutestephandlerForTerminalUseCaseMockBuilder(ResponseBase response) => useCaseResponse = response;

        public UpdateRoutestephandlerForTerminalUseCaseMockBuilder WithResponse(ResponseBase response) => new UpdateRoutestephandlerForTerminalUseCaseMockBuilder(response);

        public IUpdateOrderRoutestephandlerForTerminalUseCase Build()
        {
            Mock<IUpdateOrderRoutestephandlerForTerminalUseCase> useCaseMock = new Mock<IUpdateOrderRoutestephandlerForTerminalUseCase>();
            useCaseMock.Setup(uc => uc.ExecuteAsync(It.IsAny<UpdateOrderRoutestephandlersForTerminal>())).ReturnsAsync(useCaseResponse);

            return useCaseMock.Object;
        }
    }
}