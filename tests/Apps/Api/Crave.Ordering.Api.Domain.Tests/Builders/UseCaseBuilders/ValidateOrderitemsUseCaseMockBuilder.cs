﻿using System.Collections.Generic;
using Crave.Ordering.Api.Domain.Interfaces.UseCases;
using Crave.Ordering.Api.Domain.UseCases.Requests;
using Crave.Ordering.Api.Domain.UseCases.Responses;
using Crave.Ordering.Api.Domain.Validation.Errors;
using Moq;

namespace Crave.Ordering.Api.Domain.Tests.Builders.UseCaseBuilders
{
    public class ValidateOrderitemsUseCaseMockBuilder
    {
        private readonly ValidationResponse useCaseResponse;

        public ValidateOrderitemsUseCaseMockBuilder() : this(new ValidationResponse(new List<ValidationError>()))
        {

        }
        private ValidateOrderitemsUseCaseMockBuilder(ValidationResponse response) => useCaseResponse = response;

        public ValidateOrderitemsUseCaseMockBuilder WithResponse(ValidationResponse response) => new ValidateOrderitemsUseCaseMockBuilder(response);

        public IValidateOrderitemsUseCase Build()
        {
            Mock<IValidateOrderitemsUseCase> useCaseMock = new Mock<IValidateOrderitemsUseCase>();
            useCaseMock.Setup(uc => uc.Execute(It.IsAny<ValidateOrderitemsRequest>())).Returns(useCaseResponse);
            return useCaseMock.Object;
        }
    }
}