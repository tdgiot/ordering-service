﻿using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Api.Domain.Tests.Builders.RepositoryBuilders;
using Crave.Ordering.Api.Domain.UseCases;

namespace Crave.Ordering.Api.Domain.Tests.Builders.UseCaseBuilders
{
	internal class EnrichOrderWithServiceMethodDetailsUseCaseBuilder
	{
		public EnrichOrderWithServiceMethodDetailsUseCaseBuilder() : this(new ServiceMethodRepositoryMockBuilder().Build())
		{
		}

		private EnrichOrderWithServiceMethodDetailsUseCaseBuilder(IServiceMethodRepository serviceMethodRepository) => ServiceMethodRepository = serviceMethodRepository;
		private IServiceMethodRepository ServiceMethodRepository { get; }

		public EnrichOrderWithServiceMethodDetailsUseCaseBuilder W(IServiceMethodRepositoryBuilder serviceMethodRepositoryBuilder) =>
			new EnrichOrderWithServiceMethodDetailsUseCaseBuilder(serviceMethodRepositoryBuilder.Build());

		public EnrichOrderWithServiceMethodDetailsUseCase Build() => new EnrichOrderWithServiceMethodDetailsUseCase(ServiceMethodRepository);
	}
}
