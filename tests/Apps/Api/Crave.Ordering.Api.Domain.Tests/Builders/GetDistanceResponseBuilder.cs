﻿using Crave.Libraries.Distance.Enums;
using Crave.Libraries.Distance.Models;
using Crave.Libraries.Distance.Responses;

namespace Crave.Ordering.Api.Domain.Tests.Builders
{
	public class GetDistanceResponseBuilder : IGetDistanceResponseBuilder
	{
		private DistanceResult DistanceResult { get; }

		private Distance Distance { get; }

		public GetDistanceResponseBuilder() : this(DistanceResult.OK, new Distance(0))
		{
			
		}

		private GetDistanceResponseBuilder(DistanceResult distanceResult, Distance distance)
		{
			DistanceResult = distanceResult;
			Distance = distance;
		}

		public GetDistanceResponse Build() => new GetDistanceResponse(DistanceResult, Distance);

		public IGetDistanceResponseBuilder W(DistanceResult distanceResult) => new GetDistanceResponseBuilder(distanceResult, Distance);

		public IGetDistanceResponseBuilder W(Distance distance) => new GetDistanceResponseBuilder(DistanceResult, distance);
	}
}