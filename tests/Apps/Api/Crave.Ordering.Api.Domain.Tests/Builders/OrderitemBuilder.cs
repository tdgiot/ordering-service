﻿using System.Collections.Generic;
using AutoFixture;
using Crave.Enums;
using Crave.Ordering.Api.Domain.UseCases.Requests;

namespace Crave.Ordering.Api.Domain.Tests.Builders
{
    internal class OrderitemBuilder
	{
		public OrderitemBuilder() : this(Initialise())
		{
		}

		private OrderitemBuilder(OrderitemData orderitemData) => Data = orderitemData;

		private OrderitemData Data { get; }

        private static OrderitemData Initialise()
        {
            Fixture fixture = new Fixture();
            fixture.Register(() => (int?)null);
            fixture.Register(() => (IEnumerable<AlterationitemRequest>?)null);

            return fixture.Create<OrderitemData>();
        }

        public OrderitemBuilder W(int productId) => new OrderitemBuilder(new OrderitemData(Data)
        {
			ProductId = productId
		});

		public OrderitemBuilder W(OrderitemType orderitemType) => new OrderitemBuilder(new OrderitemData(Data)
        {
            Type = orderitemType
        });

		public OrderitemBuilder W(decimal? price) => new OrderitemBuilder(new OrderitemData(Data)
        {
            Price = price
        });

        public OrderitemBuilder WithPriceLevelItemId(int? priceLevelItemId) => new OrderitemBuilder(new OrderitemData(Data)         
        {
            PriceLevelItemId = priceLevelItemId
        });

        public OrderitemBuilder WithAlterationitems(IEnumerable<AlterationitemRequest> alterationitems) => new OrderitemBuilder(new OrderitemData(Data)
        {
            Alterationitems = alterationitems
        });

        public OrderitemRequest Build() => new OrderitemRequest(Data.Type, Data.Guid, Data.ProductId, Data.CategoryId, Data.Quantity, Data.Price, Data.PriceLevelItemId, Data.Notes, Data.Alterationitems);

		public class OrderitemData
		{
            public OrderitemData()
            {
            }

            public OrderitemData(OrderitemData data)
            {
				this.Type = data.Type;
                this.ProductId = data.ProductId;
                this.CategoryId = data.CategoryId;
                this.Quantity = data.Quantity;
                this.Price = data.Price;
				this.PriceLevelItemId = data.PriceLevelItemId;
                this.Alterationitems = data.Alterationitems;
            }

			public OrderitemType Type { get; set; }
            public string? Guid { get; set; }
			public int ProductId { get; set; }
			public int? CategoryId { get; set; }
			public int Quantity { get; set; }
			public decimal? Price { get; set; }
			public int? PriceLevelItemId { get; set; }
            public string? Notes { get; set; }
			public IEnumerable<AlterationitemRequest>? Alterationitems { get; set; } = new AlterationitemRequest[0];
		}
	}
}