﻿using System.ComponentModel.DataAnnotations;
using AutoFixture;
using Crave.Ordering.Api.Domain.UseCases.Requests;

namespace Crave.Ordering.Api.Domain.Tests.Builders
{
	internal class CustomerBuilder
	{
		public CustomerBuilder() : this(new Fixture().Create<CustomerData>())
		{
		}

		private CustomerBuilder(CustomerData customerData) => Data = customerData;

		private CustomerData Data { get; }

		public CustomerRequest Build() => new CustomerRequest(Data.Email, Data.FirstName, Data.LastName, Data.LastNamePrefix, Data.PhoneNumber);

		public class CustomerData
		{
            [StringLength(40)] public string FirstName { get; set; }
			[StringLength(40)] public string LastName { get; set; }
            [StringLength(40)] public string LastNamePrefix { get; set; }
			[StringLength(40)] public string PhoneNumber { get; set; }
			[StringLength(40)] public string Email { get; set; }
		}
	}
}