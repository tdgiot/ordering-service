﻿using Crave.Libraries.Distance;
using Crave.Libraries.Distance.Interfaces;
using Crave.Libraries.Distance.Responses;
using Moq;

namespace Crave.Ordering.Api.Domain.Tests.Builders
{
	public class DistanceClientMockBuilder : IDistanceClientBuilder
	{
		private GetDistanceResponse GetDistanceResponse { get; }

		public DistanceClientMockBuilder() : this(new GetDistanceResponseBuilder().Build())
		{
			
		}

		private DistanceClientMockBuilder(GetDistanceResponse getDistanceResponse) => GetDistanceResponse = getDistanceResponse;

		public IDistanceClient Build()
		{
			var distanceClientMock = new Mock<IDistanceClient>();
			_ = distanceClientMock.Setup(client => client.GetDistanceAsync(It.IsAny<ILocation>(), It.IsAny<ILocation>())).ReturnsAsync(GetDistanceResponse);
			return distanceClientMock.Object;
		}

		public IDistanceClientBuilder W(IGetDistanceResponseBuilder getDistanceResponseBuilder) => new DistanceClientMockBuilder(getDistanceResponseBuilder.Build());
	}
}
