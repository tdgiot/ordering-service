﻿using Crave.Ordering.Api.Domain.Converters;
using Microsoft.Extensions.Logging;

namespace Crave.Ordering.Api.Domain.Tests.Builders.Converters
{
	internal class AlterationitemConverterBuilder
	{
		private readonly ILogger<AlterationitemConverter> logger;


		public AlterationitemConverterBuilder() : this(new LoggerMockBuilder<AlterationitemConverter>().Build())
		{
			
		}

		private AlterationitemConverterBuilder(ILogger<AlterationitemConverter> logger) => this.logger = logger;

		public AlterationitemConverter Build() => new AlterationitemConverter(logger);
	}
}
