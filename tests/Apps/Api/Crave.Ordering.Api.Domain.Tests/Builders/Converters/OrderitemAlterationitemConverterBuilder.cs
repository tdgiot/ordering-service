﻿using Crave.Ordering.Api.Domain.Converters;
using Microsoft.Extensions.Logging;

namespace Crave.Ordering.Api.Domain.Tests.Builders.Converters
{
	internal class OrderitemAlterationitemConverterBuilder
	{
		private readonly ILogger<OrderitemAlterationitemConverter> logger;
		private readonly AlterationitemConverter alterationitemConverter;


		public OrderitemAlterationitemConverterBuilder() : this(new LoggerMockBuilder<OrderitemAlterationitemConverter>().Build(), new AlterationitemConverterBuilder().Build())
		{
			
		}

		private OrderitemAlterationitemConverterBuilder(ILogger<OrderitemAlterationitemConverter> logger, AlterationitemConverter alterationitemConverter)
		{
			this.logger = logger;
			this.alterationitemConverter = alterationitemConverter;
		}

		public OrderitemAlterationitemConverter Build() => new OrderitemAlterationitemConverter(logger, alterationitemConverter);
	}
}
