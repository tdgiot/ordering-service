﻿using Crave.Ordering.Api.Domain.Converters;
using Microsoft.Extensions.Logging;

namespace Crave.Ordering.Api.Domain.Tests.Builders.Converters
{
	public class OrderConverterBuilder
	{
		private readonly ILogger<OrderConverter> logger;
		private readonly DeliveryInformationConverter deliveryInformationConverter;
		private readonly ReceiptConverter receiptConverter;
		private readonly OrderitemConverter orderitemConverter;
		private readonly OrderRoutestephandlerConverter orderRoutestephandlerConverter;

		public OrderConverterBuilder() : this(
			new LoggerMockBuilder<OrderConverter>().Build(),
			new DeliveryInformationConverterBuilder().Build(),
			new ReceiptConverterBuilder().Build(),
			new OrderitemConverterBuilder().Build(),
			new OrderRoutestephandlerConverterBuilder().Build()
			)
		{
			
		}

		private OrderConverterBuilder(ILogger<OrderConverter> logger, DeliveryInformationConverter deliveryInformationConverter, ReceiptConverter receiptConverter, OrderitemConverter orderitemConverter, OrderRoutestephandlerConverter orderRoutestephandlerConverter)
		{
			this.logger = logger;
			this.deliveryInformationConverter = deliveryInformationConverter;
			this.receiptConverter = receiptConverter;
			this.orderitemConverter = orderitemConverter;
			this.orderRoutestephandlerConverter = orderRoutestephandlerConverter;
		}

		public OrderConverter Build() => new OrderConverter(logger, deliveryInformationConverter, receiptConverter, orderitemConverter, orderRoutestephandlerConverter);
	}
}
