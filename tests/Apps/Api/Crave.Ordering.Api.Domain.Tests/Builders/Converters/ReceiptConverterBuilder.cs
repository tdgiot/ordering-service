﻿using Crave.Ordering.Api.Domain.Converters;
using Microsoft.Extensions.Logging;

namespace Crave.Ordering.Api.Domain.Tests.Builders.Converters
{
	internal class ReceiptConverterBuilder
	{
		private readonly ILogger<ReceiptConverter> logger;

		public ReceiptConverterBuilder() : this(new LoggerMockBuilder<ReceiptConverter>().Build())
		{
			
		}

		private ReceiptConverterBuilder(ILogger<ReceiptConverter> logger) => this.logger = logger;

		public ReceiptConverter Build() => new ReceiptConverter(logger);
	}
}
