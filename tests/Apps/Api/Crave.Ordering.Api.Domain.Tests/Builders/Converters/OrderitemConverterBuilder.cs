﻿using Crave.Ordering.Api.Domain.Converters;
using Microsoft.Extensions.Logging;

namespace Crave.Ordering.Api.Domain.Tests.Builders.Converters
{
	internal class OrderitemConverterBuilder
	{
		private readonly ILogger<OrderitemConverter> logger;
		private readonly OrderitemAlterationitemConverter orderitemAlterationitemConverter;


		public OrderitemConverterBuilder() : this(new LoggerMockBuilder<OrderitemConverter>().Build(), new OrderitemAlterationitemConverterBuilder().Build())
		{
			
		}

		private OrderitemConverterBuilder(ILogger<OrderitemConverter> logger, OrderitemAlterationitemConverter orderitemAlterationitemConverter)
		{
			this.logger = logger;
			this.orderitemAlterationitemConverter = orderitemAlterationitemConverter;
		}

		public OrderitemConverter Build() => new OrderitemConverter(logger, orderitemAlterationitemConverter);
	}
}
