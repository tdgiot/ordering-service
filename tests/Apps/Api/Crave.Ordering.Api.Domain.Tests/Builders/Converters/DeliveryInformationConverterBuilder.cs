﻿using Crave.Ordering.Api.Domain.Converters;
using Microsoft.Extensions.Logging;

namespace Crave.Ordering.Api.Domain.Tests.Builders.Converters
{
	internal class DeliveryInformationConverterBuilder
	{
		private readonly ILogger<DeliveryInformationConverter> logger;

		public DeliveryInformationConverterBuilder() : this(new LoggerMockBuilder<DeliveryInformationConverter>().Build())
		{
			
		}

		private DeliveryInformationConverterBuilder(ILogger<DeliveryInformationConverter> logger) => this.logger = logger;

		public DeliveryInformationConverter Build() => new DeliveryInformationConverter(logger);
	}
}
