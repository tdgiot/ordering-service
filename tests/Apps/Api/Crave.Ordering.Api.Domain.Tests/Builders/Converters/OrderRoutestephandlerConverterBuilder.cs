﻿using Crave.Ordering.Api.Domain.Converters;
using Microsoft.Extensions.Logging;

namespace Crave.Ordering.Api.Domain.Tests.Builders.Converters
{
	internal class OrderRoutestephandlerConverterBuilder
	{
		private readonly ILogger<OrderRoutestephandlerConverter> logger;


		public OrderRoutestephandlerConverterBuilder() : this(new LoggerMockBuilder<OrderRoutestephandlerConverter>().Build())
		{
			
		}

		private OrderRoutestephandlerConverterBuilder(ILogger<OrderRoutestephandlerConverter> logger) => this.logger = logger;

		public OrderRoutestephandlerConverter Build() => new OrderRoutestephandlerConverter(logger);
	}
}
