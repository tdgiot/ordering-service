﻿namespace Crave.Ordering.Api.Domain.Tests.Builders
{
	internal class TimeZoneOlsonIdBuilder
	{
		public enum Region
		{
			UTC,
			Europe,
			US,
			Azia
		}

		public TimeZoneOlsonIdBuilder() : this(Region.UTC)
		{

		}

		private TimeZoneOlsonIdBuilder(Region region) => Id = region switch
		{
			Region.Azia => "Asia/Bangkok",
			Region.US => "US/Pacific",
			Region.Europe => "Europe/London",
			_ => "UTC",
		};

		private string Id { get; }

		public string Build() => Id;

		public TimeZoneOlsonIdBuilder W(Region region) => new TimeZoneOlsonIdBuilder(region);
	}
}
