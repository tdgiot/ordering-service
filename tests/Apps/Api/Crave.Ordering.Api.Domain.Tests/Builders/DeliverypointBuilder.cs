﻿using Crave.Ordering.Api.Domain.UseCases.Requests;

namespace Crave.Ordering.Api.Domain.Tests.Builders
{
	internal class DeliverypointBuilder
	{
		private int? DeliverypointgroupId { get; }
		private string DeliverypointNumber { get; }

		public DeliverypointBuilder() : this(null, string.Empty) { }

		private DeliverypointBuilder(int? deliverypointgroupId, string deliverypointNumber)
		{
			DeliverypointgroupId = deliverypointgroupId;
			DeliverypointNumber = deliverypointNumber;
		}

		public DeliverypointRequest Build() => new DeliverypointRequest(DeliverypointNumber, DeliverypointgroupId);
	}
}