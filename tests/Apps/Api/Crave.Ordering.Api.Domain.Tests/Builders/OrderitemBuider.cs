﻿using System.Linq;
using Crave.Enums;
using Crave.Ordering.Api.Domain.UseCases.Requests;

namespace Crave.Ordering.Api.Domain.Tests.Builders
{
    internal class OrderitemBuider
	{
		private OrderitemType OrderitemType { get; }
        private string? Guid { get; }
        private int? CategoryId { get; }
        private int ProductId { get; }
		private int Quantity { get; }
		private decimal? Price { get; }
		private int? PriceLevelItemId { get; }
		private string? Notes { get; }

		public OrderitemBuider() : this(OrderitemType.Product, 1, 1)
		{

		}

		private OrderitemBuider(OrderitemType orderitemType, int productId, int quantity)
		{
			OrderitemType = orderitemType;
			ProductId = productId;
			Quantity = quantity;
		}

		public OrderitemRequest Build() => new OrderitemRequest(OrderitemType, Guid, ProductId, CategoryId, Quantity, Price, PriceLevelItemId, Notes, Enumerable.Empty<AlterationitemRequest>());

		public OrderitemBuider W(OrderitemType orderitemType) => new OrderitemBuider(orderitemType, ProductId, Quantity);

		public OrderitemBuider W(int productId, int quantity) => new OrderitemBuider(OrderitemType, productId, quantity);
	}
}
