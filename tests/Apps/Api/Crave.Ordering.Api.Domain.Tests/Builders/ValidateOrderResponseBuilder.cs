﻿using System.Collections.Generic;
using Crave.Ordering.Api.Domain.UseCases.Responses;
using Crave.Ordering.Api.Domain.Validation.Errors;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.Tests.Builders;

namespace Crave.Ordering.Api.Domain.Tests.Builders
{
    public class ValidateOrderResponseBuilder
    {
        private OrderEntity orderEntity;
        private ValidationResponse validationResponse;

        public ValidateOrderResponseBuilder() : this(new OrderEntityBuilder().Build(), new ValidationResponse(new List<ValidationError>()))
        {
        }

        private ValidateOrderResponseBuilder(OrderEntity orderEntity, ValidationResponse validationResponse)
        {
            this.orderEntity = orderEntity;
            this.validationResponse = validationResponse;
        }

        public ValidateOrderResponseBuilder WithValidationResponse(ValidationResponseBuilder response) => new ValidateOrderResponseBuilder(this.orderEntity, response.Build());

        public ValidateOrderResponseBuilder WithOrderEntity(OrderEntityBuilder entity) => new ValidateOrderResponseBuilder(entity.Build(), this.validationResponse);

        public ValidateOrderResponse Build()
        {
            return new ValidateOrderResponse(this.orderEntity, this.validationResponse);
        }
    }
}