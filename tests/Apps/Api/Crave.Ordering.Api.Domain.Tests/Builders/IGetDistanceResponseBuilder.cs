﻿using Crave.Libraries.Distance.Enums;
using Crave.Libraries.Distance.Models;
using Crave.Libraries.Distance.Responses;

namespace Crave.Ordering.Api.Domain.Tests.Builders
{
	public interface IGetDistanceResponseBuilder
	{
		GetDistanceResponse Build();
		IGetDistanceResponseBuilder W(DistanceResult distanceResult);
		IGetDistanceResponseBuilder W(Distance distance);
	}
}
