﻿using Microsoft.Extensions.Logging;
using Moq;

namespace Crave.Ordering.Api.Domain.Tests.Builders
{
    public class LoggerMockBuilder<T>
    {
        public LoggerMockBuilder()
        {

        }

        public ILogger<T> Build()
        {
            Mock<ILogger<T>> mock = new Mock<ILogger<T>>();
            return mock.Object;
        }
    }
}
