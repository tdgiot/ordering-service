﻿using System;
using System.ComponentModel.DataAnnotations;
using AutoFixture;
using Crave.Ordering.Api.Domain.UseCases.Requests;

namespace Crave.Ordering.Api.Domain.Tests.Builders
{
    internal class AlterationitemBuilder
	{
		public AlterationitemBuilder():this(new Fixture().Create<AlterationitemData>())
		{
			
		}

		private AlterationitemBuilder(AlterationitemData alterationitemData) => Data = alterationitemData;

		private AlterationitemData Data { get; }

        public AlterationitemBuilder WithAlterationId(int alterationId) => new AlterationitemBuilder(new AlterationitemData(Data)
        {
            AlterationId = alterationId
        });

        public AlterationitemBuilder WithAlterationoptionId(int alterationoptionId) => new AlterationitemBuilder(new AlterationitemData(Data)
        {
            AlterationoptionId = alterationoptionId
		});

		public AlterationitemBuilder WithPrice(decimal price) => new AlterationitemBuilder(new AlterationitemData(Data)
        {
            Price = price
        });

		public AlterationitemBuilder WithPriceLevelItemId(int? priceLevelItemId) => new AlterationitemBuilder(new AlterationitemData(Data)
        {
			PriceLevelItemId = priceLevelItemId
        });

		public AlterationitemRequest Build() => new AlterationitemRequest(Data.AlterationId, Data.AlterationoptionId, Data.Price, Data.Value, Data.TimeUTC, Data.PriceLevelItemId);

		public class AlterationitemData
		{
            public AlterationitemData()
            {
            }

			public AlterationitemData(AlterationitemData data)
            {
                this.AlterationId = data.AlterationId;
                this.Price = data.Price;
				this.AlterationoptionId = data.AlterationoptionId;
				this.TimeUTC = data.TimeUTC;
                this.Value = data.Value;
				this.PriceLevelItemId = data.PriceLevelItemId;
            }

			public int AlterationId { get; set; }
			public decimal Price { get; set; }
			public int AlterationoptionId { get; set; }
			public DateTime TimeUTC { get; set; }
			[StringLength(40)] public string Value { get; set; }
			public int? PriceLevelItemId { get; set; }
		}
	}
}