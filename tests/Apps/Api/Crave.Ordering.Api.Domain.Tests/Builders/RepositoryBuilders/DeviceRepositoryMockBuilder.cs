﻿using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Api.Domain.Tests.Builders.EntityBuilders;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Moq;

namespace Crave.Ordering.Api.Domain.Tests.Builders.RepositoryBuilders
{
	internal class DeviceRepositoryMockBuilder
	{
        public DeviceRepositoryMockBuilder() : this(new DeviceEntityBuilder().Build())
        {
        }

		private DeviceRepositoryMockBuilder(DeviceEntity deviceEntity) => DeviceEntity = deviceEntity;

        private DeviceEntity DeviceEntity { get; }

        public DeviceRepositoryMockBuilder WithDevice(DeviceEntity deviceEntity) => new DeviceRepositoryMockBuilder(deviceEntity);

		public IDeviceRepository Build()
		{
			Mock<IDeviceRepository> repositoryMock = new Mock<IDeviceRepository>();
			repositoryMock.Setup(repository => repository.GetDevice(It.IsAny<int>())).Returns(DeviceEntity);
            repositoryMock.Setup(repository => repository.GetDeviceForClient(It.IsAny<int>())).Returns(DeviceEntity);
			return repositoryMock.Object;
		}
	}
}