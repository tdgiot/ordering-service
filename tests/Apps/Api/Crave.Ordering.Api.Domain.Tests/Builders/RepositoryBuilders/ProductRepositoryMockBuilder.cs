﻿using System.Collections.Generic;
using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.Tests.Builders;
using Moq;

namespace Crave.Ordering.Api.Domain.Tests.Builders.RepositoryBuilders
{
    internal class ProductRepositoryMockBuilder
	{
		public ProductRepositoryMockBuilder() : this(new ProductEntityBuilder().Build())
		{
		}

		private ProductRepositoryMockBuilder(ProductEntity productEntity) => ProductEntity = productEntity;
		private ProductEntity ProductEntity { get; }

		public ProductRepositoryMockBuilder W(ProductEntityBuilder productEntityBuilder) => W(productEntityBuilder.Build());

		public ProductRepositoryMockBuilder W(ProductEntity productEntity) => new ProductRepositoryMockBuilder(productEntity);

		public Mock<IProductRepository> Build()
		{
			var repositoryMock = new Mock<IProductRepository>();
			_ = repositoryMock.Setup(repository => repository.GetProduct(It.IsAny<int>())).Returns(ProductEntity);
            _ = repositoryMock.Setup(repository => repository.GetProducts(It.IsAny<IReadOnlyList<int>>())).Returns((IReadOnlyList<int> productIds) => new EntityCollection<ProductEntity>
            {
				ProductEntity
            });
            _ = repositoryMock.Setup(repository => repository.GetProductsWithAlterationsPrefetch(It.IsAny<IReadOnlyList<int>>())).Returns((IReadOnlyList<int> productIds) => new EntityCollection<ProductEntity>
            {
                ProductEntity
            });
            return repositoryMock;
		}
	}
}