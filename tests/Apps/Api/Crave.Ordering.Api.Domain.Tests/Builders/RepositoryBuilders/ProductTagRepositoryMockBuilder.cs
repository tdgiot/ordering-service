﻿using System.Collections.Generic;
using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Api.Domain.Tests.Builders.EntityBuilders;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.Tests.Builders;
using Moq;

namespace Crave.Ordering.Api.Domain.Tests.Builders.RepositoryBuilders
{
    internal class ProductTagRepositoryMockBuilder
    {
        public ProductTagRepositoryMockBuilder() : this(new ProductTagEntityBuilder().Build())
        {
        }

        private ProductTagRepositoryMockBuilder(ProductTagEntity productTagEntity) => ProductTagEntity = productTagEntity;
        private ProductTagEntity ProductTagEntity { get; }

        public ProductTagRepositoryMockBuilder W(ProductTagEntityBuilder productTagEntityBuilder) => W(productTagEntityBuilder.Build());

        public ProductTagRepositoryMockBuilder W(ProductTagEntity productTagEntity) => new ProductTagRepositoryMockBuilder(productTagEntity);

        public IProductTagRepository Build()
        {
            var repositoryMock = new Mock<IProductTagRepository>();
            _ = repositoryMock.Setup(repository => repository.GetProductTags(It.IsAny<IReadOnlyList<int>>())).Returns((IReadOnlyList<int> productTagIds) => new EntityCollection<ProductTagEntity>
            {
                ProductTagEntity
            });
            return repositoryMock.Object;
        }
    }
}
