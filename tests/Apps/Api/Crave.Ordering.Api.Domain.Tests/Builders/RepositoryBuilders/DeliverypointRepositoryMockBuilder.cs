﻿using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Api.Domain.Tests.Builders.EntityBuilders;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.Tests.Builders;
using Moq;

namespace Crave.Ordering.Api.Domain.Tests.Builders.RepositoryBuilders
{
	public class DeliverypointRepositoryMockBuilder
	{
		private (DeliverypointEntity deliverypointEntity, DeliverypointEntity serviceMethodDeliverypointEntity, DeliverypointEntity checkoutMethodDeliverypointEntity) Deliverypoints { get; }

		public DeliverypointRepositoryMockBuilder() : this((new DeliverypointEntityBuilder().Build(), new DeliverypointEntityBuilder().Build(), new DeliverypointEntityBuilder().Build())) { }

		private DeliverypointRepositoryMockBuilder((DeliverypointEntity deliverypointEntity, DeliverypointEntity serviceMethodDeliverypointEntity, DeliverypointEntity checkoutMethodDeliverypointEntity) deliverypoints) => Deliverypoints = deliverypoints;

		public DeliverypointRepositoryMockBuilder W((DeliverypointEntity deliverypointEntity, DeliverypointEntity serviceMethodDeliverypointEntity, DeliverypointEntity checkoutMethodDeliverypointEntity) deliverypoints) => new DeliverypointRepositoryMockBuilder(deliverypoints);

		public IDeliverypointRepository Build()
		{
			var repositoryMock = new Mock<IDeliverypointRepository>();
			_ = repositoryMock.Setup(repository => repository.GetDeliverypoint(It.IsAny<int>())).Returns(Deliverypoints.deliverypointEntity);
			_ = repositoryMock.Setup(repository => repository.GetDeliverypointForServiceMethod(It.IsAny<string>(), It.IsAny<int?>(), It.IsAny<int>())).Returns(Deliverypoints.serviceMethodDeliverypointEntity);
			_ = repositoryMock.Setup(repository => repository.GetDeliverypointForCheckoutMethod(It.IsAny<string>(), It.IsAny<int?>(), It.IsAny<int>())).Returns(Deliverypoints.checkoutMethodDeliverypointEntity);
			return repositoryMock.Object;
		}
	}
}