﻿using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.Tests.Builders;
using Moq;

namespace Crave.Ordering.Api.Domain.Tests.Builders.RepositoryBuilders
{
	public class ServiceMethodRepositoryMockBuilder : IServiceMethodRepositoryBuilder
	{
		private ServiceMethodEntity ServiceMethodEntity { get; }

		public ServiceMethodRepositoryMockBuilder() : this(new ServiceMethodEntityBuilder().Build()) { }

		private ServiceMethodRepositoryMockBuilder(ServiceMethodEntity serviceMethodEntity) => ServiceMethodEntity = serviceMethodEntity;

		public ServiceMethodRepositoryMockBuilder W(ServiceMethodEntity serviceMethodEntity) => new ServiceMethodRepositoryMockBuilder(serviceMethodEntity);

		public ServiceMethodRepositoryMockBuilder W(ServiceMethodEntityBuilder serviceMethodEntityBuilder) => W(serviceMethodEntityBuilder.Build());

		public IServiceMethodRepository Build()
		{
			var repositoryMock = new Mock<IServiceMethodRepository>();
			_ = repositoryMock.Setup(repository => repository.GetServiceMethod(It.IsAny<int>())).Returns(ServiceMethodEntity);
			_ = repositoryMock.Setup(repository => repository.GetServiceMethodWithDeliveryDistancesAsync(It.IsAny<int>())).ReturnsAsync(ServiceMethodEntity);
			return repositoryMock.Object;
		}
	}
}
