﻿using System.Collections.Generic;
using System.Linq;
using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Api.Domain.Tests.Builders.EntityBuilders;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.Tests.Builders;
using Moq;

namespace Crave.Ordering.Api.Domain.Tests.Builders.RepositoryBuilders
{
	internal class PriceScheduleRepositoryMockBuilder
	{
		public PriceScheduleRepositoryMockBuilder() : this(new PriceLevelItemEntityBuilder().Build())
		{
		}

		private PriceScheduleRepositoryMockBuilder(PriceLevelItemEntity priceLevelItemEntity) => PriceLevelItemEntity = priceLevelItemEntity;
		private PriceLevelItemEntity PriceLevelItemEntity { get; }

		public PriceScheduleRepositoryMockBuilder W(PriceLevelItemEntity priceLevelItemEntity) => new PriceScheduleRepositoryMockBuilder(priceLevelItemEntity);

		public IPriceScheduleRepository Build()
		{
			Mock<IPriceScheduleRepository> repositoryMock = new Mock<IPriceScheduleRepository>();
            repositoryMock.Setup(repository => repository.GetPriceLevelItems(It.IsAny<IReadOnlyList<int>>())).Returns((IReadOnlyList<int> priceLevelItemIds) => new EntityCollection<PriceLevelItemEntity>
            {
				PriceLevelItemEntity
			});
			return repositoryMock.Object;
		}
	}
}