﻿using AutoFixture;
using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Moq;

namespace Crave.Ordering.Api.Domain.Tests.Builders.RepositoryBuilders
{
	internal class MenuRepositoryMockBuilder
	{
		private string MenuName { get; }

		public MenuRepositoryMockBuilder() : this(new Fixture().Create<string>())
		{
		}

		private MenuRepositoryMockBuilder(string menuName) => MenuName = menuName;

		public MenuRepositoryMockBuilder W(string menuName) => new MenuRepositoryMockBuilder(menuName);

		public IMenuRepository Build()
		{
			var repositoryMock = new Mock<IMenuRepository>();
			_ = repositoryMock.Setup(repository => repository.GetMenuName(It.IsAny<int>())).Returns(MenuName);
			return repositoryMock.Object;
		}
	}
}