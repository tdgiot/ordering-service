﻿using System;
using System.Collections.Generic;
using System.Linq;
using Crave.Enums;
using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.Tests.Builders;
using Moq;

namespace Crave.Ordering.Api.Domain.Tests.Builders.RepositoryBuilders
{
    public class OrderRepositoryMockBuilder
    {
        private readonly OrderEntity[] orderEntities;

        public OrderRepositoryMockBuilder() : this(Array.Empty<OrderEntity>())
        {
        }

        private OrderRepositoryMockBuilder(OrderEntity orderEntity) : this(new[] { orderEntity })
        {
        }

        private OrderRepositoryMockBuilder(OrderEntity[] orderEntities) => this.orderEntities = orderEntities;

        public OrderRepositoryMockBuilder W(OrderEntityBuilder orderEntityBuilder) => W(orderEntityBuilder.Build());

        public OrderRepositoryMockBuilder W(OrderEntity entity) => new OrderRepositoryMockBuilder(entity);

        public OrderRepositoryMockBuilder W(OrderEntity[] entities) => new OrderRepositoryMockBuilder(entities);

        public IOrderRepository Build()
        {
            Mock<IOrderRepository> repositoryMock = new Mock<IOrderRepository>();
            OrderEntity? singleOrderEntity = orderEntities.Any() ? orderEntities[0] : null;

            _ = repositoryMock.Setup(repository => repository.GetOrderForNotification(It.IsAny<int>())).Returns(singleOrderEntity ?? new OrderEntity());
            _ = repositoryMock.Setup(repository => repository.GetByIdWithPrefetchAsync(It.IsAny<int>())).ReturnsAsync(singleOrderEntity);
            _ = repositoryMock.Setup(repository => repository.GetByIdAsync(It.IsAny<int>())).ReturnsAsync(singleOrderEntity);
            _ = repositoryMock.Setup(repository => repository.GetByGuidAsync(It.IsAny<string>())).ReturnsAsync(singleOrderEntity);
            _ = repositoryMock.Setup(repository => repository.GetByGuidWithPrefetchAsync(It.IsAny<string>())).ReturnsAsync(singleOrderEntity);

            _ = repositoryMock.Setup(repository => repository.GetPendingOrders(It.IsAny<int>(), It.IsAny<IEnumerable<int>>(), It.IsAny<IEnumerable<OrderRoutestephandlerStatus>>()))
                .Returns(new EntityCollection<OrderEntity>(orderEntities));

            _ = repositoryMock.Setup(repository => repository.GetOrders(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>()))
                .Returns(new EntityCollection<OrderEntity>(orderEntities));

            _ = repositoryMock.Setup(repository => repository.GetOrdersHistory(It.IsAny<int>()))
                .Returns(new EntityCollection<OrderEntity>(orderEntities));

            return repositoryMock.Object;
        }
    }
}
