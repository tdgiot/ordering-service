﻿using System.Collections.Generic;
using System.Linq;
using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Api.Domain.Tests.Builders.EntityBuilders;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.Tests.Builders;
using Moq;

namespace Crave.Ordering.Api.Domain.Tests.Builders.RepositoryBuilders
{
	internal class CategoryRepositoryMockBuilder
	{
		public CategoryRepositoryMockBuilder() : this(new CategoryEntityBuilder().Build())
		{
		}

		private CategoryRepositoryMockBuilder(CategoryEntity categoryEntity) => CategoryEntity = categoryEntity;
		private CategoryEntity CategoryEntity { get; }

		public CategoryRepositoryMockBuilder W(CategoryEntity categoryEntity) => new CategoryRepositoryMockBuilder(categoryEntity);

		public Mock<ICategoryRepository> Build()
		{
			var repositoryMock = new Mock<ICategoryRepository>();
			_ = repositoryMock.Setup(repository => repository.GetCategory(It.IsAny<int>())).Returns(CategoryEntity);
            _ = repositoryMock.Setup(repository => repository.GetCategories(It.IsAny<IReadOnlyList<int>>())).Returns((IReadOnlyList<int> categoryIds) => new EntityCollection<CategoryEntity>(categoryIds.Select(x => new CategoryEntity {CategoryId = x})));
			return repositoryMock;
		}
	}
}