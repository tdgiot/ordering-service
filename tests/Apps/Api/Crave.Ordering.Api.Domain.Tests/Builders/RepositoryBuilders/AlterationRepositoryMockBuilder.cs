﻿using System.Collections.Generic;
using System.Linq;
using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.Tests.Builders;
using Moq;

namespace Crave.Ordering.Api.Domain.Tests.Builders.RepositoryBuilders
{
	internal class AlterationRepositoryMockBuilder
	{
		public AlterationRepositoryMockBuilder() : this(new AlterationEntityBuilder().Build())
		{
		}

		private AlterationRepositoryMockBuilder(AlterationEntity alterationEntity) => AlterationEntity = alterationEntity;
		private AlterationEntity AlterationEntity { get; }

		public AlterationRepositoryMockBuilder W(AlterationEntity alterationEntity) => new AlterationRepositoryMockBuilder(alterationEntity);

		public IAlterationRepository Build()
		{
			var repositoryMock = new Mock<IAlterationRepository>();
			_ = repositoryMock.Setup(repository => repository.GetAlteration(It.IsAny<int>())).Returns(AlterationEntity);
            _ = repositoryMock.Setup(repository => repository.GetAlterations(It.IsAny<IReadOnlyList<int>>())).Returns((IReadOnlyList<int> alterationIds) => new EntityCollection<AlterationEntity>(alterationIds.Select(x => new AlterationEntity { AlterationId = x })));
			return repositoryMock.Object;
		}
	}
}