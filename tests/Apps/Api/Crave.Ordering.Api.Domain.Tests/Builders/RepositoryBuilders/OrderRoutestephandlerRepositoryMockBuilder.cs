﻿using System;
using System.Collections.Generic;
using System.Linq;
using Crave.Enums;
using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.Tests.Builders;
using Moq;

namespace Crave.Ordering.Api.Domain.Tests.Builders.RepositoryBuilders
{
    public class OrderRoutestephandlerRepositoryMockBuilder
    {
        private readonly RepositoryData repositoryData;

        public OrderRoutestephandlerRepositoryMockBuilder() : this(new RepositoryData())
        {

        }

        private OrderRoutestephandlerRepositoryMockBuilder(RepositoryData repositoryData)
        {
            this.repositoryData = repositoryData;
        }

        public OrderRoutestephandlerRepositoryMockBuilder WithStepSpecificRoutesteps(params OrderRoutestephandlerEntityBuilder[] builders) => new OrderRoutestephandlerRepositoryMockBuilder(new RepositoryData(this.repositoryData) 
        {
            StepSpecificRoutestephandlers = SplitOrderRoutestephandlersIntoBuckets(builders)
        });

        public OrderRoutestephandlerRepositoryMockBuilder WithOrderRoutestephandlers(params OrderRoutestephandlerEntityBuilder[] builders) => new OrderRoutestephandlerRepositoryMockBuilder(new RepositoryData(this.repositoryData)
        {
            OrderRoutestephandlers = builders.Select(x => x.Build()).ToList()
        });

        public Mock<IOrderRoutestephandlerRepository> Build()
        {
            Mock<IOrderRoutestephandlerRepository> repositoryMock = new Mock<IOrderRoutestephandlerRepository>();

            foreach (KeyValuePair<Tuple<int, int>, List<OrderRoutestephandlerEntity>> keyValue in this.repositoryData.StepSpecificRoutestephandlers)
            {
                // Item1 = OrderId | Item2 = Number
                repositoryMock.Setup(repository => repository.GetSpecificStepsOfRoute(keyValue.Key.Item1, keyValue.Key.Item2)).Returns(new EntityCollection<OrderRoutestephandlerEntity>(keyValue.Value));
            }

            foreach (OrderRoutestephandlerEntity entity in this.repositoryData.OrderRoutestephandlers)
            {
                repositoryMock.Setup(repo => repo.GetByIdAsync(entity.OrderRoutestephandlerId)).ReturnsAsync(entity);
            }

            repositoryMock.Setup(repo => repo.GetOrderRoutestephandlers(It.IsAny<List<OrderRoutestephandlerStatus>>(), It.IsAny<int>())).Returns(this.repositoryData.OrderRoutestephandlers);

            return repositoryMock;
        }

        public class RepositoryData
        {
            public RepositoryData()
            {
            }

            public RepositoryData(RepositoryData oldData)
            {
                StepSpecificRoutestephandlers = oldData.StepSpecificRoutestephandlers;
                OrderRoutestephandlers = oldData.OrderRoutestephandlers;
            }

            public Dictionary<Tuple<int, int>, List<OrderRoutestephandlerEntity>> StepSpecificRoutestephandlers { get; set; } = new Dictionary<Tuple<int, int>, List<OrderRoutestephandlerEntity>>();
            public List<OrderRoutestephandlerEntity> OrderRoutestephandlers { get; set; } = new List<OrderRoutestephandlerEntity>();
        }

        private Dictionary<Tuple<int, int>, List<OrderRoutestephandlerEntity>> SplitOrderRoutestephandlersIntoBuckets(IEnumerable<OrderRoutestephandlerEntityBuilder> builders)
        {
            Dictionary<Tuple<int, int>, List<OrderRoutestephandlerEntity>> dictionary = new Dictionary<Tuple<int, int>, List<OrderRoutestephandlerEntity>>();

            foreach (OrderRoutestephandlerEntityBuilder entityBuilder in builders)
            {
                OrderRoutestephandlerEntity entity = entityBuilder.Build();

                Tuple<int, int> key = new Tuple<int, int>(entity.OrderId, entity.Number);

                if (!dictionary.TryGetValue(key, out List<OrderRoutestephandlerEntity>? entityCollection))
                {
                    entityCollection = new List<OrderRoutestephandlerEntity>();
                    dictionary.Add(key, entityCollection);
                }

                entityCollection.Add(entity);
            }

            return dictionary;
        }
    }
}