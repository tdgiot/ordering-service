﻿using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Moq;

namespace Crave.Ordering.Api.Domain.Tests.Builders.RepositoryBuilders
{
	internal class DeliverypointgroupRepositoryMockBuilder
	{
		public IDeliverypointgroupRepository Build() => Mock.Of<IDeliverypointgroupRepository>();
	}
}
