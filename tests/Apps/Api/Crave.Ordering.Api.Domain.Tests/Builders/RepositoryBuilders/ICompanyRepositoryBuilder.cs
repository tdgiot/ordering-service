﻿using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.Tests.Builders;

namespace Crave.Ordering.Api.Domain.Tests.Builders.RepositoryBuilders
{
	public interface ICompanyRepositoryBuilder
	{
		ICompanyRepositoryBuilder W(CompanyEntity companyEntity);
		ICompanyRepositoryBuilder W(CompanyEntityBuilder companyEntityBuilder);
		ICompanyRepository Build();
	}
}
