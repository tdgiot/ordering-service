﻿using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.Tests.Builders;
using Moq;

namespace Crave.Ordering.Api.Domain.Tests.Builders.RepositoryBuilders
{
	public class CompanyRepositoryMockBuilder : ICompanyRepositoryBuilder
	{
		public CompanyRepositoryMockBuilder() : this(new CompanyEntityBuilder().Build())
		{
		}

		private CompanyRepositoryMockBuilder(CompanyEntity companyEntity) => CompanyEntity = companyEntity;

		private CompanyEntity CompanyEntity { get; }

		public ICompanyRepositoryBuilder W(CompanyEntity companyEntity) => new CompanyRepositoryMockBuilder(companyEntity);

		public ICompanyRepositoryBuilder W(CompanyEntityBuilder companyEntityBuilder) => W(companyEntityBuilder.Build());

		public ICompanyRepository Build()
		{
			var repositoryMock = new Mock<ICompanyRepository>();
			_ = repositoryMock.Setup(repository => repository.GetCompanyEntity(It.IsAny<int>())).Returns(CompanyEntity);
			_ = repositoryMock.Setup(repository => repository.GetCompanyLocationForOutletAsync(It.IsAny<int>())).ReturnsAsync(CompanyEntity);
			return repositoryMock.Object;
		}
	}
}
