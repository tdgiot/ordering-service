﻿using System.Collections.Generic;
using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Api.Domain.Tests.Builders.EntityBuilders;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.Tests.Builders;
using Moq;

namespace Crave.Ordering.Api.Domain.Tests.Builders.RepositoryBuilders
{
    internal class AlterationoptionTagRepositoryMockBuilder
    {
        public AlterationoptionTagRepositoryMockBuilder() : this(new AlterationoptionTagEntityBuilder().Build())
        {
        }

        private AlterationoptionTagRepositoryMockBuilder(AlterationoptionTagEntity alterationoptionTagEntity) => AlterationoptionTagEntity = alterationoptionTagEntity;
        private AlterationoptionTagEntity AlterationoptionTagEntity { get; }

        public AlterationoptionTagRepositoryMockBuilder W(AlterationoptionTagEntityBuilder alterationoptionTagEntityBuilder) => W(alterationoptionTagEntityBuilder.Build());

        public AlterationoptionTagRepositoryMockBuilder W(AlterationoptionTagEntity alterationoptionTagEntity) => new AlterationoptionTagRepositoryMockBuilder(alterationoptionTagEntity);

        public IAlterationoptionTagRepository Build()
        {
            var repositoryMock = new Mock<IAlterationoptionTagRepository>();
            _ = repositoryMock.Setup(repository => repository.GetAlterationoptionTags(It.IsAny<IReadOnlyList<int>>())).Returns((IReadOnlyList<int> alterationoptionTagIds) => new EntityCollection<AlterationoptionTagEntity>
            {
                AlterationoptionTagEntity
            });
            return repositoryMock.Object;
        }
    }
}
