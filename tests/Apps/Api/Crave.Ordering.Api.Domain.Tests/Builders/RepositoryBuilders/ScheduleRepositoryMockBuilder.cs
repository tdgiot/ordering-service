﻿using System.Collections.Generic;
using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.Tests.Builders;
using Moq;

namespace Crave.Ordering.Api.Domain.Tests.Builders.RepositoryBuilders
{
    internal class ScheduleRepositoryMockBuilder
	{
        private ScheduleEntity ScheduleEntity { get; }
        
        public ScheduleRepositoryMockBuilder() : this(new ScheduleEntityBuilder().Build())
        {

        }

        private ScheduleRepositoryMockBuilder(ScheduleEntity scheduleEntity) => ScheduleEntity = scheduleEntity;

        public IScheduleRepository Build()
		{
            Mock<IScheduleRepository> scheduleRepositoryMock = new Mock<IScheduleRepository>();
            scheduleRepositoryMock.Setup(dataSource => dataSource.GetSchedule(It.IsAny<int>())).Returns(ScheduleEntity);
            scheduleRepositoryMock.Setup(dataSource => dataSource.GetSchedules(It.IsAny<IReadOnlyList<int>>())).Returns((IReadOnlyList<int> scheduleIds) => new EntityCollection<ScheduleEntity>
            {
                ScheduleEntity
            });
            return scheduleRepositoryMock.Object;
        }

        public ScheduleRepositoryMockBuilder W(ScheduleEntityBuilder scheduleEntityBuilder) => W(scheduleEntityBuilder.Build());

        public ScheduleRepositoryMockBuilder W(ScheduleEntity scheduleEntity) => new ScheduleRepositoryMockBuilder(scheduleEntity);
    }
}
