﻿using System.Collections.Generic;
using System.Linq;
using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Api.Domain.Tests.Builders.EntityBuilders;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.Tests.Builders;
using Moq;

namespace Crave.Ordering.Api.Domain.Tests.Builders.RepositoryBuilders
{
	internal class TaxTariffRepositoryMockBuilder
	{
		public TaxTariffRepositoryMockBuilder() : this(new TaxTariffEntityBuilder().Build())
		{
		}

		private TaxTariffRepositoryMockBuilder(TaxTariffEntity taxTariffEntity) => TaxTariffEntity = taxTariffEntity;
		private TaxTariffEntity TaxTariffEntity { get; }

		public TaxTariffRepositoryMockBuilder W(TaxTariffEntity taxTariffEntity) => new TaxTariffRepositoryMockBuilder(taxTariffEntity);

		public ITaxTariffRepository Build()
		{
			var repositoryMock = new Mock<ITaxTariffRepository>();
			_ = repositoryMock.Setup(repository => repository.GetTaxTariff(It.IsAny<int>())).Returns(TaxTariffEntity);
            _ = repositoryMock.Setup(repository => repository.GetTaxTariffs(It.IsAny<IReadOnlyList<int>>())).Returns((IReadOnlyList<int> taxTariffIds) => new EntityCollection<TaxTariffEntity> 
            {
                new TaxTariffEntity
                {
                    TaxTariffId = taxTariffIds.FirstOrDefault(),
	    			Percentage = TaxTariffEntity.Percentage
                }
            });
			return repositoryMock.Object;
		}
	}
}