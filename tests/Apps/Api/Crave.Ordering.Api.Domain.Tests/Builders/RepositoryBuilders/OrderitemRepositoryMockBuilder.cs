﻿using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Moq;
using System.Collections.Generic;

namespace Crave.Ordering.Api.Domain.Tests.Builders.RepositoryBuilders
{
    public class OrderitemRepositoryMockBuilder
    {
        public OrderitemRepositoryMockBuilder()
        {

        }

        public Mock<IOrderitemRepository> Build()
        {
            Mock<IOrderitemRepository> repoMock = new Mock<IOrderitemRepository>();
            repoMock.Setup(repo => repo.GetOrderitems(It.IsAny<List<int>>())).Returns((List<int> info) =>
            {
                EntityCollection<OrderitemEntity> collection = new EntityCollection<OrderitemEntity>();
                foreach (int id in info)
                {
                    collection.Add(new OrderitemEntity
                    {
                        IsNew = false,
                        OrderitemId = id
                    });
                }
                return collection;
            });

            return repoMock;
        }
    }
}