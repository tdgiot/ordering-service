﻿using System.Collections.Generic;
using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Api.Domain.Tests.Builders.EntityBuilders;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.Tests.Builders;
using Moq;

namespace Crave.Ordering.Api.Domain.Tests.Builders.RepositoryBuilders
{
    internal class ProductCategoryTagRepositoryMockBuilder
    {
        public ProductCategoryTagRepositoryMockBuilder() : this(new ProductCategoryTagEntityBuilder().Build())
        {
        }

        private ProductCategoryTagRepositoryMockBuilder(ProductCategoryTagEntity productCategoryTagEntity) => ProductCategoryTagEntity = productCategoryTagEntity;
        private ProductCategoryTagEntity ProductCategoryTagEntity { get; }

        public ProductCategoryTagRepositoryMockBuilder W(ProductCategoryTagEntityBuilder productCategoryTagEntityBuilder) => W(productCategoryTagEntityBuilder.Build());

        public ProductCategoryTagRepositoryMockBuilder W(ProductCategoryTagEntity productCategoryTagEntity) => new ProductCategoryTagRepositoryMockBuilder(productCategoryTagEntity);

        public IProductCategoryTagRepository Build()
        {
            var repositoryMock = new Mock<IProductCategoryTagRepository>();
            _ = repositoryMock.Setup(repository => repository.GetProductCategoryTags(
                    It.IsAny<IReadOnlyList<int>>(),
                    It.IsAny<IReadOnlyList<int>>()
                )
            ).Returns((IReadOnlyList<int> productIds, IReadOnlyList<int> categoryIds) => new EntityCollection<ProductCategoryTagEntity>
            {
                ProductCategoryTagEntity
            });
            return repositoryMock.Object;
        }
    }
}
