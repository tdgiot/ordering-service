﻿using System;
using System.Collections.Generic;
using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Api.Domain.Tests.Builders.EntityBuilders;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Moq;

namespace Crave.Ordering.Api.Domain.Tests.Builders.RepositoryBuilders
{
	internal class AlterationitemRepositoryMockBuilder
	{
		public AlterationitemRepositoryMockBuilder() : this(new AlterationitemEntityBuilder().Build())
		{
		}

		private AlterationitemRepositoryMockBuilder(AlterationitemEntity alterationitemEntity) => AlterationitemEntity = alterationitemEntity;
		private AlterationitemEntity AlterationitemEntity { get; }

		public AlterationitemRepositoryMockBuilder W(AlterationitemEntity alterationitemEntity) => new AlterationitemRepositoryMockBuilder(alterationitemEntity);

		public IAlterationitemRepository Build()
		{
			Mock<IAlterationitemRepository> repositoryMock = new Mock<IAlterationitemRepository>();
			repositoryMock.Setup(repository => repository.GetAlterationitem(It.IsAny<int>(), It.IsAny<int>())).Returns(AlterationitemEntity);
			repositoryMock.Setup(repository => repository.GetAlterationitem(It.IsAny<int>())).Returns(AlterationitemEntity);
            repositoryMock.Setup(repository => repository.GetAlterationitems(It.IsAny<IReadOnlyList<Tuple<int, int>>>())).Returns((IReadOnlyList<Tuple<int, int>> alterationitems) => new EntityCollection<AlterationitemEntity>
            {
                AlterationitemEntity
            });
			return repositoryMock.Object;
		}
	}
}