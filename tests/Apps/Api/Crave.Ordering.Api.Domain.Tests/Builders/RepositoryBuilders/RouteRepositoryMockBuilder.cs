﻿using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.Tests.Builders;
using Moq;

namespace Crave.Ordering.Api.Domain.Tests.Builders.RepositoryBuilders
{
    public class RouteRepositoryMockBuilder
    {
        private readonly int? routeId;
        private readonly RouteEntity routeEntity;

        public RouteRepositoryMockBuilder() : this(null, new RouteEntityBuilder().Build())
        {

        }

        private RouteRepositoryMockBuilder(int? routeId, RouteEntity entity)
        {
            this.routeEntity = entity;
            this.routeId = routeId;
        }

        public RouteRepositoryMockBuilder WithRoute(RouteEntityBuilder entity) => new RouteRepositoryMockBuilder(null, entity.Build());

        public RouteRepositoryMockBuilder WithSpecificRoute(int id, RouteEntityBuilder entity) => new RouteRepositoryMockBuilder(id, entity.Build());

        public Mock<IRouteRepository> Build()
        {
            Mock<IRouteRepository> repoMock = new Mock<IRouteRepository>();

            if (routeId.HasValue)
            {
                repoMock.Setup(repo => repo.GetRoute(It.Is<int>(r => r == this.routeId))).Returns(this.routeEntity);
            }
            else
            {
                repoMock.Setup(repo => repo.GetRoute(It.IsAny<int>())).Returns(this.routeEntity);
            }

            return repoMock;
        }
    }
}