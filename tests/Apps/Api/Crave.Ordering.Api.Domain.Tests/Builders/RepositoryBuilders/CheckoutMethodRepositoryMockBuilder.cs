﻿using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Api.Domain.Tests.Builders.EntityBuilders;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.Tests.Builders;
using Moq;

namespace Crave.Ordering.Api.Domain.Tests.Builders.RepositoryBuilders
{
	internal class CheckoutMethodRepositoryMockBuilder
	{
		private CheckoutMethodEntity CheckoutMethodEntity { get; }

		public CheckoutMethodRepositoryMockBuilder() : this(new CheckoutMethodEntityBuilder().Build()) { }

		private CheckoutMethodRepositoryMockBuilder(CheckoutMethodEntity checkoutMethodEntity) => CheckoutMethodEntity = checkoutMethodEntity;

		public CheckoutMethodRepositoryMockBuilder W(CheckoutMethodEntity CheckoutMethodEntity) => new CheckoutMethodRepositoryMockBuilder(CheckoutMethodEntity);

		public ICheckoutMethodRepository Build()
		{
			var repositoryMock = new Mock<ICheckoutMethodRepository>();
			_ = repositoryMock.Setup(repository => repository.GetCheckoutMethod(It.IsAny<int>())).Returns(CheckoutMethodEntity);
			return repositoryMock.Object;
		}
	}
}