﻿using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.Tests.Builders;
using Moq;

namespace Crave.Ordering.Api.Domain.Tests.Builders.RepositoryBuilders
{
    public class TerminalRepositoryMockBuilder
    {
        private readonly TerminalEntity terminalEntity;

        public TerminalRepositoryMockBuilder() : this(new TerminalEntityBuilder().Build())
        {
        }

        private TerminalRepositoryMockBuilder(TerminalEntity entity)
        {
            this.terminalEntity = entity;
        }

        public TerminalRepositoryMockBuilder WithTerminal(TerminalEntityBuilder terminal) => new TerminalRepositoryMockBuilder(terminal.Build());

        public ITerminalRepository Build()
        {
            Mock<ITerminalRepository> repoMock = new Mock<ITerminalRepository>();
            repoMock.Setup(r => r.GetTerminal(It.IsAny<int>())).Returns(this.terminalEntity);
            return repoMock.Object;
        }
    }
}