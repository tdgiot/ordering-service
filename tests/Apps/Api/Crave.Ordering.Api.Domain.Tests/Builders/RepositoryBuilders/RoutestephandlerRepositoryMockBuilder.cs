﻿using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.Tests.Builders;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;

namespace Crave.Ordering.Api.Domain.Tests.Builders.RepositoryBuilders
{
    internal class RoutestephandlerRepositoryMockBuilder
    {
        private RoutestephandlerEntity RoutestephandlerEntity { get; }

        public RoutestephandlerRepositoryMockBuilder() : this(new RoutestephandlerEntityBuilder().Build())
        {

        }

        private RoutestephandlerRepositoryMockBuilder(RoutestephandlerEntity routestephandlerEntity) => RoutestephandlerEntity = routestephandlerEntity;

        public IRoutestephandlerRepository Build()
        {
            Mock<IRoutestephandlerRepository> routestephandlerRepositoryMock = new Mock<IRoutestephandlerRepository>();
            routestephandlerRepositoryMock.Setup(repo => repo.GetMedia(It.IsAny<int>())).Returns(RoutestephandlerEntity.MediaCollection);
            return routestephandlerRepositoryMock.Object;
        }

        public RoutestephandlerRepositoryMockBuilder W(RoutestephandlerEntityBuilder routestephandlerEntityBuilder) => W(routestephandlerEntityBuilder.Build());

        public RoutestephandlerRepositoryMockBuilder W(RoutestephandlerEntity routestephandlerEntity) => new RoutestephandlerRepositoryMockBuilder(routestephandlerEntity);
    }
}
