﻿using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Api.Domain.Tests.Builders.EntityBuilders;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Moq;

namespace Crave.Ordering.Api.Domain.Tests.Builders.RepositoryBuilders
{
	internal class OutletRepositoryMockBuilder
	{
		private OutletEntity OutletEntity { get; }

		public OutletRepositoryMockBuilder() : this(new OutletEntityBuilder().Build()) { }

		private OutletRepositoryMockBuilder(OutletEntity outletEntity) => OutletEntity = outletEntity;

		public OutletRepositoryMockBuilder W(OutletEntity outletEntity) => new OutletRepositoryMockBuilder(outletEntity);

		public IOutletRepository Build()
		{
			var repositoryMock = new Mock<IOutletRepository>();
			_ = repositoryMock.Setup(repository => repository.GetOutlet(It.IsAny<int>())).Returns(OutletEntity);
			return repositoryMock.Object;
		}
	}
}