﻿using System.ComponentModel.DataAnnotations;
using AutoFixture;
using Crave.Ordering.Api.Domain.UseCases.Requests;

namespace Crave.Ordering.Api.Domain.Tests.Builders
{
	internal class DeliveryInformationBuilder
	{
		public DeliveryInformationBuilder() : this(new Fixture().Create<DeliveryInformationData>())
		{
		}

		private DeliveryInformationBuilder(DeliveryInformationData deliveryInformationData) => Data = deliveryInformationData;

		private DeliveryInformationData Data { get; }

		public DeliveryInformationRequest Build() => new DeliveryInformationRequest(Data.BuildingName, Data.Address, Data.Zipcode, Data.City, Data.Instructions, Data.Longitude, Data.Latitude, Data.DistanceToOutletInMetres);

		public class DeliveryInformationData
		{
			[StringLength(40)] public string BuildingName { get; set; }

			[StringLength(40)] public string Address { get; set; }

			[StringLength(40)] public string Zipcode { get; set; }

			[StringLength(40)] public string City { get; set; }

			[StringLength(40)] public string Instructions { get; set; }

			public int Longitude { get; set; }
			public int Latitude { get; set; }
			public int DistanceToOutletInMetres { get; set; }
		}
	}
}