﻿using System.Collections.Generic;
using System.Linq;
using Crave.Enums;
using Crave.Ordering.Api.Domain.UseCases.Requests;

namespace Crave.Ordering.Api.Domain.Tests.Builders
{
    internal class OrderBuilder
	{
		private OrderType OrderType { get; }
		private string? Guid { get; }
		private int CompanyId { get; }
		private int? ServiceMethodId { get; }
		private int? CheckoutMethodId { get; }
        private int? ClientId { get; }
        private int? DeliverypointId { get; }
		private string? Notes { get; }
        private TippingRequest? Tipping { get; }
		private ServiceChargeRequest? ServiceCharge { get; }
		private DeliveryChargeRequest? DeliveryCharge { get; }
        private DeliverypointRequest? ServiceMethodDeliverypoint { get; }
        private DeliverypointRequest? CheckoutMethodDeliverypoint { get; }
        private CustomerRequest? Customer { get; }
        private DeliveryInformationRequest? DeliveryInformation { get; }
		private AnalyticsRequest? Analytics { get; }
		private IEnumerable<OrderitemRequest>? OrderItems { get; }
        private string OrderContext { get; }
        private int? CoversCount { get; }

        public OrderBuilder() : this(
			OrderType.Standard,
			1,
			1,
			0,
			null,
			null,
			null,
			null,
			null,
			Enumerable.Empty<OrderitemRequest>(),
			new DeliverypointBuilder().Build(),
			new DeliverypointBuilder().Build(),
			new DeliveryInformationBuilder().Build(),
			new CustomerBuilder().Build(),
            string.Empty, 
            null) { }

		private OrderBuilder(OrderType orderType, int companyId, int? serviceMethodId, int? checkoutMethodId, int? clientId, int? deliverypointId, TippingRequest? tipping, ServiceChargeRequest? serviceCharge, 
                             DeliveryChargeRequest? deliveryCharge, IEnumerable<OrderitemRequest>? orderItems, DeliverypointRequest? serviceMethodDeliverypoint, DeliverypointRequest? checkoutMethodDeliverypoint, 
                             DeliveryInformationRequest? deliveryInformation, CustomerRequest? customer, string orderContext, int? coversCount)
		{
			OrderType = orderType;
            CompanyId = companyId;
			ServiceMethodId = serviceMethodId;
			CheckoutMethodId = checkoutMethodId;
            ClientId = clientId;
            DeliverypointId = deliverypointId;
			Tipping = tipping;
			ServiceCharge = serviceCharge;
			DeliveryCharge = deliveryCharge;
			OrderItems = orderItems;
			ServiceMethodDeliverypoint = serviceMethodDeliverypoint;
			CheckoutMethodDeliverypoint = checkoutMethodDeliverypoint;
			DeliveryInformation = deliveryInformation;
            Customer = customer;
            OrderContext = orderContext;
            CoversCount = coversCount;

        }

        private OrderBuilder(OrderBuilder orderBuilder)
        {

        }

		public OrderBuilder W(DeliverypointRequest? serviceMethodDeliverypoint, DeliverypointRequest? checkoutMethodDeliverypoint) =>
			new OrderBuilder(OrderType, CompanyId, ServiceMethodId, CheckoutMethodId, ClientId, DeliverypointId, Tipping, ServiceCharge, DeliveryCharge, OrderItems, serviceMethodDeliverypoint, checkoutMethodDeliverypoint, DeliveryInformation, Customer, OrderContext, CoversCount);

		public OrderBuilder W(CustomerRequest? customer) => new OrderBuilder(OrderType, CompanyId, ServiceMethodId, CheckoutMethodId, ClientId, DeliverypointId, Tipping, ServiceCharge, DeliveryCharge, OrderItems, ServiceMethodDeliverypoint, CheckoutMethodDeliverypoint, DeliveryInformation, customer, OrderContext, CoversCount);

		public OrderBuilder W(DeliveryInformationRequest? deliveryInformation) => new OrderBuilder(OrderType, CompanyId, ServiceMethodId, CheckoutMethodId, ClientId, DeliverypointId, Tipping, ServiceCharge, DeliveryCharge, OrderItems, ServiceMethodDeliverypoint, CheckoutMethodDeliverypoint, deliveryInformation, Customer, OrderContext, CoversCount);

		public OrderBuilder W(IEnumerable<OrderitemRequest> orderItems) => new OrderBuilder(OrderType, CompanyId, ServiceMethodId, CheckoutMethodId, ClientId, DeliverypointId, Tipping, ServiceCharge, DeliveryCharge, orderItems, ServiceMethodDeliverypoint, CheckoutMethodDeliverypoint, DeliveryInformation, Customer, OrderContext, CoversCount);

        public OrderBuilder WithDeliverypointId(int? deliverypointId) => new OrderBuilder(OrderType, CompanyId, ServiceMethodId, CheckoutMethodId, ClientId, deliverypointId, Tipping, ServiceCharge, DeliveryCharge, OrderItems, ServiceMethodDeliverypoint, CheckoutMethodDeliverypoint, DeliveryInformation, Customer, OrderContext, CoversCount);

        public OrderBuilder WithClientId(int? clientId) => new OrderBuilder(OrderType, CompanyId, ServiceMethodId, CheckoutMethodId, clientId, DeliverypointId, Tipping, ServiceCharge, DeliveryCharge, OrderItems, ServiceMethodDeliverypoint, CheckoutMethodDeliverypoint, DeliveryInformation, Customer, OrderContext, CoversCount);

        public OrderBuilder W(string orderContext) => new OrderBuilder(OrderType, CompanyId, ServiceMethodId, CheckoutMethodId, ClientId, DeliverypointId, Tipping, ServiceCharge, DeliveryCharge, OrderItems, ServiceMethodDeliverypoint, CheckoutMethodDeliverypoint, DeliveryInformation, Customer, orderContext, CoversCount);

		public CreateOrderRequest Build() => new CreateOrderRequest(OrderType, Guid, CompanyId, ServiceMethodId, CheckoutMethodId, DeliverypointId, ClientId, Notes, Tipping, ServiceCharge, DeliveryCharge, ServiceMethodDeliverypoint, CheckoutMethodDeliverypoint, Customer, DeliveryInformation, Analytics, OrderItems, OptInStatus.NoOptInAsked, OrderContext, CoversCount);
	}
}