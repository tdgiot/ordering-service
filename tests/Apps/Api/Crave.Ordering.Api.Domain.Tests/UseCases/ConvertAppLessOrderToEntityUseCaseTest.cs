﻿using Crave.Ordering.Api.Core.Enums;
using Crave.Ordering.Api.Domain.Exceptions;
using Crave.Ordering.Api.Domain.Tests.Assertions;
using Crave.Ordering.Api.Domain.Tests.Builders;
using Crave.Ordering.Api.Domain.Tests.Builders.EntityBuilders;
using Crave.Ordering.Api.Domain.Tests.Builders.RepositoryBuilders;
using Crave.Ordering.Api.Domain.Tests.Builders.UseCaseBuilders;
using Crave.Ordering.Api.Domain.UseCases.Requests;
using Crave.Ordering.Api.Domain.Utils;
using Crave.Ordering.Shared.Data.LLBLGen.Tests.Builders;
using NUnit.Framework;
using System.Linq;

namespace Crave.Ordering.Api.Domain.Tests.UseCases
{
	[TestFixture]
	public class ConvertAppLessOrderToEntityUseCaseTest
	{
		static class A
		{
			public static ConvertAppLessOrderToEntityUseCaseBuilder ConvertAppLessOrderToEntityUseCase => new ConvertAppLessOrderToEntityUseCaseBuilder();
			public static EnrichOrderWithOutletDetailsUseCaseBuilder EnrichOrderWithOutletDetailsUseCase => new EnrichOrderWithOutletDetailsUseCaseBuilder();
			public static EnrichOrderWithCompanyDetailsUseCaseBuilder EnrichOrderWithCompanyDetailsUseCase => new EnrichOrderWithCompanyDetailsUseCaseBuilder();
			public static EnrichOrderWithServiceMethodDetailsUseCaseBuilder EnrichOrderWithServiceMethodDetailsUseCase => new EnrichOrderWithServiceMethodDetailsUseCaseBuilder();
			public static EnrichOrderWithCheckoutMethodDetailsUseCaseBuilder EnrichOrderWithCheckoutMethodDetailsUseCase => new EnrichOrderWithCheckoutMethodDetailsUseCaseBuilder();
			public static EnrichOrderWithDeliverypointDetailsUseCaseBuilder EnrichOrderWithDeliverypointDetailsUseCase => new EnrichOrderWithDeliverypointDetailsUseCaseBuilder();
			public static EnrichOrderWithChargeToDeliverypointUseCaseBuilder EnrichOrderWithChargeToDeliverypointUseCase => new EnrichOrderWithChargeToDeliverypointUseCaseBuilder();
			public static EnrichOrderWithOrderitemsUseCaseBuilder EnrichOrderWithOrderitemsUseCase => new EnrichOrderWithOrderitemsUseCaseBuilder();
			public static EnrichOrderitemsWithProductsUseCaseBuilder EnrichOrderitemsWithProductsUseCaseBuilder => new EnrichOrderitemsWithProductsUseCaseBuilder();
			public static EnrichOrderitemsWithCategoriesUseCaseBuilder EnrichOrderitemsWithCategoriesUseCaseBuilder => new EnrichOrderitemsWithCategoriesUseCaseBuilder();
			public static EnrichOrderitemsWithAlterationsUseCaseBuilder EnrichOrderitemsWithAlterationsUseCaseBuilder => new EnrichOrderitemsWithAlterationsUseCaseBuilder();
			public static EnrichOrderitemsWithAlterationitemsUseCaseBuilder EnrichOrderitemsWithAlterationitemsUseCaseBuilder => new EnrichOrderitemsWithAlterationitemsUseCaseBuilder();
			public static EnrichOrderitemsWithPriceLevelItemsUseCaseBuilder EnrichOrderitemsWithPriceLevelItemsUseCase => new EnrichOrderitemsWithPriceLevelItemsUseCaseBuilder();
			public static EnrichOrderWithClientUseCaseBuilder EnrichOrderWithClientUseCaseBuilder => new EnrichOrderWithClientUseCaseBuilder();

			public static CompanyEntityBuilder CompanyEntity => new CompanyEntityBuilder();
            public static OutletEntityBuilder OutletEntity => new OutletEntityBuilder();
			public static OrderBuilder Order => new OrderBuilder();
            public static DeliverypointBuilder Deliverypoint => new DeliverypointBuilder();
            public static CustomerBuilder Customer => new CustomerBuilder();
            public static DeliveryInformationBuilder DeliveryInformation => new DeliveryInformationBuilder();
            public static OrderitemBuilder Orderitem => new OrderitemBuilder();
            public static AlterationitemBuilder Alterationitem => new AlterationitemBuilder();
            public static AlterationoptionEntityBuilder Alterationoption => new AlterationoptionEntityBuilder();

            public static ProductEntityBuilder ProductEntity => new ProductEntityBuilder();
            public static DeviceEntityBuilder DeviceEntity => new DeviceEntityBuilder();
            public static ServiceMethodEntityBuilder ServiceMethodEntity => new ServiceMethodEntityBuilder();
            public static CheckoutMethodEntityBuilder CheckoutMethodEntity => new CheckoutMethodEntityBuilder();
            public static DeliverypointEntityBuilder DeliverypointEntity => new DeliverypointEntityBuilder();
            public static PriceLevelItemEntityBuilder PriceLevelItemEntity => new PriceLevelItemEntityBuilder();
            public static AlterationitemEntityBuilder AlterationitemEntity => new AlterationitemEntityBuilder();

			public static ICompanyRepositoryBuilder CompanyRepositoryMock => new CompanyRepositoryMockBuilder();
			public static OutletRepositoryMockBuilder OutletRepositoryMock => new OutletRepositoryMockBuilder();
            public static IServiceMethodRepositoryBuilder ServiceMethodRepositoryMock => new ServiceMethodRepositoryMockBuilder();
			public static CheckoutMethodRepositoryMockBuilder CheckoutMethodRepositoryMock => new CheckoutMethodRepositoryMockBuilder();
			public static DeliverypointRepositoryMockBuilder DeliverypointRepositoryMock => new DeliverypointRepositoryMockBuilder();
			public static ProductRepositoryMockBuilder ProductRepositoryMock => new ProductRepositoryMockBuilder();
			public static CategoryRepositoryMockBuilder CategoryRepositoryMock => new CategoryRepositoryMockBuilder();
			public static AlterationRepositoryMockBuilder AlterationRepositoryMock => new AlterationRepositoryMockBuilder();
			public static AlterationitemRepositoryMockBuilder AlterationitemRepositoryMock => new AlterationitemRepositoryMockBuilder();
			public static DeviceRepositoryMockBuilder DeviceRepositoryMock => new DeviceRepositoryMockBuilder();
			public static PriceScheduleRepositoryMockBuilder PriceScheduleRepositoryMock => new PriceScheduleRepositoryMockBuilder();
        }

		[Test]
		public void Execute_WithValidCompany_CopiesTheCompanyInfo()
		{
			// Arrange
			var companyEntity = A.CompanyEntity
				.Build();

			var sut = A.ConvertAppLessOrderToEntityUseCase
				.W(A.EnrichOrderWithOutletDetailsUseCase)
                .W(A.EnrichOrderWithCompanyDetailsUseCase
                       .W(A.CompanyRepositoryMock
                           .W(companyEntity)))
                    .Build();

			var order = A.Order
				.W(null, null)
				.Build();

			// Act
			var result = sut.Execute(order);

			// Assert
			result.AssertEqual(companyEntity);
		}

		[Test]
		public void Execute_WithNewCompany_ThrowsCheckoutValidationException()
		{
			static void Act()
			{
				// Arrange
				var sut = A.ConvertAppLessOrderToEntityUseCase
					.W(A.EnrichOrderWithOutletDetailsUseCase)
					.W(A.EnrichOrderWithCompanyDetailsUseCase
						.W(A.CompanyRepositoryMock
						    .W(A.CompanyEntity
								.AsNew()
								.Build())))
					.Build();

				// Act
				_ = sut.Execute(A.Order.Build());
			}

			// Assert
			var exception = Assert.Throws<CheckoutValidationException>(Act);
			Assert.AreEqual(ValidationErrorSubType.CompanyInvalid, exception.CheckoutValidationResponse.ValidationErrors.First().ValidationErrorSubType);
		}

		[Test]
		public void Execute_WithValidOutlet_CopiesTheOutletInfo()
		{
			// Arrange
			var outletEntity = A.OutletEntity
				.Build();

			var sut = A.ConvertAppLessOrderToEntityUseCase
				.W(A.EnrichOrderWithOutletDetailsUseCase
					.W(A.OutletRepositoryMock
						.W(outletEntity)))
				.Build();

			var order = A.Order
				.W(null, null)
				.Build();

			// Act
			var result = sut.Execute(order);

			// Assert
			result.AssertEqual(outletEntity);
		}

		[Test]
		public void Execute_WithValidServiceMethod_CopiesTheServiceMethodInfo()
		{
			// Arrange
			var serviceMethodEntity = A.ServiceMethodEntity
				.Build();

			var sut = A.ConvertAppLessOrderToEntityUseCase
				.W(A.EnrichOrderWithServiceMethodDetailsUseCase
					.W(A.ServiceMethodRepositoryMock
						.W(serviceMethodEntity)))
				.Build();

			var order = A.Order
				.W(null, null)
				.Build();

			// Act
			var result = sut.Execute(order);

			// Assert
			result.AssertEqual(serviceMethodEntity);
		}

		[Test]
		public void Execute_WithNewServiceMethod_ThrowsCheckoutValidationException()
		{
			static void Act()
			{
				// Arrange
				var sut = A.ConvertAppLessOrderToEntityUseCase
					.W(A.EnrichOrderWithServiceMethodDetailsUseCase
						.W(A.ServiceMethodRepositoryMock
							.W(A.ServiceMethodEntity
								.AsNew()
								.Build())))
					.Build();

				// Act
				_ = sut.Execute(A.Order.Build());
			}

			// Assert
			var exception = Assert.Throws<CheckoutValidationException>(Act);
			Assert.AreEqual(ValidationErrorSubType.ServiceMethodInvalid, exception.CheckoutValidationResponse.ValidationErrors.First().ValidationErrorSubType);
		}

		[Test]
		public void Execute_WithValidCheckoutMethod_CopiesTheCheckoutMethodInfo()
		{
			// Arrange
			var checkoutMethodEntity = A.CheckoutMethodEntity
				.Build();

			var sut = A.ConvertAppLessOrderToEntityUseCase
				.W(A.EnrichOrderWithCheckoutMethodDetailsUseCase
					.W(A.CheckoutMethodRepositoryMock
						.W(checkoutMethodEntity)))
				.Build();

			var order = A.Order
				.W(null, null)
				.Build();

			// Act
			var result = sut.Execute(order);

			// Assert
			result.AssertEqual(checkoutMethodEntity);
		}

		[Test]
		public void Execute_WithNewCheckoutMethod_ThrowsCheckoutValidationException()
		{
			static void Act()
			{
				// Arrange
				var sut = A.ConvertAppLessOrderToEntityUseCase
					.W(A.EnrichOrderWithCheckoutMethodDetailsUseCase
						.W(A.CheckoutMethodRepositoryMock
							.W(A.CheckoutMethodEntity
								.AsNew()
								.Build())))
					.Build();

				// Act
				_ = sut.Execute(A.Order.Build());
			}

			// Assert
			var exception = Assert.Throws<CheckoutValidationException>(Act);
			Assert.AreEqual(ValidationErrorSubType.CheckoutMethodInvalid, exception.CheckoutValidationResponse.ValidationErrors.First().ValidationErrorSubType);
		}

        [Test]
        public void Execute_WithValidDeliverypoint_CopiesTheDeliverypointInfo()
        {
            // Arrange
            var deliverypointEntity = A.DeliverypointEntity
                                       .Build();

            var sut = A.ConvertAppLessOrderToEntityUseCase
                       .W(A.EnrichOrderWithDeliverypointDetailsUseCase
                           .W(A.DeliverypointRepositoryMock
                               .W((deliverypointEntity, deliverypointEntity, deliverypointEntity))))
                       .Build();

            var order = A.Order
                         .WithDeliverypointId(deliverypointEntity.DeliverypointId)
                         .Build();

            // Act
            var result = sut.Execute(order);

            // Assert
            result.AssertDeliverypointEqual(deliverypointEntity);
        }

		[Test]
		public void Execute_WithValidServiceMethodDeliverypoint_CopiesTheDeliverypointInfo()
		{
			// Arrange
			var deliverypointEntity = A.DeliverypointEntity
				.Build();

			var deliverypoint = A.Deliverypoint
				.Build();

			var sut = A.ConvertAppLessOrderToEntityUseCase
				.W(A.EnrichOrderWithDeliverypointDetailsUseCase
					.W(A.DeliverypointRepositoryMock
						.W((deliverypointEntity, deliverypointEntity, deliverypointEntity))))
				.Build();

			var order = A.Order
				.W(deliverypoint, null)
				.Build();

			// Act
			var result = sut.Execute(order);

			// Assert
			result.AssertDeliverypointEqual(deliverypointEntity);
		}

		[Test]
		public void Execute_WithNewServiceMethodDeliverypoint_ThrowsCheckoutValidationException()
		{
			// Arrange
			var serviceMethodDeliverypoint = A.DeliverypointEntity
				.AsNew()
				.Build();

			var otherDeliverypoint = A.DeliverypointEntity.Build();

			var sut = A.ConvertAppLessOrderToEntityUseCase
				.W(A.EnrichOrderWithDeliverypointDetailsUseCase
					.W(A.DeliverypointRepositoryMock
						.W((otherDeliverypoint, serviceMethodDeliverypoint, otherDeliverypoint))))
				.Build();

			// Act
			void Act() => _ = sut.Execute(A.Order
				.W(A.Deliverypoint.Build(), null)
				.Build());

			// Assert
			var exception = Assert.Throws<CheckoutValidationException>(Act);
			Assert.AreEqual(ValidationErrorSubType.ServiceMethodDeliveryPointInvalid, exception.CheckoutValidationResponse.ValidationErrors.First().ValidationErrorSubType);
		}

		[Test]
		public void Execute_WithoutServiceMethodDeliverypoint_DoesNotSetTheDeliverypoint()
		{
			// Arrange
			var deliverypoint = A.Deliverypoint
				.Build();

			var sut = A.ConvertAppLessOrderToEntityUseCase
				.Build();

			var order = A.Order
				.W(null, deliverypoint)
				.Build();

			// Act
			var result = sut.Execute(order);

			// Assert
			Assert.IsNull(result.Deliverypoint);
		}

		[Test]
		public void Execute_WithValidChargeToDeliverypoint_CopiesTheDeliverypointInfo()
		{
			// Arrange
			var deliverypointEntity = A.DeliverypointEntity
				.Build();

			var dummyDeliverypointEntity = A.DeliverypointEntity
				.Build();

			var deliverypoint = A.Deliverypoint
				.Build();

			var sut = A.ConvertAppLessOrderToEntityUseCase
				.W(A.EnrichOrderWithChargeToDeliverypointUseCase
					.W(A.DeliverypointRepositoryMock
						.W((dummyDeliverypointEntity, dummyDeliverypointEntity, deliverypointEntity))))
				.Build();

			var order = A.Order
				.W(A.Deliverypoint.Build(), deliverypoint)
				.Build();

			// Act
			var result = sut.Execute(order);

			// Assert
			result.AssertCheckoutMethodEqual(deliverypointEntity);
		}

		[Test]
		public void Execute_WithNewChargeToDeliverypoint_ThrowsCheckoutValidationException()
		{
			// Arrange
			var chargeToDeliverypointEntity = A.DeliverypointEntity
				.AsNew()
				.Build();

			var otherDeliverypointEntity = A.DeliverypointEntity.Build();

			var sut = A.ConvertAppLessOrderToEntityUseCase
				.W(A.EnrichOrderWithChargeToDeliverypointUseCase
					.W(A.DeliverypointRepositoryMock
						 .W((otherDeliverypointEntity, otherDeliverypointEntity, chargeToDeliverypointEntity))))
				.Build();

			// Act
			void Act() => _ = sut.Execute(A.Order
				.W(A.Deliverypoint.Build(), A.Deliverypoint.Build())
				.Build());

			// Assert
			var exception = Assert.Throws<CheckoutValidationException>(Act);
			Assert.AreEqual(ValidationErrorSubType.CheckoutMethodDeliveryPointInvalid, exception.CheckoutValidationResponse.ValidationErrors.First().ValidationErrorSubType);
		}

		[Test]
		public void Execute_WithoutChargeToDeliverypoint_DoesNotSetTheDeliverypoint()
		{
			// Arrange
			var sut = A.ConvertAppLessOrderToEntityUseCase
				.Build();

			var order = A.Order
				.W(A.Deliverypoint.Build(), null)
				.Build();

			// Act
			var result = sut.Execute(order);

			// Assert
			Assert.IsNull(result.ChargeToDeliverypoint);
		}

		[Test]
		public void Execute_WithValidCustomer_CopiesTheCustomerInfo()
		{
			// Arrange
			var customer = A.Customer.Build();

			var sut = A.ConvertAppLessOrderToEntityUseCase.Build();

			var order = A.Order
				.W(customer)
				.Build();

			// Act
			var result = sut.Execute(order);

			// Assert
			Assert.AreEqual(customer.LastName, result.CustomerLastname);
			Assert.AreEqual(customer.Email, result.Email);
			Assert.AreEqual(PhoneNumberUtil.Standardize(customer.Phonenumber ?? string.Empty), result.CustomerPhonenumber);
		}

		[Test]
		public void Execute_WithoutCustomer_DoesNotCopyTheCustomerInfo()
		{
			// Arrange
			var sut = A.ConvertAppLessOrderToEntityUseCase.Build();

			var order = A.Order
				.W((CustomerRequest?)null)
				.Build();

			// Act
			var result = sut.Execute(order);

			// Assert
			Assert.IsEmpty(result.CustomerLastname);
			Assert.IsEmpty(result.CustomerPhonenumber);
			Assert.IsEmpty(result.Email);
		}

		[Test]
		public void Execute_WithValidDeliveryInformation_CopiesTheDeliveryInformation()
		{
			// Arrange
			var deliveryInformation = A.DeliveryInformation.Build();

			var sut = A.ConvertAppLessOrderToEntityUseCase.Build();

			var order = A.Order
				.W(deliveryInformation)
				.Build();

			// Act
			var result = sut.Execute(order);

			// Assert
			result.AssertEqual(deliveryInformation);
		}

		[Test]
		public void Execute_WithoutDeliveryInformation_DoesNotCopyTheTheDeliveryInformation()
		{
			// Arrange
			var sut = A.ConvertAppLessOrderToEntityUseCase.Build();

			var order = A.Order
				.W((DeliveryInformationRequest?)null)
				.Build();

			// Act
			var result = sut.Execute(order);

			// Assert
			Assert.IsNull(result.DeliveryInformation);
		}

        [Test]
		public void Execute_WithValidAlterationItems_CopiesTheAlterationItemInformation()
		{
			// Arrange
			var product = A.ProductEntity
                           .Build();

			var sut = A.ConvertAppLessOrderToEntityUseCase
				.W(A.EnrichOrderWithOrderitemsUseCase
                    .W(A.EnrichOrderitemsWithProductsUseCaseBuilder
                        .W(A.ProductRepositoryMock
                            .W(product)))
                    .W(A.EnrichOrderitemsWithCategoriesUseCaseBuilder
                        .W(A.CategoryRepositoryMock))
                    .W(A.EnrichOrderitemsWithAlterationsUseCaseBuilder
                        .W(A.AlterationRepositoryMock))
                    .W(A.EnrichOrderitemsWithAlterationitemsUseCaseBuilder
                        .W(A.AlterationitemRepositoryMock)))
                .Build();

			var orderitems = new[] {A.Orderitem
				.W(product.ProductId)
                .W(product.PriceIn)
				.Build()};

			var order = A.Order
                .W(orderitems)
                .Build();

			// Act
			var result = sut.Execute(order);

			// Assert
			result.AssertEqual(orderitems);
		}

        [Test]
        public void Execute_WithoutClientId_DoesNotCopyClientInformation()
        {
            // Arrange
            var sut = A.ConvertAppLessOrderToEntityUseCase.Build();

            var order = A.Order
                         .WithClientId(null)
                         .Build();

            // Act
            var result = sut.Execute(order);

            // Assert
            Assert.IsEmpty(result.ClientMacAddress);
        }

        [Test]
        public void Execute_WithClientId_CopiesClientInformation()
        {
            // Arrange
            var device = A.DeviceEntity
                          .WithIdentifier("11:22:33:44:55:66")
                          .Build();

            var sut = A.ConvertAppLessOrderToEntityUseCase
                       .W(A.EnrichOrderWithClientUseCaseBuilder
                           .WithDeviceRepository(A.DeviceRepositoryMock
                                                  .WithDevice(device)))
                       .Build();

            var order = A.Order
                         .WithClientId(1)
                         .Build();

            // Act
            var result = sut.Execute(order);

            // Assert
            Assert.AreEqual(device.Identifier, result.ClientMacAddress);
        }

        [Test]
        public void Execute_WithOrderitemWithPriceLevelItem_CopiesThePriceLevelItemInformation()
        {
            // Arrange
            var priceLevelItem = A.PriceLevelItemEntity
                                  .WithPrice(10)
                                  .Build();

            var product = A.ProductEntity
                           .WithPriceIn(5)
                           .Build();

            var sut = A.ConvertAppLessOrderToEntityUseCase
                       .W(A.EnrichOrderWithOrderitemsUseCase
                           .W(A.EnrichOrderitemsWithProductsUseCaseBuilder
                               .W(A.ProductRepositoryMock.W(product)))
                           .W(A.EnrichOrderitemsWithPriceLevelItemsUseCase
                               .W(A.PriceScheduleRepositoryMock
                                   .W(priceLevelItem))))
                       .Build();

            var orderitems = new[] {A.Orderitem
                                     .W(product.ProductId)
                                     .W(priceLevelItem.Price)
                                     .WithPriceLevelItemId(priceLevelItem.PriceLevelItemId)
                                     .Build()};

            var order = A.Order
                         .W(orderitems)
                         .Build();

            // Act
            var result = sut.Execute(order);

            // Assert
            result.AssertEqual(orderitems);
        }

        [Test]
        public void Execute_WithOrderitemAlterationitemWithPriceLevelItem_CopiesThePriceLevelItemInformation()
        {
            // Arrange
            var priceLevelItemEntity = A.PriceLevelItemEntity
                                  .WithPrice(10)
                                  .Build();

            var alterationItemEntity = A.AlterationitemEntity
                                        .WithAlterationId(1)
                                        .WithAlterationoptionId(2)
                                        .WithAlterationoption(A.Alterationoption
                                                               .WithPriceIn(5)
                                                               .Build())
                                        .Build();

            var productEntity = A.ProductEntity
                           .WithPriceIn(5)
                           .Build();

			var sut = A.ConvertAppLessOrderToEntityUseCase
                       .W(A.EnrichOrderWithOrderitemsUseCase
                           .W(A.EnrichOrderitemsWithProductsUseCaseBuilder
                               .W(A.ProductRepositoryMock.W(productEntity)))
                           .W(A.EnrichOrderitemsWithAlterationitemsUseCaseBuilder
                               .W(A.AlterationitemRepositoryMock
                                   .W(alterationItemEntity)))
						   .W(A.EnrichOrderitemsWithPriceLevelItemsUseCase
                               .W(A.PriceScheduleRepositoryMock
                                   .W(priceLevelItemEntity))))
                       .Build();

            var alterationitems = new[] { A.Alterationitem
                                           .WithAlterationId(alterationItemEntity.AlterationId)
                                           .WithAlterationoptionId(alterationItemEntity.AlterationoptionId)
                                           .WithPriceLevelItemId(priceLevelItemEntity.PriceLevelItemId)
                                           .WithPrice(priceLevelItemEntity.Price)
                                           .Build() };

            var orderitems = new[] {A.Orderitem
                                     .W(productEntity.ProductId)
                                     .W(productEntity.PriceIn)
									 .WithAlterationitems(alterationitems)
                                     .Build()};

            var order = A.Order
                         .W(orderitems)
                         .Build();

            // Act
            var result = sut.Execute(order);

            // Assert
            result.AssertEqual(orderitems);
        }

        [Test]
        public void Execute_WithOrderContext_CopiesTheOrderContext()
        {
            // Arrange
            var sut = A.ConvertAppLessOrderToEntityUseCase.Build();

            var order = A.Order
                .W("TimeSlot: 10:00 - 10:30")
                .Build();

            // Act
            var result = sut.Execute(order);

            // Assert
            Assert.AreEqual(order.OrderContext, result.Notes);
        }

        [Test]
        public void Execute_WithoutOrderContext_DoesNotCopyTheOrderContext()
        {
            // Arrange
            var sut = A.ConvertAppLessOrderToEntityUseCase.Build();

            var order = A.Order
                .W((string)null)
                .Build();

            // Act
            var result = sut.Execute(order);

            // Assert
            Assert.IsEmpty(result.Notes);
        }

        [Test]
        public void Execute_WithCompanyWithCountryCode_OrderHasCompanyCountryCode()
        {
            // Arrange
            var companyEntity = A.CompanyEntity
                .WithCountryCode("GBR")
                .Build();

            var checkoutMethodEntity = A.CheckoutMethodEntity
                .W("")
                .Build();

            var sut = A.ConvertAppLessOrderToEntityUseCase
                .W(A.EnrichOrderWithCompanyDetailsUseCase
                    .W(A.CompanyRepositoryMock
                        .W(companyEntity)))
                .W(A.EnrichOrderWithCheckoutMethodDetailsUseCase
                    .W(A.CheckoutMethodRepositoryMock
                        .W(checkoutMethodEntity)))
                .Build();

            var order = A.Order
                .W(null, null)
                .Build();

            // Act
            var result = sut.Execute(order);

            // Assert
            Assert.AreEqual(result.CountryCode, companyEntity.CountryCode);
        }

		[Test]
        public void Execute_WithCompanyAndCheckoutMethodWithCountryCode_OrderHasCheckoutMethodCountryCode()
        {
            // Arrange
            var companyEntity = A.CompanyEntity
                .WithCountryCode("GBR")
                .Build();

			var checkoutMethodEntity = A.CheckoutMethodEntity
                .W("NLD")
                .Build();

            var sut = A.ConvertAppLessOrderToEntityUseCase
                .W(A.EnrichOrderWithCompanyDetailsUseCase
                    .W(A.CompanyRepositoryMock
                        .W(companyEntity)))
                .W(A.EnrichOrderWithCheckoutMethodDetailsUseCase
                    .W(A.CheckoutMethodRepositoryMock
                        .W(checkoutMethodEntity)))
                .Build();

            var order = A.Order
                .W(null, null)
                .Build();

            // Act
            var result = sut.Execute(order);

            // Assert
            Assert.AreNotEqual(result.CountryCode, companyEntity.CountryCode);
            Assert.AreEqual(result.CountryCode, checkoutMethodEntity.CountryCode);
        }

        [Test]
        public void Execute_WithCompanyWithCurrencyCode_OrderHasCompanyCurrencyCode()
        {
            // Arrange
            var companyEntity = A.CompanyEntity
                .WithCurrencyCode("EUR")
                .Build();

            var checkoutMethodEntity = A.CheckoutMethodEntity
                .WithCurrencyCode("")
                .Build();

            var sut = A.ConvertAppLessOrderToEntityUseCase
                .W(A.EnrichOrderWithCompanyDetailsUseCase
                    .W(A.CompanyRepositoryMock
                        .W(companyEntity)))
                .W(A.EnrichOrderWithCheckoutMethodDetailsUseCase
                    .W(A.CheckoutMethodRepositoryMock
                        .W(checkoutMethodEntity)))
                .Build();

            var order = A.Order
                .W(null, null)
                .Build();

            // Act
            var result = sut.Execute(order);

            // Assert
            Assert.AreEqual(result.CurrencyCode, companyEntity.CurrencyCode);
        }

		[Test]
        public void Execute_WithCompanyAndCheckoutMethodWithCurrencyCode_OrderHasCheckoutMethodCurrencyCode()
        {
            // Arrange
            var companyEntity = A.CompanyEntity
                .WithCurrencyCode("EUR")
                .Build();

            var checkoutMethodEntity = A.CheckoutMethodEntity
                .WithCurrencyCode("GBP")
				.Build();

            var sut = A.ConvertAppLessOrderToEntityUseCase
                .W(A.EnrichOrderWithCompanyDetailsUseCase
                    .W(A.CompanyRepositoryMock
                        .W(companyEntity)))
                .W(A.EnrichOrderWithCheckoutMethodDetailsUseCase
                    .W(A.CheckoutMethodRepositoryMock
                        .W(checkoutMethodEntity)))
                .Build();

            var order = A.Order
                .W(null, null)
                .Build();

            // Act
            var result = sut.Execute(order);

            // Assert
            Assert.AreNotEqual(result.CurrencyCode, companyEntity.CurrencyCode);
            Assert.AreEqual(result.CurrencyCode, checkoutMethodEntity.CurrencyCode);
        }
	}
}
