﻿using System;
using Crave.Enums;
using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Api.Domain.UseCases;
using Crave.Ordering.Api.Domain.UseCases.Requests;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Tests;
using NSubstitute;
using NUnit.Framework;

namespace Crave.Ordering.Api.Domain.Tests.UseCases
{
    [TestFixture]
    public class CreateReceiptUseCaseTests : TestBase
    {
        private IReceiptRepository receiptRepository;
        private IOutletSellerInformationRepository outletSellerInformationRepository;
        private ICheckoutMethodRepository checkoutMethodRepository;
        private IPaymentIntegrationConfigurationRepository paymentIntegrationConfigurationRepository;

        private static readonly PaymentProviderEntity PaymentProvider = new PaymentProviderEntity { PaymentProcessorInfo = "Processor info on payment provider" };

        private static readonly PaymentIntegrationConfigurationEntity ExistingPaymentIntegrationConfiguration = new PaymentIntegrationConfigurationEntity(1) { IsNew = false, PaymentProvider = PaymentProvider };
        private static readonly PaymentIntegrationConfigurationEntity ExistingPaymentIntegrationConfigurationWithPaymentProcessorInfo = new PaymentIntegrationConfigurationEntity(2) { IsNew = false, PaymentProvider = PaymentProvider, PaymentProcessorInfo = "Processor info on payment integration configuration" };
        private static readonly PaymentIntegrationConfigurationEntity NewPaymentIntegrationConfiguration = new PaymentIntegrationConfigurationEntity(42) { IsNew = true };

        private CreateReceiptUseCase useCase;

        [OneTimeSetUp]
        public void _OneTimeSetUp()
        {
            this.receiptRepository = CreateSubstitute<IReceiptRepository>();

            this.outletSellerInformationRepository = CreateSubstitute<IOutletSellerInformationRepository>();
            this.outletSellerInformationRepository.FetchByOutletId(1).Returns(new OutletSellerInformationEntity(1)
            {
                ParentCompanyId = 1,
                SellerName = "UnitTest",
                Address = "In side your computer",
                ReceiptReceiversSms = "001001110",
                ReceiptReceiversEmail = "unit@test.local",
                Phonenumber = "0123456789",
                VatNumber = "42",
                Email = "seller@test.local"
            });
            
            this.checkoutMethodRepository = CreateSubstitute<ICheckoutMethodRepository>();
            this.checkoutMethodRepository.GetCheckoutMethod(1).Returns(new CheckoutMethodEntity { CheckoutType = CheckoutType.PaymentProvider, ReceiptTemplateId = 1, PaymentIntegrationConfigurationId = ExistingPaymentIntegrationConfiguration .PaymentIntegrationConfigurationId});
            this.checkoutMethodRepository.GetCheckoutMethod(2).Returns(new CheckoutMethodEntity { CheckoutType = CheckoutType.PaymentProvider, ReceiptTemplateId = 1, PaymentIntegrationConfigurationId = ExistingPaymentIntegrationConfigurationWithPaymentProcessorInfo.PaymentIntegrationConfigurationId });
            this.checkoutMethodRepository.GetCheckoutMethod(42).Returns(new CheckoutMethodEntity {CheckoutType = CheckoutType.ChargeToRoom});
            this.checkoutMethodRepository.GetCheckoutMethod(1337).Returns(new CheckoutMethodEntity { CheckoutType = CheckoutType.PaymentProvider});
            this.checkoutMethodRepository.GetCheckoutMethod(9001).Returns(new CheckoutMethodEntity { CheckoutType = CheckoutType.PaymentProvider, ReceiptTemplateId = 1});
            this.checkoutMethodRepository.GetCheckoutMethod(69).Returns(new CheckoutMethodEntity { CheckoutType = CheckoutType.PaymentProvider, ReceiptTemplateId = 1, PaymentIntegrationConfigurationId = NewPaymentIntegrationConfiguration.PaymentIntegrationConfigurationId });

            this.paymentIntegrationConfigurationRepository = CreateSubstitute<IPaymentIntegrationConfigurationRepository>();
            this.paymentIntegrationConfigurationRepository.FetchById(ExistingPaymentIntegrationConfiguration.PaymentIntegrationConfigurationId).Returns(ExistingPaymentIntegrationConfiguration);
            this.paymentIntegrationConfigurationRepository.FetchById(ExistingPaymentIntegrationConfigurationWithPaymentProcessorInfo.PaymentIntegrationConfigurationId).Returns(ExistingPaymentIntegrationConfigurationWithPaymentProcessorInfo);
            this.paymentIntegrationConfigurationRepository.FetchById(NewPaymentIntegrationConfiguration.PaymentIntegrationConfigurationId).Returns(NewPaymentIntegrationConfiguration);

            this.useCase = new CreateReceiptUseCase(
                this.receiptRepository,
                this.outletSellerInformationRepository,
                this.checkoutMethodRepository,
                this.paymentIntegrationConfigurationRepository);
        }

        [Test]
        public void OrderDoesNotContainCheckoutMethod_ThrowsException()
        {
            CreateReceipt request = new CreateReceipt
            {
                Order = new OrderEntity(1)
                {
                    CheckoutMethodId = null
                }
            };
            Assert.Throws<InvalidOperationException>(() => this.useCase.Execute(request));
            this.receiptRepository.DidNotReceive().CreateReceipt(Arg.Any<ReceiptEntity>());
        }

        [Test]
        public void CheckoutMethodIsNotPaymentProvider_ThrowsException()
        {
            CreateReceipt request = new CreateReceipt
            {
                Order = new OrderEntity(1)
                {
                    CheckoutMethodId = 42
                }
            };
            Assert.Throws<InvalidOperationException>(() => this.useCase.Execute(request));
            this.receiptRepository.DidNotReceive().CreateReceipt(Arg.Any<ReceiptEntity>());
        }

        [Test]
        public void CheckoutMethodDoesNotHaveReceiptTemplate_ThrowsException()
        {
            CreateReceipt request = new CreateReceipt
            {
                Order = new OrderEntity(1)
                {
                    CheckoutMethodId = 1337
                }
            };
            Assert.Throws<InvalidOperationException>(() => this.useCase.Execute(request));
            this.receiptRepository.DidNotReceive().CreateReceipt(Arg.Any<ReceiptEntity>());
        }

        [Test]
        public void CheckoutMethodDoesNotHavePaymentIntegrationConfiguration_ThrowsException()
        {
            CreateReceipt request = new CreateReceipt
            {
                Order = new OrderEntity(1)
                {
                    CheckoutMethodId = 9001
                }
            };
            Assert.Throws<InvalidOperationException>(() => this.useCase.Execute(request));
            this.receiptRepository.DidNotReceive().CreateReceipt(Arg.Any<ReceiptEntity>());
        }

        [Test]
        public void PaymentIntegrationConfigurationIsInvalid_ThrowsException()
        {
            CreateReceipt request = new CreateReceipt
            {
                Order = new OrderEntity(1)
                {
                    CheckoutMethodId = 69
                }
            };
            Assert.Throws<InvalidOperationException>(() => this.useCase.Execute(request));
            this.receiptRepository.DidNotReceive().CreateReceipt(Arg.Any<ReceiptEntity>());
        }

        [Test]
        public void AllOke_PaymentProcesserInfoOnPaymentProvider()
        {
            CreateReceipt request = new CreateReceipt
            {
                Order = new OrderEntity(1)
                {
                    CheckoutMethodId = 1,
                    CompanyId = 1,
                    OutletId = 1
                }
            };
            this.useCase.Execute(request);
            this.receiptRepository.Received(1).CreateReceipt(Arg.Is<ReceiptEntity>(receiptEntity => receiptEntity.PaymentProcessorInfo.Equals(PaymentProvider.PaymentProcessorInfo, StringComparison.InvariantCultureIgnoreCase)));
        }

        [Test]
        public void AllOke_PaymentProcesserInfoOverwriteOnPaymentIntegrationConfiguration()
        {
            CreateReceipt request = new CreateReceipt
            {
                Order = new OrderEntity(1)
                {
                    CheckoutMethodId = 2,
                    CompanyId = 1,
                    OutletId = 1
                }
            };
            this.useCase.Execute(request);
            this.receiptRepository.Received(1).CreateReceipt(Arg.Is<ReceiptEntity>(receiptEntity => receiptEntity.PaymentProcessorInfo.Equals(ExistingPaymentIntegrationConfigurationWithPaymentProcessorInfo.PaymentProcessorInfo, StringComparison.InvariantCultureIgnoreCase)));
        }
    }
}