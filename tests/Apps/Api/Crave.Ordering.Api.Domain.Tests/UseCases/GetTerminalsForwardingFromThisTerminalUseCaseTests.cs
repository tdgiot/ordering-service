﻿using System.Collections.Generic;
using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Api.Domain.UseCases;
using Crave.Ordering.Api.Domain.UseCases.Requests;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Tests;
using NSubstitute;
using NUnit.Framework;

namespace Crave.Ordering.Api.Domain.Tests.UseCases
{
    [TestFixture]
    public class GetTerminalsForwardingFromThisTerminalUseCaseTests : TestBase
    {
        private ITerminalRepository terminalRepository;
        private GetTerminalsForwardingFromThisTerminalUseCase useCase;

        [OneTimeSetUp]
        public void _OneTimeSetUp()
        {
            this.terminalRepository = this.CreateSubstitute<ITerminalRepository>();
            this.terminalRepository.GetTerminal(1).Returns(new TerminalEntity
            {
                TerminalId = 1,
                ForwardToTerminalId = 2
            });
            this.terminalRepository.GetTerminal(2).Returns(new TerminalEntity
            {
                TerminalId = 2
            });

            this.useCase = new GetTerminalsForwardingFromThisTerminalUseCase(this.terminalRepository);
        }

        [Test]
        public void TerminalIsForwarded_Once()
        {
            GetTerminalsForwardingFromThisTerminal request = new GetTerminalsForwardingFromThisTerminal
            {
                TerminalEntity = new TerminalEntity
                {
                    ForwardToTerminalId = 2
                }
            };
            List<TerminalEntity> forwardingTerminals = this.useCase.Execute(request);

            this.terminalRepository.Received(1).GetTerminal(2);
            Assert.NotNull(forwardingTerminals.Find(x => x.TerminalId == 2));
        }

        [Test]
        public void TerminalIsForwarded_Multiple()
        {
            GetTerminalsForwardingFromThisTerminal request = new GetTerminalsForwardingFromThisTerminal
            {
                TerminalEntity = new TerminalEntity
                {
                    ForwardToTerminalId = 1
                }
            };
            List<TerminalEntity> forwardingTerminals = this.useCase.Execute(request);

            this.terminalRepository.Received(2).GetTerminal(Arg.Any<int>());
            Assert.AreEqual(2, forwardingTerminals.Count);
        }
    }
}