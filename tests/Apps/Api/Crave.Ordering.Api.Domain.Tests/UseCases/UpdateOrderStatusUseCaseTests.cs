﻿using System.Threading.Tasks;
using Crave.Enums;
using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Api.Domain.Interfaces.UseCases;
using Crave.Ordering.Api.Domain.Tests.Builders.RepositoryBuilders;
using Crave.Ordering.Api.Domain.Tests.Builders.UseCaseBuilders;
using Crave.Ordering.Api.Domain.UseCases;
using Crave.Ordering.Api.Domain.UseCases.Requests;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.Tests.Builders;
using Moq;
using NUnit.Framework;

namespace Crave.Ordering.Api.Domain.Tests.UseCases
{
    [TestFixture]
    public class UpdateOrderStatusUseCaseTests
    {
        public static class A
        {
            public static OrderRepositoryMockBuilder OrderRepository => new OrderRepositoryMockBuilder();
            public static OrderEntityBuilder OrderEntity => new OrderEntityBuilder();
            public static SaveOrderUseCaseBuilder SaveOrderUseCase => new SaveOrderUseCaseBuilder();
        }

        [Test]
        public void UpdateOrderStatus_NonExistingOrder()
        {
            // Arrange
            IOrderRepository orderRepo = A.OrderRepository.Build();

            // Act
            UpdateOrderStatusUseCase useCase = new UpdateOrderStatusUseCase(orderRepo, A.SaveOrderUseCase.Build().Object);

            // Assert
            Assert.ThrowsAsync<CraveException>(() => useCase.ExecuteAsync(new UpdateOrderStatus(42, OrderStatus.InRoute)));
        }

        [Test]
        public async Task UpdateOrderStatus_Processed()
        {
            // Arrange
            IOrderRepository orderRepo = A.OrderRepository
                .W(A.OrderEntity.Build())
                .Build();

            Mock<ISaveOrderUseCase> saveOrderUseCase = A.SaveOrderUseCase.Build();
            UpdateOrderStatusUseCase useCase = new UpdateOrderStatusUseCase(orderRepo, saveOrderUseCase.Object);

            // Assert
            bool result = await useCase.ExecuteAsync(new UpdateOrderStatus(1, OrderStatus.Processed));

            Assert.That(result, Is.True);
            saveOrderUseCase.Verify(uc => uc.ExecuteAsync(It.Is<OrderEntity>(entity => entity.ProcessedUTC.HasValue)));
        }

        [Test]
        public async Task UpdateOrderStatus_WithErrorCode()
        {
            // Arrange
            IOrderRepository orderRepo = A.OrderRepository
                .W(A.OrderEntity.Build())
                .Build();

            Mock<ISaveOrderUseCase> saveOrderUseCase = A.SaveOrderUseCase.Build();

            UpdateOrderStatus status = new UpdateOrderStatus(1, OrderStatus.Processed);
            status.ErrorCode = OrderErrorCode.TimedOut;

            // Act
            UpdateOrderStatusUseCase useCase = new UpdateOrderStatusUseCase(orderRepo, saveOrderUseCase.Object);

            // Assert
            bool result = await useCase.ExecuteAsync(status);

            Assert.That(result, Is.True);
            saveOrderUseCase.Verify(uc => uc.ExecuteAsync(It.Is<OrderEntity>(entity => entity.ErrorCode == OrderErrorCode.TimedOut)));
        }
    }
}
