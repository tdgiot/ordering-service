﻿using Crave.Ordering.Api.Domain.Interfaces.UseCases;
using Crave.Ordering.Api.Domain.Tests.Builders.RepositoryBuilders;
using Crave.Ordering.Api.Domain.Tests.Builders.UseCaseBuilders;
using Crave.Ordering.Api.Domain.UseCases.Responses;
using Crave.Ordering.Shared.Data.LLBLGen.Tests.Builders;
using NUnit.Framework;
using System.Threading.Tasks;

namespace Crave.Ordering.Api.Domain.Tests.UseCases
{
	public class GetOrderUseCaseTest
	{
		private static class A
		{
			public static GetOrderUseCaseBuilder GetOrderUseCase => new GetOrderUseCaseBuilder();
			public static OrderRepositoryMockBuilder OrderRepositoryMock => new OrderRepositoryMockBuilder();
			public static OrderEntityBuilder OrderEntity => new OrderEntityBuilder();
		}

		[Test]
		public async Task ExecuteAsync_WithGuidOfExsistingOrder_ReturnsOrderWithCorrectCurrencyCode()
		{
			// Arrange
			const string currencyCode = "GBP";
			IGetOrderUseCase sut = A.GetOrderUseCase
				.W(A.OrderRepositoryMock
					.W(A.OrderEntity
						.WithCurrencyCode(currencyCode))
					)
				.Build();

			const string guid = "{A507F2D7-07DF-49BD-A680-1566177950A8}";

			// Act
			Order? result = await sut.ExecuteAsync(guid);

			// Assert
			Assert.NotNull(result);
			Assert.AreEqual(currencyCode, result?.CurrencyCode);
		}
	}
}
