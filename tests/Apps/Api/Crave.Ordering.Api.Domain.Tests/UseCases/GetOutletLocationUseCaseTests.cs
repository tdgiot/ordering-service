﻿using System;
using System.Threading.Tasks;
using Crave.Libraries.Distance.Interfaces;
using Crave.Libraries.Distance.Models;
using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Api.Domain.UseCases;
using Crave.Ordering.Api.Domain.UseCases.Requests;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Tests;
using NSubstitute;
using NUnit.Framework;

namespace Crave.Ordering.Api.Domain.Tests.UseCases
{
    [TestFixture]
    public class GetOutletLocationUseCaseTests : TestBase
    {
        private ICompanyRepository companyRepository;

        private GetOutletLocationUseCase useCase;

        [OneTimeSetUp]
        public void _OneTimeSetUp()
        {
            this.companyRepository = this.CreateSubstitute<ICompanyRepository>();
            this.companyRepository.GetCompanyLocationForOutletAsync(1).Returns(new CompanyEntity
            {
                IsNew = false,
                Longitude = 50.1,
                Latitude = 50.2
            });
            this.companyRepository.GetCompanyLocationForOutletAsync(2).Returns(new CompanyEntity
            {
                IsNew = false,
                Addressline1 = "Warmoezenierstraat 5",
                City = "Naaldwijk",
                Zipcode = "2671ZP",
                CountryCode = "NLD"
            });

            this.useCase = new GetOutletLocationUseCase(this.companyRepository);
        }

        [Test]
        public void TestExecute_InvalidCompany_ThrowsException()
        {
            GetOutletLocationRequest request = new GetOutletLocationRequest(999);

            Assert.ThrowsAsync<InvalidOperationException>(() => this.useCase.ExecuteAsync(request));
        }

        [Test]
        public async Task TestExecuteAsync_ValidCompany_Coordinates()
        {
            GetOutletLocationRequest request = new GetOutletLocationRequest(1);

            ILocation location = await this.useCase.ExecuteAsync(request);

            Assert.IsInstanceOf<Coordinates>(location);
            Assert.AreEqual("50.2,50.1", location.ToSearchString());
        }

        [Test]
        public async Task TestExecuteAsync_ValidCompany_Address()
        {
            GetOutletLocationRequest request = new GetOutletLocationRequest(2);

            ILocation location = await this.useCase.ExecuteAsync(request);

            Assert.IsInstanceOf<Address>(location);
            Assert.AreEqual("Warmoezenierstraat 5,Naaldwijk,2671ZP,Netherlands", location.ToSearchString());
        }
    }
}
