﻿using System;
using Crave.Ordering.Api.Domain.Extensions;
using Crave.Ordering.Api.Domain.Tests.Builders;
using Crave.Ordering.Api.Domain.Tests.Builders.EntityBuilders;
using Crave.Ordering.Api.Domain.Tests.Builders.ValidationBuilders;
using Crave.Ordering.Api.Domain.Validation;
using Crave.Ordering.Shared.Data.LLBLGen.Tests.Builders;
using NodaTime;
using NUnit.Framework;

namespace Crave.Ordering.Api.Domain.Tests.UseCases
{
    public class ScheduleAvailabilityValidatorTest
	{
		[Test]
		public void Execute_WithCurrentTimeWithinScheduleDayAndTime_ReturnsAvailable()
		{
			// Arrange
			var timeZoneOlsonId = A.TimeZoneOlsonId.Build();
			var currentDateTime = DateTime.UtcNow.ConvertToZonedDateTime(timeZoneOlsonId);

			var timeStart = currentDateTime.TimeOfDay.Minus(Period.FromMinutes(10));
			var timeEnd = currentDateTime.TimeOfDay.Plus(Period.FromMinutes(10));

			var scheduleEntity = A.ScheduleEntity
				.W(A.ScheduleitemEntity
					.W(currentDateTime.DayOfWeek)
					.W(timeStart, timeEnd))
				.Build();

			// Act
			var isValid = ScheduleAvailabilityValidator.IsAvailable(scheduleEntity, timeZoneOlsonId);

			// Assert
			Assert.IsTrue(isValid);
		}

		[Test]
		public void Execute_WithCurrentTimeOutsideScheduleDayAndTime_ReturnsUnavailable()
		{
			// Arrange
			var timeZoneOlsonId = A.TimeZoneOlsonId.Build();
			var currentDateTime = DateTime.UtcNow.ConvertToZonedDateTime(timeZoneOlsonId);

			var timeStart = currentDateTime.TimeOfDay.Minus(Period.FromMinutes(20));
			var timeEnd = currentDateTime.TimeOfDay.Plus(Period.FromMinutes(-10));

			var scheduleEntity = A.ScheduleEntity
				.W(A.ScheduleitemEntity
					.W(currentDateTime.DayOfWeek)
					.W(timeStart, timeEnd))
				.Build();

			// Act
			var isValid = ScheduleAvailabilityValidator.IsAvailable(scheduleEntity, timeZoneOlsonId);

			// Assert
			Assert.IsFalse(isValid);
		}

		[Test]
		public void Execute_WithScheduleWithoutDayOfWeekAndCurrentTimeInsideScheduleDayAndTime_ReturnsAvailable()
		{
			// Arrange
			var timeZoneOlsonId = A.TimeZoneOlsonId.Build();
			var currentDateTime = DateTime.UtcNow.ConvertToZonedDateTime(timeZoneOlsonId);

			var timeStart = currentDateTime.TimeOfDay.Minus(Period.FromMinutes(20));
			var timeEnd = currentDateTime.TimeOfDay.Plus(Period.FromMinutes(20));

			var scheduleEntity = A.ScheduleEntity
				.W(A.ScheduleitemEntity
					.W(currentDateTime.DayOfWeek)
					.W(timeStart, timeEnd))
				.Build();

			// Act
			var isValid = ScheduleAvailabilityValidator.IsAvailable(scheduleEntity, timeZoneOlsonId);

			// Assert
			Assert.IsTrue(isValid);
		}

		[Test]
		public void Execute_WithScheduleOnlyAvailableForOneMinuteOnTheCurrentTime_ReturnsAvailable()
		{
			// Arrange
			var timeZoneOlsonId = A.TimeZoneOlsonId.Build();
			var currentDateTime = DateTime.UtcNow.ConvertToZonedDateTime(timeZoneOlsonId);

			var timeStart = currentDateTime.TimeOfDay;
			var timeEnd = currentDateTime.TimeOfDay;

			var scheduleEntity = A.ScheduleEntity
				.W(A.ScheduleitemEntity
					.W(currentDateTime.DayOfWeek)
					.W(timeStart, timeEnd))
				.Build();

			// Act
			var isValid = ScheduleAvailabilityValidator.IsAvailable(scheduleEntity, timeZoneOlsonId);

			// Assert
			Assert.IsTrue(isValid);
		}

		private static class A
		{
			public static TimeZoneOlsonIdBuilder TimeZoneOlsonId => new TimeZoneOlsonIdBuilder();
			public static ScheduleEntityBuilder ScheduleEntity => new ScheduleEntityBuilder();
			public static ScheduleitemEntityBuilder ScheduleitemEntity => new ScheduleitemEntityBuilder();
		}
	}
}
