﻿using Crave.Ordering.Api.Domain.UseCases;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Tests;
using NUnit.Framework;

namespace Crave.Ordering.Api.Domain.Tests.UseCases
{
    [TestFixture]
    public class EnrichOrderWithOrderContextUseCaseTests : TestBase
    {

        [Test]
        public void OrderWithNotesAndOrderContext_WhitespaceBetweenPreviousOrderNotesAndOrderContext()
        {
            OrderEntity order = new OrderEntity();
            order.Notes = "Hello, world!";

            string orderContext = "TimeSlot: 10:00 - 10:30";
            string expected = "Hello, world!\r\n\r\nTimeSlot: 10:00 - 10:30";

            EnrichOrderWithOrderContextUseCase.Execute(order, orderContext);

            Assert.AreEqual(expected, order.Notes);
        }

        [Test]
        public void OrderContextWithHtml_HtmlTagsAreRemoved()
        {
            OrderEntity order = new OrderEntity();

            string orderContext = "TimeSlot: 10:00 - 10:30 // <script>hackzorsss</script>";
            string expected = "TimeSlot: 10:00 - 10:30 // hackzorsss";

            EnrichOrderWithOrderContextUseCase.Execute(order, orderContext);

            Assert.AreEqual(expected, order.Notes);
        }
    }
}
