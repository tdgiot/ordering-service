﻿using Crave.Ordering.Api.Core.Enums;
using Crave.Ordering.Api.Domain.Interfaces.UseCases;
using Crave.Ordering.Api.Domain.Tests.Builders;
using Crave.Ordering.Api.Domain.Tests.Builders.RepositoryBuilders;
using Crave.Ordering.Api.Domain.Tests.Builders.UseCaseBuilders;
using Crave.Ordering.Api.Domain.UseCases;
using Crave.Ordering.Api.Domain.UseCases.Requests;
using Crave.Ordering.Api.Domain.UseCases.Responses;
using Crave.Ordering.Api.Domain.Validation.Errors;
using Crave.Ordering.Shared.Data.LLBLGen.Tests.Builders;
using Microsoft.Extensions.Logging;
using Moq;
using NUnit.Framework;
using System.Threading.Tasks;

namespace Crave.Ordering.Api.Domain.Tests.UseCases
{
    [TestFixture]
    public class CreateAndSaveOrderUseCaseTests
    {
        public static class A
        {
            public static ILogger<ICreateAndSaveOrderUseCase> Logger => Mock.Of<ILogger<ICreateAndSaveOrderUseCase>>();
            public static ValidateOrderUseCaseBuilder ValidateOrderUseCase => new ValidateOrderUseCaseBuilder();
            public static ValidateOrderResponseBuilder ValidateOrderResponse => new ValidateOrderResponseBuilder();
            public static ValidationResponseBuilder ValidationResponse => new ValidationResponseBuilder();
            public static AnalyticsProcessingTaskRepositoryMockBuilder AnalyticsProcessingTaskRepository => new AnalyticsProcessingTaskRepositoryMockBuilder();
            public static SaveOrderUseCaseBuilder SaveOrderUseCase => new SaveOrderUseCaseBuilder();
            public static GetOrderNotesUseCaseMockBuilder GetOrderNotesUseCaseMock => new GetOrderNotesUseCaseMockBuilder();
            public static OrderEntityBuilder OrderEntity => new OrderEntityBuilder();
            public static CreateOrderRouteUseCaseMockBuilder CreateOrderRouteUseCase => new CreateOrderRouteUseCaseMockBuilder();
        }

        [Test]
        public async Task OrderValidationHasErrors_ReturnFailureWithValidationResponse()
        {
            // Arrange
            Mock<IValidateOrderUseCase> validateOrderUseCase = A.ValidateOrderUseCase
                .WithResponse(A.ValidateOrderResponse
                    .WithValidationResponse(A.ValidationResponse
                        .WithError(new ValidationError(ValidationErrorSubType.CompanyInvalid))))
                .Build();

            CreateAndSaveOrderUseCase sut = new CreateAndSaveOrderUseCase(A.Logger, null, validateOrderUseCase.Object, null, null, null);

            // Act
            CreateAndSaveOrderResponse response = await sut.ExecuteAsync(new CreateOrderRequest());

            // Assert
            Assert.That(response.Success, Is.False);
            Assert.That(response.ValidationResponse, Is.Not.Null);
        }

        [Test]
        public async Task SaveOrderThrowsCheckoutValidationException_ReturnsFailureWithValidationResponse()
        {
            // Arrange
            Mock<ISaveOrderUseCase> saveOrderUseCase = A.SaveOrderUseCase
                .WithCheckoutException()
                .Build();

            CreateAndSaveOrderUseCase sut = new CreateAndSaveOrderUseCase(A.Logger,
                null, 
                A.ValidateOrderUseCase.Build().Object, 
                A.GetOrderNotesUseCaseMock.Build(), 
                saveOrderUseCase.Object,
                A.CreateOrderRouteUseCase.Build().Object);

            // Act
            CreateAndSaveOrderResponse response = await sut.ExecuteAsync(new CreateOrderRequest());

            // Assert
            Assert.That(response.Success, Is.False);
            Assert.That(response.ValidationResponse, Is.Not.Null);
        }

        [Test]
        public async Task OrderSuccessfullySaved_ReturnsOkWithOrderGuid()
        {
            // Arrange
            Mock<ISaveOrderUseCase> saveOrderUseCase = A.SaveOrderUseCase
                .WithResponse(A.OrderEntity.WithGuid("abc-def"))
                .Build();

            CreateAndSaveOrderUseCase sut = new CreateAndSaveOrderUseCase(A.Logger, 
                A.AnalyticsProcessingTaskRepository.Build().Object,
                A.ValidateOrderUseCase.Build().Object,
                A.GetOrderNotesUseCaseMock.Build(),
                saveOrderUseCase.Object,
                A.CreateOrderRouteUseCase.Build().Object);

            // Act
            CreateAndSaveOrderResponse response = await sut.ExecuteAsync(new CreateOrderRequest());

            // Assert
            Assert.That(response.Success, Is.True);
            Assert.That(response.OrderGuid, Is.EqualTo("abc-def"));
        }
    }
}