﻿using Crave.Enums;
using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Api.Domain.Interfaces.UseCases;
using Crave.Ordering.Api.Domain.Tests.Builders.RepositoryBuilders;
using Crave.Ordering.Api.Domain.Tests.Builders.UseCaseBuilders;
using Crave.Ordering.Api.Domain.UseCases;
using Crave.Ordering.Api.Domain.UseCases.Requests;
using Crave.Ordering.Shared.Data.LLBLGen.Tests.Builders;
using Crave.Ordering.Tests;
using Moq;
using NUnit.Framework;
using System.Threading.Tasks;

namespace Crave.Ordering.Api.Domain.Tests.UseCases
{
    [TestFixture]
    public class CancelRouteUseCaseTests : TestBase
    {
        public static class A
        {
            public static OrderRepositoryMockBuilder OrderRepository => new OrderRepositoryMockBuilder();
            public static UpdateOrderRoutestephandlerStatusesUseCaseBuilder UpdateOrderRoutestephandlerStatusesUseCase => new UpdateOrderRoutestephandlerStatusesUseCaseBuilder();
            public static OrderEntityBuilder OrderEntity => new OrderEntityBuilder();

        }

        [TestCase(0, 0, "")]
        [TestCase(0, 0, null)]
        [TestCase(-1, -1, " ")]
        public void WithoutRequestParameters_Throws(int clientId, int customerId, string orderGuid)
        {
            // Arrange
            CancelRoutes request = new CancelRoutes
            {
                ClientId = clientId,
                CustomerId = customerId,
                OrderGuid = orderGuid
            };

            CancelRoutesUseCase useCase = new CancelRoutesUseCase(A.OrderRepository.Build(), A.UpdateOrderRoutestephandlerStatusesUseCase.Build().Object);

            Assert.ThrowsAsync<CraveException>(() => useCase.ExecuteAsync(request));
        }

        [Test]
        public async Task CancelOrder()
        {
            CancelRoutes request = new CancelRoutes
            {
                ClientId = 1,
            };

            IOrderRepository orderRepository = A.OrderRepository
                .W(A.OrderEntity.W((OrderId) 1337))
                .Build();

            Mock<IUpdateOrderRoutestephandlerStatusesUseCase> updateOrderRoutestephandlerStatusesUseCase = A.UpdateOrderRoutestephandlerStatusesUseCase.Build();

            CancelRoutesUseCase useCase = new CancelRoutesUseCase(orderRepository, updateOrderRoutestephandlerStatusesUseCase.Object);
            bool result = await useCase.ExecuteAsync(request);

            Assert.That(result, Is.True);

            updateOrderRoutestephandlerStatusesUseCase
                .Verify(uc => uc.ExecuteAsync(It.Is<UpdateOrderRoutestephandlerStatuses>(r => r.OrderEntity.OrderId == 1337 && r.Status == OrderStatus.Processed)));
        }
    }
}
