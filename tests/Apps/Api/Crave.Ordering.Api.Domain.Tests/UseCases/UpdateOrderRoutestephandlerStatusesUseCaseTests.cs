﻿using Crave.Enums;
using Crave.Ordering.Api.Domain.Interfaces.UseCases;
using Crave.Ordering.Api.Domain.Tests.Builders.UseCaseBuilders;
using Crave.Ordering.Api.Domain.UseCases;
using Crave.Ordering.Api.Domain.UseCases.Requests;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.Tests.Builders;
using Moq;
using NUnit.Framework;
using System.Threading.Tasks;

namespace Crave.Ordering.Api.Domain.Tests.UseCases
{
    [TestFixture]
    public class UpdateOrderRoutestephandlerStatusesUseCaseTests
    {
        public static class A
        {
            public static UpdateOrderStatusUseCaseBuilder UpdateOrderStatusUseCase => new UpdateOrderStatusUseCaseBuilder();
            public static UpdateOrderRoutestephandlerUseCaseBuilder UpdateOrderRoutestephandlerUseCase => new UpdateOrderRoutestephandlerUseCaseBuilder();
            public static OrderEntityBuilder OrderEntity => new OrderEntityBuilder();
        }
        
        [Test]
        public async Task WithoutLinkedOrderEntity()
        {
            // Arrange
            UpdateOrderRoutestephandlerStatuses request = new UpdateOrderRoutestephandlerStatuses
            {
                Status = OrderStatus.Processed,
                OrderEntity = A.OrderEntity.AsNew().Build()
            };

            Mock<IUpdateOrderStatusUseCase> updateOrderStatus = A.UpdateOrderStatusUseCase.Build();
            Mock<IUpdateOrderRoutestephandlerUseCase> updateOrderRoutestephandler = A.UpdateOrderRoutestephandlerUseCase.Build();

            // Act
            UpdateOrderRoutestephandlerStatusesUseCase useCase = new UpdateOrderRoutestephandlerStatusesUseCase(updateOrderStatus.Object, updateOrderRoutestephandler.Object);
            bool result = await useCase.ExecuteAsync(request);

            // Assert
            Assert.That(result, Is.False);
        }

        [TestCase(OrderStatus.InRoute, ExpectedResult = false)]
        [TestCase(OrderStatus.NotRouted, ExpectedResult = false)]
        [TestCase(OrderStatus.Routed, ExpectedResult = false)]
        [TestCase(OrderStatus.Unprocessable, ExpectedResult = false)]
        [TestCase(OrderStatus.Processed, ExpectedResult = true)]
        public async Task<bool> OrderStatuses(OrderStatus status)
        {
            // Arrange
            UpdateOrderRoutestephandlerStatuses request = new UpdateOrderRoutestephandlerStatuses
            {
                Status = status,
                OrderEntity = A.OrderEntity.Build()
            };

            Mock<IUpdateOrderStatusUseCase> updateOrderStatus = A.UpdateOrderStatusUseCase.Build();
            Mock<IUpdateOrderRoutestephandlerUseCase> updateOrderRoutestephandler = A.UpdateOrderRoutestephandlerUseCase.Build();

            // Act
            UpdateOrderRoutestephandlerStatusesUseCase useCase = new UpdateOrderRoutestephandlerStatusesUseCase(updateOrderStatus.Object, updateOrderRoutestephandler.Object);
            bool result = await useCase.ExecuteAsync(request);

            return result;
        }

        [Test]
        public async Task WithoutRoutestephandlers()
        {
            // Arrange
            UpdateOrderRoutestephandlerStatuses request = new UpdateOrderRoutestephandlerStatuses
            {
                Status = OrderStatus.Processed,
                OrderEntity = A.OrderEntity.Build()
            };

            Mock<IUpdateOrderStatusUseCase> updateOrderStatus = A.UpdateOrderStatusUseCase.Build();
            Mock<IUpdateOrderRoutestephandlerUseCase> updateOrderRoutestephandler = A.UpdateOrderRoutestephandlerUseCase.Build();

            // Act
            UpdateOrderRoutestephandlerStatusesUseCase useCase = new UpdateOrderRoutestephandlerStatusesUseCase(updateOrderStatus.Object, updateOrderRoutestephandler.Object);
            bool result = await useCase.ExecuteAsync(request);

            // Assert
            Assert.That(result, Is.True);
            updateOrderStatus.Verify(uc => uc.ExecuteAsync(It.Is<UpdateOrderStatus>(r => r.OrderId == 1 && r.Status == OrderStatus.Processed)));
        }

        [TestCase(OrderRoutestephandlerStatus.Completed)]
        [TestCase(OrderRoutestephandlerStatus.Failed)]
        public async Task WithRoutestephandler_OnlyWhenFailed(OrderRoutestephandlerStatus status)
        {
            // Arrange
            OrderEntity orderEntity = new OrderEntity
            {
                IsNew = false,
                Status = OrderStatus.Processed,
                OrderRoutestephandlerCollection =
                                          {
                                              new OrderRoutestephandlerEntity
                                              {
                                                  Status = status
                                              }
                                          }
            };

            UpdateOrderRoutestephandlerStatuses request = new UpdateOrderRoutestephandlerStatuses
            {
                OnlyWhenFailed = true,
                Status = OrderStatus.Processed,
                OrderEntity = orderEntity
            };

            Mock<IUpdateOrderStatusUseCase> updateOrderStatus = A.UpdateOrderStatusUseCase.Build();
            Mock<IUpdateOrderRoutestephandlerUseCase> updateOrderRoutestephandler = A.UpdateOrderRoutestephandlerUseCase.Build();

            // Act
            UpdateOrderRoutestephandlerStatusesUseCase useCase = new UpdateOrderRoutestephandlerStatusesUseCase(updateOrderStatus.Object, updateOrderRoutestephandler.Object);
            bool result = await useCase.ExecuteAsync(request);


            // Asset
            Assert.That(result, Is.True);

            if (status == OrderRoutestephandlerStatus.Failed)
            {
                updateOrderRoutestephandler.Verify(uc => uc.ExecuteAsync(It.IsAny<OrderRoutestephandlerEntity>()), Times.Once);
            }
            else
            {
                updateOrderRoutestephandler.Verify(uc => uc.ExecuteAsync(It.IsAny<OrderRoutestephandlerEntity>()), Times.Never);
            }
        }

        [Test]
        public void TerminalIsNotOwnerAndNotMaster()
        {
            Assert.ThrowsAsync<CraveException>(() => this.UpdateStatusForTerminal(1, 2, false));
        }

        [Test]
        public async Task TerminalIsNotOwnerButIsMaster()
        {
            bool result = await this.UpdateStatusForTerminal(1, 2, true);
            Assert.That(result, Is.True);
        }

        [Test]
        public async Task TerminalIsOwnerButIsNotMaster()
        {
            bool result = await this.UpdateStatusForTerminal(1, 1, false);
            Assert.That(result, Is.True);
        }

        private async Task<bool> UpdateStatusForTerminal(int routestephandlerTerminalId, int terminalId, bool terminalIsMaster)
        {
            OrderEntity orderEntity = new OrderEntity
            {
                IsNew = false,
                OrderRoutestephandlerCollection =
                                          {
                                              new OrderRoutestephandlerEntity
                                              {
                                                  TerminalId = routestephandlerTerminalId
                                              }
                                          }
            };

            UpdateOrderRoutestephandlerStatuses request = new UpdateOrderRoutestephandlerStatuses
            {
                Status = OrderStatus.Processed,
                OrderEntity = orderEntity,
                TerminalEntity = new TerminalEntity
                {
                    TerminalId = terminalId,
                    MasterTab = terminalIsMaster
                }
            };

            Mock<IUpdateOrderStatusUseCase> updateOrderStatus = A.UpdateOrderStatusUseCase.Build();
            Mock<IUpdateOrderRoutestephandlerUseCase> updateOrderRoutestephandler = A.UpdateOrderRoutestephandlerUseCase.Build();

            // Act
            UpdateOrderRoutestephandlerStatusesUseCase useCase = new UpdateOrderRoutestephandlerStatusesUseCase(updateOrderStatus.Object, updateOrderRoutestephandler.Object);
            bool result = await useCase.ExecuteAsync(request);

            return result;
        }
    }
}