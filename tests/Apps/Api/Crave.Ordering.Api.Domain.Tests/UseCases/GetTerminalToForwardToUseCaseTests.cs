﻿using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Api.Domain.UseCases;
using Crave.Ordering.Api.Domain.UseCases.Requests;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Tests;
using NSubstitute;
using NUnit.Framework;

namespace Crave.Ordering.Api.Domain.Tests.UseCases
{
    [TestFixture]
    public class GetTerminalToForwardToUseCaseTests : TestBase
    {
        private ITerminalRepository terminalRepository;
        private GetTerminalsForwardingFromThisTerminalUseCase terminalsForwardingUseCase;
        private GetTerminalToForwardToUseCase useCase;

        [OneTimeSetUp]
        public void _OneTimeSetUp()
        {
            this.terminalRepository = this.CreateSubstitute<ITerminalRepository>();
            this.terminalRepository.GetTerminal(1).Returns(new TerminalEntity
            {
                TerminalId = 1,
                ForwardToTerminalId = 2
            });
            this.terminalRepository.GetTerminal(2).Returns(new TerminalEntity
            {
                TerminalId = 2,
                ForwardToTerminalId = 3
            });
            this.terminalRepository.GetTerminal(3).Returns(new TerminalEntity
            {
                TerminalId = 3
            });

            this.terminalsForwardingUseCase = new GetTerminalsForwardingFromThisTerminalUseCase(this.terminalRepository);

            this.useCase = new GetTerminalToForwardToUseCase(this.terminalRepository, this.terminalsForwardingUseCase);
        }

        [TestCase(1, true)] // Forwarded twice
        [TestCase(2, true)] // Forwarded once
        [TestCase(3, false)] // No forward
        public void GetForwardedTerminal(int baseTerminalId, bool isForwarded)
        {
            TerminalEntity terminal = this.useCase.Execute(new GetTerminalToForwardTo
            {
                TerminalId = baseTerminalId
            });
            if (isForwarded)
            {
                Assert.AreEqual(3, terminal?.TerminalId);
            }
            else
            {
                Assert.IsNull(terminal);
            }
        }
    }
}