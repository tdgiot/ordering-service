﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Crave.Enums;
using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Api.Domain.Interfaces.UseCases;
using Crave.Ordering.Api.Domain.Tests.Builders.RepositoryBuilders;
using Crave.Ordering.Api.Domain.Tests.Builders.UseCaseBuilders;
using Crave.Ordering.Api.Domain.UseCases;
using Crave.Ordering.Api.Domain.UseCases.Requests;
using Crave.Ordering.Api.Domain.UseCases.Responses;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.Tests.Builders;
using Microsoft.Extensions.Logging;
using Moq;
using NUnit.Framework;

namespace Crave.Ordering.Api.Domain.Tests.UseCases
{
    [TestFixture]
    public class UpdateOrderStatusByGuidUseCaseTests
    {
        public static class A
        {
            public static Mock<ILogger<IUpdateOrderStatusByGuidUseCase>> Logger => new Mock<ILogger<IUpdateOrderStatusByGuidUseCase>>();
            public static OrderRepositoryMockBuilder OrderRepository => new OrderRepositoryMockBuilder();
            public static OrderEntityBuilder OrderEntity => new OrderEntityBuilder();
            public static SaveOrderUseCaseBuilder SaveOrderUseCase => new SaveOrderUseCaseBuilder();
            public static CreateOrderRouteUseCaseMockBuilder CreateOrderRouteUseCase => new CreateOrderRouteUseCaseMockBuilder();
        }

        [Test]
        public async Task NonExistingOrderGuid_ReturnsNotFound()
        {
            // Arrange
            IOrderRepository orderRepository = A.OrderRepository
                .W(A.OrderEntity.AsNew())
                .Build();

            UpdateOrderStatusByGuidUseCase sut = new UpdateOrderStatusByGuidUseCase(A.Logger.Object, orderRepository, A.SaveOrderUseCase.Build().Object, A.CreateOrderRouteUseCase.Build().Object);

            // Act
            ResponseBase response = await sut.ExecuteAsync(new UpdateOrderStatusByGuidRequest("abcd", OrderStatus.Processed));

            // Assert
            Assert.That(response.NotFound, Is.True);
        }

        [Test]
        public async Task OrderStatusIsEqual_ReturnsError()
        {
            // Arrange
            IOrderRepository orderRepository = A.OrderRepository
                .W(A.OrderEntity.W(OrderStatus.InRoute, OrderErrorCode.None))
                .Build();

            UpdateOrderStatusByGuidUseCase sut = new UpdateOrderStatusByGuidUseCase(A.Logger.Object, orderRepository, A.SaveOrderUseCase.Build().Object, A.CreateOrderRouteUseCase.Build().Object);

            // Act
            ResponseBase response = await sut.ExecuteAsync(new UpdateOrderStatusByGuidRequest("abcd", OrderStatus.InRoute));

            // Assert
            Assert.That(response.Success, Is.False);
            Assert.That(response.Message, Contains.Substring("New order status is the same as current"));
        }

        [Test]
        public async Task NewStatusIsLower_ReturnsError()
        {
            // Arrange
            IOrderRepository orderRepository = A.OrderRepository
                .W(A.OrderEntity.W(OrderStatus.InRoute, OrderErrorCode.None))
                .Build();

            UpdateOrderStatusByGuidUseCase sut = new UpdateOrderStatusByGuidUseCase(A.Logger.Object, orderRepository, A.SaveOrderUseCase.Build().Object, A.CreateOrderRouteUseCase.Build().Object);

            // Act
            ResponseBase response = await sut.ExecuteAsync(new UpdateOrderStatusByGuidRequest("abcd", OrderStatus.Routed));

            // Assert
            Assert.That(response.Success, Is.False);
            Assert.That(response.Message, Contains.Substring("Can not downgrade order status"));
        }

        [Test]
        public async Task ProcessedStatusSetsProcessedData_ReturnsSuccess()
        {
            // Arrange
            IOrderRepository orderRepository = A.OrderRepository
                .W(A.OrderEntity.W(OrderStatus.Routed, OrderErrorCode.None))
                .Build();

            Mock<ISaveOrderUseCase> saveOrderUseCase = A.SaveOrderUseCase.Build();

            UpdateOrderStatusByGuidUseCase sut = new UpdateOrderStatusByGuidUseCase(A.Logger.Object, orderRepository, saveOrderUseCase.Object, A.CreateOrderRouteUseCase.Build().Object);

            // Act
            ResponseBase response = await sut.ExecuteAsync(new UpdateOrderStatusByGuidRequest("abcd", OrderStatus.Processed));

            // Assert
            Assert.That(response.Success, Is.True, response.Message);
            
            saveOrderUseCase.Verify(uc => uc.ExecuteAsync(It.Is<OrderEntity>(e => e.ProcessedUTC.HasValue)), Times.Once);
            saveOrderUseCase.Verify(uc => uc.ExecuteAsync(It.Is<OrderEntity>(e => e.Status == OrderStatus.Processed)), Times.Once);
        }

        [TestCaseSource(nameof(GetOrderStatuses))]
        public async Task OnlyPaidStatusStartRoute_ReturnsSuccess(OrderStatus newOrderStatus)
        {
            if (newOrderStatus == OrderStatus.NotRouted)
            {
                return;
            }

            // Arrange
            IOrderRepository orderRepository = A.OrderRepository
                .W(A.OrderEntity.W(OrderStatus.NotRouted, OrderErrorCode.None))
                .Build();

            Mock<ICreateOrderRouteUseCase> createOrderRouteUseCase = A.CreateOrderRouteUseCase.Build();
            
            UpdateOrderStatusByGuidUseCase sut = new UpdateOrderStatusByGuidUseCase(A.Logger.Object, orderRepository, A.SaveOrderUseCase.Build().Object, createOrderRouteUseCase.Object);

            // Act
            ResponseBase response = await sut.ExecuteAsync(new UpdateOrderStatusByGuidRequest("abcd", newOrderStatus));
            Thread.Sleep(50);

            // Assert
            Assert.That(response.Success, Is.True, response.Message);

            Times t = newOrderStatus == OrderStatus.Paid ? Times.Once() : Times.Never();
            createOrderRouteUseCase.Verify(uc => uc.ExecuteAsync(It.IsAny<CreateOrderRoute>()), t);
        }

        public static Array GetOrderStatuses()
        {
            return Enum.GetValues(typeof(OrderStatus));
        }
    }
}