﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Crave.Enums;
using Crave.Libraries.Distance.Enums;
using Crave.Libraries.Distance.Models;
using Crave.Libraries.Distance.Responses;
using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Api.Domain.Enums;
using Crave.Ordering.Api.Domain.Models;
using Crave.Ordering.Api.Domain.UseCases;
using Crave.Ordering.Api.Domain.UseCases.Requests;
using Crave.Ordering.Api.Domain.UseCases.Responses;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Tests;
using NSubstitute;
using NSubstitute.ReturnsExtensions;
using NUnit.Framework;
using DeliveryRateResult = Crave.Ordering.Api.Domain.Enums.DeliveryRateResult;

namespace Crave.Ordering.Api.Domain.Tests.UseCases
{
    [TestFixture]
	public class GetDeliveryRateUseCaseTests : TestBase
	{
		private readonly DeliveryLocation deliveryLocationNlOffice = new DeliveryLocation(4.209951, 52.004972);
		private readonly DeliveryLocation deliveryLocationRestaurantUK = new DeliveryLocation(-0.196466, 51.3242961);

		private readonly IEnumerable<TaxTariffEntity> TaxTariffs = new List<TaxTariffEntity>
		{
			new TaxTariffEntity
			{
				TaxTariffId = 1,
				Percentage = 21d
			}
		};

		private readonly OutletEntity OutletWithTaxIncluded = new OutletEntity
		{
			DeliveryChargeProduct = new ProductEntity
			{
				SubType = ProductSubType.SystemProduct,
				TaxTariffId = 1,
				TaxTariff = new TaxTariffEntity
				{
					Percentage = 21
				}
			},
			Company = new CompanyEntity
            {
				PricesIncludeTaxes = true
            }
		};

		private readonly OutletEntity OutletWithTaxExcluded = new OutletEntity
		{
			DeliveryChargeProduct = new ProductEntity
			{
				SubType = ProductSubType.SystemProduct,
				TaxTariffId = 1,
				TaxTariff = new TaxTariffEntity
                {
					Percentage = 21
                }
            },
			Company = new CompanyEntity
            {
				PricesIncludeTaxes =  false
            }
		};

		private IServiceMethodRepository serviceMethodRepository;
		private GetDeliveryRateUseCase useCase;
		private GetDistanceToOutletUseCase getDistanceToOutletUseCase;

		[OneTimeSetUp]
		public void OneTimeSetUp()
		{
			this.serviceMethodRepository = this.CreateSubstitute<IServiceMethodRepository>();

			this.serviceMethodRepository.GetServiceMethodWithDeliveryDistancesAsync(1).ReturnsNull();
			this.serviceMethodRepository.GetServiceMethodWithDeliveryDistancesAsync(2).Returns(new ServiceMethodEntity());

			this.serviceMethodRepository.GetServiceMethodWithDeliveryDistancesAsync(3).Returns(new ServiceMethodEntity
			{
				OutletId = 1,
				DeliveryDistanceCollection =
				{
					new DeliveryDistanceEntity
					{
						MaximumInMetres = 5000,
						Price = 2
					}
				},
				Outlet = OutletWithTaxIncluded
			});

			this.serviceMethodRepository.GetServiceMethodWithDeliveryDistancesAsync(4).Returns(new ServiceMethodEntity
			{
				OutletId = 2,
				DeliveryDistanceCollection =
				{
					new DeliveryDistanceEntity
					{
						MaximumInMetres = 5000,
						Price = 2
					}
				},
				Outlet = OutletWithTaxIncluded
			});

			this.serviceMethodRepository.GetServiceMethodWithDeliveryDistancesAsync(5).Returns(new ServiceMethodEntity
			{
				OutletId = 3,
				MinimumAmountForFreeDelivery = 9,
				DeliveryDistanceCollection =
				{
					new DeliveryDistanceEntity
					{
						MaximumInMetres = 5000,
						Price = 2
					}
				},
				Outlet = OutletWithTaxIncluded
			});

			this.serviceMethodRepository.GetServiceMethodWithDeliveryDistancesAsync(6).Returns(new ServiceMethodEntity
			{
				OutletId = 3,
				DeliveryDistanceCollection =
				{
					new DeliveryDistanceEntity
					{
						MaximumInMetres = 1000,
						Price = 1.5m
					},
					new DeliveryDistanceEntity
					{
						MaximumInMetres = 2500,
						Price = 2.5m
					}
					,new DeliveryDistanceEntity
					{
						MaximumInMetres = 10000,
						Price = 3.5m
					}
				},
				Outlet = OutletWithTaxIncluded
			});

			this.serviceMethodRepository.GetServiceMethodWithDeliveryDistancesAsync(7).Returns(new ServiceMethodEntity
			{
				OutletId = 4,
				DeliveryDistanceCollection =
				{
					new DeliveryDistanceEntity
					{
						MaximumInMetres = 1000,
						Price = 1.82m,
					},
					new DeliveryDistanceEntity
					{
						MaximumInMetres = 2500,
						Price = 3.03m
					}
					,new DeliveryDistanceEntity
					{
						MaximumInMetres = 10000,
						Price = 4.24m
					}
				},
				Outlet = OutletWithTaxIncluded
			});

			this.serviceMethodRepository.GetServiceMethodWithDeliveryDistancesAsync(8).Returns(new ServiceMethodEntity
			{
				OutletId = 5,
				DeliveryDistanceCollection =
                {
					new DeliveryDistanceEntity
					{
						MaximumInMetres = 1000,
						Price = 1.5m,
					},
					new DeliveryDistanceEntity
					{
						MaximumInMetres = 2500,
						Price = 2.5m,
					}
					,new DeliveryDistanceEntity
					{
						MaximumInMetres = 10000,
						Price = 3.5m
					}
				},
				Outlet = OutletWithTaxExcluded
			});

			this.getDistanceToOutletUseCase = this.CreateSubstitute<GetDistanceToOutletUseCase>();

			this.getDistanceToOutletUseCase.ExecuteAsync(Arg.Is<GetDistanceToOutletRequest>(x => x.OutletId == 1))
				.Returns(new GetDistanceResponse(DistanceResult.NOT_FOUND, "Address not found"));

			this.getDistanceToOutletUseCase.ExecuteAsync(Arg.Is<GetDistanceToOutletRequest>(x => x.OutletId == 2))
				.Returns(new GetDistanceResponse(DistanceResult.OK, new Distance(5500)));

			this.getDistanceToOutletUseCase.ExecuteAsync(Arg.Is<GetDistanceToOutletRequest>(x => x.OutletId == 3))
				.Returns(new GetDistanceResponse(DistanceResult.OK, new Distance(750)));

			this.getDistanceToOutletUseCase.ExecuteAsync(Arg.Is<GetDistanceToOutletRequest>(x => x.OutletId == 4))
				.Returns(new GetDistanceResponse(DistanceResult.OK, new Distance(2000)));

			this.getDistanceToOutletUseCase.ExecuteAsync(Arg.Is<GetDistanceToOutletRequest>(x => x.OutletId == 5))
				.Returns(new GetDistanceResponse(DistanceResult.OK, new Distance(2000)));

			this.useCase = new GetDeliveryRateUseCase(this.getDistanceToOutletUseCase, this.serviceMethodRepository);
		}

		[Test]
		public async Task TestExecuteAsync_InvalidServiceMethod()
		{
			GetDeliveryRateRequest request = new GetDeliveryRateRequest(1, new DeliveryLocation(50, 50), 10);

			GetDeliveryRateResponse response = await this.useCase.ExecuteAsync(request);

			Assert.AreEqual(DeliveryRateResult.InvalidServiceMethod, response.Result);
		}

		[Test]
		public async Task TestExecuteAsync_NoDeliveryDistanceSpecifiedForServiceMethod()
		{
			GetDeliveryRateRequest request = new GetDeliveryRateRequest(2, new DeliveryLocation(50, 50), 10);

			GetDeliveryRateResponse response = await this.useCase.ExecuteAsync(request);

			Assert.AreEqual(DeliveryRateResult.NoDeliveryDistanceSpecifiedForServiceMethod, response.Result);
		}

		[Test]
		public async Task TestExecuteAsync_UnableToRetrieveDistanceBetweenOutletAndDeliveryLocation()
		{
			GetDeliveryRateRequest request = new GetDeliveryRateRequest(3, new DeliveryLocation(50, 50), 10);

			GetDeliveryRateResponse response = await this.useCase.ExecuteAsync(request);

			Assert.AreEqual(DeliveryRateResult.UnableToRetrieveDistanceBetweenOutletAndDeliveryLocation, response.Result);
			Assert.AreEqual("Address not found", response.ErrorMessage);
		}

		[Test]
		public async Task TestExecuteAsync_OutOfRange()
		{
			GetDeliveryRateRequest request = new GetDeliveryRateRequest(4, this.deliveryLocationNlOffice, 10);

			GetDeliveryRateResponse response = await this.useCase.ExecuteAsync(request);

			Assert.AreEqual(DeliveryRateResult.OutOfRange, response.Result);
		}

		[Test]
		public async Task TestExecuteAsync_OK_FreeDelivery()
		{
			GetDeliveryRateRequest request = new GetDeliveryRateRequest(5, this.deliveryLocationNlOffice, 10);

			GetDeliveryRateResponse response = await this.useCase.ExecuteAsync(request);

			Assert.AreEqual(DeliveryRateResult.OK, response.Result);
			Assert.AreEqual(0, response.Charge);
		}

		[Test]
		public async Task TestExecuteAsync_OK_NoFreeDelivery()
		{
			GetDeliveryRateRequest request = new GetDeliveryRateRequest(5, this.deliveryLocationNlOffice, 5);

			GetDeliveryRateResponse response = await this.useCase.ExecuteAsync(request);

			Assert.AreEqual(DeliveryRateResult.OK, response.Result);
			Assert.AreEqual(2, response.Charge);
		}

		[Test]
		public async Task TestExecuteAsync_OK_InFirstRange()
		{
			GetDeliveryRateRequest request = new GetDeliveryRateRequest(6, this.deliveryLocationRestaurantUK, 30);

			GetDeliveryRateResponse response = await this.useCase.ExecuteAsync(request);

			Assert.AreEqual(DeliveryRateResult.OK, response.Result);
			Assert.AreEqual(1.5, response.Charge);
		}

		[Test]
		public async Task TestExecuteAsync_OK_InSecondRange()
		{
			GetDeliveryRateRequest request = new GetDeliveryRateRequest(7, this.deliveryLocationRestaurantUK, 30);

			GetDeliveryRateResponse response = await this.useCase.ExecuteAsync(request);

			Assert.AreEqual(DeliveryRateResult.OK, response.Result);
			Assert.AreEqual(3.03M, response.ChargeTaxIncluded);
		}

		[Test]
		public async Task TestExecuteAsync_OK_ValidatePricesExclTax()
        {
			GetDeliveryRateRequest request = new GetDeliveryRateRequest(8, this.deliveryLocationRestaurantUK, default);
			GetDeliveryRateResponse response = await this.useCase.ExecuteAsync(request);

			Assert.AreEqual(2.50M, response.ChargeTaxExcluded);
			Assert.AreEqual(3.03M, response.ChargeTaxIncluded);
		}

		[Test]
		public async Task TestExecuteAsync_OK_ValidatePricesInclTax()
		{
			GetDeliveryRateRequest request = new GetDeliveryRateRequest(7, this.deliveryLocationRestaurantUK, default);
			GetDeliveryRateResponse response = await this.useCase.ExecuteAsync(request);

			Assert.AreEqual(2.50M, response.ChargeTaxExcluded);
			Assert.AreEqual(3.03M, response.ChargeTaxIncluded);
		}
	}
}
