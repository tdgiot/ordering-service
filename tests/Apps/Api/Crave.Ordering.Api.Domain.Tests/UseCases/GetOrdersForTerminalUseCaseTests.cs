﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Crave.Enums;
using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Api.Domain.Interfaces.UseCases;
using Crave.Ordering.Api.Domain.Tests.Builders.Converters;
using Crave.Ordering.Api.Domain.Tests.Builders.RepositoryBuilders;
using Crave.Ordering.Api.Domain.Tests.Builders.UseCaseBuilders;
using Crave.Ordering.Api.Domain.UseCases;
using Crave.Ordering.Api.Domain.UseCases.Requests;
using Crave.Ordering.Api.Domain.UseCases.Responses;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.Tests.Builders;
using Moq;
using NUnit.Framework;

namespace Crave.Ordering.Api.Domain.Tests.UseCases
{
    [TestFixture]
    public class GetOrdersForTerminalUseCaseTests
    {
        public static class A
        {
            public static TerminalRepositoryMockBuilder TerminalRepository => new TerminalRepositoryMockBuilder();
            public static TerminalEntityBuilder TerminalEntity => new TerminalEntityBuilder();
            public static OrderRoutestephandlerRepositoryMockBuilder OrderRoutestephandlerRepository => new OrderRoutestephandlerRepositoryMockBuilder();
            public static OrderRoutestephandlerEntityBuilder OrderRoutestephandlerEntity => new OrderRoutestephandlerEntityBuilder();
            public static OrderRepositoryMockBuilder OrderRepository => new OrderRepositoryMockBuilder();
            public static OrderEntityBuilder OrderEntity => new OrderEntityBuilder();
            public static UpdateOrderRoutestephandlerUseCaseBuilder UpdateOrderRoutestephandlerUseCase => new UpdateOrderRoutestephandlerUseCaseBuilder();
            public static OrderConverterBuilder OrderConverter => new OrderConverterBuilder();
        }

        [Test]
        public async Task TerminalIdDoesNotExist_ReturnsNotFound()
        {
            // Arrange
            ITerminalRepository terminalRepository = A.TerminalRepository
                .WithTerminal(A.TerminalEntity.AsNew())
                .Build();

            GetOrdersForTerminalUseCase useCase = new GetOrdersForTerminalUseCase(terminalRepository,
                A.OrderRoutestephandlerRepository.Build().Object,
                A.OrderRepository.Build(),
                A.UpdateOrderRoutestephandlerUseCase.Build().Object,
                A.OrderConverter.Build());

            // Act
            var response = await useCase.ExecuteAsync(new GetOrdersForTerminal(42));

            // Assert
            Assert.That(response.Success, Is.False);
            Assert.That(response.NotFound, Is.True);
        }

        [TestCase(TerminalType.Console, OrderRoutestephandlerStatus.WaitingToBeRetrieved, OrderRoutestephandlerStatus.RetrievedByHandler, OrderRoutestephandlerStatus.BeingHandled)]
        [TestCase(TerminalType.OnSiteServer, OrderRoutestephandlerStatus.WaitingToBeRetrieved)]
        public async Task ValidateOrderRoutestephandlerStatusForTerminalType(TerminalType type, params OrderRoutestephandlerStatus[] statuses)
        {
            // Arrange
            ITerminalRepository terminalRepository = A.TerminalRepository
                .WithTerminal(A.TerminalEntity.WithType(type))
                .Build();

            Mock<IOrderRoutestephandlerRepository> orderRoutestephandlerRepository = A.OrderRoutestephandlerRepository.Build();

            GetOrdersForTerminalUseCase useCase = new GetOrdersForTerminalUseCase(terminalRepository,
                orderRoutestephandlerRepository.Object,
                A.OrderRepository.Build(),
                A.UpdateOrderRoutestephandlerUseCase.Build().Object,
                A.OrderConverter.Build());
            
            // Act
            ResponseBase<IEnumerable<Order>> response = await useCase.ExecuteAsync(new GetOrdersForTerminal(1));

            // Assert
            Assert.That(response.Success, Is.True);
            orderRoutestephandlerRepository.Verify(repo => repo.GetOrderRoutestephandlers(new List<OrderRoutestephandlerStatus>(statuses), It.IsAny<int>()), Times.Once);
        }

        [Test]
        public async Task MarkOrderRoutestephandlerAsRetrieved_OnlyIfWaitingToBeRetrieved()
        {
            // Arrange
            ITerminalRepository terminalRepository = A.TerminalRepository
                .WithTerminal(A.TerminalEntity)
                .Build();

            Mock<IOrderRoutestephandlerRepository> orderRoutestephandlerRepository = A.OrderRoutestephandlerRepository
                .WithOrderRoutestephandlers(A.OrderRoutestephandlerEntity.WithStatus(OrderRoutestephandlerStatus.WaitingToBeRetrieved),
                    A.OrderRoutestephandlerEntity.WithStatus(OrderRoutestephandlerStatus.BeingHandled))
                .Build();

            Mock<IUpdateOrderRoutestephandlerUseCase> updateOrderRoutestephandlerStatusUseCase = A.UpdateOrderRoutestephandlerUseCase.Build();

            GetOrdersForTerminalUseCase useCase = new GetOrdersForTerminalUseCase(terminalRepository,
                orderRoutestephandlerRepository.Object,
                A.OrderRepository.Build(),
                updateOrderRoutestephandlerStatusUseCase.Object,
                A.OrderConverter.Build());

            // Act
            ResponseBase<IEnumerable<Order>> response = await useCase.ExecuteAsync(new GetOrdersForTerminal(1));

            // Assert
            Assert.That(response.Success, Is.True);
            updateOrderRoutestephandlerStatusUseCase.Verify(uc => uc.ExecuteAsync(It.IsAny<OrderRoutestephandlerEntity>()), Times.Once);
        }

        [Test]
        public async Task ReturnsConvertedOrderCollection()
        {
            // Arrange
            ITerminalRepository terminalRepository = A.TerminalRepository
                .WithTerminal(A.TerminalEntity)
                .Build();

            Mock<IOrderRoutestephandlerRepository> orderRoutestephandlerRepository = A.OrderRoutestephandlerRepository
                .WithOrderRoutestephandlers(A.OrderRoutestephandlerEntity.WithStatus(OrderRoutestephandlerStatus.WaitingToBeRetrieved),
                    A.OrderRoutestephandlerEntity.WithStatus(OrderRoutestephandlerStatus.BeingHandled))
                .Build();

            OrderEntityBuilder orderEntity = A.OrderEntity.W((OrderId) 42);

            IOrderRepository orderRepository = A.OrderRepository
                .W(orderEntity)
                .Build();

            Mock<IUpdateOrderRoutestephandlerUseCase> updateOrderRoutestephandlerStatusUseCase = A.UpdateOrderRoutestephandlerUseCase.Build();

            GetOrdersForTerminalUseCase useCase = new GetOrdersForTerminalUseCase(terminalRepository,
                orderRoutestephandlerRepository.Object,
                orderRepository,
                updateOrderRoutestephandlerStatusUseCase.Object,
                A.OrderConverter.Build());

            // Act
            ResponseBase<IEnumerable<Order>> response = await useCase.ExecuteAsync(new GetOrdersForTerminal(1));

            // Assert
            Assert.That(response.Success, Is.True);
            Assert.That(response.Model?.First().OrderId, Is.EqualTo(orderEntity.Build().OrderId));
        }
    }
}
