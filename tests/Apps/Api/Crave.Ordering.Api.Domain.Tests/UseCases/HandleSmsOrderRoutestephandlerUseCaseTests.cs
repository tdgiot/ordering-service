﻿using System;
using System.Collections.Generic;
using System.Linq;
using Crave.Enums;
using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Api.Domain.UseCases;
using Crave.Ordering.Api.Domain.UseCases.Requests;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.OrderNotifications.Domain.UseCases.Requests;
using Crave.Ordering.Shared.OrderNotifications.Domain.UseCases.Responses;
using Crave.Ordering.Shared.OrderNotifications.Interfaces.UseCases;
using Crave.Ordering.Tests;
using NSubstitute;
using NUnit.Framework;

namespace Crave.Ordering.Api.Domain.Tests.UseCases
{
    [TestFixture]
    public class HandleSmsOrderRoutestephandlerUseCaseTests : TestBase
    {
        private GetPhonenumbersForSupportpoolUseCase getPhonenumbersForSupportpoolUseCase;
        private ISendTextMessageUseCase sendTextMessageUseCase;

        private HandleSmsOrderRoutestephandlerUseCase useCase;

        private MessageQueueReponse messageQueueResponse;

        [OneTimeSetUp]
        public void _OneTimeSetUp()
        {
            this.messageQueueResponse = this.CreateMessageQueueResponse(true);

            this.getPhonenumbersForSupportpoolUseCase = this.CreateSubstitute<GetPhonenumbersForSupportpoolUseCase>();
            this.getPhonenumbersForSupportpoolUseCase.Execute(null).ReturnsForAnyArgs(new List<string> { "0123456789" });

            this.sendTextMessageUseCase = this.CreateSubstitute<ISendTextMessageUseCase>();
            this.sendTextMessageUseCase.Execute(Arg.Any<SendSMSRequest>()).ReturnsForAnyArgs(r => this.messageQueueResponse);

            IOrderRepository orderRepository = this.CreateSubstitute<IOrderRepository>();
            orderRepository.GetOrderForNotification(Arg.Any<int>()).Returns(this.CreateOrderEntity());

            this.useCase = new HandleSmsOrderRoutestephandlerUseCase(this.getPhonenumbersForSupportpoolUseCase,
                                                                     this.sendTextMessageUseCase,
                                                                     orderRepository);
        }

        [Test]
        public void GetPhonenumbersForSupportpool()
        {
            HandleNonTerminalRoutingstephandler request = new HandleNonTerminalRoutingstephandler
            {
                OrderRoutestephandlerEntity = new OrderRoutestephandlerEntity
                {
                    IsNew = false,
                    SupportpoolId = 1
                },
                OrderEntity = this.CreateOrderEntity()
            };

            this.useCase.Execute(request);

            this.getPhonenumbersForSupportpoolUseCase.ReceivedWithAnyArgs().Execute(new GetPhonenumbersForSupportpool());
        }

        [Test]
        public void OrderRoutestephandlerIsNotSaved()
        {
            HandleNonTerminalRoutingstephandler request = new HandleNonTerminalRoutingstephandler
            {
                OrderRoutestephandlerEntity = new OrderRoutestephandlerEntity()
            };
            this.useCase.Execute(request);

            this.sendTextMessageUseCase.DidNotReceiveWithAnyArgs().Execute(Arg.Any<SendSMSRequest>());
        }

        [TestCase("+31548754 00354875 123456789", 3)]
        [TestCase("+31548754,00354875,123456789", 3)]
        [TestCase("+31548754,00354875, 123456789", 3)]
        [TestCase("+31548754,00354875 123456789", 3)]
        public void PhoneNumbersAreProperlySplit(string input, int expectedCount)
        {
            // Arrange
            HandleNonTerminalRoutingstephandler request = new HandleNonTerminalRoutingstephandler
            {
                OrderRoutestephandlerEntity = new OrderRoutestephandlerEntity
                {
                    IsNew = false,
                    HandlerType = RoutestephandlerType.SMS,
                    FieldValue1 = input
                },
                OrderEntity = this.CreateOrderEntity()
            };

            // Act
            this.useCase.Execute(request);

            // Assert
            this.sendTextMessageUseCase.Received(1).Execute(Arg.Is<SendSMSRequest>(r => r.Phonenumbers.Count() == expectedCount));
        }

        private OrderEntity CreateOrderEntity()
        {
            return new OrderEntity
            {
                IsNew = false,
                CultureCode = "en-GB",
                OrderId = 1,
                DeliverypointName = "Deliverypoint",
                DeliverypointNumber = "DeliverypointNumber",
                CreatedUTC = DateTime.UtcNow,
                CompanyName = "Company",
                Notes = "Extra order notes",
                StatusText = "Processed",
                OrderitemCollection =
                       {
                           new OrderitemEntity
                           {
                               IsNew = false,
                               OrderitemId = 1,
                               ProductName = "Product name",
                               Quantity = 2,
                               ProductPriceIn = (decimal)2.10,
                               OrderitemAlterationitemCollection =
                               {
                                   new OrderitemAlterationitemEntity
                                   {
                                       IsNew = false,
                                       OrderitemAlterationitemId = 1,
                                       AlterationName = "Alteration",
                                       AlterationoptionName = "AlterationOption"
                                   }
                               }
                           }
                       }
            };
        }

        protected MessageQueueReponse CreateMessageQueueResponse(bool isSuccess, string errorMessage = "")
        {
            return new MessageQueueReponse
            {
                Success = isSuccess,
                ErrorMessage = errorMessage
            };
        }
    }
}
