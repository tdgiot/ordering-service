using Crave.Ordering.Api.Domain.Tests.Builders.RepositoryBuilders;
using Crave.Ordering.Api.Domain.Tests.Builders.UseCaseBuilders;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.Tests.Builders;
using Crave.Ordering.Shared.Data.LLBLGen.Tests.DomainTypes;
using NUnit.Framework;
using System.Collections.Generic;

namespace Crave.Ordering.Api.Domain.Tests.UseCases
{
    [TestFixture]
    public class EnrichOrderWithTaxesUseCaseTest
    {
        static class A
        {
            public static OrderEntityBuilder OrderEntity => new OrderEntityBuilder();
            public static TaxTariffEntityBuilder TaxTariffEntityBuilder => new TaxTariffEntityBuilder();
            public static OrderitemEntityBuilder OrderitemEntity => new OrderitemEntityBuilder();
            public static OrderitemAlterationitemEntityBuilder OrderitemAlterationitemEntity => new OrderitemAlterationitemEntityBuilder();
            public static TaxTariffRepositoryMockBuilder TaxTariffRepositoryMock => new TaxTariffRepositoryMockBuilder();
            public static EnrichOrderWithTaxesUseCaseBuilder EnrichOrderWithTaxesUseCase => new EnrichOrderWithTaxesUseCaseBuilder();
        }

        [TestCase(1, 1, 10, 10)]
        [TestCase(1, 2, 10, 20)]
        [TestCase(2, 1, 10, 20)]
        [TestCase(2, 2, 10, 40)]
        [TestCase(1, 1, 11.1, 11.1)]
        [TestCase(1, 2, 11.1, 22.2)]
        [TestCase(2, 1, 11.1, 22.2)]
        [TestCase(2, 2, 11.1, 44.40)]
        [TestCase(1, 1, -20, -20)]
        public void Execute_VAT_Orderitems_OrderTotal(int numberOfOrderitems, int quantity, decimal productPrice, decimal expectedTotal)
        {
            // Arrange
            var sut = A.EnrichOrderWithTaxesUseCase.Build();

            var orderitemEntityBuilder = A.OrderitemEntity
                                          .W((Quantity)quantity)
                                          .W((ProductPriceIn)productPrice);

            var orderEntity = A.OrderEntity
                               .W(EnrichOrderWithTaxesUseCaseTest.BuildOrderitems(numberOfOrderitems, orderitemEntityBuilder))
                               .Build();

            // Act
            sut.Execute(orderEntity);

            // Assert
            Assert.AreEqual(expectedTotal, orderEntity.Total);
        }

        [TestCase(1, 1, 10, 1, 5, 15)]
        [TestCase(1, 1, 10, 2, 5, 20)]
        [TestCase(1, 2, 10, 1, 5, 30)]
        [TestCase(1, 2, 10, 2, 5, 40)]
        [TestCase(2, 1, 10, 1, 5, 30)]
        [TestCase(2, 1, 10, 2, 5, 40)]
        [TestCase(2, 2, 10, 1, 5, 60)]
        [TestCase(2, 2, 10, 2, 5, 80)]
        public void Execute_VAT_OrderitemsWithProductAlterations_OrderTotal(int numberOfOrderitems, int quantity, decimal productPrice, int numberOfOrderitemAlterationitems, decimal alterationoptionPrice, decimal expectedTotal)
        {
            // Arrange
            var sut = A.EnrichOrderWithTaxesUseCase.Build();

            var orderitemEntityBuilder = A.OrderitemEntity
                                          .W((Quantity)quantity)
                                          .W((ProductPriceIn)productPrice);

            var orderitemAlterationitemEntityBuilder = A.OrderitemAlterationitemEntity
                                                        .W(false)
                                                        .W((OrderitemAlterationitemEntityBuilder.AlterationoptionPriceIn)alterationoptionPrice);

            var orderEntity = A.OrderEntity
                               .W(EnrichOrderWithTaxesUseCaseTest.BuildOrderitemsWithOrderitemAlterationitems(numberOfOrderitems, orderitemEntityBuilder, numberOfOrderitemAlterationitems, orderitemAlterationitemEntityBuilder))
                               .Build();

            // Act
            sut.Execute(orderEntity);

            // Assert
            Assert.AreEqual(expectedTotal, orderEntity.Total);
        }

        [TestCase(1, 1, 10, 1, 5, 15)]
        [TestCase(1, 1, 10, 2, 5, 20)]
        [TestCase(1, 2, 10, 1, 5, 25)]
        [TestCase(1, 2, 10, 2, 5, 30)]
        [TestCase(1, 1, 10, 1, 7.5, 17.5)]
        [TestCase(1, 1, 10, 2, 7.5, 25)]
        [TestCase(1, 2, 10, 1, 7.5, 27.5)]
        [TestCase(1, 2, 10, 2, 7.5, 35)]
        public void Execute_VAT_OrderitemsWithOrderLevelAlterations_OrderTotal(int numberOfOrderitems, int quantity, decimal productPrice, int numberOfOrderitemAlterationitems, decimal alterationoptionPrice, decimal expectedTotal)
        {
            // Arrange
            var sut = A.EnrichOrderWithTaxesUseCase.Build();

            var orderitemEntityBuilder = A.OrderitemEntity
                                          .W((Quantity)quantity)
                                          .W((ProductPriceIn)productPrice);

            var orderitemAlterationitemEntityBuilder = A.OrderitemAlterationitemEntity
                                                        .W(true)
                                                        .W((OrderitemAlterationitemEntityBuilder.AlterationoptionPriceIn)alterationoptionPrice);

            var orderEntity = A.OrderEntity
                               .W(EnrichOrderWithTaxesUseCaseTest.BuildOrderitemsWithOrderitemAlterationitems(numberOfOrderitems, orderitemEntityBuilder, numberOfOrderitemAlterationitems, orderitemAlterationitemEntityBuilder))
                               .Build();

            // Act
            sut.Execute(orderEntity);

            // Assert
            Assert.AreEqual(expectedTotal, orderEntity.Total);
        }

        [TestCase(1, 1, 10, 1, 10.1)]
        [TestCase(1, 1, 2.52, 1, 2.55)]
        [TestCase(1, 1, 10, 10, 11)]
        [TestCase(1, 1, 3.61, 10, 3.97)]
        [TestCase(1, 2, 10, 11.2, 22.24)]
        [TestCase(1, 2, 4.88, 11.2, 10.85)]
        [TestCase(2, 1, 10, 13.1, 22.62)]
        [TestCase(2, 1, 25.99, 13.1, 58.78)]
        [TestCase(2, 2, 10, 20.8, 48.32)]
        [TestCase(2, 2, 99.99, 20.8, 483.16)]
        public void Execute_SalesTax_Orderitems_OrderTotal(int numberOfOrderitems, int quantity, decimal productPrice, double taxPercentage, decimal expectedTotal)
        {
            // Arrange
            int taxTariffId = 1;
            var taxTariffEntity = A.TaxTariffEntityBuilder
                                   .W(taxTariffId)
                                   .W(taxPercentage)
                                   .Build();

            var sut = A.EnrichOrderWithTaxesUseCase
                       .W(A.TaxTariffRepositoryMock
                           .W(taxTariffEntity))
                       .Build();

            var orderitemEntityBuilder = A.OrderitemEntity
                                          .W((Quantity)quantity)
                                          .W((TaxTariffId)taxTariffId)
                                          .W((ProductPriceIn)productPrice);

            var orderEntity = A.OrderEntity
                               .W((PricesIncludeTaxes)false)
                               .W(EnrichOrderWithTaxesUseCaseTest.BuildOrderitems(numberOfOrderitems, orderitemEntityBuilder))
                               .Build();

            // Act
            sut.Execute(orderEntity);

            // Assert
            Assert.AreEqual(expectedTotal, orderEntity.Total);
        }

        [TestCase(1, 1, 10, 1, 5, 1, 15.15)]
        [TestCase(1, 1, 2.52, 1, 5.25, 1, 7.85)]
        [TestCase(1, 1, 10, 1, 10, 10, 22)]
        [TestCase(1, 1, 3.61, 1, 10.26, 10, 15.26)]
        [TestCase(1, 2, 10, 1, 15, 11.2, 55.6)]
        [TestCase(1, 2, 4.88, 2, 15.27, 11.2, 78.77)]
        [TestCase(2, 1, 10, 2, 20, 13.1, 113.1)]
        [TestCase(2, 1, 25.99, 2, 20.28, 13.1, 150.54)]
        [TestCase(2, 2, 10, 2, 25, 20.8, 289.92)]
        [TestCase(2, 2, 99.99, 2, 25.29, 20.8, 727.56)]
        public void Execute_SalesTax_OrderitemsWithProductAlterations_OrderTotal(int numberOfOrderitems, int quantity, decimal productPrice, int numberOfOrderitemAlterationitems, decimal alterationoptionPrice, double taxPercentage, decimal expectedTotal)
        {
            // Arrange
            int taxTariffId = 1;
            var taxTariffEntity = A.TaxTariffEntityBuilder
                                   .W(taxTariffId)
                                   .W(taxPercentage)
                                   .Build();

            var sut = A.EnrichOrderWithTaxesUseCase
                       .W(A.TaxTariffRepositoryMock
                           .W(taxTariffEntity))
                       .Build();

            var orderitemEntityBuilder = A.OrderitemEntity
                                          .W((Quantity)quantity)
                                          .W((ProductPriceIn)productPrice);

            var orderitemAlterationitemEntityBuilder = A.OrderitemAlterationitemEntity
                                                        .W(false)
                                                        .W((OrderitemAlterationitemEntityBuilder.AlterationoptionPriceIn)alterationoptionPrice);

            var orderEntity = A.OrderEntity
                               .W((PricesIncludeTaxes)false)
                               .W(EnrichOrderWithTaxesUseCaseTest.BuildOrderitemsWithOrderitemAlterationitems(numberOfOrderitems, orderitemEntityBuilder, numberOfOrderitemAlterationitems, orderitemAlterationitemEntityBuilder))
                               .Build();

            // Act
            sut.Execute(orderEntity);

            // Assert
            Assert.AreEqual(expectedTotal, orderEntity.Total);
        }

        [TestCase(1, 1, 10, 1, 5, 1, 15.15)]
        [TestCase(1, 1, 2.52, 1, 5.25, 1, 7.85)]
        [TestCase(1, 1, 10, 1, 10, 10, 22)]
        [TestCase(1, 1, 3.61, 1, 10.26, 10, 15.26)]
        [TestCase(1, 2, 10, 1, 15, 11.2, 38.92)]
        [TestCase(1, 2, 4.88, 2, 15.27, 11.2, 44.81)]
        public void Execute_SalesTax_OrderitemsWithOrderLevelAlterations_OrderTotal(int numberOfOrderitems, int quantity, decimal productPrice, int numberOfOrderitemAlterationitems, decimal alterationoptionPrice, double taxPercentage, decimal expectedTotal)
        {
            // Arrange
            int taxTariffId = 1;
            var taxTariffEntity = A.TaxTariffEntityBuilder
                                   .W(taxTariffId)
                                   .W(taxPercentage)
                                   .Build();

            var sut = A.EnrichOrderWithTaxesUseCase
                       .W(A.TaxTariffRepositoryMock
                           .W(taxTariffEntity))
                       .Build();

            var orderitemEntityBuilder = A.OrderitemEntity
                                          .W((Quantity)quantity)
                                          .W((ProductPriceIn)productPrice);

            var orderitemAlterationitemEntityBuilder = A.OrderitemAlterationitemEntity
                                                        .W(true)
                                                        .W((OrderitemAlterationitemEntityBuilder.AlterationoptionPriceIn)alterationoptionPrice);

            var orderEntity = A.OrderEntity
                               .W((PricesIncludeTaxes)false)
                               .W(EnrichOrderWithTaxesUseCaseTest.BuildOrderitemsWithOrderitemAlterationitems(numberOfOrderitems, orderitemEntityBuilder, numberOfOrderitemAlterationitems, orderitemAlterationitemEntityBuilder))
                               .Build();

            // Act
            sut.Execute(orderEntity);

            // Assert
            Assert.AreEqual(expectedTotal, orderEntity.Total);
        }

        private static OrderitemEntity[] BuildOrderitems(int numberOfOrderitems, OrderitemEntityBuilder orderitemEntityBuilder)
        {
            List<OrderitemEntity> orderitems = new List<OrderitemEntity>();

            for (int i = 0; i < numberOfOrderitems; i++)
            {
                orderitems.Add(orderitemEntityBuilder.Build());
            }

            return orderitems.ToArray();
        }

        private static OrderitemEntity[] BuildOrderitemsWithOrderitemAlterationitems(int numberOfOrderitems, OrderitemEntityBuilder orderitemEntityBuilder, int numberOfOrderitemAlterationitems, OrderitemAlterationitemEntityBuilder orderitemAlterationitemEntityBuilder)
        {
            List<OrderitemEntity> orderitems = new List<OrderitemEntity>();

            for (int i = 0; i < numberOfOrderitems; i++)
            {
                orderitems.Add(orderitemEntityBuilder
                               .W(EnrichOrderWithTaxesUseCaseTest.BuildOrderitemAlterationitems(numberOfOrderitemAlterationitems, orderitemAlterationitemEntityBuilder))
                               .Build());
            }

            return orderitems.ToArray();
        }

        private static OrderitemAlterationitemEntity[] BuildOrderitemAlterationitems(int numberOfOrderitemAlterationitems, OrderitemAlterationitemEntityBuilder orderitemAlterationitemEntityBuilder)
        {
            List<OrderitemAlterationitemEntity> orderitemAlterationitemEntities = new List<OrderitemAlterationitemEntity>();

            for (int j = 0; j < numberOfOrderitemAlterationitems; j++)
            {
                orderitemAlterationitemEntities.Add(orderitemAlterationitemEntityBuilder.Build());
            }

            return orderitemAlterationitemEntities.ToArray();
        }
    }
}
