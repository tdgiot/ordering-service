﻿using System.Threading.Tasks;
using Crave.Enums;
using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Api.Domain.Tests.Builders.RepositoryBuilders;
using Crave.Ordering.Api.Domain.UseCases;
using Crave.Ordering.Api.Domain.UseCases.Requests;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.Tests.Builders;
using Crave.Ordering.Shared.Messaging.Interfaces.UseCases;
using Crave.Ordering.Tests;
using Moq;
using NSubstitute;
using NUnit.Framework;

namespace Crave.Ordering.Api.Domain.Tests.UseCases
{
    [TestFixture]
    public class ExecuteRoutestepUseCaseTests : TestBase
    {
        internal static class A
        {
            public static OrderRoutestephandlerRepositoryMockBuilder OrderRoutestephandlerRepository => new OrderRoutestephandlerRepositoryMockBuilder();
            public static OrderRoutestephandlerEntityBuilder OrderRoutestephandlerEntity => new OrderRoutestephandlerEntityBuilder();
        }

        private IDeviceRepository deviceRepository;
        private SetForwardedTerminalUseCase setForwardedTerminalUseCase;
        private HandleNonTerminalRoutingstephandlerUseCase handleNonTerminalRoutingstephandlerUseCase;
        private ISendNetmessageUseCase sendNetmessageUseCase;
        private UpdateOrderRoutestephandlerUseCase updateOrderRoutestephandlerUseCase;

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            this.deviceRepository = CreateSubstitute<IDeviceRepository>();
            this.setForwardedTerminalUseCase = CreateSubstitute<SetForwardedTerminalUseCase>();
            this.handleNonTerminalRoutingstephandlerUseCase = CreateSubstitute<HandleNonTerminalRoutingstephandlerUseCase>();
            this.sendNetmessageUseCase = CreateSubstitute<ISendNetmessageUseCase>();
            this.updateOrderRoutestephandlerUseCase = CreateSubstitute<UpdateOrderRoutestephandlerUseCase>();
        }

        [Test]
        public async Task Execute_SingleConsoleRoutestep()
        {
            // Arrange
            Mock<IOrderRoutestephandlerRepository> repository = A.OrderRoutestephandlerRepository
                .WithStepSpecificRoutesteps(
                    A.OrderRoutestephandlerEntity
                        .WithOrderId(1)
                        .WithNumber(1)
                        .WithHandlerType(RoutestephandlerType.Console)
                        .WithStatus(OrderRoutestephandlerStatus.WaitingToBeRetrieved),
                    A.OrderRoutestephandlerEntity
                        .WithOrderId(1)
                        .WithNumber(2)
                        .WithHandlerType(RoutestephandlerType.HotSOS)
                        .WithStatus(OrderRoutestephandlerStatus.WaitingForPreviousStep))
                .Build();
            
            ExecuteRoutestepUseCase sut = new ExecuteRoutestepUseCase(repository.Object,
                this.deviceRepository,
                this.setForwardedTerminalUseCase,
                this.handleNonTerminalRoutingstephandlerUseCase,
                this.sendNetmessageUseCase,
                this.updateOrderRoutestephandlerUseCase);

            // Act
            await sut.ExecuteAsync(new ExecuteRoutestepRequest(1, 1));

            // Assert
            repository.Verify(x => x.GetSpecificStepsOfRoute(1, It.IsAny<int>()), Times.Exactly(1));
        }

        [Test, Ignore("Will fix later")]
        public async Task Execute_AutomatedStepBeforeConsole()
        {
            // Arrange
            Mock<IOrderRoutestephandlerRepository> repository = A.OrderRoutestephandlerRepository
                .WithStepSpecificRoutesteps(
                    A.OrderRoutestephandlerEntity
                        .WithOrderId(1)
                        .WithNumber(1)
                        .WithHandlerType(RoutestephandlerType.HotSOS)
                        .WithStatus(OrderRoutestephandlerStatus.WaitingToBeRetrieved),
                    A.OrderRoutestephandlerEntity
                        .WithOrderId(1)
                        .WithNumber(2)
                        .WithHandlerType(RoutestephandlerType.Console)
                        .WithStatus(OrderRoutestephandlerStatus.WaitingForPreviousStep))
                .Build();

            handleNonTerminalRoutingstephandlerUseCase.ExecuteAsync(Arg.Any<OrderRoutestephandlerEntity>()).Returns(
                A.OrderRoutestephandlerEntity
                    .WithStatus(OrderRoutestephandlerStatus.Completed)
                    .Build());
            
            ExecuteRoutestepUseCase sut = new ExecuteRoutestepUseCase(repository.Object,
                this.deviceRepository,
                this.setForwardedTerminalUseCase,
                this.handleNonTerminalRoutingstephandlerUseCase,
                this.sendNetmessageUseCase,
                this.updateOrderRoutestephandlerUseCase);

            // Act
            await sut.ExecuteAsync(new ExecuteRoutestepRequest(1, 1));

            // Assert
            repository.Verify(x => x.GetSpecificStepsOfRoute(1, 1), Times.Exactly(1));
            repository.Verify(x => x.GetSpecificStepsOfRoute(1, 2), Times.Exactly(1));
        }
    }
}