﻿using System;
using System.Threading.Tasks;
using Crave.Enums;
using Crave.Ordering.Api.Domain.UseCases;
using Crave.Ordering.Api.Domain.UseCases.Requests;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.OrderNotifications.Domain.UseCases.Requests;
using Crave.Ordering.Shared.OrderNotifications.Domain.UseCases.Responses;
using Crave.Ordering.Shared.OrderNotifications.Interfaces.UseCases;
using Crave.Ordering.Tests;
using NSubstitute;
using NUnit.Framework;

namespace Crave.Ordering.Api.Domain.Tests.UseCases
{
    [TestFixture]
    public class HandleEmailDocumentRoutestephandlerUseCaseTests : TestBase
    {
        private ISendMailUseCase sendMailUseCase;
        
        private HandleEmailDocumentRoutestephandlerUseCase useCase;

        private MessageQueueReponse messageQueueResponse;

        [OneTimeSetUp]
        public void _OneTimeSetUp()
        {
            this.messageQueueResponse = this.CreateMessageQueueResponse(true);

            this.sendMailUseCase = this.CreateSubstitute<ISendMailUseCase>();
            this.sendMailUseCase.ExecuteAsync(Arg.Any<SendMailRequest>()).ReturnsForAnyArgs(r => this.messageQueueResponse);

            this.useCase = new HandleEmailDocumentRoutestephandlerUseCase(this.sendMailUseCase);
        }

        [Test]
        public async Task EmailRequestPublishedOnMessageQueue_Success()
        {
            messageQueueResponse = CreateMessageQueueResponse(true);

            OrderEntity orderEntity = CreateOrder();
            HandleNonTerminalRoutingstephandler request = new HandleNonTerminalRoutingstephandler
            {
                OrderEntity = orderEntity,
                OrderRoutestephandlerEntity = CreateOrderRoutestephandler(orderEntity)
            };
            await useCase.ExecuteAsync(request);

            await sendMailUseCase.Received(1).ExecuteAsync(Arg.Any<SendMailRequest>());
            Assert.AreEqual(OrderRoutestephandlerStatus.BeingHandled, request.OrderRoutestephandlerEntity.Status);
        }

        [Test]
        public async Task EmailRequestPublishedOnMessageQueue_Failed()
        {
            messageQueueResponse = CreateMessageQueueResponse(false);

            OrderEntity orderEntity = CreateOrder();
            HandleNonTerminalRoutingstephandler request = new HandleNonTerminalRoutingstephandler
            {
                OrderEntity = orderEntity,
                OrderRoutestephandlerEntity = CreateOrderRoutestephandler(orderEntity)
            };
            await useCase.ExecuteAsync(request);

            await sendMailUseCase.Received(1).ExecuteAsync(Arg.Any<SendMailRequest>());
            Assert.AreEqual(OrderRoutestephandlerStatus.Failed, request.OrderRoutestephandlerEntity.Status);
        }

        private OrderEntity CreateOrder()
        {
            return new OrderEntity
            {
                IsNew = false,
                OrderId = 1,
                Deliverypoint = new DeliverypointEntity
                {
                    Number = "1337",
                    Deliverypointgroup = new DeliverypointgroupEntity
                    {
                        DeliverypointCaption = "Room"
                    }
                },
                DeliverypointNumber = "1337",
                DeliverypointName = "L33t",
                CreatedUTC = DateTime.UtcNow
            };
        }

        private OrderRoutestephandlerEntity CreateOrderRoutestephandler(OrderEntity order)
        {
            return new OrderRoutestephandlerEntity
            {
                File = new byte[30],
                Status = OrderRoutestephandlerStatus.BeingHandled,
                Order = order,
                FieldValue1 = "unit@test.crave"
            };
        }

        protected MessageQueueReponse CreateMessageQueueResponse(bool isSuccess, string errorMessage = "")
        {
            return new MessageQueueReponse
            {
                Success = isSuccess,
                ErrorMessage = errorMessage
            };
        }
    }
}