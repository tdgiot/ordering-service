﻿using Crave.Enums;
using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Api.Domain.Interfaces.UseCases;
using Crave.Ordering.Api.Domain.UseCases;
using Crave.Ordering.Api.Domain.UseCases.Requests;
using Crave.Ordering.Shared.Constants;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Tests;
using NSubstitute;
using NUnit.Framework;
using System.Threading.Tasks;
using Moq;

namespace Crave.Ordering.Api.Domain.Tests.UseCases
{
    [TestFixture]
    public class WriteRouteUseCaseTests : TestBase
    {
        private IOrderRoutestephandlerRepository orderRoutestephandlerRepository;
        private ISaveOrderUseCase saveOrderUseCase;
        private Mock<IStartRouteUseCase> startRouteUseCase;

        private WriteRouteUseCase useCase;

        [OneTimeSetUp]
        public void _OneTimeSetUp()
        {
            this.orderRoutestephandlerRepository = this.CreateSubstitute<IOrderRoutestephandlerRepository>();
            this.startRouteUseCase = new Mock<IStartRouteUseCase>();
            this.saveOrderUseCase = this.CreateSubstitute<ISaveOrderUseCase>();
            
            this.useCase = new WriteRouteUseCase(this.orderRoutestephandlerRepository,
                                                 this.saveOrderUseCase,
                                                 this.startRouteUseCase.Object);
        }

        [Test]
        public void SaveRouteWithoutSteps_Throws()
        {
            WriteRoute request = new WriteRoute(new OrderEntity(), new RouteEntity(), true, false);

            Assert.ThrowsAsync<CraveException>(() => this.useCase.ExecuteAsync(request));
        }

        [Test]
        public void SaveRouteWithoutFirstStep_Throws()
        {
            WriteRoute request = new WriteRoute(new OrderEntity(),
                                                new RouteEntity
                                                {
                                                    RoutestepCollection =
                                                    {
                                                        new RoutestepEntity
                                                        {
                                                            Number = RoutingConstants.FirstStepNumber + 1
                                                        }
                                                    }
                                                },
                                                true,
                                                false);

            Assert.ThrowsAsync<CraveException>(() => this.useCase.ExecuteAsync(request));
        }

        [Test]
        public async Task SaveRoute([Range(1, 3)]int numberOfRoutesteps, [Range(1, 3)]int handlersPerRoutestep)
        {
            this.startRouteUseCase.Reset();

            RouteEntity routeEntity = new RouteEntity
            {
                RouteId = 1
            };

            for (int i = 0; i < numberOfRoutesteps; i++)
            {
                routeEntity.RoutestepCollection.Add(WriteRouteUseCaseTests.CreateStep(i + 1, handlersPerRoutestep));
            }

            WriteRoute request = new WriteRoute(new OrderEntity(),
                                                routeEntity,
                                                true, // start
                                                false); // don't escalate
            await useCase.ExecuteAsync(request);


            // Calculation is a bit wonky because we're calling SaveOrderRoutestephandler twice in the write usecase
            int totalRoutestephandlersSaved = (numberOfRoutesteps * handlersPerRoutestep);

            // Saving all steps
            orderRoutestephandlerRepository
                .Received(totalRoutestephandlersSaved)
                .SaveOrderRoutestephandler(Arg.Is<OrderRoutestephandlerEntity>(entity => entity.Status == OrderRoutestephandlerStatus.WaitingForPreviousStep));

            // Starting the first steps
            startRouteUseCase.Verify(x => x.ExecuteAsync(It.IsAny<OrderRoutestephandlerEntity>()), Times.Exactly(handlersPerRoutestep));
        }

        [Test]
        public async Task SaveRouteWithEscalationRoute()
        {
            OrderEntity orderEntity = new OrderEntity
            {
                OrderRoutestephandlerCollection =
                {
                    new OrderRoutestephandlerEntity
                    {
                        Number = 10
                    }
                }
            };

            RouteEntity routeEntity = new RouteEntity
            {
                RouteId = 1,
                RoutestepCollection =
                {
                    WriteRouteUseCaseTests.CreateStep(1, 2),
                    WriteRouteUseCaseTests.CreateStep(2, 1)
                }
            };

            WriteRoute request = new WriteRoute(orderEntity,
                                                routeEntity,
                                                true, // start
                                                true); // escalate
            await this.useCase.ExecuteAsync(request);

            // When escalation route is used, the initial step number should be incremented by the max number of the escalation route
            this.orderRoutestephandlerRepository
                .Received()
                .SaveOrderRoutestephandler(Arg.Is<OrderRoutestephandlerEntity>(entity => entity.Number > 10));
        }

        private static RoutestepEntity CreateStep(int routeNumber, int numberOfSteps)
        {
            RoutestepEntity routestepEntity = new RoutestepEntity
            {
                RoutestepId = routeNumber,
                Number = routeNumber,
                Route = new RouteEntity
                {
                    EscalationRouteId = 0
                }
            };

            for (int i = 0; i < numberOfSteps; i++)
            {
                routestepEntity.RoutestephandlerCollection.Add(new RoutestephandlerEntity
                {
                    RoutestephandlerId = (i + 1),
                    HandlerType = RoutestephandlerType.Console
                });
            }

            return routestepEntity;
        }
    }
}