﻿using System.Linq;
using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Api.Domain.Tests.Builders.RepositoryBuilders;
using Crave.Ordering.Api.Domain.UseCases;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.Tests.Builders;
using Crave.Ordering.Tests;
using NUnit.Framework;

namespace Crave.Ordering.Api.Domain.Tests.UseCases
{
    [TestFixture]
    public class EnrichOrderitemsWithTagsUseCaseTests : TestBase
    {
        static class A
        {
            public static OrderitemEntityBuilder OrderitemEntity => new OrderitemEntityBuilder();
            public static ProductTagEntityBuilder ProductTagEntity => new ProductTagEntityBuilder();
            public static ProductCategoryTagEntityBuilder ProductCategoryTagEntity => new ProductCategoryTagEntityBuilder();
            public static AlterationoptionTagEntityBuilder AlterationoptionTagEntity => new AlterationoptionTagEntityBuilder();
            public static OrderitemAlterationitemEntityBuilder OrderitemAlterationitemEntity => new OrderitemAlterationitemEntityBuilder();

            public static ProductTagRepositoryMockBuilder ProductTagRepository = new ProductTagRepositoryMockBuilder();
            public static ProductCategoryTagRepositoryMockBuilder ProductCategoryTagRepository = new ProductCategoryTagRepositoryMockBuilder();
            public static AlterationoptionTagRepositoryMockBuilder AlterationoptionTagRepository = new AlterationoptionTagRepositoryMockBuilder();
        }

        private IProductTagRepository productTagRepository;
        private IProductCategoryTagRepository productCategoryTagRepository;
        private IAlterationoptionTagRepository alterationoptionTagRepository;

        private EnrichOrderitemsWithTagsUseCase enrichOrderitemsWithTagsUseCase;

        [OneTimeSetUp]
        public void _OneTimeSetUp()
        {
            productTagRepository = A.ProductTagRepository.W(
                    A.ProductTagEntity
                        .WithProductId(1)
                        .WithTagId(1)
                        .Build()
                    )
                .Build();

            productCategoryTagRepository = A.ProductCategoryTagRepository.W(
                A.ProductCategoryTagEntity
                    .WithProductId(1)
                    .WithCategoryId(1)
                    .WithTagId(2)
                    .Build()
                )
                .Build();

            alterationoptionTagRepository = A.AlterationoptionTagRepository.W(
                    A.AlterationoptionTagEntity
                        .WithAlterationoptionId(10)
                        .WithTagId(3)
                        .Build()
                )
                .Build();

            enrichOrderitemsWithTagsUseCase = new EnrichOrderitemsWithTagsUseCase(productTagRepository, productCategoryTagRepository, alterationoptionTagRepository);
        }

        [Test]
        public void OrderitemsWithTags_EnrichOrderitemsWithProductAndCategoryTags()
        {
            EntityCollection<OrderitemEntity> orderItems = new EntityCollection<OrderitemEntity>
            {
                A.OrderitemEntity
                    .WithProductId(1)
                    .WithCategoryId(1)
                    .Build(),
                A.OrderitemEntity
                    .WithProductId(2)
                    .WithCategoryId(2)
                    .Build()
            };

            enrichOrderitemsWithTagsUseCase.Execute(orderItems);

            AssertNumberOfTagsOnOrderItem(orderItems, productId: 1, expectedNumberOfTags: 2);
            AssertNumberOfTagsOnOrderItem(orderItems, productId: 2, expectedNumberOfTags: 0);

            AssertOrderItemHasTag(orderItems, productId: 1, tagId: 1);
            AssertOrderItemHasTag(orderItems, productId: 1, tagId: 2);
        }

        [Test]
        public void OrderitemsWithTags_EnrichOrderitemAlterationitemsWithTags()
        {
            EntityCollection<OrderitemEntity> orderItems = new EntityCollection<OrderitemEntity>
            {
                A.OrderitemEntity
                    .WithProductId(1)
                    .WithCategoryId(1)
                    .W(new []
                    {
                        A.OrderitemAlterationitemEntity.W(10).Build()
                    })
                    .Build()
            };

            enrichOrderitemsWithTagsUseCase.Execute(orderItems);

            AssertNumberOfTagsOnOrderItem(orderItems, productId: 1, expectedNumberOfTags: 2);

            AssertOrderItemHasTag(orderItems, productId: 1, tagId: 1);
            AssertOrderItemHasTag(orderItems, productId: 1, tagId: 2);

            AssertOrderitemAlterationitemHasTag(orderItems, productId: 1, alterationoptionId: 10, tagId: 3);
            AssertNumberOfTagsOnOrderItemAlterationitem(orderItems, productId: 1, alterationoptionId: 10, expectedNumberOfTags: 1);
        }

        private static void AssertNumberOfTagsOnOrderItem(EntityCollection<OrderitemEntity> orderItems, int productId, int expectedNumberOfTags)
        {
            var numberOfTags = orderItems
                .FirstOrDefault(item => item.ProductId == productId)
                ?.OrderitemTagCollection
                ?.Count;

            Assert.AreEqual(expectedNumberOfTags, numberOfTags);
        }

        private static void AssertNumberOfTagsOnOrderItemAlterationitem(EntityCollection<OrderitemEntity> orderItems, int productId, int alterationoptionId, int expectedNumberOfTags)
        {
            var numberOfTags = orderItems
                .FirstOrDefault(item => item.ProductId == productId)
                ?.OrderitemAlterationitemCollection
                ?.FirstOrDefault(alterationitem => alterationitem.AlterationoptionId == alterationoptionId)
                ?.OrderitemAlterationitemTagCollection
                ?.Count;

            Assert.AreEqual(expectedNumberOfTags, numberOfTags);
        }

        private static void AssertOrderItemHasTag(EntityCollection<OrderitemEntity> orderItems, int productId, int tagId)
        {
            var orderitemTag = orderItems
                .FirstOrDefault(item => item.ProductId == productId)
                ?.OrderitemTagCollection
                ?.FirstOrDefault(tag => tag.TagId == tagId);

            Assert.IsNotNull(orderitemTag);
        }

        private static void AssertOrderitemAlterationitemHasTag(EntityCollection<OrderitemEntity> orderItems, int productId, int alterationoptionId, int tagId)
        {
            var orderitemTag = orderItems
                .FirstOrDefault(item => item.ProductId == productId)
                ?.OrderitemAlterationitemCollection
                ?.FirstOrDefault(alterationitem => alterationitem.AlterationoptionId == alterationoptionId)
                ?.OrderitemAlterationitemTagCollection
                ?.FirstOrDefault(tag => tag.TagId == tagId);

            Assert.IsNotNull(orderitemTag);
        }
    }
}
