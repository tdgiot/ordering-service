﻿using System.Threading.Tasks;
using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Api.Domain.UseCases;
using Crave.Ordering.Api.Domain.UseCases.Requests;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Tests;
using NSubstitute;
using NUnit.Framework;

namespace Crave.Ordering.Api.Domain.Tests.UseCases
{
    [TestFixture]
    public class GetClientConfigurationUseCaseTests : TestBase
    {
        private IClientRepository clientRepository;
        private IDeliverypointRepository deliverypointRepository;
        private IDeliverypointgroupRepository deliverypointgroupRepository;
        private IClientConfigurationRepository clientConfigurationRepository;

        private GetClientConfigurationUseCase useCase;

        [OneTimeSetUp]
        public void _OneTimeSetUp()
        {
            clientRepository = CreateSubstitute<IClientRepository>();
            clientRepository.GetClientAsync(1).Returns(new ClientEntity {DeliverypointId = 1});
            clientRepository.GetClientAsync(2).Returns(new ClientEntity {DeliverypointId = 2});

            deliverypointRepository = CreateSubstitute<IDeliverypointRepository>();
            deliverypointRepository.GetDeliverypoint(1).Returns(new DeliverypointEntity {ClientConfigurationId = 1});
            deliverypointRepository.GetDeliverypoint(2).Returns(new DeliverypointEntity {ClientConfigurationId = null, DeliverypointgroupId = 1});

            deliverypointgroupRepository = CreateSubstitute<IDeliverypointgroupRepository>();
            deliverypointgroupRepository.GetDeliverypointgroup(1).Returns(new DeliverypointgroupEntity {ClientConfigurationId = 2});

            clientConfigurationRepository = CreateSubstitute<IClientConfigurationRepository>();
            clientConfigurationRepository.GetClientConfigurationAsync(Arg.Any<int>()).Returns(new ClientConfigurationEntity());

            useCase = new GetClientConfigurationUseCase(clientRepository, deliverypointRepository, deliverypointgroupRepository, clientConfigurationRepository);
        }

        [Test]
        public async Task WithoutDeliverypointAndClientId_ReturnsNull()
        {
            ClientConfigurationEntity? entity = await useCase.ExecuteAsync(new GetClientConfiguration());
            Assert.IsNull(entity);
        }

        [Test]
        public async Task WithoutDeliverypointWithClientId()
        {
            ClientConfigurationEntity? entity = await useCase.ExecuteAsync(new GetClientConfiguration {ClientId = 1});
            Assert.IsNotNull(entity);

            await clientRepository.Received(1).GetClientAsync(1);
            deliverypointRepository.Received(1).GetDeliverypoint(1);
            await clientConfigurationRepository.Received(1).GetClientConfigurationAsync(1);

            deliverypointgroupRepository.DidNotReceive().GetDeliverypointgroup(1);
        }

        [Test]
        public async Task DeliverypointDoesNotHaveConfiguration_FallbackToDeliverypointgroup()
        {
            ClientConfigurationEntity? entity = await useCase.ExecuteAsync(new GetClientConfiguration { ClientId = 2 });
            Assert.IsNotNull(entity);

            await clientConfigurationRepository.DidNotReceive().GetClientConfigurationAsync(1);
            await clientConfigurationRepository.Received(1).GetClientConfigurationAsync(2);
        }

        [Test]
        public async Task WithDeliverypoint()
        {
            ClientConfigurationEntity? entity = await useCase.ExecuteAsync(new GetClientConfiguration { DeliverypointId = 1 });
            Assert.IsNotNull(entity);

            await clientRepository.DidNotReceive().GetClientAsync(Arg.Any<int>());
            await clientConfigurationRepository.Received(1).GetClientConfigurationAsync(1);
        }
    }
}