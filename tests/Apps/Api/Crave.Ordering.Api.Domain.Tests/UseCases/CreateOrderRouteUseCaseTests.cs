﻿using System.Threading.Tasks;
using Crave.Enums;
using Crave.Ordering.Api.Domain.Interfaces.UseCases;
using Crave.Ordering.Api.Domain.Tests.Builders.RepositoryBuilders;
using Crave.Ordering.Api.Domain.Tests.Builders.UseCaseBuilders;
using Crave.Ordering.Api.Domain.UseCases;
using Crave.Ordering.Api.Domain.UseCases.Requests;
using Crave.Ordering.Api.Domain.UseCases.Responses;
using Crave.Ordering.Shared.Data.LLBLGen.Tests.Builders;
using Crave.Ordering.Shared.Enums;
using Crave.Ordering.Shared.OrderNotifications.Domain.UseCases.Requests;
using Crave.Ordering.Shared.OrderNotifications.Interfaces.UseCases;
using Crave.Ordering.Tests;
using Moq;
using NUnit.Framework;

namespace Crave.Ordering.Api.Domain.Tests.UseCases
{
    [TestFixture]
    public class CreateOrderRouteUseCaseTests : TestBase
    {
        static class A
        {
            public static CreateOrderRouteUseCaseBuilder CreateOrderRouteUseCase => new CreateOrderRouteUseCaseBuilder();
            public static OrderEntityBuilder Order => new OrderEntityBuilder();
            public static OrderRepositoryMockBuilder OrderRepository => new OrderRepositoryMockBuilder();
            public static ValidateOrderBeforeRoutingUseCaseMockBuilder ValidateOrderBeforeRoutingUseCase => new ValidateOrderBeforeRoutingUseCaseMockBuilder();
        }

        [Test]
        public async Task Execute_ValidationFails_ReturnsValidationResult()
        {
            Mock<ISendOrderNotificationUseCase> sendOrderNotificationUseCase = new Mock<ISendOrderNotificationUseCase>();
            Mock<IRouteOrderUseCase> routeOrderUseCase = new Mock<IRouteOrderUseCase>();

            // Arrange
            CreateOrderRouteUseCase sut = A.CreateOrderRouteUseCase
                                           .W(A.ValidateOrderBeforeRoutingUseCase
                                               .WithResult(CreateOrderRouteResult.OrderNotFound)
                                               .Build())
                                           .W(sendOrderNotificationUseCase.Object)
                                           .W(routeOrderUseCase.Object)
                                           .Build();

            // Act
            CreateOrderRouteResponse response = await sut.ExecuteAsync(new CreateOrderRoute(1));

            // Assert
            Assert.AreEqual(CreateOrderRouteResult.OrderNotFound, response.Result);
            sendOrderNotificationUseCase.Verify(x => x.ExecuteAsync(It.IsAny<SendOrderNotificationRequest>()), Times.Never);
            routeOrderUseCase.Verify(x => x.ExecuteAsync(It.IsAny<RouteOrder>()), Times.Never);
        }

        [TestCase(CheckoutType.PaymentProvider, OrderNotificationType.Receipt)]
        [TestCase(CheckoutType.ChargeToRoom, OrderNotificationType.OrderConfirmation)]
        [TestCase(CheckoutType.PayLater, OrderNotificationType.OrderConfirmation)]
        public async Task Execute_OnRoute_CorrectNotificationIsSent(CheckoutType checkoutType, OrderNotificationType orderNotificationType)
        {
            Mock<ICreateReceiptUseCase> createReceiptUseCase = new Mock<ICreateReceiptUseCase>();
            Mock<ISendOrderNotificationUseCase> sendOrderNotificationUseCase = new Mock<ISendOrderNotificationUseCase>();
            Mock<IRouteOrderUseCase> routeOrderUseCase = new Mock<IRouteOrderUseCase>();

            // Arrange
            CreateOrderRouteUseCase sut = A.CreateOrderRouteUseCase
                                           .W(A.OrderRepository
                                               .W(A.Order.W(checkoutType))
                                               .Build())
                                           .W(A.ValidateOrderBeforeRoutingUseCase
                                               .Build())
                                           .W(createReceiptUseCase.Object)
                                           .W(sendOrderNotificationUseCase.Object)
                                           .W(routeOrderUseCase.Object)
                                           .Build();

            int createReceiptCalls = orderNotificationType == OrderNotificationType.Receipt ? 1 : 0;

            // Act
            CreateOrderRouteResponse response = await sut.ExecuteAsync(new CreateOrderRoute(1));

            // Assert
            Assert.AreEqual(CreateOrderRouteResult.Ok, response.Result);
            createReceiptUseCase.Verify(x => x.Execute(It.IsAny<CreateReceipt>()), Times.Exactly(createReceiptCalls));
            sendOrderNotificationUseCase.Verify(x => x.ExecuteAsync(It.IsAny<SendOrderNotificationRequest>()), Times.Once);
            routeOrderUseCase.Verify(x => x.ExecuteAsync(It.IsAny<RouteOrder>()), Times.Once);
        }
    }
}