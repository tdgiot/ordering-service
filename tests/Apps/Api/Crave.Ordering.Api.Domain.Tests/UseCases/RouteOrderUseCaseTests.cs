﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Crave.Enums;
using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Api.Domain.Interfaces.UseCases;
using Crave.Ordering.Api.Domain.Tests.Builders.EntityBuilders;
using Crave.Ordering.Api.Domain.Tests.Builders.RepositoryBuilders;
using Crave.Ordering.Api.Domain.Tests.Builders.UseCaseBuilders;
using Crave.Ordering.Api.Domain.UseCases;
using Crave.Ordering.Api.Domain.UseCases.Requests;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Crave.Ordering.Shared.Data.LLBLGen.Tests.Builders;
using Crave.Ordering.Tests;
using Moq;
using NUnit.Framework;

namespace Crave.Ordering.Api.Domain.Tests.UseCases
{
    [TestFixture]
    public class RouteOrderUseCaseTests : TestBase
    {
        internal static class A
        {
            public static RouteRepositoryMockBuilder RouteRepository => new RouteRepositoryMockBuilder();
            public static OrderRepositoryMockBuilder OrderRepository => new OrderRepositoryMockBuilder();
            public static OrderitemRepositoryMockBuilder OrderitemRepository => new OrderitemRepositoryMockBuilder();
            public static ProductRepositoryMockBuilder ProductRepository => new ProductRepositoryMockBuilder();
            public static CategoryRepositoryMockBuilder CategoryRepository => new CategoryRepositoryMockBuilder();
            
            public static RouteEntityBuilder RouteEntity => new RouteEntityBuilder();
            public static ClientConfigurationEntityBuilder ClientConfigurationEntity => new ClientConfigurationEntityBuilder();
            public static ClientConfigurationRouteEntityBuilder ClientConfigurationRouteEntity => new ClientConfigurationRouteEntityBuilder();
            public static OrderEntityBuilder OrderEntity => new OrderEntityBuilder();
            public static OrderitemEntityBuilder OrderitemEntity => new OrderitemEntityBuilder();
            public static CompanyEntityBuilder CompanyEntity => new CompanyEntityBuilder();
            public static DeliverypointEntityBuilder DeliverypointEntity => new DeliverypointEntityBuilder();
            public static DeliverypointgroupEntityBuilder DeliverypointgroupEntity => new DeliverypointgroupEntityBuilder();
            public static ProductEntityBuilder ProductEntity => new ProductEntityBuilder();
            public static CategoryEntityBuilder CategoryEntity => new CategoryEntityBuilder();


            public static GetClientConfigurationUseCaseBuilder GetClientConfigurationUseCase => new GetClientConfigurationUseCaseBuilder();
            public static SaveOrderUseCaseBuilder SaveOrderUseCase => new SaveOrderUseCaseBuilder();
            public static WriteRouteUseCaseBuilder WriteRouteUseCase => new WriteRouteUseCaseBuilder();
            public static CreateReceiptUseCaseMockBuilder CreateReceiptUseCase => new CreateReceiptUseCaseMockBuilder();
        }

        [Test(ExpectedResult = true)]
        public async Task<bool> ExecuteForOrderWithoutOrderitems()
        {
            // Arrange
            TestData testData = new TestData(1);

            // Act
            bool result = await testData.UseCase.ExecuteAsync(new RouteOrder(testData.OrderEntity.Build()));

            // Assert
            testData.WriteRouteUseCase.Verify(uc => uc.ExecuteAsync(It.IsAny<WriteRoute>()), Times.Once);
            return result;
        }

        [Test(ExpectedResult = true)]
        public async Task<bool> OrderUsesCompanyDefinedRoute()
        {
            // Arrange
            TestData testData = new TestData(5);

            // Act
            bool result = await testData.UseCase.ExecuteAsync(new RouteOrder(testData.OrderEntity.Build()));

            // Assert
            testData.RouteRepository.Verify(repo => repo.GetRoute(100), Times.Once); // Company route
            return result;
        }

        [Test(ExpectedResult = true)]
        public async Task<bool> OrderWithNotes_RouteToOrderNotesRoute()
        {
            // Arrange
            TestData testData = new TestData(3);

            // Act
            bool result = await testData.UseCase.ExecuteAsync(new RouteOrder(testData.OrderEntity
                .WithNotes("My order notes")
                .Build()));

            // Assert
            testData.RouteRepository.Verify(repo => repo.GetRoute(3), Times.Once);
            return result;
        }

        [Test(ExpectedResult = true)]
        public async Task<bool> WakeUpRequest_RouteToSystemMessage()
        {
            // Arrange
            TestData testData = new TestData(2);

            // Act
            bool result = await testData.UseCase.ExecuteAsync(new RouteOrder(testData.OrderEntity
                .W(OrderType.RequestForWakeUp)
                .Build()));

            // Assert
            testData.RouteRepository.Verify(repo => repo.GetRoute(2), Times.Once);
            return result;
        }

        [Test(ExpectedResult = true)]
        public async Task<bool> DocumentOrderType_RouteOrderitemEmailDocumentRoute()
        {
            // Arrange
            TestData testData = new TestData(4);

            OrderitemEntity orderItem = CreateOrderItem(1, testData.OrderEntity).Build();

            OrderEntity order = testData.OrderEntity
                .W(OrderType.Document)
                .W(new[] {orderItem})
                .Build();

            // Act
            bool result = await testData.UseCase.ExecuteAsync(new RouteOrder(order));

            // Assert
            testData.RouteRepository.Verify(repo => repo.GetRoute(4), Times.Once);
            return result;
        }

        [Test(ExpectedResult = true)]
        public async Task<bool> OrderitemWithNotes_RouteToOrderNotesRoute()
        {
            // Arrange
            TestData testData = new TestData(3);

            OrderitemEntity orderItem = CreateOrderItem(1, testData.OrderEntity, notes: "Order item notes").Build();

            // Act
            bool result = await testData.UseCase.ExecuteAsync(new RouteOrder(testData.OrderEntity
                .WithNotes("")
                .W(new [] {orderItem})
                .Build()));

            // Assert
            testData.RouteRepository.Verify(repo => repo.GetRoute(3), Times.Once);
            return result;
        }

        [Test(ExpectedResult = true)]
        public async Task<bool> Orderitem_ProductHasRoute()
        {
            // Arrange
            TestData testData = new TestData(1);

            // Act
            bool result = await testData.UseCase.ExecuteAsync(new RouteOrder(testData.OrderEntity
                .W(CreateOrderItem(1, testData.OrderEntity, productId: 1))
                .Build()));

            // Assert
            testData.RouteRepository.Verify(repo => repo.GetRoute(50), Times.Once);
            return result;
        }

        [Test(ExpectedResult = true)]
        public async Task<bool> Orderitem_CategoryHasRoute()
        {
            // Arrange
            TestData testData = new TestData(1);

            OrderitemEntity orderItem = CreateOrderItem(1, testData.OrderEntity, categoryId: 1).Build();
            OrderEntity order = testData.OrderEntity
                .W(new[] {orderItem})
                .Build();

            // Act
            bool result = await testData.UseCase.ExecuteAsync(new RouteOrder(order));

            // Assert
            testData.RouteRepository.Verify(repo => repo.GetRoute(60), Times.Once);
            return result;
        }

        [Test(ExpectedResult = true)]
        public async Task<bool> Orderitem_DefaultRoute()
        {
            // Arrange
            TestData testData = new TestData(1);

            // Act
            bool result = await testData.UseCase.ExecuteAsync(new RouteOrder(testData.OrderEntity
                .W(CreateOrderItem(1, testData.OrderEntity))
                .Build()));

            // Assert
            testData.RouteRepository.Verify(repo => repo.GetRoute(1), Times.Once);
            return result;
        }

        [Test(ExpectedResult = true)]
        public async Task<bool> Orderitem_DefaultCompanyRoute()
        {
            // Arrange
            TestData testData = new TestData(5);

            // Act
            bool result = await testData.UseCase.ExecuteAsync(new RouteOrder(testData.OrderEntity
                .W(CreateOrderItem(1, testData.OrderEntity))
                .Build()));

            // Assert
            testData.RouteRepository.Verify(repo => repo.GetRoute(100), Times.Once);
            return result;
        }

        [Test(ExpectedResult = true)]
        public async Task<bool> Orderitem_MultipleWithDifferentRoutes()
        {
            // Arrange
            TestData testData = new TestData(1);

            // Act
            bool result = await testData.UseCase.ExecuteAsync(new RouteOrder(testData.OrderEntity
                .W(new[]
                {
                    CreateOrderItem(1, testData.OrderEntity).Build(),
                    CreateOrderItem(2, testData.OrderEntity, categoryId: 1).Build()
                })
                .Build()));

            // Assert
            testData.RouteRepository.Verify(repo => repo.GetRoute(It.IsAny<int>()), Times.Exactly(2));
            testData.SaveOrderUseCase.Verify(uc => uc.ExecuteAsync(It.Is<OrderEntity>(o => o.MasterOrderId == testData.OrderEntity.Build().OrderId)));
            return result;
        }

        private static ClientConfigurationEntityBuilder CreateClientConfigurationEntity(int clientId)
        {
            int routeId = clientId;
            RouteType routeType;
            switch (clientId)
            {
                case 1:
                    routeType = RouteType.Normal;
                    break;
                case 2:
                    routeType = RouteType.SystemMessage;
                    break;
                case 3:
                    routeType = RouteType.OrderNotes;
                    break;
                case 4:
                    routeType = RouteType.EmailDocument;
                    break;
                default:
                    routeType = RouteType.SystemMessage;
                    routeId = 999; // Doens't matter, should be skipped
                    break;
            }

            return A.ClientConfigurationEntity
                .WithClientConfigurationRoute(A.ClientConfigurationRouteEntity
                    .WithType(routeType)
                    .WithRoute(A.RouteEntity
                        .WithRouteId(routeId)));
        }

        private static OrderEntityBuilder CreateOrder(int clientId)
        {
            return A.OrderEntity
                .W((OrderId) 1)
                .WithClientId(clientId)
                .W(new OrderitemEntity[] {}) // No order items by default
                .WithCompanyId(1)
                .WithCompany(A.CompanyEntity
                    .WithId(1)
                    .WithName("UnitTest Company")
                    .WithRouteId(100)
                    .WithRoute(A.RouteEntity.WithRouteId(100)))
                .W((DeliverypointId) 1)
                .WithDeliverypoint(A.DeliverypointEntity
                    .WithDeliverypointgroup(A.DeliverypointgroupEntity
                        .WithId(1)
                        .WithName("UnitTest DPG")));
        }

        private OrderitemEntityBuilder CreateOrderItem(int orderItemId, OrderEntityBuilder parentOrder, int? productId = null, int? categoryId = null, string notes = "")
        {
            ProductEntity? product = productId.HasValue ? A.ProductEntity.W(productId.Value, 0).Build() : null;

            return A.OrderitemEntity
                .WithId(orderItemId)
                .WithParentOrder(parentOrder)
                .WithNotes(notes)
                .WithCategoryId(categoryId)
                .WithProduct(product)
                .W(OrderitemType.Product);
        }
        
        public class TestData
        {
            public TestData(int clientId)
            {
                this.UseCase = CreateUseCase(clientId);
                this.OrderEntity = CreateOrder(clientId);
            }

            public OrderEntityBuilder OrderEntity { get; private set; }
            public RouteOrderUseCase UseCase { get; private set; }

            public Mock<IWriteRouteUseCase> WriteRouteUseCase { get; private set; }
            public Mock<IRouteRepository> RouteRepository { get; private set; }
            public Mock<ISaveOrderUseCase> SaveOrderUseCase { get; private set; }

            private RouteOrderUseCase CreateUseCase(int clientId)
            {
                this.RouteRepository = A.RouteRepository.Build();
                this.RouteRepository.Setup(repo => repo.GetRoute(It.Is<int>(id => id != 999))).Returns<int>(i => A.RouteEntity.WithRouteId(i).Build());

                Mock<IProductRepository> productRepository = A.ProductRepository.Build();
                productRepository.Setup(repo => repo.GetProduct(1))
                    .Returns(A.ProductEntity.WithRouteId(50).Build);

                Mock<ICategoryRepository> categoryRepository = A.CategoryRepository.Build();
                categoryRepository.Setup(repo => repo.GetCategoryWithParentRoutes(1))
                    .Returns(A.CategoryEntity.WithRouteId(60).Build);

                Mock<IGetClientConfigurationUseCase> getClientConfigurationUseCase = A.GetClientConfigurationUseCase
                    .WithResponse(CreateClientConfigurationEntity(clientId))
                    .Build();

                Mock<IOrderitemRepository> orderitemRepository = A.OrderitemRepository.Build();
                orderitemRepository.Setup(repo => repo.GetOrderitems(It.IsAny<List<int>>()))
                    .Returns<List<int>>(orderitemIds =>
                    {
                        EntityCollection<OrderitemEntity> collection = new EntityCollection<OrderitemEntity>();
                        foreach (int id in orderitemIds)
                        {
                            collection.Add(new OrderitemEntity
                            {
                                OrderitemId = id
                            });
                        }
                        return collection;
                    });

                Mock<IOrderRepository> orderRepository = new Mock<IOrderRepository>();
                orderRepository.Setup(repo => repo.GetByIdWithPrefetchAsync(It.IsAny<int>())).ReturnsAsync((int id) => A.OrderEntity.W((OrderId)id).Build());

                this.SaveOrderUseCase = A.SaveOrderUseCase.Build();
                this.SaveOrderUseCase.Setup(uc => uc.ExecuteAsync(It.IsAny<OrderEntity>())).Returns<OrderEntity>(Task.FromResult);

                this.WriteRouteUseCase = A.WriteRouteUseCase.Build();

                return new RouteOrderUseCase(this.RouteRepository.Object,
                    orderRepository.Object,
                    orderitemRepository.Object,
                    productRepository.Object,
                    categoryRepository.Object,
                    getClientConfigurationUseCase.Object,
                    this.WriteRouteUseCase.Object,
                    this.SaveOrderUseCase.Object,
                    A.CreateReceiptUseCase.Build());
            }
        }
    }
}