using Crave.Enums;
using Crave.Ordering.Api.Core.Configuration;
using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Api.Domain.Tests.Builders.RepositoryBuilders;
using Crave.Ordering.Api.Domain.UseCases;
using Crave.Ordering.Api.Domain.UseCases.Requests;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.Tests.Builders;
using Crave.Ordering.Shared.Data.LLBLGen.Tests.DomainTypes;
using Crave.Ordering.Shared.OrderNotifications.Domain.UseCases.Requests;
using Crave.Ordering.Shared.OrderNotifications.Domain.UseCases.Responses;
using Crave.Ordering.Shared.OrderNotifications.Interfaces.UseCases;
using Crave.Ordering.Tests;
using NSubstitute;
using NUnit.Framework;
using System.Threading.Tasks;

namespace Crave.Ordering.Api.Domain.Tests.UseCases
{
    [TestFixture]
    public class HandleEmailOrderRoutestephandlerUseCaseTests : TestBase
    {
        private GetEmailAddressesForSupportpoolUseCase getEmailAddressesForSupportpoolUseCase;
        private ISendMailUseCase sendMailUseCase;
        private IOrderRepository orderRepository;
        private readonly string emailBodyHtml = @"<html>
                                                 <head>
                                                 <title>Test</title>
                                                 </head>
                                                 <body>
                                                 Room: [[DELIVERYPOINTNAME]]<br/>
                                                 Order: [[ORDER]]<br/>
                                                 Price: £[[PRICE]]<br/>
                                                 Time: [[CREATED]]<br/>
                                                 Notes: [[NOTES]]
                                                 </body>
                                                 </html>";

        private readonly string emailBodyWithImageHtml = @"<html>
                                                        <head>
                                                        <title>Test</title>
                                                        </head>
                                                        <body>
                                                        Room: [[DELIVERYPOINTNAME]]<br/>
                                                        Order: [[ORDER]]<br/>
                                                        Price: £[[PRICE]]<br/>
                                                        Time: [[CREATED]]<br/>
                                                        Notes: [[NOTES]]
                                                        <img src='tfooter.jpg' width='570' height='93'>
                                                        </body>
                                                        </html>";

        private const string AmazonS3BaseUrl = "https://cravecloud-test.s3-eu-west-1.amazonaws.com/";
        private const string EmailImageUrlFormat = "{0}files/generic/Unknown/routestephandlers/{1}-routestephandleremailresource-{2}w-{3}h-{4}l-{5}t-{6}.jpg";

        internal static class A
        {
            internal static OrderEntityBuilder OrderEntity => new OrderEntityBuilder(false);
            internal static OrderitemEntityBuilder OrderitemEntity => new OrderitemEntityBuilder();
            internal static OrderitemAlterationitemEntityBuilder OrderitemAlterationitemEntity => new OrderitemAlterationitemEntityBuilder();
            internal static RoutestephandlerRepositoryMockBuilder RoutestephandlerRepositoryMock => new RoutestephandlerRepositoryMockBuilder();
            internal static RoutestephandlerEntityBuilder RoutestephandlerEntity => new RoutestephandlerEntityBuilder();
            internal static MediaEntityBuilder MediaEntity => new MediaEntityBuilder();
            internal static MediaRatioTypeMediaEntityBuilder MediaRatioTypeMediaEntity => new MediaRatioTypeMediaEntityBuilder();
        }

        [OneTimeSetUp]
        public void _OneTimeSetUp()
        {
            getEmailAddressesForSupportpoolUseCase = CreateSubstitute<GetEmailAddressesForSupportpoolUseCase>();
            sendMailUseCase = CreateSubstitute<ISendMailUseCase>();
            orderRepository = CreateSubstitute<IOrderRepository>();

            orderRepository.GetOrderForNotification(Arg.Any<int>()).Returns(A.OrderEntity
                .W(A.OrderitemEntity
                    .W((PriceTotalInTax)12.5M)
                    .W(OrderitemType.Product)
                    .W((Quantity)1)
                    .W(new[]
                    {
                        A.OrderitemAlterationitemEntity
                            .W("AlterationName", "OptionName")
                            .W(5.2M).Build()
                    })).Build());

            sendMailUseCase.ExecuteAsync(Arg.Any<SendMailRequest>()).Returns(new MessageQueueReponse { Success = true });
        }

        [Test]
        public async Task Execute_RoutestepWithCustomEmailBody_OrderContainsAlterations()
        {
            // Arrange
            OrderRoutestephandlerEntity orderRoutestephandlerEntity = new OrderRoutestephandlerEntity
            {
                IsNew = false,
                HandlerType = RoutestephandlerType.EmailOrder,
                FieldValue1 = "to@mail.com",
                FieldValue4 = emailBodyHtml, // Custom body text
                FieldValue5 = "1" // use custom body
            };

            var sut = new HandleEmailOrderRoutestephandlerUseCase(new AmazonConfiguration { S3 = new AmazonS3Configuration { BaseUrl = AmazonS3BaseUrl } },
                                                                  A.RoutestephandlerRepositoryMock.Build(),
                                                                  getEmailAddressesForSupportpoolUseCase,
                                                                  sendMailUseCase,
                                                                  orderRepository);

            await sut.ExecuteAsync(new HandleNonTerminalRoutingstephandler
            {
                OrderEntity = A.OrderEntity.W((OrderId)1).Build(),
                OrderRoutestephandlerEntity = orderRoutestephandlerEntity
            });

            await sendMailUseCase.Received(1).ExecuteAsync(Arg.Is<SendMailRequest>(x => x.Message.Contains("AlterationName") && x.Message.Contains("OptionName")));
        }

        [Test]
        public async Task Execute_RoutestepWithCustomEmailBodyContainingMedia_MediaUrlIsCorrect()
        {
            // Arrange
            int routestephandlerId = 1;
            int mediaRatioTypeMediaId = 22032022;
            int top = 0;
            int left = 40;
            int height = 93;
            int width = 570;
            long ticks = 637835390463493528;

            OrderRoutestephandlerEntity orderRoutestephandlerEntity = new OrderRoutestephandlerEntity
            {
                IsNew = false,
                HandlerType = RoutestephandlerType.EmailOrder,
                FieldValue1 = "to@mail.com",
                FieldValue4 = emailBodyWithImageHtml, // Custom body text
                FieldValue5 = "1", // use custom body
                OriginatedFromRoutestepHandlerId = routestephandlerId
            };

            var sut = new HandleEmailOrderRoutestephandlerUseCase(new AmazonConfiguration { S3 = new AmazonS3Configuration { BaseUrl = AmazonS3BaseUrl } },
                                                                  A.RoutestephandlerRepositoryMock.W(
                                                                      A.RoutestephandlerEntity
                                                                      .W(routestephandlerId)
                                                                      .W(
                                                                          A.MediaEntity
                                                                          .WithName("tfooter.jpg")
                                                                          .WithMimeType("image/jpeg")
                                                                          .WithFilePathRelativeToMediaPath("tfooter.jpg")
                                                                          .W(
                                                                              A.MediaRatioTypeMediaEntity
                                                                              .W(mediaRatioTypeMediaId)
                                                                              .W(MediaType.RouteStepHandlerEmailResource)
                                                                              .WithTop(top)
                                                                              .WithLeft(left)
                                                                              .WithHeight(height)
                                                                              .WithWidth(width)
                                                                              .WithTicks(ticks)))
                                                                      .Build())
                                                                  .Build(),
                                                                  getEmailAddressesForSupportpoolUseCase,
                                                                  sendMailUseCase,
                                                                  orderRepository);


            await sut.ExecuteAsync(new HandleNonTerminalRoutingstephandler
            {
                OrderEntity = A.OrderEntity.W((OrderId)1).Build(),
                OrderRoutestephandlerEntity = orderRoutestephandlerEntity
            });

            await sendMailUseCase.Received(1).ExecuteAsync(Arg.Is<SendMailRequest>(x => x.Message.Contains(string.Format(EmailImageUrlFormat, AmazonS3BaseUrl, mediaRatioTypeMediaId, width, height, left, top, ticks))));
        }
    }
}