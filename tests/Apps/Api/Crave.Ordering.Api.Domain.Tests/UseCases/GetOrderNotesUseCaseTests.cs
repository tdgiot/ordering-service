﻿using System;
using System.Globalization;
using Crave.Enums;
using Crave.Ordering.Api.Data.Abstractions.Repositories;
using Crave.Ordering.Api.Domain.Interfaces.UseCases;
using Crave.Ordering.Api.Domain.Tests.Builders.RepositoryBuilders;
using Crave.Ordering.Api.Domain.Tests.Builders.UseCaseBuilders;
using Crave.Ordering.Api.Domain.UseCases;
using Crave.Ordering.Api.Domain.UseCases.Requests;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.Tests.Builders;
using Crave.Ordering.Tests;
using NSubstitute;
using NUnit.Framework;

namespace Crave.Ordering.Api.Domain.Tests.UseCases
{
    public class GetOrderNotesUseCaseTests : TestBase
    {
        private static class A
        {
            public static GetOrderNotesUseCaseBuilder GetOrderNotesUseCase => new GetOrderNotesUseCaseBuilder();
            public static OrderEntityBuilder OrderEntity => new OrderEntityBuilder();
            public static OrderitemEntityBuilder OrderitemEntity => new OrderitemEntityBuilder();
            public static AlterationRepositoryMockBuilder AlterationRepositoryMock => new AlterationRepositoryMockBuilder();
            public static AlterationEntityBuilder AlterationEntity => new AlterationEntityBuilder();
            public static OrderitemAlterationitemEntityBuilder OrderitemAlterationitemEntity => new OrderitemAlterationitemEntityBuilder();
        }

        private ICompanyRepository companyRepository;
        private IDeliverypointgroupRepository deliverypointgroupRepository;
        private IAlterationRepository alterationRepository;

        private GetOrderNotesUseCase useCase;

        [OneTimeSetUp]
        public void _OneTimeSetUp()
        {
            this.companyRepository = this.CreateSubstitute<ICompanyRepository>();
            this.companyRepository.GetIncludeDeliveryTimeInOrderNotes(1).Returns(true);
            this.companyRepository.GetIncludeDeliveryTimeInOrderNotes(2).Returns(false);
            this.companyRepository.GetClockMode(3).Returns(ClockMode.AmPm);

            this.deliverypointgroupRepository = this.CreateSubstitute<IDeliverypointgroupRepository>();
            this.deliverypointgroupRepository.GetEstimatedDeliveryTimeForDeliverypoint(1).Returns(30);

            this.alterationRepository = this.CreateSubstitute<IAlterationRepository>();

            this.useCase = new GetOrderNotesUseCase(this.companyRepository, this.deliverypointgroupRepository, this.alterationRepository);
        }

        protected override void _SetUp()
        {
            this.alterationRepository.GetAlteration(1).Returns(new AlterationEntity
            {
                IsNew = false,
                AlterationId = 1,
                Type = AlterationType.Options,
                OrderLevelEnabled = true
            });
            this.alterationRepository.GetAlteration(2).Returns(new AlterationEntity
            {
                IsNew = false,
                AlterationId = 2,
                Type = AlterationType.Options,
                OrderLevelEnabled = true
            });
        }

        [TestCase(1, 1, true)]
        [TestCase(1, 2, false)]
        [TestCase(2, 1, false)]
        public void AddEstimatedDeliveryTimeToOrderNotes(int companyId, int deliverypointId, bool notesShouldBeAdded)
        {
            OrderEntity entity = new OrderEntity
            {
                CompanyId = companyId,
                DeliverypointId = deliverypointId
            };
            string result = this.useCase.Execute(new GetOrderNotes { Order = entity });

            if (notesShouldBeAdded)
            {
                StringAssert.Contains("Wait Time: 30 minutes", result);
            }
            else
            {
                StringAssert.DoesNotContain("Wait Time: 30 minutes", result);
            }
        }

        [Theory]
        public void AlterationTypes(AlterationType type)
        {
            this.alterationRepository.GetAlteration(Arg.Any<int>()).Returns(new AlterationEntity
            {
                IsNew = false,
                AlterationId = 1,
                OrderLevelEnabled = true,
                Type = type,
                Name = $"Alteration-{type}"
            });
            string orderNotes = this.useCase.Execute(new GetOrderNotes { Order = this.CreateOrder(1, 3) });

            this.companyRepository.GetCompanyEntity(Arg.Any<int>()).Returns(new CompanyEntity
            {
                ClockMode = ClockMode.AmPm,
                TimeZoneOlsonId = Globalization.TimeZone.Europe_Amsterdam.OlsonTimeZoneId
            });

            DateTime localTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow.Date.AddHours(15).AddMinutes(20), Globalization.TimeZone.Europe_Amsterdam.TimeZoneInfo);

            switch (type)
            {
                case AlterationType.Options:
                    StringAssert.Contains("Option name", orderNotes);
                    break;
                case AlterationType.DateTime:
                    StringAssert.Contains(localTime.ToString("d MMMM hh:mm tt", CultureInfo.InvariantCulture), orderNotes);
                    break;
                case AlterationType.Date:
                    StringAssert.Contains($"{localTime:d MMMM}", orderNotes);
                    break;
                case AlterationType.Form:
                    StringAssert.Contains("Just a value", orderNotes);
                    break;
                case AlterationType.Email:
                case AlterationType.Numeric:
                case AlterationType.Text:
                    StringAssert.Contains($"Alteration-{type}: Just a value", orderNotes);
                    break;
                default:
                    Assert.That(orderNotes, Is.Empty);
                    break;
            }
        }

        [Theory]
        public void DateTimeAlterationClockMode(ClockMode clockMode)
        {
            this.alterationRepository.GetAlteration(Arg.Any<int>()).Returns(new AlterationEntity
            {
                IsNew = false,
                AlterationId = 1,
                OrderLevelEnabled = true,
                Type = AlterationType.DateTime,
                Name = "Alteration-DT"
            });

            this.companyRepository.GetClockMode(4).Returns(clockMode);

            this.companyRepository.GetCompanyEntity(Arg.Any<int>()).Returns(new CompanyEntity
            {
                ClockMode = clockMode,
                TimeZoneOlsonId = Globalization.TimeZone.Europe_Amsterdam.OlsonTimeZoneId
            });

            string orderNotes = this.useCase.Execute(new GetOrderNotes { Order = this.CreateOrder(1, 4) });

            DateTime localTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow.Date.AddHours(15).AddMinutes(20), Globalization.TimeZone.Europe_Amsterdam.TimeZoneInfo);

            switch (clockMode)
            {
                case ClockMode.Hour24:
                    StringAssert.Contains($"{localTime:d MMMM HH:mm}", orderNotes);
                    break;
                case ClockMode.AmPm:
                    StringAssert.Contains(localTime.ToString("d MMMM hh:mm tt", CultureInfo.InvariantCulture), orderNotes);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(clockMode), clockMode, null);
            }
        }

        [Test]
        public void Execute_WithOrderLevelEnabledTextAlteration_AddsTheTextAsNoteToTheOrderOnce()
        {
            // Arrange
            const int alterationId = 81345;
            const bool orderLevelEnabled = true;
            const string alterationName = "Order Notes";
            const string alterationOptionName = alterationName;
            const string orderNoteText = "No tomato or coleslaw on the burgers";

            string expectedOrderNotes = $"{alterationName}: {alterationOptionName}: {orderNoteText}{'\n'}{'\n'}";

            IGetOrderNotesUseCase sut = A.GetOrderNotesUseCase
                .W(A.AlterationRepositoryMock
                    .W(A.AlterationEntity
                        .WithId(alterationId)
                        .WithName(alterationName)
                        .WithType(AlterationType.Form)
                        .Build())
                    .Build())
                .Build();

            GetOrderNotes request = new GetOrderNotes 
            {
                Order = A.OrderEntity.W(new []
                {
                    A.OrderitemEntity
                        .W(new[]
                        {
                            A.OrderitemAlterationitemEntity
                                .WithId(17943075)
                                .W(orderLevelEnabled)
                                .W((int?)alterationId)
                                .W(alterationName, alterationOptionName)
                                .W(orderNoteText)
                                .Build()
                        })
                        .Build(),
                    A.OrderitemEntity
                        .W(new OrderitemAlterationitemEntity[0] )
                        .Build(),
                    A.OrderitemEntity
                        .W(new OrderitemAlterationitemEntity[0] )
                        .Build(),
                }).Build()
            };

            // Act
            string orderNotes = sut.Execute(request);

            // Assert
            Assert.AreEqual(expectedOrderNotes, orderNotes);
        }

        [Test]
        public void MultipleAlterationNotesAreSeparatedByComma()
        {
            OrderEntity orderEntity = this.CreateOrder(1, 3);
            orderEntity.OrderitemCollection[0].OrderitemAlterationitemCollection.Add(new OrderitemAlterationitemEntity
            {
                AlterationitemId = 1,
                AlterationId = 1,
                Value = "Just a value",
                AlterationoptionName = "Option name #2",
                OrderLevelEnabled = true
            });
            string orderNotes = this.useCase.Execute(new GetOrderNotes { Order = orderEntity });

            StringAssert.Contains(", ", orderNotes);
        }

        private OrderEntity CreateOrder(int orderId, int companyId)
        {
            OrderitemAlterationitemEntity orderitemAlterationitemEntity = new OrderitemAlterationitemEntity
            {
                AlterationitemId = 1,
                AlterationId = 1,
                Value = "Just a value",
                AlterationoptionName = "Option name",
                OrderLevelEnabled = true,
                TimeUTC = DateTime.UtcNow.Date.AddHours(15).AddMinutes(20)
            };
            OrderitemEntity orderitemEntity = new OrderitemEntity
            {
                OrderitemAlterationitemCollection = { orderitemAlterationitemEntity }
            };
            OrderEntity orderEntity = new OrderEntity
            {
                OrderId = orderId,
                CompanyId = companyId,
                OrderitemCollection = { orderitemEntity }
            };
            return orderEntity;
        }
    }
}
