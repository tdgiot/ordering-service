﻿using Crave.Ordering.Api.Core.Enums;
using Crave.Ordering.Api.Domain.Exceptions;
using Crave.Ordering.Api.Domain.Tests.Builders.EntityBuilders;
using Crave.Ordering.Api.Domain.Tests.Builders.ValidationBuilders;
using Crave.Ordering.Api.Domain.Validation;
using Crave.Ordering.Api.Domain.Validation.Errors;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.Tests.Builders;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Crave.Ordering.Api.Domain.Tests.Validation
{
    public class OutletValidatorTest
	{
		private static class A
		{
			public static OrderEntityBuilder OrderEntity => new OrderEntityBuilder();
			public static OutletEntityBuilder OutletEntity => new OutletEntityBuilder();
			public static OutletOperationalStateEntityBuilder OutletOperationalStateEntity => new OutletOperationalStateEntityBuilder();
			public static BusinesshourEntityBuilder BusinesshourEntity => new BusinesshourEntityBuilder();
		}


		[Test]
		public void Validate_WithNullOrderEntity_Throws()
		{
			static void Action()
			{
				// Act
				OutletValidator.Validate(null!, new List<ValidationError>());
			}

			// Assert
			_ = Assert.Throws<NullReferenceException>(Action);
		}

		[Test]
		public void Validate_WithNewOrderEntity_ReturnsNoError()
		{
			static void Action()
			{
				// Act
				OutletValidator.Validate(A.OrderEntity
					.W(A.OutletEntity.AsNew())
					.Build(), new List<ValidationError>());
			}

			// Assert
            Assert.DoesNotThrow(Action);
		}

		[Test]
		public void Validate_WithoutOutletOperationalStateEntity_Throws()
		{
			static void Action()
			{
				// Act
				OutletValidator.Validate(A.OrderEntity
					.W(A.OutletEntity.W((OutletOperationalStateEntity)null))
					.Build(), new List<ValidationError>());
			}

			// Assert
			_ = Assert.Throws<CheckoutValidationException>(Action);
		}

		[Test]
		public void Validate_WithOrderIntakeDisabled_ReturnsError()
		{
			// Arrange
			var validationErrors = new List<ValidationError>();

			// Act
			OutletValidator.Validate(A.OrderEntity
				.W(A.OutletEntity
					.W(A.OutletOperationalStateEntity
						.W(true))
				.W(Enumerable.Empty<BusinesshourEntity>()))
				.Build(), validationErrors);


			// Assert
			Assert.AreEqual(1, validationErrors.Count);
			Assert.AreEqual(ValidationErrorSubType.OrderIntakeDisabled, validationErrors[0].ValidationErrorSubType);
		}

		[Test]
		public void Validate_WithNoBusinessHoursSpecified_ReturnsNoError()
		{
			// Arrange
			var validationErrors = new List<ValidationError>();

			// Act
			OutletValidator.Validate(A.OrderEntity
				.W(A.OutletEntity
					.W(A.OutletOperationalStateEntity)
					.W(Enumerable.Empty<BusinesshourEntity>()))
				.Build(), validationErrors);

			// Assert
			Assert.AreEqual(0, validationErrors.Count);
		}

		[Test]
		public void Validate_WithBusinessHoursSpecifiedAndInsideBusinessHours_ReturnsNoError()
		{
			// Arrange
			TimeZoneOlsonId timezoneOlsonId = "Europe/Amsterdam";
			var validationErrors = new List<ValidationError>();

			// Act
			OutletValidator.Validate(A.OrderEntity
				.W(A.OutletEntity
					.W(A.OutletOperationalStateEntity)
					.W(new [] 
					{
						A.BusinesshourEntity.W(true).W("00000").Build(), 
						A.BusinesshourEntity.W(false).W("02359").Build(),
						A.BusinesshourEntity.W(true).W("10000").Build(), 
						A.BusinesshourEntity.W(false).W("12359").Build(),
						A.BusinesshourEntity.W(true).W("20000").Build(), 
						A.BusinesshourEntity.W(false).W("22359").Build(),
						A.BusinesshourEntity.W(true).W("30000").Build(), 
						A.BusinesshourEntity.W(false).W("32359").Build(),
						A.BusinesshourEntity.W(true).W("40000").Build(), 
						A.BusinesshourEntity.W(false).W("42359").Build(),
						A.BusinesshourEntity.W(true).W("50000").Build(), 
						A.BusinesshourEntity.W(false).W("52359").Build(),
						A.BusinesshourEntity.W(true).W("60000").Build(), 
						A.BusinesshourEntity.W(false).W("62359").Build(),

					}))
				.W(timezoneOlsonId)
				.Build(), validationErrors);

			// Assert
			Assert.AreEqual(0, validationErrors.Count);
		}

		[Test]
		public void Validate_WithBusinessHoursSpecifiedAndOutsideBusinessHours_ReturnsError()
		{
			// Arrange
			TimeZoneOlsonId timezoneOlsonId = "Europe/Amsterdam";
			var validationErrors = new List<ValidationError>();

			// Act
			OutletValidator.Validate(A.OrderEntity
				.W(A.OutletEntity
					.W(A.OutletOperationalStateEntity)
					.W(new[] 
					{
						A.BusinesshourEntity.W(true).W("00000").Build(), 
						A.BusinesshourEntity.W(false).W("00000").Build(),
					}))
				.W(timezoneOlsonId)
				.Build(), validationErrors);

			// Assert
			Assert.AreEqual(1, validationErrors.Count);
			Assert.AreEqual(ValidationErrorSubType.OutletClosedDueBusinessHours, validationErrors[0].ValidationErrorSubType);
		}

		[Test]
		public void Validate_WithAnOddAmountOfBusinessHours_Throws()
		{
			static void Act()
			{
				// Arrange
				TimeZoneOlsonId timezoneOlsonId = "Europe/Amsterdam";
				var validationErrors = new List<ValidationError>();

				// Act
				OutletValidator.Validate(A.OrderEntity
					.W(A.OutletEntity
						.W(A.OutletOperationalStateEntity)
						.W(new[]
						{
							A.BusinesshourEntity.W(true).W("00000").Build(),
							A.BusinesshourEntity.W(true).W("10000").Build(),
							A.BusinesshourEntity.W(true).W("20000").Build(),
							A.BusinesshourEntity.W(true).W("30000").Build(),
							A.BusinesshourEntity.W(true).W("40000").Build(),
							A.BusinesshourEntity.W(true).W("50000").Build(),
							A.BusinesshourEntity.W(true).W("60000").Build(),
						}))
					.W(timezoneOlsonId)
					.Build(), validationErrors);
			}

			// Assert
			_ = Assert.Throws<InvalidOperationException>(Act);
		}

		[Test]
		public void Validate_WithInvalidOpeningTimeInDayAndTimeStrings_Throws()
		{
			static void Act()
			{
				// Arrange
				TimeZoneOlsonId timezoneOlsonId = "Europe/Amsterdam";
				var validationErrors = new List<ValidationError>();

				// Act
				OutletValidator.Validate(A.OrderEntity
					.W(A.OutletEntity
						.W(A.OutletOperationalStateEntity)
						.W(new[]
						{
							A.BusinesshourEntity.W(true).W("03000").Build(),
							A.BusinesshourEntity.W(false).W("00000").Build(),
							A.BusinesshourEntity.W(true).W("13000").Build(),
							A.BusinesshourEntity.W(false).W("10000").Build(),
							A.BusinesshourEntity.W(true).W("23000").Build(),
							A.BusinesshourEntity.W(false).W("20000").Build(),
							A.BusinesshourEntity.W(true).W("33000").Build(),
							A.BusinesshourEntity.W(false).W("30000").Build(),
							A.BusinesshourEntity.W(true).W("43000").Build(),
							A.BusinesshourEntity.W(false).W("40000").Build(),
							A.BusinesshourEntity.W(true).W("53000").Build(),
							A.BusinesshourEntity.W(false).W("50000").Build(),
							A.BusinesshourEntity.W(true).W("63000").Build(),
							A.BusinesshourEntity.W(false).W("60000").Build(),
						}))
					.W(timezoneOlsonId)
					.Build(), validationErrors);
			}

			// Assert
			_ = Assert.Throws<InvalidCastException>(Act);
		}

		[Test]
		public void Validate_WithInvalidClosingTimeInDayAndTimeStrings_Throws()
		{
			static void Act()
			{
				// Arrange
				TimeZoneOlsonId timezoneOlsonId = "Europe/Amsterdam";
				var validationErrors = new List<ValidationError>();

				// Act
				OutletValidator.Validate(A.OrderEntity
					.W(A.OutletEntity
						.W(A.OutletOperationalStateEntity)
						.W(new[]
						{
							A.BusinesshourEntity.W(true).W("00000").Build(),
							A.BusinesshourEntity.W(false).W("03000").Build(),
							A.BusinesshourEntity.W(true).W("10000").Build(),
							A.BusinesshourEntity.W(false).W("13000").Build(),
							A.BusinesshourEntity.W(true).W("20000").Build(),
							A.BusinesshourEntity.W(false).W("23000").Build(),
							A.BusinesshourEntity.W(true).W("30000").Build(),
							A.BusinesshourEntity.W(false).W("33000").Build(),
							A.BusinesshourEntity.W(true).W("40000").Build(),
							A.BusinesshourEntity.W(false).W("43000").Build(),
							A.BusinesshourEntity.W(true).W("50000").Build(),
							A.BusinesshourEntity.W(false).W("53000").Build(),
							A.BusinesshourEntity.W(true).W("60000").Build(),
							A.BusinesshourEntity.W(false).W("63000").Build(),
						}))
					.W(timezoneOlsonId)
					.Build(), validationErrors);
			}

			// Assert
			_ = Assert.Throws<InvalidCastException>(Act);
		}

		[Test]
		public void Validate_WithoutTimeZoneOlsonId_Throws()
		{
			static void Act()
			{
				// Arrange
				TimeZoneOlsonId timezoneOlsonId = " ";
				var validationErrors = new List<ValidationError>();

				// Act
				OutletValidator.Validate(A.OrderEntity
					.W(timezoneOlsonId)
					.Build(), validationErrors);
			}

			// Assert
			_ = Assert.Throws<ArgumentNullException>(Act);
		}
		
		[Test]
		public void Validate_WithNoCustomerNameAndNoCustomerDetailsRequired_ReturnsNoError()
		{
			// Arrange
			var validationErrors = new List<ValidationError>();

			// Act
			OutletValidator.Validate(A.OrderEntity
				.W(A.OutletEntity
					.W(A.OutletOperationalStateEntity)
					.W(Enumerable.Empty<BusinesshourEntity>()))
				.W((CustomerLastname) string.Empty)
				.Build(), validationErrors);

			// Assert
			Assert.AreEqual(0, validationErrors.Count);
		}
		
		[Test]
		public void Validate_WithNoCustomerNameAndCustomerDetailsRequired_ReturnsError()
		{
			// Arrange
			var validationErrors = new List<ValidationError>();

			// Act
			OutletValidator.Validate(A.OrderEntity
				.W(A.OutletEntity
					.W((CustomerDetailsNameRequired) true)
					.W(A.OutletOperationalStateEntity)
					.W(Enumerable.Empty<BusinesshourEntity>()))
				.W((CustomerLastname) string.Empty)
				.Build(), validationErrors);

			// Assert
			Assert.AreEqual(1, validationErrors.Count);
			Assert.AreEqual(ValidationErrorSubType.CustomerLastNameInvalid, validationErrors[0].ValidationErrorSubType);
		}
		
		[Test]
		public void Validate_WithNoCustomerEmailAndNoCustomerDetailsRequired_ReturnsNoError()
		{
			// Arrange
			var validationErrors = new List<ValidationError>();

			// Act
			OutletValidator.Validate(A.OrderEntity
				.W(A.OutletEntity
					.W(A.OutletOperationalStateEntity)
					.W(Enumerable.Empty<BusinesshourEntity>()))
				.W((CustomerEmail) string.Empty)
				.Build(), validationErrors);

			// Assert
			Assert.AreEqual(0, validationErrors.Count);
		}
		
		[Test]
		public void Validate_WithNoCustomerEmailAndCustomerDetailsRequired_ReturnsError()
		{
			// Arrange
			var validationErrors = new List<ValidationError>();

			// Act
			OutletValidator.Validate(A.OrderEntity
				.W(A.OutletEntity
					.W((CustomerDetailsEmailRequired) true)
					.W(A.OutletOperationalStateEntity)
					.W(Enumerable.Empty<BusinesshourEntity>()))
				.W((CustomerEmail) string.Empty)
				.Build(), validationErrors);

			// Assert
			Assert.AreEqual(1, validationErrors.Count);
			Assert.AreEqual(ValidationErrorSubType.CustomerEmailRequired, validationErrors[0].ValidationErrorSubType);
		}

		[Test]
		public void Validate_WithInvalidCustomerEmailAndCustomerDetailsRequired_ReturnsError()
		{
			// Arrange
			var validationErrors = new List<ValidationError>();

			// Act
			OutletValidator.Validate(A.OrderEntity
				.W(A.OutletEntity
					.W((CustomerDetailsEmailRequired) true)
					.W(A.OutletOperationalStateEntity)
					.W(Enumerable.Empty<BusinesshourEntity>()))
				.W((CustomerEmail) "invalid@emailaddress")
				.Build(), validationErrors);

			// Assert
			Assert.AreEqual(1, validationErrors.Count);
			Assert.AreEqual(ValidationErrorSubType.CustomerEmailInvalid, validationErrors[0].ValidationErrorSubType);
		}
		
		[Test]
		public void Validate_WithNoCustomerPhoneNumberAndNoCustomerDetailsRequired_ReturnsNoError()
		{
			// Arrange
			var validationErrors = new List<ValidationError>();

			// Act
			OutletValidator.Validate(A.OrderEntity
				.W(A.OutletEntity
					.W(A.OutletOperationalStateEntity)
					.W(Enumerable.Empty<BusinesshourEntity>()))
				.W((CustomerPhoneNumber) string.Empty)
				.Build(), validationErrors);

			// Assert
			Assert.AreEqual(0, validationErrors.Count);
		}
		
		[Test]
		public void Validate_WithNoCustomerPhoneNumberAndCustomerDetailsRequired_ReturnsError()
		{
			// Arrange
			var validationErrors = new List<ValidationError>();

			// Act
			OutletValidator.Validate(A.OrderEntity
				.W(A.OutletEntity
					.W((CustomerDetailsPhoneRequired) true)
					.W(A.OutletOperationalStateEntity)
					.W(Enumerable.Empty<BusinesshourEntity>()))
				.W((CustomerPhoneNumber) string.Empty)
				.Build(), validationErrors);

			// Assert
			Assert.AreEqual(1, validationErrors.Count);
			Assert.AreEqual(ValidationErrorSubType.CustomerPhoneRequired, validationErrors[0].ValidationErrorSubType);
		}

		[Test]
		public void Validate_WithInvalidCustomerPhoneNumberAndCustomerDetailsRequired_ReturnsError()
		{
			// Arrange
			var validationErrors = new List<ValidationError>();

			// Act
			OutletValidator.Validate(A.OrderEntity
				.W(A.OutletEntity
					.W((CustomerDetailsPhoneRequired) true)
					.W(A.OutletOperationalStateEntity)
					.W(Enumerable.Empty<BusinesshourEntity>()))
				.W((CustomerEmail) "+44123")
				.Build(), validationErrors);

			// Assert
			Assert.AreEqual(1, validationErrors.Count);
			Assert.AreEqual(ValidationErrorSubType.CustomerPhoneInvalid, validationErrors[0].ValidationErrorSubType);
		}
	}
}
