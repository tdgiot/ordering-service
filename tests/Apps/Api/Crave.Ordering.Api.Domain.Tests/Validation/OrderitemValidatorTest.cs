using Crave.Enums;
using Crave.Ordering.Api.Core.Enums;
using Crave.Ordering.Api.Domain.Exceptions;
using Crave.Ordering.Api.Domain.Extensions;
using Crave.Ordering.Api.Domain.Tests.Builders;
using Crave.Ordering.Api.Domain.Tests.Builders.EntityBuilders;
using Crave.Ordering.Api.Domain.Tests.Builders.ValidationBuilders;
using Crave.Ordering.Api.Domain.UseCases.Requests;
using Crave.Ordering.Api.Domain.Validation;
using Crave.Ordering.Api.Domain.Validation.Errors;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.Tests.Builders;
using Crave.Ordering.Shared.Data.LLBLGen.Tests.DomainTypes;
using NodaTime;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Crave.Ordering.Api.Domain.Tests.Validation
{
    public class OrderitemValidatorTest
    {
        private static class A
        {
            public static OrderEntityBuilder OrderEntity => new OrderEntityBuilder();
            public static OrderitemEntityBuilder OrderitemEntity => new OrderitemEntityBuilder();
            public static OrderitemBuilder Orderitem => new OrderitemBuilder();
            public static ProductEntityBuilder ProductEntity => new ProductEntityBuilder();
            public static ScheduleEntityBuilder ScheduleEntity => new ScheduleEntityBuilder();
            public static ScheduleitemEntityBuilder ScheduleitemEntity => new ScheduleitemEntityBuilder();
            public static TimeZoneOlsonIdBuilder TimeZoneOlsonId => new TimeZoneOlsonIdBuilder();
            public static AlterationEntityBuilder AlterationEntity => new AlterationEntityBuilder();
            public static AlterationitemBuilder Alterationitem => new AlterationitemBuilder();
        }

        [Test]
        public void Validate_WithNullOrderEntity_Throws()
        {
            static void Action()
            {
                // Act
                OrderitemValidator.Validate(null!, Enumerable.Empty<OrderitemRequest>(), new List<ValidationError>(), true);
            }

            // Assert
            _ = Assert.Throws<NullReferenceException>(Action);
        }

        [Test]
        public void Validate_WithNullOrderItemEntities_Throws()
        {
            static void Action()
            {
                // Act
                OrderitemValidator.Validate(A.OrderEntity.Build(), null!, new List<ValidationError>(), true);
            }

            // Assert
            var exception = Assert.Throws<CheckoutValidationException>(Action);
            Assert.AreEqual(ValidationErrorSubType.OrderitemsRequired, exception.CheckoutValidationResponse.ValidationErrors.Single().ValidationErrorSubType);
        }

        [Test]
        public void Validate_WithoutAnyOrderItemsOfTypeProductAvailable_Throws()
        {
            static void Action()
            {
                // Arrange
                var orderEntity = A.OrderEntity
                                   .W(A.OrderitemEntity
                                       .W(OrderitemType.Tip))
                                   .Build();

                var validationErrors = new List<ValidationError>();

                // Act
                OrderitemValidator.Validate(orderEntity, Enumerable.Empty<OrderitemRequest>(), validationErrors, true);
            }

            // Assert
            var exception = Assert.Throws<CheckoutValidationException>(Action);
            Assert.AreEqual(ValidationErrorSubType.OrderitemsRequired, exception.CheckoutValidationResponse.ValidationErrors.Single().ValidationErrorSubType);
        }

        [Test]
        public void Validate_WithTheOrderItemNotExisting_Throws()
        {
            static void Action()
            {
                // Arrange
                var validationErrors = new List<ValidationError>();

                // Act
                OrderitemValidator.Validate(A.OrderEntity.Build(), Enumerable.Empty<OrderitemRequest>(), validationErrors, true);
            }

            // Assert
            var exception = Assert.Throws<CheckoutValidationException>(Action);
            Assert.AreEqual(ValidationErrorSubType.OrderitemsRequired, exception.CheckoutValidationResponse.ValidationErrors.Single().ValidationErrorSubType);
        }

        [Test]
        public void Validate_WithIncorrectOrderitemPrice_ReturnsError()
        {
            // Arrange
            var validationErrors = new List<ValidationError>();

            const int productId = 1;
            var orderItems = new[]
            {
                A.Orderitem
                    .W(1M)
                    .W(productId)
                .Build()
            };

            // Act
            OrderitemValidator.Validate(A.OrderEntity
                .W(A.OrderitemEntity
                    .W(orderItems[0].TempInternalGuid)
                    .W((PriceTotalInTax)2M)
                    .W(A.ProductEntity
                        .WithId(productId)
                        .W((ScheduleEntity)null)))
                .Build(),
                orderItems,
                validationErrors,
                true);

            // Assert
            Assert.AreEqual(1, validationErrors.Count);
            Assert.AreEqual(ValidationErrorSubType.ProductPriceInvalid, validationErrors[0].ValidationErrorSubType);
        }

        [Test]
        public void Validate_WithCorrectOrderitemPrice_ReturnsNoError()
        {
            // Arrange
            var validationErrors = new List<ValidationError>();

            const int productId = 1;
            var orderItems = new[]
            {
                A.Orderitem
                    .W(1M)
                    .W(productId)
                .Build()
            };

            // Act
            OrderitemValidator.Validate(A.OrderEntity
                .W(A.OrderitemEntity
                    .W(orderItems[0].TempInternalGuid)
                    .W((PriceTotalInTax)1M)
                    .W((ProductPriceIn)1M)
                    .W((int?)productId)
                    .W(A.ProductEntity
                        .W((ScheduleEntity)null)))
                .WithClientId(null)
                .Build(),
                orderItems,
                validationErrors,
                true);

            // Assert
            Assert.AreEqual(0, validationErrors.Count);
        }

        [Test]
        public void Validate_MultipleOrderItemsWithSameProduct_ReturnsNoError()
        {
            // Arrange
            var validationErrors = new List<ValidationError>();

            const int productId = 1;
            var orderItems = new[]
            {
                A.Orderitem
                    .W(1M)
                    .W(productId)
                    .Build(),
                A.Orderitem
                    .W(2M)
                    .W(productId)
                    .Build()
            };

            // Act
            OrderitemValidator.Validate(A.OrderEntity
                    .W(A.OrderitemEntity
                        .W(orderItems[0].TempInternalGuid)
                        .W((PriceTotalInTax)1M)
                        .W((ProductPriceIn)1M)
                        .W((int?)productId)
                        .W(A.ProductEntity
                            .W((ScheduleEntity)null)))
                    .W(A.OrderitemEntity
                        .W(orderItems[1].TempInternalGuid)
                        .W((PriceTotalInTax)2M)
                        .W((ProductPriceIn)2M)
                        .W((int?)productId)
                        .W(A.ProductEntity
                            .W((ScheduleEntity)null)))
                    .Build(),
                orderItems,
                validationErrors,
                true);

            // Assert
            Assert.AreEqual(0, validationErrors.Count);
        }

        [Test]
        public void Validate_InvisibleProduct_ReturnsError()
        {
            // Arrange
            var validationErrors = new List<ValidationError>();

            var orderItems = new[]
            {
                A.Orderitem
                    .W(1M)
                    .Build(),
            };

            var orderItem = A.OrderitemEntity
                            .W(orderItems[0].TempInternalGuid)
                            .W((ProductPriceIn)1M)
                            .W(A.ProductEntity
                                .W(VisibilityType.Never)
                                .W((ScheduleEntity)null));


            // Act
            OrderitemValidator.Validate(A.OrderEntity
                .W(orderItem)
                .Build(),
                orderItems,
                validationErrors,
                true);

            // Assert
            Assert.AreEqual(1, validationErrors.Count);
            Assert.AreEqual(ValidationErrorSubType.ProductOrderingDisabled, validationErrors[0].ValidationErrorSubType);
        }

        [Test]
        public void Validate_WithUnavailableProduct_ReturnsError()
        {
            // Arrange
            var timeZoneOlsonId = A.TimeZoneOlsonId.Build();
            var currentDateTime = DateTime.UtcNow.ConvertToZonedDateTime(timeZoneOlsonId);

            var timeStart = currentDateTime.TimeOfDay.Minus(Period.FromMinutes(20));
            var timeEnd = currentDateTime.TimeOfDay.Plus(Period.FromMinutes(-10));

            var validationErrors = new List<ValidationError>();

            var orderItems = new[]
            {
                A.Orderitem
                    .W(1M)
                .Build(),
            };

            var orderEntity = A.OrderEntity
                .W(timeZoneOlsonId)
                .W(A.OrderitemEntity
                    .W(orderItems[0].TempInternalGuid)
                    .W((ProductPriceIn)1M)
                    .W(A.ProductEntity
                        .W(A.ScheduleEntity
                            .W(A.ScheduleitemEntity
                                .W(currentDateTime.DayOfWeek)
                                .W(timeStart, timeEnd)))))
                .Build();

            // Act
            OrderitemValidator.Validate(orderEntity, orderItems, validationErrors, true);

            // Assert
            Assert.AreEqual(1, validationErrors.Count);
            Assert.AreEqual(ValidationErrorSubType.ProductNotAvailable, validationErrors[0].ValidationErrorSubType);
        }

        [Test]
        public void Validate_WithUnavailableProductWithoutSchedule_ReturnsError()
        {
            // Arrange
            var timeZoneOlsonId = A.TimeZoneOlsonId.Build();

            var validationErrors = new List<ValidationError>();

            var orderItems = new[]
            {
                A.Orderitem
                    .W(1M)
                    .Build(),
            };

            var orderEntity = A.OrderEntity
                .W(timeZoneOlsonId)
                .W(A.OrderitemEntity
                    .W(orderItems[0].TempInternalGuid)
                    .W((ProductPriceIn)1M)
                    .W(A.ProductEntity
                        .WithIsAvailable(false)))
                .Build();

            // Act
            OrderitemValidator.Validate(orderEntity, orderItems, validationErrors, true);

            // Assert
            Assert.AreEqual(1, validationErrors.Count);
            Assert.AreEqual(ValidationErrorSubType.ProductOrderingDisabled, validationErrors[0].ValidationErrorSubType);
        }

        [Test]
        public void Validate_WithProductWithoutScheduleAndWithOrderItems_ReturnsAvailable()
        {
            // Arrange
            var validationErrors = new List<ValidationError>();

            var orderItems = new[]
            {
                A.Orderitem
                    .W(1M)
                .Build(),
            };

            var orderEntity = A.OrderEntity
                .W(A.OrderitemEntity
                    .W(orderItems[0].TempInternalGuid)
                    .W((ProductPriceIn)1M)
                    .W(A.ProductEntity
                        .W((ScheduleEntity)null)))
                .Build();

            // Act
            OrderitemValidator.Validate(orderEntity, orderItems, validationErrors, true);

            // Assert
            Assert.AreEqual(0, validationErrors.Count);
        }


        [Test]
        public void Validate_WithAvailableProduct_ReturnsNoError()
        {
            // Arrange
            var timeZoneOlsonId = A.TimeZoneOlsonId.Build();
            var currentDateTime = DateTime.UtcNow.ConvertToZonedDateTime(timeZoneOlsonId);

            var timeStart = currentDateTime.TimeOfDay.Minus(Period.FromMinutes(20));
            var timeEnd = currentDateTime.TimeOfDay.Plus(Period.FromMinutes(20));

            var validationErrors = new List<ValidationError>();

            var orderItems = new[]
            {
                A.Orderitem
                    .W(1M)
                .Build(),
            };

            var orderEntity = A.OrderEntity
                .W(timeZoneOlsonId)
                .W(A.OrderitemEntity
                    .W(orderItems[0].TempInternalGuid)
                    .W((ProductPriceIn)1M)
                    .W(A.ProductEntity
                        .W(A.ScheduleEntity
                            .W(A.ScheduleitemEntity
                                .W(currentDateTime.DayOfWeek)
                                .W(timeStart, timeEnd)))))
                .Build();

            // Act
            OrderitemValidator.Validate(orderEntity, orderItems, validationErrors, true);

            // Assert
            Assert.AreEqual(0, validationErrors.Count);
        }

        [Test]
        public void Validate_WithWeekDayNotInScheduleAndUnavailableProduct_ReturnsError()
        {
            // Arrange
            var timeZoneOlsonId = A.TimeZoneOlsonId.Build();
            var currentDateTime = DateTime.UtcNow.ConvertToZonedDateTime(timeZoneOlsonId);

            var timeStart = currentDateTime.TimeOfDay.Minus(Period.FromMinutes(20));
            var timeEnd = currentDateTime.TimeOfDay.Plus(Period.FromMinutes(-10));

            var validationErrors = new List<ValidationError>();

            var orderItems = new[]
            {
                A.Orderitem
                    .W(1M)
                .Build(),
            };

            var orderEntity = A.OrderEntity
                .W(timeZoneOlsonId)
                .W(A.OrderitemEntity
                    .W(orderItems[0].TempInternalGuid)
                    .W((ProductPriceIn)1M)
                    .W(A.ProductEntity
                        .W(A.ScheduleEntity
                            .W(A.ScheduleitemEntity
                                .W((DayOfWeek?)null)
                                .W(timeStart, timeEnd)))))
                .Build();

            // Act
            OrderitemValidator.Validate(orderEntity, orderItems, validationErrors, true);

            // Assert
            Assert.AreEqual(1, validationErrors.Count);
            Assert.AreEqual(ValidationErrorSubType.ProductNotAvailable, validationErrors[0].ValidationErrorSubType);
        }

        [Test]
        public void Validate_WithWeekDayNotInScheduleAndAvailableProduct_ReturnsNoError()
        {
            // Arrange
            var timeZoneOlsonId = A.TimeZoneOlsonId.Build();
            var currentDateTime = DateTime.UtcNow.ConvertToZonedDateTime(timeZoneOlsonId);

            var timeStart = currentDateTime.TimeOfDay.Minus(Period.FromMinutes(20));
            var timeEnd = currentDateTime.TimeOfDay.Plus(Period.FromMinutes(10));

            var validationErrors = new List<ValidationError>();

            var orderItems = new[]
            {
                A.Orderitem
                    .W(1M)
                .Build(),
            };

            var orderEntity = A.OrderEntity
                .W(timeZoneOlsonId)
                .W(A.OrderitemEntity
                    .W(orderItems[0].TempInternalGuid)
                    .W((ProductPriceIn)1M)
                    .W(A.ProductEntity
                        .W(A.ScheduleEntity
                            .W(A.ScheduleitemEntity
                                .W((DayOfWeek?)null)
                                .W(timeStart, timeEnd)))))
                .Build();

            // Act
            OrderitemValidator.Validate(orderEntity, orderItems, validationErrors, true);

            // Assert
            Assert.AreEqual(0, validationErrors.Count);
        }

        [Test]
        public void Validate_WithProductWithScheduleOnlyAvailableForOneMinuteOnTheCurrentTime_ReturnsNoError()
        {
            // Arrange
            var timeZoneOlsonId = A.TimeZoneOlsonId.Build();
            var currentDateTime = DateTime.UtcNow.ConvertToZonedDateTime(timeZoneOlsonId);

            var timeStart = currentDateTime.TimeOfDay;
            var timeEnd = currentDateTime.TimeOfDay;

            var validationErrors = new List<ValidationError>();

            var orderItems = new[]
            {
                A.Orderitem
                    .W(1M)
                .Build(),
            };

            var orderEntity = A.OrderEntity
                .W(timeZoneOlsonId)
                .W(A.OrderitemEntity
                    .W(orderItems[0].TempInternalGuid)
                    .W((ProductPriceIn)1M)
                    .W(A.ProductEntity
                        .W(A.ScheduleEntity
                            .W(A.ScheduleitemEntity
                                .W(currentDateTime.DayOfWeek)
                                .W(timeStart, timeEnd)))))
                .Build();

            // Act
            OrderitemValidator.Validate(orderEntity, orderItems, validationErrors, true);

            // Assert
            Assert.AreEqual(0, validationErrors.Count);
        }

        [TestCase(0, true)]
        [TestCase(1, false)]
        [TestCase(2, false)]
        [TestCase(3, true)]
        public void Validate_OrderitemAlterationMatchesMinMaxOptions_NoError(int numberOfOptions, bool assertValidationError)
        {
            // Arrange
            var validationErrors = new List<ValidationError>();

            List<AlterationitemRequest> alterationitems = new List<AlterationitemRequest>(numberOfOptions);
            for (int i = 0; i < numberOfOptions; i++)
            {
                alterationitems.Add(A.Alterationitem
                                     .WithAlterationId(10)
                                     .WithAlterationoptionId((i + 1))
                                     .Build());
            }

            const int productId = 1;
            var orderItems = new[]
            {
                A.Orderitem
                 .W(1M)
                 .W(productId)
                 .WithAlterationitems(alterationitems)
                 .Build()
            };

            OrderEntityBuilder orderEntity = A.OrderEntity
                                              .W(A.OrderitemEntity
                                                  .W(orderItems[0].TempInternalGuid)
                                                  .W((PriceTotalInTax)1M)
                                                  .W((ProductPriceIn)1M)
                                                  .W((int?)productId)
                                                  .W(A.ProductEntity
                                                      .W((ScheduleEntity)null)
                                                      .WithAlterations(A.AlterationEntity
                                                                        .WithId(10)
                                                                        .WithMinMaxOptions(1, 2),
                                                                       A.AlterationEntity
                                                                        .WithId(10)
                                                                        .WithType(AlterationType.Notes))))
                                              .WithClientId(null);

            // Act
            OrderitemValidator.Validate(orderEntity.Build(), orderItems, validationErrors, true);

            // Assert
            if (assertValidationError)
            {
                Assert.AreEqual(1, validationErrors.Count);
                Assert.AreEqual(ValidationErrorSubType.AlterationItemOptionsInvalid, validationErrors[0].ValidationErrorSubType);
            }
            else
            {
                Assert.AreEqual(0, validationErrors.Count);
            }
        }

        [Test]
        public void Validate_OrderitemWithOptionalAlterationoptions_NoError()
        {
            // Arrange
            var validationErrors = new List<ValidationError>();

            var alterationitems = new[]
            {
                A.Alterationitem
                 .WithAlterationId(10)
                 .WithAlterationoptionId(1)
                 .Build()
            };

            const int productId = 1;
            var orderItems = new[]
            {
                A.Orderitem
                 .W(1M)
                 .W(productId)
                 .WithAlterationitems(alterationitems)
                 .Build()
            };

            OrderEntityBuilder orderEntity = A.OrderEntity
                                              .W(A.OrderitemEntity
                                                  .W(orderItems[0].TempInternalGuid)
                                                  .W((PriceTotalInTax)1M)
                                                  .W((ProductPriceIn)1M)
                                                  .W((int?)productId)
                                                  .W(A.ProductEntity
                                                      .W((ScheduleEntity)null)
                                                      .WithAlterations(A.AlterationEntity
                                                                        .WithId(10)
                                                                        .WithMinMaxOptions(0, 2),
                                                                       A.AlterationEntity
                                                                        .WithId(10)
                                                                        .WithType(AlterationType.Notes))))
                                              .WithClientId(null);

            // Act
            OrderitemValidator.Validate(orderEntity.Build(), orderItems, validationErrors, true);

            // Assert
            Assert.AreEqual(0, validationErrors.Count);

        }

        [Theory]
        public void Validate_OrderLevelAlterationsCheck([Values(10, 11)] int alterationId, bool orderLevelAlterationChecked)
        {
            // Arrange
            var validationErrors = new List<ValidationError>();
            int expectedErrors = orderLevelAlterationChecked && alterationId == 11 ? 1 : 0;

            var alterationitems = new[]
            {
                A.Alterationitem
                 .WithAlterationId(alterationId)
                 .WithAlterationoptionId(1)
                 .Build()
            };

            const int productId = 1;
            var orderItems = new[]
            {
                A.Orderitem
                 .W(1M)
                 .W(productId)
                 .WithAlterationitems(alterationitems)
                 .Build()
            };

            OrderEntityBuilder orderEntity = A.OrderEntity
                                              .W(A.OrderitemEntity
                                                  .W(orderItems[0].TempInternalGuid)
                                                  .W((PriceTotalInTax)1M)
                                                  .W((ProductPriceIn)1M)
                                                  .W((int?)productId)
                                                  .W(A.ProductEntity
                                                      .W((ScheduleEntity)null)
                                                      .WithAlterations(A.AlterationEntity
                                                                        .WithId(10)
                                                                        .AsOrderLevelAlteration(true)
                                                                        .WithMinMaxOptions(1, 2))))
                                              .WithClientId(null);

            // Act
            OrderitemValidator.Validate(orderEntity.Build(), orderItems, validationErrors, orderLevelAlterationChecked);

            // Assert
            Assert.AreEqual(expectedErrors, validationErrors.Count);

        }
    }
}
