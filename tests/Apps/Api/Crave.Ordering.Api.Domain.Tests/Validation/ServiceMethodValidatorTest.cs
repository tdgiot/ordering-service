﻿using Crave.Enums;
using Crave.Ordering.Api.Core.Enums;
using Crave.Ordering.Api.Domain.Exceptions;
using Crave.Ordering.Api.Domain.Tests.Builders.ValidationBuilders;
using Crave.Ordering.Api.Domain.Validation;
using Crave.Ordering.Api.Domain.Validation.Errors;
using Crave.Ordering.Shared.Data.LLBLGen.Tests.Builders;
using Crave.Ordering.Shared.Data.LLBLGen.Tests.Structs;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Crave.Ordering.Api.Domain.Tests.Validation
{
    public class ServiceMethodValidatorTest
    {
        private static class A
        {
            public static OrderEntityBuilder OrderEntity => new OrderEntityBuilder();
            public static ServiceMethodEntityBuilder ServiceMethodEntity => new ServiceMethodEntityBuilder();
        }

        [Test]
        public void Validate_WithNullOrderEntity_Throws()
        {
            static void Action()
            {
                // Act
                ServiceMethodValidator.Validate(null!, new List<ValidationError>());
            }

            // Assert
            _ = Assert.Throws<NullReferenceException>(Action);
        }

        [Test]
        public void Validate_WithNewOrderEntity_ReturnsNoError()
        {
            static void Action()
            {
                // Act
                ServiceMethodValidator.Validate(A.OrderEntity
                    .W(A.ServiceMethodEntity.AsNew())
                    .Build(), new List<ValidationError>());
            }

            // Assert
            Assert.DoesNotThrow(Action);
        }

        [Test]
        public void Validate_WithInactiveServiceMethod_ReturnsError()
        {
            // Arrange
            List<ValidationError>? validationErrors = new List<ValidationError>();

            // Act
            ServiceMethodValidator.Validate(A.OrderEntity
                .W(A.ServiceMethodEntity
                    .W((Active)false))
                .Build(), validationErrors);


            // Assert
            Assert.IsTrue(validationErrors.Any(x => x.ValidationErrorSubType == ValidationErrorSubType.ServiceMethodNotActive));
        }

        [Test]
        public void Validate_WithLastNameOnOrderRequiredAndNoLastNameOnOrder_ReturnsError()
        {
            // Arrange
            List<ValidationError>? validationErrors = new List<ValidationError>();

            // Act
            ServiceMethodValidator.Validate(A.OrderEntity
                .W((CustomerLastname)string.Empty)
                .W(A.ServiceMethodEntity
                    .W(ServiceMethodType.RoomService)) // Room service requires a last name.
                .Build(), validationErrors);

            // Assert
            Assert.IsTrue(validationErrors.Any(x => x.ValidationErrorSubType == ValidationErrorSubType.CustomerLastNameInvalid));
        }

        [Test]
        public void Validate_WithDeliveryPointRequiredAndNoDeliveryPointId_ReturnsError()
        {
            // Arrange
            List<ValidationError>? validationErrors = new List<ValidationError>();

            // Act
            ServiceMethodValidator.Validate(A.OrderEntity
                .W((int?)0)
                .W(A.ServiceMethodEntity
                    .W(ServiceMethodType.RoomService)) // Room service requires a delivery point
                .Build(), validationErrors);

            // Assert
            Assert.IsTrue(validationErrors.Any(x => x.ValidationErrorSubType == ValidationErrorSubType.ServiceMethodDeliveryPointRequired));
        }

        [Test]
        public void Validate_WithDeliveryPointNotRequiredAndDeliveryPointId_ReturnsError()
        {
            static void Act()
            {
                // Arrange
                List<ValidationError>? validationErrors = new List<ValidationError>();

                // Act
                ServiceMethodValidator.Validate(A.OrderEntity
                    .W((int?)1)
                    .W(A.ServiceMethodEntity
                        .W(ServiceMethodType.PickUp)) // Pickup does not require a delivery point
                    .Build(), validationErrors);
            }

            // Assert
            CheckoutValidationException? exception = Assert.Throws<CheckoutValidationException>(Act);
            Assert.AreEqual(ValidationErrorSubType.ServiceMethodDeliveryPointNotRequired, exception.CheckoutValidationResponse.ValidationErrors.Single().ValidationErrorSubType);
        }

        [Test]
        public void Validate_WithCoversCount_NotAllowed()
        {
            List<ValidationError>? validationErrors = new List<ValidationError>();

            // Act
            ServiceMethodValidator.Validate(A.OrderEntity
                .W((int?)1)
                .WithNumberOfCustomers(2)
                .W(A.ServiceMethodEntity
                    .W(CoversType.NeverAsk)) // Pickup does not require a delivery point
                .Build(), validationErrors);


            // Assert
            Assert.IsTrue(validationErrors.Any(x => x.ValidationErrorSubType == ValidationErrorSubType.CoversCountNotAllowed));
        }

        [Test]
        public void Validate_WithCoversCount_Allowed()
        {
            List<ValidationError>? validationErrors = new List<ValidationError>();

            // Act
            ServiceMethodValidator.Validate(A.OrderEntity
                .W((int?)1)
                .WithNumberOfCustomers(2)
                .W(A.ServiceMethodEntity
                    .W(CoversType.AlwaysAsk)) // Pickup does not require a delivery point
                .Build(), validationErrors);


            // Assert
            Assert.IsFalse(validationErrors.Any(x => x.ValidationErrorSubType == ValidationErrorSubType.CoversCountNotAllowed));
        }

        [Test]
        public void Validate_WithCoversCount_Not_Valid()
        {
            List<ValidationError>? validationErrors = new List<ValidationError>();

            // Act
            ServiceMethodValidator.Validate(A.OrderEntity
                .W((int?)1)
                .WithNumberOfCustomers(2)
                .W(A.ServiceMethodEntity
                    .W(CoversType.NeverAsk)) // Pickup does not require a delivery point
                .Build(), validationErrors);


            // Assert
            Assert.IsTrue(validationErrors.Any(x => x.ValidationErrorSubType == ValidationErrorSubType.CoversCountNotAllowed));
        }

        [Test]
        public void Validate_WithCoversCount_Invald_input()
        {
            List<ValidationError>? validationErrors = new List<ValidationError>();

            // Act
            ServiceMethodValidator.Validate(A.OrderEntity
                .W((int?)1)
                .WithNumberOfCustomers(0)
                .W(A.ServiceMethodEntity
                    .W(CoversType.AlwaysAsk)) // Pickup does not require a delivery point
                .Build(), validationErrors);


            // Assert
            Assert.IsTrue(validationErrors.Any(x => x.ValidationErrorSubType == ValidationErrorSubType.InvalidCoversCountValue));
        }


    }
}
