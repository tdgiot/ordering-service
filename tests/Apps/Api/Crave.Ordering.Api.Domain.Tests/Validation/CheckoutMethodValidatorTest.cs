using Crave.Enums;
using Crave.Ordering.Api.Core.Enums;
using Crave.Ordering.Api.Domain.Exceptions;
using Crave.Ordering.Api.Domain.Tests.Builders.ValidationBuilders;
using Crave.Ordering.Api.Domain.Validation;
using Crave.Ordering.Api.Domain.Validation.Errors;
using Crave.Ordering.Shared.Data.LLBLGen.Tests.Builders;
using Crave.Ordering.Shared.Data.LLBLGen.Tests.DomainTypes;
using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace Crave.Ordering.Api.Domain.Tests.Validation
{
    public class CheckoutMethodValidatorTest
    {
        [Test]
        public void Validate_WithNullOrderEntity_Throws()
        {
            static void Action()
            {
                // Act
                CheckoutMethodValidator.Validate(null!, new List<ValidationError>());
            }

            // Assert
            _ = Assert.Throws<NullReferenceException>(Action);
        }

        [Test]
        public void Validate_WithNewCheckoutMethodEntity_ReturnsNoError()
        {
            static void Action()
            {
                // Act
                CheckoutMethodValidator.Validate(A.OrderEntity
                    .W(A.CheckoutMethodEntity.AsNew())
                    .Build(), new List<ValidationError>());
            }

            // Assert
            Assert.DoesNotThrow(Action);
        }

        [Test]
        public void Validate_WithCheckoutMethodInactive_ReturnsError()
        {
            // Arrange
            var validationErrors = new List<ValidationError>();

            // Act
            CheckoutMethodValidator.Validate(A.OrderEntity
                .W(A.CheckoutMethodEntity
                    .W(CheckoutType.PayLater)
                    .W(1)
                    .W(false))
                .W((ChargeToDeliverypointId)null)
                .Build(), validationErrors);

            // Assert
            Assert.AreEqual(1, validationErrors.Count);
            Assert.AreEqual(ValidationErrorSubType.CheckoutMethodNotActive, validationErrors[0].ValidationErrorSubType);
        }

        [Test]
        public void Validate_WithChargeToRoomAndNoCustomerName_ReturnsError()
        {
            // Arrange
            var validationErrors = new List<ValidationError>();

            // Act
            CheckoutMethodValidator.Validate(A.OrderEntity
                .W(A.CheckoutMethodEntity
                    .W(CheckoutType.ChargeToRoom)
                    .W(1))
                .W((CustomerLastname)string.Empty)
                .W((ChargeToDeliverypointId)1)
                .Build(), validationErrors);

            // Assert
            Assert.AreEqual(1, validationErrors.Count);
            Assert.AreEqual(ValidationErrorSubType.CustomerLastNameInvalid, validationErrors[0].ValidationErrorSubType);
        }

        [Test]
        public void Validate_WithChargeToRoomAndNoChargeToDeliveryPoint_ReturnsError()
        {
            // Arrange
            var validationErrors = new List<ValidationError>();

            // Act
            CheckoutMethodValidator.Validate(A.OrderEntity
                .W(A.CheckoutMethodEntity
                    .W(CheckoutType.ChargeToRoom)
                    .W(1))
                .W((ChargeToDeliverypointId)null)
                .Build(), validationErrors);

            // Assert
            Assert.AreEqual(1, validationErrors.Count);
            Assert.AreEqual(ValidationErrorSubType.CheckoutMethodDeliveryPointRequired, validationErrors[0].ValidationErrorSubType);
        }

        [Test]
        public void Validate_WithNoDeliveryPointRequiredAndChargeToDeliveryPoint_ReturnsError()
        {
            static void Action()
            {
                // Arrange
                CheckoutMethodValidator.Validate(A.OrderEntity
                    .W(A.CheckoutMethodEntity
                        .W(CheckoutType.PaymentProvider))
                    .W((ChargeToDeliverypointId)1)
                    .Build(), new List<ValidationError>());
            }

            // Assert
            _ = Assert.Throws<CheckoutValidationException>(Action);
        }

        [Test]
        public void Validate_WithPriceRequiredAndNoPrice_ReturnsError()
        {
            // Arrange
            var validationErrors = new List<ValidationError>();

            // Act
            CheckoutMethodValidator.Validate(A.OrderEntity
                .W(0M)
                .W(A.CheckoutMethodEntity
                    .W(CheckoutType.PaymentProvider))
                .W((ChargeToDeliverypointId)null)
                .Build(), validationErrors);

            // Assert
            Assert.AreEqual(1, validationErrors.Count);
            Assert.AreEqual(ValidationErrorSubType.CheckoutMethodInvalid, validationErrors[0].ValidationErrorSubType);
        }

        [Test]
        public void Validate_WithEverythingHunkyDory_ReturnsNoError()
        {
            // Arrange
            var validationErrors = new List<ValidationError>();

            // Act
            CheckoutMethodValidator.Validate(A.OrderEntity
                .W(2M)
                .W(A.CheckoutMethodEntity
                    .W(CheckoutType.PaymentProvider))
                .W((ChargeToDeliverypointId)null)
                .W(A.OrderitemEntity.W((PriceTotalInTax)1M))
                .Build(), validationErrors);

            // Assert
            Assert.AreEqual(0, validationErrors.Count);
        }

        [Test]
        public void Validate_FreeCheckoutMethodWithPrice_ReturnsError()
        {
            // Arrange
            var validationErrors = new List<ValidationError>();

            // Act
            CheckoutMethodValidator.Validate(A.OrderEntity
                .W(2M)
                .W(A.CheckoutMethodEntity
                    .W(CheckoutType.FreeOfCharge))
                .W((ChargeToDeliverypointId)null)
                .W(A.OrderitemEntity.W((PriceTotalInTax)1M))
                .Build(), validationErrors);

            // Assert
            Assert.AreEqual(1, validationErrors.Count);
        }

        private static class A
        {
            public static OrderEntityBuilder OrderEntity => new OrderEntityBuilder();
            public static CheckoutMethodEntityBuilder CheckoutMethodEntity => new CheckoutMethodEntityBuilder();
            public static OrderitemEntityBuilder OrderitemEntity => new OrderitemEntityBuilder();
        }
    }
}