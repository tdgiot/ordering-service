using Crave.Enums;
using Crave.Ordering.Api.Core.Enums;
using Crave.Ordering.Api.Domain.Exceptions;
using Crave.Ordering.Api.Domain.Tests.Builders.EntityBuilders;
using Crave.Ordering.Api.Domain.Tests.Builders.ValidationBuilders;
using Crave.Ordering.Api.Domain.Validation;
using Crave.Ordering.Api.Domain.Validation.Errors;
using Crave.Ordering.Shared.Data.LLBLGen.Tests.Builders;
using Crave.Ordering.Shared.Data.LLBLGen.Tests.DomainTypes;
using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace Crave.Ordering.Api.Domain.Tests.Validation
{
    public class TippingValidatorTest
    {
        private static class A
        {
            public static OrderEntityBuilder OrderEntity => new OrderEntityBuilder();
            public static OutletEntityBuilder OutletEntity => new OutletEntityBuilder();
            public static OrderitemEntityBuilder OrderitemEntity => new OrderitemEntityBuilder();
        }

        [Test]
        public void Validate_WithNullOrderEntity_Throws()
        {
            static void Action()
            {
                // Act
                TippingValidator.Validate(null, new List<ValidationError>());
            }

            // Assert
            _ = Assert.Throws<NullReferenceException>(Action);
        }

        [Test]
        public void Validate_WithTippingActiveAndWithoutTippingOrderItem_Trows()
        {
            static void Action()
            {
                // Arrange
                TippingActive tippingActive = true;
                const double tippingMinimumPercentage = 1d;
                TippingProductId tippingProductId = 2;
                var order = A.OrderEntity
                    .W(A.OutletEntity
                        .W(tippingActive)
                        .W(tippingMinimumPercentage)
                        .W(tippingProductId))
                    .Build();

                // Act
                TippingValidator.Validate(order, new List<ValidationError>());
            }

            // Assert
            _ = Assert.Throws<CheckoutValidationException>(Action);
        }

        [Test]
        public void Validate_WithTippingNotActiveAndWithTippingOrderItem_Trows()
        {
            static void Action()
            {
                // Arrange
                TippingActive tippingActive = false;
                const double tippingMinimumPercentage = 1d;
                TippingProductId tippingProductId = 2;
                var order = A.OrderEntity
                    .W(A.OutletEntity
                        .W(tippingActive)
                        .W(tippingMinimumPercentage)
                        .W(tippingProductId))
                    .W(A.OrderitemEntity
                        .W(OrderitemType.Tip))
                    .Build();

                // Act
                TippingValidator.Validate(order, new List<ValidationError>());
            }

            // Assert
            _ = Assert.Throws<CheckoutValidationException>(Action);
        }

        [Test]
        public void Validate_WithTipBelowMinimum_AddsValidationError()
        {
            // Arrange
            TippingActive tippingActive = true;
            const double tippingMinimumPercentage = 5d;
            TippingProductId tippingProductId = 2;
            var order = A.OrderEntity
                .W(A.OutletEntity
                    .W(tippingActive)
                    .W(tippingMinimumPercentage)
                    .W(tippingProductId))
                .W(new[]
                {
                    A.OrderitemEntity
                        .W(OrderitemType.Product)
                        .W((PriceTotalInTax)100M)
                        .Build(),
                    A.OrderitemEntity
                        .W(OrderitemType.Tip)
                        .W((PriceTotalInTax)1M)
                        .Build(),

                })
                .Build();

            var validationErrors = new List<ValidationError>();

            // Act
            TippingValidator.Validate(order, validationErrors);

            // Assert
            Assert.AreEqual(1, validationErrors.Count);
            Assert.AreEqual(ValidationErrorSubType.TipInvalidMinimum, validationErrors[0].ValidationErrorSubType);
        }

        [Test]
        public void Validate_WithTipHighEnough_ReturnsNoValidationError()
        {
            // Arrange
            TippingActive tippingActive = true;
            const double tippingMinimumPercentage = 5d;
            TippingProductId tippingProductId = 2;
            var order = A.OrderEntity
                .W(A.OutletEntity
                    .W(tippingActive)
                    .W(tippingMinimumPercentage)
                    .W(tippingProductId))
                .W(new[]
                {
                    A.OrderitemEntity
                        .W(OrderitemType.Product)
                        .W((PriceTotalInTax)100M)
                        .Build(),
                    A.OrderitemEntity
                        .W(OrderitemType.Tip)
                        .W((PriceTotalInTax)5M)
                        .Build(),

                })
                .Build();

            var validationErrors = new List<ValidationError>();

            // Act
            TippingValidator.Validate(order, validationErrors);

            // Assert
            Assert.AreEqual(0, validationErrors.Count);
        }

        [Test]
        public void Validate_WithOrderTotalOfZero_ReturnsNoValidationError()
        {
            // Arrange
            TippingActive tippingActive = true;
            const double tippingMinimumPercentage = 5d;
            TippingProductId tippingProductId = 2;

            var order = A.OrderEntity
                .W(A.OutletEntity
                    .W(tippingActive)
                    .W(tippingMinimumPercentage)
                    .W(tippingProductId))
                .W(new[]
                {
                    A.OrderitemEntity
                        .W(OrderitemType.Product)
                        .W((PriceTotalInTax)0M)
                        .Build(),
                    A.OrderitemEntity
                        .W(OrderitemType.Tip)
                        .W((PriceTotalInTax)0M)
                        .Build()
                })
                .Build();

            var validationErrors = new List<ValidationError>();

            // Act
            TippingValidator.Validate(order, validationErrors);

            // Assert
            Assert.AreEqual(0, validationErrors.Count);
        }

        [Test]
        public void Validate_WithLowOrderAmountAndRoundedTipAmount_ReturnsNoValidationError()
        {
            // Test created to fix issue DEVLOG-966.

            // Arrange
            TippingActive tippingActive = true;
            const double tippingMinimumPercentage = 12.5d;
            TippingProductId tippingProductId = 2;

            PriceTotalInTax roundedTipAmount = 0.06M; // 0.50 * 12.5% = 0.0625, but the front-end sends a rounded value the the back-end.

            var order = A.OrderEntity
                .W(A.OutletEntity
                    .W(tippingActive)
                    .W(tippingMinimumPercentage)
                    .W(tippingProductId))
                .W(new[]
                {
                    A.OrderitemEntity
                        .W(OrderitemType.Product)
                        .W((PriceTotalInTax)0.5M)
                        .Build(),
                    A.OrderitemEntity
                        .W(OrderitemType.Tip)
                        .W(roundedTipAmount)
                        .Build()
                })
                .Build();

            var validationErrors = new List<ValidationError>();

            // Act
            TippingValidator.Validate(order, validationErrors);

            // Assert
            Assert.AreEqual(0, validationErrors.Count);
        }
    }
}
