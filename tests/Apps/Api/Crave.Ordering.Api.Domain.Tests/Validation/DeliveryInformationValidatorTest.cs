using Crave.Enums;
using Crave.Libraries.Distance.Enums;
using Crave.Libraries.Distance.Models;
using Crave.Ordering.Api.Core.Enums;
using Crave.Ordering.Api.Domain.Exceptions;
using Crave.Ordering.Api.Domain.Tests.Builders;
using Crave.Ordering.Api.Domain.Tests.Builders.EntityBuilders;
using Crave.Ordering.Api.Domain.Tests.Builders.RepositoryBuilders;
using Crave.Ordering.Api.Domain.Tests.Builders.UseCaseBuilders;
using Crave.Ordering.Api.Domain.Tests.Builders.ValidationBuilders;
using Crave.Ordering.Api.Domain.Validation.Errors;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.Tests.Builders;
using Crave.Ordering.Shared.Data.LLBLGen.Tests.DomainTypes;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Address = Crave.Ordering.Api.Domain.Tests.Builders.EntityBuilders.Address;

namespace Crave.Ordering.Api.Domain.Tests.Validation
{
    public class DeliveryInformationValidatorTest
    {
        private static class A
        {
            public static DeliveryInformationValidatorBuilder DeliveryInformationValidator => new DeliveryInformationValidatorBuilder();
            public static OrderEntityBuilder OrderEntity => new OrderEntityBuilder();
            public static ServiceMethodEntityBuilder ServiceMethodEntity => new ServiceMethodEntityBuilder();
            public static DeliveryInformationEntityBuilder DeliveryInformationEntity => new DeliveryInformationEntityBuilder();
            public static GetDeliveryRateUseCaseBuilder GetDeliveryRateUseCase => new GetDeliveryRateUseCaseBuilder();
            public static IServiceMethodRepositoryBuilder ServiceMethodRepositoryMock => new ServiceMethodRepositoryMockBuilder();
            public static IGetDistanceToOutletUseCaseBuilder GetDistanceToOutletUseCase => new GetDistanceToOutletUseCaseBuilder();
            public static IGetOutletLocationUseCaseBuilder GetOutletLocationUseCase => new GetOutletLocationUseCaseBuilder();
            public static ICompanyRepositoryBuilder CompanyRepositoryMock => new CompanyRepositoryMockBuilder();
            public static CompanyEntityBuilder CompanyEntity => new CompanyEntityBuilder();
            public static IGetDistanceBetweenLocationsUseCaseBuilder GetDistanceBetweenLocationsUseCase => new GetDistanceBetweenLocationsUseCaseBuilder();
            public static IDistanceClientBuilder DistanceClientMock => new DistanceClientMockBuilder();
            public static GetDistanceResponseBuilder GetDistanceResponse => new GetDistanceResponseBuilder();
            public static DeliveryDistanceEntityBuilder DeliveryDistanceEntity => new DeliveryDistanceEntityBuilder();
            public static OrderitemEntityBuilder OrderitemEntity => new OrderitemEntityBuilder();
        }

        
        [Test]
        public void Validate_WithNullOrderEntity_Throws()
        {
            static async Task  Action()
            {
                // Arrange
                var sut = A.DeliveryInformationValidator.Build();

                // Act
                await sut.ValidateAsync(null, new List<ValidationError>());
            }

            // Assert
            _ = Assert.ThrowsAsync<NullReferenceException>(Action);
        }

        [Test]
        public void Validate_WithDeliveryInformationRequiredAndWithoutDeliveryInformation_Trows()
        {
            static async Task Action()
            {
                // Arrange
                var sut = A.DeliveryInformationValidator.Build();
                var order = A.OrderEntity
                    .W((DeliveryInformationEntity)null)
                    .W(A.ServiceMethodEntity
                        .W(Crave.Enums.ServiceMethodType.Delivery))
                    .Build();

                // Act
                await sut.ValidateAsync(order, new List<ValidationError>());
            }

            // Assert
            var exception = Assert.ThrowsAsync<CheckoutValidationException>(Action);
            Assert.AreEqual(ValidationErrorSubType.DeliveryInformationRequired, exception.CheckoutValidationResponse.ValidationErrors.First().ValidationErrorSubType);
        }

        [Test]
        public void Validate_WithDeliveryInformationNotRequiredAndWithDeliveryInformation_Trows()
        {
            static async Task Action()
            {
                // Arrange
                var sut = A.DeliveryInformationValidator.Build();
                var order = A.OrderEntity
                    .W(A.ServiceMethodEntity
                        .W(Crave.Enums.ServiceMethodType.PickUp))
                    .Build();

                // Act
                await sut.ValidateAsync(order, new List<ValidationError>());
            }

            // Assert
            var exception = Assert.ThrowsAsync<CheckoutValidationException>(Action);
            Assert.AreEqual(ValidationErrorSubType.DeliveryInformationNotRequired, exception.CheckoutValidationResponse.ValidationErrors.First().ValidationErrorSubType);
        }

        [Test]
        public void Validate_WithDeliveryInformationRequiredAndWithoutAddress_ReturnsValidationError()
        {
            // Arrange
            var sut = A.DeliveryInformationValidator.Build();
            var order = A.OrderEntity
                .W(A.DeliveryInformationEntity
                    .W((Address)" "))
                .W(A.ServiceMethodEntity
                    .W(Crave.Enums.ServiceMethodType.Delivery))
                .Build();

            var validationErrors = new List<ValidationError>();

            // Act
            sut.ValidateAsync(order, validationErrors);

            // Assert
            Assert.AreEqual(1, validationErrors.Count);
            Assert.AreEqual(ValidationErrorSubType.DeliveryInformationAddressMissing, validationErrors[0].ValidationErrorSubType);
        }

        [Test]
        public void Validate_WithDeliveryInformationRequiredAndWithoutZipcode_ReturnsValidationError()
        {
            // Arrange
            var sut = A.DeliveryInformationValidator.Build();
            var order = A.OrderEntity
                .W(A.DeliveryInformationEntity
                    .W((Zipcode)" "))
                .W(A.ServiceMethodEntity
                    .W(Crave.Enums.ServiceMethodType.Delivery))
                .Build();

            var validationErrors = new List<ValidationError>();

            // Act
            sut.ValidateAsync(order, validationErrors);

            // Assert
            Assert.AreEqual(1, validationErrors.Count);
            Assert.AreEqual(ValidationErrorSubType.DeliveryInformationZipCodeMissing, validationErrors[0].ValidationErrorSubType);
        }

        [Test]
        public void Validate_WithDeliveryInformationRequiredAndWithoutCity_ReturnsValidationError()
        {
            // Arrange
            var sut = A.DeliveryInformationValidator.Build();
            var order = A.OrderEntity
                .W(A.DeliveryInformationEntity
                    .W((City)" "))
                .W(A.ServiceMethodEntity
                    .W(Crave.Enums.ServiceMethodType.Delivery))
                .Build();

            var validationErrors = new List<ValidationError>();

            // Act
            sut.ValidateAsync(order, validationErrors);

            // Assert
            Assert.AreEqual(1, validationErrors.Count);
            Assert.AreEqual(ValidationErrorSubType.DeliveryInformationCityMissing, validationErrors[0].ValidationErrorSubType);
        }

        [Test]
        public void Validate_WithCustomerInDeliveryRange_ReturnsNoValidationErrors()
        {
            // Arrange
            var sut = A.DeliveryInformationValidator
                .W(A.GetDeliveryRateUseCase
                    .W(A.ServiceMethodRepositoryMock
                        .W(A.ServiceMethodEntity))
                    .W(A.GetDistanceToOutletUseCase
                        .W(A.GetOutletLocationUseCase
                            .W(A.CompanyRepositoryMock
                                .W(A.CompanyEntity)))
                    .W(A.GetDistanceBetweenLocationsUseCase.W(A.DistanceClientMock))))
                .Build();

            var order = A.OrderEntity
                .W(A.DeliveryInformationEntity)
                .W(A.ServiceMethodEntity
                    .W(Crave.Enums.ServiceMethodType.Delivery))
                .Build();

            var validationErrors = new List<ValidationError>();

            // Act
            sut.ValidateAsync(order, validationErrors);

            // Assert
            Assert.AreEqual(0, validationErrors.Count);
        }

        [Test]
        public void Validate_WithDistanceApiFailing_ReturnsError()
        {
            // Arrange
            var sut = A.DeliveryInformationValidator
                .W(A.GetDeliveryRateUseCase
                    .W(A.ServiceMethodRepositoryMock
                        .W(A.ServiceMethodEntity))
                    .W(A.GetDistanceToOutletUseCase
                        .W(A.GetOutletLocationUseCase
                            .W(A.CompanyRepositoryMock
                                .W(A.CompanyEntity)))
                    .W(A.GetDistanceBetweenLocationsUseCase
                            .W(A.DistanceClientMock
                                .W(A.GetDistanceResponse
                                    .W(DistanceResult.OVER_DAILY_LIMIT))))))
                .Build();

            var order = A.OrderEntity
                .W(A.DeliveryInformationEntity)
                .W(A.ServiceMethodEntity
                    .W(Crave.Enums.ServiceMethodType.Delivery))
                .Build();

            var validationErrors = new List<ValidationError>();

            // Act
            sut.ValidateAsync(order, validationErrors);

            // Assert
            Assert.AreEqual(1, validationErrors.Count);
            Assert.AreEqual(ValidationErrorSubType.UnableToRetrieveDistanceBetweenOutletAndDeliveryLocation, validationErrors[0].ValidationErrorSubType);
        }

        [Test]
        public void Validate_WithoutDeliveryDistanceSpecified_ReturnsError()
        {
            // Arrange
            var sut = A.DeliveryInformationValidator
                .W(A.GetDeliveryRateUseCase
                    .W(A.ServiceMethodRepositoryMock
                        .W(A.ServiceMethodEntity
                            .W(new DeliveryDistanceEntity[0]))))
                .Build();

            var order = A.OrderEntity
                .W(A.DeliveryInformationEntity)
                .W(A.ServiceMethodEntity
                    .W(Crave.Enums.ServiceMethodType.Delivery))
                .Build();

            var validationErrors = new List<ValidationError>();

            // Act
            sut.ValidateAsync(order, validationErrors);

            // Assert
            Assert.AreEqual(1, validationErrors.Count);
            Assert.AreEqual(ValidationErrorSubType.NoDeliveryDistanceSpecifiedForServiceMethod, validationErrors[0].ValidationErrorSubType);
        }

        [Test]
        public void Validate_WithDistanceTooFar_ReturnsError()
        {
            // Arrange
            const int actualDistance = 15001;
            const int maximumDistance = 15000;

            // Act
            var validationErrors = DeliveryInformationValidatorTest.RunDeliveryRangeTest(maximumDistance, actualDistance);

            // Assert
            Assert.AreEqual(1, validationErrors.Count);
            Assert.AreEqual(ValidationErrorSubType.DeliveryLocationOutsideDeliveryArea, validationErrors[0].ValidationErrorSubType);
        }

        [Test]
        public void Validate_WithDistanceAtMaximum_ReturnsNoError()
        {
            // Arrange
            const int actualDistance = 15000;
            const int maximumDistance = 15000;

            // Act
            var validationErrors = DeliveryInformationValidatorTest.RunDeliveryRangeTest(maximumDistance, actualDistance);

            // Assert
            Assert.AreEqual(0, validationErrors.Count);
        }

        [Test]
        public void Validate_WithCorrectDeliveryCharge_ReturnsNoError()
        {
            // Arrange
            const decimal actualCharge = 8.95M;
            const decimal expectedCharge = 8.95M;

            // Act
            var validationErrors = DeliveryInformationValidatorTest.RunDeliveryChargeTest(expectedCharge, actualCharge);

            // Assert
            Assert.AreEqual(0, validationErrors.Count);
        }

        [Test]
        public void Validate_WithDeliveryChargeTooLow_ReturnsError()
        {
            // Arrange
            const decimal actualCharge = 8.94M;
            const decimal expectedCharge = 8.95M;

            // Act
            var validationErrors = DeliveryInformationValidatorTest.RunDeliveryChargeTest(expectedCharge, actualCharge);

            // Assert
            Assert.AreEqual(1, validationErrors.Count);
            Assert.AreEqual(ValidationErrorSubType.DeliveryChargePriceInvalid, validationErrors[0].ValidationErrorSubType);
        }

        [Test]
        public void Validate_WithDeliveryChargeTooHigh_ReturnsError()
        {
            // Arrange
            const decimal actualCharge = 8.95M;
            const decimal expectedCharge = 8.94M;

            // Act
            var validationErrors = DeliveryInformationValidatorTest.RunDeliveryChargeTest(expectedCharge, actualCharge);

            // Assert
            Assert.AreEqual(1, validationErrors.Count);
            Assert.AreEqual(ValidationErrorSubType.DeliveryChargePriceInvalid, validationErrors[0].ValidationErrorSubType);
        }

        private static List<ValidationError> RunDeliveryRangeTest(int maximumDistance, int actualDistance)
        {
            var validationErrors = new List<ValidationError>();

            var sut = A.DeliveryInformationValidator
                .W(A.GetDeliveryRateUseCase
                    .W(A.ServiceMethodRepositoryMock
                        .W(A.ServiceMethodEntity
                            .W(new[] { A.DeliveryDistanceEntity.W(maximumDistance).Build() })))
                    .W(A.GetDistanceToOutletUseCase
                        .W(A.GetOutletLocationUseCase
                            .W(A.CompanyRepositoryMock
                                .W(A.CompanyEntity)))
                        .W(A.GetDistanceBetweenLocationsUseCase
                            .W(A.DistanceClientMock
                                .W(A.GetDistanceResponse
                                    .W(DistanceResult.OK)
                                    .W(new Distance(actualDistance)))))))
                .Build();

            var order = A.OrderEntity
                .W(A.DeliveryInformationEntity)
                .W(A.ServiceMethodEntity
                    .W(Crave.Enums.ServiceMethodType.Delivery))
                .Build();


            // Act
            sut.ValidateAsync(order, validationErrors);
            return validationErrors;
        }

        private static List<ValidationError> RunDeliveryChargeTest(decimal expectedDeliveryCharge, decimal actualDeliveryCharge)
        {
            var validationErrors = new List<ValidationError>();

            var sut = A.DeliveryInformationValidator
                .W(A.GetDeliveryRateUseCase
                    .W(A.ServiceMethodRepositoryMock
                        .W(A.ServiceMethodEntity
                            .W(new[] { A.DeliveryDistanceEntity.W(expectedDeliveryCharge).Build() })))
                    .W(A.GetDistanceToOutletUseCase
                        .W(A.GetOutletLocationUseCase
                            .W(A.CompanyRepositoryMock
                                .W(A.CompanyEntity)))
                        .W(A.GetDistanceBetweenLocationsUseCase
                            .W(A.DistanceClientMock
                                .W(A.GetDistanceResponse
                                    .W(DistanceResult.OK))))))
                .Build();

            var order = A.OrderEntity
                .W(A.OrderitemEntity
                    .W(Crave.Enums.OrderitemType.DeliveryCharge)
                    .W((PriceTotalInTax)actualDeliveryCharge))
                .W(A.DeliveryInformationEntity)
                .W(A.ServiceMethodEntity
                    .W(Crave.Enums.ServiceMethodType.Delivery))
                .Build();

            OrderitemEntity deliveryChargeOrderitemEntity = order.OrderitemCollection.FirstOrDefault(x => x.Type == OrderitemType.DeliveryCharge);
             

            deliveryChargeOrderitemEntity.PriceInTax = actualDeliveryCharge;
            Assert.AreEqual(actualDeliveryCharge, deliveryChargeOrderitemEntity.DisplayPrice);
            // Act
            sut.ValidateAsync(order, validationErrors);
            return validationErrors;
        }
    }
}
