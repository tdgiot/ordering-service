using Crave.Enums;
using Crave.Ordering.Api.Core.Enums;
using Crave.Ordering.Api.Domain.Exceptions;
using Crave.Ordering.Api.Domain.Tests.Builders.EntityBuilders;
using Crave.Ordering.Api.Domain.Tests.Builders.ValidationBuilders;
using Crave.Ordering.Api.Domain.Validation;
using Crave.Ordering.Api.Domain.Validation.Errors;
using Crave.Ordering.Shared.Data.LLBLGen.Tests.Builders;
using Crave.Ordering.Shared.Data.LLBLGen.Tests.DomainTypes;
using Crave.Ordering.Shared.Data.LLBLGen.Tests.Structs;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Crave.Ordering.Api.Domain.Tests.Validation
{
    public class ServiceChargeValidatorTest
    {
        private static class A
        {
            public static OrderEntityBuilder OrderEntity => new OrderEntityBuilder();
            public static OutletEntityBuilder OutletEntity => new OutletEntityBuilder();
            public static OrderitemEntityBuilder OrderitemEntity => new OrderitemEntityBuilder();
            public static ServiceMethodEntityBuilder ServiceMethodEntity => new ServiceMethodEntityBuilder();

        }

        [Test]
        public void Validate_WithNullOrderEntity_Throws()
        {
            static void Action()
            {
                // Act
                ServiceChargeValidator.Validate(null, new List<ValidationError>());
            }

            // Assert
            _ = Assert.Throws<NullReferenceException>(Action);
        }

        [Test]
        public void Validate_WithServiceChargeRequiredAndWithoutServiceChargeOrderItem_Trows()
        {
            static void Action()
            {
                // Arrange
                var order = A.OrderEntity
                    .W(A.ServiceMethodEntity
                        .W((ServiceChargeAmount)1))
                    .W(A.OutletEntity
                        .W((ServiceChargeProductId)1))
                    .Build();

                // Act
                ServiceChargeValidator.Validate(order, new List<ValidationError>());
            }

            // Assert
            _ = Assert.Throws<CheckoutValidationException>(Action);
        }

        [Test]
        public void Validate_WithServiceChargeNotRequiredAndWithServiceChargeOrderItem_Trows()
        {
            static void Action()
            {
                // Arrange
                var order = A.OrderEntity
                    .W(A.ServiceMethodEntity.W((ServiceChargeAmount)null))
                    .W(A.ServiceMethodEntity.W((ServiceChargeAmount)null))
                    .W(A.OutletEntity)
                    .W(A.OrderitemEntity
                        .W(OrderitemType.ServiceCharge))
                    .Build();

                // Act
                ServiceChargeValidator.Validate(order, new List<ValidationError>());
            }

            // Assert
            var exception = Assert.Throws<CheckoutValidationException>(Action);
            Assert.AreEqual(ValidationErrorSubType.ServiceChargeNotRequired, exception.CheckoutValidationResponse.ValidationErrors.Single().ValidationErrorSubType);
        }

        [Test]
        public void Validate_WithServiceChargeNotRequiredAndWithoutServiceChargeOrderItem_ReturnsNoError()
        {
            // Arrange
            var order = A.OrderEntity
                .W(A.ServiceMethodEntity.W((ServiceChargeAmount)null))
                .W(A.OutletEntity)
                .Build();

            var validationErrors = new List<ValidationError>();

            // Act
            ServiceChargeValidator.Validate(order, validationErrors);

            // Assert
            Assert.AreEqual(0, validationErrors.Count);
        }

        [Test]
        public void Validate_WithWrongServiceChargeAmount_ReturnsError()
        {
            // Arrange
            var order = A.OrderEntity
                .W(A.ServiceMethodEntity
                    .W((ServiceChargeAmount)1))
                .W(A.OutletEntity
                    .W((ServiceChargeProductId)1))
                .W(A.OrderitemEntity
                    .W(OrderitemType.ServiceCharge))
                .Build();

            var validationErrors = new List<ValidationError>();

            // Act
            ServiceChargeValidator.Validate(order, validationErrors);

            // Assert
            Assert.AreEqual(1, validationErrors.Count);
            Assert.AreEqual(ValidationErrorSubType.ServiceChargeInvalid, validationErrors[0].ValidationErrorSubType);
        }

        [Test]
        public void Validate_WithCorrectServiceChargeAmount_ReturnsNoError()
        {
            // Arrange
            var order = A.OrderEntity
                .W(A.ServiceMethodEntity
                    .W((ServiceChargeAmount)1))
                .W(A.OutletEntity
                    .W((ServiceChargeProductId)1))
                .W(A.OrderitemEntity
                    .W(OrderitemType.ServiceCharge)
                    .W((PriceTotalInTax)1M)
                    .W((ProductPriceIn)1M))
                .Build();

            var validationErrors = new List<ValidationError>();

            // Act
            ServiceChargeValidator.Validate(order, validationErrors);

            // Assert
            Assert.AreEqual(0, validationErrors.Count);
        }

        [Test]
        public void Validate_WithWrongServiceChargePercentage_ReturnsError()
        {
            // Arrange
            var order = A.OrderEntity
                .W(A.ServiceMethodEntity
                    .W((ServiceChargePercentage)5))
                .W(A.OutletEntity
                    .W((ServiceChargeProductId)1))
                .W(new[]
                {
                        A.OrderitemEntity.W(OrderitemType.ServiceCharge).W((PriceTotalInTax)5.5M).Build(),
                        A.OrderitemEntity.W(OrderitemType.Product).W((PriceTotalInTax)100M).Build()

                })
                .Build();

            var validationErrors = new List<ValidationError>();

            // Act
            ServiceChargeValidator.Validate(order, validationErrors);

            // Assert
            Assert.AreEqual(1, validationErrors.Count);
            Assert.AreEqual(ValidationErrorSubType.ServiceChargeInvalid, validationErrors[0].ValidationErrorSubType);
        }

        [Test]
        public void Validate_WithCorrectServiceChargePercentage_ReturnsNoError()
        {
            // Arrange
            var order = A.OrderEntity
                .W(A.ServiceMethodEntity
                    .W((ServiceChargePercentage)5))
                .W(A.OutletEntity
                    .W((ServiceChargeProductId)1))
                .W(new[]
                {
                        A.OrderitemEntity.W(OrderitemType.ServiceCharge)
                         .W((PriceTotalInTax)5M)
                         .W((ProductPriceIn)5M)
                         .Build(),
                        A.OrderitemEntity.W(OrderitemType.Product)
                         .W((PriceTotalInTax)100M)
                         .W((ProductPriceIn)100M)
                         .Build()

                })
                .Build();

            var validationErrors = new List<ValidationError>();

            // Act
            ServiceChargeValidator.Validate(order, validationErrors);

            // Assert
            Assert.AreEqual(0, validationErrors.Count);
        }

        [Test]
        public void Validate_WithServiceChargeAmountAndPercentage_UsesThePercentage()
        {
            // Arrange
            var order = A.OrderEntity
                .W(A.ServiceMethodEntity
                    .W((ServiceChargePercentage)5)
                    .W((ServiceChargeAmount)1))
                .W(A.OutletEntity
                    .W((ServiceChargeProductId)1))
                .W(new[]
                {
                        A.OrderitemEntity.W(OrderitemType.ServiceCharge)
                         .W((PriceTotalInTax)5M)
                         .W((ProductPriceIn)5M)
                         .Build(),
                        A.OrderitemEntity.W(OrderitemType.Product)
                         .W((PriceTotalInTax)100M)
                         .W((ProductPriceIn)100M)
                         .Build()

                })
                .Build();

            var validationErrors = new List<ValidationError>();

            // Act
            ServiceChargeValidator.Validate(order, validationErrors);

            // Assert
            Assert.AreEqual(0, validationErrors.Count);
        }

        [Test]
        public void Validate_WithServiceCalculatedButOrderAmountAboveFreeLimit_ReturnsError()
        {
            // Arrange
            var order = A.OrderEntity
                .W(A.ServiceMethodEntity
                    .W((ServiceChargePercentage)5)
                    .W((MinimumAmountForFreeServiceCharge)10))
                .W(A.OutletEntity
                    .W((ServiceChargeProductId)1))
                .W(new[]
                {
                        A.OrderitemEntity.W(OrderitemType.ServiceCharge)
                         .W((PriceTotalInTax)5M)
                         .W((ProductPriceIn)5M)
                         .Build(),
                        A.OrderitemEntity.W(OrderitemType.Product)
                         .W((PriceTotalInTax)100M)
                         .W((ProductPriceIn)100M)
                         .Build()

                })
                .Build();

            var validationErrors = new List<ValidationError>();

            // Act
            ServiceChargeValidator.Validate(order, validationErrors);

            // Assert
            Assert.AreEqual(1, validationErrors.Count);
            Assert.AreEqual(ValidationErrorSubType.ServiceChargeInvalid, validationErrors[0].ValidationErrorSubType);
        }

        [Test]
        public void Validate_WithServiceCalculatedButOrderAmountEqualToFreeLimit_ReturnsError()
        {
            // Arrange
            var order = A.OrderEntity
                .W(A.ServiceMethodEntity
                    .W((ServiceChargePercentage)5)
                    .W((MinimumAmountForFreeServiceCharge)10))
                .W(A.OutletEntity
                    .W((ServiceChargeProductId)1))
                .W(new[]
                {
                        A.OrderitemEntity.W(OrderitemType.ServiceCharge)
                         .W((PriceTotalInTax)5M)
                         .W((ProductPriceIn)5M)
                         .Build(),
                        A.OrderitemEntity.W(OrderitemType.Product)
                         .W((PriceTotalInTax)10M)
                         .W((ProductPriceIn)10M)
                         .Build()

                })
                .Build();

            var validationErrors = new List<ValidationError>();

            // Act
            ServiceChargeValidator.Validate(order, validationErrors);

            // Assert
            Assert.AreEqual(1, validationErrors.Count);
            Assert.AreEqual(ValidationErrorSubType.ServiceChargeInvalid, validationErrors[0].ValidationErrorSubType);
        }

        [Test]
        public void Validate_WithServiceCalculatedButOrderAmountBelowFreeLimit_ReturnsNoError()
        {
            // Arrange
            var order = A.OrderEntity
                .W(A.ServiceMethodEntity
                    .W((ServiceChargeAmount)5)
                    .W((MinimumAmountForFreeServiceCharge)10))
                .W(A.OutletEntity
                    .W((ServiceChargeProductId)1))
                .W(new[]
                {
                        A.OrderitemEntity.W(OrderitemType.ServiceCharge)
                         .W((PriceTotalInTax)5M)
                         .W((ProductPriceIn)5M)
                         .Build(),
                        A.OrderitemEntity.W(OrderitemType.Product)
                         .W((PriceTotalInTax)9M)
                         .W((ProductPriceIn)9M)
                         .Build()

                })
                .Build();

            var validationErrors = new List<ValidationError>();

            // Act
            ServiceChargeValidator.Validate(order, validationErrors);

            // Assert
            Assert.AreEqual(0, validationErrors.Count);
        }
    }
}
