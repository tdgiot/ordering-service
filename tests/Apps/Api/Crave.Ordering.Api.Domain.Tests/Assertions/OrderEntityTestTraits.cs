﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using Crave.Ordering.Api.Domain.UseCases.Requests;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using NUnit.Framework;

namespace Crave.Ordering.Api.Domain.Tests.Assertions
{
    internal static class OrderEntityTestTraits
	{
		public static void AssertEqual(this OrderEntity orderEntity, CompanyEntity companyEntity)
		{
			var unequalProperties = new string[0]
				.PropertyEqual(nameof(orderEntity.Company), orderEntity.Company, companyEntity)
				.PropertyEqual(nameof(orderEntity.CompanyId), orderEntity.CompanyId, companyEntity.CompanyId)
				.PropertyEqual(nameof(orderEntity.CompanyName), orderEntity.CompanyName, companyEntity.Name)
				.PropertyEqual(nameof(orderEntity.CultureCode), orderEntity.CultureCode, companyEntity.CultureCode)
				.PropertyEqual(nameof(orderEntity.CountryCode), orderEntity.CountryCode, companyEntity.CountryCode)
				.PropertyEqual(nameof(orderEntity.CurrencyCode), orderEntity.CurrencyCode, companyEntity.CurrencyCode)
				.PropertyEqual(nameof(orderEntity.TimeZoneOlsonId), orderEntity.TimeZoneOlsonId, companyEntity.TimeZoneOlsonId)
				.PropertyEqual(nameof(orderEntity.PricesIncludeTaxes), orderEntity.PricesIncludeTaxes, companyEntity.PricesIncludeTaxes);

			if (!unequalProperties.Any())
			{
				return;
			}

			OrderEntityTestTraits.HandleUnequal(orderEntity.GetType().Name, companyEntity.GetType().Name, unequalProperties);
		}

		public static void AssertEqual(this OrderEntity orderEntity, OutletEntity outletEntity)
		{
			var unequalProperties = new string[0]
				.PropertyEqual(nameof(orderEntity.Outlet), orderEntity.Outlet, outletEntity)
				.PropertyEqual(nameof(orderEntity.OutletId), orderEntity.OutletId, outletEntity.OutletId);

			if (!unequalProperties.Any())
			{
				return;
			}

			OrderEntityTestTraits.HandleUnequal(orderEntity.GetType().Name, outletEntity.GetType().Name, unequalProperties);
		}

		public static void AssertEqual(this OrderEntity orderEntity, ServiceMethodEntity serviceMethodEntity)
		{
			var unequalProperties = new string[0]
				.PropertyEqual(nameof(orderEntity.ServiceMethod), orderEntity.ServiceMethod, serviceMethodEntity)
				.PropertyEqual(nameof(orderEntity.ServiceMethodId), orderEntity.ServiceMethodId, serviceMethodEntity.ServiceMethodId)
				.PropertyEqual(nameof(orderEntity.ServiceMethodName), orderEntity.ServiceMethodName, serviceMethodEntity.Name)
				.PropertyEqual(nameof(orderEntity.ServiceMethodType), orderEntity.ServiceMethodType, serviceMethodEntity.Type);

			if (!unequalProperties.Any())
			{
				return;
			}

			OrderEntityTestTraits.HandleUnequal(orderEntity.GetType().Name, serviceMethodEntity.GetType().Name, unequalProperties);
		}

		public static void AssertEqual(this OrderEntity orderEntity, CheckoutMethodEntity CheckoutMethodEntity)
		{
			var unequalProperties = new string[0]
				.PropertyEqual(nameof(orderEntity.CheckoutMethod), orderEntity.CheckoutMethod, CheckoutMethodEntity)
				.PropertyEqual(nameof(orderEntity.CheckoutMethodId), orderEntity.CheckoutMethodId, CheckoutMethodEntity.CheckoutMethodId)
				.PropertyEqual(nameof(orderEntity.CheckoutMethodName), orderEntity.CheckoutMethodName, CheckoutMethodEntity.Name)
				.PropertyEqual(nameof(orderEntity.CheckoutMethodType), orderEntity.CheckoutMethodType, CheckoutMethodEntity.CheckoutType);

			if (!unequalProperties.Any())
			{
				return;
			}

			OrderEntityTestTraits.HandleUnequal(orderEntity.GetType().Name, CheckoutMethodEntity.GetType().Name, unequalProperties);
		}

		public static void AssertDeliverypointEqual(this OrderEntity orderEntity, DeliverypointEntity deliverypointEntity)
		{
			var unequalProperties = new string[0]
				.PropertyEqual(nameof(orderEntity.Deliverypoint), orderEntity.Deliverypoint, deliverypointEntity)
				.PropertyEqual(nameof(orderEntity.DeliverypointId), orderEntity.DeliverypointId, deliverypointEntity.DeliverypointId)
				.PropertyEqual(nameof(orderEntity.DeliverypointName), orderEntity.DeliverypointName, deliverypointEntity.Name)
				.PropertyEqual(nameof(orderEntity.DeliverypointNumber), orderEntity.DeliverypointNumber, deliverypointEntity.Number);

			if (!unequalProperties.Any())
			{
				return;
			}

			OrderEntityTestTraits.HandleUnequal(orderEntity.GetType().Name, deliverypointEntity.GetType().Name, unequalProperties);
		}

		public static void AssertCheckoutMethodEqual(this OrderEntity orderEntity, DeliverypointEntity deliverypointEntity)
		{
			var unequalProperties = new string[0]
				.PropertyEqual(nameof(orderEntity.ChargeToDeliverypoint), orderEntity.ChargeToDeliverypoint, deliverypointEntity)
				.PropertyEqual(nameof(orderEntity.ChargeToDeliverypointId), orderEntity.ChargeToDeliverypointId, deliverypointEntity.DeliverypointId)
				.PropertyEqual(nameof(orderEntity.ChargeToDeliverypointName), orderEntity.ChargeToDeliverypointName, deliverypointEntity.Name)
				.PropertyEqual(nameof(orderEntity.ChargeToDeliverypointNumber), orderEntity.ChargeToDeliverypointNumber, deliverypointEntity.Number);

			if (!unequalProperties.Any())
			{
				return;
			}

			OrderEntityTestTraits.HandleUnequal(orderEntity.GetType().Name, deliverypointEntity.GetType().Name, unequalProperties);
		}

		public static void AssertEqual(this OrderEntity orderEntity, CustomerRequest customer)
		{
			var unequalProperties = new string[0]
				.PropertyEqual(nameof(orderEntity.CustomerLastname), orderEntity.CustomerLastname, customer.LastName)
				.PropertyEqual(nameof(orderEntity.CustomerPhonenumber), orderEntity.CustomerPhonenumber, customer.Phonenumber)
				.PropertyEqual(nameof(orderEntity.Email), orderEntity.Email, customer.Email);

			if (!unequalProperties.Any())
			{
				return;
			}

			OrderEntityTestTraits.HandleUnequal(orderEntity.GetType().Name, customer.GetType().Name, unequalProperties);
		}

		public static void AssertEqual(this OrderEntity orderEntity, DeliveryInformationRequest deliveryInformation)
		{
			var unequalProperties = new string[0]
				.PropertyEqual(nameof(orderEntity.DeliveryInformation.BuildingName), orderEntity.DeliveryInformation.BuildingName, deliveryInformation.BuildingName)
				.PropertyEqual(nameof(orderEntity.DeliveryInformation.Address), orderEntity.DeliveryInformation.Address, deliveryInformation.Address)
				.PropertyEqual(nameof(orderEntity.DeliveryInformation.Zipcode), orderEntity.DeliveryInformation.Zipcode, deliveryInformation.Zipcode)
				.PropertyEqual(nameof(orderEntity.DeliveryInformation.City), orderEntity.DeliveryInformation.City, deliveryInformation.City)
				.PropertyEqual(nameof(orderEntity.DeliveryInformation.Instructions), orderEntity.DeliveryInformation.Instructions, deliveryInformation.Instructions)
				.PropertyEqual(nameof(orderEntity.DeliveryInformation.Longitude), orderEntity.DeliveryInformation.Longitude, deliveryInformation.Longitude)
				.PropertyEqual(nameof(orderEntity.DeliveryInformation.Latitude), orderEntity.DeliveryInformation.Latitude, deliveryInformation.Latitude)
				.PropertyEqual(nameof(orderEntity.DeliveryInformation.DistanceToOutletInMetres), orderEntity.DeliveryInformation.DistanceToOutletInMetres, deliveryInformation.DistanceToOutletInMetres);

			if (!unequalProperties.Any())
			{
				return;
			}

			OrderEntityTestTraits.HandleUnequal(orderEntity.GetType().Name, deliveryInformation.GetType().Name, unequalProperties);
		}

        public static void AssertEqual(this OrderEntity orderEntity, OrderitemRequest[] orderitems)
        {
            foreach (var orderitem in orderitems)
            {
                var orderitemEntity = orderEntity.OrderitemCollection.First(item => item.ProductId == orderitem.ProductId);
                orderitemEntity.AssertEqual(orderitem);
            }
        }

		public static void AssertEqual(this OrderitemEntity orderitemEntity, OrderitemRequest orderitem)
		{
			var unequalProperties = new string[0]
				.PropertyEqual(nameof(orderitemEntity.Type), orderitemEntity.Type, orderitem.Type)
				.PropertyEqual(nameof(orderitemEntity.Quantity), orderitemEntity.Quantity, orderitem.Quantity)
                .PropertyEqual(nameof(orderitemEntity.ProductPriceIn), orderitemEntity.ProductPriceIn, orderitem.Price);

			orderitemEntity.AssertEqual(orderitem.Alterationitems);

            if (!unequalProperties.Any())
			{
				return;
			}

            OrderEntityTestTraits.HandleUnequal(orderitemEntity.GetType().Name, orderitem.GetType().Name, unequalProperties);
		}

        public static void AssertEqual(this OrderitemEntity orderitemEntity, IEnumerable<AlterationitemRequest>? alterationitems)
        {
            if (alterationitems == null)
            {
                return;
            }

            foreach (AlterationitemRequest alterationitem in alterationitems)
            {
                OrderitemAlterationitemEntity orderitemAlterationitemEntity = orderitemEntity.OrderitemAlterationitemCollection.First(item => item.AlterationId == alterationitem.AlterationId);
                orderitemAlterationitemEntity.AssertEqual(alterationitem);
            }
        }

		public static void AssertEqual(this OrderitemAlterationitemEntity orderitemAlterationitemEntity, AlterationitemRequest alterationitem)
        {
            var unequalProperties = new string[0]
                                    .PropertyEqual(nameof(orderitemAlterationitemEntity.AlterationId), orderitemAlterationitemEntity.AlterationId, alterationitem.AlterationId)
                                    .PropertyEqual(nameof(orderitemAlterationitemEntity.AlterationoptionPriceIn), orderitemAlterationitemEntity.AlterationoptionPriceIn, alterationitem.Price);

            if (!unequalProperties.Any())
            {
                return;
            }

            OrderEntityTestTraits.HandleUnequal(orderitemAlterationitemEntity.GetType().Name, alterationitem.GetType().Name, unequalProperties);
        }

        public static void AssertEqual(this OrderEntity orderEntity, int productId, ScheduleEntity scheduleEntity)
		{
			var product = orderEntity.OrderitemCollection.First(item => item.ProductId == productId).Product;

			if (product.Schedule == scheduleEntity)
            {
				return;
            }

			OrderEntityTestTraits.HandleUnequal(orderEntity.GetType().Name, scheduleEntity.GetType().Name, new[] { nameof(product.Schedule) });
		}


		private static void HandleUnequal(string xTypeName, string yTypeName, string[] unequalProperties)
		{
			var errorInformation = new StringBuilder($"The following properties on {xTypeName} are not equal to the equivalent property on {yTypeName}");
			errorInformation = unequalProperties.Aggregate(errorInformation, (current, unequalProperty) => current.AppendLine(unequalProperty));
			throw new AssertionException(errorInformation.ToString());
		}

		private static string[] PropertyEqual<TProperty>(this string[] self, string propertyName, TProperty x, TProperty y) =>
			EqualityComparer<TProperty>.Default.Equals(x, y)
				? self
				: self.Append(propertyName).ToArray();
	}
}