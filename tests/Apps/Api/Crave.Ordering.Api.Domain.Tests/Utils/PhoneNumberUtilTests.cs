﻿using Crave.Ordering.Api.Domain.Utils;
using NUnit.Framework;

namespace Crave.Ordering.Api.Domain.Tests.Utils
{
    [TestFixture]
    public class PhoneNumberUtilTests
    {
        [TestCase("+31 123456789", "0031123456789")]
        [TestCase("+31 123-456-789", "0031123456789")]
        [TestCase("0031 123 456 789", "0031123456789")]
        [TestCase("+31123456789", "0031123456789")]
        [TestCase(" 123456789", "123456789")]
        [TestCase("123456789 ", "123456789")]
        [TestCase("123.456.789 ", "123456789")]
        [TestCase("", "")]
        [TestCase(null, "")]
        public void Standardize(string input, string expected)
        {
            string result = PhoneNumberUtil.Standardize(input);

            Assert.AreEqual(expected, result);
        }
    }
}
