﻿using Newtonsoft.Json;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Crave.Integrations.POS.Deliverect.Tests.Utility
{
	public static class ModelFromResource
	{
		public static T Get<T>(string filename)
		{
			string resourceName = Assembly.GetExecutingAssembly().GetManifestResourceNames().Single(file => file.EndsWith(filename));
			Stream stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(resourceName);
			string result = ToEncodedString(stream);
			return JsonConvert.DeserializeObject<T>(result);
		}

		public static string ToEncodedString(Stream stream)
		{

			byte[] bytes = new byte[stream.Length];
			stream.Position = 0;
			stream.Read(bytes, 0, (int)stream.Length);

			return Encoding.UTF8.GetString(bytes);
		}
	}
}
