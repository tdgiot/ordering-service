using Crave.Enums;
using Crave.Integrations.POS.Deliverect.Domain.Requests;
using Crave.Integrations.POS.Deliverect.Domain.UseCases;
using Crave.Integrations.POS.Deliverect.Tests.Builders;
using Crave.Ordering.Pos.Tests.Builders;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.Tests.Builders;
using Crave.Ordering.Shared.Data.LLBLGen.Tests.DomainTypes;
using Crave.Ordering.Shared.Domain;
using NUnit.Framework;
using System;
using System.Linq;
using ExternalProductEntityBuilder = Crave.Ordering.Shared.Data.LLBLGen.Tests.Builders.ExternalProductEntityBuilder;

namespace Crave.Integrations.POS.Deliverect.Tests
{
    [TestFixture, Explicit]
    [Category("Integration"), Category("Deliverect")]
    public class ConvertOrderUseCaseTests
    {
        public static class A
        {
            public static DeliverectClientBuilder DeliverectClient => new DeliverectClientBuilder();
            public static OrderEntityBuilder Order => new OrderEntityBuilder();
            public static OrderitemEntityBuilder Orderitem => new OrderitemEntityBuilder();
            public static ProductEntityBuilder Product => new ProductEntityBuilder();
            public static ExternalSystemEntityBuilder ExternalSystem => new ExternalSystemEntityBuilder();
            public static ExternalProductEntityBuilder ExternalProduct => new ExternalProductEntityBuilder();
        }

        private const string ClientId = "TZ9oO2vLXc4CBcCd";
        private const string ClientSecret = "8NIwMO17CDAozXaVwtF04rZ0nLJ4MUak";

        private ConvertOrderUseCase convertOrderUseCase;
        private DeliverectClient deliverectClient;

        [SetUp]
        public void Setup()
        {
            this.convertOrderUseCase = new ConvertOrderUseCase();

            deliverectClient = A.DeliverectClient
                .WithClientId(ClientId)
                .WithClientSecret(ClientSecret)
                .Build();
        }

        [Test]
        public void Execute_WithExternalSystemIncludesTaxesTrue_RequestTaxCollectionIsNull()
        {
            OrderEntity orderEntity = A.Order
                .W((OrderId)2)
                .W((CustomerEmail)"user@crave-emenu.com")
                .W((CustomerLastname)"Craver")
                .W((CustomerPhoneNumber)"1234567890")
                .W(ServiceMethodType.PickUp)
                .W(OrderType.Standard)
                .W(OrderStatus.Processed)
                .W(CheckoutType.PaymentProvider)
                .W((CurrencyCode)"$")
                .W((decimal)15)
                .WithNotes("Hello, this  is a test note.")
                .WithGuid(Guid.NewGuid().ToString())
                .WithCheckoutMethodName("Pay Direct")
                .ClearOrderitems()
                .W(A.Orderitem
                    .WithProductName("Chicken Burger")
                    .W((PriceExTax)11)
                    .W((PriceInTax)13)
                    .W((PriceTotalInTax)11)
                    .W((PriceTotalExTax)13)
                    .W(OrderitemType.Product)
                    .W((Quantity)1)
                    .W((TaxTariffId)1)
                    .W((Tax)2)
                    .WithTaxName("Taxi")
                    .W(A.Product
                        .WithId(1)
                        .WithIsAvailable(true)
                        .WithExternalProductId(100)
                        .W(A.ExternalProduct
                            .W(100)
                            .W("Chicken Burger")
                            .Build())
                        .Build())
                    .Build())
                .W(A.Orderitem
                    .WithProductName("Tip")
                    .W(OrderitemType.Tip)
                    .W((PriceTotalInTax)2)
                    .W((Quantity)1)
                    .Build())
                .Build();

            ExternalSystemEntity externalSystem = A.ExternalSystem
                .WithBoolValue2(true)
                .Build();

            ConvertOrderRequest request = new ConvertOrderRequest(externalSystem, orderEntity);
            Response<OrderRequest> response = convertOrderUseCase.Execute(request);

            Assert.IsNotNull(response.Model);
            Assert.IsTrue(response.Success);

            Assert.IsNull(response.Model.Taxes);
        }

        [Test]
        public void Execute_WithExternalSystemIncludesTaxesFalse_RequestContainsTaxCollection()
        {
            OrderEntity orderEntity = A.Order
                .W((OrderId)2)
                .W((CustomerEmail)"user@crave-emenu.com")
                .W((CustomerLastname)"Craver")
                .W((CustomerPhoneNumber)"1234567890")
                .W(ServiceMethodType.PickUp)
                .W(OrderType.Standard)
                .W(OrderStatus.Processed)
                .W(CheckoutType.PaymentProvider)
                .W((CurrencyCode)"$")
                .W((decimal)15)
                .WithNotes("Hello, this  is a test note.")
                .WithGuid(Guid.NewGuid().ToString())
                .WithCheckoutMethodName("Pay Direct")
                .ClearOrderitems()
                .W(A.Orderitem
                    .WithProductName("Chicken Burger")
                    .W((PriceExTax)11)
                    .W((PriceInTax)13)
                    .W((PriceTotalInTax)11)
                    .W((PriceTotalExTax)13)
                    .W(OrderitemType.Product)
                    .W((Quantity)1)
                    .W((TaxTariffId)1)
                    .W((Tax)2)
                    .WithTaxName("Taxi")
                    .W(A.Product
                        .WithId(1)
                        .WithIsAvailable(true)
                        .WithExternalProductId(100)
                        .W(A.ExternalProduct
                            .W(100)
                            .W("Chicken Burger")
                            .Build())
                        .Build())
                    .Build())
                .W(A.Orderitem
                    .WithProductName("Tip")
                    .W(OrderitemType.Tip)
                    .W((PriceTotalInTax)2)
                    .W((Quantity)1)
                    .Build())
                .Build();

            ExternalSystemEntity externalSystem = A.ExternalSystem
                .WithBoolValue2(false)
                .Build();

            ConvertOrderRequest request = new ConvertOrderRequest(externalSystem, orderEntity);
            Response<OrderRequest> response = convertOrderUseCase.Execute(request);

            Assert.IsNotNull(response.Model);
            Assert.IsTrue(response.Success);

            Assert.IsNotNull(response.Model.Taxes);
        }

        [Test]
        public void Execute_WithTaxes_OnlyAddsProductTaxToTaxCollection()
        {
            OrderEntity orderEntity = A.Order
                .W((OrderId)2)
                .W((CustomerEmail)"user@crave-emenu.com")
                .W((CustomerLastname)"Craver")
                .W((CustomerPhoneNumber)"1234567890")
                .W(ServiceMethodType.PickUp)
                .W(OrderType.Standard)
                .W(OrderStatus.Processed)
                .W(CheckoutType.PaymentProvider)
                .W((CurrencyCode)"$")
                .W((decimal)15)
                .WithNotes("Hello, this  is a test note.")
                .WithGuid(Guid.NewGuid().ToString())
                .WithCheckoutMethodName("Pay Direct")
                .ClearOrderitems()
                .W(A.Orderitem
                    .WithProductName("Chicken Burger")
                    .W((PriceExTax)11)
                    .W((PriceInTax)13)
                    .W((PriceTotalInTax)11)
                    .W((PriceTotalExTax)13)
                    .W(OrderitemType.Product)
                    .W((Quantity)1)
                    .W((TaxTariffId)1)
                    .W((Tax)2)
                    .W((TaxTotal)2)
                    .WithTaxName("Taxi")
                    .W(A.Product
                        .WithId(1)
                        .WithIsAvailable(true)
                        .WithExternalProductId(100)
                        .W(A.ExternalProduct
                            .W(100)
                            .W("Chicken Burger")
                            .Build())
                        .Build())
                    .Build())
                .W(A.Orderitem
                    .WithProductName("Tip")
                    .W(OrderitemType.Tip)
                    .W((PriceTotalInTax)2)
                    .W((Quantity)1)
                    .Build())
                .Build();

            ExternalSystemEntity externalSystem = A.ExternalSystem
                .WithBoolValue2(false)
                .Build();

            ConvertOrderRequest request = new ConvertOrderRequest(externalSystem, orderEntity);
            Response<OrderRequest> response = convertOrderUseCase.Execute(request);

            Assert.IsNotNull(response.Model);
            Assert.IsTrue(response.Success);

            Assert.IsNotNull(response.Model.Taxes);
            Assert.AreEqual(1, response.Model.Taxes.Count);

            var tax = response.Model.Taxes.First();

            Assert.AreEqual("Taxi", tax.Name);
            Assert.AreEqual(1, tax.Taxes);
            Assert.AreEqual(200, tax.Total);
        }

        [Test]
        public void Execute_WithTaxes_GroupsTaxesByTaxTariffId()
        {
            OrderEntity orderEntity = A.Order
                .W((OrderId)2)
                .W((CustomerEmail)"user@crave-emenu.com")
                .W((CustomerLastname)"Craver")
                .W((CustomerPhoneNumber)"1234567890")
                .W(ServiceMethodType.PickUp)
                .W(OrderType.Standard)
                .W(OrderStatus.Processed)
                .W(CheckoutType.PaymentProvider)
                .W((CurrencyCode)"$")
                .W((decimal)15)
                .WithNotes("Hello, this  is a test note.")
                .WithGuid(Guid.NewGuid().ToString())
                .WithCheckoutMethodName("Pay Direct")
                .ClearOrderitems()

                .W(A.Orderitem
                    .WithProductName("Chicken Burger")
                    .W((PriceInTax)13)
                    .W((PriceExTax)11)
                    .W((PriceTotalInTax)13)
                    .W((PriceTotalExTax)11)
                    .W(OrderitemType.Product)
                    .W((Quantity)1)
                    .W((TaxTariffId)1)
                    .W((Tax)2M)
                    .W((TaxTotal)2)
                    .WithTaxName("Taxi")
                    .W(A.Product
                        .WithId(1)
                        .WithIsAvailable(true)
                        .WithExternalProductId(100)
                        .W(A.ExternalProduct
                            .W(100)
                            .W("Chicken Burger")
                            .Build())
                        .Build())
                    .Build())

                .W(A.Orderitem
                    .WithProductName("Fries")
                    .W((PriceInTax)9)
                    .W((PriceExTax)8)
                    .W((PriceTotalInTax)18)
                    .W((PriceTotalExTax)16)
                    .W(OrderitemType.Product)
                    .W((Quantity)2)
                    .W((TaxTariffId)1)
                    .W((Tax)1)
                    .W((TaxTotal)2)
                    .WithTaxName("Taxi")
                    .W(A.Product
                        .WithId(1)
                        .WithIsAvailable(true)
                        .WithExternalProductId(100)
                        .W(A.ExternalProduct
                            .W(100)
                            .W("Fries")
                            .Build())
                        .Build())
                    .Build())

                .W(A.Orderitem
                    .WithProductName("Tip")
                    .W(OrderitemType.Tip)
                    .W((PriceTotalInTax)2)
                    .W((Quantity)1)
                    .Build())
                .Build();

            ExternalSystemEntity externalSystem = A.ExternalSystem
                .WithBoolValue2(false)
                .Build();

            ConvertOrderRequest request = new ConvertOrderRequest(externalSystem, orderEntity);
            Response<OrderRequest> response = convertOrderUseCase.Execute(request);

            Assert.IsNotNull(response.Model);
            Assert.IsTrue(response.Success);

            Assert.IsNotNull(response.Model.Taxes);
            Assert.AreEqual(1, response.Model.Taxes.Count);

            var tax = response.Model.Taxes.First();

            Assert.AreEqual("Taxi", tax.Name);
            Assert.AreEqual(1, tax.Taxes);
            Assert.AreEqual(400, tax.Total);
        }

        [Test]
        public void Execute_WithNumberOfCustomers_MapsNumberOfCustomers()
        {
            OrderEntity orderEntity = A.Order
                .W((OrderId)2)
                .W((CustomerEmail)"user@crave-emenu.com")
                .W((CustomerLastname)"Craver")
                .W((CustomerPhoneNumber)"1234567890")
                .W(ServiceMethodType.PickUp)
                .W(OrderType.Standard)
                .W(OrderStatus.Processed)
                .W(CheckoutType.PaymentProvider)
                .W((CurrencyCode)"$")
                .W((decimal)15)
                .WithNumberOfCustomers(3)
                .WithNotes("Hello, this  is a test note.")
                .WithGuid(Guid.NewGuid().ToString())
                .WithCheckoutMethodName("Pay Direct")
                .ClearOrderitems()

                .W(A.Orderitem
                    .WithProductName("Chicken Burger")
                    .W((PriceInTax)13)
                    .W((PriceExTax)11)
                    .W((PriceTotalInTax)13)
                    .W((PriceTotalExTax)11)
                    .W(OrderitemType.Product)
                    .W((Quantity)1)
                    .W((TaxTariffId)1)
                    .W((Tax)2M)
                    .W((TaxTotal)2)
                    .WithTaxName("Taxi")
                    .W(A.Product
                        .WithId(1)
                        .WithIsAvailable(true)
                        .WithExternalProductId(100)
                        .W(A.ExternalProduct
                            .W(100)
                            .W("Chicken Burger")
                            .Build())
                        .Build())
                    .Build())

                .W(A.Orderitem
                    .WithProductName("Fries")
                    .W((PriceInTax)9)
                    .W((PriceExTax)8)
                    .W((PriceTotalInTax)18)
                    .W((PriceTotalExTax)16)
                    .W(OrderitemType.Product)
                    .W((Quantity)2)
                    .W((TaxTariffId)1)
                    .W((Tax)1)
                    .W((TaxTotal)2)
                    .WithTaxName("Taxi")
                    .W(A.Product
                        .WithId(1)
                        .WithIsAvailable(true)
                        .WithExternalProductId(100)
                        .W(A.ExternalProduct
                            .W(100)
                            .W("Fries")
                            .Build())
                        .Build())
                    .Build())

                .W(A.Orderitem
                    .WithProductName("Tip")
                    .W(OrderitemType.Tip)
                    .W((PriceTotalInTax)2)
                    .W((Quantity)1)
                    .Build())
                .Build();

            ExternalSystemEntity externalSystem = A.ExternalSystem
                .WithBoolValue2(false)
                .Build();

            ConvertOrderRequest request = new ConvertOrderRequest(externalSystem, orderEntity);
            Response<OrderRequest> response = convertOrderUseCase.Execute(request);

            Assert.IsNotNull(response.Model);
            Assert.IsTrue(response.Success);

            Assert.AreEqual(orderEntity.CoversCount, response.Model.NumberOfCustomers);
        }

        [Test]
        public void Execute_WithNumberOfCustomersNull_MapsOneNumberOfCustomers()
        {
            OrderEntity orderEntity = A.Order
                .W((OrderId)2)
                .W((CustomerEmail)"user@crave-emenu.com")
                .W((CustomerLastname)"Craver")
                .W((CustomerPhoneNumber)"1234567890")
                .W(ServiceMethodType.PickUp)
                .W(OrderType.Standard)
                .W(OrderStatus.Processed)
                .W(CheckoutType.PaymentProvider)
                .W((CurrencyCode)"$")
                .W((decimal)15)
                .WithNumberOfCustomers(null)
                .WithNotes("Hello, this  is a test note.")
                .WithGuid(Guid.NewGuid().ToString())
                .WithCheckoutMethodName("Pay Direct")
                .ClearOrderitems()

                .W(A.Orderitem
                    .WithProductName("Chicken Burger")
                    .W((PriceInTax)13)
                    .W((PriceExTax)11)
                    .W((PriceTotalInTax)13)
                    .W((PriceTotalExTax)11)
                    .W(OrderitemType.Product)
                    .W((Quantity)1)
                    .W((TaxTariffId)1)
                    .W((Tax)2M)
                    .W((TaxTotal)2)
                    .WithTaxName("Taxi")
                    .W(A.Product
                        .WithId(1)
                        .WithIsAvailable(true)
                        .WithExternalProductId(100)
                        .W(A.ExternalProduct
                            .W(100)
                            .W("Chicken Burger")
                            .Build())
                        .Build())
                    .Build())

                .W(A.Orderitem
                    .WithProductName("Fries")
                    .W((PriceInTax)9)
                    .W((PriceExTax)8)
                    .W((PriceTotalInTax)18)
                    .W((PriceTotalExTax)16)
                    .W(OrderitemType.Product)
                    .W((Quantity)2)
                    .W((TaxTariffId)1)
                    .W((Tax)1)
                    .W((TaxTotal)2)
                    .WithTaxName("Taxi")
                    .W(A.Product
                        .WithId(1)
                        .WithIsAvailable(true)
                        .WithExternalProductId(100)
                        .W(A.ExternalProduct
                            .W(100)
                            .W("Fries")
                            .Build())
                        .Build())
                    .Build())

                .W(A.Orderitem
                    .WithProductName("Tip")
                    .W(OrderitemType.Tip)
                    .W((PriceTotalInTax)2)
                    .W((Quantity)1)
                    .Build())
                .Build();

            ExternalSystemEntity externalSystem = A.ExternalSystem
                .WithBoolValue2(false)
                .Build();

            ConvertOrderRequest request = new ConvertOrderRequest(externalSystem, orderEntity);
            Response<OrderRequest> response = convertOrderUseCase.Execute(request);

            Assert.IsNotNull(response.Model);
            Assert.IsTrue(response.Success);

            Assert.AreEqual(1, response.Model.NumberOfCustomers);
        }
    }
}
