using Microsoft.Extensions.Caching.Memory;

namespace Crave.Integrations.POS.Deliverect.Tests.Builders
{
    public class DeliverectClientBuilder
    {
        public DeliverectClientBuilder() : this(new BuildData()) { }

        private DeliverectClientBuilder(BuildData data) { Data = data; }

        private BuildData Data { get; }

        public DeliverectClientBuilder WithClientId(string clientId) => new DeliverectClientBuilder(new BuildData(Data)
        {
            ClientId = clientId
        });

        public DeliverectClientBuilder WithClientSecret(string clientSecret) => new DeliverectClientBuilder(new BuildData(Data)
        {
            ClientSecret = clientSecret
        });

        public DeliverectClient Build()
        {
            MemoryCache memoryCache = new MemoryCache(new MemoryCacheOptions());
            DeliverectConfig config = new DeliverectConfig(Data.ClientId, Data.ClientSecret);

            return new DeliverectClient(memoryCache, config);
        }

        private class BuildData
        {
            public BuildData(BuildData data)
            {
                ClientId = data.ClientId;
                ClientSecret = data.ClientSecret;
            }

            public BuildData()
            {
                ClientId = string.Empty;
                ClientSecret = string.Empty;
            }

            public string ClientId { get; set; }
            public string ClientSecret { get; set; }
        }
    }
}
