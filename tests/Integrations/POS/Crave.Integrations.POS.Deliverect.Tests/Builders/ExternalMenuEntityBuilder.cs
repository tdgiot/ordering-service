﻿using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Integrations.POS.Deliverect.Tests.Builders
{
	internal class ExternalMenuEntityBuilder
	{
		private readonly bool isNew;
		private readonly string id;

		public ExternalMenuEntityBuilder() : this(false, "External Menu 1")
		{
				
		}

		private ExternalMenuEntityBuilder(bool isNew, string id)
		{
			this.isNew = isNew;
			this.id = id;
		}

		public ExternalMenuEntity Build() => new ExternalMenuEntity {IsNew = isNew, Id = id};

		public ExternalMenuEntityBuilder W(string entityId) => new ExternalMenuEntityBuilder(isNew, entityId);
	}
}
