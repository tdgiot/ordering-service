﻿using Crave.Integrations.POS.Interfaces.Repositories;
using Moq;

namespace Crave.Integrations.POS.Deliverect.Tests.Builders
{
	internal static class ExternalMenuRepositoryMockBuilder
	{
		public static IExternalMenuRepository Build() => new Mock<IExternalMenuRepository>().Object;
	}
}
