﻿using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Integrations.POS.Deliverect.Tests.Builders
{
	internal class ExternalProductEntityBuilder
	{
		private readonly bool isNew;
		private readonly string id;
		private readonly ExternalMenuEntity externalMenu;

		public ExternalProductEntityBuilder() : this(false, "External Product 1", null)
		{
		    
		}

		private ExternalProductEntityBuilder(bool isNew, string id, ExternalMenuEntity externalMenu)
		{
			this.isNew = isNew;
			this.id = id;
			this.externalMenu = externalMenu;
		}

		public ExternalProductEntity Build() =>
			new ExternalProductEntity
			{
				IsNew = isNew, Id = id, ExternalMenu = externalMenu
			};

		public ExternalProductEntityBuilder W(string entityId) => new ExternalProductEntityBuilder(isNew, entityId, externalMenu);

		public ExternalProductEntityBuilder W(ExternalMenuEntityBuilder builder) => W(builder.Build());

		public ExternalProductEntityBuilder W(ExternalMenuEntity externalMenuEntity) => new ExternalProductEntityBuilder(isNew, id, externalMenuEntity);
	}
}
