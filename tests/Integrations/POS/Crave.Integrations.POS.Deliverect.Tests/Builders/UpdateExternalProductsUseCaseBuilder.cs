﻿using Crave.Integrations.POS.Domain.UseCases;
using Crave.Integrations.POS.Interfaces.Repositories;
using Crave.Ordering.Pos.Tests.Builders;

namespace Crave.Integrations.POS.Deliverect.Tests.Builders
{
	internal class UpdateExternalProductsUseCaseBuilder
	{
		private readonly IExternalSystemRepository externalSystemRepository;

		public UpdateExternalProductsUseCaseBuilder() : this(
			new ExternalSystemRepositoryMockBuilder().Build()
		)
		{
            
		}

		private UpdateExternalProductsUseCaseBuilder(IExternalSystemRepository externalSystemRepository)
		{
			this.externalSystemRepository = externalSystemRepository;
		}

		public UpdateExternalProductsUseCase Build() => new UpdateExternalProductsUseCase(externalSystemRepository);

		public static UpdateExternalProductsUseCaseBuilder W(IExternalSystemRepository repository) => new UpdateExternalProductsUseCaseBuilder(repository);
	}
}
