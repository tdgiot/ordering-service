﻿using Crave.Integrations.POS.Deliverect.Domain.Responses;
using Microsoft.Extensions.Caching.Memory;
using NUnit.Framework;
using System.Threading.Tasks;

namespace Crave.Integrations.POS.Deliverect.Tests
{
    [TestFixture, Explicit]
    [Category("Integration"), Category("Deliverect")]
    public class DeliverectClientTests
    {
        private const string ClientId = "TZ9oO2vLXc4CBcCd";
        private const string ClientSecret = "8NIwMO17CDAozXaVwtF04rZ0nLJ4MUak";

        private DeliverectClient deliverectClient;

        [SetUp]
        public void Setup()
        {
            MemoryCache memoryCache = new MemoryCache(new MemoryCacheOptions());
            DeliverectConfig config = new DeliverectConfig(ClientId, ClientSecret);

            deliverectClient = new DeliverectClient(memoryCache, config);
        }

        [Test]
        public async Task GetAccountsTest()
        {
            GetAccountsResponse response = await deliverectClient.GetAccountsAsync();

            Assert.NotNull(response);
        }
    }
}
