﻿using Crave.Integrations.POS.Deliverect.Domain.Models;
using Crave.Integrations.POS.Deliverect.Domain.Requests;
using Crave.Integrations.POS.Deliverect.Tests.Utility;
using Crave.Integrations.POS.Deliverect.WebHooks;
using Crave.Integrations.POS.Interfaces.Repositories;
using Crave.Ordering.Pos.Tests.Builders;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Domain;
using Moq;
using NUnit.Framework;
using System.Linq;
using System.Threading.Tasks;
using Crave.Integrations.POS.Deliverect.Tests.Builders;

namespace Crave.Integrations.POS.Deliverect.Tests
{
    [TestFixture]
    public class PushMenuTests
    {
        private static class A
        {
            public static MenuUpdateWebHookBuilder MenuUpdateWebHook => new MenuUpdateWebHookBuilder();
            public static ExternalSystemRepositoryMockBuilder ExternalSystemRepositoryMock => new ExternalSystemRepositoryMockBuilder();
            public static UpdateExternalProductsUseCaseBuilder UpdateExternalProductsUseCase => new UpdateExternalProductsUseCaseBuilder();
            public static HmacValidationMockBuilder HmacValidationMock => new HmacValidationMockBuilder();
            public static ExternalSystemEntityBuilder ExternalSystemEntity => new ExternalSystemEntityBuilder();
            public static ExternalProductEntityBuilder ExternalProductEntity => new ExternalProductEntityBuilder();
            public static ExternalMenuEntityBuilder ExternalMenuEntity => new ExternalMenuEntityBuilder();
        }


        [Test]
        public async Task PushMenuTests_Menu()
        {
            // Arrange
            ExternalSystemEntity externalSystemEntity = A.ExternalSystemEntity.Build();

            IExternalSystemRepository externalSystemRepository = A.ExternalSystemRepositoryMock
                .W(externalSystemEntity)
                .Build();

            MenuUpdateWebHook sut = A.MenuUpdateWebHook
                .W(externalSystemRepository)
                .W(UpdateExternalProductsUseCaseBuilder
                    .W(externalSystemRepository)
                    .Build())
                .W(A.HmacValidationMock.Build())
                .Build();

            Menu[] menus = ModelFromResource.Get<Menu[]>("singleMenu.json");

            // Act
            Response response = await sut.ExecuteAsync(new HmacValidatedRequest<Menu[]>(menus, "", ""));

            // Assert
            Assert.IsTrue(response.Success, response.Message);
            Mock.Get(externalSystemRepository).Verify(x => x.SaveAsync(It.IsAny<ExternalSystemEntity>()), Times.Exactly(1));
            Assert.AreEqual(1, externalSystemEntity.ExternalProductCollection.Select(x => x.ExternalMenu).Distinct().Count(x => x.IsNew));
        }


        [Test]
        public async Task PushMenuTests_Menu_Disable()
        {
	        const string productId = "BURAAA";

	        ExternalSystemEntity externalSystemEntity = A.ExternalSystemEntity
                .W(A.ExternalProductEntity
	                .W(productId)
	                .W(A.ExternalMenuEntity)
	                .Build())
                .Build();

            IExternalSystemRepository externalSystemRepository = A.ExternalSystemRepositoryMock
                .W(externalSystemEntity)
                .Build();

            Menu[] menus = ModelFromResource.Get<Menu[]>("singleMenu.json");

            MenuUpdateWebHook sut = A.MenuUpdateWebHook
                .W(externalSystemRepository)
                .W(UpdateExternalProductsUseCaseBuilder
                    .W(externalSystemRepository)
                    .Build())
                .W(A.HmacValidationMock.Build())
                .Build();

            Mock<IExternalSystemRepository> externalSystemRepositoryMock = Mock.Get(externalSystemRepository);

            externalSystemRepositoryMock.Verify(x => x.SaveAsync(It.IsAny<ExternalSystemEntity>()), Times.Never);

            // Act
            Response response = await sut.ExecuteAsync(new HmacValidatedRequest<Menu[]>(menus, "", ""));

            // Assert
            externalSystemRepositoryMock.Verify(x => x.SaveAsync(It.IsAny<ExternalSystemEntity>()), Times.Once);

            Assert.IsTrue(response.Success, response.Message);


            ExternalProductEntity burger1 = externalSystemEntity.ExternalProductCollection.Single(x => x.Id == productId);

            Assert.IsFalse(burger1.IsEnabled);
            Assert.AreEqual(1, externalSystemEntity.ExternalProductCollection.Select(x => x.ExternalMenu).Distinct().Count(x => x.IsNew));

        }


        [Test]
        public async Task PushMenuTests_Double_menu()
        {
            // Arrange
            ExternalSystemEntity externalSystemEntity = A.ExternalSystemEntity.Build();

            IExternalSystemRepository externalSystemRepository = A.ExternalSystemRepositoryMock
                .W(externalSystemEntity)
                .Build();

            Menu[] menus = ModelFromResource.Get<Menu[]>("doubleMenu.json");

            MenuUpdateWebHook sut = A.MenuUpdateWebHook
                .W(externalSystemRepository)
                .W(UpdateExternalProductsUseCaseBuilder
                    .W(externalSystemRepository)
                    .Build())
                .W(A.HmacValidationMock.Build())
                .Build();

            // Act
            Response response = await sut.ExecuteAsync(new HmacValidatedRequest<Menu[]>(menus, "", ""));

            // Assert
            Mock.Get(externalSystemRepository).Verify(x => x.SaveAsync(It.IsAny<ExternalSystemEntity>()), Times.Exactly(2));

            Assert.NotNull(externalSystemEntity.ExternalProductCollection.Single(x => x.Id == "BUR1" && x.ExternalMenu.Id == "608a5dfabee8701ff5f9f8d2" && x.IsEnabled));
            Assert.NotNull(externalSystemEntity.ExternalProductCollection.Single(x => x.Id == "BUR1" && x.ExternalMenu.Id == "60520c1eb0d7837ac99bbb49" && x.IsEnabled));
            Assert.AreEqual(2, externalSystemEntity.ExternalProductCollection.Count(x => x.Id == "BUR1"));

            Assert.AreEqual(2, externalSystemEntity.ExternalProductCollection.Select(x => x.ExternalMenu).Distinct().Count(x => x.IsNew));

            Assert.IsTrue(response.Success);

        }

        [Test]
        public async Task PushMenuTests_Double_menu_exists()
        {
            ExternalSystemEntity externalSystemEntity = A.ExternalSystemEntity.Build();

            IExternalSystemRepository externalSystemRepository = A.ExternalSystemRepositoryMock
                .W(externalSystemEntity)
                .Build();

            Menu[] menus = ModelFromResource.Get<Menu[]>("doubleMenu.json");

            MenuUpdateWebHook sut = A.MenuUpdateWebHook
                .W(externalSystemRepository)
                .W(UpdateExternalProductsUseCaseBuilder
                    .W(externalSystemRepository)
                    .Build())
                .W(A.HmacValidationMock.Build())
                .Build();

            Response response = await sut.ExecuteAsync(new HmacValidatedRequest<Menu[]>(menus, "", ""));
            Mock.Get(externalSystemRepository).Verify(x => x.SaveAsync(It.IsAny<ExternalSystemEntity>()), Times.Exactly(2));

            Assert.NotNull(externalSystemEntity.ExternalProductCollection.Single(x => x.Id == "BUR1" && x.ExternalMenu.Id == "608a5dfabee8701ff5f9f8d2"));
            Assert.NotNull(externalSystemEntity.ExternalProductCollection.Single(x => x.Id == "BUR1" && x.ExternalMenu.Id == "60520c1eb0d7837ac99bbb49"));
            Assert.AreEqual(2, externalSystemEntity.ExternalProductCollection.Count(x => x.Id == "BUR1"));

            Assert.IsTrue(response.Success);

        }
    }
}
