﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Crave.Enums;
using Crave.Integrations.POS.Deliverect.Domain.Converters;
using Crave.Integrations.POS.Deliverect.Domain.Requests;
using Crave.Integrations.POS.Deliverect.Domain.UseCases;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Domain;
using Microsoft.Extensions.Caching.Memory;
using NUnit.Framework;

namespace Crave.Integrations.POS.Deliverect.Tests
{
    [TestFixture]
    public class DeliverectExternalProductConverterTests
    {
        /// <summary>
        /// allowDuplicates (crave sql database) depends on the number of Multimax (deliverect) (DEVLOG-1030)
        /// https://developers.deliverect.com/reference/post-menu-push#multi-select-modifiers
        /// </summary>
        [TestCase(0,false)]
        [TestCase(1, false)]
        [TestCase(2, true)]
        [TestCase(3, true)]
        public void ConvertModifierGroupsToExternalProducts_multimax(int multiMax, bool allowDuplicates)
        {
            Dictionary<string, Domain.Models.ModifierGroup> products = new()
            {
                {
                    "",
                    new Domain.Models.ModifierGroup()
                    {
                        MultiMax = multiMax
                    }
                },
            };

            IEnumerable<Models.ExternalProduct> result = DeliverectExternalProductConverter.ConvertModifierGroupsToExternalProducts(products);
            Assert.AreEqual(allowDuplicates, result.Single().AllowDuplicates);
        }
    }
}
