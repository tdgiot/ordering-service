using Crave.IntegrationTests.Service.Api.Helpers;
using Crave.IntegrationTests.Service.Api.Setup;
using Crave.Ordering.Api.Client;
using Crave.Ordering.Api.WebApplication;
using Microsoft.AspNetCore.Mvc.Testing;
using NUnit.Framework;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Mail;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Crave.IntegrationTests.Service.Api
{
    public class MailTests
    {
        [TestCase(Data.PostData.Order, 3, TestName = nameof(Data.PostData.Order))]
        [TestCase(Data.PostData.OrderWithPayedAlteration, 1, TestName = nameof(Data.PostData.OrderWithPayedAlteration))]
        [TestCase(Data.PostData.OrderWithPayedOrderLevelAlterationAdyen, 2, TestName = nameof(Data.PostData.OrderWithPayedOrderLevelAlterationAdyen))]
        [TestCase(Data.PostData.OrderWithFreeOrderLevelAlterations,1 , TestName = nameof(Data.PostData.OrderWithFreeOrderLevelAlterations))]
        [TestCase(Data.PostData.OrderWithEmailRoute, 3, TestName = nameof(Data.PostData.OrderWithEmailRoute))]
        [TestCase(Data.PostData.OrderWithCoversAndAdyen, 2, TestName = nameof(Data.PostData.OrderWithCoversAndAdyen))]
        public async Task Test_Html_Mails(string payload, int awaitedMails)
        {
            (WebApplicationFactory<Startup> factory, SemaphoreSlim signal, List<MailMessage> mailbox) = WebserverFactory.GetFactory.AddTestMailserver(awaitedMails);

            using HttpClient httpClient = factory.CreateClient();
            StringContent postData = new(payload, Encoding.UTF8, "application/json");

            var response = await httpClient.PostAsync("/api/v1/orders", postData);
            

            string orderGuid = await response.Content.ReadAsStringAsync();
            response.EnsureSuccessStatusCode();
            OrderDto retreiveOrder = await httpClient.GetAsyncJson<OrderDto>($"/api/v1/orders/{orderGuid}");

            if (retreiveOrder.Status == OrderStatus.WaitingForPayment)
            {
                var responsePayment = await httpClient.GetAsync($"/api/v1/PaymentStatus/Update?OrderId={retreiveOrder.OrderId}&PaymentTransactionStatus=paid");
                responsePayment.EnsureSuccessStatusCode();
            }

            //  Since mails are sent asynchronously, this semaphore waits until the specified number of "awaitedMails" of mails is fulfilled.
            await signal.WaitAsync();

            mailbox.SendMailsToTestResult();

            // See the e-mails as jpg in Visual Studio Test Explorer
        }
    }
}