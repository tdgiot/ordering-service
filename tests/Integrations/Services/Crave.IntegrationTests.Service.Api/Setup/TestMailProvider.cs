﻿using NUnit.Framework;
using Crave.Libraries.Mail;
using System.Threading.Tasks;
using System.Net.Mail;
using System.Threading;
using System.Collections.Generic;

namespace Crave.IntegrationTests.Service.Api.Setup
{
    public class TestMailProvider : IMailProvider
    {
        private int awaitNumberofMails;
        private SemaphoreSlim signal;
        private List<MailMessage> mailbox;

        public TestMailProvider(int awaitedNumberofMails, SemaphoreSlim signal, List<MailMessage> mailbox)
        {
            awaitNumberofMails = awaitedNumberofMails;
            this.signal = signal;
            this.mailbox = mailbox;
        }

        public Task<MailResponse> SendAsync(MailMessage message)
        {
            mailbox.Add(message);

            if (mailbox.Count >= awaitNumberofMails)
            {
                signal.Release();
            }

            return Task.FromResult(new MailResponse(true, null));
        }
    }

}

