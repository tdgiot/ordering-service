﻿using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using Crave.Ordering.Api.WebApplication;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System.Linq;

namespace Crave.IntegrationTests.Service.Api.Setup
{
    public static class WebserverFactory
    {
        public static WebApplicationFactory<Startup> GetFactory = new CustomWebApplicationFactory()
            .WithWebHostBuilder(a =>
                a.UseContentRoot(Directory.GetCurrentDirectory())
                    .ConfigureLogging(logging => logging.SetMinimumLevel(LogLevel.Warning)));
    }

    public class CustomWebApplicationFactory : WebApplicationFactory<Startup>
    {
        private const string MockConnectionString = "Data Source=db.office.crave-emenu.com,6736;Initial Catalog=ObymobiDevelopment;Integrated Security=SSPI;";

        protected override void ConfigureWebHost(IWebHostBuilder builder)
        {
            IConfiguration configuration = new ConfigurationBuilder()
                .AddInMemoryCollection(new Dictionary<string, string>
                {
                    { "Application:Environment" , "local" },
                    //{ "ConnectionString", MockConnectionString },
                    //{ "connectionString", MockConnectionString },

                    { "Database", MockConnectionString },
                })
                .Build();
            builder.UseConfiguration(configuration);
        }

    }
}

