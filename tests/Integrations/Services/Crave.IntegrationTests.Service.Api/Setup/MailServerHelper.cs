﻿using System;
using System.Collections.Generic;
using System.IO;
using Crave.Ordering.Api.WebApplication;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.Extensions.DependencyInjection;
using Crave.Libraries.Mail;
using CoreHtmlToImage;
using System.Threading;
using System.Net.Mail;
using NUnit.Framework;
using System.Text.RegularExpressions;

namespace Crave.IntegrationTests.Service.Api.Setup
{
    public static class MailServerHelper
    {
        public static (WebApplicationFactory<Startup> factory, SemaphoreSlim signal, List<MailMessage> mailbox) AddTestMailserver(this WebApplicationFactory<Startup> factory, int awaitedNumberofMails)
        {
            var signal = new SemaphoreSlim(0, 1);
            var mailbox = new List<MailMessage>();
            var result = factory.WithWebHostBuilder(a => a.ConfigureServices(b => b.AddSingleton<IMailProvider>(new TestMailProvider(awaitedNumberofMails, signal, mailbox))));
            return (result, signal, mailbox);
        }

        public static void SendMailsToTestResult(this List<MailMessage> mails)
        {
            mails.ForEach(SendMail);
        }

        private static void SendMail(MailMessage obj)
        {
            var converter = new HtmlConverter();
            var bytes = converter.FromHtmlString(obj.Body);

            TestContext.AddTestAttachment(SafeToTempDisk(bytes, obj.Subject, "jpg"), obj.Subject);
        }

        private static string SafeToTempDisk(byte[] bytes, string name, string filetype)
        {
            string file = Path.Combine(Path.GetTempPath(), Guid.NewGuid().ToString(), GetSafeFileName(name) + "." + filetype);
            Directory.CreateDirectory(Path.GetDirectoryName(file));
            File.WriteAllBytes(file, bytes);
            return file;
        }

        private static string GetSafeFileName(string fileName)
            => Regex.Replace(fileName.Replace(' ', '-'), "[^A-Za-z0-9 -]", "");

    }
}

