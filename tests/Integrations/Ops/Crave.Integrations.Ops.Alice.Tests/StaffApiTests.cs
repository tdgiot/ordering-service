using System;
using Crave.Integrations.Ops.Alice.Requests;
using Crave.Integrations.Ops.Alice.Responses;
using NUnit.Framework;
using RestSharp;

namespace Crave.Integrations.Ops.Alice.Tests
{
    [TestFixture, Explicit]
    [Category("Integration"), Category("Alice")]
    public class StaffApiTests
    {
        private StaffApi staffApi;

        [SetUp]
        public void Setup()
        {
            Uri baseUrl = new Uri("https://rapi.aliceapp.com");

            string apiKey = "67d98fca-0ccb-458b-bf20-497e4886e51f";
            string hotelId = "437";
            string username = "crave_sand_api";
            string password = "12NZXIatKvKt";

            this.staffApi = new StaffApi(baseUrl, apiKey, username, password, hotelId);
        }

        [Test]
        public void CreateServiceRequest()
        {
            IRestResponse<ServiceRequestResponse> response = staffApi.CreateServiceRequest(new ServiceRequestRequest
            {
                ServiceId = "21561",
                RoomNumber = "444"
            });

            Assert.NotNull(response?.Data?.Id);
        }
    }
}