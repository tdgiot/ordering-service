using System;
using System.Threading.Tasks;
using Crave.Integrations.Ops.Models;
using Crave.Integrations.Ops.Results;
using NUnit.Framework;

namespace Crave.Integrations.Ops.Alice.Tests
{
    [TestFixture, Explicit]
    [Category("Integration"), Category("Alice")]
    public class AliceClientTests
    {
        private const string API_URL = "https://rapi.aliceapp.com";
        private const string API_KEY = "67d98fca-0ccb-458b-bf20-497e4886e51f";
        private const string HOTEL_ID = "437";
        private const string USERNAME = "crave_sand_api";
        private const string PASSWORD = "12NZXIatKvKt";

        private AliceClient client;

        [SetUp]
        public void Setup()
        {
            client = new AliceClient(AliceClientTests.CreateConfiguration());
        }

        [Test]
        public async Task CreateServiceRequestAsync()
        {
            ExternalOrder externalOrder = new ExternalOrder
            {
                ExternalDeliverypoint = new ExternalDeliverypoint("444", "444"),
                ExternalProduct = new ExternalProduct("21561")
            };

            OpsResult<ExternalOrder> order = await client.SubmitServiceOrder(externalOrder);

            Assert.True(order.Success);
            Assert.NotNull(order.Model?.ExternalId);
        }

        private static AliceConfiguration CreateConfiguration() => new AliceConfiguration(new Uri(API_URL),
                                                                                          API_KEY,
                                                                                          USERNAME,
                                                                                          PASSWORD, 
                                                                                          HOTEL_ID);
    }
}