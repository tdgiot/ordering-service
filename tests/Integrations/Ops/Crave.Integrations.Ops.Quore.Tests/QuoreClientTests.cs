using System;
using System.Threading.Tasks;
using Crave.Integrations.Ops.Models;
using Crave.Integrations.Ops.Quore;
using Crave.Integrations.Ops.Results;
using NUnit.Framework;

namespace Crave.Integrations.Service.Quore.Tests
{
    [TestFixture, Explicit]
    [Category("Integration"), Category("QuoreClient")]
    public class QuoreClientTests
    {
        private const string API_URL = "https://api.quore.com/api.php";
        private const string API_TOKEN = "989876a8-ad0d-11e7-8ca0-000c29b1e469";

        private QuoreClient client;

        [OneTimeSetUp]
        public void Setup()
        {
            client = new QuoreClient(CreateConfiguration());
        }

        [Test]
        public async Task Order_SuccessAsync()
        {
            string externalDeliverypointId = "1";
            string externalDeliverypointName = "Water Boiler Room";
            string externalProductId = "1";

            ExternalOrder externalOrder = new ExternalOrder
            {
                ExternalDeliverypoint = new ExternalDeliverypoint(externalDeliverypointId, externalDeliverypointName),
                ExternalProduct = new ExternalProduct(externalProductId)
            };

            OpsResult<ExternalOrder> order = await client.SubmitServiceOrder(externalOrder);

            Assert.True(order.Success);
            Assert.NotNull(order.Model);
        }

        [Test]
        public async Task SubmitOrder_IncorrectDeliverypointAsync()
        {
            string externalDeliverypointId = "1";
            string externalDeliverypointName = "sdfdsf";
            string externalProductId = "1";
            string expectedErrorMessage = $"Failed to submit order to Quore. ExternalDeliverypoint={externalDeliverypointId}, " + 
                                          $"ExternalProduct={externalProductId}, " + 
                                          "StatusCode=OK, " + 
                                          "Content=[{\"error\":\"Invalid Area!\",\"type\":\"add-service-request-by-area-name-invalid-area\"}]";

            ExternalOrder externalOrder = new ExternalOrder
            {
                ExternalDeliverypoint = new ExternalDeliverypoint(externalDeliverypointId, externalDeliverypointName),
                ExternalProduct = new ExternalProduct(externalProductId)
            };

            OpsResult<ExternalOrder> order = await client.SubmitServiceOrder(externalOrder);

            Assert.False(order.Success);
            Assert.AreEqual(expectedErrorMessage, order.ErrorMessage);
        }

        [Test]
        public async Task SubmitOrder_IncorrectProduct()
        {
            string externalDeliverypointId = "1";
            string externalDeliverypointName = "Water Boiler Room";
            string externalProductId = "asdfdsf";
            string expectedErrorMessage = $"Failed to submit order to Quore. ExternalDeliverypoint={externalDeliverypointId}, " +
                                          $"ExternalProduct={externalProductId}, " +
                                          "StatusCode=OK, " +
                                          "Content=[{\"error\":\"There was an error creating this Guest Request because it looked like a duplicate. If this was intentional, please wait 30 seconds and try again.\",\"type\":\"add-service-request-by-area-name-creating-housekeeping-request\"}]";

            ExternalOrder externalOrder = new ExternalOrder
            {
                ExternalDeliverypoint = new ExternalDeliverypoint(externalDeliverypointId, externalDeliverypointName),
                ExternalProduct = new ExternalProduct(externalProductId)
            };

            OpsResult<ExternalOrder> order = await client.SubmitServiceOrder(externalOrder);

            Assert.False(order.Success);
            Assert.AreEqual(expectedErrorMessage, order.ErrorMessage);
        }

        private static QuoreConfiguration CreateConfiguration() => new QuoreConfiguration(new Uri(API_URL),
                                                                                          API_TOKEN);
    }
}