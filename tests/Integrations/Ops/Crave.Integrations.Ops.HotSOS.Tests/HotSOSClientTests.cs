using System;
using System.Threading.Tasks;
using Crave.Integrations.Ops.Models;
using Crave.Integrations.Ops.Results;
using NUnit.Framework;

namespace Crave.Integrations.Ops.HotSOS.Tests
{
    [TestFixture, Explicit]
    [Category("Integration"), Category("HotSOSClient")]
    public class HotSOSClientTests
    {
        private const string USERNAME = "craveapi_3237";
        private const string PASSWORD = "T3sting!";
        private const string API_URL = "https://ifc.int.hot-sos.net/api/service.svc/soap";

        private HotSOSClient client;

        [OneTimeSetUp]
        public void Setup()
        {
            client = new HotSOSClient(CreateConfiguration());
        }

        [Test]
        public async Task SubmitOrder_Success()
        {
            ExternalOrder externalOrder = new ExternalOrder
            {
                ExternalDeliverypoint = new ExternalDeliverypoint("1", "1"),
                ExternalProduct = new ExternalProduct("1")
            };

            OpsResult<ExternalOrder> order = await client.SubmitServiceOrder(externalOrder);

            Assert.True(order.Success);
            Assert.NotNull(order.Model);
        }

        private static HotSOSConfiguration CreateConfiguration() => new HotSOSConfiguration(USERNAME, 
                                                                                            PASSWORD, 
                                                                                            new Uri(API_URL));
    }
}