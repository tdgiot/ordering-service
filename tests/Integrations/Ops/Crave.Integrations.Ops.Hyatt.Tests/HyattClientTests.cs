using System;
using System.Threading.Tasks;
using Crave.Integrations.Ops.Hyatt;
using Crave.Integrations.Ops.Models;
using Crave.Integrations.Ops.Results;
using NUnit.Framework;

namespace Crave.Integrations.Service.Hyatt.Tests
{
    [TestFixture, Explicit]
    [Category("Integration"), Category("HyattClient")]
    public class HyattClientTests
    {
        private const string APPLICATION_ID = "d815607b46e49e7082907ee889ebf0c3";
        private const string API_URL = "https://api-uat.hyatt.com";
        private const string PUBLIC_KEY = "d815607b46e49e7082907ee889ebf0c3";
        private const string PRIVATE_KEY = "17d776d51e42d4b0a5b02c9121d2b80e";
        private const string HOTEL_ID = "ORLAN";

        private HyattClient client;

        [OneTimeSetUp]
        public void Setup()
        {
            client = new HyattClient(CreateConfiguration());
        }

        [Test]
        public async Task Test1Async()
        {
            ExternalOrder externalOrder = new ExternalOrder
            {
                ExternalDeliverypoint = new ExternalDeliverypoint("1", "1"),
                ExternalProduct = new ExternalProduct("9")
            };

            OpsResult<ExternalOrder> order = await client.SubmitServiceOrder(externalOrder);

            Assert.True(order.Success);
            Assert.NotNull(order.Model);
        }

        private static HyattConfiguration CreateConfiguration() => new HyattConfiguration(APPLICATION_ID,
                                                                                          new Uri(API_URL),
                                                                                          PUBLIC_KEY, 
                                                                                          PRIVATE_KEY,
                                                                                          HOTEL_ID);
    }
}