﻿using System.Net.Http;
using System.Threading.Tasks;
using Crave.Libraries.Distance.Enums;
using Crave.Libraries.Distance.Google;
using Crave.Libraries.Distance.Models;
using Crave.Libraries.Distance.Responses;
using Crave.Ordering.Tests;
using NSubstitute;
using NUnit.Framework;

namespace Crave.Ordering.Libraries.Tests.Crave.Integrations.Google.Distance
{
    [TestFixture, Explicit]
    [Category("Integration"), Category("GoogleDistanceMatrix")]
    public class GoogleDistanceClientTests : TestBase
    {
        private const string TEST_API_KEY = "AIzaSyAad2q10eF5nl2CCIBP8kbjevRX9qyvZDs";
        
        private readonly Coordinates coordinatesNlOffice = new Coordinates(4.209951, 52.004972);
        private readonly Coordinates coordinatesFlorisHome = new Coordinates(4.167862, 51.998406);

        private readonly Address addressNlOffice = new Address("Warmoezenierstraat 5", "Naaldwijk", "2671ZP", "Netherlands");
        private readonly Address addressFlorisHome = new Address("Vogelkersstraat 41", "'s-Gravenzande", "2691EJ", "Netherlands");

        private readonly Coordinates coordinatesInvalid = new Coordinates(9999, 9999);
        private readonly Address addressInvalid = new Address("Magicmagicallystreet 999", "MagicTown", "9999UL","MagicCountry");

        private GoogleDistanceClient googleDistanceClient;

        [OneTimeSetUp]
        public void _OneTimeSetUp()
        {
            IHttpClientFactory httpClientFactory = this.CreateSubstitute<IHttpClientFactory>();
            httpClientFactory.CreateClient().Returns(new HttpClient());
            
            GoogleDistanceClientConfiguration googleDistanceClientConfiguration = new GoogleDistanceClientConfiguration(GoogleDistanceClientTests.TEST_API_KEY);
            this.googleDistanceClient = new GoogleDistanceClient(googleDistanceClientConfiguration, httpClientFactory);
        }

        [Test]
        public async Task TestExecuteAsync_GetDistance_InvalidCoordinates()
        {
            Coordinates originLocation = this.coordinatesNlOffice;
            Coordinates destinationLocation = this.coordinatesInvalid;

            GetDistanceResponse response = await this.googleDistanceClient.GetDistanceAsync(originLocation, destinationLocation);

            Assert.AreEqual(response.Result, DistanceResult.ZERO_RESULTS);
            Assert.Null(response.Distance);
            Assert.Null(response.ErrorMessage);
        }

        [Test]
        public async Task TestExecuteAsync_GetDistance_InvalidAddress()
        {
            Coordinates originLocation = this.coordinatesNlOffice;
            Address destinationLocation = this.addressInvalid;

            GetDistanceResponse response = await this.googleDistanceClient.GetDistanceAsync(originLocation, destinationLocation);

            Assert.AreEqual(response.Result, DistanceResult.NOT_FOUND);
            Assert.Null(response.Distance);
            Assert.Null(response.ErrorMessage);
        }

        [Test]
        public async Task TestExecuteAsync_GetDistance_CoordinatesCoordinates()
        {
            Coordinates originLocation = this.coordinatesNlOffice;
            Coordinates destinationLocation = this.coordinatesFlorisHome;

            GetDistanceResponse response = await this.googleDistanceClient.GetDistanceAsync(originLocation, destinationLocation);

            Assert.AreEqual(response.Result, DistanceResult.OK);
            Assert.AreEqual(4779, response.Distance.Metres);
            Assert.Null(response.ErrorMessage);
        }

        [Test]
        public async Task TestExecuteAsync_GetDistance_CoordinatesAddress()
        {
            Coordinates originLocation = this.coordinatesNlOffice;
            Address destinationLocation = this.addressFlorisHome;

            GetDistanceResponse response = await this.googleDistanceClient.GetDistanceAsync(originLocation, destinationLocation);

            Assert.AreEqual(response.Result, DistanceResult.OK);
            Assert.AreEqual(4794, response.Distance.Metres);
            Assert.Null(response.ErrorMessage);
        }

        [Test]
        public async Task TestExecuteAsync_GetDistance_AddressAddress()
        {
            Address originLocation = this.addressNlOffice;
            Address destinationLocation = this.addressFlorisHome;

            GetDistanceResponse response = await this.googleDistanceClient.GetDistanceAsync(originLocation, destinationLocation);

            Assert.AreEqual(response.Result, DistanceResult.OK);
            Assert.AreEqual(4790, response.Distance.Metres);
            Assert.Null(response.ErrorMessage);
        }

        [Test]
        public async Task TestExecuteAsync_GetDistance_AddressCoordinates()
        {
            Address originLocation = this.addressNlOffice;
            Coordinates destinationLocation = this.coordinatesFlorisHome;

            GetDistanceResponse response = await this.googleDistanceClient.GetDistanceAsync(originLocation, destinationLocation);

            Assert.AreEqual(response.Result, DistanceResult.OK);
            Assert.AreEqual(4776, response.Distance.Metres);
            Assert.Null(response.ErrorMessage);
        }
    }
}
