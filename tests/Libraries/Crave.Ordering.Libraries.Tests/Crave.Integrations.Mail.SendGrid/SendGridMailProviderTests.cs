﻿using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Threading.Tasks;
using Crave.Libraries.Mail;
using Crave.Libraries.Mail.SendGrid;
using Crave.Ordering.Libraries.Tests.Builders;
using Crave.Ordering.Tests;
using NUnit.Framework;

namespace Crave.Ordering.Libraries.Tests.Crave.Integrations.Mail.SendGrid
{
    [TestFixture]
    public class SendGridMailProviderTests : TestBase
    {
        static class A
        {
            public static SendGridMailProviderBuilder SendGridMailProvider => new SendGridMailProviderBuilder();
            public static SendGridClientMockBuilder SendGridClientMock => new SendGridClientMockBuilder();
            public static SendGridClientResponseBuilder SendGridClientResponse => new SendGridClientResponseBuilder();
            public static MailMessageBuilder MailMessage => new MailMessageBuilder();
        }

        [Test]
        public async Task Execute_WithNoSender_ReturnsErrorMessage()
        {
            const string toEmailAddress = "floris@crave-emenu.com";
            const string subject = "Testing email subject";
            const string responseBody = "Missing sender";
            const HttpStatusCode httpStatusCode = HttpStatusCode.BadRequest;

            string expectedErrorMessage = $"Failed to send mail through {nameof(SendGridMailProvider)}. (To={toEmailAddress}, Subject={subject}, ResponseStatusCode={httpStatusCode}, ResponseBody={responseBody})";

            MailMessage message = A.MailMessage
                                   .W(subject)
                                   .W((MailMessageBuilder.SenderMailAddress)null)
                                   .W((MailMessageBuilder.ToMailAddress)new MailAddress(toEmailAddress))
                                   .Build();

            SendGridMailProvider sut = A.SendGridMailProvider
                                        .W(A.SendGridClientMock
                                            .W(A.SendGridClientResponse
                                                .W(new StringContent(responseBody))
                                                .W(httpStatusCode)))
                                        .Build();

            MailResponse response = await sut.SendAsync(message);

            Assert.IsFalse(response.Success);
            Assert.AreEqual(expectedErrorMessage, response.ErrorMessage);
        }

        [Test]
        public async Task Execute_WithNoRecipients_ReturnsErrorMessage()
        {
            const string senderEmailAddress = "noreply@crave-emenu.com";
            const string subject = "Testing email subject";
            const string responseBody = "Missing sender";
            const HttpStatusCode httpStatusCode = HttpStatusCode.BadRequest;

            string expectedErrorMessage = $"Failed to send mail through {nameof(SendGridMailProvider)}. (To=No recipients, Subject={subject}, ResponseStatusCode={httpStatusCode}, ResponseBody={responseBody})";

            MailMessage message = A.MailMessage
                                   .W(subject)
                                   .W((MailMessageBuilder.SenderMailAddress)new MailAddress(senderEmailAddress))
                                   .W((MailMessageBuilder.ToMailAddress)null)
                                   .Build();

            SendGridMailProvider sut = A.SendGridMailProvider
                                        .W(A.SendGridClientMock
                                            .W(A.SendGridClientResponse
                                                .W(new StringContent(responseBody))
                                                .W(httpStatusCode)))
                                        .Build();

            MailResponse response = await sut.SendAsync(message);

            Assert.IsFalse(response.Success);
            Assert.AreEqual(expectedErrorMessage, response.ErrorMessage);
        }

        [Test]
        public async Task Execute_WithValidMessage_ReturnsSuccess()
        {
            const string senderEmailAddress = "noreply@crave-emenu.com";
            const string toEmailAddress = "floris@crave-emenu.com";
            const string subject = "Testing email subject";
            const string responseBody = "";
            const HttpStatusCode httpStatusCode = HttpStatusCode.Accepted;

            string expectedErrorMessage = string.Empty;

            MailMessage message = A.MailMessage
                                   .W(subject)
                                   .W((MailMessageBuilder.SenderMailAddress)new MailAddress(senderEmailAddress))
                                   .W((MailMessageBuilder.ToMailAddress)new MailAddress(toEmailAddress))
                                   .Build();

            SendGridMailProvider sut = A.SendGridMailProvider
                                        .W(A.SendGridClientMock
                                            .W(A.SendGridClientResponse
                                                .W(new StringContent(responseBody))
                                                .W(httpStatusCode)))
                                        .Build();

            MailResponse response = await sut.SendAsync(message);

            Assert.IsTrue(response.Success);
            Assert.AreEqual(expectedErrorMessage, response.ErrorMessage);
        }
    }
}
