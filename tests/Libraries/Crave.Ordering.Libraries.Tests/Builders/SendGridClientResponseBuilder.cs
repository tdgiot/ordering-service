﻿using System.Net;
using System.Net.Http;
using AutoFixture;
using SendGrid;

namespace Crave.Ordering.Libraries.Tests.Builders
{
	internal class SendGridClientResponseBuilder
	{
		private ResponseData Data { get; }

		public SendGridClientResponseBuilder() : this(SendGridClientResponseBuilder.Initialise())
		{
		}

		private SendGridClientResponseBuilder(ResponseData data) => Data = data;

        private static ResponseData Initialise()
        {
            Fixture fixture = new Fixture();
            fixture.Register(() => HttpStatusCode.Accepted);
            fixture.Register(() => new StringContent("Testing 123"));
            return fixture.Create<ResponseData>();
        }

        public Response Build() => new Response(Data.HttpStatusCode, Data.HttpContent, null);

		public SendGridClientResponseBuilder W(HttpStatusCode httpStatusCode) => new SendGridClientResponseBuilder(new ResponseData
        {
			HttpStatusCode = httpStatusCode,
			HttpContent = Data.HttpContent
        });

        public SendGridClientResponseBuilder W(StringContent httpContent) => new SendGridClientResponseBuilder(new ResponseData
        {
            HttpStatusCode = Data.HttpStatusCode,
            HttpContent = httpContent,
        });

        public class ResponseData
        {
			public HttpStatusCode HttpStatusCode { get; set; }
			public StringContent HttpContent { get; set; }
        }
	}
}