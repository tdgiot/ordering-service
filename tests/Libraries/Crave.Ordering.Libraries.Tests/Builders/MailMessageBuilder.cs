﻿using System.Net.Mail;
using AutoFixture;

namespace Crave.Ordering.Libraries.Tests.Builders
{
    internal class MailMessageBuilder
    {
        private MailMessageData Data { get; }

        public MailMessageBuilder() : this(new Fixture().Create<MailMessageData>())
        {
        }

        private MailMessageBuilder(MailMessageData data) => Data = data;


        public MailMessageBuilder W(string subject) => new MailMessageBuilder(new MailMessageData
        {
            Subject = subject,
            Sender = Data.Sender,
            IsBodyHtml = Data.IsBodyHtml,
            Body = Data.Body,
            To = Data.To,
            Bcc = Data.Bcc
        });

        public MailMessageBuilder W(SenderMailAddress sender) => new MailMessageBuilder(new MailMessageData
        {
            Subject = Data.Subject,
            Sender = sender,
            IsBodyHtml = Data.IsBodyHtml,
            Body = Data.Body,
            To = Data.To,
            Bcc = Data.Bcc
        });

        public MailMessageBuilder W(ToMailAddress to) => new MailMessageBuilder(new MailMessageData
        {
            Subject = Data.Subject,
            Sender = Data.Sender,
            IsBodyHtml = Data.IsBodyHtml,
            Body = Data.Body,
            To = to,
            Bcc = Data.Bcc
        });

        public MailMessage Build()
        {
            MailMessage message = new MailMessage
            {
                Subject = Data.Subject,
                Sender = Data.Sender,
                IsBodyHtml = Data.IsBodyHtml,
                Body = Data.Body
            };

            if (Data.To != null)
            {
                message.To.Add(Data.To);
            }

            if (Data.Bcc != null)
            {
                message.Bcc.Add(Data.Bcc);
            }

            return message;
        }

        public class MailMessageData
        {
            public string Subject { get; set; }
            public MailAddress Sender { get; set; }
            public bool IsBodyHtml { get; set; }
            public string Body { get; set; }
            public MailAddress To { get; set; }
            public MailAddress Bcc { get; set; }
        }

        public readonly struct SenderMailAddress
        {
            private MailAddress Value { get; }

            private SenderMailAddress(MailAddress mailAddress) => Value = mailAddress;

            public static implicit operator MailAddress(SenderMailAddress mailAddress) => mailAddress.Value;

            public static implicit operator SenderMailAddress(MailAddress value) => new SenderMailAddress(value);
        }

        public readonly struct ToMailAddress
        {
            private MailAddress Value { get; }

            private ToMailAddress(MailAddress mailAddress) => Value = mailAddress;

            public static implicit operator MailAddress(ToMailAddress mailAddress) => mailAddress.Value;

            public static implicit operator ToMailAddress(MailAddress value) => new ToMailAddress(value);
        }
    }
}
