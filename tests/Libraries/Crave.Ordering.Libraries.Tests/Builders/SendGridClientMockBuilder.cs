﻿using System.Threading;
using Moq;
using SendGrid;
using SendGrid.Helpers.Mail;

namespace Crave.Ordering.Libraries.Tests.Builders
{
	internal class SendGridClientMockBuilder
	{
		private Response Response { get; }

		public SendGridClientMockBuilder() : this(new SendGridClientResponseBuilder().Build())
		{
			
		}

		private SendGridClientMockBuilder(Response response) => Response = response;

		public ISendGridClient Build()
		{
			Mock<ISendGridClient> sendGridClientMock = new Mock<ISendGridClient>();
            sendGridClientMock.Setup(client => client.SendEmailAsync(It.IsAny<SendGridMessage>(), It.IsAny<CancellationToken>())).ReturnsAsync(Response);
			return sendGridClientMock.Object;
		}

		public SendGridClientMockBuilder W(SendGridClientResponseBuilder sendGridClientResponseBuilder) => new SendGridClientMockBuilder(sendGridClientResponseBuilder.Build());
	}
}