﻿using Crave.Libraries.Mail.SendGrid;
using SendGrid;

namespace Crave.Ordering.Libraries.Tests.Builders
{
    internal class SendGridMailProviderBuilder
    {
        private ISendGridClient SendGridClient { get; }

        public SendGridMailProviderBuilder() : this(new SendGridClientMockBuilder().Build())
        {
        }

        private SendGridMailProviderBuilder(ISendGridClient sendGridClient) => SendGridClient = sendGridClient;

        public SendGridMailProviderBuilder W(SendGridClientMockBuilder sendGridClientMockBuilder) => new SendGridMailProviderBuilder(sendGridClientMockBuilder.Build());

        public SendGridMailProvider Build()
        {
            return new SendGridMailProvider(SendGridClient);
        }
    }
}
