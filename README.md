# Ordering Service
This repository contains all services which make up for the whole Ordering Service. The complete Ordering Service is made out of multiple smaller components such as API and Lambda. 
A client package is available for other projects to consume when they need to communicate with the Ordering Service.

## Api Client
A NuGet package is available on the Crave NuGet feed (`Crave.Ordering.Api.Client`) which allows easy communication with the ordering API. This package exposes clients, dto models and enums which can be used to easily make API calls to the Ordering API.

Look at the code sample below for inspiration on how to include the clients to your own project:
```
public void ConfigureServices(IServiceCollection services)
{
    services.AddHttpClient<IOrdersClient, OrdersClient>(c => c.BaseAddress = new Uri("http://localhost:5000/"));
}
```

## Components
### Ordering Api
Main entry-point for all "external" services to fetch or create orders...

## Core Maintainers

* Floris Otto 
* Danny Kort

## AWS CDK
The AWS Cloud Development Kit is used to deploy the needed infrastructure of this project to AWS.

The `cdk.json` file tells the CDK Toolkit how to execute your app.

It uses the [.NET Core CLI](https://docs.microsoft.com/dotnet/articles/core/) to compile and execute your project.

### Useful commands

* `dotnet build src` compile this app
* `cdk deploy`       deploy this stack to your default AWS account/region
* `cdk diff`         compare deployed stack with current state
* `cdk synth`        emits the synthesized CloudFormation template

&nbsp;

&nbsp;

&nbsp;

&nbsp;

---
Old stuff below this line

# Crave Ordering

This repository contains all datasources, repositories and use cases related to ordering. This document gives a brief explanation how to set up ordering and use it within your own application. 
The project has support for both .NET standard and .NET Framework 4.7.2 implementations.

## What is this repository for?

Currently this repository contains a number of use cases which be used by external projects. These use cases are the **only ones** that can be resolved by your applications bootstrapper.

- CreateOrderUseCase* - Validates and saves a new order.*
- CreateOrderRouteUseCase* - Creates and starts a route for an existing order.*
- ProcessClientOrdersUseCase* - Processes orders submitted by a specific client.*
- ProcessTerminalOrderUseCase* - Processes a specific order for a terminal.*
- GetOrdersForTerminalUseCase* - Retrieves all active orders for a terminal.*
- GetOrdersHistoryForTerminalUseCase* - Retrieves all processed orders for a terminal.*
- UpdateForwardForTerminalUseCase* - Updates the forwarding of orders for a terminal.*
- UpdateRoutestephandlerForTerminalUseCase* - Updates the status of an order routestephandler for a terminal.*

## How do I get set up?

In order to use the `Crave Ordering` use cases, pull the latest Crave.Ordering Nuget package from the CraveGet package source into your application. 
Before registering your application's use cases, either call the `ConfigureOrdering` method on your `Container` when your project is using the bootstrapper from ServiceStack, or the `AddOrdering` method on `IServiceCollection` when your project is using the Microsoft bootstrapper.

```csharp
container.ConfigureOrdering(new ConfigurationBuilder()
                                  .WithLoggingConfiguration(new LoggingConfiguration(configurationManager.Environment.ToString(), configurationManager.InstanceName, configurationManager.SeqHost, configurationManager.SeqApiKey))
                                  .WithSoapApiConfiguration(new SoapApiConfiguration(configurationManager.LegacyApiBaseUrl))
                                  .WithAmazonConfiguration(new AmazonConfiguration(configurationManager.AmazonS3BaseUrl))
                                  .WithRedisConfiguration(new RedisConfiguration(configurationManager.RedisEnabled, configurationManager.RedisHost))
                                  .WithMailConfiguration(new MailConfiguration(configurationManager.MailHost, configurationManager.MailPort, configurationManager.MailUsername, configurationManager.MailPassword))
                                  .WithSmsConfiguration(new SmsConfiguration(configurationManager.MessageBirdAccessKey))
                                  .Build());
```

This will register all data sources, repositories and use cases internally used within the `Crave Ordering` assembly to the internal dependency container. Only the use cases mentioned above will be registered to the dependency container of your application.

## Configuration

The `Crave Ordering` bootstrapper is depending on a set of configuration values before it can be used properly:

### Logging Configuration

- Environment* - Environment in which the application is running. (i.e. Dev, Test, Production)*
- Application* - Name of the application.*
- Seq host* - The hostname of the Seq logging service. (optional)*
- Seq API key* - The API key used for communicating with Seq logging service. (optional)*

### SOAP API Configuration

- Soap Api base url* - The SOAP API base url to use, i.e. 'https://test.crave-emenu.com/api'*

### Amazon Configuration

- AmazonS3BaseUrl* - The Amazon S3 base url to use, i.e. 'https://s3-eu-west-1.amazonaws.com/cravecloud-test'*

### Redis Configuration

- RedisEnabled* - Determines whether Redis is enabled or not.*
- RedisHosts* - The host to connect to.*

### Mail Configuration

- Host* - The host or IP of the SMTP server.*
- Port* - The port on which to communicate with the SMTP server.*
- Username* - The username used to authenticate the sender.*
- Password* - The password used to authenticate the sender.*

### SMS Configuration

- MessageBirdAccessKey* - The access key used to communicate with MessageBird.*

## Contribution guidelines

* All use cases available to external projects need to be registered in the `bootstrapper` in the `Crave.Ordering.Bootstrapping` project.