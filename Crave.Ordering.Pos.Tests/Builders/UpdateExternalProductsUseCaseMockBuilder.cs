﻿using Crave.Integrations.POS.Domain.UseCases.Requests;
using Crave.Integrations.POS.Interfaces.UseCases;
using Crave.Ordering.Shared.Domain;
using Moq;

namespace Crave.Ordering.Pos.Tests.Builders
{
	internal class UpdateExternalProductsUseCaseMockBuilder
	{
		private readonly Response response;

		public UpdateExternalProductsUseCaseMockBuilder() : this(Response.AsSuccess("Everything is hunky-dory in this unit test."))
		{
			
		}

		private UpdateExternalProductsUseCaseMockBuilder(Response response) => this.response = response;

		public IUpdateExternalProductsUseCase Build()
		{
			Mock<IUpdateExternalProductsUseCase> mock = new Mock<IUpdateExternalProductsUseCase>();
			mock.Setup(useCase => useCase.ExecuteAsync(It.IsAny<UpdateExternalProductsRequest>())).ReturnsAsync(response);
			return mock.Object;
		}

		public UpdateExternalProductsUseCaseMockBuilder W(Response value) => new UpdateExternalProductsUseCaseMockBuilder(value);
	}
}
