﻿using Crave.Integrations.POS.Deliverect.Domain.Models;

namespace Crave.Ordering.Pos.Tests.Builders
{
	internal class MenuBuilder
	{
		private readonly string channelLinkId;
		private readonly Category[] categories;

		public MenuBuilder() : this("channelLinkId", new [] {new CategoryBuilder().Build()})
		{

		}

		private MenuBuilder(string channelLinkId, Category[] categories)
		{
			this.channelLinkId = channelLinkId;
			this.categories = categories;
		}

		public Menu Build() => new Menu
		{
			ChannelLinkId = channelLinkId,
			Categories = categories
		};

		public MenuBuilder WithLinkId(string id) => new MenuBuilder(id, categories);

		public MenuBuilder W(params Category[] cats) => new MenuBuilder(channelLinkId, cats);
	}
}
