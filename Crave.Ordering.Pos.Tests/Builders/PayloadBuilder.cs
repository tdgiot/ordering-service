﻿using Crave.Integrations.POS.Deliverect.Domain.Models;

namespace Crave.Ordering.Pos.Tests.Builders
{
	internal class PayloadBuilder<T>
	{
		private readonly T content;

		protected PayloadBuilder(T content) => this.content = content;

		public Payload Build() => System.Text.Json.JsonSerializer.Serialize(content);
	}
}
