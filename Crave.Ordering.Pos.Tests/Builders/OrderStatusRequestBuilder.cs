﻿using Crave.Integrations.POS.Deliverect.Domain.Requests;

namespace Crave.Ordering.Pos.Tests.Builders
{
	internal class OrderStatusRequestBuilder
	{
		private readonly int channelOrderId;

		public OrderStatusRequestBuilder() : this(1)
		{
			
		}

		private OrderStatusRequestBuilder(int channelOrderId) => this.channelOrderId = channelOrderId;

		public OrderStatusRequest Build() => new OrderStatusRequest
		{
			ChannelOrderId = channelOrderId.ToString()
		};
	}
}
