﻿using Crave.Integrations.POS.Deliverect.Interfaces.Repositories;
using Crave.Integrations.POS.Deliverect.Interfaces.Validation;
using Crave.Integrations.POS.Deliverect.WebHooks;
using Crave.Integrations.POS.Interfaces;
using Crave.Integrations.POS.Interfaces.Repositories;
using Crave.Ordering.Api.Domain.Tests.Builders;
using Crave.Ordering.Shared.Gateway.Ordering;
using Microsoft.Extensions.Logging;

namespace Crave.Ordering.Pos.Tests.Builders
{
	internal class OrderStatusUpdateWebHookBuilder
	{
		private readonly ILogger<OrderStatusUpdateWebHook> logger;
		private readonly IExternalSystemLogger externalSystemLogger;
		private readonly IExternalSystemRepository externalSystemRepository;
		private readonly IOrderRoutestephandlerRepository orderRoutestephandlerRepository;
		private readonly IOrderingGateway orderingGateway;
		private readonly IHmacValidation hmacValidation;

		public OrderStatusUpdateWebHookBuilder() : this(
			new LoggerMockBuilder<OrderStatusUpdateWebHook>().Build(),
			new ExternalSystemLoggerMockBuilder().Build(),
			new ExternalSystemRepositoryMockBuilder().Build(),
			new OrderRoutestephandlerRepositoryMockBuilder().Build(),
			new OrderingGatewayMockBuilder().Build(),
			new HmacValidationBuilder().Build()
		)
		{
			
		}

		private OrderStatusUpdateWebHookBuilder(ILogger<OrderStatusUpdateWebHook> logger, IExternalSystemLogger externalSystemLogger, IExternalSystemRepository externalSystemRepository, IOrderRoutestephandlerRepository orderRoutestephandlerRepository, IOrderingGateway orderingGateway, IHmacValidation hmacValidation)
		{
			this.logger = logger;
			this.externalSystemLogger = externalSystemLogger;
			this.externalSystemRepository = externalSystemRepository;
			this.orderRoutestephandlerRepository = orderRoutestephandlerRepository;
			this.orderingGateway = orderingGateway;
			this.hmacValidation = hmacValidation;
		}

		public OrderStatusUpdateWebHook Build() => new OrderStatusUpdateWebHook(logger, externalSystemLogger, externalSystemRepository, orderRoutestephandlerRepository, orderingGateway, hmacValidation);

		public OrderStatusUpdateWebHookBuilder W(IExternalSystemRepository repository) =>
			new OrderStatusUpdateWebHookBuilder(logger, externalSystemLogger, repository, orderRoutestephandlerRepository, orderingGateway, hmacValidation);

		public OrderStatusUpdateWebHookBuilder W(IHmacValidation validation) =>
			new OrderStatusUpdateWebHookBuilder(logger, externalSystemLogger, externalSystemRepository, orderRoutestephandlerRepository, orderingGateway, validation);
	}
}
