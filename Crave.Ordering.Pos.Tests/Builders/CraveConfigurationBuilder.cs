﻿using Crave.Ordering.Shared.Gateway.Configuration;

namespace Crave.Ordering.Pos.Tests.Builders
{
	internal class CraveConfigurationBuilder
	{
		private readonly CraveGatewayConfiguration gateway;

		public CraveConfigurationBuilder() : this(new CraveGatewayConfiguration())
		{
			
		}

		public CraveConfigurationBuilder(CraveGatewayConfiguration gateway) => this.gateway = gateway;


		public CraveConfiguration Build() =>
			new CraveConfiguration
			{
				Gateway = gateway
			};
	}
}
