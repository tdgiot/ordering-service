﻿using Crave.Integrations.POS.Deliverect.Interfaces.Validation;
using Crave.Integrations.POS.Deliverect.WebHooks;
using Crave.Integrations.POS.Interfaces;
using Crave.Integrations.POS.Interfaces.Repositories;
using Crave.Integrations.POS.Interfaces.UseCases;

namespace Crave.Ordering.Pos.Tests.Builders
{
	internal class SnoozeUnsnoozeWebHookBuilder
	{
		private readonly IExternalSystemLogger externalSystemLogger;
		private readonly IExternalSystemRepository externalSystemRepository;
		private readonly IExternalProductRepository externalProductRepository;
		private readonly IUpdateItemsLinkedToExternalProductsUseCase updateItemsLinkedToExternalProductsUseCase;
		private readonly IHmacValidation hmacValidation;

		public SnoozeUnsnoozeWebHookBuilder() : this(
			new ExternalSystemLoggerMockBuilder().Build(),
			new ExternalSystemRepositoryMockBuilder().Build(),
			new ExternalProductRepositoryMockBuilder().Build(),
			new UpdateItemsLinkedToExternalProductsUseCaseMockBuilder().Build(),
			new HmacValidationBuilder().Build())
		{
			
		}

		private SnoozeUnsnoozeWebHookBuilder(IExternalSystemLogger externalSystemLogger, IExternalSystemRepository externalSystemRepository, IExternalProductRepository externalProductRepository, IUpdateItemsLinkedToExternalProductsUseCase updateItemsLinkedToExternalProductsUseCase, IHmacValidation hmacValidation)
		{
			this.externalSystemLogger = externalSystemLogger;
			this.externalSystemRepository = externalSystemRepository;
			this.externalProductRepository = externalProductRepository;
			this.updateItemsLinkedToExternalProductsUseCase = updateItemsLinkedToExternalProductsUseCase;
			this.hmacValidation = hmacValidation;
		}

		public SnoozeUnsnoozeWebHook Build() => new SnoozeUnsnoozeWebHook(externalSystemLogger, externalSystemRepository, externalProductRepository, updateItemsLinkedToExternalProductsUseCase, hmacValidation);

		public SnoozeUnsnoozeWebHookBuilder W(IExternalSystemRepository repository) =>
			new SnoozeUnsnoozeWebHookBuilder(externalSystemLogger, repository, externalProductRepository, updateItemsLinkedToExternalProductsUseCase, hmacValidation);

		public SnoozeUnsnoozeWebHookBuilder W(IHmacValidation validation) =>
			new SnoozeUnsnoozeWebHookBuilder(externalSystemLogger, externalSystemRepository, externalProductRepository, updateItemsLinkedToExternalProductsUseCase, validation);
	}
}
