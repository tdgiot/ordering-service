﻿using Crave.Integrations.POS.Deliverect.Interfaces.Validation;
using Crave.Integrations.POS.Deliverect.WebHooks;
using Crave.Integrations.POS.Interfaces;
using Crave.Integrations.POS.Interfaces.Repositories;
using Crave.Integrations.POS.Interfaces.UseCases;

namespace Crave.Ordering.Pos.Tests.Builders
{
    public class MenuUpdateWebHookBuilder
    {
        private readonly IExternalSystemLogger externalSystemLogger;
        private readonly IExternalSystemRepository externalSystemRepository;
        private readonly IUpdateExternalProductsUseCase updateExternalProductsUseCase;
        private readonly IUpdateItemsLinkedToExternalProductsUseCase updateItemsLinkedToExternalProductsUseCase;
        private readonly IHmacValidation hmacValidation;

        public MenuUpdateWebHookBuilder() : this(
            new ExternalSystemLoggerMockBuilder().Build(),
            new ExternalSystemRepositoryMockBuilder().Build(),
            new UpdateExternalProductsUseCaseMockBuilder().Build(),
            new UpdateItemsLinkedToExternalProductsUseCaseMockBuilder().Build(),
            new HmacValidationBuilder().Build())
        {

        }

        private MenuUpdateWebHookBuilder(
            IExternalSystemLogger externalSystemLogger,
            IExternalSystemRepository externalSystemRepository,
            IUpdateExternalProductsUseCase updateExternalProductsUseCase,
            IUpdateItemsLinkedToExternalProductsUseCase updateItemsLinkedToExternalProductsUseCase,
            IHmacValidation hmacValidation)
        {
            this.externalSystemLogger = externalSystemLogger;
            this.externalSystemRepository = externalSystemRepository;
            this.updateExternalProductsUseCase = updateExternalProductsUseCase;
            this.updateItemsLinkedToExternalProductsUseCase = updateItemsLinkedToExternalProductsUseCase;
            this.hmacValidation = hmacValidation;
        }

        public MenuUpdateWebHook Build() => new MenuUpdateWebHook(
            externalSystemLogger,
            externalSystemRepository,
            updateExternalProductsUseCase,
            updateItemsLinkedToExternalProductsUseCase,
            hmacValidation);

        public MenuUpdateWebHookBuilder W(IExternalSystemLogger logger) =>
            new MenuUpdateWebHookBuilder(logger, externalSystemRepository, updateExternalProductsUseCase, updateItemsLinkedToExternalProductsUseCase, hmacValidation);

        public MenuUpdateWebHookBuilder W(IExternalSystemRepository repository) =>
            new MenuUpdateWebHookBuilder(externalSystemLogger, repository, updateExternalProductsUseCase, updateItemsLinkedToExternalProductsUseCase, hmacValidation);

        public MenuUpdateWebHookBuilder W(IUpdateExternalProductsUseCase useCase) =>
            new MenuUpdateWebHookBuilder(externalSystemLogger, externalSystemRepository, useCase, updateItemsLinkedToExternalProductsUseCase, hmacValidation);

        public MenuUpdateWebHookBuilder W(IUpdateItemsLinkedToExternalProductsUseCase useCase) =>
            new MenuUpdateWebHookBuilder(externalSystemLogger, externalSystemRepository, updateExternalProductsUseCase, useCase, hmacValidation);

        public MenuUpdateWebHookBuilder W(IHmacValidation validation) =>
            new MenuUpdateWebHookBuilder(externalSystemLogger, externalSystemRepository, updateExternalProductsUseCase, updateItemsLinkedToExternalProductsUseCase, validation);
    }
}
