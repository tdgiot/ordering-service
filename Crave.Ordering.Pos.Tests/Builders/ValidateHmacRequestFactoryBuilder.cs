﻿using Crave.Integrations.POS.Deliverect.Factories;

namespace Crave.Ordering.Pos.Tests.Builders
{
	internal class ValidateHmacRequestFactoryBuilder
	{
		public ValidateHmacRequestFactory Build() => new ValidateHmacRequestFactory();
	}
}
