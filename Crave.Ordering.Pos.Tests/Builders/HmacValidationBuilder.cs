﻿using Crave.Integrations.POS.Deliverect.Domain.UseCases;
using Crave.Integrations.POS.Deliverect.Interfaces.Factories;
using Crave.Integrations.POS.Deliverect.Interfaces.Validation;
using Crave.Integrations.POS.Deliverect.Validation;

namespace Crave.Ordering.Pos.Tests.Builders
{
	internal class HmacValidationBuilder
	{
		private readonly IValidateHmacRequestFactory validateHmacRequestFactory;
		private readonly IValidateHmacUseCase validateHmacUseCase;

		public HmacValidationBuilder() : this(
			new ValidateHmacRequestFactoryBuilder().Build(),
			new ValidateHmacUseCaseBuilder().Build())
		{
			
		}

		private HmacValidationBuilder(IValidateHmacRequestFactory validateHmacRequestFactory, IValidateHmacUseCase validateHmacUseCase)
		{
			this.validateHmacRequestFactory = validateHmacRequestFactory;
			this.validateHmacUseCase = validateHmacUseCase;
		}

		public IHmacValidation Build() => new HmacValidation(validateHmacRequestFactory, validateHmacUseCase);

		public HmacValidationBuilder W(IValidateHmacRequestFactory factory) => new HmacValidationBuilder(factory, validateHmacUseCase);

		public HmacValidationBuilder W(ValidateHmacUseCase useCase) => new HmacValidationBuilder(validateHmacRequestFactory, useCase);
	}
}
