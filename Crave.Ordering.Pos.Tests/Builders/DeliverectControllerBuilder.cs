﻿using Crave.Ordering.Pos.Deliverect.Api.Controllers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Crave.Ordering.Pos.Tests.Builders
{
	internal class DeliverectControllerBuilder
	{
		private readonly HttpContext httpContext;

		public DeliverectControllerBuilder() : this(new HttpContextBuilder().Build())
		{
			
		}
		private DeliverectControllerBuilder(HttpContext httpContext) => this.httpContext = httpContext;

		public DeliverectController Build() => new DeliverectController
		{
			ControllerContext = new ControllerContext
			{
				HttpContext = httpContext
			}
		};

		public DeliverectControllerBuilder W(HttpContext context) => new DeliverectControllerBuilder(context);
	}
}
