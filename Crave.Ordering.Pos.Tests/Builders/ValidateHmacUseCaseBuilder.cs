using Crave.Integrations.POS.Deliverect.Domain.UseCases;

namespace Crave.Ordering.Pos.Tests.Builders
{
	public class ValidateHmacUseCaseBuilder
	{
		public IValidateHmacUseCase Build() => new ValidateHmacUseCase();
	}
}
