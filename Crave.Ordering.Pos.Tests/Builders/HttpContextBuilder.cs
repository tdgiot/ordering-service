﻿using System.IO;
using Microsoft.AspNetCore.Http;

namespace Crave.Ordering.Pos.Tests.Builders
{
	internal class HttpContextBuilder
	{
		private readonly string hmacSignature;
		private readonly Stream requestBody;

		public HttpContextBuilder() : this(new HmacSignatureBuilder().Build(), new StreamBuilder().Build())
		{
			
		}

		private HttpContextBuilder(string hmacSignature, Stream requestBody)
		{
			this.hmacSignature = hmacSignature;
			this.requestBody = requestBody;
		}

		public HttpContext Build()
		{
			DefaultHttpContext httpContext = new DefaultHttpContext();
			httpContext.Request.Headers["X-Server-Authorization-HMAC-SHA256"] = hmacSignature;
			httpContext.Request.Body = requestBody;
			return httpContext;
		}

		public HttpContextBuilder W(string signature) => new HttpContextBuilder(signature, requestBody);

		public HttpContextBuilder W(Stream body) => new HttpContextBuilder(hmacSignature, body);
	}
}
