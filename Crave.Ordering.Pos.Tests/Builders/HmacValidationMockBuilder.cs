﻿using Crave.Integrations.POS.Deliverect.Interfaces;
using Crave.Integrations.POS.Deliverect.Interfaces.Validation;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Moq;

namespace Crave.Ordering.Pos.Tests.Builders
{
	public class HmacValidationMockBuilder
	{
		private readonly bool validationResult;

		public HmacValidationMockBuilder() : this(true)
		{
			
		}

		private HmacValidationMockBuilder(bool validationResult) => this.validationResult = validationResult;

		public IHmacValidation Build()
		{
			Mock<IHmacValidation> mock = new Mock<IHmacValidation>();
			mock.Setup(validator => validator.ValidateRequest(It.IsAny<IHmacValidationContent>(), It.IsAny<ExternalSystemEntity>())).Returns(validationResult);
			return mock.Object;
		}

		public HmacValidationMockBuilder W(bool result) => new HmacValidationMockBuilder(result);
	}
}
