﻿using Crave.Integrations.POS.Interfaces.Repositories;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Crave.Ordering.Shared.Data.LLBLGen.HelperClasses;
using Moq;

namespace Crave.Ordering.Pos.Tests.Builders
{
	internal class ExternalProductRepositoryMockBuilder
	{
		private readonly EntityCollection<ExternalProductEntity> externalProducts;

		public ExternalProductRepositoryMockBuilder() : this(new EntityCollection<ExternalProductEntity>())
		{
			
		}
		private ExternalProductRepositoryMockBuilder(EntityCollection<ExternalProductEntity> externalProducts) => this.externalProducts = externalProducts;

		public IExternalProductRepository Build()
		{
			Mock<IExternalProductRepository> mock = new Mock<IExternalProductRepository>();
			mock.Setup(repository => repository.GetExternalProductsPrefetchedAsync(It.IsAny<int>())).ReturnsAsync(externalProducts);
			mock.Setup(repository => repository.GetExternalProductsAsync(It.IsAny<int>())).ReturnsAsync(externalProducts);
			return mock.Object;
		}
	}
}
