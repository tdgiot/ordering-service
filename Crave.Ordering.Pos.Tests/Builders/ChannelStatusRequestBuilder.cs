﻿using Crave.Integrations.POS.Deliverect.Domain.Requests;

namespace Crave.Ordering.Pos.Tests.Builders
{
	internal class ChannelStatusRequestBuilder
	{
		private readonly string status;
		private readonly string channelLinkId;
		private readonly string channelLocationId;

		public ChannelStatusRequestBuilder() : this("inactive","6033dd2a8fe40ca01ab05692", "cada1d56-ff5f-4765-af36-c8362096b766")
		{
			
		}

		private ChannelStatusRequestBuilder(string status, string channelLinkId, string channelLocationId)
		{
			this.status = status;
			this.channelLinkId = channelLinkId;
			this.channelLocationId = channelLocationId;
		}

		public ChannelStatusRequest Build() => new ChannelStatusRequest
		{
			Status = status,
			ChannelLinkId = channelLinkId,
			ChannelLocationId = channelLocationId
		};

		public ChannelStatusRequestBuilder WithLinkId(string id) => new ChannelStatusRequestBuilder(status, id, channelLocationId);

		public ChannelStatusRequestBuilder WithLocationId(string id) => new ChannelStatusRequestBuilder(status, channelLinkId, id);

		public ChannelStatusRequestBuilder WithStatus(string newStatus) => new ChannelStatusRequestBuilder(newStatus, channelLinkId, channelLocationId);
	}
}
