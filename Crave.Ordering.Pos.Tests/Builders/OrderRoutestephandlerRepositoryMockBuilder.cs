﻿using Crave.Integrations.POS.Deliverect.Interfaces.Repositories;
using Moq;

namespace Crave.Ordering.Pos.Tests.Builders
{
	internal class OrderRoutestephandlerRepositoryMockBuilder
	{
		public IOrderRoutestephandlerRepository Build()
		{
			Mock<IOrderRoutestephandlerRepository> mock = new Mock<IOrderRoutestephandlerRepository>();
			return mock.Object;
		}
	}
}
