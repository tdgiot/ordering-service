using Crave.Integrations.POS.Deliverect.Domain.Models;
using System.Security.Cryptography;
using System.Text;

namespace Crave.Ordering.Pos.Tests.Builders
{
	public class HmacKeyBuilder
	{
		private readonly byte[] secretKey;

		public HmacKeyBuilder() : this(CreateRandomSecretKey())
		{
			
		}

		private HmacKeyBuilder(byte[] secretKey) => this.secretKey = secretKey;

		public HmacKey Build() => secretKey;

		public HmacKeyBuilder W(string key) => new HmacKeyBuilder(Encoding.UTF8.GetBytes(key));

		public static byte[] CreateRandomSecretKey(int size = 64)
		{
			byte[] randomSecretKey = new byte[size];
			using RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
			rng.GetBytes(randomSecretKey);
			return randomSecretKey;
		}
	}
}
