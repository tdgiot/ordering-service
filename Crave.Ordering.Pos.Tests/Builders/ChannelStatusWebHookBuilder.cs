﻿using Crave.Integrations.POS.Deliverect.Interfaces.Validation;
using Crave.Integrations.POS.Deliverect.WebHooks;
using Crave.Integrations.POS.Interfaces;
using Crave.Integrations.POS.Interfaces.Repositories;
using Crave.Ordering.Api.Domain.Tests.Builders;
using Crave.Ordering.Shared.Gateway.Configuration;
using Microsoft.Extensions.Logging;

namespace Crave.Ordering.Pos.Tests.Builders
{
	internal class ChannelStatusWebHookBuilder
	{
		private readonly ILogger<ChannelStatusWebHook> channelStatusWebhookLogger;
		private readonly IExternalSystemLogger externalSystemLogger;
		private readonly IExternalSystemRepository externalSystemRepository;
		private readonly IHmacValidation hmacValidation;
		private readonly CraveConfiguration craveConfiguration;

		public ChannelStatusWebHookBuilder() : this(
			new LoggerMockBuilder<ChannelStatusWebHook>().Build(),
			new ExternalSystemLoggerMockBuilder().Build(),
			new ExternalSystemRepositoryMockBuilder().Build(),
			new HmacValidationBuilder().Build(),
			new CraveConfigurationBuilder().Build())
		{
			
		}

		private ChannelStatusWebHookBuilder(ILogger<ChannelStatusWebHook> channelStatusWebhookLogger, IExternalSystemLogger externalSystemLogger, IExternalSystemRepository externalSystemRepository, IHmacValidation hmacValidation, CraveConfiguration craveConfiguration)
		{
			this.channelStatusWebhookLogger = channelStatusWebhookLogger;
			this.externalSystemLogger = externalSystemLogger;
			this.externalSystemRepository = externalSystemRepository;
			this.hmacValidation = hmacValidation;
			this.craveConfiguration = craveConfiguration;
		}

		public ChannelStatusWebHook Build() => new ChannelStatusWebHook(
			channelStatusWebhookLogger,
			externalSystemLogger, 
			externalSystemRepository,
			hmacValidation, 
			craveConfiguration);

		public ChannelStatusWebHookBuilder W(ILogger<ChannelStatusWebHook> logger) =>
			new ChannelStatusWebHookBuilder(logger, externalSystemLogger, externalSystemRepository, hmacValidation, craveConfiguration);

		public ChannelStatusWebHookBuilder W(IExternalSystemLogger logger) =>
			new ChannelStatusWebHookBuilder(channelStatusWebhookLogger, logger, externalSystemRepository, hmacValidation, craveConfiguration);

		public ChannelStatusWebHookBuilder W(IExternalSystemRepository repository) =>
			new ChannelStatusWebHookBuilder(channelStatusWebhookLogger, externalSystemLogger, repository, hmacValidation, craveConfiguration);

		public ChannelStatusWebHookBuilder W(IHmacValidation validation) =>
			new ChannelStatusWebHookBuilder(channelStatusWebhookLogger, externalSystemLogger, externalSystemRepository, validation, craveConfiguration);

		public ChannelStatusWebHookBuilder W(CraveConfiguration config) =>
			new ChannelStatusWebHookBuilder(channelStatusWebhookLogger, externalSystemLogger, externalSystemRepository, hmacValidation, config);
	}
}
