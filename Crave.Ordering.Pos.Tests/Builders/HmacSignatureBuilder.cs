using Crave.Integrations.POS.Deliverect.Domain.Models;
using System.Security.Cryptography;
using System.Text;

namespace Crave.Ordering.Pos.Tests.Builders
{
	public class HmacSignatureBuilder
	{
		private readonly HmacKey key;
		private readonly Payload content;

		public HmacSignatureBuilder() : this(new HmacKeyBuilder().Build(), new UntypedPayloadBuilder().Build())
		{
		}

		private HmacSignatureBuilder(HmacKey key, Payload content)
		{
			this.key = key;
			this.content = content;
		}

		public string Build()
		{
			using HMACSHA256 hmacshA256 = new HMACSHA256(key);
			byte[] data = hmacshA256.ComputeHash(content);

			StringBuilder sBuilder = new StringBuilder();

			foreach (byte currentByte in data)
			{
				sBuilder.Append(currentByte.ToString("x2"));
			}

			return sBuilder.ToString();
		}

		public HmacSignatureBuilder W(HmacKey value) => new HmacSignatureBuilder(value, content);

		public HmacSignatureBuilder W(Payload value) => new HmacSignatureBuilder(key, value);
	}
}
