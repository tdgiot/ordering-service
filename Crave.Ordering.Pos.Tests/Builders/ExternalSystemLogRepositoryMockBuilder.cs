﻿using Crave.Integrations.POS.Interfaces;
using Moq;

namespace Crave.Ordering.Pos.Tests.Builders
{
	internal class ExternalSystemLogRepositoryMockBuilder
	{
		public IExternalSystemLogRepository Build() => new Mock<IExternalSystemLogRepository>().Object;
	}
}
