using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;

namespace Crave.Ordering.Pos.Tests.Builders
{
    public class ExternalSystemEntityBuilder
    {
        private readonly string channelLinkId;
        private readonly string hmacKey;
        private readonly Enums.ExternalSystemEnvironment environment;
        private readonly ExternalProductEntity[] externalProductEntities;
        private readonly bool boolValue2;

        public ExternalSystemEntityBuilder() : this(nameof(channelLinkId), nameof(hmacKey), Enums.ExternalSystemEnvironment.Test, null, false)
        {

        }

        private ExternalSystemEntityBuilder(string channelLinkId, string hmacKey, Enums.ExternalSystemEnvironment environment, ExternalProductEntity[] externalProductEntities, bool boolValue2)
        {
            this.channelLinkId = channelLinkId;
            this.hmacKey = hmacKey;
            this.environment = environment;
            this.externalProductEntities = externalProductEntities;
            this.boolValue2 = boolValue2;
        }

        public ExternalSystemEntity Build()
        {
            ExternalSystemEntity externalSystemEntity = new ExternalSystemEntity
            {
                IsNew = false,
                StringValue3 = channelLinkId,
                StringValue5 = hmacKey,
                Environment = environment,
                BoolValue2 = boolValue2
            };

            if (externalProductEntities != null)
            {
                externalSystemEntity.ExternalProductCollection.AddRange(externalProductEntities);
            }

            return externalSystemEntity;
        }

        public ExternalSystemEntityBuilder WithChannelLinkId(string value) => new ExternalSystemEntityBuilder(value, hmacKey, environment, externalProductEntities, boolValue2);
        public ExternalSystemEntityBuilder WithHmacKey(string value) => new ExternalSystemEntityBuilder(channelLinkId, value, environment, externalProductEntities, boolValue2);
        public ExternalSystemEntityBuilder WithBoolValue2(bool value) => new ExternalSystemEntityBuilder(channelLinkId, hmacKey, environment, externalProductEntities, value);
        public ExternalSystemEntityBuilder W(Enums.ExternalSystemEnvironment value) => new ExternalSystemEntityBuilder(channelLinkId, hmacKey, value, externalProductEntities, boolValue2);
        public ExternalSystemEntityBuilder W(params ExternalProductEntity[] productEntities) => new ExternalSystemEntityBuilder(channelLinkId, hmacKey, environment, productEntities, boolValue2);
    }
}
