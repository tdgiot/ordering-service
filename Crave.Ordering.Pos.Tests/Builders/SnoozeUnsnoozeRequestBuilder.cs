﻿using System.Collections.Generic;
using Crave.Integrations.POS.Deliverect.Domain.Models;
using Crave.Integrations.POS.Deliverect.Domain.Requests;

namespace Crave.Ordering.Pos.Tests.Builders
{
	public class SnoozeUnsnoozeRequestBuilder
	{
		private readonly List<Operation> operations;

		public SnoozeUnsnoozeRequestBuilder() : this(new List<Operation>())
		{
			
		}

		private SnoozeUnsnoozeRequestBuilder(List<Operation> operations) => this.operations = operations;

		public SnoozeUnsnoozeRequest Build() => new SnoozeUnsnoozeRequest
		{
			Operations = operations
		};
	}
}
