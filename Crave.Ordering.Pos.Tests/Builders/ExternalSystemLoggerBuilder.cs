﻿using Crave.Integrations.POS.Domain;
using Crave.Integrations.POS.Interfaces;

namespace Crave.Ordering.Pos.Tests.Builders
{
	internal class ExternalSystemLoggerBuilder
	{
		private readonly IExternalSystemLogRepository externalSystemLogRepository;

		public ExternalSystemLoggerBuilder() : this(new ExternalSystemLogRepositoryMockBuilder().Build())
		{
			
		}

		private ExternalSystemLoggerBuilder(IExternalSystemLogRepository externalSystemLogRepository) => this.externalSystemLogRepository = externalSystemLogRepository;

		public ExternalSystemLogger Build() => new ExternalSystemLogger(externalSystemLogRepository);

		public ExternalSystemLoggerBuilder W(IExternalSystemLogRepository repository) => new ExternalSystemLoggerBuilder(repository);
	}
}
