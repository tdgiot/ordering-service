﻿using Crave.Integrations.POS.Deliverect.Domain.Requests;
using Crave.Ordering.Pos.Tests.Controllers;

namespace Crave.Ordering.Pos.Tests.Builders
{
	internal class ChannelStatusRequestPayloadBuilder : PayloadBuilder<ChannelStatusRequest>
	{
		public ChannelStatusRequestPayloadBuilder() : this(new ChannelStatusRequestBuilder().Build())
		{
			
		}

		private ChannelStatusRequestPayloadBuilder(ChannelStatusRequest channelStatusRequest) : base(channelStatusRequest)
		{

		}

		public ChannelStatusRequestPayloadBuilder W(ChannelStatusRequest request) => new ChannelStatusRequestPayloadBuilder(request);
	}
}
