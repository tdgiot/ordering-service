﻿using Crave.Integrations.POS.Domain.UseCases.Requests;
using Crave.Integrations.POS.Interfaces.UseCases;
using Crave.Ordering.Shared.Domain;
using Moq;

namespace Crave.Ordering.Pos.Tests.Builders
{
	internal class UpdateItemsLinkedToExternalProductsUseCaseMockBuilder
	{
		private readonly Response response;

		public UpdateItemsLinkedToExternalProductsUseCaseMockBuilder() : this(Response.AsSuccess("Everything is hunky-dory in this unit test."))
		{
			
		}

		private UpdateItemsLinkedToExternalProductsUseCaseMockBuilder(Response response) => this.response = response;

		public IUpdateItemsLinkedToExternalProductsUseCase Build()
		{
			Mock<IUpdateItemsLinkedToExternalProductsUseCase> mock = new Mock<IUpdateItemsLinkedToExternalProductsUseCase>();
			mock.Setup(useCase => useCase.ExecuteAsync(It.IsAny<UpdateItemsLinkedToExternalProductsRequest>())).ReturnsAsync(response);
			return mock.Object;
		}

		public UpdateItemsLinkedToExternalProductsUseCaseMockBuilder W(Response value) => new UpdateItemsLinkedToExternalProductsUseCaseMockBuilder(value);

	}
}
