using Crave.Integrations.POS.Deliverect.Domain.Models;

namespace Crave.Ordering.Pos.Tests.Builders
{
	public class UntypedPayloadBuilder
	{
		private readonly string status;
		private readonly string channelLocationId;
		private readonly string channelLinkId;


		public UntypedPayloadBuilder() : this("inactive", "cada1d56-ff5f-4765-af36-c8362096b766", "6033dd2a8fe40ca01ab05692")
		{
		}

		private UntypedPayloadBuilder(string status, string channelLocationId, string channelLinkId)
		{
			this.status = status;
			this.channelLocationId = channelLocationId;
			this.channelLinkId = channelLinkId;
		}
		public Payload Build() => System.Text.Json.JsonSerializer.Serialize(new
		{
			status = status,
			channelLocationId = channelLocationId,
			channelLinkId = channelLinkId
		});

		public UntypedPayloadBuilder WithStatus(string value) => new UntypedPayloadBuilder(value, channelLocationId, channelLinkId);
		public UntypedPayloadBuilder WithChannelLocationId(string value) => new UntypedPayloadBuilder(status, value, channelLinkId);
		public UntypedPayloadBuilder WithChannelLinkId(string value) => new UntypedPayloadBuilder(status, channelLocationId, value);
	}
}
