﻿using Crave.Integrations.POS.Deliverect.Domain.Models;
using Crave.Ordering.Pos.Tests.Controllers;

namespace Crave.Ordering.Pos.Tests.Builders
{
	internal class MenuArrayPayloadBuilder : PayloadBuilder<Menu[]>
	{
		public MenuArrayPayloadBuilder() : this(new[] {new MenuBuilder().Build()})
		{
			
		}

		private MenuArrayPayloadBuilder(Menu[] menus) : base(menus)
		{
			
		}

		public MenuArrayPayloadBuilder W(Menu[] menus) => new MenuArrayPayloadBuilder(menus);
	}
}
