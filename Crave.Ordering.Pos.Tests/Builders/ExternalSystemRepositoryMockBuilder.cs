﻿using Crave.Integrations.POS.Interfaces.Repositories;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Moq;

namespace Crave.Ordering.Pos.Tests.Builders
{
	public class ExternalSystemRepositoryMockBuilder
	{
		private readonly ExternalSystemEntity externalSystemEntity;
		private readonly bool saveResult = true;

		public ExternalSystemRepositoryMockBuilder() : this(new ExternalSystemEntityBuilder().Build())
		{
			
		}

		private ExternalSystemRepositoryMockBuilder(ExternalSystemEntity externalSystemEntity) => this.externalSystemEntity = externalSystemEntity;

		public IExternalSystemRepository Build()
		{
			Mock<IExternalSystemRepository> repository = new Mock<IExternalSystemRepository>();
			repository.Setup(repo => repo.GetByLocationIdAsync(It.IsAny<string>())).ReturnsAsync(externalSystemEntity);
			repository.Setup(repo => repo.GetByChannelLinkIdAsync(It.IsAny<string>())).ReturnsAsync(externalSystemEntity);
			repository.Setup(repo => repo.GetByOrderIdAsync(It.IsAny<int>())).ReturnsAsync(externalSystemEntity);
			repository.Setup(repo => repo.GetByIdWithExternalProductsPrefetchedAsync(It.IsAny<int>(), It.IsAny<string>())).ReturnsAsync(externalSystemEntity);
			repository.Setup(repo => repo.SaveAsync(It.IsAny<ExternalSystemEntity>())).ReturnsAsync(saveResult);
			return repository.Object;
		}

		public ExternalSystemRepositoryMockBuilder W(ExternalSystemEntityBuilder builder) => W(builder.Build());

		public ExternalSystemRepositoryMockBuilder W(ExternalSystemEntity entity) => new ExternalSystemRepositoryMockBuilder(entity);
	}
}
