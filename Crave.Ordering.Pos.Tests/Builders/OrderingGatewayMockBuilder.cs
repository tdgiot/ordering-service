﻿using Crave.Ordering.Shared.Gateway.Ordering;
using Moq;

namespace Crave.Ordering.Pos.Tests.Builders
{
	internal class OrderingGatewayMockBuilder
	{
		public IOrderingGateway Build()
		{
			Mock<IOrderingGateway> mock = new Mock<IOrderingGateway>();
			return mock.Object;
		}
	}
}
