﻿using System.IO;
using System.Threading.Tasks;

namespace Crave.Ordering.Pos.Tests.Builders
{
	internal class StreamBuilder
	{
		private readonly string content;

		public StreamBuilder() : this(string.Empty)
		{
			
		}

		private StreamBuilder(string content) => this.content = content;

		public Stream Build() => BuildAsync().ConfigureAwait(false).GetAwaiter().GetResult();

		public async Task<Stream> BuildAsync()
		{
			Stream stream = new MemoryStream();
			StreamWriter streamWriter = new StreamWriter(stream);
			await streamWriter.WriteAsync(content).ConfigureAwait(false);
			await streamWriter.FlushAsync().ConfigureAwait(false);
			stream.Position = 0;
			return stream;
		}

		public StreamBuilder W(string value) => new StreamBuilder(value);
	}
}
