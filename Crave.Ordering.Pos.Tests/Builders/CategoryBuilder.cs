﻿using Crave.Integrations.POS.Deliverect.Domain.Models;

namespace Crave.Ordering.Pos.Tests.Builders
{
	internal class CategoryBuilder
	{
		private readonly string menu;

		public CategoryBuilder() : this("Menu 1")
		{
			
		}

		private CategoryBuilder(string menu) => this.menu = menu;

		public Category Build() => new Category {Menu = menu};
	}
}
