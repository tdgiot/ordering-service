﻿using Crave.Integrations.POS.Interfaces;
using Moq;

namespace Crave.Ordering.Pos.Tests.Builders
{
	internal class ExternalSystemLoggerMockBuilder
	{
		public IExternalSystemLogger Build()
		{
			Mock<IExternalSystemLogger> logger = new Mock<IExternalSystemLogger>();
			return logger.Object;
		}
	}
}
