﻿using Crave.Integrations.POS.Deliverect.Domain.Models;

namespace Crave.Ordering.Pos.Tests
{
	internal class DeliverectChannelUpdatePayload
	{
		public string HmacKey { get; } = "6033dd2a8fe40ca01ab05692";
		
		
		public string HmacKeyEncrypted { get; } = "uo1VCG6MzWnrf0YTWt3TwnEfkpwwZuHSGcIhqdK+jcs=";

		public string HmacSignature { get; } = "2f02d8a7c3c7fc69bd8f1616c7c8ccb66c5d9316020050a493d05e96287b53c0";

		public Payload Body { get; } = "{\"status\": \"inactive\", \"channelLocationId\": \"cada1d56-ff5f-4765-af36-c8362096b766\", \"channelLinkId\": \"6033dd2a8fe40ca01ab05692\"}";
	}
}
