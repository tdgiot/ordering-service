﻿using Crave.Integrations.POS.Deliverect.Domain.Models;
using Crave.Integrations.POS.Deliverect.Domain.Requests;
using Crave.Integrations.POS.Deliverect.Domain.Responses;
using Crave.Integrations.POS.Deliverect.WebHooks;
using Crave.Integrations.POS.Interfaces;
using Crave.Ordering.Api.Domain.Tests.Builders;
using Crave.Ordering.Pos.Deliverect.Api.Controllers;
using Crave.Ordering.Pos.Tests.Builders;
using Crave.Ordering.Shared.Data.LLBLGen.EntityClasses;
using Microsoft.Extensions.Logging;
using Moq;
using NUnit.Framework;
using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Crave.Ordering.Pos.Tests.Controllers
{
	public class DeliverectControllerTest
	{
		private static class A
		{
			public static LoggerMockBuilder<ChannelStatusWebHook> LoggerMock => new LoggerMockBuilder<ChannelStatusWebHook>();
			public static ExternalSystemRepositoryMockBuilder ExternalSystemRepositoryMock => new ExternalSystemRepositoryMockBuilder();
			public static ExternalSystemEntityBuilder ExternalSystemEntity => new ExternalSystemEntityBuilder();
			public static HmacSignatureBuilder HmacSignature => new HmacSignatureBuilder();
			public static HttpContextBuilder HttpContext => new HttpContextBuilder();
			public static StreamBuilder Stream => new StreamBuilder();
			public static DeliverectControllerBuilder DeliverectController => new DeliverectControllerBuilder();
			public static ChannelStatusWebHookBuilder ChannelStatusWebHook => new ChannelStatusWebHookBuilder();
			public static ChannelStatusRequestPayloadBuilder ChannelStatusRequestPayload => new ChannelStatusRequestPayloadBuilder();
			public static ChannelStatusRequestBuilder ChannelStatusRequest => new ChannelStatusRequestBuilder();
			public static HmacKeyBuilder HmacKey => new HmacKeyBuilder();
			public static MenuBuilder Menu => new MenuBuilder();
			public static MenuArrayPayloadBuilder MenuArrayPayload => new MenuArrayPayloadBuilder();
			public static MenuUpdateWebHookBuilder MenuUpdateWebHook => new MenuUpdateWebHookBuilder();
			public static ExternalSystemLogRepositoryMockBuilder ExternalSystemLogRepositoryMock => new ExternalSystemLogRepositoryMockBuilder();
			public static ExternalSystemLoggerBuilder ExternalSystemLogger => new ExternalSystemLoggerBuilder();
			public static DeliverectChannelUpdatePayload DeliverectChannelUpdatePayload => new DeliverectChannelUpdatePayload();
			public static OrderStatusUpdateWebHookBuilder OrderStatusUpdateWebHook => new OrderStatusUpdateWebHookBuilder();
			public static OrderStatusRequestBuilder OrderStatusRequest => new OrderStatusRequestBuilder();
			public static SnoozeUnsnoozeWebHookBuilder SnoozeUnsnoozeWebHook => new SnoozeUnsnoozeWebHookBuilder();
			public static SnoozeUnsnoozeRequestBuilder SnoozeUnsnoozeRequest => new SnoozeUnsnoozeRequestBuilder();
		}

		[Test]
		public async Task UpdateChannelStatus_WithChannelLinkIdAndTestEnvironmentAndValidSignature_ReturnsResult()
		{
			// Arrange
			string hmacKey = A.DeliverectChannelUpdatePayload.HmacKey;

			ChannelStatusRequest channelStatusRequest = A.ChannelStatusRequest
				.WithLinkId(hmacKey)
				.Build();

			ChannelStatusWebHook channelStatusWebHook = A.ChannelStatusWebHook
				.W(A.ExternalSystemRepositoryMock
					.W(A.ExternalSystemEntity
						.WithChannelLinkId(hmacKey)
						.W(Enums.ExternalSystemEnvironment.Test)
						.Build())
					.Build())
				.Build();

			DeliverectController sut = A.DeliverectController
				.W(A.HttpContext
					.W(A.DeliverectChannelUpdatePayload.HmacSignature)
					.W(await A.Stream
						.W(A.DeliverectChannelUpdatePayload.Body)
						.BuildAsync())
					.Build())
				.Build();

			// Act
			ChannelStatusResponse result = await sut.UpdateChannelStatus(channelStatusWebHook, channelStatusRequest);

			// Assert
			Assert.NotNull(result);
		}

		[Test]
		public async Task UpdateChannelStatus_WithHmacKeyAndLiveEnvironmentAndValidSignature_ReturnsResult()
		{
			// Arrange
			string hmacKey = A.DeliverectChannelUpdatePayload.HmacKeyEncrypted;

			ChannelStatusRequest channelStatusRequest = A.ChannelStatusRequest
				.WithLinkId(hmacKey)
				.Build();

			ChannelStatusWebHook channelStatusWebHook = A.ChannelStatusWebHook
				.W(A.ExternalSystemRepositoryMock
					.W(A.ExternalSystemEntity
						.WithHmacKey(hmacKey)
						.W(Enums.ExternalSystemEnvironment.Live)
						.Build())
					.Build())
				.Build();

			DeliverectController sut = A.DeliverectController
				.W(A.HttpContext
					.W(A.DeliverectChannelUpdatePayload.HmacSignature)
					.W(await A.Stream
						.W(A.DeliverectChannelUpdatePayload.Body)
						.BuildAsync())
					.Build())
				.Build();

			// Act
			ChannelStatusResponse result = await sut.UpdateChannelStatus(channelStatusWebHook, channelStatusRequest);

			// Assert
			Assert.NotNull(result);
		}

	
		[Test]
		public async Task UpdateChannelStatus_WithHmackKeyAndLiveEnvironmentAndInvalidSignature_ReturnsNull()
		{
			// Arrange
			string hmacKey = A.DeliverectChannelUpdatePayload.HmacKeyEncrypted;
			string signatureProvidedByEvilPersonTryingToFoolUs = "2g02d8a7c3c7fc69bd8f1616c7c8ccb66c5d9316020050a493d05e96287b53c0";

			ChannelStatusRequest channelStatusRequest = A.ChannelStatusRequest
				.WithLinkId(hmacKey)
				.Build();

			ChannelStatusWebHook channelStatusWebHook = A.ChannelStatusWebHook
				.W(A.ExternalSystemRepositoryMock
					.W(A.ExternalSystemEntity
						.WithHmacKey(hmacKey)
						.W(Enums.ExternalSystemEnvironment.Live)
						.Build())
					.Build())
				.Build();

			DeliverectController sut = A.DeliverectController
				.W(A.HttpContext
					.W(signatureProvidedByEvilPersonTryingToFoolUs)
					.W(await A.Stream
						.W(A.ChannelStatusRequestPayload
							.W(channelStatusRequest)
							.Build())
						.BuildAsync())
					.Build())
				.Build();


			// Act
			ChannelStatusResponse result = await sut.UpdateChannelStatus(channelStatusWebHook, channelStatusRequest);

			// Assert
			Assert.Null(result);
		}

		
		[Test]
		public async Task UpdateChannelStatus_WithoutRequestBody_ReturnsNull()
		{
			// Arrange
			string hmacKey = A.DeliverectChannelUpdatePayload.HmacKey;

			ChannelStatusRequest channelStatusRequest = A.ChannelStatusRequest
				.WithLinkId(hmacKey)
				.Build();

			ChannelStatusWebHook channelStatusWebHook = A.ChannelStatusWebHook
				.W(A.ExternalSystemRepositoryMock
					.W(A.ExternalSystemEntity
						.WithChannelLinkId(hmacKey)
						.W(Enums.ExternalSystemEnvironment.Test)
						.Build())
					.Build())
				.Build();

			Payload payload = A.ChannelStatusRequestPayload
				.W(channelStatusRequest)
				.Build();

			DeliverectController sut = A.DeliverectController
				.W(A.HttpContext
					.W(A.DeliverectChannelUpdatePayload.HmacSignature)
					.W((Stream) null)
					.Build())
				.Build();

			// Act
			ChannelStatusResponse result = await sut.UpdateChannelStatus(channelStatusWebHook, channelStatusRequest);

			// Assert
			Assert.Null(result);
		}

		[Test]
		public async Task MenuPush_WithChannelLinkIdAndTestEnvironmentAndValidSignature_LogsSuccess()
		{
			// Arrange
			const string channelLinkId = "6033dd2a8fe40ca01ab05692";

			Menu[] menus = 
			{
				A.Menu.WithLinkId(channelLinkId).Build(),
				A.Menu.WithLinkId(channelLinkId).Build()
			};

			IExternalSystemLogRepository externalSystemLogRepositorySpy = A.ExternalSystemLogRepositoryMock.Build();

			MenuUpdateWebHook menuUpdateWebHook = A.MenuUpdateWebHook
				.W(A.ExternalSystemLogger
					.W(externalSystemLogRepositorySpy)
					.Build())
				.W(A.ExternalSystemRepositoryMock
					.W(A.ExternalSystemEntity
						.WithChannelLinkId(channelLinkId)
						.W(Enums.ExternalSystemEnvironment.Test)
						.Build())
					.Build())
				.Build();

			Payload payload = A.MenuArrayPayload
				.W(menus)
				.Build();

			DeliverectController sut = A.DeliverectController
				.W(A.HttpContext
					.W(A.HmacSignature
						.W((HmacKey) channelLinkId)
						.W(payload)
						.Build())
					.W(await A.Stream
						.W(payload)
						.BuildAsync())
					.Build())
				.Build();


			// Act
			await sut.MenuPush(menuUpdateWebHook, menus);

			// Assert
			Mock.Get(externalSystemLogRepositorySpy).Verify(x => x.SaveAsync(It.Is<ExternalSystemLogEntity>(e => e.Success)), Times.Exactly(2));
			Mock.Get(externalSystemLogRepositorySpy).Verify(x => x.SaveAsync(It.Is<ExternalSystemLogEntity>(e => !e.Success)), Times.Never);
		}

		[Test]
		public async Task MenuPush_WithChannelLinkIdAndTestEnvironmentAndInvalidSignature_ReturnsNull()
		{
			// Arrange
			const string channelLinkId = "6033dd2a8fe40ca01ab05692";
			HmacKey keyProvidedByEvilPersonTryingToFoolUs = A.HmacKey.Build();

			Menu[] menus = 
			{
				A.Menu.WithLinkId(channelLinkId).Build(),
				A.Menu.WithLinkId(channelLinkId).Build()
			};

			IExternalSystemLogRepository externalSystemLogRepositorySpy = A.ExternalSystemLogRepositoryMock.Build();

			MenuUpdateWebHook menuUpdateWebHook = A.MenuUpdateWebHook
				.W(A.ExternalSystemLogger
					.W(externalSystemLogRepositorySpy)
					.Build())
				.W(A.ExternalSystemRepositoryMock
					.W(A.ExternalSystemEntity
						.WithChannelLinkId(channelLinkId)
						.W(Enums.ExternalSystemEnvironment.Test)
						.Build())
					.Build())
				.Build();

			Payload payload = A.MenuArrayPayload
				.W(menus)
				.Build();

			DeliverectController sut = A.DeliverectController
				.W(A.HttpContext
					.W(A.HmacSignature
						.W(keyProvidedByEvilPersonTryingToFoolUs)
						.W(payload)
						.Build())
					.W(await A.Stream
						.W(payload)
						.BuildAsync())
					.Build())
				.Build();


			// Act
			await sut.MenuPush(menuUpdateWebHook, menus);

			// Assert
			Mock.Get(externalSystemLogRepositorySpy).Verify(x => x.SaveAsync(It.Is<ExternalSystemLogEntity>(e => !e.Success)), Times.Once);
			Mock.Get(externalSystemLogRepositorySpy).Verify(x => x.SaveAsync(It.Is<ExternalSystemLogEntity>(e => e.Success)), Times.Never);
		}

		[Test]
		public async Task MenuPush_WithoutRequestBody_DoesNotSaveLog()
		{
			// Arrange
			const string channelLinkId = "6033dd2a8fe40ca01ab05692";

			Menu[] menus = 
			{
				A.Menu.WithLinkId(channelLinkId).Build(),
				A.Menu.WithLinkId(channelLinkId).Build()
			};

			IExternalSystemLogRepository externalSystemLogRepositorySpy = A.ExternalSystemLogRepositoryMock.Build();

			MenuUpdateWebHook menuUpdateWebHook = A.MenuUpdateWebHook
				.W(A.ExternalSystemLogger
					.W(externalSystemLogRepositorySpy)
					.Build())
				.W(A.ExternalSystemRepositoryMock
					.W(A.ExternalSystemEntity
						.WithChannelLinkId(channelLinkId)
						.W(Enums.ExternalSystemEnvironment.Test)
						.Build())
					.Build())
				.Build();

			Payload payload = A.MenuArrayPayload
				.W(menus)
				.Build();

			DeliverectController sut = A.DeliverectController
				.W(A.HttpContext
					.W(A.HmacSignature
						.W((HmacKey) channelLinkId)
						.W(payload)
						.Build())
					.W((Stream) null)
					.Build())
				.Build();


			// Act
			await sut.MenuPush(menuUpdateWebHook, menus);

			// Assert
			Mock.Get(externalSystemLogRepositorySpy).Verify(x => x.SaveAsync(It.IsAny<ExternalSystemLogEntity>()), Times.Never);
		}

		[Test]
		public async Task OrderStatusUpdate_WithChannelLinkIdAndTestEnvironmentAndValidSignature_ReturnsResult()
		{
			// Arrange
			string hmacKey = A.DeliverectChannelUpdatePayload.HmacKey;

			OrderStatusRequest orderStatusRequest = A.OrderStatusRequest.Build();

			OrderStatusUpdateWebHook orderStatusUpdateWebHook = A.OrderStatusUpdateWebHook
				.W(A.ExternalSystemRepositoryMock
					.W(A.ExternalSystemEntity
						.WithChannelLinkId(hmacKey)
						.W(Enums.ExternalSystemEnvironment.Test)
						.Build())
					.Build())
				.Build();

			DeliverectController sut = A.DeliverectController
				.W(A.HttpContext
					.W(A.DeliverectChannelUpdatePayload.HmacSignature)
					.W(await A.Stream
						.W(A.DeliverectChannelUpdatePayload.Body)
						.BuildAsync())
					.Build())
				.Build();

			// Act
			ActionResult result = await sut.OrderStatusUpdate(orderStatusUpdateWebHook, orderStatusRequest);

			// Assert
			Assert.IsAssignableFrom<OkResult>(result);
		}

		[Test]
		public async Task OrderStatusUpdate_WithChannelLinkIdAndTestEnvironmentAndInvalidSignature_ReturnsBadRequest()
		{
			// Arrange
			string hmacKey = A.DeliverectChannelUpdatePayload.HmacKey;

			OrderStatusRequest orderStatusRequest = A.OrderStatusRequest.Build();

			OrderStatusUpdateWebHook orderStatusUpdateWebHook = A.OrderStatusUpdateWebHook
				.W(A.ExternalSystemRepositoryMock
					.W(A.ExternalSystemEntity
						.WithChannelLinkId(hmacKey)
						.W(Enums.ExternalSystemEnvironment.Test)
						.Build())
					.Build())
				.Build();

			DeliverectController sut = A.DeliverectController
				.W(A.HttpContext
					.W("Greetings by dr. Evil")
					.W(await A.Stream
						.W(A.DeliverectChannelUpdatePayload.Body)
						.BuildAsync())
					.Build())
				.Build();

			// Act
			ActionResult result = await sut.OrderStatusUpdate(orderStatusUpdateWebHook, orderStatusRequest);

			// Assert
			Assert.IsAssignableFrom<BadRequestObjectResult>(result);
		}

		[Test]
		public async Task ToggleSnoozeProducts_WithChannelLinkIdAndTestEnvironmentAndValidSignature_ReturnsResult()
		{
			// Arrange
			string hmacKey = A.DeliverectChannelUpdatePayload.HmacKey;

			SnoozeUnsnoozeRequest snoozeUnsnoozeRequest = A.SnoozeUnsnoozeRequest.Build();

			SnoozeUnsnoozeWebHook orderStatusUpdateWebHook = A.SnoozeUnsnoozeWebHook
				.W(A.ExternalSystemRepositoryMock
					.W(A.ExternalSystemEntity
						.WithChannelLinkId(hmacKey)
						.W(Enums.ExternalSystemEnvironment.Test)
						.Build())
					.Build())
				.Build();

			DeliverectController sut = A.DeliverectController
				.W(A.HttpContext
					.W(A.DeliverectChannelUpdatePayload.HmacSignature)
					.W(await A.Stream
						.W(A.DeliverectChannelUpdatePayload.Body)
						.BuildAsync())
					.Build())
				.Build();

			// Act
			ActionResult result = await sut.ToggleSnoozeProducts(orderStatusUpdateWebHook, snoozeUnsnoozeRequest);

			// Assert
			Assert.IsAssignableFrom<OkObjectResult>(result);
		}

		[Test]
		public async Task ToggleSnoozeProducts_WithChannelLinkIdAndTestEnvironmentAndInvalidSignature_ReturnsBadRequest()
		{
			// Arrange
			string hmacKey = A.DeliverectChannelUpdatePayload.HmacKey;

			SnoozeUnsnoozeRequest snoozeUnsnoozeRequest = A.SnoozeUnsnoozeRequest.Build();

			SnoozeUnsnoozeWebHook orderStatusUpdateWebHook = A.SnoozeUnsnoozeWebHook
				.W(A.ExternalSystemRepositoryMock
					.W(A.ExternalSystemEntity
						.WithChannelLinkId(hmacKey)
						.W(Enums.ExternalSystemEnvironment.Test)
						.Build())
					.Build())
				.Build();

			DeliverectController sut = A.DeliverectController
				.W(A.HttpContext
					.W("Greetings by dr. Evil")
					.W(await A.Stream
						.W(A.DeliverectChannelUpdatePayload.Body)
						.BuildAsync())
					.Build())
				.Build();

			// Act
			ActionResult result = await sut.ToggleSnoozeProducts(orderStatusUpdateWebHook, snoozeUnsnoozeRequest);

			// Assert
			Assert.IsAssignableFrom<BadRequestObjectResult>(result);
		}
	}
}
