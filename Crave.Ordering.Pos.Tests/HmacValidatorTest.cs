using Crave.Integrations.POS.Deliverect.Domain.Models;
using Crave.Integrations.POS.Deliverect.Domain.Requests;
using Crave.Integrations.POS.Deliverect.Domain.UseCases;
using Crave.Ordering.Pos.Tests.Builders;
using NUnit.Framework;

namespace Crave.Ordering.Pos.Tests
{
	public class HmacValidatorTest
	{
		private static class A
		{
			public static HmacSignatureBuilder HmacSignature => new HmacSignatureBuilder();
			public static UntypedPayloadBuilder UntypedPayload => new UntypedPayloadBuilder();
			public static HmacKeyBuilder HmacKey => new HmacKeyBuilder();
			public static ValidateHmacUseCaseBuilder ValidateHmacUseCase => new ValidateHmacUseCaseBuilder();
			public static DeliverectChannelUpdatePayload DeliverectChannelUpdatePayload => new DeliverectChannelUpdatePayload();
		}

		[Test]
		public void IsValidHmac_WithValidHmac_ReturnsTrue()
		{
			// Arrange
			var payload = A.DeliverectChannelUpdatePayload;

			IValidateHmacUseCase sut = A.ValidateHmacUseCase
				.Build();

			string signature = A.HmacSignature
				.W((HmacKey)payload.HmacKey)
				.W(payload.Body)
				.Build();

			ValidateHmacRequest request = new ValidateHmacRequest(payload.Body, signature, payload.HmacKey);

			// Act
			bool result = sut.Execute(request);

			// Assert
			Assert.True(result);
		}

		[Test]
		public void IsValidHmac_WithInvalidHmac_ReturnsFalse()
		{
			// Arrange
			HmacKey ourKey = A.HmacKey.Build();
			HmacKey theirKey = A.HmacKey.Build();
			Payload payload = A.UntypedPayload.Build();
			
			IValidateHmacUseCase sut = A.ValidateHmacUseCase
				.Build();

			string signature = A.HmacSignature
				.W(theirKey)
				.W(payload)
				.Build();

			ValidateHmacRequest request = new ValidateHmacRequest(payload, signature, ourKey);

			// Act
			bool result = sut.Execute(request);

			// Assert
			Assert.False(result);
		}

		[Test]
		public void IsValidHmac_WithValidHmacButNullPayload_ReturnsFalse()
		{
			// Arrange
			HmacKey key = A.HmacKey.Build();
			Payload theirPayload = A.UntypedPayload.Build();
			
			IValidateHmacUseCase sut = A.ValidateHmacUseCase
				.Build();

			string signature = A.HmacSignature
				.W(key)
				.W(theirPayload)
				.Build();

			ValidateHmacRequest request = new ValidateHmacRequest(null, signature, key);

			// Act
			bool result = sut.Execute(request);

			// Assert
			Assert.False(result);
		}

		[Test]
		public void IsValidHmac_WithValidHmacButEmptyPayload_ReturnsFalse()
		{
			// Arrange
			HmacKey key = A.HmacKey.Build();
			Payload ourPayload = string.Empty;
			Payload theirPayload = A.UntypedPayload.Build();
			
			IValidateHmacUseCase sut = A.ValidateHmacUseCase
				.Build();

			string signature = A.HmacSignature
				.W(key)
				.W(theirPayload)
				.Build();

			ValidateHmacRequest request = new ValidateHmacRequest(ourPayload, signature, key);

			// Act
			bool result = sut.Execute(request);

			// Assert
			Assert.False(result);
		}

		[Test]
		public void IsValidHmac_WithValidHmacButDifferentPayload_ReturnsFalse()
		{
			// Arrange
			HmacKey key = A.HmacKey.Build();
			Payload ourPayload = A.UntypedPayload.Build();
			Payload theirPayload = A.UntypedPayload.WithStatus("SELECT * FROM Users WHERE UserId = 105 OR 1=1;").Build();
			
			IValidateHmacUseCase sut = A.ValidateHmacUseCase
				.Build();

			string signature = A.HmacSignature
				.W(key)
				.W(theirPayload)
				.Build();

			ValidateHmacRequest request = new ValidateHmacRequest(ourPayload, signature, key);

			// Act
			bool result = sut.Execute(request);

			// Assert
			Assert.False(result);
		}

		[Test]
		public void IsValidHmac_WithValidHmacButEmptySignature_ReturnsFalse()
		{
			// Arrange
			HmacKey key = A.HmacKey.Build();
			Payload ourPayload = A.UntypedPayload.Build();
			
			IValidateHmacUseCase sut = A.ValidateHmacUseCase
				.Build();

			string signature = string.Empty;

			ValidateHmacRequest request = new ValidateHmacRequest(ourPayload, signature, key);

			// Act
			bool result = sut.Execute(request);

			// Assert
			Assert.False(result);
		}

		[Test]
		public void IsValidHmac_WithExtremelyLongKey_ReturnsTrue()
		{
			// Arrange
			HmacKey key = A.HmacKey.W(string.Create(262144, ' ', (keySpan,value) => keySpan.Fill(value))).Build();
			Payload payload = A.UntypedPayload.Build();
			
			IValidateHmacUseCase sut = A.ValidateHmacUseCase
				.Build();

			string signature = A.HmacSignature
				.W(key)
				.W(payload)
				.Build();

			ValidateHmacRequest request = new ValidateHmacRequest(payload, signature, key);

			// Act
			bool result = sut.Execute(request);

			// Assert
			Assert.True(result);
		}

		[Test]
		public void IsValidHmac_WithKeyWithOddCharacterCount_ReturnsTrue()
		{
			// Arrange
			HmacKey key = string.Create(63, 'a', (keySpan,value) => keySpan.Fill(value));
			Payload payload = A.UntypedPayload.Build();
			
			IValidateHmacUseCase sut = A.ValidateHmacUseCase
				.Build();

			string signature = A.HmacSignature
				.W(key)
				.W(payload)
				.Build();

			ValidateHmacRequest request = new ValidateHmacRequest(payload, signature, key);

			// Act
			bool result = sut.Execute(request);

			// Assert
			Assert.True(result);
		}
	}
}
