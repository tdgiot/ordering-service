﻿using System.Collections.Generic;
using Amazon.CDK.AWS.APIGateway;
using Constructs;
using OrderingServiceInfra.Constructs;
using MethodOptions = OrderingServiceInfra.Constants.MethodOptions;
using Resource = Amazon.CDK.AWS.APIGateway.Resource;

namespace OrderingServiceInfra.Extensions
{
    public static class ResourceExtensions
    {
        public static void AddLambdaProxy(this Resource resource, Construct scope, Lambda.LambdaProps props)
        {
            Lambda lambda = new Lambda(scope, $"{props.Name}-Lambda", props);

            resource.AddProxy(new ProxyResourceOptions
            {
                AnyMethod = true,
                DefaultIntegration = new LambdaIntegration(lambda.Function)
            });
        }

        public static void AddSqsLambdaMethod(this Resource resource, Construct scope, string httpMethod, SqsLambda.SqsLambdaProps props)
        {
            SqsLambda sqsLambda = new SqsLambda(scope, $"{props.Name}-Sqs-Lambda", props);

            Integration integration = new AwsIntegration(new AwsIntegrationProps
            {
                Service = "sqs",
                IntegrationHttpMethod = httpMethod,
                Region = props.Region,
                Path = sqsLambda.Sqs.Queue.QueueUrl,
                Options = new IntegrationOptions
                {
                    CredentialsRole = props.GatewayRole,
                    RequestParameters = new Dictionary<string, string>
                    {
                        { "integration.request.header.Content-Type", "'application/x-www-form-urlencoded'"}
                    },
                    RequestTemplates = new Dictionary<string, string>
                    {
                        { "application/json", $"Action=SendMessage&QueueUrl=$util.urlEncode('{sqsLambda.Sqs.Queue.QueueUrl}')&MessageBody=$util.urlEncode($input.body)"}
                    },
                    IntegrationResponses = new IIntegrationResponse[]
                    {
                        new IntegrationResponse
                        {
                            StatusCode = "200"
                        }
                    }
                }
            });

            resource.AddMethod(httpMethod, integration, MethodOptions.Success);
        }
    }
}
