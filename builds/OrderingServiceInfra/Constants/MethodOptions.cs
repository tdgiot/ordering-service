﻿using System.Net;
using Amazon.CDK.AWS.APIGateway;

namespace OrderingServiceInfra.Constants
{
    public static class MethodOptions
    {
        public static readonly IMethodOptions Success = new Amazon.CDK.AWS.APIGateway.MethodOptions { MethodResponses = new IMethodResponse[] {new MethodResponse {StatusCode = "200"}}};
    }
}
