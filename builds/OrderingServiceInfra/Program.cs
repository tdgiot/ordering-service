﻿using System;
using Amazon.CDK;
using Crave.Infrastructure;
using Crave.Infrastructure.Models;
using OrderingServiceInfra.Constants;
using Environment = Amazon.CDK.Environment;

namespace OrderingServiceInfra
{
    internal sealed class Program
    {
        public static void Main()
        {
            CraveEnvironment craveEnvironment = CraveEnvironments.GetDeploymentEnvironment();

            Console.WriteLine("Deploy Environment: {0}", craveEnvironment.Environment);

            Environment deployEnvironment = new Environment
            {
                Account = craveEnvironment.AccountId,
                Region = craveEnvironment.Region
            };
            
            App app = new App();
            OrderingServiceInfraStack stack = new OrderingServiceInfraStack(app, "OrderingServiceStack", craveEnvironment, new StackProps
            {
                Env = deployEnvironment
            });

            Tags.Of(stack).Add(CraveTags.ProjectId, "ordering-service");
            Tags.Of(stack).Add(CraveTags.EnvironmentType, craveEnvironment.Environment.ToString());

            app.Synth();
        }
    }
}
