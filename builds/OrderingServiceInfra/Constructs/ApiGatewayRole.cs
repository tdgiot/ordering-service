﻿using Amazon.CDK.AWS.IAM;
using Constructs;

namespace OrderingServiceInfra.Constructs
{
    public class ApiGatewayRole : Construct
    {
        public Role Role { get; }

        public ApiGatewayRole(Construct scope, string id) : base(scope, id)
        {
            Role = new Role(this, "Gateway-Role", new RoleProps
            {
                AssumedBy = new ServicePrincipal("apigateway.amazonaws.com")
            });
        }
    }
}
