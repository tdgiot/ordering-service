﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Amazon.CDK;
using Amazon.CDK.AWS.EC2;
using Amazon.CDK.AWS.ElasticBeanstalk;
using Amazon.CDK.AWS.IAM;
using Amazon.CDK.AWS.S3.Assets;
using Constructs;
using Crave.Infrastructure;

namespace OrderingServiceInfra.Constructs
{
    public class OrderingApiBeanstalkProps
    {
        public string ApplicationName { get; set; } = "OrderingApiBeanstalk";
        public string InstanceTypes { get; set; } = "t3.small";
        public int AutoScalingMinSize { get; set; } = 1;
        public int AutoScalingMaxSize { get; set; } = 1;
        public string SslCertificateArn { get; set; } = string.Empty;
        public string CnamePrefix { get; set; }
    }

    public class OrderingApiBeanstalk : Construct
    {
        private readonly CfnEnvironment environment;

        public string EndpointUrl => this.environment.AttrEndpointUrl;

        public OrderingApiBeanstalk(Construct scope, string id, OrderingApiBeanstalkProps props) : base(scope, id)
        {
            // Create new role used for spawning and managing autoscaling
            Role ebRole = new Role(this, "BeanstalkEC2Role", new RoleProps
            {
                AssumedBy = new ServicePrincipal("ec2.amazonaws.com"),
                RoleName = "crave-ordering-ec2-service-role",
                Description = "Elastic Beanstalk role for Ordering Service API"
            });
            ebRole.AddManagedPolicy(ManagedPolicy.FromAwsManagedPolicyName("AWSElasticBeanstalkWebTier"));

            // Create new role used by the beanstalk environment
            Role ebServiceRole = new Role(this, "BeanstalkServiceRole", new RoleProps
            {
                AssumedBy = new ServicePrincipal("elasticbeanstalk.amazonaws.com"),
                RoleName = "crave-ordering-beanstalk-service-role",
                Description = "Elastic Beanstalk service role for Ordering Service API"
            });
            ebServiceRole.AddManagedPolicy(ManagedPolicy.FromAwsManagedPolicyName("service-role/AWSElasticBeanstalkEnhancedHealth"));
            ebServiceRole.AddManagedPolicy(ManagedPolicy.FromAwsManagedPolicyName("service-role/AWSElasticBeanstalkService"));

            // Create instance profile which will be linked to the EC2 machines. Is needed to actually let the machine start.
            CfnInstanceProfile ebInstanceProfile = new CfnInstanceProfile(this, "BeanStalkInstanceProfile", new CfnInstanceProfileProps
            {
                InstanceProfileName = ebRole.RoleName,
                Roles = new[] { ebRole.RoleName }
            });

            // Start configuration of Elastic Beanstalk application and environment
            CfnApplication app = new CfnApplication(this, "Application", new CfnApplicationProps
            {
                ApplicationName = props.ApplicationName
            });

            // Lookup our main VPC
            IVpc craveVpc = Vpc.FromLookup(this, "crave-vpc", new VpcLookupOptions
            {
                VpcName = CraveVpcs.CraveVpc.VpcName
            });

            // Options usable for configuration can be found on:
            // https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/command-options-general.html
            List<CfnEnvironment.OptionSettingProperty> options = new List<CfnEnvironment.OptionSettingProperty>
            {
                CreateSettingProperty("aws:autoscaling:launchconfiguration", "IamInstanceProfile", ebInstanceProfile.AttrArn),
                CreateSettingProperty("aws:autoscaling:asg", "MinSize", props.AutoScalingMinSize.ToString()),
                CreateSettingProperty("aws:autoscaling:asg", "MaxSize", props.AutoScalingMaxSize.ToString()),

                CreateSettingProperty("aws:ec2:instances", "InstanceTypes", props.InstanceTypes),
                CreateSettingProperty("aws:ec2:vpc", "VPCId", craveVpc.VpcId),
                CreateSettingProperty("aws:ec2:vpc", "Subnets", string.Join(",", craveVpc.PrivateSubnets.Select(x => x.SubnetId))),
                CreateSettingProperty("aws:ec2:vpc", "ELBSubnets", string.Join(",", craveVpc.PublicSubnets.Select(x => x.SubnetId))),
                CreateSettingProperty("aws:ec2:vpc", "AssociatePublicIpAddress", "false"),

                CreateSettingProperty("aws:elasticbeanstalk:environment", "ServiceRole", ebServiceRole.RoleArn),
                CreateSettingProperty("aws:elasticbeanstalk:environment", "LoadBalancerType", "application"),
                CreateSettingProperty("aws:elasticbeanstalk:environment:process:default", "HealthCheckPath", "/health"),
                CreateSettingProperty("aws:elasticbeanstalk:healthreporting:system", "ConfigDocument", "{\"Rules\": { \"Environment\": { \"Application\": { \"ApplicationRequests4xx\": { \"Enabled\": false } }, \"ELB\": { \"ELBRequests4xx\": {\"Enabled\": false } } } }, \"Version\": 1 }"),
                CreateSettingProperty("aws:elasticbeanstalk:managedactions", "ManagedActionsEnabled", "true"),
                CreateSettingProperty("aws:elasticbeanstalk:managedactions","PreferredStartTime", "Tue:03:00"),
                //CreateSettingProperty("aws:elasticbeanstalk:managedactions","ServiceRoleForManagedUpdates", ebServiceRole.RoleName),
                CreateSettingProperty("aws:elasticbeanstalk:managedactions:platformupdate", "UpdateLevel", "minor"),
                CreateSettingProperty("aws:elasticbeanstalk:xray", "XRayEnabled", "true")
            };

            // Set rolling and batch update parameters when running multiple instances
            if (props.AutoScalingMaxSize > 1)
            {
                options.Add(CreateSettingProperty("aws:autoscaling:updatepolicy:rollingupdate", "RollingUpdateEnabled", "true"));
                options.Add(CreateSettingProperty("aws:autoscaling:updatepolicy:rollingupdate", "RollingUpdateType", "Health"));
                options.Add(CreateSettingProperty("aws:autoscaling:updatepolicy:rollingupdate", "MaxBatchSize", "1"));
                options.Add(CreateSettingProperty("aws:autoscaling:updatepolicy:rollingupdate", "MinInstancesInService", "1"));

                options.Add(CreateSettingProperty("aws:elasticbeanstalk:command", "DeploymentPolicy", "Rolling"));
                options.Add(CreateSettingProperty("aws:elasticbeanstalk:command", "BatchSizeType", "Fixed"));
                options.Add(CreateSettingProperty("aws:elasticbeanstalk:command", "BatchSize", "1"));
            }

            // When SSL certificate has been supplied, disable listener on port 80 and
            // create new HTTPS listener on 443 linked to certificate
            if (!string.IsNullOrWhiteSpace(props.SslCertificateArn))
            {
                options.Add(CreateSettingProperty("aws:elbv2:listener:default", "ListenerEnabled", "false"));
                options.Add(CreateSettingProperty("aws:elbv2:listener:443", "Protocol", "HTTPS"));
                options.Add(CreateSettingProperty("aws:elbv2:listener:443", "SSLCertificateArns", props.SslCertificateArn));
            }

            // Pack published application assets. This will zip and upload them to S3
            Asset orderingApiAsset = new Asset(this, "OrderingApiZipped", new AssetProps
            {
                Path = Path.Combine(Directory.GetCurrentDirectory(), "artifacts/Crave.Ordering.Api.WebApplication")
            });

            // Create a new application version based on the above created asset
            CfnApplicationVersion applicationVersion = new CfnApplicationVersion(this, "AppVersion", new CfnApplicationVersionProps
            {
                ApplicationName = props.ApplicationName,
                SourceBundle = new CfnApplicationVersion.SourceBundleProperty
                {
                    S3Bucket = orderingApiAsset.S3BucketName,
                    S3Key = orderingApiAsset.S3ObjectKey
                }
            });

            // Create the actual Elastic Beanstalk environment
            // This will auto-create things like auto-scaling groups, load-balancer and EC2 machines
            // https://docs.aws.amazon.com/elasticbeanstalk/latest/platforms/platforms-supported.html#platforms-supported.dotnetlinux
            this.environment = new CfnEnvironment(this, "Environment", new CfnEnvironmentProps
            {
                EnvironmentName = $"{props.ApplicationName}-Linux",
                ApplicationName = props.ApplicationName,
                SolutionStackName = "64bit Amazon Linux 2 v2.2.1 running .NET Core",
                OptionSettings = options.ToArray(),
                VersionLabel = applicationVersion.Ref,
                CnamePrefix = props.CnamePrefix
            });

            // Make sure stuff is created in the correct order
            applicationVersion.AddDependsOn(app);

            ebInstanceProfile.AddDependsOn(ebRole.Node.DefaultChild as CfnRole ?? throw new InvalidOperationException());
            environment.AddDependsOn(ebInstanceProfile);
            environment.AddDependsOn(ebServiceRole.Node.DefaultChild as CfnRole ?? throw new InvalidOperationException());
            environment.AddDependsOn(app);
        }

        private static CfnEnvironment.OptionSettingProperty CreateSettingProperty(string optionNamespace, string optionName, string value)
        {
            return new CfnEnvironment.OptionSettingProperty
            {
                Namespace = optionNamespace,
                OptionName = optionName,
                Value = value
            };
        }
    }
}