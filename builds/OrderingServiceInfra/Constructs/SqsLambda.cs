﻿using Amazon.CDK.AWS.IAM;
using Amazon.CDK.AWS.Lambda;
using Constructs;

namespace OrderingServiceInfra.Constructs
{
    public class SqsLambda : Construct
    {
        public class SqsLambdaProps
        {
            public string Name { get; set; }
            public string Region { get; set; } = "eu-west-1";
            public IRole GatewayRole { get; set; }
            public IRole LambdaRole { get; set; }
            public string Handler { get; set; }
            public Code Code { get; set; }
            public double? ReservedConcurrentExecutions { get; set; } = 1;
        }

        public Lambda Lambda { get; }
        public Sqs Sqs { get; }

        public SqsLambda(global::Constructs.Construct scope, string id, SqsLambdaProps props) : base(scope, id)
        {
            Lambda = new Lambda(this, $"{props.Name}-Lambda", new Lambda.LambdaProps
            {
                Name = props.Name,
                Region = props.Region,
                Role = props.LambdaRole,
                Code = props.Code,
                Handler = props.Handler,
                ReservedConcurrentExecutions = props.ReservedConcurrentExecutions
            });

            Sqs = new Sqs(this, $"{props.Name}-Sqs", new Sqs.SqsProps
            {
                Name = props.Name,
                Role = props.GatewayRole
            });

            Lambda.Function.AddEventSource(new Amazon.CDK.AWS.Lambda.EventSources.SqsEventSource(Sqs.Queue));
        }
    }
}
