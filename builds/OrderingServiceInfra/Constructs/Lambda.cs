﻿using Amazon.CDK;
using Amazon.CDK.AWS.IAM;
using Amazon.CDK.AWS.Lambda;
using Constructs;

namespace OrderingServiceInfra.Constructs
{
    public class Lambda : Construct
    {
        public class LambdaProps
        {
            public string Name { get; set; }
            public string Region { get; set; } = "eu-west-1";
            public IRole Role { get; set; }
            public string Handler { get; set; }
            public Code Code { get; set; }
            public double? ReservedConcurrentExecutions { get; set; }
        }

        public Function Function { get; }

        public Lambda(Construct scope, string id, LambdaProps props) : base(scope, id)
        {
            Function = new Function(this, $"{props.Name}-Lambda", new FunctionProps
            {
                FunctionName = $"{props.Name}Lambda",
                Runtime = Runtime.DOTNET_CORE_3_1,
                Code = props.Code,
                Handler = props.Handler,
                Role = props.Role,
                ReservedConcurrentExecutions = props.ReservedConcurrentExecutions,
                Timeout = Duration.Seconds(30),
                MemorySize = 512,
                Tracing = Tracing.ACTIVE,
                Layers = new[]
                {
                    LayerVersion.FromLayerVersionArn(this, "LayerFromArn", $"arn:aws:lambda:{props.Region}:580247275435:layer:LambdaInsightsExtension:2")
                }
            });
        }
    }
}
