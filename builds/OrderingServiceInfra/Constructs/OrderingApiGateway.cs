﻿using Amazon.CDK.AWS.APIGateway;
using Amazon.CDK.AWS.Lambda;
using Crave.Infrastructure;
using Crave.Infrastructure.CDK.Extensions;
using Constructs;
using Crave.Ordering.Shared.Gateway.Constants;
using OrderingServiceInfra.Extensions;

namespace OrderingServiceInfra.Constructs
{
    public class OrderingGatewayProps
    {
        public string Region { get; set; }
    }

    public class OrderingApiGateway : Construct
    {
        public IRestApi RestApi { get; }

        public OrderingApiGateway(Construct scope, string id, OrderingGatewayProps props) : base(scope, id)
        {
            ApiGatewayRole apiGatewayRole = new ApiGatewayRole(this, "Ordering-Service-Gateway-Role");
            LambdaRole lambdaRole = new LambdaRole(this, "Ordering-Service-Lambda-Role");

            RestApi = new RestApi(this, "Ordering-Rest-Api", new RestApiProps
            {
                RestApiName = "Ordering Service",
                Description = "API Gateway for Ordering Service",
                EndpointConfiguration = new EndpointConfiguration
                {
                    Types = new[]
                    {
                        EndpointType.REGIONAL
                    }
                },
                DeployOptions = new StageOptions
                {
                    TracingEnabled = true
                }
            });

            Amazon.CDK.AWS.APIGateway.Resource events = RestApi.Root.AddResource(RouteConstants.EVENTS);

            events
                .AddResource(RouteConstants.COMPLETEROUTE)
                .AddSqsLambdaMethod(this, "POST", new SqsLambda.SqsLambdaProps
                {
                    Name = "CompleteRoute",
                    Region = props.Region,
                    LambdaRole = lambdaRole.Role,
                    GatewayRole = apiGatewayRole.Role,
                    Handler = "Crave.Ordering.CompleteRoute.Lambda::Crave.Ordering.CompleteRoute.Lambda.Function::FunctionHandler",
                    Code = Code.FromAsset("artifacts/Crave.Ordering.CompleteRoute.Lambda")
                });

            events
                .AddResource(RouteConstants.PROCESSROUTE)
                .AddSqsLambdaMethod(this, "POST", new SqsLambda.SqsLambdaProps
                {
                    Name = "ProcessRoute",
                    Region = props.Region,
                    LambdaRole = lambdaRole.Role,
                    GatewayRole = apiGatewayRole.Role,
                    Handler = "Crave.Ordering.ProcessRoute.Lambda::Crave.Ordering.ProcessRoute.Lambda.Function::FunctionHandler",
                    Code = Code.FromAsset("artifacts/Crave.Ordering.ProcessRoute.Lambda")
                });

            Amazon.CDK.AWS.APIGateway.Resource pos = RestApi.Root.AddResource(RouteConstants.POS);
            Amazon.CDK.AWS.APIGateway.Resource deliverect = pos.AddResource(RouteConstants.DELIVERECT);

            deliverect
                .AddLambdaProxy(this, new Lambda.LambdaProps
                {
                    Name = "Deliverect",
                    Region = props.Region,
                    Role = lambdaRole.Role,
                    Handler = "Crave.Ordering.Pos.Deliverect.Api::Crave.Ordering.Pos.Deliverect.Api.LambdaEntryPoint::FunctionHandlerAsync",
                    Code = Code.FromAsset("artifacts/Crave.Ordering.Pos.Deliverect.Api")
                });
        }
    }
}