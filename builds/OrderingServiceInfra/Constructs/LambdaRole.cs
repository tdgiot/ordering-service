﻿using Amazon.CDK.AWS.IAM;
using Constructs;

namespace OrderingServiceInfra.Constructs
{
    public class LambdaRole : Construct
    {
        public Role Role { get; }

        public LambdaRole(Construct scope, string id) : base(scope, id)
        {
            Role = new Role(this, "Lambda-Role", new RoleProps
            {
                AssumedBy = new ServicePrincipal("lambda.amazonaws.com")
            });

            Role.AddManagedPolicy(ManagedPolicy.FromAwsManagedPolicyName("service-role/AWSLambdaBasicExecutionRole"));
            Role.AddManagedPolicy(ManagedPolicy.FromAwsManagedPolicyName("CloudWatchLambdaInsightsExecutionRolePolicy"));
        }
    }
}
