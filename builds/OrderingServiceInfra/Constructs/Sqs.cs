﻿using Amazon.CDK.AWS.IAM;
using Amazon.CDK.AWS.SQS;
using Constructs;

namespace OrderingServiceInfra.Constructs
{
    public class Sqs : Construct
    {
        public class SqsProps
        {
            public string Name { get; set; }
            public IRole Role { get; set; }
        }

        public Queue Queue { get; }

        public Sqs(Construct scope, string id, SqsProps props) : base(scope, id)
        {
            Queue = new Queue(this, $"{props.Name}-Queue", new QueueProps
            {
                QueueName = $"{props.Name}Queue"
            });
            Queue.GrantSendMessages(props.Role);
        }
    }
}
