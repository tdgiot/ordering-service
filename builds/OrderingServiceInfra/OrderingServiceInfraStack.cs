using Amazon.CDK;
using Amazon.CDK.AWS.Route53;
using Amazon.JSII.Runtime.Deputy;
using Constructs;
using Crave.Infrastructure;
using Crave.Infrastructure.CDK.Constructs;
using Crave.Infrastructure.CDK.Extensions;
using Crave.Infrastructure.Enums;
using Crave.Infrastructure.Mappings;
using Crave.Infrastructure.Models;
using Crave.Ordering.Shared.Gateway.Constants;
using OrderingServiceInfra.Constructs;

namespace OrderingServiceInfra
{
    public class OrderingServiceInfraStack : Stack
    {
        private const string ORDERING_API = "OrderingApi";

        internal OrderingServiceInfraStack(Construct scope, string id, CraveEnvironment craveEnvironment, IStackProps? props = null) : base(scope, id, props)
        {
            AddOrderingApi(craveEnvironment);
            AddOrderingGateway(craveEnvironment);
        }

        private void AddOrderingApi(CraveEnvironment craveEnvironment)
        {
            ElasticBeanstalk orderingApiBeanstalk = new ElasticBeanstalk(this, "OrderingApi", GetApiPropsForEnvironment(craveEnvironment.Environment));

            ARecord orderingApiRecord = new ARecord(this, "HostedZone-A-Record", new ARecordProps
            {
                RecordName = "ordering-api",
                Target = RecordTarget.FromAlias(new BeanstalkTarget(new AliasRecordTargetConfig
                {
                    DnsName = orderingApiBeanstalk.EnvironmentEndpointUrlAttr,
                    HostedZoneId = AwsLoadBalancingServiceEndpoints.GetServiceEndpoint(craveEnvironment.Region).HostedZoneIdApplicationLoadBalancer,
                })),
                Zone = HostedZone.FromLookup(this, "HostedZone", new HostedZoneProviderProps
                {
                    DomainName = craveEnvironment.DomainNames.External
                })
            });

            orderingApiRecord.Node.AddDependency(orderingApiBeanstalk);
        }

        private void AddOrderingGateway(CraveEnvironment craveEnvironment)
        {
            OrderingApiGateway orderingGateway = new OrderingApiGateway(this, "Ordering-Service-Gateway", new OrderingGatewayProps
            {
                Region = craveEnvironment.Region
            });

            orderingGateway.RestApi.AddBasePathMappingForCraveCloud(this, "Ordering-Gateway-Mapping", RouteConstants.ORDERING);
        }

        private ElasticBeanstalk.ElasticBeanstalkProps GetApiPropsForEnvironment(EnvironmentType environmentType)
        {
            ElasticBeanstalk.ElasticBeanstalkProps props = environmentType switch
            {
                EnvironmentType.Production => this.ProductionProperties,
                EnvironmentType.Staging => this.ProductionProperties,
                _ => this.DevelopmentProperties
            };

            props.CnamePrefix = $"{OrderingServiceInfraStack.ORDERING_API}-{environmentType}".ToLower();
            props.HealthEndpoint = "/health";
            props.Type = ElasticBeanstalk.ElasticBeanstalkType.WebTier;
            props.PublishedArtifactsFolder = "artifacts/Crave.Ordering.Api.WebApplication";
            props.SolutionStackName = "64bit Amazon Linux 2 v2.3.4 running .NET Core";

            try
            {
                props.SslCertificateArn = CraveCertificates.GetCraveCloudXyzCertificateArn(environmentType);
            }
            catch
            {
                // Catch exception for when we want to do a test deploy to the playground
                props.SslCertificateArn = string.Empty;
            }

            return props;
        }

        private ElasticBeanstalk.ElasticBeanstalkProps DevelopmentProperties => new ElasticBeanstalk.ElasticBeanstalkProps
        {
            ApplicationName = OrderingServiceInfraStack.ORDERING_API,
            AutoScalingMinSize = 1,
            AutoScalingMaxSize = 1,
            InstanceTypes = "t3.small"
        };

        private ElasticBeanstalk.ElasticBeanstalkProps ProductionProperties => new ElasticBeanstalk.ElasticBeanstalkProps
        {
            ApplicationName = OrderingServiceInfraStack.ORDERING_API,
            AutoScalingMinSize = 2,
            AutoScalingMaxSize = 2,
            InstanceTypes = "t3.small"
        };
    }

    public class BeanstalkTarget : DeputyBase, IAliasRecordTarget
    {
        private readonly IAliasRecordTargetConfig aliasRecordTargetConfig;

        public BeanstalkTarget(IAliasRecordTargetConfig aliasRecordTargetConfig) : base(new DeputyProps(new object[] { }))
        {
            this.aliasRecordTargetConfig = aliasRecordTargetConfig;
        }

        public IAliasRecordTargetConfig Bind(IRecordSet record, IHostedZone? zone = null)
        {
            return aliasRecordTargetConfig;
        }
    }
}
